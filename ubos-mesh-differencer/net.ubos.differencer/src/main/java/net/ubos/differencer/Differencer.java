//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.differencer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;
import net.ubos.importer.ImporterException;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.externalized.json.DefaultMeshBaseJsonImporter;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.peertalk.externalized.json.PeerTalkJsonEncoder;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.util.logging.Log;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleNotFoundException;
import org.diet4j.core.ModuleResolutionException;

/**
 * Main program.
 */
public class Differencer
{
    /**
     * main() method.
     *
     * @param args command-line arguments passed in by diet4j
     * @return desired exit code
     * @throws ModuleActivationException A Module could not be activated
     * @throws ModuleNotFoundException a Module could not be found
     * @throws ModuleResolutionException a Module could not be resolved
     */
    public static int main(
            String [] args )
        throws
            ModuleActivationException,
            ModuleNotFoundException,
            ModuleResolutionException
    {
        ArgumentParser argParser = ArgumentParsers.newFor( "ubos-mesh-differencer" ).build()
                .defaultHelp( true )
                .description( "Compare two UBOS Data Mesh files and emit the difference." );

        argParser.addArgument( "--input" )
                .required( true )
                .help( "Name of the source " + EXT + " file" );

        MutuallyExclusiveGroup baselineGroup = argParser.addMutuallyExclusiveGroup();
        baselineGroup.required( true );

        baselineGroup.addArgument( "--baseline" )
                .help( "Name of the baseline " + EXT + " file" );

        baselineGroup.addArgument( "--baselineempty" )
                .setDefault( false )
                .action(Arguments.storeTrue())
                .help( "Name of the baseline " + EXT + " file" );

        argParser.addArgument( "--output" )
                .required( true )
                .help( "Name of the destination " + EXT + " file" );

        argParser.addArgument( "--emitold" )
                .setDefault( false )
                .action(Arguments.storeTrue())
                .help( "Emit the old values (off by default)");

        argParser.addArgument( "--emitnew" )
                .setDefault( false )
                .action(Arguments.storeTrue())
                .help( "Emit the new values (off by default)");

        Namespace ns;
        try {
            ns = argParser.parseArgs( args );

        } catch( ArgumentParserException ex ) {
            argParser.handleError( ex );
            return 1;
        }

        File    sourceFile    = new File( ns.getString( "input" )); // we are certain it exists
        File    destFile      = new File( ns.getString( "output" ));
        File    baseFile      = null;
        Boolean emitOldValues = ns.getBoolean( "emitold" );
        Boolean emitNewValues = ns.getBoolean( "emitnew" );

        if( ns.getString( "baseline" ) != null ) {
            baseFile = new File( ns.getString( "baseline" ));
        }
        if( emitOldValues == null ) {
            emitOldValues = Boolean.FALSE;
        }
        if( emitNewValues == null ) {
            emitNewValues = Boolean.FALSE;
        }

        if( !sourceFile.canRead() ) {
            return fatal( "Cannot read source file: " + sourceFile.getAbsolutePath() );
        }

        //

        MPrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();

        DefaultMeshBaseJsonImporter importer = DefaultMeshBaseJsonImporter.create();

        MMeshBase sourceBase = MMeshBase.Builder.create().namespaceMap( primaryNsMap ).build();
        MMeshBase baseBase;

        try {
            importer.importTo( sourceFile, null, sourceBase );

        } catch( ImporterException ex ) {
            return fatal( ex );
        } catch( IOException ex ) {
            return fatal( ex );
        }

        if( baseFile == null ) {
            // baseline empty
            baseBase = new MMeshBase.Builder() {
                        {
                            theInitializeHomeObject = false; // stay out of the public API
                        }
                }.namespaceMap( primaryNsMap )
                 .defaultNamespace( sourceBase.getDefaultNamespace() )
                 .build();

        } else {
            try {
                baseBase = MMeshBase.Builder.create().namespaceMap( primaryNsMap ).build();

                importer.importTo( baseFile, null, baseBase );

            } catch( ImporterException ex ) {
                return fatal( ex );
            } catch( IOException ex ) {
                return fatal( ex );
            }
        }

        MeshBaseDifferencer differencer = MeshBaseDifferencer.create( baseBase );
        ChangeList          changes     = differencer.determineChangeList( sourceBase );

        PeerTalkJsonEncoder encoder = PeerTalkJsonEncoder.create();

        MContextualMeshObjectIdentifierNamespaceMap exportNsMap = MContextualMeshObjectIdentifierNamespaceMap.create( primaryNsMap );

        try( Writer w = new BufferedWriter( new FileWriter( destFile, StandardCharsets.UTF_8 ))) {
            encoder.writePeerTalk( changes, emitOldValues, emitNewValues, exportNsMap, w );
            w.flush();

        } catch( EncodingException ex ) {
            return fatal( ex );
        } catch( IOException ex ) {
            return fatal( ex );
        }
        return 0;
    }

    /**
     * Factored-out fatal method.
     *
     * @param args the message and parameters
     * @return the desired exit code
     */
    static int fatal(
            Object ... args )
    {
        Log.getLogInstance( Differencer.class ).fatal( args );
        return 1;
    }

    /**
     * Factored-out error method
     *
     * @param args the message and parameters
     */
    static void error(
            Object ... args )
    {
        Log.getLogInstance( Differencer.class ).error( args );
    }

    /**
     * Factored-out warn method
     *
     * @param args the message and parameters
     */
    static void warn(
            Object ... args )
    {
        Log.getLogInstance( Differencer.class ).warn( args );
    }

    /**
     * Factored-out info method
     *
     * @param args the message and parameters
     */
    static void info(
            Object ... args )
    {
        Log.getLogInstance( Differencer.class ).info( args );
    }

    /**
     * Default file extension.
     */
    public static final String EXT = ".mesh";
}
