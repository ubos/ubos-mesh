//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.admin.meshchk;

import java.io.File;
import java.io.IOException;

/**
 * The equivalent of fsck for MeshBases.
 */
public class Main {
    /**
     * Main program
     *
     * @param args the command line arguments
     */
    public static void main(
            String [] args )
    {
        boolean checkMissingNeighbors                = false;
        boolean checkMissingTypes                    = false;
        boolean checkMultiplicities                  = false;
        boolean checkValues                          = false;
        boolean checkReferentialIntegrity            = false;
        boolean removeMissingNeighbors               = false;
        boolean removeMissingTypes                   = false;
        boolean assignDefaultsToMandatoryNulls       = false;
        boolean assignDefaultsIfIncompatibleDataType = false;
        String  dbUser                               = null;
        String  dbPassword                           = null;
        String  meshObjectsTable                     = null;
        String  namespaceMapTable                    = null;
        String  dbConnectString                      = null;

        try {
            for( int i = 0; i < args.length ; ++i ) {
                switch( args[i].toLowerCase() ) {
                    case "--checkmissingneighbors":
                        checkMissingNeighbors = true;
                        break;
                    case "--checkmissingtypes":
                        checkMissingTypes = true;
                        break;
                    case "--checkmultiplicities":
                        checkMultiplicities = true;
                        break;
                    case "--checkvalues":
                        checkValues = true;
                        break;
                    case "--checkreferentialintegrity":
                        checkReferentialIntegrity = true;
                        break;
                    case "--removemissingneighbors":
                        removeMissingNeighbors = true;
                        break;
                    case "--removemissingtypes":
                        removeMissingTypes = true;
                        break;
                    case "--assigndefaultstomandatorynulls":
                        assignDefaultsToMandatoryNulls = true;
                        break;
                    case "--assigndefaultsifincompatibledatatype":
                        assignDefaultsIfIncompatibleDataType = true;
                        break;
                    case "-u":
                    case "--user":
                        if( dbUser == null ) {
                            dbUser = args[++i];
                        } else {
                            synopsisQuit();
                        }
                        break;
                    case "--password":
                        if( dbPassword == null ) {
                            dbPassword = args[++i];
                        } else {
                            synopsisQuit();
                        }
                        break;
                    case "--meshobjectstable":
                        if( meshObjectsTable == null ) {
                            meshObjectsTable = args[++i];
                        } else {
                            synopsisQuit();
                        }
                        break;
                    case "--meshobjectidentifiernamespacemaptable":
                        if( namespaceMapTable == null ) {
                            namespaceMapTable = args[++i];
                        } else {
                            synopsisQuit();
                        }
                        break;
                    default:
                        if( args[i].startsWith( "--" ) ) {
                            // unknown option
                            synopsisQuit();
                        }
                        if( dbConnectString == null ) {
                            dbConnectString = args[i];
                        } else {
                            synopsisQuit();
                        }
                        break;
                }
            }
        } catch( ArrayIndexOutOfBoundsException ex ) {
            synopsisQuit();
        }
        if( dbConnectString == null ) {
            synopsisQuit();
        }

        if( removeMissingNeighbors ) {
            checkMissingNeighbors = true;
        }
        if( removeMissingTypes ) {
            checkMissingTypes = true;
        }
        if( assignDefaultsToMandatoryNulls || assignDefaultsIfIncompatibleDataType ) {
            checkValues = true;
        }

        if( !checkMissingNeighbors && !checkMissingTypes && !checkMultiplicities && !checkValues && !checkReferentialIntegrity ) {
            // default is to check all
            checkMissingNeighbors     = true;
            checkMissingTypes         = true;
            checkMultiplicities       = true;
            checkValues               = true;
            checkReferentialIntegrity = true;
        }

        if( meshObjectsTable == null ) {
            meshObjectsTable = "MeshObjects";
        }
        if( namespaceMapTable == null ) {
            namespaceMapTable = "MeshObjectIdentifierNamespaceMap";
        }

        try {
            MeshChk theObj = MeshChk.create( dbConnectString, dbUser, dbPassword, meshObjectsTable, namespaceMapTable );

            theObj.setCheckMissingNeighbors(                checkMissingNeighbors );
            theObj.setCheckMissingTypes(                    checkMissingTypes );
            theObj.setCheckMultiplicities(                  checkMultiplicities );
            theObj.setCheckValues(                          checkValues );
            theObj.setCheckReferentialIntegrity(            checkReferentialIntegrity );
            theObj.setRemoveMissingNeighbors(               removeMissingNeighbors );
            theObj.setRemoveMissingTypes(                   removeMissingTypes );
            theObj.setAssignDefaultsToMandatoryNulls(       assignDefaultsToMandatoryNulls );
            theObj.setAssignDefaultsIfIncompatibleDataType( assignDefaultsIfIncompatibleDataType );

            theObj.run();

        } catch( IOException ex ) {
            ex.printStackTrace( System.err );
        }
    }

    static void synopsisQuit()
    {
        System.err.println( "Arguments:" );

        System.err.println( "    [--checkmissingneighbors]                : check for missing neighbor MeshObjects" );
        System.err.println( "    [--checkmissingtypes]                    : check for MeshTypes used that cannot be resolved" );
        System.err.println( "    [--checkmultiplicities]                  : check that all RoleType multiplicities are obeyed" );
        System.err.println( "    [--checkvalues]                          : check that all PropertyValues are allowed" );
        System.err.println( "    [--checkreferentialintegrity]            : check referential integrity" );
        System.err.println( "    [--removemissingneighbors]               : remove references to missing neighbor MeshObjects" );
        System.err.println( "    [--removemissingtypes]                   : remove references to MeshTypes used that cannot be resolved" );
        System.err.println( "    [--assigndefaultstomandatorynulls]       : assign default values to non-optional properties that are null" );
        System.err.println( "    [--assigndefaultsifincompatibledatatype] : assign default values to properties with values not conforming with the property's data type" );

        System.err.println( "    [--user <user>]                          : the database username to use" );
        System.err.println( "    [--password <pass>]                      : the database password to use" );

        System.err.println( "    [--meshobjectstable <table>]             : the database table containing the MeshObjects (default: MeshObjects)" );
        System.err.println( "    [--settingstable <table>]                : the database table containing the MeshBase settings (default: MeshBaseSettings)" );
        System.err.println( "    jdbc:<engine>://<host>/<database         : the JDBC database connection string" );

        System.exit( 1 );
    }

    /**
     * Default name of the table that contains the MeshObjects in the database.
     */
    public static final String DEFAULT_TABLE = "MeshObjects";
}
