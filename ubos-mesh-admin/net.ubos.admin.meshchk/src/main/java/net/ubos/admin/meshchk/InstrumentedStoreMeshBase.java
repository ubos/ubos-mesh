//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.admin.meshchk;

import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.a.AMeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObjectIdentifierFactory;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StoreContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.m.ImmutableMMeshObjectSetFactory;
import net.ubos.meshbase.a.AMeshBaseLifecycleManager;
import net.ubos.meshbase.store.MeshObjectStoreValueMapper;
import net.ubos.meshbase.store.StoreHeadMeshBaseView;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.store.StoreMeshBaseSwappingSmartMap;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.util.StoreValueMapper;

/**
 * An StoreMeshBase that has been instrumented for the purposes of Igck.
 */
public class InstrumentedStoreMeshBase
    extends
        StoreMeshBase
{
    /**
     * Factory method.
     *
     * @param meshObjectStore the Store containing the MeshObjects
     * @param localNsStore the Store containing the namespaces
     * @return the created InstrumentedStoreMeshBase
     */
    public static InstrumentedStoreMeshBase create(
                Store<StoreValue> meshObjectStore,
                Store<StoreValue> localNsStore )
    {
        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();

        if( meshObjectStore == null ) {
            throw new IllegalArgumentException( "Must provide a Store for MeshObjects" );
        }
        if( localNsStore == null ) {
            throw new IllegalArgumentException( "Must provide a Store for local namespace mappings" );
        }

        MeshObjectIdentifierNamespace defaultNamespace = primaryNsMap.getPrimary();

        AMeshObjectIdentifierFactory idFact
                = AMeshObjectIdentifierFactory.create( primaryNsMap, defaultNamespace );

        MeshObjectIdentifierDeserializer idDeserializer = ExternalNameHashMeshObjectIdentifierBothSerializer.create( primaryNsMap, idFact );

        ImmutableMMeshObjectSetFactory setFactory
                = ImmutableMMeshObjectSetFactory.create( AMeshObject.class, AMeshObjectIdentifier.class );

        StoreContextualMeshObjectIdentifierNamespaceMap localNsMap = StoreContextualMeshObjectIdentifierNamespaceMap.create(
                localNsStore,
                primaryNsMap );

        ContextualMeshObjectIdentifierBothSerializer persistenceIdSerializer
                = ContextualMeshObjectIdentifierBothSerializer.create( idFact, localNsMap );

        MeshObjectStoreValueMapper objectMapper = MeshObjectStoreValueMapper.create( persistenceIdSerializer, persistenceIdSerializer );

        MyMap objectStorage = new MyMap( objectMapper, meshObjectStore );

        StoreHeadMeshBaseView headMeshBaseView = StoreHeadMeshBaseView.create( objectStorage, idFact, idDeserializer );

        InstrumentedStoreMeshBase ret = new InstrumentedStoreMeshBase(
                headMeshBaseView,
                idFact,
                setFactory,
                AMeshBaseLifecycleManager.create(),
                persistenceIdSerializer );

        objectMapper.setMeshBaseView( ret );
        idFact.setMeshBase( ret );

        ret.initializeHomeObject();

        return ret;
    }

    /**
     * Constructor.
     *
     * @param meshObjectStorage holds the MeshObjects in this MeshBase
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBase
     * @param setFactory the factory for MeshObjectSets appropriate for this MeshBase
     * @param life the MeshBaseLifecycleManager to use
     */
    protected InstrumentedStoreMeshBase(
            StoreHeadMeshBaseView                        headMeshBaseView,
            MeshObjectIdentifierFactory                  identifierFactory,
            MeshObjectSetFactory                         setFactory,
            AMeshBaseLifecycleManager                    life,
            ContextualMeshObjectIdentifierBothSerializer persistenceIdSerializer )
    {
        super( headMeshBaseView, identifierFactory, null, setFactory, life, null, false, false, null, persistenceIdSerializer );
    }

    /**
     * Write a particular MeshObject back to disk.
     *
     * @param obj the MeshObject to write to disk
     */
    public void flush(
            MeshObject obj )
    {
        MyMap map = (MyMap) theHeadMeshBaseView.getStorage();

        map.flush( obj );
    }

    /**
     * Make some methods more accessible.
     */
    static class MyMap
        extends
            StoreMeshBaseSwappingSmartMap
    {
        /**
         * Constructor.
         *
         * @param mapper the <code>StoreValueMapper</code> to use
         * @param store the underlying <code>Store</code>
         */
        public MyMap(
                StoreValueMapper<MeshObjectIdentifier,MeshObject,StoreValue> mapper,
                Store<StoreValue>                                            store )
        {
            super( mapper, store );
        }

        /**
         * Write a particular MeshObject back to disk.
         *
         * @param obj the MeshObject to write to disk
         */
        public void flush(
                MeshObject obj )
        {
            // the saveValueToStorage does not do anything in the superclass
            super.saveValueToStorageUponCommit( obj.getIdentifier(), obj );
        }
    }
}
