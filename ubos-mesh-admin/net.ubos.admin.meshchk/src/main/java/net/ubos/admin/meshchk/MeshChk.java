//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.admin.meshchk;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.a.AMeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObjectNeighborManager;
import net.ubos.meshbase.MeshBaseError;
import net.ubos.meshbase.MeshBaseErrorListener;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.store.Store;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;

/**
 * The MeshBase integrity checker.
 */
public class MeshChk
    implements
        MeshBaseErrorListener
{
    private static final Log log = Log.getLogInstance( MeshChk.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param dbConnectString the JDBC database connection string
     * @param dbUser the database user to use
     * @param dbPassword the database password to use
     * @param meshObjectsTable the table in the database containing the MeshObjects
     * @param settingsTable the table in the database containing the settings
     * @return the created MeshChk
     * @throws IOException thrown if an input/output problem occurred
     */
    public static MeshChk create(
            String dbConnectString,
            String dbUser,
            String dbPassword,
            String meshObjectsTable,
            String settingsTable )
        throws
            IOException
    {
        // example: jdbc:mysql://localhost/test

        Pattern jdbcPattern = Pattern.compile( "(jdbc:)?([a-z0-9]+)://([^/:]+(:\\d+)?)(/([^/]+))?" );
        Matcher matcher     = jdbcPattern.matcher( dbConnectString );

        if( !matcher.matches()) {
            throw new IOException( "Cannot parse JDBC connection string: " + dbConnectString );
        }
        String dbType = matcher.group( 2 );
        String dbHost = matcher.group( 3 );
        String dbPort = matcher.group( 4 );
        String dbName = matcher.group( 6 );

        MysqlStore meshObjectStore;
        MysqlStore settingsStore;
        if( "mysql".equals( dbType) ) {
            MysqlDataSource dataSource = new MysqlDataSource();
            if( dbHost != null && !dbHost.isEmpty() ) {
                dataSource.setServerName(   dbHost );
            }
            if( dbPort != null && !dbPort.isEmpty() ) {
                dataSource.setPortNumber(   Integer.parseInt( dbPort ));
            }
            if( dbName != null && !dbName.isEmpty() ) {
                dataSource.setDatabaseName( dbName );
            }
            if( dbUser != null && !dbUser.isEmpty() ) {
                dataSource.setUser(         dbUser );
            }
            if( dbPassword != null && !dbPassword.isEmpty() ) {
                dataSource.setPassword(     dbPassword );
            }

            meshObjectStore = MysqlStore.create( dataSource, meshObjectsTable );
            settingsStore   = MysqlStore.create( dataSource, settingsTable );

        } else {
            throw new IOException( "Cannot identify database engine for connection string " + dbConnectString );
        }

        InstrumentedStoreMeshBase mb = InstrumentedStoreMeshBase.create( meshObjectStore, settingsStore );

        return new MeshChk( mb );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param mb the MeshBase to be tested
     */
    protected MeshChk(
            InstrumentedStoreMeshBase mb )
    {
        theMeshBase = mb;
    }

    /**
     * Set the checkMissingNeighbors flag.
     *
     * @param newValue the new value
     */
    public void setCheckMissingNeighbors(
            boolean newValue )
    {
        theCheckMissingNeighbors = newValue;
    }

    /**
     * Set the checkMissingTypes flag.
     *
     * @param newValue the new value
     */
    public void setCheckMissingTypes(
            boolean newValue )
    {
        theCheckMissingTypes = newValue;
    }

    /**
     * Set the checkMultiplicities flag.
     *
     * @param newValue the new value
     */
    public void setCheckMultiplicities(
            boolean newValue )
    {
        theCheckMultiplicities = newValue;
    }

    /**
     * Set the checkValues flag.
     *
     * @param newValue the new value
     */
    public void setCheckValues(
            boolean newValue )
    {
        theCheckValues = newValue;
    }

    /**
     * Set the checkReferentialIntegrity flag.
     *
     * @param newValue the new value
     */
    public void setCheckReferentialIntegrity(
            boolean newValue )
    {
        theCheckReferentialIntegrity = newValue;
    }

    /**
     * Set the removeMissingNeighbors flag.
     *
     * @param newValue the new value
     */
    public void setRemoveMissingNeighbors(
            boolean newValue )
    {
        theRemoveMissingNeighbors = newValue;
    }

    /**
     * Set the removeMissingTypes flag.
     *
     * @param newValue the new value
     */
    public void setRemoveMissingTypes(
            boolean newValue )
    {
        theRemoveMissingTypes = newValue;
    }

    /**
     * Set the assignDefaultsToMandatoryNulls flag.
     *
     * @param newValue the new value
     */
    public void setAssignDefaultsToMandatoryNulls(
            boolean newValue )
    {
        theAssignDefaultsToMandatoryNulls = newValue;
    }

    /**
     * Set the assignDefaultsIfIncompatibleDataType flag.
     *
     * @param newValue the new value
     */
    public void setAssignDefaultsIfIncompatibleDataType(
            boolean newValue )
    {
        theAssignDefaultsIfIncompatibleDataType = newValue;
    }

    /**
     * Set the verbosity level. Higher means more verbose.
     *
     * @param newValue the new level
     */
    public void setVerbose(
            int newValue )
    {
        theVerbose = newValue;
    }

    /**
     * Run the operation.
     */
    public void run()
    {
        theMeshBase.addDirectErrorListener( this );

        theMeshObjectCount = 0;
        theErrorCount      = 0;
        theFixedCount      = 0;

        theMeshBase.iterator().batchForEach(
                512,
                (MeshObject current) -> runOne( (AMeshObject) current ));

        if( theErroneousCount == 0 ) {
            System.out.printf(
                    "Congratulations, no errors found in %d MeshObjects.\n",
                    theMeshObjectCount );

        } else if( theFixedCount > 0 ) {
            System.out.printf("Found %d erroneous out of %d MeshObjects (%02.1f%%), %d errors total, fixed %d.\n",
                    theErroneousCount,
                    theMeshObjectCount,
                    100.f * theErroneousCount / theMeshObjectCount,
                    theErrorCount,
                    theFixedCount );

        } else {
            System.out.printf("Found %d erroneous out of %d MeshObjects (%02.1f%%), %d errors total.\n",
                    theErroneousCount,
                    theMeshObjectCount,
                    100.f * theErroneousCount / theMeshObjectCount,
                    theErrorCount );
        }
        theMeshBase.removeErrorListener( this );
    }

    /**
     * Check a single MeshObject.
     *
     * @param current the MeshObject to check
     */
    protected void runOne(
            AMeshObject current )
    {
        ++theMeshObjectCount;
        if( theCheckMissingNeighbors ) {
            AMeshObjectIdentifier []   neighborIds = current.getNeighborMeshObjectIdentifiers();
            Set<AMeshObjectIdentifier> toRemove    = theRemoveMissingNeighbors ? new HashSet<>( neighborIds.length ) : null;

            for( int i=0 ; i<neighborIds.length ; ++i ) {
                if( neighborIds[i] == null ) {
                    addToHaveErrors( current.getIdentifier() );
                    error(  current.getIdentifier(),
                            "null neighbor identifier (" + i + "/" + neighborIds.length + ")",
                            "updated: " + TimeStampValue.create( current.getTimeUpdated() ),
                            "(type " + ArrayHelper.arrayToString( current.getEntityTypes(), (EntityType t) -> theTypeIdSerializer.toExternalForm( t.getIdentifier())) + ")" );

                    if( toRemove != null ) {
                        toRemove.add( neighborIds[i] );
                    }
                } else {
                    MeshObject neighbor = theMeshBase.findMeshObjectByIdentifier( neighborIds[i] );
                    if( neighbor == null ) {
                        addToHaveErrors( current.getIdentifier() );
                        error(  current.getIdentifier(),
                                "neighbor (" + i + "/" + neighborIds.length + ") cannot be found:",
                                "updated: " + TimeStampValue.create( current.getTimeUpdated() ),
                                theIdSerializer.toExternalForm( neighborIds[i] ),
                                "(type " + arrayToString( current.getEntityTypes() ) + ")" );

                        if( toRemove != null ) {
                            toRemove.add( neighborIds[i] );
                        }
                    }
                }
            }
            if( theRemoveMissingNeighbors && toRemove != null && !toRemove.isEmpty() ) {
                if( current instanceof AMeshObject ) {
                    for( AMeshObjectIdentifier id : toRemove ) {
                        int index = NM.determineRelationshipIndex( (AMeshObject) current, id );
                        NM.removeNeighbor( (AMeshObject) current, index );
                    }
                    theHaveBeenFixed.add( current.getIdentifier() );

                    info( current.getIdentifier(), "removed " + toRemove.size() + " unresolvable neighbor(s)" );

                } else {
                    log.warn( "Cannot remove", toRemove.size() + "neighbor(s), not an AMeshObject:", theIdSerializer.toExternalForm( current.getIdentifier()));
                }
            }
        }
        if( theCheckReferentialIntegrity ) {
            MeshObjectIdentifier [] neighborIds = current.getNeighborMeshObjectIdentifiers();

            for( int i=0 ; i<neighborIds.length ; ++i ) {
                AMeshObject neighbor = (AMeshObject) theMeshBase.findMeshObjectByIdentifier( neighborIds[i] );
                if( !neighbor.isRelated( current.getIdentifier() )) {
                    addToHaveErrors( current.getIdentifier() );

                    error(  current.getIdentifier(),
                            "lists " + theIdSerializer.toExternalForm( neighborIds[i] ) + " as neighbor, but neighbor is not pointing back",
                            "updated: " + TimeStampValue.create( current.getTimeUpdated() ) + " and " + TimeStampValue.create( neighbor.getTimeUpdated() ),
                            "(type " + arrayToString( current.getEntityTypes())
                                    + " and " + arrayToString( neighbor.getEntityTypes())
                                    + ", roles " + arrayToString( current.getRoleTypes( neighbor ))+ ")" );

                } else {
                    RoleType [] hereRoles  = current.getRoleTypes( neighbor );
                    RoleType [] thereRoles = neighbor.getRoleTypes( current );
                    if( !ArrayHelper.hasSameContentOutOfOrder( hereRoles, thereRoles, (RoleType one, RoleType two ) -> one.getInverseRoleType().equals( two ) )) {
                        addToHaveErrors( current.getIdentifier() );
                        error(   current.getIdentifier(),
                                "has different roles than corresponding roles of neighbor " + theIdSerializer.toExternalForm( neighborIds[i] ),
                                "updated: " + TimeStampValue.create( current.getTimeUpdated() ) + " and " + TimeStampValue.create( neighbor.getTimeUpdated() ),
                                "( " + arrayToString( hereRoles )
                                     + " vs " + arrayToString( thereRoles ) + " )" );
                    }
                }
            }
        }
        if( theCheckMultiplicities ) {
            for( EntityType entityType : current.getEntityTypes()) {
                for( RoleType roleType : entityType.getRoleTypes()) {
                    MeshObjectIdentifier [] others = current.getNeighborMeshObjectIdentifiersFor( roleType );

                    MultiplicityValue mult = roleType.getMultiplicity();
                    if(    mult.getMinimum() > others.length
                        || ( mult.getMaximum() != MultiplicityValue.N && others.length > mult.getMaximum() ))
                    {
                        addToHaveErrors( current.getIdentifier() );
                        error(  current.getIdentifier(),
                                "RoleType " + theTypeIdSerializer.toExternalForm( roleType.getIdentifier()) + " (" + roleType.getMultiplicity().toString() + ") has " + others.length,
                                "updated: " + TimeStampValue.create( current.getTimeUpdated() ),
                                "(type " + arrayToString( current.getEntityTypes()) + ")" );
                    }
                }
            }
        }
        if( theHaveBeenFixed.remove( current.getIdentifier() )) {
            theMeshBase.flush( current );
            ++theFixedCount;
        }
        Integer haveErrors = theHaveErrors.get( current.getIdentifier() );
        if( haveErrors != null && haveErrors > 0 ) {
            theErrorCount += haveErrors;
            ++theErroneousCount;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unresolveableEntityType(
            MeshBaseError.UnresolvableEntityType event )
    {
        if( theCheckMissingTypes ) {
            addToHaveErrors( event.getMeshObject().getIdentifier() );
            error(  event.getMeshObject().getIdentifier(),
                    "unknown EntityType " + theTypeIdSerializer.toExternalForm( event.getMeshTypeIdentifier()),
                    "updated: " + TimeStampValue.create( event.getMeshObject().getTimeUpdated() ));

            if( theRemoveMissingTypes ) {
                // just writing it back will do this
                theHaveBeenFixed.add( event.getMeshObject().getIdentifier() );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unresolveableRoleType(
            MeshBaseError.UnresolvableRoleType event )
    {
        if( theCheckMissingTypes ) {
            addToHaveErrors( event.getMeshObject().getIdentifier() );
            error(  event.getMeshObject().getIdentifier(),
                    "unknown RoleType " + theTypeIdSerializer.toExternalForm( event.getMeshTypeIdentifier()),
                    "updated: " + TimeStampValue.create( event.getMeshObject().getTimeUpdated() ));

            if( theRemoveMissingTypes ) {
                // just writing it back will do this
                theHaveBeenFixed.add( event.getMeshObject().getIdentifier() );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unresolveablePropertyType(
            MeshBaseError.UnresolvablePropertyType event )
    {
        if( theCheckMissingTypes ) {
            addToHaveErrors( event.getMeshObject().getIdentifier() );
            error(  event.getMeshObject().getIdentifier(),
                    "unknown PropertyType " + theTypeIdSerializer.toExternalForm( event.getMeshTypeIdentifier()),
                    "updated: " + TimeStampValue.create( event.getMeshObject().getTimeUpdated() ));

            if( theRemoveMissingTypes ) {
                // just writing it back will do this
                theHaveBeenFixed.add( event.getMeshObject().getIdentifier() );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incompatibleDataType(
            MeshBaseError.IncompatibleDataType event )
    {
        if( theCheckValues ) {
            addToHaveErrors( event.getMeshObject().getIdentifier() );
            error(  event.getMeshObject().getIdentifier(),
                    "value " + event.getPropertyValue() + " incompatible with type " + event.getPropertyType().getDataType() + " of PropertyType " + event.getPropertyType(),
                    "updated: " + TimeStampValue.create( event.getMeshObject().getTimeUpdated() ));

            if( theAssignDefaultsIfIncompatibleDataType ) {
                // just writing it back will do this
                theHaveBeenFixed.add( event.getMeshObject().getIdentifier() );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propertyNotOptional(
            MeshBaseError.PropertyNotOptional event )
    {
        if( theCheckValues ) {
            addToHaveErrors( event.getMeshObject().getIdentifier() );
            error(  event.getMeshObject().getIdentifier(),
                    "PropertyType " + theTypeIdSerializer.toExternalForm( event.getPropertyType().getIdentifier()) + " does not allow null values",
                    "updated: " + TimeStampValue.create( event.getMeshObject().getTimeUpdated() ));

            if( theAssignDefaultsToMandatoryNulls ) {
                // just writing it back will do this
                theHaveBeenFixed.add( event.getMeshObject().getIdentifier() );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void otherError(
            MeshBaseError.OtherError event )
    {
        log.error( event );
    }

    /**
     * Report an error.
     *
     * @param id identifier of the affected MeshObject
     * @param msgs the messages in increasing verbosity
     */
    protected void error(
            MeshObjectIdentifier id,
            Object ...           msgs )
    {
        if( theVerbose == 0 ) {
            System.err.println( theIdSerializer.toExternalForm( id ));
        } else {
            StringBuilder msg = new StringBuilder();
            msg.append( "Error: " );
            msg.append( theIdSerializer.toExternalForm( id ) );
            for( int i=0 ; i<theVerbose && i<msgs.length; ++i ) {
                msg.append( ' ' );
                msg.append( msgs[i] );
            }
            System.err.println( msg );
        }
    }

    /**
     * Report an informational message.
     *
     * @param id identifier of the affected MeshObject
     * @param msgs the messages in increasing verbosity
     */
    protected void info(
            MeshObjectIdentifier id,
            Object ...           msgs )
    {
        if( theVerbose == 0 ) {
            System.err.println( theIdSerializer.toExternalForm( id ));
        } else {
            StringBuilder msg = new StringBuilder();
            msg.append( "Info: " );
            msg.append( theIdSerializer.toExternalForm( id ) );
            for( int i=0 ; i<theVerbose && i<msgs.length; ++i ) {
                msg.append( ' ' );
                msg.append( msgs[i] );
            }
            System.out.println( msg );
        }
    }

    /**
     * Helper method to turn an array into a String.
     *
     * @param array the array
     * @return the String
     */
    protected String arrayToString(
            MeshObject [] array )
    {
        return ArrayHelper.arrayToString( array, "", " / ", "", (MeshObject o) -> theIdSerializer.toExternalForm( o.getIdentifier() ) );
    }

    /**
     * Helper method to turn an array into a String.
     *
     * @param array the array
     * @return the String
     */
    protected String arrayToString(
            MeshType [] array )
    {
        return ArrayHelper.arrayToString( array, "", " / ", "", (MeshType t) -> theTypeIdSerializer.toExternalForm( t.getIdentifier() ) );
    }

    /**
     * Utility method to increment the value in the theHaveErrors hash.
     *
     * @param id the MeshObjectIdentifier
     */
    protected void addToHaveErrors(
            MeshObjectIdentifier id )
    {
        Integer found = theHaveErrors.get( id );
        if( found != null ) {
            theHaveErrors.put( id, found + 1 );
        } else {
            theHaveErrors.put( id, 1 );
        }
    }

    /**
     * The MeshBase to be tested.
     */
    protected InstrumentedStoreMeshBase theMeshBase;

    /**
     * If true, check for missing neighbors.
     */
    protected boolean theCheckMissingNeighbors;

    /**
     * If true, check for missing types.
     */
    protected boolean theCheckMissingTypes;

    /**
     * If true, check for invalid multiplicities.
     */
    protected boolean theCheckMultiplicities;

    /**
     * If true, check PropertyValues.
     */
    protected boolean theCheckValues;

    /**
     * If true, check that all relationships are bidirectional.
     */
    protected boolean theCheckReferentialIntegrity;

    /**
     * If true, remove missing neighbors.
     */
    protected boolean theRemoveMissingNeighbors;

    /**
     * If true, remove missing types.
     */
    protected boolean theRemoveMissingTypes;

    /**
     * If true, assign default values to non-optional properties whose value
     * is currently null.
     */
    protected boolean theAssignDefaultsToMandatoryNulls;

    /**
     * If true, assign default values to properties whose current value does not
     * conform to the property's data type.
     */
    protected boolean theAssignDefaultsIfIncompatibleDataType;

    /**
     * The verbosity level.
     */
    protected int theVerbose;

    /**
     * Running counter for examined MeshObjects.
     */
    protected int theMeshObjectCount;

    /**
     * Running counter for errors.
     */
    protected int theErrorCount;

    /**
     * Running counter for MeshObjects with at least one error.
     */
    protected int theErroneousCount;

    /**
     * Running counter for the number of MeshObjects that were fixed.
     */
    protected int theFixedCount;

    /**
     * Flag errors from callbacks. Keep those around until the main processing
     * loop comes around to catch up with processing MeshObjects that had errors
     * upon deserialization from disk.
     */
    protected Map<MeshObjectIdentifier,Integer> theHaveErrors = new HashMap<>();

    /**
     * Mark a MeshObject has having to be written back to disk
     */
    protected Set<MeshObjectIdentifier> theHaveBeenFixed = new HashSet<>();

    /**
     * The NeighborManager to use.
     */
    protected static AMeshObjectNeighborManager NM = AMeshObjectNeighborManager.SINGLETON;

    /**
     * Knows how to serialize MeshTypeIdentifiers for error output purposes.
     */
    protected static final MeshTypeIdentifierSerializer theTypeIdSerializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

    /**
     * Knows how to serialize MeshObjectIdentifiers for error output purposes.
     */
    protected static final MeshObjectIdentifierSerializer theIdSerializer = ExternalNameHashMeshObjectIdentifierSerializer.SINGLETON;
}
