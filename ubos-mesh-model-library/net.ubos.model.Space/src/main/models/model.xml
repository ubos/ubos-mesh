<?xml version="1.0" encoding="UTF-8"?>

<subjectarea ID="net.ubos.model.Space">
    <name>net.ubos.model.Space</name>
    <username>space</username>
    <userdescription>The structure of homesteads.</userdescription>

    <entitytype>
        <name>Space</name>
        <username>space</username>
        <userdescription>A space in a homestead</userdescription>
        <isabstract/>

        <propertytype>
            <name>Name</name>
            <username>name</username>
            <userdescription>Name of the space.</userdescription>
            <StringDataType/>
            <isoptional/>
        </propertytype>
    </entitytype>

    <relationshiptype>
        <name>Space_Contains_Space</name>
        <username>contains</username>
        <userdescription>A space can be contained inside another space.</userdescription>
        <isabstract/>

        <src>
            <e>Space</e>
            <MultiplicityValue>0:n</MultiplicityValue>
        </src>
        <dest>
            <e>Space</e>
            <MultiplicityValue>0:1</MultiplicityValue>
        </dest>
    </relationshiptype>

    <relationshiptype>
        <name>Object_AppearsIn_Space</name>
        <username>appears in</username>
        <userdescription>A MeshObject can appear in a Space.</userdescription>

        <src>
            <MultiplicityValue>0:n</MultiplicityValue>
        </src>
        <dest>
            <e>Space</e>
            <MultiplicityValue>0:n</MultiplicityValue>
        </dest>
    </relationshiptype>

    <entitytype>
        <name>WebSpace</name>
        <username>web space</username>
        <userdescription>A space in a homestead that's represented in 2D on a web page or such.</userdescription>
        <supertype>Space</supertype>

        <propertytype>
            <name>WallSVGPath</name>
            <username>wall SVG path</username>
            <userdescription>A SVG path that renders the wall of the WebSpace.</userdescription>
            <StringDataType/>
            <isoptional/>
        </propertytype>

        <propertytype>
            <name>WallSVGOffset</name>
            <username>wall SVG offset</username>
            <userdescription>The offset for the wall SVG with respect to the center of the WebSpace.</userdescription>
            <PointDataType/>
            <isoptional/>
        </propertytype>
    </entitytype>

    <relationshiptype>
        <name>WebSpace_Contains_WebSpace</name>
        <username>contains</username>
        <userdescription>A web space can be contained inside another web space.</userdescription>

        <src>
            <supertype>Space_Contains_Space-S</supertype>
            <e>WebSpace</e>
            <MultiplicityValue>0:n</MultiplicityValue>

            <propertytype>
                <name>Position</name>
                <username>position</username>
                <userdescription>The position of the contained web space within the containing web space.</userdescription>
                <PointDataType/>
            </propertytype>
        </src>
        <dest>
            <supertype>Space_Contains_Space-D</supertype>
            <e>WebSpace</e>
            <MultiplicityValue>0:1</MultiplicityValue>
        </dest>
    </relationshiptype>

    <entitytype>
        <name>Zone</name>
        <username>zone</username>
        <userdescription>A zone inside a space.</userdescription>
        <isabstract/>

        <propertytype>
            <name>Name</name>
            <username>name</username>
            <userdescription>Name of the zone.</userdescription>
            <StringDataType/>
            <isoptional/>
        </propertytype>
    </entitytype>

    <relationshiptype>
        <name>Space_Contains_Zone</name>
        <username>contains</username>
        <userdescription>A space may contain zones.</userdescription>
        <isabstract/>

        <src>
            <e>Space</e>
            <MultiplicityValue>0:n</MultiplicityValue>
        </src>
        <dest>
            <e>Zone</e>
            <MultiplicityValue>1:1</MultiplicityValue>
        </dest>
    </relationshiptype>

    <entitytype>
        <name>WebZone</name>
        <username>web zone</username>
        <userdescription>A zone in a web space.</userdescription>

        <supertype>Zone</supertype>
        <propertytype>
            <name>WallSVGPath</name>
            <username>wall SVG path</username>
            <userdescription>A SVG path that renders the wall of the WebZone.</userdescription>
            <StringDataType/>
            <isoptional/>
        </propertytype>

        <propertytype>
            <name>WallSVGOffset</name>
            <username>wall SVG offset</username>
            <userdescription>The offset for the wall SVG with respect to the center of the WebZone.</userdescription>
            <PointDataType/>
            <isoptional/>
        </propertytype>
    </entitytype>

    <relationshiptype>
        <name>WebSpace_Contains_WebZone</name>
        <username>contains</username>
        <userdescription>A web space may contain web zones.</userdescription>

        <src>
            <supertype>Space_Contains_Zone-S</supertype>
            <e>WebSpace</e>
            <MultiplicityValue>0:n</MultiplicityValue>

            <propertytype>
                <name>Position</name>
                <username>position</username>
                <userdescription>The position of the contained web space within the containing web space.</userdescription>
                <PointDataType/>
            </propertytype>
        </src>
        <dest>
            <supertype>Space_Contains_Zone-D</supertype>
            <e>Zone</e>
            <MultiplicityValue>1:1</MultiplicityValue>
        </dest>
    </relationshiptype>

    <entitytype>
        <name>ViewletReference</name>
        <username>viewlet reference</username>
        <userdescription>Identifies a Viewlet to be used in a space, with corresponding parameters</userdescription>

        <propertytype>
            <name>ViewletName</name>
            <username>viewlet name</username>
            <userdescription>Name of the viewlet.</userdescription>
            <StringDataType/>
        </propertytype>
    </entitytype>

    <relationshiptype>
        <name>Space_Provides_ViewletReference</name>
        <username>provides</username>
        <userdescription>Identifies the Viewlets available in a Space.</userdescription>

        <src>
            <e>Space</e>
            <MultiplicityValue>0:n</MultiplicityValue>
        </src>
        <dest>
            <e>ViewletReference</e>
            <MultiplicityValue>1:1</MultiplicityValue>
        </dest>
    </relationshiptype>


    <relationshiptype>
        <name>Zone_Configures_ViewletReference</name>
        <username>configures</username>
        <userdescription>Configures a Viewlet available in a space for the settings appropriate in this particular zone.</userdescription>

        <src>
            <e>Zone</e>
            <MultiplicityValue>0:n</MultiplicityValue>

            <propertytype>
                <name>ViewletParameterString</name>
                <username>viewlet parameter string</username>
                <userdescription>Parameters for the viewlet, as string.</userdescription>
                <StringDataType/>
                <isoptional/>
            </propertytype>
        </src>
        <dest>
            <e>ViewletReference</e>
            <MultiplicityValue>0:n</MultiplicityValue>
        </dest>
    </relationshiptype>
</subjectarea>
