//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.json;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.nonblessed.NonblessedJsonUtils;
import net.ubos.mesh.nonblessed.NonblessedUtils;
import net.ubos.meshbase.MeshBase;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Knows how to handle JSON.
 */
public class DefaultJsonImporterHandler
    extends
        AbstractBasicFileImporterHandler
    implements
        NonblessedImporterHandler
{
    /**
     * Constructor with default.
     *
     * @param okScore when importing works, what score should be reported
     */
    public DefaultJsonImporterHandler(
            double okScore )
    {
        super( DEFAULT_JSON_FILENAME_PATTERN, okScore );

        theTolerateBlankFile = true;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     */
    public DefaultJsonImporterHandler(
            String  filenamePattern,
            double  okScore )
    {
        super( Pattern.compile( filenamePattern ), okScore );

        theTolerateBlankFile = true;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     * @param tolerateBlankFile if true, do not complain if the file is empty
     */
    public DefaultJsonImporterHandler(
            Pattern filenamePattern,
            double  okScore,
            boolean tolerateBlankFile )
    {
        super( filenamePattern, okScore );

        theTolerateBlankFile = tolerateBlankFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream() ) {
            return IMPOSSIBLE;
        }

        MeshObject hereMeshObject = toBeImported.getHereMeshObject();
        setRequiredTopAttributes( hereMeshObject, null );

        HasReadSomething hasReadSomething = new HasReadSomething();

        double ret;

        try {
            InputStream in = toBeImported.createStream();

            ret = jsonReaderImport( new JsonReader( new InputStreamReader( in )), hasReadSomething, null, hereMeshObject, context );

        } catch( EOFException ex ) {
            if( theTolerateBlankFile && !hasReadSomething.theFlag ) {
                return SUFFICIENT;
            } else {
                throw ex;
            }

        } catch( MalformedJsonException ex ) {
            ret = IMPOSSIBLE;
        }
        return ret;
    }

    /**
     * Knows how to import JSON from a JsonReader.
     *
     * @param jr the to-be imported stream
     * @param hasReadSomething keep track whether at least some data was read
     * @param parentObject the parent object, if any
     * @param hereObject import into here
     * @param context the context for the import
     * @return the score
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    protected double jsonReaderImport(
            JsonReader             jr,
            HasReadSomething       hasReadSomething,
            MeshObject             parentObject,
            MeshObject             hereObject,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret;

        switch( jr.peek() ) {
            case BEGIN_ARRAY:
                hasReadSomething.theFlag = true;
                ret = jsonReaderImportArray( jr, hasReadSomething, hereObject, context );
                break;

            case BEGIN_OBJECT:
                hasReadSomething.theFlag = true;
                ret = jsonReaderImportObject( jr, hasReadSomething, hereObject, context );
                break;

            case STRING:
                hasReadSomething.theFlag = true;
                setRequiredNonTopAttributes( hereObject, NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONSTRING );
                hereObject.setAttributeValue( NonblessedUtils.CONTENT_ATTRIBUTE, jr.nextString() );
                ret = theOkScore;
                break;

            case NUMBER:
                hasReadSomething.theFlag = true;
                setRequiredNonTopAttributes( hereObject, NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONNUMBER );
                // Gson does not let us determine whether a Number is specified as a Long or a Double.
                // So we parse it as a String, and then make up our minds ourselves.
                String parsed = jr.nextString();
                try {
                    if( parsed.indexOf( '.' ) >= 0 ) {
                        hereObject.setAttributeValue( NonblessedUtils.CONTENT_ATTRIBUTE, Double.parseDouble( parsed ) );
                    } else {
                        hereObject.setAttributeValue( NonblessedUtils.CONTENT_ATTRIBUTE, Long.parseLong( parsed ) );
                    }
                } catch( NumberFormatException ex ) {
                    hereObject.setAttributeValue( NonblessedUtils.CONTENT_ATTRIBUTE, jr.nextDouble() );
                }
                ret = theOkScore;
                break;

            case BOOLEAN:
                hasReadSomething.theFlag = true;
                setRequiredNonTopAttributes(hereObject, NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONBOOLEAN );
                hereObject.setAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE, jr.nextBoolean() );
                ret = theOkScore;
                break;

            case NULL:
                hasReadSomething.theFlag = true;
                jr.nextNull();
                setRequiredNonTopAttributes(hereObject, NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONNULL );
                hereObject.setAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE, null );
                ret = theOkScore;
                break;

            default:
                hasReadSomething.theFlag = true;
            // END_ARRAY
            // END_OBJECT
            // NAME
            // END_DOCUMENT
                ret = IMPOSSIBLE;
                break;

        }
        return ret;
    }

    /**
     * Knows how to import a JSON array from a JsonReader.
     *
     * @param jr the to-be imported stream
     * @param hasReadSomething keep track whether at least some data was read
     * @param hereObject import into here
     * @param context the context for the import
     * @return the score
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    protected double jsonReaderImportArray(
            JsonReader             jr,
            HasReadSomething       hasReadSomething,
            MeshObject             hereObject,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double almost = 0.0d;

        MeshBase mb = context.getMeshBase();

        setRequiredNonTopAttributes(hereObject, NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONARRAY );

        jr.beginArray();
        int count = 0;
        while( jr.hasNext() ) {
            MeshObject childObject = mb.createMeshObjectBelow( hereObject, String.valueOf( count ));
            setRequiredRoleAttributes(hereObject, childObject, NonblessedJsonUtils.RELATIONSHIPTYPE_ROLE_VALUE_JSON_CONTAINS, null, count );

            almost += jsonReaderImport( jr, hasReadSomething, hereObject, childObject, context );
            ++count;
        }
        jr.endArray();

        if( count == 0 ) {
            return theOkScore;
        } else {
            return almost / count;
        }
    }

    /**
     * Knows how to import a JSON Object from a JsonReader.
     *
     * @param jr the to-be imported stream
     * @param hasReadSomething keep track whether at least some data was read
     * @param hereObject import into here
     * @param context the context for the import
     * @return the score
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    protected double jsonReaderImportObject(
            JsonReader             jr,
            HasReadSomething       hasReadSomething,
            MeshObject             hereObject,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double almost = 0.0d;

        MeshBase mb = context.getMeshBase();

        setRequiredNonTopAttributes(hereObject, NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONOBJECT );

        jr.beginObject();
        int count = 0;
        while( jr.hasNext() ) {
            String name = jr.nextName();

            MeshObject childObject = mb.createMeshObjectBelow( hereObject, name );
            setRequiredRoleAttributes(hereObject, childObject, NonblessedJsonUtils.RELATIONSHIPTYPE_ROLE_VALUE_JSON_CONTAINS, name, null );

            almost += jsonReaderImport( jr, hasReadSomething, hereObject, childObject, context );
            ++count;
        }
        jr.endObject();

        if( count == 0 ) {
            return theOkScore;
        } else {
            return almost / count;
        }
    }

    /**
     * If true, will not complain if the file is empty.
     */
    protected final boolean theTolerateBlankFile;

    /**
     * The default pattern of file names we support.
     */
    public static final Pattern DEFAULT_JSON_FILENAME_PATTERN = Pattern.compile( ".*\\.json$", Pattern.CASE_INSENSITIVE );

    /**
     * Helper class.
     */
    static class HasReadSomething
    {
        public boolean theFlag = false;
    }
}
