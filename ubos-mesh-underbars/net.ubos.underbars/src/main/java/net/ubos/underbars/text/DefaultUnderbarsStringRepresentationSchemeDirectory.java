//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.text;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.text.StringRepresentationScheme;
import net.ubos.mesh.text.StringRepresentationSchemeDirectory;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.underbars.resources.TemplateMap;

/**
 * The StringRepresentationSchemeDirectory we use with Underbars.
 */
public class DefaultUnderbarsStringRepresentationSchemeDirectory
    implements
        StringRepresentationSchemeDirectory
{
    /**
     * Factory method.
     *
     * @param fragmentMap knows where the Handlebars templates to use are
     * @param normalIdSerializer use this to serialize MeshObjectIdentifiers for non-URLs
     * @param urlIdSerializer use this to serialize MeshObjectIdentifiers for URLs
     * @param typeIdSerializer use this to serialize MeshTypeIdentifiers
     * @return the created instance
     */
    public static DefaultUnderbarsStringRepresentationSchemeDirectory create(
            TemplateMap                        fragmentMap,
            MeshObjectIdentifierBothSerializer normalIdSerializer,
            MeshObjectIdentifierBothSerializer urlIdSerializer,
            MeshTypeIdentifierBothSerializer   typeIdSerializer )
    {
        StringRepresentationScheme plain          = PlainStringRepresentationScheme.create(          PLAIN_ENTRY,            normalIdSerializer, typeIdSerializer );
        StringRepresentationScheme shell          = PlainStringRepresentationScheme.create(          HTTP_SHELL_ENTRY,       normalIdSerializer, typeIdSerializer, plain );
        StringRepresentationScheme html           = HtmlHandlebarsStringRepresentationScheme.create( HTML_ENTRY,             normalIdSerializer, urlIdSerializer, typeIdSerializer, fragmentMap, plain );
        StringRepresentationScheme editHtml       = HtmlHandlebarsStringRepresentationScheme.create( EDIT_HTML_ENTRY,        normalIdSerializer, urlIdSerializer, typeIdSerializer, fragmentMap, html );
        StringRepresentationScheme futureEditHtml = HtmlHandlebarsStringRepresentationScheme.create( FUTURE_EDIT_HTML_ENTRY, normalIdSerializer, urlIdSerializer, typeIdSerializer, fragmentMap, plain );
        StringRepresentationScheme markup         = HtmlHandlebarsStringRepresentationScheme.create( MARKUP_ENTRY,           normalIdSerializer, urlIdSerializer, typeIdSerializer, fragmentMap, plain );
        StringRepresentationScheme xequalsyequals = HtmlHandlebarsStringRepresentationScheme.create( XEQUALSYEQUALS_ENTRY,   normalIdSerializer, urlIdSerializer, typeIdSerializer, fragmentMap, plain );

        Map<String,StringRepresentationScheme> content = new HashMap<>();
        content.put( PLAIN_ENTRY,            plain );
        content.put( HTTP_SHELL_ENTRY,       shell );
        content.put( HTML_ENTRY,             html  );
        content.put( EDIT_HTML_ENTRY,        editHtml  );
        content.put( FUTURE_EDIT_HTML_ENTRY, futureEditHtml  );
        content.put( MARKUP_ENTRY,           markup  );
        content.put( XEQUALSYEQUALS_ENTRY,   xequalsyequals );

        return new DefaultUnderbarsStringRepresentationSchemeDirectory( content, plain );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param content the content of the directory
     * @param fallback the StringRepresentationScheme to return if a named scheme cannot be found
     */
    protected DefaultUnderbarsStringRepresentationSchemeDirectory(
            Map<String,? extends StringRepresentationScheme> content,
            StringRepresentationScheme                       fallback )
    {
        theContent  = content;
        theFallback = fallback;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringRepresentationScheme findScheme(
            String schemeName )
    {
        StringRepresentationScheme ret = theContent.get( schemeName );
        if( ret == null ) {
            ret = theFallback;
        }
        return ret;
    }

    /**
     * Name of the plain text entry.
     */
    public final static String PLAIN_ENTRY = "Plain";

    /**
     * Name of the HTML entry for displaying information.
     */
    public final static String HTML_ENTRY = "Html";

    /**
     * Name of the HTML entry for editing information.
     */
    public final static String EDIT_HTML_ENTRY = "EditHtml";

    /**
     * Name of the HTML entry for editing future information (MeshObject does not exist yet)
     */
    public final static String FUTURE_EDIT_HTML_ENTRY = "FutureEditHtml";

    /**
     * Name of the Markup entry for emitting strings to be used in markup. Instead of,
     * say, the HTML_ENTRY emitting "<span>The color is red</span>", this will
     * emit the value to be usable in markup, e.g. "#ff0000".
     */
    public static final String MARKUP_ENTRY = "Markup";

    /**
     * Name of the xequalsyequals entry for emitting PointValues in format x=1 y=2 as is
     * needed for SVG text elements.
     */
    public static final String XEQUALSYEQUALS_ENTRY = "xequalsyequals";

    /**
     * Name of the Http Shell entry.
     */
    public final static String HTTP_SHELL_ENTRY = "HttpShell";

    /**
     * The content of the directory.
     */
    protected final Map<String,? extends StringRepresentationScheme> theContent;

    /**
     * The fallback entry.
     */
    protected final StringRepresentationScheme theFallback;
}
