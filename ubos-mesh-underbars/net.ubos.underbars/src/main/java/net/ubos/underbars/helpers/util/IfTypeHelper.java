//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.logging.Log;

/**
 * Execute body if the provided Object supperts a certain type.
 */
public class IfTypeHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    private static final Log log = Log.getLogInstance( IfTypeHelper.class );

    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  obj,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String  type  = options.hash( TYPE_ARGNAME );
        boolean exact = options.hash( EXACT_ARGNAME, false );

        boolean cond;
        if( exact ) {
            cond = obj.getClass().getName().equals( type );
        } else {
            cond = supportsType( obj.getClass(), type );
        }

        Options.Buffer buffer = options.buffer();

        if( cond ) {
            buffer.append(options.fn());
        } else {
            buffer.append(options.inverse());
        }
        return buffer;
    }

    /**
     * Recursively invoked helper to detect whether the
     * provided class has this type name, or any of its
     * interfaces or superclasses does.
     *
     * @param candidate the candidate class
     * @param typeName name of the type to support
     * @return true or false
     */
    protected boolean supportsType(
            Class<?> candidate,
            String   typeName )
    {
        if( candidate == null ) {
            return false;
        }
        if( candidate.getName().equals( typeName )) {
            return true;
        }
        if( supportsType( candidate.getSuperclass(), typeName )) {
            return true;
        }
        for( Class<?> current : candidate.getInterfaces() ) {
            if( supportsType( current, typeName )) {
                return true;
            }
        }
        return false;
    }

    /**
     * Name of the argument that specifies the type.
     */
    public static final String TYPE_ARGNAME = "type";

    /**
     * Name of the argument that indicates whether the type has to be met exactly.
     */
    public static final String EXACT_ARGNAME = "exact";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        TYPE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        EXACT_ARGNAME
    };
}
