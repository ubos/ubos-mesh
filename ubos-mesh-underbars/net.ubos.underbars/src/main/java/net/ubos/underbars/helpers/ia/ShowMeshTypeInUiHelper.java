//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.ia;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import static net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper.MESHTYPE_ARGNAME;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Encapsulates our decisions which EntityTypes and other MeshTypes to show in the UI, and which not.
 */
public class ShowMeshTypeInUiHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        Object   meshType = options.hash( MESHTYPE_ARGNAME );
        MeshType type;
        if( meshType instanceof String ) {
            try {
                type = ModelBase.SINGLETON.findMeshType( (String) meshType );

            } catch( ParseException ex ) {
                throw new IllegalArgumentException( ex );

            } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                throw new IllegalArgumentException( ex );
            }

        } else if( meshType instanceof MeshType ) {
            type = (MeshType) meshType;

        } else {
            throw new ClassCastException( "Unsuitable object: " + meshType );
        }

        MeshTypeIdentifier id = type.getIdentifier();
        if( "net.ubos.model.Identity".equals( id.getSubjectAreaPart() )) {
            return false;
        }
        if( "net.ubos.model.Amazon".equals( id.getSubjectAreaPart()) && "AmazonOrderLineItem".equals( id.getLocalPart() )) {
            return false;
        }

        return true;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHTYPE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}

