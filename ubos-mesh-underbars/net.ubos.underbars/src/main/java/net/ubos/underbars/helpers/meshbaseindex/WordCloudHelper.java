//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.meshbaseindex;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.OrderedSetWithValue;
import net.ubos.util.StringHelper;
import net.ubos.web.vl.StructuredResponse;

/**
 * Displays a word cloud.
 */
public class WordCloudHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        int max = options.hash( MAX_ARGNAME, 50 );

        MeshBase      mb     = (MeshBase) model.get( ViewletModel.MESHBASE_KEY );
        MeshBaseIndex index = mb.getMeshBaseIndex();

        OrderedSetWithValue<String,Long> toShow = index.wordsWithCount( max );

        String svgId = model.obtainNewDomId();

        Options.Buffer buffer = options.buffer();
        buffer.append( "<svg id=\"" )
              .append( svgId )
              .append( "\" class=\"" )
              .append( getCssClass() )
              // .append( "\" viewBox=\"0 0 100 100\" xmlns=\"http://www.w3.org/2000/svg" )
              .append( "\">\n" );
        buffer.append( "</svg>\n" );

        buffer.append( "<script>\n" );
        buffer.append( "net_ubos_underbars_WordCloudHelper_displayWordCloud( \"" ).append( svgId ).append( "\", [ " );
        
        String sep = "";
        int    i   = 0;
        for( String word : toShow ) {
            if( i >= max ) {
                break;
            }
            buffer.append( sep );
            buffer.append( "\"" ).append( word ).append( "\"" );
            sep = ", ";
        }
        buffer.append( "] );\n" );

        buffer.append( "</script>\n" );

        StructuredResponse sr = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
        sr.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars/WordCloudHelper.css" );
        sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars/lib/d3.v4.js" );
        sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars/lib/d3.layout.cloud.js" );
        sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars/WordCloudHelper.js" );

        return buffer.toString();
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MAX_ARGNAME
    };
}
