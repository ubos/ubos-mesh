//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.PagingCursorIterator;
import net.ubos.vl.CannotViewException;
import net.ubos.web.vl.PaginatingWebViewlet;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewedMeshObjects;

/**
 * Collects functionality common to HandlebarsViewlets that paginate their content.
 */
public abstract class AbstractMeshObjectPaginatingHtmlHandlebarsViewlet
    extends
        AbstractHtmlHandlebarsViewlet
    implements
        PaginatingWebViewlet<MeshObject>
{
    /**
     * Constructor, for subclasses only.
     *
     * @param viewletName the computable name of the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param viewed the WebViewedMeshObjects to use
     * @param defaultPageLength the default page length
     * @param idSerializer how to serialize MeshObjectIdentifiers for the purpose of paginating
     */
    protected AbstractMeshObjectPaginatingHtmlHandlebarsViewlet(
            String                             viewletName,
            TemplateMap                        templateMap,
            WebViewedMeshObjects               viewed,
            int                                defaultPageLength,
            MeshObjectIdentifierBothSerializer idSerializer )
    {
        super( viewletName, templateMap, viewed );

        theDefaultPageLength = defaultPageLength;
        theIdSerializer      = idSerializer;
    }

    /**
     * Obtain a CursorIterator over the to-be-paged content.
     *
     * @return the CursorIterator
     */
    protected abstract CursorIterator<MeshObject> getContentIterator();

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> getCurrentPageStartIterator()
        throws
            CannotViewException
    {
        CursorIterator<MeshObject> iter = getContentIterator();

        String pageStart = (String) theViewedMeshObjects.getViewletParameter( PAGE_START_NAME );

        // leave the CursorIterator at the place we got it, and only move it a page start has been specified
        if( pageStart != null ) {
            try {
                MeshBaseView mbv   = getViewedMeshObjects().getSubject().getMeshBaseView();
                MeshObject   start = mbv.findMeshObjectByIdentifierOrThrow( theIdSerializer.meshObjectIdentifierFromExternalForm( pageStart ));

                iter.moveToBefore( start );

            } catch( Throwable t ) {
                throw new CannotViewException.InvalidParameter( this, PAGE_START_NAME, pageStart, null );
            }
        }
        return iter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> getWithinPageIterator()
        throws
            CannotViewException
    {
        CursorIterator<MeshObject> pageStart = getCurrentPageStartIterator();
        CursorIterator<MeshObject> delegate  = pageStart.createCopy(); // don't move page start around

        return PagingCursorIterator.create( getPageSize(), delegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        int ret;

        String pageLengthString = (String) theViewedMeshObjects.getViewletParameter( PAGE_LENGTH_NAME );
        if( pageLengthString != null ) {
            ret = Integer.getInteger( pageLengthString );
            if( ret < 1 ) {
                ret = theDefaultPageLength;
            }
        } else {
                ret = theDefaultPageLength;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDestinationUrlForStart(
            MeshObject startLocation )
    {
        WebMeshObjectsToView toView = getViewedMeshObjects().getMeshObjectsToView();

        toView = toView.withViewletParameter( PAGE_START_NAME, theIdSerializer.toExternalForm( startLocation.getIdentifier()));

        return toView.asUrlString();
    }

    /**
     * Default page length.
     */
    protected final int theDefaultPageLength;

    /**
     * Knows how to serialize the MeshObjectIdentifiers we use to indicate the start of the page.
     */
    protected final MeshObjectIdentifierBothSerializer theIdSerializer;

    /**
     * Name of the Viewlet parameter indicating the MeshObject with which the current page starts.
     */
    public static final String PAGE_START_NAME = "page-start";

    /**
     * Name of the Viewlet parameter indicating the page length.
     */
    public static final String PAGE_LENGTH_NAME = "page-length";
}
