//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resources;

import net.ubos.web.assets.AssetMap;
import net.ubos.web.skin.SkinManager;

/**
 * Objects supporting this interface know how to find resources such as
 * Assets, Skins or Templates.
 */
public interface ResourceManager
{
    /**
     * Obtain a map of all assets.
     *
     * @return the asset map
     */
    public AssetMap getAssetMap();

    /**
     * Obtain the SkinManager.
     *
     * @return the skin manager
     */
    public SkinManager getSkinManager();

    /**
     * Obtain a map of all fragment templates.
     *
     * @return the map
     */
    public TemplateMap getFragmentTemplateMap();

    /**
     * Obtain the TemplateMap for a named Viewlet in this package.
     *
     * @param viewletName name of the Viewlet
     * @return the map
     */
    public TemplateMap getViewletTemplateMapFor(
            String viewletName );
}
