//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import java.io.IOException;
import com.github.jknack.handlebars.Options;
import net.ubos.model.primitives.MeshType;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Insert a MeshTypeIdentifier.
 */
public class MeshTypeIdHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshType type = getDefaultMeshType( model, options );

        return formatMeshTypeIdentifier(
                model,
                type.getIdentifier(),
                options );
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHTYPE_ARGNAME
    };
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        STRING_REPRESENTATION_ARGNAME,
        FLAVOR_ARGNAME,
        MAX_LENGTH_ARGNAME
    };
}
