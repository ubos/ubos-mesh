//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.io.Serializable;
import net.ubos.mesh.IllegalAttributeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Inserts the value of a RoleAttribute.
 */
public class RoleAttributeHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj      = getPrimaryMeshObject(  model, options );
        MeshObject neighbor = getNeighborMeshObject( model, options );

        String attributeName = options.hash( ROLE_ATTRIBUTE_NAME_ARGNAME );

        try {
            Serializable value = obj.getRoleAttributeValue( neighbor, attributeName );

            String text = formatRoleAttribute(
                    obj,
                    neighbor,
                    attributeName,
                    value,
                    model,
                    options );

            return text;

        } catch( IllegalAttributeException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( NotPermittedException ex ) {
            throw new IllegalArgumentException( ex );
        }
    }
    
    /**
     * Name of the parameter indicating the PropertyType.
     */
    public static final String ROLE_ATTRIBUTE_NAME_ARGNAME = "roleAttributeName";
    
    /**
     * Name of the parameter indicating the default value to be used.
     */
    public static final String DEFAULT_VALUE_ARGNAME = "defaultValue";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        NEIGHBOR_ARGNAME,
        ROLE_ATTRIBUTE_NAME_ARGNAME
    };
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DEFAULT_VALUE_ARGNAME,
        FLAVOR_ARGNAME,
        MAX_LENGTH_ARGNAME,
        STRING_REPRESENTATION_ARGNAME
    };    
}
