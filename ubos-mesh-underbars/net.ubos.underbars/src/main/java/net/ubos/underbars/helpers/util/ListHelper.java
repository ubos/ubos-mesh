//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * Creates a list from the arguments
 */
public class ListHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public List<?> apply(
            Object  context,
            Options options )
        throws
            IOException
    {
        ArrayList<Object> ret = new ArrayList<>();

        for( Object param : options.params ) {
            ret.add( param );
        }
        return ret;
    }
}
