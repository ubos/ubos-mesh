//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.text;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Template;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.text.AbstractStringRepresentationScheme;
import net.ubos.mesh.text.StringRepresentationScheme;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.resolvers.CalendarValueResolver;
import net.ubos.underbars.resolvers.HandlebarsModelResolver;
import net.ubos.underbars.resolvers.StructuredResponseValueResolver;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.logging.Log;
import net.ubos.util.text.StringifierParameters;

/**
 * Represent things in HTML by using Handlebars
 */
public abstract class HandlebarsStringRepresentationScheme
    extends
        AbstractStringRepresentationScheme
{
    private static final Log log = Log.getLogInstance( HandlebarsStringRepresentationScheme.class );

    /**
     * Private constructor, use factory method.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     * @param fragmentMap has the Handlebars templates
     * @param delegate the StringRepresentationScheme to delegate to if no entry was found
     */
    protected HandlebarsStringRepresentationScheme(
            String                         prefix,
            MeshObjectIdentifierSerializer idSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer,
            TemplateMap                    fragmentMap,
            StringRepresentationScheme     delegate )
    {
        super( prefix, idSerializer, typeIdSerializer, delegate );

        theFragmentMap = fragmentMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable parseAttributeValue(
            String                raw,
            String                attributeName,
            StringifierParameters pars )
        throws
            ParseException
    {
        return raw;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatAttribute(
            MeshObject            obj,
            String                attributeName,
            Serializable          currentValue,
            StringifierParameters pars )
    {
        String flavor;
        if( currentValue == null ) {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_NULL_FLAVOR );
        } else {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_FLAVOR );
        }
        String templateName;

        if( currentValue == null || currentValue instanceof CharSequence ) {
            // we use the String not Data version for null values
            templateName = thePathPrefix + "StringAttribute/" + flavor;
        } else if( currentValue instanceof Number ) {
            templateName = thePathPrefix + "NumberAttribute/" + flavor;
        } else if( currentValue instanceof Boolean ) {
            templateName = thePathPrefix + "BooleanAttribute/" + flavor;
        } else {
            templateName = thePathPrefix + "DataAttribute/" + flavor;
        }

        Template t = null;
        try {
            t = theFragmentMap.obtainFor( templateName );

        } catch( IOException ex ) {
            log.debug( ex );
        }
        if( t == null ) {
            if( theDelegate != null ) {
                return theDelegate.formatAttribute( obj, attributeName, currentValue, pars ); // this may be recursive

            } else {
                log.error( "No Template found at " + templateName );
                return "NO TEMPLATE";
            }
        }

        ViewletModel vlModel = (ViewletModel) pars.get( AbstractMeshObjectHelper.VIEWLET_MODEL_KEY );

        String shellVar     = vlModel.obtainNewShellVar();
        String shellPropVar = "1"; // FIXME?

        Map<String,Object> localPars = new HashMap<>();
        localPars.put( "meshObject", obj );

        localPars.put( "an", attributeName );
        localPars.put( "av", currentValue );

        localPars.put( "shellvar",     shellVar );
        localPars.put( "shellpropvar", shellPropVar );

        ViewletModel vlSubmodel = vlModel.createSubmodel( localPars );

        Context c = Context.newBuilder( vlSubmodel )
                .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON, CalendarValueResolver.SINGLETON )
                .build();

        try {
            String ret = t.apply( c );
            return ret;

        } catch( IOException ex ) {
            log.error( ex );

            return "ERROR";
        }
    }

     /**
     * {@inheritDoc}
     */
    @Override
    public String formatRoleAttribute(
            MeshObject            obj,
            MeshObject            neighbor,
            String                attributeName,
            Serializable          currentValue,
            StringifierParameters pars )
    {
        String flavor;
        if( currentValue == null ) {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_NULL_FLAVOR );
        } else {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_FLAVOR );
        }
        String templateName;

        if( currentValue == null || currentValue instanceof CharSequence ) {
            // we use the String not Data version for null values
            templateName = thePathPrefix + "StringAttribute/" + flavor;
        } else if( currentValue instanceof Number ) {
            templateName = thePathPrefix + "NumberAttribute/" + flavor;
        } else if( currentValue instanceof Boolean ) {
            templateName = thePathPrefix + "BooleanAttribute/" + flavor;
        } else {
            templateName = thePathPrefix + "DataAttribute/" + flavor;
        }

        Template t = null;
        try {
            t = theFragmentMap.obtainFor( templateName );

        } catch( IOException ex ) {
            log.debug( ex );
        }
        if( t == null ) {
            if( theDelegate != null ) {
                return theDelegate.formatRoleAttribute( obj, neighbor, attributeName, currentValue, pars ); // this may be recursive

            } else {
                log.error( "No Template found at " + templateName );
                return "NO TEMPLATE";
            }
        }

        ViewletModel vlModel = (ViewletModel) pars.get( AbstractMeshObjectHelper.VIEWLET_MODEL_KEY );

        String shellVar         = vlModel.obtainNewShellVar();
        String neighborShellVar = vlModel.obtainNewShellVar();
        String shellPropVar     = "1"; // FIXME?

        Map<String,Object> localPars = new HashMap<>();
        localPars.put( "meshObject", obj );
        localPars.put( "neighbor", neighbor );

        localPars.put( "an", attributeName );
        localPars.put( "av", currentValue );

        localPars.put( "shellvar",         shellVar );
        localPars.put( "shellpropvar",     shellPropVar );
        localPars.put( "neighborshellvar", neighborShellVar );

        ViewletModel vlSubmodel = vlModel.createSubmodel( localPars );

        Context c = Context.newBuilder( vlSubmodel )
                .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON, CalendarValueResolver.SINGLETON )
                .build();

        try {
            String ret = t.apply( c );
            return ret;

        } catch( IOException ex ) {
            log.error( ex );

            return "ERROR";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatProperty(
            MeshObject            obj,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars )
    {
        // short class name
        String shortClassName = propertyType.getDataType().getClass().getName();
        int    lastPeriod     = shortClassName.lastIndexOf( '.' );
        shortClassName        = shortClassName.substring( lastPeriod+1 );

        String flavor;
        if( currentValue == null ) {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_NULL_FLAVOR );
        } else {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_FLAVOR );
        }
        String templateName = thePathPrefix + shortClassName + "/" + flavor;

        Template t = null;
        try {
            t = theFragmentMap.obtainFor( templateName );

        } catch( IOException ex ) {
            log.debug( ex );
        }
        if( t == null ) {
            if( theDelegate != null ) {
                return theDelegate.formatProperty( obj, propertyType, currentValue, pars ); // this may be recursive

            } else {
                log.error( "No Template found at " + templateName );
                return "NO TEMPLATE";
            }
        }

        ViewletModel vlModel = (ViewletModel) pars.get( AbstractMeshObjectHelper.VIEWLET_MODEL_KEY );
        String       upload  = (String)       pars.get( StringifierParameters.UPLOAD_KEY, StringifierParameters.DEFAULT_UPLOAD_VALUE );

        String shellVar     = vlModel.obtainNewShellVar();
        String shellPropVar = vlModel.obtainNewShellSubVarFor( shellVar );

        Map<String,Object> localPars = new HashMap<>();
        localPars.put( "meshObject", obj );

        localPars.put( "pt", propertyType );
        localPars.put( "pv", currentValue );

        localPars.put( "shellvar",     shellVar );
        localPars.put( "shellpropvar", shellPropVar );

        localPars.put( "upload", upload );

        if( currentValue instanceof TimeStampValue ) {
            localPars.put( "pvAsCal", ((TimeStampValue)currentValue).getAsCalendar( TimeZone.getDefault() ));
        }

        ViewletModel vlSubmodel = vlModel.createSubmodel( localPars );

        Context c = Context.newBuilder( vlSubmodel )
                .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON, CalendarValueResolver.SINGLETON )
                .build();

        try {
            String ret = t.apply( c );
            return ret;

        } catch( IOException ex ) {
            log.error( ex );

            return "ERROR";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatFutureProperty(
            String                objVar,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars )
    {
        // short class name
        String shortClassName = propertyType.getDataType().getClass().getName();
        int    lastPeriod     = shortClassName.lastIndexOf( '.' );
        shortClassName        = shortClassName.substring( lastPeriod+1 );

        String flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_FLAVOR );
        String templateName = thePathPrefix + shortClassName + "/" + flavor;

        Template t = null;
        try {
            t = theFragmentMap.obtainFor( templateName );

        } catch( IOException ex ) {
            log.debug( ex );
        }
        if( t == null ) {
            if( theDelegate != null ) {
                return theDelegate.formatFutureProperty( objVar, propertyType, currentValue, pars ); // this may be recursive

            } else {
                log.error( "No Template found at " + templateName );
                return "NO TEMPLATE";
            }
        }

        ViewletModel vlModel = (ViewletModel) pars.get( AbstractMeshObjectHelper.VIEWLET_MODEL_KEY );
        String       upload  = (String)       pars.get( StringifierParameters.UPLOAD_KEY, StringifierParameters.DEFAULT_UPLOAD_VALUE );

        String shellPropVar = vlModel.obtainNewShellSubVarFor( objVar );

        Map<String,Object> localPars = new HashMap<>();

        localPars.put( "pt", propertyType );
        localPars.put( "pv", currentValue );

        localPars.put( "shellvar",     objVar );
        localPars.put( "shellpropvar", shellPropVar );

        localPars.put( "upload", upload );

        if( currentValue instanceof TimeStampValue ) {
            localPars.put( "pvAsCal", ((TimeStampValue)currentValue).getAsCalendar( TimeZone.getDefault() ));
        }

        ViewletModel vlSubmodel = vlModel.createSubmodel( localPars );

        Context c = Context.newBuilder( vlSubmodel )
                .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON, CalendarValueResolver.SINGLETON )
                .build();

        try {
            String ret = t.apply( c );
            return ret;

        } catch( IOException ex ) {
            log.error( ex );

            return "ERROR";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatRoleProperty(
            MeshObject            obj,
            MeshObject            neighbor,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars )
    {
        // short class name
        String shortClassName = propertyType.getDataType().getClass().getName();
        int    lastPeriod     = shortClassName.lastIndexOf( '.' );
        shortClassName        = shortClassName.substring( lastPeriod+1 );

        String flavor;
        if( currentValue == null ) {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_NULL_FLAVOR );
        } else {
            flavor = (String) pars.get( StringifierParameters.FLAVOR_KEY, StringifierParameters.DEFAULT_FLAVOR );
        }
        String templateName = thePathPrefix + shortClassName + "/" + flavor;

        Template t = null;
        try {
            t = theFragmentMap.obtainFor( templateName );

        } catch( IOException ex ) {
            log.debug( ex );
        }
        if( t == null ) {
            if( theDelegate != null ) {
                return theDelegate.formatRoleProperty( obj, neighbor, propertyType, currentValue, pars ); // this may be recursive

            } else {
                log.error( "No Template found at " + templateName );
                return "NO TEMPLATE";
            }
        }

        ViewletModel vlModel = (ViewletModel) pars.get( AbstractMeshObjectHelper.VIEWLET_MODEL_KEY );

        String shellVar         = vlModel.obtainNewShellVar();
        String neighborShellVar = vlModel.obtainNewShellVar();
        String shellPropVar     = "1"; // FIXME?

        Map<String,Object> localPars = new HashMap<>();
        localPars.put( "meshObject", obj );
        localPars.put( "neighbor", neighbor );

        localPars.put( "pt", propertyType );
        localPars.put( "pv", currentValue );

        localPars.put( "shellvar",         shellVar );
        localPars.put( "shellpropvar",     shellPropVar );
        localPars.put( "neighborshellvar", neighborShellVar );

        if( currentValue instanceof TimeStampValue ) {
            localPars.put( "pvAsCal", ((TimeStampValue)currentValue).getAsCalendar( TimeZone.getDefault() ));
        }

        ViewletModel vlSubmodel = vlModel.createSubmodel( localPars );

        Context c = Context.newBuilder( vlSubmodel )
                .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON, CalendarValueResolver.SINGLETON )
                .build();

        try {
            String ret = t.apply( c );
            return ret;

        } catch( IOException ex ) {
            log.error( ex );

            return "ERROR";
        }
    }

    /**
     * Has the Handlebars templates.
     */
    protected final TemplateMap theFragmentMap;
}
