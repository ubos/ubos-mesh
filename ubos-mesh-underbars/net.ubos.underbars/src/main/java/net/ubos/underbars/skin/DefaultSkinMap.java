//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.skin;

import java.io.IOException;
import net.ubos.util.nameserver.AbstractMNameServer;
import net.ubos.util.nameserver.NameServer;
import net.ubos.web.skin.Skin;
import net.ubos.web.skin.SkinMap;

/**
 * Default implementation of SkinMap.
 */
public class DefaultSkinMap
    extends
        AbstractMNameServer<String,Skin>
    implements
        SkinMap
{
    /**
     * Test compile all skins.
     *
     * @throws IOException a problem occurred compiling a Skin
     */
    public void testCompile()
         throws
            IOException
    {
        for( Skin s : valueIterator() ) {
            HandlebarsSkin realSkin = (HandlebarsSkin)s;

            realSkin.testCompile();
        }
    }
}
