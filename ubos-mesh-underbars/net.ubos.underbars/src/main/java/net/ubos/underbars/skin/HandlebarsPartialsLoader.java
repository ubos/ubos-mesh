//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.skin;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.io.AbstractTemplateLoader;
import com.github.jknack.handlebars.io.TemplateSource;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Knows how to load templates for the partials.
 */
public class HandlebarsPartialsLoader
    extends
        AbstractTemplateLoader
{
    /**
     * Constructor.
     * 
     * @param partialTemplateSources the files containing the templates for partials
     * @param hb the Handlebars instance
     */
    public HandlebarsPartialsLoader(
            Map<String,TemplateSource> partialTemplateSources,
            Handlebars                 hb )
    {
        thePartialTemplateSources = partialTemplateSources;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TemplateSource sourceAt(
            String string )
        throws
            IOException
    {
        TemplateSource ret = thePartialTemplateSources.get( string );
        if( ret != null ) {
            return ret;
        }
        throw new IOException( "Failed to load skin with name: " + string );
    }

    /**
     * Obtain a Set of our content.
     * 
     * @return the set
     */
    public Set<Entry<String,TemplateSource>> entrySet()
    {
        return thePartialTemplateSources.entrySet();
    }

    /**
     * Map of partial name to the TemplateSource containing the partial.
     */
    protected Map<String,TemplateSource> thePartialTemplateSources;
}
