//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.resources.ResourceManager;
import net.ubos.util.ResourceHelper;
import net.ubos.util.logging.Log;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * A ViewletFactoryChoice that instantiates the DefaultHtmlHandlebarsViewlet as default,
 * pretending to be a Viewlet class with a certain name, called the <code>viewletName</code>.
 * This is identical to creating a DefaultViewletFactoryChoice with a Viewlet class named
 * viewletName that does not add any functionality itself.
 */
public class DefaultHtmlHandlebarsViewletFactoryChoice
        extends
            AbstractHtmlHandlebarsViewletFactoryChoice
{
    private static final Log log = Log.getLogInstance( DefaultHtmlHandlebarsViewletFactoryChoice.class ); // our own, private logger

    /**
     * Factory method to create an array, for Viewlets that emit HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param viewletType1 Viewlet type supported by the first Viewlet choice
     * @param matchQuality1 the quality of the match for the first Viewlet choice
     * @return return the choice
     */
    public static DefaultHtmlHandlebarsViewletFactoryChoice [] createSeveral(
            String                viewletName,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1 )
    {
        DefaultHtmlHandlebarsViewletFactoryChoice [] ret = {
            create( viewletName, placement1, dimensionality1, detail1, matchQuality1, resourceManager, resourceHelper ),
        };
        return ret;
    }

    /**
     * Factory method to create an array, for Viewlets that emit HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param viewletType1 Viewlet type supported by the first Viewlet choice
     * @param matchQuality1 the quality of the match for the first Viewlet choice
     * @param viewletType2 Viewlet type supported by the second Viewlet choice
     * @param matchQuality2 the quality of the match for the second Viewlet choice
     * @return return the choice
     */
    public static DefaultHtmlHandlebarsViewletFactoryChoice [] createSeveral(
            String                viewletName,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1,
            WebViewletPlacement   placement2,
            ViewletDimensionality dimensionality2,
            ViewletDetail         detail2,
            double                matchQuality2 )
    {
        DefaultHtmlHandlebarsViewletFactoryChoice [] ret = {
            create( viewletName, placement1, dimensionality1, detail1, matchQuality1, resourceManager, resourceHelper ),
            create( viewletName, placement2, dimensionality2, detail2, matchQuality2, resourceManager, resourceHelper )
        };
        return ret;
    }

    /**
     * Factory method to create an array, for Viewlets that emit HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param viewletType1 Viewlet type supported by the first Viewlet choice
     * @param matchQuality1 the quality of the match for the first Viewlet choice
     * @param viewletType2 Viewlet type supported by the second Viewlet choice
     * @param matchQuality2 the quality of the match for the second Viewlet choice
     * @param viewletType3 Viewlet type supported by the third Viewlet choice
     * @param matchQuality3 the quality of the match for the third Viewlet choice
     * @return return the choice
     */
    public static DefaultHtmlHandlebarsViewletFactoryChoice [] createSeveral(
            String                viewletName,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1,
            WebViewletPlacement   placement2,
            ViewletDimensionality dimensionality2,
            ViewletDetail         detail2,
            double                matchQuality2,
            WebViewletPlacement   placement3,
            ViewletDimensionality dimensionality3,
            ViewletDetail         detail3,
            double                matchQuality3 )
    {
        DefaultHtmlHandlebarsViewletFactoryChoice [] ret = {
            create( viewletName, placement1, dimensionality1, detail1, matchQuality1, resourceManager, resourceHelper ),
            create( viewletName, placement2, dimensionality2, detail2, matchQuality2, resourceManager, resourceHelper ),
            create( viewletName, placement3, dimensionality3, detail3, matchQuality3, resourceManager, resourceHelper ),
        };
        return ret;
    }

    /**
     * Factory method, for Viewlets that emit HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param viewletType Viewlet type supported by the Viewlet
     * @param matchQuality the quality of the match
     * @return return the choice
     */
    public static DefaultHtmlHandlebarsViewletFactoryChoice create(
            String                viewletName,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper )
    {
        TemplateMap templateMap = resourceManager.getViewletTemplateMapFor( viewletName );

        if( templateMap != null ) {
            return new DefaultHtmlHandlebarsViewletFactoryChoice(
                    viewletName,
                    templateMap,
                    placement,
                    dimensionality,
                    detail,
                    matchQuality,
                    resourceHelper );

        } else {
            log.error( "Null TemplateMap for viewlet " + viewletName );
            return null;
        }
    }

    /**
     * Constructor. This is private, so to subclass, use AbstractWebViewletFactoryChoice.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param viewletType  Viewlet type supported by the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param resourceHelper the ResourceHelper to use
     * @param matchQuality the quality of the match
     */
    protected DefaultHtmlHandlebarsViewletFactoryChoice(
            String                viewletName,
            TemplateMap           templateMap,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceHelper        resourceHelper )
    {
        super( viewletName, templateMap, placement, dimensionality, detail, matchQuality, resourceHelper );
    }
}
