//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.meshbaseindex;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.model.primitives.EntityType;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.helpers.WrongHashAttributesException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.util.StringHelper;

/**
 * Looks up an EntityType in the MeshBaseIndex.
 */
public class SearchForBlessedWithHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );
        if( !options.hash.containsKey( MESHTYPE_ARGNAME ) && !options.hash.containsKey( MESHTYPEID_ARGNAME )) {
            throw new WrongHashAttributesException( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES, SearchForBlessedWithHelper.class );
        }

        EntityType type     = (EntityType) getDefaultMeshType( model, options );
        MeshBase   mb       = (MeshBase) model.get( ViewletModel.MESHBASE_KEY );
        MeshBaseIndex index = mb.getMeshBaseIndex();

        MeshObjectSet ret;
        if( index == null ) {
            ret = MeshObjectSet.DEFAULT_EMPTY_SET;
        } else {
            ret = index.searchForBlessedWithEntityType( type );
        }
        return ret;
    }

    /**
     * Name of the argument on Helpers that specifies the EntityType to look up.
     */
    public static final String ENTITYTYPE_ARGNAME = "entityType";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MESHTYPE_ARGNAME,
        MESHTYPEID_ARGNAME
    };
}
