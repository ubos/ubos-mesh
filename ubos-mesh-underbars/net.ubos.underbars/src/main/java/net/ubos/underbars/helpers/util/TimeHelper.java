//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.Date;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.helpers.mesh.AbstractMeshTimeHelper;
import static net.ubos.underbars.helpers.mesh.AbstractMeshTimeHelper.formatTime;
import net.ubos.util.StringHelper;

/**
 * General-purpose Helper to format times, uses the same code as TimeUpdatedHelper and TimeCreatedHelper.
 */
public class TimeHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  model,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        long then;
        if( model instanceof Long ) {
            then = (Long) model;
        } else if( model instanceof Date ) {
            then = ((Date)model).getTime();
        } else {
            throw new IllegalArgumentException( "Unknown type of timestamp:" + model );
        }

        return formatTime( then, options );
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        AbstractMeshTimeHelper.TIMEZONE_ARGNAME,
        AbstractMeshTimeHelper.LOCALE_ARGNAME,
        AbstractMeshTimeHelper.DATESTYLE_ARGNAME,
        AbstractMeshTimeHelper.TIMESTYLE_ARGNAME
    };
}
