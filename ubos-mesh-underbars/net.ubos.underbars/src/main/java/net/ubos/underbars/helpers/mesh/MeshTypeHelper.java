//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.StringValue;
import net.ubos.underbars.helpers.WrongHashAttributesException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Insert the user-visible name of a MeshType.
 */
public class MeshTypeHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshType   type        = getDefaultMeshType( model, options );
        MeshObject obj         = options.hash( MESHOBJECT_ARGNAME );
        String     nullContent = optionalHash( NULL_CONTENT_ARGNAME, "", options );

        if( ( type == null && obj == null ) || ( type != null && obj != null ) ) {
            throw new WrongHashAttributesException(
                    options,
                    MANDATORY_ARGNAMES,
                    OPTIONAL_ARGNAMES,
                    "Exactly one must be given: " + MESHTYPE_ARGNAME + ", " + MESHTYPEID_ARGNAME + " or " + MESHOBJECT_ARGNAME,
                    getClass() );
        }

        if( obj != null ) {
            EntityType [] blessedWith = obj.getEntityTypes();
            if( blessedWith.length > 0 ) {
                type = blessedWith[0]; // randomly pick one
            }
        }

        String ret;
        if( type == null ) {
            ret = nullContent;

        } else {
            StringValue value = type.getUserVisibleName();
            if( value == null ) {
                value = type.getName();
            }

            ret = StringHelper.Capitalization.FIRST.capitalize( value.getAsString() );
        }

        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MESHTYPE_ARGNAME,
        MESHTYPEID_ARGNAME,
        MESHOBJECT_ARGNAME,
        NULL_CONTENT_ARGNAME
    };
}
