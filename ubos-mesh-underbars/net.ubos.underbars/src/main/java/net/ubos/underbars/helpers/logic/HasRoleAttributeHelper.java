//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.logic;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import static net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper.MESHOBJECT_ARGNAME;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Determine whether a MeshObject has a certain RoleAttribute either with an identified neighbor or
 * any neighbor.
 */
public class HasRoleAttributeHelper
    extends
        AbstractMeshObjectHelper
{
    @Override
    public Boolean apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj          = getPrimaryMeshObject( model, options );
        MeshObject neighbor     = getNeighborMeshObject( model, options );
        String     roleAtt      = options.hash( ROLE_ATTRIBUTE_ARGNAME );
        boolean    neighborSide = SIDE_ARG_NEIGHBOR.equals( options.hash( SIDE_ARGNAME ));

        boolean ret;
        if( neighborSide ) {
            if( neighbor == null ) {
                MeshObjectSet neighbors = obj.traverseToNeighbors();
                ret = neighbors.find( (MeshObject candidate ) -> candidate.hasRoleAttribute( obj, roleAtt) ) != null;
            } else {
                ret = neighbor.hasRoleAttribute( obj, roleAtt );
            }
        } else {
            if( neighbor == null ) {
                MeshObjectSet neighbors = obj.traverseToNeighbors();
                ret = neighbors.find( (MeshObject candidate ) -> obj.hasRoleAttribute( candidate, roleAtt) ) != null;
            } else {
                ret = obj.hasRoleAttribute( neighbor, roleAtt );
            }
        }
        return ret;
    }

    /**
     * Name of the argument that indicates on which side the RoleAttribute should be found.
     */
    public static final String SIDE_ARGNAME = "side";

    /**
     * Value of the that indicates on which side the RoleAttribute should be found indicating
     * it is the neighbor's side, not ours.
     */
    public static final String SIDE_ARG_NEIGHBOR = "neighbor";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        ROLE_ATTRIBUTE_ARGNAME,
        NEIGHBOR_ARGNAME,
        SIDE_ARGNAME
    };
}
