//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.logic;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import static net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper.MESHOBJECT_ARGNAME;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Determine whether a MeshObject carries a certain Attribute.
 */
public class HasAttributeHelper
    extends
        AbstractMeshObjectHelper
{
    @Override
    public Boolean apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj  = getPrimaryMeshObject( model, options );
        String     att  = options.hash( ATTRIBUTE_ARGNAME );

        boolean ret = obj.hasAttribute( att );
        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        ATTRIBUTE_ARGNAME
    };
}
