//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Insert a MeshObjectIdentifier.
 */
public class MeshObjectIdHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String     nullContent = options.hash( NULL_CONTENT_ARGNAME );
        String     show        = options.hash( SHOW_ARGNAME, SHOW_ARGVALUE_BOTH );
        MeshObject relativeTo  = options.hash( RELATIVETO_ARGNAME );

        if( !show.endsWith( SHOW_ARGVALUE_BOTH ) && relativeTo != null ) {
            throw new IllegalArgumentException( "Must not provide both " + SHOW_ARGNAME + " and " + RELATIVETO_ARGNAME );
        }

        MeshObject obj = getPrimaryMeshObject( model, options );

        String ret;
        if( obj == null ) {
            ret = "";

        } else if( relativeTo != null ) {
            MeshObjectIdentifier primaryId  = obj.getIdentifier();
            MeshObjectIdentifier relativeId = relativeTo.getIdentifier();

            if( primaryId.getNamespace() == relativeId.getNamespace()) {
                if( relativeId.getLocalId().isEmpty() ) {
                    ret = primaryId.getLocalId();

                } else if( primaryId.getLocalId().startsWith( relativeId.getLocalId() + '/' ) ) {
                    ret = primaryId.getLocalId().substring( relativeId.getLocalId().length() + 1 );

                } else {
                    // cannot do relative
                    ret = formatMeshObjectIdentifier(
                            model,
                            obj.getIdentifier(),
                            options );
                }
            } else {
                // cannot do relative
                ret = formatMeshObjectIdentifier(
                        model,
                        obj.getIdentifier(),
                        options );
            }

        } else {
            switch( show ) {
                case SHOW_ARGVALUE_LOCALID:
                    ret = obj.getIdentifier().getLocalId();
                    break;

                case SHOW_ARGVALUE_NAMESPACE:
                    ret = obj.getIdentifier().getNamespace().getPreferredExternalName();
                    if( ret == null ) {
                        ret = "???"; // FIXME
                    }
                    break;

                default:
                    ret = formatMeshObjectIdentifier(
                            model,
                            obj.getIdentifier(),
                            options );

                    break;
            }
        }
        if( ret.isEmpty() && nullContent != null ) {
            ret = nullContent;
        }

        return ret;
    }

    /**
     * Name of the argument on this Helper that indicates which part of the MeshObjectIdentifier to
     * show.
     */
    public static final String SHOW_ARGNAME = "show";

    /**
     * Value of the SHOW_ARGUMENT indicating that both namespace and local id should be shown. This
     * is the default.
     */
    public static final String SHOW_ARGVALUE_BOTH = "both";

    /**
     * Value of the SHOW_ARGUMENT indicating that only the namespace should be shown.
     */
    public static final String SHOW_ARGVALUE_NAMESPACE = "namespace";

    /**
     * Value of the SHOW_ARGUMENT indicating that only local id should be shown.
     */
    public static final String SHOW_ARGVALUE_LOCALID = "localid";

    /**
     * Name of the argument on this Helper that indicates the MeshObject relative to whose MeshObjectIdentifier
     * this MeshObjectIdentifier shall be shown.
     */
    public static final String RELATIVETO_ARGNAME = "relativeto";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        FLAVOR_ARGNAME,
        MAX_LENGTH_ARGNAME,
        STRING_REPRESENTATION_ARGNAME,
        NULL_CONTENT_ARGNAME,
        SHOW_ARGNAME,
        RELATIVETO_ARGNAME
    };
}
