//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.paginate;

import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.web.vl.PaginatingWebViewlet;

/**
 * A go-to-the-previous page button for Viewlets that paginate.
 */
public class GoBackwardHelper
    extends
        AbstractPagingNavigationHelper
{
    private static final Log log = Log.getLogInstance( GoBackwardHelper.class );

    /**
     * Constructor.
     */
    public GoBackwardHelper() {}

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDestinationUrl(
            ViewletModel model )
    {
        try {
            @SuppressWarnings("unchecked")
            PaginatingWebViewlet<MeshObject> v = (PaginatingWebViewlet<MeshObject>) model.get( ViewletModel.VIEWLET_KEY );

            CursorIterator<MeshObject> start    = v.getCurrentPageStartIterator();
            int                        pageSize = v.getPageSize();

            if( start.hasPrevious( pageSize )) {
                CursorIterator<MeshObject> startPreviousPage = start.createCopy();
                startPreviousPage.moveBy( -pageSize );

                return v.getDestinationUrlForStart( startPreviousPage.next() );

            } else if( start.hasPrevious() ) {
                // not a full page
                CursorIterator<MeshObject> startPreviousPage = start.createCopy();
                startPreviousPage.moveToBeforeFirst();

                return v.getDestinationUrlForStart( startPreviousPage.next() );

            } else {
                return null;
            }

        } catch( CannotViewException ex ) {
            log.error( ex );
            return null;
        }
    }
}
