//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ArrayHelper;

/**
 * Obtain the RoleAttributes on the side of one MeshObject with a neighbor.
 */
public class RoleAttributeNamesHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject primary  = getPrimaryMeshObject( model, options );
        MeshObject neighbor = getNeighborMeshObject(model, options );
        int        max      = optionalHash( MAX_ARGNAME, -1, options );

        String [] roleAttributeNames = primary.getOrderedRoleAttributeNames( neighbor );
        if( max > 0 && roleAttributeNames.length > max ) {
            roleAttributeNames = ArrayHelper.copyIntoNewArray( roleAttributeNames, 0, max, String.class );
        }

        return roleAttributeNames;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        NEIGHBOR_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MAX_ARGNAME
    };
}
