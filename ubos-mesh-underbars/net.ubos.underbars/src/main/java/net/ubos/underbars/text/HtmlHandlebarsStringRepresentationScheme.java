//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.text;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.text.StringRepresentationScheme;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.util.ResourceHelper;
import net.ubos.util.StringHelper;
import net.ubos.util.text.StringifierParameters;
import net.ubos.web.vl.WebMeshObjectsToView;

/**
 * Defines a HTML-ifying escapeToContentFormat strategy
 */
public class HtmlHandlebarsStringRepresentationScheme
    extends
        HandlebarsStringRepresentationScheme
{
    /**
     * Factory method.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param fragmentMap has the Handlebars templates
     * @param normalIdSerializer the serializer for emitting MeshObjectIdentifiers in readable HTML
     * @param urlIdSerializer the serializer for emitting hyperlinks to MeshObjects
     * @param typeIdSerializer the serializer for emitting MeshTypeIdentifiers
     * @return the created instance
     */
    public static HtmlHandlebarsStringRepresentationScheme create(
            String                         prefix,
            MeshObjectIdentifierSerializer normalIdSerializer,
            MeshObjectIdentifierSerializer urlIdSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer,
            TemplateMap                    fragmentMap )
    {
        return new HtmlHandlebarsStringRepresentationScheme( prefix, normalIdSerializer, urlIdSerializer, typeIdSerializer, fragmentMap, null );
    }

    /**
     * Factory method with a delegate.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param normalIdSerializer the serializer for emitting MeshObjectIdentifiers in readable HTML
     * @param urlIdSerializer the serializer for emitting hyperlinks to MeshObjects
     * @param typeIdSerializer the serializer for emitting MeshTypeIdentifiers
     * @param fragmentMap has the Handlebars templates
     * @param delegate the StringRepresentationScheme to delegate to if no entry was found
     * @return the created instance
     */
    public static HtmlHandlebarsStringRepresentationScheme create(
            String                         prefix,
            MeshObjectIdentifierSerializer normalIdSerializer,
            MeshObjectIdentifierSerializer urlIdSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer,
            TemplateMap                    fragmentMap,
            StringRepresentationScheme     delegate )
    {
        return new HtmlHandlebarsStringRepresentationScheme( prefix, normalIdSerializer, urlIdSerializer, typeIdSerializer, fragmentMap, delegate );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param fragmentMap has the Handlebars templates
     * @param normalIdSerializer the serializer for emitting MeshObjectIdentifiers in readable HTML
     * @param urlIdSerializer the serializer for emitting hyperlinks to MeshObjects
     * @param typeIdSerializer the serializer for emitting MeshTypeIdentifiers
     * @param delegate the StringRepresentationScheme to delegate to if no entry was found
     */
    protected HtmlHandlebarsStringRepresentationScheme(
            String                         prefix,
            MeshObjectIdentifierSerializer normalIdSerializer,
            MeshObjectIdentifierSerializer urlIdSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer,
            TemplateMap                    fragmentMap,
            StringRepresentationScheme     delegate )
    {
        super( prefix, normalIdSerializer, typeIdSerializer, fragmentMap, delegate );

        theUrlIdSerializer = urlIdSerializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshObjectIdentifier(
            MeshObjectIdentifier  id,
            StringifierParameters pars )
    {
        String idString = theIdSerializer.toExternalForm( id );

        Integer maxLength = (Integer) pars.get( StringifierParameters.MAX_LENGTH_KEY );
        if( maxLength != null ) {
            int len = idString.length();

            if( len > maxLength ) {
                if( maxLength < 4 ) {
                    idString = idString.substring( 0, maxLength );
                } else if( maxLength < 12 ) {
                    idString = idString.substring( 0, maxLength - 3 ) + "...";
                } else {
                    idString = idString.substring( 0, maxLength - 3 - 4 ) + "..." + idString.substring( len - 4 );
                }
            }
        }

        return escapeToContentFormat( idString );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshTypeIdentifier(
            MeshTypeIdentifier    id,
            StringifierParameters pars )
    {
        String idString = theTypeIdSerializer.toExternalForm( id );

        Integer maxLength = (Integer) pars.get( StringifierParameters.MAX_LENGTH_KEY );
        if( maxLength != null ) {
            int len = idString.length();

            if( len > maxLength ) {
                if( maxLength < 4 ) {
                    idString = idString.substring( 0, maxLength );
                } else if( maxLength < 12 ) {
                    idString = idString.substring( 0, maxLength - 3 ) + "...";
                } else {
                    idString = idString.substring( 0, maxLength - 3 - 4 ) + "..." + idString.substring( len - 4 );
                }
            }
        }

        return escapeToContentFormat( idString );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshObjectLinkStart(
            MeshObject                    obj,
            StringifierParameters         pars )
    {
        WebMeshObjectsToView toView = (WebMeshObjectsToView) pars.get( StringifierParameters.MESHOBJECTS_TO_VIEW_KEY  );
        String idString;
        if( toView == null ) {
            idString = theUrlIdSerializer.toExternalForm( obj.getIdentifier() );
        } else {
            idString = toView.asUrlString();
        }

        String cssClass = (String) pars.get( StringifierParameters.HTML_ADDITIONAL_CSS_CLASS_KEY );
        String addArgs  = (String) pars.get( StringifierParameters.HTML_URL_ADDITIONAL_ARGUMENTS_KEY );
        String target   = (String) pars.get( StringifierParameters.LINK_TARGET_KEY );
        String title    = (String) pars.get( StringifierParameters.LINK_TITLE_KEY );

        StringBuilder ret = new StringBuilder();
        ret.append( "<a href=\"" );
        ret.append( escapeToContentFormat( idString ));
        if( addArgs != null ) {
            ret.append( "?" );
            ret.append( escapeToContentFormat( addArgs ));
        }
        if( cssClass != null ) {
            ret.append( "\" class=\"" );
            ret.append( cssClass );
        }
        if( target != null ) {
            ret.append( "\" target=\"" );
            ret.append( target );
        }
        if( title != null ) {
            ret.append( "\" title=\"" );
            ret.append( title );
        }
        ret.append( "\">" );

        return ret.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshObjectLinkEnd(
            MeshObject            obj,
            StringifierParameters pars )
    {
        return "</a>";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escapeToContentFormat(
            String raw )
    {
        return StringHelper.stringToHtml( raw );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String unescapeFromContentFormat(
            String raw )
    {
        return StringHelper.htmlToString( raw );
    }

    /**
     * Serializes links in URLs.
     */
    protected final MeshObjectIdentifierSerializer theUrlIdSerializer;

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( HtmlHandlebarsStringRepresentationScheme.class );
}
