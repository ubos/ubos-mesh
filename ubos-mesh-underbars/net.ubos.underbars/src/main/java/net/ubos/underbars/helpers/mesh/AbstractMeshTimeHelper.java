//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Functionality common to Helpers that deal with MeshObjects and times.
 */
public abstract class AbstractMeshTimeHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        long then = getRespectiveTime( obj );

        return formatTime( then, options );
    }

    /**
     * Factored-out actual formatting routine, so we could use it elsewhere, too.
     *
     * @param then the time to render
     * @param options the options provided to this Helper
     * @return the formatted time
     */
    public static String formatTime(
            long then,
            Options options )
    {
        String localeString = options.hash( LOCALE_ARGNAME, null );
        Locale locale       = localeString != null ? Locale.forLanguageTag( localeString ) : null;

        int dateStyle = determineStyle( options.hash( DATESTYLE_ARGNAME, null ), "DateStyle" );
        int timeStyle = determineStyle( options.hash( TIMESTYLE_ARGNAME, null ), "TimeStyle" );

        DateFormat format;
        if( locale != null ) {
            format = DateFormat.getDateTimeInstance( dateStyle, timeStyle, locale );
        } else {
            format = DateFormat.getDateTimeInstance( dateStyle, timeStyle );
        }

        String ret = format.format( new Date( then ));

        return ret;
    }

    /**
     * Helper method to parse date or time format
     *
     * @param format format string
     * @param tagName for error reporting
     * @return DateFormat, or null
     */
    public static int determineStyle(
            String format,
            String tagName )
    {
        int ret;
        if( format != null && format.length() > 0 ) {
            if( "FULL".equalsIgnoreCase( format )) {
                ret = DateFormat.FULL;

            } else if( "LONG".equalsIgnoreCase( format )) {
                ret = DateFormat.LONG;

            } else if( "MEDIUM".equalsIgnoreCase( format )) {
                ret = DateFormat.MEDIUM;

            } else if( "SHORT".equalsIgnoreCase( format )) {
                ret = DateFormat.SHORT;

            } else {
                throw new IllegalArgumentException( tagName + " attribute must be one of \"FULL\", \"LONG\", \"MEDIUM\", \"SHORT\"." );
            }
        } else {
            ret = DateFormat.SHORT;
        }
        return ret;
    }

    /**
     * This method is implemented by our respective subclasses, to obtain
     * the time that's applicable to that subclass, such as timeCreated, timeUpdated etc.
     *
     * @param obj the MeshObject
     * @return the time, in Systems.currentTimeMillis() format
     */
    protected abstract long getRespectiveTime(
            MeshObject obj );

    /**
     * Name of the argument indicating the timezone to be used.
     */
    public static final String TIMEZONE_ARGNAME = "timeZone";

    /**
     * Name of the argument indicating the Locale to be used.
     */
    public static final String LOCALE_ARGNAME = "locale";

    /**
     * Name of the argument indicating the date style to be used.
     */
    public static final String DATESTYLE_ARGNAME = "dateStyle";

    /**
     * Name of the argument indicating the time style to be used.
     */
    public static final String TIMESTYLE_ARGNAME = "timeStyle";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        TIMEZONE_ARGNAME,
        LOCALE_ARGNAME,
        DATESTYLE_ARGNAME,
        TIMESTYLE_ARGNAME
    };
}
