//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.templatesource;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import net.ubos.util.ResourceHelper;
import net.ubos.util.nameserver.AbstractMNameServer;
import net.ubos.util.FileUtils;

/**
 * A map of HandlebarsFileTemplateSources in a directory.
 */
public class FileTemplateSourceManager
    extends
        AbstractMNameServer<String,FileTemplateSource>
    implements
        FileTemplateSourceMap
{
    /**
     * Factory method.
     *
     * @return the created FileTemplateSourceManager instance
     */
    public static FileTemplateSourceManager create()
    {
        FileTemplateSourceManager ret = new FileTemplateSourceManager();
        return ret;
    }

    /**
     * Private constructor, use factory method.
     */
    protected FileTemplateSourceManager() {}

    /**
     * Look for FileTemplateSources in this directory, and add them to the FileTemplateSourceMap.
     *
     * @param dir the directory containing the FileTemplateSources
     * @param allowedExtensions contains the allowed file extensions; will ignore everything else
     */
    public void addFileTemplatesInDir(
            File        dir,
            Set<String> allowedExtensions )
    {
        if( !dir.exists() ) {
            return;
        }
        if( !dir.isDirectory() ) {
            throw new IllegalArgumentException( "Not a directory: " + dir.getAbsolutePath() );
        }

        // FIXME: this should probably not find files recursively, or least then not chop off the relative path component.

        File [] templates = FileUtils.recursivelyFindFiles(
                dir,
                (File file) -> {
                    if( !file.isFile() ) {
                        return false;
                    }
                    if( allowedExtensions == null ) {
                        return true;
                    }
                    String filename = file.getName();
                    int lastDot = filename.lastIndexOf( '.' );
                    if( lastDot < 0 ) {
                        return false;
                    } else {
                        return allowedExtensions.contains( filename.substring( lastDot+1 ));
                    }
                });

        if( templates.length > 0 ) {
            int dirLen = dir.toString().length() + 1; // also include trailing slash
            for( File t : templates ) {
                String templateName = t.getPath().substring( dirLen );
                int lastDot = templateName.lastIndexOf( '.' );
                if( lastDot >= 0 ) {
                    templateName = templateName.substring( 0, lastDot );
                }
                super.put( templateName, FileTemplateSource.create( t ) );
            }
        }
    }

    
    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( FileTemplateSourceManager.class );

    /**
     * Defines known web asset types.
     */
    public static final Set<String> TEMPLATE_EXTENSIONS = new HashSet<>();
    static {
        String [] extensions = theResourceHelper.getResourceStringArrayOrDefault(
                "TemplateExtensions",
                new String [] {
                    "hbs"
                } );

        TEMPLATE_EXTENSIONS.addAll( Arrays.asList( extensions ));
    }    
}
