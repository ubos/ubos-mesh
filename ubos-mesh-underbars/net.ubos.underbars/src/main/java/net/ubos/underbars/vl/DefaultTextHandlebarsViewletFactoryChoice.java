//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.resources.ResourceManager;
import net.ubos.util.ResourceHelper;
import net.ubos.util.logging.Log;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * A ViewletFactoryChoice that instantiates the DefaultTextHandlebarsViewlet as default,
 * pretending to be a Viewlet class with a certain name, called the <code>viewletName</code>.
 * This is identical to creating a DefaultViewletFactoryChoice with a Viewlet class named
 * viewletName that does not add any functionality itself.
 */
public class DefaultTextHandlebarsViewletFactoryChoice
        extends
            AbstractTextHandlebarsViewletFactoryChoice
{
    private static final Log log = Log.getLogInstance( DefaultTextHandlebarsViewletFactoryChoice.class ); // our own, private logger

    /**
     * Factory method, for Viewlets that emit HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param type1 the type of Viewlet
     * @param matchQuality1 the quality of the match
     * @return return the choice
     */
    public static DefaultTextHandlebarsViewletFactoryChoice [] createSeveral(
            String                viewletName,
            String                supportedMimeType,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1 )
    {
        return new DefaultTextHandlebarsViewletFactoryChoice[] {
            create( viewletName, supportedMimeType, placement1, dimensionality1, detail1, matchQuality1, resourceManager, resourceHelper )
        };
    }

    /**
     * Factory method, for Viewlets that emit HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param type1 the type of Viewlet
     * @param matchQuality1 the quality of the match
     * @return return the choice
     */
    public static DefaultTextHandlebarsViewletFactoryChoice [] createSeveral(
            String                viewletName,
            String                supportedMimeType,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1,
            WebViewletPlacement   placement2,
            ViewletDimensionality dimensionality2,
            ViewletDetail         detail2,
            double                matchQuality2 )
    {
        return new DefaultTextHandlebarsViewletFactoryChoice[] {
            create( viewletName, supportedMimeType, placement1, dimensionality1, detail1, matchQuality1, resourceManager, resourceHelper ),
            create( viewletName, supportedMimeType, placement2, dimensionality2, detail2, matchQuality2, resourceManager, resourceHelper )
        };
    }

    /**
     * Factory method, for Viewlets that emit HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param type1 the type of Viewlet
     * @param matchQuality1 the quality of the match
     * @return return the choice
     */
    public static DefaultTextHandlebarsViewletFactoryChoice [] createSeveral(
            String                viewletName,
            String                supportedMimeType,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1,
            WebViewletPlacement   placement2,
            ViewletDimensionality dimensionality2,
            ViewletDetail         detail2,
            double                matchQuality2,
            WebViewletPlacement   placement3,
            ViewletDimensionality dimensionality3,
            ViewletDetail         detail3,
            double                matchQuality3 )
    {
        return new DefaultTextHandlebarsViewletFactoryChoice[] {
            create( viewletName, supportedMimeType, placement1, dimensionality1, detail1, matchQuality1, resourceManager, resourceHelper ),
            create( viewletName, supportedMimeType, placement2, dimensionality2, detail2, matchQuality2, resourceManager, resourceHelper ),
            create( viewletName, supportedMimeType, placement3, dimensionality3, detail3, matchQuality3, resourceManager, resourceHelper ),
        };
    }

    /**
     * Factory method, for Viewlets that emit something else than HTML.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param supportedMimeType the supported MIME type
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param type the type of Viewlet
     * @param matchQuality the quality of the match
     * @return return the choice
     */
    public static DefaultTextHandlebarsViewletFactoryChoice create(
            String                viewletName,
            String                supportedMimeType,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper )
    {
        TemplateMap templateMap = resourceManager.getViewletTemplateMapFor( viewletName );

        if( templateMap != null ) {
            return new DefaultTextHandlebarsViewletFactoryChoice(
                    viewletName,
                    supportedMimeType,
                    templateMap,
                    placement,
                    dimensionality,
                    detail,
                    matchQuality,
                    resourceHelper );

        } else {
            log.error( "Null TemplateMap for viewlet " + viewletName );
            return null;
        }
    }

    /**
     * Constructor. This is private, so to subclass, use AbstractWebViewletFactoryChoice.
     *
     * @param toView the WebMeshObjectsToView to view
     * @param viewletName name of the Viewlet for which this is a choice
     * @param viewletTypes set of Viewlet types supported by the Viewlet
     * @param supportedMimeType the supported MIME type
     * @param templateMap the named Templates used for the Viewlet
     * @param resourceHelper the ResourceHelper to use
     * @param type the type of Viewlet
     * @param matchQuality the quality of the match
     */
    protected DefaultTextHandlebarsViewletFactoryChoice(
            String                viewletName,
            String                supportedMimeType,
            TemplateMap           templateMap,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceHelper        resourceHelper )
    {
        super( viewletName, supportedMimeType, templateMap, placement, dimensionality, detail, matchQuality, resourceHelper );
    }
}
