//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.html;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Add an HTML Link tag to the StructuredResponse.
 */
public class LinkHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel context,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String rel  = options.hash( REL_ARGNAME );
        String href = options.hash( HREF_ARGNAME );
        
        StructuredResponse sr = (StructuredResponse) context.get( ViewletModel.STRUCTURED_RESPONSE_KEY );
        sr.ensureHtmlHeaderLink( rel, href );
        
        return "";
    }

    /**
     * Name of the argument that indicates the type of relationship.
     */
    public static final String REL_ARGNAME = "rel";

    /**
     * Name of the argument that indicates the href.
     */
    public static final String HREF_ARGNAME = "href";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        REL_ARGNAME,
        HREF_ARGNAME
    };
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
