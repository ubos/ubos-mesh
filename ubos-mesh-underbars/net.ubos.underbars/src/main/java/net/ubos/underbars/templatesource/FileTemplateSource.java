//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.templatesource;

import com.github.jknack.handlebars.io.AbstractTemplateSource;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import net.ubos.util.StreamUtils;

/**
 * A Handlebars TemplateSource that's backed by a File.
 * Seems Handlebars does not have this class directly.
 *
 * This strips any trailing CR in the last line, as many editors append a last
 * CR at the end of the file, and that can get in the way.
 */
public class FileTemplateSource
        extends
           AbstractTemplateSource
{
    /**
     * Factory method.
     *
     * @param file the file containing the resources
     * @return the created FileTemplateSource
     */
    public static FileTemplateSource create(
            File file )
    {
        return new FileTemplateSource( file );
    }

    /**
     * Factory method.
     *
     * @param fileName name of the file containing the resources
     * @return the created FileTemplateSource
     */
    public static FileTemplateSource create(
            String fileName )
    {
        return new FileTemplateSource( new File( fileName ));
    }

    /**
     * Constructor.
     *
     * @param file the file containing the resources
     */
    protected FileTemplateSource(
            File file )
    {
        theFile = file;

        if( !theFile.exists() ) {
            throw new IllegalArgumentException( "File does not exist: " + theFile );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String content(
            Charset set )
        throws
            IOException
    {
        String ret = StreamUtils.slurp( new FileReader( theFile, set ));

        boolean foundCr = false;
        for( int i= ret.length()-1 ; i>=0 ; --i ) {
            char c = ret.charAt( i );
            if( !Character.isWhitespace( c )) {
                ret = ret.substring( 0, i+1 );
                break;
            }
            if( foundCr ) {
                if( !( c == '\n' || c == '\r' )) {
                    ret = ret.substring( 0, i+1 );
                    break;
                }
            } else {
                if( c == '\n' || c == '\r' ) {
                    foundCr = true;
                }
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String filename()
    {
        return theFile.getAbsolutePath();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long lastModified()
    {
        return theFile.lastModified();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return getClass().getName() + ": source " + theFile.getAbsolutePath();
    }

    /**
     * The file's name.
     */
    protected File theFile;
}
