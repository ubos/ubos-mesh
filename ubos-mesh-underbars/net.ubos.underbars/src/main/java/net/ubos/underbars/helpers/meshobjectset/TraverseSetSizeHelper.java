//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.meshobjectset;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.traverse.ByRoleAttributeTraverseSpecification;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.model.traverse.TraverseTranslatorException;
import net.ubos.underbars.helpers.HelpersRuntimeException;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import static net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper.TRAVERSE_ARGNAME;
import static net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper.getPrimaryMeshObject;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;
import static net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper.getTraverseSpecification;

/**
 * Determine the size of a MeshObjectSet obtained by traversal.
 * The same functionality can be provided by the SizeHelper applied to a MeshObjectSet obtained
 * through a traversal, but this implementation often does not actually need to traverse to
 * the neighbor MeshObjects, making it (much) more efficient.
 */
public class TraverseSetSizeHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Integer apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        try {
            Integer ret;
            TraverseSpecification traversalSpec = getTraverseSpecification( model, options, obj, TRAVERSE_ARGNAME );
            if( traversalSpec instanceof RoleType ) {
                ret = obj.traverseCount( (RoleType) traversalSpec );
                
            } else if(    traversalSpec instanceof ByRoleAttributeTraverseSpecification
                       && ((ByRoleAttributeTraverseSpecification)traversalSpec).onlySelectsBySourceRoleAttributeName() )
            {
                ret = obj.aggregateRoleAttributes(
                        ((ByRoleAttributeTraverseSpecification)traversalSpec).getSourceRoleAttributeName(),
                        0,
                        ( Integer balance, Serializable value ) -> (Integer) ( balance + 1 ));
                
            } else {
                MeshObjectSet set = obj.traverse( traversalSpec );
                ret = set.size();
            }

            return ret;

        } catch( TraverseTranslatorException ex ) {
            throw new HelpersRuntimeException( ex );
        }
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        TRAVERSE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
