//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.skin;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;
import net.ubos.web.vl.StructuredResponse;

/**
 * Enables a Viewlet to request a particular skin.
 */
public class RequestSkinHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String skin = options.hash( SKIN_ARGNAME );

        StructuredResponse sr = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
        sr.setSectionContent( StructuredResponse.REQUESTED_SKIN_SECTION, skin );

        return "";
    }

    /**
     * Name of the argument on Helpers that specifies the name of the skin to use.
     */
    public static final String SKIN_ARGNAME = "skin";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        SKIN_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
