//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import java.io.IOException;
import com.github.jknack.handlebars.Options;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.MeshBase;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Look up a MeshObject by identifier and such.
 */
public class MeshObjectLookupHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String                        identifier = options.hash( MESHOBJECTID_ARGNAME );
        MeshObjectIdentifierNamespace namespace  = options.hash( NAMESPACE_ARGNAME );

        MeshBase   mb = Underbars.getMeshBaseNameServer().getDefaultMeshBase();
        MeshObject ret;

        try {
            if( namespace == null ) {
                ret = mb.findMeshObjectByIdentifier( identifier );
            } else {
                ret = mb.findMeshObjectByIdentifier( mb.createMeshObjectIdentifierIn( namespace, identifier ));
            }
            return ret;

        } catch( ParseException ex ) {
            throw new IllegalArgumentException( ex );
        }
    }

    /**
     * Name of the argument on this Helper that specifies the MeshObjectIdentifierNamespace to use.
     */
    public static final String NAMESPACE_ARGNAME = "namespace";

    /**
     * Name of the argument on this Helper that specifies the String form of the MeshObjectIdentifier.
     */
    public static final String MESHOBJECTID_ARGNAME = "meshObjectId";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECTID_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        NAMESPACE_ARGNAME
    };
}
