//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ArrayHelper;

/**
 * Returns the PropertyTypes of a MeshObject
 */
public class PropertyTypesHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj  = getPrimaryMeshObject( model, options );
        EntityType type = (EntityType) getDefaultMeshType( model, options );
        int        max  = optionalHash( MAX_ARGNAME, -1, options );

        PropertyType [] ret;
        if( type == null ) {
            ret = obj.getOrderedPropertyTypes();
        } else {
            ret = type.getOrderedPropertyTypes();
        }

        if( max > 0 && ret.length > max ) {
            ret = ArrayHelper.copyIntoNewArray( ret, 0, max, PropertyType.class );
        }

        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MESHTYPE_ARGNAME,
        MAX_ARGNAME
    };
}
