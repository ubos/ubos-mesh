//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.helper.BlockHelper;
import com.github.jknack.handlebars.helper.ConditionalHelpers;
import com.github.jknack.handlebars.helper.StringHelpers;
import io.undertow.Undertow;
import io.undertow.server.handlers.resource.ResourceHandler;
import java.io.IOException;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleDeactivationException;
import org.diet4j.core.ModuleSettings;
import net.ubos.underbars.helpers.bracket.BracketHelper;
import net.ubos.underbars.helpers.bracket.ContentHelper;
import net.ubos.underbars.helpers.bracket.IfContentHelper;
import net.ubos.underbars.helpers.bracket.NotIfContentHelper;
import net.ubos.underbars.helpers.html.EscapeHtmlHelper;
import net.ubos.underbars.helpers.util.CommentHelper;
import net.ubos.underbars.helpers.html.InsertHeaderLinksHelper;
import net.ubos.underbars.helpers.html.InsertHeaderScriptsHelper;
import net.ubos.underbars.helpers.html.LinkHelper;
import net.ubos.underbars.helpers.html.ScriptHelper;
import net.ubos.underbars.helpers.html.StylesheetHelper;
import net.ubos.underbars.helpers.mesh.AllocateShellVarHelper;
import net.ubos.underbars.helpers.mesh.AttributeEditWrapperHelper;
import net.ubos.underbars.helpers.mesh.AttributeNamesHelper;
import net.ubos.underbars.helpers.mesh.AttributeHelper;
import net.ubos.underbars.helpers.mesh.BlessableByHelper;
import net.ubos.underbars.helpers.mesh.BlessedByHelper;
import net.ubos.underbars.helpers.mesh.MeshObjectHelper;
import net.ubos.underbars.helpers.mesh.MeshObjectIdHelper;
import net.ubos.underbars.helpers.mesh.MeshObjectLinkHelper;
import net.ubos.underbars.helpers.mesh.MeshObjectLookupHelper;
import net.ubos.underbars.helpers.mesh.MeshTypeHelper;
import net.ubos.underbars.helpers.mesh.MeshTypeIdHelper;
import net.ubos.underbars.helpers.mesh.NeighborsHelper;
import net.ubos.underbars.helpers.mesh.OptionalPropertyEditWrapperHelper;
import net.ubos.underbars.helpers.mesh.PropertyHelper;
import net.ubos.underbars.helpers.mesh.PropertyTypesHelper;
import net.ubos.underbars.helpers.mesh.RelationshipBlessableByHelper;
import net.ubos.underbars.helpers.mesh.RoleAttributeNamesHelper;
import net.ubos.underbars.helpers.mesh.RoleTypesHelper;
import net.ubos.underbars.helpers.mesh.TimeCreatedHelper;
import net.ubos.underbars.helpers.mesh.TimeUpdatedHelper;
import net.ubos.underbars.helpers.mesh.UnblessableFromHelper;
import net.ubos.underbars.helpers.html.OverlayContentHelper;
import net.ubos.underbars.helpers.html.OverlayHelper;
import net.ubos.underbars.helpers.ia.ShowMeshTypeInUiHelper;
import net.ubos.underbars.helpers.logic.BlessedWithHelper;
import net.ubos.underbars.helpers.logic.ContainsHelper;
import net.ubos.underbars.helpers.logic.HasAttributeHelper;
import net.ubos.underbars.helpers.logic.HasRoleAttributeHelper;
import net.ubos.underbars.helpers.logic.RelatedHelper;
import net.ubos.underbars.helpers.mesh.FuturePropertyHelper;
import net.ubos.underbars.helpers.mesh.RepeatTraverseToSingleHelper;
import net.ubos.underbars.helpers.mesh.RoleAttributeHelper;
import net.ubos.underbars.helpers.mesh.RolePropertyHelper;
import net.ubos.underbars.helpers.mesh.RolePropertyTypesHelper;
import net.ubos.underbars.helpers.meshbaseindex.SearchForBlessedWithHelper;
import net.ubos.underbars.helpers.meshobjectset.IntersectSetHelper;
import net.ubos.underbars.helpers.meshobjectset.MinusSetHelper;
import net.ubos.underbars.helpers.meshobjectset.SortHelper;
import net.ubos.underbars.helpers.mesh.TraverseHelper;
import net.ubos.underbars.helpers.mesh.TraverseToSingleHelper;
import net.ubos.underbars.helpers.meshbaseindex.WordCloudHelper;
import net.ubos.underbars.helpers.meshobjectset.ClipHelper;
import net.ubos.underbars.helpers.meshobjectset.SizeHelper;
import net.ubos.underbars.helpers.meshobjectset.TraverseSetSizeHelper;
import net.ubos.underbars.helpers.meshobjectset.UnionSetHelper;
import net.ubos.underbars.helpers.paginate.GoBackwardHelper;
import net.ubos.underbars.helpers.paginate.GoForwardHelper;
import net.ubos.underbars.helpers.paginate.GoToFirstHelper;
import net.ubos.underbars.helpers.paginate.GoToLastHelper;
import net.ubos.underbars.helpers.skin.RequestSkinHelper;
import net.ubos.underbars.helpers.nonblessed.CsvColumnNamesHelper;
import net.ubos.underbars.helpers.nonblessed.JsonHelper;
import net.ubos.underbars.helpers.util.IfTypeHelper;
import net.ubos.underbars.helpers.util.AppHelper;
import net.ubos.underbars.helpers.util.AssertParameterHelper;
import net.ubos.underbars.helpers.util.CapitalizeHelper;
import net.ubos.underbars.helpers.util.ConcatHelper;
import net.ubos.underbars.helpers.util.EscapeQuotesHelper;
import net.ubos.underbars.helpers.util.FragmentHelper;
import net.ubos.underbars.helpers.util.ListHelper;
import net.ubos.underbars.helpers.util.SafeFormHelper;
import net.ubos.underbars.helpers.util.SafeFormHiddenInputHelper;
import net.ubos.underbars.helpers.util.ShowInvocationHelper;
import net.ubos.underbars.helpers.util.SprintfHelper;
import net.ubos.underbars.helpers.util.TimeHelper;
import net.ubos.underbars.helpers.util.PrepostfixHelper;
import net.ubos.underbars.helpers.util.RpnHelper;
import net.ubos.underbars.helpers.util.VarHelper;
import net.ubos.underbars.helpers.vl.IfViewletParameterMatchesHelper;
import net.ubos.underbars.helpers.vl.UnlessViewletParameterMatchesHelper;
import net.ubos.underbars.helpers.vl.ViewletHelper;
import net.ubos.underbars.helpers.vl.ViewletIncludeIframeHelper;
import net.ubos.underbars.helpers.vl.ViewletIncludeInlineHelper;
import net.ubos.underbars.helpers.vl.ViewletParameterHelper;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.util.logging.Log;
import net.ubos.web.assets.PrefixRemovingAssetManager;

/**
 * Main program of the Undertow daemon. Activate using diet4j.
 */
public class ModuleInit
{
    private static final Log log = Log.getLogInstance( ModuleInit.class );

    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE = "ubos-mesh-underbars";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        try {
            log.traceMethodCallEntry( ModuleInit.class, "moduleActivate", thisModule );

            theUnderbars = Underbars.class; // prevent garbage collection

            ModuleSettings settings = thisModule.getModuleSettings();

            Underbars.setContextPath( settings.getString( "context" ));
            Underbars.getHandlebars().getCache().setReload( "true".equalsIgnoreCase( settings.getString( "development", "false" )));

            Underbars.registerPathPrefixDelegate("/s",
                    new ResourceHandler( new MappedPathResourceManager( PrefixRemovingAssetManager.create( "/s", Underbars.getAssetMap() ))) );

            registerHelpers();

            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE );
            Underbars.registerAssets(    resourceManager.getAssetMap() );
            Underbars.registerFragments( resourceManager.getFragmentTemplateMap() );
            resourceManager.checkResources(); // only do this here so we have things like Helpers registered

            startUndertowServer( settings );

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }

    /**
     * Factored out: register the Helpers we know.
     */
    protected static void registerHelpers()
    {
        Handlebars h = Underbars.getHandlebars();

        h.registerHelpers( ConditionalHelpers.class ); // default, but not registered
        h.registerHelpers( StringHelpers.class );      // default, but not registered
        h.registerHelpers( BlockHelper.class );        // default, but not registered

        h.registerHelper( "bracket:bracket",                    new BracketHelper() );
        h.registerHelper( "bracket:content",                    new ContentHelper() );
        h.registerHelper( "bracket:ifContent",                  new IfContentHelper() );
        h.registerHelper( "bracket:notIfContent",               new NotIfContentHelper() );

        h.registerHelper( "html:escapeHtml",                    new EscapeHtmlHelper() );
        h.registerHelper( "html:insertHeaderLinks",             new InsertHeaderLinksHelper() );
        h.registerHelper( "html:insertHeaderScripts",           new InsertHeaderScriptsHelper() );
        h.registerHelper( "html:link",                          new LinkHelper() );
        h.registerHelper( "html:overlay",                       new OverlayHelper() );
        h.registerHelper( "html:overlay:content",               new OverlayContentHelper() );
        h.registerHelper( "html:script",                        new ScriptHelper() );
        h.registerHelper( "html:stylesheet",                    new StylesheetHelper() );

        h.registerHelper( "ia:showMeshTypeInUi",                new ShowMeshTypeInUiHelper() );

        h.registerHelper( "logic:blessedWith",                  new BlessedWithHelper() );
        h.registerHelper( "logic:contains",                     new ContainsHelper() );
        h.registerHelper( "logic:hasAttribute",                 new HasAttributeHelper() );
        h.registerHelper( "logic:hasRoleAttribute",             new HasRoleAttributeHelper() );
        h.registerHelper( "logic:related",                      new RelatedHelper() );

        h.registerHelper( "mesh:allocateShellVar",              new AllocateShellVarHelper() );
        h.registerHelper( "mesh:attributeEditWrapper",          new AttributeEditWrapperHelper() );
        h.registerHelper( "mesh:attributeNames",                new AttributeNamesHelper() );
        h.registerHelper( "mesh:attribute",                     new AttributeHelper() );
        h.registerHelper( "mesh:blessableBy",                   new BlessableByHelper() );
        h.registerHelper( "mesh:blessedBy",                     new BlessedByHelper() );
        h.registerHelper( "mesh:futureProperty",                new FuturePropertyHelper() );
        h.registerHelper( "mesh:meshObject",                    new MeshObjectHelper() );
        h.registerHelper( "mesh:meshObjectId",                  new MeshObjectIdHelper() );
        h.registerHelper( "mesh:meshObjectLink",                new MeshObjectLinkHelper() );
        h.registerHelper( "mesh:meshObjectLookup",              new MeshObjectLookupHelper() );
        h.registerHelper( "mesh:meshType",                      new MeshTypeHelper() );
        h.registerHelper( "mesh:meshTypeId",                    new MeshTypeIdHelper() );
        h.registerHelper( "mesh:neighbors",                     new NeighborsHelper() );
        h.registerHelper( "mesh:optionalPropertyEditWrapper",   new OptionalPropertyEditWrapperHelper() );
        h.registerHelper( "mesh:property",                      new PropertyHelper() );
        h.registerHelper( "mesh:propertyTypes",                 new PropertyTypesHelper() );
        h.registerHelper( "mesh:relationshipBlessableBy",       new RelationshipBlessableByHelper() );
        h.registerHelper( "mesh:repeatTraverseToSingle",        new RepeatTraverseToSingleHelper() );
        h.registerHelper( "mesh:roleAttribute",                 new RoleAttributeHelper() );
        h.registerHelper( "mesh:roleAttributeNames",            new RoleAttributeNamesHelper() );
        h.registerHelper( "mesh:roleTypes",                     new RoleTypesHelper() );
        h.registerHelper( "mesh:roleProperty",                  new RolePropertyHelper() );
        h.registerHelper( "mesh:rolePropertyTypes",             new RolePropertyTypesHelper() );
        h.registerHelper( "mesh:timeCreated",                   new TimeCreatedHelper() );
        h.registerHelper( "mesh:timeUpdated",                   new TimeUpdatedHelper() );
        h.registerHelper( "mesh:traverse",                      new TraverseHelper() );
        h.registerHelper( "mesh:traverseToSingle",              new TraverseToSingleHelper() );
        h.registerHelper( "mesh:unblessableFrom",               new UnblessableFromHelper() );

        h.registerHelper( "meshBaseIndex:searchForBlessedWith", new SearchForBlessedWithHelper() );
        h.registerHelper( "meshBaseIndex:wordCloud",            new WordCloudHelper() );

        h.registerHelper( "nb:json",                            new JsonHelper() );
        h.registerHelper( "nb:csvColumnNames",                  new CsvColumnNamesHelper() );

        h.registerHelper( "paginate:goBackward",                new GoBackwardHelper() );
        h.registerHelper( "paginate:goForward",                 new GoForwardHelper() );
        h.registerHelper( "paginate:goToFirst",                 new GoToFirstHelper() );
        h.registerHelper( "paginate:goToLast",                  new GoToLastHelper() );

        h.registerHelper( "set:clip",                           new ClipHelper() );
        h.registerHelper( "set:intersect",                      new IntersectSetHelper() );
        h.registerHelper( "set:minus",                          new MinusSetHelper() );
        h.registerHelper( "set:size",                           new SizeHelper() );
        h.registerHelper( "set:sort",                           new SortHelper() );
        h.registerHelper( "set:traverseSize",                   new TraverseSetSizeHelper() );
        h.registerHelper( "set:union",                          new UnionSetHelper() );

        h.registerHelper( "skin:requestSkin",                   new RequestSkinHelper() );

        h.registerHelper( "util:app",                           new AppHelper() );
        h.registerHelper( "util:assertParam",                   new AssertParameterHelper() );
        h.registerHelper( "util:capitalize",                    new CapitalizeHelper() );
        h.registerHelper( "util:comment",                       new CommentHelper() );
        h.registerHelper( "util:concat",                        new ConcatHelper() );
        h.registerHelper( "util:escapeQuotes",                  new EscapeQuotesHelper() );
        h.registerHelper( "util:fragment",                      new FragmentHelper() );
        h.registerHelper( "util:ifType",                        new IfTypeHelper() );
        h.registerHelper( "util:list",                          new ListHelper() );
        h.registerHelper( "util:prepostfix",                    new PrepostfixHelper() );
        h.registerHelper( "util:rpn",                           new RpnHelper() );
        h.registerHelper( "util:safeForm",                      new SafeFormHelper() );
        h.registerHelper( "util:safeFormHiddenInput",           new SafeFormHiddenInputHelper() );
        h.registerHelper( "util:showInvocation",                new ShowInvocationHelper() );
        h.registerHelper( "util:sprintf",                       new SprintfHelper() );
        h.registerHelper( "util:time",                          new TimeHelper() );
        h.registerHelper( "util:var",                           new VarHelper() );

        h.registerHelper( "vl:ifVlParamMatches",                new IfViewletParameterMatchesHelper() );
        h.registerHelper( "vl:vl",                              new ViewletHelper() );
        h.registerHelper( "vl:vlIncludeInline",                 new ViewletIncludeInlineHelper() );
        h.registerHelper( "vl:vlIncludeIframe",                 new ViewletIncludeIframeHelper() );
        h.registerHelper( "vl:vlParam",                         new ViewletParameterHelper() );
        h.registerHelper( "vl:unlessVlParamMatches",            new UnlessViewletParameterMatchesHelper() );
    }

    /**
     * Factored out: start the Undertow server during moduleActivate
     *
     * @param settings: this Module's settings
     */
    protected static void startUndertowServer(
            ModuleSettings settings )
    {
        Undertow.Builder builder = Undertow.builder();

        builder.addHttpListener(
                settings.getInteger( "port", 80 ),
                settings.getString(  "host", "localhost" ));
        if( settings.containsKey( "serverthreads" )) {
            builder.setIoThreads(
                    settings.getInteger( "serverthreads" ));
        }

        builder.setHandler( Underbars.getTopHttpHandler() );

        theUndertowServer = builder.build();
        theUndertowServer.start();
    }

    /**
     * Diet4j module deactivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleDeactivationException thrown if module deactivation failed
     */
    public static void moduleDeactivate(
            Module thisModule )
        throws
            ModuleDeactivationException
    {
        log.traceMethodCallEntry( ModuleInit.class, "moduleDeactivate", thisModule );

        try {
            if( theUndertowServer != null ) {
                theUndertowServer.stop();
            }

        } catch( Exception ex ) {
            throw new ModuleDeactivationException( thisModule.getModuleMeta(), ex );
        }

        theUnderbars = null;
    }

    /**
     * The HTTP server.
     */
    protected static Undertow theUndertowServer;

    /**
     * The Underbars singleton.
     */
    protected static Class<Underbars> theUnderbars;
}
