//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Lets the user choose the EntityTypes by which a MeshObject may be blessed.
 */
public class BlessableByHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        String  superType              = options.hash( SUPERTYPE_ARGNAME );
        String  subjectArea            = options.hash( SUBJECTAREA_ARGNAME );
        String  meshObjectShellVarName = options.hash( MESHOBJECT_SHELL_VAR_NAME_ARGNAME );
        boolean offerIdentifierInput   = options.hash( OFFER_IDENTIFIER_INPUT_ARGNAME, false );
        boolean offerNone              = options.hash( OFFER_NONE_INPUT_ARGNAME, false );

        EntityType  typeLimit;
        SubjectArea saLimit;

        try {
            if( superType != null ) {
                typeLimit = ModelBase.SINGLETON.findEntityType( superType );
            } else {
                typeLimit = null;
            }
            if( subjectArea != null ) {
                saLimit = ModelBase.SINGLETON.findSubjectArea( subjectArea );
            } else {
                saLimit = null;
            }
        } catch( ParseException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new IllegalArgumentException( ex );
        }

        if( typeLimit != null && saLimit != null ) {
            throw new IllegalArgumentException( "Limit either by SubjectArea or supertype, but not both" );
        }
        if( meshObjectShellVarName == null && obj == null ) {
            throw new IllegalArgumentException( "Must specify at least one of " + MESHOBJECT_ARGNAME + " and " + MESHOBJECT_SHELL_VAR_NAME_ARGNAME );
        }

        MeshObjectIdentifierSerializer shellIdSerializer     = model.getShellMeshObjectIdentifierSerializer();
        MeshTypeIdentifierSerializer   shellTypeIdSerializer = model.getShellMeshTypeIdentifierSerializer();

        Options.Buffer buffer = options.buffer();

        if( meshObjectShellVarName == null ) {
            meshObjectShellVarName = model.obtainNewShellVar();

            // also emit a field, so this new variable actually has a value
            buffer.append( "<input type=\"hidden\" name=\"shell." );
            buffer.append( meshObjectShellVarName );
            buffer.append( "\" value=\"" );
            buffer.append( shellIdSerializer.toExternalForm( obj.getIdentifier()) );
            buffer.append( "\"/>\n" );
        }

        buffer.append( "<select name=\"shell." );
        buffer.append( meshObjectShellVarName );
        buffer.append( ".bless\" class=\"" ).append( getCssClass() ).append( "\">\n" );

        if( offerIdentifierInput || offerNone ) {
            buffer.append( " <option value=\"\">None</option>\n" );
        }

        // compare this code to the MeshBaseViewlet

        StringHelper.Capitalization cap = StringHelper.Capitalization.FIRST;

        boolean hasAtLeastOne = false;
        if( saLimit != null ) {
            // don't need to print optiongroups
            EntityType [] saTypes = saLimit.getOrderedEntityTypes();

            for( int i=0 ; i<saTypes.length ; ++i ) {
                if( saTypes[i].getIsAbstract().value()) {
                    continue;
                }
                if( hasNonZeroMinMultiplicity( saTypes[i] )) {
                    continue;
                }

                buffer.append( " <option value=\"" );
                buffer.append( shellTypeIdSerializer.toExternalForm( saTypes[i].getIdentifier() ));
                if( obj != null && obj.isBlessedBy( saTypes[i] )) {
                    buffer.append( "\" disabled=\"disabled" );
                }
                buffer.append( "\">" );
                buffer.append( cap.capitalize( saTypes[i].getUserVisibleName().value() ));
                buffer.append( "</option>\n" );

                hasAtLeastOne = true;
            }

        } else {
            // need to list all EntityTypes from the ModelBase, and select the ones that we currently filter by.
            // We'll try alphabetically

            SubjectArea [] sas = ModelBase.SINGLETON.getOrderedLoadedSubjectAreas();

            for( SubjectArea sa : sas ) {
                StringBuilder prefix = new StringBuilder(); // don't emit if there's nothing in this SA
                prefix.append( " <optgroup label=\"" );
                prefix.append( cap.capitalize( sa.getUserVisibleName().value() ));
                prefix.append( "\">\n" );

                EntityType [] saTypes = sa.getOrderedEntityTypes();

                for( int i=0 ; i<saTypes.length ; ++i ) {
                    if( saTypes[i].getIsAbstract().value()) {
                        continue;
                    }
                    if( hasNonZeroMinMultiplicity( saTypes[i] )) {
                        continue;
                    }
                    if( typeLimit != null && !typeLimit.equalsOrIsSupertype( saTypes[i] )) {
                        continue;
                    }

                    buffer.append( prefix );
                    prefix.setLength( 0 );

                    buffer.append( "  <option value=\"" );
                    buffer.append( shellTypeIdSerializer.toExternalForm( saTypes[i].getIdentifier()));
                    if( obj != null && obj.isBlessedBy( saTypes[i] )) {
                        buffer.append( "\" disabled=\"disabled" );
                    }
                    buffer.append( "\">" );
                    buffer.append( cap.capitalize( saTypes[i].getUserVisibleName().value() ));
                    buffer.append( "</option>\n" );

                    hasAtLeastOne = true;
                }
                if( prefix.length() == 0 ) {
                    buffer.append( " </optgroup>\n" );
                }
            }
        }

        if( !hasAtLeastOne ) {
            buffer.append( " <optgroup label=\"" );
            buffer.append( "None available" );
            buffer.append( "\">\n" );
        }

        buffer.append( "</select>\n" );
        if( offerIdentifierInput ) {
            buffer.append( "<input type=\"text\" name=\"" );
            buffer.append( meshObjectShellVarName );
            buffer.append( ".bless\"/>\n" );
        }
        return buffer;
    }

    /**
     * Helper to determine whether this EntityType participates in at least one RoleType that has a non-zero
     * minimum multiplicity.
     *
     * @param type the EntityType
     * @return true if it participates in at least one RoleType with a non-zero minimum multiplicity
     */
    protected boolean hasNonZeroMinMultiplicity(
            EntityType type )
    {
        for( RoleType rt : type.getConcreteRoleTypes()) {
            if( rt.getMultiplicity().getMinimum() != 0 ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Name of the argument that indicates which supertype should be used as a filter
     * for the emitted EntityTypes.
     */
    public static final String SUPERTYPE_ARGNAME = "supertype";

    /**
     * Name of the argument that indicates which SubjectArea should be used as a filter
     * for the emitted EntityTypes.
     */
    public static final String SUBJECTAREA_ARGNAME = "subjectarea";

    /**
     * Name of the argument that indicates whether or not a text field is offered that
     * allows entering a MeshTypeIdentifier directly,
     */
    public static final String OFFER_IDENTIFIER_INPUT_ARGNAME = "offerIdentifierInput";

    /**
     * Name of the argument that indicates that the value "None" should be one of
     *  the values.
     */
    public static final String OFFER_NONE_INPUT_ARGNAME = "offerNone";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        SUPERTYPE_ARGNAME,
        SUBJECTAREA_ARGNAME,
        MESHOBJECT_SHELL_VAR_NAME_ARGNAME,
        OFFER_IDENTIFIER_INPUT_ARGNAME,
        OFFER_NONE_INPUT_ARGNAME
    };
}
