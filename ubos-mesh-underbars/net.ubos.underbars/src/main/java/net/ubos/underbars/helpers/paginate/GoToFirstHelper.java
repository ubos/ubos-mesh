//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.paginate;

import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.web.vl.PaginatingWebViewlet;

/**
 * A go-to-the-first page button for Viewlets that paginate.
 */
public class GoToFirstHelper
    extends
        AbstractPagingNavigationHelper
{
    private static final Log log = Log.getLogInstance( GoToFirstHelper.class );

    /**
     * Constructor.
     */
    public GoToFirstHelper() {}

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDestinationUrl(
            ViewletModel model )
    {
        try {
            @SuppressWarnings("unchecked")
            PaginatingWebViewlet<MeshObject> v = (PaginatingWebViewlet<MeshObject>) model.get( ViewletModel.VIEWLET_KEY );

            CursorIterator<MeshObject> start    = v.getCurrentPageStartIterator();
            int                        pageSize = v.getPageSize();

            if( start.hasPrevious()) {
                CursorIterator<MeshObject> startFirstPage = start.createCopy();
                startFirstPage.moveToBeforeFirst();

                return v.getDestinationUrlForStart( startFirstPage.next() );

            } else {
                return null;
            }

        } catch( CannotViewException ex ) {
            log.error( ex );
            return null;
        }
    }
}
