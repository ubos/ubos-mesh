//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.web.vl.StructuredResponse;

/**
 * This Helper aids in debugging, by emitting how it has been invoked.
 */
public class ShowInvocationHelper
    implements
        Helper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  model,
            Options options )
        throws
            IOException
    {
        Options.Buffer buffer = options.buffer();
        buffer.append( "<div class=\"" ).append( getClass().getName().replace( '.', '-' )).append( "\">\n" );

        buffer.append( " <h2>Helper:&nbsp;" ).append( getClass().getName() ).append( "</h2>\n" );

        buffer.append( " <table>\n" );

        insertModel( buffer, model );

        buffer.append( "  <tr>\n" );

        if( options.params.length == 0 ) {
            buffer.append( "   <th>Params:</th>\n" );
            buffer.append( "   <td colspan=\"2\">(none)</td>\n" );
            buffer.append( "  </tr>\n" );

        } else {
            buffer.append( "   <th rowspan=\"" ).append( String.valueOf( options.params.length )).append( "\">Params:</th>\n" );
            for( int i=0 ; i<options.params.length ; ++i ) {
                if( i>0 ) {
                    buffer.append( "  <tr>\n" );
                }
                buffer.append( "   <td>" ).append( String.valueOf( i )).append( "</td>\n" );
                buffer.append( "   <td>\n" );
                insertValue( buffer, options.params[i] );
                buffer.append( "   </td>\n" );
                buffer.append( "  </tr>\n" );
            }
        }

        String [] keys = new String[ options.hash.size() ];
        options.hash.keySet().toArray( keys );
        Arrays.sort( keys );

        buffer.append( "  <tr>\n" );

        if( keys.length == 0 ) {
            buffer.append( "   <th>Hash:</th>\n" );
            buffer.append( "   <td colspan=\"2\">(none)</td>\n" );
            buffer.append( "  </tr>\n" );

        } else {
            buffer.append( "   <th rowspan=\"" ).append( String.valueOf( keys.length )).append( "\">Hash:</th>\n" );
            for( int i=0 ; i<keys.length ; ++i ) {
                Object value = options.hash.get( keys[i] );
                if( i>0 ) {
                    buffer.append( "  <tr>\n" );
                }
                buffer.append( "   <td>" ).append( keys[i] ).append( "</td>\n" );
                buffer.append( "   <td>\n" );
                insertValue( buffer, value );
                buffer.append( "   </td>\n" );
                buffer.append( "  </tr>\n" );
            }
        }

        buffer.append( " </table>\n" );

        buffer.append( "</div>" );

        if( model instanceof HandlebarsModel ) {
            StructuredResponse sr = (StructuredResponse) ((HandlebarsModel)model).get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
            sr.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars/ShowInvocationHelper.css" );
        }

        return buffer;
    }

    /**
     * Helper to insert a model.
     *
     * @param buffer the buffer
     * @param model the model
     * @throws IOException buffer writing failed
     */
    protected void insertModel(
            Options.Buffer buffer,
            Object         model )
        throws
            IOException
    {
        if( model instanceof HandlebarsModel ) {
            HandlebarsModel realModel = (HandlebarsModel) model;
            String [] keys = realModel.getSortedKeys();

            buffer.append( "  <tr>\n" );
            buffer.append( "   <th rowspan=\"" ).append( String.valueOf( keys.length )).append( "\">HandlebarsModel:</th>\n" );

            if( keys.length == 0 ) {
                buffer.append( "   <td colspan=\"2\">(none)</td>\n" );
                buffer.append( "  </tr>\n" );
            } else {
                for( int i=0 ; i<keys.length ; ++i ) {
                    Object value = realModel.get( keys[i] );
                    if( i>0 ) {
                        buffer.append( "  <tr>\n" );
                    }
                    buffer.append( "   <td>" ).append( keys[i] ).append( "</td>\n" );
                    buffer.append( "   <td>\n" );
                    insertValue( buffer, value );
                    buffer.append( "   </td>\n" );
                    buffer.append( "  </tr>\n" );
                }
            }
            if( realModel.getDelegate() != null ) {
                // recursion seems easiest :-)
                insertModel( buffer, realModel.getDelegate());
            }

        } else if( model instanceof Map ) {
            @SuppressWarnings("unchecked")
            Map<String,Object> realModel = (Map<String,Object>) model;
            String [] keys = new String[ realModel.size() ];
            realModel.keySet().toArray( keys );
            Arrays.sort( keys );

            buffer.append( "  <tr>\n" );
            buffer.append( "   <th rowspan=\"" ).append( String.valueOf( keys.length )).append( "\">HandlebarsModel:</th>\n" );

            if( keys.length == 0 ) {
                buffer.append( "   <td colspan=\"2\">(none)</td>\n" );
                buffer.append( "  </tr>\n" );
            } else {
                for( int i=0 ; i<keys.length ; ++i ) {
                    Object value = realModel.get( keys[i] );
                    if( i>0 ) {
                        buffer.append( "  <tr>\n" );
                    }
                    buffer.append( "   <td>" ).append( keys[i] ).append( "</td>\n" );
                    buffer.append( "   <td>\n" );
                    insertValue( buffer, value );
                    buffer.append( "   </td>\n" );
                    buffer.append( "  </tr>\n" );
                }
            }

        } else {
            buffer.append( "  <tr>\n" );
            buffer.append( "   <th rowspan=\"2\">Model:</th>\n" );
            buffer.append( "   <td colspan=\"2\">" ).append( model.getClass().getName() ).append( "</td>\n" );
            buffer.append( "  </tr>\n" );
            buffer.append( "  <tr>\n" );
            buffer.append( "  </tr>\n" );
            buffer.append( "   <td colspan=\"2\">\n" );
            buffer.append( "    <pre>" ).append( model.toString() ).append( "</pre>\n" );
            buffer.append( "   </td>\n" );
            buffer.append( "  </tr>\n" );
        }
    }

    /**
     * Helper to insert a model.
     *
     * @param buffer the buffer
     * @param value the value
     * @throws IOException buffer writing failed
     */
    protected void insertValue(
            Options.Buffer buffer,
            Object         value )
        throws
            IOException
    {
        if( value == null ) {
            buffer.append( "    <pre>null</pre>\n" );

        } else {
            buffer.append( "    <pre>" ).append( value.toString() ).append( "</pre>\n" );
            buffer.append( "    <div><code>Class: ").append( value.getClass().getName() ).append( "</code></div>\n" );
        }
    }
}
