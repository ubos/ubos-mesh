//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resolvers;

import com.github.jknack.handlebars.ValueResolver;
import net.ubos.web.vl.StructuredResponse;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public final class StructuredResponseValueResolver
    implements
        ValueResolver
{
    public static final StructuredResponseValueResolver SINGLETON = new StructuredResponseValueResolver();

    /**
     * Private constructor, for singleton only.
     */
    protected StructuredResponseValueResolver()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object resolve(
            final Object context,
            final String name )
    {
        Object value = null;
        if( context instanceof StructuredResponse ) {
            value = ((StructuredResponse) context).getSectionContent( name );
        }
        return value == null ? UNRESOLVED : value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object resolve(
            final Object context )
    {
        return UNRESOLVED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Set<Map.Entry<String, Object>> propertySet(
            final Object context )
    {
        if( context instanceof StructuredResponse ) {
            StructuredResponse realContext = (StructuredResponse) context;

            return (Set<Map.Entry<String, Object>>) (Set<?>) realContext.sections();
        }
        return Collections.emptySet();
    }
}
