//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

import java.io.IOException;
import com.github.jknack.handlebars.Options;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;
import net.ubos.web.HttpRequest;
import net.ubos.web.SubHttpRequest;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Includes another Viewlet as in iframe in the parent Viewlet's HTML.
 */
public class ViewletIncludeIframeHelper
    extends
        AbstractViewletIncludeHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String cssClass = options.hash( CSS_CLASS_ARGNAME );

        StructuredResponse parentResponse = (StructuredResponse) model.get( ViewletModel.STRUCTURED_RESPONSE_KEY );

        try {
            HttpRequest          childRequest  = SubHttpRequest.create( parentResponse.getRequest() );
            WebMeshObjectsToView childToView   = determineChildToView( childRequest, model, options );

            childToView = childToView.withRequiredViewletPlacement( WebViewletPlacement.IFRAME );

            StringBuilder ret = new StringBuilder();
            ret.append( "<iframe src=\"");
            ret.append( StringHelper.stringToHtml( childToView.asUrlString() ));
            if( cssClass != null ) {
                ret.append( "\" class=\"" );
                ret.append( cssClass );
            }
            ret.append( "\"></iframe>\n" );
            return ret.toString();

        } catch( Throwable t ) {
            parentResponse.reportProblem( t );

            return "ERROR";
        }
    }


    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        SUBJECT_ARGNAME,
        NAME_ARGNAME,
        REQUIRED_VIEWLET_DIMENSIONALITY_ARGNAME,
        RECOMMENDED_VIEWLET_DIMENSIONALITY_ARGNAME,
        REQUIRED_VIEWLET_DETAIL_ARGNAME,
        RECOMMENDED_VIEWLET_DETAIL_ARGNAME,
        CSS_CLASS_ARGNAME
    };
}
