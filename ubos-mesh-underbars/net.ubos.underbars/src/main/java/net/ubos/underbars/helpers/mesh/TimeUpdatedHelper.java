//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import net.ubos.mesh.MeshObject;

/**
 * Insert the timeUpdated property of a MeshObject.
 */
public class TimeUpdatedHelper
    extends
        AbstractMeshTimeHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    protected long getRespectiveTime(
            MeshObject obj )
    {
        return obj.getTimeUpdated();
    }
}
