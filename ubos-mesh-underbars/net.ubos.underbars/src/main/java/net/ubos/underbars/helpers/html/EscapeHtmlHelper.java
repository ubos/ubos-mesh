//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.html;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Return the provided argument, but with HTML escaped.
 */
public class EscapeHtmlHelper
    extends
        AbstractHandlebarsHelper<String>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            String  context,
            Options options )
        throws
            IOException
    {
        checkNoArguments( options );
        
        String ret = StringHelper.stringToHtml( context );
        return ret;
    }
}