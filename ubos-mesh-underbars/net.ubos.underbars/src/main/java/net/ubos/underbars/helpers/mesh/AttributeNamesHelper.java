//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ArrayHelper;

/**
 * Obtain the names of the Attributes of a MeshObject
 */
public class AttributeNamesHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );
        int        max = optionalHash( MAX_ARGNAME, -1, options );

        String [] attributeNames = obj.getOrderedAttributeNames();
        if( max > 0 && attributeNames.length > max ) {
            attributeNames = ArrayHelper.copyIntoNewArray( attributeNames, 0, max, String.class );
        }
        return attributeNames;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MAX_ARGNAME
    };
}
