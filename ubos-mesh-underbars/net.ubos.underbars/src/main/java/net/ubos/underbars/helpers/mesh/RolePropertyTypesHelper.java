//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RoleType;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ArrayHelper;

/**
 * Iterates over the Role PropertyTypes of a MeshObject with respect to a neighbor.
 */
public class RolePropertyTypesHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj      = getPrimaryMeshObject(  model, options );
        MeshObject neighbor = getNeighborMeshObject( model, options );
        RoleType   roleType = optionalHash( ROLE_TYPE_ARGNAME, null, options );
        int        max      = optionalHash( MAX_ARGNAME, -1, options );

        PropertyType [] ret;
        if( roleType == null ) {
            ret = obj.getOrderedRolePropertyTypes( neighbor );
        } else if( obj.isRelated( roleType, neighbor )) {
            ret = roleType.getPropertyTypes();
        } else {
            ret = new PropertyType[0];
        }

        if( max > 0 && ret.length > max ) {
            ret = ArrayHelper.copyIntoNewArray( ret, 0, max, PropertyType.class );
        }
        return ret;
    }

    /**
     * Name of the argument that restricts the returned PropertyTypes to those of this RoleType.
     */
    public static final String ROLE_TYPE_ARGNAME = "roleType";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        NEIGHBOR_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        ROLE_TYPE_ARGNAME,
        MAX_ARGNAME
    };
}
