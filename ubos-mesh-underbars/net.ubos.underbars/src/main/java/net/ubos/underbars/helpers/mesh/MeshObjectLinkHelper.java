//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Insert the link to a MeshObject.
 */
public class MeshObjectLinkHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        Options.Buffer buf = options.buffer();
        buf.append( formatMeshObjectLinkStart( model, obj, options ));
        buf.append( options.fn());
        buf.append( formatMeshObjectLinkEnd( model, obj, options ));

        return buf;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        STRING_REPRESENTATION_ARGNAME,
        CSS_CLASS_ARGNAME,
        VIEWLET_ARGNAME,
        LINK_TITLE_ARGNAME,
        LINK_TARGET_ARGNAME,
        LINK_EXTRA_ARGNAME
    };
}
