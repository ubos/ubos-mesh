//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.util;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import net.ubos.util.ArrayHelper;
import net.ubos.util.UrlUtils;
import net.ubos.web.AbstractUrl;

/**
 * A Url, implemented for Undertow.
 */
public class UndertowUrl
    extends
        AbstractUrl
{
    /**
     * Factory method.
     */
    static UndertowUrl create(
            String               protocol,
            String               server,
            int                  port,
            String               serverPlusNonDefaultPort,
            String               relativeBaseUri,
            String               queryString,
            Map<String,String[]> urlArguments,
            String               contextPath )
    {
        return new UndertowUrl(
                protocol,
                server,
                port,
                serverPlusNonDefaultPort,
                relativeBaseUri,
                queryString,
                urlArguments,
                contextPath );
    }

    /**
     * Private constructor, for subclasses only.
     *
     * @param protocol http or https
     * @param server the server
     * @param port the server port
     * @param serverPlusNonDefaultPort the server, plus, if the port is non-default, a colon and the port number
     * @param relativeBaseUri the relative base URI
     * @param queryString the string past the ? in the URL
     * @param urlArguments the arguments given in the URL, if any
     * @param contextPath the JEE context path
     */
    protected UndertowUrl(
            String               protocol,
            String               server,
            int                  port,
            String               serverPlusNonDefaultPort,
            String               relativeBaseUri,
            String               queryString,
            Map<String,String[]> urlArguments,
            String               contextPath )
    {
        super( protocol, server, port, serverPlusNonDefaultPort, relativeBaseUri, queryString, urlArguments, contextPath );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UndertowUrl appendOrReplaceUrlArgument(
            String name,
            String value )
    {
        Map<String,String[]> newArguments = new HashMap<>();
        for( Map.Entry<String,String[]> entry : theUrlArguments.entrySet() ) {
            if( !name.equals( entry.getKey() )) {
                newArguments.put( entry.getKey(), entry.getValue() );
            }
        }
        newArguments.put( name, new String[] { value } );

        StringBuilder   newQueryString = new StringBuilder();
        StringTokenizer pairTokenizer  = new StringTokenizer( theQueryString, "&" );
        String          sep            = "";

        boolean found = false;
        while( pairTokenizer.hasMoreTokens() ) {
            String    pair     = pairTokenizer.nextToken();
            String [] keyValue = pair.split( "=", 2 );

            newQueryString.append( sep );
            newQueryString.append( keyValue[0] );
            newQueryString.append( '=' );

            String key = UrlUtils.decodeUrlArgument( keyValue[0] );
            String newValue;
            if( key.equals( name )) {
                newQueryString.append( UrlUtils.encodeToValidUrlArgument( value ));
                found = true;

            } else if( keyValue.length == 2 ) {
                newQueryString.append( keyValue[1] );
            }
            sep = "&";
        }
        if( !found ) {
            newQueryString.append( sep );
            newQueryString.append( UrlUtils.encodeToValidUrlArgument( name ));
            newQueryString.append( '=' );
            newQueryString.append( UrlUtils.encodeToValidUrlArgument( value ));
        }

        return new UndertowUrl(
                theProtocol,
                theServer,
                thePort,
                theServerPlusNonDefaultPort,
                theRelativeBaseUri,
                newQueryString.toString(),
                newArguments,
                theContextPath );

    }
}

