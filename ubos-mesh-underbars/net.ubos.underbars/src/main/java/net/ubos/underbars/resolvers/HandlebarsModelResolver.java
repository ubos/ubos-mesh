//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resolvers;

import com.github.jknack.handlebars.ValueResolver;
import java.util.Map;
import java.util.Set;
import net.ubos.underbars.HandlebarsModel;

/**
 * Bridges the Handlebars model resolution mechanism to our HandlebarsModel.
 */
public class HandlebarsModelResolver
    implements
        ValueResolver
{
    /**
     * Singleton.
     */
    public static final HandlebarsModelResolver SINGLETON = new HandlebarsModelResolver();

    /**
     * Private constructor, singleton only
     */
    private HandlebarsModelResolver()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Object resolve(
            Object context,
            String name )
    {
        Object ret = null;
        if( context instanceof HandlebarsModel ) {
            ret = ((HandlebarsModel) context).get( name );
        }
        return ret == null ? UNRESOLVED : ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object resolve(
            Object context )
    {
        if( context instanceof HandlebarsModel ) {
            return context;
        }
        return UNRESOLVED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Map.Entry<String, Object>> propertySet(
            Object o )
    {
        return null;
    }
}
