//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.paginate;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Factors out functionality common to helpers to support paged navigation.
 */
public abstract class AbstractPagingNavigationHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * Obtain the URL to the Viewlet at the destination page. If this
     * returns null, navigation is not possible (e.g. last page).
     *
     * @param model the Handlebars model
     * @return the destination page URL as String
     */
    protected abstract String getDestinationUrl(
            ViewletModel model );

    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String label          = options.hash( LABEL_ARGNAME );
        String img            = options.hash( IMG_ARGNAME );
        String imgAlt         = options.hash( IMG_ALT_ARGNAME );
        String inactiveLabel  = options.hash( INACTIVE_LABEL_ARGNAME );
        String inactiveImg    = options.hash( INACTIVE_IMG_ARGNAME );
        String inactiveImgAlt = options.hash( INACTIVE_IMG_ALT_ARGNAME );

        Options.Buffer buffer = options.buffer();

        String destinationUrl = getDestinationUrl( model );

        if( destinationUrl != null ) {
            buffer.append( "<a href=\"" );
            buffer.append( StringHelper.stringToHtmlUrl( destinationUrl ));
            buffer.append( "\">" );
            if( img != null ) {
                buffer.append( "<img src=\"" );
                buffer.append( img );
                if( imgAlt != null ) {
                    buffer.append( "\" alt=\"" );
                    buffer.append( imgAlt );
                    buffer.append( "\" title=\"" );
                    buffer.append( imgAlt );
                }
                buffer.append( "\">" );
            }
            if( label != null ) {
                buffer.append( label );
            }
            buffer.append( "</a>" );

        } else {
            if( inactiveLabel == null ) {
                inactiveLabel = label;
            }
            if( inactiveImg == null ) {
                inactiveImg = img;
            }
            if( inactiveImgAlt == null ) {
                inactiveImgAlt = imgAlt;
            }
            if( inactiveImg != null ) {
                buffer.append( "<img src=\"" );
                buffer.append( inactiveImg );
                if( inactiveImgAlt != null ) {
                    buffer.append( "\" alt=\"" );
                    buffer.append( inactiveImgAlt );
                    buffer.append( "\" title=\"" );
                    buffer.append( inactiveImgAlt );
                }
                buffer.append( "\">" );
            }
            if( inactiveLabel != null ) {
                buffer.append( inactiveLabel );
            }
        }
        return buffer;
    }

    /**
     * Name of the argument indicating a label that shall be shown.
     */
    public static final String LABEL_ARGNAME = "label";

    /**
     * Name of the argument indicating the URL to an image that shall be shown.
     */
    public static final String IMG_ARGNAME = "img";

    /**
     * Name of the argument indicating the "alt" attribute of an image that shall be shown.
     */
    public static final String IMG_ALT_ARGNAME = "imgAlt";

    /**
     * Name of the argument indicating a label that shall be shown when the button is inactive and
     * paginating forward is not possible. Overrides label.
     */
    public static final String INACTIVE_LABEL_ARGNAME = "inactiveLabel";

    /**
     * Name of the argument indicating the URL to an image that shall be shown when the button is inactive
     * and paginating forward is not possible. Overrides img.
     */
    public static final String INACTIVE_IMG_ARGNAME = "inactiveImg";

    /**
     * Name of the argument indicating the "alt" attribute of an image that shall be shown when the button is inactive
     * and paginating forward is not possible. Overrides imgAlt.
     */
    public static final String INACTIVE_IMG_ALT_ARGNAME = "inactiveImgAlt";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        LABEL_ARGNAME,
        IMG_ARGNAME,
        IMG_ALT_ARGNAME,
        INACTIVE_LABEL_ARGNAME,
        INACTIVE_IMG_ARGNAME,
        INACTIVE_IMG_ALT_ARGNAME
    };
}
