//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers;

/**
 * Wraps an Exception into a RuntimeException so we can throw it in one of our Helpers.
 */
public class HelpersRuntimeException
    extends
        RuntimeException
{
    /**
     * Constructor.
     *
     * @param cause the cause
     */
    public HelpersRuntimeException(
            Throwable cause )
    {
        super( cause );
    }

    /**
     * Constructor.
     *
     * @param message the message
     * @param cause the cause
     */
    public HelpersRuntimeException(
            String    message,
            Throwable cause )
    {
        super( message, cause );
    }
}
