//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resources;

import java.io.File;
import java.io.IOException;
import net.ubos.underbars.templatesource.FileTemplateSourceManager;
import net.ubos.util.nameserver.AbstractMNameServer;
import net.ubos.util.FileUtils;

/**
 * Knows how to manage the TemplateMaps that belong to the various Viewlets, keyed by Viewlet name.
 */
public class ViewletTemplateManager
    extends
        AbstractMNameServer<String,TemplateMap>
{
    /**
     * Factory method.
     *
     * @return the created instance
     */
    public static ViewletTemplateManager create()
    {
        ViewletTemplateManager ret = new ViewletTemplateManager();
        return ret;
    }

    /**
     * Private constructor, use factory method.
     */
    protected ViewletTemplateManager()
    {}

    /**
     * Look for Templates in this directory, and add them.
     *
     * @param dir the directory containing the Templates
     */
    public void addTemplatesInDir(
            File dir )
    {
        if( !dir.exists() ) {
            return;
        }
        if( !dir.isDirectory() ) {
            throw new IllegalArgumentException( "Not a directory: " + dir.getAbsolutePath() );
        }

        File [] directories = FileUtils.findFiles(
                dir,
                (File file) -> file.isDirectory());

        if( directories != null && directories.length > 0 ) {
            int dirLen = dir.toString().length();
            for( File current : directories ) {
                String dirName = current.getAbsolutePath();
                String name    = dirName.substring( dirLen+1 );

                FileTemplateSourceManager sourceMap = FileTemplateSourceManager.create();
                sourceMap.addFileTemplatesInDir( current, FileTemplateSourceManager.TEMPLATE_EXTENSIONS );

                TemplateMap templateMap = ViewletTemplateMap.create( sourceMap );

                super.put( name, templateMap );
            }
        }
    }

    /**
     * Test compile all skins.
     *
     * @throws IOException a skin could not be compiled
     */
    public void testCompile()
            throws
            IOException
    {
        for( TemplateMap m : valueIterator() ) {
            m.testCompile();
        }
    }
}
