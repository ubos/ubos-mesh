//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.Serializable;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.text.StringRepresentationScheme;
import net.ubos.mesh.text.StringRepresentationSchemeDirectory;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.PropertyValueParsingException;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.model.traverse.TraverseTranslatorException;
import net.ubos.model.util.SubjectAreaTagMap;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.text.StringifierParameters;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebMeshObjectsToViewFactory;
import net.ubos.web.vl.WebViewlet;
import net.ubos.web.vl.WebViewletState;
import net.ubos.model.traverse.TraverseTranslator;

/**
 * Functionality common to Handlebars Helpers dealing with MeshObjects.
 */
public abstract class AbstractMeshObjectHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * Obtain the default MeshObject. This is here to be consistent across many helpers.
     *
     * @param model The ViewletModel.
     * @param options The options object.
     * @return the default MeshObject
     */
    public static MeshObject getPrimaryMeshObject(
            ViewletModel model,
            Options      options )
    {
        MeshObject ret;
        if( options.hash.containsKey( MESHOBJECT_ARGNAME )) {
            ret = options.hash( MESHOBJECT_ARGNAME );
        } else {
            ret = (MeshObject) model.get( HandlebarsModel.MESHOBJECT_KEY );
        }
        return ret;
    }

    /**
     * Obtain the default neighbor MeshObject. This is here to be consistent across many helpers.
     *
     * @param model The ViewletModel.
     * @param options The options object.
     * @return the neighbor MeshObject
     */
    public static MeshObject getNeighborMeshObject(
            ViewletModel model,
            Options      options )
    {
        MeshObject neighbor = options.hash( NEIGHBOR_ARGNAME );
        // not sure there is a neighbor in a model
        return neighbor;
    }

    /**
     * Obtain the default MeshType. This is here to be consistent across many tags.
     *
     * @param model The ViewletModel.
     * @param options The options object.
     * @return the default MeshType
     */
    public static MeshType getDefaultMeshType(
            ViewletModel model,
            Options      options )
    {
        MeshType obj = options.hash( MESHTYPE_ARGNAME, null );
        if( obj == null ) {
            Object typeId = options.hash( MESHTYPEID_ARGNAME, null );
            if( typeId != null ) {
                if( typeId instanceof MeshTypeIdentifier ) {
                    obj = ModelBase.SINGLETON.findMeshTypeOrNull( (MeshTypeIdentifier) typeId );
                } else if( typeId instanceof String ) {
                    obj = ModelBase.SINGLETON.findMeshTypeOrNull( (String) typeId );
                } else {
                    throw new IllegalArgumentException( MESHTYPE_ARGNAME + " must be a MeshTypeIdentifier or a String, is: " + typeId.getClass() );
                }
            }
        }
        return obj;
    }

    /**
     * Obtain the default TraverseSpecification. This is here to be consistent across many tags.
     *
     * @param model The ViewletModel.
     * @param options The options object.
     * @param startObject the MeshObject on which the TraverseSpecification is supposed be applied
     * @return the default TraverseSpecification, or null if none was specified
     * @throws TraverseTranslatorException if translation failed
     */
    public static TraverseSpecification getDefaultTraverseSpecification(
            ViewletModel model,
            Options      options,
            MeshObject   startObject )
        throws
            TraverseTranslatorException
    {
        return getTraverseSpecification( model, options, startObject, TRAVERSE_ARGNAME );
    }

    /**
     * Helper to find a TraverseSpecification.
     *
     * @param model the ViewletModel to use
     * @param options determine options from here
     * @param startObject the MeshObject on which the TraverseSpecification is supposed be applied
     * @param argName name of the options hash parameter that contains the String form of the TraverseSpecification
     * @return the TraverseSpecification
     * @throws TraverseTranslatorException if translation failed
     */
    public static TraverseSpecification getTraverseSpecification(
            ViewletModel model,
            Options      options,
            MeshObject   startObject,
            String       argName )
        throws
            TraverseTranslatorException
    {
        String              traversalString = (String) options.hash( argName );
        TraverseTranslator translator      = Underbars.getTraverseTranslator();

        TraverseSpecification ret = translator.translateTraverseSpecification(
                startObject,
                (SubjectAreaTagMap) model.get( ViewletModel.SUBJECT_AREA_TAG_MAP_KEY ),
                traversalString );
        return ret;
    }

    /**
     * Make looking up a PropertyType consistent everywhere.
     *
     * @param meshObject the context MeshObject, if any
     * @return the PropertyType
     * @throws ParseException the PropertyType identifier could not be parsed
     */
    public static PropertyType determinePropertyType(
            ViewletModel model,
            Options      options,
            String       paramName,
            MeshObject   meshObject )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        Object param = options.hash( paramName );
        if( param == null ) {
            return null;
        }
        return determinePropertyType( model, meshObject, param );
    }

    public static PropertyType determinePropertyType(
            ViewletModel model,
            MeshObject   meshObject,
            Object      propertyTypeObject )
        throws
            ParseException,
            MeshTypeNotFoundException
    {

        PropertyType ret = null;
        if( propertyTypeObject instanceof PropertyType ) {
            ret = (PropertyType) propertyTypeObject;

        } else if( propertyTypeObject instanceof MeshTypeIdentifier ) {
            ret = findPropertyTypeOrThrow( (MeshTypeIdentifier) propertyTypeObject );

        } else if( propertyTypeObject instanceof String ) {
            ret = findPropertyTypeOrThrow(
                    meshObject,
                    (String) propertyTypeObject,
                    (SubjectAreaTagMap) model.get( ViewletModel.SUBJECT_AREA_TAG_MAP_KEY ) );

        } else {
            throw new IllegalArgumentException( "Unexpected type " + propertyTypeObject.getClass().getName() + ": " + propertyTypeObject );
        }
        if( ret == null ) {
            throw new IllegalArgumentException( "Cannot find PropertyType: " + propertyTypeObject );
        }
        return ret;
    }

    /**
     * Helper to find a PropertyType.
     *
     * @param id the MeshTypeIdentifier
     * @return the PropertyType
     * @throws MeshTypeNotFoundException the MeshType could not be found
     */
    public static PropertyType findPropertyTypeOrThrow(
            MeshTypeIdentifier id )
        throws
            MeshTypeNotFoundException
    {
        return ModelBase.SINGLETON.findPropertyType( id );
    }

    /**
     * Helper to find a PropertyType.
     *
     * @param id the MeshTypeIdentifier asString
     * @return the PropertyType
     * @throws ParseException the PropertyType identifier could not be parsed
     * @throws MeshTypeNotFoundException the MeshType could not be found
     */
    public static PropertyType findPropertyTypeOrThrow(
            String id )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        return ModelBase.SINGLETON.findPropertyType( id );
    }

    /**
     * Helper to find a PropertyType.
     *
     * @param obj the MeshObject that supposedly carries a property with this type, or null if not known
     * @param id the MeshTypeIdentifier
     * @param tagMap maps short names to SubjectAreas
     * @return the PropertyType
     * @throws ParseException the PropertyType identifier could not be parsed
     * @throws MeshTypeNotFoundException the MeshType could not be found
     */
    public static PropertyType findPropertyTypeOrThrow(
            MeshObject        obj,
            String            id,
            SubjectAreaTagMap tagMap )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        PropertyType ret = ModelBase.SINGLETON.findPropertyTypeOrNull( id );
        if( ret == null ) {
            ret = (PropertyType) tagMap.findCollectableMeshType( id );
        }
        if( ret == null ) {
            int slash = id.indexOf( '/' );
            if( slash >= 0 ) {
                SubjectArea sa = tagMap.getSubjectAreaFor( id.substring( 0, slash ) );
                if( sa != null ) {
                    ret = (PropertyType) sa.findCollectableMeshTypeByLocalIdentifierOrNull( id.substring( slash+1 ));
                }

            } else if( obj != null ) {
                PropertyType[] allPropTypes = obj.getPropertyTypes();
                for( PropertyType pt : allPropTypes ) {
                    if( pt.getName().value().equals( id ) ) {
                        ret = pt;
                        break;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Helper to find a Role PropertyType.
     *
     * @param obj the MeshObject that supposedly carries a property with this type with respect to this neighbor
     * @param neighbor the neighbor MeshObject
     * @param id the MeshTypeIdentifier
     * @param tagMap maps short names to SubjectAreas
     * @return the PropertyType
     * @throws ParseException the PropertyType identifier could not be parsed
     * @throws MeshTypeNotFoundException the MeshType could not be found
     */
    public static PropertyType findRolePropertyTypeOrThrow(
            MeshObject        obj,
            MeshObject        neighbor,
            String            id,
            SubjectAreaTagMap tagMap )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        PropertyType ret = ModelBase.SINGLETON.findPropertyTypeOrNull( id );
        if( ret == null ) {
            ret = (PropertyType) tagMap.findCollectableMeshType( id );
        }
        if( ret == null ) {
            int slash = id.indexOf( '/' );
            if( slash >= 0 ) {
                SubjectArea sa = tagMap.getSubjectAreaFor( id.substring( 0, slash ) );
                if( sa != null ) {
                    ret = (PropertyType) sa.findCollectableMeshTypeByLocalIdentifierOrNull( id.substring( slash+1 ));
                }

            } else {
                PropertyType[] allPropTypes = obj.getRolePropertyTypes( neighbor );
                for( PropertyType pt : allPropTypes ) {
                    if( pt.getName().value().equals( id ) ) {
                        ret = pt;
                        break;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Format a MeshObjectIdentifier.
     *
     * @param model the ViewletModel to use
     * @param id the MeshObjectIdentifier
     * @param options determine options from here
     * @return String output
     */
    public static String formatMeshObjectIdentifier(
            ViewletModel                  model,
            MeshObjectIdentifier          id,
            Options                       options )
    {
        StringRepresentationSchemeDirectory stringRepDir
                = (StringRepresentationSchemeDirectory) model.get( HandlebarsModel.STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY );

        StringRepresentationScheme scheme
                = stringRepDir.findScheme( options.hash( STRING_REPRESENTATION_ARGNAME, STRING_REPRESENTATION_ARG_DEFAULT ));

        int maxLength = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        if( maxLength != -1 ) {
            pars = pars.with( StringifierParameters.MAX_LENGTH_KEY, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }
        return scheme.formatMeshObjectIdentifier( id, pars );
    }

    /**
     * Format a MeshObject.
     *
     * @param model the ViewletModel to use
     * @param obj the MeshObject
     * @param options determine options from here
     * @return String output
     */
    public static String formatMeshObject(
            ViewletModel model,
            MeshObject   obj,
            Options      options )
    {
        StringRepresentationSchemeDirectory stringRepDir
                = (StringRepresentationSchemeDirectory) model.get( HandlebarsModel.STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY );

        StringRepresentationScheme scheme
                = stringRepDir.findScheme( options.hash( STRING_REPRESENTATION_ARGNAME, STRING_REPRESENTATION_ARG_DEFAULT ));

        int maxLength = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        if( maxLength != -1 ) {
            pars = pars.with(StringifierParameters.MAX_LENGTH_KEY, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        return scheme.formatMeshObject( obj, pars );
    }

    /**
     * Format the opening tag of an a href link to the MeshObject
     *
     * @param model the ViewletModel to use
     * @param obj the MeshObject
     * @param options determine options from here
     * @return String output
     */
    public static String formatMeshObjectLinkStart(
            ViewletModel model,
            MeshObject   obj,
            Options      options )
    {
        StringRepresentationSchemeDirectory stringRepDir
                = (StringRepresentationSchemeDirectory) model.get( HandlebarsModel.STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY );

        WebMeshObjectsToViewFactory toViewFactory
                = (WebMeshObjectsToViewFactory) model.get( ViewletModel.WEB_MESH_OBJECTS_TO_VIEW_FACTORY_KEY );

        StringRepresentationScheme scheme
                = stringRepDir.findScheme( options.hash( STRING_REPRESENTATION_ARGNAME, STRING_REPRESENTATION_ARG_DEFAULT ));

        StructuredResponse parentResponse = (StructuredResponse) model.get( ViewletModel.STRUCTURED_RESPONSE_KEY );

        String cssClass = options.hash( CSS_CLASS_ARGNAME );
        String flavor   = options.hash( FLAVOR_ARGNAME );
        String viewlet  = options.hash( VIEWLET_ARGNAME );
        String title    = options.hash( LINK_TITLE_ARGNAME );
        String target   = options.hash( LINK_TARGET_ARGNAME );
        String extra    = options.hash( LINK_EXTRA_ARGNAME );

        WebMeshObjectsToView toView      = (WebMeshObjectsToView) model.get( ViewletModel.TOVIEW_KEY );
        WebMeshObjectsToView childToView = toViewFactory.obtainFor( obj, parentResponse.getRequest().getUrl().getContextUrl() );

        long when = toView.getWhen();
        if( when != Long.MAX_VALUE ) {
            childToView = childToView.withAtTime( when );
        }
        if( viewlet != null ) {
            childToView = childToView.withRequiredViewletName( viewlet );
        }

        StringifierParameters pars = StringifierParameters.EMPTY;
        pars = pars.with( StringifierParameters.MESHOBJECTS_TO_VIEW_KEY, childToView );
        if( cssClass != null ) {
            pars = pars.with( StringifierParameters.HTML_ADDITIONAL_CSS_CLASS_KEY, cssClass );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }
        if( extra != null ) {
            pars = pars.with( StringifierParameters.HTML_URL_ADDITIONAL_ARGUMENTS_KEY, extra );
        }
        if( title != null ) {
            pars = pars.with( StringifierParameters.LINK_TITLE_KEY, title );
        }
        if( target != null ) {
            pars = pars.with( StringifierParameters.LINK_TARGET_KEY, target );
        }

        return scheme.formatMeshObjectLinkStart( obj, pars );
    }

    /**
     * Format the closing tag of an a href link to the MeshObject
     *
     * @param model the ViewletModel to use
     * @param obj the MeshObject
     * @param options determine options from here
     * @return String output
     */
    public static String formatMeshObjectLinkEnd(
            ViewletModel model,
            MeshObject   obj,
            Options      options )
    {
        StringRepresentationSchemeDirectory stringRepDir
                = (StringRepresentationSchemeDirectory) model.get( HandlebarsModel.STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY );

        StringRepresentationScheme scheme
                = stringRepDir.findScheme( options.hash( STRING_REPRESENTATION_ARGNAME, STRING_REPRESENTATION_ARG_DEFAULT ));

        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        return scheme.formatMeshObjectLinkEnd( obj, pars );
    }

    /**
     * Format an Attribute value.
     *
     * @param obj the MeshObject whose Attribute this is
     * @param name the Attribute's name
     * @param value the Attribute's value
     * @param model the ViewletModel to use
     * @param options determine options from here
     * @return String output
     */
    public static String formatAttribute(
            MeshObject   obj,
            String       name,
            Serializable value,
            ViewletModel model,
            Options      options )
    {
        StringRepresentationScheme scheme = findStringRepresentationScheme( model, options );

        int maxLength = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        pars = pars.with( VIEWLET_MODEL_KEY, model );

        if( maxLength != -1 ) {
            pars = pars.with( StringifierParameters.MAX_LENGTH_KEY, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        return scheme.formatAttribute( obj, name, value, pars );
    }

    /**
     * Format a Role Attribute value.
     *
     * @param obj the MeshObject on whose side of the relationship with neighbor this Role Attribute is
     * @param neighbor the neighbor MeshObject
     * @param model the ViewletModel to use
     * @param name the Role Attribute's name
     * @param value the Role Attribute's value
     * @param options determine options from here
     * @return String output
     */
    public static String formatRoleAttribute(
            MeshObject   obj,
            MeshObject   neighbor,
            String       name,
            Serializable value,
            ViewletModel model,
            Options      options )
    {
        StringRepresentationScheme scheme = findStringRepresentationScheme( model, options );

        int maxLength = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        pars = pars.with( VIEWLET_MODEL_KEY, model );

        if( maxLength != -1 ) {
            pars = pars.with( StringifierParameters.MAX_LENGTH_KEY, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        return scheme.formatRoleAttribute( obj, neighbor, name, value, pars );
    }

    /**
     * Format a Property.
     *
     * @param obj the MeshObject whose Property this is
     * @param propertyType the PropertyType
     * @param currentValue the current PropertyValue of the PropertyType
     * @param defaultValue show this default value, given as String or PropertyValue, instead, if the value is null
     * @param model the ViewletModel to use
     * @param options determine options from here
     * @return String output
     * @throws PropertyValueParsingException thrown if a default value cannot be parsed
     */
    public static String formatProperty(
            MeshObject    obj,
            PropertyType  propertyType,
            PropertyValue currentValue,
            Object        defaultValue,
            ViewletModel  model,
            Options       options )
        throws
            PropertyValueParsingException
    {
        StringRepresentationScheme scheme = findStringRepresentationScheme( model, options );

        int maxLength     = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor     = options.hash( FLAVOR_ARGNAME );
        String showUpload = options.hash( SHOW_UPLOAD_ARGNAME, StringifierParameters.DEFAULT_UPLOAD_VALUE );

        StringifierParameters pars = StringifierParameters.EMPTY;
        pars = pars.with( VIEWLET_MODEL_KEY, model );
        pars = pars.with( StringifierParameters.UPLOAD_KEY, showUpload );

        if( maxLength != -1 ) {
            pars = pars.with( StringifierParameters.MAX_LENGTH_KEY, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        PropertyValue toShow;
        if( currentValue == null ) {
            if( defaultValue == null ) {
                toShow = null;

            } else if( defaultValue instanceof PropertyValue ) {
                toShow = (PropertyValue) defaultValue;

            } else if( defaultValue instanceof String ) {
                toShow = scheme.parsePropertyValue( (String) defaultValue, propertyType );

            } else {
                throw new IllegalArgumentException( "Unexpected type " + defaultValue.getClass().getName() + ": " + defaultValue );
            }
        } else {
            toShow = currentValue;
        }

        return scheme.formatProperty( obj, propertyType, toShow, pars );
    }

    /**
     * Format a Property that doesn't exist yet
     *
     * @param objVar name of the Http Shell var
     * @param propertyType the PropertyType
     * @param currentValue the current PropertyValue of the PropertyType
     * @param defaultValue show this default value, given as String or PropertyValue, instead, if the value is null
     * @param model the ViewletModel to use
     * @param options determine options from here
     * @return String output
     * @throws PropertyValueParsingException thrown if a default value cannot be parsed
     */
    public static String formatFutureProperty(
            String        objVar,
            PropertyType  propertyType,
            PropertyValue currentValue,
            Object        defaultValue,
            ViewletModel  model,
            Options       options )
        throws
            PropertyValueParsingException
    {
        StringRepresentationScheme scheme = findStringRepresentationScheme( model, options, "Future" );

        int maxLength     = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor     = options.hash( FLAVOR_ARGNAME );
        String showUpload = options.hash( SHOW_UPLOAD_ARGNAME, StringifierParameters.DEFAULT_UPLOAD_VALUE );

        StringifierParameters pars = StringifierParameters.EMPTY;
        pars = pars.with( VIEWLET_MODEL_KEY, model );
        pars = pars.with( StringifierParameters.UPLOAD_KEY, showUpload );

        if( maxLength != -1 ) {
            pars = pars.with( StringifierParameters.MAX_LENGTH_KEY, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        PropertyValue toShow;
        if( currentValue == null ) {
            if( defaultValue == null ) {
                toShow = null;

            } else if( defaultValue instanceof PropertyValue ) {
                toShow = (PropertyValue) defaultValue;

            } else if( defaultValue instanceof String ) {
                toShow = scheme.parsePropertyValue( (String) defaultValue, propertyType );

            } else {
                throw new IllegalArgumentException( "Unexpected type " + defaultValue.getClass().getName() + ": " + defaultValue );
            }
        } else {
            toShow = currentValue;
        }

        return scheme.formatFutureProperty( objVar, propertyType, toShow, pars );
    }

    /**
     * Format a Role Property.
     *
     * @param obj the MeshObject on whose side of the relationship with neighbor the Property this is
     * @param neighbor the neighbor
     * @param propertyType the PropertyType
     * @param currentValue the current PropertyValue of the PropertyType
     * @param defaultValue show this default value, given as String or PropertyValue, instead, if the value is null
     * @param model the ViewletModel to use
     * @param options determine options from here
     * @return String output
     * @throws PropertyValueParsingException thrown if a default value cannot be parsed
     */
    public static String formatRoleProperty(
            MeshObject    obj,
            MeshObject    neighbor,
            PropertyType  propertyType,
            PropertyValue currentValue,
            Object        defaultValue,
            ViewletModel  model,
            Options       options )
        throws
            PropertyValueParsingException
    {
        StringRepresentationScheme scheme = findStringRepresentationScheme( model, options );

        int maxLength = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        pars = pars.with( VIEWLET_MODEL_KEY, model );

        if( maxLength != -1 ) {
            pars = pars.with( StringifierParameters.MAX_LENGTH_KEY, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        PropertyValue toShow;
        if( currentValue == null ) {
            if( defaultValue == null ) {
                toShow = null;

            } else if( defaultValue instanceof PropertyValue ) {
                toShow = (PropertyValue) defaultValue;

            } else if( defaultValue instanceof String ) {
                toShow = scheme.parsePropertyValue( (String) defaultValue, propertyType );

            } else {
                throw new IllegalArgumentException( "Unexpected type " + defaultValue.getClass().getName() + ": " + defaultValue );
            }
        } else {
            toShow = currentValue;
        }

        return scheme.formatRoleProperty( obj, neighbor, propertyType, toShow, pars );
    }

    /**
     * Format a DataType.
     *
     * @param model the ViewletModel to use
     * @param dataType the DataType to format
     * @param options determine options from here
     * @return String output
     */
    public static String formatDataType(
            ViewletModel  model,
            DataType dataType,
            Options  options )
    {
        StringRepresentationSchemeDirectory stringRepDir
                = (StringRepresentationSchemeDirectory) model.get( HandlebarsModel.STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY );

        StringRepresentationScheme scheme
                = stringRepDir.findScheme( options.hash( STRING_REPRESENTATION_ARGNAME, STRING_REPRESENTATION_ARG_DEFAULT ));

        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }

        return scheme.formatDataType( dataType, pars );
    }

    /**
     * Format a MeshTypeIdentifier.
     *
     * @param model the ViewletModel to use
     * @param id the MeshTypeIdentifier
     * @param options determine options from here
     * @return String output
     */
    public static String formatMeshTypeIdentifier(
            ViewletModel       model,
            MeshTypeIdentifier id,
            Options            options )
    {
        StringRepresentationSchemeDirectory stringRepDir
                = (StringRepresentationSchemeDirectory) model.get( HandlebarsModel.STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY );

        StringRepresentationScheme scheme
                = stringRepDir.findScheme( options.hash( STRING_REPRESENTATION_ARGNAME, STRING_REPRESENTATION_ARG_DEFAULT ));

        int maxLength = options.hash( MAX_LENGTH_ARGNAME, -1 );
        String flavor = options.hash( FLAVOR_ARGNAME );

        StringifierParameters pars = StringifierParameters.EMPTY;
        if( maxLength != -1 ) {
            pars = pars.with( MAX_LENGTH_ARGNAME, maxLength );
        }
        if( flavor != null ) {
            pars = pars.with( StringifierParameters.FLAVOR_KEY, flavor );
        }
        return scheme.formatMeshTypeIdentifier( id, pars );
    }

    /**
     * A consistent way of finding the correct StringRepresentationScheme.
     *
     * @param model the ViewletModel to use
     * @param options determine options from here
     * @return the StringRepresentationScheme
     */
    public static StringRepresentationScheme findStringRepresentationScheme(
            ViewletModel model,
            Options      options )
    {
        return findStringRepresentationScheme( model, options, null );
    }

    /**
     * A consistent way of finding the correct StringRepresentationScheme.
     *
     * @param model the ViewletModel to use
     * @param options determine options from here
     * @param fallbackPrefixName if none we given, use the StringRepresentationScheme with this prefix as fallback
     * @return the StringRepresentationScheme
     */
    public static StringRepresentationScheme findStringRepresentationScheme(
            ViewletModel model,
            Options      options,
            String       fallbackPrefixName )
    {
        StringRepresentationSchemeDirectory stringRepDir
                = (StringRepresentationSchemeDirectory) model.get( HandlebarsModel.STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY );

        String schemeName = options.hash( STRING_REPRESENTATION_ARGNAME );
        if( schemeName == null ) {
            // has not been explicitly stated
            WebViewlet      vl    = (WebViewlet) model.get( ViewletModel.VIEWLET_KEY );
            WebViewletState state = vl.getState();

            if( state.isDefaultState() ) {
                schemeName = STRING_REPRESENTATION_ARG_DEFAULT;
            } else {
                schemeName = state.getName() + STRING_REPRESENTATION_ARG_DEFAULT;
            }
            if( fallbackPrefixName != null ) {
                schemeName = fallbackPrefixName + schemeName;
            }
        }

        StringRepresentationScheme ret = stringRepDir.findScheme( schemeName );
        return ret;
    }

    /**
     * Name of the argument on Helpers that can specify the primary MeshObject.
     */
    public static final String MESHOBJECT_ARGNAME = "meshObject";

    /**
     * Name of the argument on Helpers that can specify the HttpShell variable
     * that should be used for the primary MeshObject.
     */
    public static final String MESHOBJECT_SHELL_VAR_NAME_ARGNAME = "meshObjectShellVarName";

    /**
     * Name of the argument on Helpers that can specify the neighbor MeshObject.
     */
    public static final String NEIGHBOR_ARGNAME = "neighbor";

    /**
     * Name of the argument on Helpers that can specify the HttpShell variable
     * that should be used on the neighbor MeshObject.
     */
    public static final String NEIGHBOR_SHELL_VAR_NAME_ARGNAME = "neighborShellVarName";

    /**
     * Name of the argument on Helpers that indicates a MeshType for this Helper.
     */
    public static final String MESHTYPE_ARGNAME = "meshType";

    /**
     * Name of the argument on Helpers that indicates the identifier for a MeshType for this Helper,
     * unless specified in more detail.
     */
    public static final String MESHTYPEID_ARGNAME = "meshTypeId";

    /**
     * Name of the argument on Helpers that indicates a TraversalSpecification for this Helper.
     */
    public static final String TRAVERSE_ARGNAME = "traverse";

    /**
     * Name of the argument that holds the MeshObjectSet to be tested.
     */
    public static final String MESHOBJECTSET_ARGNAME = "meshObjectSet";

    /**
     * Name of the argument that holds a second MeshObjectSet.
     */
    public static final String MESHOBJECTSET2_ARGNAME = "meshObjectSet2";

    /**
     * Name of the parameter indicating the object whose membership in a set shall be tested.
     */
    public static final String CANDIDATE_ARGNAME = "candidate";

    /**
     * Name of the parameter indicating the PropertyType.
     */
    public static final String PROPERTY_TYPE_ARGNAME = "propertyType";

    /**
     * Name of the parameter indicating the default value to be used.
     */
    public static final String DEFAULT_VALUE_ARGNAME = "defaultValue";

    /**
     * Name of the parameter indicating the name of an Attribute.
     */
    public static final String ATTRIBUTE_ARGNAME = "attributeName";

    /**
     * Name of the parameter indicating the name of a RoleAttribute.
     */
    public static final String ROLE_ATTRIBUTE_ARGNAME = "roleAttributeName";

    /**
     * Name of the argument on Helpers that indicates the maximum length of the emitted text.
     */
    public static final String MAX_LENGTH_ARGNAME = "maxLength";

    /**
     * Name of the argument on Helpers that indicates which Viewlet type should be
     * used.
     */
    public static final String VIEWLET_ARGNAME = "viewlet";

    /**
     * Name of the argument on Helpers that indicates a title on a link.
     */
    public static final String LINK_TITLE_ARGNAME = "title";

    /**
     * Name of the argument on Helpers that indicates the target of a link.
     */
    public static final String LINK_TARGET_ARGNAME = "target";

    /**
     * Name of the argument on Helpers that adds extra arguments to a generated URL.
     */
    public static final String LINK_EXTRA_ARGNAME = "extra";

    /**
     * Name of the argument on Helpers that indicates whether or not a upload button should be shown.
     */
    public static final String SHOW_UPLOAD_ARGNAME = "showUpload";

    /**
     * Key into the StringifierParameters that points to the ViewletModel.
     */
    public static final String VIEWLET_MODEL_KEY = "viewletModel";

}
