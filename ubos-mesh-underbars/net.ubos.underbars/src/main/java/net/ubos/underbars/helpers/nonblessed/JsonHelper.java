//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.nonblessed;

import com.github.jknack.handlebars.Options;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.nonblessed.NonblessedJsonUtils;
import net.ubos.mesh.nonblessed.NonblessedUtils;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Print a JSON element.
 */
public class JsonHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj    = getPrimaryMeshObject( model, options );
        String     indent = (String)  options.hash.getOrDefault( INDENT_ARGNAME, null );

        StringWriter w  = new StringWriter();
        JsonWriter   jw = new JsonWriter( w );
        jw.setSerializeNulls( true );

        if( indent != null ) {
            jw.setIndent( indent );
        }

        doIt( obj, jw );

        return w.toString();
    }

    /**
     * Recursive processing function.
     *
     * @param obj the current MeshObject
     * @param jw append here
     * @throws IOException
     */
    protected void doIt(
            MeshObject obj,
            JsonWriter jw )
        throws
            IOException
    {
        String type = (String) obj.getAttributeValue(NonblessedUtils.OBJECTTYPE_ATTRIBUTE  );
        if( type == null ) {
            return;
        }
        switch( type ) {
            case NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONARRAY:
                emitArray( obj, jw );
                break;

            case NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONOBJECT:
                emitObject( obj, jw );
                break;

            case NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONSTRING:
                jw.value((String) obj.getAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE ));
                break;

            case NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONNUMBER:
                jw.value((Number) obj.getAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE ));
                break;

            case NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONBOOLEAN:
                jw.value((Boolean) obj.getAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE ));
                break;

            case NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONNULL:
                jw.nullValue();
                break;
        }
    }

    /**
     * Emit a JSON array
     *
     * @param obj the current MeshObject
     * @param jw append here
     * @throws IOException
     */
    protected void emitArray(
            MeshObject obj,
            JsonWriter jw )
        throws
            IOException
    {
        jw.beginArray();

        for( MeshObject child : NonblessedJsonUtils.traverseToJsonArrayElements( obj )) {
            doIt( child, jw );
        }

        jw.endArray();
    }

    /**
     * Emit a JSON object
     *
     * @param obj the current MeshObject
     * @param jw append here
     * @throws IOException
     */
    protected void emitObject(
            MeshObject    obj,
            JsonWriter jw )
        throws
            IOException
    {
        jw.beginObject();

        MeshObjectSet children = obj.traverse(NonblessedJsonUtils.TO_CHILDREN_SPEC );
        for( MeshObject child : children ) {
            String fieldName = (String) obj.getRoleAttributeValue(child, NonblessedUtils.LOCAL_NAME_ROLE_ATTRIBUTE );

            jw.name( fieldName );
            doIt( child, jw );
        }

        jw.endObject();
    }

    /**
     * Emit a JSON String
     *
     * @param obj the current MeshObject
     * @param indent the current indentation
     * @param indentStep the additional indentation to append for child elements
     * @param depth the maximum remaining depth
     * @param buf append here
     */
    protected void emitString(
            MeshObject    obj,
            String        indent,
            StringBuilder buf )
    {
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Name of the argument that indicates the indent String (not integer).
     */
    public static final String INDENT_ARGNAME = "indent";

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        INDENT_ARGNAME
    };
}
