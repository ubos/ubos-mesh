//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Allocates one or more Shell variables.
 */
public class AllocateShellVarHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES ); // any others may be passed through to the fragment

        String varName = options.hash( VAR_NAME_ARGNAME );
        
        Options.Buffer buffer       = options.buffer();
        Context        childContext = Context.newContext( options.context, model );

        for( String current : varName.split( "," )) {
            current = current.trim();
            childContext.combine( current, model.obtainNewShellVar() );
        }
        
        buffer.append( options.apply( options.fn, childContext ));

        return buffer;
    }

    /**
     * Name of the argument indicating the name or names of the shell vars
     * that should be allocated.
     */
    public static final String VAR_NAME_ARGNAME = "varName";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        VAR_NAME_ARGNAME
    };
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
