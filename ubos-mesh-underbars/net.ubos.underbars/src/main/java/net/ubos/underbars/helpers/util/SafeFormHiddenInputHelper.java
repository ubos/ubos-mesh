//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Generates a hidden HTML input element that contains a CSRF token.
 */
public class SafeFormHiddenInputHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        Options.Buffer buffer = options.buffer();
        
        String csrfToken = model.getCsrfToken();
        buffer.append( " <input type=\"hidden\" name=\"" )
              .append( model.getCsrfFormInputName() )
              .append( "\" value=\"" )
              .append( csrfToken )
              .append( "\"/>\n" );

        return buffer;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
