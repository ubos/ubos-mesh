//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resources;

import java.io.IOException;
import com.github.jknack.handlebars.Template;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * <p>A directory of compiled Templates.
 *
 * <p>This delegates to the Handlebars cache for holding compiled templates. The
 * advantage is that this supports automatic recompile when the flag is set
 * in Handlebars.getCache().
 */
public abstract class TemplateMap
{
    /**
     * Obtain the named Template.
     *
     * @param key name of the Template
     * @return the Template
     * @throws IOException thrown if the Template could not be compiled
     */
    public abstract Template obtainFor(
            String key )
        throws
            IOException;

    /**
     * Obtain an iterator over the names of all known keys.
     *
     * @return the iterator
     */
    public abstract CursorIterator<String> keyIterator();

    /**
     * Test compile the templates.
     *
     * @throws IOException thrown if a Template could not be compiled
     */
    public void testCompile()
        throws
            IOException
    {
        for( String key : keyIterator() ) {
            obtainFor( key ); // compile. Will throw if syntax error
        }
    }
}
