//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.paginate;

import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.web.vl.PaginatingWebViewlet;

/**
 * A go-to-the-last page button for Viewlets that paginate.
 */
public class GoToLastHelper
    extends
        AbstractPagingNavigationHelper
{
    private static final Log log = Log.getLogInstance( GoToLastHelper.class );

    /**
     * Constructor.
     */
    public GoToLastHelper() {}

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDestinationUrl(
            ViewletModel model )
    {
        try {
            @SuppressWarnings("unchecked")
            PaginatingWebViewlet<MeshObject> v = (PaginatingWebViewlet<MeshObject>) model.get( ViewletModel.VIEWLET_KEY );

            CursorIterator<MeshObject> start    = v.getCurrentPageStartIterator();
            int                        pageSize = v.getPageSize();

            if( start.hasNext( pageSize+1 )) {
                CursorIterator<MeshObject> startLastPage = start.createCopy();
                startLastPage.moveToAfterLast();
                startLastPage.moveBy( -pageSize );

                return v.getDestinationUrlForStart( startLastPage.next() );

            } else {
                return null;
            }

        } catch( CannotViewException ex ) {
            log.error( ex );
            return null;
        }
    }
}
