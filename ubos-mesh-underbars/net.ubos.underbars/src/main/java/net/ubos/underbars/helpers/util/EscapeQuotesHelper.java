//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Escapes single and double quotes in the argument by preceding them with a backslash.
 */
public class EscapeQuotesHelper
    extends
        AbstractHandlebarsHelper<String>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            String  model,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String ret = model;
        ret = ret.replace( "\"", "\\\"" );
        ret = ret.replace( "'", "\\'" );
        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
