//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.model.traverse.TraverseTranslatorException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.helpers.HelpersRuntimeException;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Determine a single MeshObject, or null, by traversing from a MeshObject by means of a
 * TraversalSpecification, expecting the resulting set to have zero or one members.
 */
public class RepeatTraverseToSingleHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject            obj = AbstractMeshObjectHelper.getPrimaryMeshObject( model, options );
        TraverseSpecification traversalSpec;
        try {
            traversalSpec = AbstractMeshObjectHelper.getTraverseSpecification( model, options, obj, AbstractMeshObjectHelper.TRAVERSE_ARGNAME );

        } catch( TraverseTranslatorException ex ) {
            throw new HelpersRuntimeException( ex );
        }

        boolean reverse = DIRECTION_REVERSE_ARGVALUE.equals( options.hash( DIRECTION_ARGNAME, null ));
        int     max     = options.hash( MAX_ARGNAME, Integer.MAX_VALUE );

        if( reverse ) {
            applyReverse( model, options, obj, null, traversalSpec, max );
        } else {
            applyForward( model, options, obj, null, traversalSpec, max );
        }
        return null;
    }

    /**
     * Iterate in forward direction.
     */
    protected void applyForward(
            ViewletModel          model,
            Options               options,
            MeshObject            obj,
            MeshObject            previous,
            TraverseSpecification traversalSpec,
            int                   max )
        throws
            IOException
    {
        if( max <=0 ) {
            return;
        }
        emit( model, options, obj, previous );

        MeshObject nextObj = obj.traverse( traversalSpec ).getSingleMember();
        if( nextObj != null ) {
            applyForward( model, options, nextObj, obj, traversalSpec, max-1 );
        }
    }

    /**
     * Iterate in backward direction.
     */
    protected void applyReverse(
            ViewletModel          model,
            Options               options,
            MeshObject            obj,
            MeshObject            previous,
            TraverseSpecification traversalSpec,
            int                   max )
        throws
            IOException
    {
        if( obj == null || max <=0 ) {
            emit( model, options, previous, obj );
            return;
        }
        MeshObject nextObj = obj.traverse( traversalSpec ).getSingleMember();
        applyReverse( model, options, nextObj, obj, traversalSpec, max-1 );

        if( previous != null ) {
            emit( model, options, previous, obj );
        }
    }

    /**
     * Emit the content of the helper.
     */
    protected void emit(
            ViewletModel model,
            Options      options,
            MeshObject   current,
            MeshObject   previous )
        throws
            IOException
    {
        Context childContext = Context.newContext( options.context, model );
        childContext.combine( "current", current ).combine( "previous", previous );

        options.buffer().append( options.apply( options.fn, childContext ));
    }

    /**
     * Name of the argument on this helper that specifies the direction in which we iterate.
     */
    public static final String DIRECTION_ARGNAME = "direction";

    /**
     * Value for DIRECTION_ARGNAME that indicates the reverse direction.
     */
    public static final String DIRECTION_REVERSE_ARGVALUE = "reverse";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        AbstractMeshObjectHelper.MESHOBJECT_ARGNAME,
        AbstractMeshObjectHelper.TRAVERSE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DIRECTION_ARGNAME,
        MAX_ARGNAME
    };
}

