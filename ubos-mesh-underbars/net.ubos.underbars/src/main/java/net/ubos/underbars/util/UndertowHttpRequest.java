//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.util.HeaderMap;
import io.undertow.util.Headers;
import java.nio.charset.StandardCharsets;
import net.ubos.mesh.MeshObject;
import net.ubos.util.ArrayHelper;
import net.ubos.util.ResourceHelper;
import net.ubos.util.StreamUtils;
import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.web.AbstractHttpRequest;
import net.ubos.web.AbstractUrl;
import net.ubos.web.HttpRequest;
import net.ubos.web.IncomingHttpCookie;
import net.ubos.web.MimePart;
import net.ubos.web.Url;

/**
 * An HttpRequest that delegates to Undertow.
 */
public class UndertowHttpRequest
    extends
        AbstractHttpRequest
    implements
        CanBeDumped
{
    private static final Log log = Log.getLogInstance( UndertowHttpRequest.class ); // our own, private logger

    /**
      * Smart factory method.
      *
      * @param exchange the HttpServerExchange from which to create a UndertowHttpRequest.
      * @param appContext the context path of the app, e.g. /blog
      * @return the created UndertowHttpRequest
      */
    public static UndertowHttpRequest create(
             HttpServerExchange exchange,
             String             appContext )
    {
        UndertowHttpRequest ret = UndertowHttpRequest.internalCreate( exchange, appContext );
        return ret;
    }

    /**
      * Internal factory method.
      *
      * @param exchange the HttpServletRequest from which to create a SaneRequest.
      * @param appContext the context path of the app, e.g. /blog
      * @return the created SaneServletRequest
      */
    protected static UndertowHttpRequest internalCreate(
             HttpServerExchange exchange,
             String             appContext )
    {
        Map<String,String[]>   urlArguments    = new HashMap<>();
        Map<String,String[]>   postedArguments = new HashMap<>();
        Map<String,MimePart[]> mimeParts       = new HashMap<>();

        String postData = null;

        String queryString      = exchange.getQueryString();
        String method           = exchange.getRequestMethod().toString();
        String server           = exchange.getHostName();
        int    port             = exchange.getHostPort();
        String protocol         = exchange.getProtocol().toString().equalsIgnoreCase( "https" ) ? "https" : "http";
        String relativeBaseUri  = exchange.getRequestURI();
        String contextPath      = appContext;
        String clientIp         = exchange.getSourceAddress().toString();

        String serverWithNonDefaultPort = AbstractUrl.constructServerPlusNonDefaultPort( server, protocol, port );

        Map<String,Cookie> undertowCookies = exchange.getRequestCookies();
        IncomingCookieAdapter [] cookies;

        if( undertowCookies != null ) {
            cookies = new IncomingCookieAdapter[ undertowCookies.size() ];
            int i=0;
            for( Cookie uc : undertowCookies.values() ) {
                cookies[i++] = new IncomingCookieAdapter( uc );
            }
        } else {
            cookies = new IncomingCookieAdapter[0];
        }

        HeaderMap requestHeaders = exchange.getRequestHeaders();

        String mimeType;
        // URL parameters override POSTed fields: more intuitive for the user
        if( POST_METHOD.equalsIgnoreCase( method ) ) { // we do our own parsing

            mimeType = requestHeaders.getFirst( Headers.CONTENT_TYPE );
            try {
                exchange.startBlocking();
                BufferedInputStream inStream = new BufferedInputStream( exchange.getInputStream() );
                if( mimeType == null || mimeType.startsWith( FORM_DATA_MIME_URLENCODED )) {
                    long    length = exchange.getRequestContentLength();
                    byte [] buf    = StreamUtils.slurp( inStream, length > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) length );

                    postData = new String( buf, StandardCharsets.UTF_8 );

                    try {
                        AbstractUrl.addUrlEncodedArguments( postData, postedArguments );
                    } catch( Throwable t ) {
                        if( mimeType == null ) {
                            log.info( t );
                        } else {
                            log.warn( t );
                        }
                    }

                } else if( mimeType.startsWith( FORM_DATA_MIME_MULTIPART )) {
                    addFormDataArguments( inStream, mimeType, postedArguments, mimeParts );
                } else {
                    long    length = exchange.getRequestContentLength();
                    byte [] buf    = StreamUtils.slurp( inStream, length > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) length );

                    postData = new String( buf, StandardCharsets.UTF_8 );
                }
                postedArguments.remove( SUBMIT_PARAMETER_NAME ); // delete the contribution of the submit button

            } catch( IOException ex ) {
                log.error( ex );
            }
        } else {
            mimeType = null;
        }

        AbstractUrl.addUrlEncodedArguments( queryString, urlArguments );
        urlArguments.remove( SUBMIT_PARAMETER_NAME ); // delete the contribution of the submit button

        HttpRequest requestAtProxy = null;
        if( theDetermineRequestFromProxyOriginalRequest ) {
            // We might be behind a reverse proxy. If the appropriate headers are set, use them for
            // an original request. If not, use the local ones.

            String clientIpAtProxy     = requestHeaders.getFirst( HTTP_PROXY_HEADER_FORWARDED_FOR );
            String serverAtProxy       = requestHeaders.getFirst( HTTP_PROXY_FORWARDED_SERVER );
            String protocolAtProxy     = requestHeaders.getFirst( HTTP_PROXY_FORWARDED_PROTOCOL );
            String contextPathAtProxy  = requestHeaders.getFirst( HTTP_PROXY_FORWARDED_CONTEXT );
            String tmp                 = requestHeaders.getFirst( HTTP_PROXY_FORWARDED_PORT );

            if(    clientIpAtProxy     != null
                || serverAtProxy       != null
                || serverAtProxy       != null
                || protocolAtProxy     != null
                || contextPathAtProxy  != null
                || tmp                 != null )
            {
                if( clientIpAtProxy == null ) {
                    clientIpAtProxy = clientIp;
                }
                if( serverAtProxy == null ) {
                    serverAtProxy = server;
                }
                if( contextPathAtProxy == null ) {
                    contextPathAtProxy = contextPath;
                }

                if( protocolAtProxy != null ) {
                    protocolAtProxy = protocolAtProxy.equalsIgnoreCase( "https" ) ? "https" : "http";
                } else {
                    protocolAtProxy = protocol;
                }

                int portAtProxy;
                if( tmp != null ) {
                    portAtProxy = Integer.parseInt( tmp );
                } else {
                    portAtProxy = port;
                }

                String serverWithNonDefaultPortAtProxy = AbstractUrl.constructServerPlusNonDefaultPort( serverAtProxy, protocolAtProxy, portAtProxy );
                if( serverWithNonDefaultPortAtProxy == null ) {
                    serverWithNonDefaultPortAtProxy = serverWithNonDefaultPort;
                }

                String relativeBaseUriAtProxy;
                if( contextPathAtProxy != null ) {
                    if( contextPathAtProxy.endsWith( "/" )) {
                        // supposed to be given without slash, but then it not always is, particularly for "/" itself
                        contextPathAtProxy = contextPathAtProxy.substring( 0, contextPathAtProxy.length()-1 );
                    }
                    relativeBaseUriAtProxy = contextPathAtProxy + relativeBaseUri.substring( contextPath.length() );
                } else {
                    relativeBaseUriAtProxy = relativeBaseUri;
                }

                Url urlAtProxy = UndertowUrl.create(
                        protocolAtProxy,
                        serverAtProxy,
                        portAtProxy,
                        serverWithNonDefaultPortAtProxy,
                        relativeBaseUriAtProxy,
                        queryString,
                        urlArguments,
                        contextPathAtProxy );

                requestAtProxy = new UndertowHttpRequest(
                        urlAtProxy,
                        method,
                        null,
                        postData,
                        postedArguments,
                        mimeParts,
                        cookies,
                        mimeType,
                        clientIpAtProxy,
                        exchange );
            }
        }

        Url url = UndertowUrl.create(
                protocol,
                server,
                port,
                serverWithNonDefaultPort,
                relativeBaseUri,
                queryString,
                urlArguments,
                contextPath );

        UndertowHttpRequest ret = new UndertowHttpRequest(
                url,
                method,
                requestAtProxy,
                postData,
                postedArguments,
                mimeParts,
                cookies,
                mimeType,
                clientIp,
                exchange );

        if( log.isTraceEnabled() ) {
            log.traceConstructor( ret, ret );
        }
        return ret;
    }

    /**
     * Private constructor, use factory method
     *
     * @param url the incoming requests
     * @param method the HTTP method
     * @param requestAtProxy the SaneRequest received by the reverse proxy, if any
     * @param postData the data HTTP POST'd, if any
     * @param postedArguments the arguments given via HTTP POST, if any
     * @param mimeParts the MimeParts given via HTTP POST, if any
     * @param cookies the sent cookies
     * @param mimeType the sent MIME type, if any
     * @param clientIp the client's IP address
     * @param delegate the HttpServerExchange from which this UndertowHttpRequest is created
     */
    UndertowHttpRequest(
            Url                      url,
            String                   method,
            HttpRequest              requestAtProxy,
            String                   postData,
            Map<String,String[]>     postedArguments,
            Map<String,MimePart[]>   mimeParts,
            IncomingCookieAdapter [] cookies,
            String                   mimeType,
            String                   clientIp,
            HttpServerExchange       delegate )
    {
        super( url,
               method,
               requestAtProxy );

        theDelegate         = delegate;
        thePostData         = postData;
        thePostedArguments  = postedArguments;
        theMimeParts        = mimeParts;
        theCookies          = cookies;
        theMimeType         = mimeType;
        theClientIp         = clientIp;
    }

    /**
     * Helper to parse formdata-encoded POST data, and put them in the right places.
     *
     * @param inStream the incoming data
     * @param mime the MIME type of the incoming content
     * @param arguments the URL arguments in the process of being assembled
     * @param mimeParts the MIME parts in the process of being assembled
     * @throws IOException thrown if an I/O error occurred
     */
    @SuppressWarnings( { "UnusedAssignment", "UnnecessaryLabelOnBreakStatement" } )
    protected static void addFormDataArguments(
            BufferedInputStream    inStream,
            String                 mime,
            Map<String,String[]>   arguments,
            Map<String,MimePart[]> mimeParts )
        throws
            IOException
    {
        // determine boundary
        String  stringBoundary = FormDataUtils.determineBoundaryString( mime );
        byte [] byteBoundary   = FormDataUtils.constructByteBoundary( stringBoundary, StandardCharsets.UTF_8 );

        // forward to first boundary
        StreamUtils.slurpUntilBoundary( inStream, byteBoundary );
        boolean hasData = FormDataUtils.advanceToBeginningOfLine( inStream );

        // past first boundary now
        outer:
        while( hasData ) { // for all parts

            HashMap<String,String> partHeaders = new HashMap<>();
            String currentLogicalLine = null;
            while( true ) { // for all headers in this part
                String line = FormDataUtils.readStringLine( inStream, StandardCharsets.UTF_8 );
                if( line == null ) {
                    break outer; // end of stream -- we don't want heads and no content
                }
                if( line.startsWith( stringBoundary )) {
                    // not sure why it would do this here, but let's be safe
                    break;
                }
                if( line.length() == 0 ) {
                    break; // done with headers
                }
                if( Character.isWhitespace( line.charAt( 0 ) )) {
                    // continuation line
                    currentLogicalLine += line; // will throw if no currentLogicalLine, which is fine
                } else {
                    if( currentLogicalLine != null ) {
                        FormDataUtils.addNameValuePairTo( currentLogicalLine, partHeaders );
                    }
                    currentLogicalLine = line;
                }
            }
            if( currentLogicalLine != null ) {
                // don't forget the last line
                FormDataUtils.addNameValuePairTo( currentLogicalLine, partHeaders );
            }

            // have headers now, let's get the data
            byte [] partData = StreamUtils.slurpUntilBoundary( inStream, byteBoundary );
            if( partData == null || partData.length == 0 ) {
                hasData = false; // this is redundant, but here for clarity
                break outer; // end of stream -- we don't want heads and no content
            }
            partData = FormDataUtils.stripTrailingBoundary( partData, byteBoundary );
            hasData  = FormDataUtils.advanceToBeginningOfLine( inStream );

            String partMime = partHeaders.get( "content-type" );
            if( partMime == null ) {
                partMime = "text/plain"; // apparently the default
            }

            String partName        = null;
            String partDisposition = null;
            String disposition     = partHeaders.get( "content-disposition" );
            if( disposition != null ) {
                String [] dispositionData = disposition.split( ";" );
                partDisposition = dispositionData[0];

                for( int i=1 ; i<dispositionData.length ; ++i ) { // ignore first, that's different
                    String current = dispositionData[i];
                    int    equals  = current.indexOf( '=' );
                    if( equals > 0 ) {
                        String key   = current.substring( 0, equals ).trim().toLowerCase();
                        String value = current.substring( equals+1 ).trim();

                        if( value.startsWith( "\"" ) && value.endsWith( "\"" )) {
                            value = value.substring( 1, value.length()-1 );
                        }

                        if( "name".equals( key )) {
                            partName = value;
                        }
                    }
                }
            }

            if( partName != null ) {
                MimePart part = MimePart.create( partName, partHeaders, partDisposition, partData, partMime, StandardCharsets.UTF_8 );

                MimePart [] already = mimeParts.get( partName );
                MimePart [] toPut;
                if( already != null ) {
                    toPut = ArrayHelper.append( already, part, MimePart.class );
                } else {
                    toPut = new MimePart[] { part };
                }
                mimeParts.put( partName, toPut );
            } else {
                log.warn( "Skipping unnamed part" );
            }

            // adding to post parameters too
            if( partName != null && "form-data".equals( partDisposition ) && partMime.startsWith( "text/" )) {
                String    value   = new String( partData, StandardCharsets.UTF_8 );
                String [] already = arguments.get( partName );
                String [] toPut;

                if( already != null ) {
                    toPut = ArrayHelper.append( already, value, String.class );
                } else {
                    toPut = new String[] { value };
                }
                arguments.put( partName, toPut );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UndertowHttpRequest getHttpRequestAtProxy()
    {
        return (UndertowHttpRequest) super.getHttpRequestAtProxy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UndertowHttpRequest getOriginalHttpRequest()
    {
        return (UndertowHttpRequest) super.getOriginalHttpRequest();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getMultivaluedPostedArgument(
            String argName )
    {
        if( thePostedArguments == null ) {
            return null;
        }
        String [] ret = thePostedArguments.get( argName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String,String[]> getPostedArguments()
    {
        return Collections.unmodifiableMap( thePostedArguments );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IncomingHttpCookie[] getCookies()
    {
        return theCookies;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentType()
    {
        return theMimeType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPostData()
    {
        return thePostData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getClientIp()
    {
        return theClientIp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<String> requestedMimeTypesIterator()
    {
        if( theRequestedMimeTypes == null ) {
            // first split by comma, then by semicolon
            String header = getAcceptHeader();
            if( header != null ) {
                theRequestedMimeTypes = header.split( "," );

                Arrays.sort(theRequestedMimeTypes, ( String o1, String o2 ) -> {
                    final String qString = ";q=";

                    float priority1;
                    float priority2;

                    int semi1 = o1.indexOf( qString );
                    if( semi1 >= 0 ) {
                        priority1 = Float.parseFloat( o1.substring( semi1 + qString.length() ));
                    } else {
                        priority1 = 1.f;
                    }
                    int semi2 = o2.indexOf( qString );
                    if( semi2 >= 0 ) {
                        priority2 = Float.parseFloat( o2.substring( semi2 + qString.length() ));
                    } else {
                        priority2 = 1.f;
                    }

                    int ret;
                    if( priority1 > priority2 ) {
                        ret = 1;
                    } else if( priority1 == priority2 ) {
                        ret = 0;
                    } else {
                        ret = -1;
                    }
                    return ret;
                });

            } else {
                theRequestedMimeTypes = new String[0];
            }
        }
        return ArrayCursorIterator.<String>create( theRequestedMimeTypes );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAcceptHeader()
    {
        String ret = theDelegate.getRequestHeaders().getFirst( Headers.ACCEPT );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAuthorizationHeader()
    {
        String ret = theDelegate.getRequestHeaders().getFirst( Headers.AUTHORIZATION );
        return ret;
    }

    /**
     * Obtain the delegate request in the Undertow world.
     *
     * @return the delegate
     */
    public HttpServerExchange getDelegate()
    {
        return theDelegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(
            String name,
            Object value )
    {
        theAttributes.put( name, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getAttribute(
            String name )
    {
        Object ret = theAttributes.get( name );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAttribute(
            String name )
    {
        theAttributes.remove( name );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> getMimePartNames()
    {
        if( theMimeParts != null ) {
            return MapCursorIterator.createForKeys( theMimeParts );
        } else {
            return ZeroElementCursorIterator.create();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MimePart [] getMultivaluedMimeParts(
            String argName )
    {
        if( theMimeParts == null ) {
            return null;
        }
        MimePart [] ret = theMimeParts.get( argName );
        return ret;
    }

    /**
     * Obtain the name of the server aka virtual host.
     *
     * @return the name of the server
     */
    public String getServerName()
    {
        return theDelegate.getHostName();
    }

    /**
     * Obtain the Account used by the client, if any has been determined.
     *
     * @return the Account MeshObject
     */
    public MeshObject getClientAccount()
    {
        return (MeshObject) getAttribute( ACCOUNT_ATTRIBUTE );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return getUrl().getAbsoluteFullUri();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theDetermineRequestFromProxyOriginalRequest",
                    "theDelegate",
                    "theLocation",
                    "theMethod",
                    "thePostedArguments",
                    "theRequestedMimeTypes",
                    "theClientIp",
                    "theRequestAtProxy"
                },
                new Object[] {
                    theDetermineRequestFromProxyOriginalRequest,
                    theDelegate,
                    theUrl,
                    theMethod,
                    thePostedArguments,
                    theRequestedMimeTypes,
                    theClientIp,
                    theRequestAtProxy
                } );
    }

    /**
     * The underlying HttpServerExchange.
     */
    protected final HttpServerExchange theDelegate;

    /**
     * The incoming cookies.
     */
    protected final IncomingHttpCookie [] theCookies;

    /**
     * The data that was posted, if any.
     */
    protected final String thePostData;

    /**
     * The arguments to the Request that were POST'd, if any.
     */
    protected Map<String,String[]> thePostedArguments;

    /**
     * The MIME type, if any.
     */
    protected String theMimeType;

    /**
     * The requested MIME types, in sequence of prioritization. Allocated as needed.
     */
    protected String [] theRequestedMimeTypes;

    /**
     * The conveyed MimeParts, if any.
     */
    protected Map<String,MimePart[]> theMimeParts;

    /**
     * The IP address of the client.
     */
    protected String theClientIp;

    /**
     * Undertow does not seem to have attributes on a request, so we do it ourselves.
     */
    protected Map<String,Object> theAttributes = new HashMap<>();

    /**
     * Name of the form MIME type that JEE does know how to parse. :-(
     */
    public static final String FORM_DATA_MIME_URLENCODED = "application/x-www-form-urlencoded";

    /**
     * Name of the form MIME type that JEE does not know how to parse. :-(
     */
    public static final String FORM_DATA_MIME_MULTIPART = "multipart/form-data";

    /**
     * If true, determine the SaneServletRequest from non-standard HTTP headers set by a reverse proxy.
     */
    public static final boolean theDetermineRequestFromProxyOriginalRequest
            = ResourceHelper.getInstance( UndertowHttpRequest.class ).getResourceBooleanOrDefault(
                    "DetermineRequestFromProxyOriginalRequest",
                    false );

    /**
     * HTTP header name for the client IP address in a reverse proxy configuration.
     */
    public static final String HTTP_PROXY_HEADER_FORWARDED_FOR = "X-Forwarded-For";

    /**
     * HTTP header name for the host in a reverse proxy configuration.
     */
    public static final String HTTP_PROXY_HEADER_FORWARDED_HOST = "X-Forwarded-Host";

    /**
     * HTTP header name for the server in a reverse proxy configuration.
     */
    public static final String HTTP_PROXY_FORWARDED_SERVER = "X-Forwarded-Server";

    /**
     * HTTP header name for the protocol in a reverse proxy configuration.
     */
    public static final String HTTP_PROXY_FORWARDED_PROTOCOL = "X-Forwarded-Proto";

    /**
     * HTTP header name for the port in a reverse proxy configuration.
     */
    public static final String HTTP_PROXY_FORWARDED_PORT = "X-Forwarded-Port";

    /**
     * HTTP header name for the context path in a reverse proxy configuration.
     */
    public static final String HTTP_PROXY_FORWARDED_CONTEXT = "X-Forwarded-Context";

    /**
     * Name of a POSTed parameter that represents the submit button.
     */
    public static final String SUBMIT_PARAMETER_NAME = "submit";

    /**
     * Request attribute name whose value is the ClientAuthenticationStatus.
     */
    public static final String CLIENT_AUTH_STATUS_ATTRIBUTE = "CLIENT_AUTH_STATUS";

    /**
     * Request attribute name whose value is the client MeshObject (the Principal).
     */
    public static final String CLIENT_ATTRIBUTE = "CLIENT";

    /**
     * Request attribute name whose value is the client identifier (the Principal's identifier).
     */
    public static final String CLIENT_ID_ATTRIBUTE = "CLIENT_ID";

    /**
     * Request attribute name whose value is the account MeshObject.
     */
    public static final String ACCOUNT_ATTRIBUTE = "ACCOUNT";

    /**
     * Request attribute name whose value is the account identifier.
     */
    public static final String ACCOUNT_ID_ATTRIBUTE = "ACCOUNT_ID";
}
