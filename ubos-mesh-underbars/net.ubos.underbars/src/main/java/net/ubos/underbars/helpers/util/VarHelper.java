//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Sets a local variable.
 */
public class VarHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String name  = options.hash( NAME_ARGNAME );
        Object value = options.hash( VALUE_ARGNAME );

        Map<String,Object> extraValues = new HashMap<>(); // cannot use Map.of() -- requires non-null values
        extraValues.put( name, value );

        ViewletModel subModel     = model.createSubmodel( extraValues );
        Context      childContext = Context.newContext( options.context, subModel );

        Options.Buffer buf = options.buffer();

        buf.append( options.apply( options.fn, childContext ));

        return buf;
    }

    /**
     * Name of the argument on this Helper that indicates the name of the variable
     * that should be set.
     */
    public static final String NAME_ARGNAME = "name";

    /**
     * Name of the argument on this Helper that indicates the value of the variable
     * that should be set.
     */
    public static final String VALUE_ARGNAME = "value";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        NAME_ARGNAME,
        VALUE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
