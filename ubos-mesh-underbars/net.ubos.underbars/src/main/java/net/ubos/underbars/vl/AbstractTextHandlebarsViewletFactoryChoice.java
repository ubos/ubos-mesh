//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.underbars.resources.TemplateMap;
import net.ubos.util.ResourceHelper;
import net.ubos.vl.CannotViewException;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.AbstractWebViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Factored out for easier subclassing
 */
public abstract class AbstractTextHandlebarsViewletFactoryChoice
        extends
            AbstractWebViewletFactoryChoice
{
    /**
     * Constructor. This is private, so to subclass, use AbstractWebViewletFactoryChoice.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param supportedMimeType the supported MIME type
     * @param templateMap the named Templates used for the Viewlet
     * @param resourceHelper the ResourceHelper to use
     * @param viewletType type of the Viewlet
     * @param matchQuality the quality of the match
     */
    protected AbstractTextHandlebarsViewletFactoryChoice(
            String                viewletName,
            String                supportedMimeType,
            TemplateMap           templateMap,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceHelper        resourceHelper )
    {
        super( viewletName, supportedMimeType, placement, dimensionality, detail, matchQuality, resourceHelper );

        theTemplateMap = templateMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HandlebarsViewlet instantiateViewlet()
        throws
            CannotViewException
    {
        return DefaultTextHandlebarsViewlet.create( theName, theSupportedMimeType, theTemplateMap, thePlacement, theDimensionality, theDetail );
    }

    /**
     * The templates for the Viewlet.
     */
    protected final TemplateMap theTemplateMap;
}
