//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars;

import com.github.jknack.handlebars.Handlebars;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.server.handlers.form.FormParserFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.bot.BotRunner;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.BracketMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.UrlMeshObjectIdentifierBothSerializer;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.model.primitives.externalized.DelegatingMeshObjectIdentifierBothSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.model.traverse.ByRoleAttributeTraverseTranslator;
import net.ubos.model.traverse.RoleTypeTraverseTranslator;
import net.ubos.model.traverse.SpaceMakesSequentialTraverseTranslator;
import net.ubos.model.util.SimpleSubjectAreaTagMap;
import net.ubos.model.util.SubjectAreaTagMap;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.underbars.resources.FragmentTemplateMap;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.skin.HandlebarsSkinManager;
import net.ubos.underbars.text.DefaultUnderbarsStringRepresentationSchemeDirectory;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ResourceHelper;
import net.ubos.util.logging.Log;
import net.ubos.util.nameserver.MNameServer;
import net.ubos.util.nameserver.WritableNameServer;
import net.ubos.vl.DefaultViewletFactory;
import net.ubos.vl.ViewletFactory;
import net.ubos.vl.ViewletMatcher;
import net.ubos.web.HttpRequest;
import net.ubos.web.assets.DefaultAssetMap;
import net.ubos.web.httpshell.HttpShell;
import net.ubos.web.httpshell.HttpShellHandler;
import net.ubos.web.skin.Skin;
import net.ubos.web.skin.SkinType;
import net.ubos.web.vl.DefaultWebMeshObjectsToViewFactory;
import net.ubos.web.vl.DefaultWebViewletProcessor;
import net.ubos.web.vl.WebMeshObjectsToViewFactory;
import net.ubos.web.vl.WebViewletProcessor;
import net.ubos.web.assets.AssetMap;
import net.ubos.web.skin.SkinManager;
import net.ubos.web.skin.SkinMap;
import net.ubos.model.traverse.TraverseTranslator;

/**
 * Central registry for the Underbars framework.
 */
public abstract class Underbars
    extends
        Daemon
{
    private static final Log log = Log.getLogInstance( Underbars.class ); // our own, private logger

    /**
     * Keep this abstract.
     */
    private Underbars() {}

    /**
     * Obtain the app's context path (without host)
     *
     * @return the context path
     */
    public static String getContextPath()
    {
        return theContextPath;
    }

    /**
     * Set the app's context path (without host)
     *
     * @param newPath the context path
     */
    static void setContextPath(
            String newPath )
    {
        theContextPath = newPath;
    }

    /**
     * Register a Http Shell Handler.
     *
     * @param name the name of the new Http Shell Handler
     * @param handler the new Http Shell Handler.
     */
    public void registerHttpShellHandler(
            String           name,
            HttpShellHandler handler )
    {
        theHandlerDirectory.put( name, handler );
    }

    /**
     * Obtain the top Undertow HttpHandler. The result of this method is handed
     * over to Undertow as the entry point to all of our HttpHandlers.
     *
     * @return the HttpHandler
     */
    static HttpHandler getTopHttpHandler()
    {
        return theTopHttpHandler;
    }

    /**
     * Set an optional security HttpHandler.
     *
     * @param handler the HttpHandler
     */
    public static void setSecurityHttpHandler(
            HttpHandler handler )
    {
        theSecurityHandler = handler;
    }

    /**
     * Obtain the HttpHandler that the security module is supposed to call as "next"
     *
     * @return the HttpHandler
     */
    public static HttpHandler getSecurityNextHttpHandler()
    {
        return theMainHttpHandler;
    }

    /**
     * Obtain the ViewletFactory in use.
     *
     * @return the ViewletFactory
     */
    public static ViewletFactory getViewletFactory()
    {
        return theViewletFactory;
    }

    /**
     * Convenience method to register a new Viewlet, to match the other registration methods.
     *
     * @param matcher the Viewlet's ViewletMatcher
     */
    public static void registerViewlet(
            ViewletMatcher matcher )
    {
        theViewletFactory.registerViewlet( matcher );
    }

    /**
     * Register an AssetMap as a delegate to the main AssetMap.
     *
     * @param map the AssetMap
     */
    public static void registerAssets(
            AssetMap map )
    {
        theWebAssetManager.addDelegate( map );
    }

    /**
     * Obtain the main AssetMap.
     *
     * @return the main AssetMap
     */
    public static AssetMap getAssetMap()
    {
        return theWebAssetManager;
    }

    /**
     * If an HttpHandler is registered for this relative path, return it.
     *
     * @param relativePath the relative path
     * @return the found HttpHandler or null
     */
    static HttpHandler getPathHandlerFor(
            String relativePath )
    {
        return thePathHandlers.get( relativePath );
    }

    /**
     * If a path prefix handler is registered for this relative path, return it.
     *
     * @param pathPrefix the path prefix
     * @return the found HttpHandler or null
     */
    static HttpHandler getPathPrefixHandlerFor(
            String pathPrefix )
    {
        return thePathPrefixHandlers.get( pathPrefix );
    }

    /**
     * Obtain the configured WebMeshObjectsToViewFactory.
     *
     * @return the configured WebMeshObjectsToViewFactory
     */
    public static WebMeshObjectsToViewFactory getMeshObjectsToViewFactory()
    {
        return theToViewFactory;
    }

    /**
     * Set the TraverseTranslator to use
     *
     * @param translator the TraverseTranslator
     */
    public static void setTraverseTranslator(
            TraverseTranslator translator )
    {
        theTraverseTranslator = translator;
    }

    /**
     * Determine the configured TraverseTranslator.
     *
     * @return the TraverseTranslator
     */
    public static TraverseTranslator getTraverseTranslator()
    {
        return theTraverseTranslator;
    }

    /**
     * Set the name of the Skin to be used if no other Skin is specified in a request.
     *
     * @param defaultSkinName the name of the skin
     */
    public static void setDefaultSkinName(
            String defaultSkinName )
    {
        theDefaultSkinName = defaultSkinName;
    }

    /**
     * Get the name of the Skin to be used if no other Skin is specified in a request.
     *
     * @return the name of the skin
     */
    public static String getDefaultSkinName()
    {
        return theDefaultSkinName;
    }

    /**
     * Determine which ViewletProcessor included Viewlets should use.
     *
     * @return the WebViewletProcessor
     */
    public static WebViewletProcessor getSubViewletProcessor()
    {
        return theSubViewletProcessor;
    }

    /**
     * Obtain the top Viewlet Model.
     *
     * @return the ViewletModel
     */
    public static ViewletModel getTopViewletModel()
    {
        return theTopViewletModel;
    }

    /**
     * Register the Skins from this SkinManager. We can't register it as a delegate, because
     * then finding the right Skin with the right fallback would be really convoluted.
     *
     * @param delegate the SkinMap
     */
    public static void registerSkinsFromManager(
            SkinManager delegate )
    {
        theSkinManager.addSkinsFrom( delegate );
    }

    /**
     * Find a Skin with one of the provided names.
     *
     * @param names the candidate names for Skins. Each element may be null
     * @return the found Skin, or a fallback Skin
     */
    static Skin findSkin(
            SkinType  type,
            String    name1,
            String    name2,
            String    name3 )
    {
        SkinMap foundMap = theSkinManager.get( type );
        if( foundMap == null ) {
            foundMap = theSkinManager.getFallbackSkinMap();
        }

        Skin found = null;
        if( name1 != null ) {
            found = foundMap.get( name1 );
        }
        if( found == null && name2 != null ) {
            found = foundMap.get( name2 );
        }
        if( found == null && name3 != null ) {
            found = foundMap.get( name3 );
        }
        if( found == null ) {
            found = type.getFallbackSkin();
        }
        return found;

//        SkinMap foundMap = theSkinManager.getLocal( type );
//        Skin    found;
//
//        if( foundMap == null ) {
//            for( NameServer<SkinType,? extends SkinMap> delegate : theSkinManager.delegateIterator() ) {
//                foundMap = delegate.get( type );
//                if( foundMap == null ) {
//                    continue;
//                }
//                if( name1 != null ) {
//                    found = foundMap.get( name1 );
//                    if( found != null ) {
//                        return found;
//                    }
//                }
//                if( name2 != null ) {
//                    found = foundMap.get( name2 );
//                    if( found != null ) {
//                        return found;
//                    }
//                }
//            }
//        }
//
//        if( foundMap == null ) {
//            foundMap = theSkinManager.getFallbackSkinMap();
//        }
//        if( name1 != null ) {
//            found = foundMap.get( name1 );
//            if( found != null ) {
//                return found;
//            }
//        }
//        if( name2 != null ) {
//            found = foundMap.get( name2 );
//            if( found != null ) {
//                return found;
//            }
//        }
//
//        found = foundMap.getFallbackSkin();
//        if( found != null ) {
//            return found;
//        }
//
//        found = type.getFallbackSkin();
//        return found;
    }

    /**
     * Register an entire TemplateMap as a delegate to the primary TemplateMap.
     *
     * @param templates the HandlebarsTemplateManager
     */
    public static void registerFragments(
            TemplateMap templates )
    {
        theFragmentManager.addDelegate( templates );
    }

    /**
     * Obtain the fragment TemplateMap for this package.
     *
     * @return the TemplateMap
     */
    public static FragmentTemplateMap getFragmentTemplateManager()
    {
        return theFragmentManager;
    }

    /**
     * Factored-out helper to invoke the HttpShell.
     *
     * @param request the incoming request
     * @return null, or, if given, a URL to redirect to without further processing
     * @throws IOException an I/O problem occurred
     */
    static String runHttpShell(
            HttpRequest request )
        throws
            IOException
    {
        if( theShell != null ) {
            return theShell.performOperationsIfNeeded( request );
        } else {
            return null;
        }
    }

    /**
     * Obtain the Handlebars instance to use.
     *
     * @return Handlebars
     */
    public static Handlebars getHandlebars()
    {
        return theHandlebars;
    }

    /**
     * Register a delegate HttpHandler for a particular relative path.
     *
     * @param path the path
     * @param delegate the delegate HttpHandler
     * @see #unregisterPathDelegate(java.lang.String)
     */
    public static void registerPathDelegate(
            String      path,
            HttpHandler delegate )
    {
        int    slash  = path.indexOf( '/' );
        String firstStep;
        if( slash >= 0 ) {
            firstStep = path.substring( 0, slash );
        } else {
            firstStep = path;
        }

        if( firstStep.length() > PATH_PREFIX_MAX_LENGTH ) {
            throw new IllegalArgumentException( "First element in path longer than permitted max " + PATH_PREFIX_MAX_LENGTH + ": " + path );
        }

        if( thePathHandlers.containsKey( path )) {
            throw new IllegalArgumentException( "Path already registered: " + path );
        }

        thePathHandlers.put( path, delegate );
    }

    /**
     * Unregister a delegate HttpHandler for a particular relative path.
     *
     * @param path the path
     * @see #registerPathDelegate(java.lang.String, io.undertow.server.HttpHandler)
     */
    public static void unregisterPathDelegate(
            String path )
    {
        if( !thePathHandlers.containsKey( path )) {
            throw new IllegalArgumentException( "Path not registered: " + path );
        }

        thePathHandlers.remove( path );
    }

    /**
     * Register a delegate HttpHandler for a particular path prefix.
     *
     * @param prefix the path prefix
     * @param delegate the delegate HttpHandler
     * @see #unregisterPathPrefixDelegate(java.lang.String)
     */
    public static void registerPathPrefixDelegate(
            String      prefix,
            HttpHandler delegate )
    {
        if( prefix.length() > PATH_PREFIX_MAX_LENGTH ) {
            throw new IllegalArgumentException( "Path prefix longer than permitted max " + PATH_PREFIX_MAX_LENGTH + ": " + prefix );
        }

        if( thePathPrefixHandlers.containsKey( prefix )) {
            throw new IllegalArgumentException( "Path prefix already registered: " + prefix );
        }

        thePathPrefixHandlers.put( prefix, delegate );
    }

    /**
     * Unregister a delegate HttpHandler for a particular path prefix.
     *
     * @param prefix the path prefix
     * @see #registerPathPrefixDelegate(java.lang.String, io.undertow.server.HttpHandler)
     */
    public static void unregisterPathPrefixDelegate(
            String prefix )
    {
        if( !thePathPrefixHandlers.containsKey( prefix )) {
            throw new IllegalArgumentException( "Path prefix not registered: " + prefix );
        }

        thePathPrefixHandlers.remove( prefix );
    }

    /**
     * Set the Store that has user credentials.
     *
     * @param credentialsStore the Store
     */
    public static void setUserCredentialsStore(
            Store<StoreValue> credentialsStore )
    {
        theCredentialsStore = credentialsStore;
    }

    /**
     * Obtain the Store with the user credentials.
     *
     * @return the Store
     */
    public static Store<StoreValue> getUserCredentialsStore()
    {
        return theCredentialsStore;
    }

    public static void start()
    {
        MeshBase mb = getMeshBaseNameServer().getDefaultMeshBase();

        BracketMeshObjectIdentifierBothSerializer normalIdSerializer = BracketMeshObjectIdentifierBothSerializer.create(
                mb.getPrimaryNamespaceMap(), mb );

        MeshObjectIdentifierBothSerializer urlIdSerializer  = UrlMeshObjectIdentifierBothSerializer.create( theContextPath, normalIdSerializer );
        MeshTypeIdentifierBothSerializer   typeIdSerializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

        MeshObjectIdentifierBothSerializer shellIdSerializer  = ExternalNameHashMeshObjectIdentifierBothSerializer.create(
                mb.getPrimaryNamespaceMap(), mb );

        DelegatingMeshObjectIdentifierBothSerializer tryEverythingSerializer
                = DelegatingMeshObjectIdentifierBothSerializer.create( normalIdSerializer, urlIdSerializer );

        theStringRepresentationSchemeDirectory
                = DefaultUnderbarsStringRepresentationSchemeDirectory.create(
                        theFragmentManager,
                        normalIdSerializer,
                        urlIdSerializer,
                        typeIdSerializer );

        theShell = HttpShell.create(
                theHandlerDirectory,
                theStringRepresentationSchemeDirectory.findScheme( DefaultUnderbarsStringRepresentationSchemeDirectory.HTTP_SHELL_ENTRY ),
                tryEverythingSerializer,
                urlIdSerializer,
                mb );


// Viewlets

        theToViewFactory = DefaultWebMeshObjectsToViewFactory.create( urlIdSerializer, Underbars.getMeshBaseNameServer() );

        theTopViewletModel = ViewletModel.create(
                theTagMap,
                theSubViewletProcessor,
                theToViewFactory,
                theStringRepresentationSchemeDirectory,
                shellIdSerializer,
                typeIdSerializer );

        // Start serving content
        theMainHttpHandler = new BlockingHandler( new DispatchingHandler() );

        // Start the bot runner
        theBotRunner = new BotRunner();
        mb.addSoftTransactionListener( theBotRunner );

        theBotRunner.start();
    }

    protected static SubjectAreaTagMap theTagMap = SimpleSubjectAreaTagMap.create();

    /**
     * The parent / fallback of all ViewletModels used.
     */
    protected static ViewletModel theTopViewletModel;

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper
            = ResourceHelper.getInstance( DispatchingHandler.class );

    /**
     * The maximum number of characters allowed for path-prefix matching.
     */
    private static final int PATH_PREFIX_MAX_LENGTH
            = theResourceHelper.getResourceIntegerOrDefault( "PathPrefixMaxLength", 3 );

    /**
     * The known path-prefix HttpHandlers
     */
    protected static final Map<String,HttpHandler> thePathPrefixHandlers = new HashMap<>();

    /**
     * The known path HttpHandlers
     */
    protected static final Map<String,HttpHandler> thePathHandlers = new HashMap<>();

    /**
     * Manages the known skins.
     */
    protected static final HandlebarsSkinManager theSkinManager = HandlebarsSkinManager.create();

    /**
     * Manages the known fragment templates.
     */
    protected static final FragmentTemplateMap theFragmentManager = FragmentTemplateMap.create();

    /**
     * Knows how to translate TranversalSpecifications from text to String form.
     */
    protected static TraverseTranslator theTraverseTranslator
            = SpaceMakesSequentialTraverseTranslator.create(
                    RoleTypeTraverseTranslator.create(
                            ByRoleAttributeTraverseTranslator.create() ));

    /**
     * Factory for MeshObjectsToView objects
     */
    protected static WebMeshObjectsToViewFactory theToViewFactory;

    /**
     * The HttpShell.
     */
    protected static HttpShell theShell;

    /**
     * The Handlebars instance to use.
     */
    protected static final Handlebars theHandlebars = new Handlebars();
    static {
        theHandlebars.setPrettyPrint( true ); // allows white space removal
    }

    /**
     * The directory of Http Shell handlers.
     */
    protected static WritableNameServer<String,HttpShellHandler> theHandlerDirectory
            = MNameServer.create();

    /**
     * The ViewletFactory to use.
     */
    protected static final ViewletFactory theViewletFactory = DefaultViewletFactory.create();

    /**
     * The currently known web assets.
     */
    protected static final DefaultAssetMap theWebAssetManager = DefaultAssetMap.create();

    /**
     * Knows how to process included Viewlets.
     */
    protected static final WebViewletProcessor theSubViewletProcessor = new DefaultWebViewletProcessor();

    /**
     * The initial main HttpHandler. Before it is set to something else, it will return 404 only.
     */
    protected static HttpHandler theMainHttpHandler
            = (HttpServerExchange hse) -> hse.setStatusCode( 404 );

    /**
     * The handler that delegates to security, if one has been set
     */
    protected static HttpHandler theSecurityHandler = null;

    /**
     * The top HttpHandler. Before it is set, it will return 404 only.
     */
    protected static final HttpHandler theTopHttpHandler = new ErrorHandler( new TopHttpHandler() );

    /**
     * The app's context path.
     */
    protected static String theContextPath;

    /**
     * Where user credentials are stored.
     */
    protected static Store<StoreValue> theCredentialsStore;

    /**
     * The BotRunner, if any.
     */
    protected static BotRunner theBotRunner;

    /**
     * Name of the Skin to be used if no other Skin is specified.
     */
    protected static String theDefaultSkinName;

    public static final String ACCOUNT_SESSION_ATTRIBUTE_NAME = "account";
    public static final String PRINCIPAL_SESSION_ATTRIBUTE_NAME = "principal";
    public static final String WHO_SESSION_ATTRIBUTE_NAME = "who";


    /**
     * The Top-level HTTP handler.
     */
    static class TopHttpHandler
        implements
            HttpHandler
    {
        /**
         * {@inheritDoc}
         */
        @Override
        public void handleRequest(
                HttpServerExchange hse )
            throws
                Exception
        {
            if( theSecurityHandler == null ) {
                theMainHttpHandler.handleRequest( hse );
            } else {
                theSecurityHandler.handleRequest( hse );
            }
        }
    }

    /**
     * Knows how to parse forms.
     */
    public static final FormParserFactory theFormParseFactory = FormParserFactory.builder().build();
}
