//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.skin;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.TemplateSource;
import net.ubos.underbars.resolvers.HandlebarsModelResolver;
import net.ubos.underbars.resolvers.StructuredResponseValueResolver;
import net.ubos.web.skin.Skin;
import net.ubos.web.skin.SkinProcessingException;
import net.ubos.web.vl.StructuredResponse;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Map.Entry;
import net.ubos.web.skin.SkinType;

/**
 * A Skin implemented as a Handlebars template. We keep the TemplateSource around, rather than
 * the Template, because Handlebars does the caching already, and it can be deactivated during
 * development for interactive feedback
 */
public class HandlebarsSkin
    implements
        Skin
{
    /**
     * Factory method.
     *
     * @param templateSource the TemplateSource
     * @param type the type of Skin this is
     * @param partialTemplateSources the files containing the templates for partials
     * @param hb the Handlebars object to use to compile
     * @return the created HandlebarsSkin instance
     */
    public static HandlebarsSkin create(
            TemplateSource             templateSource,
            SkinType                   type,
            Map<String,TemplateSource> partialTemplateSources,
            Handlebars                 hb )
    {
        return new HandlebarsSkin( templateSource, type, "text/html;charset=utf-8", partialTemplateSources, hb );
    }

    /**
     * Factory method.
     *
     * @param templateSource the TemplateSource
     * @param type the type of Skin this is
     * @param contentType the emitted MIME type
     * @param partialTemplateSources the files containing the templates for partials
     * @param hb the Handlebars instance
     * @return the created HandlebarsSkin instance
     * @throws IOException thrown if the template cannot be compiled or found
     */
    public static HandlebarsSkin create(
            TemplateSource             templateSource,
            SkinType                   type,
            String                     contentType,
            Map<String,TemplateSource> partialTemplateSources,
            Handlebars                 hb )
        throws
            IOException
    {
        Template t = hb.compile( templateSource ); // precompile at boot

        return new HandlebarsSkin( templateSource, type, contentType, partialTemplateSources, hb );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param templateSource the file containing the Handlebars template
     * @param type the type of Skin this is
     * @param contentType the emitted MIME type
     * @param partialTemplateSources the files containing the templates for partials
     * @param hb the Handlebars instance
     */
    protected HandlebarsSkin(
            TemplateSource             templateSource,
            SkinType                   type,
            String                     contentType,
            Map<String,TemplateSource> partialTemplateSources,
            Handlebars                 hb )
    {
        theTemplateSource = templateSource;
        theType           = type;
        theContentType    = contentType;
        thePartialsLoader = new HandlebarsPartialsLoader( partialTemplateSources, hb );
        theHandlebars     = hb;
    }

    /**
     * Obtain the partials loader.
     *
     * @return the partials loader
     */
    public HandlebarsPartialsLoader getPartialLoader()
    {
        return thePartialsLoader;
    }

    /**
     * Test compile the skin.
     *
     * @throws IOException an I/O problem occurred
     */
    public void testCompile()
        throws
            IOException
    {
        Template t = compile();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SkinType getType()
    {
        return theType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int format(
            StructuredResponse response,
            OutputStream       rawOutput )
        throws
            SkinProcessingException
    {
        try {
            Template t = compile(); // compile if needed

            Context c = Context.newBuilder( SkinModel.create( response ))
                    .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON )
                    .build();

            String output = t.apply( c );
            rawOutput.write( output.getBytes() );

            return response.getHttpStatus();

        } catch( IOException ex ) {
            throw new SkinProcessingException( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentType()
    {
        return theContentType;
    }

    /**
     * Factored-out single place where we compile the template.
     *
     * @return the Template
     * @throws IOException an I/O problem occurred
     */
    protected Template compile()
        throws
            IOException
    {
        for( Entry<String,TemplateSource> partialsEntry : thePartialsLoader.entrySet() ) {
            Template t = theHandlebars.compile( partialsEntry.getValue() );
        }

        Template ret = theHandlebars.with( thePartialsLoader ).compile( theTemplateSource );

        return ret;
    }

    /**
     * The type of Skin this is.
     */
    protected final SkinType theType;

    /**
     * The Handlebars Template source to use
     */
    protected final TemplateSource theTemplateSource;

    /**
     * Knows how to lood partials.
     */
    protected final HandlebarsPartialsLoader thePartialsLoader;

    /**
     * The emitted MIME type.
     */
    protected final String theContentType;

    /**
     * The Handlebars object to compile.
     */
    protected final Handlebars theHandlebars;
}
