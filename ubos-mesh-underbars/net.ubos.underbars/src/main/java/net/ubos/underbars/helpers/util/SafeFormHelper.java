//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.Map;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Generates an HTML form with an extra CSRF token in it.
 */
public class SafeFormHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        // do not checkArguments(): we pass through all of our arguments

        Options.Buffer buffer = options.buffer();
        
        buffer.append( "<form" );

        // pass all named arguments straight through
        for( Map.Entry<String,Object> arg : options.hash.entrySet() ) {
            String key   = arg.getKey();
            Object value = arg.getValue();
                    
            if( value instanceof CharSequence ) {
                buffer.append( " " );
                buffer.append( key );
                buffer.append( "=\"" );
                buffer.append( value.toString() );
                buffer.append( "\"" );
            }
        }
        
        buffer.append( ">\n" );

        if( "POST".equals( options.hash( METHOD_ARGNAME ))) {
            String csrfToken = model.getCsrfToken();
            buffer.append( " <input type=\"hidden\" name=\"" )
                  .append( model.getCsrfFormInputName() )
                  .append( "\" value=\"" )
                  .append( csrfToken )
                  .append( "\"/>\n" );
        }

        CharSequence content = options.fn();
        buffer.append( content );

        buffer.append( "</form>\n" );
        return buffer;
    }
    
    /**
     * Name of the argument indicating the HTTP method to use.
     */
    public static final String METHOD_ARGNAME = "method";
}
