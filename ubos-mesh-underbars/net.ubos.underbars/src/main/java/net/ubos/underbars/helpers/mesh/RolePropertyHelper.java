//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.PropertyValueParsingException;
import net.ubos.model.util.SubjectAreaTagMap;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Insert the value of a RoleProperty.
 */
public class RolePropertyHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj      = getPrimaryMeshObject( model, options );
        MeshObject neighbor = getNeighborMeshObject( model, options );

        Object propertyTypeModel = options.hash( ROLE_PROPERTY_TYPE_ARGNAME );
        Object defaultValueModel = options.hash( DEFAULT_VALUE_ARGNAME );

        PropertyType  propertyType = null;

        try {
            if( propertyTypeModel instanceof PropertyType ) {
                propertyType = (PropertyType) propertyTypeModel;

            } else if( propertyTypeModel instanceof MeshTypeIdentifier ) {
                propertyType = findPropertyTypeOrThrow( (MeshTypeIdentifier) propertyTypeModel );

            } else if( propertyTypeModel instanceof String ) {
                propertyType = findRolePropertyTypeOrThrow(
                        obj,
                        neighbor,
                        (String) propertyTypeModel,
                        (SubjectAreaTagMap) model.get( ViewletModel.SUBJECT_AREA_TAG_MAP_KEY ) );

            } else {
                throw new IllegalArgumentException( "Unexpected type " + propertyTypeModel.getClass().getName() + ": " + propertyTypeModel );
            }
            if( propertyType == null ) {
                throw new IllegalArgumentException( "Cannot find PropertyType: " + propertyTypeModel );
            }

            PropertyValue value = obj.getRolePropertyValue( neighbor, propertyType );

            String text = formatRoleProperty(
                    obj,
                    neighbor,
                    propertyType,
                    value,
                    defaultValueModel,
                    model,
                    options );

            return text;

        } catch( ParseException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( IllegalPropertyTypeException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( PropertyValueParsingException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( NotPermittedException ex ) {
            throw new IllegalArgumentException( ex );
        }
    }

    /**
     * Name of the parameter indicating the PropertyType.
     */
    public static final String ROLE_PROPERTY_TYPE_ARGNAME = "rolePropertyType";

    /**
     * Name of the parameter indicating the default value to be used.
     */
    public static final String DEFAULT_VALUE_ARGNAME = "defaultValue";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        NEIGHBOR_ARGNAME,
        ROLE_PROPERTY_TYPE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DEFAULT_VALUE_ARGNAME,
        FLAVOR_ARGNAME,
        MAX_LENGTH_ARGNAME,
        STRING_REPRESENTATION_ARGNAME
    };
}
