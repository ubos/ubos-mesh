//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.EntityType;
import static net.ubos.underbars.helpers.AbstractHandlebarsHelper.MAX_ARGNAME;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ArrayHelper;

/**
 * Iterate over a MeshObject's neighbors.
 */
public class NeighborsHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );
        int        max = optionalHash( MAX_ARGNAME, -1, options );

        MeshObject [] neighbors = obj.traverseToNeighbors().getMeshObjects();
        if( max > 0 && neighbors.length > max ) {
            neighbors = ArrayHelper.copyIntoNewArray( neighbors, 0, max, MeshObject.class );
        }
        return neighbors;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MAX_ARGNAME
    };
}
