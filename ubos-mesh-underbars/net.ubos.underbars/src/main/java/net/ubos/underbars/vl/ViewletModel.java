//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.text.StringRepresentationSchemeDirectory;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.model.util.SubjectAreaTagMap;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.WebMeshObjectsToViewFactory;
import net.ubos.web.vl.WebViewletProcessor;

/**
 * The object being passed to a Handlebars resources implementing a Viewlet.
 */
public abstract class ViewletModel
    extends
        HandlebarsModel
{
    /**
     * Factory method for the shared top singleton instance of this class.
     *
     * @param tagMap the SubjectAreaTagMap to use
     * @param viewletProcessor the ViewletProcessor to be used by included Viewlets
     * @param toViewFactory factory for new MeshobjectsToView objects
     * @param stringRepDir the StringRepresentationSchemeDirectory to use
     * @param shellIdSerializer the MeshObjectIdentifierSerializer to use for the purposes of the HttpShell
     * @param shellTypeIdSerializer the MeshTypeIdentifierSerializer to use for the purposes of the HttpShell
     * @return the created SkinContext instance
     */
    public static ViewletModel create(
            SubjectAreaTagMap                   tagMap,
            WebViewletProcessor                 viewletProcessor,
            WebMeshObjectsToViewFactory         toViewFactory,
            StringRepresentationSchemeDirectory stringRepDir,
            MeshObjectIdentifierSerializer      shellIdSerializer,
            MeshTypeIdentifierSerializer        shellTypeIdSerializer )
    {
        Map<String,Object> data = new HashMap<>();

        data.put( MODELBASE_KEY,  ModelBase.SINGLETON );

        data.put( SUBJECT_AREA_TAG_MAP_KEY, tagMap );
        data.put( STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY, stringRepDir );

        data.put( VIEWLET_PROCESSOR_KEY,                viewletProcessor );
        data.put( WEB_MESH_OBJECTS_TO_VIEW_FACTORY_KEY, toViewFactory );

        data.put( SHELL_MESH_OBJECT_IDENTIFIER_SERIALIZER_KEY, shellIdSerializer );
        data.put( SHELL_MESH_TYPE_IDENTIFIER_SERIALIZER_KEY,   shellTypeIdSerializer );

        ViewletModel.TopSingleton ret = new ViewletModel.TopSingleton( data );

        return ret;
    }

    /**
     * Private constructor, use factory method.
     *
     * @param data the data in the ViewletModel
     * @param delegate optional delegate to consult if a local value does not exist
     */
    protected ViewletModel(
            Map<String,Object> data,
            HandlebarsModel    delegate )
    {
        super( data, delegate );
    }

    /**
     * Obtain a new, unique id for DOM nodes.
     *
     * @return a new id
     */
    public abstract String obtainNewDomId();

    /**
     * Obtain a new, unique Http Shell variable name.
     *
     * @return a new variable name
     */
    public abstract String obtainNewShellVar();

    /**
     * Obtain a new, unique Http Shell sub-variable name for this Http Shell variable.
     *
     * @param shellVar the shell variable
     * @return a new sub-variable name
     */
    public abstract String obtainNewShellSubVarFor(
            String shellVar );

    /**
     * Set the current overlay id.
     *
     * @param id the id
     */
    public abstract void setCurrentOverlayId(
            String id );

    /**
     * Unset the current overlay id.
     */
    public abstract void unsetCurrentOverlayId();

    /**
     * Obtain the current overlay id.
     *
     * @return the current overlay id
     */
    public abstract String getCurrentOverlayId();

    /**
     * Obtain the currently valid CSRF token for forms.
     *
     * @return the token
     */
    public String getCsrfToken()
    {
         StructuredResponse response = (StructuredResponse) get( STRUCTURED_RESPONSE_KEY );
         return response.getCsrfToken();
    }

    /**
     * Obtain the name of the input field in a form that contains the CRSF token.
     *
     * @return the field name
     */
    public String getCsrfFormInputName()
    {
         StructuredResponse response = (StructuredResponse) get( STRUCTURED_RESPONSE_KEY );
         return response.getCsrfFormInputName();
    }

    /**
     * Create a sub-model that can have local data which overrides what is specified in the delegate.
     *
     * @param data local data for the sub-model
     * @return the created sub-model
     */
    public abstract ViewletModel createSubmodel(
            Map<String,Object> data );

    /**
     * Create a sub-model that does not have local data.
     *
     * @return the created sub-model
     */
    public abstract ViewletModel createSubmodel();

    /**
     * Obtain the WebViewletProcessor to use for included Viewlets.
     *
     * @return the WebViewletProcessor
     */
    public WebViewletProcessor getViewletProcessor()
    {
        return (WebViewletProcessor) get( VIEWLET_PROCESSOR_KEY );
    }

    /**
     * Obtain the MeshObjectIdentifierSerializer to use when emitting MeshObjectIdentifiers
     * for the purposes of the HttpShell.
     *
     * @return the MeshObjectIdentifierSerializer
     */
    public MeshObjectIdentifierSerializer getShellMeshObjectIdentifierSerializer()
    {
        return (MeshObjectIdentifierSerializer) get( SHELL_MESH_OBJECT_IDENTIFIER_SERIALIZER_KEY );
    }

    /**
     * Obtain the MeshObjectIdentifierSerializer to use when emitting MeshTypeIdentifiers
     * for the purposes of the HttpShell.
     *
     * @return the MeshObjectIdentifierSerializer
     */
    public MeshTypeIdentifierSerializer getShellMeshTypeIdentifierSerializer()
    {
        return (MeshTypeIdentifierSerializer) get( SHELL_MESH_TYPE_IDENTIFIER_SERIALIZER_KEY );
    }

    /**
     * Key for the parsed MeshObjectsToView object for the Viewlet that is currently being processed.
     */
    public static final String TOVIEW_KEY = "toview";

    /**
     * Key for the Viewlet that is currently being processed.
     */
    public static final String VIEWLET_KEY = "vl";

    /**
     * Key for the Subject of the current Viewlet.
     */
    public static final String SUBJECT_KEY = "subject";

    /**
     * Key for the SubjectAreaTagMap.
     */
    public static final String SUBJECT_AREA_TAG_MAP_KEY = "satagmap";

    /**
     * Key for the URL to post to for this Viewlet.
     */
    public static final String POST_URL_KEY = "postUrl";

    /**
     * Key for the ViewletProcessor to use for included Viewlets.
     */
    public static final String VIEWLET_PROCESSOR_KEY = "viewletProcessor";

    /**
     * Key for the WebMeshObjectsToViewFactory to use.
     */
    public static final String WEB_MESH_OBJECTS_TO_VIEW_FACTORY_KEY = "webMeshObjectsToViewFactory";

    /**
     * Key for the MeshObjectIdentifierSerializer to use for the purposes of the HttpShell.
     */
    public static final String SHELL_MESH_OBJECT_IDENTIFIER_SERIALIZER_KEY = "meshObjectIdentifierSerializer";

    /**
     * Key for the MeshTypeIdentifierSerializer to use for the purposes of the HttpShell.
     */
    public static final String SHELL_MESH_TYPE_IDENTIFIER_SERIALIZER_KEY = "meshTypeIDentifierSerializer";

    /**
     * The top-most instance of ViewletModel, which is a singleton.
     */
    static public class TopSingleton
        extends
            ViewletModel
    {
        /**
         * Private constructor, use factory method.
         *
         * @param data the data in the ViewletModel
         */
        protected TopSingleton(
                Map<String,Object> data )
        {
            super( data, null );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ViewletModel createSubmodel(
                Map<String,Object> data )
        {
            return new TopForRequest( data, this );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ViewletModel createSubmodel()
        {
            return new TopForRequest( new HashMap<>(), this );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewDomId()
        {
            throw new UnsupportedOperationException( "Cannot invoke on TopSingleton" );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewShellVar()
        {
            throw new UnsupportedOperationException( "Cannot invoke on TopSingleton" );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewShellSubVarFor(
                String shellVar )
        {
            throw new UnsupportedOperationException( "Cannot invoke on TopSingleton" );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setCurrentOverlayId(
                String id )
        {
            throw new UnsupportedOperationException( "Cannot invoke on TopSingleton" );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void unsetCurrentOverlayId()
        {
            throw new UnsupportedOperationException( "Cannot invoke on TopSingleton" );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getCurrentOverlayId()
        {
            throw new UnsupportedOperationException( "Cannot invoke on TopSingleton" );
        }
    }

    /**
     * The top-most instance of A ViewletModel for a particular request.
     */
    static public class TopForRequest
        extends
            ViewletModel
    {
        /**
         * Private constructor, use factory method.
         *
         * @param data the data in the ViewletModel
         * @param delegate the ViewletModel to delegate to
         */
        protected TopForRequest(
                Map<String,Object> data,
                ViewletModel       delegate )
        {
            super( data, delegate );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ViewletModel createSubmodel(
                Map<String,Object> data )
        {
            return new Below( data, this );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ViewletModel createSubmodel()
        {
            return new Below( new HashMap<>(), this );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewDomId()
        {
            return "u" + ( ++theCurrentDomId );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewShellVar()
        {
            return "v" + ( ++theCurrentShellVarId );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewShellSubVarFor(
                String shellVar )
        {
            if( theShellSubvars == null ) {
                theShellSubvars = new HashMap<>();
            }
            Integer mostRecent = theShellSubvars.get( shellVar );
            int ret;
            if( mostRecent == null ) {
                ret = 1;

            } else {
                ret = mostRecent + 1;
            }
            theShellSubvars.put( shellVar, ret );
            return "s" + ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setCurrentOverlayId(
                String id )
        {
            if( theCurrentOverlayId == null ) {
                theCurrentOverlayId = id;
            } else {
                throw new IllegalStateException( "Already have current overlay id: " + id + " vs " + theCurrentOverlayId );
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void unsetCurrentOverlayId()
        {
            if( theCurrentOverlayId == null ) {
                throw new IllegalStateException( "No current overlay id" );
            }
            theCurrentOverlayId = null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getCurrentOverlayId()
        {
            if( theCurrentOverlayId != null ) {
                return theCurrentOverlayId;
            } else {
                throw new IllegalStateException( "No current overlay id" );
            }
        }

        /**
         * The current instance id. This is a counter that can only be incremented
         * during the life of this Model. It allows Helpers to obtain a unique
         * instance ID for HTML tags they are generating.
         */
        protected int theCurrentDomId = 0;

        /**
         * The current shell variable id. This is a counter that can only be incremented
         * during the life of this Model. It allows Helpers to obtain a unique
         * instance ID for HTML tags they are generating.
         */
        protected int theCurrentShellVarId = 0;

        /**
         * Keeps track of sub-variables issued for shell variables.
         */
        protected Map<String,Integer> theShellSubvars;

        /**
         * The current overlay id, if any.
         */
        protected String theCurrentOverlayId;
    }

    /**
     * Non-top instances of ViewletModel
     */
    static class Below
        extends
            ViewletModel
    {
        /**
         * Private constructor, use factory method.
         *
         * @param data the data in the ViewletModel
         * @param delegate the ViewletModel to delegate to
         */
        protected Below(
                Map<String,Object> data,
                ViewletModel       delegate )
        {
            super( data, delegate );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ViewletModel createSubmodel(
                Map<String,Object> data )
        {
            return new Below( data, this );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ViewletModel createSubmodel()
        {
            return new Below( new HashMap<>(), this );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewDomId()
        {
            return ((ViewletModel)theDelegate).obtainNewDomId();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewShellVar()
        {
            return ((ViewletModel)theDelegate).obtainNewShellVar();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String obtainNewShellSubVarFor(
                String shellVar )
        {
            return ((ViewletModel)theDelegate).obtainNewShellSubVarFor( shellVar );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setCurrentOverlayId(
                String id )
        {
            ((ViewletModel)theDelegate).setCurrentOverlayId( id );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void unsetCurrentOverlayId()
        {
            ((ViewletModel)theDelegate).unsetCurrentOverlayId();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getCurrentOverlayId()
        {
            return ((ViewletModel)theDelegate).getCurrentOverlayId();
        }
    }
}
