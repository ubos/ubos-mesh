//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

import java.io.IOException;
import com.github.jknack.handlebars.Options;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.HandlebarsViewlet;
import net.ubos.util.ResourceHelper;
import net.ubos.util.StringHelper;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.vl.ViewletFactory;
import net.ubos.vl.ViewletFactoryChoice;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewlet;
import net.ubos.web.vl.WebViewletState;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Marks the boundaries of a Viewlet.
 */
public class ViewletHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        HandlebarsViewlet   vl        = (HandlebarsViewlet) model.get( ViewletModel.VIEWLET_KEY );
        WebViewletState     state     = vl.getState();
        MeshObject          subject   = getPrimaryMeshObject( model, options );
        WebViewletPlacement placement = (WebViewletPlacement) vl.getViewedMeshObjects().getViewletPlacement();

        boolean generateForm = options.hash.containsKey( FORM_ID_ARGNAME ) && WebViewletState.EDIT.equals( state );

        boolean allowAlternatives     = "true".equalsIgnoreCase( (String) vl.getViewedMeshObjects().getViewletParameter( SHOW_ALTERNATIVES_ARGNAME, "true" ) );
        boolean showHistory           = "true".equalsIgnoreCase( options.hash( SHOW_HISTORY_ARGNAME,      "false" ));
        boolean showChangeState       = "true".equalsIgnoreCase( options.hash( SHOW_CHANGE_STATE_ARGNAME, "false" ));
        boolean showRefresh           = "true".equalsIgnoreCase( options.hash( SHOW_REFRESH_ARGNAME,      "false" ));
        boolean showAlternatives      = "true".equalsIgnoreCase( options.hash( SHOW_ALTERNATIVES_ARGNAME, ( placement != WebViewletPlacement.INLINED && allowAlternatives ) ? "true" : "false" ));
        String  specifiedStatesString = options.hash( VIEWLET_STATES_ARGNAME,    "" );
        int     max                   = options.hash( MAX_LENGTH_ARGNAME,        15 );
        String  localeString          = options.hash( LOCALE_ARGNAME,            null );

        specifiedStatesString = specifiedStatesString.trim();

        Options.Buffer buffer = options.buffer();

        if( showHistory || showChangeState || showRefresh || showAlternatives ) {
            StringBuilder buf = new StringBuilder();
            if( showHistory ) {
                Locale locale = localeString != null ? Locale.forLanguageTag( localeString ) : null;
                int dateStyle = determineStyle( options.hash( DATESTYLE_ARGNAME, null ), "DateStyle" );
                int timeStyle = determineStyle( options.hash( TIMESTYLE_ARGNAME, null ), "TimeStyle" );

                insertHistory( model, subject, dateStyle, timeStyle, locale, max, buf );
            }
            if( showChangeState ) {
                insertChangeState( model, specifiedStatesString, buf );
            }
            if( showRefresh ) {
                insertRefresh( model, buf );
            }
            if( showAlternatives ) {
                insertAlternatives( model, buf );
            }

            if( buf.length() > 0 ) {
                buffer.append( "<nav class=\"vl-tools\">\n" );
                buffer.append( buf );
                buffer.append( "</nav>\n" );
            }
        }

        buffer.append( "<div class=\"" ).append( vl.getCssClass() ).append( " vl " );
        buffer.append( placement.getCssClass() );
        buffer.append( "\">\n" );

        if( generateForm ) {
            String formId = options.hash( FORM_ID_ARGNAME );
            if( formId == null || formId.trim().isEmpty() ) {
                formId = model.obtainNewDomId();
            }
            String postUrl = (String) model.get( ViewletModel.POST_URL_KEY );
            buffer.append( " <form id=\"" );
            buffer.append( formId );
            buffer.append( "\" action=\"" );
            buffer.append( StringHelper.stringToHtml( postUrl ));
            buffer.append( "\" method=\"POST\" enctype=\"multipart/form-data\" accept-charset=\"UTF-8\">\n" );
        }

        buffer.append( options.fn());

        if( generateForm ) {
            buffer.append( " </form>\n" );
        }
        buffer.append( "</div>\n" );

        StructuredResponse sr = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
        sr.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars/ViewletHelper.css" );
        sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars/ToggleCssClass.js" );

        return buffer;
    }

    /**
     * Insert the meshObjectHistory markup.
     *
     * @param model the ViewletModel
     * @param buf append content here
     */
    protected void insertHistory(
            ViewletModel   model,
            MeshObject     subject,
            int            dateStyle,
            int            timeStyle,
            Locale         locale,
            int            max,
            StringBuilder  buf )
        throws
            IOException
    {
        MeshObjectHistory history = subject.getMeshBaseView().getMeshBase().meshObjectHistory( subject.getIdentifier() );

        if( history == null ) {
            return;// we silently skip out
        }

        DateFormat format;
        if( locale != null ) {
            format = DateFormat.getDateTimeInstance( dateStyle, timeStyle, locale );
        } else {
            format = DateFormat.getDateTimeInstance( dateStyle, timeStyle );
        }

        WebViewlet           currentViewlet = (WebViewlet) model.get( ViewletModel.VIEWLET_KEY );
        WebMeshObjectsToView toView         = currentViewlet.getViewedMeshObjects().getMeshObjectsToView();

        HistoryCursorIterator<MeshObject> cursor = history.iterator();
        cursor.moveToAfterLast();
        MeshObject mostRecent = cursor.peekPrevious();

        cursor.moveToBefore( subject );

        String instanceId = String.valueOf( model.obtainNewDomId());

        buf.append( "<div class=\"" );
        buf.append( getCssClass() );
        buf.append( " popup\" id=\"" );
        buf.append( instanceId );
        buf.append( "\">\n" );

        buf.append( " <div class=\"label\">" );
        buf.append( "<a href=\"javascript:toggle_css_class( '" ).append( instanceId ).append( "', 'expanded' )\">" );

        buf.append( theResourceHelper.getResourceString( "HistoryLabel" ));

        buf.append( "</a>" );
        buf.append( "</div>\n" );

        buf.append( " <ul>\n" );

        List<MeshObject> before = cursor.peekPrevious( max );
        List<MeshObject> after  = cursor.peekNext( max );

        int startBeforeAt;
        if( before.size() >= max/2 ) {
            if( after.size() >= max/2 ) {
                startBeforeAt = max/2;
            } else {
                startBeforeAt = after.size();
            }
        } else {
            startBeforeAt = 0;
        }
        for( int i=startBeforeAt ; i<2*max ; ++i ) {
            MeshObject then;
            if( i<before.size() ) {
                then = before.get( before.size()-i-1 ); // reverse order
            } else if( i >= before.size() + after.size() ) {
                 break;
            } else {
                then = after.get( i - before.size() );
            }

            Date thenDate = new Date( then.getTimeUpdated() );
            WebMeshObjectsToView thenToView = toView.withViewletState( WebViewletState.VIEW );

            if( then.getTimeUpdated() != subject.getTimeUpdated() ) {
                thenToView = thenToView.withAtTime( then.getTimeUpdated() );
            }

            String thenUrlString = thenToView.asUrlString();

            buf.append( " <li" );
            String        sep     = "";
            StringBuilder liClazz = new StringBuilder();

            if( then.getTimeUpdated() == subject.getTimeUpdated() ) {
                liClazz.append( sep ).append( "selected" );
                sep = " ";
            }
            if( then.getTimeUpdated() == mostRecent.getTimeUpdated() ) { // not ==
                liClazz.append( sep ).append( "head" );
                sep = " ";
            }
            if( !sep.isEmpty() ) {
                buf.append( " class=\"" ).append( liClazz ).append( "\"" );
            }
            buf.append( ">\n" );

            buf.append( "  <a href=\"" );
            buf.append( StringHelper.stringToHtml( thenUrlString ));
            buf.append( "\">" );

            String dateTime = format.format( thenDate );
            buf.append( dateTime );
            if( then.getTimeUpdated() == mostRecent.getTimeUpdated() ) { // not ==
                buf.append( "&nbsp;(HEAD)");
            }
            buf.append( "</a></li>\n" );
        }

        buf.append( " </ul>\n" );
        buf.append( "</div>\n" );

    }

    /**
     * Insert the ChangeState markup.
     *
     * @param model the ViewletModel
     * @param specifiedStatesString the applicable parameter
     * @param buf append content here
     */
    protected void insertChangeState(
            ViewletModel   model,
            String         specifiedStatesString,
            StringBuilder  buf )
    {
        HandlebarsViewlet    vl             = (HandlebarsViewlet) model.get( ViewletModel.VIEWLET_KEY );
        WebViewletState      currentState   = vl.getState();
        Set<WebViewletState> possibleStates = vl.getPossibleStates();

        if( !specifiedStatesString.isEmpty() ) {
            String []     specifiedStates = specifiedStatesString.split( "\\s*,\\s*" );

            ArrayList<WebViewletState> toShow = new ArrayList<>();
            for( String current : specifiedStates ) {
                current = current.toLowerCase();

                WebViewletState found = null;
                for( WebViewletState possible : possibleStates ) {
                    if( current.equals( possible.getName().toLowerCase() )) {
                        found = possible;
                        break;
                    }
                }
                if( found != null ) {
                    toShow.add( found );
                }
            }
            if( toShow.isEmpty() ) {
                return; // skip
            }

            String instanceId = String.valueOf( model.obtainNewDomId());
            buf.append( "<div class=\"" );
            buf.append( getCssClass() );
            buf.append( " popup\" id=\"" );
            buf.append( instanceId );
            buf.append( "\">\n" );

            WebMeshObjectsToView currentToView = vl.getViewedMeshObjects().getMeshObjectsToView();

            if( toShow.size() == 1 ) {
                WebViewletState possibleState = toShow.get( 0 );
                if( possibleState.equals( currentState )) {
                    buf.append( "  <div class=\"label selected\">" );
                    buf.append( currentState.getUserVisibleName() );
                    buf.append( "  </div>\n" );

                } else {
                    WebMeshObjectsToView newToView = currentToView.withViewletState( possibleState );

                    buf.append( "  <div class=\"label\">" );
                    buf.append( "<a href=\"" );
                    buf.append( StringHelper.stringToHtmlUrl( newToView.asUrlString() ));
                    buf.append( "\">" );
                    buf.append( possibleState.getUserVisibleName() );
                    buf.append( "</a>" );
                    buf.append( "  </div>\n" );
                }

            } else { // more than 1
                buf.append( " <div class=\"label\">" );
                buf.append( "<a href=\"javascript:toggle_css_class( '" ).append( instanceId ).append( "', 'expanded' )\">" );

                buf.append( theResourceHelper.getResourceString( "ChangeStateLabel" ));

                buf.append( "</a>" );
                buf.append( "</div>\n" );

                buf.append( " <ul>\n" );
                for( WebViewletState state : toShow ) {
                    if( state.equals( currentState )) {
                        buf.append( "  <li class=\"selected\">" );
                        buf.append( state.getUserVisibleName() );
                        buf.append( "  </li>\n" );

                    } else {
                        WebMeshObjectsToView newToView = currentToView.withViewletState( state );

                        buf.append( "  <li>" );
                        buf.append( "<a href=\"" );
                        buf.append( StringHelper.stringToHtmlUrl( newToView.asUrlString() ));
                        buf.append( "\">" );
                        buf.append( state.getUserVisibleName() );
                        buf.append( "</a>" );
                        buf.append( "  </li>\n" );
                    }
                }
                buf.append( " </ul>\n" );
            }
            buf.append( "</div>\n" );
        }
    }

    /**
     * Insert the refresh markup.
     *
     * @param model the ViewletModel
     * @param buf append content here
     */
    protected void insertRefresh(
            ViewletModel  model,
            StringBuilder buf )
    {
        WebMeshObjectsToView toView = (WebMeshObjectsToView) model.get( ViewletModel.TOVIEW_KEY );

        buf.append( "<div class=\"" ).append( getCssClass() ).append( " popup\">\n" );
        buf.append( " <div class=\"label\">" );
        buf.append( "<a href=\"" ).append( StringHelper.stringToHtmlUrl( toView.asUrlString() ));
        buf.append( "\">" );
        buf.append( theResourceHelper.getResourceString( "RefreshLabel" ));
        buf.append( "</a></div>\n" );
        buf.append( "</div>\n" );
    }

    /**
     * Insert the ViewletAlternatives markup.
     *
     * @param model the ViewletModel
     * @param buf append content here
     */
    protected void insertAlternatives(
            ViewletModel  model,
            StringBuilder buf )
    {
        HandlebarsViewlet    vl     = (HandlebarsViewlet) model.get( ViewletModel.VIEWLET_KEY );
        WebMeshObjectsToView toView = vl.getViewedMeshObjects().getMeshObjectsToView();
        ViewletFactory       vlFact = vl.getFactory();

        toView = toView.withRequiredViewletName( null );
        toView = toView.withRecommendedViewletName( null );
        toView = toView.withoutViewletParameters();

        ViewletFactoryChoice [] candidates = vlFact.determineOrderedViewletChoices( toView );

        if( candidates.length > 0 ) {

            String instanceId = String.valueOf( model.obtainNewDomId());

            buf.append( "<div class=\"" );
            buf.append( getCssClass() );
            buf.append( " vl-alternatives\" id=\"" );
            buf.append( instanceId );
            buf.append( "\">\n" );

            buf.append( " <div class=\"label\">" );
            buf.append( "<a href=\"javascript:toggle_css_class( '" ).append( instanceId ).append( "', 'expanded' )\">" );
            buf.append( theResourceHelper.getResourceString( "AlternativesLabel" ));
            buf.append( "</a>" );
            buf.append( "</div>\n" );

            buf.append( " <ul>\n" );
            for( int i=0 ; i<candidates.length ; ++i ) {
                WebMeshObjectsToView newToView = toView;
                newToView = newToView.withRequiredViewletName( candidates[i].getName() );

                buf.append( "  <li" );
                if( candidates[i].getName().equals( vl.getName() )) {
                    buf.append( " class=\"selected\"" );
                }
                buf.append( "><a href=\"" );
                buf.append( StringHelper.stringToHtmlUrl( newToView.asUrlString()));
                buf.append( "\">" );

                buf.append( candidates[i].getUserVisibleName() );

                buf.append( "</a>" );

                buf.append( "</li>\n" );
            }
            buf.append( " </ul>\n" );
            buf.append( "</div>\n" );
        }
    }

    /**
     * Helper method to parse date or time format
     *
     * @param format format string
     * @param tagName for error reporting
     * @return DateFormat, or null
     */
    protected int determineStyle(
            String format,
            String tagName )
    {
        int ret;
        if( format != null && format.length() > 0 ) {
            if( "FULL".equalsIgnoreCase( format )) {
                ret = DateFormat.FULL;

            } else if( "LONG".equalsIgnoreCase( format )) {
                ret = DateFormat.LONG;

            } else if( "MEDIUM".equalsIgnoreCase( format )) {
                ret = DateFormat.MEDIUM;

            } else if( "SHORT".equalsIgnoreCase( format )) {
                ret = DateFormat.SHORT;

            } else {
                throw new IllegalArgumentException( tagName + " attribute must be one of \"FULL\", \"LONG\", \"MEDIUM\", \"SHORT\"." );
            }
        } else {
            ret = DateFormat.SHORT;
        }
        return ret;
    }

    /**
     * Name of an argument indicating the ID of the form to be generated in case
     * of a Viewlet in the "edit" state. If this is not given, no form is generated
     * (to support included Viewlets). If it has an empty value, it will be auto-generated.
     */
    public static final String FORM_ID_ARGNAME = "formId";

    /**
     * Name of an argument indicating that the user should be able to select an alternate version
     * of this MeshObject from the MeshObjectHistory. This is only shown if history support is on.
     */
    public static final String SHOW_HISTORY_ARGNAME = "showHistory";

    /**
     * Name of an argument indicating that the user should be able to select an alternate ViewletState.
     */
    public static final String SHOW_CHANGE_STATE_ARGNAME = "showChangeState";

    /**
     * Name of an argument indicating that that a refresh button should be displayed.
     */
    public static final String SHOW_REFRESH_ARGNAME = "showRefresh";

    /**
     * Name of an argument indicating that the user should be able to select an alternate Viewlet.
     */
    public static final String SHOW_ALTERNATIVES_ARGNAME = "showAlternatives";

    /**
     * Name of an argument specifying the allowed Viewlet states.
     */
    public static final String VIEWLET_STATES_ARGNAME = "viewletStates";

    /**
     * Name of the argument indicating the Locale to be used.
     */
    public static final String LOCALE_ARGNAME = "locale";

    /**
     * Name of the argument indicating the date style to be used.
     */
    public static final String DATESTYLE_ARGNAME = "dateStyle";

    /**
     * Name of the argument indicating the time style to be used.
     */
    public static final String TIMESTYLE_ARGNAME = "timeStyle";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        FORM_ID_ARGNAME,
        SHOW_HISTORY_ARGNAME,
        SHOW_CHANGE_STATE_ARGNAME,
        VIEWLET_STATES_ARGNAME,
        SHOW_ALTERNATIVES_ARGNAME,
        SHOW_REFRESH_ARGNAME,
        LOCALE_ARGNAME,
        DATESTYLE_ARGNAME,
        TIMESTYLE_ARGNAME
    };

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( ViewletHelper.class );
}
