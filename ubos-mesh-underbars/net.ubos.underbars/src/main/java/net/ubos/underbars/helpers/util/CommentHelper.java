//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * Do-nothing tag to hold a comment not to be emitted.
 */
public class CommentHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  context,
            Options options )
        throws
            IOException
    {
        return "";
    }
}