//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import net.ubos.underbars.vl.ViewletModel;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.ArrayHelper;

/**
 * Obtain the RoleTypes in which a MeshObject is participating. If a neighbor
 * MeshObject is provided, it will only provide the RoleTypes this principal
 * MeshObject participates in with this neighbor. If no neighbor is provided, it
 * will be all RoleTypes with all neighbors.
 */
public class RoleTypesHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj      = getPrimaryMeshObject(  model, options );
        MeshObject neighbor = getNeighborMeshObject( model, options );
        int        max      = optionalHash( MAX_ARGNAME, -1, options );

        RoleType [] ret;

        if( neighbor == null ) {
            ret = obj.getRoleTypes();
        } else {
            ret = obj.getRoleTypes( neighbor );
        }

        if( max > 0 && ret.length > max ) {
            ret = ArrayHelper.copyIntoNewArray( ret, 0, max, RoleType.class );
        }
        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        NEIGHBOR_ARGNAME,
        MAX_ARGNAME
    };
}
