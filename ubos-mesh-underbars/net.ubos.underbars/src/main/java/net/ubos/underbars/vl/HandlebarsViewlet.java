//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.web.vl.WebViewlet;

/**
 * Specializes WebViewlets for Handlebars implementation.
 */
public interface HandlebarsViewlet
    extends
        WebViewlet
{}
