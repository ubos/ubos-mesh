//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.underbars.resources.TemplateMap;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Template;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.underbars.resolvers.HandlebarsModelResolver;
import net.ubos.util.logging.Log;
import net.ubos.web.vl.AbstractWebViewlet;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.ToViewStructuredResponsePair;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewedMeshObjects;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Factors out commonly used functionality of WebViewlets emitting text (other
 * than HTML) and implemented in Handlebars.
 */
public abstract class AbstractTextHandlebarsViewlet
        extends
            AbstractWebViewlet
        implements
            HandlebarsViewlet
{
    private static final Log log = Log.getLogInstance( AbstractHtmlHandlebarsViewlet.class ); // our own, private logger

    /**
     * Constructor, for subclasses only.
     *
     * @param viewletName the computable name of the Viewlet
     * @param contentType the content type emitted by this Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param viewed the WebViewedMeshObjects to use
     */
    protected AbstractTextHandlebarsViewlet(
            String               viewletName,
            String               contentType,
            TemplateMap          templateMap,
            WebViewedMeshObjects viewed )
    {
        super( viewletName, viewed );

        theContentType = contentType;
        theTemplateMap = templateMap;
    }

    /**
     * Obtain the Handlebars Templates helping to implement this Viewlet.
     *
     * @return the NamedTemplateMap
     */
    public TemplateMap getTemplateMap()
    {
        return theTemplateMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processRequest(
            ToViewStructuredResponsePair rr )
        throws
            IOException
    {
        StructuredResponse response = rr.getStructuredResponse();

        ViewletModel         parentVlModel = (ViewletModel) theViewedMeshObjects.getViewletParameter( ViewletModel.class.getName() );
        WebMeshObjectsToView toView        = rr.getToView();

        Map<String,Object> data = new HashMap<>();
        data.put( ViewletModel.STRUCTURED_RESPONSE_KEY, response );
        data.put( ViewletModel.CONTEXT_KEY,             response.getRequest().getUrl().getContextPath() );
        data.put( ViewletModel.TOVIEW_KEY,              toView );

        data.put( ViewletModel.SUBJECT_KEY,    toView.getSubject() );
        data.put( ViewletModel.MESHOBJECT_KEY, toView.getSubject() );
        data.put( ViewletModel.MESHBASE_KEY,   toView.getSubject().getMeshBase() );

        data.put( ViewletModel.VIEWLET_KEY,    this );
        data.put( ViewletModel.POST_URL_KEY,   getPostUrl( toView ));

        ViewletModel vlModel = parentVlModel.createSubmodel( data );

        Context c = Context.newBuilder( vlModel )
                .push( HandlebarsModelResolver.SINGLETON )
                .build();

        try {
            Template template = theTemplateMap.obtainFor( SECTION_NAME );
            String   result   = template.apply( c );

            response.setBinaryContent( result.getBytes( StandardCharsets.UTF_8 ), theContentType );

        } catch( IOException ex ) {
            // happens if the Handlebars template was changed, with syntax errors, after initial deployment

            log.error( ex ); // details to the log, only high-level in HTML

            // not sure what to do
            StringBuilder buf = new StringBuilder();
            buf.append( "ERROR:\n" );
            buf.append( "More details are in the system log.\n" );

            response.setBinaryContent( buf.toString().getBytes( StandardCharsets.UTF_8 ), SelectableMimeType.TEXT_PLAIN.getMimeType() );
        }
    }

    /**
     * The content type we emit.
     */
    protected final String theContentType;

    /**
     * Maps the names of the StructuredResponse entries to the Templates responsible for them.
     */
    protected final TemplateMap theTemplateMap;

    /**
     * Name of the section that contains all of the text output.
     */
    public static final String SECTION_NAME = "text";
}
