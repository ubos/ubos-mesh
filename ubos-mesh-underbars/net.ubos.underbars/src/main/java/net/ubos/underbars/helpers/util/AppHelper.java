//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.daemon.AppInfo;
import net.ubos.daemon.Daemon;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Shows information about the App.
 */
public class AppHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  context,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String display     = options.hash( DISPLAY_ARGNAME, DISPLAY_ARGNAME_FULLNAME );
        String nullContent = options.hash( NULL_CONTENT_ARGNAME, "" );

        Options.Buffer buffer = options.buffer();
        String         content;

        AppInfo appInfo = Daemon.getAppInfo();
        if( appInfo == null ) {
            content = nullContent;

        } else {
            switch( display ) {
                case DISPLAY_ARGNAME_FULLNAME:
                    if( appInfo.getFullAppName() == null ) {
                        content = nullContent;
                    } else {
                        content = appInfo.getFullAppName();
                    }
                    break;

                case DISPLAY_ARGNAME_SHORTNAME:
                    if( appInfo.getShortAppName() == null ) {
                        content = nullContent;
                    } else {
                        content = appInfo.getShortAppName();
                    }
                    break;

                default:
                    content = "???";
                    break;
            }
        }
        buffer.append( content );

        return buffer;
    }

    /**
     * Name of the argument indicating what information about the admin user should be shown.
     */
    public static final String DISPLAY_ARGNAME = "display";

    /**
     * Values for DISPLAY_ARGNAME.
     */
    public static final String DISPLAY_ARGNAME_FULLNAME  = "fullname";
    public static final String DISPLAY_ARGNAME_SHORTNAME = "shortname";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DISPLAY_ARGNAME,
        NULL_CONTENT_ARGNAME
    };
}
