//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.vl.CannotViewException;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.HttpRequest;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebMeshObjectsToViewFactory;
import net.ubos.web.vl.WebViewlet;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Functionality common to Helpers including other Viewlets.
 */
public abstract class AbstractViewletIncludeHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    protected WebMeshObjectsToView determineChildToView(
            HttpRequest  childRequest,
            ViewletModel model,
            Options      options )
        throws
            CannotViewException,
            ParseException,
            IOException
    {
        MeshObject subject = options.hash( SUBJECT_ARGNAME );
        String     vlName  = options.hash( NAME_ARGNAME );

        ViewletDimensionality requireDimensionality     = parseDimensionalityString( options.hash( REQUIRED_VIEWLET_DIMENSIONALITY_ARGNAME ));
        ViewletDimensionality recommendedDimensionality = parseDimensionalityString( options.hash( RECOMMENDED_VIEWLET_DIMENSIONALITY_ARGNAME ));
        ViewletDetail         requiredDetail            = parseDetailString(         options.hash( REQUIRED_VIEWLET_DETAIL_ARGNAME ));
        ViewletDetail         recommendedDetail         = parseDetailString(         options.hash( RECOMMENDED_VIEWLET_DETAIL_ARGNAME ));

        WebViewlet           parentViewlet  = (WebViewlet) model.get( ViewletModel.VIEWLET_KEY );
        WebMeshObjectsToView parentToView   = (WebMeshObjectsToView) model.get( ViewletModel.TOVIEW_KEY );

        WebMeshObjectsToViewFactory factory = Underbars.getMeshObjectsToViewFactory();
        WebMeshObjectsToView childToView;

        if( subject == null ) {
            childToView = factory.obtainFor( childRequest );
        } else {
            childToView = factory.obtainFor( subject, childRequest.getUrl().getContextUrl() );
        }
        if( parentToView.getWhen() != Long.MAX_VALUE ) {
            childToView = childToView.withAtTime( parentToView.getWhen() );
        }
        if( vlName != null ) {
            childToView = childToView.withRequiredViewletName( vlName );
        }
        if( requireDimensionality != null ) {
            childToView = childToView.withRequiredViewletDimensionality( requireDimensionality );
        }
        if( recommendedDimensionality != null ) {
            childToView = childToView.withRequiredViewletDimensionality( recommendedDimensionality );
        }
        if( requiredDetail != null ) {
            childToView = childToView.withRequiredViewletDetail( requiredDetail );
        }
        if( recommendedDetail != null ) {
            childToView = childToView.withRequiredViewletDetail( recommendedDetail );
        }

        childToView = childToView.withViewletParameter( ViewletModel.class.getName(), model ); // pass on this context as the parent context
        childToView = childToView.withViewletState( parentViewlet.getState() );

        return childToView;
    }

    /**
     * Helper to parse a parameter
     */
    protected WebViewletPlacement parsePlacementString(
            String raw )
    {
        if( raw == null ) {
            return null;
        }

        return WebViewletPlacement.valueOf( raw.toUpperCase() );
    }

    /**
     * Helper to parse a parameter
     */
    protected ViewletDimensionality parseDimensionalityString(
            String raw )
    {
        if( raw == null ) {
            return null;
        }

        return ViewletDimensionality.valueOf( raw.toUpperCase() );
    }

    /**
     * Helper to parse a parameter
     */
    protected ViewletDetail parseDetailString(
            String raw )
    {
        if( raw == null ) {
            return null;
        }

        return ViewletDetail.valueOf( raw.toUpperCase() );
    }

    /**
     * Name of an argument indicating the (changed) subject of the to-be-included Viewlet.
     */
    public static final String SUBJECT_ARGNAME = "subject";

    /**
     * Name of an argument indicating the name of the to-be-included Viewlet.
     */
    public static final String NAME_ARGNAME = "name";

    // No Placement parameters -- the subclass determines that

    /**
     * Name of an argument in the incoming request that indicates the required Viewlet dimensionality.
     */
    public static final String REQUIRED_VIEWLET_DIMENSIONALITY_ARGNAME = WebMeshObjectsToView.REQUIRED_VIEWLET_DIMENSIONALITY_PAR_NAME;

    /**
     * Name of an argument in the incoming request that indicates the recommended Viewlet dimensionality.
     */
    public static final String RECOMMENDED_VIEWLET_DIMENSIONALITY_ARGNAME = WebMeshObjectsToView.RECOMMENDED_VIEWLET_DIMENSIONALITY_PAR_NAME;

    /**
     * Name of an argument in the incoming request that indicates the required Viewlet detail.
     */
    public static final String REQUIRED_VIEWLET_DETAIL_ARGNAME = WebMeshObjectsToView.REQUIRED_VIEWLET_DETAIL_PAR_NAME;

    /**
     * Name of an argument in the incoming request that indicates the recommended Viewlet detail.
     */
    public static final String RECOMMENDED_VIEWLET_DETAIL_ARGNAME = WebMeshObjectsToView.RECOMMENDED_VIEWLET_DETAIL_PAR_NAME;
}
