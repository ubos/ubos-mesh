//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import io.undertow.server.handlers.Cookie;
import java.nio.charset.StandardCharsets;
import net.ubos.util.StringHelper;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.web.AbstractHttpCookie;
import net.ubos.web.IncomingHttpCookie;

/**
 * Bridges the Cookie interface into the Undertow cookies.
 */
class IncomingCookieAdapter
        extends
            AbstractHttpCookie
        implements
            IncomingHttpCookie,
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( IncomingCookieAdapter.class );

    /**
     * Constructor.
     *
     * @param delegate the Undertow Cookie we delegate to
     */
    public IncomingCookieAdapter(
            Cookie delegate )
    {
        theDelegate = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        if( theName == null ) {
            String delegateName = theDelegate.getName();
            theName = decodeCookieName(delegateName);
        }
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue()
    {
        if( theValue == null ) {
            String delegateValue = theDelegate.getValue();
            theValue = decodeFromQuotedString(delegateValue);
        }
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[]{
                    "getName()",
                    "getValue()"
                },
                new Object[]{
                    getName(),
                    getValue()
                });
    }

    /**
     * Decode a quoted String per the HTTP spec. If this is not a quoted String,
     * return the String unchanged.
     *
     * @param quoted the input String
     * @return the de-quoted String
     */
    public static String decodeFromQuotedString(
            String quoted )
    {
        if( quoted.length() < 2 ) {
            return quoted;
        }
        if( !quoted.startsWith( "\"" )) {
            return quoted;
        }
        if( !quoted.endsWith( "\"" )) {
            return quoted;
        }

        StringBuilder buf = new StringBuilder( quoted.length()-2 );
        for( int i=1 ; i<quoted.length()-1 ; ++i ) {
            char c = quoted.charAt( i );
            switch( c ) {
                case '\\':
                    if( i<quoted.length()-1 ) {
                        // not last character
                        if( quoted.charAt( i+1 ) != '"' ) {
                            buf.append( c ); // something else
                        }
                    } else {
                        // last char
                        buf.append( c );
                    }
                    break;
                default:
                    buf.append( c );
                    break;
            }
        }
        return buf.toString();
    }

    /**
     * Encode a cookie's name safely. Notably, PHP has this
     * nasty habit of turning periods into underscores.
     *
     * @param name the to-be-encoded name
     * @return the encoded name
     */
    public static String encodeCookieName(
            String name )
    {
        String temp = URLEncoder.encode( name, StandardCharsets.UTF_8 );
        String ret  = temp.replaceAll( "\\.", "!" );

        return ret;
    }

    /**
     * Decode a cookie's name safely. Notably, PHP has this
     * nasty habit of turning periods into underscores.
     *
     * @param encoded the encoded name
     * @return the decoded name
     */
    public static String decodeCookieName(
            String encoded )
    {
        String temp = encoded.replaceAll( "!", "." );
        String ret = URLDecoder.decode( temp, StandardCharsets.UTF_8 );
        return ret;
    }

    /**
     * The Servlet Cookie we delegate to.
     */
    protected final Cookie theDelegate;

    /**
     * The decoded name.
     */
    protected String theName;

    /**
     * The decoded value.
     */
    protected String theValue;
}
