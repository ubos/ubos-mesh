//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.templatesource;

import net.ubos.util.nameserver.NameServer;

/**
 * Maps names of Handlebars FileTemplateSources to the FileTemplateSources.
 */
public interface FileTemplateSourceMap
    extends
        NameServer<String,FileTemplateSource>
{
}
