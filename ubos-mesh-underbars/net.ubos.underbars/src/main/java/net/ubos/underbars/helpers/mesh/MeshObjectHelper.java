//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import java.io.IOException;
import com.github.jknack.handlebars.Options;
import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Insert a MeshObject according to the specific StringRepresentationScheme.
 */
public class MeshObjectHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        return formatMeshObject(
                model,
                obj,
                options );
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        FLAVOR_ARGNAME,
        MAX_LENGTH_ARGNAME,
        STRING_REPRESENTATION_ARGNAME
    };
}
