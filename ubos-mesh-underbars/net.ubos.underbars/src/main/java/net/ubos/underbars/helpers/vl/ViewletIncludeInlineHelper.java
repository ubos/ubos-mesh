//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

import java.io.IOException;
import com.github.jknack.handlebars.Options;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;
import net.ubos.vl.ViewletFactory;
import net.ubos.web.HttpRequest;
import net.ubos.web.SubHttpRequest;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.ToViewStructuredResponsePair;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewlet;
import net.ubos.web.vl.WebViewletProcessor;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Includes another Viewlet inlined into the parent Viewlet's HTML.
 */
public class ViewletIncludeInlineHelper
    extends
        AbstractViewletIncludeHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        StructuredResponse parentResponse = (StructuredResponse) model.get( ViewletModel.STRUCTURED_RESPONSE_KEY );
        ViewletFactory     vlFact         = Underbars.getViewletFactory();

        try {
            HttpRequest          childRequest  = SubHttpRequest.create( parentResponse.getRequest() );
            WebMeshObjectsToView childToView   = determineChildToView( childRequest, model, options );
            StructuredResponse   childResponse = StructuredResponse.create( childRequest );

            childToView = childToView.withRequiredViewletPlacement( WebViewletPlacement.INLINED );

            WebViewlet viewlet = (WebViewlet) vlFact.obtainFor( childToView );

            if( viewlet != null ) {
                ToViewStructuredResponsePair pair = ToViewStructuredResponsePair.create(
                        childToView,
                        childResponse );

                WebViewletProcessor viewletProcessor = (WebViewletProcessor) model.get( ViewletModel.VIEWLET_PROCESSOR_KEY );

                viewletProcessor.processViewlet( viewlet, pair );
            }
            // pass header items on to the parent response
            parentResponse.ensureHtmlHeaderLinks( childResponse.getHtmlHeaderLinks() );
            parentResponse.ensureHtmlHeaderScriptSources( childResponse.getHtmlHeaderScriptSources() );

            String ret = childResponse.getSectionContent( StructuredResponse.HTML_BODY_INLINED_SECTION );
            return ret;

        } catch( Throwable t ) {
            parentResponse.reportProblem( t );

            return "ERROR";
        }
    }


    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        SUBJECT_ARGNAME,
        NAME_ARGNAME,
        REQUIRED_VIEWLET_DIMENSIONALITY_ARGNAME,
        RECOMMENDED_VIEWLET_DIMENSIONALITY_ARGNAME,
        REQUIRED_VIEWLET_DETAIL_ARGNAME,
        RECOMMENDED_VIEWLET_DETAIL_ARGNAME
    };
}
