//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.session.Session;
import io.undertow.server.session.SessionConfig;
import io.undertow.server.session.SessionManager;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import net.ubos.mesh.MeshObjectIdentifierNamespaceNotFoundParseException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.underbars.util.UndertowHttpRequest;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.factory.FactoryException;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.vl.NoViewletFoundException;
import net.ubos.vl.ViewletFactory;
import net.ubos.web.HttpRequest;
import net.ubos.web.ProblemReporter;
import net.ubos.web.skin.Skin;
import net.ubos.web.skin.SkinProcessingException;
import net.ubos.web.skin.SkinType;
import net.ubos.web.util.ExceptionWithHttpStatusCode;
import net.ubos.web.vl.DefaultWebViewletProcessor;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.ToViewStructuredResponsePair;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewlet;
import net.ubos.web.vl.WebViewletPlacement;
import org.xnio.http.RedirectException;

/**
 * The main Undertow HttpHandler we use.
 */
public final class DispatchingHandler
    extends
        DefaultWebViewletProcessor
    implements
        HttpHandler
{
    private static final Log log = Log.getLogInstance( DispatchingHandler.class ); // our own, private logger

    /**
     * Constructor.
     */
    public DispatchingHandler()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleRequest(
            HttpServerExchange hse )
        throws
            Exception
    {
        SessionManager sessionManager = hse.getAttachment( SessionManager.ATTACHMENT_KEY );
        SessionConfig  sessionConfig  = hse.getAttachment( SessionConfig.ATTACHMENT_KEY );
        Session        session        = sessionManager != null ? sessionManager.getSession( hse, sessionConfig ) : null;

        ThreadIdentityManager.Who oldWho  = ThreadIdentityManager.getWho();
        ThreadIdentityManager.Who who     = session != null ? (ThreadIdentityManager.Who) session.getAttribute( Underbars.WHO_SESSION_ATTRIBUTE_NAME ) : null;

        String      relativePath = hse.getRelativePath();
        HttpHandler foundHandler;

        // Path match first

        foundHandler = Underbars.getPathHandlerFor( relativePath );
        if( foundHandler == null && relativePath.length() > 1 ) {
            int    slash  = relativePath.indexOf( '/', 1 );
            String firstStep;
            if( slash >= 0 ) {
                firstStep = relativePath.substring( 0, slash );
            } else {
                firstStep = relativePath;
            }
            foundHandler = Underbars.getPathPrefixHandlerFor( firstStep );
        }

        try {
            ThreadIdentityManager.setWho( who );

            if( foundHandler != null ) {
                foundHandler.handleRequest( hse );
                if( hse.getStatusCode() == 404 ) {
                    hse.getResponseHeaders().put( Headers.CONTENT_TYPE, "text/plain" );
                    hse.getResponseSender().send( "Not found." );
                }

            } else {
                meshHandle( hse );

            }
        } catch( Throwable t ) {
            log.error( t ); // e.g. out of memory errors
            hse.setStatusCode( 500 );

        } finally {
            ThreadIdentityManager.setWho( oldWho );
        }
    }

    /**
     * No Undertow handler matches; the mesh's job.
     *
     * @param hse request-response pair
     * @throws Exception something went wrong
     */
    protected void meshHandle(
            HttpServerExchange hse )
        throws
            Exception
    {
        UndertowHttpRequest request = UndertowHttpRequest.create( hse, Underbars.getContextPath() );

        StructuredResponse response = StructuredResponse.create( request );
        request.setAttribute( ProblemReporter.PROBLEM_REPORTER_ATTRIBUTE_NAME, response );

        int                          status   = StatusCodes.OK;
        String                       location = null;
        WebMeshObjectsToView         toView   = null; // make compiler happy
        WebViewlet                   viewlet  = null; // make compiler happy
        ToViewStructuredResponsePair pair;

        try {
            location = processHttpShell( request );
            if( location != null ) {
                response.setLocation( StatusCodes.SEE_OTHER, location );
                status = StatusCodes.SEE_OTHER;

            } else {
                try {
                    toView = Underbars.getMeshObjectsToViewFactory().obtainFor( request );

                } catch( Throwable ex ) {
                    handleMeshObjectsToViewFactoryException( ex ); // always throws
                }

                if( toView != null ) {
                    try {
                        ViewletFactory vlFact = Underbars.getViewletFactory();

                        viewlet = (WebViewlet) vlFact.obtainFor( toView );

                    } catch( FactoryException ex ) {
                        handleViewletFactoryException( ex ); // always throws
                    }
                }

                // cannot put this into the WebMeshObjectsToViewFactory; at a minimum, would need a subclass
                toView = toView.withViewletParameter( ViewletModel.class.getName(), Underbars.getTopViewletModel());

                pair = ToViewStructuredResponsePair.create(
                        toView,
                        response );

                if( viewlet != null ) {
                    processViewlet( viewlet, pair );
                }
                location = response.getLocation();
                if( location != null ) {
                    status = response.getHttpStatus();
                }
            }

        } catch( RedirectException ex ) {
            if( location == null ) {
                location = ex.getLocation();
                status   = ex.getStatusCode();
            }
        } catch( ExceptionWithHttpStatusCode ex ) {
            response.reportProblem( ex );
            status = ex.getDesiredHttpStatusCode();

        } catch( Throwable ex ) {
            response.reportProblem( ex );
        }


        if( location != null ) {
            hse.setStatusCode( status );
            hse.getResponseHeaders().add( LOCATION_HEADER, location );

        } else if( response.isBinary() ) {

            ByteBuffer buf = ByteBuffer.wrap( response.getBinaryContent() );

            hse.getResponseHeaders().put( Headers.CONTENT_TYPE, response.getBinaryContentType() );
            hse.setStatusCode( status );
            hse.getResponseSender().send( buf );

        } else {
            SkinType skinType = SkinType.fromRequestOrNull( request );

            if( skinType == null && viewlet != null ) {
                switch( (WebViewletPlacement) viewlet.getViewedMeshObjects().getViewletPlacement() ) {
                    case ROOT:
                        switch( viewlet.getViewedMeshObjects().getViewletDimensionality() ) {
                            case DIM_2D:
                                skinType = SkinType.DEFAULT_2D;
                                break;
                            case DIM_3D:
                                skinType = SkinType.DEFAULT_3D;
                                break;
                        }
                        break;

                    case IFRAME:
                        switch( viewlet.getViewedMeshObjects().getViewletDimensionality() ) {
                            case DIM_2D:
                                skinType = SkinType.IFRAME_2D;
                                break;
                            case DIM_3D:
                                skinType = SkinType.IFRAME_3D;
                                break;
                        }
                        break;
                }
            }

            if( skinType == null ) {
                skinType = SkinType.DEFAULT_2D;
            }

            String requestedSkin = request.getUrl().getUrlArgument( "skin" );
            if( toView == null ) {
                // This is a 404, and we don't want to leak the primary skin for now ...
                // FIXME This is a hack that needs to be removed again
                requestedSkin = "net.ubos.underbars.skin.bare";
            }

            Skin skin = Underbars.findSkin(
                    skinType,
                    response.getSectionContent( StructuredResponse.REQUESTED_SKIN_SECTION ),
                    requestedSkin,
                    Underbars.getDefaultSkinName() );

            ByteArrayOutputStream rawOutput = new ByteArrayOutputStream( 16536 );
            try {
                int newStatus = skin.format( response, rawOutput );
                if( status == StatusCodes.OK ) {
                    status = newStatus;
                }

            } catch( SkinProcessingException ex ) {
                log.error( ex );
                status = StatusCodes.INTERNAL_SERVER_ERROR;
            }

            ByteBuffer buf = ByteBuffer.wrap( rawOutput.toByteArray() );

            hse.getResponseHeaders().put( Headers.CONTENT_TYPE, skin.getContentType() );
            hse.setStatusCode( status );
            hse.getResponseSender().send( buf );
        }
        hse.endExchange();
    }

    /**
     * Process the commands contained in the request for the HttpShell.
     *
     * @param request the incoming request
     * @return URL to redirect to, or null
     * @throws IOException an I/O problem
     */
    protected String processHttpShell(
            HttpRequest request )
        throws
            IOException
    {
        return Underbars.runHttpShell( request );
    }

    /**
     * Handle exceptions thrown when attempting to create a MeshObjectsToView. This method is
     * factored out so subclasses can easily override.
     *
     * @param t the thrown exception
     * @throws ExceptionWithHttpStatusCode
     */
    protected void handleMeshObjectsToViewFactoryException(
            Throwable t )
        throws
            ExceptionWithHttpStatusCode
    {
//        if( t instanceof MalformedURLException ) {
//            throw new Exception( t );
//        }
//
        Throwable cause = t; // FIXME?
        // Throwable cause = t.getCause();

        if( cause instanceof CannotViewException.NoSubject ) {
            throw new ExceptionWithHttpStatusCode( cause, StatusCodes.NOT_FOUND ); // 404

        } else if( cause instanceof MeshObjectIdentifierNamespaceNotFoundParseException ) {
            throw new ExceptionWithHttpStatusCode( cause, StatusCodes.NOT_FOUND ); // 404

        } else if( cause instanceof NotPermittedException ) {
            throw new ExceptionWithHttpStatusCode( cause, StatusCodes.FORBIDDEN ); // 402

        } else {
            throw new ExceptionWithHttpStatusCode( cause, StatusCodes.BAD_REQUEST ); // 400
        }
    }

    /**
     * Handle exceptions thrown when attempting to create a Viewlet. This method is
     * factored out so subclasses can easily override.
     *
     * @param t the thrown exception
     * @throws ExceptionWithHttpStatusCode
     */
    protected void handleViewletFactoryException(
            Throwable t )
        throws
            ExceptionWithHttpStatusCode
    {
        if( t instanceof NoViewletFoundException ) {
            throw new ExceptionWithHttpStatusCode( t, StatusCodes.FORBIDDEN ); // 402

        } else {
            throw new ExceptionWithHttpStatusCode( t, StatusCodes.BAD_REQUEST ); // 400
        }
    }

    private static final HttpString LOCATION_HEADER = new HttpString( "Location" );
}
