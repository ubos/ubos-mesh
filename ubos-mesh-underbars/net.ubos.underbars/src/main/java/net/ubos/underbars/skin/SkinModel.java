//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.skin;

import java.util.HashMap;
import java.util.Map;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.web.HttpRequest;
import net.ubos.web.vl.StructuredResponse;

/**
 * The object being passed to a Handlebars resources implementing a Skin.
 */
public class SkinModel
    extends
        HandlebarsModel
{
    /**
     * Factory method.
     * 
     * @param response the StructuredResponse 
     * @return the created SkinContext instance
     */
    public static SkinModel create(
            StructuredResponse response )
    {
        HttpRequest request = response.getRequest();

        Map<String,Object> data = new HashMap<>();

        data.put( URL_KEY,     request.getUrl().getAbsoluteFullUri() );
        data.put( CONTEXT_KEY, request.getUrl().getContextPath() );

        data.put( STRUCTURED_RESPONSE_KEY, response );

        SkinModel ret = new SkinModel( data, null );

        return ret;
    }

    /**
     * Private constructor, use factory method.
     *
     * @param data the locally kept data for the model
     * @param delegate optional delegate to consult if a local value does not exist
     */
    protected SkinModel(
            Map<String,Object> data,
            HandlebarsModel    delegate )
    {
        super( data, delegate );
    }
}
