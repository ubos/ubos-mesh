//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.nonblessed;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.nonblessed.NonblessedCsvUtils;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Enumerate all names of all attributes used in the CSV row objects contained by this
 * CSV spreadsheet object.
 */
public class CsvColumnNamesHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        Set<String> attributeNames = new HashSet<>();

        MeshObject obj = getPrimaryMeshObject( model, options );
        String []  ret = NonblessedCsvUtils.determineColumnNames( obj );
        Arrays.sort( ret );
        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
