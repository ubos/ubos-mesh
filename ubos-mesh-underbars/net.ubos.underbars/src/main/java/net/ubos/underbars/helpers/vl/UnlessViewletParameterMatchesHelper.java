//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

/**
 * Emit contained content if the value of a ViewletAttribute does not match the
 * provided value.
 */
public class UnlessViewletParameterMatchesHelper
    extends
        AbstractViewletParameterMatchesHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean evaluate(
            Object [] viewletParameterValues,
            Object    comparison )
    {
        return !compare( viewletParameterValues, comparison );
    }
}
