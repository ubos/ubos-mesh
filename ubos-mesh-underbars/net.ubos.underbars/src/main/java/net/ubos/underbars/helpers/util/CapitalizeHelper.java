//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Capitalizes a string in various ways.
 */
public class CapitalizeHelper
    extends
        AbstractHandlebarsHelper<String>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            String  model,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String text = model;
        String type = options.hash( TYPE_ARGNAME, "" );

        String ret = StringHelper.findCapitalizationOrDefault( type.toUpperCase() ).capitalize( text );

        return ret;
    }

    /**
     * Name of the argument indicating the type of capitalization.
     */
    public static final String TYPE_ARGNAME = "type";

    /**
     * Value for TYPE_ARGNAME that indicates all lowercase.
     * So "hail to the Chief of ABC, today and tomorrow" becomes "hail to the chief of abc, today and tomorrow".
     */
    public static final String TYPE_ARGNAME_LOWERCASE = "lower";

    /**
     * Value for TYPE_ARGNAME that indicates all uppercase.
     * So "hail to the Chief of ABC, today and tomorrow" becomes "HAIL TO THE CHIEF OF ABC, TODAY AND TOMORROW".
     */
    public static final String TYPE_ARGNAME_UPPERCASE = "upper";

    /**
     * Value for TYPE_ARGNAME that indicates to capitalize the first letter, leave
     * everything else unchanged.
     * So "hail to the Chief of ABC, today and tomorrow" becomes "hail to the Chief of ABC, today and tomorrow".
     */
    public static final String TYPE_ARGNAME_VERYFIRST = "veryfirst";

    /**
     * Value for TYPE_ARGNAME that indicates to capitalize the first letter of each word, leave
     * everything else unchanged.
     * So "hail to the Chief of ABC, today and tomorrow" becomes "Hail To The Chief Of ABC, Today And Tomorrow".
     */
    public static final String TYPE_ARGNAME_FIRST = "first";

    /**
     * Value for TYPE_ARGNAME that indicates proper English headline capitalization should be
     * attempted.
     * So "hail to the Chief of ABC, today and tomorrow" becomes "Hail to the Chief of ABC, Today and Tomorrow".
     */
    public static final String TYPE_ARGNAME_HEADLINE = "headline";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        TYPE_ARGNAME
    };
}
