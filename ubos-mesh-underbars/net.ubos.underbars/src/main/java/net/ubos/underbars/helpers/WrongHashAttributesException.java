//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers;

import com.github.jknack.handlebars.Options;
import java.util.Arrays;

/**
 * Thrown if the provided arguments to an AbstractHandlebarsHelper are not
 * compatible with the list of mandatory and optional arguments that the
 * Helper understands.
 *
 * Note: Not sure how to make this work without "raw type" warnings
 */
public class WrongHashAttributesException
    extends
        RuntimeException
{
    /**
     * Constructor.
     *
     * @param options the Options object that contains the provided arguments
     * @param mandatoryArgNames the names of the arguments that the Helper requires
     * @param optionalArgNames the names of the arguments that the Helper additionally understands
     * @param helperClass the class of the Helper where this happened
     */
    public WrongHashAttributesException(
            Options                                   options,
            String []                                 mandatoryArgNames,
            String []                                 optionalArgNames,
            Class<? extends AbstractHandlebarsHelper> helperClass )
    {
        this( options, mandatoryArgNames, optionalArgNames, null, helperClass );
    }

    /**
     * Constructor.
     *
     * @param options the Options object that contains the provided arguments
     * @param mandatoryArgNames the names of the arguments that the Helper requires
     * @param optionalArgNames the names of the arguments that the Helper additionally understands
     * @param message an optional message
     * @param helperClass the class of the Helper where this happened
     */
    public WrongHashAttributesException(
            Options                                   options,
            String []                                 mandatoryArgNames,
            String []                                 optionalArgNames,
            String                                    message,
            Class<? extends AbstractHandlebarsHelper> helperClass )
    {
        theOptions           = options;
        theMandatoryArgNames = mandatoryArgNames;
        theOptionalArgNames  = optionalArgNames;
        theMessage           = message;
        theHelperClass       = helperClass;

        Arrays.sort( theMandatoryArgNames );
        Arrays.sort( theOptionalArgNames );

        theProvidedArgNames = new String[ options.hash.size() ];
        options.hash.keySet().toArray( theProvidedArgNames );
        Arrays.sort( theProvidedArgNames );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( "Invalid arguments were provided to Helper " ).append( theHelperClass.getCanonicalName()).append( ":\n" );
        buf.append( "required: " ).append( String.join( ", ", theMandatoryArgNames )).append( "\n" );
        buf.append( "optional: " ).append( String.join( ", ", theOptionalArgNames )).append( "\n" );
        buf.append( "provided: " ).append( String.join( ", ", theProvidedArgNames ));

        if( theMessage != null ) {
            buf.append( "\n" ).append( theMessage );
        }
        return buf.toString();
    }

    /**
     * The Options object that contains the provided arguments.
     */
    protected final Options theOptions;

    /**
     * The names of the arguments that the Helper requires.
     */
    protected final String [] theMandatoryArgNames;

    /**
     * The names of the arguments that the Helper additionally understands.
     */
    protected final String [] theOptionalArgNames;

    /**
     * The names of the arguments that were provided.
     */
    protected final String [] theProvidedArgNames;

    /**
     * An optional extra message.
     */
    protected final String theMessage;

    /**
     * The class of the Helper.
     */
    protected Class<? extends AbstractHandlebarsHelper> theHelperClass;
}
