//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.bracket;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * See comment in BracketHelper.
 */
public class NotIfContentHelper
    extends
        AbstractHandlebarsHelper<BracketHelper.Record>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            BracketHelper.Record context,
            Options              options )
        throws
            IOException
    {
        checkNoArguments( options );

        Context parentContext = options.context.parent();

        CharSequence content = options.apply( options.fn, parentContext );
        context.foundNotIfContent( content );
        
        return "";
    }
}
