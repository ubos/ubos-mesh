//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.meshobjectset;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Calculate a MeshObjectSet by intersecting two other MeshObjectSets.
 */
public class IntersectSetHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObjectSet one = options.hash( MESHOBJECTSET_ARGNAME );
        MeshObjectSet two = options.hash( MESHOBJECTSET2_ARGNAME );

        MeshObjectSet ret = one.intersection( two );
        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECTSET_ARGNAME,
        MESHOBJECTSET2_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
