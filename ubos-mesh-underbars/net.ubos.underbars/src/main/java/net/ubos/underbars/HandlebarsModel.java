//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars;

import java.util.Arrays;
import java.util.Map;

/**
 * Superclass of the Models we pass into Handlebars templates.
 */
public abstract class HandlebarsModel
{
    /**
     * Private constructor, for subclasses only
     *
     * @param data the locally kept data for the model
     * @param delegate optional delegate to consult if a local value does not exist
     */
    protected HandlebarsModel(
            Map<String,Object> data,
            HandlebarsModel    delegate )
    {
        theData     = data;
        theDelegate = delegate;
    }

    /**
     * Obtain a named value. If the name does not exist, throw an Exception.
     *
     * @param name the name
     * @return the value, or null
     */
    public Object get(
            String name )
    {
        Object ret;

        if( "size".equals( name )) {
            ret = theData.size();

        } else if( "empty".equals( name )) {
            ret = theData.isEmpty();

        } else if( theData.containsKey( name )) {
            ret = theData.get( name );

        } else if( theDelegate != null ) {
            ret = theDelegate.get( name );

        } else {
            ret = null; // must return null instead of throwing an exception
        }
        return ret;
    }
    
    /**
     * Obtain the keys in this model, sorted alphabetically.
     * 
     * @return the keys
     */
    public String [] getSortedKeys()
    {
        String [] ret = new String[ theData.size() + 2 ];
        theData.keySet().toArray( ret );
        ret[ ret.length - 2 ] = "size";
        ret[ ret.length - 1 ] = "empty";
        
        Arrays.sort( ret );
        return ret;
    }

    /**
     * Get the delegate HandlebarsModel, if any.
     * 
     * @return the delegate
     */
    public HandlebarsModel getDelegate()
    {
        return theDelegate;
    }

    /**
     * Local data. This is a separate member variable, instead of implementation inheritance, so we can figure
     * out delegation more cleanly
     */
    protected final Map<String,Object> theData;

    /**
     * Optional delegate.
     */
    protected final HandlebarsModel theDelegate;

    /**
     * Key into the model for the requested URL.
     */
    public static final String URL_KEY = "URL";

    /**
     * Key into the model for the app's context path.
     */
    public static final String CONTEXT_KEY = "CONTEXT";

    /**
     * Key into the model for the StructuredResponse currently being assembled.
     */
    public static final String STRUCTURED_RESPONSE_KEY = "sr";

    /**
     * Key into the model for the primary object currently processed.
     * This is usually the same as the Subject of the current Viewlet, but it
     * might be something else in case the Viewlet includes other tags.
     */
    public static final String MESHOBJECT_KEY = "meshObject";

    /**
     * Key into the model for the MeshBase currently being used.
     */
    public static final String MESHBASE_KEY = "meshBase";

    /**
     * Key into the model for the ModelBase singleton.
     */
    public static final String MODELBASE_KEY = "modelBase";

    /**
     * Key into the model for the StringRepresentationSchemeDirectory.
     */
    public static final String STRING_REPRESENTATION_SCHEME_DIRECTORY_KEY = "stringrepdir";
}
