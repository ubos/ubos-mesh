//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

/**
 * Emit contained content if the value of a ViewletAttribute matches the
 * provided value.
 */
public class IfViewletParameterMatchesHelper
    extends
        AbstractViewletParameterMatchesHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean evaluate(
            Object [] viewletParameterValues,
            Object    comparison )
    {
        return compare( viewletParameterValues, comparison );
    }
}
