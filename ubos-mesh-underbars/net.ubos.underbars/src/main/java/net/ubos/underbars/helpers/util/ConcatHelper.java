//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * Concatenates its arguments
 */
public class ConcatHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  context,
            Options options )
        throws
            IOException
    {
        StringBuilder buf = new StringBuilder();
        buf.append( context );
        for( Object param : options.params ) {
            buf.append( param );
        }
        return buf.toString();
    }
}
