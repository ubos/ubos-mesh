//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.underbars.resolvers.HandlebarsModelResolver;
import net.ubos.underbars.resolvers.StructuredResponseValueResolver;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.logging.Log;

/**
 * Insert a fragment.
 *
 * {{util:fragment fragment="foo/bar/myfragment.hbs" a="b" c="d" }}
 */
public class FragmentHelper
    extends
        AbstractInsertTemplateHelper
{
    private static final Log log = Log.getLogInstance( FragmentHelper.class ); // our own, private logger

    /**
     * Constructor.
     */
    public FragmentHelper() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkMandatoryArguments( options, MANDATORY_ARGNAMES ); // any others may be passed through to the fragment

        String fragment = options.hash( FRAGMENT_ARGNAME );

        // pass on the parameters
        Map<String,Object> data = new HashMap<>();
        data.putAll( options.hash );

        ViewletModel subModel = model.createSubmodel( data );

        Context c = Context.newBuilder( subModel )
                .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON )
                .build();

        String content = processTemplate( fragment, c );

        return content;
    }

    /**
     * Name of the argument indicating the name of the fragment we are inserting.
     */
    public static final String FRAGMENT_ARGNAME = "fragment";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        FRAGMENT_ARGNAME
    };
}
