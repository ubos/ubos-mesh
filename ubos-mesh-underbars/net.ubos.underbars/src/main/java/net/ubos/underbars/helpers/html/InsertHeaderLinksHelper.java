//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.html;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import net.ubos.underbars.skin.SkinModel;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * Insert the HTML link tags that have been accumulated in the StructuredResponse.
 */
public class InsertHeaderLinksHelper
    extends
        AbstractHandlebarsHelper<SkinModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            SkinModel context,
            Options   options )
        throws
            IOException
    {
        checkNoArguments( options );

        StructuredResponse      sr  = (StructuredResponse) context.get( SkinModel.STRUCTURED_RESPONSE_KEY );
        Map<String,Set<String>> map = sr.getHtmlHeaderLinks();
        if( map != null ) {
            StringBuilder buf        = new StringBuilder( map.size() * 20 );
            String        appContext = sr.getRequest().getUrl().getContextPath();

            for( Map.Entry<String,Set<String>> entry : map.entrySet()) {
                for( String href : entry.getValue() ) {
                    buf.append( "  <link rel=\"" )
                       .append( entry.getKey() )
                       .append( "\" href=\"" )
                       .append( appContext )
                       .append( href )
                       .append( "\">\n" );
                }
            }
            return buf.toString();
            
        } else {
            return "";
        }
    }
}
