//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;
import net.ubos.vl.Viewlet;

/**
 * Emits the value of a Viewlet Parameter. If the Viewlet Parameters has multiple
 * values, they are joined with the separator.
 */
public class ViewletParameterHelper
    extends
        AbstractHandlebarsHelper<Viewlet>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Viewlet model,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );
        checkParams( options, 1, 1 );
        
        String viewletAttributeName = (String) options.param( 0 );
        String separator             = options.hash( SEPARATOR_ARGNAME, DEFAULT_SEPARATOR_ARG_VALUE );

        Options.Buffer buffer = options.buffer();

        Object [] viewletAttributeValue = model.getViewedMeshObjects().getMultivaluedViewletParameter( viewletAttributeName );
        if( viewletAttributeValue != null && viewletAttributeValue.length > 0 ) {
            String sep = "";
            for( Object current : viewletAttributeValue ) {
                buffer.append( sep );
                buffer.append( String.valueOf( current ));
                sep = separator;
            }
        }
        
        return buffer;
    }
    
    /**
     * Name of the argument that indicates the separator for multiple values.
     */
    public static final String SEPARATOR_ARGNAME = "separator";

    /**
     * Default value for the separator.
     */
    public static final String DEFAULT_SEPARATOR_ARG_VALUE = ", ";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        SEPARATOR_ARGNAME
    };
}
