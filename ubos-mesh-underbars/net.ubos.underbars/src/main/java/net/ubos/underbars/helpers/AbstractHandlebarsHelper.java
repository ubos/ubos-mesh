//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StringHelper;

/**
 * Collects functionality common to Handlebars Helpers.
 *
 * @param <T> type of context object
 */
public abstract class AbstractHandlebarsHelper<T>
    implements
        Helper<T>
{
    /**
     * Determine the CSS class used by elements created by this AbstractHandlebarsHelper.
     *
     * @return the CSS class
     */
    public String getCssClass()
    {
        return getClass().getName().replace( '.', '-' );
    }

    /**
     * Check that no hash parameters are there.
     *
     * @param options the Options object that contains the provided arguments
     * @throws WrongHashAttributesException thrown if the provided arguments are incompatible
     *         with the mandatory and/or optional ones
     */
    protected void checkNoArguments(
            Options options )
        throws
            WrongHashAttributesException
    {
        if( !options.hash.isEmpty() ) {
            throw new WrongHashAttributesException( options, StringHelper.EMPTY_ARRAY, StringHelper.EMPTY_ARRAY, getClass() );
        }
    }

    /**
     * Check that the required hash parameters are indeed there, and
     * that no other arguments have been passed in than what we understand.
     *
     * @param options the Options object that contains the provided arguments
     * @param mandatoryArgNames names of the arguments that are mandatory
     * @param optionalArgNames names of the arguments that are optional
     * @throws WrongHashAttributesException thrown if the provided arguments are incompatible
     *         with the mandatory and/or optional ones
     */
    protected void checkArguments(
            Options   options,
            String [] mandatoryArgNames,
            String [] optionalArgNames )
        throws
            WrongHashAttributesException
    {
        for( String arg : mandatoryArgNames ) {
            if( !options.hash.containsKey( arg )) {
                throw new WrongHashAttributesException( options, mandatoryArgNames, optionalArgNames, getClass() );
            }
        }

        for( String arg : options.hash.keySet() ) {
            if(    !ArrayHelper.isIn( arg, mandatoryArgNames, true )
                && !ArrayHelper.isIn( arg, optionalArgNames,  true ))
            {
                throw new WrongHashAttributesException( options, mandatoryArgNames, optionalArgNames, getClass() );
            }
        }
    }

    /**
     * Check that the required hash parameters are indeed there, but ignore all others.
     *
     * @param options the Options object that contains the provided arguments
     * @param mandatoryArgNames names of the arguments that are mandatory
     * @throws WrongHashAttributesException thrown if the provided arguments are incompatible
     *         with the mandatory ones
     */
    protected void checkMandatoryArguments(
            Options   options,
            String [] mandatoryArgNames )
        throws
            WrongHashAttributesException
    {
        for( String arg : mandatoryArgNames ) {
            if( !options.hash.containsKey( arg )) {
                throw new WrongHashAttributesException( options, mandatoryArgNames, StringHelper.EMPTY_ARRAY, getClass() );
            }
        }
    }

    /**
     * Check that the right number of positional params are there.
     *
     * @param options the Options object that contains the provided params
     * @param minAllowed the minimum number of allowed params
     * @param maxAllowed the maximum number of allowed params
     * @throws WrongParamsException thrown if the provided number of params was different from the correct number
     */
    protected void checkParams(
            Options options,
            int     minAllowed,
            int     maxAllowed )
        throws
            WrongParamsException
    {
        if( options.params.length < minAllowed || options.params.length > maxAllowed ) {
            throw new WrongParamsException( options, minAllowed, maxAllowed, getClass() );
        }
    }

    /**
     * Obtain the value of an optional hash attribute.
     *
     * @param name name of the attribute
     * @param defaultValue value to use if the attribute cannot be found.
     * @param options the Handlebars Helper Options
     * @return value
     * @param <T> type of the value
     */
    public <T> T optionalHash(
            String  name,
            T       defaultValue,
            Options options )
    {
        T ret = options.hash( name, defaultValue );
        return ret;
    }

//    /**
//     * Factor out how we process a loop. For use by subclasses.
//     *
//     * @param loopVar name of the loop variable
//     * @param values data to iterate over
//     * @param model the current Handlebars model
//     * @param options the Handlebars Options
//     * @throws IOException thrown if an I/O error occurred
//     */
//    protected void loopThrough(
//            String    loopVar,
//            Object [] values,
//            Object    model,
//            Options   options )
//        throws
//            IOException
//    {
//        int max = options.hash( MAX_ARGNAME, values.length );
//        if( max > values.length ) {
//            max = values.length;
//        }
//
//        for( int i=0 ; i<max ; ++i ) {
//            Context childContext = Context.newContext( options.context, model );
//
//            // similar to what Handlebar's EachHelper does
//            boolean even = i % 2 == 0;
//            childContext
//                    .combine( "@key",   i )
//                    .combine( "@index", i )
//                    .combine( "@first", i == 0               ? "first" : "" )
//                    .combine( "@last",  i == values.length-1 ? "last"  : "" )
//                    .combine( "@odd",   even ? ""     : "odd" )
//                    .combine( "@even",  even ? "even" : ""    )
//                    // 1-based index
//                    .combine( "@index_1", i + 1 )
//                    .combine( loopVar, values[i] );
//
//            options.buffer().append( options.apply( options.fn, childContext ));
//        }
//    }
//
//    /**
//     * Factor out how we process a loop. For use by subclasses.
//     *
//     * @param loopVar1 name of the loop variable looping through values1
//     * @param values1 first data elements to iterate over
//     * @param loopVar2 name of the loop variable looping through values2
//     * @param values2 second data elements to iterate over
//     * @param model the current Handlebars model
//     * @param options the Handlebars Options
//     * @throws IOException thrown if an I/O error occurred
//     */
//    protected void loopThrough(
//            String    loopVar1,
//            Object [] values1,
//            String    loopVar2,
//            Object [] values2,
//            Object    model,
//            Options   options )
//        throws
//            IOException
//    {
//        for( int i=0 ; i<values1.length ; ++i ) {
//            Context childContext = Context.newContext( options.context, model );
//
//            // similar to what Handlebar's EachHelper does
//            boolean even = i % 2 == 0;
//            childContext
//                    .combine( "@key",   i )
//                    .combine( "@index", i )
//                    .combine( "@first", i == 0              ? "first" : "" )
//                    .combine( "@last",  i == values1.length ? "last"  : "" )
//                    .combine( "@odd",   even ? ""     : "odd" )
//                    .combine( "@even",  even ? "even" : ""    )
//                    // 1-based index
//                    .combine( "@index_1", i + 1 )
//                    .combine( loopVar1, values1[i] )
//                    .combine( loopVar2, values2[i] );
//
//            options.buffer().append( options.apply( options.fn, childContext ));
//        }
//    }
//
//    /**
//     * If no loop variable is set in an iterating Helper, use this one as a default.
//     */
//    public static final String DEFAULT_LOOP_VAR_VALUE = "current";

    /**
     * Name of the argument on Helpers that indicates the name of the
     * StringRepresentation's flavor to use to format an object.
     */
    public static final String FLAVOR_ARGNAME = "flavor";

    /**
     * Name of the argument on Helpers that indicates the name of the
     * StringRepresentation to use to format an object.
     */
    public static final String STRING_REPRESENTATION_ARGNAME = "stringRepresentation";

    /**
     * Name of the default StringRepresentation that becomes the value for
     * the FLAVOR_ARGNAME if no other is provided.
     */
    public static final String STRING_REPRESENTATION_ARG_DEFAULT = "Html";

    /**
     * Name of the argument on Helpers that indicates if a required parameter is null,
     * do not emit an error and emit this argument instead.
     */
    public static final String NULL_CONTENT_ARGNAME = "nullContent";

    /**
     * Name of the argument on Helpers that specifies the maxmimum number of objects to return..
     */
    public static final String MAX_ARGNAME = "max";

    /**
     * Name of the argument indicating the CSS tag to use.
     */
    public static final String CSS_CLASS_ARGNAME = "cssClass";
}
