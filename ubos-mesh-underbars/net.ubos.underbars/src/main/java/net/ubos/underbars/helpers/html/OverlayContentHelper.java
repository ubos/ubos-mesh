//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.html;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.underbars.helpers.util.AbstractInsertTemplateHelper;
import net.ubos.underbars.resolvers.HandlebarsModelResolver;
import net.ubos.underbars.resolvers.StructuredResponseValueResolver;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ResourceHelper;
import net.ubos.web.Url;

/**
 * Define the content of an HTML overlay.
 */
public class OverlayContentHelper
    extends
        AbstractInsertTemplateHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkMandatoryArguments( options, MANDATORY_ARGNAMES ); // any others may be passed through to the fragment

        String     cssClass    = options.hash( CSS_CLASS_ARGNAME, null );
        String     action      = options.hash( ACTION_ARGNAME, null );
        String     submitLabel = options.hash( SUBMIT_LABEL_ARGNAME, null );
        String     fragment    = options.hash( FRAGMENT_ARGNAME );
        boolean    activated   = options.hash( ACTIVATED_ARGNAME, false );

        // pass on the parameters
        Map<String,Object> data = new HashMap<>();
        data.putAll( options.hash );

        ViewletModel subModel = model.createSubmodel( data );

        Context c = Context.newBuilder( subModel )
                .push( HandlebarsModelResolver.SINGLETON, StructuredResponseValueResolver.SINGLETON )
                .build();

        String fragmentContent = processTemplate( fragment, c );

        CharSequence content = options.fn();
        String       domId   = model.getCurrentOverlayId();

        Options.Buffer buffer = options.buffer();

        buffer.append( "<div class=\"" );
        buffer.append( getCssClass() );
        if( cssClass != null && cssClass.length() > 0 ) {
            buffer.append( " " );
            buffer.append( cssClass );
        }
        buffer.append( "\" id=\"" ).append( domId ).append( "\"" );
        buffer.append( ">\n" );

        if( action != null ) {
            buffer.append( " <form action=\"" )
                  .append( action )
                  .append( "\" method=\"POST\" enctype=\"multipart/form-data\" accept-charset=\"UTF-8\">\n" );
        }

        if( content.length() > 0 ) {
            buffer.append( "  <div class=\"overlay-content\">\n" );
            buffer.append( content );
            buffer.append( "  </div>\n" );
        }
        if( fragmentContent != null ) {
            buffer.append( "  <div class=\"overlay-content\">\n" );
            buffer.append( fragmentContent );
            buffer.append( "  </div>\n" );
        }

        if( action != null ) {
            buffer.append( "  <div class=\"overlay-buttons\">\n" );
            buffer.append( "   <table>\n" );
            buffer.append( "    <tr>\n" );
            buffer.append( "     <td><a class=\"cancel\" href=\"javascript:html_overlay_hide( '" )
                  .append( domId )
                  .append( "' )\">Cancel</a></td>\n" );
            buffer.append( "     <td><input type=\"submit\" class=\"submit\" value=\"" );
            if( submitLabel != null ) {
                buffer.append( submitLabel );
            } else {
                buffer.append( DEFAULT_SUBMIT_LABEL );
            }
            buffer.append( "\"></td>\n" );
            buffer.append( "    </tr>\n" );
            buffer.append( "   </table>\n" );
            buffer.append( "  </div>\n" );

            buffer.append( " </form>\n" );
        }
        buffer.append( "</div>" );

        if( activated ) {
            buffer.append( "<script>\n" );
            buffer.append( "    html_overlay_show( '" ).append( domId ).append( "', {} );\n" );
            buffer.append( "</script>\n" );
        }
        return buffer;
    }

    /**
     * Default label for the submit button.
     */
    public static final String DEFAULT_SUBMIT_LABEL
            = ResourceHelper.getInstance(OverlayContentHelper.class )
            .getResourceStringOrDefault( "DefaultSubmitLabel", "Submit" );

    /**
     * Name of the argument indicating the Url to which to submit the form.
     */
    public static final String ACTION_ARGNAME = "action";

    /**
     * Name of the argument indicating the label on the submit button.
     */
    public static final String SUBMIT_LABEL_ARGNAME = "submitLabel";

    /**
     * Name of the argument indicating the .hbs file to use as a template for the content.
     * Without the ".hbs" extension.
     */
    public static final String FRAGMENT_ARGNAME = "fragment";

    /**
     * Name of the argument indicating whether to activate it already.
     */
    public static final String ACTIVATED_ARGNAME = "activated";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        FRAGMENT_ARGNAME
    };
}
