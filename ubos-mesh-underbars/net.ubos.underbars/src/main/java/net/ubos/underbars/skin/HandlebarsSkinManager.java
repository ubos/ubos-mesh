//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.skin;

import com.github.jknack.handlebars.EscapingStrategy;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.io.TemplateSource;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.templatesource.FileTemplateSource;
import net.ubos.util.nameserver.AbstractMNameServer;
import net.ubos.web.skin.Skin;
import net.ubos.web.skin.SkinManager;
import net.ubos.web.skin.SkinMap;
import net.ubos.web.skin.SkinType;

/**
 * Maintains the set of known Skins.
 */
public class HandlebarsSkinManager
    extends
        AbstractMNameServer<SkinType,SkinMap>
    implements
        SkinManager
{
    /**
     * Factory method.
     *
     * @return the created SkinMap instance
     */
    public static HandlebarsSkinManager create()
    {
        HandlebarsSkinManager ret = new HandlebarsSkinManager();
        return ret;
    }

    /**
     * Private constructor, use factory method.
     */
    protected HandlebarsSkinManager()
    {}

    /**
     * Look for skins in this directory, and add them to the SkinManager.
     * This assumes the directory contains one directory for each skin,
     * which is named skin.hbs.
     *
     * This does not compile the Skins.
     *
     * @param dir the directory containing the skins
     */
    public void addSkinsInDir(
            File dir )
    {
        if( !dir.exists() ) {
            return;
        }
        if( !dir.isDirectory() ) {
            throw new IllegalArgumentException( "Not a directory: " + dir.getAbsolutePath() );
        }
        Handlebars hb = Underbars.getHandlebars().with( EscapingStrategy.NOOP );

        File [] skinDirs = dir.listFiles( REAL_DIRS_ONLY_FILTER );

        for( File skinDir : skinDirs ) {
            String  skinName     = skinDir.getName();
            File [] skinTypeDirs = skinDir.listFiles( REAL_DIRS_ONLY_FILTER );

            for( File skinTypeDir : skinTypeDirs ) {
                SkinType skinType = skinTypeFromDirectoryName( skinTypeDir.getName() );
                if( skinType == null ) {
                    // don't know about this one
                    continue;
                }

                File [] hbsFiles = skinTypeDir.listFiles( HBS_FILTER );

                File mainSkinFile = null;
                for( File hbsFile : hbsFiles ) {
                    if( hbsFile.getName().equals( SKIN_FILENAME )) {
                        mainSkinFile = hbsFile;
                        break;
                    }
                }
                if( mainSkinFile == null ) {
                    // not a complete skin -- skip
                    continue;
                }

                TemplateSource             mainSkinTemplateSource     = FileTemplateSource.create( mainSkinFile );
                Map<String,TemplateSource> partialSkinTemplateSources = new HashMap<>();

                for( File hbsFile : hbsFiles ) {
                    if( hbsFile.equals( mainSkinFile )) {
                        continue;
                    }
                    String partialName = hbsFile.getName();
                    partialName = partialName.substring( 0, partialName.length() - HBS_EXT.length() );

                    partialSkinTemplateSources.put( partialName, FileTemplateSource.create( hbsFile.getAbsolutePath() ));
                }

                HandlebarsSkin skin = HandlebarsSkin.create( mainSkinTemplateSource, skinType, partialSkinTemplateSources, hb );

                SkinMap skinMap = get( skinType );
                if( skinMap == null ) {
                    skinMap = new DefaultSkinMap();
                    put( skinType, skinMap );
                }
                skinMap.put( skinName, skin );
            }
        }
    }

    /**
     * Add the skins from another SkinManager.
     */
    public void addSkinsFrom(
            SkinManager other )
    {
        for( SkinType type : other.keyIterator()) {
            SkinMap delegateMap = other.get( type );
            SkinMap myMap       = theLocalMappings.get( type );

            if( myMap == null ) {
                myMap = new DefaultSkinMap();
                theLocalMappings.put( type, myMap );
            }

            for( String skinName : delegateMap.keyIterator() ) {
                Skin skin = delegateMap.get( skinName );

                myMap.put( skinName, skin );
            }
        }
    }

    /**
     * Test compile all skins.
     *
     * @throws IOException a problem occurred compiling a Skin
     */
    public void testCompile()
         throws
            IOException
    {
        for( SkinMap s : valueIterator()) {
            DefaultSkinMap realSkinMap = (DefaultSkinMap)s;

            realSkinMap.testCompile();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SkinMap getFallbackSkinMap()
    {
        return get( SkinType.DEFAULT_2D );
    }

    /**
     * Obtain the Skin Type from the name of the directory in which it is contained.
     *
     * @param dirName name of the directory
     * @return SkinType
     */
    public static SkinType skinTypeFromDirectoryName(
            String dirName )
    {
        int    lastSlash = dirName.lastIndexOf( '/' );
        String name;

        if( lastSlash > 0 ) {
            name = dirName.substring( lastSlash+1 );
        } else {
            name = dirName;
        }
        name = name.toUpperCase();

        SkinType ret = SkinType.valueOf( name );
        return ret;
    }

    /**
     * The Handlebars extension.
     */
    public static final String HBS_EXT = ".hbs";

    /**
     * A FileFilter that returns only real subdirectories.
     */
    private static final FileFilter REAL_DIRS_ONLY_FILTER
            = ( File candidate )
                    -> candidate.isDirectory() && !candidate.getName().equals( "." ) && !candidate.getName().equals( ".." );

    /**
     * A FileFilter that returns only .hbs files.
     */
    private static final FileFilter HBS_FILTER
            = ( File candidate )
                    -> candidate.isFile() && candidate.getName().endsWith( HBS_EXT );

    /**
     * Name of the file that indicates a skin.
     */
    public static final String SKIN_FILENAME = "skin" + HBS_EXT;
}
