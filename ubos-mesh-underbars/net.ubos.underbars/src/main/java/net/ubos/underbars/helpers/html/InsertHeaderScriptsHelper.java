//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.html;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.Set;
import net.ubos.underbars.skin.SkinModel;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * Insert the HTML script tags that have been accumulated in the StructuredResponse.
 */
public class InsertHeaderScriptsHelper
    extends
        AbstractHandlebarsHelper<SkinModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            SkinModel context,
            Options   options )
        throws
            IOException
    {
        checkNoArguments( options );

        StructuredResponse sr   = (StructuredResponse) context.get( SkinModel.STRUCTURED_RESPONSE_KEY );
        Set<String>        list = sr.getHtmlHeaderScriptSources();
        if( list != null ) {
            StringBuilder buf        = new StringBuilder( list.size() * 20 );
            String        appContext = sr.getRequest().getUrl().getContextPath();
            for( String src : list ) {
                buf.append( "  <script src=\"" )
                   .append( appContext )
                   .append( src )
                   .append( "\"></script>\n" );
            }
            return buf.toString();
            
        } else {
            return "";
        }
    }
}
