//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.html;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.web.vl.StructuredResponse;

/**
 * Insert the HTML needed to define an overlay.
 */
public class OverlayHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        boolean disabled      = options.hash( DISABLED_ARGNAME, false );
        String  title         = options.hash( TITLE_ARGNAME );
        String  label         = options.hash( LABEL_ARGNAME );
        String  labelCssClass = options.hash( LABEL_CSS_CLASS_ARGNAME );

        String  domId    = model.obtainNewDomId();

        if( title == null ) {
            title = label;
        }

        try {
            model.setCurrentOverlayId( domId );

            Options.Buffer buffer = options.buffer();

            if( disabled ) {
                buffer.append( "<span class=\"html-overlay-disabled\">" );
            } else {
                buffer.append( "<a class=\"button overlay\" href=\"javascript:html_overlay_show( '" + domId + "', {} )\"" );
                buffer.append( " title=\"" ).append( title ).append( "\"" );
                buffer.append( ">" );
            }
            if( labelCssClass != null ) {
                buffer.append( "<span class=\"" + labelCssClass + "\">" );
                buffer.append( "<span>" );
            }
            buffer.append( label );
            if( labelCssClass != null ) {
                buffer.append( "</span>" );
                buffer.append( "</span>" );
            }

            if( disabled ) {
                buffer.append( "</span>" );
            } else {
                buffer.append( "</a>" );
            }

            buffer.append( options.fn() ); // keep this out of the <a>

            StructuredResponse sr = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
            sr.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars/OverlayHelper.css" );
            sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars/OverlayHelper.js" );

            return buffer;

        } finally {
            model.unsetCurrentOverlayId();
        }
    }

    /**
     * Name of the argument indicating whether this overlay has been disabled.
     */
    public static final String DISABLED_ARGNAME = "disabled";

    /**
     * Name of the argument indicating the title for this overlay.
     */
    public static final String TITLE_ARGNAME = "title";
    
    /**
     * Name of the argument indicating the label for this overlay.
     */
    public static final String LABEL_ARGNAME = "label";
    
    /**
     * Name of the argument indicating the CSS class to be used for the span enclosing the label.
     */
    public static final String LABEL_CSS_CLASS_ARGNAME = "labelCssClass";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        LABEL_ARGNAME
    };
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DISABLED_ARGNAME,
        TITLE_ARGNAME,
        LABEL_CSS_CLASS_ARGNAME
    };
}
