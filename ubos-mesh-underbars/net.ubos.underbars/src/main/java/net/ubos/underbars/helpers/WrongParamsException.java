//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers;

import com.github.jknack.handlebars.Options;

/**
 * Thrown if the provided params to an AbstractHandlebarsHelper are not
 * compatible with the the number of correct arguments that the
 * Helper understands.
 */
public class WrongParamsException
    extends
        RuntimeException
{
    /**
     * Constructor.
     * 
     * @param options the Options object that contains the provided params
     * @param minAllowed the minimum number of allowed params
     * @param maxAllowed the maximum number of allowed params
     * @param helperClass the class of the Helper where this happened
     */
    public WrongParamsException(
            Options                                   options,
            int                                       minAllowed,
            int                                       maxAllowed,
            Class<? extends AbstractHandlebarsHelper> helperClass )
    {
        theOptions     = options;
        theMinAllowed  = minAllowed;
        theMaxAllowed  = maxAllowed;
        theHelperClass = helperClass;
    }
 
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( "Invalid params were provided to Helper " ).append( theHelperClass.getCanonicalName()).append( ":\n" );
        buf.append( "required: from " ).append( theMinAllowed ).append( " to " ).append( theMaxAllowed ).append( "\n" );
        buf.append( "provided: " ).append( theOptions.params.length );
        
        return buf.toString();
    }
    
    /**
     * The Options object that contains the provided arguments.
     */
    protected final Options theOptions;
    
    /**
     * The minimum allowed number of params.
     */
    protected final int theMinAllowed;
    
    /**
     * The maximum allowed number of params.
     */
    protected final int theMaxAllowed;
    
    /**
     * The class of the Helper.
     */
    protected Class<? extends AbstractHandlebarsHelper> theHelperClass;
}
