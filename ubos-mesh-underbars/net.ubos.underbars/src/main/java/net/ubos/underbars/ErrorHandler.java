//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 *
 */
public class ErrorHandler
    implements
        HttpHandler
{
    public ErrorHandler(
            final HttpHandler next )
    {
        theNext = next;
    }

    @Override
    public void handleRequest(final HttpServerExchange ex) throws Exception {
        ex.addDefaultResponseListener(exchange -> {
            if (!exchange.isResponseChannelAvailable()) {
                return false;
            }
            final int code = exchange.getStatusCode();
            if (code == 401) {
                exchange.getResponseSender().send( "Error: 401" );
                return true;
            } else if (code == 403) {
                exchange.getResponseSender().send( "Error: 403" );
                return true;
            } else if (code == 500) {
                exchange.getResponseSender().send( "Error: 500" );
                return true;
            }
            return false;
        });
        theNext.handleRequest(ex);
    }

    /**
     * The next handler to invoke.
     */
    private final HttpHandler theNext;
}
