//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.web.vl.StructuredResponse;

/**
 * A Helper that wraps the HTML when editing Attributes.
 */
public class AttributeEditWrapperHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String shellvar         = options.hash( SHELL_VAR_ARGNAME );
        String shellpropvar     = options.hash( SHELL_PROP_VAR_ARGNAME );
        String neighborshellvar = options.hash( NEIGHBOR_SHELL_VAR_ARGNAME );
        String an               = options.hash( ATTRIBUTE_NAME_ARGNAME );
        String av               = options.hash( ATTRIBUTE_VALUE_ARGNAME );

        String cssPrefix = "net-ubos-underbars-AttributeEditWrapperHelper";
        String jsPrefix  = "net_ubos_underbars_AttributeEditWrapperHelper";

        // This is used both for Attributes and RoleAttributes
        String neighborAddOn;
        if( neighborshellvar == null ) {
            neighborAddOn = "";
        } else {
            neighborAddOn = ".to." + neighborshellvar;
        }

        Options.Buffer buffer = options.buffer();

        buffer.append( "<div class=\"")
              .append( cssPrefix )
              .append( "\" id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( "\">\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.value\" class=\"" )
              .append( cssPrefix )
              .append( "-value\">\n" );

        CharSequence content = options.fn();
        buffer.append( content );

        buffer.append( " </div>\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.remove\" class=\"" )
              .append( cssPrefix )
              .append( "-remove\">\n" );

        buffer.append( "  <a href=\"javascript:" )
              .append( jsPrefix )
              .append( "_doRemove( 'shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "', '" )
              .append( shellpropvar )
              .append( "' )\">Remove</a>\n" );

        buffer.append( " </div>\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.null\" class=\"" )
              .append( cssPrefix )
              .append( "-null\">\n" );

        buffer.append( "  <input id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( ".attributevalue." )
              .append( shellpropvar )
              .append( ".null\" name=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( ".attributevalue." )
              .append( shellpropvar )
              .append( ".null\" value=\"false\">\n" );

        buffer.append( " </div>\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.create\" class=\"" )
              .append( cssPrefix )
              .append( "-create\">\n" );

        buffer.append( "  <a href=\"javascript:" )
              .append( jsPrefix )
              .append( "_doCreate( 'shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "', '" )
              .append( shellpropvar )
              .append( "' )\">Create</a>\n" );

        buffer.append( " </div>\n" );        
        buffer.append( "</div>\n" );

        buffer.append( "<script>\n" );

        buffer.append( jsPrefix )
              .append( "_initAttribute_value( \"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "\", \"" )
              .append( shellpropvar )
              .append( "\", " )
              .append( av == null ? "true" : "false" )
              .append( " );\n" );

        buffer.append( "</script>\n" );
        
        StructuredResponse sr = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
        sr.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars/AttributeEditWrapperHelper.css" );
        sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars/AttributeEditWrapperHelper.js" );

        return buffer;
    }

    /**
     * Name of the argument indicating the HttpShell variable for the primary MeshObject, e.g. "v1".
     */
    public static final String SHELL_VAR_ARGNAME = "shellvar";
    
    /**
     * Name of the argument indicating the HttpShell Property variable, e.g. "1".
     */
    public static final String SHELL_PROP_VAR_ARGNAME = "shellpropvar";

    /**
     * Name of the argument indicating the HttpShell variable for the neighbor MeshObject, e.g. "v2".
     */
    public static final String NEIGHBOR_SHELL_VAR_ARGNAME = "neighborshellvar";

    /**
     * Name of the argument indicating the name of the Attribute being edited.
     */
    public static final String ATTRIBUTE_NAME_ARGNAME = "an";
    
    /**
     * Name of the argument indicating the current value of the Attribute.
     */
    public static final String ATTRIBUTE_VALUE_ARGNAME = "av";
    

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        SHELL_VAR_ARGNAME,
        SHELL_PROP_VAR_ARGNAME,
        ATTRIBUTE_NAME_ARGNAME,
        ATTRIBUTE_VALUE_ARGNAME
    };
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        NEIGHBOR_SHELL_VAR_ARGNAME
    };
}
