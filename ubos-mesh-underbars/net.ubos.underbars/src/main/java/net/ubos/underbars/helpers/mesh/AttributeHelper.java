//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.io.Serializable;
import net.ubos.mesh.IllegalAttributeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Inserts the value of an Attribute.
 */
public class AttributeHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        String attributeName = options.hash( ATTRIBUTE_NAME_ARGNAME );

        try {
            Serializable value = obj.getAttributeValue( attributeName );

            String text = formatAttribute(
                    obj,
                    attributeName,
                    value,
                    model,
                    options );

            return text;

        } catch( IllegalAttributeException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( NotPermittedException ex ) {
            throw new IllegalArgumentException( ex );
        }
    }

    /**
     * Name of the parameter indicating the PropertyType.
     */
    public static final String ATTRIBUTE_NAME_ARGNAME = "attributeName";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        ATTRIBUTE_NAME_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DEFAULT_VALUE_ARGNAME,
        FLAVOR_ARGNAME,
        MAX_LENGTH_ARGNAME,
        STRING_REPRESENTATION_ARGNAME
    };
}
