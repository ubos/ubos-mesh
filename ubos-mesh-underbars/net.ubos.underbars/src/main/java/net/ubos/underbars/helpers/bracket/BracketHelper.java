//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.bracket;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * Collects at least one ContentHelper, and one or more IfContentHelper or NotIfContentHelper.
 * This will emit the content of the ContentHelper and the IfContentHelper in the specified
 * sequence, if the content in the ifContentHelper is not just whitespace. If theere is no
 * content in the ContentHelper, it will emit the content in the NotIfContentHelper instead.
 */
public class BracketHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  context,
            Options options )
        throws
            IOException
    {
        checkNoArguments( options );

        Record  myRecord     = new Record();
        Context childContext = Context.newContext( options.context, myRecord );

        options.apply( options.fn, childContext );
        
        Options.Buffer buffer = options.buffer();
        if( myRecord.theHaveContent ) {
            buffer.append( myRecord.theIfContent );
        } else {
            buffer.append( myRecord.theNotIfContent );
        }
        return buffer;
    }
    
    /**
     * Stores the information provided by our child tags.
     */
    public static class Record
    {
        /**
         * The Content child tag notifies us of more content.
         * 
         * @param content the found content
         */
        public void foundContent(
                CharSequence content )
        {
            if( theIfContent == null ) {
                theIfContent = new StringBuilder();
            }
            theIfContent.append( content );
            theHaveContent = true;
        }

        /**
         * The IfContent child tag notifies us of more content.
         * 
         * @param content the found content
         */
        public void foundIfContent(
                CharSequence content )
        {
            if( theIfContent == null ) {
                theIfContent = new StringBuilder();
            }
            theIfContent.append( content );
        }

        /**
         * The NotIfContent child tag notifies us of more content.
         * 
         * @param content the found content
         */
        public void foundNotIfContent(
                CharSequence content )
        {
            if( theNotIfContent == null ) {
                theNotIfContent = new StringBuilder();
            }
            theNotIfContent.append( content );
        }

        /**
         * Aggregate content in the "if" case here.
         */
        protected StringBuilder theIfContent;

        /**
         * Aggregate content in the "not if" case here.
         */
        protected StringBuilder theNotIfContent;

        /**
         * Set to true as soon as we definitely have content from the ContentTag
         */
        protected boolean theHaveContent = false; 
    }
}