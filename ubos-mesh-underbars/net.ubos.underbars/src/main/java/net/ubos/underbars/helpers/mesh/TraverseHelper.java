//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.model.traverse.TraverseTranslatorException;
import net.ubos.underbars.helpers.HelpersRuntimeException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Calculate a MeshObjectSet by traversing from a MeshObject by means of a TraversalSpecification.
 */
public class TraverseHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        try {
            TraverseSpecification traversalSpec = getTraverseSpecification( model, options, obj, TRAVERSE_ARGNAME );
            MeshObjectSet         ret           = obj.traverse( traversalSpec );

            return ret;

        } catch( TraverseTranslatorException ex ) {
            throw new HelpersRuntimeException( ex );
        }
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        TRAVERSE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
