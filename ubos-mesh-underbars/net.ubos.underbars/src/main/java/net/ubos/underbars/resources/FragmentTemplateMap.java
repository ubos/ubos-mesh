//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resources;

import com.github.jknack.handlebars.EscapingStrategy;
import com.github.jknack.handlebars.Template;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.templatesource.FileTemplateSource;
import net.ubos.underbars.templatesource.FileTemplateSourceManager;
import net.ubos.util.cursoriterator.CompositeCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * A TemplateMap for fragments.
 */
public class FragmentTemplateMap
    extends
        TemplateMap
{
    /**
     * Factory method.
     *
     * @return the created instance
     */
    public static FragmentTemplateMap create()
    {
        FragmentTemplateMap ret = new FragmentTemplateMap();

        return ret;
    }

    /**
     * Private constructor, use factory method.
     */
    protected FragmentTemplateMap()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Template obtainFor(
            String key )
        throws
            IOException
    {
        FileTemplateSource source = theSourceMap.get( key );

        if( source != null ) {
            return Underbars.getHandlebars().with( EscapingStrategy.NOOP ).compile( source );
        }

        for( TemplateMap delegate : theDelegates ) {
            Template found = delegate.obtainFor( key );
            if( found != null ) {
                return found;
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> keyIterator()
    {
        if( theDelegates == null ) {
            return theSourceMap.keyIterator(); // this is easiest
        }

        List<CursorIterator<String>> delegates = new ArrayList<>( 1 + theDelegates.size() );
        delegates.add( theSourceMap.keyIterator() );

        for( TemplateMap delegate : theDelegates ) {
            delegates.add( delegate.keyIterator() );
        }

        CompositeCursorIterator<String> ret = CompositeCursorIterator.create( delegates );
        return ret;
    }

    /**
     * Look for Templates in this directory, and add them.
     *
     * @param dir the directory containing the Templates
     * @param allowedExtensions contains the allowed file extensions; will ignore everything else
     */
    public void addTemplatesInDir(
            File        dir,
            Set<String> allowedExtensions )
    {
        theSourceMap.addFileTemplatesInDir( dir, allowedExtensions );
    }

    /**
     * Add a delegate.
     *
     * @param delegate the new delegate
     */
    public void addDelegate(
            TemplateMap delegate )
    {
        theDelegates.add( delegate );
    }

    /**
     * The map of uncompiled sources.
     */
    protected final FileTemplateSourceManager theSourceMap = FileTemplateSourceManager.create();

    /**
     * The delegates.
     */
    protected final ArrayList<TemplateMap> theDelegates = new ArrayList<>();
}
