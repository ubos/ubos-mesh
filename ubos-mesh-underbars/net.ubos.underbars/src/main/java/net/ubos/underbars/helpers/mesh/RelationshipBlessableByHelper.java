//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.ArrayList;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.helpers.WrongHashAttributesException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.Pair;

/**
 * Lists the RoleTypes by which a MeshObject's side of a Relationship with another MeshObject
 * may be blessed.
 */
public class RelationshipBlessableByHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject primary  = getPrimaryMeshObject( model, options );
        MeshObject neighbor = getNeighborMeshObject( model, options );

        MeshBase mb = primary.getMeshBase();

        String primaryShellVarName  = options.hash( MESHOBJECT_SHELL_VAR_NAME_ARGNAME );
        String neighborShellVarName = options.hash(NEIGHBOR_SHELL_VAR_NAME_ARGNAME );

        if( primary == null && primaryShellVarName == null ) {
            throw new WrongHashAttributesException(
                    options,
                    MANDATORY_ARGNAMES,
                    OPTIONAL_ARGNAMES,
                    "Must specify at least " + MESHOBJECT_ARGNAME + " or " + MESHOBJECT_SHELL_VAR_NAME_ARGNAME,
                    getClass() );
        }

        if( neighbor == null && neighborShellVarName == null ) {
            throw new WrongHashAttributesException(
                    options,
                    MANDATORY_ARGNAMES,
                    OPTIONAL_ARGNAMES,
                    "Must specify at least " + NEIGHBOR_ARGNAME + " or " + NEIGHBOR_SHELL_VAR_NAME_ARGNAME,
                    getClass() );
        }

        SubjectArea [] sas = ModelBase.SINGLETON.getOrderedLoadedSubjectAreas();

        ArrayList<Pair<SubjectArea,ArrayList<RoleType>>> candidates = new ArrayList<>();

        for( SubjectArea sa : sas ) {
            ArrayList<RoleType> candidatesInSa = new ArrayList<>();

            RelationshipType [] relTypes = sa.getRelationshipTypes();

            for( RelationshipType relType : relTypes ) {
                if( relType.getIsAbstract().value() ) {
                    continue;
                }

                for( RoleType roleType : new RoleType[] { relType.getSource(), relType.getDestination() } ) {
                    RoleType invRoleType = roleType.getInverseRoleType();

                    if( primary != null ) {
                        if( roleType.getEntityType() != null ) {
                            if( !primary.isBlessedBy( roleType.getEntityType() )) {
                                continue;
                            }
                        }
                    }
                    if( neighbor != null ) {
                        if( invRoleType.getEntityType() != null ) {
                            if( !neighbor.isBlessedBy( invRoleType.getEntityType() )) {
                                continue;
                            }
                        }
                    }
                    candidatesInSa.add( roleType );
                }
            }
            if( !candidatesInSa.isEmpty() ) {
                candidates.add( new Pair<>( sa, candidatesInSa ));
            }
        }

        MeshTypeIdentifierSerializer shellTypeIdSerializer = model.getShellMeshTypeIdentifierSerializer();

        Options.Buffer buffer = options.buffer();

        if( primary != null && primaryShellVarName == null ) {
            String identifier = formatMeshObjectIdentifier( model, primary.getIdentifier(), options );

            primaryShellVarName = model.obtainNewShellVar();

            buffer.append( "<input type=\"hidden\" name=\"shell." );
            buffer.append( primaryShellVarName );
            buffer.append( "\" value=\"" );
            buffer.append( identifier );
            buffer.append( "\"/>\n" );
        }
        if( neighbor != null && neighborShellVarName == null ) {
            String identifier = formatMeshObjectIdentifier( model, neighbor.getIdentifier(), options );

            neighborShellVarName = model.obtainNewShellVar();

            buffer.append( "<input type=\"hidden\" name=\"shell." );
            buffer.append( neighborShellVarName );
            buffer.append( "\" value=\"" );
            buffer.append( identifier );
            buffer.append( "\"/>\n" );
        }

        if( candidates.isEmpty() ) {
            buffer.append( "<span class=\"notice\">This pair of MeshObjects cannot be blessed with any available Role Types.</span>\n" );

        } else {
            buffer.append( "<select name=\"shell." );
            buffer.append( primaryShellVarName );
            buffer.append (".to." );
            buffer.append( neighborShellVarName );
            buffer.append( ".blessRole\" class=\"" ).append( getCssClass() ).append( "\">\n" );

            for( Pair<SubjectArea,ArrayList<RoleType>> pair : candidates ) {
                SubjectArea sa = pair.getName();

                buffer.append( " <optgroup label=\"" );
                buffer.append( sa.getUserVisibleName().value() );
                buffer.append( "\">\n" );

                for( RoleType roleType : pair.getValue() ) {
                    buffer.append( " <option value=\"" );
                    buffer.append( shellTypeIdSerializer.toExternalForm( roleType.getIdentifier()));

                    if( primary != null && neighbor != null && primary.isRelated( roleType, neighbor ) ) {
                        buffer.append( "\" disabled=\"disabled" );
                    }
                    buffer.append( "\">" );
                    buffer.append( roleType.getUserVisibleName().value() );
                    buffer.append( "</option>\n" );
                }
                buffer.append( "</optgroup>\n" );
            }
            buffer.append( "</select>\n" );
        }

        return buffer;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        NEIGHBOR_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        MESHOBJECT_SHELL_VAR_NAME_ARGNAME,
        NEIGHBOR_SHELL_VAR_NAME_ARGNAME
    };
}
