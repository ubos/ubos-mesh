//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Template;
import java.io.IOException;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.FragmentTemplateMap;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.logging.Log;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * Factors out how to process a named template.
 */
public abstract class AbstractInsertTemplateHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    private static final Log log = Log.getLogInstance( AbstractInsertTemplateHelper.class ); // our own, private logger

    /**
     * Find and process a template, return the result.
     * 
     * @param templateName name of the template
     * @param c the Context to use
     * @return the result, or null if an error occurred
     */    
    protected String processTemplate(
            String  templateName,
            Context c )
    {
        try {
            FragmentTemplateMap map      = Underbars.getFragmentTemplateManager();
            Template                template = map.obtainFor( templateName );

            if( template == null ) {
                StringBuilder msg = new StringBuilder();
                msg.append( "Template instantiation error in FragmentHelper: no template " );
                msg.append( templateName );

                log.error( msg ); // details to the log, only high-level in HTML

                StringBuilder buf = new StringBuilder();
                buf.append( "<div class=\"handlebars-error\">\n" );
                buf.append( " <p>Helper template missing: " ).append( templateName ).append( "</p>\n" );
                buf.append( "</div>\n" );

                return buf.toString();
            }

            String result = template.apply( c );

            return result;

        } catch( IOException ex ) {
            // happens if the Handlebars template was changed, with syntax errors, after initial deployment

            log.error( ex ); // details to the log, only high-level in HTML

            StringBuilder buf = new StringBuilder();
            buf.append( "<div class=\"handlebars-error\">\n" );
            buf.append( " <p>Helper error: " ).append( templateName ).append( "</p>\n" );
            buf.append( "</div>\n" );

            return buf.toString();
        }
    }
}
