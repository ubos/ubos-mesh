//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;
import net.ubos.web.vl.StructuredResponse;

/**
 * A Helper that wraps the HTML when editing optional PropertyTypes.
 */
public class OptionalPropertyEditWrapperHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String        shellvar         = options.hash( SHELL_VAR_ARGNAME );
        String        shellpropvar     = options.hash( SHELL_PROP_VAR_ARGNAME );
        String        neighborshellvar = options.hash( NEIGHBOR_SHELL_VAR_ARGNAME );
        PropertyType  pt               = options.hash( PROPERTY_TYPE_ARGNAME );
        PropertyValue pv               = options.hash( PROPERTY_VALUE_ARGNAME );

        String cssPrefix = "net-ubos-underbars-OptionalPropertyEditWrapperHelper";
        String jsPrefix  = "net_ubos_underbars_OptionalPropertyEditWrapperHelper";

        // This is used both for Properties and RoleProperties
        String neighborAddOn;
        if( neighborshellvar == null ) {
            neighborAddOn = "";
        } else {
            neighborAddOn = ".to." + neighborshellvar;
        }

        Options.Buffer buffer = options.buffer();

        buffer.append( "<div class=\"")
              .append( cssPrefix )
              .append( "\" id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( "\">\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.value\" class=\"" )
              .append( cssPrefix )
              .append( "-value\">\n" );

        if( pv == null && pt.getDefaultValue() != null ) {
            // redefine pv to point to the default value

            Map<String,Object> overridden = new HashMap<>();
            overridden.put( "pv", pt.getDefaultValue() );

            ViewletModel childModel = model.createSubmodel( overridden );
            Context childContext = Context.newContext( options.context, childModel );

            options.apply( options.fn, childContext );

        } else {
            CharSequence content = options.fn();
            buffer.append( content );
        }

        buffer.append( " </div>\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.remove\" class=\"" )
              .append( cssPrefix )
              .append( "-remove\">\n" );

        buffer.append( "  <a href=\"javascript:" )
              .append( jsPrefix )
              .append( "_doRemove( 'shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "', '" )
              .append( shellpropvar )
              .append( "' )\">Remove</a>\n" );

        buffer.append( " </div>\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.null\" class=\"" )
              .append( cssPrefix )
              .append( "-null\">\n" );

        buffer.append( "  <input id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( ".propertyvalue." )
              .append( shellpropvar )
              .append( ".null\" name=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( ".propertyvalue." )
              .append( shellpropvar )
              .append( ".null\" value=\"false\">\n" );

        buffer.append( " </div>\n" );

        buffer.append( " <div id=\"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "." )
              .append( shellpropvar )
              .append( ".span.create\" class=\"" )
              .append( cssPrefix )
              .append( "-create\">\n" );

        buffer.append( "  <a href=\"javascript:" )
              .append( jsPrefix )
              .append( "_doCreate( 'shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "', '" )
              .append( shellpropvar )
              .append( "' )\">Create</a>\n" );

        buffer.append( " </div>\n" );        
        buffer.append( "</div>\n" );

        buffer.append( "<script>\n" );

        buffer.append( jsPrefix )
              .append( "_initProperty_value( \"shell." )
              .append( shellvar )
              .append( neighborAddOn )
              .append( "\", \"" )
              .append( shellpropvar )
              .append( "\", " )
              .append( pv == null ? "true" : "false" )
              .append( ", " )
              .append( String.valueOf( pt.getIsReadOnly().value() ))
              .append( " );\n" );

        buffer.append( "</script>\n" );
        
        StructuredResponse sr = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
        sr.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars/OptionalPropertyEditWrapperHelper.css" );
        sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars/OptionalPropertyEditWrapperHelper.js" );

        return buffer;
    }

    /**
     * Name of the argument indicating the HttpShell MeshObject variable, e.g. "v1".
     */
    public static final String SHELL_VAR_ARGNAME = "shellvar";
    
    /**
     * Name of the argument indicating the HttpShell Property variable, e.g. "1".
     */
    public static final String SHELL_PROP_VAR_ARGNAME = "shellpropvar";

    /**
     * Name of the argument indicating the HttpShell variable for the neighbor MeshObject, e.g. "v2".
     */
    public static final String NEIGHBOR_SHELL_VAR_ARGNAME = "neighborshellvar";

    /**
     * Name of the argument indicating the PropertyType being edited.
     */
    public static final String PROPERTY_TYPE_ARGNAME = "pt";
    
    /**
     * Name of the argument indicating the current PropertyValue.
     */
    public static final String PROPERTY_VALUE_ARGNAME = "pv";
    

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        SHELL_VAR_ARGNAME,
        SHELL_PROP_VAR_ARGNAME,
        PROPERTY_TYPE_ARGNAME,
        PROPERTY_VALUE_ARGNAME
    };
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        NEIGHBOR_SHELL_VAR_ARGNAME
    };
}
