//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Lets the user choose the EntityTypes from which a MeshObject may be unblessed.
 */
public class UnblessableFromHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        String  superType   = options.hash( SUPERTYPE_ARGNAME );
        String  subjectArea = options.hash( SUBJECTAREA_ARGNAME );

        EntityType  typeLimit;
        SubjectArea saLimit;

        try {
            if( superType != null ) {
                typeLimit = ModelBase.SINGLETON.findEntityType( superType );
            } else {
                typeLimit = null;
            }
            if( subjectArea != null ) {
                saLimit = ModelBase.SINGLETON.findSubjectArea( subjectArea );
            } else {
                saLimit = null;
            }
        } catch( ParseException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new IllegalArgumentException( ex );
        }

        if( typeLimit != null && saLimit != null ) {
            throw new IllegalArgumentException( "Limit either by SubjectArea or supertype, but not both" );
        }

        EntityType [] types = obj.getEntityTypes();

        MeshObjectIdentifierSerializer shellIdSerializer     = model.getShellMeshObjectIdentifierSerializer();
        MeshTypeIdentifierSerializer   shellTypeIdSerializer = model.getShellMeshTypeIdentifierSerializer();

        Options.Buffer buffer = options.buffer();

        String meshObjectShellVarName = model.obtainNewShellVar();

        buffer.append( "<input type=\"hidden\" name=\"shell." );
        buffer.append( meshObjectShellVarName );
        buffer.append( "\" value=\"" );
        buffer.append( shellIdSerializer.toExternalForm( obj.getIdentifier() ));
        buffer.append( "\"/>\n" );

        buffer.append( "<select name=\"shell." );
        buffer.append( meshObjectShellVarName );
        buffer.append( ".unbless\" class=\"" ).append( getCssClass() ).append( "\">\n" );

        boolean hasAtLeastOne = false;
        if( saLimit != null ) {
            // don't need to print optiongroups

            for( int i=0 ; i<types.length ; ++i ) {
                if( types[i].getIsAbstract().value()) {
                    continue;
                }
                if( types[i].getSubjectArea() != saLimit ) {
                    continue;
                }

                buffer.append( " <option value=\"" );
                buffer.append( shellTypeIdSerializer.toExternalForm( types[i].getIdentifier()));
                buffer.append( "\">" );
                buffer.append( types[i].getUserVisibleName().value() );
                buffer.append( "</option>\n" );

                hasAtLeastOne = true;
            }

        } else {
            // need to list all EntityTypes from the ModelBase, and select the ones that we currently filter by.
            // We'll try alphabetically

            SubjectArea [] sas = ModelBase.SINGLETON.getOrderedLoadedSubjectAreas();

            for( SubjectArea sa : sas ) {
                StringBuilder prefix = new StringBuilder();
                prefix.append( " <optgroup label=\"" );
                prefix.append( sa.getUserVisibleName().value() );
                prefix.append( "\">\n" );

                EntityType [] saTypes = sa.getEntityTypes();

                for( int i=0 ; i<saTypes.length ; ++i ) {
                    if( saTypes[i].getIsAbstract().value()) {
                        continue;
                    }
                    if( typeLimit != null && !typeLimit.equalsOrIsSupertype( saTypes[i] )) {
                        continue;
                    }

                    buffer.append( prefix );
                    prefix.setLength( 0 );

                    buffer.append( "  <option value=\"" );
                    buffer.append( shellTypeIdSerializer.toExternalForm( saTypes[i].getIdentifier()));
                    buffer.append( "\">" );
                    buffer.append( saTypes[i].getUserVisibleName().value() );
                    buffer.append( "</option>\n" );

                    hasAtLeastOne = true;
                }
                if( prefix.length() == 0 ) {
                    buffer.append( " </optgroup>\n" );
                }
            }
        }
        if( !hasAtLeastOne ) {
            buffer.append( " <optgroup label=\"" );
            buffer.append( "None available" );
            buffer.append( "\">\n" );
        }
        buffer.append( "</select>\n" );
        return buffer;
    }

    /**
     * Name of the argument that indicates which supertype should be used as a filter
     * for the emitted EntityTypes.
     */
    public static final String SUPERTYPE_ARGNAME = "supertype";

    /**
     * Name of the argument that indicates which SubjectArea should be used as a filter
     * for the emitted EntityTypes.
     */
    public static final String SUBJECTAREA_ARGNAME = "subjectarea";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        SUPERTYPE_ARGNAME,
        SUBJECTAREA_ARGNAME
    };
}
