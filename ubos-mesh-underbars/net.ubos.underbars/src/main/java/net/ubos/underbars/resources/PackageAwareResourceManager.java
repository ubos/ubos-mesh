//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resources;

import java.io.File;
import java.io.IOException;
import net.ubos.underbars.skin.HandlebarsSkinManager;
import net.ubos.underbars.templatesource.FileTemplateSourceManager;
import net.ubos.web.assets.DefaultAssetMap;
import net.ubos.web.util.PackageAware;
import net.ubos.web.assets.AssetMap;
import net.ubos.web.skin.SkinManager;

/**
 * This implementation of ResourceManager is aware of the conventions for where to place
 * non-source resources (assets, handlebar templates etc). It automatically scans the right
 * places,and has high-level methods to return what it found.
 */
public class PackageAwareResourceManager
    implements
        PackageAware,
        ResourceManager
{
    /**
     * Factory method. This does not attempt to compile the resources.
     *
     * @param ubosPackage the name of the UBOS package
     * @return the created instance
     */
    public static PackageAwareResourceManager createEmpty(
            String ubosPackage )
    {
        DefaultAssetMap        assetMap               = DefaultAssetMap.create();
        HandlebarsSkinManager  skinManager            = HandlebarsSkinManager.create();
        ViewletTemplateManager viewletTemplateManager = ViewletTemplateManager.create();
        FragmentTemplateMap    fragmentTemplateMap    = FragmentTemplateMap.create();

        // the first one wins, so we add those in reverse order
        for( int i=PackageAware.PACKAGE_RESOURCES_PARENT_DIRS.length-1 ; i>=0 ; --i ) {
            String parentDir = PackageAware.PACKAGE_RESOURCES_PARENT_DIRS[i];

            assetMap.addAssetsInDir(new File( parentDir + "/" + ubosPackage + "/assets" ),
                    DefaultAssetMap.WEB_ASSET_EXTENSIONS );

            skinManager.addSkinsInDir(
                    new File( parentDir + "/" + ubosPackage + "/skins" ) );

            viewletTemplateManager.addTemplatesInDir(
                    new File( parentDir + "/" + ubosPackage + "/viewlets" ) );

            fragmentTemplateMap.addTemplatesInDir(
                    new File( parentDir + "/" + ubosPackage + "/fragments" ),
                    FileTemplateSourceManager.TEMPLATE_EXTENSIONS );
        }

        return new PackageAwareResourceManager( ubosPackage, assetMap, skinManager, viewletTemplateManager, fragmentTemplateMap );
    }

    /**
     * Read and check all resources. This is a separate method so we can do this when all dependencies
     * have been set up, and a Handlebars template, for example, can find all referenced Helpers.
     *
     * @throws IOException an I/O error occurred
     */
    public void checkResources()
         throws
            IOException
    {
        if( thePackageResourcesParentDirOverride == null ) {
            // Don't test compile in override mode -- we are developing and this would be tedious
            theSkinManager.testCompile();
            theViewletTemplateManager.testCompile();
            theFragmentTemplateMap.testCompile();
        }
    }

    /**
     * Private constructor, use factory method.
     *
     * @param ubosPackage the name of the UBOS package
     * @param assetMap the DefaultAssetMap to use
     * @param skinManager the SkinM to use
     * @param viewletTemplateManager the ViewletTemplateManager to use
     * @param fragmentTemplateMap the FragmentTemplateMap to use
     */
    protected PackageAwareResourceManager(
            String                 ubosPackage,
            DefaultAssetMap        assetMap,
            HandlebarsSkinManager  skinManager,
            ViewletTemplateManager viewletTemplateManager,
            FragmentTemplateMap    fragmentTemplateMap )
    {
        theUbosPackage             = ubosPackage;
        theAssetMap                = assetMap;
        theSkinManager             = skinManager;
        theViewletTemplateManager  = viewletTemplateManager;
        theFragmentTemplateMap     = fragmentTemplateMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AssetMap getAssetMap()
    {
        return theAssetMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SkinManager getSkinManager()
    {
        return theSkinManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FragmentTemplateMap getFragmentTemplateMap()
    {
        return theFragmentTemplateMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TemplateMap getViewletTemplateMapFor(
            String viewletName )
    {
        return theViewletTemplateManager.get( viewletName );
    }

    /**
     * Name of the UBOS package for which this is the ResourceManager. For debugging.
     */
    protected final String theUbosPackage;

    /**
     * The asset map.
     */
    protected final DefaultAssetMap theAssetMap;

    /**
     * The skin map.
     */
    protected final HandlebarsSkinManager theSkinManager;

    /**
     * The fragment template map.
     */
    protected final FragmentTemplateMap theFragmentTemplateMap;

    /**
     * The TemplateMaps for Viewlets, keyed by Viewlet name.
     */
    protected final ViewletTemplateManager theViewletTemplateManager;
}
