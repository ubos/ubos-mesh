//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.underbars.resources.TemplateMap;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.DefaultWebViewedMeshObjects;
import net.ubos.web.vl.WebViewedMeshObjects;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * A Viewlet class that can impersonate any other Handlebars-based Viewlet emitting HTML,
 * as long as the Viewlet does not override or add any methods.
 */
public class DefaultHtmlHandlebarsViewlet
        extends
            AbstractHtmlHandlebarsViewlet
{
    /**
     * Factory method.
     *
     * @param viewletName the computable name of the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @return the created Viewlet
     */
    public static DefaultHtmlHandlebarsViewlet create(
            String                viewletName,
            TemplateMap           templateMap,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail )
    {
        DefaultWebViewedMeshObjects  viewed = new DefaultWebViewedMeshObjects( placement, dimensionality, detail );
        DefaultHtmlHandlebarsViewlet ret    = new DefaultHtmlHandlebarsViewlet( viewletName, templateMap, viewed );

        viewed.setViewlet( ret );

        return ret;
    }

    /**
     * Constructor. This is protected: use factory method or subclass.
     *
     * @param viewletName the computable name of the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param viewed the WebViewedMeshObjects to use
     */
    protected DefaultHtmlHandlebarsViewlet(
            String               viewletName,
            TemplateMap          templateMap,
            WebViewedMeshObjects viewed )
    {
        super( viewletName, templateMap, viewed );
    }
}
