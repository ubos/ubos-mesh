//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resources;

import com.github.jknack.handlebars.EscapingStrategy;
import com.github.jknack.handlebars.Template;
import java.io.IOException;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.templatesource.FileTemplateSource;
import net.ubos.underbars.templatesource.FileTemplateSourceMap;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * A TemplateMap for Viewlet Templates.
 */
public class ViewletTemplateMap
    extends
        TemplateMap
{
    /**
     * Factory method for a non-empty map.
     * This compiles all Templates so we fail fast upon boot.
     *
     * @param sourceMap the map containing the resources sources, tagged with section names
     * @return the created ViewletTemplateMap instance
     */
    public static ViewletTemplateMap create(
            FileTemplateSourceMap sourceMap )
    {
        ViewletTemplateMap ret = new ViewletTemplateMap( sourceMap );

        return ret;
    }

    /**
     * Constructor.
     *
     * @param sourceMap the map of uncompiled resource sources
     */
    protected ViewletTemplateMap(
            FileTemplateSourceMap sourceMap )
    {
        theSourceMap  = sourceMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Template obtainFor(
            String key )
        throws
            IOException
    {
        FileTemplateSource source = theSourceMap.get( key );

        if( source == null ) {
            throw new NullPointerException( "FileTemplateSource not known: " + key );
        }

        return Underbars.getHandlebars().with( EscapingStrategy.NOOP ).compile( source );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> keyIterator()
    {
        return theSourceMap.keyIterator();
    }

    /**
     * The map of uncompiled sources.
     */
    protected final FileTemplateSourceMap theSourceMap;
}
