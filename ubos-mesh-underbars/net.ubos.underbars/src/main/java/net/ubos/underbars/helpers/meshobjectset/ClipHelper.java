//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.meshobjectset;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StringHelper;

/**
 * Clip the MeshObjectSet to something smaller if needed
 */
public class ClipHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObjectSet one = options.hash( MESHOBJECTSET_ARGNAME );
        int           max = options.hash( MAX_ARGNAME );

        MeshObjectSet ret;
        if( one.size() > max ) {
            MeshObject [] clipped = ArrayHelper.copyIntoNewArray( one.getMeshObjects(), 0, max, MeshObject.class );
            ret = one.getFactory().createImmutableMeshObjectSet( clipped );
        } else {
            ret = one;
        }

        return ret;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECTSET_ARGNAME,
        MAX_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
