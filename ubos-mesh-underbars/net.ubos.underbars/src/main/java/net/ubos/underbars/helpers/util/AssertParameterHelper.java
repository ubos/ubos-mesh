//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;

/**
 * This helper checks that a certain parameter has been provided and it is valid.
 * It does not emit any output.
 */
public class AssertParameterHelper
    extends
        AbstractHandlebarsHelper<HandlebarsModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            HandlebarsModel model,
            Options         options )
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String name   = options.hash( NAME_ARGNAME );
        Boolean null_ = options.hash( NULL_ARGNAME );
        String type   = options.hash( TYPE_ARGNAME );

        if( name == null ) {
            throw new IllegalArgumentException( "Name attribute is missing" );
        }

        try {
            Object value = model.get( name ); // may throw HandlebarsModelNoKeyException

            if( value == null ) {
                if( null_ != null && !null_ ) {
                    throw new IllegalArgumentException( "Parameter \"" + name + "\" must not be null" );
                }
            } else if( type != null ) {
                Class clazz = model.getClass().getClassLoader().loadClass( type ); // is this a good ClassLoader?
                if( !clazz.isInstance( value )) {
                    throw new IllegalArgumentException( "Parameter \"" + name + "\" is not of type " + type );
                }
            }

            return "";
        } catch( ClassNotFoundException ex ) {
            throw new IllegalArgumentException( ex );
        }
    }

    /**
     * Name of the argument indicating the name of the parameter that is supposed to be checked.
     */
    public static final String NAME_ARGNAME = "name";

    /**
     * Name of the argument indicating whether the parameter is allowed to have a value of null.
     */
    public static final String NULL_ARGNAME = "null";

    /**
     * Name of the argument indicating what the permitted type of the parameter is.
     */
    public static final String TYPE_ARGNAME = "type";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        NAME_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        NULL_ARGNAME,
        TYPE_ARGNAME
    };
}
