//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.resolvers;

import com.github.jknack.handlebars.ValueResolver;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Silly Java Calendar API does not understand JavaBeans conventions, so we need
 * to write a custom resolver.
 */
public class CalendarValueResolver
    implements
        ValueResolver
{
    /**
     * Singleton.
     */
    public static final CalendarValueResolver SINGLETON = new CalendarValueResolver();

    /**
     * Private constructor, singleton only
     */
    private CalendarValueResolver()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Object resolve(
            Object context,
            String name )
    {
        Object ret = null;
        if( context instanceof Calendar ) {
            Calendar cal = (Calendar) context;
            
            switch( name ) {
                case YEAR:
                    ret = cal.get( Calendar.YEAR );
                    break;

                case MONTH:
                    ret = cal.get( Calendar.MONTH ) + 1;
                    break;
                
                case DAY:
                    ret = cal.get( Calendar.DAY_OF_MONTH );
                    break;
                
                case HOUR:
                    ret = cal.get( Calendar.HOUR_OF_DAY );
                    break;
                
                case MINUTE:
                    ret = cal.get( Calendar.MINUTE );
                    break;
                
                case SECOND:
                    ret = cal.get( Calendar.SECOND ) + ((double) cal.get( Calendar.MILLISECOND ))/1000;
                    break;
                    
                case TIMEZONE:
                    ret = cal.getTimeZone().getDisplayName();
                    break;

            }
        }
        return ret == null ? UNRESOLVED : ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object resolve(
            Object context )
    {
        if( context instanceof Calendar ) {
            return context;
        }
        return UNRESOLVED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Map.Entry<String, Object>> propertySet(
            Object context )
    {
        Set<Map.Entry<String, Object>> ret = null;

        if( context instanceof Calendar ) {
            Calendar cal = (Calendar) context;
            Map<String,Object> map = new HashMap<>();

            map.put( YEAR,     cal.get( Calendar.YEAR ));
            map.put( MONTH,    cal.get( Calendar.MONTH ));
            map.put( DAY,      cal.get( Calendar.DAY_OF_MONTH ));
            map.put( HOUR,     cal.get( Calendar.HOUR_OF_DAY ));
            map.put( MINUTE,   cal.get( Calendar.MINUTE ));
            map.put( SECOND,   cal.get( Calendar.SECOND ) + ((double) cal.get( Calendar.MILLISECOND ))/1000);
            map.put( TIMEZONE, cal.getTimeZone().getID() );

            ret = map.entrySet();
        }
        return ret;
    }
    
    /**
     * Names of the fields we can resolve.
     */
    public static final String YEAR     = "year";
    public static final String MONTH    = "month";
    public static final String DAY      = "day";
    public static final String HOUR     = "hour";
    public static final String MINUTE   = "minute";
    public static final String SECOND   = "second";
    public static final String TIMEZONE = "timezone";
}
