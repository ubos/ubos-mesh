//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.text;

import java.io.Serializable;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.text.AbstractStringRepresentationScheme;
import net.ubos.mesh.text.StringRepresentationScheme;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.util.ResourceHelper;
import net.ubos.util.text.StringifierParameters;

/**
 * Plain-String String Representation
 */
public class PlainStringRepresentationScheme
    extends
        AbstractStringRepresentationScheme
{
    /**
     * Factory method.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param idSerializer use this to serialize MeshObjectIdentifiers
     * @param typeIdSerializer use this to serialize MeshTypeIdentifiers
     * @return the created instance
     */
    public static PlainStringRepresentationScheme create(
            String                         prefix,
            MeshObjectIdentifierSerializer idSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer )
    {
        return new PlainStringRepresentationScheme( prefix, idSerializer, typeIdSerializer, null );
    }

    /**
     * Factory method with a delegate.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param idSerializer use this to serialize MeshObjectIdentifiers
     * @param typeIdSerializer use this to serialize MeshTypeIdentifiers
     * @param delegate the StringRepresentationScheme to delegate to if no entry was found
     * @return the created instance
     */
    public static PlainStringRepresentationScheme create(
            String                         prefix,
            MeshObjectIdentifierSerializer idSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer,
            StringRepresentationScheme     delegate )
    {
        return new PlainStringRepresentationScheme( prefix, idSerializer, typeIdSerializer, delegate );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param idSerializer use this to serialize MeshObjectIdentifiers
     * @param typeIdSerializer use this to serialize MeshTypeIdentifiers
     * @param delegate the StringRepresentationScheme to delegate to if no entry was found
     */
    protected PlainStringRepresentationScheme(
            String                         prefix,
            MeshObjectIdentifierSerializer idSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer,
            StringRepresentationScheme     delegate )
    {
        super( prefix, idSerializer, typeIdSerializer, delegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshObjectIdentifier(
            MeshObjectIdentifier  id,
            StringifierParameters pars )
    {
        String asString = theIdSerializer.toExternalForm( id );

        return escapeToContentFormat( asString );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshTypeIdentifier(
            MeshTypeIdentifier    id,
            StringifierParameters pars )
    {
        String asString = theTypeIdSerializer.toExternalForm( id );

        return escapeToContentFormat( asString );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshObjectLinkStart(
            MeshObject            obj,
            StringifierParameters pars )
    {
        return "";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshObjectLinkEnd(
            MeshObject            obj,
            StringifierParameters pars )
    {
        return "";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatAttribute(
            MeshObject            obj,
            String                attributeName,
            Serializable          currentValue,
            StringifierParameters pars )
    {
        if( currentValue instanceof CharSequence ) {
            return currentValue.toString();
        }
        return theResourceHelper.getResourceString( "NonStringAttributeValue" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatRoleAttribute(
            MeshObject            obj,
            MeshObject            neighbor,
            String                attributeName,
            Serializable          currentValue,
            StringifierParameters pars )
    {
        if( currentValue instanceof CharSequence ) {
            return currentValue.toString();
        }
        return theResourceHelper.getResourceString( "NonStringAttributeValue" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable parseAttributeValue(
            String                raw,
            String                attributeName,
            StringifierParameters pars )
        throws
            ParseException
    {
        if( theResourceHelper.getResourceString( "NonStringAttributeValue" ).equals( raw )) {
            throw new ParseException( raw, 0 );
        }
        return raw;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatProperty(
            MeshObject            obj,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars )
    {
        String ret;
        if( currentValue == null ) {
            ret = theResourceHelper.getResourceString( "NullValue" );

        } else {
            ret = currentValue.getUserVisibleString();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatFutureProperty(
            String                objVar,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars )
    {
        String ret;
        if( currentValue == null ) {
            ret = theResourceHelper.getResourceString( "NullValue" );

        } else {
            ret = currentValue.getUserVisibleString();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatRoleProperty(
            MeshObject            obj,
            MeshObject            neighbor,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars )
    {
        String ret;
        if( currentValue == null ) {
            ret = theResourceHelper.getResourceString( "NullValue" );

        } else {
            ret = currentValue.getUserVisibleString();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escapeToContentFormat(
            String raw )
    {
        return raw;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String unescapeFromContentFormat(
            String raw )
    {
        return raw;
    }

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( PlainStringRepresentationScheme.class );
}
