//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * A simple and incomplete reverse Polish notation calculator.
 */
public class RpnHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Number apply(
            Object  t,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        Deque<Double> stack = new ArrayDeque<>();
        operate( stack, t );

        for( Object param : options.params ) {
            operate( stack, param );
        }

        Number ret;
        if( stack.isEmpty() ) {
            ret = null;
        } else {
            ret = stack.pop();
        }
        return ret;
    }

    /**
     * Performs a single operation.
     *
     * @param stack the Stack to operate on
     * @param arg the argument to apply
     */
    protected void operate(
            Deque<Double> stack,
            Object        arg )
    {
        if( arg instanceof String ) {
            operateString( stack, (String) arg );
        } else if( arg instanceof Number ) {
            operateNumber( stack, ((Number) arg).doubleValue() );
        } else {
            throw new ClassCastException( "Cannot operate on: " + arg );
        }
    }

    /**
     * Performs a single operation.
     *
     * @param stack the Stack to operate on
     * @param arg the argument to apply
     */
    protected void operateString(
            Deque<Double> stack,
            String        arg )
    {
        Double a;
        Double b;

        switch( arg ) {
            case "+":
                b = stack.pop();
                a = stack.pop();
                stack.push( a + b );
                break;

            case "-":
                b = stack.pop();
                a = stack.pop();
                stack.push( a - b );
                break;

            case "*":
                b = stack.pop();
                a = stack.pop();
                stack.push( a * b );
                break;

            case "/":
                b = stack.pop();
                a = stack.pop();
                stack.push( a / b );
                break;

            default:
                operateNumber( stack, Double.parseDouble( arg ));
                break;
        }
    }

    /**
     * Performs a single operation.
     *
     * @param stack the Stack to operate on
     * @param arg the argument to apply
     */
    protected void operateNumber(
            Deque<Double> stack,
            Double        arg )
    {
        stack.push( arg );
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
