//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.paginate;

import net.ubos.mesh.MeshObject;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.web.vl.PaginatingWebViewlet;

/**
 * A go-to-the-next page button for Viewlets that paginate.
 */
public class GoForwardHelper
    extends
        AbstractPagingNavigationHelper
{
    private static final Log log = Log.getLogInstance( GoForwardHelper.class );

    /**
     * Constructor.
     */
    public GoForwardHelper() {}

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDestinationUrl(
            ViewletModel model )
    {
        try {
            @SuppressWarnings("unchecked")
            PaginatingWebViewlet<MeshObject> v = (PaginatingWebViewlet<MeshObject>) model.get( ViewletModel.VIEWLET_KEY );

            CursorIterator<MeshObject> start    = v.getCurrentPageStartIterator();
            int                        pageSize = v.getPageSize();

            if( start.hasNext( pageSize+1 )) {
                CursorIterator<MeshObject> startNext = start.createCopy();
                startNext.moveBy( pageSize );

                return v.getDestinationUrlForStart( startNext.next() );
            } else {
                return null;
            }

        } catch( CannotViewException ex ) {
            log.error( ex );
            return null;
        }
    }
}
