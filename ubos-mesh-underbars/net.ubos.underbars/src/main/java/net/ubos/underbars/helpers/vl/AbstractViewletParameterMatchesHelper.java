//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.vl;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;
import net.ubos.vl.Viewlet;

/**
 * Factors out functionality common to Helpers matching ViewletAttributes.
 */
public abstract class AbstractViewletParameterMatchesHelper
    extends
        AbstractHandlebarsHelper<Viewlet>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Viewlet model,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );
        checkParams( options, 2, 2 );

        String viewletAttributeName = (String) options.param( 0 );
        Object comparison           = options.param( 1 );

        Object [] viewletAttributeValue = model.getViewedMeshObjects().getMultivaluedViewletParameter( viewletAttributeName );
        Options.Buffer buffer = options.buffer();

        if( comparison != null && evaluate( viewletAttributeValue, comparison )) {
            buffer.append( options.fn() );
        }

        return buffer;
    }
    
    /**
     * Evaluate the predicate. If true, emit the contained content.
     * 
     * @param viewletParameterValues the multi-values of the ViewletAttribute to be compared
     * @param comparison the value to compare against
     * @return true if the content of the Helper shall be emitted
     */
    protected abstract boolean evaluate(
            Object [] viewletParameterValues,
            Object    comparison );

    /**
     * Perform the actual comparison.
     * 
     * @param viewletParameterValues the multi-values of the ViewletAttribute to be compared
     * @param comparison the value to compare against
     * @return true if one of the viewletParameterValues equals the comparison
     */
    protected boolean compare(
            Object [] viewletParameterValues,
            Object comparison )
    {
        if( viewletParameterValues == null || viewletParameterValues.length == 0 ) {
            return comparison == null;
        }

        if( comparison == null ) {
            return false;
        }
        
        for( Object current : viewletParameterValues ) {
            if( comparison == current ) {
                return true;
            }
            if( comparison.equals( current )) {
                return true;
            }
        }
        return false;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
