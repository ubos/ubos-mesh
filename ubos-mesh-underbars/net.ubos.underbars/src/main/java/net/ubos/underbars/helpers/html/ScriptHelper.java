//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.html;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Add an HTML script to the StructuredResponse.
 */
public class ScriptHelper
    extends
        AbstractHandlebarsHelper<ViewletModel>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel context,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String src = options.hash( SRC_ARGNAME );

        StructuredResponse sr = (StructuredResponse) context.get( ViewletModel.STRUCTURED_RESPONSE_KEY );
        sr.ensureHtmlHeaderScriptSource( src );

        return "";
    }

    /**
     * Name of the argument that indicates the src.
     */
    public static final String SRC_ARGNAME = "src";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        SRC_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
