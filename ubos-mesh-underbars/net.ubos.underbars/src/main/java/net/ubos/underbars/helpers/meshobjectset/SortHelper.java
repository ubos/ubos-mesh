//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.meshobjectset;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.set.ByAttributeValueSorter;
import net.ubos.mesh.set.ByPropertyValueSorter;
import net.ubos.mesh.set.DefaultMeshObjectSorter;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.MeshObjectSorter;
import net.ubos.model.primitives.PropertyType;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.underbars.helpers.WrongHashAttributesException;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Sort the provided MeshObjectSet.
 */
public class SortHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObjectSet set     = options.hash( MESHOBJECTSET_ARGNAME );
        boolean       reverse = options.hash( REVERSE_ARGNAME, false );

        Object  propertyTypeObject = optionalHash( SORT_BY_PROPERTY_TYPE_ARGNAME,  null, options );
        String  attributeName      = optionalHash( SORT_BY_ATTRIBUTE_NAME_ARGNAME, null, options );
        Boolean byId               = optionalHash( SORT_BY_IDENTIFIER_ARGNAME,     null, options );

        int count = 0;
        if( propertyTypeObject != null ) {
            ++count;
        }
        if( attributeName != null ) {
            ++count;
        }
        if( byId != null ) {
            ++count;
        }
        if( count != 1 ) {
            throw new WrongHashAttributesException(
                    options,
                    MANDATORY_ARGNAMES,
                    OPTIONAL_ARGNAMES,
                    String.format(
                            "Specify exacly one of %s, %s and %s.",
                            SORT_BY_PROPERTY_TYPE_ARGNAME,
                            SORT_BY_ATTRIBUTE_NAME_ARGNAME,
                            SORT_BY_IDENTIFIER_ARGNAME ),
                    getClass() );
        }

        MeshObjectSorter sorter;
        if( propertyTypeObject != null ) {
            try {
                PropertyType pt = determinePropertyType( model, options, SORT_BY_PROPERTY_TYPE_ARGNAME, null );

                sorter = ByPropertyValueSorter.create( pt, reverse );

            } catch( ParseException ex ) {
                throw new IllegalArgumentException( ex );

            } catch( MeshTypeNotFoundException ex ) {
                throw new IllegalArgumentException( ex );
            }
        } else if( attributeName != null ) {
            sorter = ByAttributeValueSorter.create( attributeName, reverse );

        } else {
            if( reverse ) {
               sorter = DefaultMeshObjectSorter.BY_REVERSE_IDENTIFIER;
            } else {
               sorter = DefaultMeshObjectSorter.BY_IDENTIFIER;
            }
        }
        MeshObjectSet ret = set.ordered( sorter );

        return ret;
    }


    /**
     * Name of the argument on Helpers that indicates the PropertyType
     * on whose value shall be sorted.
     */
    public static final String SORT_BY_PROPERTY_TYPE_ARGNAME = "sortByPropertyType";

    /**
     * Name of the argument on Helpers that indicates the name of the Attribute
     * on whose value shall be sorted.
     */
    public static final String SORT_BY_ATTRIBUTE_NAME_ARGNAME = "sortByAttribute";

    /**
     * Name of the argument on Helpers that indicates that the MeshObjectIdentifier
     * shall be used to sort.
     */
    public static final String SORT_BY_IDENTIFIER_ARGNAME = "sortByIdentifier";

    /**
     * Name of the argument on Helpers that can sort their result, indicating that the order
     * shall be reversed.
     */
    public static final String REVERSE_ARGNAME = "reverse";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECTSET_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        SORT_BY_PROPERTY_TYPE_ARGNAME,
        SORT_BY_ATTRIBUTE_NAME_ARGNAME,
        SORT_BY_IDENTIFIER_ARGNAME,
        REVERSE_ARGNAME
    };
}
