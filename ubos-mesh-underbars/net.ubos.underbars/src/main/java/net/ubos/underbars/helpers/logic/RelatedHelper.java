//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.logic;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.model.traverse.TraverseTranslatorException;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Determine whether two MeshObjects are related. If no TraversalSpecification is given,
 * we checked whether they are direct neighbors. If a TraversalSpecification is given,
 * we see whether we can find the second MeshObject by traversing from the first.
 */
public class RelatedHelper
    extends
        AbstractMeshObjectHelper
{
    @Override
    public Boolean apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj      = getPrimaryMeshObject(  model, options );
        MeshObject neighbor = getNeighborMeshObject( model, options );

        try {
            TraverseSpecification traverse = getDefaultTraverseSpecification( model, options, obj );

            boolean ret;
            if( traverse == null ) {
                ret = obj.isRelated( neighbor );
            } else {
                MeshObjectSet found = obj.traverse( traverse );
                ret = found.contains( neighbor );
            }
            return ret;

        } catch( TraverseTranslatorException ex ) {
            throw new IllegalArgumentException( ex );
        }
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        NEIGHBOR_ARGNAME,

    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        TRAVERSE_ARGNAME
    };
}
