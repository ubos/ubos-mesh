//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.util;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * If a value exists and is not empty, emit it with prefix and postfix. If empty,
 * do not emit anything.
 */
public class PrepostfixHelper
    extends
        AbstractHandlebarsHelper<String>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            String  t,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        if( t != null && !t.isEmpty() ) {        
            String prefix  = options.param( 0, "" );
            String postfix = options.param( 1, "" );
            
            if( options.hash( TRIM_ARGNAME, true )) {
                t = t.trim();
            }

            return transform( prefix ) + t + transform( postfix );
        }
        return null;
    }

    /**
     * Transform the String. Here we interpret \x as Java does.
     * 
     * @param in the String to be transformed
     * @return the transformed String
     */
    protected String transform(
            String in )
    {
        StringBuilder ret = new StringBuilder( in.length());

        boolean escapeOn = false;
        for( int i=0 ; i<in.length() ; ++i ) {
            char c = in.charAt( i );
            if( escapeOn ) {
                escapeOn = false;
                switch( c ) {
                    case 'n':
                        c ='\n';
                        break;
                    case 't':
                        c ='\t';
                        break;
                    case '\\':
                        c ='\\';
                        break;
                    default:
                        ret.append( '\\' ); // we leave it as is
                        break;
                }
                ret.append( c );
                
            } else if( c == '\\' ) {
                escapeOn = true;
            } else {
                ret.append( c );
            }
        }
        return ret.toString();
    }
    
    /**
     * Name of the argument indicating whether to trim the content. Default is true.
     */
    public static final String TRIM_ARGNAME = "trim";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;
    
    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        TRIM_ARGNAME
    };
}
