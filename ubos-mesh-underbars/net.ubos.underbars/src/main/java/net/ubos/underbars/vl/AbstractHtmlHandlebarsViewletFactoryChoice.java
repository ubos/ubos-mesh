//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl;

import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.util.ResourceHelper;
import net.ubos.vl.CannotViewException;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.AbstractWebViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Factored out for easier subclassing
 */
public abstract class AbstractHtmlHandlebarsViewletFactoryChoice
    extends
        AbstractWebViewletFactoryChoice
{
    /**
     * Constructor. This is private, so to subclass, use AbstractWebViewletFactoryChoice.
     *
     * @param viewletName name of the Viewlet for which this is a choice
     * @param templateMap the named Templates used for the Viewlet
     * @param resourceHelper the ResourceHelper to use
     * @param matchQuality the quality of the match
     */
    protected AbstractHtmlHandlebarsViewletFactoryChoice(
            String                viewletName,
            TemplateMap           templateMap,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceHelper        resourceHelper )
    {
        super( viewletName, SelectableMimeType.TEXT_HTML.getMimeType(), placement, dimensionality, detail, matchQuality, resourceHelper );

        theTemplateMap = templateMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HandlebarsViewlet instantiateViewlet()
        throws
            CannotViewException
    {
        return DefaultHtmlHandlebarsViewlet.create( theName, theTemplateMap, thePlacement, theDimensionality, theDetail );
    }

    /**
     * The templates for the Viewlet.
     */
    protected final TemplateMap theTemplateMap;
}
