//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.logic;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.EntityType;
import net.ubos.underbars.helpers.mesh.AbstractMeshObjectHelper;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Determine whether a MeshObject is blessed with a certain EntityType.
 */
public class BlessedWithHelper
    extends
        AbstractMeshObjectHelper
{
    @Override
    public Boolean apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        boolean permitSubtype = options.hash( PERMIT_SUBTYPE_ARGNAME, true );

        MeshObject obj  = getPrimaryMeshObject( model, options );
        EntityType type = (EntityType) getDefaultMeshType( model, options );

        boolean ret = obj.isBlessedBy( type, permitSubtype );
        return ret;
    }

    /**
     * Name of the boolean argument whether to permit a subtype or only
     * the exact specified EntityType.
     */
    public static final String PERMIT_SUBTYPE_ARGNAME = "permitSubtype";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        PERMIT_SUBTYPE_ARGNAME,
        MESHTYPE_ARGNAME,
        MESHTYPEID_ARGNAME
    };
}
