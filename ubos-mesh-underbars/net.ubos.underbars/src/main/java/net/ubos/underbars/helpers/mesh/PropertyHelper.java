//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.helpers.mesh;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.PropertyValueParsingException;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.underbars.vl.ViewletModel;

/**
 * Insert the value of a Property.
 */
public class PropertyHelper
    extends
        AbstractMeshObjectHelper
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        MeshObject obj = getPrimaryMeshObject( model, options );

        Object defaultValueModel = options.hash( DEFAULT_VALUE_ARGNAME );
        String nullContent       = options.hash( NULL_CONTENT_ARGNAME );

        try {
            PropertyType propertyType = determinePropertyType( model, options, PROPERTY_TYPE_ARGNAME, obj );

            PropertyValue value = obj.getPropertyValue( propertyType );

            String ret;
            if( value != null || nullContent == null ) {
                ret = formatProperty(
                        obj,
                        propertyType,
                        value,
                        defaultValueModel,
                        model,
                        options );
            } else {
                ret = nullContent;
            }

            return ret;

        } catch( ParseException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( IllegalPropertyTypeException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( PropertyValueParsingException ex ) {
            throw new IllegalArgumentException( ex );

        } catch( NotPermittedException ex ) {
            throw new IllegalArgumentException( ex );
        }
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = {
        MESHOBJECT_ARGNAME,
        PROPERTY_TYPE_ARGNAME
    };

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DEFAULT_VALUE_ARGNAME,
        NULL_CONTENT_ARGNAME,
        FLAVOR_ARGNAME,
        MAX_LENGTH_ARGNAME,
        STRING_REPRESENTATION_ARGNAME,
        SHOW_UPLOAD_ARGNAME
    };
}
