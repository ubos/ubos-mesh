function net_ubos_underbars_AttributeEditWrapperHelper_initAttribute_value( nodeName, editIndex, isNull ) {
    var nullInput  = document.getElementById( nodeName + '.attributevalue.' + editIndex + '.null' );
    var createSpan = document.getElementById( nodeName + '.' + editIndex + '.span.create' );
    var removeSpan = document.getElementById( nodeName + '.' + editIndex + '.span.remove' );
    var valueSpan  = document.getElementById( nodeName + '.' + editIndex + '.span.value' );

    if( isNull ) {
        nullInput.value = 'true';
        createSpan.style.display = 'block';
        removeSpan.style.display = 'none';
        valueSpan.style.display  = 'none';
    } else {
        nullInput.value = 'false';
        createSpan.style.display = 'none';
        removeSpan.style.display = 'inline';
        valueSpan.style.display  = 'inline';
    }
}

function net_ubos_underbars_AttributeEditWrapperHelper_doRemove( nodeName, editIndex ) {
    var nullInput  = document.getElementById( nodeName + '.attributevalue.' + editIndex + '.null' );
    var createSpan = document.getElementById( nodeName + '.' + editIndex + '.span.create' );
    var removeSpan = document.getElementById( nodeName + '.' + editIndex + '.span.remove' );
    var valueSpan  = document.getElementById( nodeName + '.' + editIndex + '.span.value' );

    nullInput.value = 'true';

    createSpan.style.display = 'block';
    removeSpan.style.display = 'none';
    valueSpan.style.display  = 'none';
}
function net_ubos_underbars_AttributeEditWrapperHelper_doCreate( nodeName, editIndex ) {
    var nullInput  = document.getElementById( nodeName + '.attributevalue.' + editIndex + '.null' );
    var createSpan = document.getElementById( nodeName + '.' + editIndex + '.span.create' );
    var removeSpan = document.getElementById( nodeName + '.' + editIndex + '.span.remove' );
    var valueSpan  = document.getElementById( nodeName + '.' + editIndex + '.span.value' );

    nullInput.value = 'false';

    createSpan.style.display = 'none';
    removeSpan.style.display = 'inline';
    valueSpan.style.display  = 'inline';
}
