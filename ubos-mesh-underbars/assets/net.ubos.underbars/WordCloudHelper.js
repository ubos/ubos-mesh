function net_ubos_underbars_WordCloudHelper_displayWordCloud( domId, myWords, brackets = null ) {

    // determine the size from the already-created SVG
    var el = document.getElementById( domId );
    var width  = el.clientWidth;
    var height = el.clientHeight;

    var svg = d3.select("#" + domId );

    if( brackets === null ) {
        brackets = new Map();
        brackets.set( 4, 70 );
        brackets.set( 15, 50 );
        brackets.set( 25, 35 );
    }

    var annotatedWords = [];
    for( var i=0 ; i<myWords.length ; ++i ) {
        var fontsize = 20; // default
        for( var [ limit, found ] of brackets ) {
            if( i < limit ) {
                fontsize = found;
                break;
            }
        }

        annotatedWords[i] = {
            text: myWords[i],
            fontsize: fontsize
        };
    }
    var layout = d3.layout.cloud()
                 .size([width, height])
                 .words( annotatedWords )
                 .padding(5)
                 .fontSize( function(d) {
                        return d.fontsize;
                 })
                 .rotate( function(d) { return (d.text.length %2 === 1 ? 360 : -360 )  / d.text.length; } )
                 .on("end", draw);

    layout.start();

    function draw(words) {
        svg
         .append("g")
           .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
           .selectAll("text")
           .data(words)
           .enter()
        .append("a")
          .attr( "href", function(d) { return "/?search=" + d.text; } )
        .append( "text" )
          .style("font-size", function(d) { return d.size + "px"; })
          .attr("text-anchor", "middle")
          .attr("transform", function(d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
          })
          .text(function(d) { return d.text; });
    }
}
