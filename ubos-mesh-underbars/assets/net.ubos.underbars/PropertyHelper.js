document.addEventListener("DOMContentLoaded", function() {
    var spans = document.getElementsByClassName( "TimeStampValue" );
    for( var i=0 ; i<spans.length ; ++i ) {
        spans[i].innerHTML = new Date( parseInt( spans[i].dataset.timestampvalue )).toLocaleString( [], { "dateStyle": "short", "timeStyle": "short" } );
    }
});
