//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.csv;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.Iterator;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.nonblessed.NonblessedCsvUtils;
import net.ubos.meshbase.MeshBase;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Knows how to handle comma-separated values.
 */
public class DefaultCsvImporterHandler
    extends
        AbstractBasicFileImporterHandler
    implements
        NonblessedImporterHandler
{
    /**
     * Builder class.
     */
    public static class Builder
    {
        public Builder format(
                CSVFormat format )
        {
            theFormat = format;
            return this;
        }
        public Builder minColumns(
                int minColumns )
        {
            theMinColumns = minColumns;
            return this;
        }
        public Builder okScore(
                double okScore )
        {
            theOkScore = okScore;
            return this;
        }
        public Builder filenamePattern(
                Pattern filenamePattern )
        {
            theFilenamePattern = filenamePattern;
            return this;
        }
        public DefaultCsvImporterHandler build()
        {
            return new DefaultCsvImporterHandler( theFilenamePattern, theOkScore, theFormat, theMinColumns );
        }
        protected CSVFormat theFormat          = DEFAULT_FORMAT;
        protected int       theMinColumns      = DEFAULT_MIN_COLUMNS;
        protected Pattern   theFilenamePattern = DEFAULT_CSV_FILENAME_PATTERN;
        protected double    theOkScore         = ImporterHandler.FALLBACK;
    }

    /**
     * Constructor with defaults.
     *
     * @param okScore when importing works, what score should be reported
     */
    public DefaultCsvImporterHandler(
            double okScore )
    {
        super( DEFAULT_CSV_FILENAME_PATTERN, okScore );

        theMinColumns = DEFAULT_MIN_COLUMNS;
        theFormat     = DEFAULT_FORMAT;
    }

    /**
     * Constructor.
     *
     * @param format the CSVFormat to use
     * @param minColumns the minimum number of columns to expect
     * @param okScore when importing works, what score should be reported
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     */
    public DefaultCsvImporterHandler(
            Pattern   filenamePattern,
            double    okScore,
            CSVFormat format,
            int       minColumns )
    {
        super( filenamePattern, okScore );

        theMinColumns = minColumns;
        theFormat     = format;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream() ) {
            return IMPOSSIBLE;
        }

        double ret;

        try {
            Reader r = new InputStreamReader( toBeImported.createStream() );

            Iterator<CSVRecord> recordIter = theFormat.parse( r ).iterator();
            if( !recordIter.hasNext()) {
                return IMPOSSIBLE; // not even one row -- let someone else handle it
            }

            int count = 0;
            CSVRecord headerRecord = recordIter.next();
            if( headerRecord.size() < theMinColumns ) {
                return IMPOSSIBLE;
            }

            MeshObject hereObject = toBeImported.getHereMeshObject();
            MeshBase   mb         = context.getMeshBase();

            setRequiredTopAttributes( hereObject, NonblessedCsvUtils.OBJECTTYPE_VALUE_CSVFILE );

            while( recordIter.hasNext() ) {
                CSVRecord currentRecord = recordIter.next();

                MeshObject childObject = mb.createMeshObjectBelow( hereObject, "Row-" + String.valueOf( count ));
                setRequiredNonTopAttributes( childObject, NonblessedCsvUtils.OBJECTTYPE_VALUE_CSVROW );
                setRequiredRoleAttributes( hereObject, childObject, NonblessedCsvUtils.RELATIONSHIPTYPE_ROLE_VALUE_CSV_CONTAINS, null, count );

                int size = Math.min( headerRecord.size(), currentRecord.size() ); // Some rows are short
                for( int colIndex = 0 ; colIndex < size ; ++colIndex ) {
                    String name  = headerRecord.get( colIndex );
                    String value = currentRecord.get( colIndex );

                    if( name.startsWith( "\ufeff" )) { // apparently can happen, see also Amazon importer
                        name = name.substring( 1 );
                    }
                    // apparently, in the Amazon exports, we can have a line that consists entirely of hundreds of \u0000
                    // so we simply strip leading \u0000 -- don't think that will hurt us elsewhere
                    int nonNull = 0;
                    for( ; nonNull<value.length() ; ++nonNull ) {
                        if( value.charAt( nonNull ) != '\u0000' ) {
                            break;
                        }
                    }
                    if( nonNull > 0 ) {
                        childObject.setAttributeValue( name, value.substring( nonNull ));
                    } else {
                        childObject.setAttributeValue( name, value );
                    }
                }
                ++count;
            }
            ret = theOkScore;

        } catch( IllegalStateException ex ) {
            ret = IMPOSSIBLE;
        } catch( IllegalArgumentException ex ) {
            ret = IMPOSSIBLE;
        }

        return ret;
    }

    /**
     * The minimum number of columns to expect.
     */
    protected final int theMinColumns;

    /**
     * How we parse CSV files.
     */
    protected final CSVFormat theFormat;

    /**
     * The default minimum number of columns to expect.
     */
    public static final int DEFAULT_MIN_COLUMNS = 2;

    /**
     * Default for how we parse CSV files.
     */
    public static final CSVFormat DEFAULT_FORMAT
            = CSVFormat.Builder.create( CSVFormat.DEFAULT )
            .build();

    /**
     * The default pattern of file names we support.
     */
    public static final Pattern DEFAULT_CSV_FILENAME_PATTERN = Pattern.compile( ".*\\.csv$", Pattern.CASE_INSENSITIVE );
}
