//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.daemon.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.SQLException;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleSettings;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.util.logging.Log;

/**
 * Main program of the Undertow daemon. Activate using diet4j.
 */
public class ModuleInit
{
    private static final Log log = Log.getLogInstance( ModuleInit.class );

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        log.traceMethodCallEntry( ModuleInit.class, "moduleActivate", thisModule );

        ModuleSettings settings = thisModule.getModuleSettings();

        String dbName   = settings.getString( "maindbname" );
        String dbUser   = settings.getString( "maindbuser" );
        String dbPass   = settings.getString( "maindbpass" );
        String dbServer = settings.getString( "maindbserver", "127.0.0.1" );

        String meshObjectsTableName  = settings.getString( "meshobjectstable",         "MeshObjects" );
        String primaryNsTableName    = settings.getString( "primarynamespacetable",    "PrimaryNamespaceMap" );
        String contextualNsTableName = settings.getString( "contextualnamespacetable", "ContextualNamespaceMap" );

        try {
            MysqlDataSource ds = new MysqlDataSource();
            ds.setDatabaseName( dbName );
            ds.setUser( dbUser );
            ds.setPassword( dbPass );
            ds.setServerName( dbServer );
            ds.setServerTimezone( "UTC" );

            MysqlStore meshObjectStore   = MysqlStore.create( ds, meshObjectsTableName );
            MysqlStore primaryNsStore    = MysqlStore.create( ds, primaryNsTableName );
            MysqlStore contextualNsStore = MysqlStore.create( ds, contextualNsTableName );

            StorePrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create(
                    primaryNsStore );

            // FIXME AccessManager accessMgr = null;

            mainMb = StoreMeshBase.Builder.create( meshObjectStore, contextualNsStore, primaryNsMap ).build();

            Daemon.setMainMeshBase( mainMb );

        } catch( SQLException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }

    /**
     * Diet4j module deactivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleDeactivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        log.traceMethodCallEntry( ModuleInit.class, "moduleDeactivate", thisModule );

        mainMb.die();
    }

    /**
     * The main MeshBase.
     */
    protected static StoreMeshBase mainMb;
}
