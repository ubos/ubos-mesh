//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import java.nio.ByteBuffer;
import net.fusejna.ErrorCodes;
import net.fusejna.StructFuseFileInfo;
import net.fusejna.StructStat;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.Filesystem.FilesystemSubjectArea;

/**
 * Refers to a MeshObject that's blessed with File.
 */
public class FileBlessedMeshObjectFsPath
    extends
        AbstractMeshObjectFsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @param meshObject the referred-to object
     * @return the created instance
     */
    public static FileBlessedMeshObjectFsPath create(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        return new FileBlessedMeshObjectFsPath( fs, path, meshObject );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file ssystem
     * @param meshObject the referred-to object
     */
    protected FileBlessedMeshObjectFsPath(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        super( fs, path, FsObjectType.FILE, meshObject );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doGetattr(
            StructStat.StatWrapper stat )
    {
        super.doGetattr( stat );

        BlobValue content;
        try {
            content = (BlobValue) theMeshObject.getPropertyValue( FilesystemSubjectArea.FILE_CONTENT );

        } catch( IllegalPropertyTypeException ex ) {
            Filesystem.FILESYSTEM_LOG.warn( ex );
            return -ErrorCodes.EACCES();

        } catch( NotPermittedException ex ) {
            Filesystem.FILESYSTEM_LOG.warn( ex );
            return -ErrorCodes.EACCES();
        }

        stat.size( content.value().length );

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doRead(
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        BlobValue content;
        try {
            content = (BlobValue) theMeshObject.getPropertyValue( FilesystemSubjectArea.FILE_CONTENT );

        } catch( IllegalPropertyTypeException ex ) {
            Filesystem.FILESYSTEM_LOG.warn( ex );
            return -ErrorCodes.EACCES();

        } catch( NotPermittedException ex ) {
            return -ErrorCodes.EACCES();
        }

        byte [] contentBytes  = content.value();
        int     contentLength = contentBytes.length;

        int     bytesToRead = (int) Math.min( contentLength - offset, size );
        byte [] bytesRead   = new byte[ bytesToRead ];

        System.arraycopy( contentBytes, (int) offset, bytesRead, 0, bytesToRead );
        buffer.put(bytesRead);

        return bytesToRead;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doTruncate(
            long offset )
    {
        try {
            theMeshObject.getMeshBase().execute(
                    ( Transaction tx ) -> {
                        BlobValue value = (BlobValue) theMeshObject.getPropertyValue( FilesystemSubjectArea.FILE_CONTENT );
                        if( value != null ) {
                            byte [] data = value.value();
                            if( data.length > offset ) {
                                byte [] newData = new byte[ (int) offset ];
                                System.arraycopy( data, 0, newData, 0, (int) offset );
                                data = null;

                                theMeshObject.setPropertyValue( FilesystemSubjectArea.FILE_CONTENT, value.getDataType().createBlobValue( newData ) );
                            }
                        }
                        return null;
                    } );

            return 0;

        } catch( NullPointerException ex ) {
            return -ErrorCodes.EPERM(); // operation not permitted

        } catch( TransactionActionException ex ) {
            Filesystem.FILESYSTEM_LOG.warn( ex );
            return -ErrorCodes.EACCES(); // operation not permitted
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doWrite(
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        byte [] content;
        if( offset > 0 ) {
            // append
            byte [] oldContent;
            try {
                BlobValue contentValue = (BlobValue) theMeshObject.getPropertyValue( FilesystemSubjectArea.FILE_CONTENT );
                oldContent = contentValue.value();

            } catch( IllegalPropertyTypeException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EACCES();

            } catch( NotPermittedException ex ) {
                return -ErrorCodes.EACCES();
            }

            content = new byte[ (int)( size + offset ) ];
            System.arraycopy( oldContent, 0, content, 0, Math.min( oldContent.length, (int) offset ));

            buffer.get( content, (int) offset, (int) size );

        } else {
            content = new byte[ (int) size ];
            buffer.get( content, (int) offset, (int) size );
        }

        BlobValue value = FilesystemSubjectArea.FILE_CONTENT_type.createBlobValue(
                content,
                null );

        try {
            theMeshObject.getMeshBase().execute(
                    ( Transaction tx ) -> {
                            theMeshObject.setPropertyValue( FilesystemSubjectArea.FILE_CONTENT, value );
                            return null;
                    } );

            return (int) size;

        } catch( NullPointerException ex ) {
            return -ErrorCodes.EACCES(); // operation not permitted

        } catch( TransactionActionException ex ) {
            Filesystem.FILESYSTEM_LOG.warn( ex );
            return -ErrorCodes.EACCES(); // operation not permitted
        }
    }
}
