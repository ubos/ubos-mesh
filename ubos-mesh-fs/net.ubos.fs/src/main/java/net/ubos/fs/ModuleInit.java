//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs;

import java.io.File;
import net.fusejna.FuseFilesystem;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleSettings;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseNameServer;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Filesystem.FilesystemSubjectArea;
import net.ubos.util.logging.Log;

/**
 * Activate using diet4j.
 */
public class ModuleInit
{
    private static final Log log = Log.getLogInstance( ModuleInit.class );

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @return the mounted Filesystem
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static FuseFilesystem moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        log.traceMethodCallEntry( ModuleInit.class, "moduleActivate", thisModule );

        ModuleSettings settings = thisModule.getModuleSettings();

        String  dir       = settings.getString(  "dir" );
        boolean mkdir     = settings.getBoolean( "mkdir", true );
        String [] options = settings.getString(  "options", "" ).split( "\\s+" );

        if( options.length == 1 && options[0].isEmpty() ) {
            options = new String[0];
        }

        if( dir != null ) {
            File dirFile = new File( dir );

            if( mkdir ) {
                if( dirFile.exists() ) {
                    if( !dirFile.isDirectory() ) {
                        throw new ModuleActivationException(
                                thisModule.getModuleMeta(),
                                "File exists and is not a directory: " + dirFile.getAbsolutePath() );
                    }
                    if( !dirFile.canExecute() ) {
                        throw new ModuleActivationException(
                                thisModule.getModuleMeta(),
                                "Not permitted to access directory: " + dirFile.getAbsolutePath() );
                    }
                } else {
                    if( !dirFile.mkdirs()) {
                        throw new ModuleActivationException(
                                thisModule.getModuleMeta(),
                                "Failed to create directory: " + dirFile.getAbsolutePath() );
                    }
                }
            }

            MeshBaseNameServer nameServer = Daemon.getMeshBaseNameServer();
            if( nameServer != null ) {
                MeshBase mb = nameServer.getDefaultMeshBase();

                mb.execute( (Transaction tx ) -> {
                        try {
                            MeshObject home = tx.getMeshBase().getHomeObject();
                            if( !home.isBlessedBy( FilesystemSubjectArea.DIRECTORY )) {
                                home.bless( FilesystemSubjectArea.DIRECTORY );
                            }
                        } catch( Throwable t ) {
                            log.error( t );
                        }
                        return null;
                    });

                theFilesystem = Filesystem.create( mb, options );

                try {
                    theFilesystem.mount( dirFile, false );

                } catch( Throwable ex ) {
                    log.error( ex );
                    throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
                }
            }
        }

        return theFilesystem;
    }

    /**
     * Diet4j module deactivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleDeactivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        if( theFilesystem != null ) {
            try {
                theFilesystem.unmount();

            } catch( Throwable ex ) {
                log.error( ex );
                throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
            }
        }
    }

    /**
     * The Filesystem singleton.
     */
    protected static FuseFilesystem theFilesystem;
}
