//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.fusejna.ErrorCodes;
import net.fusejna.StructStat;
import net.fusejna.XattrFiller;
import net.fusejna.XattrListFiller;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.fs.endecoder.DecodingException;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;

/**
 * Common superclass of FileMeshObjectFsPath, DirectoryMeshObjectFsPath
 * and OtherMeshObjectFsPath
 */
public abstract class AbstractMeshObjectFsPath
    extends
        FsPath
{
    /**
     * Private constructor for subclasses only.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @param type the type of object in the file system that this represents or could represent
     * @param meshObject the referred-to object
     */
    protected AbstractMeshObjectFsPath(
            Filesystem   fs,
            String       path,
            FsObjectType type,
            MeshObject   meshObject )
    {
        super( fs, path, type );

        theMeshObject = meshObject;
    }

    /**
     * {@inheritDoc}
     * This may be augmented by subclasses
     */
    @Override
    protected int doGetattr(
            StructStat.StatWrapper stat )
    {
        super.doGetattr( stat );

        if( theMeshObject != null ) {
            long timeUpdated = theMeshObject.getTimeUpdated(); // millis

            stat.mtime( timeUpdated / 1000, ( timeUpdated * 1000000 ) % 1000000000 );
        }

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doGetxattr(
            String      name,
            XattrFiller filler,
            long        size,
            long        position )
    {
        if( !name.startsWith( Filesystem.EXT_ATTR_PREFIX )) {
            return 0;
        }

        if( Filesystem.MESH_TYPES_XATTR_NAME.equals( name )) {
            EntityType [] types = theMeshObject.getEntityTypes();

            MeshTypeIdentifierSerializer ser = theFilesystem.getEncoder().getMeshTypeIdentifierSerializer();

            StringBuilder buf = new StringBuilder();
            String        sep = "";
            for( EntityType type : types ) {
                buf.append( sep );
                buf.append( ser.toExternalForm( type.getIdentifier() ));
                sep = " ";
            }
            byte [] data = buf.toString().getBytes();
            filler.set( data );

            return 0;
        }

        if( name.startsWith( Filesystem.MESH_OJBECT_PROPERTY_XATTR_PREFIX )) {
            String propertyTypeId = name.substring( Filesystem.MESH_OJBECT_PROPERTY_XATTR_PREFIX.length());
            try {
                PropertyType  pt = ModelBase.SINGLETON.findPropertyType( propertyTypeId );
                PropertyValue pv = theMeshObject.getPropertyValue( pt );

                byte[] data = theFilesystem.getEncoder().encodePropertyValue( pt, pv );
                filler.set( data );

                return 0;

            } catch( ParseException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( NotPermittedException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( IllegalPropertyTypeException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted
            }
        }
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doListxattr(
            XattrListFiller filler )
    {
        PropertyType [] pts = theMeshObject.getPropertyTypes();
        String []       ids = new String[ pts.length+1 ];

        ids[0] = Filesystem.MESH_TYPES_XATTR_NAME;

        MeshTypeIdentifierSerializer ser = theFilesystem.getEncoder().getMeshTypeIdentifierSerializer();

        for( int i=0 ; i<pts.length ; ++i ) {
            ids[i+1] = Filesystem.MESH_OJBECT_PROPERTY_XATTR_PREFIX + ser.toExternalForm( pts[i].getIdentifier() );
        }
        filler.add( ids );

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doRemovexattr(
            String name )
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doSetxattr(
            String     name,
            ByteBuffer buffer,
            long       size,
            int        flags,
            int        position )
    {
        if( !name.startsWith( Filesystem.EXT_ATTR_PREFIX )) {
            return -ErrorCodes.EPERM(); // operation not permitted
        }

        // FIXME: this does not deal with attributes whose values are being sent in more
        // FIXME: than one invocation of this method

        byte [] data = new byte[(int) (size + position)];
        buffer.get( data, position, (int) size );

        if( Filesystem.MESH_TYPES_XATTR_NAME.equals( name )) {

            String [] stringValues = new String( data ).split( "\\s+" );
            if( stringValues.length == 1 && stringValues[0].isEmpty() ) {
                stringValues = new String[0];
            }
            Map<MeshTypeIdentifier,EntityType> newTypes = new HashMap<>( stringValues.length );

            for( int i=0 ; i<stringValues.length ; ++i ) {
                try {
                    EntityType type = ModelBase.SINGLETON.findEntityType( stringValues[i] );

                    newTypes.put( type.getIdentifier(), type );

                } catch( ParseException ex ) {
                    Filesystem.FILESYSTEM_LOG.warn( ex );
                    return -ErrorCodes.EPERM(); // operation not permitted

                } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                    Filesystem.FILESYSTEM_LOG.warn( ex );
                    return -ErrorCodes.EPERM(); // operation not permitted
                }
            }

            // This is not incremental, so we need to do a diff
            try {
                theMeshObject.getMeshBase().execute(
                        ( Transaction tx ) -> {
                            Map<MeshTypeIdentifier,EntityType> oldTypes = new HashMap<>();
                            for( EntityType type : theMeshObject.getEntityTypes() ) {
                                oldTypes.put( type.getIdentifier(), type );
                            }

                            for( EntityType type : newTypes.values() ) {
                                if( !oldTypes.containsKey( type.getIdentifier() )) {
                                    theMeshObject.bless( type );
                                }
                            }
                            for( EntityType type : oldTypes.values() ) {
                                if( !newTypes.containsKey( type.getIdentifier() )) {
                                    theMeshObject.unbless( type );
                                }
                            }
                            return null;
                        } );

            } catch( NullPointerException ex ) {
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( TransactionActionException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted
            }

            return 0;
        }

        if( name.startsWith( Filesystem.MESH_OJBECT_PROPERTY_XATTR_PREFIX )) {
            String   propertyTypeId = name.substring( Filesystem.MESH_OJBECT_PROPERTY_XATTR_PREFIX.length());

            try {
                PropertyType  pt = ModelBase.SINGLETON.findPropertyType( propertyTypeId );
                PropertyValue pv = theFilesystem.getEncoder().parsePropertyValue( pt, data );

                theMeshObject.getMeshBase().execute(
                        ( Transaction tx ) -> {
                            theMeshObject.setPropertyValue( pt, pv );
                            return null;
                        } );

                return 0;

            } catch( NullPointerException ex ) {
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( ParseException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( DecodingException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( TransactionActionException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted
            }
        }
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doUnlink()
    {
        try {
            theMeshObject.getMeshBase().execute(
                    ( Transaction tx ) -> {
                        theMeshObject.getMeshBase().deleteMeshObject( theMeshObject );
                        return null;
                    } );

            return 0;

        } catch( NullPointerException ex ) {
            return -ErrorCodes.EPERM(); // operation not permitted

        } catch( TransactionActionException ex ) {
            return -ErrorCodes.EPERM(); // operation not permitted
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doRmdir()
    {
        try {
            theMeshObject.getMeshBase().execute(
                    ( Transaction tx ) -> {
                        theMeshObject.getMeshBase().deleteMeshObject( theMeshObject );
                        return null;
                    } );

            return 0;

        } catch( NullPointerException ex ) {
            return -ErrorCodes.EPERM(); // operation not permitted

        } catch( TransactionActionException ex ) {
            return -ErrorCodes.EPERM(); // operation not permitted
        }
    }

    /**
     * Determine the MeshObject which is referred to by this path
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * The referred to MeshObject.
     */
    protected final MeshObject theMeshObject;
}
