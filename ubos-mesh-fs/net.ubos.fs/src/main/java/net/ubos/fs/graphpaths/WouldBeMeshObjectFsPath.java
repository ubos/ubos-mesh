//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import net.fusejna.ErrorCodes;
import net.fusejna.StructFuseFileInfo;
import net.fusejna.types.TypeMode;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Filesystem.FilesystemSubjectArea;

import static net.ubos.fs.Filesystem.FILESYSTEM_LOG;

/**
 * Would be referring the a MeshObject directly, but the MeshObject does not
 * exist.
 */
public class WouldBeMeshObjectFsPath
    extends
        FsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @return the created instance
     */
    public static WouldBeMeshObjectFsPath create(
            Filesystem fs,
            String     path )
    {
        return new WouldBeMeshObjectFsPath( fs, path );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     */
    protected WouldBeMeshObjectFsPath(
            Filesystem fs,
            String     path )
    {
        super( fs, path, FsObjectType.DOES_NOT_EXIST );
    }

    @Override
    protected int doCreate(
            TypeMode.ModeWrapper               mode,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        MeshBase mb = theFilesystem.getMeshBase();

        try {
            mb.execute( (Transaction tx) -> {
                MeshObject obj = mb.createMeshObject( thePath );
                return obj;
            } );

            return 0;

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return ErrorCodes.EACCES();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doMkdir(
            TypeMode.ModeWrapper mode )
    {
        MeshBase mb = theFilesystem.getMeshBase();

        try {
            mb.execute( ( Transaction tx ) -> {
                MeshObject obj = mb.createMeshObject( thePath, FilesystemSubjectArea.DIRECTORY );
                return obj;
            } );

            return 0;

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return ErrorCodes.EACCES();
        }
    }
}
