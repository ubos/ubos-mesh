//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs;

import java.io.File;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.fusejna.DirectoryFiller;
import net.fusejna.ErrorCodes;
import net.fusejna.FlockCommand;
import net.fusejna.FuseFilesystem;
import net.fusejna.StructFlock;
import net.fusejna.StructFuseFileInfo;
import net.fusejna.StructStat;
import net.fusejna.StructStatvfs;
import net.fusejna.StructTimeBuffer;
import net.fusejna.XattrFiller;
import net.fusejna.XattrListFiller;
import net.fusejna.types.TypeMode;
import net.ubos.fs.endecoder.FsEnDecoder;
import net.ubos.fs.endecoder.SimpleFsEnDecoder;
import net.ubos.fs.graphpaths.DirectoryBlessedMeshObjectFsPath;
import net.ubos.fs.graphpaths.FileBlessedMeshObjectFsPath;
import net.ubos.fs.graphpaths.HomeObjectFsPath;
import net.ubos.fs.graphpaths.MeshObjectNeighborFileFsPath;
import net.ubos.fs.graphpaths.MeshObjectNeighborLinkFsPath;
import net.ubos.fs.graphpaths.MeshObjectRelationshipDirectoryFsPath;
import net.ubos.fs.graphpaths.OtherMeshObjectFsPath;
import net.ubos.fs.graphpaths.WouldBeMeshObjectFsPath;
import net.ubos.fs.modelpaths.MarkerDirectoryFsPath;
import net.ubos.fs.modelpaths.ModelDirectoryFsPath;
import net.ubos.fs.modelpaths.ModelEntityTypeFileFsPath;
import net.ubos.fs.modelpaths.ModelPropertyTypeFileFsPath;
import net.ubos.fs.modelpaths.ModelRelationshipTypeDirectoryFsPath;
import net.ubos.fs.modelpaths.ModelRoleTypeFileFsPath;
import net.ubos.fs.modelpaths.ModelSubjectAreaDirectoryFsPath;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.CollectableMeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.SubjectAreaNotFoundException;
import net.ubos.model.Filesystem.FilesystemSubjectArea;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;

/**
 * A FUSE file system for the UBOS Data Mesh.
 *
 * The mapping between how a filesystem is supposed to look like and the
 * UBOS Data Mesh namespace, and the various characteristics of the objects on both
 * sides is not  exactly trivial. You think you came up with a good mapping, but
 * then you realize it won't work for the home object, or that perfectly
 * normal files in the file system suddenly have to turn into directories,
 * which won't work for users.
 *
 * The primary requirement here is that we can take a regular directory, and
 * interpret it as MeshObjects, rather than the other way around. In other
 * words, the design decision is that we can't lose file system objects when sliding
 * UBOS Data Mesh underneath, and we will accept that not all aspects of MeshObjects
 * may be exposed to the file system under all circumstances.
 *
 * So here is the current plan:
 *
 * General principles:
 *
 * * UBOS Mesh FS claims all files and directories that start with local name .mesh-fs
 *
 * Mapping from file system into UBOS Data Mesh:
 *
 * * A File a/b/c gets mapped to a MeshObject with id a/b/c. If the file is
 *   non-empty, it is blessed with type File and the content is held in File_Content.
 * * A Directory a/b/c gets mapped to a MeshObject with id a/b/c that is blessed
 *   with type Directory
 *
 * Mapping from UBOS Data Mesh to the file system:
 *
 * * A MeshObject with id a/b/c appears as file with name a/b/c, unless it is
 *   blessed with type Directory, in which case it appears as directory
 * * All files are empty, unless the MeshObject is blessed with type File.
 *   In this case, the file content is stored in File_Content
 * * MeshObjects "below" another MeshObject in the identifier namespace may be
 *   invisite in the file system. To access them, bless the MeshObjects on the path with
 *   type Directory.
 * * A MeshObject a/b/c is not mapped into the file system if MeshObjects a or
 *   a/b do not exist. To access in the file system, create those intermediate
 *   objects with type Directory
 * * Types and Properties of a MeshObject are stored in the corresponding file's
 *   or directory's extended attributes: types as a space-separated list of
 *   MeshTypeIdentifiers, in extended attribute "mesh.types", and
 *   properties in extended attributes "mesh.property.&lt;propertyTypeId>&gt;".
 * * If a MeshObject a/b/c participates in at least one relationship, the
 *   directory a/b/.mesh-fs-rel-c provides information about its relationship(s).
 * * This relationship directory contains 2*N files if the MeshObject has N neighbors,
 *   two for each neighbor.
 * * One is a symlink to the file representing the neighbor Meshobject; the
 *   other a regular file of size 0 that holds the extended file system attributes
 *   for the Role. This cannot be the same, because symlinks cannot hold extended
 *   attributes, and regular files cannot be created in an atomic operation to
 *   hold certain content (while symlinks can).
 * * The local name of each file is the hash of the neighbor's MeshObjectIdentifier;
 *   in case of the symlink with postfix -link.
 * * To create a relationship, create the symlink and the other file will magically
 *   appear.
 */
public class Filesystem
    extends
        FuseFilesystem
{
    public static final Log FILESYSTEM_LOG = Log.getLogInstance( Filesystem.class ); // all events related to the file system go here

    /**
     * Factory method.
     *
     * @param mb the MeshBase to be mounted
     * @param options FUSE mount options
     * @return the created instance
     */
    public static Filesystem create(
            MeshBase   mb,
            String ... options )
    {
        return new Filesystem(
                mb,
                SimpleFsEnDecoder.create(
                        ExternalNameHashMeshObjectIdentifierBothSerializer.create( mb.getPrimaryNamespaceMap(), mb ),
                        DefaultMMeshTypeIdentifierBothSerializer.SINGLETON ),
                options );
    }

    /**
     * Factory method.
     *
     * @param mb the MeshBase to be mounted
     * @param idSerializer how MeshObjectIdentifiers will show up
     * @param options FUSE mount options
     * @return the created instance
     */
    public static Filesystem create(
            MeshBase                           mb,
            MeshObjectIdentifierBothSerializer idSerializer,
            String ...                         options )
    {
        return new Filesystem(
                mb,
                SimpleFsEnDecoder.create(
                        idSerializer,
                        DefaultMMeshTypeIdentifierBothSerializer.SINGLETON ),
                options );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param mb      the MeshBase to be mounted
     * @param encoder the encoder for encoding and decoding between semantic and byte-level data
     * @param options options provided to the FUSE mount command
     */
    protected Filesystem(
            MeshBase    mb,
            FsEnDecoder encoder,
            String []   options )
    {
        theMeshBase = mb;
        theEncoder  = encoder;
        theOptions  = options;
    }

    /**
     * Obtain the UID on the files in the file system
     *
     * @return the UID
     */
    public int getUid()
    {
        return getFuseContextUid().intValue();
    }


    /**
     * Obtain the GID on the files in the file system.
     *
     * @return the GID
     */
    public int getGid()
    {
        return getFuseContextGid().intValue();
    }

    /**
     * Obtain the Encoder.
     *
     * @return the FsEncoder
     */
    public FsEnDecoder getEncoder()
    {
        return theEncoder;
    }

    /**
     * Factory method for FsPaths
     *
     * @param path the path as seen in the Filesystem
     * @return the created instance
     */
    public FsPath parse(
            String path )
    {
        if( "/".equals( path )) {
            // the home object is special
            return HomeObjectFsPath.create( this );
        }

        Matcher m;

        // ^/\.mesh-fs    // prefix
        //     (?:
        //         /
        //         ([^/]+)          // group 1: tag, e.g. "model"
        //         (?:"
        //             /
        //             ([^/]+)      // group 2: tag, e.g. subject area identifier
        //             (?:
        //                 /
        //                 ([^/]+)  // group 3: tag, e.g. MeshType local Identifier
        //             )?
        //         )?
        //     )?
        // $

        m = MESH_FS_MARKER_PATH_PATTERN.matcher( path );
        if( m.matches() ) {
            try {
                String tag1 = m.group( 1 );
                String tag2 = m.group( 2 );
                String tag3 = m.group( 3 );

                if( tag1 == null ) {
                    return MarkerDirectoryFsPath.create( this, path );

                } else if( "model".equals( tag1 )) {
                    if( tag2 == null ) {
                        return ModelDirectoryFsPath.create( this, path );

                    } else {
                        SubjectArea sa = ModelBase.SINGLETON.findSubjectArea( tag2 );

                        if( tag3 == null ) {
                            return ModelSubjectAreaDirectoryFsPath.create( this, path, sa );

                        } else {
                            CollectableMeshType cmt = sa.findCollectableMeshTypeByLocalIdentifier( tag3 );

                            if( cmt instanceof EntityType ) {
                                return ModelEntityTypeFileFsPath.create( this, path, (EntityType) cmt );

                            } else if( cmt instanceof RelationshipType ) {
                                return ModelRelationshipTypeDirectoryFsPath.create( this, path, (RelationshipType) cmt );

                            } else if( cmt instanceof PropertyType ) {
                                return ModelPropertyTypeFileFsPath.create( this, path, (PropertyType) cmt );

                            } else if( cmt instanceof RoleType ) {
                                return ModelRoleTypeFileFsPath.create( this, path, (RoleType) cmt );

                            } else {
                                FILESYSTEM_LOG.error( "Unexpected type:", cmt );
                            }
                        }
                    }

                } // else: no other tag1 known

            } catch( ParseException ex ) {
                FILESYSTEM_LOG.warn( ex );

                return InvalidFsPath.create( this, path );

            } catch( SubjectAreaNotFoundException ex ) {
                FILESYSTEM_LOG.warn( ex );

                return InvalidFsPath.create( this, path );

            } catch( CollectableMeshTypeNotFoundException ex ) {
                FILESYSTEM_LOG.warn( ex );

                return InvalidFsPath.create( this, path );
            }
        }


        // ^/*                             // get rid of leading slashes
        //     (.*?)/*                     // group 1: full name or dirname of MeshObjectIdentifier, ignore trailing slash
        //     (?:
        //         \.mesh-fs
        //         (.*)                    // group 2: the rest
        //     )?
        // $

        m = MESHOBJECT_PATH_PATTERN.matcher( path );
        if( m.matches() ) {
            try {
                String beforeMeshPattern = m.group( 1 );
                String remainder         = m.group( 2 );

                if( remainder == null ) {
                    // only group 1 matched, so it's a MeshObject directly

                    MeshObject meshObject = theMeshBase.findMeshObjectByIdentifier( beforeMeshPattern );

                    if( meshObject == null ) {
                        return WouldBeMeshObjectFsPath.create( this, beforeMeshPattern );

                    } else if( meshObject.isBlessedBy( FilesystemSubjectArea.FILE ) ) {
                        return FileBlessedMeshObjectFsPath.create( this, path, meshObject );

                    } else if( meshObject.isBlessedBy( FilesystemSubjectArea.DIRECTORY ) ) {
                        return DirectoryBlessedMeshObjectFsPath.create( this, path, meshObject );

                    } else {
                        return OtherMeshObjectFsPath.create( this, path, meshObject );
                    }
                } else {
                    //         -
                    //         ([^\\.]*)       // group 1: type of UBOS Data Mesh directory that this is
                    //         -
                    //         ([^/]*)         // group 2: basename of MeshObjectIdentifier, or HOME_OBJECT_NAME
                    //         (?:/
                    //             ([0-9a-f]+) // group 3: hash of the identifier of the neighbor
                    //             (\\.link)?    // group 4: the symlink vs the properties file
                    //         )?

                    m = MESHOBJECT_PATH_SUBPATTERN.matcher( remainder );
                    if( m.matches() ) {
                        String dirType      = m.group( 1 );
                        String baseName     = m.group( 2 );
                        String neighborHash = m.group( 3 );
                        String link         = m.group( 4 );

                        if( !MESH_LOCAL_FILE_RELATIONSHIP.equals( dirType ) ) {
                            // that's the only one we know
                            return InvalidFsPath.create( this, path );
                        }

                        String meshObjectId;
                        if( beforeMeshPattern.isEmpty() ) {
                            meshObjectId = baseName;
                        } else {
                            meshObjectId = beforeMeshPattern + "/" + baseName;
                        }
                        MeshObject meshObject = theMeshBase.findMeshObjectByIdentifier( meshObjectId );
                        if( meshObject != null ) {
                            if( neighborHash == null || neighborHash.isEmpty() ) {
                                return MeshObjectRelationshipDirectoryFsPath.create( this, path, meshObject );

                            } else {
                                MeshObject neighbor = findNeighborFromHash( meshObject, neighborHash );

                                if( neighbor != null ) {
                                    if( link == null || link.isEmpty() ) {
                                        return MeshObjectNeighborFileFsPath.create( this, path, meshObject, neighborHash, neighbor );
                                    } else {
                                        return MeshObjectNeighborLinkFsPath.create( this, path, meshObject, neighborHash, neighbor );
                                    }
                                } else {
                                    return InvalidFsPath.create( this, path );
                                }
                            }
                        } else {
                            return InvalidFsPath.create( this, path );
                        }

                    } else {
                        return InvalidFsPath.create( this, path );
                    }
                }
            } catch( ParseException ex ) {
                FILESYSTEM_LOG.warn( ex );

                return InvalidFsPath.create( this, path );
            }
        }
        else {
            return InvalidFsPath.create( this, path );
        }
    }

    /**
     * Obtain the MeshBase.
     *
     * @return the MeshBase
     */
    public MeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * Helper method to find a neighbor MeshObject given the hash of its identifier.
     *
     * @param obj          the MeshObject whose neighbor we are looking for.
     * @param neighborHash hash of the identifier of the neighbor
     * @return the found neighbor, or null
     */
    protected MeshObject findNeighborFromHash(
            MeshObject obj,
            String neighborHash )
    {
        MeshObjectSet neighbors = obj.traverseToNeighbors();

        for( MeshObject neighbor : neighbors ) {
            MeshObjectIdentifier neighborId = neighbor.getIdentifier();

            String candidateHash = theEncoder.neighborLinkName( neighborId );
            if( neighborHash.equals( candidateHash ) ) {
                return obj.getMeshBaseView().findMeshObjectByIdentifier( neighborId );
            }
        }

        return null;
    }

    @Override
    public int access(
            String path,
            int    mask )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.access( mask );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public void afterUnmount(
            File file )
    {
        // noop
    }

    @Override
    public void beforeMount(
            File file )
    {
        // noop
    }

    @Override
    public int bmap(
            String                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int chmod(
            String               path,
            TypeMode.ModeWrapper mode )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int chown(
            String path,
            long   uid,
            long   gid )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int create(
            String                             path,
            TypeMode.ModeWrapper               mode,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.create( mode, fileInfo );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public void destroy()
    {
        // no op
    }

    @Override
    public int fgetattr(
            String                             path,
            StructStat.StatWrapper             stat,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return getattr( path, stat );
    }

    @Override
    public int flush(
            String                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        // no op
        return 0;
    }

    @Override
    public int fsync(
            String                             path,
            int                                i,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        // no op
        return 0;
    }

    @Override
    public int fsyncdir(
            String                             path,
            int                                i,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        // no op
        return 0;
    }

    @Override
    public int ftruncate(
            String                             path,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return truncate( path, offset );
    }

    @Override
    public int getattr(
            String                 path,
            StructStat.StatWrapper stat )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.getattr( stat );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    protected String getName()
    {
        return "meshfs";
    }

    @Override
    protected String[] getOptions()
    {
        return theOptions;
    }

    @Override
    public int getxattr(
            String      path,
            String      name,
            XattrFiller filler,
            long        size,
            long        position )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.getxattr( name, filler, size, position );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public void init()
    {
        // nop op
    }

    @Override
    public int link(
            String s,
            String s1 )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int listxattr(
            String          path,
            XattrListFiller filler )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.listxattr( filler );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int lock(
            String                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo,
            FlockCommand                       command,
            StructFlock.FlockWrapper           flock )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int mkdir(
            String               path,
            TypeMode.ModeWrapper mode )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.mkdir( mode );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int mknod(
            String               path,
            TypeMode.ModeWrapper mode,
            long                 l )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int open(
            String                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.open( fileInfo );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int opendir(
            String                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.opendir( fileInfo );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int read(
            String                             path,
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.read( buffer, size, offset, fileInfo );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int readdir(
            String          path,
            DirectoryFiller filler )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.readdir( filler );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int readlink(
            String     path,
            ByteBuffer buffer,
            long       size )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.readlink( buffer, size );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int release(
            String                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        // no op
        return 0;
    }

    @Override
    public int releasedir(
            String                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        // no op
        return 0;
    }

    @Override
    public int removexattr(
            String path,
            String name )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.removexattr( name );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int rename(
            String oldpath,
            String newpath )
    {
        try {
            FsPath fsp = parse( oldpath );
            return fsp.rename( newpath );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int rmdir(
            String path )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.rmdir();

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int setxattr(
            String     path,
            String     name,
            ByteBuffer buffer,
            long       size,
            int        flags,
            int        position )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.setxattr( name, buffer, size, flags, position );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int statfs(
            String                       path,
            StructStatvfs.StatvfsWrapper statvfs )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int symlink(
            String newpath,
            String oldpath )
    {
        try {
            FsPath fsp = parse( oldpath );
            return fsp.symlink( newpath );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int truncate(
            String path,
            long   offset )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.truncate( offset );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int unlink(
            String path )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.unlink();

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    @Override
    public int utimens(
            String                             path,
            StructTimeBuffer.TimeBufferWrapper buffer )
    {
        return -ErrorCodes.EACCES();
    }

    @Override
    public int write(
            String                             path,
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        try {
            FsPath fsp = parse( path );
            return fsp.write( buffer, size, offset, fileInfo );

        } catch( Throwable t ) {
            FILESYSTEM_LOG.warn( t );
            return -ErrorCodes.EACCES();
        }
    }

    /**
     * The MeshBase that is to be mapped.
     */
    protected final MeshBase theMeshBase;

    /**
     * Encoder used to encode and decode between byte-level representation for the
     * filesystem, and semantic representation in UBOS Data Mesh.
     */
    protected final FsEnDecoder theEncoder;

    /**
     * The options provided to the mount command.
     */
    protected final String [] theOptions;

    /**
     * The directory that marks this filesystem as an UBOS Data Mesh filesystem.
     */
    public static final String MESH_FS_MARKER_PATH = ".mesh-fs";

    /**
     * Regular express that parses graphpaths to the UBOS Data Mesh FS marker directory and its contents. This is
     * public so we can write a test for it.
     */
    public static final Pattern MESH_FS_MARKER_PATH_PATTERN = Pattern.compile(
            "^/" + Pattern.quote( MESH_FS_MARKER_PATH )   // prefix
            + "(?:"
                + "/"
                + "([^/]+)"         // group 1: tag, e.g. "model"
                + "(?:"
                    + "/"
                    + "([^/]+)"     // group 2: tag, e.g. subject area identifier
                    + "(?:"
                        + "/"
                        + "([^/]+)" // group 3: tag, e.g. MeshType local name
                        + ")?"
                + ")?"
            + ")?"
            + "$" );

    /**
     * Prefix of filenames that UBOS Data Mesh claims in the filesystem.
     */
    public static final String MESH_LOCAL_FILE_PREFIX = ".mesh-fs";

    /**
     * The relationship type of local directory
     */
    public static final String MESH_LOCAL_FILE_RELATIONSHIP = "rel";

    /**
     * Regular expression that parses paths to MeshObjects. This is public so we can write
     * a test for it.
     */
    public static final Pattern MESHOBJECT_PATH_PATTERN = Pattern.compile(
            "^/*"                   // get rid of leading slashes
            + "(.*?)/*"             // group 1: full name or dirname of MeshObjectIdentifier, ignore trailing slash
            + "(?:"
                + Pattern.quote( MESH_LOCAL_FILE_PREFIX )
                + "(.*)"            // group 2: the rest
            + ")?"
            + "$" );

    /**
     * Regular express that parses path trailers after the MESH_LOCAL_FILE_PREFIX.
     */
    public static final Pattern MESHOBJECT_PATH_SUBPATTERN = Pattern.compile(
            "-"
            + "([^.]*)"         // group 1: type of UBOS Data Mesh directory that this is -- don't need to escape .
            + "-"
            + "([^/]*)"         // group 2: basename of MeshObjectIdentifier, or HOME_OBJECT_NAME
            + "(?:/"
            +     "([0-9a-f]+)" // group 3: hash of the identifier of the neighbor
            +     "(\\.link)?"  // group 4: the symlink vs the properties file
            + ")?"
    );

    /**
     * Extended attribute name prefix.
     */
    public static final String EXT_ATTR_PREFIX = "user.mesh.";

    /**
     * Name of the extended attribute that holds the EntityTypes of a file representing a Meshobject.
     */
    public static final String MESH_TYPES_XATTR_NAME = EXT_ATTR_PREFIX + "types";

    /**
     * Name of the extended attribute that holds the RoleTypes of a neighbor file.
     */
    public static final String ROLE_TYPES_XATTR_NAME = EXT_ATTR_PREFIX + "roletypes";

    /**
     * Prefix for the names of the extended attributes that holds the pPoperties of an AttributableMeshObject.
     */
    public static final String MESH_OJBECT_PROPERTY_XATTR_PREFIX = EXT_ATTR_PREFIX + "property.";
}
