//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.endecoder;

import net.ubos.model.primitives.DataType;
import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Thrown if a property value could not be parsed.
 */
public class DecodingException
    extends
        AbstractLocalizedException
{
    /**
     * Constructor,
     *
     * @param type the DataType of the value being parsed
     * @param s the String that could not be decoded
     */
    protected DecodingException(
            DataType  type,
            String    s )
    {
        theType   = type;
        theString = s;
    }

    /**
     * Constructor,
     *
     * @param type the DataType of the value being parsed
     * @param s the String that could not be decoded
     * @param cause the underlying cause, if any
     */
    protected DecodingException(
            DataType  type,
            String    s,
            Throwable cause )
    {
        super( cause );

        theType   = type;
        theString = s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getLocalizationParameters()
    {
        return new Object[] {
                theType,
                theString
        };
    }

    /**
     * The DataType of the value being parsed.
     */
    protected DataType theType;

    /**
     * The String that could not be decoded.
     */
    protected String theString;
}
