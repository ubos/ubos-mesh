//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs;

import net.fusejna.DirectoryFiller;
import net.fusejna.ErrorCodes;
import net.fusejna.StructFuseFileInfo;
import net.fusejna.StructStat;
import net.fusejna.XattrFiller;
import net.fusejna.XattrListFiller;
import net.fusejna.types.TypeMode;

import java.nio.ByteBuffer;

/**
 * Captures the type of object that this.
 */
public enum FsObjectType
{
    DIRECTORY( TypeMode.S_IFDIR, 0711 ) {
        @Override
        public int create(
                FsPath                             path,
                TypeMode.ModeWrapper               mode,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EEXIST(); // exists already
        }

        @Override
        public int getattr(
                FsPath                 path,
                StructStat.StatWrapper stat )
        {
            return path.doGetattr( stat );
        }

        @Override
        public int getxattr(
                FsPath      path,
                String      name,
                XattrFiller filler,
                long        size,
                long        position )
        {
            return path.doGetxattr( name, filler, size, position );
        }

        @Override
        public int listxattr(
                FsPath          path,
                XattrListFiller filler )
        {
            return path.doListxattr( filler );
        }

        @Override
        public int mkdir(
                FsPath               path,
                TypeMode.ModeWrapper mode )
        {
            return -ErrorCodes.EEXIST(); // exists already
        }

        @Override
        public int open(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EISDIR();
        }

        @Override
        public int opendir(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return 0;
        }

        @Override
        public int read(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EISDIR();
        }

        @Override
        public int readdir(
                FsPath          path,
                DirectoryFiller filler )
        {
            return path.doReaddir( filler );
        }


        @Override
        public int readlink(
                FsPath     path,
                ByteBuffer buffer,
                long       size )
        {
            return -ErrorCodes.EISDIR();
        }

        @Override
        public int removexattr(
                FsPath path,
                String name )
        {
            return path.doRemovexattr( name );
        }

        @Override
        public int rename(
                FsPath path,
                String newpath )
        {
            return path.doRename( newpath );
        }

        @Override
        public int rmdir(
                FsPath path )
        {
            return path.doRmdir();
        }

        @Override
        public int setxattr(
                FsPath     path,
                String     name,
                ByteBuffer buffer,
                long       size,
                int        flags,
                int        position )
        {
            return path.doSetxattr( name, buffer, size, flags, position );
        }

        @Override
        public int symlink(
                FsPath path,
                String oldpath )
        {
            return -ErrorCodes.EISDIR();
        }

        @Override
        public int truncate(
                FsPath path,
                long   offset )
        {
            return -ErrorCodes.EISDIR();
        }

        @Override
        public int unlink(
                FsPath path )
        {
            return path.doUnlink();
        }

        @Override
        public int write(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EISDIR();
        }
    },

    FILE( TypeMode.S_IFREG, 0600 ) {

        @Override
        public int create(
                FsPath                             path,
                TypeMode.ModeWrapper               mode,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EEXIST(); // exists already
        }

        @Override
        public int getattr(
                FsPath                 path,
                StructStat.StatWrapper stat )
        {
            return path.doGetattr( stat );
        }

        @Override
        public int getxattr(
                FsPath      path,
                String      name,
                XattrFiller filler,
                long        size,
                long        position )
        {
            return path.doGetxattr( name, filler, size, position );
        }

        @Override
        public int listxattr(
                FsPath          path,
                XattrListFiller filler )
        {
            return path.doListxattr( filler );
        }

        @Override
        public int mkdir(
                FsPath               path,
                TypeMode.ModeWrapper mode )
        {
            return -ErrorCodes.EEXIST(); // exists already
        }

        @Override
        public int open(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return 0;
        }

        @Override
        public int opendir(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.ENOTDIR();
        }

        @Override
        public int read(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return path.doRead( buffer, size, offset, fileInfo );
        }

        @Override
        public int readdir(
                FsPath          path,
                DirectoryFiller filler )
        {
            return path.doReaddir( filler );
        }


        @Override
        public int readlink(
                FsPath     path,
                ByteBuffer buffer,
                long       size )
        {
            return -ErrorCodes.EINVAL();
        }

        @Override
        public int removexattr(
                FsPath path,
                String name )
        {
            return path.doRemovexattr( name );
        }

        @Override
        public int rename(
                FsPath path,
                String newpath )
        {
            return path.doRename( newpath );
        }

        @Override
        public int rmdir(
                FsPath path )
        {
            return path.doRmdir();
        }

        @Override
        public int setxattr(
                FsPath     path,
                String     name,
                ByteBuffer buffer,
                long       size,
                int        flags,
                int        position )
        {
            return path.doSetxattr( name, buffer, size, flags, position );
        }

        @Override
        public int symlink(
                FsPath path,
                String oldpath )
        {
            return -ErrorCodes.EINVAL();
        }

        @Override
        public int truncate(
                FsPath path,
                long   offset )
        {
            return path.doTruncate( offset );
        }

        @Override
        public int unlink(
                FsPath path )
        {
            return path.doUnlink();
        }

        @Override
        public int write(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return path.doWrite( buffer, size, offset, fileInfo );
        }
    },

    SYMLINK( TypeMode.S_IFLNK, 0777 ) {
        @Override
        public int create(
                FsPath                             path,
                TypeMode.ModeWrapper               mode,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EEXIST(); // exists already
        }

        @Override
        public int getattr(
                FsPath                 path,
                StructStat.StatWrapper stat )
        {
            return path.doGetattr( stat );
        }

        @Override
        public int getxattr(
                FsPath      path,
                String      name,
                XattrFiller filler,
                long        size,
                long        position )
        {
            return path.doGetxattr( name, filler, size, position );
        }

        @Override
        public int listxattr(
                FsPath          path,
                XattrListFiller filler )
        {
            return path.doListxattr( filler );
        }

        @Override
        public int mkdir(
                FsPath               path,
                TypeMode.ModeWrapper mode )
        {
            return -ErrorCodes.EEXIST(); // exists already
        }

        @Override
        public int open(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.ENOTDIR();
        }

        @Override
        public int opendir(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.ENOTDIR();
        }

        @Override
        public int read(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EINVAL();
        }

        @Override
        public int readdir(
                FsPath           path,
                DirectoryFiller filler )
        {
            return -ErrorCodes.EINVAL();
        }


        @Override
        public int readlink(
                FsPath     path,
                ByteBuffer buffer,
                long       size )
        {
            return path.doReadlink( buffer, size );
        }

        @Override
        public int removexattr(
                FsPath path,
                String name )
        {
            return path.doRemovexattr( name );
        }

        @Override
        public int rename(
                FsPath path,
                String newpath )
        {
            return path.doRename( newpath );
        }

        @Override
        public int rmdir(
                FsPath path )
        {
            return -ErrorCodes.EINVAL();
        }

        @Override
        public int setxattr(
                FsPath     path,
                String     name,
                ByteBuffer buffer,
                long       size,
                int        flags,
                int        position )
        {
            return path.doSetxattr( name, buffer, size, flags, position );
        }

        @Override
        public int symlink(
                FsPath path,
                String oldpath )
        {
            return -ErrorCodes.EEXIST();
        }

        @Override
        public int truncate(
                FsPath path,
                long   offset )
        {
            return -ErrorCodes.EINVAL();
        }

        @Override
        public int unlink(
                FsPath path )
        {
            return path.doUnlink();
        }

        @Override
        public int write(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.EINVAL();
        }
    },

    DOES_NOT_EXIST( 0, 0 ) {
        @Override
        public int create(
                FsPath                             path,
                TypeMode.ModeWrapper               mode,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return path.doCreate( mode, fileInfo );
        }

        @Override
        public int getattr(
                FsPath                 path,
                StructStat.StatWrapper stat )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int getxattr(
                FsPath      path,
                String      name,
                XattrFiller filler,
                long        size,
                long        position )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int listxattr(
                FsPath          path,
                XattrListFiller filler )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int mkdir(
                FsPath               path,
                TypeMode.ModeWrapper mode )
        {
            return path.doMkdir( mode );
        }

        @Override
        public int open(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int opendir(
                FsPath                             path,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int read(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int readdir(
                FsPath           path,
                DirectoryFiller filler )
        {
            return -ErrorCodes.ENOENT();
        }


        @Override
        public int readlink(
                FsPath     path,
                ByteBuffer buffer,
                long       size )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int removexattr(
                FsPath path,
                String name )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int rename(
                FsPath path,
                String newpath )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int rmdir(
                FsPath path )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int setxattr(
                FsPath     path,
                String     name,
                ByteBuffer buffer,
                long       size,
                int        flags,
                int        position )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int symlink(
                FsPath path,
                String oldpath )
        {
            return path.doSymlink( oldpath );
        }

        @Override
        public int truncate(
                FsPath path,
                long   offset )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int unlink(
                FsPath path )
        {
            return -ErrorCodes.ENOENT();
        }

        @Override
        public int write(
                FsPath                             path,
                ByteBuffer                         buffer,
                long                               size,
                long                               offset,
                StructFuseFileInfo.FileInfoWrapper fileInfo )
        {
            return -ErrorCodes.ENOENT();
        }
    };

    /**
     * Private constructor, with the Linux constant that represents it.
     *
     * @param linuxConstant file type
     * @param permissionBitmap the default permission bitmap
     */
    FsObjectType(
            long linuxConstant,
            int  permissionBitmap )
    {
        theLinuxConstant    = linuxConstant;
        thePermissionBitmap = permissionBitmap;
    }

    /**
     * Determine our default access for an object of this type.
     *
     * @param path the FsPathObject being affected
     * @param mask the permissions to be checked
     * @return return code
     */
    public final int access(
            FsPath path,
            int    mask )
    {
        int inverse = ~thePermissionBitmap;
        int result  = ( mask & inverse ) & 07777;

        if( result > 0 ) {
            return -ErrorCodes.EPERM(); // operation not permitted
        } else {
            return 0;
        }
    }

    /**
     * Create and open a file.
     *
     * If the file does not exist, first create it with the specified
     * mode, and then open it.
     *
     * @param path the FsPathObject being affected
     * @param mode the mode of the file
     * @param fileInfo ??? -- file handle?
     * @return return code
     */
    public abstract int create(
            FsPath                             path,
            TypeMode.ModeWrapper               mode,
            StructFuseFileInfo.FileInfoWrapper fileInfo );

    /**
     * Get file attributes.
     *
     * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
     * ignored.      The 'st_ino' field is ignored except if the 'use_ino'
     * mount option is given.
     *
     * @param path the FsPathObject being affected
     * @param stat struct to write
     * @return return code
     */
    public abstract int getattr(
            FsPath                 path,
            StructStat.StatWrapper stat );

    /**
     * Get extended attributes
     *
     * @param path the FsPathObject being affected
     * @param name name of the attribute
     * @param filler write the value of the attribute here
     * @param size length of the buffer
     * @return return code (plus length of value written???)
     */
    public abstract int getxattr(
            FsPath      path,
            String      name,
            XattrFiller filler,
            long        size,
            long        position );

    /**
     * List extended attributes
     *
     * @param path the FsPathObject being affected
     * @param filler write the list here
     * @return return code (plus length of value written???)
     */
    public abstract int listxattr(
            FsPath          path,
            XattrListFiller filler );

    /**
     * Create a directory
     *
     * Note that the mode argument may not have the type specification
     * bits set, i.e. S_ISDIR(mode) can be false.  To obtain the
     * correct directory type bits use  mode|S_IFDIR
     *
     * @param path the FsPathObject being affected
     * @param mode the mode for the directory
     * @return return code
     */
    public abstract int mkdir(
            FsPath               path,
            TypeMode.ModeWrapper mode );

    /**
     * File open operation
     *
     * No creation (O_CREAT, O_EXCL) and by default also no
     * truncation (O_TRUNC) flags will be passed to open(). If an
     * application specifies O_TRUNC, fuse first calls truncate()
     * and then open(). Only if 'atomic_o_trunc' has been
     * specified and kernel version is 2.6.24 or later, O_TRUNC is
     * passed on to open.
     *
     * Unless the 'default_permissions' mount option is given,
     * open should check if the operation is permitted for the
     * given flags. Optionally open may also return an arbitrary
     * filehandle in the fuse_file_info structure, which will be
     * passed to all file operations.
     *
     * @param path the FsPathObject being affected
     * @param fileInfo ??? the flags for opening
     * @return return code
     */
    public abstract int open(
            FsPath                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo );

    /**
     * Open directory
     *
     * Unless the 'default_permissions' mount option is given,
     * this method should check if opendir is permitted for this
     * directory. Optionally opendir may also return an arbitrary
     * filehandle in the fuse_file_info structure, which will be
     * passed to readdir, closedir and fsyncdir.
     *
     * @param path the FsPathObject being affected
     * @param fileInfo ??? the flags for opening
     * @return return code
     */
    public abstract int opendir(
            FsPath                             path,
            StructFuseFileInfo.FileInfoWrapper fileInfo );

    /**
     * Read data from an open file
     *
     * Read should return exactly the number of bytes requested except
     * on EOF or error, otherwise the rest of the data will be
     * substituted with zeroes.      An exception to this is when the
     * 'direct_io' mount option is specified, in which case the return
     * value of the read system call will reflect the return value of
     * this operation.
     *
     * @param path the FsPathObject being affected
     * @param buffer place to read data into
     * @param size snumber of bytes to read
     * @param offset where to start reading within the file
     * @param fileInfo ????
     * @return number of bytes read
     */
    public abstract int read(
            FsPath                             path,
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo );

    /**
     * Read directory
     *
     * The filesystem may choose between two modes of operation:
     *
     * 1) The readdir implementation ignores the offset parameter, and
     * passes zero to the filler function's offset.  The filler
     * function will not return '1' (unless an error happens), so the
     * whole directory is read in a single readdir operation.  This
     * works just like the old getdir() method.
     *
     * 2) The readdir implementation keeps track of the offsets of the
     * directory entries.  It uses the offset parameter and always
     * passes non-zero offset to the filler function.  When the buffer
     * is full (or an error happens) the filler function will return
     * '1'.
     *
     * @param path the FsPathObject being affected
     * @param filler the buffer to write to
     * @return return code
     */
    public abstract int readdir(
            FsPath           path,
            DirectoryFiller filler );


    /**
     * Read the target of a symbolic link
     *
     * The buffer should be filled with a null terminated string.  The
     * buffer size argument includes the space for the terminating
     * null character.      If the linkname is too long to fit in the
     * buffer, it should be truncated.      The return value should be 0
     * for success.
     *
     * @param path the FsPathObject being affected
     * @param buffer the buffer to write
     * @param size length of the buffer
     * @return return code
     */
    public abstract int readlink(
            FsPath     path,
            ByteBuffer buffer,
            long       size );

    /**
     * Remove extended attributes
     *
     * @param path the FsPathObject being affected
     * @param name name of the attribute
     * @return return code
     */
    public abstract int removexattr(
            FsPath path,
            String name );

    /**
     * Rename a file
     *
     * @param path the FsPathObject being affected
     * @param newpath the new name of the file
     * @return return code
     */
    public abstract int rename(
            FsPath path,
            String newpath );

    /**
     * Remove a directory
     *
     * @param path the FsPathObject being affected
     * @return return code
     */
    public abstract int rmdir(
            FsPath path );

    /**
     * Set extended attributes
     *
     * @param path the FsPathObject being affected
     * @param name name of the attribute
     * @param buffer new value of the attribute
     * @param size length of the value
     * @param flags create or replace flags
     * @param position
     * @return return code
     */
    public abstract int setxattr(
            FsPath     path,
            String     name,
            ByteBuffer buffer,
            long       size,
            int        flags,
            int        position );

    /**
     * Create a symbolic link. This object refers to the
     * to-be created file, and oldpath refers to the destination of
     * the to-be-created symlink.
     *
     * @param path the FsPathObject being affected
     * @param oldpath the destination of the symlink
     * @return return code
     */
    public abstract int symlink(
            FsPath path,
            String oldpath );

    /**
     * Change the size of a file
     *
     * @param path the FsPathObject being affected
     * @param offset ???
     * @return return code
     */
    public abstract int truncate(
            FsPath path,
            long   offset );

    /**
     * Remove a file
     *
     * @param path the FsPathObject being affected
     * @return return code
     */
    public abstract int unlink(
        FsPath path );

    /**
     * Write data to an open file
     *
     * Write should return exactly the number of bytes requested
     * except on error.      An exception to this is when the 'direct_io'
     * mount option is specified (see read operation).
     *
     * @param path the FsPathObject being affected
     * @param buffer contains the data to be written
     * @param size the number of bytes in the buffer
     * @param offset the offset into the file
     * @param fileInfo ????
     * @return number of bytes written
     */
    public abstract int write(
            FsPath                             path,
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo );

    /**
     * Obtain the Linux constant representing this type.
     *
     * @return the constant
     */
    public long getLinuxConstant()
    {
        return theLinuxConstant;
    }

    /**
     * Obtain the default permission bitmap.
     *
     * @return the bitmap
     */
    public int getPermissionBitmap()
    {
        return thePermissionBitmap;
    }

    /**
     * The Linux constant representing this type.
     */
    protected long theLinuxConstant;

    /**
     * The default permission bitmap.
     */
    protected int thePermissionBitmap;
}
