//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import java.util.ArrayList;
import java.util.Collections;
import net.fusejna.DirectoryFiller;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;

/**
 * Refers to a MeshObject that's blessed with Directory
 */
public class DirectoryBlessedMeshObjectFsPath
    extends
        AbstractMeshObjectFsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the Filesystem
     * @param meshObject the referred-to object
     * @return the created instance
     */
    public static DirectoryBlessedMeshObjectFsPath create(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        return new DirectoryBlessedMeshObjectFsPath(
                fs,
                path,
                meshObject );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the Filesystem
     * @param meshObject the referred-to object
     */
    protected DirectoryBlessedMeshObjectFsPath(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        super( fs, path, FsObjectType.DIRECTORY, meshObject );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doReaddir(
            DirectoryFiller filler )
    {
        MeshObjectIdentifier meshObjectId = theMeshObject.getIdentifier();

        CursorIterator<MeshObject> descendants = theMeshObject.getMeshBaseView().getIterator( meshObjectId );
        CursorIterator<MeshObject> children    = FilteringCursorIterator.create(
                descendants,
                (MeshObject candidate) -> {
                    String below = candidate.getIdentifier().getLocalId().substring( meshObjectId.getLocalId().length());
                    if( below != null && below.indexOf( '/' ) < 0 ) {
                        return true;
                    } else {
                        return false;
                    }
                } );

        ArrayList<String> names = new ArrayList<>();
        for( MeshObject child : children ) {
            String localName = child.getIdentifier().getLocalId().substring( meshObjectId.getLocalId().length());

            names.add( localName );
            names.add( Filesystem.MESH_LOCAL_FILE_PREFIX + "-" + Filesystem.MESH_LOCAL_FILE_RELATIONSHIP + "-" + localName );
        }
        Collections.sort( names );

        filler.add( names );

        return 0;
    }
}
