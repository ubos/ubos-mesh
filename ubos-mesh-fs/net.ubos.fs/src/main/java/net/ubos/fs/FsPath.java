//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs;

import net.fusejna.DirectoryFiller;
import net.fusejna.ErrorCodes;
import net.fusejna.StructFuseFileInfo;
import net.fusejna.StructStat;
import net.fusejna.XattrFiller;
import net.fusejna.XattrListFiller;
import net.fusejna.types.TypeMode;

import java.nio.ByteBuffer;

/**
 * An interpreted Path into the filesystem.
 * 
 * API documentation is copied from fuse.h
 */
public abstract class FsPath
{
    /**
     * Private constructor for subclasses only.
     * 
     * @param fs the file system
     * @param path the path as seen in the file system
     * @param type the type of object in the file system that this represents or could represent
     */
    protected FsPath(
            Filesystem   fs,
            String       path,
            FsObjectType type )
    {
        theFilesystem = fs;
        thePath       = path;
        theType       = type;
    }
    
    /**
     * Determine the file system in which this is a path.
     * 
     * @return the file system
     */
    public Filesystem getFilesystem()
    {
        return theFilesystem;
    }

    /**
     * Check file access permissions
     *
     * This will be called for the access() system call.  If the
     * 'default_permissions' mount option is given, this method is not
     * called.
     *
     * This method is not called under Linux kernel versions 2.4.x
     *
     * @param mask the permissions to be checked
     * @return return code
     */
    public final int access(
            int mask )
    {
        return theType.access( this, mask );
    }

    /**
     * Create and open a file.
     *
     * If the file does not exist, first create it with the specified
     * mode, and then open it.
     *
     * @param mode the mode of the file
     * @param fileInfo ??? -- file handle?
     * @return return code
     */
    public final int create(
            TypeMode.ModeWrapper               mode,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return theType.create( this, mode, fileInfo );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param mode the mode of the file
     * @param fileInfo ??? -- file handle?
     * @return return code
     */
    protected int doCreate(
            TypeMode.ModeWrapper               mode,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return -ErrorCodes.EPERM();
    }

    /**
     * Get file attributes.
     *
     * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
     * ignored.      The 'st_ino' field is ignored except if the 'use_ino'
     * mount option is given.
     * 
     * @param stat struct to write
     * @return return code
     */
    public final int getattr(
            StructStat.StatWrapper stat )
    {
        return theType.getattr( this, stat );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param stat struct to write
     * @return return code
     */
    protected int doGetattr(
            StructStat.StatWrapper stat )
    {
        // for these fields, see man 7 inode

        // stat.st_dev.set( ??? );                   // ID of device containing file -- hope we don't need
        // stat.st_ino.set( ??? );                   // Inode number -- hope we don't need
        stat.mode( theType.getLinuxConstant() | theType.getPermissionBitmap() );
        // File type and mode
        // stat.st_nlink.set( 1 );                   // Number of hard links
        stat.uid( theFilesystem.getUid() );          // User ID of owner
        stat.gid( theFilesystem.getGid() );          // Group ID of owner
        // stat.st_rdev.set( ??? );                  // Device ID (if special file) -- hope we don't need
        stat.size( 0 );                              // file size -- may be overwritten
        stat.blksize( 4096 );                        // Block size for filesystem I/O
        // stat.st_blocks.set( ??? );                // Number of 512B blocks allocated -- hope we don't need

        stat.atime( 0, 0 );                // Time of last access -- we don't track this
        stat.mtime( 0, 0 );                // Time of last modification
        stat.ctime( 0, 0 );                // Time of last status change

        return 0;
    }

    /**
     * Get extended attributes
     *
     * @param name name of the attribute
     * @param filler write the value of the attribute here
     * @param size length of the buffer
     * @return return code (plus length of value written???)
     */
    public final int getxattr(
            String      name,
            XattrFiller filler,
            long        size,
            long        position )
    {
        return theType.getxattr( this, name, filler, size, position );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param name name of the attribute
     * @param filler write the value of the attribute here
     * @param size length of the buffer
     * @return return code (plus length of value written???)
     */
    protected int doGetxattr(
            String      name,
            XattrFiller filler,
            long        size,
            long        position )
    {
        return 0;
    }

    /**
     * List extended attributes
     *
     * @param filler write the list here
     * @return return code (plus length of value written???)
     */
    public final int listxattr(
            XattrListFiller filler )
    {
        return theType.listxattr( this, filler );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param filler write the list here
     * @return return code (plus length of value written???)
     */
    protected int doListxattr(
            XattrListFiller filler )
    {
        return 0;
    }

    /**
     * Create a directory
     *
     * Note that the mode argument may not have the type specification
     * bits set, i.e. S_ISDIR(mode) can be false.  To obtain the
     * correct directory type bits use  mode|S_IFDIR
     *
     * @param mode the mode for the directory
     * @return return code
     */
    public final int mkdir(
            TypeMode.ModeWrapper mode )
    {
        return theType.mkdir( this, mode );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param mode the mode for the directory
     * @return return code
     */
    protected int doMkdir(
            TypeMode.ModeWrapper mode )
    {
        return -ErrorCodes.EPERM();
    }

     /**
     * File open operation
     *
     * No creation (O_CREAT, O_EXCL) and by default also no
     * truncation (O_TRUNC) flags will be passed to open(). If an
     * application specifies O_TRUNC, fuse first calls truncate()
     * and then open(). Only if 'atomic_o_trunc' has been
     * specified and kernel version is 2.6.24 or later, O_TRUNC is
     * passed on to open.
     *
     * Unless the 'default_permissions' mount option is given,
     * open should check if the operation is permitted for the
     * given flags. Optionally open may also return an arbitrary
     * filehandle in the fuse_file_info structure, which will be
     * passed to all file operations.
     *
     * @param fileInfo ??? the flags for opening
     * @return return code
     */
    public final int open(
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return theType.open( this, fileInfo );
    }

    /**
     * Open directory
     *
     * Unless the 'default_permissions' mount option is given,
     * this method should check if opendir is permitted for this
     * directory. Optionally opendir may also return an arbitrary
     * filehandle in the fuse_file_info structure, which will be
     * passed to readdir, closedir and fsyncdir.
     *
     * @param fileInfo ??? the flags for opening
     * @return return code
     */
    public final int opendir(
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return theType.opendir( this, fileInfo );
    }

    /**
     * Read data from an open file
     *
     * Read should return exactly the number of bytes requested except
     * on EOF or error, otherwise the rest of the data will be
     * substituted with zeroes.      An exception to this is when the
     * 'direct_io' mount option is specified, in which case the return
     * value of the read system call will reflect the return value of
     * this operation.
     *
     * @param buffer place to read data into
     * @param size snumber of bytes to read
     * @param offset where to start reading within the file
     * @param fileInfo ????
     * @return number of bytes read
     */
    public final int read(
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return theType.read( this, buffer, size, offset, fileInfo );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param buffer place to read data into
     * @param size snumber of bytes to read
     * @param offset where to start reading within the file
     * @param fileInfo ????
     * @return number of bytes read
     */
    protected int doRead(
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return -ErrorCodes.EPERM();
    }

    /**
     * Read directory
     *
     * The filesystem may choose between two modes of operation:
     *
     * 1) The readdir implementation ignores the offset parameter, and
     * passes zero to the filler function's offset.  The filler
     * function will not return '1' (unless an error happens), so the
     * whole directory is read in a single readdir operation.  This
     * works just like the old getdir() method.
     *
     * 2) The readdir implementation keeps track of the offsets of the
     * directory entries.  It uses the offset parameter and always
     * passes non-zero offset to the filler function.  When the buffer
     * is full (or an error happens) the filler function will return
     * '1'.
     *
     * @param filler the buffer to write to
     * @return return code
     */
    public final int readdir(
            DirectoryFiller filler )
    {
        return theType.readdir( this, filler );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param filler the buffer to write to
     * @return return code
     */
    protected int doReaddir(
            DirectoryFiller filler )
    {
        return -ErrorCodes.EPERM();
    }

    /**
     * Read the target of a symbolic link
     *
     * The buffer should be filled with a null terminated string.  The
     * buffer size argument includes the space for the terminating
     * null character.      If the linkname is too long to fit in the
     * buffer, it should be truncated.      The return value should be 0
     * for success.
     * 
     * @param buffer the buffer to write
     * @param size length of the buffer
     * @return return code
     */
    public final int readlink(
            ByteBuffer buffer,
            long       size )
    {
        return theType.readlink( this, buffer, size );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param buffer the buffer to write
     * @param size length of the buffer
     * @return return code
     */
    protected int doReadlink(
            ByteBuffer buffer,
            long       size )
    {
        return -ErrorCodes.EPERM();
    }

    /**
     * Remove extended attributes
     *
     * @param name name of the attribute
     * @return return code
     */
    public final int removexattr(
            String name )
    {
        return theType.removexattr( this, name );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param name name of the attribute
     * @return return code
     */
    protected int doRemovexattr(
            String name )
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * Rename a file
     *
     * @param newpath the new name of the file
     * @return return code
     */
    public final int rename(
            String newpath )
    {
        return theType.rename( this, newpath );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param newpath the new name of the file
     * @return return code
     */
    protected int doRename(
            String newpath )
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * Remove a directory
     *
     * @return return code
     */
    public final int rmdir()
    {
        return theType.rmdir( this );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @return return code
     */
    protected int doRmdir()
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * Set extended attributes
     *
     * @param name name of the attribute
     * @param buffer new value of the attribute
     * @param size length of the value
     * @param flags create or replace flags
     * @param position
     * @return return code
     */
    public final int setxattr(
            String     name,
            ByteBuffer buffer,
            long       size,
            int        flags,
            int        position )
    {
        return theType.setxattr( this, name, buffer, size, flags, position );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param name name of the attribute
     * @param buffer new value of the attribute
     * @param size length of the value
     * @param flags create or replace flags
     * @param position
     * @return return code
     */
    protected int doSetxattr(
            String     name,
            ByteBuffer buffer,
            long       size,
            int        flags,
            int        position )
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * Create a symbolic link. This object refers to the
     * to-be created file, and oldpath refers to the destination of
     * the to-be-created symlink.
     *
     * @param oldpath the destination of the symlink
     * @return return code
     */
    public final int symlink(
            String oldpath )
    {
        return theType.symlink( this, oldpath );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param oldpath the destination of the symlink
     * @return return code
     */
    protected int doSymlink(
            String oldpath )
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * Change the size of a file
     *
     * @param offset ???
     * @return return code
     */
    public final int truncate(
            long offset )
    {
        return theType.truncate( this, offset );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param offset ???
     * @return return code
     */
    protected int doTruncate(
            long offset )
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * Remove a file
     *
     * @return return code
     */
    public final int unlink()
    {
        return theType.unlink( this );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @return return code
     */
    protected int doUnlink()
    {
        return -ErrorCodes.EPERM();
    }

    /**
     * Write data to an open file
     *
     * Write should return exactly the number of bytes requested
     * except on error.      An exception to this is when the 'direct_io'
     * mount option is specified (see read operation).
     *
     * @param buffer contains the data to be written
     * @param size the number of bytes in the buffer
     * @param offset the offset into the file
     * @param fileInfo ????
     * @return number of bytes written
     */
    public final int write(
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return theType.write( this, buffer, size, offset, fileInfo );
    }

    /**
     * Overriden by those subclasses that actually do this.
     *
     * @param buffer contains the data to be written
     * @param size the number of bytes in the buffer
     * @param offset the offset into the file
     * @param fileInfo ????
     * @return number of bytes written
     */
    protected int doWrite(
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        return -ErrorCodes.EPERM();
    }

    /**
     * The FileSystem in which this is a path.
     */
    protected final Filesystem theFilesystem;

    /**
     * The path that was parsed.
     */
    protected final String thePath;

    /**
     * The type of object in the file system that this represents or could represent.
     */
    protected FsObjectType theType;
}
