//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import net.fusejna.DirectoryFiller;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.mesh.MeshObject;
import net.ubos.util.logging.Log;

import java.util.ArrayList;
import java.util.Collections;
import net.ubos.mesh.set.MeshObjectSet;

/**
 * Refers to an existing MeshObject's relationship directory.
 */
public class MeshObjectRelationshipDirectoryFsPath
    extends
        FsPath
{
    private static final Log log = Log.getLogInstance( MeshObjectRelationshipDirectoryFsPath.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @return the created instance
     *
     * @param fs the file system
     * @param path the path as seen in the Filesystem
     * @param meshObject the referred-to object
     */
    public static MeshObjectRelationshipDirectoryFsPath create(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        return new MeshObjectRelationshipDirectoryFsPath(
                fs,
                path,
                meshObject );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the Filesystem
     * @param meshObject the referred-to object
     */
    protected MeshObjectRelationshipDirectoryFsPath(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        super( fs, path, FsObjectType.DIRECTORY );

        theMeshObject = meshObject;
    }

    /**
     * Determine the MeshObject which is referred to by this path
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doReaddir(
            DirectoryFiller filler )
    {
        ArrayList<String> names       = new ArrayList<>();
        MeshObjectSet     neighbors = theMeshObject.traverseToNeighbors();

        for( MeshObject neighbor : neighbors ) {
            String neighborName = theFilesystem.getEncoder().neighborLinkName( neighbor.getIdentifier() );
            names.add( neighborName );
            names.add( neighborName + ".link" );
        }

        Collections.sort( names );

        filler.add( names );

        return 0;
    }

    /**
     * The referred to MeshObject.
     */
    protected final MeshObject theMeshObject;
}
