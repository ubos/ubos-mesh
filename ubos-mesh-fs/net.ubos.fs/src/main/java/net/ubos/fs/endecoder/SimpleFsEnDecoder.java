//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.endecoder;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.ParseException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanDataType;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorDataType;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyDataType;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.ExtentDataType;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatDataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerDataType;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.MultiplicityDataType;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointDataType;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodDataType;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampDataType;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.UnknownEnumeratedValueException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;

/**
 * A simple FSEnDecoder.
 */
public class SimpleFsEnDecoder
    implements
        FsEnDecoder
{
    private static final Log log = Log.getLogInstance( SimpleFsEnDecoder.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param moSerializer knows how to serializer MeshObjectIdentifiers
     * @param mtSerializer knows how to serializer MeshTypeIdentifiers
     * @return the created instance
     */
    public static SimpleFsEnDecoder create(
            MeshObjectIdentifierBothSerializer moSerializer,
            MeshTypeIdentifierBothSerializer   mtSerializer )
    {
        return new SimpleFsEnDecoder( moSerializer, mtSerializer );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param moSerializer knows how to serializer MeshObjectIdentifiers
     * @param mtSerializer knows how to serializer MeshTypeIdentifiers
     */
    protected SimpleFsEnDecoder(
            MeshObjectIdentifierBothSerializer moSerializer,
            MeshTypeIdentifierBothSerializer   mtSerializer )
    {
        theMeshObjectIdentifierSerializer = moSerializer;
        theMeshTypeIdentifierSerializer   = mtSerializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifierBothSerializer getMeshObjectIdentifierBothSerializer()
    {
        return theMeshObjectIdentifierSerializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeIdentifierSerializer getMeshTypeIdentifierSerializer()
    {
        return theMeshTypeIdentifierSerializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] encodeMeshObjectIdentifier(
            MeshObjectIdentifier id )
    {
        return theMeshObjectIdentifierSerializer.toExternalForm( id ).getBytes();
    }

    @Override
    public MeshObjectIdentifier decodeMeshObjectIdentifier(
            byte[] value )
        throws
            ParseException
    {
        return theMeshObjectIdentifierSerializer.meshObjectIdentifierFromExternalForm( new String( value ));
    }

    /**
     * Construct the name of the symbolic link to a neighbor.
     *
     * @param neighborId the MeshObjectIdentifier of a neighbor
     * @return the name of the link
     */
    public String neighborLinkName(
            MeshObjectIdentifier neighborId )
    {
        String idAsString = theMeshObjectIdentifierSerializer.toExternalForm( neighborId );
        byte   [] digest  = SHA256.digest( idAsString.getBytes() );

        String hex = String.format( "%064x", new BigInteger( 1, digest ) );
        return hex;
    }

    @Override
    public byte[] encodeMeshTypeIdentifier(
            MeshTypeIdentifier id )
    {
        return theMeshTypeIdentifierSerializer.toExternalForm( id ).getBytes();
    }

    @Override
    public MeshTypeIdentifier decodeMeshTypeIdentifier(
            byte[] value )
        throws
            ParseException
    {
        return theMeshTypeIdentifierSerializer.fromExternalForm( new String( value ));
    }

    @Override
    public PropertyValue parsePropertyValue(
            PropertyType pt,
            byte []      value )
        throws
            DecodingException
    {
        PropertyValue ret;
        DataType      type = pt.getDataType();

        if( type instanceof BlobDataType ) {
            BlobDataType realType = (BlobDataType) type;

            ret = realType.createBlobValue( value, realType.getDefaultMimeType() );

        } else if( type instanceof BooleanDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                char firstChar = stringValue.charAt( 0 );
                switch( firstChar ) {
                    case 'T':
                    case 't':
                        ret = BooleanValue.create( true );
                        break;
                    case 'F':
                    case 'f':
                        ret = BooleanValue.create( false );
                        break;
                    default:
                        throw new DecodingException( type, stringValue );
                }
            }

        } else if( type instanceof ColorDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                int intValue = Integer.parseInt( stringValue );
                ret = ColorValue.create( intValue );
            }

        } else if( type instanceof CurrencyDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                try {
                    ret = CurrencyValue.parseCurrencyValue( stringValue );

                } catch( ParseException ex ) {
                    throw new DecodingException( type, stringValue, ex );
                }
            }

        } else if( type instanceof EnumeratedDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                try {
                    ret = ((EnumeratedDataType) type).select( stringValue );

                } catch( UnknownEnumeratedValueException.Key ex ) {
                    throw new DecodingException( type, stringValue, ex );
                }
            }

        } else if( type instanceof ExtentDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                int    colon = stringValue.indexOf( ':' );
                double a     = Double.parseDouble( stringValue.substring( 0, colon ) );
                double b     = Double.parseDouble( stringValue.substring( colon + 1 ) );
                ret = ExtentValue.create( a, b );
            }

        } else if( type instanceof FloatDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                ret = FloatValue.create( Double.parseDouble( stringValue ) );
            }

        } else if( type instanceof IntegerDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                ret = IntegerValue.create( Integer.parseInt( stringValue ) );
            }

        } else if( type instanceof MultiplicityDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                int    colon   = stringValue.indexOf( ':' );
                String aString = stringValue.substring( 0, colon ).trim();
                String bString = stringValue.substring( colon + 1 ).trim();
                int    a       = Integer.parseInt( aString );
                int b = (MultiplicityValue.N_SYMBOL.equals( bString ) || "N".equals( bString )) ? MultiplicityValue.N : Integer.parseInt(
                        bString );
                ret = MultiplicityValue.create( a, b );
            }

        } else if( type instanceof PointDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                int colon = stringValue.indexOf( ':' );
                int a     = Integer.parseInt( stringValue.substring( 0, colon ) );
                int b     = Integer.parseInt( stringValue.substring( colon + 1 ) );
                ret = PointValue.create( a, b );
            }

        } else if( type instanceof StringDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                ret = StringValue.create( stringValue );
            }

        } else if( type instanceof TimePeriodDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                try {
                    int[] values   = new int[6];
                    int   oldSlash = 0;
                    int   slash;
                    int   index;

                    for( index = 0; index < values.length; ++index ) {
                        slash = stringValue.indexOf( '/', oldSlash );
                        if( slash < 0 ) {
                            values[index] = Integer.parseInt( stringValue.substring( oldSlash ) );
                            break;
                        }
                        values[index] = Integer.parseInt( stringValue.substring( oldSlash, slash ) );
                        oldSlash = slash + 1;
                    }
                    // FIXME -- using integer for float seconds
                    ret = TimePeriodValue.create(
                            (short) ((index >= 5) ? values[index - 5] : 0),
                            (short) ((index >= 4) ? values[index - 4] : 0),
                            (short) ((index >= 3) ? values[index - 3] : 0),
                            (short) ((index >= 2) ? values[index - 2] : 0),
                            (short) ((index >= 1) ? values[index - 1] : 0),
                            (float) ((index >= 0) ? values[index] : 0) );

                } catch( NumberFormatException ex ) {
                    throw new DecodingException( type, stringValue, ex );
                }
            }

        } else if( type instanceof TimeStampDataType ) {
            String stringValue = new String( value );

            if( NULL_VALUE.equals( stringValue )) {
                ret = null;
            } else {
                if( "NOW".equalsIgnoreCase( stringValue ) ) {
                    return TimeStampValue.now();
                }
                try {
                    ret = TimeStampValue.createFromRfc3339( stringValue );

                } catch( ParseException ex ) {
                    throw new DecodingException( type, stringValue, ex );
                }
            }

        } else {
            ret = null;
            log.error( "Unknown data type " + type );
        }

        return ret;
    }

    @Override
    public byte [] encodePropertyValue(
            PropertyType  pt,
            PropertyValue pv )
    {
        byte [] ret;
        if( pv instanceof BlobValue ) {
            BlobValue realValue = (BlobValue) pv;

            ret = realValue.value();

        } else if( pv instanceof BooleanValue ) {
            BooleanValue realValue = (BooleanValue) pv;

            if( realValue.value() ) {
                ret = "TRUE".getBytes();
            } else {
                ret = "FALSE".getBytes();
            }

        } else if( pv instanceof ColorValue ) {
            ColorValue realValue = (ColorValue) pv;

            ret = realValue.getAsCssHex().getBytes();

        } else if( pv instanceof CurrencyValue ) {
            CurrencyValue realValue = (CurrencyValue) pv;

            ret = realValue.value().getBytes();

        } else if( pv instanceof EnumeratedValue ) {
            EnumeratedValue realValue = (EnumeratedValue) pv;

            ret = realValue.value().getBytes();

        } else if( pv instanceof ExtentValue ) {
            ExtentValue realValue = (ExtentValue) pv;

            ret = realValue.toString().getBytes();

        } else if( pv instanceof FloatValue ) {
            FloatValue realValue = (FloatValue) pv;

            ret = String.valueOf( realValue.value() ).getBytes();

        } else if( pv instanceof IntegerValue ) {
            IntegerValue realValue = (IntegerValue) pv;

            ret = String.valueOf( realValue.value() ).getBytes();

        } else if( pv instanceof MultiplicityValue ) {
            MultiplicityValue realValue = (MultiplicityValue) pv;

            ret = realValue.toString().getBytes();

        } else if( pv instanceof PointValue ) {
            PointValue realValue = (PointValue) pv;

            ret = realValue.toString().getBytes();

        } else if( pv instanceof StringValue ) {
            StringValue realValue = (StringValue) pv;

            ret = realValue.value().getBytes();

        } else if( pv instanceof TimePeriodValue ) {
            TimePeriodValue realValue = (TimePeriodValue) pv;

            ret = realValue.value().getBytes();

        } else if( pv instanceof TimeStampValue ) {
            TimeStampValue realValue = (TimeStampValue) pv;

            ret = realValue.value().getBytes();

        } else {
            ret = null;
            log.error( "Unknown PropertyValue type " + pv );
        }
        return ret;
    }

    /**
     * Deserializer for MeshObjectIdentifiers.
     */
    protected MeshObjectIdentifierBothSerializer theMeshObjectIdentifierSerializer;

    /**
     * Serializer for MeshTypeIdentifiers.
     */
    protected MeshTypeIdentifierBothSerializer theMeshTypeIdentifierSerializer;

    /**
     * SHA256 algorithm.
     */
    static final MessageDigest SHA256;
    static {
        MessageDigest javaIsGreat = null;
        try {
            javaIsGreat = MessageDigest.getInstance( "SHA-256" );

        } catch( Throwable t ) {
            log.error( t );
        }
        SHA256 = javaIsGreat;
    }

    /**
     * The special value that indicates NULL.
     */
    public static final String NULL_VALUE = "<NULL>";
}
