//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import net.fusejna.DirectoryFiller;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;

/**
 * Refers to the directory at the root of the UBOS Data Mesh file system that marks this as a UBOS Data Mesh file system.
 */
public class MarkerDirectoryFsPath
    extends
        FsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @return the created instance
     */
    public static MarkerDirectoryFsPath create(
            Filesystem fs,
            String     path )
    {
        return new MarkerDirectoryFsPath( fs, path );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file ssystem
     */
    protected MarkerDirectoryFsPath(
            Filesystem fs,
            String     path )
    {
        super( fs, path, FsObjectType.DIRECTORY );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doReaddir(
            DirectoryFiller filler )
    {
        filler.add( "model" );

        return 0;
    }
}
