//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import net.fusejna.DirectoryFiller;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.ModelBase;

/**
 * The directory containing the models.
 */
public class ModelDirectoryFsPath
    extends
        FsPath
{
    /**
     * Factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @return the created instance
     */
    public static ModelDirectoryFsPath create(
            Filesystem fs,
            String path )
    {
        return new ModelDirectoryFsPath( fs, path );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     */
    protected ModelDirectoryFsPath(
            Filesystem fs,
            String path )
    {
        super( fs, path, FsObjectType.DIRECTORY );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doReaddir(
            DirectoryFiller filler )
    {
        SubjectArea [] sas = ModelBase.SINGLETON.getLoadedSubjectAreas();

        MeshTypeIdentifierSerializer ser = theFilesystem.getEncoder().getMeshTypeIdentifierSerializer();

        for( SubjectArea sa : sas ) {
            filler.add( ser.toExternalForm( sa.getIdentifier()));
        }
        return 0;
    }
}
