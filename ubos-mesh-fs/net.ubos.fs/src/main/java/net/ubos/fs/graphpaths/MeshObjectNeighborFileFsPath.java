//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import net.fusejna.ErrorCodes;
import net.fusejna.XattrFiller;
import net.fusejna.XattrListFiller;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.RoleType;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;

/**
 * Refers to a property file holding the RoleTypes etc to a neighbor MeshObject.
 */
public class MeshObjectNeighborFileFsPath
    extends
        FsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @param meshObject the referred-to object
     * @param neighborHash the hash of the neighbor's identifier
     * @param neighbor the neighbor MeshObject being pointed to
     * @return the created instance
     */
    public static MeshObjectNeighborFileFsPath create(
            Filesystem fs,
            String     path,
            MeshObject meshObject,
            String     neighborHash,
            MeshObject neighbor )
    {
        return new MeshObjectNeighborFileFsPath(
                fs,
                path,
                meshObject,
                neighborHash,
                neighbor );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @param meshObject the referred-to object
     * @param neighborHash the hash of the neighbor's identifier
     * @param neighbor the neighbor MeshObject being pointed to
     */
    protected MeshObjectNeighborFileFsPath(
            Filesystem fs,
            String     path,
            MeshObject meshObject,
            String     neighborHash,
            MeshObject neighbor )
    {
        super( fs, path, FsObjectType.FILE );

        theMeshObject   = meshObject;
        theNeighborHash = neighborHash;
        theNeighbor     = neighbor;
    }

    /**
     * Determine the MeshObject which is referred to by this path
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doGetxattr(
            String      name,
            XattrFiller filler,
            long        size,
            long        position )
    {
        if( !name.startsWith( Filesystem.EXT_ATTR_PREFIX )) {
            return 0;
        }

        if( Filesystem.ROLE_TYPES_XATTR_NAME.equals( name )) {
            RoleType[] roleTypes = theMeshObject.getRoleTypes( theNeighbor );

            MeshTypeIdentifierSerializer ser = theFilesystem.getEncoder().getMeshTypeIdentifierSerializer();

            StringBuilder buf = new StringBuilder();
            String        sep = "";
            for( RoleType roleType : roleTypes ) {
                buf.append( sep );
                buf.append( ser.toExternalForm( roleType.getIdentifier() ));
                sep = " ";
            }
            byte[] data = buf.toString().getBytes();
            filler.set( data );

            return 0;
        }

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doListxattr(
            XattrListFiller filler )
    {
        filler.add( Filesystem.ROLE_TYPES_XATTR_NAME );

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doRemovexattr(
            String name )
    {
        return -ErrorCodes.EPERM(); // operation not permitted
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doUnlink()
    {
        try {
            theMeshObject.getMeshBase().execute( ( Transaction tx ) -> {
                theMeshObject.unrelate( theNeighbor );
                return null;
            } );

            return 0;
        } catch( NullPointerException ex ) {
            return -ErrorCodes.EPERM(); // operation not permitted
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doSetxattr(
            String     name,
            ByteBuffer buffer,
            long       size,
            int        flags,
            int        position )
    {
        if( !name.startsWith( Filesystem.EXT_ATTR_PREFIX )) {
            return -ErrorCodes.EPERM(); // operation not permitted
        }

        // FIXME: this does not deal with attributes whose values are being sent in more
        // FIXME: than one invocation of this method

        byte [] data = new byte[(int) (size + position)];
        buffer.get( data, position, (int) size );

        if( Filesystem.ROLE_TYPES_XATTR_NAME.equals( name )) {

            String [] stringValues = new String( data ).split( "\\s+" );
            if( stringValues.length == 1 && stringValues[0].isEmpty() ) {
                stringValues = new String[0];
            }
            Map<MeshTypeIdentifier,RoleType> newTypes = new HashMap<>( stringValues.length );

            for( int i=0 ; i<stringValues.length ; ++i ) {
                try {
                    RoleType type = ModelBase.SINGLETON.findRoleType( stringValues[i] );

                    newTypes.put( type.getIdentifier(), type );

                } catch( ParseException ex ) {
                    Filesystem.FILESYSTEM_LOG.warn( ex );
                    return -ErrorCodes.EPERM(); // operation not permitted

                } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                    Filesystem.FILESYSTEM_LOG.warn( ex );
                    return -ErrorCodes.EPERM(); // operation not permitted
                }
            }

            // This is not incremental, so we need to do a diff
            try {
                theMeshObject.getMeshBase().execute(
                        ( Transaction tx ) -> {
                            Map<MeshTypeIdentifier,RoleType> oldTypes = new HashMap<>();
                            for( RoleType type : theMeshObject.getRoleTypes(theNeighbor ) ) {
                                oldTypes.put( type.getIdentifier(), type );
                            }

                            for( RoleType type : newTypes.values() ) {
                                if( !oldTypes.containsKey( type.getIdentifier() )) {
                                    theMeshObject.blessRole( type, theNeighbor );
                                }
                            }
                            for( RoleType type : oldTypes.values() ) {
                                if( !newTypes.containsKey( type.getIdentifier() )) {
                                    theMeshObject.unblessRole( type, theNeighbor );
                                }
                            }
                            return null;
                        } );

            } catch( NullPointerException ex ) {
                return -ErrorCodes.EPERM(); // operation not permitted

            } catch( TransactionActionException ex ) {
                Filesystem.FILESYSTEM_LOG.warn( ex );
                return -ErrorCodes.EPERM(); // operation not permitted
            }

            return 0;
        }
        return -ErrorCodes.EPERM(); // operation not permitted

    }

    /**
     * The referred to MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * The hash of the neighbor MeshObject's identifier.
     */
    protected final String theNeighborHash;
}
