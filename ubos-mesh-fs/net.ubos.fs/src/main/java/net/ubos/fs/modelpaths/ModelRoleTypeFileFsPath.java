//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.model.primitives.RoleType;

/**
 * A file that represents a RoleType.
 */
public class ModelRoleTypeFileFsPath
     extends
        AbstractModelTypeFilsFsPath
{
    /**
     * Factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param roleType the RoleType that this file represents
     * @return the created instance
     */
    public static ModelRoleTypeFileFsPath create(
            Filesystem fs,
            String     path,
            RoleType   roleType )
    {
        return new ModelRoleTypeFileFsPath( fs, path, roleType );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param roleType the RoleType that this file represents
     */
    protected ModelRoleTypeFileFsPath(
            Filesystem fs,
            String     path,
            RoleType   roleType )
    {
        super( fs, path, FsObjectType.FILE );

        theRoleType = roleType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFileContent()
    {
        StringBuilder buf = new StringBuilder();

        append( buf, "Name",         theRoleType.getUserVisibleName() );
        append( buf, "Description",  theRoleType.getUserVisibleDescription() );
        append( buf, "Multiplicity", theRoleType.getMultiplicity() );
        // append( buf, "IsSymmetric",  theRoleType.getIsSymmtric() );

        return buf.toString();
    }

    /**
     * The RoleType that this file represents.
     */
    protected RoleType theRoleType;
}
