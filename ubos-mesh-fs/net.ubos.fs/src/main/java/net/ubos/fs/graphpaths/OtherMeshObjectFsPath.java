//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.mesh.MeshObject;

/**
 * Refers to a MeshObject that is blessed with neither File nor Directory.
 */
public class OtherMeshObjectFsPath
    extends
        AbstractMeshObjectFsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the Filesystem
     * @param meshObject the referred-to object
     * @return the created instance
     */
    public static OtherMeshObjectFsPath create(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        return new OtherMeshObjectFsPath(
                fs,
                path,
                meshObject );
    }

    /**
     * Private constructor, use factory method.
     * 
     * @param fs the file system
     * @param path the path as seen in the Filesystem
     * @param meshObject the referred-to object
     */
    protected OtherMeshObjectFsPath(
            Filesystem fs,
            String     path,
            MeshObject meshObject )
    {
        super( fs, path, FsObjectType.FILE, meshObject );
    }
}
