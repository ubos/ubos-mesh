//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.model.primitives.EntityType;

/**
 * A file that represents an EntityType.
 */
public class ModelEntityTypeFileFsPath
    extends
        AbstractModelTypeFilsFsPath
{
    /**
     * Factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param entityType the EntityType that this file represents
     * @return the created instance
     */
    public static ModelEntityTypeFileFsPath create(
            Filesystem fs,
            String     path,
            EntityType entityType )
    {
        return new ModelEntityTypeFileFsPath( fs, path, entityType );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param entityType the EntityType that this file represents
     */
    protected ModelEntityTypeFileFsPath(
            Filesystem fs,
            String     path,
            EntityType entityType )
    {
        super( fs, path, FsObjectType.FILE );

        theEntityType = entityType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFileContent()
    {
        StringBuilder buf = new StringBuilder();

        append( buf, "Name",                    theEntityType.getUserVisibleName());
        append( buf, "Description",             theEntityType.getUserVisibleDescription());
        append( buf, "Supertypes",              theEntityType.getDirectSupertypes() );
        append( buf, "IsAbstract",              theEntityType.getIsAbstract() );
        append( buf, "Synonyms",                theEntityType.getSynonyms() );
        append( buf, "Inherited PropertyTypes", theEntityType.getInheritedPropertyTypes() );
        append( buf, "Local PropertyTypes",     theEntityType.getLocalPropertyTypes() );

        return buf.toString();
    }

    /**
     * The EntityType that this file represents.
     */
    protected EntityType theEntityType;
}
