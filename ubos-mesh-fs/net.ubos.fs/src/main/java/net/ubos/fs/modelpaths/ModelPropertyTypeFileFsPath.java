//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.model.primitives.PropertyType;

/**
 * A file that represents a PropertyType.
 */
public class ModelPropertyTypeFileFsPath
    extends
        AbstractModelTypeFilsFsPath
{
    /**
     * Factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param propertyType the PropertyType that this file represents
     * @return the created instance
     */
    public static ModelPropertyTypeFileFsPath create(
            Filesystem   fs,
            String       path,
            PropertyType propertyType )
    {
        return new ModelPropertyTypeFileFsPath( fs, path, propertyType );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param propertyType the PropertyType that this file represents
     */
    protected ModelPropertyTypeFileFsPath(
            Filesystem   fs,
            String       path,
            PropertyType propertyType )
    {
        super( fs, path, FsObjectType.FILE );

        thePropertyType = propertyType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFileContent()
    {
        StringBuilder buf = new StringBuilder();

        append( buf, "Name",         thePropertyType.getUserVisibleName() );
        append( buf, "Description",  thePropertyType.getUserVisibleDescription() );
        append( buf, "Overrides",    thePropertyType.getOverride() );
        append( buf, "IsOptional",   thePropertyType.getIsOptional() );
        append( buf, "IsReadOnly",   thePropertyType.getIsReadOnly() );
        append( buf, "DataType",     thePropertyType.getDataType() );
        append( buf, "DefaultValue", thePropertyType.getDefaultValue());

        return buf.toString();
    }

    /**
     * The PropertyType that this file represents.
     */
    protected PropertyType thePropertyType;
}
