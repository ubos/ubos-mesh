//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import java.nio.ByteBuffer;
import net.fusejna.StructFuseFileInfo;
import net.fusejna.StructStat;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;

/**
 * Collects functionality common to files in the filesystem that represent ModelTypes.
 */
public abstract class AbstractModelTypeFilsFsPath
    extends
        FsPath
{
    /**
     * Private constructor for subclasses only
     *
     * @param fs   the file system
     * @param path the path as seen in the file system
     * @param type the type of object in the file system that this represents or could represent
     */
    protected AbstractModelTypeFilsFsPath(
            Filesystem fs,
            String path,
            FsObjectType type )
    {
        super( fs, path, type );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doGetattr(
            StructStat.StatWrapper stat )
    {
        super.doGetattr( stat );

        String content = getFileContent();

        stat.size( content.getBytes().length );

        return 0;
    }

    /**
     * Helper method for consistent formatting.
     *
     * @param buf append text here
     * @param label label for the new text
     * @param value value for the new text
     * @return the same StringBuilder
     */
    protected StringBuilder append(
            StringBuilder         buf,
            String                label,
            MeshTypeIdentifier [] value )
    {
        if( value != null && value.length > 0 ) {
        MeshTypeIdentifierSerializer ser = theFilesystem.getEncoder().getMeshTypeIdentifierSerializer();

        StringBuilder buf2 = new StringBuilder();
            String        sep  = "";
            for( MeshTypeIdentifier v : value ) {
                buf2.append( sep );
                buf2.append( ser.toExternalForm( v ));
                sep = "\n";
            }
            append( buf, label, buf2 );
        }
        return buf;
    }

    /**
     * Helper method for consistent formatting.
     *
     * @param buf append text here
     * @param label label for the new text
     * @param value value for the new text
     * @return the same StringBuilder
     */
    protected StringBuilder append(
            StringBuilder buf,
            String        label,
            MeshType []   value )
    {
        if( value != null && value.length > 0 ) {
        MeshTypeIdentifierSerializer ser = theFilesystem.getEncoder().getMeshTypeIdentifierSerializer();

        StringBuilder buf2 = new StringBuilder();
            String        sep  = "";
            for( MeshType v : value ) {
                buf2.append( sep );
                buf2.append( ser.toExternalForm( v.getIdentifier()));
                sep = "\n";
            }
            append( buf, label, buf2 );
        }
        return buf;
    }

    /**
     * Helper method for consistent formatting.
     *
     * @param buf append text here
     * @param label label for the new text
     * @param value value for the new text
     * @return the same StringBuilder
     */
    protected StringBuilder append(
            StringBuilder buf,
            String        label,
            MeshType      value )
    {
        if( value != null ) {
            append( buf, label, theFilesystem.getEncoder().getMeshTypeIdentifierSerializer().toExternalForm( value.getIdentifier()));
        }
        return buf;
    }

    /**
     * Helper method for consistent formatting.
     *
     * @param buf append text here
     * @param label label for the new text
     * @param value value for the new text
     * @return the same StringBuilder
     */
    protected StringBuilder append(
            StringBuilder buf,
            String        label,
            DataType      value )
    {
        if( value != null ) {
            append( buf, label, value.toString() );
        }
        return buf;
    }

    /**
     * Helper method for consistent formatting.
     *
     * @param buf append text here
     * @param label label for the new text
     * @param value value for the new text
     * @return the same StringBuilder
     */
    protected StringBuilder append(
            StringBuilder buf,
            String        label,
            PropertyValue value )
    {
        if( value != null ) {
            String content;
            if( value instanceof StringValue ) {
                content = ((StringValue)value).value();
            } else if( value instanceof BlobValue ) {
                content = ((BlobValue)value).getAsString();
            } else if( value instanceof BooleanValue ) {
                content = ((BooleanValue)value).toString();
            } else if( value instanceof MultiplicityValue ) {
                content = ((MultiplicityValue)value).toString();
            } else {
                content = "<unknown>";
            }
            append( buf, label, content );
        }
        return buf;
    }

    /**
     * Helper method for consistent formatting.
     *
     * @param buf append text here
     * @param label label for the new text
     * @param value value for the new text
     * @return the same StringBuilder
     */
    protected StringBuilder append(
            StringBuilder buf,
            String        label,
            CharSequence  value )
    {
        final int indent = 25;
        buf.append( String.format( "%" + ( indent-2 ) + "s: ", label ));

        // could do a better job at line breaking
        StringBuilder lineBreak = new StringBuilder();
        lineBreak.append( '\n' );
        for( int j=0 ; j<indent ; ++j ) {
            lineBreak.append( ' ' );
        }

        String sep = "";
        for( int i=0 ; i<value.length() ; ++i ) {
            char c = value.charAt( i );
            switch( c ) {
                case '\n':
                    sep = lineBreak.toString();
                    break;
                default :
                    buf.append( sep );
                    buf.append( c );
                    sep = "";
                    break;
            }
        }
        buf.append( '\n' );

        return buf;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected final int doRead(
            ByteBuffer                         buffer,
            long                               size,
            long                               offset,
            StructFuseFileInfo.FileInfoWrapper fileInfo )
    {
        byte [] contentBytes  = getFileContent().getBytes();
        int     contentLength = contentBytes.length;

        int     bytesToRead = (int) Math.min( contentLength - offset, size );
        byte [] bytesRead   = new byte[ bytesToRead ];

        System.arraycopy( contentBytes, (int) offset, bytesRead, 0, bytesToRead );
        buffer.put(bytesRead);

        return bytesToRead;
    }

    /**
     * Obtain the content of the "file".
     *
     * @return the content
     */
    protected abstract String getFileContent();
}
