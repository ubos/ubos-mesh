//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs;

/**
 * A path that does not and cannot exist in a Mesh Filesystem.
 */
public class InvalidFsPath
    extends
        FsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @return: the created instance
     */
    public static InvalidFsPath create(
            Filesystem   fs,
            String       path )
    {
        return new InvalidFsPath( fs, path );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     */
    protected InvalidFsPath(
            Filesystem   fs,
            String       path )
    {
        super( fs, path, FsObjectType.DOES_NOT_EXIST );
    }
}
