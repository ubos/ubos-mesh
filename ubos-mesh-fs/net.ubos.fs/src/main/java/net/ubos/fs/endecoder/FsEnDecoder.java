//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.endecoder;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;

import java.text.ParseException;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;

/**
 * Knows how to encode and decode between high-level semantic PropertyValues
 * and file system-level byte representation.
 */
public interface FsEnDecoder
{
    /**
     * Obtain the serializer for MeshObjectIdentifiers that we use.
     *
     * @return the serializer
     */
    public MeshObjectIdentifierBothSerializer getMeshObjectIdentifierBothSerializer();

    /**
     * Obtain the serializer for MeshTypeIdentifiers that we use.
     *
     * @return the serializer
     */
    public MeshTypeIdentifierSerializer getMeshTypeIdentifierSerializer();

    /**
     * Encode a MeshObjectIdentifier.
     *
     * @param id the MeshObjectIdentifier
     * @return encoded version
     */
    public byte [] encodeMeshObjectIdentifier(
            MeshObjectIdentifier id );

    /**
     * Decode a MeshObjectIdentifier.
     *
     * @param value the encoded version
     * @return the decoded MeshObjectIdentifier
     * @throws ParseException the value cannot be parsed successfully
     */
    public MeshObjectIdentifier decodeMeshObjectIdentifier(
            byte [] value )
        throws
            ParseException;

    /**
     * Construct the name of the symbolic link to a neighbor.
     *
     * @param neighborId the MeshObjectIdentifier of a neighbor
     * @return the name of the link
     */
    public String neighborLinkName(
            MeshObjectIdentifier neighborId );

    /**
     * Encode a MeshTypeIdentifier.
     *
     * @param id the MeshTypeIdentifier
     * @return encoded version
     */
    public byte [] encodeMeshTypeIdentifier(
            MeshTypeIdentifier id );

    /**
     * Decode a MeshTypeIdentifier.
     *
     * @param value the encoded version
     * @return the decoded MeshTypeIdentifier
     * @throws ParseException the value cannot be parsed successfully
     */
    public MeshTypeIdentifier decodeMeshTypeIdentifier(
            byte [] value )
        throws
            ParseException;

    /**
     * Decode bytes into a PropertyValue of the provided PropertyType
     *
     * @param pt the PropertyType
     * @param value the bytes to be parsed
     * @return the resulting PropertyValue
     */
    public PropertyValue parsePropertyValue(
            PropertyType pt,
            byte []      value )
        throws
            DecodingException;

    /**
     * Encode a PropertyValue of the provided PropertyType into bytes.
     *
     * @param pt the PropertyType
     * @param pv the PropertyValue to be emitted
     * @return the resulting bytes
     */
    public byte [] encodePropertyValue(
            PropertyType pt,
            PropertyValue pv );

}
