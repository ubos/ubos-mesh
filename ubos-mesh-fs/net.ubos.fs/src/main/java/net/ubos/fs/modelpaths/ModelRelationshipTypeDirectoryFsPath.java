//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.model.primitives.RelationshipType;

/**
 * A file that represents a RelationshipType.
 */
public class ModelRelationshipTypeDirectoryFsPath
    extends
        AbstractModelTypeFilsFsPath
{
    /**
     * Factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param relationshipType the RelationshipType that this file represents
     * @return the created instance
     */
    public static ModelRelationshipTypeDirectoryFsPath create(
            Filesystem       fs,
            String           path,
            RelationshipType relationshipType )
    {
        return new ModelRelationshipTypeDirectoryFsPath( fs, path, relationshipType );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param relationshipType the RelationshipType that this file represents
     */
    protected ModelRelationshipTypeDirectoryFsPath(
            Filesystem       fs,
            String           path,
            RelationshipType relationshipType )
    {
        super( fs, path, FsObjectType.FILE );

        theRelationshipType = relationshipType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFileContent()
    {
        StringBuilder buf = new StringBuilder();

        append( buf, "Name",        theRelationshipType.getUserVisibleName() );
        append( buf, "Description", theRelationshipType.getUserVisibleDescription() );
        append( buf, "IsAbstract",  theRelationshipType.getIsAbstract() );
        append( buf, "Source",      theRelationshipType.getSource() );
        append( buf, "Destination", theRelationshipType.getDestination() );

        return buf.toString();
    }

    /**
     * The RelationshipType that this file represents.
     */
    protected RelationshipType theRelationshipType;
}
