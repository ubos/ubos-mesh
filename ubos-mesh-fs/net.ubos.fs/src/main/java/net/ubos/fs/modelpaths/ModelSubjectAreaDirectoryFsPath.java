//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.modelpaths;

import net.fusejna.DirectoryFiller;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.SubjectArea;

/**
 * The directory representing a SubjectArea in the model.
 */
public class ModelSubjectAreaDirectoryFsPath
    extends
        FsPath
{
    /**
     * Factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param sa   the SubjectArea that this directory represents
     * @return the created instance
     */
    public static ModelSubjectAreaDirectoryFsPath create(
            Filesystem  fs,
            String      path,
            SubjectArea sa )
    {
        return new ModelSubjectAreaDirectoryFsPath( fs, path, sa );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs   the file system
     * @param path the path as seen in the Filesystem
     * @param sa   the SubjectArea that this directory represents
     */
    protected ModelSubjectAreaDirectoryFsPath(
            Filesystem  fs,
            String      path,
            SubjectArea sa )
    {
        super( fs, path, FsObjectType.DIRECTORY );

        theSubjectArea = sa;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doReaddir(
            DirectoryFiller filler )
    {
        CollectableMeshType[] inSa = theSubjectArea.getCollectableMeshTypes();

        for( CollectableMeshType cmt : inSa ) {
            filler.add( cmt.getIdentifier().getLocalPart() );
        }
        return 0;
    }

    /**
     * The SubjectArea that this directory represents.
     */
    protected SubjectArea theSubjectArea;
}
