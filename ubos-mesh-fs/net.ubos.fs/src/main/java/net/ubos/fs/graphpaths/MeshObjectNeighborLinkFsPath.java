//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.graphpaths;

import java.nio.ByteBuffer;
import net.fusejna.ErrorCodes;
import net.ubos.fs.Filesystem;
import net.ubos.fs.FsObjectType;
import net.ubos.fs.FsPath;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;


/**
 * Refers to a symlink pointing to a neighbor MeshObject.
 */
public class MeshObjectNeighborLinkFsPath
    extends
        FsPath
{
    /**
     * Factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @param meshObject the referred-to object
     * @param neighborHash the hash of the neighbor's identifier
     * @param neighbor the neighbor MeshObject being pointed to
     * @return the created instance
     */
    public static MeshObjectNeighborLinkFsPath create(
            Filesystem fs,
            String     path,
            MeshObject meshObject,
            String     neighborHash,
            MeshObject neighbor )
    {
        return new MeshObjectNeighborLinkFsPath(
                fs,
                path,
                meshObject,
                neighborHash,
                neighbor );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param fs the file system
     * @param path the path as seen in the file system
     * @param meshObject the referred-to object
     * @param neighborHash the hash of the neighbor's identifier
     * @param neighbor the neighbor MeshObject being pointed to
     */
    protected MeshObjectNeighborLinkFsPath(
            Filesystem fs,
            String     path,
            MeshObject meshObject,
            String     neighborHash,
            MeshObject neighbor )
    {
        super( fs, path, FsObjectType.SYMLINK );

        theMeshObject   = meshObject;
        theNeighborHash = neighborHash;
        theNeighbor     = neighbor;
    }

    /**
     * Determine the MeshObject which is referred to by this path
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    @Override
    protected int doReadlink(
            ByteBuffer buffer,
            long       size )
    {
        String content = getLinkContent();

        byte [] contentBytes  = content.getBytes();
        int     contentLength = contentBytes.length;

        int     bytesToRead = (int) Math.min( contentLength, size );
        byte [] bytesRead   = new byte[ bytesToRead ];

        System.arraycopy( contentBytes, 0, bytesRead, 0, bytesToRead );
        buffer.put(bytesRead);

        return 0; // success
    }

    @Override
    protected int doUnlink()
    {
        try {
            theMeshObject.getMeshBase().execute( ( Transaction tx ) -> {
                theMeshObject.unrelate( theNeighbor );
                return null;
            } );

            return 0;

        } catch( NullPointerException ex ) {
            return -ErrorCodes.EPERM(); // operation not permitted
        }
    }

    /**
     * Obtain the link content.
     *
     * @return the link content
     */
    protected String getLinkContent()
    {
        MeshObjectIdentifierSerializer ser = theFilesystem.getEncoder().getMeshObjectIdentifierBothSerializer();

        String neighborId = ser.toExternalForm( theNeighbor.getIdentifier());
        String content;

        if( neighborId.isEmpty() ) {
            content = theFilesystem.getMountPoint().getAbsolutePath();
        } else {
            content = theFilesystem.getMountPoint().getAbsolutePath() + "/" + neighborId;
        }
        return content;
    }

    /**
     * The referred to MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * The hash of the neighbor MeshObject's identifier.
     */
    protected final String theNeighborHash;
}
