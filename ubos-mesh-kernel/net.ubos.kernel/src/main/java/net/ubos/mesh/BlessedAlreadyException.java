//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.MeshType;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * This Exception is thrown if an operation requires a MeshObject or a relationship to
 * be not blessed with a certain type, but it is already.
 */
public abstract class BlessedAlreadyException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject on which the illegal operation was attempted, if available
     * @param type the MeshType of the already-existing blessing
     */
    protected BlessedAlreadyException(
            MeshObject obj,
            MeshType   type )
    {
        super( obj );

        theType = type;
    }

    /**
     * Obtain the MeshType of the already-existing blessing.
     *
     * @return the MeshType
     */
    public MeshType getType()
    {
        return theType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[]{
                    "meshObject",
                    "meshType",
                    "types"
                },
                new Object[] {
                    theMeshObject,
                    theType,
                    MeshTypeUtils.meshTypeIdentifiersOrNull( theMeshObject )
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theType };
    }

    /**
     * The MeshType of the already-existing blessing.
     */
    protected final MeshType theType;
}
