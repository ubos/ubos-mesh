//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import java.util.Comparator;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.AbstractMeshObjectSet;
import net.ubos.mesh.set.MeshObjectSelector;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.OrderedMeshObjectSet;
import net.ubos.mesh.set.OrderedMeshObjectSetWithValue;
import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * In-memory implementation of OrderedMeshObjectSetWithValue
 * 
 * @param <V> the type of value
 */
public class OrderedMMeshObjectSetWithValue<V>
    extends
        AbstractMeshObjectSet
    implements
        OrderedMeshObjectSetWithValue<V>
{
    /**
     * Factory method. Orders from smallest to largest.
     * 
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     * @return the created OrderedMMeshObjectSetWithValue
     * @param <U> the type of value
     */
    public static <U extends Comparable<U>> OrderedMMeshObjectSetWithValue<U> createSmallToLarge(
            MeshObjectSetFactory factory )
    {
        return new OrderedMMeshObjectSetWithValue<>( ( U v1, U v2 ) -> v1.compareTo( v2 ), factory );
    }

    /**
     * Factory method. Orders from largest to smallest.
     * 
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     * @return the created OrderedMMeshObjectSetWithValue
     * @param <U> the type of value
     */
    public static <U extends Comparable<U>> OrderedMMeshObjectSetWithValue<U> createLargeToSmall(
            MeshObjectSetFactory factory )
    {
        return new OrderedMMeshObjectSetWithValue<>( ( U v1, U v2 ) -> v2.compareTo( v1 ), factory );
    }

    /**
     * Factory method. Orders based on provided comparator
     * 
     * @param comparator knows how to compare values
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     * @return the created OrderedMMeshObjectSetWithValue
     * @param <U> the type of value
     */
    public static <U extends Comparable<U>> OrderedMMeshObjectSetWithValue<U> create(
            Comparator<U>        comparator,
            MeshObjectSetFactory factory )
    {
        return new OrderedMMeshObjectSetWithValue<>( comparator, factory );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param comparator knows how to compare values
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     */
    protected OrderedMMeshObjectSetWithValue(
            Comparator<V>        comparator,
            MeshObjectSetFactory factory )
    {
        super( factory );
        
        theComparator = comparator;
        theContent = new MeshObject[0];
        theValues  = new Object[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject[] getMeshObjects()
    {
        return theContent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getMeshObject(
            int index )
    {
        return theContent[ index ];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getFirstMeshObject()
    {
        if( theContent != null && theContent.length > 0 ) {
            return theContent[0];
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getFirst()
    {
        return getFirstMeshObject();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getLastMeshObject()
    {
        if( theContent != null && theContent.length > 0 ) {
            return theContent[theContent.length-1];
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getLast()
    {
        return getLastMeshObject();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int findIndexOf(
            MeshObject candidate )
    {
        for( int i=0 ; i<theContent.length ; ++i ) {
            if( candidate == theContent[i] ) {
                return i;
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedMeshObjectSet subset(
            MeshObjectSelector selector )
    {
        return theFactory.createOrderedImmutableMeshObjectSet( getMeshObjects(), selector );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMMeshObjectSet inverse()
    {
        MeshObject [] newContent = new MeshObject[ theContent.length ];
        for( int i=0 ; i<theContent.length ; ++i ) {
            newContent[ theContent.length - i - 1 ] = theContent[i];
        }
        return new OrderedImmutableMMeshObjectSet( theFactory, newContent );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> iterator()
    {
        return ArrayCursorIterator.create( theContent );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        return theContent.length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public V put(
            MeshObject obj,
            V          v )
    {
        // either we have the object already, or not
        // if we have it already, we probably have to reorder
        
        V ret = null;

        int newIndex = theContent.length;
        for( int i=0 ; i<theContent.length ; ++i ) {
            if( theComparator.compare( v, (V) theValues[i] ) >= 0 ) {
                newIndex = i;
                break;
            }
        }

        int foundAt = -1;
        for( int i=0 ; i<theContent.length ; ++i ) {
            if( obj == theContent[i] ) {
                foundAt = i;
                ret     = (V) theValues[i];
                break;
            }
        }
        if( foundAt >= 0 ) {
            if( newIndex < foundAt ) {
                System.arraycopy( theContent, newIndex, theContent, newIndex+1, foundAt-newIndex );
                System.arraycopy( theValues,  newIndex, theValues,  newIndex+1, foundAt-newIndex );
                
                theContent[newIndex] = obj;
                theValues[newIndex]  = v;
                
            } else if( newIndex > foundAt ) {
                System.arraycopy( theContent, foundAt+1, theContent, foundAt, newIndex-foundAt );
                System.arraycopy( theValues,  foundAt+1, theValues,  foundAt, newIndex-foundAt );
                
                theContent[newIndex] = obj;
                theValues[newIndex]  = v;
                
            } else {
                theValues[newIndex] = v; // might have different value, even in the same place
            }
            
        } else {
            MeshObject [] newContent = new MeshObject[ theContent.length+1 ];
            Object     [] newValues  = new Object[ theContent.length+1 ];

            System.arraycopy( theContent, 0, newContent, 0, newIndex );
            System.arraycopy( theValues,  0, newValues,  0, newIndex );
            
            newContent[newIndex] = obj;
            newValues[newIndex]  = v;
            
            System.arraycopy( theContent, newIndex, newContent, newIndex+1, theContent.length - newIndex );
            System.arraycopy( theValues,  newIndex, newValues,  newIndex+1, theContent.length - newIndex );
            
            theContent = newContent;
            theValues  = newValues;
        }
        return ret;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public V getValueFor(
            MeshObject obj )
    {
        for( int i=0 ; i<theContent.length ; ++i ) {
            if( obj == theContent[i] ) {
                return (V) theValues[i];
            }
        }
        return null;
    }

    /**
     * Defines the ordering.
     */
    protected final Comparator<V> theComparator;

    /**
     * The current content of this set.
     */
    protected MeshObject [] theContent;

    /**
     * The values, in the same sequence as the content
     */
    protected Object [] theValues;
}