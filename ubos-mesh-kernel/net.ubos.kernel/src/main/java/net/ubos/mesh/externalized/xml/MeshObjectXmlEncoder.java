//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.xml;

import net.ubos.mesh.externalized.MeshObjectEncoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class MeshObjectXmlEncoder
    extends
        AbstractMeshObjectXmlEncoder
    implements
        MeshObjectEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static MeshObjectXmlEncoder create()
    {
        return new MeshObjectXmlEncoder();
    }

    /**
     * Constructor, use factory method.
     */
    protected MeshObjectXmlEncoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_OBJECT_XML_ENCODING_ID;
    }
}
