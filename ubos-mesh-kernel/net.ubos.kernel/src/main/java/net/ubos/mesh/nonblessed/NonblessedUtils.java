//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.nonblessed;

/**
 * Definitions and utility functions related to data representations that aren't blessed.
 */
public abstract class NonblessedUtils
{
    /**
     * Keep this abstract.
     */
    private NonblessedUtils() {}

    /**
     * Name of the Attribute of an untyped MeshObject whose value is the type of object that was imported.
     */
    public static final String OBJECTTYPE_ATTRIBUTE = "nb-type";

    /**
     * Name of the RoleAttribute from a parent MeshObject to a child MeshObject whose values indicates the nature
     * of the parent-child relationship.
     *
     * <p>Setting a value for this is required.
     */
    public static final String RELATIONSHIPTYPE_ROLE_ATTRIBUTE = "nb-rel";

    /**
     * Name of the RoleAttribute from a parent MeshObject to a child MeshObject whose value is the local name
     * of the child MeshObject with respect to the parent MeshObject. For example, if an ImporterHandler were
     * to import a file contained in a directory, this would contain the local name of the file.
     * This may or may not apply to a given import: for example, the importer-li may be more appropriate.
     */
    public static final String LOCAL_NAME_ROLE_ATTRIBUTE = "nb-localname";

    /**
     * Name of the RoleAttribute from a parent MeshObject to a child MeshObject whose value is the local index
     * of the child MeshObject with respect to the parent MeshObject. For example, if an ImporterHandler were
     * to import rows in a CSV file, this would contain the row number of the row in the spreadsheet.
     * This may or may not apply to a given import: for example, the importer-ln may be more appropriate.
     */
    public static final String LOCAL_INDEX_ROLE_ATTRIBUTE = "nb-localindex";

    /**
     * Name of the Attribute on a MeshObject whose value indicates data uninterpreted as a blob.
     */
    public static final String CONTENT_ATTRIBUTE = "nb-content";

    /**
     * Value for OBJECTTYPE_ATTRIBUTE that indicates this MeshObject represents a file whose content was
     * not further interpreted.
     */
    public static final String OBJECTTYPE_VALUE_FILE = "file";

    /**
     * Value for OBJECTTYPE_ATTRIBUTE that indicates this MeshObject was created
     * from a directory.
     */
    public static final String OBJECTTYPE_VALUE_DIRECTORY = "directory";

    /**
     * Value for RELATIONSHIPTYPE_ROLE_ATTRIBUTE that indicates the parent MeshObject
     * was created from a directory, and the child MeshObject from a contained file (not saying anything
     * about the contained file's type; could be another directory or plain file)
     */
    public static final String RELATIONSHIPTYPE_ROLE_VALUE_DIRECTORY_CONTAINS = "directory-contains";

}
