//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

/**
 * An ImmutableTraversePathSet that is also ordered.
 */
public interface OrderedImmutableTraversePathSet
    extends
        ImmutableTraversePathSet,
        OrderedTraversePathSet
{
    // nothing
}
