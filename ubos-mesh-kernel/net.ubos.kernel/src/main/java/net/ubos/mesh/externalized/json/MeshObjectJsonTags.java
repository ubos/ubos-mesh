//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.json;

import net.ubos.model.primitives.externalized.json.PropertyValueJsonTags;

/**
 * JSON Tags for the serialization of ExternalizedMeshObjects.
 */
public interface MeshObjectJsonTags
        extends
            PropertyValueJsonTags
{
    /** Encoding ID */
    public static final String MESH_OBJECT_JSON_ENCODING_ID
            = MeshObjectJsonTags.class.getPackage().getName();

    /** Tag indicating a MeshObject. */
    public static final String MESHOBJECT_TAG = "MO";

    /** Tag indicating the time the MeshObject was created. */
    public static final String TIME_CREATED_TAG = "TC";

    /** Tag indicating the time the MeshObject was updated. */
    public static final String TIME_UPDATED_TAG = "TU";

    /** Tag indicating the MeshObject's state. */
    public static final String STATE_TAG = "S";

    /** Value indicating that the MeshObject is dead. */
    public static final String STATE_VALUE_DEAD = "D";

    /** Tag indicating the Identifier of a MeshObject. */
    public static final String IDENTIFIER_TAG = "ID";

    /** Tag indicating the type section. */
    public static final String TYPES_TAG = "T";

    /** Tag indicating the (String) Attributes section. */
    public static final String STRING_ATTRIBUTES_TAG = "AS";

    /** Tag indicating the (non-String) Attributes section. */
    public static final String ATTRIBUTES_TAG = "A";

    /** Tag indicating the Properties section. */
    public static final String PROPERTIES_TAG = "PT";

    /** Tag indicating the neighbors section. */
    public static final String NEIGHBORS_TAG = "R";

    /** Tag indicating a MeshObject's history section */
    public static final String MESH_OBJECT_HISTORY_TAG = "H";
}
