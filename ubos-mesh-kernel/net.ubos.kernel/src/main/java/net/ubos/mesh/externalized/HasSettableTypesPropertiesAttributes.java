//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import java.io.Serializable;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;

/**
 * Simplifies parsing code that can now be applied to MeshObjects and
 * ThisEnds.
 */
public interface HasSettableTypesPropertiesAttributes
    extends
        HasTypesPropertiesAttributes
{
    /**
     * Add an Attribute.
     *
     * @param name the Attribute's name
     */
    public abstract void addAttributeName(
            String name );

    /**
     * Add the value for an Attribute.
     *
     * @param newValue the Attribute's value
     */
    public abstract void addAttributeValue(
            Serializable newValue );

    /**
     * Add a type by identifier
     *
     * @param identifier the identifier
     */
    public abstract void addType(
            MeshTypeIdentifier identifier );

    /**
     * Add a PropertyType, using its MeshTypeIdentifier.
     *
     * @param identifier the PropertyType's identifier
     */
    public abstract void addPropertyType(
            MeshTypeIdentifier identifier );

    /**
     * Add a PropertyValue.
     *
     * @param newValue the PropertyValue.
     */
    public abstract void addPropertyValue(
            PropertyValue newValue );
}
