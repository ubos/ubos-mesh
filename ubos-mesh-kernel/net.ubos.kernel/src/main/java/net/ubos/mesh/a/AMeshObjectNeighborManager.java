//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.a;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import net.ubos.mesh.AttributesMap;
import net.ubos.mesh.IllegalRolePropertyTypeException;
import net.ubos.mesh.NotRelatedException;
import net.ubos.mesh.PropertiesMap;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;

/**
 * Abstraction to manager neighbors and associated information
 * in an AMeshObject. Instances of this type do not actually store any
 * information; that is provided by the AMeshObject provided as argument
 * into each method call.
 *
 * Note: we order the neighbors by youngest-first (i.e. the larger timeCreated comes first)
 */
public class AMeshObjectNeighborManager
{
    private static final Log log = Log.getLogInstance( AMeshObjectNeighborManager.class ); // our own, private logger

    /**
     * Constructor for subclasses only. Use SINGLETON.
     */
    protected AMeshObjectNeighborManager() {}

    /**
     * Determine whether a MeshObject has neighbors.
     *
     * @param subject the MeshObject in question
     * @return true if it has at least one neighbor
     */
    public boolean hasNeighbors(
            AMeshObject subject )
    {
        AMeshObjectIdentifier [] neighborIdentifiers = subject.theNeighborIdentifiers;

        if( neighborIdentifiers == null ) {
            return false;
        }
        if( neighborIdentifiers.length == 0 ) {
            return false;
        }
        return true;
    }

    /**
     * Determine the index of a relationship in a MeshObject's relationship table.
     * Returns -1 if not related.
     *
     * @param subject the MeshObject in question
     * @param neighborIdentifier identifier of the neighbor
     * @return the index, or -1
     */
    public int determineRelationshipIndex(
            AMeshObject           subject,
            AMeshObjectIdentifier neighborIdentifier )
    {
        AMeshObjectIdentifier [] neighborIdentifiers = subject.theNeighborIdentifiers;
        if( neighborIdentifiers == null || neighborIdentifiers.length == 0 ) {
            return -1;
        }
        for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
            if( neighborIdentifier.equals( neighborIdentifiers[i] )) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Determine whether a MeshObject is related to another.
     *
     * @param subject the MeshObject in question
     * @param neighborIdentifier identifier of the neighbor
     * @return true if they are related
     */
    public boolean isRelated(
            AMeshObject           subject,
            AMeshObjectIdentifier neighborIdentifier )
    {
        int index = determineRelationshipIndex( subject, neighborIdentifier );
        return index >= 0;
    }

    /**
     * Obtain the set of identifiers of neighbor MeshObjects.
     *
     * @param subject the MeshObject in question
     * @return the set of identifiers of neighbor MeshObjects
     */
    public AMeshObjectIdentifier [] getNeighborIdentifiers(
            AMeshObject subject )
    {
        AMeshObjectIdentifier [] ret = subject.theNeighborIdentifiers;
        return ret;
    }

    /**
     * Obtain the set of RoleTypes for all neighbors.
     *
     * @param subject the MeshObject in question
     * @return the RoleTypes of neighbor MeshObjects, in the same sequence as getNeighborIdentifiers()
     */
    public RoleType[][] getRoleTypes(
            AMeshObject subject )
    {
        RoleType [][] ret = subject.theNeighborRoleTypes;
        return ret;
    }

    /**
     * Obtain the same of RoleAttributes for all neighbors.
     *
     * @param subject the MeshObject in question
     * @return the AttributeMaps of neighbor MeshObjects, in the same sequence as getNeighborIdentifiers()
     */
    public AttributesMap [] getRoleAttributes(
            AMeshObject subject )
    {
        AttributesMap [] ret = subject.theNeighborRoleAttributes;
        return ret;
    }

    /**
     * Obtain the set of Role PropertyTypes for all neighbors.
     *
     * @param subject the MeshObject in question
     * @return the PropertyTypes of all Roles with all neighbors
     */
    public PropertiesMap [] getRolePropertyTypes(
            AMeshObject subject )
    {
        PropertiesMap [] ret = subject.theNeighborRoleProperties;
        return ret;
    }

    /**
     * Obtain the set of RoleTypes for a particular neighbor.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @return the RoleTypes of the neighbor MeshObject
     * @throws NotRelatedException thrown if the specified MeshObject is not actually a neighbor
     */
    public RoleType [] getRoleTypesFor(
            AMeshObject subject,
            AMeshObject neighbor )
        throws
            NotRelatedException
    {
        int index = determineRelationshipIndex( subject, neighbor.getIdentifier() );

        if( index >= 0 ) {
            return subject.theNeighborRoleTypes[index];
        } else {
            throw new NotRelatedException( subject, neighbor );
        }
    }

    /**
     * Obtain the set of RoleAttributes for a particular neighbor.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @return the RoleAttributes of the neighbor MeshObject
     * @throws NotRelatedException thrown if the specified MeshObject is not actually a neighbor
     */
    public AttributesMap getRoleAttributesFor(
            AMeshObject subject,
            AMeshObject neighbor )
        throws
            NotRelatedException
    {
        int index = determineRelationshipIndex( subject, neighbor.getIdentifier() );

        if( index >= 0 ) {
            return subject.theNeighborRoleAttributes[index];
        } else {
            throw new NotRelatedException( subject, neighbor );
        }
    }

    /**
     * Obtain the set of RoleProperties for a particular neighbor.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @return the RoleProperties of the neighbor MeshObject
     * @throws NotRelatedException thrown if the specified MeshObject is not actually a neighbor
     */
    public PropertiesMap getRolePropertiesFor(
            AMeshObject subject,
            AMeshObject neighbor )
        throws
            NotRelatedException
    {
        int index = determineRelationshipIndex( subject, neighbor.getIdentifier() );

        if( index >= 0 ) {
            return subject.theNeighborRoleProperties[index];
        } else {
            throw new NotRelatedException( subject, neighbor );
        }
    }

    /**
     * Set the value of a Role Property for the subject with a particular neighbor.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @param pt the Role PropertyType
     * @param newValue the new Role PropertyValue
     * @return the previous PropertyValue
     * @throws NotRelatedException thrown if the specified MeshObject is not actually a neighbor
     */
    public PropertyValue setRolePropertyValue(
            AMeshObject   subject,
            AMeshObject   neighbor,
            PropertyType  pt,
            PropertyValue newValue )
        throws
            NotRelatedException
    {
        int index = determineRelationshipIndex( subject, neighbor.getIdentifier() );

        if( index >= 0 ) {
            if( subject.theNeighborRoleProperties[index] == null ) {
                subject.theNeighborRoleProperties[index] = new PropertiesMap();
            }
            return subject.theNeighborRoleProperties[index].put( pt, newValue );

        } else {
            throw new IllegalRolePropertyTypeException( subject, neighbor, pt );
        }
    }

    /**
     * Find the index of a neighbor. If it does not exist, add it.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @return the index at which the neighbor has been founded or added
     */
    public int findOrAddNeighbor(
            AMeshObject subject,
            AMeshObject neighbor )
    {
        AMeshObjectIdentifier neighborIdentifier = neighbor.getIdentifier();

        int ret = -1;
        if( subject.theNeighborIdentifiers == null || subject.theNeighborIdentifiers.length == 0 ) {
            subject.theNeighborIdentifiers    = new AMeshObjectIdentifier[] { neighborIdentifier };
            subject.theNeighborTimeCreateds   = new long[]                  { neighbor.getTimeCreated() };
            subject.theNeighborRoleTypes      = new RoleType[][]            { null };
            subject.theNeighborRoleProperties = new PropertiesMap[]         { null };
            subject.theNeighborRoleAttributes = new AttributesMap[]         { null };

            ret = 0;

        } else {
            // insert by timeCreated
            long neighborTimeCreated = neighbor.getTimeCreated();

            for( int i=0 ; i<subject.theNeighborIdentifiers.length ; ++i ) {
                if( subject.theNeighborTimeCreateds[i] > neighborTimeCreated ) {
                    // as long as the current neighbor is younger (larger timeCreated), keep going
                    continue;
                }
                if( subject.theNeighborTimeCreateds[i] < neighborTimeCreated ) {
                    // too far; need to add here in the middle
                    AMeshObjectIdentifier [] newNeighborIdentifiers    = new AMeshObjectIdentifier[ subject.theNeighborIdentifiers.length + 1 ];
                    long []                  newNeighborTimeCreateds   = new long[ subject.theNeighborIdentifiers.length + 1 ];
                    RoleType [][]            newNeighborRoleTypes      = new RoleType[ subject.theNeighborIdentifiers.length + 1 ][];
                    PropertiesMap []         newNeighborRoleProperties = new PropertiesMap[ subject.theNeighborIdentifiers.length + 1 ];
                    AttributesMap []         newNeighborRoleAttributes = new AttributesMap[ subject.theNeighborIdentifiers.length + 1 ];

                    System.arraycopy( subject.theNeighborIdentifiers,    0, newNeighborIdentifiers,    0, i );
                    System.arraycopy( subject.theNeighborTimeCreateds,   0, newNeighborTimeCreateds,   0, i );
                    System.arraycopy( subject.theNeighborRoleTypes,      0, newNeighborRoleTypes,      0, i );
                    System.arraycopy( subject.theNeighborRoleProperties, 0, newNeighborRoleProperties, 0, i );
                    System.arraycopy( subject.theNeighborRoleAttributes, 0, newNeighborRoleAttributes, 0, i );

                    newNeighborIdentifiers[i]    = neighborIdentifier;
                    newNeighborTimeCreateds[i]   = neighbor.getTimeCreated();
                    newNeighborRoleTypes[i]      = null;
                    newNeighborRoleProperties[i] = null;
                    newNeighborRoleAttributes[i] = null;

                    System.arraycopy( subject.theNeighborIdentifiers,    i, newNeighborIdentifiers,    i+1, subject.theNeighborIdentifiers.length-i );
                    System.arraycopy( subject.theNeighborTimeCreateds,   i, newNeighborTimeCreateds,   i+1, subject.theNeighborIdentifiers.length-i );
                    System.arraycopy( subject.theNeighborRoleTypes,      i, newNeighborRoleTypes,      i+1, subject.theNeighborIdentifiers.length-i );
                    System.arraycopy( subject.theNeighborRoleProperties, i, newNeighborRoleProperties, i+1, subject.theNeighborIdentifiers.length-i );
                    System.arraycopy( subject.theNeighborRoleAttributes, i, newNeighborRoleAttributes, i+1, subject.theNeighborIdentifiers.length-i );

                    subject.theNeighborIdentifiers    = newNeighborIdentifiers;
                    subject.theNeighborTimeCreateds   = newNeighborTimeCreateds;
                    subject.theNeighborRoleTypes      = newNeighborRoleTypes;
                    subject.theNeighborRoleProperties = newNeighborRoleProperties;
                    subject.theNeighborRoleAttributes = newNeighborRoleAttributes;

                    ret = i;

                    break;
                }
                if( neighborIdentifier.equals( subject.theNeighborIdentifiers[i] )) {
                    // found
                    ret = i;
                    break;
                }
            }
            if( ret == -1 ) {
                // ran out; need to add at the end
                subject.theNeighborIdentifiers    = ArrayHelper.append( subject.theNeighborIdentifiers,    neighborIdentifier,   AMeshObjectIdentifier.class );
                subject.theNeighborTimeCreateds   = ArrayHelper.append( subject.theNeighborTimeCreateds,   neighbor.getTimeCreated() );
                subject.theNeighborRoleTypes      = ArrayHelper.append( subject.theNeighborRoleTypes,      (RoleType [])   null, RoleType[].class );
                subject.theNeighborRoleProperties = ArrayHelper.append( subject.theNeighborRoleProperties, (PropertiesMap) null, PropertiesMap.class );
                subject.theNeighborRoleAttributes = ArrayHelper.append( subject.theNeighborRoleAttributes, (AttributesMap) null, AttributesMap.class );

                ret = subject.theNeighborIdentifiers.length-1;
            }
        }
        return ret;
    }

    /**
     * Remove a neighbor.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @return the index at which the neighbor has been removed
     * @throws NotRelatedException thrown if the subject and the neighbor aren't related
     */
    public int removeNeighbor(
            AMeshObject subject,
            AMeshObject neighbor )
        throws
            NotRelatedException
    {
        int index = determineRelationshipIndex( subject, neighbor.getIdentifier() );
        if( index < 0 ) {
            throw new NotRelatedException( subject, neighbor );
        }
        removeNeighbor( subject, index );
        return index;
    }

    /**
     * Remove a neighbor.
     *
     * @param subject the MeshObject in question
     * @param index the index of the to-be-removed neighbor
     */
    public void removeNeighbor(
            AMeshObject subject,
            int         index )
        throws
            NotRelatedException
    {
        subject.theNeighborIdentifiers    = ArrayHelper.remove( subject.theNeighborIdentifiers,    index, AMeshObjectIdentifier.class );
        subject.theNeighborTimeCreateds   = ArrayHelper.remove( subject.theNeighborTimeCreateds,   index );
        subject.theNeighborRoleTypes      = ArrayHelper.remove( subject.theNeighborRoleTypes,      index, RoleType[].class );
        subject.theNeighborRoleProperties = ArrayHelper.remove( subject.theNeighborRoleProperties, index, PropertiesMap.class );
        subject.theNeighborRoleAttributes = ArrayHelper.remove( subject.theNeighborRoleAttributes, index, AttributesMap.class );
    }

    /**
     * Remove all neighbors.
     *
     * @param subject the MeshObject in question
     */
    public void removeAllNeighbors(
            AMeshObject subject )
    {
        subject.theNeighborIdentifiers    = null;
        subject.theNeighborTimeCreateds   = null;
        subject.theNeighborRoleTypes      = null;
        subject.theNeighborRoleProperties = null;
        subject.theNeighborRoleAttributes = null;
    }

    /**
     * Append a RoleType to an existing RoleType array.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @param toAdd the RoleType to add
     * @throws NotRelatedException thrown if the subject and the neighbor aren't related
     */
    public void appendRoleType(
            AMeshObject subject,
            AMeshObject neighbor,
            RoleType    toAdd )
        throws
            NotRelatedException
    {
        appendRoleTypes( subject, neighbor, new RoleType[] { toAdd } );
    }

    /**
     * Append several RoleTypes to an existing RoleType array.
     *
     * @param subject the MeshObject in question
     * @param neighbor the neighbor
     * @param toAdd the RoleTypes to add
     * @throws NotRelatedException thrown if the subject and the neighbor aren't related
     */
    public void appendRoleTypes(
            AMeshObject subject,
            AMeshObject neighbor,
            RoleType [] toAdd )
        throws
            NotRelatedException
    {
        int index = determineRelationshipIndex( subject, neighbor.getIdentifier() );
        if( index < 0 ) {
            throw new NotRelatedException( subject, neighbor );
        }
        if( subject.theNeighborRoleTypes[index] == null ) {
            subject.theNeighborRoleTypes[index] = toAdd;
        } else {
            subject.theNeighborRoleTypes[index] = ArrayHelper.append( subject.theNeighborRoleTypes[index], toAdd, RoleType.class );
        }
    }

    /**
     * Remove a RoleType from an existing RoleType array.
     *
     * @param subject the MeshObject in question
     * @param neighbor identifier of the neighbor
     * @param toRemove the RoleType to remove
     * @throws NotRelatedException thrown if the subject and the neighbor aren't related
     */
    public void removeRoleType(
            AMeshObject subject,
            AMeshObject neighbor,
            RoleType    toRemove )
        throws
            NotRelatedException
    {
        int index = determineRelationshipIndex( subject, neighbor.getIdentifier() );
        if( index < 0 ) {
            throw new NotRelatedException( subject, neighbor );
        }
        subject.theNeighborRoleTypes[index] = ArrayHelper.remove( subject.theNeighborRoleTypes[index], toRemove, false, RoleType.class );

        if( subject.theNeighborRoleTypes[index].length == 0 ) {
            subject.theNeighborRoleTypes[index]      = null;
            subject.theNeighborRoleProperties[index] = null;

        } else if( subject.theNeighborRoleProperties[index] != null ) {
            // remove unnecessary role properties
            HashSet<PropertyType> remainingProperties = new HashSet<>();
            for( RoleType current : subject.theNeighborRoleTypes[index] ) {
                for( PropertyType current2 : current.getPropertyTypes() ) {
                    remainingProperties.add( current2 );
                }
            }
            HashMap<PropertyType,PropertyValue> removedProperties = new HashMap<>();

            for( Map.Entry<PropertyType,PropertyValue> current : subject.theNeighborRoleProperties[index].entrySet() ) {
                if( !remainingProperties.contains( current.getKey() )) {
                    removedProperties.put( current.getKey(), current.getValue() );
                }
            }
            for( PropertyType current : removedProperties.keySet() ) {
                subject.theNeighborRoleProperties[index].remove( current );
            }
            if( subject.theNeighborRoleProperties[index].isEmpty() ) {
                subject.theNeighborRoleProperties[index] = null;
            }
        }
    }

    /**
     * Purge the neighbor information if no RoleTypes or RoleAttributes are on a relationship.
     *
     * @param subject the MeshObject in question
     * @param index the index of the neighbor from the subject's point of view
     * @param neighbor the neighbor
     * @param neighborIndex the index of the subject from the neighbor's point of view
     * @return true if anything was purged
     */
    public boolean purge(
            AMeshObject subject,
            int         index,
            AMeshObject neighbor,
            int         neighborIndex )
    {
        if( subject.theNeighborRoleTypes[index] != null && subject.theNeighborRoleTypes[index].length > 0 ) {
            return false;
        }
        if( subject.theNeighborRoleAttributes[index] != null && !subject.theNeighborRoleAttributes[index].isEmpty()) {
            return false;
        }
        if( neighbor.theNeighborRoleTypes[neighborIndex] != null && neighbor.theNeighborRoleTypes[neighborIndex].length > 0 ) {
            return false;
        }
        if( neighbor.theNeighborRoleAttributes[neighborIndex] != null && !neighbor.theNeighborRoleAttributes[neighborIndex].isEmpty()) {
            return false;
        }

        removeNeighbor( subject,  index );
        removeNeighbor( neighbor, neighborIndex );

        return true;
    }

    /**
     * This is invoked by the Transaction that has just fixed the timeCreated timestamp of a MeshObject
     * that was both 1) created during the transaction, and 2) became one of our neighbors. For those
     * implementations of neighbor management that order by timeCreated, this means the timeCreated now
     * needs to updated / the order of neighbors be reshuffled.
     *
     * @param subject the MeshObject in question
     * @param neighborUpdated the neighbor whose timeCreated was updated
     */
    public void neighborTimeCreatedWasUpdated(
            AMeshObject subject,
            AMeshObject neighborUpdated )
    {
        AMeshObjectIdentifier neighborIdentifier = neighborUpdated.getIdentifier();
        long                  neighborCreated    = neighborUpdated.getTimeCreated();

        boolean fixed = false; // just to be paranoid

        // the neighbor should be towards the end, as it was inserted with timeCreated of -1 ("oldest possible")
        // so we iterate from the end towards the front
        outer:
        for( int currentNeighborIndex=subject.theNeighborIdentifiers.length-1 ; currentNeighborIndex>=0 ; --currentNeighborIndex ) {
            if( neighborIdentifier.equals( subject.theNeighborIdentifiers[currentNeighborIndex] )) {
                for( int youngerNeighborIndex=0 ; youngerNeighborIndex<subject.theNeighborIdentifiers.length ; ++youngerNeighborIndex ) {
                    // we may be able to stop at currentNeighborIndex, instead of the end of the array, but we
                    // want to be defensive, in case the timeCreated became older (not sure why that would be)
                    if( neighborCreated >= subject.theNeighborTimeCreateds[youngerNeighborIndex] ) {
                        // if the updated neighbor is younger than then current (its creation time is larger)
                        fixed = true;
                        if( currentNeighborIndex == youngerNeighborIndex ) {
                            // surprisingly, we are in the right place
                            subject.theNeighborTimeCreateds[currentNeighborIndex] = neighborCreated;
                            break outer;
                        }
                        RoleType []   neighborUpdatedRoleTypes = subject.theNeighborRoleTypes[currentNeighborIndex];
                        PropertiesMap neighborRoleProperties   = subject.theNeighborRoleProperties[currentNeighborIndex];
                        AttributesMap neighborRoleAttributes   = subject.theNeighborRoleAttributes[currentNeighborIndex];

                        System.arraycopy( subject.theNeighborIdentifiers,    youngerNeighborIndex, subject.theNeighborIdentifiers,    youngerNeighborIndex+1, currentNeighborIndex-youngerNeighborIndex );
                        System.arraycopy( subject.theNeighborTimeCreateds,   youngerNeighborIndex, subject.theNeighborTimeCreateds,   youngerNeighborIndex+1, currentNeighborIndex-youngerNeighborIndex );
                        System.arraycopy( subject.theNeighborRoleTypes,      youngerNeighborIndex, subject.theNeighborRoleTypes,      youngerNeighborIndex+1, currentNeighborIndex-youngerNeighborIndex );
                        System.arraycopy( subject.theNeighborRoleProperties, youngerNeighborIndex, subject.theNeighborRoleProperties, youngerNeighborIndex+1, currentNeighborIndex-youngerNeighborIndex );
                        System.arraycopy( subject.theNeighborRoleAttributes, youngerNeighborIndex, subject.theNeighborRoleAttributes, youngerNeighborIndex+1, currentNeighborIndex-youngerNeighborIndex );

                        subject.theNeighborIdentifiers[youngerNeighborIndex]    = neighborIdentifier;
                        subject.theNeighborTimeCreateds[youngerNeighborIndex]   = neighborCreated;
                        subject.theNeighborRoleTypes[youngerNeighborIndex]      = neighborUpdatedRoleTypes;
                        subject.theNeighborRoleProperties[youngerNeighborIndex] = neighborRoleProperties;
                        subject.theNeighborRoleAttributes[youngerNeighborIndex] = neighborRoleAttributes;

                        break outer;
                    }
                }
            }
        }
        if( !fixed ) {
            log.error( "Could not reorder neighbor sequence" );
        }
    }

    /**
     * Singleton instance of this class.
     */
    public static final AMeshObjectNeighborManager SINGLETON = new AMeshObjectNeighborManager();
}
