//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.text;

import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies MeshObjectIdentifiers.
 */
public class MeshObjectIdentifierStringifier
        extends
            AbstractStringifier<MeshObjectIdentifier>
{
    /**
     * Factory method.
     *
     * @return the created MeshObjectStringifier
     */
    public static MeshObjectIdentifierStringifier create()
    {
        return new MeshObjectIdentifierStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected MeshObjectIdentifierStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            MeshObjectIdentifier  arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = ExternalNameHashMeshObjectIdentifierSerializer.SINGLETON.toExternalForm( arg );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (MeshObjectIdentifier) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }
}
