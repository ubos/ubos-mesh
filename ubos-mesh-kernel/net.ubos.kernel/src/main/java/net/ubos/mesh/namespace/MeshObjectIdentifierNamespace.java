//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace;

import java.util.Set;

/**
 * <p>A namespace for MeshObjectIdentifiers.
 *
 * <p>A MeshObjectIdentifierNamespace can be known by many external names, such as
 * http://example.com and =example. External names are used to identify a namespace
 * across the network. All external names of a MeshObjectIdentifierNamespace are
 * synonyms, referring to the same MeshObjectIdentifierNamespace. One of these
 * external names is the preferred one, and used for serialization of a MeshObjectIdentifier
 * for purposes of communication across the network. Over time, the set of external names
 * for a given MeshObjectIdentifierNamespace may change, while conceptually remaining
 * to be the same MeshObjectIdentifierNamespace. For example, a user may move their
 * MeshBase from http://example.com to https://example.net.
 */
public interface MeshObjectIdentifierNamespace
    extends
        Comparable<MeshObjectIdentifierNamespace>
{
    /**
     * Obtain the known external names for this MeshObjectIdentifierNamespace.
     *
     * @return the set of external names, as immutable set
     */
    public Set<String> getExternalNames();

    /**
     * Obtain the preferred external name for this MeshObjectIdentifierNamespace.
     * The value returned here is also found in the set returned by
     * {@link #getExternalNames()}.
     *
     * @return the default external name
     */
    public String getPreferredExternalName();

    /**
     * Set the preferred external name for this MeshObjectIdentifierNamespace.
     * This external name must already be one of the assigned external names.
     *
     * @param externalName the external name
     */
    public void setPreferredExternalName(
            String externalName );

    /**
     * Add an extra external name to this MeshObjectIdentifierNamespace.
     *
     * @param externalName the external name
     */
    public void addExternalName(
            String externalName );

    /**
     * Remove an existing external name from this MeshObjectIdentifierNamespace.
     *
     * @param externalName the external name
     */
    public void removeExternalName(
            String externalName );

    /**
     * Defines a consistent ordering of namespaces.
     *
     * @param other the other MeshObjectIdentifierNamespace to compare to
     * @return strcmp-style value
     */
    public int compare(
            MeshObjectIdentifierNamespace other );
}
