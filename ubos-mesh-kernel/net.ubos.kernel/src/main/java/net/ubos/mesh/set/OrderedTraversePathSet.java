//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.model.traverse.TraversePath;

/**
 * A TraversePathSet whose content is ordered according to some criteria.
 * Consequently, it provides methods that only are appropriate for OrderedTraversePathSets.
 */
public interface OrderedTraversePathSet
    extends
        TraversePathSet
{
    /**
     * Obtain a TraversePath at a particular index.
     *
     * @param index the index specifying the TraversePath that we are looking for
     * @return the TraversePath at this index
     */
    public abstract TraversePath getTraversePath(
            int index );

    /**
     * Determine the index of a certain TraversePath in this ordered set.
     * Generally, index == findIndexOf( TraversePath( index )).
     *
     * @param candidate the TraversePath that we are looking for in this set
     * @return the index of the found TraversePath, or -1 if not found
     */
    public abstract int findIndexOf(
            TraversePath candidate );

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract OrderedMeshObjectSet getDestinationsAsSet();

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract OrderedMeshObjectSet getStepAsSet(
            int index );

    /**
     * Obtain the maximum number of elements in ths set.
     * 
     * @return the maximum number of elements in the set, or UNLIMITED
     */
    public abstract int getMaximumElements();

    /**
     * Special code to specify "all levels" instead of a limited number.
     */
    public static final int UNLIMITED = -1;
}


