//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import java.util.HashSet;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.set.AbstractMeshObjectSetFactory;
import net.ubos.mesh.set.CompositeImmutableMeshObjectSet;
import net.ubos.mesh.set.ImmutableMeshObjectSet;
import net.ubos.mesh.set.MeshObjectSelector;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.MeshObjectSorter;
import net.ubos.mesh.set.OrderedImmutableMeshObjectSet;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.model.traverse.TraversePath;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.util.ArrayHelper;
import net.ubos.mesh.set.TraversePathSet;
import net.ubos.mesh.set.TraversePathSorter;

/**
 * An MeshObjectSetFactory that creates in-memory, immutable MeshObjectSets.
 * The method setMeshBase() must be called after the constructor.
 */
public class ImmutableMMeshObjectSetFactory
        extends
            AbstractMeshObjectSetFactory
{
    /**
     * Default factory method for the factory itself.
     *
     * @return the created ImmutableMMeshObjectSetFactory
     */
    public static ImmutableMMeshObjectSetFactory create()
    {
        return new ImmutableMMeshObjectSetFactory( MeshObject.class, MeshObjectIdentifier.class );
    }

    /**
     * Factory method for the factory itself.
     *
     * @param componentClass           the Class to use to allocate arrays of MeshObjects
     * @param componentIdentifierClass the Class to use to allocate arrays of MeshObjectIdentifiers
     * @return the created ImmutableMMeshObjectSetFactory
     */
    public static ImmutableMMeshObjectSetFactory create(
            Class<? extends MeshObject>           componentClass,
            Class<? extends MeshObjectIdentifier> componentIdentifierClass )
    {
        return new ImmutableMMeshObjectSetFactory( componentClass, componentIdentifierClass );
    }

    /**
     * Constructor.
     *
     * @param componentClass           the Class to use to allocate arrays of MeshObjects
     * @param componentIdentifierClass the Class to use to allocate arrays of MeshObjectIdentifiers
     */
    protected ImmutableMMeshObjectSetFactory(
            Class<? extends MeshObject>           componentClass,
            Class<? extends MeshObjectIdentifier> componentIdentifierClass )
    {
        super( componentClass, componentIdentifierClass );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMMeshObjectSet createImmutableMeshObjectSet(
            MeshObject [] members )
    {
        return (ImmutableMMeshObjectSet) super.createImmutableMeshObjectSet( members );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMMeshObjectSet createImmutableMeshObjectSet(
            MeshObjectSet      input,
            MeshObjectSelector selector )
    {
        return (ImmutableMMeshObjectSet) super.createImmutableMeshObjectSet( input, selector );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet [] operands )
    {
        return (CompositeImmutableMMeshObjectSet) super.createImmutableMeshObjectSetUnification( operands );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet one,
            MeshObjectSet two )
    {
        return (CompositeImmutableMMeshObjectSet) super.createImmutableMeshObjectSetUnification( one, two );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet one,
            MeshObject    two )
    {
        return (CompositeImmutableMMeshObjectSet) super.createImmutableMeshObjectSetUnification( one, two );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet [] operands )
    {
        return (CompositeImmutableMMeshObjectSet) super.createImmutableMeshObjectSetIntersection( operands );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet one,
            MeshObjectSet two )
    {
        return (CompositeImmutableMMeshObjectSet) super.createImmutableMeshObjectSetIntersection( one, two );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet one,
            MeshObject    two )
    {
        return (CompositeImmutableMMeshObjectSet) super.createImmutableMeshObjectSetIntersection( one, two );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMMeshObjectSet createImmutableMeshObjectSetMinus(
            MeshObjectSet one,
            MeshObjectSet two )
    {
        return (CompositeImmutableMMeshObjectSet) super.createImmutableMeshObjectSetMinus( one, two );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMeshObjectSet createSingleMemberImmutableMeshObjectSet(
            MeshObject member )
    {
        return new OrderedImmutableMMeshObjectSet( this, new MeshObject[] { member } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObject []      candidates,
            MeshObjectSelector selector )
    {
        // check for duplicates first -- this is a time-critical section
        HashSet<MeshObjectIdentifier> temp = new HashSet<>( candidates.length );
        for( int i=0 ; i<candidates.length ; ++i ) {
            if( candidates[i] == null ) {
                throw new IllegalArgumentException( "Cannot add a null object to a MeshObjectSet" );
            }
            if( candidates[i].getIsDead() ) {
                throw new IllegalArgumentException( "Cannot add a dead object to a MeshObjectSet: " + candidates[i] );
            }
            MeshObjectIdentifier identifier = candidates[i].getIdentifier();
            if( temp.contains( identifier )) {
                throw new IllegalArgumentException( "Cannot create a MeshObjectSet with duplicate members: " + candidates[i] );
            }
            temp.add( identifier );
        }

        MeshObject [] content;

        if( selector != null ) {
            int count = 0;
            content = (MeshObject []) ArrayHelper.createArray( theComponentClass, candidates.length );
            for( int i=0 ; i<candidates.length ; ++i ) {
                if( selector.accepts( candidates[i] )) {
                    content[count++] = candidates[i];
                }
            }
            if( count < content.length ) {
                content = ArrayHelper.copyIntoNewArray( content, 0, count, theComponentClass );
            }

        } else {
            content = ArrayHelper.copyIntoNewArray( candidates, theComponentClass );
        }

        ImmutableMeshObjectSet ret = new ImmutableMMeshObjectSet( this, content );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet []   operands,
            MeshObjectSelector selector )
    {
        if( operands.length < 1 ) {
            throw new IllegalArgumentException();
        }
        MeshBaseView mbv = null;
        for( int i=0 ; i<operands.length ; ++i ) {
            if( mbv == null ) {
                mbv = operands[i].getMeshBaseView();
                if( mbv != null && mbv.getMeshBase() != theMeshBase ) {
                    throw WrongMeshBaseViewException.create( theMeshBase, mbv );
                }
            } else if( mbv != operands[i].getMeshBaseView() ) {
                throw WrongMeshBaseViewException.create( mbv, operands[i].getMeshBaseView() );
            }
        }

        MeshObject [] content = unify( operands, selector );

        return new CompositeImmutableMMeshObjectSet.Unification( this, content, operands );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet []   operands,
            MeshObjectSelector selector )
    {
        if( operands.length < 1 ) {
            throw new IllegalArgumentException();
        }
        MeshBaseView mbv = null;
        for( int i=0 ; i<operands.length ; ++i ) {
           if( mbv == null ) {
                mbv = operands[i].getMeshBaseView();
                if( mbv != null && mbv.getMeshBase() != theMeshBase ) {
                    throw WrongMeshBaseViewException.create( theMeshBase, mbv );
                }
            } else if( mbv != operands[i].getMeshBaseView() ) {
                throw WrongMeshBaseViewException.create( mbv, operands[i].getMeshBaseView() );
            }
        }

        MeshObject [] content = intersect( operands, selector );

        return new CompositeImmutableMMeshObjectSet.Intersection( this, content, operands );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetMinus(
            MeshObjectSet      one,
            MeshObjectSet      two,
            MeshObjectSelector selector )
    {
        MeshBaseView mbv = one.getMeshBaseView();
        if( mbv != null ) {
            if( mbv.getMeshBase() != theMeshBase ) {
                throw WrongMeshBaseViewException.create( theMeshBase, mbv );
            }
            if( mbv != two.getMeshBaseView() ) {
                throw WrongMeshBaseViewException.create( mbv, two.getMeshBaseView() );
            }
        }

        MeshObject [] oneContent = one.getMeshObjects();
        MeshObject [] result     = (MeshObject []) ArrayHelper.createArray( theComponentClass, oneContent.length );

        int count = 0;
        for( int i=0 ; i<oneContent.length ; ++i ) {
            if( !two.contains( oneContent[i] )) {
                if( selector == null || selector.accepts( oneContent[i] )) {
                    result[ count++ ] = oneContent[i];
                }
            }
        }
        if( count < result.length ) {
            result = ArrayHelper.copyIntoNewArray( result, 0, count, theComponentClass );
        }
        return new CompositeImmutableMMeshObjectSet.Minus( this, result, new MeshObjectSet[] { one, two } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObject [] contentInOrder )
    {
        return createOrderedImmutableMeshObjectSet( contentInOrder, (MeshObjectSelector) null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObject []      candidatesInOrder,
            MeshObjectSelector selector )
    {
        // check for duplicates first
        for( int i=0 ; i<candidatesInOrder.length ; ++i ) {
            if( candidatesInOrder[i] == null ) {
                throw new IllegalArgumentException( "Cannot add a null object to a MeshObjectSet" );
            }
            if( candidatesInOrder[i].getIsDead() ) {
                throw new IllegalArgumentException( "Cannot add a dead object to a MeshObjectSet: " + candidatesInOrder[i] );
            }
            for( int j=0 ; j<i ; ++j ) {
                if( candidatesInOrder[i] == candidatesInOrder[j] ) {
                    throw new IllegalArgumentException( "Cannot create a MeshObjectSet with duplicate members: " + candidatesInOrder[i] );
                }
            }
        }

        MeshObject [] content;

        if( selector != null ) {
            int count = 0;
            content = (MeshObject []) ArrayHelper.createArray( theComponentClass, candidatesInOrder.length );
            for( int i=0 ; i<candidatesInOrder.length ; ++i ) {
                if( selector.accepts( candidatesInOrder[i] )) {
                    content[count++] = candidatesInOrder[i];
                }
            }
            if( count < content.length ) {
                content = ArrayHelper.copyIntoNewArray( content, 0, count, theComponentClass );
            }

        } else {
            content = ArrayHelper.copyIntoNewArray( candidatesInOrder, theComponentClass );
        }

        OrderedImmutableMMeshObjectSet ret = new OrderedImmutableMMeshObjectSet( this, content );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObject []    content,
            MeshObjectSorter sorter )
    {
        return new OrderedImmutableMMeshObjectSet(
                this,
                sorter.getOrderedInNew( content) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObjectSet    content,
            MeshObjectSorter sorter )
    {
        // I was thinking of sorting only when the content is actually requested, but
        // it's unlikely the user will request an ordered set and then not use it, so that

        return new OrderedImmutableMMeshObjectSet(
                this,
                sorter.getOrderedInNew( content.getMeshObjects() ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObject             startObject,
            TraverseSpecification specification )
    {
        MeshObjectSet ret = startObject.traverse( specification );

        if( ret.getFactory() != this ) {
            return (ImmutableMeshObjectSet) ret;
        } else {
            // happens if two MeshObjectSetFactories are in use
            return createImmutableMeshObjectSet( ret.getMeshObjects() );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMMeshObjectSet createImmutableMeshObjectSet(
            MeshObjectSet          startSet,
            TraverseSpecification specification )
    {
        MeshObjectSet ret = startSet.traverse( specification );

        if( ret.getFactory() != this ) {
            return (ImmutableMMeshObjectSet) ret;
        } else {
            // happens if two MeshObjectSetFactories are in use
            return createImmutableMeshObjectSet( ret.getMeshObjects() );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMTraversePathSet createSingleMemberImmutableTraversePathSet(
            TraversePath singleMember )
    {
        return new OrderedImmutableMTraversePathSet( this, new TraversePath[] { singleMember }, 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMTraversePathSet createImmutableTraversePathSet(
            TraversePath [] content )
    {
        return new ImmutableMTraversePathSet( this, content );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMTraversePathSet createImmutableTraversePathSet(
            TraverseSpecification spec,
            MeshObjectSet          set )
    {
        if( theMeshBase != set.getMeshBaseView().getMeshBase() ) {
            throw WrongMeshBaseViewException.create( theMeshBase, set.getMeshBaseView() );
        }

        MeshObject   [] res     = set.getMeshObjects();
        TraversePath [] content = new TraversePath[ res.length ];
        for( int i=0 ; i<content.length ; ++i ) {
            content[i] = TraversePath.create( spec, res[i] );
        }

        return new ImmutableMTraversePathSet( this, content );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMTraversePathSet createImmutableTraversePathSet(
            MeshObject            start,
            TraverseSpecification spec )
    {
        return createImmutableTraversePathSet( spec, start.traverse( spec ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMTraversePathSet createImmutableTraversePathSet(
            MeshObjectSet          startSet,
            TraverseSpecification spec )
    {
        return createImmutableTraversePathSet( spec, startSet.traverse( spec ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMTraversePathSet createImmutableTraversePathSet(
            TraversePathSet       startSet,
            TraverseSpecification spec )
    {
        TraversePath []  oldPaths = startSet.getTraversePaths();
        MeshObjectSet [] newSets  = new MeshObjectSet[ oldPaths.length ];

        int count = 0;
        for( int i=0 ; i<oldPaths.length ; ++i ) {
            newSets[i] = oldPaths[i].getLastMeshObject().traverse( spec );

            count += newSets[i].size();
        }

        TraversePath [] newPaths = new TraversePath[ count ];
        int count2 = 0;
        for( int i=0 ; i<oldPaths.length ; ++i ) {
            for( int j=0 ; j<newSets[i].size() ; ++j ) {
                newPaths[ count2++ ] = TraversePath.create( newPaths[i], spec, newSets[i].get( j ));
            }
        }
        return createImmutableTraversePathSet( newPaths );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMTraversePathSet createOrderedImmutableTraversePathSet(
            TraversePathSet    content,
            TraversePathSorter sorter )
    {
        return createOrderedImmutableTraversePathSet( content, sorter, Integer.MAX_VALUE );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMTraversePathSet createOrderedImmutableTraversePathSet(
            TraversePathSet    content,
            TraversePathSorter sorter,
            int                 max )
    {
        TraversePath [] sorted = sorter.getOrderedInNew( content.getTraversePaths() );

        return new OrderedImmutableMTraversePathSet( this, sorted, max );
    }

    /**
     * Default instance.
     */
    public static final ImmutableMMeshObjectSetFactory SINGLETON = ImmutableMMeshObjectSetFactory.create();

    /**
     * Default empty set.
     */
    public static final ImmutableMMeshObjectSet EMPTY_SET = new ImmutableMMeshObjectSet( SINGLETON, new MeshObject[0] );
}
