//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

//
// Copyright (C) Johannes Ernst. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;

/**
 * Knows how to serialize MeshObjectIdentifiers in the hash notation,
 * using the preferred external name for the MeshObjectIdentifierNamespace.
 */
public class ExternalNameHashMeshObjectIdentifierSerializer
    extends
        AbstractHashMeshObjectIdentifierSerializer
{
    /**
     * Singleton instance.
     */
    public static final ExternalNameHashMeshObjectIdentifierSerializer SINGLETON
            = new ExternalNameHashMeshObjectIdentifierSerializer();

    /**
     * Constructor.
     */
    protected ExternalNameHashMeshObjectIdentifierSerializer()
    {}

     /**
     * {@inheritDoc}
     */
    @Override
    protected String determineNamespaceString(
            MeshObjectIdentifierNamespace ns )
    {
        return ns.getPreferredExternalName();
    }
}
