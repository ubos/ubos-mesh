//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.text.ParseException;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;

/**
 * Factory for MeshObjectIdentifiers. A MeshObjectIdentifierFactory is always associated with
 * a single MeshBase.
 */
public interface MeshObjectIdentifierFactory
{
    /**
     * Obtain the default MeshObjectIdentifierNamespace in which MeshObjectIdentifiers are allocated
     * if no other MeshObjectIdentifierNamespace is specified.
     *
     * @return the default MeshObjectIdentifierNamespace
     */
    public abstract MeshObjectIdentifierNamespace getDefaultNamespace();

    /**
     * Obtain the map of all MeshObjectIdentifierNamespaces we are aware of.
     *
     * @return the PrimaryMeshObjectIdentifierNamespaceMap
     */
    public abstract PrimaryMeshObjectIdentifierNamespaceMap getPrimaryNamespaceMap();

    /**
     * Obtain the MeshBase that this MeshObjectIdentifierFactory works on.
     *
     * @return the MeshBase that this MeshObjectIdentifierFactory on
     */
    public abstract MeshBase getMeshBase();

    /**
     * Determine the MeshObjectIdentifier of the Home MeshObject of this MeshBase.
     *
     * @return the Identifier
     */
    public abstract MeshObjectIdentifier getHomeMeshObjectIdentifier();

    /**
     * Create a unique, random MeshObjectIdentifier in the default namespace.
     *
     * @return the created Identifier
     */
    public abstract MeshObjectIdentifier createRandomMeshObjectIdentifier();

    /**
     * Create a MeshObjectIdentifier which has localId in the default namespace.
     *
     * @param localId the local Id in the namespace
     * @return the created MeshObjectIdentifier
     */
    public abstract MeshObjectIdentifier createMeshObjectIdentifier(
            String localId );

    /**
     * Determine the MeshObjectIdentifier of the Home MeshObject in this MeshObjectIdentifierNamespace.
     *
     * @param ns the namespace
     * @return the Identifier
     */
    public abstract MeshObjectIdentifier getHomeObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns );

    /**
     * Create a unique, random MeshObjectIdentifier in the provided MeshObjectIdentifierNamespace.
     *
     * @param ns the namespace
     * @return the created Identifier
     */
    public abstract MeshObjectIdentifier createRandomMeshObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns );

    /**
     * Create a MeshObjectIdentifier which has localId in the provided MeshObjectIdentifierNamespace.
     *
     * @param ns the namespace
     * @param localId the local Id in the namespace
     * @return the created MeshObjectIdentifier
     */
    public abstract MeshObjectIdentifier createMeshObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns,
            String                        localId );

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a trailing String. This String must not contain a slash; use the method takes takes an
     * array instead.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append the String to append
     * @return the created MeshObjectIdentifier
     * @throws ParseException thrown if the append String was invalid
     */
    public abstract MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String               append )
        throws
            ParseException;

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending one or more trailing Strings. None of the provided Strings may contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append the Strings to append
     * @return the created MeshObjectIdentifier
     * @throws ParseException thrown if the append String was invalid
     */
    public abstract MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String []            append )
        throws
            ParseException;

   /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a trailing String and potentially another sequence of trailing Strings.
     * Convenience method that allows providing the context MeshObjectIdentifier in String form.
     * None of the provided Strings may contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append the String to append
     * @param appendMore more Strings to append
     * @return the created MeshObjectIdentifier
     * @throws ParseException thrown if the append or appendMore String was invalid
     */
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String               append,
            String ...           appendMore )
        throws
            ParseException;
    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a random trailing String.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @return the created Identifier
     */
    public abstract MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier );

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a sequence of Strings and then a random trailing String.
     * None of the provided Strings must contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append Strings to append
     * @return the created Identifier
     * @throws ParseException thrown if the append String was invalid
     */
    public abstract MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String ...           append )
        throws
            ParseException;
}
