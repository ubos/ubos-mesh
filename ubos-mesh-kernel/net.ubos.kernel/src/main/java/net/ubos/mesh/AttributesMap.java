//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A Java workaround to avoid creation of generic arrays.
 * Public so subclasses in other packages can use it.
 */
public class AttributesMap
    extends
        HashMap<String,Serializable>
{
    /**
     * Create a copy of this object.
     *
     * @return the clone
     */
    @Override
    public AttributesMap clone()
    {
        AttributesMap ret = new AttributesMap();
        for( Map.Entry<String,Serializable> current : this.entrySet() ) {
            ret.put( current.getKey(), current.getValue() );
        }
        return ret;
    }

    /**
     * Create a copy of all objects in this array.
     *
     * @param original the array to be copied
     * @return copy
     */
    public static AttributesMap [] cloneArray(
            AttributesMap [] original )
    {
        if( original == null ) {
            return null;
        }
        AttributesMap [] ret = new AttributesMap[ original.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            AttributesMap current = original[i];
            if( current != null ) {
                ret[i] = current.clone();
            }
        }
        return ret;
    }
}
