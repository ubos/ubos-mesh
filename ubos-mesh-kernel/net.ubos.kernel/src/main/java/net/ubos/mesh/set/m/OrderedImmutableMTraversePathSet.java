//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.OrderedImmutableTraversePathSet;
import net.ubos.mesh.set.OrderedMeshObjectSet;
import net.ubos.model.traverse.TraversePath;

/**
 * This TraversalPathSet has the same content as a passed-in TraversalPathSet,
 * but in an order specified by a passed-in sorting algorithm. This set is
 * immutable. It is kept in memory.
 */
public class OrderedImmutableMTraversePathSet
    extends
        ImmutableMTraversePathSet
    implements
        OrderedImmutableTraversePathSet
{
    /**
     * Private constructor, use factory method. Note that the content must
     * have been limited to the maximum allowed number by the caller of the constructor.
     *
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     * @param orderedContent the content of this set in order
     * @param max the maximum number of elements in the set
     */
    protected OrderedImmutableMTraversePathSet(
            MeshObjectSetFactory factory,
            TraversePath []      orderedContent,
            int                  max )
    {
        super( factory, orderedContent );
        
        theMaximum = max;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TraversePath getTraversePath(
            int index )
    {
        return currentContent[ index ];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int findIndexOf(
            TraversePath candidate )
    {
        for( int i=0 ; i<currentContent.length ; ++i ) {
            if( candidate == currentContent[i] ) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Obtain the destinations of the contained TraversalPaths as a MeshObjectSet.
     * While the same MeshObject may be a destination of more than one contained
     * TraversalPath, the MeshObjectSet naturally only contains this MeshObject once.
     *
     * @return the destinations of the contained TraversalPaths as a MeshObjectSet
     */
    @Override
    public OrderedMeshObjectSet getDestinationsAsSet()
    {
        return (OrderedMeshObjectSet) super.getDestinationsAsSet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedMeshObjectSet getStepAsSet(
            int index )
    {
        return (OrderedMeshObjectSet) super.getStepAsSet( index );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getMaximumElements()
    {
        return theMaximum;
    }
    
    /**
     * The maximum number of elements in the set.
     */
    protected int theMaximum;
}
