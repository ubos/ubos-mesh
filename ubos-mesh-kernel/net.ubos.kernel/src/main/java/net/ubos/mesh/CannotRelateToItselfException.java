//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Thrown if an attempt is made to relate a MeshObject to itself.
 */
public class CannotRelateToItselfException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param meshObject the MeshObject
     */
    public CannotRelateToItselfException(
            MeshObject meshObject )
    {
        super( meshObject );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject"
                },
                new Object[] {
                    theMeshObject
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject };
    }
}
