//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * This Exception is thrown when there is an attempt to access
  * a RoleProperty that cannot exist on the Relationship between this MeshObject
  * and a neighbor MeshObject, because the Relationship  has not been blessed with
  * a RelationshipType one of whose RoleTypes provides this PropertyType.
  */
public class IllegalRolePropertyTypeException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject whose Roles with the neighbor MeshObject did not have the PropertyType
     * @param neighbor the neighbor MeshObject
     * @param pt the PropertyType that did not exist
     */
    public IllegalRolePropertyTypeException(
            MeshObject   obj,
            MeshObject   neighbor,
            PropertyType pt )
    {
        super( obj );

        theNeighbor     = neighbor;
        thePropertyType = pt;
    }

    /**
     * Obtain the neighbor MeshObject.
     *
     * @return the neighbor MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * Obtain the PropertyType that did not exist.
     *
     * @return the PropertyType
     */
    public PropertyType getPropertyType()
    {
        return thePropertyType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshBaseView",
                    "meshObject",
                    "neighbor",
                    "propertyType",
                    "types"
                },
                new Object[] {
                    theMeshBaseView,
                    theMeshObject,
                    theNeighbor.getIdentifier(),
                    thePropertyType,
                    MeshTypeUtils.meshTypeIdentifiersOrNull( theMeshObject )
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        try {
            RoleType []           types   = theMeshObject.getRoleTypes( theNeighbor );
            MeshTypeIdentifier [] typeIds = new MeshTypeIdentifier[ types.length ];

            for( int i=0 ; i<types.length ; ++i ) {
                typeIds[i] = types[i].getIdentifier();
            }

            return new Object[] { theMeshObject, theNeighbor.getIdentifier(), thePropertyType, typeIds };

        } catch( IsDeadException ex ) {
            return new Object[] { theMeshObject, theNeighbor.getIdentifier(), thePropertyType, new String[] { "<unknown, MeshObject is dead>" }};
        }
    }

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * The PropertyType that did not exist.
     */
    protected final PropertyType thePropertyType;
}
