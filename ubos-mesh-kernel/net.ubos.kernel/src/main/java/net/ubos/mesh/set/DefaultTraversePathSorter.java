//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import java.util.Arrays;
import java.util.Comparator;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.traverse.TraversePath;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StringHelper;

/**
 * A default TraversePathSorter implementation leveraging the Java collections API.
 */
public class DefaultTraversePathSorter
        implements
            TraversePathSorter
{
    /**
     * Construct one. Specify the comparison criteria like the Java collections API does.
     *
     * @param c the Comparator defining the sorting criteria
     */
    public DefaultTraversePathSorter(
            Comparator<TraversePath> c )
    {
        this( c, null );
    }

    /**
     * Construct one. Specify the comparison criteria like the Java collections API does.
     *
     * @param c the Comparator defining the sorting criteria
     * @param userName a user-visible name for this object
     */
    public DefaultTraversePathSorter(
            Comparator<TraversePath> c,
            String                    userName )
    {
        theComparator  = c;
        theUserName    = userName;
    }

    /**
     * Obtain the Comparator that determines the ordering.
     *
     * @return the Comparator that determines the ordering
     */
    public Comparator<TraversePath> getComparator()
    {
        return theComparator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TraversePath [] getOrderedInSame(
            TraversePath [] unorderedSet )
    {
        Arrays.sort( unorderedSet, theComparator );
        return unorderedSet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TraversePath [] getOrderedInNew(
            TraversePath [] unorderedSet )
    {
        TraversePath [] unorderedSet2 = ArrayHelper.copyIntoNewArray( unorderedSet, TraversePath.class );

        return getOrderedInSame( unorderedSet2 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIndexOfNew(
            TraversePath    newObject,
            TraversePath [] orderedSet )
    {
        int index = Arrays.binarySearch( orderedSet, newObject, theComparator );
        if( index >= 0 ) {
            return index;
        } else {
            return -index-1;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIndexOf(
            TraversePath    objectToLookFor,
            TraversePath [] orderedSet )
    {
        int index = Arrays.binarySearch( orderedSet, objectToLookFor, theComparator );
        if( index >= 0 ) {
            return index;
        } else {
            return -1;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserName()
    {
        return theUserName;
    }

    /**
     * The Comparator that determines the sort order.
     */
    protected Comparator<TraversePath> theComparator;

    /**
     * The user-visible name for this TraversePathSorter.
     */
    protected String theUserName;

    /**
     * An abstract superclass for the Comparators defined in this file.
     */
    public static abstract class AbstractComparator
        implements
            Comparator<TraversePath>
    {
        /**
         * Constructor.
         *
         * @param index the index of the MeshObject in the TraversePath that we compare. -1 indicates the last MeshObject in the TraversePath
         */
        public AbstractComparator(
               int index )
        {
            theIndex = index;
        }

        /**
         * Helper method to find the right MeshObject given the parameters
         * passed into the compare method.
         *
         * @param o the TraversePath, as Object
         * @return the found MeshObject
         */
        protected MeshObject getMeshObjectFrom(
                TraversePath o )
        {
            if( theIndex == -1 ) {
                return o.getLastMeshObject();
            } else {
                return o.getMeshObjectAt( theIndex );
            }
        }

        /**
         * The index in the TrraversalPath where we compare.
         */
        protected int theIndex;
    }

    /**
     * A pre-defined Comparator class to compare by update time.
     */
    public static class UpdateTimeComparator
        extends
            AbstractComparator
    {
        /**
         * Constructor.
         *
         * @param index the index of the MeshObject in the TraversePath that we compare. -1 indicates the last MeshObject in the TraversePath
         */
        public UpdateTimeComparator(
               int index )
        {
            super( index );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(
                TraversePath one,
                TraversePath two )
        {
            long oneValue = getMeshObjectFrom( one ).getTimeUpdated();
            long twoValue = getMeshObjectFrom( two ).getTimeUpdated();

            if( oneValue < twoValue ) {
                return -1;
            } else if( oneValue == twoValue ) {
                return 0;
            } else {
                return +1;
            }
        }
    }

    /**
     * A pre-defined Comparator class to compare by creation time.
     */
    public static class CreationTimeComparator
        extends
            AbstractComparator
    {
        /**
         * Constructor.
         *
         * @param index the index of the MeshObject in the TraversePath that we compare. -1 indicates the last MeshObject in the TraversePath
         */
        public CreationTimeComparator(
               int index )
        {
            super( index );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(
                TraversePath one,
                TraversePath two )
        {
            long oneValue = getMeshObjectFrom( one ).getTimeCreated();
            long twoValue = getMeshObjectFrom( two ).getTimeCreated();

            if( oneValue < twoValue ) {
                return -1;
            } else if( oneValue == twoValue ) {
                return 0;
            } else {
                return +1;
            }
        }
    }

    /**
     * A pre-defined Comparator class to compare by the value of a certain PropertyType on both
 compared objects.
     */
    public static class ByPropertyComparator
        extends
            AbstractComparator
    {
        /**
         * Constructor.
         *
         * @param index the index of the MeshObject in the TraversePath that we compare. -1 indicates the last
         *        MeshObject in the TraversePath
         * @param pt the PropertyType that indicates which properties we compare
         */
        public ByPropertyComparator(
               int          index,
               PropertyType pt )
        {
            super( index );

            thePropertyType = pt;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(
                TraversePath one,
                TraversePath two )
        {
            try {
                PropertyValue oneValue = getMeshObjectFrom( one ).getPropertyValue( thePropertyType );
                PropertyValue twoValue = getMeshObjectFrom( two ).getPropertyValue( thePropertyType );

                return PropertyValue.compare( oneValue, twoValue );

            } catch( IllegalPropertyTypeException ex ) {
                return 0;

            } catch( NotPermittedException ex ) {
                return 0;
            }
        }

        /**
         * The PropertyType by which we compare.
         */
        protected PropertyType thePropertyType;
    }

    /**
     * A pre-defined Comparator class to compare by the the user-visibile String of two MeshObjects.
     */
    public static class ByUserVisibleStringComparator
        extends
            AbstractComparator
    {
        /**
         * Constructor.
         *
         * @param index the index of the MeshObject in the TraversePath that we compare. -1 indicates the last
         *        MeshObject in the TraversePath
         */
        public ByUserVisibleStringComparator(
               int index )
        {
            super( index );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(
                TraversePath one,
                TraversePath two )
        {
            MeshObject objectOne = getMeshObjectFrom( one );
            MeshObject objectTwo = getMeshObjectFrom( two );

            String valueOne = objectOne.getUserVisibleString();
            String valueTwo = objectTwo.getUserVisibleString();

            if( valueOne == null && valueTwo == null ) {
                valueOne = DefaultMeshObjectSorter.theSortingOnlySerializer.toExternalForm( objectOne.getIdentifier());
                valueTwo = DefaultMeshObjectSorter.theSortingOnlySerializer.toExternalForm( objectTwo.getIdentifier());
            }
            int ret = StringHelper.compareTo( valueOne, valueTwo );
            return ret;
        }
    }
}

