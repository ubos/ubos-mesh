//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.EntityType;
import net.ubos.util.logging.Dumper;

/**
 * This Exception is thrown if a MeshObject was supposed to be blessed with an EntityType
 * that is declared as abstract.
 */
public class EntityTypeIsAbstractException
        extends
            IsAbstractException
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject that could not be blessed
     * @param type the EntityType that is abstract
     */
    public EntityTypeIsAbstractException(
            MeshObject obj,
            EntityType type )
    {
        super( obj, type );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType getAbstractMeshType()
    {
        return (EntityType) super.getAbstractMeshType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshBaseView",
                    "meshObject",
                    "type"
                },
                new Object[] {
                    theMeshBaseView,
                    theMeshObject,
                    theType
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theType };
    }
}
