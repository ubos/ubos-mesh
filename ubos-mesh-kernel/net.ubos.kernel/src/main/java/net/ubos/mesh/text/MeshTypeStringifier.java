//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.text;

import net.ubos.model.primitives.MeshType;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies MeshTypes, well, their identifiers.
 */
public class MeshTypeStringifier
        extends
            AbstractStringifier<MeshType>
{
    /**
     * Factory method.
     *
     * @return the created EnumeratedValueStringifier
     */
    public static MeshTypeStringifier create()
    {
        return new MeshTypeStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected MeshTypeStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            MeshType              arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON.toExternalForm( arg.getIdentifier() );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (MeshType) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshType unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }
}
