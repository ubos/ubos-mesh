//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.primitives.PropertyComparisonPredicate;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.Log;

/**
 * An implementation of MeshObjectSelector that accepts all MeshObjects which have a property
 * that compares successfully with a constant.
 */
public class ByPropertyValueSelector
        extends
            AbstractMeshObjectSelector
{
    private static final Log log = Log.getLogInstance( ByPropertyValueSelector.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param propertyType the type of the property that is being compared
     * @param comparisonValue the value to which the property is being compared
     * @param operator the type of comparison to be made
     * @return the created ByPropertyValueSelector
     */
    public static ByPropertyValueSelector create(
            PropertyType               propertyType,
            PropertyValue              comparisonValue,
            PropertyComparisonPredicate operator )
    {
        return new ByPropertyValueSelector( propertyType, comparisonValue, operator );
    }

    /**
     * Constructor.
     *
     * @param propertyType the type of the property that is being compared
     * @param comparisonValue the value to which the property is being compared
     * @param operator the type of comparison to be made
     */
    protected ByPropertyValueSelector(
            PropertyType               propertyType,
            PropertyValue              comparisonValue,
            PropertyComparisonPredicate operator )
    {
        thePropertyType    = propertyType;
        theComparisonValue = comparisonValue;
        theOperator        = operator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accepts(
            MeshObject candidate )
    {
        if( candidate == null ) {
            throw new IllegalArgumentException();
        }

        try {
            PropertyValue value = candidate.getPropertyValue( thePropertyType );
            boolean       ret   = theOperator.test( value, theComparisonValue );

            return ret;

        } catch( IllegalPropertyTypeException ex ) {
            log.error( ex );
        } catch( NotPermittedException ex ) {
            log.error( ex );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof ByPropertyValueSelector ) {
            ByPropertyValueSelector realOther = (ByPropertyValueSelector) other;

            if( !thePropertyType.equals( realOther.thePropertyType )) {
                return false;
            }
            if( PropertyValue.compare( theComparisonValue, realOther.theComparisonValue ) != 0 ) {
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 41 * hash + (this.thePropertyType != null ? this.thePropertyType.hashCode() : 0);
        hash = 41 * hash + (this.theComparisonValue != null ? this.theComparisonValue.hashCode() : 0);
        hash = 41 * hash + (this.theOperator != null ? this.theOperator.hashCode() : 0);
        return hash;
    }

    /**
     * The PropertyType for the comparison.
     */
    protected PropertyType thePropertyType;

    /**
     * The comparison value.
     */
    protected PropertyValue theComparisonValue;

    /**
     * The comparison operator.
     */
    protected PropertyComparisonPredicate theOperator;
}
