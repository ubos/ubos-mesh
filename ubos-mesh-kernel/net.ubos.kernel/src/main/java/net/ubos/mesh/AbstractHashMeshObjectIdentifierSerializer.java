//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//


package net.ubos.mesh;

import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;

/**
 * Factors out common functionality of MeshObjectIdentifierSerializer that use the
 * hash notation.
 */
public abstract class AbstractHashMeshObjectIdentifierSerializer
    implements
        MeshObjectIdentifierSerializer
{
    /**
     * {@inheritDoc}
     */
    @Override
    public String toExternalForm(
            MeshObjectIdentifier id )
    {
        String nsString = determineNamespaceString( id.getNamespace() );
        if( nsString == null ) {
            return escapeSeparator( id.getLocalId() );

        } else {
            return escapeSeparator( nsString ) + SEPARATOR + escapeSeparator( id.getLocalId() );
        }
    }

    /**
     * Let subclasses decide how they want to represent the namespace component.
     *
     * @param ns the namespace
     * @return the string representation
     */
    protected abstract String determineNamespaceString(
            MeshObjectIdentifierNamespace ns );

    /**
     * Escape the SEPARATOR in a String
     *
     * @param in the String that may contain the separator
     * @return the escaped String
     */
    protected String escapeSeparator(
            String in )
    {
        if( in.indexOf( SEPARATOR ) < 0 && in.indexOf( ESCAPE ) < 0 ) {
            return in;
        }

        StringBuilder ret = new StringBuilder();

        for( char c : in.toCharArray() ) {
            switch( c ) {
                case SEPARATOR:
                    // no break
                case ESCAPE:
                    ret.append( ESCAPE );
                    // no break
                default:
                    ret.append( c );
            }
        }
        return ret.toString();
    }

    /**
     * The separator.
     */
    public static final char SEPARATOR = '#';

    /**
     * The escape character.
     */
    public static final char ESCAPE = '\\';
}
