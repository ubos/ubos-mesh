//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

/**
 * A MeshObjectSet that is composed, in some fashion, from other MeshObjectSets.
 * This interface does not specify the way in which the MeshObjectSet is composed.
 */
public interface CompositeMeshObjectSet
        extends
            MeshObjectSet
{
    /**
     * Obtain the operands that were used to construct this set.
     *
     * @return the operands that were used to construct this set
     */
    public abstract MeshObjectSet [] getOperands();
}
