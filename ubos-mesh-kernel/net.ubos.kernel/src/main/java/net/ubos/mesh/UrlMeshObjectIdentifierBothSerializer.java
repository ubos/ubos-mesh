//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.text.ParseException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;

/**
 * Knows how to serialize and deserialize MeshObjectIdentifiers in URL form, so they can be entered into a
 * browser and the right MeshObject shows up.
 */
public class UrlMeshObjectIdentifierBothSerializer
    implements
        MeshObjectIdentifierBothSerializer
{
    /**
     * Factory method.
     *
     * @param contextUrl the URL, in string form, to which the MeshObjectIdentifier is appended.
     * @param delegate the actual serialization below the context URL is performed by the delegate
     * @return the created object
     */
    public static UrlMeshObjectIdentifierBothSerializer create(
            String                             contextUrl,
            MeshObjectIdentifierBothSerializer delegate )
    {
        return new UrlMeshObjectIdentifierBothSerializer( contextUrl, delegate );
    }

    /**
     * Constructor
     *
     * @param contextUrl the URL, in string form, to which the MeshObjectIdentifier is appended.
     * @param delegate the actual serialization below the context URL is performed by the delegate
     */
    protected UrlMeshObjectIdentifierBothSerializer(
            String                             contextUrl,
            MeshObjectIdentifierBothSerializer delegate )
    {
        theContextWithSlash = contextUrl.endsWith( "/" ) ? contextUrl : contextUrl + "/";
        theDelegate         = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toExternalForm(
            MeshObjectIdentifier identifier )
    {
        String withoutQuestion = theDelegate.toExternalForm( identifier );
        withoutQuestion = withoutQuestion.replace( "?", "%3f" );
        return  theContextWithSlash + withoutQuestion;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            String externalForm )
        throws
            ParseException
    {
        if( externalForm.startsWith( theContextWithSlash )) {
            String withQuestion = externalForm.substring( theContextWithSlash.length());
            withQuestion = withQuestion.replace( "%3F", "?" );
            return theDelegate.meshObjectIdentifierFromExternalForm( withQuestion );
        } else {
            throw new ParseException( "Not within context " + theContextWithSlash + ": " + externalForm, 0 );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            MeshObjectIdentifier contextIdentifier,
            String               externalForm )
        throws
            ParseException
    {
        if( externalForm.startsWith( theContextWithSlash )) {
            return theDelegate.meshObjectIdentifierFromExternalForm( contextIdentifier, externalForm.substring( theContextWithSlash.length()));
        } else {
            throw new ParseException( "Not within context " + theContextWithSlash + ": " + externalForm, 0 );
        }
    }

    /**
     * The context URL with a trailing slash.
     */
    protected final String theContextWithSlash;

    /**
     * Delegate most of the serialization to this object.
     */
    protected final MeshObjectIdentifierBothSerializer theDelegate;
}
