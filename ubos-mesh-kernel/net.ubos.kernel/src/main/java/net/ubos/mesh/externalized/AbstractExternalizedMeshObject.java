//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import net.ubos.mesh.MeshObjectIdentifier;

import net.ubos.util.ArrayHelper;

/**
 * Factors out functionality common to ExternalizedMeshObject implementations.
 */
public abstract class AbstractExternalizedMeshObject
        implements
            ExternalizedMeshObject
{
    /**
     * Constructor for subclasses only. Subclass sets all properties.
     */
    protected AbstractExternalizedMeshObject()
    {
        // noop
    }

    /**
     * Constructor for subclasses only. Set all local properties.
     *
     * @param identifier the MeshObjectIdentifier of the MeshObject
     * @param timeCreated the time the MeshObject was created
     * @param timeUpdated the time the MeshObject was last updated
     */
    protected AbstractExternalizedMeshObject(
            MeshObjectIdentifier identifier,
            long                 timeCreated,
            long                 timeUpdated )
    {
        theIdentifier  = identifier;
        theTimeCreated = timeCreated;
        theTimeUpdated = timeUpdated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getIdentifier()
    {
        return theIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeCreated()
    {
        return theTimeCreated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof ExternalizedMeshObject )) {
            return false;
        }

        ExternalizedMeshObject realOther = (ExternalizedMeshObject) other;

        if( !theIdentifier.equals( realOther.getIdentifier() )) {
            return false;
        }
        if( theTimeCreated != realOther.getTimeCreated() ) {
            return false;
        }
        if( theTimeUpdated != realOther.getTimeUpdated() ) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getTypeIdentifiers(), realOther.getTypeIdentifiers(), true )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getPropertyTypes(), realOther.getPropertyTypes(), true )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getPropertyValues(), realOther.getPropertyValues(), true )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getThisEnds(), realOther.getThisEnds(), true )) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theIdentifier.hashCode();
    }

    /**
     * The MeshObjectIdentifier of this MeshObject.
     */
    protected MeshObjectIdentifier theIdentifier;

    /**
     * Our creation time in System.currentTimeMillis() format.
     */
    protected long theTimeCreated = -1L;

    /**
     * Our time of last update in System.currentTimeMillis() format.
     */
    protected long theTimeUpdated = -1L;
}
