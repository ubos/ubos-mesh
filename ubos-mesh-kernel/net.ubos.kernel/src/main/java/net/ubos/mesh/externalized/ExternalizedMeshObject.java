//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import java.io.Serializable;
import net.ubos.mesh.MeshObjectIdentifier;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;

/**
 * Representation of a MeshObject that can be easily serialized and deserialized.
 */
public interface ExternalizedMeshObject
{
    /**
     * Obtain the MeshObjectIdentifier
     *
     * @return the MeshObjectIdentifier
     */
    public abstract MeshObjectIdentifier getIdentifier();

    /**
     * Obtain the time the MeshObject was created.
     *
     * @return the time the MeshObject was created, in System.currentTimeMillis() format
     */
    public abstract long getTimeCreated();

    /**
     * Obtain the time the MeshObject was last updated.
     *
     * @return the time the MeshObject was last updated, in System.currentTimeMillis() format
     */
    public abstract long getTimeUpdated();

    /**
     * Obtain the names of the Attributes carried by the MeshObject.
     *
     * @return the names of the Attributes
     * @see #setAttributeNames
     */
    public abstract String [] getAttributeNames();

    /**
     * Obtain the values of the Attributes carried by the MeshObject, in the same sequence
     * as the Attribute names returned by getAttributeNames().
     *
     * @return the values of the Attributes
     * @see #getAttributeNames()
     */
    public abstract Serializable [] getAttributeValues();

    /**
     * Obtain the MeshTypeIdentifiers of the EntityTypes with which the MeshObject has been blessed.
     *
     * @return the MeshTypeIdentifiers of the EntityTypes
     */
    public abstract MeshTypeIdentifier [] getTypeIdentifiers();

    /**
     * Obtain the MeshTypeIdentifiers of the MeshObject's PropertyTypes.
     *
     * @return the MeshTypeIdentifiers of the MeshObject's PropertyTypes
     * @see #getPropertyValues()
     */
    public abstract MeshTypeIdentifier [] getPropertyTypes();

    /**
     * Obtain the PropertyValues of the MeshObject's properties, in the same sequence
     * as the PropertyTypes returned by getPropertyTypes.
     *
     * @return the PropertyValues of the MeshObject's properties
     * @see #getPropertyTypes()
     */
    public abstract PropertyValue [] getPropertyValues();

    /**
     * Get our ends of the relationships.
     *
     * @return our end of the relationships
     */
    public abstract HasTypesPropertiesAttributes [] getThisEnds();

    /**
     * Determine whether this ExternalizedMeshObject represents a deleted MeshObject.
     *
     * @return true or false
     */
    public abstract boolean getIsDead();

    /**
     * Obtain the ExternalizedMeshObjects that represent the historical versions of this ExternalizedMeshObject.
     *
     * @return the history, or null
     */
    public abstract ExternalizedMeshObject [] getHistorical();

    /**
     * Determine whether some meaningful value has been set.
     *
     * @return if false, we may have read an empty section
     */
    public boolean hasSomethingBeenSet();
}
