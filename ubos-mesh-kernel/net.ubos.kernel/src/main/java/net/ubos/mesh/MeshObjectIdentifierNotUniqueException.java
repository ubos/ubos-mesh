//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

/**
 * This Exception is thrown if an attempt is made to create a new MeshObject with a
 * MeshObjectIdentifier that is used already for a different MeshObject. This
 * typically indicates a programming error by the application programmer.
 */
public class MeshObjectIdentifierNotUniqueException
    extends
        MeshObjectGraphModificationException
{
    /**
     * Constructor.
     *
     * @param existing the MeshObject that exists already with the same MeshObjectIdentifier
     */
    public MeshObjectIdentifierNotUniqueException(
            MeshObject existing )
    {
        super( existing.getMeshBaseView() );

        theExisting = existing;
    }

    /**
     * Obtain the MeshObject that already existed with this MeshObjectIdentifier
     *
     * @return the already-existing MeshObject
     */
    public final MeshObject getExistingMeshObject()
    {
        return theExisting;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theExisting };
    }

    /**
     * The already-existing MeshObject.
     */
    protected final MeshObject theExisting;
}
