//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import java.util.Comparator;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.InverseComparator;
import net.ubos.util.logging.Log;

/**
 * Sorts by the value of a Property available on all MeshObjects in the MeshObjectSet.
 * This is a convenience class.
 */
public class ByPropertyValueSorter
        extends
            DefaultMeshObjectSorter
{
    private static final Log log = Log.getLogInstance( ByPropertyValueSorter.class ); // our own, private logger

    /**
     * Factory method.
     * 
     * @param property the PropertyType by whose values we sort
     * @return the created ByPropertyValueSorter
     */
    public static ByPropertyValueSorter create(
            PropertyType property )
    {
        Comparator<MeshObject> c = new PropertyValueComparator( property );
        return new ByPropertyValueSorter( c, null );
    }

    /**
     * Factory method.
     * 
     * @param property the PropertyType by whose values we sort
     * @param userName localized user-visible name in the current locale
     * @return the created ByPropertyValueSorter
     */
    public static ByPropertyValueSorter create(
            PropertyType property,
            String       userName )
    {
        Comparator<MeshObject> c = new PropertyValueComparator( property );
        return new ByPropertyValueSorter( c, userName );
    }

    /**
     * Factory method.
     *
     * @param property the PropertyType by whose values we sort
     * @param inverse if true, sort in reverse order
     * @return the created ByPropertyValueSorter
     */
    public static ByPropertyValueSorter create(
            PropertyType property,
            boolean      inverse )
    {
        Comparator<MeshObject> c = new PropertyValueComparator( property );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByPropertyValueSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param property the PropertyType by whose values we sort
     * @param inverse if true, sort in reverse order
     * @param userName localized user-visible name in the current locale
     * @return the created ByPropertyValueSorter
     */
    public static ByPropertyValueSorter create(
            PropertyType property,
            boolean      inverse,
            String       userName )
    {
        Comparator<MeshObject> c = new PropertyValueComparator( property );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByPropertyValueSorter( c, userName );
    }

    /**
     * Constructor for subclasses only.
     *
     * @param c specify the comparison criteria as the Java collections API does
     * @param userName localized user-visible name in the current locale
     */
    protected ByPropertyValueSorter(
            Comparator<MeshObject> c,
            String                 userName )
    {
        super( c, userName );
    }

    /**
     * The underlying Comparator of property values.
     */
    public static class PropertyValueComparator
            implements
                Comparator<MeshObject>
    {
        /**
         * Constructor.
         * 
         * @param property the PropertyType that selects the property to compare
         */
        public PropertyValueComparator(
                PropertyType property )
        {
            if( property == null ) {
                throw new NullPointerException( "Null PropertyType" );
            }
            thePropertyType = property;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(
                MeshObject o1,
                MeshObject o2 )
        {
            PropertyValue o1Value = null; // better have a default
            PropertyValue o2Value = null; // better have a default
            
            try {
                o1Value = o1.getPropertyValue( thePropertyType );
            } catch( IllegalPropertyTypeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }

            try {
                o2Value = o2.getPropertyValue( thePropertyType );
            } catch( IllegalPropertyTypeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }
            
            int ret = PropertyValue.compare( o1Value, o2Value );
            return ret;
        }
        
        /**
         * The PropertyType that selects the property to compare.
         */
        protected PropertyType thePropertyType;
    }
}
