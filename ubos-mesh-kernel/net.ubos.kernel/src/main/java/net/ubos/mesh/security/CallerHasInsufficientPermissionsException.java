//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.security;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.MeshBaseView;

/**
 * This Exception indicates that a caller had insufficient permissions to perform
 * the requested operation. Other callers may have different permissions.
 */
public class CallerHasInsufficientPermissionsException
        extends
            NotPermittedException
{
    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView on which the illegal operation was attempted
     * @param obj the primary MeshObject related to which the illegal operation was attempted, if any
     * @param caller identifies the caller that did not have sufficient permissions and triggered this Exception.
     *               This caller may be null, indicating an anonymous caller.
     */
    public CallerHasInsufficientPermissionsException(
            MeshBaseView mbv,
            MeshObject   obj,
            MeshObject   caller )
    {
        super( mbv );

        theMeshObject = obj;
        theCaller     = caller;
    }

    /**
     * Obtain the MeshObject on which the illegal operation was attempted.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * Obtain the caller who did not have sufficient permissions.
     *
     * @return the caller
     */
    public MeshObject getCaller()
    {
        return theCaller;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object [] { theCaller };
    }

    /**
     * The MeshObject on which the illegal operation was attempted, if any. This may be null if,
     * for example, the operation was an attempt to create a new MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The caller that did not have sufficient permissions and triggered this Exception.
     */
    protected final MeshObject theCaller;
}
