//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.logging.Dumper;

/**
 * This Exception is thrown if we try to bless a relationship between two MeshObjects with a RelationshipType
 * that is declared as abstract.
 */
public class RelationshipTypeIsAbstractException
        extends
            IsAbstractException
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject that could not be blessed
     * @param neighbor the neighbor with whom the relationship was supposed to be blessed
     * @param thisEnd the RoleType on the side of obj whose containing RelationshipType is abstract
     */
    public RelationshipTypeIsAbstractException(
            MeshObject obj,
            MeshObject neighbor,
            RoleType   thisEnd )
    {
        super( obj, thisEnd.getRelationshipType() );

        theNeighbor = neighbor;
        theThisEnd  = thisEnd;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType getAbstractMeshType()
    {
        return (RelationshipType) super.getAbstractMeshType();
    }

    /**
     * Obtain the RoleType on this end of the RelationshipType which was abstract.
     *
     * @return the RoleType
     */
    public RoleType getThisEnd()
    {
        return theThisEnd;
    }

    /**
     * Obtain the neighbor with whom the relationship was supposed to be blessed.
     *
     * @return the neighbor
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshBaseView",
                    "meshObject",
                    "type",
                    "neighbor"
                },
                new Object[] {
                    theMeshBaseView,
                    theMeshObject,
                    theType,
                    theNeighbor
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theNeighbor, theType };
    }

    /**
     * The neighbor with whom the relationship was supposed to be blessed.
     */
    protected final MeshObject theNeighbor;

    /**
     * The RoleType on this side of the Relationship.
     */
    protected final RoleType theThisEnd;
}
