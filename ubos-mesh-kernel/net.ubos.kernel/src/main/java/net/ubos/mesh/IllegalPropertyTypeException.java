//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.model.primitives.PropertyType;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * This Exception is thrown when there is an attempt to access
  * a Property that cannot exist on this MeshObject because the MeshObject
  * has not been blessed with an EntityType that provides this PropertyType.
  */
public class IllegalPropertyTypeException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject that did not have the PropertyType
     * @param pt the PropertyType that was illegal on this MeshObject
     */
    public IllegalPropertyTypeException(
            MeshObject   obj,
            PropertyType pt )
    {
        super( obj );

        thePropertyType = pt;
    }

    /**
     * Obtain the PropertyType that did not exist on this MeshObject.
     *
     * @return the PropertyType
     */
    public PropertyType getPropertyType()
    {
        return thePropertyType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshBaseView",
                    "meshObject",
                    "propertyType",
                    "types"
                },
                new Object[] {
                    theMeshBaseView,
                    theMeshObject,
                    thePropertyType,
                    MeshTypeUtils.meshTypeIdentifiersOrNull( theMeshObject )
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        try {
            EntityType [] types = theMeshObject.getEntityTypes();

            return new Object[] { theMeshObject, thePropertyType, types };

        } catch( IsDeadException ex ) {
            return new Object[] { theMeshObject, thePropertyType, new EntityType[0] };
        }
    }

    /**
     * The PropertyType that did not exist on this MeshObject.
     */
    protected final PropertyType thePropertyType;
}
