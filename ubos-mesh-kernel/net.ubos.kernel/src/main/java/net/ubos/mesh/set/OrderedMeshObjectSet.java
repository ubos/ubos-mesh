//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;

/**
 * This is a MeshObjectSet whose content is ordered according to some criteria.
 * Consequently, it provides methods that only are appropriate for OrderedMeshObjectSets.
 */
public interface OrderedMeshObjectSet
    extends
        MeshObjectSet
{
    /**
     * Obtain a MeshObject at a particular index.
     *
     * @param index the index specifying the MeshObject that we are looking for
     * @return the MeshObject at this index
     */
    public abstract MeshObject getMeshObject(
            int index );

    /**
     * Obtain the first MeshObject in the OrderedMeshObjectSet, or null if the OrderedMeshObjectSet is empty.
     *
     * @return the first MeshObject, if any
     */
    public abstract MeshObject getFirstMeshObject();

    /**
     * Obtain the last MeshObject in the OrderedMeshObjectSet, or null if the OrderedMeshObjectSet is empty.
     *
     * @return the last MeshObject, if any
     */
    public abstract MeshObject getLastMeshObject();

    /**
     * Determine the index of a certain MeshObject in this ordered set.
     * Generally, index == findIndexOf( getMeshObject( index )).
     *
     * @param candidate the MeshObject that we are looking for in this set
     * @return the index of the found MeshObject, or -1 if not found
     */
    public abstract int findIndexOf(
            MeshObject candidate );

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract OrderedMeshObjectSet subset(
            MeshObjectSelector selector );

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract OrderedMeshObjectSet inverse();

    /**
     * Special code to specify "all levels" instead of a limited number.
     */
    public static final int UNLIMITED = -1;
}
