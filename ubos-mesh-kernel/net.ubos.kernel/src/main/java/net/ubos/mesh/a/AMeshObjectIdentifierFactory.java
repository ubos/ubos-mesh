//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.a;

import java.text.ParseException;
import java.util.Comparator;
import net.ubos.mesh.AbstractMeshObjectIdentifierFactory;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.util.ResourceHelper;
import net.ubos.util.token.UniqueStringGenerator;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;

/**
 * Default implementation of MeshObjectIdentifierFactory for the A implementation.
 */
public class AMeshObjectIdentifierFactory
        extends
            AbstractMeshObjectIdentifierFactory
{
    /**
     * Factory method.
     *
     * @param namespaceMap knows about MeshObjectIdentifierNamespaces
     * @param defaultNamespace the MeshObjectIdentifierNamespace to use when no other namespace is specified
     * @return the created DefaultAMeshObjectIdentifierFactory
     */
    public static AMeshObjectIdentifierFactory create(
            PrimaryMeshObjectIdentifierNamespaceMap namespaceMap,
            MeshObjectIdentifierNamespace           defaultNamespace )
    {
        UniqueStringGenerator generator = UniqueStringGenerator.create( DEFAULT_RANDOM_CHARS, DEFAULT_ID_LENGTH );

        AMeshObjectIdentifierFactory ret = new AMeshObjectIdentifierFactory( namespaceMap, defaultNamespace, generator );
        return ret;
    }

    /**
     * Factory method, specify the UniqueStringGenerator to use for automatic identifier generation.
     *
     * @param namespaceMap knows about MeshObjectIdentifierNamespaces
     * @param defaultNamespace the MeshObjectIdentifierNamespace to use when no other namespace is specified
     * @param generator the UniqueStringGenerator to use
     * @return the created DefaultAMeshObjectIdentifierFactory
     */
    public static AMeshObjectIdentifierFactory create(
            PrimaryMeshObjectIdentifierNamespaceMap namespaceMap,
            MeshObjectIdentifierNamespace           defaultNamespace,
            UniqueStringGenerator                   generator )
    {
        AMeshObjectIdentifierFactory ret = new AMeshObjectIdentifierFactory( namespaceMap, defaultNamespace, generator );
        return ret;
    }

    /**
     * Constructor.
     *
     * @param namespaceMap knows about MeshObjectIdentifierNamespaces
     * @param defaultNamespace the MeshObjectIdentifierNamespace to use when no other namespace is specified
     * @param generator the UniqueStringGenerator to use
     */
    protected AMeshObjectIdentifierFactory(
            PrimaryMeshObjectIdentifierNamespaceMap namespaceMap,
            MeshObjectIdentifierNamespace           defaultNamespace,
            UniqueStringGenerator                   generator )
    {
        super( namespaceMap, defaultNamespace, generator );
    }

    /**
     * Allow our friends (onlY) to sed a new default namespace.
     *
     * @param newDefaultNamespace the new default namespace
     */
    public void setDefaultNamespace(
            MeshObjectIdentifierNamespace newDefaultNamespace )
    {
        theDefaultNamespace = newDefaultNamespace;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AMeshObjectIdentifier getHomeMeshObjectIdentifier()
    {
        return AMeshObjectIdentifier.create( theDefaultNamespace, "" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns,
            String                        localId )
    {
        return new AMeshObjectIdentifier( ns, localId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getHomeObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns )
    {
        return AMeshObjectIdentifier.create(  ns, "" );
    }

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( AMeshObjectIdentifierFactory.class );

    /**
     * The minimum length for a local id.
     */
    public final static int MINIMUM_ID_LENGTH = theResourceHelper.getResourceIntegerOrDefault( "MinimumIdLength", 3 );

    /**
     * The default length for an automatically generated id.
     */
    public final static int DEFAULT_ID_LENGTH = theResourceHelper.getResourceIntegerOrDefault( "DefaultIdLength", 40 );

    /**
     * The characters that are used in randomly generated identifiers.
     */
    public static final char [] DEFAULT_RANDOM_CHARS = theResourceHelper.getResourceStringOrDefault(
            "DefaultRandomChars",
            "0123456789abcdef" ).toCharArray();

    /**
     * Default ordering for MeshObjectIdentifiers created by this factory.
     */
    public static final Comparator<MeshObjectIdentifier> DEFAULT_COMPARATOR
            = ( MeshObjectIdentifier one, MeshObjectIdentifier two ) -> {
                if( one == null ) {
                    return two == null ? 0 : -1;
                }
                int nsComp = one.getNamespace().compare( two.getNamespace() );
                if( nsComp != 0 ) {
                    return nsComp;
                }
                return one.getLocalId().compareTo( two.getLocalId() );
            };
}
