//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;
import net.ubos.model.traverse.TraverseSpecification;

/**
 * A MeshObjectSelector that accepts all MeshObjects that have at least N
 * and less than M related MeshObjects via a certain TraverseSpecification.
 */
public class ByRelatedMeshObjectSelector
        extends
            AbstractMeshObjectSelector
{
    /**
     * Factory method.
     *
     * @param traversal the TraverseSpecification
     * @param atLeast the minimum number of MeshObjects we need to find
     * @param lessThan the upper ceiling of MeshObjects wwe need to find (not inclusive)
     * @return the created ByRelatedMeshObjectSelector
     */
    public static ByRelatedMeshObjectSelector createOneOrMore(
            TraverseSpecification traversal,
            int                    atLeast,
            int                    lessThan )
    {
        return new ByRelatedMeshObjectSelector( traversal, atLeast, lessThan );
    }

    /**
     * Constructor.
     *
     * @param traversal the TraverseSpecification
     * @param atLeast the minimum number of MeshObjects we need to find
     * @param lessThan the upper ceiling of MeshObjects wwe need to find (not inclusive)
     */
    protected ByRelatedMeshObjectSelector(
            TraverseSpecification traversal,
            int                    atLeast,
            int                    lessThan )
    {
        theTraverseSpecification = traversal;
        theAtLeast                = atLeast;
        theLessThan               = lessThan;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accepts(
            MeshObject candidate )
    {
        if( candidate == null ) {
            throw new IllegalArgumentException();
        }

        MeshObjectSet found = candidate.traverse( theTraverseSpecification );

        int size = found.size();

        if( size < theAtLeast ) {
            return false;
        }
        if( size >= theLessThan ) {
            return false;
        }
        return true;
    }

    /**
     * Obtain the TraverseSpecification.
     *
     * @return the TraverseSpecification
     */
    public TraverseSpecification getTraverseSpecification()
    {
        return theTraverseSpecification;
    }

    /**
     * Determine the lower limit.
     *
     * @return the lower limit
     */
    public int getAtLeast()
    {
        return theAtLeast;
    }

    /**
     * Determine the upper limit.
     *
     * @return the upper limit
     */
    public int getLessThan()
    {
        return theLessThan;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof ByRelatedMeshObjectSelector ) {
            ByRelatedMeshObjectSelector realOther = (ByRelatedMeshObjectSelector) other;

            if( theAtLeast != realOther.theAtLeast ) {
                return false;
            }
            if( theLessThan != realOther.theLessThan ) {
                return false;
            }
            if( theTraverseSpecification == null ) {
                if( realOther.theTraverseSpecification != null ) {
                    return false;
                }
            } else if( !theTraverseSpecification.equals( realOther.theTraverseSpecification )) {
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 61 * hash + (this.theTraverseSpecification != null ? this.theTraverseSpecification.hashCode() : 0);
        hash = 61 * hash + this.theAtLeast;
        hash = 61 * hash + this.theLessThan;
        return hash;
    }

    /**
     * The TraverseSpecification.
     */
    protected TraverseSpecification theTraverseSpecification;

    /**
     * Lower limit, inclusive.
     */
    protected int theAtLeast;

    /**
     * Upper limit, exclusive.
     */
    protected int theLessThan;
}
