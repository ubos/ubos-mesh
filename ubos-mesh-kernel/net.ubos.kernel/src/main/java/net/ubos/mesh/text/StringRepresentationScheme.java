//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.text;

import java.io.Serializable;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.PropertyValueParsingException;
import net.ubos.util.text.StringifierParameters;

/**
 * A class implementing this interface defines a scheme for how to represent
 * MeshObjects as strings. It supports both the representation of an
 * object as a string, and, for some types of objects, the parsing of a string
 * back into the object.
 *
 * The notion of a "flavor" lets a StringRepresentationScheme support multiple
 * "flavors" of the same presentation, such as "short" or "verbose". When
 * no flavor is specified, the "default" flavor is used.
 *
 * Optional StringifierParameters allow the customization of the string
 * representation. They are specific to the StringRepresentationScheme used.
 * For example, a StringRepresentationScheme that knows how to convert
 * MeshObjects into hyperlinks might have a parameter that specifies the
 * root URL to be used below which the MeshObjectIdentifiers of the MeshObjects
 * are to be mapped.
 */
public interface StringRepresentationScheme
{
    /**
     * Format a MeshObjectIdentifier, using the "default" flavor.
     *
     * @param id the to-be formatted object
     * @return string representation
     */
    default String formatMeshObjectIdentifier(
            MeshObjectIdentifier id )
    {
        return formatMeshObjectIdentifier( id, StringifierParameters.EMPTY );
    }

    /**
     * Format a MeshTypeIdentifier.
     *
     * @param id the to-be formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatMeshTypeIdentifier(
            MeshTypeIdentifier    id,
            StringifierParameters pars );

    /**
     * Format a MeshTypeIdentifier, using the "default" flavor.
     *
     * @param id the to-be formatted object
     * @return string representation
     */
    default String formatMeshObjectIdentifier(
            MeshTypeIdentifier id )
    {
        return formatMeshTypeIdentifier( id, StringifierParameters.EMPTY );
    }

    /**
     * Format an Identifier.
     *
     * @param id the to-be formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatMeshObjectIdentifier(
            MeshObjectIdentifier  id,
            StringifierParameters pars );

    /**
     * Format an Attribute value (or null), using the "default" flavor.
     *
     * @param obj the MeshObject whose Attribute this is
     * @param attributeName the name of the Attribute
     * @param currentValue the to-be-formatted value
     * @return string representation
     */
    default String formatAttribute(
            MeshObject   obj,
            String       attributeName,
            Serializable currentValue )
    {
        return formatAttribute( obj, attributeName, currentValue, StringifierParameters.EMPTY );
    }

    /**
     * Format an Attribute value (or null).
     *
     * @param obj the MeshObject whose Attribute this is
     * @param attributeName the name of the Attribute
     * @param currentValue the to-be-formatted value
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatAttribute(
            MeshObject            obj,
            String                attributeName,
            Serializable          currentValue,
            StringifierParameters pars );

    /**
     * Parse a String into an Attribute value,
     * using the "default" flavor.
     *
     * @param raw the String to be parsed
     * @param attributeName the name of the Attribute
     * @return the Attribute value (which may be null)
     * @throws ParseException thrown if parsing was unsuccessful
     */
    default Serializable parseAttributeValue(
            String raw,
            String attributeName )
        throws
            ParseException
    {
        return parseAttributeValue( raw, attributeName, StringifierParameters.EMPTY );
    }

    /**
     * Parse a String into an Attribute value.
     *
     * @param raw the String to be parsed
     * @param attributeName the name of the Attribute
     * @param pars use these StringifierParameters
     * @return the Attribute value (which may be null)
     * @throws ParseException thrown if parsing was unsuccessful
     */
    Serializable parseAttributeValue(
            String                raw,
            String                attributeName,
            StringifierParameters pars )
        throws
            ParseException;

    /**
     * Format a Role Attribute value (or null).
     *
     * @param obj the MeshObject on whose side of the relationship with neighbor this Role Attribute is
     * @param neighbor the neighbor
     * @param attributeName the name of the Attribute
     * @param currentValue the to-be-formatted value
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatRoleAttribute(
            MeshObject            obj,
            MeshObject            neighbor,
            String                attributeName,
            Serializable          currentValue,
            StringifierParameters pars );

    /**
     * Format a MeshObject, using the "default" flavor.
     *
     * @param obj the to-be-formatted object
     * @return string representation
     */
    default String formatMeshObject(
            MeshObject obj )
    {
        return formatMeshObject( obj, StringifierParameters.EMPTY );
    }

    /**
     * Format a MeshObject.
     *
     * @param obj the to-be-formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    public String formatMeshObject(
            MeshObject            obj,
            StringifierParameters pars );

    /**
     * Format the start of a link to a MeshObject, using the "default" flavor.
     * For example, for an HTML StringRepresentationScheme, this could emit
     * the HTML &lt;a href=&quot;&quot;&gt; start tag of a link (not the end tag).
     *
     * @param obj the to-be-formatted object
     * @return string representation
     */
    default String formatMeshObjectLinkStart(
            MeshObject obj )
    {
        return formatMeshObjectLinkStart( obj, StringifierParameters.EMPTY );
    }

    /**
     * Format the start of a link to a MeshObject.
     * For example, for an HTML StringRepresentationScheme, this could emit
     * the HTML &lt;a href=&quot;&quot;&gt; start tag of a link (not the end tag).
     *
     * @param obj the to-be-formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatMeshObjectLinkStart(
            MeshObject            obj,
            StringifierParameters pars );

    /**
     * Format the end of a link to a MeshObject, using the "default" flavor.
     * For example, for an HTML StringRepresentationScheme, this could emit
     * the HTML &lt;/a&gt; end tag of a link (not the start tag).
     *
     * @param obj the to-be-formatted object
     * @return string representation
     */
    default String formatMeshObjectLinkEnd(
            MeshObject obj )
    {
        return formatMeshObjectLinkEnd( obj, StringifierParameters.EMPTY );
    }

    /**
     * Format the end of a link to a MeshObject.
     * For example, for an HTML StringRepresentationScheme, this could emit
     * the HTML &lt;/a&gt; end tag of a link (not the start tag).
     *
     * @param obj the to-be-formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatMeshObjectLinkEnd(
            MeshObject            obj,
            StringifierParameters pars );

    /**
     * Format a PropertyValue (or null) for the provided PropertyType,
     * using the "default" flavor.
     * The PropertyType is provided as a StringRepresentationScheme may
     * vary its string representation based on information carried by the
     * PropertyType, such as min or max values.
     *
     * @param obj the MeshObject whose Property this is
     * @param propertyType the PropertyType for the PropertyValue
     * @param currentValue the to-be-formatted object
     * @return string representation
     */
    default String formatProperty(
            MeshObject    obj,
            PropertyType  propertyType,
            PropertyValue currentValue )
    {
        return formatProperty( obj, propertyType, currentValue, StringifierParameters.EMPTY );
    }

    /**
     * Format a PropertyValue (or null) for the provided PropertyType.
     * The PropertyType is provided as a StringRepresentationScheme may
     * vary its string representation based on information carried by the
     * PropertyType, such as min or max values.
     *
     * @param obj the MeshObject whose Property this is
     * @param propertyType the PropertyType for the PropertyValue
     * @param currentValue the to-be-formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatProperty(
            MeshObject            obj,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars );

    default String formatFutureProperty(
            String        objVar,
            PropertyType  propertyType,
            PropertyValue currentValue )
    {
        return formatFutureProperty( objVar, propertyType, currentValue, StringifierParameters.EMPTY );
    }

    String formatFutureProperty(
            String                objVar,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars );

    /**
     * Format a RolePropertyValue (or null) for the provided PropertyType.
     * The PropertyType is provided as a StringRepresentationScheme may
     * vary its string representation based on information carried by the
     * PropertyType, such as min or max values.
     *
     * @param obj the MeshObject on whose side of the relationship with neighbor this Role Property is
     * @param neighbor the neighbor
     * @param propertyType the PropertyType for the PropertyValue
     * @param currentValue the to-be-formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    String formatRoleProperty(
            MeshObject            obj,
            MeshObject            neighbor,
            PropertyType          propertyType,
            PropertyValue         currentValue,
            StringifierParameters pars );

    /**
     * Parse a String into a PropertyValue of the provided PropertyType,
     * using the "default" flavor.
     *
     * @param raw the String to be parsed
     * @param propertyType the PropertyType for the PropertyValue
     * @return the PropertyValue (which may be null)
     * @throws PropertyValueParsingException thrown if parsing was unsuccessful
     */
    default PropertyValue parsePropertyValue(
            String       raw,
            PropertyType propertyType )
        throws
            PropertyValueParsingException
    {
        return parsePropertyValue( raw, propertyType, StringifierParameters.EMPTY );
    }

    /**
     * Parse a String into a PropertyValue of the provided PropertyType,
     * using the "default" flavor.
     *
     * @param raw the String to be parsed
     * @param mime the MIME type of the String, if known
     * @param propertyType the PropertyType for the PropertyValue
     * @return the PropertyValue (which may be null)
     * @throws PropertyValueParsingException thrown if parsing was unsuccessful
     */
    default PropertyValue parsePropertyValue(
            String       raw,
            String       mime,
            PropertyType propertyType )
        throws
            PropertyValueParsingException
    {
        return parsePropertyValue( raw, mime, propertyType, StringifierParameters.EMPTY );
    }

    /**
     * Parse a String into a PropertyValue of the provided PropertyType.
     *
     * @param raw the String to be parsed
     * @param mime the MIME type of the String, if known
     * @param propertyType the PropertyType for the PropertyValue
     * @param pars use these StringifierParameters
     * @return the PropertyValue (which may be null)
     * @throws PropertyValueParsingException thrown if parsing was unsuccessful
     */
    PropertyValue parsePropertyValue(
            String                raw,
            String                mime,
            PropertyType          propertyType,
            StringifierParameters pars )
        throws
            PropertyValueParsingException;

    /**
     * Parse a String into a PropertyValue of the provided PropertyType.
     *
     * @param raw the String to be parsed
     * @param propertyType the PropertyType for the PropertyValue
     * @param pars use these StringifierParameters
     * @return the PropertyValue (which may be null)
     * @throws PropertyValueParsingException thrown if parsing was unsuccessful
     */
    PropertyValue parsePropertyValue(
            String                raw,
            PropertyType          propertyType,
            StringifierParameters pars )
        throws
            PropertyValueParsingException;

    /**
     * Format a DataType, using the default flavor.
     *
     * @param dataType the to-be-formatted object
     * @return string representation
     */
    default String formatDataType(
            DataType dataType )
    {
        return formatDataType( dataType, StringifierParameters.EMPTY );
    }

    /**
     * Format a DataType.
     *
     * @param dataType the to-be-formatted object
     * @param pars use these StringifierParameters
     * @return string representation
     */
    public String formatDataType(
            DataType              dataType,
            StringifierParameters pars );
}
