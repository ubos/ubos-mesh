//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.util.exception.LocalizedParseException;
import net.ubos.util.logging.Log;

/**
 * When attempting to parse a MeshObjectIdentifier, the specified namespace could not be found.
 */
public class MeshObjectIdentifierNamespaceNotFoundParseException
    extends
        LocalizedParseException
{
    private static final Log log = Log.getLogInstance( MeshObjectIdentifierNamespaceNotFoundParseException.class );

    /**
     * Constructor.
     *
     * @param string the text that could not be parsed
     */
    public MeshObjectIdentifierNamespaceNotFoundParseException(
            String string )
    {
        super( string, null, 0, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theString };
    }
}
