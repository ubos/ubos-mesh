//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.util.logging.CanBeDumped;

/**
 * This Exception is thrown if a blessing operation failed because the
 * MeshType is declared as abstract.
 */
public abstract class IsAbstractException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject that could not be blessed
     * @param type the MeshType that is abstract
     */
    protected IsAbstractException(
            MeshObject          obj,
            CollectableMeshType type )
    {
        super( obj );

        theType = type;
    }

    /**
     * Obtain the MeshType that was abstract.
     *
     * @return the MeshType
     */
    public CollectableMeshType getAbstractMeshType()
    {
        return theType;
    }

    /**
     * The CollectableMeshType that was abstract.
     */
    protected final CollectableMeshType theType;
}

