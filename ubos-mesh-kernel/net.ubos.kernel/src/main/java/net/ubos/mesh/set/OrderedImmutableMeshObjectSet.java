//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

/**
 * An ImmutableMeshObjectSet that is also ordered.
 */
public interface OrderedImmutableMeshObjectSet
        extends
            ImmutableMeshObjectSet,
            OrderedMeshObjectSet
{
    // nothing
}
