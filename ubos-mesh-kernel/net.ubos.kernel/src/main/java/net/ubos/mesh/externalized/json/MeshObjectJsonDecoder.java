//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.json;

import net.ubos.mesh.externalized.MeshObjectDecoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class MeshObjectJsonDecoder
    extends
        AbstractMeshObjectJsonDecoder
    implements
        MeshObjectDecoder
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static MeshObjectJsonDecoder create()
    {
        return new MeshObjectJsonDecoder();
    }

    /**
     * Constructor. Use factory method.
     */
    protected MeshObjectJsonDecoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_OBJECT_JSON_ENCODING_ID;
    }
}
