//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.text.ParseException;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.MeshBase;
import net.ubos.util.logging.Log;
import net.ubos.util.token.UniqueStringGenerator;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.util.ArrayHelper;

/**
 * Factors out common features of MeshObjectIdentifierFactories.
 */
public abstract class AbstractMeshObjectIdentifierFactory
        implements
            MeshObjectIdentifierFactory
{
    private static final Log log = Log.getLogInstance( AbstractMeshObjectIdentifierFactory.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param namespaceMap knows about MeshObjectIdentifierNamespaces
     * @param defaultNamespace the MeshObjectIdentifierNamespace to use when no other namespace is specified
     * @param generator the UniqueStringGenerator to use
     */
    protected AbstractMeshObjectIdentifierFactory(
            PrimaryMeshObjectIdentifierNamespaceMap namespaceMap,
            MeshObjectIdentifierNamespace           defaultNamespace,
            UniqueStringGenerator                   generator )
    {
        theNamespaceMap          = namespaceMap;
        theDefaultNamespace      = defaultNamespace;
        theUniqueStringGenerator = generator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifierNamespace getDefaultNamespace()
    {
        return theDefaultNamespace;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrimaryMeshObjectIdentifierNamespaceMap getPrimaryNamespaceMap()
    {
        return theNamespaceMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * Set the MeshBase to which this MeshObjectIdentifierFactory belongs.
     * This is invoked by the MeshBase's constructor and does not need to invoked
     * by the application programmer. It can only be invoked once; subsequent
     * invocations throw an IllegalStateException.
     *
     * @param mb the MeshBase
     * @throws IllegalStateException thrown if this call is performed more than one on the same instance
     */
    public void setMeshBase(
            MeshBase mb )
        throws
            IllegalStateException
    {
        if( mb == null ) {
            throw new NullPointerException();
        }
        if( theMeshBase != null ) {
            throw new IllegalStateException();
        }
        theMeshBase = mb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifier()
    {
        String localId = theUniqueStringGenerator.createUniqueToken();

        MeshObjectIdentifier ret = createMeshObjectIdentifierIn( theDefaultNamespace, localId );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifier(
            String localId )
    {
        MeshObjectIdentifier ret = createMeshObjectIdentifierIn( theDefaultNamespace, localId );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns )
    {
        String localId = theUniqueStringGenerator.createUniqueToken();

        MeshObjectIdentifier ret = createMeshObjectIdentifierIn( ns, localId );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String               append )
        throws
            ParseException
    {
        String localId = constructBelowLocalId( baseIdentifier.getLocalId(), append.split( "/" )  );

        MeshObjectIdentifier ret = createMeshObjectIdentifierIn( baseIdentifier.getNamespace(), localId );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String []            append )
        throws
            ParseException
    {
        String localId = constructBelowLocalId( baseIdentifier.getLocalId(), append );

        MeshObjectIdentifier ret = createMeshObjectIdentifierIn( baseIdentifier.getNamespace(), localId );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String               append,
            String ...           appendMore )
        throws
            ParseException
    {
        String [] realAppend;
        if( appendMore.length > 0 ) {
            realAppend = new String[ appendMore.length + 1 ];
            realAppend[0] = append;
            for( int i=0 ; i<appendMore.length ; ++i ) {
                realAppend[i+1] = appendMore[i];
            }
        } else {
            realAppend = new String[] { append };
        }
        String localId = constructBelowLocalId( baseIdentifier.getLocalId(), realAppend );

        MeshObjectIdentifier ret = createMeshObjectIdentifierIn( baseIdentifier.getNamespace(), localId );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier )
    {
        try {
            String localId = constructBelowLocalId( baseIdentifier.getLocalId(), theUniqueStringGenerator.createUniqueToken());

            MeshObjectIdentifier ret = createMeshObjectIdentifierIn( baseIdentifier.getNamespace(), localId );
            return ret;

        } catch( ParseException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String ...           append )
        throws
            ParseException
    {
        String [] realAppend = ArrayHelper.append( append, theUniqueStringGenerator.createUniqueToken(), String.class );

        String localId = constructBelowLocalId( baseIdentifier.getLocalId(), realAppend );

        MeshObjectIdentifier ret = createMeshObjectIdentifierIn( baseIdentifier.getNamespace(), localId );
        return ret;
    }

    /**
     * Helper to construct a local identifier below another local identifier
     *
     * @param parent the parent identifier
     * @param children the child identifiers
     * @return the new local identifier
     * @throws ParseException thrown if the child contained a path separator
     */
    protected String constructBelowLocalId(
            String     parent,
            String ... children )
        throws
            ParseException
    {
        if( children.length == 0  ) {
            throw new IllegalArgumentException( "Don't provide zero-length children" );
        }
        StringBuilder buf = new StringBuilder();
        String        sep = "";
        if( !parent.isEmpty() ) {
            buf.append( parent );
            sep = PATH_SEPARATOR;
        }

        for( String child : children ) {
            if( child.isEmpty() ) {
                throw new ParseException( "Don't provide an empty child", 0 );
            }
            if( child.contains( PATH_SEPARATOR )) {
                throw new ParseException( "Child must not contain path separator: " + child, child.indexOf( PATH_SEPARATOR ) );
            }
            buf.append( sep );
            buf.append( child );
            sep = PATH_SEPARATOR;
        }
        return buf.toString();
    }

    /**
     * The MeshBase to which this factory belongs.
     */
    protected MeshBase theMeshBase;

    /**
     * Knows about MeshObjectIdentifierNamespaces.
     */
    protected final PrimaryMeshObjectIdentifierNamespaceMap theNamespaceMap;

    /**
     * The MeshObjectIdentifierNamespace to use when no other namespace is specified.
     */
    protected MeshObjectIdentifierNamespace theDefaultNamespace;

    /**
     * The generator of unique Strings.
     */
    protected final UniqueStringGenerator theUniqueStringGenerator;

    /**
     * The character that separates elements of a path.
     */
    public static final String PATH_SEPARATOR = "/";
}
