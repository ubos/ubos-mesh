//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

/**
 * Classes that know how to encode Mesh-related information into some kind of
 * serialized form implement this.
 */
public interface Encoder
{
    /**
     * Obtain a String that identifies the encoding scheme implemented by this Encoder.
     *
     * @return the encoding id.
     */
    public String getEncodingId();
}
