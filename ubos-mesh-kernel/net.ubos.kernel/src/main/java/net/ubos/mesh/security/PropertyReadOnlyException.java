//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.security;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.model.primitives.PropertyType;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * This Exception indicates that a property is read-only and could not be modified.
 * A read-only property is read-only for everybody, not just specifically for this
 * caller. For example, a calculated property may be read-only.
 */
public class PropertyReadOnlyException
        extends
            NotPermittedException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject whose property it is, if available
     * @param pt the PropertyType that was read-onl, if available
     */
    public PropertyReadOnlyException(
            MeshObject   obj,
            PropertyType pt )
    {
        super( obj.getMeshBaseView() );

        if( pt != null && !pt.getIsReadOnly().value() ) {
            throw new IllegalArgumentException( "PropertyType must be read-only, but isn't: " + pt );
        }
        theMeshObject   = obj;
        thePropertyType = pt;
    }

    /**
     * Obtain the MeshObject on which the illegal operation was attempted.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * Obtain the PropertyType that identified a non-existing Property.
     *
     * @return the PropertyType
     */
    public PropertyType getPropertyType()
    {
        return thePropertyType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshObject",
                    "propertyType",
                    "types"
                },
                new Object[] {
                    theMeshObject,
                    thePropertyType,
                    MeshTypeUtils.meshTypeIdentifiersOrNull( theMeshObject )
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, thePropertyType };
    }

    /**
     * The MeshObject on which the illegal operation was attempted, if any. This may be null if,
     * for example, the operation was an attempt to create a new MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The PropertyType that was illegal on the MeshObject.
     */
    protected PropertyType thePropertyType;
}
