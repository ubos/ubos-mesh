//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * A triple of a MeshObject, a neighbor MeshObject and a Role PropertyType,
  * representing a Role Property of this side of the relationship between the MeshObject and the neighbor.
  */
public final class RoleProperty
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance(RoleProperty.class); // our own, private logger

    /**
      * Constructor. This constructor does not (and should not, due to possible blessing later)
      * check whether this relationship actually carries the specified PropertyType.
      *
      * @param mo the MeshObject
      * @param neighbor the neighbor MeshObject
      * @param rpt the Role PropertyType
      */
    public RoleProperty(
            MeshObject   mo,
            MeshObject   neighbor,
            PropertyType rpt )
    {
        theMeshObject       = mo;
        theNeighbor         = neighbor;
        theRolePropertyType = rpt;
    }

    /**
     * Obtain the MeshObject.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * Obtain the neighbor MeshObject.
     *
     * @return the neighbor MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
      * Obtain the Role PropertyType.
      *
      * @return the Role PropertyType
      */
    public PropertyType getRolePropertyType()
    {
        return theRolePropertyType;
    }

    /**
      * Obtain the value of the role property for this relationships.
      *
      * @return the value of the property for this relationship
      * @throws NotPermittedException thrown if the caller did not have sufficient access rights
      * @throws IllegalPropertyTypeException thrown if the relationship does not currently carry the PropertyType
      * @see #setValue
      */
    public PropertyValue getValue()
       throws
            IllegalPropertyTypeException,
            NotPermittedException
    {
        return theMeshObject.getRolePropertyValue( theNeighbor, theRolePropertyType );
    }

    /**
     * Set the value of the role property for this relationship.
     *
     * @param newValue the new property value on this relationship
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @throws IllegalPropertyValueException thrown if the specified value is an illegal value
     *         for this property
     * @throws IllegalPropertyTypeException thrown if the relationship does not currently carry the PropertyType
     * @throws TransactionException thrown if this method is not invoked between proper
     *         Transaction boundaries
     * @see #getValue
     */
    public void setValue(
            PropertyValue newValue )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        theMeshObject.setRolePropertyValue( theNeighbor, theRolePropertyType, newValue );
    }

    /**
     * This is a convenience function that sets the value of the role property
     * to a reasonable default. The reasonable default is obtained from the
     * PropertyType, and if not present, from the underlying DataType.
     * This is particularly useful when a default value is needed when the
     * property was previously null for this MeshObject.
     *
     * @throws IllegalPropertyTypeException thrown if the relationship does not currently carry the PropertyType
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @throws TransactionException thrown if this method is not invoked between
     *         proper Transaction boundaries
     */
    public void setDefaultValue()
        throws
            IllegalPropertyTypeException,
            NotPermittedException,
            TransactionException
    {
        try {
            PropertyValue defaultValue = theRolePropertyType.getDefaultValue();
            if( defaultValue == null ) {
                defaultValue = theRolePropertyType.getDataType().instantiate();
            }
            setValue( defaultValue );

        } catch( IllegalPropertyValueException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof RoleProperty ) {
            RoleProperty realOther = (RoleProperty) other;

            return    theMeshObject.equals( realOther.theMeshObject )
                   && theNeighbor.equals( realOther.theNeighbor )
                   && theRolePropertyType.equals( realOther.theRolePropertyType );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = theMeshObject.hashCode() ^ theNeighbor.hashCode() & theRolePropertyType.hashCode();
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject",
                    "theNeighbor",
                    "theRolePropertyType"
                },
                new Object[] {
                    theMeshObject,
                    theNeighbor,
                    theRolePropertyType
                });
    }

    /**
     * The MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * The Role PropertyType.
     */
    protected final PropertyType theRolePropertyType;
}
