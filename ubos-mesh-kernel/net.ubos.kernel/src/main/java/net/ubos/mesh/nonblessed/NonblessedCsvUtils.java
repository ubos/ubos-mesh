//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.nonblessed;

import java.io.Serializable;
import java.util.HashSet;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.traverse.ByRoleAttributeTraverseSpecification;
import net.ubos.model.traverse.TraverseSpecification;

/**
 * Definitions and utility functions related to nonblessed JSON representation.
 */
public abstract class NonblessedCsvUtils
{
    /**
     * Keep this abstract.
     */
    private NonblessedCsvUtils() {}

    /**
     * Given a MeshObject representing a nonblessed CSV, determine the names of the columns.
     *
     * @param obj the MeshObject
     * @return the names of the columns
     */
    public static String [] determineColumnNames(
            MeshObject obj )
    {
        HashSet<String> attributeNames = new HashSet<>();

        MeshObjectSet rows = obj.traverse( TO_ROWS_SPEC );

        for( MeshObject row : rows ) {
            for( String attributeName : row.getAttributeNames() ) {
                attributeNames.add( attributeName );
            }
        }
        String [] ret = new String[ attributeNames.size() ];
        attributeNames.toArray( ret );

        return ret;
    }

    /**
     * Value for OJBECT_TYPE_ATTRIBUTE that indicates this MeshObject represents a CSV file.
     */
    public static final String OBJECTTYPE_VALUE_CSVFILE = "csv-file";

    /**
     * Value for OJBECT_TYPE_ATTRIBUTE that indicates this MeshObject represents a row in a CSV file.
     */
    public static final String OBJECTTYPE_VALUE_CSVROW = "csv-row";

    /**
     * Value for IMPORTER_PARENT_CHILD_RELATIONSHIP_ROLE_ATTRIBUTE that indicates the child MeshObject was
     * created from a row inside a CSV file that created the parent MeshObject.
     */
    public static final String RELATIONSHIPTYPE_ROLE_VALUE_CSV_CONTAINS = "csv-contains";

    /**
     * TraverseSpecification to find contained rows.
     */
    public static final TraverseSpecification TO_ROWS_SPEC
            = ByRoleAttributeTraverseSpecification.createSource(NonblessedUtils.RELATIONSHIPTYPE_ROLE_ATTRIBUTE,
                    (Serializable s) -> RELATIONSHIPTYPE_ROLE_VALUE_CSV_CONTAINS.equals( s ) );
}
