//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

/**
 * Factors out common functionality of MeshObjectSelector implementations.
 */
public abstract class AbstractMeshObjectSelector
        implements
            MeshObjectSelector
{
    /**
     * Convenience method to invert the selection of a MeshObjectSelector
     * by creating a new MeshObjectSelector.
     *
     * @return a MeshObjectSelector that selects and unselects in the exact opposite way
     *         as this MeshObjectSelector
     */
    public MeshObjectSelector invert()
    {
        return InvertingMeshObjectSelector.create( this );
    }
}
