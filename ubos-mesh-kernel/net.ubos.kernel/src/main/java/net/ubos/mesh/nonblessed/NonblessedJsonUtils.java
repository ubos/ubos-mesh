//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.nonblessed;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.ByRoleAttributeValueSorter;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.OrderedMeshObjectSet;
import net.ubos.model.traverse.ByRoleAttributeTraverseSpecification;
import net.ubos.model.traverse.TraverseSpecification;

/**
 * Definitions and utility functions related to nonblessed JSON representation.
 */
public abstract class NonblessedJsonUtils
{
    /**
     * Keep this abstract.
     */
    private NonblessedJsonUtils() {}

    /**
     * Obtain an ordered MeshObjectSet of the MeshObjects representing the elements of a
     * JSON array, given the MeshObject representing the array itself.
     *
     * @param jsonArray
     * @return
     */
    public static OrderedMeshObjectSet traverseToJsonArrayElements(
            MeshObject jsonArray )
    {
        MeshObjectSet        children = jsonArray.traverse( TO_CHILDREN_SPEC );
        OrderedMeshObjectSet ret      = children.ordered( ByRoleAttributeValueSorter.createSource(jsonArray, NonblessedUtils.LOCAL_INDEX_ROLE_ATTRIBUTE ));

        return ret;
    }

    /**
     * Value for IMPORTER_OJBECT_TYPE_ATTRIBUTE that indicates this MeshObject was created
     * from a JSON file.
     */
    public static final String OBJECTTYPE_VALUE_JSONFILE = "json-file";

    /**
     * Value for IMPORTER_PARENT_CHILD_RELATIONSHIP_ROLE_ATTRIBUTE that indicates the child MeshObject was
     * created from a JSON object that was contained in a JSON object that created the parent MeshObject.
     */
    public static final String RELATIONSHIPTYPE_ROLE_VALUE_JSON_CONTAINS = "json-contains";

    public static final String OBJECTTYPE_VALUE_JSONOBJECT  = "json-object";
    public static final String OBJECTTYPE_VALUE_JSONARRAY   = "json-array";
    public static final String OBJECTTYPE_VALUE_JSONSTRING  = "json-string";
    public static final String OBJECTTYPE_VALUE_JSONNUMBER  = "json-number";
    public static final String OBJECTTYPE_VALUE_JSONBOOLEAN = "json-boolean";
    public static final String OBJECTTYPE_VALUE_JSONNULL    = "json-null";

    /**
     * TraverseSpecification to find contained array elements.
     */
    public static final TraverseSpecification TO_CHILDREN_SPEC
            = ByRoleAttributeTraverseSpecification.createSource(NonblessedUtils.RELATIONSHIPTYPE_ROLE_ATTRIBUTE,
                    (Serializable s) -> RELATIONSHIPTYPE_ROLE_VALUE_JSON_CONTAINS.equals( s ) );

}
