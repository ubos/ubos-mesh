//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.xml;

import net.ubos.model.primitives.externalized.xml.PropertyValueXmlTags;

/**
 * XML Tags for the serialization of ExternalizedMeshObjects.
 */
public interface MeshObjectXmlTags
        extends
            PropertyValueXmlTags
{
    /** Encoding ID */
    public static final String MESH_OBJECT_XML_ENCODING_ID
            = MeshObjectXmlTags.class.getPackage().getName();

    /** Tag indicating a MeshObject. */
    public static final String MESHOBJECT_TAG = "MO";

    /** Tag indicating the time the MeshObject was created. */
    public static final String TIME_CREATED_TAG = "TC";

    /** Tag indicating the time the MeshObject was updated. */
    public static final String TIME_UPDATED_TAG = "TU";

    /** Tag indicating the Identifier of a MeshObject or MeshObjectIdentifierNamespace. */
    public static final String IDENTIFIER_TAG = "ID";

    /** Tag indicating meta, aka type information. */
    public static final String TYPE_TAG = "T";

    /** Tag indicating a (String) Attribute. */
    public static final String STRING_ATTRIBUTE_TAG = "AS";

    /** Tag indicating a (non-String) Attribute. */
    public static final String ATTRIBUTE_TAG = "A";

    /** Tag indicating the name of an Attribute. */
    public static final String ATTRIBUTE_NAME_TAG = "N";

    /** Tag indicating a PropertyType. */
    public static final String PROPERTY_TYPE_TAG = "PT";

    /** Tag indicating a relationship to a neighbor. */
    public static final String NEIGHBOR_TAG = "R";
}
