//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.meshbase.MeshBaseView;

/**
 * <p>This Exception is thrown if an operation is attempted on a MeshBaseView
 * that the caller was not permitted to perform. More specific subclasses
 * are defined in the <code>net.ubos.mesh.security</code> package.</p>
 */
public abstract class NotPermittedException
        extends
            MeshObjectGraphModificationException
{
    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView on which the illegal operation was attempted
     */
    protected NotPermittedException(
            MeshBaseView  mbv )
    {
        super( mbv );
    }
}
