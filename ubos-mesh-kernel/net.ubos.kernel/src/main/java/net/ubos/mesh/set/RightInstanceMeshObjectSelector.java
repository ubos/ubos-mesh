//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;

/**
 * Selects any MeshObject contained in a set of allowed MeshObjects identified
 * by their identifiers.
 *
 * FIXME this could be more efficient.
 */
public class RightInstanceMeshObjectSelector
        extends
            AbstractMeshObjectSelector
{
    /**
     * Factory method.
     *
     * @param single a single allowed MeshObject
     * @return the created RightInstanceMeshObjectSelector
     */
    public static RightInstanceMeshObjectSelector create(
            MeshObject single )
    {
        return new RightInstanceMeshObjectSelector( new MeshObjectIdentifier[] { single.getIdentifier() } );
    }

    /**
     * Factory method.
     *
     * @param singleId identifier of a single allowed MeshObject
     * @return the created RightInstanceMeshObjectSelector
     */
    public static RightInstanceMeshObjectSelector create(
            MeshObjectIdentifier singleId )
    {
        return new RightInstanceMeshObjectSelector( new MeshObjectIdentifier[] { singleId } );
    }

    /**
     * Factory method.
     *
     * @param allowed the allowed MeshObjects
     * @return the created RightInstanceMeshObjectSelector
     */
    public static RightInstanceMeshObjectSelector create(
            MeshObject [] allowed )
    {
        MeshObjectIdentifier [] allowedIds = new MeshObjectIdentifier[ allowed.length ];
        for( int i=0 ; i<allowed.length ; ++i ) {
            allowedIds[i] = allowed[i].getIdentifier();
        }
        return new RightInstanceMeshObjectSelector( allowedIds );
    }

    /**
     * Factory method.
     *
     * @param allowedIds identifiers of the allowed MeshObjects
     * @return the created RightInstanceMeshObjectSelector
     */
    public static RightInstanceMeshObjectSelector create(
            MeshObjectIdentifier [] allowedIds )
    {
        return new RightInstanceMeshObjectSelector( allowedIds );
    }

    /**
     * Factory method.
     *
     * @param allowed the allowed MeshObjects
     * @return the created RightInstanceMeshObjectSelector
     */
    public static RightInstanceMeshObjectSelector create(
            MeshObjectSet allowed )
    {
        return new RightInstanceMeshObjectSelector( allowed.asIdentifiers() );
    }

    /**
     * Private onstructor, use factory method.
     *
     * @param ids MeshObjectIdentifiers of the allowed MeshObjects
     */
    protected RightInstanceMeshObjectSelector(
            MeshObjectIdentifier [] ids )
    {
        theAllowedIds = ids;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accepts(
            MeshObject candidate )
    {
        MeshObjectIdentifier id = candidate.getIdentifier();
        for( int i=0 ; i<theAllowedIds.length ; ++i ) {
            if( id.equals( theAllowedIds[i] )) {
                return true;
            }
        }
        return false;
    }

    /**
     * The allowed identifiers.
     */
    protected MeshObjectIdentifier [] theAllowedIds;
}
