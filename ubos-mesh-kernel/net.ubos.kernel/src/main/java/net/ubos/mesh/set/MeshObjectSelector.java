//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;

/**
  * This interface allows us to define selection criteria for MeshObjects. For example, it is
  * used as a parameter to subsetting methods.
  */
public interface MeshObjectSelector
{
    /**
      * Returns true if this MeshObject shall be selected.
      *
      * @param candidate the MeshObject that shall be tested
      * @return true of this MeshObject shall be selected
      */
    public abstract boolean accepts(
            MeshObject candidate );
}
