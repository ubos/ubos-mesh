//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

/**
 * An ImmutableMeshObjectSet that is also composite.
 */
public interface CompositeImmutableMeshObjectSet
        extends
            ImmutableMeshObjectSet,
            CompositeMeshObjectSet
{
    // nothing
}
