//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import java.util.Arrays;
import java.util.Comparator;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StringHelper;

/**
 * This is a default MeshObjectSorter implementation that uses the Java collections API.
 */
public class DefaultMeshObjectSorter
        implements
            MeshObjectSorter
{
    /**
     * Factory method.
     *
     * @param c specify the comparison criteria as the Java collections API does
     * @return the created DefaultMeshObjectSorter
     */
    public static DefaultMeshObjectSorter create(
            Comparator<MeshObject> c )
    {
        return new DefaultMeshObjectSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param c specify the comparison criteria as the Java collections API does
     * @param userName localized user-visible name in the current locale
     * @return the created DefaultMeshObjectSorter
     */
    public static DefaultMeshObjectSorter create(
            Comparator<MeshObject> c,
            String                 userName )
    {
        return new DefaultMeshObjectSorter( c, userName );
    }

    /**
     * Constructor for subclasses only.
     *
     * @param c specify the comparison criteria as the Java collections API does
     * @param userName localized user-visible name in the current locale
     */
    protected DefaultMeshObjectSorter(
            Comparator<MeshObject> c,
            String                 userName )
    {
        theComparator  = c;
        theUserName    = userName;
    }

    /**
     * Obtain the comparator that was specified at construction time.
     *
     * @return the Comparator that was specified at construction time
     */
    public Comparator<MeshObject> getComparator()
    {
        return theComparator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getOrderedInSame(
            MeshObject [] unorderedSet )
    {
        Arrays.sort( unorderedSet, theComparator );
        return unorderedSet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getOrderedInNew(
            MeshObject [] unorderedSet )
    {
        return getOrderedInSame(
                ArrayHelper.copyIntoNewArray(
                        unorderedSet,
                        MeshObject.class ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIndexOfNew(
            MeshObject    newMeshObject,
            MeshObject [] orderedSet )
    {
        int index = Arrays.binarySearch( orderedSet, newMeshObject, theComparator );
        if( index >= 0 ) {
            return index;
        } else {
            return -index-1;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIndexOf(
            MeshObject    objectToLookFor,
            MeshObject [] orderedSet )
    {
        int index = Arrays.binarySearch( orderedSet, objectToLookFor, theComparator );
        if( index >= 0 ) {
            return index;
        } else {
            return -1;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserName()
    {
        return theUserName;
    }

    /**
     * The Comparator that we are using to sort.
     */
    protected Comparator<MeshObject> theComparator;

    /**
     * The user-visible name for this MeshObjectSorter.
     */
    protected String theUserName;

    /**
     * Simple MeshObjectIdentifierSerializer to produce consistent values for sorting purposes only.
     */
    protected static final MeshObjectIdentifierSerializer theSortingOnlySerializer = ( MeshObjectIdentifier id ) -> {
        if( id.getNamespace() == null ) {
            return id.getLocalId();
        } else {
            return id.getNamespace().getPreferredExternalName() + "#" + id.getLocalId();
        }
    };

    /**
     * Default instance of this class that sorts by the MeshObject's identifier. While
     * this is not necessarily a comprehensible ordering to the user, it exists on
     * all MeshObjects and will always remain the same.
     */
    public static final DefaultMeshObjectSorter BY_IDENTIFIER = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                String id1 = theSortingOnlySerializer.toExternalForm( o1.getIdentifier());
                String id2 = theSortingOnlySerializer.toExternalForm( o2.getIdentifier());

                int ret = id1.compareTo( id2 );
                return ret;
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_IDENTIFIER" );

    /**
     * Default instance of this class that sorts by the MeshObject's identifier in reverse order. While
     * this is not necessarily a comprehensible ordering to the user, it exists on
     * all MeshObjects and will always remain the same.
     */
    public static final DefaultMeshObjectSorter BY_REVERSE_IDENTIFIER = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                String id1 = theSortingOnlySerializer.toExternalForm( o1.getIdentifier());
                String id2 = theSortingOnlySerializer.toExternalForm( o2.getIdentifier());

                int ret = id1.compareTo( id2 );
                return -ret; // notice the minus
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_REVERSE_IDENTIFIER" );

    /**
     * Default instance of this class that sorts by the MeshObject's user-visible String.
     * This comparison follows the same approach as DefaultTraversePathSorter.ByUserVisibleStringComparator.
     */
    public static final DefaultMeshObjectSorter BY_USER_VISIBLE_STRING = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                String valueOne = o1.getUserVisibleString();
                String valueTwo = o2.getUserVisibleString();

                if( valueOne == null && valueTwo == null ) {
                    valueOne = theSortingOnlySerializer.toExternalForm( o1.getIdentifier());
                    valueTwo = theSortingOnlySerializer.toExternalForm( o2.getIdentifier());
                }

                int ret = StringHelper.compareTo( valueOne, valueTwo );
                return ret;
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_USER_VISIBLE_STRING" );

    /**
     * Default instance of this class that sorts by the MeshObject's user-visible String in reverse order.
     */
    public static final DefaultMeshObjectSorter BY_REVERSE_USER_VISIBLE_STRING = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                String valueOne = o1.getUserVisibleString();
                String valueTwo = o2.getUserVisibleString();

                if( valueOne == null && valueTwo == null ) {
                    valueOne = theSortingOnlySerializer.toExternalForm( o1.getIdentifier());
                    valueTwo = theSortingOnlySerializer.toExternalForm( o2.getIdentifier());
                }

                int ret = StringHelper.compareTo( valueOne, valueTwo );
                return -ret; // notice the minus
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_REVERSE_USER_VISIBLE_STRING" );

    /**
     * Default instance of this class that sorts by the MeshObject's timeCreated pseudo-property.
     */
    public static final DefaultMeshObjectSorter BY_TIME_CREATED = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                long valueOne = o1.getTimeCreated();
                long valueTwo = o2.getTimeCreated();

                if( valueOne < valueTwo ) {
                    return -1;
                } else if( valueOne == valueTwo ) {
                    return 0;
                } else {
                    return +1;
                }
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_TIME_CREATED" );

    /**
     * Default instance of this class that sorts by the MeshObject's timeCreated pseudo-property in reverse order.
     */
    public static final DefaultMeshObjectSorter BY_REVERSE_TIME_CREATED = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                long valueOne = o1.getTimeCreated();
                long valueTwo = o2.getTimeCreated();

                if( valueOne < valueTwo ) {
                    return +1;
                } else if( valueOne == valueTwo ) {
                    return 0;
                } else {
                    return -1;
                }
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_REVERSE_TIME_CREATED" );

    /**
     * Default instance of this class that sorts by the MeshObject's timeUpdated pseudo-property.
     */
    public static final DefaultMeshObjectSorter BY_TIME_UPDATED = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                long valueOne = o1.getTimeUpdated();
                long valueTwo = o2.getTimeUpdated();

                if( valueOne < valueTwo ) {
                    return -1;
                } else if( valueOne == valueTwo ) {
                    return 0;
                } else {
                    return +1;
                }
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_TIME_UPDATED" );

    /**
     * Default instance of this class that sorts by the MeshObject's timeUpdated pseudo-property in reverse order.
     */
    public static final DefaultMeshObjectSorter BY_REVERSE_TIME_UPDATED = new DefaultMeshObjectSorter(
            ( MeshObject o1, MeshObject o2 ) -> {
                long valueOne = o1.getTimeUpdated();
                long valueTwo = o2.getTimeUpdated();

                if( valueOne < valueTwo ) {
                    return +1;
                } else if( valueOne == valueTwo ) {
                    return 0;
                } else {
                    return -1;
                }
            },
            DefaultMeshObjectSorter.class.getName() + ".BY_REVERSE_TIME_UPDATED" );
}
