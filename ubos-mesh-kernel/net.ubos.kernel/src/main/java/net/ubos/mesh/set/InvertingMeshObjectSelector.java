//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;

/**
 * A MeshObjectSelector that selects the opposite of what a delegate MeshObjectSelector
 * would select.
 */
public class InvertingMeshObjectSelector
        implements
            MeshObjectSelector
{
    /**
     * Factory method.
     *
     * @param delegate the delegate
     * @return the created InvertingMeshObjectSelector
     */
    public static InvertingMeshObjectSelector create(
            MeshObjectSelector delegate )
    {
        return new InvertingMeshObjectSelector( delegate );
    }

    /**
     * Constructor.
     *
     * @param delegate the delegate
     */
    protected InvertingMeshObjectSelector(
            MeshObjectSelector delegate )
    {
        theDelegate = delegate;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accepts(
            MeshObject candidate )
    {
        boolean ret = theDelegate.accepts( candidate );
        return !ret;
    }

    /**
     * Obtain the delegate MeshObjectSelector.
     *
     * @return the delegate MeshObjectSelector
     */
    public MeshObjectSelector getDelegate()
    {
        return theDelegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof InvertingMeshObjectSelector ) {
            InvertingMeshObjectSelector realOther = (InvertingMeshObjectSelector) other;

            if( !theDelegate.equals( realOther.theDelegate )) {
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theDelegate.hashCode();
    }

    /**
     * The delegate MeshObjectSelector.
     */
    protected MeshObjectSelector theDelegate;
}
