//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.text;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies MeshTypeIdentifiers.
 */
public class MeshTypeIdentifierStringifier
        extends
            AbstractStringifier<MeshTypeIdentifier>
{
    /**
     * Factory method.
     *
     * @return the created EnumeratedValueStringifier
     */
    public static MeshTypeIdentifierStringifier create()
    {
        return new MeshTypeIdentifierStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected MeshTypeIdentifierStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            MeshTypeIdentifier    arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON.toExternalForm( arg );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (MeshTypeIdentifier) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeIdentifier unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }
}
