//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import java.io.Serializable;
import java.util.Comparator;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.util.InverseComparator;
import net.ubos.util.logging.Log;

/**
 * Sorts by the value of an RoleAttribute available on all MeshObjects in the MeshObjectSet with respect to
 * a neighbor MeshObject.
 * This is a convenience class.
 */
public class ByRoleAttributeValueSorter
        extends
            DefaultMeshObjectSorter
{
    private static final Log log = Log.getLogInstance( ByRoleAttributeValueSorter.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param source the MeshObject on whose side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createSource(
            MeshObject source,
            String     roleAttributeName )
    {
        Comparator<MeshObject> c = new SourceRoleAttributeValueComparator( source, roleAttributeName );
        return new ByRoleAttributeValueSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param source the MeshObject on whose side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @param userName localized user-visible name in the current locale
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createSource(
            MeshObject source,
            String     roleAttributeName,
            String     userName )
    {
        Comparator<MeshObject> c = new SourceRoleAttributeValueComparator( source, roleAttributeName );
        return new ByRoleAttributeValueSorter( c, userName );
    }

    /**
     * Factory method.
     *
     * @param source the MeshObject on whose side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @param inverse if true, sort in reverse order
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createSource(
            MeshObject source,
            String     roleAttributeName,
            boolean    inverse )
    {
        Comparator<MeshObject> c = new SourceRoleAttributeValueComparator( source, roleAttributeName );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByRoleAttributeValueSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param source the MeshObject on whose side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @param inverse if true, sort in reverse order
     * @param userName localized user-visible name in the current locale
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createSource(
            MeshObject source,
            String     roleAttributeName,
            boolean    inverse,
            String     userName )
    {
        Comparator<MeshObject> c = new SourceRoleAttributeValueComparator( source, roleAttributeName );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByRoleAttributeValueSorter( c, userName );
    }


    /**
     * Factory method.
     *
     * @param destination the MeshObject on whose other side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createDestination(
            MeshObject destination,
            String     roleAttributeName )
    {
        Comparator<MeshObject> c = new DestinationRoleAttributeValueComparator( destination, roleAttributeName );
        return new ByRoleAttributeValueSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param destination the MeshObject on whose other side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @param userName localized user-visible name in the current locale
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createDestination(
            MeshObject destination,
            String     roleAttributeName,
            String     userName )
    {
        Comparator<MeshObject> c = new DestinationRoleAttributeValueComparator( destination, roleAttributeName );
        return new ByRoleAttributeValueSorter( c, userName );
    }

    /**
     * Factory method.
     *
     * @param destination the MeshObject on whose other side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @param inverse if true, sort in reverse order
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createDestination(
            MeshObject destination,
            String     roleAttributeName,
            boolean    inverse )
    {
        Comparator<MeshObject> c = new DestinationRoleAttributeValueComparator( destination, roleAttributeName );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByRoleAttributeValueSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param destination the MeshObject on whose other side the RoleAttribute is located
     * @param roleAttributeName the name of the RoleAttribute by whose values we sort
     * @param inverse if true, sort in reverse order
     * @param userName localized user-visible name in the current locale
     * @return the created instance
     */
    public static ByRoleAttributeValueSorter createDestination(
            MeshObject destination,
            String     roleAttributeName,
            boolean    inverse,
            String     userName )
    {
        Comparator<MeshObject> c = new DestinationRoleAttributeValueComparator( destination, roleAttributeName );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByRoleAttributeValueSorter( c, userName );
    }

    /**
     * Constructor for subclasses only.
     *
     * @param c specify the comparison criteria as the Java collections API does
     * @param userName localized user-visible name in the current locale
     */
    protected ByRoleAttributeValueSorter(
            Comparator<MeshObject> c,
            String                 userName )
    {
        super( c, userName );
    }

    /**
     * The underlying Comparator of role attribute values on the source side.
     */
    public static class SourceRoleAttributeValueComparator
            implements
                Comparator<MeshObject>
    {
        /**
         * Constructor.
         *
         * @param source the MeshObject on whose side the RoleAttribute is located
         * @param roleAttributeName the name of the attribute
         */
        public SourceRoleAttributeValueComparator(
                MeshObject source,
                String     roleAttributeName )
        {
            if( source == null ) {
                throw new NullPointerException( "Null source" );
            }
            if( roleAttributeName == null ) {
                throw new NullPointerException( "Null RoleAttribute name" );
            }
            theSource            = source;
            theRoleAttributeName = roleAttributeName;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @SuppressWarnings("unchecked")
        public int compare(
                MeshObject o1,
                MeshObject o2 )
        {
            Serializable o1Value = null; // better have a default
            Serializable o2Value = null; // better have a default

            try {
                o1Value = theSource.getRoleAttributeValue( o1, theRoleAttributeName );
            } catch( IllegalPropertyTypeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }

            try {
                o2Value = theSource.getRoleAttributeValue( o2, theRoleAttributeName );
            } catch( IllegalPropertyTypeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }

            int ret;
            if( o1Value instanceof Comparable && o2Value instanceof Comparable ) {
                Comparable realO1Value = (Comparable) o1Value;
                Comparable realO2Value = (Comparable) o2Value;

                ret = realO1Value.compareTo( realO2Value );

            } else {
                ret = o2Value.hashCode() - o1Value.hashCode();
            }
            return ret;
        }

        /**
         * The source MeshObject.
         */
        protected final MeshObject theSource;

        /**
         * The name of the RoleAttribute by which to compare.
         */
        protected final String theRoleAttributeName;
    }

    /**
     * The underlying Comparator of role attribute values on the destination side.
     */
    public static class DestinationRoleAttributeValueComparator
            implements
                Comparator<MeshObject>
    {
        /**
         * Constructor.
         *
         * @param destination the MeshObject on the other side of which the RoleAttribute is located
         * @param roleAttributeName the name of the attribute
         */
        public DestinationRoleAttributeValueComparator(
                MeshObject destination,
                String     roleAttributeName )
        {
            if( destination == null ) {
                throw new NullPointerException( "Null destination" );
            }
            if( roleAttributeName == null ) {
                throw new NullPointerException( "Null RoleAttribute name" );
            }
            theDestination       = destination;
            theRoleAttributeName = roleAttributeName;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @SuppressWarnings("unchecked")
        public int compare(
                MeshObject o1,
                MeshObject o2 )
        {
            Serializable o1Value = null; // better have a default
            Serializable o2Value = null; // better have a default

            try {
                o1Value = o1.getRoleAttributeValue( theDestination, theRoleAttributeName );
            } catch( IllegalPropertyTypeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }

            try {
                o2Value = o2.getRoleAttributeValue( theDestination, theRoleAttributeName );
            } catch( IllegalPropertyTypeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }

            int ret;
            if( o1Value instanceof Comparable && o2Value instanceof Comparable ) {
                Comparable realO1Value = (Comparable) o1Value;
                Comparable realO2Value = (Comparable) o2Value;

                ret = realO1Value.compareTo( realO2Value );

            } else {
                ret = o2Value.hashCode() - o1Value.hashCode();
            }
            return ret;
        }

        /**
         * The destination MeshObject.
         */
        protected final MeshObject theDestination;

        /**
         * The name of the RoleAttribute by which to compare.
         */
        protected final String theRoleAttributeName;
    }
}
