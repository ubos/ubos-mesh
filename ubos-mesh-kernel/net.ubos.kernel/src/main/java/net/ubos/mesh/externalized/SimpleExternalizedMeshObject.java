//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import java.io.Serializable;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * This implementation of ExternalizedMeshObject is fully initialized in the
 * factory method.
 */
public class SimpleExternalizedMeshObject
        extends
            AbstractExternalizedMeshObject
        implements
            CanBeDumped,
            Serializable
{
    private static final long serialVersionUID = 1L; // help with serialization

    /**
     * Factory method.
     *
     * @param identifier the MeshObjectIdentifier of the MeshObject
     * @param typeNames the MeshTypeIdentifier identifying the EntityTypes with which the MeshObject is currently blessed
     * @param timeCreated the time the MeshObject was created
     * @param timeUpdated the time the MeshObject was last updated
     * @param attributeNames names of the Attributes carried by this MeshObject
     * @param attributeValues values of the Attributes carried by this MeshObject, in the same sequence as attributeNames
     * @param propertyTypes the current PropertyTypes of the MeshObject, in the same sequence as propertyValues
     * @param propertyValues the current values of the PropertyTypes, in the same sequence as propertyTypes
     * @param thisEnds our ends of the relationships we participate in
     * @param isDead if true, this represents a MeshObject that has been deleted
     * @return the created SimpleExternalizedMeshObject
     */
    public static SimpleExternalizedMeshObject create(
            MeshObjectIdentifier  identifier,
            long                  timeCreated,
            long                  timeUpdated,
            String []             attributeNames,
            Serializable []       attributeValues,
            MeshTypeIdentifier [] typeNames,
            MeshTypeIdentifier [] propertyTypes,
            PropertyValue  []     propertyValues,
            ThisEnd []            thisEnds,
            boolean               isDead )
    {
        SimpleExternalizedMeshObject ret = new SimpleExternalizedMeshObject(
                identifier,
                timeCreated,
                timeUpdated,
                attributeNames,
                attributeValues,
                typeNames,
                propertyTypes,
                propertyValues,
                thisEnds,
                isDead );

        return ret;
    }

    /**
     * Construct one from externalized data.
     *
     * @param identifier the MeshObjectIdentifier of the MeshObject
     * @param timeCreated the time the MeshObject was created
     * @param timeUpdated the time the MeshObject was last updated
     * @param attributeNames names of the Attributes carried by this MeshObject
     * @param attributeValues values of the Attributes carried by this MeshObject, in the same sequence as attributeNames
     * @param typeNames the MeshTypeIdentifier identifying the EntityTypes with which the MeshObject is currently blessed
     * @param propertyTypes the current PropertyTypes of the MeshObject, in the same sequence as propertyValues
     * @param propertyValues the current values of the PropertyTypes, in the same sequence as propertyTypes
     * @param thisEnds our ends of the relationships we participate in
     * @param isDead if true, this represents a MeshObject that has been deleted
     */
    protected SimpleExternalizedMeshObject(
            MeshObjectIdentifier  identifier,
            long                  timeCreated,
            long                  timeUpdated,
            String []             attributeNames,
            Serializable []       attributeValues,
            MeshTypeIdentifier [] typeNames,
            MeshTypeIdentifier [] propertyTypes,
            PropertyValue  []     propertyValues,
            ThisEnd []            thisEnds,
            boolean               isDead )
    {
        super( identifier, timeCreated, timeUpdated );

        // do some sanity checking

        if( identifier == null ) {
            throw new IllegalArgumentException( "null Identifier" );
        }

        if( attributeNames != null ) {
            for( String current : attributeNames ) {
                if( current == null ) {
                    throw new IllegalArgumentException( "null attributeName" );
                }
            }
        } else {
            attributeNames = new String[0];
        }

        if( attributeValues == null ) {
            attributeValues = new Serializable[0];
        }

        if( attributeNames.length != attributeValues.length ) {
            throw new IllegalArgumentException( "Not same length: attributeNames and attributeValues" );
        }

        if( typeNames != null ) {
            for( MeshTypeIdentifier current : typeNames ) {
                if( current == null ) {
                    throw new IllegalArgumentException( "null typeName" );
                }
            }
        } else {
            typeNames = new MeshTypeIdentifier[0];
        }

        if( propertyTypes != null ) {
            for( MeshTypeIdentifier current : propertyTypes ) {
                if( current == null ) {
                    throw new IllegalArgumentException( "null PropertyType" );
                }
            }
        } else {
            propertyTypes = new MeshTypeIdentifier[0];
        }

        if( propertyValues == null ) {
            propertyValues = new PropertyValue[0];
        }
        if( propertyTypes.length != propertyValues.length ) {
            throw new IllegalArgumentException( "Not same length: propertyTypes and propertyValues" );
        }

        if( thisEnds != null ) {
            for( ThisEnd current : thisEnds ) {
                if( current == null ) {
                    throw new IllegalArgumentException( "null ThisEnd" );
                }
                if( current.getIdentifier() == null ) {
                    throw new IllegalArgumentException( "null neighbor Identifier" );
                }

                if( current.theRolePropertyTypeIdentifiers != null ) {
                    for( MeshTypeIdentifier current2 : current.theRoleTypeIdentifiers ) {
                        if( current2 == null ) {
                            throw new IllegalArgumentException( "null RoleTypeIdentifier" );
                        }
                    }
                } else {
                        current.theRolePropertyTypeIdentifiers = new MeshTypeIdentifier[0];
                }

                if( current.theRolePropertyTypeIdentifiers.length == 0 ) {
                    if( current.theRolePropertyTypeIdentifiers != null && current.theRolePropertyTypeIdentifiers.length != 0 ) {
                        throw new IllegalArgumentException( "No Role PropertyTypes allowed when no RoleTypes" );
                    }
                    if( current.theRolePropertyValues != null && current.theRolePropertyValues.length != 0 ) {
                        throw new IllegalArgumentException( "No Role PropertyValues allowed when no RoleTypes" );
                    }
                } else {
                    if(    (    ( current.theRolePropertyTypeIdentifiers == null || current.theRolePropertyTypeIdentifiers.length == 0 )
                             && ( current.theRolePropertyValues          != null && current.theRolePropertyValues.length > 0 ) )
                        || (    ( current.theRolePropertyTypeIdentifiers != null && current.theRolePropertyTypeIdentifiers.length > 0 )
                             && ( current.theRolePropertyValues          == null || current.theRolePropertyValues.length == 0 )) )
                    {
                        throw new IllegalArgumentException( "Role PropertyTypes and PropertyValues must have same length" );
                    }
                    if( current.theRolePropertyTypeIdentifiers == null ) {
                        current.theRolePropertyTypeIdentifiers = new MeshTypeIdentifier[0];
                    }
                    if( current.theRolePropertyValues == null ) {
                        current.theRolePropertyValues = new PropertyValue[0];
                    }
                    for( MeshTypeIdentifier current2 : current.theRolePropertyTypeIdentifiers ) {
                        if( current2 == null ) {
                            throw new IllegalArgumentException( "Null PropertyType" );
                        }
                    }
                }

            }
        } else {
            thisEnds = new ThisEnd[0];
        }

        theIdentifier      = identifier;
        theAttributeNames  = attributeNames;
        theAttributeValues = attributeValues;
        theTypeNames       = typeNames;
        thePropertyTypes   = propertyTypes;
        thePropertyValues  = propertyValues;
        theThisEnds        = thisEnds;
        theIsDead          = isDead;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String [] getAttributeNames()
    {
        return theAttributeNames;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Serializable [] getAttributeValues()
    {
        return theAttributeValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshTypeIdentifier [] getTypeIdentifiers()
    {
        return theTypeNames;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshTypeIdentifier [] getPropertyTypes()
    {
        return thePropertyTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue [] getPropertyValues()
    {
        return thePropertyValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ThisEnd [] getThisEnds()
    {
        return theThisEnds;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getIsDead()
    {
        return theIsDead;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObject [] getHistorical()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasSomethingBeenSet()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theIdentifier",
                    "theTypeNames",
                    "thePropertyTypes",
                    "thePropertyValues",
                    "theTimeCreated",
                    "theTimeUpdated",
                    "theThisEnds"
                },
                new Object[] {
                    theIdentifier,
                    theTypeNames,
                    thePropertyTypes,
                    thePropertyValues,
                    theTimeCreated,
                    theTimeUpdated,
                    theThisEnds
                });
    }

    /**
     * The identity of our EntityTypes.
     */
    protected final MeshTypeIdentifier [] theTypeNames;

    /**
     * The names of the Attributes.
     */
    protected final String [] theAttributeNames;

    /**
     * The values of the Attributes, in same sequence as theAttributeNames.
     */
    protected final Serializable [] theAttributeValues;

    /**
     * The identity of our PropertyTypes, in same sequence as thePropertyValues.
     */
    protected final MeshTypeIdentifier [] thePropertyTypes;

    /**
     * The PropertyValues, in the same sequence as thePropertyTypes.
     */
    protected final PropertyValue [] thePropertyValues;

    /**
     * Our ends of the relationships with neighbors.
     */
    protected final ThisEnd [] theThisEnds;

    /**
     * If true, this represents a MeshObject that has been deleted.
     */
    protected final boolean theIsDead;

    /**
     * Our end of a relationship with a neighbor.
     */
    public static class ThisEnd
        implements
            HasTypesPropertiesAttributes,
            CanBeDumped
    {
        /**
         * Constructor.
         *
         * @param neighborIdentifier identifier of the neighbor MeshObject
         * @param neighborTimeCreated the timeCreated of the neighbor MeshObject
         * @param roleAttributeNames names of the RoleAttributes on this end of this Relationship
         * @param roleAttributeValues values of the RoleAttributes on this end of this Relationship
         * @param roleTypeIdentifiers identifiers of the RoleTypes on this end of this Relationship
         * @param rolePropertyTypeIdentifiers  identifiers of the PropertyTypes on this end of this Relationship
         * @param rolePropertyValues values of the Properties on this end of this Relationship
         */
        public ThisEnd(
                MeshObjectIdentifier  neighborIdentifier,
                long                  neighborTimeCreated,
                String []             roleAttributeNames,
                Serializable []       roleAttributeValues,
                MeshTypeIdentifier [] roleTypeIdentifiers,
                MeshTypeIdentifier [] rolePropertyTypeIdentifiers,
                PropertyValue []      rolePropertyValues )
        {
            theNeighborIdentifier          = neighborIdentifier;
            theNeighborTimeCreated         = neighborTimeCreated;
            theRoleAttributeNames          = roleAttributeNames;
            theRoleAttributeValues         = roleAttributeValues;
            theRoleTypeIdentifiers         = roleTypeIdentifiers;
            theRolePropertyTypeIdentifiers = rolePropertyTypeIdentifiers;
            theRolePropertyValues          = rolePropertyValues;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MeshObjectIdentifier getIdentifier()
        {
            return theNeighborIdentifier;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public long getTimeCreated()
        {
            return theNeighborTimeCreated;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String [] getAttributeNames()
        {
            return theRoleAttributeNames;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Serializable [] getAttributeValues()
        {
            return theRoleAttributeValues;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MeshTypeIdentifier [] getTypeIdentifiers()
        {
            return theRoleTypeIdentifiers;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MeshTypeIdentifier [] getPropertyTypes()
        {
            return theRolePropertyTypeIdentifiers;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public PropertyValue [] getPropertyValues()
        {
            return theRolePropertyValues;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void dump(
                Dumper d )
        {
            d.dump( this,
                    new String[] {
                        "theNeighborIdentifier",
                        "theRoleAttributeNames",
                        "theRoleAttributeValues",
                        "theRoleTypeIdentifiers",
                        "theRolePropertyTypeIdentifiers",
                        "theRolePropertyValues"
                    },
                    new Object[] {
                        theNeighborIdentifier,
                        theRoleAttributeNames,
                        theRoleAttributeValues,
                        theRoleTypeIdentifiers,
                        theRolePropertyTypeIdentifiers,
                        theRolePropertyValues
                    });
        }

        /**
         * Identifier of our neighbor.
         */
        protected final MeshObjectIdentifier theNeighborIdentifier;

        /**
         * The timeCreated of our neighbor
         */
        protected final long theNeighborTimeCreated;

        /**
         * Names of the RoleAttributes we carry with this neighbor.
         */
        protected final String [] theRoleAttributeNames;

        /**
         * Values of the RoleAttributes we carry with this neighbor.
         */
        protected final Serializable [] theRoleAttributeValues;

        /**
         * RoleTypes we participate in with this neighbor.
         */
        protected final MeshTypeIdentifier [] theRoleTypeIdentifiers;

        /**
         * PropertyTypes of the Role Properties for this relationship.
         */
        protected MeshTypeIdentifier [] theRolePropertyTypeIdentifiers;

        /**
         * PropertyValues of the Role Properties for this relationship.
         */
        protected PropertyValue [] theRolePropertyValues;

        /**
         * Convenience constant.
         */
        public static final ThisEnd [] EMPTY_ARRAY = new ThisEnd[0];
    }
}
