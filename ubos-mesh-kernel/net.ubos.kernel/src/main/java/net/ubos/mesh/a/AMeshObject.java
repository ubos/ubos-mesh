//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import net.ubos.mesh.AbstractMeshObject;
import net.ubos.mesh.AttributesMap;
import net.ubos.mesh.CannotRelateToItselfException;
import net.ubos.mesh.EntityNotBlessedException;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.IllegalPropertyValueException;
import net.ubos.mesh.IllegalRoleAttributeException;
import net.ubos.mesh.IllegalRolePropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.NotRelatedException;
import net.ubos.mesh.PropertiesMap;
import net.ubos.mesh.RelationshipTypeIsAbstractException;
import net.ubos.mesh.RoleTypeBlessedAlreadyException;
import net.ubos.mesh.RoleTypeNotBlessedException;
import net.ubos.mesh.RoleTypeRequiresEntityTypeException;
import net.ubos.mesh.externalized.SimpleExternalizedMeshObject;
import net.ubos.mesh.security.PropertyReadOnlyException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.AbstractMeshBaseView;
import net.ubos.meshbase.HeadMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.meshbase.a.AMeshBase;
import net.ubos.meshbase.a.ATransaction;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.security.NeedNeighborException;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.meshbase.transaction.MeshObjectAttributesRemoveChange;
import net.ubos.meshbase.transaction.MeshObjectPropertyChange;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributeChange;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributesAddChange;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributesRemoveChange;
import net.ubos.meshbase.transaction.MeshObjectRolePropertyChange;
import net.ubos.meshbase.transaction.MeshObjectRoleTypeAddChange;
import net.ubos.meshbase.transaction.MeshObjectRoleTypeRemoveChange;
import net.ubos.meshbase.transaction.MeshObjectUnblessChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StringHelper;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
 * One particular implementation of MeshObject.
 */
public class AMeshObject
        extends
            AbstractMeshObject
{
    private static final Log log = Log.getLogInstance( AMeshObject.class ); // our own, private logger

    /**
     * Constructor for regular instantiation.
     *
     * @param identifier the MeshObjectIdentifier of the MeshObject
     * @param mbv the MeshBaseView that this MeshObject belongs to
     * @param state the MeshObject's State
     */
    public AMeshObject(
            AMeshObjectIdentifier identifier,
            AbstractMeshBaseView  mbv,
            State                 state )
    {
        super( mbv, state );

        theIdentifier = identifier;
    }

    /**
     * Constructor for re-instantiation from external storage.
     *
     * @param identifier the MeshObjectIdentifier of the MeshObject
     * @param mbv the MeshBaseView that this MeshObject belongs to
     * @param attributes the Attributes with their values of the MeshObject, if any
     * @param properties the properties with their values of the MeshObject, if any
     * @param entityTypes the EntityTypes of the MeshObject, if any
     * @param neighborIdentifiers the current neighbors of the MeshObject, given as Identifiers
     * @param neighborTimeCreateds the timeCreateds of the neighbors, in sequence
     * @param neighborRoleAttributes the RoleAttributes of the relationships with the various neighbors, in sequence
     * @param neighborRoleTypes the RoleTypes of the relationships with the various neighbors, in sequence
     * @param neighborRoleProperties the Role PropertyValues of the relationships with the various neighbors, in sequence
     * @param state the MeshObject's State
     */
    public AMeshObject(
            AMeshObjectIdentifier    identifier,
            AbstractMeshBaseView     mbv,
            AttributesMap            attributes,
            PropertiesMap            properties,
            EntityType []            entityTypes,
            AMeshObjectIdentifier [] neighborIdentifiers,
            long []                  neighborTimeCreateds,
            AttributesMap []         neighborRoleAttributes,
            RoleType [][]            neighborRoleTypes,
            PropertiesMap []         neighborRoleProperties,
            State                    state )
    {
        super( mbv, state );

        theIdentifier             = identifier;
        theAttributes             = attributes;
        theProperties             = properties;
        theNeighborIdentifiers    = neighborIdentifiers;
        theNeighborTimeCreateds   = neighborTimeCreateds;
        theNeighborRoleAttributes = neighborRoleAttributes;
        theNeighborRoleTypes      = neighborRoleTypes;
        theNeighborRoleProperties = neighborRoleProperties;

        if( entityTypes != null && entityTypes.length > 0 ) {
            ensureMeshTypes();
            for( int i=0 ; i<entityTypes.length ; ++i ) {
                theEntityTypes.add( entityTypes[i] );
            }
        }
    }

    /**
     * Private constructor for cloning only.
     *
     * @param identifier the MeshObjectIdentifier of the MeshObject
     * @param mbv the MeshBaseView that this MeshObject belongs to
     * @param attributes the Attributes with their values of the MeshObject, if any
     * @param properties the properties with their values of the MeshObject, if any
     * @param entityTypes the EntityTypes of the MeshObject, if any
     * @param neighborIdentifiers the current neighbors of the MeshObject, given as Identifiers
     * @param neighborTimeCreateds the timeCreateds of the neighbors, in sequence
     * @param neighborRoleAttributes the RoleAttributes of the relationships with the various neighbors, in sequence
     * @param neighborRoleTypes the RoleTypes of the relationships with the various neighbors, in sequence
     * @param neighborRoleProperties the Role PropertyValues of the relationships with the various neighbors, in sequence
     * @param state the MeshObject's State
     */
    protected AMeshObject(
            AMeshObjectIdentifier    identifier,
            AbstractMeshBaseView     mbv,
            AttributesMap            attributes,
            PropertiesMap            properties,
            HashSet<EntityType>      entityTypes,
            AMeshObjectIdentifier [] neighborIdentifiers,
            long []                  neighborTimeCreateds,
            AttributesMap []         neighborRoleAttributes,
            RoleType [][]            neighborRoleTypes,
            PropertiesMap []         neighborRoleProperties,
            State                    state )
    {
        super( mbv, state );

        theIdentifier             = identifier;
        theAttributes             = attributes;
        theProperties             = properties;
        theNeighborIdentifiers    = neighborIdentifiers;
        theNeighborTimeCreateds   = neighborTimeCreateds;
        theNeighborRoleAttributes = neighborRoleAttributes;
        theNeighborRoleTypes      = neighborRoleTypes;
        theNeighborRoleProperties = neighborRoleProperties;
        theEntityTypes            = entityTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AMeshBase getMeshBase()
    {
        return (AMeshBase) super.getMeshBase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AMeshObjectIdentifier getIdentifier()
    {
        return theIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverseToNeighbors()
    {
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        int                        count     = 0;
        MeshObject []              almost;

        synchronized( obtainSyncObject( this ) ) {
            checkAlive();

            MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );

            if( neighborIdentifiers != null && neighborIdentifiers.length > 0 ) {
                MeshObject [] found  = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );

                almost = new MeshObject[ found.length ];

                for( int i=0 ; i<found.length ; ++i ) {
                    try {
                        accessMgr.checkPermittedIsRelated( this, found[i] );
                        almost[count++] = found[i];

                    } catch( NotPermittedException ex ) {
                        log.debug( ex );
                    }
                }
            } else {
                return MeshObjectSet.DEFAULT_EMPTY_SET;
            }
        }
        if( count < almost.length ) {
            almost = ArrayHelper.copyIntoNewArray( almost, 0, count, MeshObject.class );
        }
        return getMeshBase().getMeshObjectSetFactory().createImmutableMeshObjectSet( almost );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int neighborsCount()
    {
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        int                        count     = 0;

        synchronized( obtainSyncObject( this ) ) {
            checkAlive();

            MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );

            if( neighborIdentifiers != null && neighborIdentifiers.length > 0 ) {
                try { // try without resolving neighbors first
                    for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
                        try {
                            accessMgr.checkPermittedIsRelated( this, neighborIdentifiers[i] );
                            ++count;

                        } catch( NotPermittedException ex2 ) {
                            log.debug( ex2 );
                        }
                    }
                    
                } catch( NeedNeighborException ex ) {
                    count = 0;
                    MeshObject [] found  = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );

                    for( int i=0 ; i<found.length ; ++i ) {
                        try {
                            accessMgr.checkPermittedIsRelated( this, found[i] );
                            ++count;

                        } catch( NotPermittedException ex2 ) {
                            log.debug( ex2 );
                        }
                    }
                }
            }
        }
        return count;
    }

    /**
     * Find neighbor MeshObjects of this MeshObject that are known by their
     * MeshObjectIdentifiers.
     * We pass in the MeshBase to use because this may be invoked when a MeshObject's member
     * variable has been zero'd out already.
     *
     * @param mbv the MeshBaseView to use
     * @param neighborIdentifiers the MeshObjectIdentifiers of the MeshObjects we are looking for
     * @return the MeshObjects that we found
     */
    protected AMeshObject [] findRelatedMeshObjects(
            MeshBaseView            mbv,
            MeshObjectIdentifier [] neighborIdentifiers )
    {
        AMeshObject [] ret = (AMeshObject []) mbv.findMeshObjectsByIdentifier( neighborIdentifiers );
        return ret;
    }

    /**
     * Find a single neighbor MeshObject of this MeshObject that is known by its
     * MeshObjectIdentifier.
     * We pass in the MeshBase to use because this may be invoked when a MeshObject's member
     * variable has been zero'd out already.
     * This internal helper method may be overridden by subclasses.
     *
     * @param mbv the MeshBaseView to use
     * @param neighborIdentifier the MeshObjectIdentifier of the MeshObject we are looking for
     * @return the MeshObject that we found
     */
    protected MeshObject findRelatedMeshObject(
            MeshBaseView         mbv,
            MeshObjectIdentifier neighborIdentifier )
    {
        MeshObject [] ret = findRelatedMeshObjects( mbv, new MeshObjectIdentifier[] { neighborIdentifier } );
        return ret[0];
    }

    /**
     * Obtain the MeshObjectIdentifiers of the neighbors of this MeshObject. This is used for integrity checks only.
     *
     * @return the MeshObjectIdentifiers of the neighbors, if any
     */
    public AMeshObjectIdentifier [] getNeighborMeshObjectIdentifiers()
    {
        AMeshObjectIdentifier[] ret;

        synchronized( obtainSyncObject( this ) ) {
            checkAlive();

            AMeshObjectNeighborManager nMgr        = getNeighborManager();
            AMeshObjectIdentifier []   neighborIds = nMgr.getNeighborIdentifiers( this );

            if( theNeighborIdentifiers != null ) {
                ret = new AMeshObjectIdentifier [ neighborIds.length ];
                System.arraycopy( neighborIds, 0, ret, 0, neighborIds.length );
            } else {
                ret = new AMeshObjectIdentifier[0];
            }
        }

        return ret;
    }

    /**
     * Obtain the MeshObjectIdentifiers of the neighbors of this MeshObject with the specific RoleType.
     * This works on partial graphs.
     *
     * @param rt the RoleType
     * @return the MeshObjectIdentifiers of the neighbors, if any
     */
    public AMeshObjectIdentifier [] getNeighborMeshObjectIdentifiersFor(
            RoleType rt )
    {
        AMeshObjectIdentifier [] ret;
        int count;

        AMeshObjectNeighborManager nMgr = getNeighborManager();

        synchronized( obtainSyncObject( this ) ) {
            checkAlive();

            AMeshObjectIdentifier [] neighborIds = nMgr.getNeighborIdentifiers( this );
            RoleType [][]            roleTypes   = nMgr.getRoleTypes( this );

            count = 0;
            ret = new AMeshObjectIdentifier[ neighborIds.length ];

            for( int i=0 ; i<neighborIds.length ; ++i ) {
                for( int j=0 ; j<roleTypes[i].length ; ++j ) {
                    if( roleTypes[i][j].isSubtypeOfOrEquals( rt ) ) {
                        ret[count++] = neighborIds[i];
                        break;
                    }
                }
            }
        }
        if( count < ret.length ) {
            ret = ArrayHelper.copyIntoNewArray( ret, AMeshObjectIdentifier.class );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRelated(
            MeshObject otherObject )
    {
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), otherObject.getMeshBaseView() );

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        boolean ret;

        synchronized( obtainSyncObject( this, otherObject )) {
            checkAlive();
            otherObject.checkAlive();

            accessMgr.checkPermittedIsRelated( this, otherObject );

            ret = nMgr.isRelated( this, (AMeshObjectIdentifier) otherObject.getIdentifier() );
        }
        return ret;
    }

    /**
     * Determine whether this MeshObject is related to another MeshObject. This is used for integrity checks only.
     *
     * @param otherObjectIdentifier identifier of the MeshObject to which this MeshObject may be related
     * @return true if this MeshObject is currently related to otherObject
     */
    public boolean isRelated(
            MeshObjectIdentifier otherObjectIdentifier )
    {
        AMeshObjectNeighborManager nMgr = getNeighborManager();
        boolean ret;

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            ret = nMgr.isRelated( this, (AMeshObjectIdentifier) otherObjectIdentifier );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unbless(
            EntityType [] types )
        throws
            RoleTypeRequiresEntityTypeException,
            EntityNotBlessedException,
            TransactionException,
            NotPermittedException
    {
        if( types == null || types.length == 0 ) {
            return;
        }

        checkTransaction();

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        EntityType []              oldTypes;

        synchronized( obtainSyncObject( this )) {// protect against simultaneous read access: alive, isWriteable, permissions, theEntityTypes, theProperties
            checkAlive();
            checkIsWriteable();

            if( theEntityTypes == null ) {
                throw new EntityNotBlessedException( this, types[0] );
            }
            for( int i=0 ; i<types.length ; ++i ) {
                if( !theEntityTypes.contains( types[i] )) {
                     throw new EntityNotBlessedException( this, types[i] );
                }
            }
            accessMgr.checkPermittedUnbless( this, types );

            oldTypes = new EntityType[ theEntityTypes.size() ];
            theEntityTypes.toArray( oldTypes ); // don't filter by permission

            EntityType [] remainingTypes = ArrayHelper.removeIfPresent( oldTypes, types, false, EntityType.class );

            // check that we don't break a Relationship
            AMeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );
            RoleType [][]            roleTypes           = nMgr.getRoleTypes( this );
            AMeshObject []           neighbors;

            if( neighborIdentifiers != null && neighborIdentifiers.length > 0 ) {
                neighbors = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );
                for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
                    if( roleTypes[i] == null ) {
                        continue;
                    }

                    for( RoleType rt : roleTypes[i] ) {
                        EntityType requiredType = rt.getEntityType();
                        if( requiredType != null ) {
                            boolean found = false;
                            for( int j=0 ; j<remainingTypes.length ; ++j ) {

                                if( remainingTypes[j].equalsOrIsSupertype( requiredType )) {
                                    found = true;
                                    break;
                                }
                            }
                            if( !found ) {
                                throw new RoleTypeRequiresEntityTypeException( this, requiredType, rt, neighbors[i] );
                            }
                        }
                    }
                }
            } else {
                neighbors = null;
            }

            for( int i=0 ; i<types.length ; ++i ) {
                theEntityTypes.remove( types[i] );
            }

            // remove unnecessary properties
            if( theProperties != null ) {
                HashMap<PropertyType,PropertyValue> removedProperties = new HashMap<>();

                for( Map.Entry<PropertyType,PropertyValue> current : theProperties.entrySet() ) {
                    boolean found = false;

                    for( EntityType type : theEntityTypes ) {
                        if( ArrayHelper.isIn( current.getKey(), type.getPropertyTypes(), false )) {
                            found = true;
                            break;
                        }
                    }
                    if( !found ) {
                        removedProperties.put( current.getKey(), current.getValue() );

                        if( PropertyValue.compare( current.getValue(), current.getKey().getDefaultValue() ) != 0 ) {
                            appendChangeToTransaction( new MeshObjectPropertyChange(
                                    this,
                                    current.getKey(),
                                    current.getValue(),
                                    current.getKey().getDefaultValue() ));
                        }
                    }
                }
                for( PropertyType current : removedProperties.keySet() ) {
                    theProperties.remove( current );
                }
                if( theProperties.isEmpty() ) {
                    theProperties = null;
                }
            }
            if( neighbors != null ) {
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    nMgr.purge(
                            this,
                            nMgr.determineRelationshipIndex( this, neighborIdentifiers[i] ),
                            neighbors[i],
                            nMgr.determineRelationshipIndex( neighbors[i], theIdentifier ));
                }
            }
        }

        appendChangeToTransaction( new MeshObjectUnblessChange(
                this,
                oldTypes,
                types,
                getEntityTypes() ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasRoleAttribute(
            MeshObject neighbor,
            String     name )
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        if( name == null ) {
            throw new NullPointerException();
        }

        AMeshObjectNeighborManager nMgr = getNeighborManager();
        boolean ret = false;

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            int index = nMgr.determineRelationshipIndex( this, (AMeshObjectIdentifier) neighbor.getIdentifier());
            if( index < 0 ) {
                throw new IllegalRoleAttributeException( this, neighbor, name );
            }

            getAccessManager().checkPermittedGetRoleAttribute( this, neighbor, name );

            if( theNeighborRoleAttributes[index] != null ) {
                ret = theNeighborRoleAttributes[index].containsKey( name );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable [] getRoleAttributeValues(
            MeshObject neighbor,
            String []  names )
        throws
            IllegalRoleAttributeException,
            NotPermittedException
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );
        if( names.length == 0 ) {
            return PropertyValue.EMPTY_ARRAY;
        }

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        Serializable []            ret       = new Serializable[ names.length ];

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            int index = nMgr.determineRelationshipIndex( this, (AMeshObjectIdentifier) neighbor.getIdentifier());
            if( index < 0 ) {
                throw new IllegalRoleAttributeException( this, neighbor, names[0] );
            }

            for( String name : names ) {
                accessMgr.checkPermittedGetRoleAttribute( this, neighbor, name );
            }

            if( theNeighborRoleAttributes[index] != null ) {
                for( int i=0 ; i<names.length ; ++i ) {
                    if( theNeighborRoleAttributes[index].containsKey( names[i] )) {
                        ret[i] = theNeighborRoleAttributes[index].get( names[i] );
                    } else {
                        throw new IllegalRoleAttributeException( this, neighbor, names[i] );
                    }
                }
            } else {
                throw new IllegalRoleAttributeException( this, neighbor, names[0] );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable [] setRoleAttributeValues(
            MeshObject      neighbor,
            String []       names,
            Serializable [] newValues )
        throws
            CannotRelateToItselfException,
            NotPermittedException,
            TransactionException
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );
        if( names.length != newValues.length ) {
            throw new IllegalArgumentException( "Attribute names and values must have same length" );
        }
        if( names.length == 0 ) {
            return new Serializable[0];
        }

        checkTransaction();

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        Serializable []       oldValues      = new Serializable[ names.length ];
        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        String [] existing;
        String [] added      = new String[ names.length ];
        int       addedCount = 0;

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            // check whether we need to look for "create" permission in addition to "set" permission
            int index = nMgr.determineRelationshipIndex( this, realNeighborId );

            for( int i=0 ; i<names.length ; ++i ) {
                if( index < 0 || theNeighborRoleAttributes[index] == null || !theNeighborRoleAttributes[index].containsKey( names[i] )) {
                    accessMgr.checkPermittedCreateRoleAttribute( this, neighbor, names[i] );
                }
                accessMgr.checkPermittedSetRoleAttribute( this, neighbor, names[i], newValues[i] );
            }

            // now do it
            index = nMgr.findOrAddNeighbor( this, realNeighbor );
            nMgr.findOrAddNeighbor( realNeighbor, this );

            if( theNeighborRoleAttributes[index] == null ) {
                theNeighborRoleAttributes[index] = new AttributesMap();
            }
            existing = new String[ theNeighborRoleAttributes[index].size() ];
            theNeighborRoleAttributes[index].keySet().toArray( existing );

            for( int i=0 ; i<names.length ; ++i ) {
                if( !theNeighborRoleAttributes[index].containsKey( names[i] )) {
                    added[addedCount++] = names[i];
                }
                oldValues[i] = theNeighborRoleAttributes[index].put( names[i], newValues[i] );
            }
        }
        if( addedCount > 0 ) {
            appendChangeToTransaction( new MeshObjectRoleAttributesAddChange(
                    this,
                    existing,
                    added,
                    ArrayHelper.append( existing, added, String.class ),
                    neighbor ));
        }

        for( int i=0 ; i<names.length ; ++i ) {
            if(    ( oldValues[i] == null && newValues[i] != null )
                || ( oldValues[i] != null && !oldValues[i].equals( newValues[i] )))
            {
                appendChangeToTransaction( new MeshObjectRoleAttributeChange(
                        this,
                        names[i],
                        oldValues[i],
                        newValues[i],
                        neighbor ));
            }
        }
        return oldValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable [] deleteRoleAttributes(
            MeshObject neighbor,
            String []  names )
        throws
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        if( names.length == 0 ) {
            return new Serializable[0];
        }

        checkTransaction();

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        Serializable []       oldValues      = new Serializable[ names.length ];
        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        String [] existing;

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            int index = nMgr.determineRelationshipIndex( this, realNeighborId );
            if( index < 0 ) {
                throw new IllegalRoleAttributeException( this, neighbor, names[0] );
            }
            if( theNeighborRoleAttributes[index] == null ) {
                throw new IllegalRoleAttributeException( this, neighbor, names[0] );
            }

            existing = new String[ theNeighborRoleAttributes[index].size() ];
            theNeighborRoleAttributes[index].keySet().toArray( existing );

            for( String name : names ) {
                if( !theNeighborRoleAttributes[index].containsKey( name )) {
                    throw new IllegalRoleAttributeException( this, neighbor, name );
                }
                accessMgr.checkPermittedDeleteRoleAttribute( this, neighbor, name ); // will throw
            }
            for( int i=0 ; i<names.length ; ++i ) {
                oldValues[i] = theNeighborRoleAttributes[index].remove( names[i] );
            }

            nMgr.purge(
                    this,
                    index,
                    realNeighbor,
                    nMgr.determineRelationshipIndex( realNeighbor, theIdentifier ));
        }

        for( int i=0 ; i<names.length ; ++i ) {
            if( oldValues[i] != null ) {
                appendChangeToTransaction( new MeshObjectRoleAttributeChange(
                        this,
                        names[i],
                        oldValues[i],
                        null,
                        neighbor ));
            }
        }
        appendChangeToTransaction( new MeshObjectRoleAttributesRemoveChange(
                this,
                existing,
                names,
                ArrayHelper.remove( existing, names, true, String.class ),
                neighbor ));

        return oldValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getRoleAttributeNames(
            MeshObject neighbor )
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        String [] ret;
        int count;

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            int index = nMgr.determineRelationshipIndex( this, realNeighborId );

            if( index < 0 ) {
                ret   = StringHelper.EMPTY_ARRAY;
                count = 0;

            } else if( theNeighborRoleAttributes[index] == null ) {
                ret   = StringHelper.EMPTY_ARRAY;
                count = 0;

            } else {
                ret   = new String[ theNeighborRoleAttributes[index].size() ];
                count = 0;
                for( String name : theNeighborRoleAttributes[index].keySet() ) {
                    try {
                        accessMgr.checkPermittedGetRoleAttribute( this, neighbor, name );
                        ret[count++] = name;

                    } catch( NotPermittedException ex ) {
                        log.debug(  ex );
                    }
                }
            }
        }
        if( count < ret.length ) {
            ret = ArrayHelper.copyIntoNewArray( ret, 0, count, String.class );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getRoleAttributeNames()
    {
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        MeshObjectIdentifier []    neighborIdentifiers    = nMgr.getNeighborIdentifiers( this );
        AttributesMap []           neighborRoleAttributes = nMgr.getRoleAttributes( this );

        Set<String> almost = new HashSet<>();

        synchronized( obtainSyncObject( this )) {
            checkAlive();
            
            try { // try without resolving the neighbors first
                for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
                    if( neighborRoleAttributes[i] != null ) {
                        for( String name : neighborRoleAttributes[i].keySet() ) {
                            try {
                                accessMgr.checkPermittedGetRoleAttribute( this, neighborIdentifiers[i], name );
                                almost.add( name );

                            } catch( NotPermittedException ex2 ) {
                                log.debug( ex2 );
                            }
                        }
                    }
                }
            } catch( NeedNeighborException ex ) {
                almost = new HashSet<>();
                MeshObject [] neighbors = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );

                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( neighborRoleAttributes[i] != null ) {
                        for( String name : neighborRoleAttributes[i].keySet() ) {
                            try {
                                accessMgr.checkPermittedGetRoleAttribute( this, neighbors[i], name );
                                almost.add( name );

                            } catch( NotPermittedException ex2 ) {
                                log.debug( ex2 );
                            }
                        }
                    }
                }
                
            }
        }
        String [] ret = ArrayHelper.copyIntoNewArray( almost, String.class );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <S> S aggregateRoleAttributes(
            String                         name,
            S                              initialValue,
            BiFunction<S, Serializable, S> operator)
    {
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        S                          ret       = initialValue;

        synchronized( obtainSyncObject( this ) ) {
            checkAlive();

            MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );

            if( neighborIdentifiers != null && neighborIdentifiers.length > 0 ) {
                ArrayList<Serializable> workList = new ArrayList<>();

                try { // try without resolving neighbors first
                    for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
                        try {
                            if( theNeighborRoleAttributes[i] != null && theNeighborRoleAttributes[i].containsKey( name )) {
                                accessMgr.checkPermittedGetRoleAttribute( this, neighborIdentifiers[i], name );

                                Serializable value = theNeighborRoleAttributes[i].get( name );
                                workList.add( value );
                            }
                        } catch( NotPermittedException ex2 ) {
                            log.debug( ex2 );
                        }
                    }
                    
                } catch( NeedNeighborException ex ) {
                    workList             = new ArrayList<>();
                    MeshObject [] found  = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );

                    for( int i=0 ; i<found.length ; ++i ) {
                        try {
                            accessMgr.checkPermittedGetRoleAttribute( this, found[i], name );

                            if( theNeighborRoleAttributes[i] != null && theNeighborRoleAttributes[i].containsKey( name )) {
                                Serializable value = theNeighborRoleAttributes[i].get( name );
                                workList.add( value );
                            }

                        } catch( NotPermittedException ex2 ) {
                            log.debug( ex2 );
                        }
                    }
                }
                for( Serializable value : workList ) {
                    ret = operator.apply( ret, value );
                }
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void blessRole(
            RoleType [] roleTypesToAdd,
            MeshObject  neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeBlessedAlreadyException,
            EntityNotBlessedException,
            RelationshipTypeIsAbstractException,
            TransactionException,
            NotPermittedException
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        if( roleTypesToAdd.length == 0 ) {
            return;
        }

        for( RoleType toAdd: roleTypesToAdd ) {
            if( toAdd == null ) {
                throw new IllegalArgumentException( "null RoleType" );
            }
            if( toAdd.getRelationshipType().getIsAbstract().value() ) {
                throw new RelationshipTypeIsAbstractException( this, neighbor, toAdd );
            }
        }

        RoleType [] neighborRoleTypesToAdd = new RoleType[ roleTypesToAdd.length ];

        for( int i=0 ; i<roleTypesToAdd.length ; ++i ) {
            neighborRoleTypesToAdd[i] = roleTypesToAdd[i].getInverseRoleType();
        }
        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        checkTransaction();

        RoleType [] roleTypes;
        RoleType [] neighborRoleTypes;

        synchronized( obtainSyncObject( this, neighbor )) { // protects against simultaneous read access: alive, neighbors
            checkAlive();
            neighbor.checkAlive();

            // check compatibility of source and destination EntityTypes
            for( int i=0 ; i<roleTypesToAdd.length ; ++i ) {
                RoleType   otherEnd             = roleTypesToAdd[i].getInverseRoleType();
                EntityType requiredType         = roleTypesToAdd[i].getEntityType();
                EntityType requiredNeighborType = otherEnd.getEntityType();

                if( requiredType != null ) {
                    boolean found = false;
                    if( theEntityTypes != null ) {
                        for( MeshTypeWithProperties amt : theEntityTypes ) {
                            if( amt.equalsOrIsSupertype( requiredType )) {
                                found = true;
                                break;
                            }
                        }
                    }
                    if( !found ) {
                        throw new EntityNotBlessedException( this, requiredType );
                    }
                }
                if( requiredNeighborType != null ) {
                    boolean found = false;
                    if( realNeighbor.theEntityTypes != null ) {
                        for( MeshTypeWithProperties amt : realNeighbor.theEntityTypes ) {
                            if( amt.equalsOrIsSupertype( requiredNeighborType )) {
                                found = true;
                                break;
                            }
                        }
                    }
                    if( !found ) {
                        throw new EntityNotBlessedException( realNeighbor, requiredNeighborType );
                    }
                }
            }

            // determine if we are related already, if not, relate
            nMgr.findOrAddNeighbor( this, realNeighbor );
            nMgr.findOrAddNeighbor( realNeighbor, this );

            roleTypes         = nMgr.getRoleTypesFor( this, realNeighbor );
            neighborRoleTypes = nMgr.getRoleTypesFor( realNeighbor, this );

            // check we don't have those RoleTypes already
            for( RoleType toAdd : roleTypesToAdd ) {
                RoleType neighborToAdd = toAdd.getInverseRoleType();

                // make sure we do everything that might throw an exception first, and do assignments later
                if( roleTypes != null ) {
                    for( int i=0 ; i<roleTypes.length ; ++i ) {
                        if( roleTypes[i].isSubtypeOfOrEquals( toAdd )) {
                            throw new RoleTypeBlessedAlreadyException( this, toAdd, realNeighbor );

                        } else if( toAdd.isSubtypeOfOrEquals( roleTypes[i] )) {
                            roleTypes[i] = toAdd;
                            break;
                        }
                    }
                }

                if( neighborRoleTypes != null ) {
                    for( int i=0 ; i<neighborRoleTypes.length ; ++i ) {
                        if( neighborRoleTypes[i].isSubtypeOfOrEquals( neighborToAdd )) {
                            throw new RoleTypeBlessedAlreadyException( realNeighbor, neighborToAdd, realNeighbor );

                        } else if( neighborToAdd.isSubtypeOfOrEquals( neighborRoleTypes[i] )) {
                            neighborRoleTypes[i] = neighborToAdd;
                            return;
                        }
                    }
                }
            }

            accessMgr.checkPermittedBless( this, roleTypesToAdd, neighbor );
            accessMgr.checkPermittedBless( realNeighbor, neighborRoleTypesToAdd, this );

            for( RoleType thisEnd : roleTypesToAdd ) {
                RoleType otherEnd = thisEnd.getInverseRoleType();

                nMgr.appendRoleType( this, realNeighbor, thisEnd );
                nMgr.appendRoleType( realNeighbor, this, otherEnd );

                // initialize default values
                for( PropertyType pt : thisEnd.getPropertyTypes()) {
                    PropertyValue defaultValue = pt.getDefaultValue();
                    if( defaultValue != null ) {
                        nMgr.setRolePropertyValue( this, realNeighbor, pt, defaultValue );
                    }
                }
                for( PropertyType pt : otherEnd.getPropertyTypes()) {
                    PropertyValue defaultValue = pt.getDefaultValue();
                    if( defaultValue != null ) {
                        nMgr.setRolePropertyValue( realNeighbor, this, pt, defaultValue );
                    }
                }
            }
        }

        appendChangeToTransaction( new MeshObjectRoleTypeAddChange(
                this,
                roleTypes != null ? roleTypes : RoleType.EMPTY_ARRAY,
                roleTypesToAdd,
                nMgr.getRoleTypesFor( this, realNeighbor ),
                neighbor ));

        // do not emit MeshObjectRolePropertyChange -- we only set the RoleProperties to default values
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unblessRole(
            RoleType [] roleTypesToRemove,
            MeshObject  neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeNotBlessedException,
            TransactionException,
            NotPermittedException
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        if( roleTypesToRemove.length == 0 ) {
            return;
        }

        for( RoleType toRemove : roleTypesToRemove ) {
            if( toRemove == null ) {
                throw new IllegalArgumentException( "null RoleType" );
            }
        }

        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        checkTransaction();

        RoleType [] oldRoleTypesHere;
        RoleType [] oldRoleTypesThere;
        RoleType [] roleTypesToRemoveThere;
        RoleType [] newRoleTypesHere;
        RoleType [] newRoleTypesThere;

         ArrayList<MeshObjectRolePropertyChange> rolePropertyChanges = new ArrayList<>();

        synchronized( obtainSyncObject( this, neighbor )) { // protects against simultaneous read access: alive, neighbors
            checkAlive();
            neighbor.checkAlive();

            int index         = nMgr.determineRelationshipIndex( this, realNeighborId );
            int neighborIndex = nMgr.determineRelationshipIndex( realNeighbor, theIdentifier );

            if( index < 0 ) {
                throw new NotRelatedException( this, realNeighbor );
            }
            if( neighborIndex < 0 ) {
                throw new NotRelatedException( realNeighbor, this );
            }

            oldRoleTypesHere = nMgr.getRoleTypesFor( this, realNeighbor );

            for( RoleType thisEnd : roleTypesToRemove ) {
                if( oldRoleTypesHere == null || !ArrayHelper.isIn( thisEnd, oldRoleTypesHere, false )) {
                    throw new RoleTypeNotBlessedException( this, thisEnd, realNeighbor );
                }
            }

            oldRoleTypesThere = nMgr.getRoleTypesFor( realNeighbor, this );

            roleTypesToRemoveThere = new RoleType[ roleTypesToRemove.length ];

            for( int i=0 ; i<roleTypesToRemove.length ; ++i ) {
                roleTypesToRemoveThere[i] = roleTypesToRemove[i].getInverseRoleType();

                if( oldRoleTypesThere == null || !ArrayHelper.isIn( roleTypesToRemoveThere[i], oldRoleTypesThere, false )) {
                    throw new RoleTypeNotBlessedException( realNeighbor, roleTypesToRemoveThere[i], realNeighbor );
                }
            }

            accessMgr.checkPermittedUnbless( this, roleTypesToRemove, neighbor );
            accessMgr.checkPermittedUnbless( realNeighbor, roleTypesToRemoveThere, this );

            PropertiesMap roleProperties         = nMgr.getRolePropertiesFor( this, realNeighbor );
            PropertiesMap neighborRoleProperties = nMgr.getRolePropertiesFor( realNeighbor, this );

            for( RoleType thisEnd : roleTypesToRemove ) {

                RoleType otherEnd = thisEnd.getInverseRoleType();

                if( roleProperties != null ) {
                    for( PropertyType propertyType : thisEnd.getPropertyTypes() ) {
                        PropertyValue propertyValue = roleProperties.get( propertyType );
                        if( propertyValue != null ) {
                            rolePropertyChanges.add(new MeshObjectRolePropertyChange(
                                    this,
                                    propertyType,
                                    propertyValue,
                                    null,
                                    neighbor ));
                        }
                    }
                }
                if( neighborRoleProperties != null ) {
                    for( PropertyType propertyType : otherEnd.getPropertyTypes() ) {
                        PropertyValue propertyValue = neighborRoleProperties.get( propertyType );
                        if( propertyValue != null ) {
                            rolePropertyChanges.add(new MeshObjectRolePropertyChange(
                                    neighbor,
                                    propertyType,
                                    propertyValue,
                                    null,
                                    this ));
                        }
                    }
                }

                nMgr.removeRoleType( this, realNeighbor, thisEnd );
                nMgr.removeRoleType( realNeighbor, this, otherEnd );
            }
            if( nMgr.purge( this,  index, realNeighbor, neighborIndex )) {
                newRoleTypesHere  = RoleType.EMPTY_ARRAY;
                newRoleTypesThere = RoleType.EMPTY_ARRAY;
            } else {
                newRoleTypesHere  = nMgr.getRoleTypesFor( this, realNeighbor );
                newRoleTypesThere = nMgr.getRoleTypesFor( realNeighbor, this );
            }
        }

        if( !rolePropertyChanges.isEmpty() ) {
            for( MeshObjectRolePropertyChange current : rolePropertyChanges ) {
                appendChangeToTransaction( current );
            }
        }

        appendChangeToTransaction( new MeshObjectRoleTypeRemoveChange(
                this,
                oldRoleTypesHere,
                roleTypesToRemove,
                newRoleTypesHere != null ? newRoleTypesHere : RoleType.EMPTY_ARRAY,
                realNeighbor ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasRoleProperty(
            MeshObject   neighbor,
            PropertyType pt )
        throws
            NotPermittedException
    {
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );
        if( pt == null ) {
            throw new NullPointerException();
        }

        AMeshObjectNeighborManager nMgr = getNeighborManager();

        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, theEntityTypes, checkPermissions
            checkAlive();

            int index = nMgr.determineRelationshipIndex( this, realNeighborId );
            if( index < 0 ) {
                return false;
            }
            try {
                getAccessManager().checkPermittedGetRoleProperty( this, realNeighbor, pt );

            } catch( NotPermittedException ex ) {
                return false;
            }

            RoleType [] rts = nMgr.getRoleTypesFor( this, realNeighbor );
            if( rts == null ) {
                return false;
            }
            RoleType requiredType = (RoleType) pt.getMeshTypeWithProperties();
            boolean  found        = false;

            for( RoleType type : rts ) {
                if( type.equalsOrIsSupertype( requiredType )) {
                    found = true;
                    break;
                }
            }
            return found;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType [] getRolePropertyTypes()
    {
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );
        RoleType [][]           neighborRoleTypes   = nMgr.getRoleTypes( this );

        Set<PropertyType> almost = new HashSet<>();

        synchronized( obtainSyncObject( this )) {
            checkAlive();
            
            try { // try without resolving the neighbors first
                for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
                    if( neighborRoleTypes[i] != null ) {
                        for( RoleType rt : neighborRoleTypes[i] ) {
                            for( PropertyType pt : rt.getPropertyTypes() ) {
                                try {
                                    accessMgr.checkPermittedGetRoleProperty( this, neighborIdentifiers[i], pt );
                                    almost.add( pt );

                                } catch( NotPermittedException ex2 ) {
                                    log.debug( ex2 );
                                }
                            }
                        }
                    }
                }

            } catch( NeedNeighborException ex ) {
                almost = new HashSet<>();

                MeshObject [] neighbors  = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers ); // do this in one swoop
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( neighborRoleTypes[i] != null ) {
                        for( RoleType rt : neighborRoleTypes[i] ) {
                            for( PropertyType pt : rt.getPropertyTypes() ) {
                                try {
                                    accessMgr.checkPermittedGetRoleProperty( this, neighbors[i], pt );
                                    almost.add( pt );

                                } catch( NotPermittedException ex2 ) {
                                    log.debug( ex2 );
                                }
                            }
                        }
                    }
                }
            }
        }
        PropertyType [] ret = ArrayHelper.copyIntoNewArray( almost, PropertyType.class );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue [] getRolePropertyValues(
            MeshObject      neighbor,
            PropertyType [] pts )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            NotPermittedException
    {
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        if( pts.length == 0 ) {
            return PropertyValue.EMPTY_ARRAY;
        }
        for( int i=0 ; i<pts.length ; ++i ) {
            if( pts[i] == null ) {
                throw new NullPointerException();
            }
        }

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        PropertyValue []      ret            = new PropertyValue[ pts.length ];
        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            if( nMgr.determineRelationshipIndex( this, realNeighborId ) < 0 ) {
                throw new IllegalRolePropertyTypeException( this, neighbor, pts[0] );
            }

            RoleType [] roleTypes = nMgr.getRoleTypesFor( this, realNeighbor );

            for( int i=0 ; i<pts.length ; ++i ) {
                RoleType requiredType = (RoleType) pts[i].getMeshTypeWithProperties();
                boolean  found        = false;

                if( roleTypes != null ) {
                    for( RoleType type : roleTypes ) {
                        if( type.equalsOrIsSupertype( requiredType )) {
                            found = true;
                            break;
                        }
                    }
                }
                if( !found ) {
                    throw new IllegalRolePropertyTypeException( this, neighbor, pts[i] );
                }
            }

            for( PropertyType currentType : pts ) {
                accessMgr.checkPermittedGetRoleProperty( this, neighbor, currentType );
            }

            int index = nMgr.determineRelationshipIndex( this, realNeighborId );

            if( theNeighborRoleProperties[index] != null ) {
                for( int i=0 ; i<pts.length ; ++i ) {
                    ret[i] = theNeighborRoleProperties[index].get( pts[i] );
                }
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue [] setRolePropertyValues(
            MeshObject       neighbor,
            PropertyType []  pts,
            PropertyValue [] newValues )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        if( pts.length != newValues.length ) {
            throw new IllegalArgumentException( "PropertyTypes and PropertyValues must have same length" );
        }
        if( pts.length == 0 ) {
            return PropertyValue.EMPTY_ARRAY;
        }

        for( int i=0 ; i<pts.length ; ++i ) {
            if( pts[i] == null ) {
                throw new NullPointerException();
            }
            if( pts[i].getIsReadOnly().value() ) {
                throw new PropertyReadOnlyException( this, pts[i] );
            }
            DataType type = pts[i].getDataType();
            if( newValues[i] != null ) {
                try {
                    if( type.conforms( newValues[i] ) != 0 ) {
                        throw new IllegalPropertyValueException( this, pts[i], newValues[i] );
                    }
                } catch( ClassCastException ex ) {
                    throw new IllegalPropertyValueException( this, pts[i], newValues[i] );
                }
            }
        }

        checkTransaction();

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        AMeshObject           realNeighbor   = (AMeshObject) neighbor;
        AMeshObjectIdentifier realNeighborId = realNeighbor.getIdentifier();

        PropertyValue [] oldValues = new PropertyValue[ pts.length ];

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            RoleType [] roleTypes = nMgr.getRoleTypesFor( this, realNeighbor );

            for( int i=0 ; i<pts.length ; ++i ) {
                RoleType requiredType = (RoleType) pts[i].getMeshTypeWithProperties();
                boolean  found        = false;

                if( roleTypes != null ) {
                    for( RoleType type : roleTypes ) {
                        if( type.equalsOrIsSupertype( requiredType )) {
                            found = true;
                            break;
                        }
                    }
                }
                if( !found ) {
                    throw new IllegalRolePropertyTypeException( this, neighbor, pts[i] );
                }
            }

            for( int i=0 ; i<pts.length ; ++i ) {
                accessMgr.checkPermittedSetRoleProperty( this, neighbor, pts[i], newValues[i] );
            }

            int index = nMgr.determineRelationshipIndex( this, realNeighborId );

            if( theNeighborRoleProperties[index] == null ) {
                theNeighborRoleProperties[index] = new PropertiesMap();
            }

            for( int i=0 ; i<pts.length ; ++i ) {
                oldValues[i] = theNeighborRoleProperties[index].put( pts[i], newValues[i] );
            }
        }

        for( int i=0 ; i<pts.length ; ++i ) {
            if( PropertyValue.compare( oldValues[i], newValues[i] ) != 0 ) {

                appendChangeToTransaction( new MeshObjectRolePropertyChange(
                        this,
                        pts[i],
                        oldValues[i],
                        newValues[i],
                        neighbor ));
            }
        }

        return oldValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <S> S aggregateRoles(
            PropertyType                  pt,
            S                             initialValue,
            BiFunction<S,PropertyValue,S> operator )
    {
        MeshTypeWithProperties mtwp = pt.getMeshTypeWithProperties();
        if( !( mtwp instanceof RoleType )) {
            throw new IllegalArgumentException( "PropertyType is not defined on a RoleType:" + pt.toString() );
        }
        RoleType                   rt        = (RoleType) mtwp;
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        S                          ret       = initialValue;

        synchronized( obtainSyncObject( this ) ) {
            checkAlive();

            MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );

            if( neighborIdentifiers != null && neighborIdentifiers.length > 0 ) {
                ArrayList<PropertyValue> workList = new ArrayList<>();

                try { // try without resolving neighbors first
                    for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
                        if( theNeighborRoleTypes[i] != null ) {
                            boolean found = false;
                            for( int j = 0 ; j<theNeighborRoleTypes[i].length ; ++j ) {
                                if( theNeighborRoleTypes[i][j].isSubtypeOfOrEquals( rt )) {
                                    found = true;
                                    break;
                                }
                            }
                            if( found ) {
                                try {
                                    accessMgr.checkPermittedGetRoleProperty( this, neighborIdentifiers[i], pt );
                                    if( theNeighborRoleProperties[i] == null ) {
                                        workList.add( null );
                                    } else {
                                        PropertyValue value = theNeighborRoleProperties[i].get( pt );
                                        workList.add( value );
                                    }
                                } catch( NotPermittedException ex2 ) {
                                    log.debug( ex2 );
                                }
                            }
                        }
                    }
                    
                } catch( NeedNeighborException ex ) {
                    workList             = new ArrayList<>();
                    MeshObject [] found  = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );

                    for( int i=0 ; i<found.length ; ++i ) {
                        try {
                            accessMgr.checkPermittedGetRoleProperty( this, found[i], pt );

                            if( theNeighborRoleProperties[i] == null ) {
                                workList.add( null );
                            } else {
                                PropertyValue value = theNeighborRoleProperties[i].get( pt );
                                workList.add( value );
                            }

                        } catch( NotPermittedException ex2 ) {
                            log.debug( ex2 );
                        }
                    }
                }
                for( PropertyValue value : workList ) {
                    ret = operator.apply( ret, value );
                }
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            TraverseSpecification theTraverseSpec )
    {
        if( !( theTraverseSpec instanceof RoleType )) {
            return theTraverseSpec.traverse( this );
        }
        RoleType rt = (RoleType) theTraverseSpec;

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            MeshObjectIdentifier [] neighborIds = nMgr.getNeighborIdentifiers( this );
            RoleType [][]           roleTypes   = nMgr.getRoleTypes( this );

            MeshObjectSet ret;
            if( neighborIds != null && neighborIds.length > 0 ) {
                int count = 0;
                MeshObjectIdentifier [] almost = new MeshObjectIdentifier[ neighborIds.length ];

                for( int i=0 ; i<neighborIds.length ; ++i ) {
                    if( roleTypes[i] != null ) {
                        for( int j=0 ; j<roleTypes[i].length ; ++j ) {
                            if( roleTypes[i][j].isSubtypeOfOrEquals( rt ) ) {
                                almost[count++] = neighborIds[i];
                                break;
                            }
                        }
                    }
                }
                if( count < almost.length ) {
                    almost = ArrayHelper.copyIntoNewArray( almost, 0, count, MeshObjectIdentifier.class );
                }

                MeshObject [] almostRet  = findRelatedMeshObjects( theMeshBaseView, almost ); // do this in one swoop
                MeshObject [] almostRet2 = theMeshBaseView.createArray( almostRet.length );

                count = 0;
                for( MeshObject current : almostRet ) {
                    try {
                        accessMgr.checkPermittedTraverse( this, rt, current );
                        almostRet2[ count++ ] = current;

                    } catch( NotPermittedException ex ) {
                        log.debug( this, current, count, ex );
                    }
                }
                if( count < almostRet2.length ) {
                    almostRet2 = ArrayHelper.copyIntoNewArray( almostRet2, 0, count, getClass() );
                }
                ret = getMeshBase().getMeshObjectSetFactory().createImmutableMeshObjectSet( almostRet2 );

            } else {
                ret = MeshObjectSet.DEFAULT_EMPTY_SET;
            }
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int traverseCount(
            RoleType rt )
    {
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        int                        ret       = 0;

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            MeshObjectIdentifier [] neighborIds = nMgr.getNeighborIdentifiers( this );
            RoleType [][]           roleTypes   = nMgr.getRoleTypes( this );

            if( neighborIds != null && neighborIds.length > 0 ) {
                int count = 0;
                MeshObjectIdentifier [] almost = new MeshObjectIdentifier[ neighborIds.length ];

                for( int i=0 ; i<neighborIds.length ; ++i ) {
                    if( roleTypes[i] != null ) {
                        for( int j=0 ; j<roleTypes[i].length ; ++j ) {
                            if( roleTypes[i][j].isSubtypeOfOrEquals( rt ) ) {
                                almost[count++] = neighborIds[i];
                                break;
                            }
                        }
                    }
                }
                if( count < almost.length ) {
                    almost = ArrayHelper.copyIntoNewArray( almost, 0, count, MeshObjectIdentifier.class );
                }

                try { // try without resolving neighbors first
                    for( MeshObjectIdentifier current : almost ) {
                        try {
                            accessMgr.checkPermittedTraverse( this, rt, current );
                            ++count;

                        } catch( NotPermittedException ex2 ) {
                            log.debug( this, current, count, ex2 );
                        }
                    }
                    
                } catch( NeedNeighborException ex ) {
                    ret = 0;
                    MeshObject [] almostRet  = findRelatedMeshObjects( theMeshBaseView, almost ); // do this in one swoop

                    for( MeshObject current : almostRet ) {
                        try {
                            accessMgr.checkPermittedTraverse( this, rt, current );
                            ++count;

                        } catch( NotPermittedException ex2 ) {
                            log.debug( this, current, count, ex2 );
                        }
                    }
                }
            }
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getRoleTypes()
    {
        HashSet<RoleType> almost;
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this )) {
            checkAlive();

            MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );
            RoleType [][]           roleTypes           = nMgr.getRoleTypes( this );

            if( neighborIdentifiers == null || neighborIdentifiers.length == 0 ) {
                return RoleType.EMPTY_ARRAY;
            }

            int n  = neighborIdentifiers.length;
            almost = new HashSet<>( n ); // fudge

            if( roleTypes != null ) {
                try { // try without neighbor resolution first
                    for( int i=0 ; i<roleTypes.length ; ++i ) {
                        if( roleTypes[i] != null ) {
                            for( int j=0 ; j<roleTypes[i].length ; ++j ) {

                                try {
                                    accessMgr.checkPermittedTraverse(
                                            this,
                                            roleTypes[i][j],
                                            neighborIdentifiers[i] );

                                    almost.add( roleTypes[i][j] );
                                } catch( NotPermittedException ex2 ) {
                                    log.debug( ex2 );
                                }
                            }
                        }
                    }
                    
                } catch( NeedNeighborException ex ) {
                    almost = new HashSet<>( n ); // fudge
                    MeshObject [] neighbors = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );

                    for( int i=0 ; i<roleTypes.length ; ++i ) {
                        if( roleTypes[i] != null ) {
                            for( int j=0 ; j<roleTypes[i].length ; ++j ) {

                                try {
                                    accessMgr.checkPermittedTraverse(
                                            this,
                                            roleTypes[i][j],
                                            neighbors[i] );

                                    almost.add( roleTypes[i][j] );

                                } catch( NotPermittedException ex2 ) {
                                    log.debug( ex2 );
                                }
                            }
                        }
                    }
                }
            }
        }

        RoleType [] ret = new RoleType[ almost.size() ];
        almost.toArray( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getRoleTypes(
            MeshObject neighbor )
    {
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), neighbor.getMeshBaseView() );

        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();
        HashSet<RoleType>          almost;

        synchronized( obtainSyncObject( this )) {
            checkAlive();
            neighbor.checkAlive();

            MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );
            RoleType [][]           roleTypes           = nMgr.getRoleTypes( this );

            if( neighborIdentifiers == null || neighborIdentifiers.length == 0 ) {
                return RoleType.EMPTY_ARRAY;
            }

            if( neighborIdentifiers.length == 0 ) {
                almost = null;

            } else {
                almost = new HashSet<>();
                for( int i=0 ; i<neighborIdentifiers.length ; ++i ) {
                    if( neighbor.getIdentifier().equals( neighborIdentifiers[i] )) {
                        if( roleTypes[i] != null ) {
                            for( int j=0 ; j<roleTypes[i].length ; ++j ) {

                                try {
                                    accessMgr.checkPermittedTraverse(
                                            this,
                                            roleTypes[i][j],
                                            neighbor );

                                    almost.add( roleTypes[i][j] );

                                } catch( NotPermittedException ex ) {
                                    log.debug( ex );
                                }
                            }
                        }
                    }
                }
            }
        }

        if( almost == null || almost.isEmpty() ) {
            return RoleType.EMPTY_ARRAY;
        } else {
            RoleType [] ret = ArrayHelper.copyIntoNewArray( almost, RoleType.class );
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete()
        throws
            TransactionException
    {
        AMeshObjectNeighborManager nMgr      = getNeighborManager();
        AccessManager              accessMgr = getAccessManager();

        // on our side
        AttributesMap                   removedAttributes = null;
        EntityType []                   removedTypes      = null;
        Map<PropertyType,PropertyValue> changedProperties = null;

        Map<MeshObject,AttributesMap>                   removedMyRoleAttributes = null; // keyed by neighbor
        Map<MeshObject,RoleType[]>                      removedMyRoleTypes      = null;
        Map<MeshObject,Map<PropertyType,PropertyValue>> changedMyRoleProperties = null;

        // on the neighbors' side
        Map<MeshObject,RoleType[]>                      removedNeighborRoleTypes      = null;
        Map<MeshObject,AttributesMap>                   removedNeighborRoleAttributes = null;
        Map<MeshObject,Map<PropertyType,PropertyValue>> changedNeighborRoleProperties = null;

        checkTransaction();

        synchronized( obtainSyncObject( this )) {

            // first check permissions for everything -- start with Entity stuff
            // then do all the relationship stuff, from checking to modifications
            // then do the entity modifications
            // Note: we might not need to check property access permissions if we check for unbless permissions

            // check Attributes
            if( theAttributes != null && !theAttributes.isEmpty()) {
                for( String name : theAttributes.keySet() ) {
                    accessMgr.checkPermittedDeleteAttribute( this, name );
                }
            }

            // check Properties
            if( theProperties != null && !theProperties.isEmpty()) {
                for( Map.Entry<PropertyType,PropertyValue> current : theProperties.entrySet() ) {
                    if( PropertyValue.compare( current.getKey().getDefaultValue(), current.getValue() ) != 0 ) {
                        accessMgr.checkPermittedSetProperty( this, current.getKey(), current.getKey().getDefaultValue() );
                    }
                }
            }

            // check EntityTypes
            if( theEntityTypes != null && !theEntityTypes.isEmpty()) {
                removedTypes = new EntityType[ theEntityTypes.size() ];
                theEntityTypes.toArray( removedTypes );
                accessMgr.checkPermittedUnbless( this, removedTypes );
            }

            if( nMgr.hasNeighbors( this ) ) {
                MeshObjectIdentifier [] neighborIdentifiers = nMgr.getNeighborIdentifiers( this );
                RoleType [][]           myRoleTypes         = nMgr.getRoleTypes( this );
                PropertiesMap []        myRolePropertyTypes = nMgr.getRolePropertyTypes( this );
                AttributesMap []        myRoleAttributes    = nMgr.getRoleAttributes( this );

                AMeshObject [] neighbors = findRelatedMeshObjects( theMeshBaseView, neighborIdentifiers );

                // first we check that we have permissions for everything

                //   RoleAttributes
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( myRoleAttributes[i] != null && !myRoleAttributes[i].isEmpty() ) {
                        for( String name : myRoleAttributes[i].keySet() ) {
                            accessMgr.checkPermittedDeleteRoleAttribute( this, neighbors[i], name );
                        }
                    }
                    AttributesMap neighborRoleAttributes = nMgr.getRoleAttributesFor( neighbors[i], this );
                    if( neighborRoleAttributes != null ) {
                        for( String name : neighborRoleAttributes.keySet() ) {
                            accessMgr.checkPermittedDeleteRoleAttribute( neighbors[i], this, name );
                        }
                    }
                }

                //   RoleProperties
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( myRolePropertyTypes[i] != null && !myRolePropertyTypes[i].isEmpty()) {
                        for( Map.Entry<PropertyType,PropertyValue> current : myRolePropertyTypes[i].entrySet() ) {
                            if( PropertyValue.compare( current.getKey().getDefaultValue(), current.getValue() ) != 0 ) {
                                accessMgr.checkPermittedSetRoleProperty( this, neighbors[i], current.getKey(), current.getKey().getDefaultValue() );
                            }
                        }
                    }
                    PropertiesMap neighborRolePropertyTypes = nMgr.getRolePropertiesFor( neighbors[i], this );
                    if( neighborRolePropertyTypes != null && !neighborRolePropertyTypes.isEmpty() ) {
                        for( Map.Entry<PropertyType,PropertyValue> current : neighborRolePropertyTypes.entrySet() ) {
                            if( PropertyValue.compare( current.getKey().getDefaultValue(), current.getValue() ) != 0 ) {
                                accessMgr.checkPermittedSetRoleProperty( neighbors[i], this, current.getKey(), current.getKey().getDefaultValue() );
                            }
                        }
                    }
                }

                //   RoleTypes
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( myRoleTypes[i] != null && myRoleTypes[i].length != 0 ) {
                        accessMgr.checkPermittedUnbless( this, myRoleTypes[i], neighbors[i] );
                    }
                    RoleType [] neighborRoleTypes = nMgr.getRoleTypesFor( neighbors[i], this );
                    if( neighborRoleTypes != null && neighborRoleTypes.length > 0 ) {
                        accessMgr.checkPermittedUnbless( neighbors[i], neighborRoleTypes, this );
                    }
                }

                // now we modify this MeshObject and the neighbor MeshObjects

                //   RoleAttributes
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( myRoleAttributes[i] != null && !myRoleAttributes[i].isEmpty() ) {
                        if( removedMyRoleAttributes == null ) {
                            removedMyRoleAttributes = new HashMap<>();
                        }
                        removedMyRoleAttributes.put( neighbors[i], myRoleAttributes[i] );
                    }
                    AttributesMap neighborRoleAttributes = nMgr.getRoleAttributesFor( neighbors[i], this );
                    if( neighborRoleAttributes != null && !neighborRoleAttributes.isEmpty() ) {
                        if( removedNeighborRoleAttributes == null ) {
                            removedNeighborRoleAttributes = new HashMap<>();
                        }
                        removedNeighborRoleAttributes.put( neighbors[i], neighborRoleAttributes );
                    }
                }

                //   RoleProperties
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( myRolePropertyTypes[i] != null && !myRolePropertyTypes[i].isEmpty()) {
                        for( Map.Entry<PropertyType,PropertyValue> current : myRolePropertyTypes[i].entrySet() ) {
                            if( PropertyValue.compare( current.getKey().getDefaultValue(), current.getValue() ) != 0 ) {
                                if( changedMyRoleProperties == null ) {
                                    changedMyRoleProperties = new HashMap<>();
                                }
                                Map<PropertyType,PropertyValue> changedHere = changedMyRoleProperties.get( neighbors[i] );
                                if( changedHere == null ) {
                                    changedHere = new HashMap<>();
                                    changedMyRoleProperties.put( neighbors[i], changedHere );
                                }
                                changedHere.put( current.getKey(), current.getValue() );
                            }
                        }
                    }
                    PropertiesMap neighborRolePropertyTypes = nMgr.getRolePropertiesFor( neighbors[i], this );
                    if( neighborRolePropertyTypes != null && !neighborRolePropertyTypes.isEmpty() ) {
                        for( Map.Entry<PropertyType,PropertyValue> current : neighborRolePropertyTypes.entrySet() ) {
                            if( PropertyValue.compare( current.getKey().getDefaultValue(), current.getValue() ) != 0 ) {
                                if( changedNeighborRoleProperties == null ) {
                                    changedNeighborRoleProperties = new HashMap<>();
                                }
                                Map<PropertyType,PropertyValue> changedThere = changedNeighborRoleProperties.get( neighbors[i] );
                                if( changedThere == null ) {
                                    changedThere = new HashMap<>();
                                    changedNeighborRoleProperties.put( neighbors[i], changedThere );
                                }
                                changedThere.put( current.getKey(), current.getValue() );
                            }
                        }
                    }
                }

                //   RoleTypes
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    if( myRoleTypes[i] != null && myRoleTypes[i].length != 0 ) {
                        if( removedMyRoleTypes == null ) {
                            removedMyRoleTypes = new HashMap<>();
                        }
                        removedMyRoleTypes.put( neighbors[i], myRoleTypes[i] );
                    }
                    RoleType [] neighborRoleTypes = nMgr.getRoleTypesFor( neighbors[i], this );
                    if( neighborRoleTypes != null && neighborRoleTypes.length > 0 ) {
                        if( removedNeighborRoleTypes == null ) {
                            removedNeighborRoleTypes = new HashMap<>();
                        }
                        removedNeighborRoleTypes.put( neighbors[i], neighborRoleTypes );
                    }
                }

                // remove relationship from the MeshObject and from the neighbors
                nMgr.removeAllNeighbors( this );
                for( int i=0 ; i<neighbors.length ; ++i ) {
                    nMgr.removeNeighbor( neighbors[i], this );
                }
            }

            // Attributes
            if( theAttributes != null && !theAttributes.isEmpty()) {
                removedAttributes = theAttributes;
                theAttributes = null;
            }

            // Properties
            if( theProperties != null && !theProperties.isEmpty()) {
                for( Map.Entry<PropertyType,PropertyValue> current : theProperties.entrySet() ) {
                    if( PropertyValue.compare( current.getKey().getDefaultValue(), current.getValue() ) != 0 ) {
                        if( changedProperties == null ) {
                            changedProperties = new HashMap<>();
                        }
                        changedProperties.put( current.getKey(), current.getValue() );
                    }
                }
                theProperties = null;
            }

            // EntityTypes
            if( theEntityTypes != null && !theEntityTypes.isEmpty()) {
                theEntityTypes = null;
            }

            theState = State.DEAD; // this needs to happen rather late so the other code still works
        }

        // now we add them to the Transaction

        // RoleAttribute events
        addRoleAttributesRemovedToTx( removedMyRoleAttributes, true );
        addRoleAttributesRemovedToTx( removedNeighborRoleAttributes, false );

        // RoleProperty events
        addRolePropertiesChangedToTx( changedMyRoleProperties, true );
        addRolePropertiesChangedToTx( changedNeighborRoleProperties, false );

        // RoleType events
        addRoleTypesRemovedToTx( removedMyRoleTypes, removedNeighborRoleTypes );

        // Attribute events
        if( removedAttributes != null && !removedAttributes.isEmpty() ) {
            String [] removedAttributeNames = new String[ removedAttributes.size() ];
            removedAttributes.keySet().toArray( removedAttributeNames );

            for( Map.Entry<String,Serializable> current : removedAttributes.entrySet() ) {
                appendChangeToTransaction( new MeshObjectAttributeChange(
                        this,
                        current.getKey(),
                        current.getValue(),
                        null ));
            }

            appendChangeToTransaction( new MeshObjectAttributesRemoveChange(
                    this,
                    removedAttributeNames,
                    removedAttributeNames,
                    StringHelper.EMPTY_ARRAY ));
        }

        // Property events
        if( changedProperties != null && !changedProperties.isEmpty() ) {
            for( Map.Entry<PropertyType,PropertyValue> current : changedProperties.entrySet() ) {
                appendChangeToTransaction( new MeshObjectPropertyChange(
                        this,
                        current.getKey(),
                        current.getValue(),
                        current.getKey().getDefaultValue() ));
            }
        }

        // EntityType events
        if( removedTypes != null && removedTypes.length > 0 ) {
            appendChangeToTransaction( new MeshObjectUnblessChange(
                    this,
                    removedTypes,
                    removedTypes,
                    EntityType.EMPTY_ARRAY ));
        }
    }

    /**
     * Helper method to insert removed-role-attribute events into a Transaction's changes.
     *
     * @param removedRoleAttributes if forward is true, the set of RoleAttributes on our end; if false, the
     *        set of RoleAttributes on the neighbor's end
     * @param forward if true, the RoleAttributes were removed from this MeshObject; false if they were removed
     *        from our neighbors
     */
    protected void addRoleAttributesRemovedToTx(
            Map<MeshObject,AttributesMap> removedRoleAttributes,
            boolean                       forward )
    {
        if( removedRoleAttributes != null && !removedRoleAttributes.isEmpty() ) {
            for( Map.Entry<MeshObject,AttributesMap> current : removedRoleAttributes.entrySet() ) {
                String [] removedAttributeNames = new String[ current.getValue().size() ];
                current.getValue().keySet().toArray( removedAttributeNames );

                for( Map.Entry<String,Serializable> current2 : current.getValue().entrySet() ) {
                    appendChangeToTransaction( new MeshObjectRoleAttributeChange(
                            forward ? this : current.getKey(),
                            current2.getKey(),
                            current2.getValue(),
                            null,
                            forward ? current.getKey() : this ));
                }

                appendChangeToTransaction( new MeshObjectRoleAttributesRemoveChange(
                        forward ? this : current.getKey(),
                        removedAttributeNames,
                        removedAttributeNames,
                        StringHelper.EMPTY_ARRAY,
                        forward ? current.getKey() : this ));
            }
        }
    }

    /**
     * Helper method to insert role-attribute-changed events into a Transaction's changes.
     *
     * @param rolePropertiesChanged if forward is true, the set of RoleProperties on our end; if false, the
     *        set of RoleProperties on the neighbor's end
     * @param forward if true, the RoleProperties were changed on this MeshObject; false if they were changed
     *        on our neighbors
     */
    protected void addRolePropertiesChangedToTx(
            Map<MeshObject,Map<PropertyType,PropertyValue>> rolePropertiesChanged,
            boolean                                         forward )
    {
        if( rolePropertiesChanged != null && !rolePropertiesChanged.isEmpty() ) {
            for( Map.Entry<MeshObject,Map<PropertyType,PropertyValue>> current : rolePropertiesChanged.entrySet() ) {

                for( Map.Entry<PropertyType,PropertyValue> current2 : current.getValue().entrySet() ) {
                    appendChangeToTransaction( new MeshObjectRolePropertyChange(
                            forward ? this : current.getKey(),
                            current2.getKey(),
                            current2.getValue(),
                            null,
                            forward ? current.getKey() : this ));
                }
            }
        }
    }

    /**
     * Helper method to insert removed-role-types events into a Transaction's changes.
     * This assumes that all RoleTypes between this MeshObject and the listed MeshObjects have been removed.
     *
     * @param roleTypesRemoved the set of removed RoleTypes on our end
     * @param neighborRoleTypesRemoved the set of removed RoleTypes on our neighbor's end
     */
    protected void addRoleTypesRemovedToTx(
            Map<MeshObject,RoleType[]> roleTypesRemoved,
            Map<MeshObject,RoleType[]> neighborRoleTypesRemoved )
    {
        if( roleTypesRemoved != null && !roleTypesRemoved.isEmpty() ) {
            for( Map.Entry<MeshObject,RoleType[]> current : roleTypesRemoved.entrySet() ) {

                MeshObject  neighbor        = current.getKey();
                RoleType [] removed         = roleTypesRemoved.get( neighbor );

                appendChangeToTransaction( new MeshObjectRoleTypeRemoveChange(
                        this,
                        removed,
                        removed,
                        RoleType.EMPTY_ARRAY,
                        neighbor ));
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SimpleExternalizedMeshObject asExternalized()
    {
        MeshTypeIdentifier [] types;
        String             [] attributeNames;
        Serializable       [] attributeValues;
        MeshTypeIdentifier [] propertyTypes;
        PropertyValue      [] propertyValues;
        SimpleExternalizedMeshObject.ThisEnd [] thisEnds;

        synchronized( obtainSyncObject( this )) {
            if( theEntityTypes != null && !theEntityTypes.isEmpty() ) {
                types = new MeshTypeIdentifier[ theEntityTypes.size() ];

                int i=0;
                for( EntityType current : theEntityTypes ) {
                    types[i++] = current.getIdentifier();
                }
            } else {
                types = null;
            }

            if( theAttributes != null && !theAttributes.isEmpty() ) {
                attributeNames  = new String[ theAttributes.size() ];
                attributeValues = new Serializable[ attributeNames.length ];

                int i=0;
                for( String current : theAttributes.keySet() ) {
                    attributeNames[i]  = current;
                    attributeValues[i] = theAttributes.get( current );
                    ++i;
                }
            } else {
                attributeNames  = null;
                attributeValues = null;
            }

            if( theProperties != null && !theProperties.isEmpty() ) {
                propertyTypes  = new MeshTypeIdentifier[ theProperties.size() ];
                propertyValues = new PropertyValue[ propertyTypes.length ];

                int i=0;
                for( PropertyType current : theProperties.keySet() ) {
                    propertyTypes[i]  = current.getIdentifier();
                    propertyValues[i] = theProperties.get( current );
                    ++i;
                }
            } else {
                propertyTypes  = null;
                propertyValues = null;
            }

            if( theNeighborIdentifiers != null ) {
                thisEnds = new SimpleExternalizedMeshObject.ThisEnd[ theNeighborIdentifiers.length ];
            } else {
                thisEnds = SimpleExternalizedMeshObject.ThisEnd.EMPTY_ARRAY;
            }

            for( int i=0 ; i<thisEnds.length ; ++i ) {
                String []             roleAttributeNames;
                Serializable []       roleAttributeValues;
                MeshTypeIdentifier [] roleTypeIdentifiers;
                MeshTypeIdentifier [] rolePropertyTypeIdentifiers;
                PropertyValue []      rolePropertyTypeValues;

                if( theNeighborRoleAttributes[i] != null ) {
                    roleAttributeNames  = new String[ theNeighborRoleAttributes[i].size() ];
                    roleAttributeValues = new Serializable[ roleAttributeNames.length ];

                } else {
                    roleAttributeNames  = new String[ 0 ];
                    roleAttributeValues = new Serializable[ 0 ];
                }

                if( theNeighborRoleAttributes[i] != null ) {
                    int j=0;
                    for( HashMap.Entry<String,Serializable> current : theNeighborRoleAttributes[i].entrySet() ) {
                        roleAttributeNames[j]  = current.getKey();
                        roleAttributeValues[j] = current.getValue();
                        ++j;
                    }
                }

                if( theNeighborRoleTypes[i] != null ) {
                    roleTypeIdentifiers = new MeshTypeIdentifier[ theNeighborRoleTypes[i].length ];
                } else {
                    roleTypeIdentifiers = new MeshTypeIdentifier[ 0 ];
                }

                if( theNeighborRoleProperties[i] != null ) {
                    rolePropertyTypeIdentifiers = new MeshTypeIdentifier[ theNeighborRoleProperties[i].size() ]; // this skips unset properties
                    rolePropertyTypeValues      = new PropertyValue[ rolePropertyTypeIdentifiers.length ];
                } else {
                    rolePropertyTypeIdentifiers = new MeshTypeIdentifier[ 0 ]; // this skips unset properties
                    rolePropertyTypeValues      = new PropertyValue[ 0 ];
                }

                for( int j=0 ; j<roleTypeIdentifiers.length ; ++j ) {
                    roleTypeIdentifiers[j] = theNeighborRoleTypes[i][j].getIdentifier();
                }

                if( theNeighborRoleProperties[i] != null ) {
                    int j=0;
                    for( HashMap.Entry<PropertyType,PropertyValue> current : theNeighborRoleProperties[i].entrySet() ) {
                        rolePropertyTypeIdentifiers[j] = current.getKey().getIdentifier();
                        rolePropertyTypeValues[j]      = current.getValue();
                        ++j;
                    }
                }

                thisEnds[i] = new SimpleExternalizedMeshObject.ThisEnd(
                        theNeighborIdentifiers[i],
                        theNeighborTimeCreateds[i],
                        roleAttributeNames,
                        roleAttributeValues,
                        roleTypeIdentifiers,
                        rolePropertyTypeIdentifiers,
                        rolePropertyTypeValues );
            }
        }

        SimpleExternalizedMeshObject ret = SimpleExternalizedMeshObject.create(
                getIdentifier(),
                theTimeCreated,
                theTimeUpdated,
                attributeNames,
                attributeValues,
                types,
                propertyTypes,
                propertyValues,
                thisEnds,
                theState == State.DEAD );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        // if it has a name, return that
        try {
            PropertyType nameType = getPropertyTypeByName( "Name" );
            StringValue  name     = (StringValue) getPropertyValue( nameType );
            if( name != null && name.value().length() > 0 ) {
                return name.value();
            }
        } catch( MeshTypeNotFoundException ex ) {
            // ignore
        } catch( IllegalPropertyTypeException ex ) {
            // ignore
        } catch( NotPermittedException ex ) {
            // ignore
        } catch( ClassCastException ex ) {
            // ignore
        }

        String ret = getUserVisibleString( getEntityTypes() ); // that makes a random sequence, but that's the best we can do
        if( ret != null ) {
            return ret;
        }

        ret = theIdentifier.getLocalId();
        if( ret.length() > 0 ) {
            return ret;
        }
        return "<HOME>";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString(
            EntityType [] types )
    {
        String ret;
        if( types.length > 0 ) {
            ret = types[0].getUserVisibleName().value();
        } else {
            ret = null;
        }

        return ret;
    }

    /**
     * Find a manager for the MeshObject's neighbors.
     *
     * @return the AMeshObjectNeighborManager
     */
    public AMeshObjectNeighborManager getNeighborManager()
    {
        return AMeshObjectNeighborManager.SINGLETON;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void neighborTimeCreatedWasUpdated(
            AbstractMeshObject neighborUpdated )
    {
        AMeshObjectNeighborManager nMgr = getNeighborManager();
        nMgr.neighborTimeCreatedWasUpdated( this, (AMeshObject) neighborUpdated );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object obtainSyncObject(
            MeshObject obj )
    {
        // This trivial implementation essentially makes critical sections single-threaded
        return theMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object obtainSyncObject(
            MeshObject obj,
            MeshObject neighbor )
    {
        // This trivial implementation essentially makes critical sections single-threaded
        return theMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void appendChangeToTransaction(
            Change toAdd )
    {
        ((ATransaction)theMeshBaseView.getMeshBase().getCurrentTransaction()).addChange( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theIdentifier",
                    "theTimeCreated",
                    "theTimeUpdated",
                    "theMeshBaseView",
                    "theProperties",
                    "theMeshTypes",
                    "theAttributes",
                    "theNeighborIdentifiers",
                    "theNeighborTimeCreateds",
                    "theNeighborRoleTypes",
                    "theNeighborRoleAttributes"
                },
                new Object[] {
                    theIdentifier,
                    theTimeCreated,
                    theTimeUpdated,
                    theMeshBaseView instanceof HeadMeshBaseView ? "HEAD" : ( "History at " + ((HistoricMeshBaseView)theMeshBaseView).getTimeUpdated() ),
                    theProperties,
                    theEntityTypes,
                    theAttributes,
                    theNeighborIdentifiers,
                    theNeighborTimeCreateds,
                    theNeighborRoleTypes,
                    theNeighborRoleAttributes
                });
    }

    /**
     * Create a copy of this AMeshObject for a different MeshBaseView. This is used for inserting the current
     * snapshot into a history.
     *
     * @param mbv the MeshBaseView for the copy
     * @return the created copy
     */
    public AMeshObject createCopyForMeshBaseView(
            AbstractMeshBaseView mbv )
    {
        RoleType [][] copiedNeighborRoleTypes;
        if( theNeighborRoleTypes == null ) {
            copiedNeighborRoleTypes = null;

        } else {
            copiedNeighborRoleTypes = new RoleType[ theNeighborRoleTypes.length ][];
            for( int i=0 ; i<theNeighborRoleTypes.length ; ++i ) {
                if( theNeighborRoleTypes[i] != null ) {
                    copiedNeighborRoleTypes[i] = ArrayHelper.copyIntoNewArray( theNeighborRoleTypes[i], RoleType.class );
                }
            }
        }

        @SuppressWarnings("unchecked")
        AMeshObject ret = new AMeshObject(
                theIdentifier,
                mbv,
                theAttributes           == null ? null : theAttributes.clone(),
                theProperties           == null ? null : theProperties.clone(),
                theEntityTypes          == null ? null : (HashSet<EntityType>) theEntityTypes.clone(),
                theNeighborIdentifiers  == null ? null : ArrayHelper.copyIntoNewArray( theNeighborIdentifiers, AMeshObjectIdentifier.class ),
                theNeighborTimeCreateds == null ? null : theNeighborTimeCreateds.clone(),
                AttributesMap.cloneArray( theNeighborRoleAttributes ),
                copiedNeighborRoleTypes,
                PropertiesMap.cloneArray( theNeighborRoleProperties ),
                theState );

        ret.setTimeCreated( theTimeCreated );
        ret.setTimeUpdated( theTimeUpdated );

        return ret;
    }

    /**
      * Storage for the Identifier property.
      */
    protected final AMeshObjectIdentifier theIdentifier;

    /**
     * The set of MeshObjecs to which this MeshObject is directly related. This is
     * expressed as a set of MeshObjectIdentifiers in order to not prevent garbage collection.
     */
    protected AMeshObjectIdentifier [] theNeighborIdentifiers;

    /**
     * The timeCreated of the MeshObjects identified by theNeighborIdentifiers.
     * This is needed so we can order neighbors by their timeCreated.
     */
    protected long [] theNeighborTimeCreateds;

    /**
     * The set of sets of RoleTypes that goes with theNeighborIdentifiers.
     */
    protected RoleType [][] theNeighborRoleTypes;

    /**
     * The set of Role Properties that goes with theNeighborIdentifiers.
     */
    protected PropertiesMap [] theNeighborRoleProperties;

    /**
     * The set of Role Attributes that goes with theNeighborIdentifiers.
     */
    protected AttributesMap [] theNeighborRoleAttributes;
}
