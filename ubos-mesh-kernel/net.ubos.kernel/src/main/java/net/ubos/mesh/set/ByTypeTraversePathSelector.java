//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.model.traverse.TraversePath;
import net.ubos.util.logging.Log;
import net.ubos.model.primitives.MeshTypeWithProperties;

/**
 * A simple implementation of TraversePathSelector that accepts all TraversePaths
 whose first and/or last MeshObject matches a certain type (or subtype).
 */
public class ByTypeTraversePathSelector
        implements
            TraversePathSelector
{
    private static final Log log = Log.getLogInstance( ByTypeTraversePathSelector.class ); // our own, private logger

    /**
     * Factory method.
     * 
     * @param firstType the type we are looking for in the first MeshObject of the path -- null matches all
     * @param firstSubtypeAllowed  if true, we could also be an instance of a subtype of firstType
     * @param lastType the type we are looking for in the last MeshObject of the path -- null matches all
     * @param lastSubtypeAllowed  if true, we could also be an instance of a subtype of lastType
     * @return the created ByTypeTraversePathSelector
     */
    public static ByTypeTraversePathSelector create(
            MeshTypeWithProperties firstType,
            boolean              firstSubtypeAllowed,
            MeshTypeWithProperties lastType,
            boolean              lastSubtypeAllowed )
    {
        return new ByTypeTraversePathSelector( firstType, firstSubtypeAllowed, lastType, lastSubtypeAllowed );
    }

    /**
     * Construct one with the types that we are looking for.
     *
     * @param firstType the type we are looking for in the first MeshObject of the path -- null matches all
     * @param firstSubtypeAllowed  if true, we could also be an instance of a subtype of firstType
     * @param lastType the type we are looking for in the last MeshObject of the path -- null matches all
     * @param lastSubtypeAllowed  if true, we could also be an instance of a subtype of lastType
     */
    protected ByTypeTraversePathSelector(
            MeshTypeWithProperties firstType,
            boolean              firstSubtypeAllowed,
            MeshTypeWithProperties lastType,
            boolean              lastSubtypeAllowed )
    {
        theFirstType           = firstType;
        theFirstSubtypeAllowed = firstSubtypeAllowed;
        theLastType            = lastType;
        theLastSubtypeAllowed  = lastSubtypeAllowed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accepts(
            TraversePath candidate )
    {
        if( candidate == null ) {
            throw new IllegalArgumentException();
        }

        MeshTypeWithProperties [] candidateFirstTypes;
        try {
            candidateFirstTypes = candidate.getFirstMeshObject().getEntityTypes();

        } catch( MeshObjectsNotFoundException ex ) {
            log.error( ex );
            return false;
        }
        if( theFirstType != null ) {
            boolean found = false;
            if( theFirstSubtypeAllowed ) {
                for( int i=0 ; i<candidateFirstTypes.length ; ++i ) {
                    if( candidateFirstTypes[i].isSubtypeOfOrEquals( theFirstType )) {
                        found = true;
                        break;
                    }
                }
            } else {
                for( int i=0 ; i<candidateFirstTypes.length ; ++i ) {
                    if( candidateFirstTypes[i].equals( theFirstType )) {
                        found = true;
                        break;
                    }
                }
            }
            if( !found ) {
                return false;
            }
        }

        MeshTypeWithProperties [] candidateLastTypes = candidate.getLastMeshObject().getEntityTypes();
        if( theLastType != null ) {
            boolean found = false;
            if( theLastSubtypeAllowed ) {
                for( int i=0 ; i<candidateLastTypes.length ; ++i ) {
                    if( candidateLastTypes[i].isSubtypeOfOrEquals( theLastType )) {
                        found = true;
                        break;
                    }
                }
            } else {
                for( int i=0 ; i<candidateLastTypes.length ; ++i ) {
                    if( candidateLastTypes[i].equals( theLastType )) {
                        found = true;
                        break;
                    }
                }
            }
            if( !found ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Serialize into external form.
     *
     * @return external form
     * @throws UnsupportedOperationException always thrown
     */
    public String toExternalForm()
    {
        throw new UnsupportedOperationException( "FIXME" );
    }

    /**
     * The MeshTypeWithProperties against which we compare our first MeshObject.
     */
    protected MeshTypeWithProperties theFirstType;

    /**
     * The MeshTypeWithProperties against which we compare our last MeshObject.
     */
    protected MeshTypeWithProperties theLastType;

    /**
     * Do we allow a subtype instance or only instances of the exact type for the first MeshObject.
     */
    protected boolean theFirstSubtypeAllowed;

    /**
     * Do we allow a subtype instance or only instances of the exact type for the last MeshObject.
     */
    protected boolean theLastSubtypeAllowed;
}
