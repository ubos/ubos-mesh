//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import java.util.HashSet;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectUtils;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.exception.NotSingleMemberException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * <p>Collects functionality common to various types of MeshObjectSets.</p>
  *
  * <p>It has a private variable to hold the current content. It also holds an array of
  * counters that count how many times the corresponding content object has been added
  * to the content. For example, if a subclass of AbstractActiveMeshObjectSet defines its
  * behavior as being the unification of 3 other sets, and each of those 3 sets contains
  * object X, the counter for object X would be 3. This allows us to efficiently determine
  * whether a given event of an underlying set requires us to remove an object from the
  * current content, or not.</p>
  */
public abstract class AbstractMeshObjectSet
        implements
            MeshObjectSet,
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( AbstractMeshObjectSet.class ); // our own, private logger

    /**
      * Constructor to be used by subclasses only.
      *
      * @param factory the MeshObjectSetFactory that created this MeshObjectSet
      */
    protected AbstractMeshObjectSet(
            MeshObjectSetFactory factory )
    {
        theFactory = factory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDebugName(
            String newValue )
    {
        theDebugName = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDebugName()
    {
        return theDebugName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshObjectSetFactory getFactory()
    {
        return theFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshBaseView getMeshBaseView()
    {
        if( isEmpty() ) {
            return null;
        }
        MeshObject first = iterator().next();
        return first.getMeshBaseView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject get(
            int n )
    {
        MeshObject [] meshObjects = getMeshObjects();
        return meshObjects[n];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getSingleMember()
        throws
            NotSingleMemberException
    {
        int size = getSize();
        switch( size ) {
            case 0:
                return null;

            case 1:
                return get( 0 );

            default:
                throw new NotSingleMemberException( "MeshObjectSet has wrong size", size );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier[] asIdentifiers()
    {
        return MeshObjectUtils.meshObjectIdentifiers( getMeshObjects() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract CursorIterator<MeshObject> iterator();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(
            MeshObject testObject )
        throws
            WrongMeshBaseViewException
    {
        // using an Iterator is most effecient for potentially large sets that aren't all loaded at the same time
        boolean checkedMbv = false;
        for( MeshObject current : this ) {
            if( !checkedMbv ) {
                WrongMeshBaseViewException.checkCompatible( current.getMeshBaseView(), testObject.getMeshBaseView() );
                checkedMbv = true;
            }
            if( current.equals( testObject )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsAll(
            MeshObjectSet subset )
        throws
            WrongMeshBaseViewException
    {
        WrongMeshBaseViewException.checkCompatible( getMeshBaseView(), subset.getMeshBaseView() );

        for( MeshObject current2 : subset ) {
            boolean found = false;

            for( MeshObject current1 : this ) {
                if( current2.equals( current1 )) {
                    found = true;
                    break;
                }
            }
            if( !found ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasSameContent(
            MeshObjectSet other )
    {
        if( getSize() != other.getSize() ) {
            return false;
        }
        // given that's the same size, if we can find each object, we are in business
        for( MeshObject current : this ) {
            if( !other.contains( current )) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(
            MeshObjectIdentifier identifier )
    {
        // using an Iterator is most effecient for potentially large sets that aren't all loaded at the same time
        for( MeshObject current : this ) {

            if( current.getIdentifier().equals( identifier )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject find(
            MeshObjectSelector selector )
    {
        // using an Iterator is most effecient for potentially large sets that aren't all loaded at the same time
        for( MeshObject current : this ) {

            if( selector.accepts( current )) {
                return current;
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet subset(
            MeshObjectSelector selector )
    {
        return theFactory.createImmutableMeshObjectSet( this, selector );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedMeshObjectSet ordered(
            MeshObjectSorter sorter )
    {
        return theFactory.createOrderedImmutableMeshObjectSet( this, sorter );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        return size() == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract int size();

    /**
     * {@inheritDoc}
     */
    @Override
    public final int getSize()
    {
        return size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet intersection(
            MeshObjectSet otherSet )
    {
        MeshObjectSet ret = theFactory.createImmutableMeshObjectSetIntersection( this, otherSet );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet union(
            MeshObjectSet otherSet )
    {
        MeshObjectSet ret = theFactory.createImmutableMeshObjectSetUnification( this, otherSet );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet minus(
            MeshObjectSet otherSet )
    {
        MeshObjectSet ret = theFactory.createImmutableMeshObjectSetMinus( this, otherSet );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            TraverseSpecification theTraverseSpecification )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "traverse", theTraverseSpecification );
        }

        MeshObjectSet us = theFactory.createImmutableMeshObjectSet( getMeshObjects() );
        return us.traverse( theTraverseSpecification );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverseToNeighbors()
    {
        MeshObject [] meshObjects = getMeshObjects();

        MeshObjectSet [] neighbors = new MeshObjectSet[ meshObjects.length ];

        for( int i=0 ; i<meshObjects.length ; ++i ) {
            neighbors[i] = meshObjects[i].traverseToNeighbors();
        }

        return theFactory.createImmutableMeshObjectSetUnification( neighbors );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<MeshObject> asSet()
    {
        HashSet<MeshObject> ret = new HashSet<>();
        for( MeshObject current : getMeshObjects() ) {
            ret.add( current );
        }
        return ret;
    }

    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshObject"
                },
                new Object[] {
                    getMeshObjects()
                } );
    }

    /**
     * The traceMethodCallEntry name, if any.
     */
    protected String theDebugName;

    /**
     * The MeshObjectSetFactory by which this MeshObjectSet was created.
     */
    protected MeshObjectSetFactory theFactory;
}
