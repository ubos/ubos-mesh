//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.json;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.mesh.externalized.HasTypesPropertiesAttributes;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.model.primitives.externalized.json.AbstractPropertyValueJsonEncoder;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;

/**
 * Utility methods to encode/decode an ExternalizedMeshObject to/from JSON.
 */
public abstract class AbstractMeshObjectJsonEncoder
        extends
            AbstractPropertyValueJsonEncoder
        implements
            MeshObjectJsonTags
{
    /**
     * Serialize a MeshObject.
     *
     * @param obj the input MeshObject
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    public void writeMeshObject(
            MeshObject                     obj,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException
    {
        JsonWriter jw = new JsonWriter( w );
        jw.setSerializeNulls( true );

        writeExternalizedMeshObject( obj.asExternalized(), null, idSerializer, jw );

        jw.flush();
    }

    /**
     * Serialize a ExternalizedMeshObject to a JsonWriter.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param objHistory if non-null, also write these ExternalizedMeshObjects that represent the MeshObject's history
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param out the JsonWriter to to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    public void writeExternalizedMeshObject(
            ExternalizedMeshObject         obj,
            List<ExternalizedMeshObject>   objHistory,
            MeshObjectIdentifierSerializer idSerializer,
            JsonWriter                     out )
        throws
            EncodingException,
            IOException
    {
        writeExternalizedMeshObject( obj, true, objHistory, idSerializer, out );
    }

    /**
     * Serialize a ExternalizedMeshObject to a JsonWriter.
     *
     * @param obj the ExternalizedMeshObject to encode. This may be null if we are writing a MeshObjectHistory
     *            and the HEAD version does not exist because the MeshObject was deleted
     * @param includeIdentifier if false, do not include the MeshObjectIdentifier
     * @param objHistory if non-null, also write these ExternalizedMeshObjects that represent the MeshObject's history
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param out the JsonWriter to to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    public void writeExternalizedMeshObject(
            ExternalizedMeshObject         obj,
            boolean                        includeIdentifier,
            List<ExternalizedMeshObject>   objHistory,
            MeshObjectIdentifierSerializer idSerializer,
            JsonWriter                     out )
        throws
            EncodingException,
            IOException
    {
        out.beginObject();

        if( obj != null ) {
            if( includeIdentifier ) {
                out.name( IDENTIFIER_TAG ).value( idSerializer.toExternalForm( obj.getIdentifier()));
            }
            out.name( TIME_CREATED_TAG ).value( obj.getTimeCreated() );
            out.name( TIME_UPDATED_TAG ).value( obj.getTimeUpdated() );

            if( obj.getIsDead() ) {
                out.name( STATE_TAG ).value( STATE_VALUE_DEAD );
            }

            writeExternalizedMeshObjectAttributes(  obj, out );
            writeExternalizedMeshObjectTypes(       obj, out );
            writeExternalizedMeshObjectProperties(  obj, out );
            writeExternalizedMeshObjectNeighbors(   obj, idSerializer, out );
        }

        if( objHistory != null ) {
            out.name( MESH_OBJECT_HISTORY_TAG );
            out.beginArray();
            for( ExternalizedMeshObject histObj : objHistory ) {
                writeExternalizedMeshObject( histObj, false, null, idSerializer, out );
            }
            out.endArray();
        }

        out.endObject();
    }

    /**
     * Serialize the Attributes section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param out the JsonWriter to to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    protected void writeExternalizedMeshObjectAttributes(
            ExternalizedMeshObject obj,
            JsonWriter             out )
        throws
            EncodingException,
            IOException
    {
        String [] attributeNames  = obj.getAttributeNames();
        if( attributeNames == null || attributeNames.length == 0 ) {
            return;
        }
        Serializable [] attributeValues = obj.getAttributeValues();

        out.name( STRING_ATTRIBUTES_TAG );
        out.beginObject();

        for( int i=0 ; i<attributeNames.length ; ++i ) {
            if( attributeValues[i] instanceof String ) {
                out.name( attributeNames[i] );
                out.value( (String) attributeValues[i] );
            }
        }
        out.endObject();

        out.name( ATTRIBUTES_TAG );
        out.beginObject();

        for( int i=0 ; i<attributeNames.length ; ++i ) {
            if( !( attributeValues[i] instanceof String )) {
                out.name( attributeNames[i] );
                out.value( serializableAsBase64String( attributeValues[i] ));
            }
        }
        out.endObject();
    }

    /**
     * Serialize the EntityTypes section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param out the JsonWriter to to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    protected void writeExternalizedMeshObjectTypes(
            ExternalizedMeshObject obj,
            JsonWriter             out )
        throws
            EncodingException,
            IOException
    {
        MeshTypeIdentifier [] allTypes = obj.getTypeIdentifiers();
        if( allTypes == null || allTypes.length == 0 ) {
            return;
        }

        out.name( TYPES_TAG );
        out.beginArray();
        for( int i=0 ; i<allTypes.length ; ++i ) {
            out.value( theTypeIdSerializer.toExternalForm( allTypes[i] ));
        }
        out.endArray();
    }

    /**
     * Serialize the Properties section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param out the JsonWriter to to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    protected void writeExternalizedMeshObjectProperties(
            ExternalizedMeshObject obj,
            JsonWriter             out )
        throws
            EncodingException,
            IOException
    {
        MeshTypeIdentifier [] allPropertyTypes  = obj.getPropertyTypes();
        if( allPropertyTypes == null || allPropertyTypes.length == 0 ) {
            return;
        }
        PropertyValue [] allPropertyValues = obj.getPropertyValues();

        out.name( PROPERTIES_TAG );
        out.beginObject();

        if( allPropertyTypes != null ) {
            for( int i=0 ; i<allPropertyTypes.length ; ++i ) {
                out.name( theTypeIdSerializer.toExternalForm( allPropertyTypes[i] ));
                writePropertyValue( allPropertyValues[i], out );
            }
        }
        out.endObject();
    }

    /**
     * Serialize the neighbors section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param out the JsonWriter to to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    protected void writeExternalizedMeshObjectNeighbors(
            ExternalizedMeshObject         obj,
            MeshObjectIdentifierSerializer idSerializer,
            JsonWriter                     out )
        throws
            EncodingException,
            IOException
    {
        HasTypesPropertiesAttributes [] thisEnds = obj.getThisEnds();
        if( thisEnds == null || thisEnds.length == 0 ) {
            return;
        }

        out.name( NEIGHBORS_TAG );
        out.beginObject();

        for( int i=0 ; i<thisEnds.length ; ++i ) {
            MeshObjectIdentifier  currentNeighbor  = thisEnds[i].getIdentifier();
            MeshTypeIdentifier [] currentRoleTypes = thisEnds[i].getTypeIdentifiers();

            MeshTypeIdentifier [] currentRolePropertyTypes   = thisEnds[i].getPropertyTypes();
            PropertyValue      [] currentRolePropertyValues  = thisEnds[i].getPropertyValues();
            String             [] currentRoleAttributeNames  = thisEnds[i].getAttributeNames();
            Serializable       [] currentRoleAttributeValues = thisEnds[i].getAttributeValues();

            out.name( idSerializer.toExternalForm( currentNeighbor ));
            out.beginObject();

            out.name( TIME_CREATED_TAG ).value( thisEnds[i].getTimeCreated() );

            if( currentRoleTypes != null && currentRoleTypes.length > 0 ) {
                out.name( TYPES_TAG );
                out.beginArray();
                for( int j=0 ; j<currentRoleTypes.length ; ++j ) {
                    out.value( theTypeIdSerializer.toExternalForm( currentRoleTypes[j] ));
                }
                out.endArray();
            }

            if( currentRolePropertyTypes != null && currentRolePropertyTypes.length > 0 ) {
                out.name( PROPERTIES_TAG );
                out.beginObject();
                for( int j=0 ; j<currentRolePropertyTypes.length ; ++j ) {
                    out.name( theTypeIdSerializer.toExternalForm( currentRolePropertyTypes[j] ));
                    writePropertyValue( currentRolePropertyValues[j], out );
                }
                out.endObject();
            }

            if( currentRoleAttributeNames != null && currentRoleAttributeNames.length > 0 ) {

                out.name( STRING_ATTRIBUTES_TAG );
                out.beginObject();
                for( int j=0 ; j<currentRoleAttributeNames.length ; ++j ) {
                    if( currentRoleAttributeValues[j] instanceof String ) {
                        out.name( currentRoleAttributeNames[j] );
                        out.value( (String) currentRoleAttributeValues[j] );
                    }
                }
                out.endObject();

                out.name( ATTRIBUTES_TAG );
                out.beginObject();
                for( int j=0 ; j<currentRoleAttributeNames.length ; ++j ) {
                    if( !( currentRoleAttributeValues[j] instanceof String )) {
                        out.name( currentRoleAttributeNames[j] );
                        out.value( serializableAsBase64String( currentRoleAttributeValues[j] ));
                    }
                }
                out.endObject();
            }
            out.endObject();
        }
        out.endObject();
    }

    /**
     * Knows how to serialize MeshTypeIdentifiers.
     */
    protected static final MeshTypeIdentifierSerializer theTypeIdSerializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;
}
