//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.text.ParseException;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.util.exception.InvalidCharacterParseException;

/**
 * Knows how to serialize and deserialize MeshObjectIdentifiers in the bracket notation.
 */
public class BracketMeshObjectIdentifierBothSerializer
    implements
        MeshObjectIdentifierBothSerializer
{
    /**
     * Factory method.
     *
     * @param nsMap find namespaces here
     * @param idFact use this factory for MeshObjectIdentifiers
     * @return the created object
     */
    public static BracketMeshObjectIdentifierBothSerializer create(
            PrimaryMeshObjectIdentifierNamespaceMap nsMap,
            MeshObjectIdentifierFactory             idFact )
    {
        return new BracketMeshObjectIdentifierBothSerializer( nsMap, idFact );
    }

    /**
     * Constructor.
     *
     * @param nsMap find namespaces here
     * @param idFact use this factory for MeshObjectIdentifiers
     */
    protected BracketMeshObjectIdentifierBothSerializer(
            PrimaryMeshObjectIdentifierNamespaceMap nsMap,
            MeshObjectIdentifierFactory             idFact )
    {
        theNsMap  = nsMap;
        theIdFact = idFact;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toExternalForm(
            MeshObjectIdentifier identifier )
    {
        String preferred = identifier.getNamespace().getPreferredExternalName();
        if( preferred == null ) {
            return identifier.getLocalId();
        } else {
            return NAMESPACE_OPEN_CHAR + preferred + NAMESPACE_CLOSE_CHAR + identifier.getLocalId();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            String externalForm )
        throws
            ParseException
    {
        return meshObjectIdentifierFromExternalForm( null, externalForm );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            MeshObjectIdentifier contextIdentifier,
            String               externalForm )
        throws
            ParseException
    {
        String [] parsed = checkParseRawId( externalForm );

        MeshObjectIdentifier ret;

        if( parsed[0] == null ) {
            if( contextIdentifier == null ) {
                ret = theIdFact.createMeshObjectIdentifierIn( theNsMap.getPrimary(), parsed[1] );

            } else {
                ret = theIdFact.createMeshObjectIdentifierIn( contextIdentifier.getNamespace(), parsed[1] );
            }

        } else {
            MeshObjectIdentifierNamespace ns = theNsMap.findByExternalName( parsed[0] );
            if( ns == null ) {
                throw new MeshObjectIdentifierNamespaceNotFoundParseException( externalForm );
            }

            ret = theIdFact.createMeshObjectIdentifierIn( ns, parsed[1] );
        }
        return ret;
    }

    /**
     * Check whether the proposed String for a MeshObjectIdentifier is valid,
     * and parse it into the Namespace component and the localId component.
     *
     * @param raw the String
     * @return pair of name of the namespace (or null) and localId
     * @throws ParseException thrown if the String is not valid
     */
    String [] checkParseRawId(
            String raw )
        throws
            ParseException
    {
        if( raw == null ) {
            throw new NullPointerException();
        }
        if( raw.length() == 0 ) {
            return new String[] { null, raw };
        }

        raw = raw.trim();

        String namespace;
        String localId;
        if( raw.startsWith( NAMESPACE_OPEN_CHAR )) {
            if( raw.indexOf( NAMESPACE_OPEN_CHAR, NAMESPACE_OPEN_CHAR.length() ) > 0 ) {
                throw new InvalidCharacterParseException( raw, "Identifier must not contain a second namespace open bracket.", 0, NAMESPACE_OPEN_CHAR );
            }
            int close = raw.indexOf( NAMESPACE_CLOSE_CHAR, NAMESPACE_OPEN_CHAR.length() );
            if( close < 0 ) {
                throw new InvalidCharacterParseException( raw, "Identifier does not contain a namespace close bracket.", 0, NAMESPACE_CLOSE_CHAR );
            }
            if( raw.indexOf( NAMESPACE_CLOSE_CHAR, close+1 ) > 0 ) {
                throw new InvalidCharacterParseException( raw, "Identifier must not contain a second namespace close bracket.", 0, NAMESPACE_CLOSE_CHAR );
            }
            namespace = raw.substring( NAMESPACE_OPEN_CHAR.length(), close );
            localId   = raw.substring( close + 1 );

        } else {
            if( raw.indexOf( NAMESPACE_OPEN_CHAR, NAMESPACE_OPEN_CHAR.length() ) > 0 ) {
                throw new InvalidCharacterParseException( raw, "Identifier namespace open bracket must be at the beginning.", 0, NAMESPACE_OPEN_CHAR );
            }
            if( raw.indexOf( NAMESPACE_CLOSE_CHAR ) > 0 ) {
                throw new InvalidCharacterParseException( raw, "Identifier must not contain a namespace close bracket without an open bracket first.", 0, NAMESPACE_CLOSE_CHAR );
            }
            namespace = null;
            localId   = raw;
        }

        return new String[] { namespace, localId };
    }

    /**
     * Find namespaces here.
     */
    protected final PrimaryMeshObjectIdentifierNamespaceMap theNsMap;

    /**
     * Use this factory.
     */
    protected final MeshObjectIdentifierFactory theIdFact;

    /**
     * The character that opens the namespace section
     */
    public static final String NAMESPACE_OPEN_CHAR = "(";

    /**
     * The character that closes the namespace section
     */
    public static final String NAMESPACE_CLOSE_CHAR = ")";
}
