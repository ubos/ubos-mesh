//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.security;

import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.MeshBaseView;

/**
 * Thrown if an attempt is made to delete the home MeshObject of a MeshBase.
 */
public class MustNotDeleteHomeObjectException
        extends
            NotPermittedException
{
    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView on which the illegal operation was attempted
     */
    public MustNotDeleteHomeObjectException(
            MeshBaseView mbv )
    {
        super( mbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return null;
    }
}
