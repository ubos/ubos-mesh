//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

//
// Copyright (C) Johannes Ernst. All rights reserved. License: see package.
//

package net.ubos.mesh.text;

import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies MeshObjects, well, their identifiers.
 */
public class MeshObjectStringifier
        extends
            AbstractStringifier<MeshObject>
{
    /**
     * Factory method.
     *
     * @return the created MeshObjectStringifier
     */
    public static MeshObjectStringifier create()
    {
        return new MeshObjectStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected MeshObjectStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            MeshObject            arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = ExternalNameHashMeshObjectIdentifierSerializer.SINGLETON.toExternalForm( arg.getIdentifier());

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (MeshObject) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }
}
