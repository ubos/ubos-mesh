//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

/**
 * Marker interface for TraversePathSets that are immutable.
 */
public interface ImmutableTraversePathSet
        extends
            TraversePathSet
{
    // nothing
}