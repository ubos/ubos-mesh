//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;
import net.ubos.util.OrderedSetWithValue;

/**
 * An ordered MeshObjectSet that carries a value for each MeshObject in
 * the set, ordered by the ordering of this value.
 * 
 * @param <V> the type of value
 */
public interface OrderedMeshObjectSetWithValue<V>
    extends
        OrderedMeshObjectSet,
        OrderedSetWithValue<MeshObject,V>
{
}
