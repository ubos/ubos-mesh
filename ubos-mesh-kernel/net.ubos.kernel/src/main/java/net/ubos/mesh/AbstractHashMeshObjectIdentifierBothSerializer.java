//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.text.ParseException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.util.logging.Log;

/**
 * Factors out common functionality for the hash notation of MeshObjectIdentifiers.
 */
public abstract class AbstractHashMeshObjectIdentifierBothSerializer
    extends
        AbstractHashMeshObjectIdentifierSerializer
    implements
        MeshObjectIdentifierBothSerializer
{
    private static final Log log = Log.getLogInstance( AbstractHashMeshObjectIdentifierBothSerializer.class );

    /**
     * Constructor.
     *
     * @param idFact use this factory for MeshObjectIdentifiers
     */
    protected AbstractHashMeshObjectIdentifierBothSerializer(
            MeshObjectIdentifierFactory idFact )
    {
        theIdFact = idFact;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            String externalForm )
        throws
            ParseException
    {
        return meshObjectIdentifierFromExternalForm( null, externalForm );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            MeshObjectIdentifier contextIdentifier,
            String               externalForm )
        throws
            ParseException
    {
        StringBuilder part1 = new StringBuilder();
        StringBuilder part2 = null;

        StringBuilder appendHere = part1;

        boolean escaping = false;

        for( char c : externalForm.toCharArray() ) {
            if( escaping ) {
                appendHere.append( c );
                escaping = false;

            } else {
                switch( c ) {
                    case ESCAPE:
                        escaping = true;
                        break;

                    case SEPARATOR:
                        if( part2 == null ) {
                            part2 = new StringBuilder();
                            appendHere = part2;
                        } else {
                            log.error( "Found second separator in:", externalForm );
                        }
                        break;

                    default:
                        appendHere.append( c );
                        break;
                }
            }
        }

        MeshObjectIdentifier ret;

        if( part2 == null ) {
            ret = createFor( contextIdentifier, null, part1.toString() );

        } else {
            ret = createFor( contextIdentifier, part1.toString(), part2.toString() );
        }
        return ret;
    }

    /**
     * Let subclasses decide how they want to resolve the namespace name and
     * localId to a MeshObjectIdentifier.
     *
     * @param contextIdentifier if the externalForm is given as a relative expression, it is relative to this Identifier
     * @param nsName the name of the namespace, or null
     * @param localId the local id of the identifier
     * @return the created identifier
     */
    protected abstract MeshObjectIdentifier createFor(
            MeshObjectIdentifier contextIdentifier,
            String               nsName,
            String               localId );

    /**
     * Use this factory.
     */
    protected final MeshObjectIdentifierFactory theIdFact;
}
