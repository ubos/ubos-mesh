//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.io.Serializable;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * A pair of a MeshObject and a name of the Attribute,
  * representing an Attribute of this MeshObject.
  */
public final class Attribute
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance(Attribute.class); // our own, private logger

    /**
      * Constructor. This constructor does not (and should not, due to possible setting later)
      * check whether this MeshObject actually carries the specified Attribute.
      *
      * @param mo the MeshObject
      * @param an the name of the Attribute
      */
    public Attribute(
            MeshObject mo,
            String     an )
    {
        theMeshObject    = mo;
        theAttributeName = an;
    }

    /**
     * Obtain the MeshObject.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
      * Obtain the name of the Attribute.
      *
      * @return the name of the Attribute
      */
    public String getAttributeName()
    {
        return theAttributeName;
    }

    /**
      * Obtain the value of the Attribute for this MeshObject.
      *
      * @return the value of the Attribute for this MeshObject
      * @throws NotPermittedException thrown if the caller did not have sufficient access rights
      * @see #setValue
      */
    public Serializable getValue()
       throws
            NotPermittedException
    {
        return theMeshObject.getAttributeValue( theAttributeName );
    }

    /**
     * Set the value of the Attribute for this MeshObject.
     *
     * @param newValue the new Attribute value on this MeshObject
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @throws TransactionException thrown if this method is not invoked between proper
     *         Transaction boundaries
     * @see #getValue
     */
    public void setValue(
            Serializable newValue )
        throws
            NotPermittedException,
            TransactionException
    {
        theMeshObject.setAttributeValue( theAttributeName, newValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof Attribute ) {
            Attribute realOther = (Attribute) other;

            return    theMeshObject.equals( realOther.theMeshObject )
                   && theAttributeName.equals( realOther.theAttributeName );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = theMeshObject.hashCode() ^ theAttributeName.hashCode();
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject",
                    "theAttributeName"
                },
                new Object[] {
                    theMeshObject,
                    theAttributeName
                });
    }

    /**
     * The MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The name of the Attribute.
     */
    protected final String theAttributeName;
}
