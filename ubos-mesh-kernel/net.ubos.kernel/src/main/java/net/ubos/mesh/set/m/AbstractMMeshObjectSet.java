//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.AbstractMeshObjectSet;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.MeshObjectSorter;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.CursorIterable;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Abstract superclass for those MeshObjectSets that keep their content in memory.
 */
public abstract class AbstractMMeshObjectSet
        extends
            AbstractMeshObjectSet
        implements
            CursorIterable<MeshObject>
{
    /**
      * Constructor to be used by subclasses only.
      *
      * @param factory the MeshObjectSetFactory that created this MeshObjectSet
      */
    protected AbstractMMeshObjectSet(
            MeshObjectSetFactory factory )
    {
        super( factory );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshObject [] getMeshObjects()
    {
        return theContent;
    }

    /**
     * Set the initial content of the set, for subclasses only.
     *
     * @param initialContent the initial content of the set
     * @throws IllegalStateException thrown if this MeshObjectSet already has an initial content
     * @throws IllegalArgumentException thrown if the initial content is null, or the array lenghts did not match
     * @throws WrongMeshBaseViewException thrown if the provided MeshObjects dot not all reside in the same MeshBase
     */
    protected void setInitialContent(
            MeshObject [] initialContent )
        throws
            IllegalStateException,
            IllegalArgumentException,
            WrongMeshBaseViewException
    {
        if( theContent != null ) {
            throw new IllegalStateException( "initialized already" );
        }
        if( initialContent == null ) {
            throw new IllegalArgumentException();
        }

        MeshBaseView mbv = null;
        for( int i=0; i<initialContent.length; ++i ) {
            if( mbv == null ) {
                mbv = initialContent[i].getMeshBaseView();
                WrongMeshBaseViewException.checkCompatible( theFactory.getMeshBase(), mbv );

            } else {
                WrongMeshBaseViewException.checkCompatible( mbv, initialContent[i].getMeshBaseView() );
            }
        }

        theContent = initialContent;
    }

    /**
     * Set the initial content of the set, for subclasses only.
     *
     * @param initialContent the initial content of the set, with the counters to be calculated
     * @throws IllegalStateException thrown if this MeshObjectSet already has an initial content
     * @throws IllegalArgumentException thrown if the initial content is null
     * @throws WrongMeshBaseViewException thrown if the provided MeshObjects dot not all reside in the same MeshBase
     */
    protected void setInitialContent(
            MeshObject [][] initialContent )
        throws
            IllegalStateException,
            IllegalArgumentException,
            WrongMeshBaseViewException
    {
        if( theContent != null ) {
            throw new IllegalStateException( "initialized already" );
        }
        if( initialContent == null ) {
            throw new IllegalArgumentException();
        }

        // potentially over-allocate
        int count = 0;
        for( int i=0 ; i<initialContent.length ; ++i ) {
            count += initialContent[i].length;
        }

        MeshBaseView mbv  = null;
        theContent = new MeshObject[ count ];
        count             = 0;

        for( int i=0 ; i<initialContent.length ; ++i ) {
            for( int j=0 ; j<initialContent[i].length ; ++j ) {
                MeshObject candidate = initialContent[i][j];

                if( mbv == null ) {
                    mbv = candidate.getMeshBaseView();
                    WrongMeshBaseViewException.checkCompatible( theFactory.getMeshBase(), mbv );

                } else {
                    WrongMeshBaseViewException.checkCompatible( mbv, candidate.getMeshBaseView() );
                }

                boolean found = false;
                for( int k=0 ; k<count ; ++k ) {
                    if( theContent[k] == candidate ) {
                        found = true;
                        break;
                    }
                }
                if( !found ) {
                    theContent[count++] = candidate;
                }
            }
        }

        // now shrink if over-allocated
        if( count < theContent.length ) {
            MeshObject [] oldContent  = theContent;

            theContent = new MeshObject[ count ];

            for( int i=0 ; i<count ; ++i ) {
                theContent[i] = oldContent[i];
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> iterator()
    {
        return ArrayCursorIterator.<MeshObject>create(theContent );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        return theContent.length;
    }

    /**
     * This is invoked by our subclasses to potentially reorder the set according to
     * the specified MeshObjectSorter.
     *
     * @param theSorter the MeshObjectSorter according to which the current content is sorted
     */
    protected final void potentiallyReorder(
            MeshObjectSorter theSorter )
    {
        theContent = theSorter.getOrderedInNew(theContent );
    }

    /**
     * The current content of this set.
     */
    protected MeshObject [] theContent;
}
