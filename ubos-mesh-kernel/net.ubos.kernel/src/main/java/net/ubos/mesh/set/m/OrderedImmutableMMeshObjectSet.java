//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSelector;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.OrderedImmutableMeshObjectSet;
import net.ubos.mesh.set.OrderedMeshObjectSet;

/**
 * This MeshObjectSet has the same content as a passed-in MeshObjectSet,
 * but in an order specified by a passed-in sorting algorithm. This set is
 * immutable. It is kept in memory.
 */
public class OrderedImmutableMMeshObjectSet
    extends
        ImmutableMMeshObjectSet
    implements
        OrderedImmutableMeshObjectSet
{
    /**
     * Private constructor, use factory method. Note that the content must
     * have been limited to the maximum allowed number by the caller of the constructor.
     *
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     * @param orderedContent the content of this set in order
     */
    OrderedImmutableMMeshObjectSet(
            MeshObjectSetFactory factory,
            MeshObject []        orderedContent )
    {
        super( factory, orderedContent );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getMeshObject(
            int index )
    {
        return theContent[ index ];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getFirstMeshObject()
    {
        if( theContent != null && theContent.length > 0 ) {
            return theContent[0];
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getLastMeshObject()
    {
        if( theContent != null && theContent.length > 0 ) {
            return theContent[theContent.length-1];
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int findIndexOf(
            MeshObject candidate )
    {
        for( int i=0 ; i<theContent.length ; ++i ) {
            if( candidate == theContent[i] ) {
                return i;
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedMeshObjectSet subset(
            MeshObjectSelector selector )
    {
        return theFactory.createOrderedImmutableMeshObjectSet( getMeshObjects(), selector );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderedImmutableMMeshObjectSet inverse()
    {
        MeshObject [] newContent = new MeshObject[ theContent.length ];
        for( int i=0 ; i<theContent.length ; ++i ) {
            newContent[ theContent.length - i - 1 ] = theContent[i];
        }
        return new OrderedImmutableMMeshObjectSet( theFactory, newContent );
    }
}
