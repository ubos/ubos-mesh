//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.json;

import net.ubos.mesh.externalized.MeshObjectEncoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class MeshObjectJsonEncoder
    extends
        AbstractMeshObjectJsonEncoder
    implements
        MeshObjectEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static MeshObjectJsonEncoder create()
    {
        return new MeshObjectJsonEncoder();
    }

    /**
     * Constructor, use factory method.
     */
    protected MeshObjectJsonEncoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_OBJECT_JSON_ENCODING_ID;
    }
}
