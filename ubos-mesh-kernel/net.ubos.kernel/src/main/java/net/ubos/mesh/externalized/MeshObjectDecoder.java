//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import java.io.IOException;
import java.io.Reader;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;

/**
 * This interface is supported by classes that know how to deserialize
 * ExternalizedMeshObject.
 */
public interface MeshObjectDecoder
    extends
        Decoder
{
    /**
     * Deserialize an ExternalizedMeshObject from a stream.
     *
     * @param r read from here
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return return the just-instantiated ExternalizedMeshObject
     * @throws DecodingException thrown if a problem occurred during decoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    public ExternalizedMeshObject decodeExternalizedMeshObject(
            Reader                           r,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException;
}
