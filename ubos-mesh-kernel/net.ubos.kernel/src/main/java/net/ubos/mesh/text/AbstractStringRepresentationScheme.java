//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.text;

import java.nio.charset.StandardCharsets;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.PropertyValueParsingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.util.StringHelper;
import net.ubos.util.text.StringifierParameters;

/**
 * Collects functionality common to StringRepresentationScheme implementations
 */
public abstract class AbstractStringRepresentationScheme
    implements
        StringRepresentationScheme
{
    /**
     * Private constructor, for subclasses only.
     *
     * @param prefix the prefix in the path for where we look for templates
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     * @param delegate the StringRepresentationScheme to delegate to if no entry was found
     */
    protected AbstractStringRepresentationScheme(
            String                         prefix,
            MeshObjectIdentifierSerializer idSerializer,
            MeshTypeIdentifierSerializer   typeIdSerializer,
            StringRepresentationScheme     delegate )
    {
        thePathPrefix       = "StringRepresentation/" + prefix + "/";
        theIdSerializer     = idSerializer;
        theTypeIdSerializer = typeIdSerializer;
        theDelegate         = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String formatMeshObject(
            MeshObject            obj,
            StringifierParameters pars )
    {
        String text = obj.getUserVisibleString();
        text = StringHelper.Capitalization.FIRST.capitalize( text );
        return escapeToContentFormat( text );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue parsePropertyValue(
            String                raw,
            PropertyType          propertyType,
            StringifierParameters pars )
        throws
            PropertyValueParsingException
    {
        return parsePropertyValue( raw, null, propertyType, pars );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue parsePropertyValue(
            String                raw,
            String                mime,
            PropertyType          propertyType,
            StringifierParameters pars )
        throws
            PropertyValueParsingException
    {
        PropertyValue ret = propertyType.getDataType().parse( raw, mime, propertyType );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String formatDataType(
            DataType              dataType,
            StringifierParameters pars )
    {
        // FIXME
        return dataType.toString();
    }

    /**
     * Escape a String to the content format, such as text or HTML, if needed.
     * This makes it easy to have the same code for plain text and HTML.
     *
     * @param raw the String to be escaped
     * @return return the escaped String
     */
    protected String escapeToContentFormat(
            String raw )
    {
        return raw;
    }

    /**
     * Unescape a String, if needed. This makes it easy to have the same code for plain text and HTML.
     *
     * @param raw the escaped String
     * @return the String
     */
    protected String unescapeFromContentFormat(
            String raw )
    {
        return raw;
    }

    /**
     * Escape a String to being usable as part of a URL. Strings may be unicode.
     *
     * @param s the String
     * @return the URL fragment
     */
    protected String stringToUrl(
            String s )
    {
        StringBuilder buf = new StringBuilder();

        for( byte b : s.getBytes( StandardCharsets.UTF_8 )) {
            if(    ( b >= 0x30 && b <= 0x39 ) // 0-9
                || ( b >= 0x41 && b <= 0x5a ) // A-Z
                || ( b >= 0x61 && b <= 0x7a ) // a-z
                || b == 0x2c   // comma
                || b == 0x2d   // hypen
                || b == 0x2e   // period
                || b == 0x2f   // slash
                || b == 0x3b   // semicolon
                || b == 0x5f ) // underscore
            {
                buf.append( (char) b );

            } else {
                buf.append( '%' );
                buf.append( hex(( b & 0xf0 ) >> 4 ));
                buf.append( hex( b & 0x0f ));
            }
        }
        return buf.toString();
    }

    protected char hex(
            int nibble )
    {
        char ret;
        if( nibble <= 9 ) {
            ret = (char)( '0' + nibble );
        } else {
            ret = (char)( 'a' + nibble - 10 );
        }
        return ret;
    }

    /**
     * The full path prefix for where we look for templates.
     */
    protected final String thePathPrefix;

    /**
     * Knows how to serialize MeshObjectIdentifiers according to this scheme.
     */
    protected final MeshObjectIdentifierSerializer theIdSerializer;

    /**
     * Knows how to serialize MeshTypeIdentifiers according to this scheme.
     */
    protected final MeshTypeIdentifierSerializer theTypeIdSerializer;

    /**
     * The StringRepresentationScheme to delegate to if no entry was found.
     */
    protected final StringRepresentationScheme theDelegate;
}
