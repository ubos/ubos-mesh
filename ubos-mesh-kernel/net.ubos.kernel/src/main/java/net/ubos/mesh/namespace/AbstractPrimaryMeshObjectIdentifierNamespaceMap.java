//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace;

import java.util.Map;
import java.util.WeakHashMap;
import net.ubos.util.Pair;
import net.ubos.util.StringHelper;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.smartmap.SmartMap;

/**
 * Collects functionality common to PrimaryMeshObjectIdentifierNamespaceMap implementations.
 */
public abstract class AbstractPrimaryMeshObjectIdentifierNamespaceMap
    implements
        PrimaryMeshObjectIdentifierNamespaceMap
{
    /**
     * Constructor.
     *
     * @param localNameMap resolves local names to namespaces
     * @param externalNameMap resolves external names to local names
     */
    protected AbstractPrimaryMeshObjectIdentifierNamespaceMap(
            SmartMap<String,DefaultMeshObjectIdentifierNamespace> localNameMap,
            SmartMap<String,String>                               externalNameMap )
    {
        theLocalNameMap        = localNameMap;
        theExternalNameMap     = externalNameMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifierNamespace getPrimary()
    {
        // allocated as needed
        DefaultMeshObjectIdentifierNamespace ret = theLocalNameMap.get( PRIMARY_KEY );
        if( ret == null ) {
            ret = new DefaultMeshObjectIdentifierNamespace( this );

            theLocalNameMap.put(        PRIMARY_KEY, ret );
            theInverseLocalNameMap.put( ret,         PRIMARY_KEY );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized MeshObjectIdentifierNamespace findByLocalName(
            String localName )
    {
        return theLocalNameMap.get( localName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalNameFor(
            MeshObjectIdentifierNamespace ns )
    {
        return theInverseLocalNameMap.get( (DefaultMeshObjectIdentifierNamespace) ns );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized DefaultMeshObjectIdentifierNamespace findByExternalName(
            String externalName )
    {
        String localName = theExternalNameMap.get( externalName );
        if( localName == null ) {
            return null;
        }
        return theLocalNameMap.get( localName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized DefaultMeshObjectIdentifierNamespace obtainByExternalName(
            String externalName )
    {
        DefaultMeshObjectIdentifierNamespace ret = null;

        String localName = theExternalNameMap.get( externalName );
        if( localName != null ) {
            ret = theLocalNameMap.get( localName );
        }

        if( ret == null ) {
            Pair<String,DefaultMeshObjectIdentifierNamespace> created = createAllocateNamespace();
            localName = created.getName();
            ret       = created.getValue();

            ret.thePreferredExternalName = externalName;
            ret.theExternalNames.add( externalName );

            theLocalNameMap.put(        localName,    ret );
            theInverseLocalNameMap.put( ret,          localName );
            theExternalNameMap.put(     externalName, localName );
        }
        return ret;
    }

    /**
     * Invoked by a member namespace, to perform checking and doing.
     *
     * @param namespace the namespace whose preferred name shall be changed
     * @param externalName the external name to be set as preferred
     */
    protected void setPreferredExternalName(
            DefaultMeshObjectIdentifierNamespace namespace,
            String                                externalName )
    {
        if( !namespace.theExternalNames.contains( externalName ) ) {
            throw new IllegalArgumentException( "Not a known external name: " + externalName );
        }
        synchronized( this ) {
            String localName = theInverseLocalNameMap.get( namespace );
            if( localName == null ) {
                throw new IllegalArgumentException( "Unknown namespace: " + namespace );
            }

            if( StringHelper.compareTo( namespace.thePreferredExternalName, externalName ) != 0 ) {
                namespace.thePreferredExternalName = externalName;

                // update on disk -- no need to update other tables
                theLocalNameMap.put( localName, namespace );
            }
        }
    }

    /**
     * Invoked by a member namespace, to perform checking and doing.
     *
     * @param namespace the namespace to which the external name shall be added
     * @param externalName the external name to be added
     */
    protected void addExternalName(
            DefaultMeshObjectIdentifierNamespace namespace,
            String                                externalName )
    {
        if( theExternalNameMap.get( externalName ) != null ) {
            throw new IllegalArgumentException( "External name is already used for a namespace: " + externalName );
        }

        synchronized( this ) {
            String localName = theInverseLocalNameMap.get( namespace );
            if( localName == null ) {
                throw new IllegalArgumentException( "Unknown namespace: " + namespace );
            }

            if( namespace.theExternalNames.isEmpty() ) {
                namespace.thePreferredExternalName = externalName;
            }
            namespace.theExternalNames.add( externalName );

            // update both -- no need to update inverse table
            theLocalNameMap.put( localName, namespace );
            theExternalNameMap.put( externalName, localName );
        }
    }

    /**
     * Invoked by a member namespace, to perform checking and doing.
     *
     * @param namespace the namespace from which the external name shall be removed
     * @param externalName the external name to be removed
     */
    protected void removeExternalName(
            DefaultMeshObjectIdentifierNamespace namespace,
            String                                externalName )
    {
        String already = theExternalNameMap.get( externalName );
        if( already == null ) {
            throw new IllegalArgumentException( "External name is not used for any namespace namespace: " + externalName );
        }

        String localName = theInverseLocalNameMap.get( namespace );
        if( StringHelper.compareTo( already, localName ) != 0 ) {
            throw new IllegalArgumentException( "External name is used for a different namespace: " + externalName );
        }

        synchronized( this ) {
            namespace.theExternalNames.remove( externalName );
            if( externalName.equals( namespace.thePreferredExternalName )) {

                if( namespace.theExternalNames.isEmpty() ) {
                    namespace.thePreferredExternalName = null;
                } else {
                    namespace.thePreferredExternalName = namespace.theExternalNames.iterator().next(); // can we do better?
                }
            }

            // update both -- no need to update inverse table
            theLocalNameMap.put( localName, namespace ); //
            theExternalNameMap.remove( externalName );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> getLocalNameIterator()
    {
        return localNameIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObjectIdentifierNamespace> namespaceIterator()
    {
        return new MappingCursorIterator<>(
                localNameIterator(),
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public MeshObjectIdentifierNamespace mapDelegateValueToHere(
                            String delegateValue )
                    {
                        return findByLocalName( delegateValue );
                    }

                    @Override
                    public String mapHereToDelegateValue(
                            MeshObjectIdentifierNamespace value )
                    {
                        return getLocalNameFor( value );
                    }
                } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObjectIdentifierNamespace> getNamespaceIterator()
    {
        return namespaceIterator();
    }

    /**
     * Create and allocate a local name for a Namespace.
     *
     * @return a pair of the created namespace and its local name
     */
    protected abstract Pair<String,DefaultMeshObjectIdentifierNamespace> createAllocateNamespace();

    /**
     * Maps local names to namespaces. This needs to be persistent.
     */
    protected final SmartMap<String,DefaultMeshObjectIdentifierNamespace> theLocalNameMap;

    /**
     * Maps namespaces to local names. Inverse of {@link theLocalNameMap}.
     * This does not need to be persistent. Because it is weak, it will clean up
     * if the localNameMap is purging entries from cache.
     */
    protected final Map<DefaultMeshObjectIdentifierNamespace,String> theInverseLocalNameMap = new WeakHashMap<>();

    /**
     * Maps external names to local names. This needs to be persistent.
     */
    protected final SmartMap<String,String> theExternalNameMap;

    /**
     * The local name of the primary namespace.
     */
    protected static final String PRIMARY_KEY = "primary";
}
