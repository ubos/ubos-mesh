//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace;

import net.ubos.util.smartmap.SmartMap;

/**
 * Factors out functionality common to many implementations of DelegatingMeshObjectIdentifierNamespaceMap.
 */
public abstract class AbstractContextualMeshObjectIdentifierNamespaceMap
    implements
        ContextualMeshObjectIdentifierNamespaceMap
{
    /**
     * Constructor.
     *
     * @param localNameMap resolves our local names to local names in the primary
     * @param inverseLocalNameMap resolves names in the primary to our local names
     * @param primary the primary map that owns our namespaces
     */
    protected AbstractContextualMeshObjectIdentifierNamespaceMap(
            SmartMap<String,String>                 localNameMap,
            SmartMap<String,String>                 inverseLocalNameMap,
            PrimaryMeshObjectIdentifierNamespaceMap primary )
    {
        theLocalNameMap        = localNameMap;
        theInverseLocalNameMap = inverseLocalNameMap;
        thePrimary             = primary;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrimaryMeshObjectIdentifierNamespaceMap getPrimaryMap()
    {
        return thePrimary;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifierNamespace getDefaultNamespace()
    {
        String primaryLocalName = theLocalNameMap.get( DEFAULT_LOCAL_NAME );
        if( primaryLocalName == null ) {
            return null;
        }

        MeshObjectIdentifierNamespace ret = thePrimary.findByLocalName( primaryLocalName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifierNamespace obtainDefaultNamespace()
    {
        MeshObjectIdentifierNamespace ret;

        String primaryLocalName = theLocalNameMap.get( DEFAULT_LOCAL_NAME );
        if( primaryLocalName == null ) {
            ret              = thePrimary.getPrimary();
            primaryLocalName = thePrimary.getLocalNameFor( ret );

            theLocalNameMap.put(        DEFAULT_LOCAL_NAME, primaryLocalName );
            theInverseLocalNameMap.put( primaryLocalName,   DEFAULT_LOCAL_NAME );

        } else {
            ret = thePrimary.findByLocalName( primaryLocalName );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized MeshObjectIdentifierNamespace findByLocalName(
            String localName )
    {
        String primaryLocalName = theLocalNameMap.get( localName );
        if( primaryLocalName == null ) {
            return null;
        }

        MeshObjectIdentifierNamespace ret = thePrimary.findByLocalName( primaryLocalName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalNameFor(
            MeshObjectIdentifierNamespace ns )
    {
        String primaryLocalName = thePrimary.getLocalNameFor( ns );
        if( primaryLocalName == null ) {
            return null;
        }

        String ret = theInverseLocalNameMap.get( primaryLocalName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String obtainLocalNameFor(
            MeshObjectIdentifierNamespace ns )
    {
        String primaryLocalName = thePrimary.getLocalNameFor( ns );
        if( primaryLocalName == null ) {
            throw new IllegalArgumentException( "This namespace does not belong to our primary: " + ns );
        }

        String localName = theInverseLocalNameMap.get( primaryLocalName );
        if( localName == null ) {
            localName = generateNewLocalName();

            theLocalNameMap.put(         localName,        primaryLocalName );
            theInverseLocalNameMap.put(  primaryLocalName, localName );
        }
        return localName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void register(
            String                        localName,
            MeshObjectIdentifierNamespace ns )
    {
        if( theLocalNameMap.containsKey( localName )) {
            throw new IllegalArgumentException( "Local name is used already: " + localName );
        }

        String primaryLocalName = thePrimary.getLocalNameFor( ns );
        if( primaryLocalName == null ) {
            throw new IllegalArgumentException( "This namespace does not belong to our primary: " + ns );
        }

        if( theInverseLocalNameMap.containsKey( primaryLocalName )) {
            throw new IllegalArgumentException(
                    "Namespace is already registered locally: "
                    + ns
                    + ": "
                    + theInverseLocalNameMap.get( primaryLocalName )
                    + " vs "
                    + localName );
        }

        theLocalNameMap.put(         localName,        primaryLocalName );
        theInverseLocalNameMap.put(  primaryLocalName, localName );
    }

    /**
     * Generate a new local name.
     *
     * @return the local name
     */
    protected abstract String generateNewLocalName();

    /**
     * The primary PrimaryMeshObjectIdentifierNamespaceMap that maintains our namespaces.
     */
    protected final PrimaryMeshObjectIdentifierNamespaceMap thePrimary;

    /**
     * Maps local names to local names in the primary. This needs to be persistent.
     */
    protected final SmartMap<String,String> theLocalNameMap;

    /**
     * Maps primary local names to local names. Inverse of {@link theLocalNameMap}.
     * This does not need to be persistent.
     */
    protected final SmartMap<String,String> theInverseLocalNameMap;
}
