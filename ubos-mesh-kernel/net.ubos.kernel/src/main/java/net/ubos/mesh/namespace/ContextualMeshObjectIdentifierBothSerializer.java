//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace;

import net.ubos.mesh.AbstractHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.util.StringHelper;

/**
 * Serializes MeshObjectIdentifiers using the local name of MeshObjectIdentifiers in a
 * ContextualMeshObjectIdentifiersNamespaceMap.
 */
public class ContextualMeshObjectIdentifierBothSerializer
    extends
        AbstractHashMeshObjectIdentifierBothSerializer
{
    /**
     * Factory method.
     *
     * @param idFact the MeshObjectIdentifierFactory to use
     * @param map the MeshObjectIdentifiersNamespaceMap that knows the local names
     * @return the created instance
     */
    public static ContextualMeshObjectIdentifierBothSerializer create(
            MeshObjectIdentifierFactory                idFact,
            ContextualMeshObjectIdentifierNamespaceMap map )
    {
        return new ContextualMeshObjectIdentifierBothSerializer( idFact, map );
    }

    /**
     * Constructor.
     *
     * @param idFact the MeshObjectIdentifierFactory to use
     * @param map the MeshObjectIdentifiersNamespaceMap that knows the local names
     */
    protected ContextualMeshObjectIdentifierBothSerializer(
            MeshObjectIdentifierFactory                idFact,
            ContextualMeshObjectIdentifierNamespaceMap map )
    {
        super( idFact );

        theMap = map;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String determineNamespaceString(
            MeshObjectIdentifierNamespace ns )
    {
        String localNsName = theMap.obtainLocalNameFor( ns );

        String ret;
        if( StringHelper.compareTo( ContextualMeshObjectIdentifierNamespaceMap.DEFAULT_LOCAL_NAME, localNsName ) == 0 ) {
            ret = null;

        } else {
            ret = localNsName;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MeshObjectIdentifier createFor(
            MeshObjectIdentifier contextIdentifier,
            String               nsName,
            String               localId )
    {
        MeshObjectIdentifierNamespace ns;
        if( nsName == null ) {
            if( contextIdentifier == null ) {
                ns = theMap.obtainDefaultNamespace();
            } else {
                ns = contextIdentifier.getNamespace();
            }

        } else {
            ns = theMap.findByLocalName( nsName );
        }

        MeshObjectIdentifier ret = theIdFact.createMeshObjectIdentifierIn( ns, localId );
        return ret;
    }

    /**
     * Obtain the Map. This is for test instrumentation.
     *
     * @return the map
     */
    public ContextualMeshObjectIdentifierNamespaceMap getMap()
    {
        return theMap;
    }

    /**
     * The map.
     */
    protected final ContextualMeshObjectIdentifierNamespaceMap theMap;
}
