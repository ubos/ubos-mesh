//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace.m;

import java.util.HashMap;
import net.ubos.mesh.namespace.AbstractContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * In-memory implementation.
 */
public class MContextualMeshObjectIdentifierNamespaceMap
    extends
        AbstractContextualMeshObjectIdentifierNamespaceMap
{
    /**
     * Factory.
     *
     * @param primary the delegate map
     * @return the created map
     */
    public static MContextualMeshObjectIdentifierNamespaceMap create(
            PrimaryMeshObjectIdentifierNamespaceMap primary )
    {
        return new MContextualMeshObjectIdentifierNamespaceMap(
                MSmartMap.create(),
                MSmartMap.create(),
                primary );
    }

    /**
     * Constructor with the map we delegate to.
     *
     * @param localNameMap resolves our local names to local names in the primary
     * @param inverseLocalNameMap resolves names in the primary to our local names
     * @param primary the delegate map
     */
    protected MContextualMeshObjectIdentifierNamespaceMap(
            SmartMap<String,String>                 localNameMap,
            SmartMap<String,String>                 inverseLocalNameMap,
            PrimaryMeshObjectIdentifierNamespaceMap primary )
    {
        super( localNameMap, inverseLocalNameMap, primary );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> localNameIterator()
    {
        return theLocalNameMap.keyIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String generateNewLocalName()
    {
        String localName = String.valueOf( theLocalNameCounter++ );
        return localName;
    }

    /**
     * The next local name issued when a new namespace is created.
     */
    protected int theLocalNameCounter = 0;
}
