//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.util.history.History;

/**
 * The historical record of a MeshObject's evolution over time.
 */
public interface MeshObjectHistory
    extends
        History<MeshObject>
{
    /**
     * Obtain the MeshBase with this MeshObjectHistory.
     *
     * @return the MeshBase
     */
    public MeshBase getMeshBase();

    /**
     * Obtain the identifier of the MeshObject whose history this is.
     *
     * @return the MeshObjectIdentifier
     */
    public MeshObjectIdentifier getMeshObjectIdentifier();
}
