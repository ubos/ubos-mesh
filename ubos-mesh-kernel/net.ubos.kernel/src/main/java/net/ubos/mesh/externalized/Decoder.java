//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

/**
 * Classes that know how to decode some kind of serialized form of  Mesh-related
 * information implement this.
 */
public interface Decoder
{
    /**
     * Obtain a String that identifies the encoding scheme implemented by this Decoder.
     *
     * @return the encoding id.
     */
    public String getEncodingId();
}
