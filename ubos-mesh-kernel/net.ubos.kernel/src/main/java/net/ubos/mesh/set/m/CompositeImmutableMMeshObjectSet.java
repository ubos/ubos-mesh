//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.CompositeImmutableMeshObjectSet;
import net.ubos.mesh.set.MeshObjectSet;

import net.ubos.mesh.set.MeshObjectSetFactory;

/**
 * <p>This MeshObjectSet is calculated as a composition of other
 *    MeshObjectSets. The type of composition can be specified by
 *    choosing an appropriate factory method.</p>
 *
 * <p>CompositeImmutableMeshObjectSets are immutable, even if some of
 *    their arguments may be not.</p>
 */
public abstract class CompositeImmutableMMeshObjectSet
        extends
            ImmutableMMeshObjectSet
        implements
            CompositeImmutableMeshObjectSet
{
    /**
     * Private constructor, use factory methods.
     *
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     * @param content the content of this set
     * @param operands the operands that were used to construct the content
     */
    protected CompositeImmutableMMeshObjectSet(
            MeshObjectSetFactory factory,
            MeshObject    []     content,
            MeshObjectSet []     operands )
    {
        super( factory, content );

        theOperands = operands;
    }

    /**
     * Obtain the operands that were used to construct this set.
     *
     * @return the operands that were used to construct this set
     */
    @Override
    public MeshObjectSet [] getOperands()
    {
        return theOperands;
    }

    /**
     * The operands of this set.
     */
    protected MeshObjectSet [] theOperands;

    /**
     * An immutable set that is constructed as a unification of the content of other sets.
     */
    public static class Unification
            extends
                CompositeImmutableMMeshObjectSet
    {
        /**
         * Private constructor, use factory methods.
         *
         * @param factory the MeshObjectSetFactory that created this MeshObjectSet
         * @param content the content of this set
         * @param operands the operands that were used to construct the content
         */
        protected Unification(
                MeshObjectSetFactory factory,
                MeshObject    []     content,
                MeshObjectSet []     operands )
        {
            super( factory, content, operands );
        }
    }

    /**
     * An immutable set that is constructed as an intersection of the content of other sets.
     */
    public static class Intersection
            extends
                CompositeImmutableMMeshObjectSet
    {
        /**
         * Private constructor, use factory methods.
         *
         * @param factory the MeshObjectSetFactory that created this MeshObjectSet
         * @param content the content of this set
         * @param operands the operands that were used to construct the content
         */
        protected Intersection(
                MeshObjectSetFactory factory,
                MeshObject    []     content,
                MeshObjectSet []     operands )
        {
            super( factory, content, operands );
        }
    }

    /**
     * An immutable set that is constructed as a set subtraction.
     */
    public static class Minus
            extends
                CompositeImmutableMMeshObjectSet
    {
        /**
         * Private constructor, use factory methods.
         *
         * @param factory the MeshObjectSetFactory that created this MeshObjectSet
         * @param content the content of this set
         * @param operands the operands that were used to construct the content
         */
        protected Minus(
                MeshObjectSetFactory factory,
                MeshObject    []     content,
                MeshObjectSet []     operands )
        {
            super( factory, content, operands );
        }
    }
}
