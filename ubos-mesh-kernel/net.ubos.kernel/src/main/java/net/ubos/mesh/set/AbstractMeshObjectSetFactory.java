//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.util.ArrayHelper;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.logging.Log;

/**
 * Factors out common behaviors of many MeshObjectSetFactories.
 */
public abstract class AbstractMeshObjectSetFactory
        implements
            MeshObjectSetFactory
{
    private static final Log log = Log.getLogInstance( AbstractMeshObjectSetFactory.class ); // our own, private logger

    /**
     * Constructor for subclasses only.
     *
     * @param componentClass           the Class to use to allocate arrays of MeshObjects
     * @param componentIdentifierClass the Class to use to allocate arrays of MeshObjectIdentifiers
     */
    protected AbstractMeshObjectSetFactory(
            Class<? extends MeshObject>           componentClass,
            Class<? extends MeshObjectIdentifier> componentIdentifierClass )
    {
        theComponentClass           = componentClass;
        theComponentIdentifierClass = componentIdentifierClass;
        theMeshBase                 = null; // not initialized
    }

    /**
     * Set the MeshBase on whose behalf this factory works.
     *
     * @param newValue the new value
     */
    public void setMeshBase(
            MeshBase newValue )
    {
        if( theMeshBase != null ) {
            throw new IllegalStateException( "Already have MeshBase, cannot reset after it has been set" );
        }
        theMeshBase = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObject [] members )
    {
        return createImmutableMeshObjectSet( members, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObjectSet      input,
            MeshObjectSelector selector )
    {
        MeshObject [] inputContent = input.getMeshObjects();
        MeshObject [] content;
        if( selector != null ) {
            content = ArrayHelper.createArray( theComponentClass, inputContent.length );
            int count = 0;
            for( int i=0 ; i<inputContent.length ; ++i ) {
                try {
                    if( selector.accepts( inputContent[i] )) {
                        content[ count++ ] = inputContent[i];
                    }
                } catch( IsDeadException ex ) {
                    if( log.isDebugEnabled() ) {
                        log.debug( ex );
                    }
                }
            }
            if( count < content.length ) {
                content = ArrayHelper.copyIntoNewArray( content, 0, count, theComponentClass );
            }
        } else {
            content = inputContent;
        }

        ImmutableMeshObjectSet ret = createImmutableMeshObjectSet( content );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet [] operands )
    {
        return createImmutableMeshObjectSetUnification( operands, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet one,
            MeshObjectSet two )
    {
        return createImmutableMeshObjectSetUnification( new MeshObjectSet[] { one, two } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet one,
            MeshObject    two )
    {
        // FIXME this can be optimized I guess
        return createImmutableMeshObjectSetUnification( new MeshObjectSet[] { one, createSingleMemberImmutableMeshObjectSet( two ) } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet [] operands )
    {
        return createImmutableMeshObjectSetIntersection( operands, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet one,
            MeshObjectSet two )
    {
        return createImmutableMeshObjectSetIntersection( new MeshObjectSet[] { one, two } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet one,
            MeshObject    two )
    {
        return createImmutableMeshObjectSetIntersection(
                one,
                createSingleMemberImmutableMeshObjectSet( two ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetMinus(
            MeshObjectSet one,
            MeshObjectSet two )
    {
        return createImmutableMeshObjectSetMinus( one, two, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetMinus(
            MeshObjectSet one,
            MeshObject    two )
    {
        return createImmutableMeshObjectSetMinus(
                one,
                createSingleMemberImmutableMeshObjectSet( two ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * Helper method for implementation and subclasses: create array with the merged
     * content contained in all the sets and without duplicates.
     *
     * @param inputSets the set of input MeshObjectSets to be unified
     * @param selector selector for the resulting MeshObjects
     * @return the MeshObjects that are contained in the inputSets, but without duplicates
     */
    protected MeshObject [] unify(
            MeshObjectSet []   inputSets,
            MeshObjectSelector selector )
    {
        int count = 0;
        for( int i=0 ; i<inputSets.length ; ++i ) {
            count += inputSets[i].size();
        }

        MeshObject [] objs = ArrayHelper.createArray( theComponentClass, count );

        count = 0;
        for( int i=0 ; i<inputSets.length ; ++i ) {
            MeshObject [] candidates = inputSets[i].getMeshObjects();

            for( int j=0 ; j<candidates.length ; ++j ) {
                if( !ArrayHelper.isIn( candidates[j], objs, 0, count, false )) {
                    if( selector == null || selector.accepts( candidates[j] )) {
                        objs[ count++ ] = candidates[j];
                    }
                }
            }
        }
        if( count < objs.length ) {
            objs = ArrayHelper.copyIntoNewArray( objs, 0, count, MeshObject.class );
        }

        return objs;
    }

    /**
     * Helper method for implementation and subclasses: create array with the content
     * contained in everyone of the sets and without duplicates.
     *
     * @param inputSets the set of input MeshObjectSets to be intersected
     * @param selector selector for the resulting MeshObjects
     * @return the MeshObjects that are contained in all of the inputSets
     */
    protected MeshObject [] intersect(
            MeshObjectSet []   inputSets,
            MeshObjectSelector selector )
    {
        MeshObject [] ret = ArrayHelper.copyIntoNewArray(
                inputSets[0].getMeshObjects(),
                theComponentClass ); // shorten later, this is max

        int takenOut = 0;
        for( int i=1; i<inputSets.length ; ++i ) {
            MeshObject [] thisContent = inputSets[i].getMeshObjects();
            for( int j=0 ; j<ret.length ; ++j ) {
                MeshObject testObject = ret[j];
                if( testObject == null ) {
                    continue; // was removed previously
                }
                if( !ArrayHelper.isIn( testObject, thisContent, false )) {
                    if( selector == null || !selector.accepts( testObject )) { // note this is !accepts
                        ret[j] = null;
                        ++takenOut;
                    }
                }
            }
        }
        if( takenOut > 0 ) {
            MeshObject [] old = ret;

            ret = ArrayHelper.createArray( theComponentClass, old.length - takenOut );

            for( int i=0, j=0 ; i<old.length ; ++i ) {
                if( old[i] != null ) {
                    ret[j++] = old[i];
                }
            }
        }
        return ret;
    }

    /**
     * The MeshBase to which this MeshObjectSetFactory belongs.
     */
    protected MeshBase theMeshBase;

    /**
     * The Class to use to allocate arrays of MeshObject.
     */
    protected Class<? extends MeshObject> theComponentClass;

    /**
     * The Class to use to allocate arrays of MeshObjectIdentifier.
     */
    protected Class<? extends MeshObjectIdentifier> theComponentIdentifierClass;
}
