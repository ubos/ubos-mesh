//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

/**
  * This Exception is thrown when an operation is attempted
  * that is illegal on this MeshObject.
  */
public abstract class IllegalOperationTypeException
        extends
            MeshObjectGraphModificationException
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject on which the illegal operation was attempted
     */
    protected IllegalOperationTypeException(
            MeshObject obj )
    {
        super( obj.getMeshBaseView() );

        theMeshObject = obj;
    }

    /**
     * Obtain the MeshObject on which the illegal operation was attempted.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * The MeshObject on which the illegal operation was attempted.
     */
    protected final MeshObject theMeshObject;
}
