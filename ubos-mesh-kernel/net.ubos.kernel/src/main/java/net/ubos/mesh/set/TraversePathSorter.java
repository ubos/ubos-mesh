//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.model.traverse.TraversePath;

/**
 * This interface is supported by objects that can sort an array of TraversePaths.
 */
public interface TraversePathSorter
{
    /**
     * Pass in an array of TraversePath in arbitrary order, and return the same set
     * in order.
     *
     * @param unorderedSet the unordered set
     * @return this a convenience return value: it's the same array as was passed in
     */
    public abstract TraversePath [] getOrderedInSame(
            TraversePath [] unorderedSet );

    /**
     * Pass in an array of TraversePath in arbitrary order, and return a new set
     * with the same content in order.
     *
     * @param unorderedSet the unordered set
     * @return the new array with the ordered content
     */
    public abstract TraversePath [] getOrderedInNew(
            TraversePath [] unorderedSet );

    /**
     * Obtain the index where a new TraversePath would be inserted to keep the
     * passed-in array ordered according to this sorter.
     *
     * @param newMeshObject the new TraversePath potentially to be inserted
     * @param orderedSet the set into which newMeshObject could potentially be inserted
     * @return the index where newMeshObject would be inserted
     */
    public abstract int getIndexOfNew(
            TraversePath    newMeshObject,
            TraversePath [] orderedSet );

    /**
     * Obtain the index of a an existing TraversePath in the array.
     *
     * @param objectToLookFor the TraversePath that we look for
     * @param orderedSet the set in which we look.
     * @return the index of objectToLookFor within orderedSet, or -1 if not found
     */
    public abstract int getIndexOf(
            TraversePath    objectToLookFor,
            TraversePath [] orderedSet );

    /**
     * Obtain the user visible-name for this sorter.
     *
     * @return the user-visible name
     */
    public abstract String getUserName();
}
