//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.model.traverse.TraversePath;

/**
  * This interface allows us to indicate whether or not a certain TraversePath
  * matches a selection criteria.
  */
public interface TraversePathSelector
{
     /**
      * Returns true if a candidate TraversalPath shall be accepted.
      *
      * @param candidate the candidate TraversalPath
      * @return true if this candidate shall be accepted
      */
    public abstract boolean accepts(
            TraversePath candidate );
}
