//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.xml;

import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.externalized.HasSettableTypesPropertiesAttributes;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.model.primitives.MeshTypeIdentifierDeserializer;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import net.ubos.model.primitives.externalized.xml.PropertyValueXmlDecoderHandler;
import net.ubos.model.primitives.externalized.xml.UnknownTokenException;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * Factored-out SAX callback handler.
 */
public class MeshObjectXmlDecoderHandler
    extends
        PropertyValueXmlDecoderHandler
    implements
        MeshObjectXmlTags
{
    private static final Log log = Log.getLogInstance(MeshObjectXmlDecoderHandler.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param idDeserializer knows now to deserializer MeshObjectIdentifiers
     */
    public MeshObjectXmlDecoderHandler(
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        theIdDeserializer = idDeserializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startElement1(
            String     namespaceURI,
            String     localName,
            String     qName,
            Attributes attrs )
        throws
            SAXException
    {
        String identifier;
        String timeCreated;
        String timeUpdated;
        String name;
        String type;

        switch( qName ) {
            case MESHOBJECT_TAG:
                theMeshObjectBeingParsed = new ParserFriendlyExternalizedMeshObject();
                theHasTypesAndPropertiesBeingParsed = theMeshObjectBeingParsed;
                identifier      = attrs.getValue( IDENTIFIER_TAG );
                timeCreated     = attrs.getValue( TIME_CREATED_TAG );
                timeUpdated     = attrs.getValue( TIME_UPDATED_TAG );
                if( identifier != null ) {
                    try {
                        theMeshObjectBeingParsed.setIdentifier(
                                theIdDeserializer.meshObjectIdentifierFromExternalForm( identifier ));
                    } catch( ParseException ex ) {
                        error( ex );
                    }
                }
                if( timeCreated != null && timeCreated.length() > 0 ) {
                    theMeshObjectBeingParsed.setTimeCreated( Long.parseLong( timeCreated ));
                }
                if( timeUpdated != null && timeUpdated.length() > 0 ) {
                    theMeshObjectBeingParsed.setTimeUpdated( Long.parseLong( timeUpdated ));
                }
                break;

            case STRING_ATTRIBUTE_TAG: // same for both
            case ATTRIBUTE_TAG:
                name = attrs.getValue( ATTRIBUTE_NAME_TAG );
                if( name != null && name.length() > 0 ) {
                    theHasTypesAndPropertiesBeingParsed.addAttributeName( name );
                } else {
                    log.error( "empty '" + ATTRIBUTE_NAME_TAG + "' on '" + ATTRIBUTE_TAG + "'" );
                }
                break;

            case TYPE_TAG:
                // no op
                break;

            case PROPERTY_TYPE_TAG:
                type = attrs.getValue( TYPE_TAG );
                if( type != null && type.length() > 0 ) {
                    try {
                        theHasTypesAndPropertiesBeingParsed.addPropertyType(
                                theTypeIdDeserializer.fromExternalForm( type ));
                    } catch( ParseException ex ) {
                        throw new LocalizedSAXParseException.Delegate( theLocator, ex );
                    }
                } else {
                    log.error( "empty '" + TYPE_TAG + "' on '" + PROPERTY_TYPE_TAG + "'" );
                }
                break;

            case NEIGHBOR_TAG:
                identifier  = attrs.getValue( IDENTIFIER_TAG );
                timeCreated = attrs.getValue( TIME_CREATED_TAG );

                if( identifier != null ) {
                    try {
                        theHasTypesAndPropertiesBeingParsed = new ParserFriendlyExternalizedMeshObject.ThisEnd(
                                theIdDeserializer.meshObjectIdentifierFromExternalForm( identifier ),
                                timeCreated == null || timeCreated.isBlank() ? -1L : Long.parseLong( timeCreated )); // default for old files

                    } catch( ParseException ex ) {
                        error( ex );
                    }
                } else {
                    log.error( "empty '" + IDENTIFIER_TAG + "' on '" + NEIGHBOR_TAG + "'" );
                }
                break;

            default:
                startElement2( namespaceURI, localName, qName, attrs );
                break;
        }
    }

    /**
     * Invoked when no previous start-element parsing rule has matched. Allows subclasses to add to parsing.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @param attrs the Attributes at this element
     * @throws SAXException thrown if a parsing error occurrs
     */
    protected void startElement2(
            String     namespaceURI,
            String     localName,
            String     qName,
            Attributes attrs )
        throws
            SAXException
    {
        throw new UnknownTokenException( namespaceURI, qName, theLocator );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void endElement1(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        switch( qName ) {
            case MESHOBJECT_TAG:
                // no op
                break;

            case STRING_ATTRIBUTE_TAG:
                if( theCharacters == null ) { // empty String shows up as null
                    theHasTypesAndPropertiesBeingParsed.addAttributeValue( "" );
                } else {
                    theHasTypesAndPropertiesBeingParsed.addAttributeValue( theCharacters.toString() );
                }
                break;

            case ATTRIBUTE_TAG:
                try {
                    theHasTypesAndPropertiesBeingParsed.addAttributeValue(
                            theParent.base64StringAsSerializable( theCharacters.toString() ));
                } catch( DecodingException ex ) {
                    throw new LocalizedSAXParseException.Delegate( theLocator, ex );
                } catch( IOException ex ) {
                    throw new LocalizedSAXParseException.Delegate( theLocator, ex );
                }
                break;

            case TYPE_TAG:
                try {
                    theHasTypesAndPropertiesBeingParsed.addType(
                            theTypeIdDeserializer.fromExternalForm( theCharacters.toString() ) );
                } catch( ParseException ex ) {
                    throw new LocalizedSAXParseException.Delegate( theLocator, ex );
                }
                break;

            case PROPERTY_TYPE_TAG:
                theHasTypesAndPropertiesBeingParsed.addPropertyValue( thePropertyValue );
                break;

            case NEIGHBOR_TAG:
                theMeshObjectBeingParsed.addThisEnd( theHasTypesAndPropertiesBeingParsed );
                theHasTypesAndPropertiesBeingParsed = null;
                break;

            default:
                endElement2( namespaceURI, localName, qName );
                break;
        }
    }

    /**
     * Invoked when no previous end-element parsing rule has matched. Allows subclasses to add to parsing.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @throws SAXException thrown if a parsing error occurrs
     */
    protected void endElement2(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        throw new UnknownTokenException( namespaceURI, qName, theLocator );
    }

    /**
     * Knows how to deserialize MeshObjectIdentifiers.
     */
    protected final MeshObjectIdentifierDeserializer theIdDeserializer;

    /**
     * The ExternalizedMeshObject that is currently being parsed, if any.
     */
    protected ParserFriendlyExternalizedMeshObject theMeshObjectBeingParsed = null;

    /**
     * The MeshObject or ThisEnd that is currently being parsed, if any.
     */
    protected HasSettableTypesPropertiesAttributes theHasTypesAndPropertiesBeingParsed = null;

    /**
     * Knows how to deserialize MeshTypeIdentifiers.
     */
    protected static final MeshTypeIdentifierDeserializer theTypeIdDeserializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;
}
