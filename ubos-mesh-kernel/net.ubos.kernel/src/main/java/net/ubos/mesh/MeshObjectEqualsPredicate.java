//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.util.function.BiPredicate;

/**
 * Different implementations of equality for MeshObjects.
 */
public enum MeshObjectEqualsPredicate
    implements
        BiPredicate<MeshObject,MeshObject>
{
    /**
     * Equality by referring to the same Java object.
     */
    BY_IDENTITY {
        @Override
        public boolean test(
                MeshObject one,
                MeshObject two )
        {
            return one == two;
        }
    },

    /**
     * Equality by having the same MeshObjectIdentifier. This makes two MeshObjects equal
     * if they represent the same MeshObject at different times, for example, or
     * different MeshBases.
     */
    BY_IDENTIFIER {
        @Override
        public boolean test(
                MeshObject one,
                MeshObject two )
        {
            if( one == null ) {
                return two == null;
            }
            return one.getIdentifier().equals( two.getIdentifier() );
        }
    },

    /**
     * Equality by having the same MeshObjectIdentifier and time of last update. This makes two
     * MeshObjects equal if they represent the same MeshObject in the same version in different
     * MeshBases.
     */
    BY_IDENTIFIER_AND_TIME_UPDATED {
        @Override
        public boolean test(
                MeshObject one,
                MeshObject two )
        {
            if( one == null ) {
                return two == null;
            }
            if( ! one.getIdentifier().equals( two.getIdentifier() )) {
                return false;
            }
            return one.getTimeUpdated() == two.getTimeUpdated();
        }
    }
}
