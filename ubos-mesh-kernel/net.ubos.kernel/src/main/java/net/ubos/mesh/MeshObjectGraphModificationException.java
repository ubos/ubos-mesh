//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.meshbase.MeshBaseView;
import net.ubos.util.exception.AbstractLocalizedRuntimeException;

/**
 * Superclass of all Exceptions that occur when unsuccessfully manipulating the
 * graph of MeshObjects, or individual MeshObjects.
 */
public abstract class MeshObjectGraphModificationException
        extends
            AbstractLocalizedRuntimeException
{
    /**
     * Constructor without a message or a cause.
     *
     * @param mbv the MeshBaseView in which this Exception was created
     */
    protected MeshObjectGraphModificationException(
            MeshBaseView mbv )
    {
        theMeshBaseView = mbv;
    }

    /**
     * Constructor with a message but no cause.
     *
     * @param mbv the MeshBaseView in which graph modification failed
     * @param msg the message
     */
    public MeshObjectGraphModificationException(
            MeshBaseView mbv,
            String       msg )
    {
        super( msg );

        theMeshBaseView = mbv;
    }

    /**
     * Constructor with no message but a cause.
     *
     * @param mbv the MeshBaseView in which graph modification failed
     * @param cause the Throwable that caused this Exception
     */
    public MeshObjectGraphModificationException(
            MeshBaseView mbv,
            Throwable    cause )
    {
        super( cause );

        theMeshBaseView = mbv;
    }

    /**
     * Constructor with a message and a cause.
     *
     * @param mbv the MeshBaseView in which graph modification failed
     * @param msg the message
     * @param cause the Exception that caused this Exception
     */
    public MeshObjectGraphModificationException(
            MeshBaseView mbv,
            String       msg,
            Throwable    cause )
    {
        super( msg, cause );

        theMeshBaseView = mbv;
    }

    /**
     * Obtain the MeshBaseView in which the graph modification failed.
     *
     * @return the MeshBaseView
     */
    public MeshBaseView getMeshBaseView()
    {
        return theMeshBaseView;
    }

    /**
     * The MeshBase in which the graph modification failed.
     */
    protected final MeshBaseView theMeshBaseView;
}
