//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.util.MeshTypeUtils;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Thrown if an operation requires a relationship to be not blessed with a certain RoleType, but it is.
 */
public class RoleTypeBlessedAlreadyException
        extends
            BlessedAlreadyException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject on which the illegal operation was attempted
     * @param type the RoleType of the already-existing blessing
     * @param neighbor the MeshObject identifying the other end of the relationship
     */
    public RoleTypeBlessedAlreadyException(
            MeshObject obj,
            RoleType   type,
            MeshObject neighbor )
    {
        super( obj, type );

        theNeighbor = neighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType getType()
    {
        return (RoleType) super.getType();
    }

    /**
     * Obtain the MeshObject at the other end of the relationship.
     *
     * @return the MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[]{
                    "meshObject",
                    "meshType",
                    "neighbor",
                    "types"
                },
                new Object[] {
                    theMeshObject,
                    theType,
                    theNeighbor,
                    MeshTypeUtils.meshTypeIdentifiersOrNull( theMeshObject )
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theType, theNeighbor };
    }

    /**
     * The MeshObject at the other end of the relationship.
     */
    protected final MeshObject theNeighbor;
}
