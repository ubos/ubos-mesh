//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * This Exception is thrown when there is an attempt to access  a RoleAttribute that does not exist on this
  * end of a relationship between this MeshObject and a neighbor MeshObject.
  */
public class IllegalRoleAttributeException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject on whose side of the relationship the RoleAttribute should have existed
     * @param neighbor the MeshObject on the other side of the relationship
     * @param name the name of the RoleAttribute that did not exist
     */
    public IllegalRoleAttributeException(
            MeshObject obj,
            MeshObject neighbor,
            String     name )
    {
        super( obj );

        theNeighbor      = neighbor;
        theAttributeName = name;
    }

    /**
     * Obtain the neighbor MeshObject.
     *
     * @return the neighbor MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * Obtain the name of the RoleAttribute that did not exist.
     *
     * @return the name
     */
    public String getAttributeName()
    {
        return theAttributeName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshBaseView",
                    "meshObject",
                    "neighbor",
                    "attributeName",
                    "attributes"
                },
                new Object[] {
                    theMeshBaseView,
                    theMeshObject,
                    theNeighbor,
                    theAttributeName,
                    theMeshObject.getAttributeNames()
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theNeighbor, theAttributeName, theMeshObject.getAttributeNames() };
    }

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * The name of the RoleAttribute that did not exist.
     */
    protected final String theAttributeName;
}
