//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.a;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;

/**
 * Implements MeshObjectIdentifier for the "A" implementation.
 */
public class AMeshObjectIdentifier
        implements
             MeshObjectIdentifier
{
    /**
     * Factory method.
     *
     * @param namespace the namespace in which this identifier has been allocated
     * @param localId the localId of the to-be AMeshObjectIdentifier
     * @return the created AMeshObjectIdentifier
     */
    static AMeshObjectIdentifier create(
            MeshObjectIdentifierNamespace namespace,
            String                        localId )
    {
        return new AMeshObjectIdentifier( namespace, localId );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param namespace the namespace in which this identifier has been allocated
     * @param localId the localId of the to-be-created AMeshObjectIdentifier
     */
    protected AMeshObjectIdentifier(
            MeshObjectIdentifierNamespace namespace,
            String                        localId )
    {
        if( namespace == null ) {
            throw new NullPointerException();
        }
        if( localId == null ) {
            throw new NullPointerException();
        }
        if( localId.startsWith( "/" )) {
            throw new IllegalArgumentException( "AMeshObjectIdentifier's localId must not start with a /: " + localId );
        }
        if( localId.contains( "//" )) {
            throw new IllegalArgumentException( "AMeshObjectIdentifier's localId must not contain double-/: " + localId );
        }

        theNamespace = namespace;
        theLocalId   = localId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalId()
    {
        return theLocalId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifierNamespace getNamespace()
    {
        return theNamespace;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int hashCode()
    {
        return theNamespace.hashCode() ^ theLocalId.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean equals(
            Object otherValue )
    {
        // trying to speed this up a bit
        if( this == otherValue ) {
            return true;
        }
        if( otherValue instanceof AMeshObjectIdentifier ) {
            AMeshObjectIdentifier realValue = (AMeshObjectIdentifier) otherValue;

            if( !theNamespace.equals( realValue.theNamespace )) {
                return false;
            }
            return theLocalId.equals( realValue.theLocalId );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return theNamespace + "::" + theLocalId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            MeshObjectIdentifier o )
    {
        int ret = theNamespace.compareTo( o.getNamespace() );
        if( ret == 0 ) {
            ret = theLocalId.compareTo( o.getLocalId() );
        }
        return ret;
    }

    /**
     * The MeshObjectIdentifierNamespace in which this identifier was allocated.
     */
    protected final MeshObjectIdentifierNamespace theNamespace;

    /**
     * The real value for the localId.
     */
    protected final String theLocalId;
}
