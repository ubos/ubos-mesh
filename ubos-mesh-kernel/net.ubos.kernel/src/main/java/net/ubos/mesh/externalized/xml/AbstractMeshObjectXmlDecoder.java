//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.xml;

import java.io.IOException;
import java.io.Reader;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.xml.AbstractPropertyValueXmlDecoder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Decodes an ExternalizedMeshObject from XML.
 */
public abstract class AbstractMeshObjectXmlDecoder
    extends
        AbstractPropertyValueXmlDecoder
    implements
        MeshObjectXmlTags
{
    /**
     * Deserialize an ExternalizedMeshObject from a stream.
     *
     * @param r read from here
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return return the just-instantiated ExternalizedMeshObject
     * @throws DecodingException thrown if decoding failed
     * @throws IOException an I/O problem occurred
     */
    public synchronized ExternalizedMeshObject decodeExternalizedMeshObject(
            Reader                           r,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        MeshObjectXmlDecoderHandler myHandler = new MeshObjectXmlDecoderHandler( idDeserializer );
        myHandler.setParent( this );

        try {
            theParser.parse( new InputSource( r ), myHandler );

            return myHandler.theMeshObjectBeingParsed;

        } catch( SAXException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }
}
