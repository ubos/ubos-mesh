//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * A pair of a MeshObject and a PropertyType, representing a Property of this MeshObject.
  */
public final class Property
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance(Property.class); // our own, private logger

    /**
      * Constructor. This constructor does not (and should not, due to possible blessing later)
      * check whether this MeshObject actually carries the specified PropertyType.
      *
      * @param mo the MeshObject
      * @param pt the PropertyType
      */
    public Property(
            MeshObject   mo,
            PropertyType pt )
    {
        theMeshObject   = mo;
        thePropertyType = pt;
    }

    /**
     * Obtain the MeshObject.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
      * Obtain the PropertyType.
      *
      * @return the PropertyType
      */
    public PropertyType getPropertyType()
    {
        return thePropertyType;
    }

    /**
      * Obtain the value of the Property for this MeshObject.
      *
      * @return the value of the Property for this MeshObject
      * @throws NotPermittedException thrown if the caller did not have sufficient access rights
      * @throws IllegalPropertyTypeException thrown if the MeshObject does not currently carry the PropertyType
      * @see #setValue
      */
    public PropertyValue getValue()
       throws
            IllegalPropertyTypeException,
            NotPermittedException
    {
        return theMeshObject.getPropertyValue( thePropertyType );
    }

    /**
     * Set the value of the Property for this MeshObject. This will
     * throw an Exception if the PropertyType is not carried by the
     * MeshObject.
     *
     * @param newValue the new Property value on this MeshObject
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @throws IllegalPropertyValueException thrown if the specified value is an illegal value
     *         for this property
     * @throws IllegalPropertyTypeException thrown if the MeshObject does not currently carry the PropertyType
     * @throws TransactionException thrown if this method is not invoked between proper
     *         Transaction boundaries
     * @see #getValue
     */
    public void setValue(
            PropertyValue newValue )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        theMeshObject.setPropertyValue( thePropertyType, newValue );
    }

    /**
     * This is a convenience function that sets the value of the Property
     * to a reasonable default. The reasonable default is obtained from the
     * PropertyType, and if not present, from the underlying DataType.
     * This is particularly useful when a default value is needed when the
     * Property was previously null for this MeshObject.
     *
     * @throws IllegalPropertyTypeException thrown if the MeshObject does not currently carry the PropertyType
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @throws TransactionException thrown if this method is not invoked between
     *         proper Transaction boundaries
     */
    public void setDefaultValue()
        throws
            IllegalPropertyTypeException,
            NotPermittedException,
            TransactionException
    {
        try {
            PropertyValue defaultValue = thePropertyType.getDefaultValue();
            if( defaultValue == null ) {
                defaultValue = thePropertyType.getDataType().instantiate();
            }
            setValue( defaultValue );

        } catch( IllegalPropertyValueException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof Property ) {
            Property realOther = (Property) other;

            return    theMeshObject.equals( realOther.theMeshObject )
                   && thePropertyType.equals( realOther.thePropertyType );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = theMeshObject.hashCode() ^ thePropertyType.hashCode();
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject",
                    "thePropertyType"
                },
                new Object[] {
                    theMeshObject,
                    thePropertyType
                });
    }

    /**
     * The MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The PropertyType.
     */
    protected final PropertyType thePropertyType;
}
