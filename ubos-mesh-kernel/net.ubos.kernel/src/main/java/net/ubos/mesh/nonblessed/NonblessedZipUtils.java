//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.nonblessed;

/**
 * Definitions and utility functions related to nonblessed ZIP file representation.
 */
public abstract class NonblessedZipUtils
{
    /**
     * Keep this abstract.
     */
    private NonblessedZipUtils() {}

    /**
     * Value for OJBECT_TYPE_ATTRIBUTE that indicates this MeshObject represents a ZIP file.
     */
    public static final String OBJECTTYPE_VALUE_ZIPFILE = "zip-file";
}
