//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.meshbase.MeshBaseView;
import net.ubos.model.traverse.TraversePath;
import net.ubos.util.cursoriterator.CursorIterable;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * A set of TraversePaths. It is similar to
 * MeshObjectSet, but it contains TraversePaths, not MeshObjects.
 */
public interface TraversePathSet
        extends
            CursorIterable<TraversePath>
{
    /**
     * Set a name for this TraversalPathSet. Its only purpose is to help in debugging.
     *
     * @param newValue the new value
     */
    public abstract void setDebugName(
            String newValue );

    /**
     * Obtain a name for this TraversalPathSet. Its only purpose is to help in debugging.
     *
     * @return the name
     */
    public abstract String getDebugName();

    /**
     * Obtain the MeshObjectSetFactory by which this TraversePathSet was created.
     *
     * @return the MeshObjectSetFactory
     */
    public abstract MeshObjectSetFactory getFactory();

    /**
     * Shorthand to obtain the MeshBaseView in which all of the contained MeshObjects reside.
     *
     * @return the MeshBaseView, or null if the set is empty
     */
    public abstract MeshBaseView getMeshBaseView();

    /**
      * Obtain the TraversePaths contained in this TraversePathSet.
      *
      * @return the TraversalPaths contained in this TraversePathSet
      */
    public abstract TraversePath [] getTraversePaths();

    /**
     * Assuming that this set contains no more than one TraversePath, obtain this one TraversePath. This
     * method is often very convenient if it is known to the application programmer that
     * a certain set may only contain one member. Invoking this method if the set has more
     * than one member will throw an Exception.
     *
     * @return the one element of the set, or null if the set is empty
     * @throws IllegalStateException thrown if the set contains more than one element
     */
    public abstract TraversePath getSingleMember()
        throws
            IllegalStateException;

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract CursorIterator<TraversePath> iterator();

    /**
     * Determine whether a certain TraversePath is contained in this set.
     * This method uses equals() to determine whether the path is contained.
     *
     * @param testObject the test TraversePath
     * @return true if testObject is contained in this set
     */
    public abstract boolean contains(
            TraversePath testObject );

    /**
     * Determine whether this set is empty.
     *
     * @return true if this set is empty
     */
    public abstract boolean isEmpty();

    /**
     * Determine the number of members of this set.
     *
     * @return the number of members of this set
     */
    public abstract int size();

    /**
     * Determine the number of members in this set. Same as size(), for JavaBeans-aware software.
     *
     * @return the number of members in this set
     */
    public abstract int getSize();

    /**
     * Obtain the destinations of the contained TraversalPaths as a MeshObjectSet.
     * While the same MeshObject may be a destination of more than one contained
     * TraversalPath, the MeshObjectSet naturally only contains this MeshObject once.
     *
     * @return the destinations of the contained TraversalPaths as a MeshObjectSet
     */
    public abstract MeshObjectSet getDestinationsAsSet();

    /**
     * Obtain the MeshObjects found at the given index in all the contained TraversalPaths,
     * and return them as a MeshObjectSet.
     * While the same MeshObject may be a step in more than one contained TraversalPath,
     * the MeshObjectSet naturally only contains this MeshObject once.
     *
     * @param index the index from which we want to obtain the MeshObject
     * @return the MeshObjects found at the given index as a MeshObjectSet
     */
    public abstract MeshObjectSet getStepAsSet(
            int index );
}
