//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;

/**
 * <p>Represents the identity of a {@link net.ubos.mesh.MeshObject MeshObject}.
 *    A MeshObject's MeshObjectIdentifier does not change during the lifetime of the
 *    MeshObject.
 * <p>Different MeshBases may implement MeshObjectIdentifier differently. Use
 *    a MeshObjectIdentifierFactory to instantiate objects supporting this interface.
 */
public interface MeshObjectIdentifier
    extends
        Comparable<MeshObjectIdentifier>
{
    /**
     * Obtain the namespace in which this MeshObjectIdentifier was allocated.
     *
     * @return the namespace
     */
    public abstract MeshObjectIdentifierNamespace getNamespace();

    /**
     * Obtain the localId component.
     *
     * @return the localId component
     */
    public abstract String getLocalId();

    /**
     * To save memory, this constant is allocated here and used wherever appropriate.
     */
    public static final MeshObjectIdentifier [] EMPTY_ARRAY = {};
}
