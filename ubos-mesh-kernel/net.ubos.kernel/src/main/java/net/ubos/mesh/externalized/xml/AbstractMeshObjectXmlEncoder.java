//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.xml;

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.xml.AbstractPropertyValueXmlEncoder;
import net.ubos.mesh.externalized.HasTypesPropertiesAttributes;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.XmlUtils;

/**
 * Encodes an ExternalizedMeshObject to XML.
 */
public abstract class AbstractMeshObjectXmlEncoder
        extends
            AbstractPropertyValueXmlEncoder
        implements
            MeshObjectXmlTags
{
    /**
     * Serialize a MeshObject.
     *
     * @param obj the input MeshObject
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    public void writeMeshObject(
            MeshObject                     obj,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException
    {
        writeExternalizedMeshObject( obj.asExternalized(), MESHOBJECT_TAG, idSerializer, w );
    }

    /**
     * Serialize an ExternalizedMeshObject to a StringBuilder with an alternate XML top-level tag.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param meshObjectTagName the XML top-level tag to use for this ExternalizedMeshObject
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    public void writeExternalizedMeshObject(
            ExternalizedMeshObject         obj,
            String                         meshObjectTagName,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException
    {
        writeOpeningTag( obj, meshObjectTagName, idSerializer, w );

        writeExternalizedMeshObjectAttributes(  obj, w );
        writeExternalizedMeshObjectTypes(       obj, w );
        writeExternalizedMeshObjectProperties(  obj, w );
        writeExternalizedMeshObjectNeighbors(   obj, idSerializer, w );

        w.write( "</" );
        w.write( meshObjectTagName );
        w.write( ">\n" );
    }

    /**
     * Serialize the opening tag, to make it easy for subclasses to add to the attributes list.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param meshObjectTagName the XML top-level tag to use for this ExternalizedMeshObject
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void writeOpeningTag(
            ExternalizedMeshObject         obj,
            String                         meshObjectTagName,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException
    {
        w.write( "<" );
        w.write( meshObjectTagName );
        w.write( " " );
        w.write( IDENTIFIER_TAG );
        w.write( "=\"" );
        w.write( XmlUtils.escape( idSerializer.toExternalForm( obj.getIdentifier() )) );
        w.write( "\" " );
        w.write( TIME_CREATED_TAG );
        w.write( "=\"" );
        writeLong( obj.getTimeCreated(), w );
        w.write( "\" " );
        w.write( TIME_UPDATED_TAG );
        w.write( "=\"" );
        writeLong( obj.getTimeUpdated(), w );
        w.write( "\">\n" );
    }

    /**
     * Serialize the Attributes section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void writeExternalizedMeshObjectAttributes(
            ExternalizedMeshObject obj,
            Writer                 w )
        throws
            EncodingException,
            IOException
    {
        String []       attributeNames  = obj.getAttributeNames();
        Serializable [] attributeValues = obj.getAttributeValues();

        for( int i=0 ; i<attributeNames.length ; ++i ) {
            if( attributeValues[i] instanceof String ) {
                w.write(  " " );
                w.write(  " <" );
                w.write( STRING_ATTRIBUTE_TAG );
                w.write(  " " );
                w.write( ATTRIBUTE_NAME_TAG );
                w.write(  "=\"" );
                writeQuotedString( attributeNames[i], w );
                w.write( "\">" );
                writeQuotedString( (String) attributeValues[i], w );
                w.write(  "</" );
                w.write( STRING_ATTRIBUTE_TAG );
                w.write( ">\n" );
            } else {
                w.write(  " " );
                w.write(  " <" );
                w.write( ATTRIBUTE_TAG );
                w.write(  " " );
                w.write( ATTRIBUTE_NAME_TAG );
                w.write(  "=\"" );
                writeQuotedString( attributeNames[i], w );
                w.write( "\">" );
                w.write( serializableAsBase64String( attributeValues[i] ));
                w.write(  "</" );
                w.write( ATTRIBUTE_TAG );
                w.write( ">\n" );
            }
        }
    }

    /**
     * Serialize the types section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void writeExternalizedMeshObjectTypes(
            ExternalizedMeshObject obj,
            Writer                 w )
        throws
            EncodingException,
            IOException
    {
        MeshTypeIdentifier [] allTypes = obj.getTypeIdentifiers();
        if( allTypes != null ) {
            for( int i=0 ; i<allTypes.length ; ++i ) {
                w.write( " " );
                w.write( "<" );
                w.write( TYPE_TAG );
                w.write( ">" );
                w.write( theTypeIdSerializer.toExternalForm( allTypes[i] ));
                w.write( "</" );
                w.write( TYPE_TAG );
                w.write( ">\n" );
            }
        }
    }

    /**
     * Serialize the properties section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void writeExternalizedMeshObjectProperties(
            ExternalizedMeshObject obj,
            Writer                 w )
        throws
            EncodingException,
            IOException
    {
        MeshTypeIdentifier [] allPropertyTypes  = obj.getPropertyTypes();
        PropertyValue []      allPropertyValues = obj.getPropertyValues();
        if( allPropertyTypes != null ) {
            for( int i=0 ; i<allPropertyTypes.length ; ++i ) {
                PropertyValue value        = allPropertyValues[i];

                w.write( " " );
                w.write( "<" );
                w.write( PROPERTY_TYPE_TAG );
                w.write( " " );
                w.write( TYPE_TAG );
                w.write( "=\"" );
                w.write( theTypeIdSerializer.toExternalForm( allPropertyTypes[i] ));
                w.write( "\">" );
                writePropertyValue( value, w );
                w.write( "</" );
                w.write( PROPERTY_TYPE_TAG );
                w.write( ">\n" );
            }
        }
    }

    /**
     * Serialize the neighbors section.
     *
     * @param obj the ExternalizedMeshObject to encode
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void writeExternalizedMeshObjectNeighbors(
            ExternalizedMeshObject         obj,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException
    {
        HasTypesPropertiesAttributes [] thisEnds = obj.getThisEnds();
        if( thisEnds != null ) {
            for( int i=0 ; i<thisEnds.length ; ++i ) {
                MeshObjectIdentifier  currentNeighbor        = thisEnds[i].getIdentifier();
                long                  currentNeighborCreated = thisEnds[i].getTimeCreated();
                MeshTypeIdentifier [] currentRoleTypes       = thisEnds[i].getTypeIdentifiers();

                MeshTypeIdentifier [] currentRolePropertyTypes   = thisEnds[i].getPropertyTypes();
                PropertyValue      [] currentRolePropertyValues  = thisEnds[i].getPropertyValues();
                String             [] currentRoleAttributeNames  = thisEnds[i].getAttributeNames();
                Serializable       [] currentRoleAttributeValues = thisEnds[i].getAttributeValues();

                w.write( " <" );
                w.write( NEIGHBOR_TAG );
                w.write( " " );
                w.write( IDENTIFIER_TAG );
                w.write( "=\"" );
                w.write( XmlUtils.escape( idSerializer.toExternalForm( currentNeighbor )) );
                w.write( "\" " );
                w.write( TIME_CREATED_TAG );
                w.write( "=\"" );
                writeLong( currentNeighborCreated, w );
                w.write( "\"" );

                if(    ( currentRoleTypes          == null || currentRoleTypes.length          == 0 )
                    && ( currentRoleAttributeNames == null || currentRoleAttributeNames.length == 0 ))
                {
                    w.write( "/>\n" );

                } else {
                    w.write( ">\n" );

                    if( currentRoleTypes != null ) {
                        for( int j=0 ; j<currentRoleTypes.length ; ++j ) {
                            w.write( "  <" );
                            w.write( TYPE_TAG );
                            w.write( ">" );
                            w.write( theTypeIdSerializer.toExternalForm( currentRoleTypes[j] ));
                            w.write( "</" );
                            w.write( TYPE_TAG );
                            w.write( ">\n" );
                        }

                        for( int j=0 ; j<currentRolePropertyTypes.length ; ++j ) {
                            w.write( "<" );
                            w.write( PROPERTY_TYPE_TAG );
                            w.write( " " );
                            w.write( TYPE_TAG );
                            w.write( "=\"" );
                            w.write( theTypeIdSerializer.toExternalForm( currentRolePropertyTypes[j] ));
                            w.write( "\">" );
                            writePropertyValue( currentRolePropertyValues[j], w );
                            w.write( "</" );
                            w.write( PROPERTY_TYPE_TAG );
                            w.write( ">\n" );
                        }
                    }

                    if( currentRoleAttributeNames != null ) {
                        for( int j=0 ; j<currentRoleAttributeNames.length ; ++j ) {
                            if( currentRoleAttributeValues[j] instanceof String ) {
                                w.write(  " " );
                                w.write(  " <" );
                                w.write( STRING_ATTRIBUTE_TAG );
                                w.write(  " " );
                                w.write( ATTRIBUTE_NAME_TAG );
                                w.write(  "=\"" );
                                writeQuotedString( currentRoleAttributeNames[j], w );
                                w.write( "\">" );
                                writeQuotedString( (String) currentRoleAttributeValues[j], w );
                                w.write(  "</" );
                                w.write( STRING_ATTRIBUTE_TAG );
                                w.write( ">\n" );

                            } else {
                                w.write(  " " );
                                w.write(  " <" );
                                w.write( ATTRIBUTE_TAG );
                                w.write(  " " );
                                w.write( ATTRIBUTE_NAME_TAG );
                                w.write(  "=\"" );
                                writeQuotedString( currentRoleAttributeNames[j], w );
                                w.write( "\">" );
                                w.write( serializableAsBase64String( currentRoleAttributeValues[j] ));
                                w.write(  "</" );
                                w.write( ATTRIBUTE_TAG );
                                w.write( ">\n" );
                            }
                        }
                    }

                    w.write( " </" );
                    w.write( NEIGHBOR_TAG );
                    w.write( ">\n" );
                }
            }
        }
    }

    /**
     * Knows how to serialize MeshTypeIdentifiers.
     */
    protected static final MeshTypeIdentifierSerializer theTypeIdSerializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;
}
