//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import java.util.ArrayList;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.ImmutableMeshObjectSet;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.util.ArrayHelper;

/**
  * A simple implementation of an immutable MeshObjectSet that keeps its content in
  * memory.
  */
public class ImmutableMMeshObjectSet
        extends
            AbstractMMeshObjectSet
        implements
            ImmutableMeshObjectSet
{
    /**
     * Constructor to be used by subclasses only.
     *
     * @param factory the MeshObjectSetFactory that created this MeshObjectSet
     * @param content the content of the MeshObjectSet
     */
    protected ImmutableMMeshObjectSet(
            MeshObjectSetFactory factory,
            MeshObject []        content )
    {
        super( factory );

        setInitialContent( content );
    }

    /**
     * {@inheritDoc}
     */
    public MeshObjectSet traverse(
            RoleType role )
    {
        ArrayList<MeshObject> almostRet = new ArrayList<>( theContent.length * 3 ); // fudge
        for( int i = 0 ; i < theContent.length ; ++i ) {
            MeshObject [] found = theContent[i].traverse( role ).getMeshObjects();

            for( int j=0 ; j<found.length ; ++j ) {
                if( ! almostRet.contains( found[j] )) {
                    almostRet.add( found[j] );
                }
            }
        }

        MeshObjectSet ret = theFactory.createImmutableMeshObjectSet( ArrayHelper.copyIntoNewArray( almostRet, MeshObject.class ));
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            TraverseSpecification theSpec )
    {
        if( theSpec instanceof RoleType ) {
            return traverse( (RoleType) theSpec );

        } else {
            return theSpec.traverse( this );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImmutableMMeshObjectSet inverse()
    {
        MeshObject [] newContent = new MeshObject[ theContent.length ];
        for( int i=0 ; i<theContent.length ; ++i ) {
            newContent[ theContent.length - i - 1 ] = theContent[i];
        }
        return new ImmutableMMeshObjectSet( theFactory, newContent );
    }
}
