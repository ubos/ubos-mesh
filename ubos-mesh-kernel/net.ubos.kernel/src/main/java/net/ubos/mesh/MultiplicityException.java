//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.RoleType;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * This Exception indicates a violation in the multiplicity of a RoleType. In other
 * words, this Exception is thrown if a relationship between MeshObjects A and B is
 * supposed to be blessed or unblessed with a RelationshipType X, as a result of which A would
 * participate in more or fewer RoleTypes of RelationshipType type X than allowed per
 * the definition of the RoleType.
 */
public class MultiplicityException
        extends
            MeshObjectGraphModificationException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param meshObject the MeshObject where the multiplicity violation was discovered
     * @param roleType the RoleType whose multiplicity was violated
     * @param neighbors the neighbor MeshObjects participating in relationships already that have roleType on this end
     */
    public MultiplicityException(
            MeshObject    meshObject,
            RoleType      roleType,
            MeshObject [] neighbors )
    {
        super( meshObject.getMeshBaseView() );

        theMeshObject = meshObject;
        theRoleType   = roleType;
        theNeighbors  = neighbors;
    }

    /**
     * Obtain the MeshObject where the multiplicity violation was discovered.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * Obtain the RoleType whose multiplicity was violated.
     *
     * @return the RoleType
     */
    public RoleType getRoleType()
    {
        return theRoleType;
    }

    /**
     * Obtain the other MeshObjects participating in relationships already that have the RoleType on this end.
     *
     * @return the MeshObjects
     */
    public MeshObject [] getNeighbors()
    {
        return theNeighbors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject",
                    "theRoleType",
                    "theOthers"
                },
                new Object[] {
                    theMeshObject,
                    theRoleType,
                    theNeighbors
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            theMeshObject,
            theRoleType,
            theRoleType != null ? theRoleType.getMultiplicity() : null,
            theNeighbors.length
        };
    }

    /**
     * The MeshObject for which we discovered a violation.
     */
    protected final MeshObject theMeshObject;

    /**
     * The RoleType for which we discovered a violation.
     */
    protected final RoleType theRoleType;

    /**
     * The neighbor MeshObjects participating in relationships already that have the RoleType on this end.
     */
    protected final MeshObject [] theNeighbors;
}
