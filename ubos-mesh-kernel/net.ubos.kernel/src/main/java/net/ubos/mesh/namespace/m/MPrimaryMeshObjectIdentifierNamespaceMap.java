//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace.m;

import net.ubos.mesh.namespace.DefaultMeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.AbstractPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.util.Pair;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * In-memory implementation of MeshObjectIdentifierNamespaceMap.
 */
public class MPrimaryMeshObjectIdentifierNamespaceMap
    extends
        AbstractPrimaryMeshObjectIdentifierNamespaceMap
{
    /**
     * Factory method.
     *
     * @return the created MPrimaryMeshObjectIdentifierNamespaceMap
     */
    public static MPrimaryMeshObjectIdentifierNamespaceMap create()
    {
        return new MPrimaryMeshObjectIdentifierNamespaceMap(
                MSmartMap.create(),
                MSmartMap.create());
    }

    /**
     * Constructor.
     *
     * @param localNameMap resolves local names to namespaces
     * @param externalNameMap resolves external names to local names
     */
    protected MPrimaryMeshObjectIdentifierNamespaceMap(
            SmartMap<String,DefaultMeshObjectIdentifierNamespace> localNameMap,
            SmartMap<String,String>                               externalNameMap )
    {
        super( localNameMap, externalNameMap );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<String,DefaultMeshObjectIdentifierNamespace> createAllocateNamespace()
    {
        String                               localName = "ns-" + String.valueOf( theLocalNameCounter++ );
        DefaultMeshObjectIdentifierNamespace ns        = new DefaultMeshObjectIdentifierNamespace( this );

        return new Pair<>( localName, ns );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> localNameIterator()
    {
        return theLocalNameMap.keyIterator();
    }

    /**
     * The next local name issued when a new namespace is created.
     */
    protected int theLocalNameCounter = 0;
}
