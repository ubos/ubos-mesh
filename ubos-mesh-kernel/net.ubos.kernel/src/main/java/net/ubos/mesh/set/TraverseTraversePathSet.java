//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.model.traverse.TraverseSpecification;

/**
 * A TraversePathSet that is created by having traversed a TraverseSpecification.
 */
public interface TraverseTraversePathSet
        extends
            TraversePathSet
{
    /**
     * Obtain the set of start MeshObjects. If this TraversalTraversePathSet was created
     * by traversing from a single MeshObject, this will return a single-element set.
     *
     * @return the start NeshObjects
     */
    public abstract MeshObjectSet getStartOfTraverseSet();
    
    /**
     * Obtain the TraverseSpecification that was traversed.
     * 
     * @return the TraverseSpecification
     */
    public abstract TraverseSpecification getTraverseSpecification();
}
