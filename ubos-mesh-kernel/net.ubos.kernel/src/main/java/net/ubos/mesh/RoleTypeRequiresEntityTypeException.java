//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.EntityType;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Thrown if a MeshObject cannot be unblessed because a RoleType requires this
 * MeshObject to continue being blessed by that MeshType.
 */
public class RoleTypeRequiresEntityTypeException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject on which the illegal operation was attempted, if available
     * @param entityType the EntityType of the attempted unblessing
     * @param roleType the RoleType of that blocked the attempted unblessing
     * @param neighbor the MeshObject at the other end of the relationship
     */
    public RoleTypeRequiresEntityTypeException(
            MeshObject obj,
            EntityType entityType,
            RoleType   roleType,
            MeshObject neighbor )
    {
        super( obj );

        theEntityType = entityType;
        theRoleType   = roleType;
        theNeighbor   = neighbor;
    }

    /**
     * Obtain the EntityType of the attempted unblessing.
     *
     * @return the EntityType
     */
    public EntityType getEntityType()
    {
        return theEntityType;
    }

    /**
     * Obtain the RoleType that blocked the unblessing of the MeshObject.
     *
     * @return the RoleType
     */
    public RoleType getRoleType()
    {
        return theRoleType;
    }

    /**
     * Obtain the MeshObject at the other end of the relationship.
     *
     * @return the MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[]{
                    "meshObject",
                    "entityType",
                    "roleType",
                    "neighbor",
                    "types"
                },
                new Object[] {
                    theMeshObject,
                    theEntityType,
                    theRoleType,
                    theNeighbor,
                    MeshTypeUtils.meshTypeIdentifiersOrNull( theMeshObject )
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theEntityType, theRoleType, theNeighbor };
    }

    /**
     * The EntityType whose unblessing was attempted.
     */
    protected final EntityType theEntityType;

    /**
     * The RoleType that blocked the unblessing.
     */
    protected final RoleType theRoleType;

    /**
     * The MeshObject at the other end of the relationship that blocked the unblessing.
     */
    protected final MeshObject theNeighbor;
}
