//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import java.io.IOException;
import java.io.Writer;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;

/**
 * This interface is supported by classes that know how to serialize
 * ExternalizedMeshObject.
 */
public interface MeshObjectEncoder
    extends
        Encoder
{
    /**
     * Serialize a MeshObject.
     *
     * @param obj the input MeshObject
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param w the Writer to which to write the ExternalizedMeshObject
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    public void writeMeshObject(
            MeshObject                     obj,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException;
}
