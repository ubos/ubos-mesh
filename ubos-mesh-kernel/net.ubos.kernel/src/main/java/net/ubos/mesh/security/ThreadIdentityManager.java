//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.security;

import net.ubos.mesh.MeshObject;

import java.util.HashMap;
import java.util.concurrent.Callable;

/**
 * A ThreadIdentityManager manages caller identities associated with Threads.
 */
public abstract class ThreadIdentityManager
{
    /**
     * Determine the identity of the caller. This may return null, indicating that
     * the caller is anonymous.
     *
     * @return the identity of the caller, or null.
     */
    public static MeshObject getCaller()
    {
        Who w = getWho();
        if( w != null ) {
            return w.getCaller();
        } else {
            return null;
        }
    }

    /**
     * Determine the principal of the caller. This may return null, indicating that
     * the caller is anonymous.
     *
     * @return the principal of the caller, or null.
     */
    public static MeshObject getCallerPrincipal()
    {
        Who w = getWho();
        if( w != null ) {
            return w.getPrincipal();
        } else {
            return null;
        }
    }

    /**
     * Determine the groups of the caller. This may return null, indicating that
     * the caller is anonymous. If the caller is known, a (possibly empty) array will
     * be returned.
     *
     * @return the groups of the caller, or null.
     */
    public static String [] getCallerGroups()
    {
        Who w = getWho();
        if( w != null ) {
            return w.getGroups();
        } else {
            return null;
        }
    }

    /**
     * Determine both caller and its groups at the same time. This may return null,
     * indicating that the caller is anonymous.
     *
     * @return Pair of caller and groups
     */
    public static Who getWho()
    {
        synchronized( theCallersOnThreads ) {
            Who ret = theCallersOnThreads.get( Thread.currentThread() );
            return ret;
        }
    }

    /**
     * Determine whether the caller is a member of the specified group.
     *
     * @param group the group
     * @return true if the caller is a member of the specified group
     */
    public static boolean isCallerGroupMemberOf(
            String group )
    {
        Who w = getWho();
        if( w == null ) {
            return false;
        }
        String [] groups = w.getGroups();
        if( groups == null ) {
            return false;
        }
        for( String g : groups ) {
            if( group.equals( group )) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine whether the current Thread has super user privileges.
     *
     * @return true if the current Thread has super user privileges.
     */
    public static boolean isSu()
    {
        Integer level;
        synchronized( theSuThreads ) {
            level = theSuThreads.get( Thread.currentThread() );
        }

        if( level == null ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Set the identity of the caller on this Thread. This will unset any previous
     * identity set on this Thread. Generally, the sequence of invocation should be:
     * <pre>
     * oldWho = ThreadIdentityManager.getWho();
     * try {
     *     ThreadIdentityManager.setWho( newWho );
     *     performWorkAsNewCaller();
     * } finally {
     *     ThreadIdentityManager.setWho( oldWho );
     * }
     * </pre>
     *
     * @param caller the account of the caller, or null if no account
     * @param principal the principal used by the caller, or null if anonymous
     * @return the previously set caller, if any
     * @see #unsetCaller
     */
    public static Who setWho(
            MeshObject caller,
            MeshObject principal )
    {
        return setWho( caller, principal, EMPTY_GROUPS );
    }

    /**
     * Set the identity of the caller on this Thread. This will unset any previous
     * identity set on this Thread. Generally, the sequence of invocation should be:
     * <pre>
     * oldWho = ThreadIdentityManager.getWho();
     * try {
     *     ThreadIdentityManager.setWho( newWho );
     *     performWorkAsNewCaller();
     * } finally {
     *     ThreadIdentityManager.setWho( oldWho );
     * }
     * </pre>
     *
     * @param caller the account of the caller, or null if no account
     * @param principal the principal used by the caller, or null if anonymous
     * @param callerGroups an array of groups the caller is part of
     * @return the previously set caller, if any
     * @see #unsetCaller
     */
    public static Who setWho(
            MeshObject caller,
            MeshObject principal,
            String []  callerGroups )
    {
        return setWho( new Who( caller, principal, callerGroups ));
    }

    /**
     * Set the identity of the caller on this Thread. This will unset any previous
     * identity set on this Thread. Generally, the sequence of invocation should be:
     * <pre>
     * oldWho = ThreadIdentityManager.getWho();
     * try {
     *     ThreadIdentityManager.setWho( newWho );
     *     performWorkAsNewCaller();
     * } finally {
     *     ThreadIdentityManager.setWho( oldWho );
     * }
     * </pre>
     *
     * @param who identity of the caller
     * @return the previously set caller, if any
     * @see #unsetCaller
     */
    public static Who setWho(
            Who who )
    {
        Who oldWho;
        if( who != null ) {
            if( who.getPrincipal() == null ) {
                throw new NullPointerException();
            }
            synchronized( theCallersOnThreads ) {
                oldWho = theCallersOnThreads.put(
                        Thread.currentThread(),
                        who );
            }
        } else {
            synchronized( theCallersOnThreads ) {
                oldWho = theCallersOnThreads.remove(
                        Thread.currentThread());
            }
        }
        return oldWho;
    }

    /**
     * Unset the identity of the caller on this Thread. This is called when the caller
     * is done.
     *
     * @return the previously set caller, if any
     * @see #setWho
     */
    public static Who unsetCaller()
    {
        Who oldWho;
        synchronized( theCallersOnThreads ) {
            oldWho = theCallersOnThreads.remove( Thread.currentThread() );
        }
        return oldWho;
    }

    /**
     * Make the current Thread have super user rights.
     */
    public static void sudo()
    {
        Thread t = Thread.currentThread();

        synchronized( theSuThreads ) {
            Integer level = theSuThreads.get( t );
            if( level == null ) {
                level = 1;
            } else {
                ++level;
            }
            theSuThreads.put( t, level );
        }
    }

    /**
     * Release super user rights from the current Thread.
     */
    public static void sudone()
    {
        Thread t = Thread.currentThread();

        synchronized( theSuThreads ) {
            Integer level = theSuThreads.get( t );
            int     l     = level - 1;

            if( l > 0 ) {
                theSuThreads.put( t, l );
            } else {
                theSuThreads.remove( t );
            }
        }
    }

    /**
     * Execute this action as the provided user.
     *
     * @param caller the caller, or null if anonymous
     * @param r the Runnable containing the action
     */
    public static void execAs(
            MeshObject caller,
            Runnable   r )
    {
        execAs( caller, null, EMPTY_GROUPS, r );
    }

    /**
     * Execute this action as the provided user.
     *
     * @param caller the caller, or null if anonymous
     * @param principal the principal of the caller, or null
     * @param callerGroups an array of groups the caller is part of
     * @param r the Runnable containing the action
     */
    public static void execAs(
            MeshObject caller,
            MeshObject principal,
            String []  callerGroups,
            Runnable   r )
    {
        execAs( new Who( caller, principal, callerGroups ), r );
    }

    /**
     * Execute this action as the provided user.
     *
     * @param who the caller
     * @param r the Runnable containing the action
     */
    public static void execAs(
            Who      who,
            Runnable r )
    {
        Who oldWho = getWho();
        try {
            setWho( who );

            r.run();

        } finally {
            setWho( oldWho );
        }
    }

    /**
     * Execute this action as the provided user.
     *
     * @param caller the caller, or null if anonymous
     * @param c the Callable containing the action
     * @param <T> the return type of the Callable
     * @return the result
     * @throws Exception something went wrong
     */
    public static <T> T execAs(
            MeshObject  caller,
            Callable<T> c )
        throws
            Exception
    {
        return execAs( new Who( caller, null, EMPTY_GROUPS ), c );
    }

    /**
     * Execute this action as the provided user.
     *
     * @param caller the account of the caller, or null if no account
     * @param principal the principal used by the caller, or null if anonymous
     * @param callerGroups an array of groups the caller is part of
     * @param c the Callable containing the action
     * @param <T> the return type of the Callable
     * @return the result
     * @throws Exception something went wrong
     */
    public static <T> T execAs(
            MeshObject  caller,
            MeshObject  principal,
            String []   callerGroups,
            Callable<T> c )
        throws
            Exception
    {
        return execAs( new Who( caller, principal, callerGroups ), c );
    }

    /**
     * Execute this action as the provided user.
     *
     * @param who the caller
     * @param c the Callable containing the action
     * @param <T> the return type of the Callable
     * @return the result
     * @throws Exception something went wrong
     */
    public static <T> T execAs(
            Who         who,
            Callable<T> c )
        throws
            Exception
    {
        Who oldWho = getWho();
        try {
            setWho( oldWho );

            return c.call();

        } finally {
            setWho( oldWho );
        }
    }

    /**
     * Execute this action as super user.
     *
     * @param r the Runnable containing the action
     */
    public static void suExec(
            Runnable r )
    {
        try {
            sudo();

            r.run();

        } finally {
            sudone();
        }
    }

    /**
     * Execute this action as super user.
     *
     * @param c the Callable containing the action
     * @param <T> the return type of the Callable
     * @return the result
     * @throws Exception something went wrong
     */
    public static <T> T suExec(
            Callable<T> c )
        throws
            Exception
    {
        try {
            sudo();

            return c.call();

        } finally {
            sudone();
        }
    }

    /**
     * The identities of the callers in the various threads.
     */
    protected static final HashMap<Thread,Who> theCallersOnThreads = new HashMap<>();

    /**
     * The threads that currently are su'd. The value of the HashMap counts the number of
     * su invocations on that Thread.
     */
    protected static final HashMap<Thread,Integer> theSuThreads = new HashMap<>();

    /**
     * For efficiency, use the same empty array every time.
     */
    public static final String [] EMPTY_GROUPS = new String[0];

    /**
     * Captures an Account, a Principal, and groups.
     */
    public static class Who {
        public Who(
                MeshObject caller,
                MeshObject principal )
        {
            theCaller    = caller;
            thePrincipal = principal;
            theGroups    = new String[0];
        }

        public Who(
                MeshObject caller,
                MeshObject principal,
                String []  groups )
        {
            theCaller    = caller;
            thePrincipal = principal;
            theGroups    = groups;
        }

        public MeshObject getCaller()
        {
            return theCaller;
        }

        public MeshObject getPrincipal()
        {
            return thePrincipal;
        }

        public String [] getGroups()
        {
            return theGroups;
        }

        /**
         * The caller.
         */
        protected MeshObject theCaller;

        /**
         * The principal.
         */
        protected MeshObject thePrincipal;

        /**
         * The Groups.
         */
        protected String [] theGroups;
    }
}
