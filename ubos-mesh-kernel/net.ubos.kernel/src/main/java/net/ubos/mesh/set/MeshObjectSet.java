//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.set.m.ImmutableMMeshObjectSetFactory;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.util.cursoriterator.CursorIterable;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.exception.NotSingleMemberException;

/**
  * An unordered collection of MeshObject without duplicates (set semantics).
  */
public interface MeshObjectSet
        extends
            CursorIterable<MeshObject>
{
    /**
     * Set a name for this MeshObjectSet. Its only purpose is to help in debugging.
     *
     * @param newValue the new value
     */
    public abstract void setDebugName(
            String newValue );

    /**
     * Obtain a name for this MeshObjectSet. Its only purpose is to help in debugging.
     *
     * @return the name
     */
    public abstract String getDebugName();

    /**
     * Obtain the MeshObjectSetFactory by which this MeshObjectSet was created.
     *
     * @return the MeshObjectSetFactory
     */
    public abstract MeshObjectSetFactory getFactory();

    /**
     * Shorthand to obtain the MeshBaseView in which all of the contained MeshObjects reside.
     *
     * @return the MeshBaseView, or null if the set is empty
     */
    public abstract MeshBaseView getMeshBaseView();

    /**
      * Obtain the MeshObjects contained in this set.
      *
      * @return the MeshObjects contained in this set
      */
    public abstract MeshObject[] getMeshObjects();

    /**
     * Get the Nth MeshObject contained in this set.
     * While the order of MeshObjects in a MeshObjectSet typically does not change,
     * no assumptions should be made about this. Compare
     * with {@link net.ubos.mesh.set.OrderedMeshObjectSet OrderedMeshObjectSet}.
     *
     * @param n the index of the MeshObject to be returned
     * @return the Nth MeshObject contained in this set.
     */
    public abstract MeshObject get(
            int n );

    /**
     * Assuming that this set contains no more than one object, obtain this one object. This
     * method is often very convenient if it is known to the application programmer that
     * a certain set may only contain one member. Invoking this method if the set has more
     * than one member will throw an IllegalStateException.
     *
     * @return the one element of the set, or null if the set is empty
     * @throws NotSingleMemberException thrown if the set contains more than one element
     */
    public abstract MeshObject getSingleMember()
        throws
            NotSingleMemberException;

    /**
     * Convenience method to return the content of this MeshObjectSet as an
     * array of the canonical Identifiers of the member MeshObjects.
     *
     * @return the array of IdentifierValues representing the IdentifierValues of the members
     *         of this MeshObjectSet
     */
    public abstract MeshObjectIdentifier[] asIdentifiers();

    /**
     * Obtain an Iterator iterating over the content of this set.
     *
     * @return an Iterator iterating over the content of this set
     */
    @Override
    public abstract CursorIterator<MeshObject> iterator();

    /**
     * Determine whether this set contains a certain MeshObject.
     *
     * @param testObject the MeshObject to look for
     * @return true if this set contains the given MeshObject
     * @throws WrongMeshBaseViewException thrown if the testObject is contained in a different MeshBase than the MeshObjects in this set
     */
    public abstract boolean contains(
            MeshObject testObject )
        throws
            WrongMeshBaseViewException;

    /**
     * Determine whether this set contains a MeshObject with this Identifier.
     *
     * @param identifier the Identifier of the MeshObject to look for
     * @return true if this set contains the given MeshObject
     */
    public abstract boolean contains(
            MeshObjectIdentifier identifier );

    /**
     * Determine whether this set contains all MeshObjects in a supposed subset.
     *
     * @param subset the supposed subset
     * @return true if this set contains all MeshObjects in the supposed subset
     * @throws WrongMeshBaseViewException thrown if a tested object is contained in a different MeshBase than the MeshObjects in this set
     */
    public abstract boolean containsAll(
            MeshObjectSet subset )
        throws
            WrongMeshBaseViewException;

    /**
     * Determine whether this set has the same content as another set.
     *
     * @param other the MeshObjectSet to compare to
     * @return true if it has the same content
     */
    public abstract boolean hasSameContent(
            MeshObjectSet other );

    /**
     * Convenience method to to easily find a member of this set by providing a
     * MeshObjectSelector that will select the MeshObject to be found. This method will return
     * the match and THEN STOP. If you expect more than one match, do not use this method.
     *
     * @param selector the criteria for selection
     * @return the first found MeshObject, or null if none
     */
    public abstract MeshObject find(
            MeshObjectSelector selector );

    /**
     * Create a subset of this set by providing a MeshObjectSelector that will select the MeshObjects
     * to be selected for the subset. This method will return all matches in this set.
     *
     * @param selector the criteria for selection
     * @return subset of this set
     */
    public abstract MeshObjectSet subset(
            MeshObjectSelector selector );

    /**
     * Create a new OrderedMeshObjectSet with the same content as this MeshObjectSet, but sorted
     * according to a MeshObjectSorter.
     *
     * @param sorter the MeshObjectSorter to use
     * @return the OrderedMeshObjectSet
     */
    public abstract OrderedMeshObjectSet ordered(
            MeshObjectSorter sorter );

    /**
     * Create a new MeshObjectSet with the same content as this MeshObjectSet, but with the
     * order reversed. This is applied to MeshObjectSet, not just OrderedMeshObjectSet, because in practice,
     * we iterate over either.
     *
     * @return the MeshObjectSet
     */
    public abstract MeshObjectSet inverse();

    /**
     * Determine whether this set is empty.
     *
     * @return true if this set is empty
     */
    public abstract boolean isEmpty();

    /**
     * Determine the number of members in this set.
     *
     * @return the number of members in this set
     */
    public abstract int size();

    /**
     * Determine the number of members in this set. Same as size(), for JavaBeans-aware software.
     *
     * @return the number of members in this set
     */
    public abstract int getSize();

    /**
     * Convenience method to intersection two MeshObjectSets using this MeshObjectSet's MeshObjectSetFactory.
     *
     * @param otherSet the MeshObjectSet to intersection this MeshObjectSet with
     * @return the intersection
     */
    public abstract MeshObjectSet intersection(
            MeshObjectSet otherSet );

    /**
     * Convenience method to union two MeshObjectSets using this MeshObjectSet's MeshObjectSetFactory.
     *
     * @param otherSet the MeshObjectSet to union this MeshObjectSet with
     * @return the intersection
     */
    public abstract MeshObjectSet union(
            MeshObjectSet otherSet );

    /**
     * Convenience method to remove the members of a MeshObjectSet from this MeshObjectSet.
     *
     * @param otherSet the MeshObjectSet whose members shall be removed from this MeshObjectSet
     * @return a new MeshObjectSet without the removed members
     */
    public abstract MeshObjectSet minus(
            MeshObjectSet otherSet );

    /**
      * Returns an MeshObjectSet which is the union of all MeshObjectSets obtained
      * by traversing this TraverseSpecification for each of the MeshObjects in this set. Note
      * that the semantics of MeshObjectSet do not allow duplicates and thus there
      * won't be any duplicates in this result. This is a convenience function.
      *
      * @param theTraverseSpecification specifies how to traverse
      * @return the set of MeshObjects obtained through the traversal
      */
    public abstract MeshObjectSet traverse(
            TraverseSpecification theTraverseSpecification );

    /**
     * Traverse to the neighbor MeshObjects of all the members of this set. This is
     * a convenience method.
     *
     * @return the set of neighbor MeshObjects
     */
    public abstract MeshObjectSet traverseToNeighbors();

    /**
     * Convert to a Java Set.
     *
     * @return the Set
     */
    public abstract Set<MeshObject> asSet();

    /**
     * Default empty set.
     */
    public static final MeshObjectSet DEFAULT_EMPTY_SET = ImmutableMMeshObjectSetFactory.SINGLETON.createImmutableMeshObjectSet( new MeshObject[0] );
}
