//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.ubos.mesh.a.AMeshObjectNeighborManager;
import net.ubos.mesh.security.PropertyReadOnlyException;
import net.ubos.mesh.security.ReadOnlyMeshBaseViewException;
import net.ubos.meshbase.AbstractMeshBaseView;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.security.NeedNeighborException;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.meshbase.transaction.MeshObjectAttributesAddChange;
import net.ubos.meshbase.transaction.MeshObjectAttributesRemoveChange;
import net.ubos.meshbase.transaction.MeshObjectPropertyChange;
import net.ubos.meshbase.transaction.MeshObjectBlessChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.PropertyTypeNotFoundException;
import net.ubos.util.ArrayHelper;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Log;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.util.StringHelper;

/**
 * This collects functionality that is probably useful to all implementations
 * of {@link MeshObject}. Its use by implementations, however, is optional.
 *
 * Implementation notes:
 * * Write operations require Transactions. Once we checked for the existence of a valid Transaction,
 *   we don't need to do anything to protect against concurrent write access.
 * * Read operations do not require Transactions. We do need to protect against a write operation
 *   and a (or several) read operations running concurrently,
 */
public abstract class AbstractMeshObject
        implements
            MeshObject,
            CanBeDumped
{
    static final Log log = Log.getLogInstance( AbstractMeshObject.class ); // our own, private logger

    /**
     * Constructor, for subclasses only.
     *
     * @param mbv the MeshBaseView that this MeshObject belongs to
     * @param state the MeshObject's State
     */
    public AbstractMeshObject(
            AbstractMeshBaseView mbv,
            State                state )
    {
        theMeshBaseView = mbv;

        theTimeCreated  = -1L; // updated at Transaction commit time
        theTimeUpdated  = -1L; // same

        theState = state;
    }

    /**
     * Let Transactions, and StoreMeshBases set the correct timeCreated.
     *
     * @param timeCreated the time created
     */
    public void setTimeCreated(
            long timeCreated )
    {
        theTimeCreated = timeCreated;
    }

    /**
     * Let Transactions, and StoreMeshBases set the correct timeUpdated.
     *
     * @param timeUpdated the updated
     */
    public void setTimeUpdated(
            long timeUpdated )
    {
        theTimeUpdated = timeUpdated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase getMeshBase()
    {
        return theMeshBaseView.getMeshBase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBaseView getMeshBaseView()
    {
        return theMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final long getTimeCreated()
    {
        return theTimeCreated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean getIsDead()
    {
        if( theState == State.DEAD ) {
            return true;
        }
        MeshBase mb = getMeshBaseView().getMeshBase(); // this allows us not to synchronize this method

        if( mb.isDead() ) {
            return true; // the whole MeshBase died
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkAlive()
        throws
            IsDeadException
    {
        if( getIsDead() ) {
            throw new IsDeadException( this );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getIsWriteable()
    {
        if( theMeshBaseView == null ) {
            return false;
        }
        return !theMeshBaseView.isFrozen();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkIsWriteable()
        throws
            ReadOnlyMeshBaseViewException
    {
        if( !getIsWriteable() ) {
            throw new ReadOnlyMeshBaseViewException( theMeshBaseView );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isHomeObject()
    {
        MeshObject home = theMeshBaseView.getHomeObject();
        boolean    ret  = home.getIdentifier().equals( getIdentifier() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasAttribute(
            String name )
    {
        if( name == null ) {
            throw new NullPointerException();
        }
        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, theAttributes, permissions
            checkAlive();

            if( theAttributes == null ) {
                return false;
            }

            try {
                getAccessManager().checkPermittedGetAttribute( this, name );

            } catch( NotPermittedException ex ) {
                return false;
            }

            return theAttributes.containsKey( name );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable getAttributeValue(
            String name )
        throws
            IllegalAttributeException,
            NotPermittedException
    {
        return getAttributeValues( new String[] { name } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable [] getAttributeValues(
            String [] names )
        throws
            IllegalAttributeException,
            NotPermittedException
    {
        if( names == null ) {
            throw new NullPointerException();
        }
        if( names.length == 0 ) {
            return new Serializable[0];
        }
        AccessManager accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, theAttributes, checkPermissions
            checkAlive();

            if( theAttributes == null ) {
                throw new IllegalAttributeException( this, names[0] );
            }

            for( int i=0 ; i<names.length ; ++i ) {
                if( names[i] == null ) {
                    throw new NullPointerException( "Null Attribute name" );
                }
                if( !theAttributes.containsKey( names[i] )) {
                    throw new IllegalAttributeException( this, names[i] );
                }

                accessMgr.checkPermittedGetAttribute( this, names[i] );
            }

            Serializable [] ret = new Serializable[ names.length ];
            for( int i=0 ; i<names.length ; ++i ) {
                ret[i] = theAttributes.get( names[i] );
            }
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttributeValue(
            String       name,
            Serializable newValue )
        throws
            NotPermittedException,
            TransactionException
    {
        setAttributeValues( new String[] { name }, new Serializable[] { newValue } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable [] setAttributeValues(
            String []       names,
            Serializable [] newValues )
        throws
            NotPermittedException,
            TransactionException
    {
        if( names == null || newValues == null ) {
            throw new NullPointerException();
        }

        if( names.length != newValues.length ) {
            throw new IllegalArgumentException( "Attribute names and Attribute values must have same length" );
        }

        if( names.length == 0 ) {
            return new Serializable[0]; // Exception instead?
        }

        checkTransaction();

        AccessManager   accessMgr  = getAccessManager();
        Serializable [] oldValues  = new Serializable[ names.length ];
        String       [] existing;
        String       [] added      = new String[ names.length ];
        int             addedCount = 0;

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous read access: alive, writeable, theAttributes, checkPermissions
            checkAlive();
            checkIsWriteable();

            for( int i=0 ; i<names.length ; ++i ) {
                if( names[i] == null ) {
                    throw new NullPointerException();
                }
                if( names[i].isEmpty() ) {
                    throw new IllegalArgumentException( "Attribute name must not be empty" );
                }

                if( theAttributes == null || !theAttributes.containsKey( names[i] )) {
                    accessMgr.checkPermittedCreateAttribute( this, names[i] );
                }
                accessMgr.checkPermittedSetAttribute( this, names[i], newValues[i] );
            }

            ensureAttributes();

            existing = new String[ theAttributes.size() ];
            theAttributes.keySet().toArray( existing );

            for( int i=0 ; i<names.length ; ++i ) {
                if( !theAttributes.containsKey( names[i] )) {
                    added[addedCount++] = names[i];
                }
                oldValues[i] = theAttributes.put( names[i], newValues[i] );
            }
        }

        if( addedCount > 0 ) {
            appendChangeToTransaction( new MeshObjectAttributesAddChange(
                    this,
                    existing,
                    added,
                    ArrayHelper.append( existing, added, String.class ) ));
        }

        for( int i=0 ; i<names.length ; ++i ) {
            if(    ( oldValues[i] == null && newValues[i] != null )
                || ( oldValues[i] != null && !oldValues[i].equals( newValues[i] )))
            {
                appendChangeToTransaction( new MeshObjectAttributeChange(
                        this,
                        names[i],
                        oldValues[i],
                        newValues[i] ));
            }
        }

        return oldValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttributeValues(
            Map<String,Serializable> newValues )
        throws
            NotPermittedException,
            TransactionException
    {
        if( newValues == null ) {
            throw new NullPointerException();
        }

        // FIXME Not a very smart implementation
        String  []      names  = new String[ newValues.size() ];
        Serializable [] values = new Serializable[ names.length ];

        int i=0;
        for( String currentName : newValues.keySet() ) {
            names[i]  = currentName;
            values[i] = newValues.get( currentName );
            ++i;
        }
        setAttributeValues( names, values );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAttribute(
            String name )
        throws
            IllegalAttributeException,
            NotPermittedException,
            TransactionException
    {
        deleteAttributes( new String[] { name } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAttributes(
            String [] names )
        throws
            IllegalAttributeException,
            NotPermittedException,
            TransactionException
    {
        if( names == null ) {
            throw new NullPointerException();
        }
        if( names.length == 0 ) {
            return; // Exception instead?
        }

        checkTransaction();

        AccessManager   accessMgr = getAccessManager();
        Serializable [] oldValues = new Serializable[ names.length ];
        String       [] newNames;

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, isWriteable, theAttributes, checkPermissions
            checkAlive();
            checkIsWriteable();

            for( String name : names ) {
                accessMgr.checkPermittedDeleteAttribute( this, name );
            }

            if( theAttributes == null ) {
                throw new IllegalAttributeException( this, names[0] );
            }
            for( int i=0 ; i<names.length ; ++i ) {
                String name = names[i];
                if( name.isEmpty() ) {
                    throw new IllegalArgumentException( "Attribute name must not be empty" );
                }
                if( !theAttributes.containsKey( name )) {
                    throw new IllegalAttributeException( this, name );
                }
            }
            for( int i=0 ; i<names.length ; ++i ) {
                String name = names[i];
                oldValues[i] = theAttributes.remove( name );
            }
            newNames = new String[ theAttributes.size() ];
            theAttributes.keySet().toArray( newNames );

            if( theAttributes.isEmpty() ) {
                theAttributes = null;
            }

        }
        for( int i=0 ; i<names.length ; ++i ) {
            if( oldValues[i] != null ) {
                appendChangeToTransaction( new MeshObjectAttributeChange(
                        this,
                        names[i],
                        oldValues[i],
                        null ));
            }
        }
        appendChangeToTransaction( new MeshObjectAttributesRemoveChange(
                this,
                ArrayHelper.append( names, newNames, String.class ),
                names,
                newNames ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getAttributeNames()
    {
        AccessManager accessMgr = getAccessManager();
        String []     ret;
        int           count;

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, theAttributes, checkPermissions
            checkAlive();

            if( theAttributes == null || theAttributes.isEmpty() ) {
                return StringHelper.EMPTY_ARRAY;
            }
            ret = new String[ theAttributes.size() ];

            count=0;
            for( String name : theAttributes.keySet() ) {
                try {
                    accessMgr.checkPermittedGetAttribute( this, name );

                    ret[count++] = name;

                } catch( NotPermittedException ex ) {
                    // no op
                }
            }
        }
        if( count < ret.length ) {
            ret = ArrayHelper.copyIntoNewArray( ret, 0, count, String.class );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getOrderedAttributeNames()
    {
        String [] ret = getAttributeNames();
        Arrays.sort( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Attribute [] getAttributes()
    {
        String []    attNames = getAttributeNames();
        Attribute [] ret      = new Attribute[ attNames.length ];

        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = new Attribute( this, attNames[i] );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unrelate(
            MeshObject neighbor )
        throws
            TransactionException,
            NotPermittedException
    {
        String [] roleAttributeNames = getRoleAttributeNames( neighbor );
        deleteRoleAttributes( neighbor, roleAttributeNames );

        RoleType [] roleTypes = getRoleTypes( neighbor );
        unblessRole( roleTypes, neighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasProperty(
            PropertyType pt )
    {
        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, theEntityTypes, checkPermissions
            checkAlive();

            try {
                getAccessManager().checkPermittedGetProperty( this, pt );

            } catch( NotPermittedException ex ) {
                return false;
            }

            EntityType requiredType = (EntityType) pt.getMeshTypeWithProperties();
            boolean    found        = false;

            if( theEntityTypes != null ) {
                for( MeshTypeWithProperties type : theEntityTypes ) {
                    if( type.equalsOrIsSupertype( requiredType )) {
                        found = true;
                        break;
                    }
                }
            }
            return found;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue getPropertyValue(
            PropertyType pt )
        throws
            IllegalPropertyTypeException,
            NotPermittedException
    {
        return getPropertyValues( new PropertyType[] { pt } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue [] getPropertyValues(
            PropertyType [] pts )
        throws
            IllegalPropertyTypeException,
            NotPermittedException
    {
        if( pts == null ) {
            throw new NullPointerException();
        }
        if( pts.length == 0 ) {
            return PropertyValue.EMPTY_ARRAY; // Exception instead?
        }

        for( int i=0 ; i<pts.length ; ++i ) {
            if( pts[i] == null ) {
                throw new NullPointerException( "Null PropertyType" );
            }
        }

        AccessManager accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, theEntityTypes, checkPermissions, theProperties
            checkAlive();

            for( int i=0 ; i<pts.length ; ++i ) {
                EntityType requiredType = (EntityType) pts[i].getMeshTypeWithProperties();
                boolean    found        = false;

                if( theEntityTypes != null ) {
                    for( MeshTypeWithProperties type : theEntityTypes ) {
                        if( type.equalsOrIsSupertype( requiredType )) {
                            found = true;
                            break;
                        }
                    }
                }
                if( !found ) {
                    throw new IllegalPropertyTypeException( this, pts[i] );
                }

                accessMgr.checkPermittedGetProperty( this, pts[i] );
            }

            PropertyValue [] ret = new PropertyValue[ pts.length ];
            if( theProperties != null ) {
                for( int i=0 ; i<pts.length ; ++i ) {
                    ret[i] = theProperties.get( pts[i] );
                }
            }
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue setPropertyValue(
            PropertyType  pt,
            PropertyValue newValue )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        return setPropertyValues( new PropertyType[] { pt }, new PropertyValue[] { newValue } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue [] setPropertyValues(
            PropertyType []  pts,
            PropertyValue [] newValues )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        if( pts.length != newValues.length ) {
            throw new IllegalArgumentException( "PropertyTypes and PropertyValues must have same length" );
        }

        for( int i=0 ; i<pts.length ; ++i ) {
            if( pts[i] == null ) {
                throw new NullPointerException();
            }
            if( pts[i].getIsReadOnly().value() ) {
                throw new PropertyReadOnlyException( this, pts[i] );
            }
            DataType type = pts[i].getDataType();
            if( newValues[i] != null ) {
                try {
                    if( type.conforms( newValues[i] ) != 0 ) {
                        throw new IllegalPropertyValueException( this, pts[i], newValues[i] );
                    }
                } catch( ClassCastException ex ) {
                    throw new IllegalPropertyValueException( this, pts[i], newValues[i] );
                }
            }
        }

        checkTransaction();

        AccessManager    accessMgr = getAccessManager();
        PropertyValue [] oldValues;

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, isWriteable, theEntityTypes, checkPermissions, theProperties
            checkAlive();
            checkIsWriteable();

            for( int i=0 ; i<pts.length ; ++i ) {
                EntityType requiredType = (EntityType) pts[i].getMeshTypeWithProperties();
                boolean    found        = false;

                if( theEntityTypes != null ) {
                    for( MeshTypeWithProperties amt : theEntityTypes ) {
                        if( amt.equalsOrIsSupertype( requiredType )) {
                            found = true;
                            break;
                        }
                    }
                }
                if( !found ) {
                    throw new IllegalPropertyTypeException( this, pts[i] );
                }
            }

            for( int i=0 ; i<pts.length ; ++i ) {
                accessMgr.checkPermittedSetProperty( this, pts[i], newValues[i] );
            }

            oldValues = new PropertyValue[ pts.length ];
            ensureProperties();

            for( int i=0 ; i<pts.length ; ++i ) {
                oldValues[i] = theProperties.put( pts[i], newValues[i] );
            }
        }

        for( int i=0 ; i<pts.length ; ++i ) {
            if( PropertyValue.compare( oldValues[i], newValues[i] ) != 0 ) {
                appendChangeToTransaction( new MeshObjectPropertyChange(
                        this,
                        pts[i],
                        oldValues[i],
                        newValues[i] ));
            }
        }

        return oldValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPropertyValues(
            Map<PropertyType,PropertyValue> newValues )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        // FIXME Not a very smart implementation
        PropertyType  [] types  = new PropertyType[ newValues.size() ];
        PropertyValue [] values = new PropertyValue[ types.length ];

        int i=0;
        for( PropertyType currentType : newValues.keySet() ) {
            types[i]  = currentType;
            values[i] = newValues.get( currentType );
            ++i;
        }
        setPropertyValues( types, values );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType [] getPropertyTypes()
    {
        ArrayList<PropertyType> almostRet = new ArrayList<>();
        AccessManager           accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: alive, theEntityTypes, checkPermissions
            checkAlive();

            // we cannot look at the properties table, because it may not contain keys for all of them
            if( theEntityTypes == null || theEntityTypes.isEmpty() ) {
                return new PropertyType[0];
            }
            for( EntityType type : theEntityTypes ) {
                PropertyType [] current = type.getPropertyTypes();
                for( int i=0 ; i<current.length ; ++i ) {
                    if( !almostRet.contains( current[i] )) {
                        try {
                            accessMgr.checkPermittedGetProperty( this, current[i] );
                            almostRet.add( current[i] );

                        } catch( NotPermittedException ex ) {
                            log.debug( ex );
                        }
                    }
                }
            }
        }
        PropertyType [] ret = ArrayHelper.copyIntoNewArray( almostRet, PropertyType.class );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType [] getOrderedPropertyTypes()
    {
        PropertyType [] ret = getPropertyTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Property [] getProperties()
    {
        PropertyType [] pts = getPropertyTypes();
        Property []     ret = new Property[ pts.length ];

        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = new Property( this, pts[i] );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType getPropertyTypeByName(
            String propertyName )
        throws
            MeshTypeNotFoundException
    {
        AccessManager accessMgr = getAccessManager();

        for( EntityType current : getEntityTypes() ) { // getEntityTypes() synchronizes and filters by permissions -- right
            PropertyType propertyType = current.findPropertyTypeByName( propertyName );
            if( propertyType != null ) {
                try {
                    accessMgr.checkPermittedGetProperty( this, propertyType );
                    return propertyType;

                } catch( NotPermittedException ex ) {
                    log.debug( ex );
                }
            }
        }
        throw new PropertyTypeNotFoundException( null, propertyName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable getRoleAttributeValue(
            MeshObject neighbor,
            String     name )
        throws
            IllegalRoleAttributeException,
            NotPermittedException
    {
        return getRoleAttributeValues(
                neighbor,
                new String[] { name } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable setRoleAttributeValue(
            MeshObject   neighbor,
            String       name,
            Serializable newValue )
        throws
            CannotRelateToItselfException,
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException
    {
        return setRoleAttributeValues(
                neighbor,
                new String []       { name },
                new Serializable [] { newValue } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setRoleAttributeValues(
            MeshObject               neighbor,
            Map<String,Serializable> newValues )
        throws
            CannotRelateToItselfException,
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException
    {
        String []       names  = new String[ newValues.size() ];
        Serializable [] values = new Serializable[ names.length ];

        int i=0;
        for( String currentName : newValues.keySet() ) {
            names[i]  = currentName;
            values[i] = newValues.get( currentName );
            ++i;
        }
        setRoleAttributeValues( neighbor, names, values );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable deleteRoleAttribute(
            MeshObject neighbor,
            String     name )
        throws
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException
    {
        return deleteRoleAttributes( neighbor, new String[] { name } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getOrderedRoleAttributeNames(
            MeshObject neighbor )
    {
        String [] ret = getRoleAttributeNames( neighbor );
        Arrays.sort( ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getOrderedRoleAttributeNames()
    {
        String [] ret = getRoleAttributeNames();
        Arrays.sort( ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleAttribute [] getRoleAttributes(
            MeshObject neighbor )
    {
        String        [] rans = getRoleAttributeNames( neighbor );
        RoleAttribute [] ret  = new RoleAttribute[ rans.length ];

        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = new RoleAttribute( this, neighbor, rans[i] );
        }
        return ret;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void bless(
            EntityType type )
        throws
            EntityBlessedAlreadyException,
            EntityTypeIsAbstractException,
            TransactionException,
            NotPermittedException
    {
        bless( new EntityType[] { type } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bless(
            EntityType [] types )
        throws
            EntityBlessedAlreadyException,
            EntityTypeIsAbstractException,
            TransactionException,
            NotPermittedException
    {
        for( int i=0 ; i<types.length ; ++i ) {
            if( types[i] == null ) {
                throw new IllegalArgumentException( "null EntityType at index " + i );
            }
            if( types[i].getIsAbstract().value() ) {
                throw new EntityTypeIsAbstractException( this, types[i] );
            }
        }

        checkTransaction();

        ArrayList<EntityType> oldTypes = new ArrayList<>();

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous read access: alive, isWriteable, permissions, theEntityTypes, theProperties

            checkAlive();
            checkIsWriteable();
            getAccessManager().checkPermittedBless( this, types );

            // throw Exception if the new type is already here, or a supertype of an existing one.
            // However, subtypes are allowed but need to be removed.
            if( theEntityTypes != null ) {
                EntityType toRemove = null;
                for( EntityType already : theEntityTypes ) {
                    oldTypes.add( already );
                    for( int i=0 ; i<types.length ; ++i ) {
                        if( already.isSubtypeOfOrEquals( types[i] )) {
                            throw new EntityBlessedAlreadyException( this, types[i] );
                        } else if( types[i].isSubtypeOfDoesNotEqual( already )) {
                            toRemove = already;
                        }
                    }
                }
                if( toRemove != null ) {
                    if( theEntityTypes.size() == 1 ) {
                        theEntityTypes = null;
                    } else {
                        theEntityTypes.remove( toRemove );
                    }
                }
            }

            ensureMeshTypes();

            for( int i=0 ; i<types.length ; ++i ) {
                if( !theEntityTypes.contains( types[i] )) {
                    theEntityTypes.add( types[i] );

                    // initialize default values
                    for( PropertyType pt : types[i].getPropertyTypes()) {
                        PropertyValue defaultValue = pt.getDefaultValue();
                        if( defaultValue != null ) {
                            ensureProperties();
                            theProperties.put( pt, defaultValue );
                        }
                    }
                }
            }
        }

        appendChangeToTransaction( new MeshObjectBlessChange(
                this,
                ArrayHelper.copyIntoNewArray( oldTypes, EntityType.class ),
                types,
                ArrayHelper.copyIntoNewArray( theEntityTypes.iterator(), EntityType.class )));

        // do not emit MeshObjectPropertyChange -- we only set them to defaults
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unbless(
            EntityType type )
        throws
            RoleTypeRequiresEntityTypeException,
            EntityNotBlessedException,
            TransactionException,
            NotPermittedException
    {
        unbless( new EntityType[] { type } );
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getEntityTypes()
    {
        AccessManager accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this ) ) { // protected against simultaneous read access: alive, theEntityTypes, permissions
            checkAlive();

            if( theEntityTypes != null ) {
                EntityType [] ret = new EntityType[ theEntityTypes.size() ];

                int index = 0;
                for( EntityType current : theEntityTypes ) {
                    try {
                        accessMgr.checkPermittedBlessedBy( this, current );
                        ret[ index++ ] = current;

                    } catch( NotPermittedException ex ) {
                        log.debug( ex );
                    }
                }
                if( index < ret.length ) {
                    ret = ArrayHelper.copyIntoNewArray( ret, 0, index, EntityType.class );
                }

                return ret;
            } else {
                return EntityType.EMPTY_ARRAY;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getOrderedEntityTypes()
    {
        EntityType [] ret = getEntityTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isBlessedBy(
            EntityType type )
    {
        return isBlessedBy( type, true );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBlessedBy(
            EntityType type,
            boolean    considerSubtypes )
    {
        synchronized( obtainSyncObject( this ) ) { // protected against simultaneous read access: alive, permissions, theEntityTypes
            checkAlive();

            try {
                getAccessManager().checkPermittedBlessedBy( this, type );

            } catch( NotPermittedException ex ) {
                log.debug( ex );
                // caller is not allowed to know
                return false;
            }

            if( theEntityTypes != null ) {
                if( considerSubtypes ) {
                    for( EntityType actualBlessed : theEntityTypes ) {
                        if( actualBlessed.isSubtypeOfOrEquals( type )) {
                            return true;
                        }
                    }
                } else {
                    for( EntityType actualBlessed : theEntityTypes ) {
                        if( actualBlessed.equals( type )) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] determineBlessedSubtypes(
            EntityType type )
        throws
            EntityNotBlessedException
    {
        int           count;
        EntityType [] found;
        AccessManager accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this ) ) { // protected against simultaneous read access: alive, permissions, theEntityTypes
            checkAlive();

            try {
                accessMgr.checkPermittedBlessedBy( this, type );

            } catch( NotPermittedException ex ) {
                log.debug( ex );
                // caller is not allowed to know
                return EntityType.EMPTY_ARRAY;
            }

            if( !isBlessedBy( type )) {
                throw new EntityNotBlessedException( this, type );
            }
            // This means theEntityTypes != null

            count = 0;
            found = new EntityType[ theEntityTypes.size() ];

            for( EntityType actualBlessed : theEntityTypes ) {
                if( actualBlessed.isSubtypeOfDoesNotEqual( type )) {
                    try {
                        accessMgr.checkPermittedBlessedBy( this, actualBlessed );
                        found[ count++ ] = actualBlessed;

                    } catch( NotPermittedException ex ) {
                        log.debug( ex );
                    }
                }
            }
        }
        if( count < found.length ) {
            found = ArrayHelper.copyIntoNewArray( found, 0, count, EntityType.class );
        }
        return found;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType determineSingleBlessedSubtype(
            EntityType type )
        throws
            EntityNotBlessedException,
            IllegalStateException
    {
        EntityType [] found = determineBlessedSubtypes( type );
        switch( found.length ) {
            case 0:
                return null;
            case 1:
                return found[0];
            default:
                throw new IllegalStateException( "More than one blessed subtype found: " + ArrayHelper.arrayToString( found ));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void blessRole(
            RoleType   thisEnd,
            MeshObject neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeBlessedAlreadyException,
            EntityNotBlessedException,
            RelationshipTypeIsAbstractException,
            TransactionException,
            NotPermittedException
    {
        blessRole( new RoleType[] { thisEnd }, neighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unblessRole(
            RoleType   thisEnd,
            MeshObject neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeNotBlessedException,
            TransactionException,
            NotPermittedException
    {
        unblessRole( new RoleType[] { thisEnd }, neighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRelated(
            RoleType   thisEnd,
            MeshObject neighbor )
        throws
            CannotRelateToItselfException
    {
        if( this == neighbor ) {
            throw new CannotRelateToItselfException( this );
        }
        RoleType [] allRoleTypes = getRoleTypes( neighbor );
        if( ArrayHelper.isIn( thisEnd, allRoleTypes, false )) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getOrderedRoleTypes()
    {
        RoleType [] ret = getRoleTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getOrderedRoleTypes(
            MeshObject neighbor )
    {
        RoleType [] ret = getRoleTypes( neighbor );
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue getRolePropertyValue(
            MeshObject   neighbor,
            PropertyType pt )
        throws
            IllegalRolePropertyTypeException,
            NotPermittedException
    {
        return getRolePropertyValues(
                neighbor,
                new PropertyType[] { pt } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue setRolePropertyValue(
            MeshObject    neighbor,
            PropertyType  pt,
            PropertyValue newValue )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        return setRolePropertyValues(
                neighbor,
                new PropertyType[]  { pt },
                new PropertyValue[] { newValue } )[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setRolePropertyValues(
            MeshObject                      neighbor,
            Map<PropertyType,PropertyValue> newValues )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException
    {
        // FIXME Not a very smart implementation
        PropertyType  [] types  = new PropertyType[ newValues.size() ];
        PropertyValue [] values = new PropertyValue[ types.length ];

        int i=0;
        for( PropertyType currentType : newValues.keySet() ) {
            types[i] = currentType;
            values[i] = newValues.get( currentType );
            ++i;
        }
        setRolePropertyValues( neighbor, types, values );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType [] getRolePropertyTypes(
            MeshObject neighbor )
    {
        ArrayList<PropertyType> almostRet;
        AccessManager           accessMgr = getAccessManager();

        synchronized( obtainSyncObject( this ) ) { // protect against simultaneous write access: getRoleTypes(), permissions
            // we cannot look at the properties table, because it may not contain keys for all of them

            RoleType [] types = getRoleTypes( neighbor ); // this will checkAlive()
            if( types == null || types.length == 0 ) {
                return PropertyType.EMPTY_ARRAY;
            }

            almostRet = new ArrayList<>();
            for( RoleType type : types ) {
                PropertyType [] current = type.getPropertyTypes();
                for( int i=0 ; i<current.length ; ++i ) {
                    if( !almostRet.contains( current[i] )) {
                        try {
                            accessMgr.checkPermittedGetRoleProperty( this, neighbor, current[i] );
                            almostRet.add( current[i] );

                        } catch( NotPermittedException ex ) {
                            log.debug( ex );
                        }
                    }
                }
            }
        }
        PropertyType [] ret = ArrayHelper.copyIntoNewArray( almostRet, PropertyType.class );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType getRolePropertyTypeByName(
            MeshObject neighbor,
            String     propertyName )
        throws
            MeshTypeNotFoundException
    {
        for( RoleType current : getRoleTypes( neighbor ) ) {
            PropertyType propertyType = current.findPropertyTypeByName( propertyName );
            if( propertyType != null ) {
                return propertyType;
            }
        }
        throw new PropertyTypeNotFoundException( null, propertyName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType [] getOrderedRolePropertyTypes(
            MeshObject neighbor )
    {
        PropertyType [] ret = getRolePropertyTypes( neighbor );
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType [] getOrderedRolePropertyTypes()
    {
        PropertyType [] ret = getRolePropertyTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleProperty [] getRoleProperties(
            MeshObject neighbor )
    {
        PropertyType [] pts = getRolePropertyTypes( neighbor );
        RoleProperty [] ret = new RoleProperty[ pts.length ];

        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = new RoleProperty( this, neighbor, pts[i] );
        }
        return ret;
    }

    /**
     * Add a Change to the ChangeSet of the current HeadTransaction.
     *
     * @param toAdd the Change to add
     */
    protected abstract void appendChangeToTransaction(
            Change toAdd );

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean equals(
            Object other )
    {
        if( other instanceof MeshObject ) {
            MeshObject realOther = (MeshObject) other;
            if( getIdentifier().equals( realOther.getIdentifier() )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int hashCode()
    {
        return getIdentifier().hashCode();
    }

    /**
     * Delete this MeshObject. This must only be invoked by our MeshObjectLifecycleManager
     * and thus is defined down here, not higher up in the inheritance hierarchy.
     *
     * @throws TransactionException thrown if invoked outside of proper Transaction boundaries
     */
    public abstract void delete()
        throws
            TransactionException;

    /**
     * Helper to check that we are within the proper Transaction boundaries.
     *
     * @throws TransactionException throw if this has been invoked outside of proper Transaction boundaries
     */
    public void checkTransaction()
        throws
            TransactionException
    {
        EditableMeshBaseView mbv = (EditableMeshBaseView) getMeshBaseView();
        mbv.checkTransaction();
    }

    /**
     * Convenience method to find the AccessManager.
     *
     * @return the AccessManager, or null
     */
    protected AccessManager getAccessManager()
    {
        AccessManager ret = getMeshBaseView().getMeshBase().getAccessManager();
        return ret;
    }

    /**
     * Internal helper to make sure the theEntityTypes property exists.
     */
    protected void ensureMeshTypes()
    {
        if( theEntityTypes == null ) {
            theEntityTypes = new HashSet<>();
        }
    }

    /**
     * Internal helper to make sure the theProperties property exists.
     */
    protected void ensureProperties()
    {
        if( theProperties == null ) {
            theProperties = new PropertiesMap();
        }
    }

    /**
     * Internal helper to make sure the theAttributes property exists.
     */
    protected void ensureAttributes()
    {
        if( theAttributes == null ) {
            theAttributes = new AttributesMap();
        }
    }

    /**
     * This is invoked by the Transaction that has just fixed the timeCreated timestamp of a MeshObject
     * that was both 1) created during the transaction, and 2) became one of our neighbors. For those
     * implementations of neighbor management that order by timeCreated, this means the timeCreated now
     * needs to updated / the order of neighbors be reshuffled.
     *
     * @param neighborUpdated the neighbor whose timeCreated was updated
     */
    public abstract void neighborTimeCreatedWasUpdated(
            AbstractMeshObject neighborUpdated );

    /**
     * Obtain an object on which to synchronize operations on this MeshObject.
     *
     * @param obj the MeshObject
     * @return the sync object
     */
    protected abstract Object obtainSyncObject(
            MeshObject obj );

    /**
     * Obtain an object on which to synchronize operations on this MeshObject and on this neighbor MeshObject.
     *
     * @param obj the MeshObject
     * @param neighbor the neighbor Meshobject
     * @return the sync object
     */
    protected abstract Object obtainSyncObject(
            MeshObject obj,
            MeshObject neighbor );

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return super.toString() + "{ id: " + getIdentifier() + " }";
    }

    /**
      * The MeshBaseView in which this MeshObject lives. If this is null, the MeshObject
      * is dead.
      */
    protected AbstractMeshBaseView theMeshBaseView;

    /**
     * The time this MeshObject was created.
     * This is a long in System.currentTimeMillis() format.
     */
    protected long theTimeCreated;

    /**
     * The time this MeshObject was last updated.
     * This is a long in System.currentTimeMillis() format.
     */
    protected long theTimeUpdated;

    /**
     * State of this MeshObject
     */
    protected State theState;

    /**
     * The set of Properties with their types and values. This is allocated as needed.
     */
    protected PropertiesMap theProperties;

    /**
     * The set of EntityTypes of this MeshObject
     */
    protected HashSet<EntityType> theEntityTypes;

    /**
     * The set of Attributes with their names and values. This is allocated as needed.
     */
    protected AttributesMap theAttributes;

    /**
     * State of the MeshObject
     */
    public static enum State {
        ALIVE,
        DEAD
    };
}
