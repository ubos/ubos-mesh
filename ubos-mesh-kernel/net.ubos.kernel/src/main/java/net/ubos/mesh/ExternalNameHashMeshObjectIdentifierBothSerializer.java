//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;

/**
 * Knows how to serialize and deserialize MeshObjectIdentifiers in the hash notation.
 * This uses the preferred external name for the MeshObjectIdentifierNamespace.
 */
public class ExternalNameHashMeshObjectIdentifierBothSerializer
    extends
        AbstractHashMeshObjectIdentifierBothSerializer
{
    /**
     * Factory method.
     *
     * @param nsMap find namespaces here
     * @param idFact use this factory for MeshObjectIdentifiers
     * @return the created object
     */
    public static ExternalNameHashMeshObjectIdentifierBothSerializer create(
            PrimaryMeshObjectIdentifierNamespaceMap nsMap,
            MeshObjectIdentifierFactory             idFact )
    {
        return new ExternalNameHashMeshObjectIdentifierBothSerializer( nsMap, idFact );
    }

    /**
     * Constructor.
     *
     * @param nsMap find namespaces here
     * @param idFact use this factory for MeshObjectIdentifiers
     */
    protected ExternalNameHashMeshObjectIdentifierBothSerializer(
            PrimaryMeshObjectIdentifierNamespaceMap nsMap,
            MeshObjectIdentifierFactory             idFact )
    {
        super( idFact );
        theNsMap = nsMap;
    }

     /**
     * {@inheritDoc}
     */
    @Override
    protected String determineNamespaceString(
            MeshObjectIdentifierNamespace ns )
    {
        return ns.getPreferredExternalName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MeshObjectIdentifier createFor(
            MeshObjectIdentifier contextIdentifier,
            String               nsName,
            String               localId )
    {
        MeshObjectIdentifier ret;

        if( nsName == null ) {
            if( contextIdentifier == null ) {
                ret = theIdFact.createMeshObjectIdentifierIn( theNsMap.getPrimary(), localId );
            } else {
                ret = theIdFact.createMeshObjectIdentifierIn( contextIdentifier.getNamespace(), localId );
            }

        } else {
            MeshObjectIdentifierNamespace ns = theNsMap.obtainByExternalName( nsName );

            ret = theIdFact.createMeshObjectIdentifierIn( ns, localId );
        }

        return ret;
    }

    /**
     * Find namespaces here.
     */
    protected final PrimaryMeshObjectIdentifierNamespaceMap theNsMap;
}
