//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

/**
 * Marker interface for MeshObjectSets that are immutable.
 */
public interface ImmutableMeshObjectSet
        extends
            MeshObjectSet
{
    // nothing
}
