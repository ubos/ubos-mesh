//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Default implementation of MeshObjectIdentifierNamespace.
 */
public class DefaultMeshObjectIdentifierNamespace
    implements
        MeshObjectIdentifierNamespace
{
    /**
     * Constructor.
     *
     * @param map the AbstractMeshObjectIdentifierNamespace this MeshObjectIdentifierNamespace belongs to
     */
    public DefaultMeshObjectIdentifierNamespace(
            AbstractPrimaryMeshObjectIdentifierNamespaceMap map )
    {
        theMap                   = map;
        thePreferredExternalName = null;
        theExternalNames         = new HashSet<>();
    }

    /**
     * Constructor for instances restored from disk.
     *
     * @param preferredName the preferred name of the MeshObjectIdentifierNamespace -- no need to tell the map
     * @param externalNames the external names of the MeshObjectIdentifierNamespace -- no need to tell the map
     * @param map the DefaultMeshObjectIdentifierNamespace this MeshObjectIdentifierNamespace belongs to
     */
    public DefaultMeshObjectIdentifierNamespace(
            String                                          preferredName,
            Set<String>                                     externalNames,
            AbstractPrimaryMeshObjectIdentifierNamespaceMap map )
    {
        theMap                   = map;
        thePreferredExternalName = preferredName;
        theExternalNames         = new HashSet<>();

        theExternalNames.addAll( externalNames );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getExternalNames()
    {
        return Collections.unmodifiableSet( theExternalNames );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPreferredExternalName()
    {
        return thePreferredExternalName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPreferredExternalName(
            String externalName )
    {
        // let the map manage this
        theMap.setPreferredExternalName( this, externalName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addExternalName(
            String externalName )
    {
        // let the map manage this
        theMap.addExternalName( this, externalName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeExternalName(
            String externalName )
    {
        // let the map manage this
        theMap.removeExternalName( this, externalName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(
            MeshObjectIdentifierNamespace other )
    {
        if( thePreferredExternalName == null ) {
            if( other.getPreferredExternalName() == null ) {
                return hashCode() - other.hashCode();
            } else {
                return -1;
            }
        }
        if( other.getPreferredExternalName() == null ) {
            return 1;
        }
        return thePreferredExternalName.compareTo( other.getPreferredExternalName() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            MeshObjectIdentifierNamespace o )
    {
        int ret;
        if( thePreferredExternalName == null ) {
            if( o.getPreferredExternalName() == null ) {
                ret = 0;
            } else {
                ret = -1;
            }
        } else if( o.getPreferredExternalName() == null ) {
            ret = 1;
        } else {
            ret = thePreferredExternalName.compareTo( o.getPreferredExternalName() );
        }

        if( ret == 0 ) {
            Set<String> otherExternalNames = o.getExternalNames();
            if( theExternalNames.isEmpty() ) {
                if( otherExternalNames.isEmpty() ) {
                    ret = 0;
                } else {
                    ret = -1;
                }
            } else if( otherExternalNames.isEmpty() ) {
                ret = 1;
            } else {
                ArrayList<String> sortedExternalNames = new ArrayList<>( theExternalNames );
                Collections.sort( sortedExternalNames );
                ArrayList<String> sortedOtherExternalNames = new ArrayList<>( otherExternalNames );
                Collections.sort( sortedOtherExternalNames );

                int max = Math.min( sortedExternalNames.size(), sortedOtherExternalNames.size() );
                for( int i=0 ; i<max ; ++i ) {
                    ret = sortedExternalNames.get( i ).compareTo( sortedOtherExternalNames.get( i ));
                    if( ret != 0 ) {
                        break;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        if( thePreferredExternalName != null ) {
            return thePreferredExternalName;
        }
        switch( theExternalNames.size() ) {
            case 0:
                return "unnamed";

            case 1:
                return theExternalNames.iterator().next();

            default:
                return theExternalNames.iterator().next() + "(et al)";
        }
    }

    /**
     * The preferred external name.
     */
    protected String thePreferredExternalName;

    /**
     * The set of current external names. This contains thePreferredExternalName.
     */
    protected final Set<String> theExternalNames;

    /**
     * The DefaultMeshObjectIdentifierNamespace this MeshObjectIdentifierNamespace belongs to
     */
    protected final AbstractPrimaryMeshObjectIdentifierNamespaceMap theMap;
}
