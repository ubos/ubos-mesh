//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.io.Serializable;
import java.util.Map;
import java.util.function.BiFunction;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.security.ReadOnlyMeshBaseViewException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.util.history.HasTimeUpdated;
import net.ubos.util.livedead.IsDeadException;

/**
 * <p>The core abstraction for the UBOS Mesh.
 *
 * <p>Each MeshObject is managed by exactly one {@link net.ubos.meshbase.MeshBase}.
 *
 * <p>Each MeshObject is identified by a {@link MeshObjectIdentifier}, which constitutes the globally unique
 * identity of the MeshObject. (There may be different instances of MeshObject with the same MeshObjectIdentifier,
 * e.g, because the MeshObjects are contained in different MeshBases, or represent the same MeshObject at different
 * points in history.)
 *
 * <p>Each MeshObject is contained in a MeshBase of its MeshBase, which is either identical to the MeshBase itself
 * (in which case the MeshObject is the HEAD version), or a different MeshBaseView (in which case the MeshObject
 * represents an older version).
 *
 * <p>Each MeshObject may be blessed by one or more {@link net.ubos.model.primitives.EntityType}s,
 * which may define properties for the MeshObject (by means of
 * {@link net.ubos.model.primitives.PropertyType}).
 *
 * <p>Each MeshObject may carry any number of named Attributes with values, which are untyped.
 *
 * <p>Each MeshObject can stand on its own or be related to one or more others. A relationship
 * is established either by blessing an ordered pair of MeshObjects with a RoleType, or by setting
 * at least one RoleAttribute. RoleTypes may define PropertyTypes, which can be set on the
 * respective end of a relationship between two MeshObjects.
 *
 * <p>Properties and Attributes of MeshObjects may be read, and relationships may be traversed outside
 * of {@link net.ubos.meshbase.transaction.Transaction} boundaries; for updates, Transactions
 * are needed.
 *
 * <p>A caller may or may not have the appropriate authorization to perform any of the
 * operations. If it does not, such an operation may throw NotPermittedException.
 * Other operations may silently return only a subset of the information they could return
 * to some callers. The interface does not make any statements on this level about
 * the exact behavior, other than stating that the experienced behavior may be different
 * for different callers.
 */
public interface MeshObject
    extends
        HasTimeUpdated
{
    /**
     * Obtain the globally unique identifier of this MeshObject.
     *
     * @return the globally unique identifier of this MeshObject
     */
    public abstract MeshObjectIdentifier getIdentifier();

    /**
     * Obtain the MeshBase that contains this MeshObject.
     *
     * @return the MeshBase that contains this MeshObject, or null.
     */
    public abstract MeshBase getMeshBase();

    /**
     * Obtain the MeshBaseView that contains this MeshObject. This is immutable for the
     * lifetime of this instance.
     *
     * @return the MeshBaseView that contains this MeshObject
     */
    public abstract MeshBaseView getMeshBaseView();

    /**
     * Obtain the time of creation of this MeshObject. This is immutable for the
     * lifetime of the MeshObject.
     *
     * @return the time this MeshObject was created in <code>System.currentTimeMillis()</code> format
     */
    public abstract long getTimeCreated();

    /**
     * Obtain the time of last update of this MeshObject. This changes automatically
     * every time the MeshObject is changed.
     *
     * @return the time this MeshObject was last updated in <code>System.currentTimeMillis()</code> format
     */
    @Override
    public abstract long getTimeUpdated();

    /**
     * Determine whether this MeshObject is dead and should not be used any further.
     *
     * @return true if the MeshObject is dead
     */
    public abstract boolean getIsDead();

    /**
     * Throw an IsDeadException if this MeshObject is dead and should not be used any further.
     * Does nothing as long as this MeshObject is alive.
     *
     * @throws IsDeadException thrown if this MeshObject is dead already
     */
    public abstract void checkAlive()
        throws
            IsDeadException;

    /**
     * Determine whether this MeshObject can, in principle, be updated. This does not
     * make any assertions about whether the caller has permissions to do so.
     *
     * @return true if it can be updated
     */
    public abstract boolean getIsWriteable();

    /**
     * Throw a ReadOnlyMeshBaseViewRuntimeException if this MeshObject cannot be updated.
     * Does nothing otherwise.
     *
     * @throws ReadOnlyMeshBaseViewException if this MeshObject cannot be updated
     */
    public abstract void checkIsWriteable()
        throws
            ReadOnlyMeshBaseViewException;

    /**
     * Determine whether this MeshObject is the home object of its MeshBase.
     *
     * @return true if it is the home object
     */
    public abstract boolean isHomeObject();

// -- Attributes --

    /**
     * Determine whether this MeshObject carries an Attribute with this name.
     *
     * @param name the name of the Attribute
     * @return true if this MeshObject carries this Attribute.
     */
    public abstract boolean hasAttribute(
            String name );

    /**
     * Get the value of an Attribute on this MeshObject.
     *
     * @param name the name of the Attribute
     * @return the value of the Attribute
     * @throws IllegalAttributeException thrown if the Attribute does not exist on this MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #hasAttribute(java.lang.String)
     * @see #setAttributeValue(java.lang.String, java.io.Serializable)
     */
    public abstract Serializable getAttributeValue(
            String name )
        throws
            IllegalAttributeException,
            NotPermittedException;

    /**
     * Convenience method to access all values for the Attributes with the provided names, in the same sequence
     * as the provided names. If an Attribute does not exist on this MeshObject, or if access to one of the
     * Attributes is not permitted, this will throw an exception.
     *
     * @param names the names of the Attributes
     * @return the values of the Attributes, in the same sequence as the names
     * @throws IllegalAttributeException thrown if at least one of the Attributes does not exist on this MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #getAttributeValue(java.lang.String)
     * @see #setAttributeValues(java.lang.String[], java.io.Serializable[])
     */
    public abstract Serializable [] getAttributeValues(
            String [] names )
        throws
            IllegalAttributeException,
            NotPermittedException;

    /**
     * Set the value of an Attribute on this MeshObject. This will create the Attribute if it does not exist yet.
     *
     * @param name the name of the Attribute
     * @param newValue the new value of the Attribute
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #getAttributeValue(java.lang.String)
     */
    public abstract void setAttributeValue(
            String       name,
            Serializable newValue )
        throws
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the value of several Attributes at the same time. The names of the Attributes
     * and their new values are given in the same sequence. This method sets either all values, or none.
     *
     * @param names the names of the Attributes
     * @param newValues the new values of the Attributes
     * @return old values of the Attributes
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #setAttributeValue(java.lang.String, java.io.Serializable)
     * @see #setAttributeValues(java.util.Map)
     * @see #getAttributeValues(java.lang.String[])
     */
    public abstract Serializable [] setAttributeValues(
            String []       names,
            Serializable [] newValues )
        throws
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the value of several Attributes at the same time. The names of the Attributes
     * and their new values are given as pairs in the Map. This method sets either all values, or none.
     *
     * @param newValues Map of Attribute name to Attribute value
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #setAttributeValue(java.lang.String, java.io.Serializable)
     * @see #setAttributeValues(java.lang.String[], java.io.Serializable[])
     * @see #getAttributeValues(java.lang.String[])
     */
    public abstract void setAttributeValues(
            Map<String,Serializable> newValues )
        throws
            NotPermittedException,
            TransactionException;

    /**
     * Remove an Attribute from this MeshObject.
     *
     * @param name the name of the Attribute
     * @throws IllegalAttributeException thrown if the Attribute does not exist on this MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #hasAttribute(java.lang.String)
     * @see #setAttributeValue(java.lang.String, java.io.Serializable)
     * @see #getAttributeValue(java.lang.String)
     * @see #getAttributeNames()
     * @see #deleteAttributes(java.lang.String[])
     */
    public abstract void deleteAttribute(
            String name )
        throws
            IllegalAttributeException,
            NotPermittedException,
            TransactionException;

    /**
     * Remove several Attributes from this MeshObject at the same time. Either all the Attributes will be removed
     * or none.
     *
     * @param names the names of the Attributes
     * @throws IllegalAttributeException thrown if an Attribute does not exist on this MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #hasAttribute(java.lang.String)
     * @see #setAttributeValue(java.lang.String, java.io.Serializable)
     * @see #getAttributeValue(java.lang.String)
     * @see #getAttributeNames()
     * @see #deleteAttribute(java.lang.String)
     */
    public abstract void deleteAttributes(
            String [] names )
        throws
            IllegalAttributeException,
            NotPermittedException,
            TransactionException;

    /**
     * Get the names of all the Attributes on this MeshObject.
     *
     * @return the set of all names of all Attributes
     */
    public abstract String [] getAttributeNames();

    /**
     * Get the names of all the Attributes on this MeshObject,
     * consistently ordered by name.
     *
     * @return the set of all names of all Attributes
     */
    public abstract String [] getOrderedAttributeNames();

    /**
     * Get all names and values of Attributes on this MeshObject
     *
     * @return the set of all Attributes on this MeshObject
     */
    public abstract Attribute [] getAttributes();

// -- Properties --

    /**
     * Determine whether this MeshObject carries this PropertyType.
     * This is a convenience method because this could also be determined by looking at the EntityTypes.
     *
     * @param pt the PropertyType
     * @return true of this MeshObject carries this PropertyType
     */
    public abstract boolean hasProperty(
            PropertyType pt );

    /**
     * Obtain the value of a Property.
     *
     * @param pt the PropertyType that identifies the correct Property of this MeshObject
     * @return the current value of the Property
     * @throws IllegalPropertyTypeException thrown if the PropertyType does not exist on this MeshObject
     *         because the MeshObject has not been blessed with a MeshType that provides this PropertyType
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #setPropertyValue
     */
    public abstract PropertyValue getPropertyValue(
            PropertyType pt )
        throws
            IllegalPropertyTypeException,
            NotPermittedException;

    /**
     * Obtain all PropertyValues for the PropertyTypes provided, in the same sequence as the provided
     * PropertyTypes. If a PropertyType does not exist on this MeshObject, or if access to one of the
     * PropertyTypes is not permitted, this will throw an exception. This is a convenience method.
     *
     * @param pts the PropertyTypes
     * @return the PropertyValues, in the same sequence as PropertyTypes
     * @throws IllegalPropertyTypeException thrown if one PropertyType does not exist on this MeshObject
     *         because the MeshObject has not been blessed with a MeshType that provides this PropertyType
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract PropertyValue [] getPropertyValues(
            PropertyType [] pts )
        throws
            IllegalPropertyTypeException,
            NotPermittedException;

    /**
     * Set the value of a Property.
     *
     * @param pt the PropertyType that identifies the correct Property of this MeshObject
     * @param newValue the new value of the Property
     * @return old value of the Property
     * @throws IllegalPropertyTypeException thrown if the PropertyType does not exist on this MeshObject
     *         because the MeshObject has not been blessed with a MeshType that provides this PropertyType
     * @throws IllegalPropertyValueException thrown if the new value is an illegal value for this Property
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #getPropertyValue
     */
    public abstract PropertyValue setPropertyValue(
            PropertyType  pt,
            PropertyValue newValue )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the value of several Properties at the same time. The PropertyTypes identifying the
     * Properties and their new PropertyValues are given in the same sequence. This method sets either all values, or none.
     *
     * @param pts the PropertyTypes that identify the correct Properties of this MeshObject
     * @param pvs the new values of the Properties
     * @return old values of the Properties
     * @throws IllegalPropertyTypeException thrown if one PropertyType does not exist on this MeshObject
     *         because the MeshObject has not been blessed with a MeshType that provides this PropertyType
     * @throws IllegalPropertyValueException thrown if the new value is an illegal value for this Property
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     */
    public abstract PropertyValue [] setPropertyValues(
            PropertyType []  pts,
            PropertyValue [] pvs )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the value of several Properties at the same time. The PropertyTypes identifying the
     * Properties and their new PropertyValues are given as pairs in the Map. This method sets either all values, or none.

     * @param newValues Map of PropertyType to PropertyValue
     * @throws IllegalPropertyTypeException thrown if one PropertyType does not exist on this MeshObject
     *         because the MeshObject has not been blessed with a MeshType that provides this PropertyType
     * @throws IllegalPropertyValueException thrown if the new value is an illegal value for this Property
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     */
    public abstract void setPropertyValues(
            Map<PropertyType,PropertyValue> newValues )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException;

    /**
     * Obtain the set of all PropertyTypes currently available on this MeshObject. This is a convenience method
     * because this could also be determined by looking at the MeshObject's EntityTypes and the PropertyTypes
     * they carry.
     *
     * @return the set of all PropertyTypes
     */
    public abstract PropertyType [] getPropertyTypes();

    /**
     * Obtain the set of all PropertyTypes currently available on this MeshObject,
     * consistently ordered by their UserVisibleName.
     *
     * @return the set of all PropertyTypes
     */
    public abstract PropertyType [] getOrderedPropertyTypes();

    /**
     * <p>This is a convenience method to obtain a PropertyType of this MeshObject by providing its
     * (short) name. If such a PropertyType could be found, this call returns it. If not,
     * it throws an MeshTypeNotFoundException.
     *
     * <p>Warning: sometimes (rarely), a MeshObject may carry two Properties with the same (short) name.
     * It is undefined which of those PropertyTypes this call will return.
     *
     * @param propertyName the name of the property
     * @return the PropertyType
     * @throws MeshTypeNotFoundException thrown if a Property by this name could not be found
     * @see #setPropertyValue
     */
    public abstract PropertyType getPropertyTypeByName(
            String propertyName )
        throws
            MeshTypeNotFoundException;

    /**
     * Get all types and values of Properties on this MeshObject
     *
     * @return the set of all Properties on this MeshObject
     */
    public abstract Property [] getProperties();

// -- RoleAttributes --

    /**
     * Determine whether this MeshObject's side of a relationship with this neighbor MeshObject carries this RoleAttribute.
     *
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @return true if this site of the relationship carries this RoleAttribute.
     */
    public abstract boolean hasRoleAttribute(
            MeshObject neighbor,
            String     name );

    /**
     * Get the value of a RoleAttribute on this MeshObject's side of a relationship with this neighbor MeshObject.
     *
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @return the value of the RoleAttribute
     * @throws IllegalRoleAttributeException thrown if the RoleAttribute does not exist on this MeshObject's
     *         side with the neighbor MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #setRoleAttributeValue
     */
    public abstract Serializable getRoleAttributeValue(
            MeshObject neighbor,
            String     name )
        throws
            IllegalRoleAttributeException,
            NotPermittedException;

    /**
     * Get the values of several RoleAttributes on this MeshObject's side of a relationship with this neighbor MeshObject
     * at the same time.
     *
     * @param neighbor the neighbor MeshObject
     * @param names the names of the RoleAttributes
     * @return the value of the RoleAttribute
     * @throws IllegalRoleAttributeException thrown if the RoleAttribute does not exist on this MeshObject's
     *         side with the neighbor MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #setRoleAttributeValues
     */
    public abstract Serializable [] getRoleAttributeValues(
            MeshObject neighbor,
            String []  names )
        throws
            IllegalRoleAttributeException,
            NotPermittedException;

    /**
     * Set the value of a RoleAttribute on this MeshObject's side of a relationship with this neighbor MeshObject.
     *
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @param newValue the new value of the RoleAttribute
     * @return the old value of the RoleAttribute
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws IllegalRoleAttributeException thrown if the RoleAttribute does not exist on this MeshObject's
     *         side with the neighbor MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #getAttributeValue
     */
    public abstract Serializable setRoleAttributeValue(
            MeshObject   neighbor,
            String       name,
            Serializable newValue )
        throws
            CannotRelateToItselfException,
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the value of several RoleAttribute on this MeshObject's side of a relationship with
     * this neighbor MeshObject at the same time. This does not throw IllegalRoleAttributeRuntimeException as it
     * automatically creates new RoleAttributes that do not exist yet.
     *
     * @param neighbor the neighbor MeshObject
     * @param names the names of the RoleAttribute
     * @param newValues the new values of the RoleAttribute, in the same sequence as the names
     * @return the old values of the RoleAttributes
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #getRoleAttributeValues(net.ubos.mesh.MeshObject, java.lang.String[])
     * @see #setRoleAttributeValues(net.ubos.mesh.MeshObject, java.util.Map)
     */
    public abstract Serializable [] setRoleAttributeValues(
            MeshObject      neighbor,
            String []       names,
            Serializable [] newValues )
        throws
            CannotRelateToItselfException,
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the value of several RoleAttribute on this MeshObject's side of a relationship with
     * this neighbor MeshObject at the same time.
     *
     * @param neighbor the neighbor MeshObject
     * @param newValues the new values of the RoleAttributes, keyed by the name of the RoleAttribute
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws IllegalRoleAttributeException thrown if the RoleAttribute does not exist on this MeshObject's
     *         side with the neighbor MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #getRoleAttributeValues(net.ubos.mesh.MeshObject, java.lang.String[])
     * @see #setRoleAttributeValues(net.ubos.mesh.MeshObject, java.lang.String[], java.io.Serializable[])
     */
    public abstract void setRoleAttributeValues(
            MeshObject               neighbor,
            Map<String,Serializable> newValues )
        throws
            CannotRelateToItselfException,
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException;

    /**
     * Remove an RoleAttribute from this MeshObject's side of a relationship with this neighbor MeshObject.
     *
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @return the old value
     * @throws IllegalRoleAttributeException thrown if the RoleAttribute does not exist on this MeshObject's
     *         side with the neighbor MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     */
    public abstract Serializable deleteRoleAttribute(
            MeshObject neighbor,
            String     name )
        throws
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException;

    /**
     * Remove several RoleAttributes from this MeshObject's side of a relationship with this neighbor MeshObject.
     * Either all RoleAttributes will be removed, or none.
     *
     * @param neighbor the neighbor MeshObject
     * @param names the names of the RoleAttributes
     * @return the old values
     * @throws IllegalRoleAttributeException thrown if the RoleAttribute does not exist on this MeshObject's
     *         side with the neighbor MeshObject
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     */
    public abstract Serializable [] deleteRoleAttributes(
            MeshObject neighbor,
            String []  names )
        throws
            IllegalRoleAttributeException,
            NotPermittedException,
            TransactionException;

    /**
     * Get the names of all the RoleAttributes on this MeshObject's side of a relationship with this neighbor MeshObject.
     *
     * @param neighbor the neighbor MeshObject
     * @return the set of all names of all RoleAttributes
     */
    public abstract String [] getRoleAttributeNames(
            MeshObject neighbor );

    /**
     * Get the names of all the RoleAttributes on this MeshObject's side of a relationship with any of its neighbor MeshObjects.
     *
     * @return the set of all names of all RoleAttributes
     */
    public abstract String [] getRoleAttributeNames();

    /**
     * Get the names of all the RoleAttributes on this MeshObject's side of a relationship with this neighbor MeshObject,
     * consistently ordered by their names.
     *
     * @param neighbor the neighbor MeshObject
     * @return the set of all names of all RoleAttributes
     */
    public abstract String [] getOrderedRoleAttributeNames(
            MeshObject neighbor );

    /**
     * Get the names of all the RoleAttributes on this MeshObject's side of a relationship with any of its neighbor MeshObjects.
     * consistently ordered by their names.
     *
     * @return the set of all names of all RoleAttributes
     */
    public abstract String [] getOrderedRoleAttributeNames();

    /**
     * Get all names and values of RoleAttributes on this MeshObject's side of a relationship with this neighbor MeshObject.
     *
     * @param neighbor the neighbor MeshObject
     * @return the set of all RoleAttributes on this relationship
     */
    public abstract RoleAttribute [] getRoleAttributes(
            MeshObject neighbor );

    /**
     * Aggregate all values of a named RoleAttribute that this MeshObject participates in.
     * This exists to avoid hitting the disk when neighbor objects don't actually have to be
     * retrieved to perform the operation.
     * 
     * @param name the name of the RoleAttribute to aggregate
     * @param initialValue the initial value of the aggregate
     * @param operator the aggregation function: it takes the previous aggregate, the new value, and returns the new aggregate
     * @return the aggregate
     * @param <S> the type of "sum" that is being created by the aggregate
     */
    public abstract <S> S aggregateRoleAttributes(
            String                       name,
            S                            initialValue,
            BiFunction<S,Serializable,S> operator );

// -- Neighbors --

    /**
     * Traverse from this MeshObject to all directly related MeshObjects.
     *
     * @return the set of MeshObjects that are directly related to this MeshObject
     */
    public abstract MeshObjectSet traverseToNeighbors();

    /**
     * Determine the number of neighbors of this MeshObject.
     * 
     * @return the number of neighbors
     */
    public abstract int neighborsCount();

    /**
     * Unrelate this MeshObject from another MeshObject. This will also remove all RoleTypes and all RoleAttributes
     * on the relationship.
     *
     * @param neighbor the MeshObject to unrelate from
     * @throws NotRelatedException thrown if this MeshObject is not related to the other MeshObject
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void unrelate(
            MeshObject neighbor )
        throws
            NotRelatedException,
            TransactionException,
            NotPermittedException;

    /**
     * Determine whether this MeshObject is related to another MeshObject. A MeshObject is related to another
     * MeshObject if at least one end of their relationship is blessed with a RoleType, or carries a RoleAttribute.
     *
     * @param otherObject the MeshObject to which this MeshObject may be related
     * @return true if this MeshObject is currently related to otherObject
     */
    public abstract boolean isRelated(
            MeshObject otherObject );

// -- blessing --

    /**
     * Make this MeshObject support the provided EntityType.
     * (The name of this method comes from Perl's bless method.)
     *
     * @param type the new EntityType to be supported by this MeshObject
     * @throws EntityBlessedAlreadyException thrown if this MeshObject is blessed already with this EntityType
     * @throws EntityTypeIsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void bless(
            EntityType type )
        throws
            EntityBlessedAlreadyException,
            EntityTypeIsAbstractException,
            TransactionException,
            NotPermittedException;

    /**
     * Make this MeshObject support the provided one or more EntityTypes. As a result, the
     * MeshObject will either be blessed with all of the EntityTypes, or none.
     * (The name of this method comes from Perl's bless method.)
     *
     * @param types the new EntityTypes to be supported by this MeshObject
     * @throws EntityBlessedAlreadyException thrown if this MeshObject is blessed already with at least one of these EntityTypes
     * @throws EntityTypeIsAbstractException thrown if at least one of the EntityTypes is abstract and cannot be instantiated
     * @throws TransactionException thrown if invoked outside of proper transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void bless(
            EntityType [] types )
        throws
            EntityBlessedAlreadyException,
            EntityTypeIsAbstractException,
            TransactionException,
            NotPermittedException;

    /**
     * Makes this MeshObject stop supporting the provided EntityType. This may fail with an
     * RoleTypeRequiresEntityTypeException because the RoleType of a relationship in which
     * this MeshObject participates requires this MeshObject to have the EntityType
     * that is supposed to be unblessed. To avoid this, unbless the relevant relationship(s)
     * first.
     *
     * @param type the EntityType that the MeshObject will stop supporting
     * @throws RoleTypeRequiresEntityTypeException thrown if this MeshObject plays a role that requires the MeshObject
     *         to remain being blessed with this EntityType
     * @throws EntityNotBlessedException thrown if this MeshObject does not currently support this EntityType
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public void unbless(
            EntityType type )
        throws
            RoleTypeRequiresEntityTypeException,
            EntityNotBlessedException,
            TransactionException,
            NotPermittedException;

    /**
     * Makes this MeshObject stop supporting all of the provided EntityTypes. As a result,
     * the MeshObject will either be unblessed from all of the EntityTypes, or none.
     * This may fail with an
     * RoleTypeRequiresEntityTypeException because the RoleType of a relationship in which
     * this MeshObject participates requires this MeshObject to have the EntityType
     * that is supposed to be unblessed. To avoid this, unbless the relevant relationship(s) first.
     *
     * @param types the EntityTypes that the MeshObject will stop supporting
     * @throws RoleTypeRequiresEntityTypeException thrown if this MeshObject plays one or more roles that requires the
     *         MeshObject to remain being blessed with at least one of the EntityTypes
     * @throws EntityNotBlessedException thrown if this MeshObject does not support at least one of the given EntityTypes
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public void unbless(
            EntityType [] types )
        throws
            RoleTypeRequiresEntityTypeException,
            EntityNotBlessedException,
            TransactionException,
            NotPermittedException;

    /**
     * Obtain the EntityTypes that this MeshObject is currently blessed with.
     *
     * @return the EntityTypes
     */
    public abstract EntityType [] getEntityTypes();

    /**
     * Obtain the EntityTypes that this MeshObject is currently blessed with,
     * consistently ordered by the their UserVisibleName.
     *
     * @return the EntityTypes
     */
    public abstract EntityType [] getOrderedEntityTypes();

    /**
     * Determine whether this MeshObject currently supports this EntityType.
     * By default, this returns true even if the MeshObject is blessed by a
     * subtype of the provided EntityType instead of the EntityType directly.
     *
     * @param type the EntityType to look for
     * @return true if this MeshObject supports this MeshType or a subtype
     */
    public abstract boolean isBlessedBy(
            EntityType type );

    /**
     * Determine whether this MeshObject currently supports this EntityType.
     * Specify whether or not subtypes of the provided EntityType should be considered.
     *
     * @param type the EntityType to look for
     * @param considerSubtypes if true, return true even if only a subtype matches
     * @return true if this MeshObject supports this MeshType
     */
    public abstract boolean isBlessedBy(
            EntityType type,
            boolean    considerSubtypes );

    /**
     * Determine the specific subtypes of the provided EntityType with which this
     * MeshObject has been blessed. If this MeshObject has not been blessed with a
     * subtype of the provided EntityType, return a zero-length array. If this MeshObject
     * has not been blessed with the provided EntityType at all, throws an
     * EntityNotBlessedException.
     *
     * @param type the EntityType
     * @return the subtypes, if any
     * @throws EntityNotBlessedException thrown if the MeshObject is not blessed by the EntityType
     * @see #determineSingleBlessedSubtype
     */
    public abstract EntityType [] determineBlessedSubtypes(
            EntityType type )
        throws
            EntityNotBlessedException;

    /**
     * Convenience method to determine the single subtype of the provided EntityType with
     * which this MeshObject has been blessed. If this MeshObject has not been blessed with a
     * subtype of the provided EntityType, return <code>null</code>. If it has
     * been blessed with more than one subtype, throw an IllegalStateException. If this MeshObject
     * has not been blessed with the provided EntityType at all, throw an
     * EntityNotBlessedException.
     *
     * @param type the EntityType
     * @return the subtype, if any
     * @throws EntityNotBlessedException thrown if the MeshObject is not blessed by the EntityType
     * @throws IllegalStateException thrown if the MeshObject is blessed by more than one subtype
     * @see #determineBlessedSubtypes
     */
    public abstract EntityType determineSingleBlessedSubtype(
            EntityType type )
        throws
            EntityNotBlessedException,
            IllegalStateException;

// -- Blessing roles/relationships --

    /**
     * Make a relationship of this MeshObject to another MeshObject support the provided RoleType.
     * (The name of this method comes from Perl's bless method.)
     *
     * @param thisEnd the RoleType of the RelationshipType that is instantiated at the end that this MeshObject is attached to
     * @param neighbor the MeshObject whose relationship to this MeshObject shall be blessed
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws RoleTypeBlessedAlreadyException thrown if the relationship to the other MeshObject is blessed
     *         already with this RoleType
     * @throws EntityNotBlessedException thrown if this MeshObject is not blessed by a requisite EntityType
     * @throws RelationshipTypeIsAbstractException thrown if the RoleType belongs to an abstract RelationshipType
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #unblessRole
     */
    public void blessRole(
            RoleType   thisEnd,
            MeshObject neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeBlessedAlreadyException,
            EntityNotBlessedException,
            RelationshipTypeIsAbstractException,
            TransactionException,
            NotPermittedException;

    /**
     * Make a relationship of this MeshObject to another MeshObject support the provided RoleTypes.
     * As a result, this relationship will support either all RoleTypes or none.
     * (The name of this method comes from Perl's bless method.)
     *
     * @param thisEnd the RoleTypes of the RelationshipTypes that are instantiated at the end that this MeshObject is attached to
     * @param neighbor the MeshObject whose relationship to this MeshObject shall be blessed
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws RoleTypeBlessedAlreadyException thrown if the relationship to the other MeshObject is blessed
     *         already with one ore more of the given RoleTypes
     * @throws EntityNotBlessedException thrown if this MeshObject is not blessed by a requisite EntityType
     * @throws RelationshipTypeIsAbstractException thrown if one of the RoleTypes belong to an abstract RelationshipType
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #unblessRole
     */
    public void blessRole(
            RoleType [] thisEnd,
            MeshObject  neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeBlessedAlreadyException,
            EntityNotBlessedException,
            RelationshipTypeIsAbstractException,
            TransactionException,
            NotPermittedException;

    /**
     * Make a relationship of this MeshObject to another MeshObject stop supporting the provided RoleType.
     *
     * @param thisEnd the RoleType of the RelationshipType at the end that this MeshObject is attached to, and that shall be removed
     * @param neighbor the other MeshObject whose relationship to this MeshObject shall be unblessed
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws RoleTypeNotBlessedException thrown if the relationship to the other MeshObject does not support the RoleType
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public void unblessRole(
            RoleType   thisEnd,
            MeshObject neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeNotBlessedException,
            TransactionException,
            NotPermittedException;

    /**
     * Make a relationship of this MeshObject to another MeshObject stop supporting the provided RoleTypes.
     * As a result, either all RoleTypes will be unblessed or none.
     *
     * @param thisEnd the RoleTypes of the RelationshipTypes at the end that this MeshObject is attached to, and that shall be removed
     * @param neighbor the other MeshObject whose relationship to this MeshObject shall be unblessed
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws RoleTypeNotBlessedException thrown if the relationship to the other MeshObject does not support at least one of the RoleTypes
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public void unblessRole(
            RoleType [] thisEnd,
            MeshObject  neighbor )
        throws
            CannotRelateToItselfException,
            RoleTypeNotBlessedException,
            TransactionException,
            NotPermittedException;

// -- Traversal --

    /**
      * Traverse a TraverseSpecification from this MeshObject to obtain a set of MeshObjects
      *
      * @param ts the TraverseSpecification to traverse
      * @return the set of MeshObjects found as a result of the traversal
      */
    public abstract MeshObjectSet traverse(
            TraverseSpecification ts );

    /**
     * Determine the number of neighbor MeshObjects found by traversing from this MeshObject.
     * 
     * @param rt the RoleType to traverse
     * @return the number of neighbor MeshObjects
     */
    public abstract int traverseCount(
            RoleType rt );

    /**
     * Obtain the RoleTypes that this MeshObject currently participates in.
     * This will return only one instance of the same RoleType object, even if the
     * MeshObject participates in this RoleType multiple times with different neighbor
     * MeshObjects.
     *
     * @return the RoleTypes that this MeshObject currently participates in.
     */
    public abstract RoleType [] getRoleTypes();

    /**
     * Obtain the RoleTypes that this MeshObject currently participates in, consistently
     * ordered by their user-visible name.
     * This will return only one instance of the same RoleType object, even if the
     * MeshObject participates in this RoleType multiple times with different other
     * MeshObjects.
     *
     * @return the RoleTypes that this MeshObject currently participates in.
     */
    public abstract RoleType [] getOrderedRoleTypes();

    /**
     * Obtain the RoleTypes that this MeshObject currently participates in with the
     * specified neighbor MeshObject.
     *
     * @param neighbor the other MeshObject
     * @return the RoleTypes that this MeshObject currently participates in.
     */
    public abstract RoleType [] getRoleTypes(
            MeshObject neighbor );

    /**
     * Obtain the RoleTypes that this MeshObject currently participates in with the
     * specified neighbor MeshObject, consistently ordered by their user-visible name.
     *
     * @param neighbor the other MeshObject
     * @return the RoleTypes that this MeshObject currently participates in.
     */
    public abstract RoleType [] getOrderedRoleTypes(
            MeshObject neighbor );

    /**
     * Determine whether this MeshObject's relationship to the other MeshObject is blessed
     * with a given RoleType. Also returns false if the two MeshObjects are not related.
     *
     * @param thisEnd the RoleTypes of the RelationshipTypes at the end that this MeshObject is attached to
     * @param neighbor the other MeshObject
     * @return true if this MeshObject has a relationship to the other MeshObject  and it is blessed with the given RoleType
     */
    public abstract boolean isRelated(
            RoleType   thisEnd,
            MeshObject neighbor );

// -- Role Properties --

    /**
     * Determine whether this side of the relationship between this MeshObject and a neighbor carries this PropertyType.
     * This is a convenience method because this could also be determined by looking at the RoleTypes and which
     * PropertyTypes they define.
     *
     * @param neighbor the other MeshObject
     * @param pt the PropertyType
     * @return true of this MeshObject carries this PropertyType
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract boolean hasRoleProperty(
            MeshObject   neighbor,
            PropertyType pt )
        throws
            NotPermittedException;

    /**
     * Obtain the value of a RoleProperty on this side of the relationship between this MeshObject and a neighbor.
     *
     * @param neighbor the other MeshObject
     * @param pt the PropertyType that identifies the correct Property of this Role
     * @return the current value of the Property
     * @throws IllegalRolePropertyTypeException thrown if the PropertyType does not exist on this Role
     *         because the relationship to the neighbor MeshObject has not been blessed with a RelationshipType
     *         that provides this PropertyType on this Role
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @see #setRolePropertyValue
     */
    public abstract PropertyValue getRolePropertyValue(
            MeshObject   neighbor,
            PropertyType pt )
        throws
            IllegalRolePropertyTypeException,
            NotPermittedException;

    /**
     * Convenience method to obtain all values for all provided PropertyTypes on this side of the relationship
     * between this MeshObject and a neighbor, in the same sequence as the provided
     * PropertyTypes. If a PropertyType does not exist on this Role, or if access to one of the
     * PropertyTypes is not permitted, this will throw an exception. This is a convenience method.
     *
     * @param neighbor the other MeshObject
     * @param pts the PropertyTypes that identify the current Properties of this Role
     * @return the PropertyValues, in the same sequence as PropertyTypes
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws IllegalRolePropertyTypeException thrown if one PropertyType does not exist on this Role
     *         because the relationship to the neighbor MeshObject has not been blessed with a RelationshipType
     *         that provides this PropertyType on this Role
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract PropertyValue [] getRolePropertyValues(
            MeshObject      neighbor,
            PropertyType [] pts )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            NotPermittedException;

    /**
     * Set the value of a Role Property on this side of the relationship between this MeshObject and a neighbor.
     *
     * @param neighbor the other MeshObject
     * @param pt the PropertyType that identifies the correct Property of this Role
     * @param newValue the new value of the Property
     * @return old value of the Property
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws IllegalRolePropertyTypeException thrown if the PropertyType does not exist on this Role
     *         because the relationship to the neighbor MeshObject has not been blessed with a RelationshipType
     *         that provides this PropertyType on this Role
     * @throws IllegalPropertyValueException thrown if the new value is an illegal value for this Role Property
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @see #getRolePropertyValue
     */
    public abstract PropertyValue setRolePropertyValue(
            MeshObject    neighbor,
            PropertyType  pt,
            PropertyValue newValue )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the values of several RoleProperties on this side of the relationship between
     * this MeshObject and a neighbor at the same time.
     * The PropertyTypes identifying the Role Properties and their new PropertyValues are given in the
     * same sequence. This method sets either all values, or none.
     *
     * @param neighbor the other MeshObject
     * @param pts the PropertyTypes that identify the correct Properties of this Role
     * @param pvs the new values of the Properties
     * @return old values of the Properties
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws IllegalRolePropertyTypeException thrown if one PropertyType does not exist on this Role
     *         because the relationship to the neighbor MeshObject has not been blessed with a ReolationshipType
     *        that provides this PropertyType on this Role
     * @throws IllegalPropertyValueException thrown if the new value is an illegal value for this Role Property
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     */
    public abstract PropertyValue [] setRolePropertyValues(
            MeshObject       neighbor,
            PropertyType []  pts,
            PropertyValue [] pvs )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException;

    /**
     * Convenience method to set the values of several RoleProperties on this side of the relationship between
     * this MeshObject and a neighbor at the same time. The PropertyTypes identifying the Properties
     * and their new PropertyValues are given in the Map. This method sets either all values, or none.

     * @param neighbor the other MeshObject
     * @param newValues Map of PropertyType to PropertyValue
     * @throws CannotRelateToItselfException thrown if the neighbor MeshObject is the MeshObject itself
     * @throws IllegalRolePropertyTypeException thrown if one PropertyType does not exist on this Role
     *         because the relationship to the neighbor MeshObject has not been blessed with a ReolationshipType
     *        that provides this PropertyType on this Role
     * @throws IllegalPropertyValueException thrown if the new value is an illegal value for this Role Property
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     */
    public abstract void setRolePropertyValues(
            MeshObject                      neighbor,
            Map<PropertyType,PropertyValue> newValues )
        throws
            CannotRelateToItselfException,
            IllegalRolePropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException,
            TransactionException;

    /**
     * Obtain the set of all PropertyTypes currently used on this side of the relationship between this MeshObject
     * and a neighbor.
     *
     * @param neighbor the other MeshObject
     * @return the set of all PropertyTypes with this Role
     */
    public abstract PropertyType [] getRolePropertyTypes(
            MeshObject neighbor );

    /**
     * Obtain the set of all PropertyTypes currently used on this side of the relationship between this MeshObject
     * and any of its neighbors.
     *
     * @return the set of all PropertyTypes with this Role
     */
    public abstract PropertyType [] getRolePropertyTypes();

    /**
     * Obtain the set of all PropertyTypes currently used on this side of the relationship between this MeshObject
     * and neighbor, consistently ordered by UserVisibleName.
     *
     * @param neighbor the other MeshObject
     * @return the set of all PropertyTypes with this Role
     */
    public abstract PropertyType [] getOrderedRolePropertyTypes(
            MeshObject neighbor );

    /**
     * Obtain the set of all PropertyTypes currently used on this side of the relationship between this MeshObject
     * and any of its neighbors, consistently ordered by UserVisibleName.
     *
     * @return the set of all PropertyTypes with this Role
     */
    public abstract PropertyType [] getOrderedRolePropertyTypes();

    /**
     * <p>Convenience method to obtain a PropertyType used on this side of the relationship between this MeshObject
     * and a neighbor, by providing its  (short) name. If such a PropertyType could be found, this call returns it. If not,
     * it throws an MeshTypeNotFoundException.
     *
     * <p>Warning: sometimes (rarely), a Role may carry two Properties with the same (short) name.
     * It is undefined which of those PropertyTypes this call will return.
     *
     * @param neighbor the other MeshObject
     * @param propertyName the name of the property
     * @return the PropertyType
     * @throws MeshTypeNotFoundException thrown if a Role Property by this name could not be found
     */
    public abstract PropertyType getRolePropertyTypeByName(
            MeshObject neighbor,
            String     propertyName )
        throws
            MeshTypeNotFoundException;

    /**
     * Get the names and values of all RoleProperties on this MeshObject's side of a relationship with this neighbor,
     *
     * @param neighbor the neighbor MeshObject
     * @return the set of all RoleProperties on this relationship
     */
    public abstract RoleProperty [] getRoleProperties(
            MeshObject neighbor );

    /**
     * Aggregate all RoleProperties of a certain PropertyType that this MeshObject participates in.
     * This exists to avoid hitting the disk when neighbor objects don't actually have to be
     * retrieved to perform the operation.
     * 
     * @param pt the Role PropertyType to aggregate
     * @param initialValue the initial value of the aggregate
     * @param operator the aggregation function: it takes the previous aggregate, the value of the RoleProperty and returns the new aggregate
     * @return the aggregate
     * @param <S> the type of "sum" that is being created by the aggregate
     */
    public abstract <S> S aggregateRoles(
            PropertyType                  pt,
            S                             initialValue,
            BiFunction<S,PropertyValue,S> operator );

// -- Other --

    /**
     * Obtain the same MeshObject as ExternalizedMeshObject so it can be easily serialized.
     * There should be no need for the application programmer to invoke this.
     *
     * @return this MeshObject as ExternalizedMeshObject
     */
    public abstract ExternalizedMeshObject asExternalized();

    /**
     * Obtain a String that renders this instance suitable for showing to a user.
     *
     * @return the user-visible String representing this instance
     */
    public String getUserVisibleString();

    /**
     * Obtain a String that renders this instance suitable for showing to a user.
     * Specify the prioritized sequence of EntityTypes that have a chance of defining
     * that user-visible String. For example, if a MeshObject is blessed with EntityTypes
     * A, B and D, of which B and D define a user-visible String other than null,
     * and if this method is invoked with EntityTypes A, C, D, B, this method will
     * return the result of D's get_UserVisibleString() method.
     * For programming simplicity, this method may be invoked with EntityTypes that the
     * current MeshObject is not currently blessed with. If no user-visible String can be
     * determined from the specified EntityTypes (or if none are specified), this method
     * will return a generic default, such as the String form of the MeshObjectIdentifier.
     *
     * @param types the EntityTypes to be consulted, in sequence, until a non-null result is found
     * @return the user-visible String representing this instance
     */
    public String getUserVisibleString(
            EntityType [] types );
}
