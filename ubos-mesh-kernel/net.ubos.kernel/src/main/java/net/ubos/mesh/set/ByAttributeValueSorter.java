//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import java.io.Serializable;
import java.util.Comparator;
import net.ubos.mesh.IllegalAttributeException;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.util.InverseComparator;
import net.ubos.util.logging.Log;

/**
 * Sorts by the value of an Attribute available on all MeshObjects in the MeshObjectSet.
 * This is a convenience class.
 */
public class ByAttributeValueSorter
        extends
            DefaultMeshObjectSorter
{
    private static final Log log = Log.getLogInstance( ByAttributeValueSorter.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param attributeName the name of the Attribute by whose values we sort
     * @return the created ByPropertyValueSorter
     */
    public static ByAttributeValueSorter create(
            String attributeName )
    {
        Comparator<MeshObject> c = new AttributeValueComparator( attributeName );
        return new ByAttributeValueSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param attributeName the name of the Attribute by whose values we sort
     * @param userName localized user-visible name in the current locale
     * @return the created ByPropertyValueSorter
     */
    public static ByAttributeValueSorter create(
            String attributeName,
            String userName )
    {
        Comparator<MeshObject> c = new AttributeValueComparator( attributeName );
        return new ByAttributeValueSorter( c, userName );
    }

    /**
     * Factory method.
     *
     * @param attributeName the name of the Attribute by whose values we sort
     * @param inverse if true, sort in reverse order
     * @return the created ByPropertyValueSorter
     */
    public static ByAttributeValueSorter create(
            String  attributeName,
            boolean inverse )
    {
        Comparator<MeshObject> c = new AttributeValueComparator( attributeName );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByAttributeValueSorter( c, null );
    }

    /**
     * Factory method.
     *
     * @param attributeName the name of the Attribute by whose values we sort
     * @param inverse if true, sort in reverse order
     * @param userName localized user-visible name in the current locale
     * @return the created ByPropertyValueSorter
     */
    public static ByAttributeValueSorter create(
            String  attributeName,
            boolean inverse,
            String  userName )
    {
        Comparator<MeshObject> c = new AttributeValueComparator( attributeName );
        if( inverse ) {
            c = new InverseComparator<>( c );
        }
        return new ByAttributeValueSorter( c, userName );
    }

    /**
     * Constructor for subclasses only.
     *
     * @param c specify the comparison criteria as the Java collections API does
     * @param userName localized user-visible name in the current locale
     */
    protected ByAttributeValueSorter(
            Comparator<MeshObject> c,
            String                 userName )
    {
        super( c, userName );
    }

    /**
     * The underlying Comparator of attribute values.
     */
    public static class AttributeValueComparator
            implements
                Comparator<MeshObject>
    {
        /**
         * Constructor.
         *
         * @param attributeName the PropertyType that selects the property to compare
         */
        public AttributeValueComparator(
                String attributeName )
        {
            if( attributeName == null ) {
                throw new NullPointerException( "Null Attribute name" );
            }
            theAttributeName = attributeName;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @SuppressWarnings("unchecked")
        public int compare(
                MeshObject o1,
                MeshObject o2 )
        {
            Serializable o1Value = null; // better have a default
            Serializable o2Value = null; // better have a default

            try {
                o1Value = o1.getAttributeValue( theAttributeName );
            } catch( IllegalAttributeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }

            try {
                o2Value = o2.getAttributeValue( theAttributeName );
            } catch( IllegalAttributeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }

            int ret;
            if( o1Value instanceof Comparable && o2Value instanceof Comparable ) {
                Comparable realO1Value = (Comparable) o1Value;
                Comparable realO2Value = (Comparable) o2Value;

                ret = realO1Value.compareTo( realO2Value );

            } else {
                ret = o2Value.hashCode() - o1Value.hashCode();
            }
            return ret;
        }

        /**
         * The name of the Attribute by which to compare.
         */
        protected String theAttributeName;
    }
}
