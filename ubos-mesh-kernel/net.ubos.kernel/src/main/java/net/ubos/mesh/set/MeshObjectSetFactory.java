//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.traverse.TraversePath;
import net.ubos.model.traverse.TraverseSpecification;

/**
 * Classes supporting this type know how to instantiate MeshObjectSets of various
 * kinds. This is important to be able to implement sets whose implementation varies,
 * e.g. sets whose content is determined upon construction vs sets whose content is
 * only determined once it is being iterated over, and then it may only be done
 * incrementally.
 */
public interface MeshObjectSetFactory
{
    /**
     * Obtain the MeshBase on which this MeshObjectFactory operates. This goes
     * with the MeshBase, not the MeshBaseView, as it is specific to the
     * implementation of the MeshBase, not which of its MeshBaseView contains
     * the MeshObjects in the set.
     *
     * @return the MeshBase
     */
    public MeshBase getMeshBase();

    /**
     * Factory method to construct a MeshObjectSet with the single specified member.
     * Given that it has only one member, we might as well return an OrderedMeshObjectSet.
     *
     * @param member the content of the set
     * @return the created MeshObjectSet
     */
    public OrderedImmutableMeshObjectSet createSingleMemberImmutableMeshObjectSet(
            MeshObject member );

    /**
     * Factory method to construct a MeshObjectSet with the specified members.
     *
     * @param members the content of the set
     * @return the created MeshObjectSet
     */
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObject [] members );

    /**
     * Factory method to construct a MeshObjectSet with the specified members, as long
     * as they are selected by the MeshObjectSelector.
     *
     * @param candidates the candidate members of the set
     * @param selector determines which candidates are included
     * @return the created MeshObjectSet
     */
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObject []      candidates,
            MeshObjectSelector selector );

    /**
     * Factory method to construct a MeshObjectSet with the members of another
     * MeshObjectSet, as long as they are selected by the MeshObjectSelector.
     *
     * @param input the input MeshObjectSet
     * @param selector determines which candidates are included
     * @return the created MeshObjectSet
     */
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObjectSet      input,
            MeshObjectSelector selector );

    /**
     * Factory method to construct a MeshObjectSet with all the members of the provided
     * MeshObjectSets.
     *
     * @param operands the sets to unify
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet [] operands );

    /**
     * Factory method to construct a MeshObjectSet with all the members of the provided
     * MeshObjectSets, as long as they are selected by the MeshObjectSelector.
     *
     * @param operands the sets to unify
     * @param selector the MeshObjectSelector to use, if any
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet []   operands,
            MeshObjectSelector selector );

    /**
     * Convenience factory method to construct a unification of two MeshObjectSets.
     *
     * @param one the first set to unify
     * @param two the second set to unify
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet one,
            MeshObjectSet two );

    /**
     * Convenience factory method to construct a unification of a MeshObjectSet and
     * a second single-element MeshObjectSet.
     *
     * @param one the first set to unify
     * @param two the second set to unify
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetUnification(
            MeshObjectSet one,
            MeshObject    two );

    /**
     * Factory method to construct a MeshObjectSet that contains those MeshObjects that are contained
     * in all of the provided MeshObjectSets.
     *
     * @param operands the sets to intersect
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet [] operands );

    /**
     * Factory method to construct a MeshObjectSet that conatins those MeshObjects that are
     * contained in all of the provided MeshObjectSets, as long as they are also
     * selected by the MeshObjectSelector.
     *
     * @param operands the sets to intersect
     * @param selector the MeshObjectSelector to use, if any
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet []   operands,
            MeshObjectSelector selector );

    /**
     * Convenience factory method to construct an intersection of two MeshObjectSets.
     *
     * @param one the first set to intersect
     * @param two the second set to intersect
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet one,
            MeshObjectSet two );

    /**
     * Convenience factory method to construct an intersection of two MeshObjectSets.
     *
     * @param one the first set to intersect
     * @param two the second set to intersect
     * @return the created MeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetIntersection(
            MeshObjectSet one,
            MeshObject    two );

    /**
     * Factory method to construct a MeshObjectSet that contains those MeshObjects from
     * a first MeshObjectSet that are not contained in a second MeshObjectSet.
     *
     * @param one the first MeshObjectSet
     * @param two the second MeshObjectSet
     * @return the created CompositeImmutableMeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetMinus(
            MeshObjectSet one,
            MeshObjectSet two );

    /**
     * Factory method to construct a MeshObjectSet that contains those MeshObjects from
     * a first MeshObjectSet that are not contained in a second MeshObjectSet.
     *
     * @param one the first MeshObjectSet
     * @param two the second MeshObjectSet
     * @return the created CompositeImmutableMeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetMinus(
            MeshObjectSet one,
            MeshObject    two );

    /**
     * Factory method to construct a MeshObjectSet that contains those MeshObjects from
     * a first MeshObjectSet that are not contained in a second MeshObjectSet, as long
     * as they are also selected by the MeshObjectSelector.
     *
     * @param one the first MeshObjectSet
     * @param two the second MeshObjectSet
     * @param selector the MeshObjectSelector to use, if any
     * @return the created CompositeImmutableMeshObjectSet
     */
    public CompositeImmutableMeshObjectSet createImmutableMeshObjectSetMinus(
            MeshObjectSet      one,
            MeshObjectSet      two,
            MeshObjectSelector selector );

    /**
     * Factory method to create an OrderedMeshObjectSet.
     *
     * @param contentInOrder the content of the OrderedMeshObjectSet, in order
     * @return the created OrderedImmutableMeshObjectSet
     */
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObject [] contentInOrder );

    /**
     * Factory method to create an OrderedMeshObjectSet with the specified members, as long
     * as they are selected by the MeshObjectSelector.
     *
     * @param candidatesInOrder the candidate content of the OrderedMeshObjectSet, in order
     * @param selector determines which candidates are included
     * @return the created OrderedImmutableMeshObjectSet
     */
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObject []      candidatesInOrder,
            MeshObjectSelector selector );

    /**
     * Factory method to create an OrderedMeshObjectSet.
     *
     * @param content the content of the OrderedMeshObjectSet, in any order
     * @param sorter the MeshObjectSorter that determines the ordering within the OrderedMeshObjectSet
     * @return the created OrderedImmutableMeshObjectSet
     */
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObject []    content,
            MeshObjectSorter sorter );

    /**
     * Factory method to create an OrderedMeshObjectSet.
     *
     * @param content the content of the OrderedMeshObjectSet
     * @param sorter the MeshObjectSorter that determines the ordering within the OrderedMeshObjectSet
     * @return the created OrderedImmutableMeshObjectSet
     */
    public OrderedImmutableMeshObjectSet createOrderedImmutableMeshObjectSet(
            MeshObjectSet    content,
            MeshObjectSorter sorter );

   /**
     * Factory method to construct a ImmutableMeshObjectSet as the result of
     * traversing from a MeshObject through a TraverseSpecification.
     *
     * @param startObject the MeshObject from where we start the traversal
     * @param specification the TraverseSpecification to apply to the startObject
     * @return the created ActiveMeshObjectSet
     */
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObject             startObject,
            TraverseSpecification specification );

    /**
     * Factory method to construct a ImmutableMeshObjectSet as the result of
     * traversing from a MeshObjectSet through a TraverseSpecification.
     *
     * @param startSet the MeshObjectSet from where we start the traversal
     * @param specification the TraverseSpecification to apply to the startObject
     * @return the created ActiveMeshObjectSet
     */
    public ImmutableMeshObjectSet createImmutableMeshObjectSet(
            MeshObjectSet          startSet,
            TraverseSpecification specification );

    /**
     * Factory method to create an ImmutableTraversePathSet.
     * Given that it has only one member, it might as well return an ordered TraversePathSet.
     *
     * @param singleMember the single member of the ImmutableTraversePathSet
     * @return return the created ImmutableTraversePathSet
     */
    public ImmutableTraversePathSet createSingleMemberImmutableTraversePathSet(
            TraversePath singleMember );

    /**
     * Factory method to create an ImmutableTraversePathSet.
     *
     * @param content the content for the ImmutableTraversePathSet
     * @return the created ImmutableTraversePathSet
     */
    public ImmutableTraversePathSet createImmutableTraversePathSet(
            TraversePath [] content );

    /**
     * Factory method to create an ImmutableTraversePathSet.
     * This creates a set of TraversePaths each with length 1.
     * The destination of each TraversePath corresponds to the elements of the
     * given MeshObjectSet. The TraverseSpecification is passed in.
     *
     * @param spec the traversed TraverseSpecification
     * @param set used to construct the content for the ImmutableTraversePathSet
     * @return the created ImmutableTraversePathSet
     */
    public ImmutableTraversePathSet createImmutableTraversePathSet(
            TraverseSpecification spec,
            MeshObjectSet          set );

    /**
     * Factory method to create an ImmutableTraversePathSet.
     *
     * @param start the MeshObject from which we start the traversal
     * @param spec the TraverseSpecification from the start MeshObject
     * @return the created ImmutableTraversePathSet
     */
    public ImmutableTraversePathSet createImmutableTraversePathSet(
            MeshObject             start,
            TraverseSpecification spec );

    /**
     * Factory method to create an ImmutableTraversePathSet.
     *
     * @param startSet the MeshObjectSet from which we start the traversal
     * @param spec the TraverseSpecification from the start MeshObject
     * @return the created ImmutableTraversePathSet
     */
    public ImmutableTraversePathSet createImmutableTraversePathSet(
            MeshObjectSet          startSet,
            TraverseSpecification spec );

    /**
     * Factory method to create an ImmutableTraversePathSet.
     *
     * @param startSet the TraversePathSet from whose destination MeshObject we start the traversal
     * @param spec the TraverseSpecification from the start MeshObject
     * @return the created ImmutableTraversePathSet
     */
    public ImmutableTraversePathSet createImmutableTraversePathSet(
            TraversePathSet       startSet,
            TraverseSpecification spec );

    /**
     * Factory method to create an OrderedImmutableTraversePathSet.
     *
     * @param content the content of the TraversePathSet
     * @param sorter the TraversePathSorter that determines the ordering within the OrderedTraversePathSet
     * @return the created OrderedImmutableTraversePathSet
     */
    public OrderedImmutableTraversePathSet createOrderedImmutableTraversePathSet(
            TraversePathSet    content,
            TraversePathSorter sorter );

    /**
     * Factory method to create an OrderedImmutableTraversePathSet.
     *
     * @param content the content of the TraversePathSet
     * @param sorter the TraversePathSorter that determines the ordering within the OrderedTraversePathSet
     * @param max the maximum number of TraversePaths that will be contained by this set. If the underlying set contains more,
     *        this set will only contain the first max TraversePaths according to the sorter.
     * @return the created OrderedImmutableTraversePathSet
     */
    public OrderedImmutableTraversePathSet createOrderedImmutableTraversePathSet(
            TraversePathSet    content,
            TraversePathSorter sorter,
            int                 max );
}
