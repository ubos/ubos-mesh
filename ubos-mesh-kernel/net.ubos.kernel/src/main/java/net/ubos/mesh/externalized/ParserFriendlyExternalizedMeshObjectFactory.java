//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

/**
 * Knows how to instantiate the right subclass of ParserFriendlyExternalizedMeshObject.
 */
public interface ParserFriendlyExternalizedMeshObjectFactory
{
    /**
     * Factory method.
     *
     * @return the created ParserFriendlyExternalizedMeshObject
     */
    public ParserFriendlyExternalizedMeshObject createParserFriendlyExternalizedMeshObject();
}
