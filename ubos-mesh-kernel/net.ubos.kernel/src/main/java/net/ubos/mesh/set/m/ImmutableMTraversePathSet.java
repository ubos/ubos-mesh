//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.ImmutableTraversePathSet;
import net.ubos.model.traverse.TraversePath;

/**
 * A simple, immutable TraversalPathSet implementation.
 */
public class ImmutableMTraversePathSet
        extends
            AbstractMTraversePathSet
        implements
            ImmutableTraversePathSet
{
    /**
     * Private constructor, use factory method.
     *
     * @param factory the MeshObjectSetFactory that created this TraversalPathSet
     * @param content the content for the ImmutableMTraversalPathSet
     */
    protected ImmutableMTraversePathSet(
            MeshObjectSetFactory factory,
            TraversePath []     content )
    {
        super( factory );

        super.setInitialContent( content );
    }
}
