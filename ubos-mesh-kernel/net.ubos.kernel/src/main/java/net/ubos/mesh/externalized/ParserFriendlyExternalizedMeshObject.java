//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import java.io.Serializable;
import java.util.ArrayList;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * A temporary buffer for a to-be-deserialized MeshObject.
 */
public class ParserFriendlyExternalizedMeshObject
        extends
            AbstractExternalizedMeshObject
        implements
            HasSettableTypesPropertiesAttributes,
            CanBeDumped
{
    /**
     * Set the MeshObjectIdentifier of the MeshObject.
     *
     * @param newValue the new value
     */
    public void setIdentifier(
            MeshObjectIdentifier newValue )
    {
        theIdentifier = newValue;
    }

    /**
     * Set the TimeCreated.
     *
     * @param newValue the new value
     */
    public void setTimeCreated(
            long newValue )
    {
        theTimeCreated = newValue;
        theSomethingHasBeenSet = true;
    }

    /**
     * Set the TimeUpdated.
     *
     * @param newValue the new value
     */
    public void setTimeUpdated(
            long newValue )
    {
        theTimeUpdated = newValue;
        theSomethingHasBeenSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeIdentifier [] getTypeIdentifiers()
    {
        MeshTypeIdentifier [] ret = theMeshTypes.toArray( new MeshTypeIdentifier[ theMeshTypes.size() ]);
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAttributeName(
            String name )
    {
        theAttributeNames.add( name );
        theSomethingHasBeenSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getAttributeNames()
    {
        String [] ret = theAttributeNames.toArray( new String[ theAttributeNames.size() ]);
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAttributeValue(
            Serializable newValue )
    {
        theAttributeValues.add( newValue );
        theSomethingHasBeenSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable [] getAttributeValues()
    {
        Serializable [] ret = theAttributeValues.toArray( new Serializable[ theAttributeValues.size() ]);
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addType(
            MeshTypeIdentifier identifier )
    {
        theMeshTypes.add( identifier );
        theSomethingHasBeenSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeIdentifier [] getPropertyTypes()
    {
        MeshTypeIdentifier [] ret = thePropertyTypes.toArray( new MeshTypeIdentifier[ thePropertyTypes.size() ]);
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPropertyType(
            MeshTypeIdentifier identifier )
    {
        thePropertyTypes.add( identifier );
        theSomethingHasBeenSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyValue [] getPropertyValues()
    {
        PropertyValue [] ret = thePropertyValues.toArray( new PropertyValue[ thePropertyValues.size() ]);
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPropertyValue(
            PropertyValue newValue )
    {
        thePropertyValues.add( newValue );
        theSomethingHasBeenSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HasTypesPropertiesAttributes [] getThisEnds()
    {
        HasTypesPropertiesAttributes [] ret = theThisEnds.toArray( new HasTypesPropertiesAttributes[ theThisEnds.size() ]);
        return ret;
    }

    /**
     * Add a ThisEnd, i.e this end of the relationship to a neighbor.
     *
     * @param thisEnd the ThisEnds
     */
    public void addThisEnd(
            HasTypesPropertiesAttributes thisEnd )
    {
        if( thisEnd == null ) {
            throw new NullPointerException();
        }
        theThisEnds.add( thisEnd );
        theSomethingHasBeenSet = true;
    }

    /**
     * Set the PropertyValue that is currently being parsed.
     *
     * @param newPropertyValue the PropertyValue
     */
    public void setCurrentPropertyValue(
            PropertyValue newPropertyValue )
    {
        theCurrentPropertyValue = newPropertyValue;
    }

    /**
     * Obtain the PropertyValue that is currently being parsed.
     *
     * @return the PropertyValue
     */
    public PropertyValue getCurrentPropertyValue()
    {
        return theCurrentPropertyValue;
    }

    /**
     * Is this MeshObject alive or dead.
     *
     * @param newValue if true, this represents a MeshObject that has been deleted
     */
    public void setIsDead(
            boolean newValue )
    {
        theIsDead = newValue;
        theSomethingHasBeenSet = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getIsDead()
    {
        return theIsDead;
    }

    /**
     * Add an ExternalizedMeshObject that represents a historical version of this ExternalizedMeshObject.
     *
     * @param toAdd the ExternalizedMeshObject
     */
    public void addHistorical(
            ExternalizedMeshObject toAdd )
    {
        if( toAdd == null ) {
            throw new NullPointerException();
        }
        if( theHistory == null ) {
            theHistory = new ArrayList<>();
        }
        theHistory.add( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObject [] getHistorical()
    {
        if( theHistory == null ) {
            return null;
        } else {
            ExternalizedMeshObject [] ret = theHistory.toArray( new ExternalizedMeshObject[ theHistory.size() ]);
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasSomethingBeenSet()
    {
        return theSomethingHasBeenSet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theIdentifier",
                    "theAttributeNames",
                    "theAttributeValues",
                    "theMeshTypes",
                    "thePropertyTypes",
                    "thePropertyValues",
                    "theTimeCreated",
                    "theTimeUpdated",
                    "theThisEnds"
                },
                new Object[] {
                    theIdentifier,
                    theAttributeNames,
                    theAttributeValues,
                    theMeshTypes,
                    thePropertyTypes,
                    thePropertyValues,
                    theTimeCreated,
                    theTimeUpdated,
                    theThisEnds
                });
    }

    /**
     * The MeshTypeIdentifiers of the MeshTypes.
     */
    protected ArrayList<MeshTypeIdentifier> theMeshTypes = new ArrayList<>();

    /**
     * The names of the Attributes.
     */
    protected ArrayList<String> theAttributeNames = new ArrayList<>();

    /**
     * The values of the Attributes that go with the Attribute names
     */
    protected ArrayList<Serializable> theAttributeValues = new ArrayList<>();

    /**
     * The MeshTypeIdentifiers of the PropertyTypes.
     */
    protected ArrayList<MeshTypeIdentifier> thePropertyTypes = new ArrayList<>();

    /**
     * The PropertyValues that go with the PropertyTypes.
     */
    protected ArrayList<PropertyValue> thePropertyValues = new ArrayList<>();

    /**
     * This end of the relationships in which this ExternalizedMeshObject participates.
     */
    protected ArrayList<HasTypesPropertiesAttributes> theThisEnds = new ArrayList<>();

    /**
     * If true, this represents a MeshObject that has been deleted.
     */
    protected boolean theIsDead;

    /**
     * The PropertyValue that is currently being parsed.
     */
    protected PropertyValue theCurrentPropertyValue;

    /**
     * The history of this MeshObject, if any.
     */
    protected ArrayList<ExternalizedMeshObject> theHistory;

    /**
     * Is set to true if at least one (non-identifier, non-history) values has been
     * set. This lets us detect whether actual data was found.
     */
    protected boolean theSomethingHasBeenSet = false;

    /**
     * Represents our end of a relationship.
     */
    public static class ThisEnd
        implements
            HasSettableTypesPropertiesAttributes,
            CanBeDumped
    {
        /**
         * Constructor.
         *
         * @param identifier the neighbor MeshObjectIdentifier
         * @param timeCreated the timeCreated of the neighbor
         */
        public ThisEnd(
                MeshObjectIdentifier identifier,
                long                 timeCreated )
        {
            theIdentifier  = identifier;
            theTimeCreated = timeCreated;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MeshObjectIdentifier getIdentifier()
        {
            return theIdentifier;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public long getTimeCreated()
        {
            return theTimeCreated;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void addAttributeName(
                String name )
        {
            theAttributeNames.add( name );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String [] getAttributeNames()
        {
            String [] ret = theAttributeNames.toArray( new String[ theAttributeNames.size() ]);
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void addAttributeValue(
                Serializable newValue )
        {
            theAttributeValues.add( newValue );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Serializable [] getAttributeValues()
        {
            Serializable [] ret = theAttributeValues.toArray( new Serializable[ theAttributeValues.size() ]);
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void addType(
                MeshTypeIdentifier identifier )
        {
            theMeshTypes.add( identifier );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MeshTypeIdentifier [] getTypeIdentifiers()
        {
            MeshTypeIdentifier [] ret = theMeshTypes.toArray( new MeshTypeIdentifier[ theMeshTypes.size() ]);
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void addPropertyType(
                MeshTypeIdentifier identifier )
        {
            thePropertyTypes.add( identifier );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MeshTypeIdentifier [] getPropertyTypes()
        {
            MeshTypeIdentifier [] ret = thePropertyTypes.toArray( new MeshTypeIdentifier[ thePropertyTypes.size() ]);
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void addPropertyValue(
                PropertyValue newValue )
        {
            thePropertyValues.add( newValue );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public PropertyValue [] getPropertyValues()
        {
            PropertyValue [] ret = thePropertyValues.toArray( new PropertyValue[ thePropertyValues.size() ]);
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void dump(
                Dumper d )
        {
            d.dump( this,
                    new String[] {
                        "theIdentifier",
                        "theAttributeNames",
                        "theAttributeValues",
                        "theMeshTypes",
                        "thePropertyTypes",
                        "thePropertyValues"
                    },
                    new Object[] {
                        theIdentifier,
                        theAttributeNames,
                        theAttributeValues,
                        theMeshTypes,
                        thePropertyTypes,
                        thePropertyValues
                    });
        }

        /**
         * The Identifier of the neighbor MeshObject.
         */
        protected final MeshObjectIdentifier theIdentifier;

        /**
         * The timeCreated of the neighbor MeshObject.
         */
        protected final long theTimeCreated;

        /**
         * The names of the Attributes.
         */
        protected ArrayList<String> theAttributeNames = new ArrayList<>();

        /**
         * The values of the Attributes that go with the Attribute names
         */
        protected ArrayList<Serializable> theAttributeValues = new ArrayList<>();

        /**
         * The MeshTypeIdentifiers of the RoleTypes.
         */
        protected ArrayList<MeshTypeIdentifier> theMeshTypes = new ArrayList<>();

        /**
         * The MeshTypeIdentifiers of the PropertyTypes.
         */
        protected ArrayList<MeshTypeIdentifier> thePropertyTypes = new ArrayList<>();

        /**
         * The PropertyValues that go with the PropertyTypes.
         */
        protected ArrayList<PropertyValue> thePropertyValues = new ArrayList<>();
    }
}
