//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.model.traverse.TraversePath;
import net.ubos.util.ArrayHelper;
import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * This abstract class factors out some functionality common to most TraversePathSets.
 */
public abstract class AbstractTraversePathSet
        implements
            TraversePathSet
{
    /**
     * Constructor.
     *
     * @param factory the MeshObjectSetFactory that created this TraversePathSet
     */
    protected AbstractTraversePathSet(
             MeshObjectSetFactory factory )
    {
        theFactory = factory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDebugName(
            String newValue )
    {
        theDebugName = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDebugName()
    {
        return theDebugName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshObjectSetFactory getFactory()
    {
        return theFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshBaseView getMeshBaseView()
    {
        if( isEmpty() ) {
            return null;
        }
        TraversePath first = iterator().next();
        return first.getMeshBaseView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TraversePath getSingleMember()
        throws
            IllegalStateException
    {
        TraversePath [] currentContent = getTraversePaths();
        switch( currentContent.length ) {
            case 0:
                return null;

            case 1:
                return currentContent[0];

            default:
                throw new IllegalStateException( "Set had " + currentContent.length + " members, not one" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<TraversePath> iterator()
    {
        return ArrayCursorIterator.<TraversePath>create( getTraversePaths() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(
            TraversePath testObject )
    {
        TraversePath [] currentContent = getTraversePaths();

        for( int i=0 ; i<currentContent.length ; ++i ) {
            if( testObject.equals( currentContent[i] )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        return size() == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        return getTraversePaths().length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int getSize()
    {
        return size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet getDestinationsAsSet()
    {
        // there may be duplicates
        TraversePath [] paths   = getTraversePaths();
        MeshObject   [] content = new MeshObject[ paths.length ];
        int count = 0;

        for( int i=0 ; i<paths.length ; ++i ) {
            boolean found   = false;
            MeshObject  current = paths[i].getLastMeshObject();
            for( int j=0 ; j<i ; ++j ) {
                if( content[j] == current ) {
                    found = true;
                    break;
                }
            }
            if( found ) {
                continue;
            }
            content[ count++ ] = current;
        }
        if( count < content.length ) {
            content = ArrayHelper.copyIntoNewArray( content, 0, count, MeshObject.class );
        }
        return theFactory.createOrderedImmutableMeshObjectSet( content ); // must be ordered, so subclasses can simply typecast
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet getStepAsSet(
            int index )
    {
        // there may be duplicates
        TraversePath [] paths   = getTraversePaths();
        MeshObject   [] content = new MeshObject[ paths.length ];
        int count = 0;

        for( int i=0 ; i<paths.length ; ++i ) {
            boolean found   = false;
            MeshObject  current = paths[i].getMeshObjectAt( index );
            for( int j=0 ; j<i ; ++j ) {
                if( content[j] == current ) {
                    found = true;
                    break;
                }
            }
            if( found ) {
                continue;
            }
            content[ count++ ] = current;
        }
        if( count < content.length ) {
            content = ArrayHelper.copyIntoNewArray( content, 0, count, MeshObject.class );
        }
        return theFactory.createOrderedImmutableMeshObjectSet( content ); // must be ordered, so subclasses can simply typecast
    }

    /**
     * The debug name, if any.
     */
    protected String theDebugName;

    /**
     * The MeshObjectSetFactory by which this MeshObjectSet was created.
     */
    protected MeshObjectSetFactory theFactory;
}
