//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set;

import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeWithProperties;

/**
 * A simple implementation of MeshObjectSelector that accepts all MeshObjects of
 * a certain type (or subtype).
 */
public class ByTypeMeshObjectSelector
        extends
            AbstractMeshObjectSelector
{
    /**
     * Factory method.
     *
     * @param filterType the type whose instances we accept, including subtypes
     * @return the created ByTypeMeshObjectSelector
     */
    public static ByTypeMeshObjectSelector create(
            EntityType filterType )
    {
        return new ByTypeMeshObjectSelector( filterType, DEFAULT_SUBTYPE_ALLOWED );
    }

    /**
     * Factory method.
     *
     * @param filterType the type whose instances we accept
     * @param subtypeAllowed  if true, we also accept instances of a subtype
     * @return the created ByTypeMeshObjectSelector
     */
    public static ByTypeMeshObjectSelector create(
            EntityType filterType,
            boolean    subtypeAllowed )
    {
        return new ByTypeMeshObjectSelector( filterType, subtypeAllowed );
    }

    /**
     * Construct one with the type whose instances we accept.
     *
     * @param filterType the type whose instances we accept
     * @param subtypeAllowed  if true, we also accept instances of a subtype
     */
    protected ByTypeMeshObjectSelector(
            EntityType filterType,
            boolean    subtypeAllowed )
    {
        theFilterType     = filterType;
        theSubtypeAllowed = subtypeAllowed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accepts(
            MeshObject candidate )
    {
        if( candidate == null ) {
            throw new IllegalArgumentException();
        }

        MeshTypeWithProperties [] candidateTypes = candidate.getEntityTypes();

        if( theSubtypeAllowed ) {
            for( int i=0 ; i<candidateTypes.length ; ++i ) {
                if( candidateTypes[i].isSubtypeOfOrEquals( theFilterType )) {
                    return true;
                }
            }
        } else {
            for( int i=0 ; i<candidateTypes.length ; ++i ) {
                if( candidateTypes[i].equals( theFilterType )) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Obtain the EntityType for whose subtypes we filter.
     *
     * @return the EntityType for whose subtypes we filter
     */
    public EntityType getFilterType()
    {
        return theFilterType;
    }

    /**
     * Determine whether subtypes are allowed.
     *
     * @return are subtypes allowed
     */
    public boolean isSubtypesAllowed()
    {
        return theSubtypeAllowed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof ByTypeMeshObjectSelector ) {
            ByTypeMeshObjectSelector realOther = (ByTypeMeshObjectSelector) other;

            if( theSubtypeAllowed != realOther.theSubtypeAllowed ) {
                return false;
            }
            if( !theFilterType.equals( realOther.theFilterType )) {
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 41 * hash + (this.theFilterType != null ? this.theFilterType.hashCode() : 0);
        hash = 41 * hash + (this.theSubtypeAllowed ? 1 : 0);
        return hash;
    }

    /**
     * The EntityType for whose instances we accept.
     */
    protected EntityType theFilterType;

    /**
     * Do we allow a subtype instance.
     */
    protected boolean theSubtypeAllowed;

    /**
     * Defines that by default, subtypes are allowed for this selector.
     */
    private static final boolean DEFAULT_SUBTYPE_ALLOWED = true;

    /**
     * Separates the components of the external form.
     */
    public static final String EXTERNAL_FORM_WITHIN_STEP_SEPARATOR = ",";
}
