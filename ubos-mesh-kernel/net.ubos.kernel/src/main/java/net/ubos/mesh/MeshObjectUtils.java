//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

/**
 * Collection of utility methods for MeshObjects.
 */
public abstract class MeshObjectUtils
{
    /**
     * Private constructor, this class cannot be instantiated.
     */
    private MeshObjectUtils()
    {}

    /**
     * Construct an array of MeshObjectIdentifiers from an array of MeshObjects.
     *
     * @param objs the MeshObjects
     * @return the MeshObjectIdentifiers of the MeshObjects
     */
    public static MeshObjectIdentifier [] meshObjectIdentifiers(
            MeshObject [] objs )
    {
        if( objs == null ) {
            return null;
        }
        MeshObjectIdentifier [] ret = new MeshObjectIdentifier[ objs.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = objs[i].getIdentifier();
        }
        return ret;
    }
}
