//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * This Exception is thrown if two MeshObjects are to become unrelated, but are not
 * currently related.
 */
public class NotRelatedException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param meshObject the first MeshObject
     * @param notNeighbor the second MeshObject
     */
    public NotRelatedException(
            MeshObject meshObject,
            MeshObject notNeighbor )
    {
        super( meshObject );

        theNotNeighbor = notNeighbor;
    }

    /**
     * Obtain the MeshObject at the other end of the relationship that does not exist.
     *
     * @return the MeshObject
     */
    public MeshObject getNotNeighbor()
    {
        return theNotNeighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject",
                    "theNotNeighbor"
                },
                new Object[] {
                    theMeshObject,
                    theNotNeighbor
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theNotNeighbor };
    }

    /**
     * The MeshObject at the other end of the relationship that does not exist.
     */
    protected final MeshObject theNotNeighbor;
}

