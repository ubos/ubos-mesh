//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.primitives.EntityType;

/**
 * Thrown if an operation requires a MeshObject to
 * be not blessed with a certain EntityType, but it is already.
 */
public class EntityBlessedAlreadyException
        extends
            BlessedAlreadyException
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject on which the illegal operation was attempted
     * @param type the EntityType of the already-existing blessing
     */
    public EntityBlessedAlreadyException(
            MeshObject obj,
            EntityType type )
    {
        super( obj, type );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType getType()
    {
        return (EntityType) super.getType();
    }
}
