//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace;

import net.ubos.util.cursoriterator.CursorIterator;

/**
 * <p>A MeshObjectIdentifierNamespaceMap that is used within a specific, narrow, context, such as
 * for serializing / deserializing MeshObjectIdentifiers to / from disk, or for a particular
 * data exchange.
 *
 * <p>A ContextualMeshObjectIdentifierNamespaceMap does not "own" its MeshObjectIdentifierNamespaces,
 * but delegates to a primary MeshObjectIdentifierNamespaceMap, assigning different local names
 * for the MeshObjectIdentifierNamespaces, however. With one exception: if there is a
 * MeshObjectIdentifierNamespace that does not have an external name, it is managed here,
 * as such as un-external-named MeshObjectIdentifierNamespace is unique to the context, and
 * cannot be global.
 */
public interface ContextualMeshObjectIdentifierNamespaceMap
{
    /**
     * Obtain the primary PrimaryMeshObjectIdentifierNamespaceMap that actually "owns" the
     * MeshObjectIdentifierNamespaces we only reference here.
     *
     * @return the primary PrimaryMeshObjectIdentifierNamespaceMap
     */
    public PrimaryMeshObjectIdentifierNamespaceMap getPrimaryMap();

    /**
     * Obtain the default MeshObjectIdentifierNamespace, which is the MeshObjectIdentifierNamespace
     * that is assumed if no other MeshObjectIdentifierNamespace has been specified. This
     * primarily servces to reduce storage needs.
     *
     * @return the default, or null
     */
    public MeshObjectIdentifierNamespace getDefaultNamespace();

    /**
     * Obtain the default MeshObjectIdentifierNamespace, which is the MeshObjectIdentifierNamespace
     * that is assumed if no other MeshObjectIdentifierNamespace has been specified. This
     * primarily servces to reduce storage needs. Create one if none has been allocated so far.
     *
     * @return the default
     */
    public MeshObjectIdentifierNamespace obtainDefaultNamespace();

    /**
     * Find an existing MeshObjectIdentifierNamespace, given a local name.
     *
     * @param localName the local name
     * @return the MeshObjectIdentifierNamespace, or null
     */
    public MeshObjectIdentifierNamespace findByLocalName(
            String localName );

    /**
     * Determine the local name of a MeshObjectIdentifierNamespace within this map.
     *
     * @param ns the namespace
     * @return the local name, or null if not known
     */
    public String getLocalNameFor(
            MeshObjectIdentifierNamespace ns );

    /**
     * Obtain the local name of a MeshObjectIdentifierNamespace within this map.
     * Unlike {@link getLocalNameFor}, this will create a new local name
     * if the MeshObjectIdentifierNamespace is not known yet
     *
     * @param ns the namespace
     * @return the local name
     */
    public String obtainLocalNameFor(
            MeshObjectIdentifierNamespace ns );

    /**
     * Obtain an iterator over the local names.
     *
     * @return the iterator
     */
    public CursorIterator<String> localNameIterator();

    /**
     * Register a MeshObjectIdentifierNamespace with this local name. This is for the
     * restore-from-disk use case etc.
     * Throws an exception if registered already.
     *
     * @param localName use this local name
     * @param ns the namespace
     */
    public void register(
            String                        localName,
            MeshObjectIdentifierNamespace ns );

    /**
     * The local name of the default Namespace.
     */
    public static final String DEFAULT_LOCAL_NAME = ""; // empty
}
