//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.security;

import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.MeshBaseView;

/**
 * This Exception indicates that a graph modification was attempted in a MeshBaseView
 * that was read-only.
 */
public class ReadOnlyMeshBaseViewException
        extends
            NotPermittedException
{
    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView on which the illegal operation was attempted
     */
    public ReadOnlyMeshBaseViewException(
            MeshBaseView mbv )
    {
        super( mbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return null;
    }
}
