//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.set.m;

import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.AbstractTraversePathSet;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.model.traverse.TraversePath;

/**
 * Factors out common functionality of in-memory TraversePathSet implementations.
 */
public abstract class AbstractMTraversePathSet
        extends
            AbstractTraversePathSet
{
    /**
     * Private constructor for subtypes only.
     *
     * @param factory the MeshObjectSetFactory that created this TraversePathSet
     */
    protected AbstractMTraversePathSet(
            MeshObjectSetFactory factory )
    {
        super( factory );
    }

    /**
     * Set the initial content of the set, for subclasses only.
     *
     * @param initialContent the initial content of the set
     */
    protected void setInitialContent(
            TraversePath [] initialContent )
    {
        MeshBaseView mbv = null;

        for( TraversePath current : initialContent ) {
            if( mbv == null ) {
                mbv = current.getMeshBaseView();
                WrongMeshBaseViewException.checkCompatible( theFactory.getMeshBase(), mbv );
            } else {
                WrongMeshBaseViewException.checkCompatible( current.getMeshBaseView(), mbv );
            }
        }
        currentContent = initialContent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final TraversePath [] getTraversePaths()
    {
        return currentContent;
    }

    /**
     * The content of this set.
     */
    protected TraversePath [] currentContent;
}
