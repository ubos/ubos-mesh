//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * This Exception is thrown when there is an attempt to access
  * an Attribute on a MeshObject that does not exist.
  */
public class IllegalAttributeException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject that did not have the Attribute
     * @param name the name of the Attribute that did not exist on this MeshObject
     */
    public IllegalAttributeException(
            MeshObject obj,
            String     name )
    {
        super( obj );

        theAttributeName = name;
    }

    /**
     * Obtain the name of the Attribute that did not exist.
     *
     * @return the name
     */
    public String getAttributeName()
    {
        return theAttributeName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshBaseView",
                    "meshObject",
                    "attributeName",
                    "attributes"
                },
                new Object[] {
                    theMeshBaseView,
                    theMeshObject,
                    theAttributeName,
                    theMeshObject.getAttributeNames()
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, theAttributeName };
    }

    /**
     * The name of the Attribute that did not exist on the MeshObject.
     */
    protected final String theAttributeName;
}
