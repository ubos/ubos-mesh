//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * <p>A pair of MeshObject and one or more PropertyTypes,
  *    representing one or more properties of this MeshObject.</p>
  *
  * <p>Note: while the sequence in the array has no semantic meaning for the PropertyGroup,
  *    it is essential in getter and setting functions to determine which property
  *    has which value.</p>
  */
public final class PropertyGroup
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance(PropertyGroup.class); // our own, private logger

    /**
     * Constructor.
     *
     * @param mo the MeshObject
     * @param ptGroup the PropertyTypeGroup
     */
    public PropertyGroup(
            MeshObject        mo,
            PropertyTypeGroup ptGroup )
    {
        theMeshObject        = mo;
        thePropertyTypeGroup = ptGroup;
    }

    /**
     * Obtain the MeshObject in this PropertyGroup.
     *
     * @return the MeshObject in this PropertyGroup
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * Obtain the PropertyTypeGroup.
     *
     * @return the PropertyTypeGroup
     */
    public PropertyTypeGroup getPropertyTypeGroup()
    {
        return thePropertyTypeGroup;
    }

    /**
     * Obtain the values of the Properties contained in the PropertyTypeGroup
     * for this MeshObject, in the same sequence as getPropertyTypeGroup().getPropertyTypes().
     *
     * @return the values of the Properties contained in the PropertyGroup for this MeshObject
     * @throws IllegalPropertyTypeException thrown if one of the PropertyTypes was not applicable to this MeshObject
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @see #setValues
     */
    public PropertyValue [] getValues()
        throws
            IllegalPropertyTypeException,
            NotPermittedException
    {
        PropertyType  [] pts = thePropertyTypeGroup.getContainedPropertyTypes();
        PropertyValue [] ret = new PropertyValue[ pts.length ];

        for( int i=pts.length-1 ; i>=0 ; --i ) {
            ret[i] = theMeshObject.getPropertyValue( pts[i] );
        }
        return ret;
    }

    /**
     * Set new values for the Properties contained in the PropertyTypeGroup
     * for this MeshObject, in the same sequence as getPropertyTypes().
     *
     * @param newValues the new values for the Properties on this MeshObject
     * @throws IllegalPropertyTypeException thrown if one of the PropertyTypes was not applicable to this MeshObject
     * @throws IllegalPropertyValueException thrown if the specified value is an illegal
     *         value for this PropertyType
     * @throws TransactionException thrown if this method is not invoked between
     *         proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @see #getValues
     */
    public void setValues(
            PropertyValue [] newValues )
        throws
            IllegalPropertyValueException,
            IllegalPropertyTypeException,
            TransactionException,
            NotPermittedException
    {
        PropertyType [] pts = thePropertyTypeGroup.getContainedPropertyTypes();

        log.assertLog( newValues.length == pts.length, "invalid length of new value for PropertyTypeGroup" );

        for( int i=pts.length-1 ; i>=0 ; --i ) {
            theMeshObject.setPropertyValue( pts[i], newValues[i] );
        }
    }

    /**
     * A convenience function that sets the value of the Properties contained
     * in the PropertyTypeGroup to a reasonable default. The reasonable default is obtained
     * from the PropertyType, and if not present, from the underlying DataType.
     * This is particularly useful when a default value is needed when the
     * Properties were previously null for this MeshObject.
     *
     * @throws IllegalPropertyTypeException thrown if one of the PropertyTypes was not applicable to this MeshObject
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @throws TransactionException thrown if this method is not invoked between
     *         proper Transaction boundaries
     */
    public void setDefaultValue()
        throws
            IllegalPropertyTypeException,
            NotPermittedException,
            TransactionException
    {
        PropertyType [] pts = thePropertyTypeGroup.getContainedPropertyTypes();

        for( int i=pts.length-1 ; i>=0 ; --i ) {
            try {
                theMeshObject.setPropertyValue(
                        pts[i],
                        pts[i].getDataType().instantiate() );

            } catch( IllegalPropertyValueException ex ) {
                log.error( ex );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof PropertyGroup ) {
            PropertyGroup realOther = (PropertyGroup) other;

            if( ! theMeshObject.equals( realOther.theMeshObject )) {
                return false;
            }
            return thePropertyTypeGroup.equals( realOther.thePropertyTypeGroup );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = theMeshObject.hashCode() ^ thePropertyTypeGroup.hashCode();
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject",
                    "thePropertyTypeGroup"
                },
                new Object[] {
                    theMeshObject,
                    thePropertyTypeGroup
                });
    }

    /**
     * The MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The PropertyTypeGroup.
     */
    protected final PropertyTypeGroup thePropertyTypeGroup;
}
