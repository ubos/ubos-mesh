//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.io.Serializable;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * A triple of a MeshObject, a neighbor MeshObject and the name of a RoleAttribute,
  * representing a RoleAttribute on this side of the relationship between this MeshObject
  * and its neighbor.
  */
public final class RoleAttribute
        implements
            CanBeDumped
{
    /**
      * Constructor. This constructor does not (and should not, due to possible setting later)
      * check whether this MeshObject actually carries the specified Attribute.
      *
      * @param mo the MeshObject
      * @param neighbor the neighbor MeshObject
      * @param ran the name of the RoleAttribute
      */
    public RoleAttribute(
            MeshObject mo,
            MeshObject neighbor,
            String     ran )
    {
        theMeshObject        = mo;
        theNeighbor          = neighbor;
        theRoleAttributeName = ran;
    }

    /**
     * Obtain the MeshObject.
     *
     * @return the MeshObject
     */
    public MeshObject getMeshObject()
    {
        return theMeshObject;
    }

    /**
     * Obtain the neighbor MeshObject.
     *
     * @return the MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
      * Obtain the name of the RoleAttribute.
      *
      * @return the name of the RoleAttribute
      */
    public String getRoleAttributeName()
    {
        return theRoleAttributeName;
    }

    /**
      * Obtain the value of the RoleAttribute.
      *
      * @return the value of the RoleAttribute
      * @throws NotPermittedException thrown if the caller did not have sufficient access rights
      * @see #setValue
      */
    public Serializable getValue()
       throws
            NotPermittedException
    {
        return theMeshObject.getAttributeValue( theRoleAttributeName );
    }

    /**
     * Set the value of the RoleAttribute.
     *
     * @param newValue the new value of the RoleAttribute
     * @throws NotPermittedException thrown if the caller did not have sufficient access rights
     * @throws TransactionException thrown if this method is not invoked between proper
     *         Transaction boundaries
     * @see #getValue
     */
    public void setValue(
            Serializable newValue )
        throws
            NotPermittedException,
            TransactionException
    {
        theMeshObject.setAttributeValue( theRoleAttributeName, newValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof RoleAttribute ) {
            RoleAttribute realOther = (RoleAttribute) other;

            return    theMeshObject.equals( realOther.theMeshObject )
                   && theNeighbor.equals( realOther.theNeighbor )
                   && theRoleAttributeName.equals( realOther.theRoleAttributeName );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = theMeshObject.hashCode() ^ theNeighbor.hashCode() ^ theRoleAttributeName.hashCode();
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMeshObject",
                    "theNeighbor",
                    "theAttributeName"
                },
                new Object[] {
                    theMeshObject,
                    theNeighbor,
                    theRoleAttributeName
                });
    }

    /**
     * The MeshObject.
     */
    protected final MeshObject theMeshObject;

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * The name of the RoleAttribute.
     */
    protected final String theRoleAttributeName;
}
