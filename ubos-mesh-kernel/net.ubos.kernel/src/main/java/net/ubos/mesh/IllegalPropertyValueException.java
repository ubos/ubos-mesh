//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import net.ubos.model.util.MeshTypeUtils;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * This Exception is thrown when a PropertyValue with an incorrect DataType is
  * specified as the new value of a Property, or when the new value of the
  * Property is outside of its allowed domain.
  */
public class IllegalPropertyValueException
        extends
            IllegalOperationTypeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param obj the MeshObject on which the violation was discovered
     * @param pt the PropertyType whose value was attempted to be set
     * @param illegalValue the value for the PropertyType that was illegal
     */
    public IllegalPropertyValueException(
            MeshObject    obj,
            PropertyType  pt,
            PropertyValue illegalValue )
    {
        super( obj );

        thePropertyType = pt;
        theIllegalValue = illegalValue;
    }

    /**
     * Obtain the PropertyType whose value was attempted to be set
     *
     * @return the PropertyType
     */
    public PropertyType getPropertyType()
    {
        return thePropertyType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshObject",
                    "propertyType",
                    "illegalValue",
                    "types"
                },
                new Object[] {
                    theMeshObject,
                    thePropertyType,
                    theIllegalValue,
                    MeshTypeUtils.meshTypeIdentifiersOrNull( theMeshObject )
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theMeshObject, thePropertyType, theIllegalValue };
    }

    /**
     * The PropertyType that was illegal on the MeshObject.
     */
    protected final PropertyType thePropertyType;

    /**
     * The illegal PropertyValue.
     */
    protected final PropertyValue theIllegalValue;
}
