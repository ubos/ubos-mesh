//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.json;

import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.ParseException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.HasSettableTypesPropertiesAttributes;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierDeserializer;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.json.AbstractPropertyValueJsonDecoder;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;

/**
 *
 */
public abstract class AbstractMeshObjectJsonDecoder
    extends
        AbstractPropertyValueJsonDecoder
    implements
        MeshObjectJsonTags
{
    private static final Log log = Log.getLogInstance(AbstractMeshObjectJsonDecoder.class );

    /**
     * Deserialize an ExternalizedMeshObject from a stream.
     *
     * @param r read from here
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return return the just-instantiated ExternalizedMeshObject
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public ParserFriendlyExternalizedMeshObject decodeExternalizedMeshObject(
            Reader                           r,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        JsonReader jr = new JsonReader( r );
        return decodeExternalizedMeshObject( jr, idDeserializer );
    }

    /**
     * Deserialize an ExternalizedMeshObject from a stream.
     *
     * @param jr read from here
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return return the just-instantiated ExternalizedMeshObject
     * @throws DecodingException thrown if a problem occurred during decoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    public ParserFriendlyExternalizedMeshObject decodeExternalizedMeshObject(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        return decodeExternalizedMeshObject( jr, null, idDeserializer );
    }

    /**
     * Deserialize an ExternalizedMeshObject from a stream but use the provided MeshObjectIdentifier (if any)
     *
     * @param jr read from here
     * @param id if given, use this MeshObjectIdentifier
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return return the just-instantiated ExternalizedMeshObject
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public ParserFriendlyExternalizedMeshObject decodeExternalizedMeshObject(
            JsonReader                       jr,
            MeshObjectIdentifier             id,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            ParserFriendlyExternalizedMeshObject ret = new ParserFriendlyExternalizedMeshObject();
            if( id != null ) {
                ret.setIdentifier( id );
            }

            jr.beginObject();

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case IDENTIFIER_TAG:
                        if( id == null ) {
                            ret.setIdentifier( idDeserializer.meshObjectIdentifierFromExternalForm( jr.nextString() ));
                        } else {
                            // should not happen
                            jr.nextString(); // ignore
                        }
                        break;

                    case TIME_CREATED_TAG:
                        ret.setTimeCreated( jr.nextLong() );
                        break;

                    case TIME_UPDATED_TAG:
                        ret.setTimeUpdated( jr.nextLong() );
                        break;

                    case STATE_TAG:
                        ret.setIsDead( STATE_VALUE_DEAD.equals( jr.nextString() ));
                        break;

                    case STRING_ATTRIBUTES_TAG:
                        decodeStringAttributesSection( ret, jr );
                        break;

                    case ATTRIBUTES_TAG:
                        decodeAttributesSection( ret, jr );
                        break;

                    case TYPES_TAG:
                        decodeTypesSection( ret, jr );
                        break;

                    case PROPERTIES_TAG:
                        decodePropertiesSection( ret, jr );
                        break;

                    case NEIGHBORS_TAG:
                        decodeNeighborsSection( ret, idDeserializer, jr );
                        break;

                    case MESH_OBJECT_HISTORY_TAG:
                        jr.beginArray();
                        while( jr.hasNext() ) {
                            ParserFriendlyExternalizedMeshObject historyObject = decodeExternalizedMeshObject( jr, id, idDeserializer );
                            ret.addHistorical( historyObject );
                        }
                        jr.endArray();
                }
            }
            jr.endObject();

            return ret;

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Helper method to decode the StringAttributes section.
     *
     * @param obj the ExternalizedMeshObject being assembled
     * @param jr read from here
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodeStringAttributesSection(
            HasSettableTypesPropertiesAttributes obj,
            JsonReader                           jr )
        throws
            DecodingException,
            IOException
    {
        jr.beginObject();

        while( jr.hasNext() ) {
            String name  = jr.nextName();
            String value = jr.nextString();

            obj.addAttributeName( name );
            obj.addAttributeValue( value );
        }

        jr.endObject();
    }

    /**
     * Helper method to decode the Attributes section.
     *
     * @param obj the ExternalizedMeshObject being assembled
     * @param jr read from here
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodeAttributesSection(
            HasSettableTypesPropertiesAttributes obj,
            JsonReader                           jr )
        throws
            DecodingException,
            IOException
    {
        jr.beginObject();

        while( jr.hasNext() ) {
            String name            = jr.nextName();
            String serializedValue = jr.nextString();

            Serializable value     = base64StringAsSerializable( serializedValue );

            obj.addAttributeName( name );
            obj.addAttributeValue( value );
        }

        jr.endObject();
    }

    /**
     * Helper method to decode the MeshTypes section.
     *
     * @param obj the ExternalizedMeshObject being assembled
     * @param jr read from here
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodeTypesSection(
            HasSettableTypesPropertiesAttributes obj,
            JsonReader                           jr )
        throws
            DecodingException,
            IOException
    {
        try {
            jr.beginArray();

            while( jr.hasNext() ) {
                String typeName = jr.nextString();

                MeshTypeIdentifier typeId = theTypeIdDeserializer.fromExternalForm( typeName );
                obj.addType( typeId );
            }

            jr.endArray();
        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Helper method to decode the Properties section.
     *
     * @param obj the ExternalizedMeshObject being assembled
     * @param jr read from here
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodePropertiesSection(
            HasSettableTypesPropertiesAttributes obj,
            JsonReader                           jr )
        throws
            DecodingException,
            IOException
    {
        try {
            jr.beginObject();

            while( jr.hasNext() ) {
                String typeName = jr.nextName();

                PropertyType  type  = ModelBase.SINGLETON.findPropertyType( theTypeIdDeserializer.fromExternalForm( typeName ));
                PropertyValue value = parsePropertyValue( type.getDataType(), jr );

                obj.addPropertyType( type.getIdentifier() );
                obj.addPropertyValue( value );
            }

            jr.endObject();

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );

        } catch( MeshTypeWithIdentifierNotFoundException ex ) {
            throw new DecodingException.Installation( ex );
        }
    }

    /**
     * Helper method to decode the neighbors section.
     *
     * @param obj the ExternalizedMeshObject being assembled
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @param jr read from here
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodeNeighborsSection(
            ParserFriendlyExternalizedMeshObject obj,
            MeshObjectIdentifierDeserializer     idDeserializer,
            JsonReader                           jr )
        throws
            DecodingException,
            IOException
    {
        try {
            jr.beginObject();

            while( jr.hasNext() ) {
                String neighborId = jr.nextName();

                ParserFriendlyExternalizedMeshObject.ThisEnd end         = null;
                long                                         timeCreated = -1L; // some default for old files

                jr.beginObject();
                while( jr.hasNext() ) {
                    String tagName = jr.nextName();

                    switch( tagName ) {
                        case TIME_CREATED_TAG:
                            timeCreated = jr.nextLong();
                            break;

                        case STRING_ATTRIBUTES_TAG:
                            if( end == null ) {
                                end = new ParserFriendlyExternalizedMeshObject.ThisEnd(
                                        idDeserializer.meshObjectIdentifierFromExternalForm( neighborId ),
                                        timeCreated );
                            }
                            decodeStringAttributesSection( end, jr );
                            break;

                        case ATTRIBUTES_TAG:
                            if( end == null ) {
                                end = new ParserFriendlyExternalizedMeshObject.ThisEnd(
                                        idDeserializer.meshObjectIdentifierFromExternalForm( neighborId ),
                                        timeCreated );
                            }
                            decodeAttributesSection( end, jr );
                            break;

                        case TYPES_TAG:
                            if( end == null ) {
                                end = new ParserFriendlyExternalizedMeshObject.ThisEnd(
                                        idDeserializer.meshObjectIdentifierFromExternalForm( neighborId ),
                                        timeCreated );
                            }
                            decodeTypesSection( end, jr );
                            break;

                        case PROPERTIES_TAG:
                            if( end == null ) {
                                end = new ParserFriendlyExternalizedMeshObject.ThisEnd(
                                        idDeserializer.meshObjectIdentifierFromExternalForm( neighborId ),
                                        timeCreated );
                            }
                            decodePropertiesSection( end, jr );
                            break;
                    }
                }
                jr.endObject();

                if( end == null ) {
                    end = new ParserFriendlyExternalizedMeshObject.ThisEnd(
                            idDeserializer.meshObjectIdentifierFromExternalForm( neighborId ),
                            timeCreated );
                }
                obj.addThisEnd( end );
            }

            jr.endObject();

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Knows how to deserialize MeshTypeIdentifiers.
     */
    protected static final MeshTypeIdentifierDeserializer theTypeIdDeserializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;
}
