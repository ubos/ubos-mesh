//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.text;

/**
 * Collects the known StringRepresentationSchemes.
 */
public interface StringRepresentationSchemeDirectory
{
    /**
     * Obtain a named StringRepresentationScheme.
     * 
     * @param schemeName name of the scheme
     * @return the StringRepresentationScheme, or a default. This never returns null.
     */
    public StringRepresentationScheme findScheme(
            String schemeName );
}
