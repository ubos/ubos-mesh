//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized.xml;

import net.ubos.mesh.externalized.MeshObjectDecoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class MeshObjectXmlDecoder
    extends
        AbstractMeshObjectXmlDecoder
    implements
        MeshObjectDecoder
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static MeshObjectXmlDecoder create()
    {
        return new MeshObjectXmlDecoder();
    }

    /**
     * Constructor. Use factory method.
     */
    protected MeshObjectXmlDecoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_OBJECT_XML_ENCODING_ID;
    }
}
