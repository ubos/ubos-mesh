//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace;

import net.ubos.util.cursoriterator.CursorIterator;

/**
 * <p>Collects and maintains a set of references to MeshObjectIdentifierNamespaces relevant in a
 * particular context. Instances of this type may or may not "own" the MeshObjectIdentifierNamespaces
 * they know of, so multiple instances of MeshObjectIdentifierNamespaceMap may know of the same
 * MeshObjectIdentifierNamespace.
 *
 * <p>Within this context, a MeshObjectIdentifierNamespace has a local name. This local name is only used
 * within this narrow context. In a different context, the same MeshObjectIdentifierNamespace will generally
 * have a different local name.
 *
 * <p>Such a context might be the serialization of MeshObjectIdentifiers to and from disk within the
 * implementation of a MeshBase, or for the purpose of data exchange from one place to another.
 *
 * <p>There is no obtainLocalNameFor( ns ), because they only way to create a MeshObjectIdentifierNamespace is
 * through an external name, and that automatically allocates a local name
 */
public interface PrimaryMeshObjectIdentifierNamespaceMap
{
    /**
     * Obtain the primary MeshObjectIdentifierNamespace at this location. This often has no external name,
     * and is the default for the primary MeshBase.
     *
     * @return the primary MeshObjectIdentifierNamespace
     */
    public MeshObjectIdentifierNamespace getPrimary();

    /**
     * Find an existing MeshObjectIdentifierNamespace, given an external name.
     *
     * @param externalName the external name
     * @return the found MeshObjectIdentifierNamespace, or null
     */
    public MeshObjectIdentifierNamespace findByExternalName(
            String externalName );

    /**
     * Obtain a MeshObjectIdentifierNamespace, given an external name. This is the same as
     * findByExternalName, except that if it does not exist yet, it will be created.
     *
     * @param externalName the external name
     * @return the found MeshObjectIdentifierNamespace, or a newly created one
     */
    public MeshObjectIdentifierNamespace obtainByExternalName(
            String externalName );

    /**
     * Find an existing MeshObjectIdentifierNamespace, given a local name.
     *
     * @param localName the local name
     * @return the MeshObjectIdentifierNamespace, or null
     */
    public MeshObjectIdentifierNamespace findByLocalName(
            String localName );

    /**
     * Determine the local name of a MeshObjectIdentifierNamespace within this map.
     *
     * @param ns the namespace
     * @return the local name, or null if not known
     */
    public String getLocalNameFor(
            MeshObjectIdentifierNamespace ns );

    /**
     * Obtain an iterator over the local names.
     *
     * @return the iterator
     */
    public CursorIterator<String> localNameIterator();

    /**
     * Obtain an iterator over the local names.
     *
     * @return the iterator
     */
    public CursorIterator<String> getLocalNameIterator();

    /**
     * Obtain an iterator over the namespaces.
     *
     * @return the iterator
     */
    public CursorIterator<MeshObjectIdentifierNamespace> namespaceIterator();

    /**
     * Obtain an iterator over the namespaces.
     *
     * @return the iterator
     */
    public CursorIterator<MeshObjectIdentifierNamespace> getNamespaceIterator();
}
