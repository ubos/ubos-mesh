//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh;

import java.util.HashMap;
import java.util.Map;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;

/**
 * A Java workaround to avoid creation of generic arrays.
 * Public so subclasses in other packages can use it.
 */
public class PropertiesMap
    extends
        HashMap<PropertyType,PropertyValue>
{
    /**
     * Create a copy of this object.
     *
     * @return the clone
     */
    public PropertiesMap clone()
    {
        PropertiesMap ret = new PropertiesMap();
        for( Map.Entry<PropertyType,PropertyValue> current : this.entrySet() ) {
            ret.put( current.getKey(), current.getValue() );
        }
        return ret;
    }

    /**
     * Create a copy of all objects in this array.
     *
     * @param original the array to be copied
     * @return copy
     */
    public static PropertiesMap [] cloneArray(
            PropertiesMap [] original )
    {
        if( original == null ) {
            return null;
        }
        PropertiesMap [] ret = new PropertiesMap[ original.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            PropertiesMap current = original[i];
            if( current != null ) {
                ret[i] = original[i].clone();
            }
        }
        return ret;
    }
}
