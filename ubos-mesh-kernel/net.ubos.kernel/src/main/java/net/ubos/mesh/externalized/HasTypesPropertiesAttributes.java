//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.externalized;

import java.io.Serializable;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;

/**
 * Simplifies parsing code that can now be applied to MeshObjects and
 * ThisEnds.
 */
public interface HasTypesPropertiesAttributes
{
    /**
     * Obtain the MeshObjectIdentifier
     *
     * @return the MeshObjectIdentifier
     */
    public abstract MeshObjectIdentifier getIdentifier();

    /**
     * Obtain the time the MeshObject was created.
     *
     * @return the time the MeshObject was created, in System.currentTimeMillis() format
     */
    public abstract long getTimeCreated();

    /**
     * Obtain the names of the Attributes
     *
     * @return the names of the Attributes
     * @see #setAttributeNames
     */
    public abstract String [] getAttributeNames();

    /**
     * Obtain the values of the Attributes, in the same sequence
     * as the Attribute names returned by getAttributeNames().
     *
     * @return the values of the Attributes
     * @see #getAttributeNames()
     */
    public abstract Serializable [] getAttributeValues();

    /**
     * Get the identifiers of the types at this end, i.e. EntityTypes or RoleTypes.
     *
     * @return the identifiers of the types
     */
    public abstract MeshTypeIdentifier [] getTypeIdentifiers();

    /**
     * Obtain the MeshTypeIdentifiers of the PropertyTypes.
     *
     * @return the MeshTypeIdentifiers of the PropertyTypes
     * @see #getPropertyValues()
     */
    public abstract MeshTypeIdentifier [] getPropertyTypes();

    /**
     * Obtain the PropertyValues of the properties, in the same sequence
     * as the PropertyTypes returned by getPropertyTypes.
     *
     * @return the PropertyValues of the properties
     * @see #getPropertyTypes()
     */
    public abstract PropertyValue [] getPropertyValues();
}
