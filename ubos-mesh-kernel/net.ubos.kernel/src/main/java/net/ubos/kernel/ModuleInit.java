//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.text.MeshObjectIdentifierStringifier;
import net.ubos.mesh.text.MeshObjectStringifier;
import net.ubos.mesh.text.MeshTypeIdentifierStringifier;
import net.ubos.mesh.text.MeshTypeStringifier;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import net.ubos.model.primitives.text.AnyPropertyValueStringifier;
import net.ubos.model.primitives.text.BlobMimeOptionsStringifier;
import net.ubos.model.primitives.text.BlobValueStringifier;
import net.ubos.model.primitives.text.BooleanValueStringifier;
import net.ubos.model.primitives.text.ColorValueStringifier;
import net.ubos.model.primitives.text.CurrencyValueStringifier;
import net.ubos.model.primitives.text.EnumeratedDataTypeDomainStringifier;
import net.ubos.model.primitives.text.EnumeratedValueChoiceHtmlStringifier;
import net.ubos.model.primitives.text.EnumeratedValueStringifier;
import net.ubos.model.primitives.text.ExtentValueStringifier;
import net.ubos.model.primitives.text.FloatValueStringifier;
import net.ubos.model.primitives.text.IntegerValueStringifier;
import net.ubos.model.primitives.text.MultiplicityValueStringifier;
import net.ubos.model.primitives.text.PointValueStringifier;
import net.ubos.model.primitives.text.StringValueStringifier;
import net.ubos.model.primitives.text.TimePeriodValueStringifier;
import net.ubos.model.primitives.text.TimeStampValueStringifier;
import net.ubos.util.text.ArrayStringifier;
import net.ubos.util.text.DefaultMessageFormatter;
import net.ubos.util.text.HtmlifyingDelegatingStringifier;
import net.ubos.util.text.InvalidStringifier;
import net.ubos.util.text.Stringifier;

/**
 * Activate this Module using diet4j. Used to initialze various pre-defined Stringifiers.
 */
public class ModuleInit
{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        DefaultMessageFormatter.TEXT_PLAIN.registerChildStringifiers( theTextPlainAddons );
        DefaultMessageFormatter.TEXT_HTML.registerChildStringifiers( theTextHtmlAddons );
        DefaultMessageFormatter.TEXT_HTML_EDIT.registerChildStringifiers( theTextHtmlEditAddons );
    }

    /**
     * Diet4j module deactivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleDeactivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        DefaultMessageFormatter.TEXT_HTML_EDIT.unregisterChildStringifiers( theTextHtmlEditAddons.keySet() );
        DefaultMessageFormatter.TEXT_HTML.unregisterChildStringifiers( theTextHtmlAddons.keySet() );
        DefaultMessageFormatter.TEXT_PLAIN.unregisterChildStringifiers( theTextPlainAddons.keySet() );
    }

    /**
     * The additional child Stringifiers defined in this Module for plain text.
     */
    private static final Map<String,Stringifier<?>> theTextPlainAddons = new HashMap<>();
    static {
        theTextPlainAddons.put( "blobpv",         BlobValueStringifier.create() );
        theTextPlainAddons.put( "booleanpv",      BooleanValueStringifier.create() );
        theTextPlainAddons.put( "colorpv",        ColorValueStringifier.create() );
        theTextPlainAddons.put( "currencypv",     CurrencyValueStringifier.create() );

        theTextPlainAddons.put( "enumpv",         EnumeratedValueStringifier.create( true ) );
        theTextPlainAddons.put( "enumchoice",     InvalidStringifier.create() );
        theTextPlainAddons.put( "enumdomain",     EnumeratedDataTypeDomainStringifier.create( EnumeratedValueStringifier.create( true ), ", " ));

        theTextPlainAddons.put( "extentpv",       ExtentValueStringifier.create() );
        theTextPlainAddons.put( "floatpv",        FloatValueStringifier.create() );
        theTextPlainAddons.put( "integerpv",      IntegerValueStringifier.create() );

        theTextPlainAddons.put( "mimelist",       BlobMimeOptionsStringifier.create( null,       null,                             ",",  null,        " (selected)" ));
        theTextPlainAddons.put( "mimeoptions",    InvalidStringifier.create() );

        theTextPlainAddons.put( "multiplicitypv", MultiplicityValueStringifier.create() );
        theTextPlainAddons.put( "pointpv",        PointValueStringifier.create() );
        theTextPlainAddons.put( "stringpv",       StringValueStringifier.create() );
        theTextPlainAddons.put( "timeperiodpv",   TimePeriodValueStringifier.create() );
        theTextPlainAddons.put( "timestamppv",    TimeStampValueStringifier.create() );

        // theTextPlainAddons.put( "type",             DataTypeStringifier.create() );
        theTextPlainAddons.put( "pv",             AnyPropertyValueStringifier.create() );
        // FIXME: we have a MultiplicityStringifier already; we should have one per subtype of PropertyValue,
        // and we should have one that takes any PropertyValue and delegates

        theTextPlainAddons.put( "meshobject",        MeshObjectStringifier.create() );
        theTextPlainAddons.put( "meshobjectarray",   ArrayStringifier.create( ", ", MeshObjectStringifier.create()));
        theTextPlainAddons.put( "meshobjectid",      MeshObjectIdentifierStringifier.create() );
        theTextPlainAddons.put( "meshobjectidarray", ArrayStringifier.create( ", ", MeshObjectIdentifierStringifier.create()));

        theTextPlainAddons.put( "meshtype",          MeshTypeStringifier.create() );
        theTextPlainAddons.put( "meshtypearray",     ArrayStringifier.create( ", ", MeshTypeStringifier.create()));
        theTextPlainAddons.put( "meshtypeid",        MeshTypeIdentifierStringifier.create() );
        theTextPlainAddons.put( "meshtypeidarray",   ArrayStringifier.create( ", ", MeshTypeIdentifierStringifier.create()));
    }

    /**
     * The additional child Stringifiers defined in this Module for HTML.
     */
    private static final Map<String,Stringifier<?>> theTextHtmlAddons = new HashMap<>();
    static {
        theTextHtmlAddons.put( "currency",         HtmlifyingDelegatingStringifier.create( CurrencyValueStringifier.create()));

        theTextHtmlAddons.put( "enum",             HtmlifyingDelegatingStringifier.create( EnumeratedValueStringifier.create( true ) ));
        theTextHtmlAddons.put( "enumdomain",       EnumeratedDataTypeDomainStringifier.create( HtmlifyingDelegatingStringifier.create( EnumeratedValueStringifier.create( true )), "<li>", "</li><li>", "</li>" ));
    }

    /**
     * The additional child Stringifiers defined in this Module for editing HTML.
     */
    private static final Map<String,Stringifier<?>> theTextHtmlEditAddons = new HashMap<>();
    static {
        theTextHtmlEditAddons.put( "currency",         HtmlifyingDelegatingStringifier.create( CurrencyValueStringifier.create()));

        theTextHtmlEditAddons.put( "enumchoice",       EnumeratedValueChoiceHtmlStringifier.create( "option" ));
        theTextHtmlEditAddons.put( "enumdomain",       InvalidStringifier.create() );

        theTextHtmlEditAddons.put( "mimeoptions",      BlobMimeOptionsStringifier.create( "<option>", "<option selected=\"selected\">", "\n", "</option>", "</option>" ));
    }
}
