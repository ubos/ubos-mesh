//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.zip;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import net.ubos.importer.handler.ImporterHandlerNode;

/**
 *
 */
public class ZipImporterHandlerDirectoryNode
    extends
        ImporterHandlerNode
{
    public ZipImporterHandlerDirectoryNode(
            ImporterHandlerNode parent,
            ZipFile             file,
            ZipEntry            entry )
    {
        super( parent );

        theZipFile = file;
        theZipEntry = entry;

        theName = DefaultZipImporterHandler.getLocalNameOf( theZipEntry );
    }

    @Override
    public String getName()
    {
        return theName;
    }

    @Override
    public File getFile()
    {
        return null;
    }

    @Override
    public long getTimeOfExport()
    {
        long ret = theZipEntry.getLastModifiedTime().toMillis();
        return ret;
    }

    @Override
    public boolean canGetStream()
    {
        return false;
    }

    @Override
    public InputStream createStream()
        throws
            IOException
    {
        throw new UnsupportedOperationException();
    }

    public ZipFile getZipFile()
    {
        return theZipFile;
    }

    public ZipEntry getZipEntry()
    {
        return theZipEntry;
    }

    protected final String theName;
    protected final ZipFile theZipFile;
    protected final ZipEntry theZipEntry;
}
