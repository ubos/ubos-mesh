//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.zip;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipFile;
import net.ubos.importer.handler.ImporterHandlerNode;

/**
 * A placeholder for a directory in a ZIP file that isn't actually contained in the ZIP file.
 * (apparently that happens.)
 */
public class ZipImporterHandlerMissingDirectoryNode
    extends
        ImporterHandlerNode
{
    public ZipImporterHandlerMissingDirectoryNode(
            ImporterHandlerNode parent,
            ZipFile             file,
            String              name )
    {
        super( parent );

        theZipFile = file;
        theName    = name;
    }

    @Override
    public String getName()
    {
        return theName;
    }

    @Override
    public File getFile()
    {
        return null;
    }

    @Override
    public long getTimeOfExport()
    {
        long ret = theParent.getTimeOfExport();
        return ret;
    }

    @Override
    public boolean canGetStream()
    {
        return false;
    }

    @Override
    public InputStream createStream()
        throws
            IOException
    {
        throw new UnsupportedOperationException();
    }

    public ZipFile getZipFile()
    {
        return theZipFile;
    }

    protected final String theName;
    protected final ZipFile theZipFile;
}
