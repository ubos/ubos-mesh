//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.remainders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;

/**
 * An ImporterHandler that does nothing other than rate the remaining, so far unhandled ImporterHandlerNodes,
 * based on a file that contains patterns with the expected paths for this Importer.
 * If one of the patterns matches, it returns one score; if none does, it returns anoter.
 * @author jernst
 */
public class RateRemaindersImporterHandler
    implements
        ImporterHandler
{
    /**
     * Constructor.
     *
     * @param patternStream read the expected Patterns from here
     * @param matchingScore if an ImporterHandlerNode matches a pattern, return this score
     * @param nonMatchingScore if no ImporterHandlerNode matches a pattern, return this score
     * @throws PatternSyntaxException the expression's syntax is invalid
     * @throws IOException the patternStream could not be read
     */
    public RateRemaindersImporterHandler(
            InputStream patternStream,
            double      matchingScore,
            double      nonMatchingScore )
        throws
            PatternSyntaxException,
            IOException
    {
        theMatchingScore    = matchingScore;
        theNonMatchingScore = nonMatchingScore;

        if( patternStream == null ) {
            thePatterns = new Pattern[0];

        } else {
            List<Pattern> patterns = new ArrayList<>();
            String        line;

            try( BufferedReader r = new BufferedReader( new InputStreamReader( patternStream )) ) {
                while( (line = r.readLine()) != null ) {
                    line = line.trim();
                    if( line.startsWith( "#" )) {
                        continue;
                    }
                    if( line.isEmpty()) {
                        continue;
                    }
                    Pattern found = Pattern.compile( line );
                    patterns.add( found );
                }
            }
            thePatterns = new Pattern[ patterns.size() ];
            patterns.toArray( thePatterns );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void augmentNode(
            ImporterHandlerNode currentNode )
    {
        // no op
    }

    /**
     * Analyze this File and report how well it matches the expectations of this
     * ImporterHandler.
     *
     * @param importCandidate the ImporterHandlerNode potentially to be imported
     */
    public void analyzeRecusively(
            ImporterHandlerNode importCandidate )
    {
        String path = importCandidate.getPath();

        boolean matched = false;
        for( int i=0 ; i<thePatterns.length ; ++i ) {
            Pattern currentPattern = thePatterns[i];
            if( currentPattern.matcher( path ).matches() ) {
                matched = true;
                break;
            }
        }
        importCandidate.setAnalyzeResult( matched );

        for( ImporterHandlerNode childCandidate : importCandidate.childIter() ) {
            analyzeRecusively( childCandidate );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        String path = toBeImported.getPath();

        for( int i=0 ; i<thePatterns.length ; ++i ) {
            Pattern currentPattern = thePatterns[i];
            if( currentPattern.matcher( path ).matches() ) {
                return theMatchingScore;
            }
        }
        return theNonMatchingScore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pattern getFilenamePattern()
    {
        return null;
    }

    /**
     * The score to return when a pattern matches
     */
    protected final double theMatchingScore;

    /**
     * The score to return when none of the patterns match
     */
    protected final double theNonMatchingScore;

    /**
     * The Patters to match against.
     */
    protected final Pattern [] thePatterns;
}
