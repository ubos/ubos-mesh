//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.regex.Pattern;

/**
 * Knows how to handle a particular type of content.
 */
public interface ImporterHandler
{
    /**
     * Augment the Node with what you know about child nodes.
     *
     * @param currentNode the current Node
     * @throws IOException an I/O problem occurred
     */
    public void augmentNode(
            ImporterHandlerNode currentNode )
        throws
            IOException;

    /**
     * Perform the import from a File or other data source, as represented by toBeImported.
     * This returns a double value, which can be 1) negative, meaning the data cannot be imported,
     * 1) 0, meaning the rating should not be used when constructing a compound rating, and
     * 3) above 0 all the way to 1, meaning it can be badly imported all the way to perfect.
     * There are symbolic constants defined below.
     *
     * @param toBeImported the to-be imported file
     * @param context context for this import
     * @return the score
     * @throws UnexpectedContentException the ImporterHandler was not prepared for the content it found
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException;

    /**
     * Obtain a regular expression that matches the relative path including filename that this ImporterContentHandler
     * believes it can handle.
     *
     * @return the Pattern
     */
    public Pattern getFilenamePattern();

    /**
     * A parsing score that represents parsing is impossible.
     */
    public static final double IMPOSSIBLE = -1.0d;

    /**
     * A parsing score that represents parsing is possible, but should not be rated
     * as it tells us nothing whether this is indeed a good match.
     */
    public static final double UNRATED = 0.0d;

    /**
     * A parsing score that represents that while parsing is not impossible, this looks wrong.
     * This is returned when finding data elements in the data source that we have never heard
     * of, and we doubt belong there. This happens in situations such as when an Importer is reading
     * a ZIP file that has lots of entries in it it is not expecting, asking whether this may be
     * a ZIP file of the wrong format.
     */
    public static final double MAYBE_WRONG = 0.01d;

    /**
     * A parsing score that represents parsing is possible, but given this is a fallback, the
     * score isn't so great.
     */
    public static final double FALLBACK = 0.2d;

    /**
     * A passing parsing score that's nothing to write home about.
     */
    public static final double SUFFICIENT = 0.4d;

    /**
     * Constant that represents parsing went perfectly.
     */
    public static final double PERFECT = 1.0d;

    /**
     * The Pattern that matches all filenames
     */
    public static final Pattern ANY_FILENAME_PATTERN = Pattern.compile( ".+" );

    /**
     * Knows how to create an InputStream.
     */
    public static interface InputStreamFactory
    {
        /**
         * Create a new InputStream.
         *
         * @return the InputStream
         * @throws IOException the InputStream could not be created
         */
        public InputStream create()
            throws
                IOException;
    }
}
