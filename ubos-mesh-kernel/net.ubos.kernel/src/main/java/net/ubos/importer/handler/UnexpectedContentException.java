//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

/**
 * Thrown if the to-be-imported file contained different syntax or content of the data than expected.
 */
public class UnexpectedContentException
    extends
        Exception
{
    /**
     * Constructor.
     *
     * @param msg the message
     */
    public UnexpectedContentException(
            String msg )
    {
        super( msg );
    }
}
