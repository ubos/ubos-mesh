//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.zip;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import net.ubos.importer.handler.ImporterHandlerNode;

/**
 *
 */
public class ZipImporterHandlerRegularFileNode
    extends
        ImporterHandlerNode
{
    public ZipImporterHandlerRegularFileNode(
            ImporterHandlerNode parent,
            ZipFile             file,
            ZipEntry            entry )
    {
        super( parent );

        theName     = DefaultZipImporterHandler.getLocalNameOf( entry );
        theZipFile  = file;
        theZipEntry = entry;
    }

    @Override
    public String getName()
    {
        return theName;
    }

    @Override
    public File getFile()
    {
        return null;
    }

    @Override
    public long getTimeOfExport()
    {
        long ret = theZipEntry.getLastModifiedTime().toMillis();
        return ret;
    }

    @Override
    public boolean canGetStream()
    {
        return true;
    }

    @Override
    public InputStream createStream()
            throws IOException
    {
        return theZipFile.getInputStream( theZipEntry );
    }

    public ZipFile getZipFile()
    {
        return theZipFile;
    }

    public ZipEntry getZipEntry()
    {
        return theZipEntry;
    }

    protected final String theName;
    protected final ZipFile theZipFile;
    protected final ZipEntry theZipEntry;
}
