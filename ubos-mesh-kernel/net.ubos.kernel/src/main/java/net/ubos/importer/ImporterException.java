//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer;

/**
 * Thrown if something went wrong during import. Inner classes provide details.
 */
public abstract class ImporterException
        extends
            Exception
{
    /**
     * Constructor for subclasses only.
     *
     * @param importer the Importer that could not import
     * @param message the message
     * @param cause the underlying cause, if any
     */
    public ImporterException(
            Importer  importer,
            String    message,
            Throwable cause )
    {
        super( message, cause );

        theImporter = importer;
    }

    /**
     * Obtain the Importer that threw this Exception.
     *
     * @return the Importer
     */
    public Importer getImporter()
    {
        return theImporter;
    }

    /**
     * The Importer that threw the Exception.
     */
    protected final Importer theImporter;

    /**
     * This importer does not support the file format.
     */
    public static class UnsupportedFileFormat
        extends
            ImporterException
    {
        /**
         * Constructor.
         *
         * @param importer the Importer
         */
        public UnsupportedFileFormat(
                Importer importer )
        {
            super( importer, "Format not recognized", null );

            theFormatName = null;
        }

        /**
         * Constructor.
         *
         * @param importer the Importer
         * @param formatName name of the unsupported format, if known
         */
        public UnsupportedFileFormat(
                Importer importer,
                String   formatName )
        {
            super( importer, "Format not supported: " + formatName, null );

            theFormatName = formatName;
        }

        /**
         * Name of the unsupported format.
         */
        protected final String theFormatName;
    }

    /**
     * This importer does not support parsing something that isn't a file.
     */
    public static class NotAFile
        extends
            ImporterException
    {
        /**
         * Constructor.
         *
         * @param importer the Importer
         */
        public NotAFile(
                Importer importer )
        {
            super( importer, "Not a file", null );
        }
    }

    /**
     * This importer does not support parsing something that isn't a directory.
     */
    public static class NotADirectory
        extends
            ImporterException
    {
        /**
         * Constructor.
         *
         * @param importer the Importer
         */
        public NotADirectory(
                Importer importer )
        {
            super( importer, "Not a directory", null );
        }
    }

    /**
     * The importer thinks it supports the format, but failed to parse.
     */
    public static class Failure
        extends
            ImporterException
    {
        /**
         * Constructor.
         *
         * @param importer the Importer
         * @param cause the underlying cause
         */
        public Failure(
                Importer  importer,
                Throwable cause )
        {
            super( importer, "Failed to parse", cause );
        }
    }
}
