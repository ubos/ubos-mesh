//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//


package net.ubos.importer.handler.directory;

import java.io.File;
import java.util.regex.Pattern;
import net.ubos.util.FileUtils;

/**
 * Knows how to handle a directory and all files in the tree below.
 * This will not create MeshObjects for intermediate directories.
 */
public class SkipIntermediateDirectoryImporterHandler
    extends
        AbstractDirectoryImporterHandler
{
    /**
     * Constructor with defaults
     */
    public SkipIntermediateDirectoryImporterHandler()
    {
        super( UNRATED, null );
    }

    /**
     * Constructor.
     *
     * @param okScore when importing works, what score should be reported
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     */
    public SkipIntermediateDirectoryImporterHandler(
            double  okScore,
            Pattern filenamePattern )
    {
        super( okScore, filenamePattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected File [] determineChildren(
            File start )
    {
        File [] ret = FileUtils.recursivelyFindFiles( start, (File f) -> f.isFile() );
        return ret;
    }
}