//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer;

import java.io.File;
import java.io.IOException;
import net.ubos.meshbase.history.EditableHistoryMeshBase;

/**
 * This interface is supported by classes that know how to import external data
 * into UBOS Mesh.
 */
public interface Importer
{
    /**
     * Obtain a name for this Importer. This is used for error messages.
     *
     * @return the name
     */
    public default String getName()
    {
        return getIdentifier();
    }

    /**
     * Obtain a unique identifier for this Importer.
     *
     * @return the identifier
     */
    public default String getIdentifier()
    {
        return getClass().getName();
    }

    /**
     * Analyze this File and report how well it matches the expectations of this
     * Importer.
     *
     * @param importCandidate the File potentially to be imported
     * @return the analysis report
     * @throws IOException an I/O problem occurred
     */
    public String analyze(
            File importCandidate )
        throws
            IOException;

    /**
     * Attempt to import data into this MeshBase. This method taking File needs to exist, because otherwise
     * we cannot have Importers that use ZipFile internally (it needs File, not InputStream or such).
     * If successful, returns an ImporterScore that explains how well it worked.
     *
     * @param toBeImported the File to be imported
     * @param defaultIdNamespace if given, use this is a default namespace for MeshObjectIdentifiers of imported MeshObjects
     * @param mb the MeshBase to import into
     * @return the score
     * @throws ImporterException thrown if a parsing problem occurred; details are in the cause
     * @throws IOException an I/O problem occurred
     */
    public ImporterScore importTo(
            File                    toBeImported,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            ImporterException,
            IOException;
}

