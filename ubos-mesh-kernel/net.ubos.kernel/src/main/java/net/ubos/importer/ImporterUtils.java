//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer;

import java.text.ParseException;
import java.util.function.Consumer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.EntityType;

/**
 * Useful utilities for Importers.
 */
public abstract class ImporterUtils
{
    /**
     * Keep this abstract.
     */
    private ImporterUtils() {}

    /**
     * Convenience function to find or create a MeshObject with this identifier.
     *
     * @param id the identifier
     * @param mb the MeshBase
     * @return the created or found MeshObject
     * @throws ParseException the identifier could not be parsed
     */
    public static MeshObject findOrCreate(
            String   id,
            MeshBase mb )
        throws
            ParseException
    {
        MeshObject ret = mb.findMeshObjectByIdentifier( id );
        if( ret == null ) {
            ret = mb.createMeshObject( id );
        }
        return ret;
    }

    /**
     * Convenience function to find or create a MeshObject with this identifier.
     *
     * @param id the identifier
     * @param mb the MeshBase
     * @return the created or found MeshObject
     */
    public static MeshObject findOrCreate(
            MeshObjectIdentifier id,
            MeshBase             mb )
    {
        MeshObject ret = mb.findMeshObjectByIdentifier( id );
        if( ret == null ) {
            ret = mb.createMeshObject( id );
        }
        return ret;
    }

    /**
     * Convenience function to find or create a MeshObject with this identifier and type.
     *
     * @param id the identifier
     * @param type the type
     * @param mb the MeshBase
     * @return the created or found MeshObject
     * @throws ParseException the identifier could not be parsed
     */
    public static MeshObject findOrCreate(
            String     id,
            EntityType type,
            MeshBase   mb )
        throws
            ParseException
    {
        return findOrCreate( id, type, mb, null );
    }

    /**
     * Convenience function to find or create a MeshObject with this identifier and type.
     *
     * @param id the identifier
     * @param type the type
     * @param mb the MeshBase
     * @return the created or found MeshObject
     */
    public static MeshObject findOrCreate(
            MeshObjectIdentifier id,
            EntityType           type,
            MeshBase             mb )
    {
        return findOrCreate( id, type, mb, null );
    }

    /**
     * Convenience function to find or create a MeshObject with this identifier and type,
     * and run some code if it had to be created.
     *
     * @param id the identifier
     * @param type the type
     * @param mb the MeshBase
     * @param runIfCreated run this code if it had to be created
     * @return the created or found MeshObject
     * @throws ParseException the identifier could not be parsed
     */
    public static MeshObject findOrCreate(
            String               id,
            EntityType           type,
            MeshBase             mb,
            Consumer<MeshObject> runIfCreated )
        throws
            ParseException
    {
        MeshObject ret = mb.findMeshObjectByIdentifier( id );
        if( ret == null ) {
            ret = mb.createMeshObject( id, type );

            if( runIfCreated != null ) {
                runIfCreated.accept( ret );
            }
        }
        return ret;
    }

    /**
     * Convenience function to find or create a MeshObject with this identifier and type,
     * and run some code if it had to be created.
     *
     * @param id the identifier
     * @param type the type
     * @param mb the MeshBase
     * @param runIfCreated run this code if it had to be created
     * @return the created or found MeshObject
     */
    public static MeshObject findOrCreate(
            MeshObjectIdentifier id,
            EntityType           type,
            MeshBase             mb,
            Consumer<MeshObject> runIfCreated )
    {
        MeshObject ret = mb.findMeshObjectByIdentifier( id );
        if( ret == null ) {
            ret = mb.createMeshObject( id, type );

            if( runIfCreated != null ) {
                runIfCreated.accept( ret );
            }
        }
        return ret;
    }

    /**
     * Convenience function to create a new MeshObject with a slightly different
     * MeshObjectIdentifier if the provided one is taken already.
     *
     * @param baseId the identifier that may be taken already
     * @param type the type
     * @param mb the MeshBase
     * @return the created MeshObject
     * @throws ParseException failed to parse the id
     */
    public static MeshObject createWithoutConflict(
            String     baseId,
            EntityType type,
            MeshBase   mb )
        throws
            ParseException
    {
        return createWithoutConflict( mb.createMeshObjectIdentifier( baseId ), type, mb );
    }

    /**
     * Convenience function to create a new MeshObject with a slightly different
     * MeshObjectIdentifier if the provided one is taken already.
     *
     * @param baseId the identifier that may be taken already
     * @param type the type
     * @param mb the MeshBase
     * @return the created MeshObject
     * @throws ParseException failed to parse the id
     */
    public static MeshObject createWithoutConflict(
            MeshObjectIdentifier baseId,
            EntityType           type,
            MeshBase             mb )
        throws
            ParseException
    {
        MeshObjectIdentifier candidateId = baseId;
        int                  count       = 0;

        while( mb.findMeshObjectByIdentifier( candidateId ) != null ) {
            candidateId = mb.createMeshObjectIdentifierBelow( baseId, String.valueOf( ++count ));
        }
        MeshObject ret = mb.createMeshObject( candidateId, type );
        return ret;
    }
}
