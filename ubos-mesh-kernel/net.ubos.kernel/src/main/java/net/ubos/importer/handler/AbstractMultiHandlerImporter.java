//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.AbstractImporter;
import net.ubos.importer.ImporterException;
import net.ubos.importer.ImporterScore;
import net.ubos.importer.handler.remainders.RateRemaindersImporterHandler;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.util.StringHelper;
import net.ubos.util.logging.Log;

/**
 * Implements an Importer that can read one or more files in several formats.
 */
public abstract class AbstractMultiHandlerImporter
    extends
        AbstractImporter
{
    /**
     * Constructor.
     *
     * @param name name of the Importer
     * @param contentHandlers they are tried in turn when encountering a piece of content
     * @param remaindersImporterHandler the ImporterHandler to run at the end, if any
     */
    protected AbstractMultiHandlerImporter(
            String                        name,
            ImporterHandler []            contentHandlers,
            RateRemaindersImporterHandler remaindersImporterHandler )
    {
        super( name );

        theContentHandlers   = contentHandlers;
        theRemaindersHandler = remaindersImporterHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String analyze(
            File importCandidate )
        throws
            IOException
    {
        if( theRemaindersHandler == null ) {
            return null;
        }

        ImporterHandlerNode rootNode;
        if( importCandidate.isFile() ) {
            rootNode = new ImporterHandlerRegularFileNode( null, importCandidate );
        } else if( importCandidate.isDirectory() ) {
            rootNode = new ImporterHandlerDirectoryNode( null, importCandidate );
        } else {
            return null;
        }

        createLayOfTheLand( rootNode );
        theRemaindersHandler.analyzeRecusively( rootNode );
        rootNode.calculcateTreeAnalyzeResults();

        StringBuilder almost = new StringBuilder();

        analyzeReport( rootNode, almost, 0 );

        return almost.toString();
    }

    /**
     * Recursively invoked report method for the analysis results.
     *
     * We attempt to summarize folders that are all the same.
     *
     * @param node the current node, recursively
     * @param buf append report here
     * @param level indentation level
     */
    protected void analyzeReport(
            ImporterHandlerNode node,
            StringBuilder       buf,
            int                 level )
    {
        String  name       = node.getName();
        Boolean result     = node.getAnalyzeResult();
        Boolean treeResult = node.getTreeAnalyzeResult();
        String  msg;

        if( treeResult == null ) {
            if( result == null ) {
                msg = "<not analyzed>";
            } else if( result ) {
                msg = "matches";
            } else {
                msg = "DOES NOT MATCH";
            }

        } else if( treeResult ) {
            msg = "matches";
            if( node.childIter().hasNext() ) {
                msg += " including subtree below";
            }

        } else {
            msg = "DOES NOT MATCH";
            if( node.childIter().hasNext() ) {
                msg += " (and neither does anything in the subtree below)";
            }
        }
        buf.append( StringHelper.blanks( level * 4 ));
        buf.append( name );
        buf.append( ": " );
        buf.append( msg );
        buf.append( "\n" );

        if( treeResult == null ) {
            for( ImporterHandlerNode child : node.childIter() ) {
                analyzeReport( child, buf, level+1 );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImporterScore importTo(
            File                    toBeImported,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            ImporterException,
            IOException
    {
        double score = ImporterScore.BEST;

        // Notes on algorithm:
        //
        // It would have been nice if we simply could have recursively walked the filesystem (and perhaps into
        // archive files like ZIP), determined the best ImporterHandler to run, run it and be done with it.
        //
        // Unfortunately, for real-world Importers (like the Facebook one), some ImporterHandlers really need to be
        // run before others, because they create some MeshObjects lots of others depend on. An example would be
        // that the file that contains the user's name really needs to be imported and the primary user MeshObject
        // created before, say, their social media posts should be parsed. This needs to happen regardless where
        // in the file system hierarchy that profile file resides.
        //
        // So that easy recursive algorithm won't work. Instead we first create a tree of Nodes from the filesystem
        // (and other things that we know how to unpack, like ZIP files), and then we run through all the
        // ImporterHandlers in sequence who get to try out all the nodes.

        ImporterHandlerNode rootNode;
        if( toBeImported.isFile() ) {
            rootNode = new ImporterHandlerRegularFileNode( null, toBeImported );
        } else if( toBeImported.isDirectory() ) {
            rootNode = new ImporterHandlerDirectoryNode( null, toBeImported );
        } else {
            return null;
        }

        createLayOfTheLand( rootNode );

        try {
            score = mb.execute( ( tx ) -> {

                rootNode.setHereMeshObject( mb.getHomeObject() );

                ImporterHandlerContext context = setupImport( rootNode, defaultIdNamespace, mb );

                for( ImporterHandler currentHandler : theContentHandlers ) {
                    runImporterHandlerOnAll( currentHandler, rootNode, context );
                }
                if( theRemaindersHandler != null ) {
                    runImporterHandlerOnAll( theRemaindersHandler, rootNode, context );
                }

                double score2 = completeImport( rootNode, context );
                return score2;
            } );

        } catch( Throwable t ) {
            throw new ImporterException.Failure( this, t );
        }

        Log log = getLog();
        if( log.isDebugEnabled() ) {
            log.debug( "Lay of the land:", rootNode.toString() );
        }

        if( score >= 0 ) {
            return new ImporterScore( this, score, null );
        } else {
            throw new ImporterException.UnsupportedFileFormat( this );
        }
    }

    /**
     * Invoked prior to an import, can be overridden by subclasses.
     *
     * @param rootNode the root node of the lay of the land
     * @param defaultIdNamespace if given, use this is a default namespace for MeshObjectIdentifiers of imported MeshObjects
     * @param mb the MeshBase to use
     * @return a suitable context object
     * @throws IOException thrown if an I/O problem occurred
     */
    protected ImporterHandlerContext setupImport(
            ImporterHandlerNode     rootNode,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            IOException
    {
        if( defaultIdNamespace != null ) {
            MeshObjectIdentifierNamespace thisNs = mb.getDefaultNamespace();
            thisNs.addExternalName( defaultIdNamespace );
            thisNs.setPreferredExternalName( defaultIdNamespace );
        }

        return new ImporterHandlerContext( rootNode.getTimeOfExport(), mb );
    }

    /**
     * Invoked after the import, can be overridden by subclasses.
     *
     * @param rootNode the root node of the lay of the land
     * @param context context for the import
     * @return the score of the import
     */
    protected double completeImport(
            ImporterHandlerNode    rootNode,
            ImporterHandlerContext context )
    {
        ScoreAggregator agg = new ScoreAggregator();
        agg.determineFrom( rootNode );

        return agg.getTotalScore();
    }

    /**
     * For this Node (invoked recursively, ask the ImporterHandlers to determine what child Nodes
     * also need to be considered. Given that it's only the ImporterHandlers that know about child nodes
     * (e.g. the contain of Zip files) we need to ask them. We do this by asking them to fill in the
     * child Nodes to this Node if they know how to.
     *
     * @param hereNode the current Node
     * @throws IOException an I/O problem occurred
     */
    protected void createLayOfTheLand(
            ImporterHandlerNode hereNode )
        throws
            IOException
    {
        if( hereNode.isCertainAboutChildren() ) {
            return;
        }

        for( ImporterHandler currentHandler : theContentHandlers ) {
            currentHandler.augmentNode( hereNode );

            if( hereNode.isCertainAboutChildren() ) {
                for( ImporterHandlerNode child : hereNode.childIter() ) {
                    createLayOfTheLand( child );
                }
                break;
            }
        }
        // we return with !isCertainAboutChildren() if none of the ImporterHandler knew what to do here
    }

    /**
     * Run the provided ImporterHandler on the entire tree.
     *
     * @param handler the ImporterHandler to run
     * @param hereNode the top of the hierarchy on which to run
     * @param context the importing context
     * @throws IOException an I/O problem occurred
     */
    protected void runImporterHandlerOnAll(
            ImporterHandler         handler,
            ImporterHandlerNode     hereNode,
            ImporterHandlerContext  context )
        throws
            IOException
    {
        if( !hereNode.isImportComplete() ) {
            Pattern pattern = handler.getFilenamePattern();
            if( pattern == null || pattern.matcher( hereNode.getPath() ).matches() ) {
                try {
                    double result = handler.doImport( hereNode, context );
                    if( result > ImporterHandler.IMPOSSIBLE ) {
                        hereNode.setImportCompleteWithScore( handler, result );
                    }
                } catch( UnexpectedContentException ex ) {
                    // ignore, this didn't work
                } catch( ParseException ex ) {
                    // ignore, this didn't work
                }
            }
        }

        for( ImporterHandlerNode childNode : hereNode.childIter() ) {
            runImporterHandlerOnAll( handler, childNode, context );
        }
    }

    /**
     * Get the appropriate logger for our subclass.
     *
     * @return the logger
     */
    protected Log getLog()
    {
        return Log.getLogInstance( getClass() );
    }

    /**
     * They are tried in sequence to handle a piece of content.
     */
    protected final ImporterHandler [] theContentHandlers;

    /**
     * The ImporterHandler to be run at the end, which handles what is left over (if any).
     */
    protected final RateRemaindersImporterHandler theRemaindersHandler;

    /**
     * Helper class that calculates the aggregate score of the import.
     */
    static class ScoreAggregator
    {
        /**
         * Perform the aggregation.
         *
         * @param node the start node
         */
        public void determineFrom(
                ImporterHandlerNode node )
        {
            ++theNodeCount;

            Double score = node.getCompletionScore();
            if( score == null ) {
                // didn't get run
                ++theMeaningfulNodeCount;

            } else if( score != 0.0 ) {
                ++theMeaningfulNodeCount;
                theAggregateScore += score; // may be positive or negative addition, but we skip the 0 scores
            }

            for( ImporterHandlerNode child : node.childIter() ) {
                determineFrom( child );
            }
        }

        /**
         * Determine the total score.
         *
         * @return total score
         */
        public double getTotalScore()
        {
            if( theMeaningfulNodeCount == 0 ) {
                return ImporterHandler.FALLBACK;
            } else {
                return theAggregateScore / theMeaningfulNodeCount;
            }
        }

        /**
         * Number of nodes encountered.
         */
        protected int theNodeCount;

        /**
         * Number of nodes encountered that have a meaningful (not WORST) score.
         */
        protected int theMeaningfulNodeCount;

        /**
         * Aggregate score.
         */
        protected double theAggregateScore;
    }
}
