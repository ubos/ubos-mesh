//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import net.ubos.meshbase.history.EditableHistoryMeshBase;

/**
 * A context object that is being provided to the ImporterHandlers when
 * importing. This may be subclassed, and that's the primary reason it exists.
 */

public class ImporterHandlerContext
{
    /**
     * Constructor.
     *
     * @param timeOfExport the time when the data source being imported was exported
     * @param mb the MeshBase
     */
    public ImporterHandlerContext(
            long                    timeOfExport,
            EditableHistoryMeshBase mb )
    {
        theTimeOfExport = timeOfExport;
        theMeshBase     = mb;
    }

    /**
     * Obtain the time when the data source being imported was exported.
     *
     * @return the time of export
     */
    public long getTimeOfExport()
    {
        return theTimeOfExport;
    }

    /**
     * Obtain the MeshBase.
     *
     * @return the MeshBase
     */
    public EditableHistoryMeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * The time of export.
     */
    protected final long theTimeOfExport;

    /**
     * The MeshBase.
     */
    protected final EditableHistoryMeshBase theMeshBase;
}
