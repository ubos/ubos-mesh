//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.zip;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.nonblessed.NonblessedZipUtils;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Knows how to handle a ZIP file.
 */
public class DefaultZipImporterHandler
    implements
        NonblessedImporterHandler
{
    /**
     * Constructor with defaults.
     */
    public DefaultZipImporterHandler()
    {
        theOkScore         = UNRATED;
        theFilenamePattern = DEFAULT_ZIP_FILENAME_PATTERN;
    }

    /**
     * Constructor.
     *
     * @param okScore when importing works, what score should be reported
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     */
    public DefaultZipImporterHandler(
            double  okScore,
            Pattern filenamePattern )
    {
        theOkScore         = okScore;
        theFilenamePattern = filenamePattern;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void augmentNode(
            ImporterHandlerNode hereNode )
        throws
            IOException
    {
        File f = hereNode.getFile();
        if( f == null || !f.isFile() ) {
            return;
        }

        // Leave ZIP file opened
        try {
            ZipFile zipFile = new ZipFile( f );
            Enumeration<? extends ZipEntry> zipIter = zipFile.entries();

            while( zipIter.hasMoreElements() ) {
                ZipEntry  entry = zipIter.nextElement();
                String    name  = entry.getName();
                while( name.endsWith( "/" )) { // directories apparently have trailing slashes
                    name = name.substring( 0, name.length()-1 );
                }
                while( name.startsWith( "/" )) { // that's against the ZIP spec, but happens, apparently
                    name = name.substring( 1 );
                }
                String [] path  = name.split( "/" );

                ImporterHandlerNode currentNode = hereNode;

                for( int i=0 ; i<path.length ; ++i ) {

                    ImporterHandlerNode childNode = currentNode.getChildByName( path[i] );
                    if( childNode == null ) {
                        ZipEntry childEntry = zipFile.getEntry( concat( path, i+1 ));
                        if( childEntry == null ) {
                            childEntry = zipFile.getEntry( concat( path, i+1 ) + "/" );
                        }
                        if( childEntry == null ) {
                            childNode = new ZipImporterHandlerMissingDirectoryNode( currentNode, zipFile, path[i] );

                        } else if( childEntry.isDirectory() ) {
                            childNode = new ZipImporterHandlerDirectoryNode( currentNode, zipFile, childEntry );

                        } else {
                            childNode = new ZipImporterHandlerRegularFileNode( currentNode, zipFile, childEntry );
                            childNode.setNoChildren();
                        }
                        currentNode.addChild( childNode );
                    }
                    currentNode = childNode;
                }
            }
        } catch( ZipException ex ) {
            // not a zip file -- ignore
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        File f = toBeImported.getFile();
        if( f == null || !f.isFile() ) {
            return IMPOSSIBLE;
        }

        MeshObject hereObject = toBeImported.getHereMeshObject();
        setRequiredTopAttributes(hereObject, NonblessedZipUtils.OBJECTTYPE_VALUE_ZIPFILE );

        doImportRecursively( toBeImported, context );

        return theOkScore;
    }

    protected void doImportRecursively(
            ImporterHandlerNode    hereNode,
            ImporterHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        MeshObject hereObject = hereNode.getHereMeshObject();
        MeshBase mb = context.getMeshBase();

        for( ImporterHandlerNode currentChildNode : hereNode.childIter() ) {

            MeshObject currentChild = mb.createMeshObjectBelow( hereObject, currentChildNode.getName());
            setRequiredNonTopAttributes(currentChild, OBJECTTYPE_VALUE_ZIPENTRY );
            setRequiredRoleAttributes(hereObject, currentChild, RELATIONSHIP_ROLE_VALUE_ZIP_CONTAINS, currentChildNode.getName(), null );

            currentChildNode.setHereMeshObject( currentChild );

            doImportRecursively( currentChildNode, context );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pattern getFilenamePattern()
    {
        return theFilenamePattern;
    }

    /**
     * Helper to concatenate a path from 0 to n.
     *
     * @param path the path
     * @param n the number of elements
     * @return the concatenated path
     */
    protected static String concat(
            String [] path,
            int       n )
    {
        StringBuilder almost = new StringBuilder();
        String        sep    = "";

        for( int i=0 ; i<n ; ++i ) {
            almost.append( sep );
            almost.append( path[i] );
            sep = "/";
        }
        return almost.toString();
    }

    /**
     * Helper method to determine the local name of the ZipEntry.
     *
     * @param entry the ZipEntry
     * @return the local name part of the part
     */
    public static String getLocalNameOf(
            ZipEntry entry )
    {
        String name = entry.getName();

        while( name.endsWith( "/" )) {
            name = name.substring( 0, name.length()-1 );
        }
        int lastSlash = name.lastIndexOf( "/" );
        if( lastSlash >= 0 ) {
            name = name.substring( lastSlash+1 );
        }
        return name;
    }

    /**
     * When importing works, what score should be reported.
     */
    protected final double theOkScore;

    /**
     * The path and filename pattern this ImporterContentHandler can handle.
     */
    protected final Pattern theFilenamePattern;

    /**
     * The default pattern of file names we support.
     */
    public static final Pattern DEFAULT_ZIP_FILENAME_PATTERN = Pattern.compile( ".*\\.zip$", Pattern.CASE_INSENSITIVE );

    /**
     * Value for IMPORTER_OJBECT_TYPE_ATTRIBUTE that indicates this MeshObject was created
     * from an entry in the ZIP file.
     */
    public static final String OBJECTTYPE_VALUE_ZIPENTRY = "zip-entry";

    /**
     * Value for IMPORTER_PARENT_CHILD_RELATIONSHIP_ROLE_ATTRIBUTE that indicates the parent MeshObject
     * was created either from a ZipFile, or a file contained in a ZipFile and the child MeshObject from
     * a contained file (not saying anything about the contained file's type; could be another directory or plain file)
     */
    public static final String RELATIONSHIP_ROLE_VALUE_ZIP_CONTAINS = "zipdirectory-contains";

}