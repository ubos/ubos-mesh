//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.file;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.nonblessed.NonblessedUtils;
import net.ubos.util.StreamUtils;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Knows how to handle other content as blob. This is untyped; compare TypedBlobImporterHandler.
 */
public class DefaultFileImporterHandler
    extends
        AbstractBasicFileImporterHandler
    implements
        NonblessedImporterHandler
{
    /**
     * Constructor with default.
     *
     * @param okScore when importing works, what score should be reported
     */
    public DefaultFileImporterHandler(
            double okScore )
    {
        super( ANY_FILENAME_PATTERN, okScore );

        theStringExtensions = new HashSet<>();
        for( String ext : STRING_FILNAME_EXTENSIONS ) {
            theStringExtensions.add( ext );
        }
        theMaxBytes = DEFAULT_MAX_BYTES;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     */
    public DefaultFileImporterHandler(
            String    filenamePattern,
            double    okScore )
    {
        super( Pattern.compile( filenamePattern ), okScore );

        theStringExtensions = new HashSet<>();
        for( String ext : STRING_FILNAME_EXTENSIONS ) {
            theStringExtensions.add( ext );
        }
        theMaxBytes = DEFAULT_MAX_BYTES;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param stringExtensions the set of filename extensions that should be imported as String, not bytes
     * @param okScore when importing works, what score should be reported
     */
    public DefaultFileImporterHandler(
            String    filenamePattern,
            String [] stringExtensions,
            double    okScore )
    {
        super( Pattern.compile( filenamePattern ), okScore );

        theStringExtensions = new HashSet<>();
        for( String ext : stringExtensions ) {
            theStringExtensions.add( ext );
        }
        theMaxBytes = DEFAULT_MAX_BYTES;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param stringExtensions the set of filename extensions that should be imported as String, not bytes
     * @param okScore when importing works, what score should be reported
     */
    public DefaultFileImporterHandler(
            Pattern     filenamePattern,
            Set<String> stringExtensions,
            double      okScore )
    {
        super( filenamePattern, okScore );

        theStringExtensions = stringExtensions;
        theMaxBytes         = DEFAULT_MAX_BYTES;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     * @param maxLength the maximum length of content that will be imported
     */
    public DefaultFileImporterHandler(
            Pattern filenamePattern,
            double  okScore,
            int     maxLength )
    {
        super( filenamePattern, okScore );

        theMaxBytes = maxLength;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream() ) {
            return IMPOSSIBLE;
        }

        double ret = theOkScore;

        MeshObject hereObject = toBeImported.getHereMeshObject();

        setRequiredTopAttributes(hereObject, NonblessedUtils.OBJECTTYPE_VALUE_FILE );

        byte [] data    = StreamUtils.slurp( toBeImported.createStream(), theMaxBytes );
        String  ext     = toBeImported.getName();
        int     lastDot = ext.lastIndexOf(  '.' );

        if( lastDot >= 0 ) {
            ext = ext.substring( lastDot+1 );
        }
        ext = ext.toLowerCase();
        if( theStringExtensions.contains( ext )) {
            try {
                hereObject.setAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE, new String( data ));

            } catch( Throwable ex ) {
                // in spite of the file extension, String did not work
                hereObject.setAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE, data );
            }

        } else {
            hereObject.setAttributeValue(NonblessedUtils.CONTENT_ATTRIBUTE, data );
        }

        if( data.length >= theMaxBytes ) {
            hereObject.setAttributeValue( IMPORTER_CONTENT_TRUNCATED_ATTRIBUTE, true );
            ret = Math.min( ret, SUFFICIENT );
        }
        return ret;
    }

    /**
     * The filename extensions that are to be imported as String.
     */
    protected Set<String> theStringExtensions;

    /**
     * The maximum number of bytes we read.
     */
    protected final int theMaxBytes;

    /**
     * The default set of file extensions of files that are to be imported as String, not byte[]
     */
    public static final String [] STRING_FILNAME_EXTENSIONS = new String[] {
            "txt",
            "md",
            "html",
            "css",
            "json",
            "js",
            "xml"
    };

    /**
     * Default maximum number of bytes that we read.
     */
    public static final int DEFAULT_MAX_BYTES = 1<<26; // 64 MiB

    /**
     * Name of the Attribute on a created MeshObject whose value indicates that the content has been truncated.
     */
    public static final String IMPORTER_CONTENT_TRUNCATED_ATTRIBUTE = "importer-content-truncated";
}
