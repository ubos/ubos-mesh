//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.noop;

import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Does nothing. This allows us to define an ImporterHandler that matches before some other
 * ImporterHandler, and prevents it from running.
 */
public class NoOpImporterHandler
    extends
        AbstractBasicFileImporterHandler
    implements
        NonblessedImporterHandler
{
    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     * @param deleteHereObject if true, delete the here-MeshObject
     */
    public NoOpImporterHandler(
            String  filenamePattern,
            double  okScore,
            boolean deleteHereObject )
    {
        super( Pattern.compile( filenamePattern ), okScore );

        theDeleteHereObject = deleteHereObject;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     * @param deleteHereObject if true, delete the here-MeshObject
     */
    public NoOpImporterHandler(
            Pattern filenamePattern,
            double  okScore,
            boolean deleteHereObject )
    {
        super( filenamePattern, okScore );

        theDeleteHereObject = deleteHereObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void augmentNode(
            ImporterHandlerNode currentNode )
    {
        // no op -- FIXME?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        MeshObject hereObject = toBeImported.getHereMeshObject();

        if( theDeleteHereObject ) {
            hereObject.getMeshBase().deleteMeshObject( hereObject );
        } else {
            setRequiredTopAttributes( hereObject, null );
        }

        return theOkScore;
    }

    /**
     * If true, delete the here-MeshObject.
     */
    protected final boolean theDeleteHereObject;
}
