//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.directory;

import java.io.File;
import java.util.regex.Pattern;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Knows how to handle a directory and all files in the tree below.
 * This will create MeshObjects for each subdirectory and contained files.
 */
public class DefaultDirectoryImporterHandler
    extends
        AbstractDirectoryImporterHandler
{
    /**
     * Constructor with defaults.
     */
    public DefaultDirectoryImporterHandler()
    {
        super( UNRATED, null );
    }

    /**
     * Constructor.
     *
     * @param okScore when importing works, what score should be reported
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     */
    public DefaultDirectoryImporterHandler(
            double  okScore,
            Pattern filenamePattern )
    {
        super( okScore, filenamePattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected File [] determineChildren(
            File start )
    {
        File [] ret = start.listFiles(NO_DOT_OR_OTHER_STRANGE_FILES_FILTER );
        return ret;
    }
}