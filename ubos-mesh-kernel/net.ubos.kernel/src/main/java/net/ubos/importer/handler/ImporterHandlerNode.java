//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import net.ubos.mesh.MeshObject;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;

/**
 * A Node in the processing tree that we create.
 *
 * This is used in two different situations:
 * * When analyzing with the RateRemainderImporterHandler
 * * when actually doing the importing
 * So it carries properties to support both, although not all of them will be used in either mode.
 * (This cuts down on the number of classes we need to create, at the expense of a bit of clarity)
 */
public abstract class ImporterHandlerNode
{
    /**
     * Constructor.
     *
     * @param parent the parent node
     */
    public ImporterHandlerNode(
            ImporterHandlerNode parent )
    {
        theParent = parent;
    }

    /**
     * Obtain the name of this node.
     *
     * @return the name
     */
    public abstract String getName();

    /**
     * Obtain the path from the top node of the tree to this node.
     *
     * @return the path
     */
    public String getPath()
    {
        if( theParent == null ) {
            return getName();
        } else {
            return theParent.getPath() + "/" + getName();
        }
    }

    /**
     * Obtain this node's parent node.
     *
     * @return the parent, if any
     */
    public ImporterHandlerNode getParent()
    {
        return theParent;
    }

    /**
     * Obtain the time when the data source being imported was exported.
     *
     * @param the time of export
     */
    public abstract long getTimeOfExport();

    /**
     * Obtain the node's ultimate parent node. That may be itself.
     *
     * @return the ultimate parent
     */
    public ImporterHandlerNode getAncestor()
    {
        if( theParent == null ) {
            return this;
        } else {
            return theParent.getAncestor();
        }
    }

    /**
     * We have determined that this Node does not have children.
     */
    public void setNoChildren()
    {
        if( theChildren != null ) {
            throw new IllegalStateException();
        }
        theChildren = EMPTY_LIST;
    }

    /**
     * Set the child nodes for this node.
     *
     * @param children the child Nodes
     */
    public void setChildNodes(
            ImporterHandlerNode [] children )
    {
        if( theChildren != null ) {
            throw new IllegalStateException();
        }
        theChildren = new ArrayList<>();

        HashSet<String> check = new HashSet<>();

        for( ImporterHandlerNode child : children ) {
            if( check.contains( child.getName() )) {
                throw new IllegalArgumentException( "Adding more than one child with name: " + child.getName() );
            }
            theChildren.add( child );
            check.add( child.getName() );
        }
    }

    /**
     * Add a single child node for this node.
     *
     * @param child the child node to be added
     */
    public void addChild(
            ImporterHandlerNode child )
    {
        if( theChildren == null ) {
            theChildren = new ArrayList<>();
        }

        for( ImporterHandlerNode already : theChildren ) {
            if( already.getName().equals( child.getName() )) {
                throw new IllegalArgumentException( "Have child with name already: " + child.getName() );
            }
        }
        theChildren.add( child );
    }

    /**
     * Obtain an iterator over the child nodes.
     *
     * @return the CursorIterator
     */
    public CursorIterator<ImporterHandlerNode> childIter()
    {
        if( theChildren == null ) { // can happen if a directory in a ZIP file contains no files
            return ZeroElementCursorIterator.create();
        } else {
            return ArrayListCursorIterator.create( theChildren );
        }
    }

    /**
     * If true, the children and their descendants have been filled in.
     * If false, they still need to be determined;
     *
     * @return true if all children and their descendants have been filled in
     */
    public boolean isCertainAboutChildren()
    {
        return theChildren != null;
    }

    /**
     * Obtain a child node of this node by its local name.
     * This works whether or not this Node is certain about its children at this time.
     *
     * @param name name of the child node
     * @return the child node, or null
     */
    public ImporterHandlerNode getChildByName(
            String name )
    {
        if( theChildren == null ) {
            return null;
        }

        for( ImporterHandlerNode child : theChildren ) {
            if( child.getName().equals( name )) {
                return child;
            }
        }
        return null;
    }


    /**
     * If this node corresponds to a file (any kind, including directories) in the filesystem.
     * this will return the corresponding File object
     *
     * @return the File object, if any
     */
    public abstract File getFile();

    /**
     * If this node corresponds to content that can be streamed (e.g. a file may be), this will
     * return true.
     *
     * @return true if the conntent can be streamed
     */
    public abstract boolean canGetStream();

    /**
     * If this node corresponds to content that can be streamed, open a stream for that content.
     *
     * @return the created stream
     * @throws IOException an I/O problem occurred
     */
    public abstract InputStream createStream()
        throws
            IOException;

    /**
     * An importer has successfully completed the import corresponding to this node.
     *
     * @param handler the ImporterHandler that was successfully run
     * @param completionScore the score of the import
     */
    public void setImportCompleteWithScore(
            ImporterHandler handler,
            double          completionScore )
    {
        theCompletionHandler = handler;
        theCompletionScore   = completionScore;
    }

    /**
     * Determine whether the import corresponding to this node was completed.
     *
     * @return true if the import was completed
     */
    public boolean isImportComplete()
    {
        return theCompletionScore != null;
    }

    /**
     * Obtain the completion score of the import corresponding to this node.
     *
     * @return the score, if any. This can be positive, negative and null.
     */
    public Double getCompletionScore()
    {
        return theCompletionScore;
    }

    /**
     * If there is a MeshObject that corresponds to this node, return it.
     *
     * @return the MeshObject
     */
    public MeshObject getHereMeshObject()
    {
        return theHereMeshObject;
    }

    /**
     * Enable an importer to set the MeshObject it created that corresponds to this node.
     *
     * @param hereMeshObject the MeshObject
     */
    public void setHereMeshObject(
            MeshObject hereMeshObject )
    {
        if( theHereMeshObject != null ) {
            throw new IllegalStateException();
        }
        theHereMeshObject = hereMeshObject;
    }

    /**
     * Set the result of the analysis.
     *
     * @param result the result
     */
    public void setAnalyzeResult(
            boolean result )
    {
        theAnalyzeResult = result;
    }

    /**
     * Obtain the result of the analysis. This may return true -- yes, matched --, or false
     * -- no, not matched -- or null -- the analysis was not run.
     *
     * @return the result
     */
    public Boolean getAnalyzeResult()
    {
        return theAnalyzeResult;
    }

    public Boolean getTreeAnalyzeResult()
    {
        return theTreeAnalyzeResult;
    }

    public void calculcateTreeAnalyzeResults()
    {
        for( ImporterHandlerNode childNode : childIter() ) {
            childNode.calculcateTreeAnalyzeResults();
        }

        theTreeAnalyzeResult = theAnalyzeResult;
        for( ImporterHandlerNode childNode : childIter() ) {
            Boolean childValue = childNode.getTreeAnalyzeResult();
            if( theTreeAnalyzeResult == null ) {
                theTreeAnalyzeResult = childValue;

            } else if( !Objects.equals( theTreeAnalyzeResult, childValue ) ) {
                // not the same
                theTreeAnalyzeResult = null;
                return;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();

        recursiveToString( buf, 0 );

        return buf.toString();
    }

    /**
     * Internal helper to recursively create the String format.
     *
     * @param buf append content here
     * @param level how far down are we in the recursion stack
     */
    protected void recursiveToString(
            StringBuilder buf,
            int           level )
    {
        for( int i=0 ; i<level ; ++i ) {
            buf.append( "    " );
        }
        buf.append( getName() );
        buf.append( ": " );
        buf.append( getClass().getName() );
        buf.append( " - completion: " );
        if( theCompletionScore == null ) {
            buf.append( "no" );
        } else {
            buf.append( theCompletionScore );
            buf.append( " by " );
            buf.append( theCompletionHandler.getClass().getName() );
        }
        buf.append( ", here: " );
        if( theHereMeshObject == null ) {
            buf.append( "null" );
        } else {
            buf.append( theHereMeshObject.getIdentifier().getLocalId() );
        }
        buf.append( "\n" );

        if( theChildren == null ) {
            for( int i=0 ; i<level ; ++i ) {
                buf.append( "    " );
            }
            buf.append( "    <null children>\n" );
        } else {
            for( ImporterHandlerNode child : theChildren ) {
                child.recursiveToString( buf, level+1 );
            }
        }
    }

    /**
     * This node's parent node, if any.
     */
    protected final ImporterHandlerNode theParent;

    /**
     * This node's child nodes. This is null as long as we don't know whether there are any.
     */
    protected ArrayList<ImporterHandlerNode> theChildren = null;

    /**
     * The importer that successfully parsed this node.
     * Null if not processed yet.
     */
    protected ImporterHandler theCompletionHandler;

    /**
     * The completion score of the importer when successfully processing this node.
     * May be null if not processed.
     */
    protected Double theCompletionScore = null;

    /**
     * The MeshObject created by the importer for this node.
     */
    protected MeshObject theHereMeshObject;

    /**
     * Result of the analyze on this node.
     */
    protected Boolean theAnalyzeResult;

    /**
     * Result of the analyze on this node and all of its child nodes.
     * This is True of all child nodes are True; False if all child nodes are False;
     * Null otherwise;
     */
    protected Boolean theTreeAnalyzeResult;

    /**
     * Convenience constant to avoid object allocation.
     */
    public static final ImporterHandlerNode [] EMPTY_ARRAY = {};

    /**
     * Convenience constant to avoid object allocation.
     */
    protected static final ArrayList<ImporterHandlerNode> EMPTY_LIST = new ArrayList<>();
}

