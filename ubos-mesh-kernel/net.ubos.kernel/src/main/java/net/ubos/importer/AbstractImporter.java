//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer;

/**
 * Functionality useful to many implementations of Importer.
 */
public abstract class AbstractImporter
    implements
        Importer
{
    /**
     * Constructor.
     *
     * @param name name of the Importer
     */
    public AbstractImporter(
            String name )
    {
        theName = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getIdentifier()
    {
        return getClass().getSimpleName(); // default
    }

    /**
     * Name of the Importer.
     */
    protected final String theName;
}

