//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer;

/**
 * Represents how well a given Importer is able to import a certain File or other data source.
 *
 * For now, the quality is just a score between 0 and 1 if the
 * Importer can import at least some of the data. If the Importer cannot
 * import this file at all, an ImporterException is thrown.
 */
public class ImporterScore
{
    /**
     * Constructor.
     *
     * @param importer the Importer for which this is a score
     * @param matchQuality the match quality
     * @param explanation the explanation why this score
     */
    public ImporterScore(
            Importer importer,
            double   matchQuality,
            String   explanation )
    {
        theImporter     = importer;
        theMatchQuality = matchQuality;
        theExplanation  = explanation;
    }

    /**
     * Obtain the Importer for which this is a score.
     *
     * @return the Importer
     */
    public Importer getImporter()
    {
        return theImporter;
    }

    /**
     * Obtain the match quality.
     *
     * @return match quality
     */
    public double getScore()
    {
        return theMatchQuality;
    }

    /**
     * Convenience method to determine whether this importer can import the file at all.
     *
     * @return true if it can import the file, even if it is very badly
     */
    public boolean canImport()
    {
        return theMatchQuality >= WORST;
    }

    /**
     * Obtain the explanation for the score.
     *
     * @return the explanation
     */
    public String getExplanation()
    {
        return theExplanation;
    }

    /**
     * Compare to another. This returns the same values as strcmp.
     *
     * @param other the ImporterScore to compare to
     * @return -1, 0 or 1
     */
    public int compareTo(
            ImporterScore other )
    {
        if( theMatchQuality < other.theMatchQuality ) {
            return -1;
        } else if( theMatchQuality == other.theMatchQuality ) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Compare to another. This is a static so we can compare nulls as well.
     *
     * @param a the first ImporterScore
     * @param b the ImporterScore to compare to
     * @return -1, 0 or 1
     */
    public static int compareTo(
            ImporterScore a,
            ImporterScore b )
    {
        if( a == null ) {
            if( b == null ) {
                return 0;
            } else {
                return -1;
            }
        } else if( b == null ) {
            return 1;
        } else {
            return a.compareTo( b );
        }
    }

    /**
     * The importer that was scored.
     */
    protected final Importer theImporter;

    /**
     * The match quality.
     */
    protected final double theMatchQuality;

    /**
     * The explanation.
     */
    protected final String theExplanation;

    /**
     * Can import, but is worst quality.
     */
    public static final double WORST = 0.0d;

    /**
     * Best quality.
     */
    public static final double BEST = 1.0d;
}
