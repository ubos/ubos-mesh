//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.ubos.util.logging.Log;

/**
 *
 */
public class ImporterHandlerRegularFileNode
    extends
        ImporterHandlerNode
{
    private static final Log log = Log.getLogInstance( ImporterHandlerRegularFileNode.class );

    public ImporterHandlerRegularFileNode(
            ImporterHandlerNode parent,
            File file )
    {
        super( parent );

        theFile = file;

        if( parent == null ) {
            // root node
            theName = file.getName();

        } else {
            // we might have skipped directories, so we need to calculate the name
            File   parentFile = parent.getFile();
            String parentName = parentFile.getPath();
            String thisName   = theFile.getPath();

            if( thisName.startsWith( parentName + "/" )) {
                theName = thisName.substring( parentName.length()+1 );
            } else {
                log.error( "Unexpected child node: " + parentName + " vs " + thisName );
                theName = thisName;
            }
        }
    }

    @Override
    public String getName()
    {
        return theName;
    }

    @Override
    public File getFile()
    {
        return theFile;
    }

    @Override
    public long getTimeOfExport()
    {
        long ret = theFile.lastModified(); // best we can do
        return ret;
    }

    @Override
    public boolean canGetStream()
    {
        return true;
    }

    @Override
    public InputStream createStream()
        throws
            IOException
    {
        return new FileInputStream( theFile );
    }

    protected final File theFile;

    protected final String theName;

}
