//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.nonblessed.NonblessedUtils;

/**
 * All ImporterHandlers that do not bless MeshObjects or Relationships inherit from this interface.
 * This is discouraged and should only be used for "fallback" importing support.
 */
public interface NonblessedImporterHandler
    extends
        ImporterHandler
{
    /**
     * Factored-out to make it easier for NonblessedImporterHandler implementations to set the required Attribute
     * and RoleAttribute values.
     *
     * @param obj the imported MeshObject
     * @param objectType value for IMPORTER_OJBECT_TYPE_ATTRIBUTE
     */
    public default void setRequiredTopAttributes(
            MeshObject obj,
            String     objectType )
    {
        obj.setAttributeValue( IMPORTER_NAME_ATTRIBUTE, getClass().getName() );
        obj.setAttributeValue( NonblessedUtils.OBJECTTYPE_ATTRIBUTE, objectType );
    }

    /**
     * Factored-out to make it easier for NonblessedImporterHandler implementations to set the required Attribute
     * and RoleAttribute values.
     *
     * @param obj the imported MeshObject
     * @param objectType value for IMPORTER_OJBECT_TYPE_ATTRIBUTE
     */
    public default void setRequiredNonTopAttributes(
            MeshObject obj,
            String     objectType )
    {
        obj.setAttributeValue( NonblessedUtils.OBJECTTYPE_ATTRIBUTE, objectType );
    }

    /**
     * Factored-out to make it easier for NonblessedImporterHandler implementations to set the required Attribute
     * and RoleAttribute values.
     *
     * @param parent the imported parent MeshObject
     * @param child the imported child MeshObject
     * @param containsType the nature of the parent-child relationship
     * @param localChildName local name of the child within the context of the parent
     * @param localChildIndex local index of the child within the context of the parent
     */
    public default void setRequiredRoleAttributes(
            MeshObject parent,
            MeshObject child,
            String     containsType,
            String     localChildName,
            Integer    localChildIndex )
    {
        parent.setRoleAttributeValue( child, NonblessedUtils.RELATIONSHIPTYPE_ROLE_ATTRIBUTE, containsType );
        if( localChildName != null ) {
            parent.setRoleAttributeValue( child, NonblessedUtils.LOCAL_NAME_ROLE_ATTRIBUTE, localChildName );
        }
        if( localChildIndex != null ) {
            parent.setRoleAttributeValue( child, NonblessedUtils.LOCAL_INDEX_ROLE_ATTRIBUTE, localChildIndex );
        }
    }

    /**
     * Name of the Attribute on a created MeshObject whose value indicates the name of the ImporterHandler
     * that imported this MeshObject. This only needs to be set for the top-most MeshObject imported by this
     * ImporterHandler.
     *
     * <p>Setting a value for this is required.
     */
    public static final String IMPORTER_NAME_ATTRIBUTE = "importer";
}
