//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.zip;

import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Assign a neutral score to ImporterHandlerNodes that are directories in ZIP files.
 */
public class NoOpZipDirectoryImporterHandler
    implements
        NonblessedImporterHandler
{
    /**
     * Constructor.
     *
     * @param okScore the score to assign to directories in ZIP files.
     * @param deleteHereObject if true, delete the here-MeshObject
     */
    public NoOpZipDirectoryImporterHandler(
            double  okScore,
            boolean deleteHereObject )
    {
        theOkScore = okScore;

        theDeleteHereObject = deleteHereObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void augmentNode(
            ImporterHandlerNode currentNode )
    {
        // no op -- FIXME?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        MeshObject hereObject = toBeImported.getHereMeshObject();

        if( toBeImported instanceof ZipImporterHandlerDirectoryNode ) {
            if( theDeleteHereObject ) {
                hereObject.getMeshBase().deleteMeshObject( hereObject );
            }
            return theOkScore;

        } else if( toBeImported instanceof ZipImporterHandlerMissingDirectoryNode ) {
            if( theDeleteHereObject ) {
                hereObject.getMeshBase().deleteMeshObject( hereObject );
            }
            return theOkScore;

        } else {
            return IMPOSSIBLE;
        }
    }

    @Override
    public Pattern getFilenamePattern()
    {
        return null;
    }

    /**
     * The score we assign.
     */
    protected final double theOkScore;

    /**
     * If true, delete the here-MeshObject.
     */
    protected final boolean theDeleteHereObject;
}
