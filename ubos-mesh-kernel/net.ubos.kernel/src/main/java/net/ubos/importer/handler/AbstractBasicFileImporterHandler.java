//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import java.io.File;
import java.util.regex.Pattern;

/**
 * Default functionality common to ImporterContentHandlers that read files that are not interpreted
 * as containing other files (so not directories, not ZipFiles).
 */
public abstract class AbstractBasicFileImporterHandler
    implements
        ImporterHandler
{
    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     */
    protected AbstractBasicFileImporterHandler(
            Pattern filenamePattern,
            double  okScore )
    {
        theFilenamePattern = filenamePattern;
        theOkScore         = okScore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void augmentNode(
            ImporterHandlerNode currentNode )
    {
        // no op
        File f = currentNode.getFile();
        if( f != null && f.isFile() ) {
            currentNode.setNoChildren();

        } else if( currentNode.canGetStream() ) {
            currentNode.setNoChildren();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pattern getFilenamePattern()
    {
        return theFilenamePattern;
    }

    /**
     * The path and filename pattern this ImporterHandler can handle.
     */
    protected final Pattern theFilenamePattern;

    /**
     * When importing works, what score should be reported.
     */
    protected final double theOkScore;
}
