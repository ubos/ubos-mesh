//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.directory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.handler.ImporterHandlerDirectoryNode;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.importer.handler.ImporterHandlerRegularFileNode;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.nonblessed.NonblessedUtils;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Functionality common to ContentHandlers that process directories.
 */
public abstract class AbstractDirectoryImporterHandler
    implements
        NonblessedImporterHandler
{
    /**
     * Constructor.
     *
     * @param okScore when importing works, what score should be reported
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     */
    protected AbstractDirectoryImporterHandler(
            double  okScore,
            Pattern filenamePattern )
    {
        theOkScore         = okScore;
        theFilenamePattern = filenamePattern;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void augmentNode(
            ImporterHandlerNode currentNode )
    {
        File f = currentNode.getFile();
        if( f == null || !f.isDirectory() ) {
            return;
        }

        File [] childFiles = determineChildren( f );
        ImporterHandlerNode [] children   = new ImporterHandlerNode[ childFiles.length ];

        for( int i=0 ; i<childFiles.length ; ++i ) {
            File currentChildFile = childFiles[i];
            if( currentChildFile.isDirectory() ) {
                children[i] = new ImporterHandlerDirectoryNode( currentNode, childFiles[i] );
            } else {
                children[i] = new ImporterHandlerRegularFileNode( currentNode, childFiles[i] );
            }
        }
        currentNode.setChildNodes( children );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        File f = toBeImported.getFile();
        if( f == null || !f.isDirectory() ) {
            return IMPOSSIBLE;
        }

        MeshObject hereObject = toBeImported.getHereMeshObject();
        MeshBase   mb         = context.getMeshBase();

        setRequiredTopAttributes(hereObject, NonblessedUtils.OBJECTTYPE_VALUE_DIRECTORY );

        for( ImporterHandlerNode currentChildNode : toBeImported.childIter() ) {

            MeshObject currentChild = mb.createMeshObjectBelow( hereObject, currentChildNode.getName());
            setRequiredRoleAttributes(hereObject,
                    currentChild,
                    NonblessedUtils.RELATIONSHIPTYPE_ROLE_VALUE_DIRECTORY_CONTAINS,
                    currentChildNode.getName(),
                    null );

            currentChildNode.setHereMeshObject( currentChild );
        }
        return theOkScore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pattern getFilenamePattern()
    {
        return theFilenamePattern;
    }

    /**
     * Find the child File objects to be processed.
     *
     * @param start the parent directory
     * @return the child files to process
     */
    protected abstract File [] determineChildren(
            File start );

    /**
     * The path and filename pattern this ImporterContentHandler can handle.
     */
    protected final Pattern theFilenamePattern;

    /**
     * When importing works, what score should be reported.
     */
    protected final double theOkScore;

    /**
     * Filter out . and .. entries.
     */
    public static final FilenameFilter NO_DOT_OR_OTHER_STRANGE_FILES_FILTER =
            ( File dir, String name ) -> {
                if( name.equals( ".." )) {
                    return false;
                }
                if( name.equals( "." )) {
                    return false;
                }
                if( name.equals( ".DS_Store" )) { // Apple metadata files
                    return false;
                }
                if( name.startsWith( "._" )) { // Apple metadata files
                    return false;
                }
                return true;
            };
}
