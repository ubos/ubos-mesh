//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler;

import java.io.File;
import java.io.InputStream;

/**
 *
 */
public class ImporterHandlerDirectoryNode
    extends
        ImporterHandlerNode
{
    public ImporterHandlerDirectoryNode(
            ImporterHandlerNode parent,
            File                dir )
    {
        super( parent );

        theDirectory   = dir;
    }

    @Override
    public String getName()
    {
        return theDirectory.getName();
    }

    @Override
    public File getFile()
    {
        return theDirectory;
    }

    @Override
    public long getTimeOfExport()
    {
        long ret = theDirectory.lastModified(); // best we can do
        return ret;
    }

    @Override
    public boolean canGetStream()
    {
        return false;
    }

    @Override
    public InputStream createStream()
    {
        throw new UnsupportedOperationException();
    }

    protected final File theDirectory;
}
