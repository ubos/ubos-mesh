//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import net.ubos.model.primitives.StringValue;

/**
 * This is data wanting to become a dependency, during reading.
 */
public abstract class ExternalizedDependency
{
    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setName(
            StringValue newValue )
    {
        if( theName != null ) {
            throw new IllegalStateException( "Have name already: " + theName + " vs " + newValue );
        }
        theName = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public StringValue getName()
    {
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "Dependency on module " + theName.value();
    }

    /**
     * Name of the referenced SubjectArea.
     */
    protected StringValue theName;
}
