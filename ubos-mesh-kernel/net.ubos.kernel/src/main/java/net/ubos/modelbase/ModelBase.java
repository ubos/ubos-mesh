//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.modelbase.m.MModelBase;
import net.ubos.util.cursoriterator.CursorIterator;

/**
  * <p>This interface is implemented by classes that act as a repository of model-level concepts.
  * The concepts held in a ModelBase include EntityTypes, RelationshipTypes, PropertyTypes,
  * SubjectAreas and the like. A ModelBase is comparable to a database's data dictionary,
  * except that the ModelBase holds conceptual-level concepts, not database-level ones.
  * For example, it holds EntityTypes and their inheritance relationships, rather than tables.
  *
  * <p>ModelBases may contain their entire model from the time they are created or restored.
  * Others may demand-load models or model fragments as needed. This interface does not distinguish
  * implementation differences such as these ones.
  */
public interface ModelBase
{
    /**
     * Singleton instance.
     */
    public final ModelBase SINGLETON = MModelBase.create();

    /**
      * Obtain the manager for MeshType lifecycles. This manager cannot be
      * re-assigned as it is specific to the implementation of the ModelBase.
      *
      * @return the MeshTypeLifecycleManager for this ModelBase
      */
    public abstract MeshTypeLifecycleManager getMeshTypeLifecycleManager();

    /**
      * Obtain an Iterator to iterate over all MeshTypes contained in this ModelBase.
      *
      * @return the Iterator
      */
    public CursorIterator<MeshType> iterator();

    /**
     * Get all currently loaded SubjectAreas.
     *
     * @return the currently loaded SubjectAreas.
     */
    public SubjectArea [] getLoadedSubjectAreas();

    /**
     * Get all currently loaded SubjectAreas, ordered by UserVisibleName.
     *
     * @return the currently loaded SubjectAreas.
     */
    public SubjectArea [] getOrderedLoadedSubjectAreas();

    /**
     * Obtain the MeshTypeSynonymDictionary.
     *
     * @return the MeshTypeSynonyDirectory.
     */
    public MeshTypeSynonymDictionary getSynonymDictionary();

    /**
     * Find a MeshType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found MeshType
     * @return the found MeshType
     * @throws MeshTypeWithIdentifierNotFoundException thrown if the MeshType could not be found
     */
    public abstract MeshType findMeshType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find a MeshType by its unique identifier.
     *
     * @param identifierAsString Identifier of the to-be-found MeshType
     * @return the found MeshType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws MeshTypeWithIdentifierNotFoundException thrown if the MeshType could not be found
     */
    public abstract MeshType findMeshType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find a MeshType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found MeshType
     * @return the MeshType, or null if not found
     */
    public abstract MeshType findMeshTypeOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Find a MeshType by its unique identifier.
     *
     * @param identifierAsString Identifier of the to-be-found MeshType
     * @return the MeshType, or null if not found
     */
    public abstract MeshType findMeshTypeOrNull(
            String identifierAsString );

    /**
     * Find a SubjectArea by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found SubjectArea
     * @return the found SubjectArea
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
     */
    public abstract SubjectArea findSubjectArea(
            MeshTypeIdentifier identifier )
        throws
            SubjectAreaNotFoundException;

    /**
     * Find a SubjectArea by its unique identifier.
     *
     * @param identifierAsString Identifier of the to-be-found SubjectArea
     * @return the found SubjectArea
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
     */
    public abstract SubjectArea findSubjectArea(
            String identifierAsString )
        throws
            ParseException,
            SubjectAreaNotFoundException;

    /**
     * Find a SubjectArea by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found SubjectArea
     * @return the SubjectArea, or null if not found
     */
    public abstract SubjectArea findSubjectAreaOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Find a SubjectArea by its unique identifier.
     *
     * @param identifierAsString Identifier of the to-be-found SubjectArea
     * @return the SubjectArea, or null if not found
     */
    public abstract SubjectArea findSubjectAreaOrNull(
            String identifierAsString );

    /**
     * Find a CollectableMeshType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found CollectableMeshType
     * @return the found CollectableMeshType
     * @throws MeshTypeWithIdentifierNotFoundException thrown if an CollectableMeshType with this Identifier cannot be found
     */
    public abstract CollectableMeshType findCollectableMeshType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Shortcut for finding a CollectableMeshType in the ModelBase.
     *
     * @param identifierAsString the Identifier of the CollectableMeshType, as String for convenience
     * @return the found CollectableMeshType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws MeshTypeWithIdentifierNotFoundException thrown if an CollectableMeshType with this Identifier cannot be found
     */
    public abstract CollectableMeshType findCollectableMeshType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find a CollectableMeshType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found MeshTypeWithProperties
     * @return the MeshTypeWithProperties, or null if not found
     */
    public abstract CollectableMeshType findCollectableMeshTypeOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Shortcut for finding a CollectableMeshType in the ModelBase.
     *
     * @param identifierAsString the Identifier of the MeshTypeWithProperties, as String for convenience
     * @return the MeshTypeWithProperties, or null if not found
     */
    public abstract CollectableMeshType findCollectableMeshTypeOrNull(
            String identifierAsString );

    /**
     * Find a CollectableMeshType by name and its containing SubjectArea.
     *
     * @param subjectArea the SubjectArea in which we are looking for the CollectableMeshType
     * @param entityTypeName the name of the CollectableMeshType
     * @return the found CollectableMeshType
     * @throws CollectableMeshTypeNotFoundException thrown if the CollectableMeshType cannot be found
     */
    public abstract CollectableMeshType findCollectableMeshType(
            SubjectArea subjectArea,
            String      entityTypeName )
        throws
            CollectableMeshTypeNotFoundException;

    /**
     * Shortcut for finding a CollectableMeshType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param entityTypeName the name of the CollectableMeshType
     * @return the found CollectableMeshType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
     * @throws CollectableMeshTypeNotFoundException thrown if the SubjectArea or the CollectableMeshType cannot be found
     */
    public abstract CollectableMeshType findCollectableMeshType(
            String subjectAreaName,
            String entityTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            CollectableMeshTypeNotFoundException;

    /**
      * Find a CollectableMeshType by name and its containing SubjectArea.
      *
      * @param subjectArea the SubjectArea in which we are looking for the CollectableMeshType
      * @param entityTypeName the name of the CollectableMeshType
      * @return the found CollectableMeshType, or null if not found
      */
    public abstract CollectableMeshType findCollectableMeshTypeOrNull(
            SubjectArea subjectArea,
            String      entityTypeName );

    /**
     * Shortcut for finding a CollectableMeshType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param entityTypeName the name of the CollectableMeshType
     * @return the found CollectableMeshType, or null if not found
     */
    public abstract CollectableMeshType findCollectableMeshTypeOrNull(
            String subjectAreaName,
            String entityTypeName );

    /**
     * Find an MeshTypeWithProperties by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found MeshTypeWithProperties
     * @return the found MeshTypeWithProperties
     * @throws MeshTypeWithIdentifierNotFoundException thrown if an MeshTypeWithProperties with this Identifier cannot be found
     */
    public abstract MeshTypeWithProperties findMeshTypeWithProperties(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Shortcut for finding an MeshTypeWithProperties in the ModelBase.
     *
     * @param identifierAsString the Identifier of the MeshTypeWithProperties, as String for convenience
     * @return the found MeshTypeWithProperties
     * @throws MeshTypeWithIdentifierNotFoundException thrown if an MeshTypeWithProperties with this Identifier cannot be found
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     */
    public abstract MeshTypeWithProperties findMeshTypeWithProperties(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find an MeshTypeWithProperties by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found MeshTypeWithProperties
     * @return the MeshTypeWithProperties, or null if not found
     */
    public abstract MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Shortcut for finding an MeshTypeWithProperties in the ModelBase.
     *
     * @param identifierAsString the Identifier of the MeshTypeWithProperties, as String for convenience
     * @return the MeshTypeWithProperties, or null if not found
     */
    public abstract MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            String identifierAsString );

    /**
     * Find a MeshTypeWithProperties by name and its containing SubjectArea.
     *
     * @param subjectArea the SubjectArea in which we are looking for the EntityType
     * @param name the name of the MeshTypeWithProperties
     * @return the found MeshTypeWithProperties
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws MeshTypeWithPropertiesNotFoundException thrown if the MeshTypeWithProperties cannot be found
     */
    public abstract MeshTypeWithProperties findMeshTypeWithProperties(
            SubjectArea subjectArea,
            String      name )
        throws
            ParseException,
            MeshTypeWithPropertiesNotFoundException;

    /**
     * Shortcut for finding a MeshTypeWithProperties directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param name the name of the MeshTypeWithProperties
     * @return the found MeshTypeWithProperties
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
     * @throws MeshTypeWithPropertiesNotFoundException thrown if the MeshTypeWithProperties cannot be found
     */
    public abstract MeshTypeWithProperties findMeshTypeWithProperties(
            String subjectAreaName,
            String name )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            MeshTypeWithPropertiesNotFoundException;

    /**
     * Find a MeshTypeWithProperties by name and its containing SubjectArea.
     *
     * @param subjectArea the SubjectArea in which we are looking for the MeshTypeWithProperties
     * @param name the name of the MeshTypeWithProperties
     * @return the found MeshTypeWithProperties, or null if not found
     */
    public abstract MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            SubjectArea subjectArea,
            String      name );

    /**
     * Shortcut for finding a MeshTypeWithProperties directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param name the name of the MeshTypeWithProperties
     * @return the found MeshTypeWithProperties, or null if not found
     */
    public abstract MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            String subjectAreaName,
            String name );

    /**
     * Find an EntityType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found EntityType
     * @return the found EntityType
     * @throws MeshTypeWithIdentifierNotFoundException thrown if an EntityType with this Identifier cannot be found
     */
    public abstract EntityType findEntityType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Shortcut for finding an EntityType in the ModelBase.
     *
     * @param identifierAsString the Identifier of the EntityType, as String for convenience
     * @return the found EntityType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws MeshTypeWithIdentifierNotFoundException thrown if an EntityType with this Identifier cannot be found
     */
    public abstract EntityType findEntityType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find an EntityType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found EntityType
     * @return the EntityType, or null if not found
     */
    public abstract EntityType findEntityTypeOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Shortcut for finding an EntityType in the ModelBase.
     *
     * @param identifierAsString the Identifier of the EntityType, as String for convenience
     * @return the EntityType, or null if not found
     */
    public abstract EntityType findEntityTypeOrNull(
            String identifierAsString );

    /**
     * Find a EntityType by name and its containing SubjectArea.
     *
     * @param subjectArea the SubjectArea in which we are looking for the EntityType
     * @param entityTypeName the name of the EntityType
     * @return the found EntityType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws EntityTypeNotFoundException thrown if the EntityType cannot be found
     */
    public abstract EntityType findEntityType(
            SubjectArea subjectArea,
            String      entityTypeName )
        throws
            ParseException,
            EntityTypeNotFoundException;

    /**
     * Shortcut for finding a EntityType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param entityTypeName the name of the EntityType
     * @return the found EntityType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
     * @throws EntityTypeNotFoundException thrown if the EntityType cannot be found
     */
    public abstract EntityType findEntityType(
            String subjectAreaName,
            String entityTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            EntityTypeNotFoundException;

    /**
      * Find a EntityType by name and its containing SubjectArea.
      *
      * @param subjectArea the SubjectArea in which we are looking for the EntityType
      * @param entityTypeName the name of the EntityType
      * @return the found EntityType, or null if not found
      */
    public abstract EntityType findEntityTypeOrNull(
            SubjectArea subjectArea,
            String      entityTypeName );

    /**
     * Shortcut for finding a EntityType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param entityTypeName the name of the EntityType
     * @return the found EntityType, or null if not found
     */
    public abstract EntityType findEntityTypeOrNull(
            String subjectAreaName,
            String entityTypeName );

    /**
     * Find a RelationshipType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found RelationshipType
     * @return the found RelationshipType
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a RelationshipType with this Identifier cannot be found
     */
    public abstract RelationshipType findRelationshipType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Shortcut for finding a RelationshipType in the ModelBase.
     *
     * @param identifierAsString the Identifier of the RelationshipType, as String for convenience
     * @return the RelationshipType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a RelationshipType with this Identifier cannot be found
     */
    public abstract RelationshipType findRelationshipType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find a RelationshipType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found RelationshipType
     * @return the RelationshipType, or null
     */
    public abstract RelationshipType findRelationshipTypeOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Shortcut for finding a RelationshipType in the ModelBase.
     *
     * @param identifierAsString the Identifier of the RelationshipType, as String for convenience
     * @return the RelationshipType, or null
     */
    public abstract RelationshipType findRelationshipTypeOrNull(
            String identifierAsString );

    /**
      * Find a RelationshipType by name and its containing SubjectArea.
      *
      * @param subjectArea the SubjectArea in which we are looking for the RelationshipType
      * @param relationshipTypeName the name of the RelationshipType
      * @return the found RelationshipType
      * @throws RelationshipTypeNotFoundException thrown if the RelationshipType cannot be found
      */
    public abstract RelationshipType findRelationshipType(
            SubjectArea subjectArea,
            String      relationshipTypeName )
        throws
            RelationshipTypeNotFoundException;

    /**
     * Shortcut for finding a RelationshipType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param relationshipTypeName the name of the RelationshipType
     * @return the found RelationshipType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
     * @throws RelationshipTypeNotFoundException thrown if the RelationshipType cannot be found
     */
    public abstract RelationshipType findRelationshipType(
            String subjectAreaName,
            String relationshipTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            RelationshipTypeNotFoundException;

    /**
      * Find a RelationshipType by name and its containing SubjectArea.
      *
      * @param subjectArea the SubjectArea in which we are looking for the RelationshipType
      * @param relationshipTypeName the name of the RelationshipType
      * @return the found RelationshipType, or null if not found
      */
    public abstract RelationshipType findRelationshipTypeOrNull(
            SubjectArea subjectArea,
            String      relationshipTypeName );

    /**
     * Shortcut for finding a RelationshipType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param relationshipTypeName the name of the RelationshipType
     * @return the found RelationshipType, or null if not found
     */
    public abstract RelationshipType findRelationshipTypeOrNull(
            String subjectAreaName,
            String relationshipTypeName );

    /**
     * Find a PropertyType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found PropertyType
     * @return the found PropertyType
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a PropertyType with this Identifier cannot be found
     */
    public abstract PropertyType findPropertyType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find a PropertyType by its unique identifier.
     *
     * @param identifierAsString the Identifier of the PropertyType, as String for convenience
     * @return the found PropertyType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a PropertyType with this Identifier cannot be found
     */
    public abstract PropertyType findPropertyType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Shortcut for finding a PropertyType in the ModelBase.
     *
     * @param identifier Identifier of the to-be-found PropertyType
     * @return the PropertyType, or null
     */
    public abstract PropertyType findPropertyTypeOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Shortcut for finding a PropertyType in the ModelBase.
     *
     * @param identifierAsString the Identifier of the PropertyType, as String for convenience
     * @return the PropertyType, or null
     */
    public abstract PropertyType findPropertyTypeOrNull(
            String identifierAsString );

    /**
      * Find a PropertyType by name and its MeshTypeWithProperties (EntityType or RoleType).
      *
      * @param meshTypeWithProperties the MeshTypeWithProperties within which we are looking for the PropertyType
      * @param propertyTypeName the name of the PropertyType
      * @return the found PropertyType
      * @throws PropertyTypeNotFoundException thrown if the PropertyType cannot be found
      */
    public abstract PropertyType findPropertyType(
            MeshTypeWithProperties meshTypeWithProperties,
            String                 propertyTypeName )
        throws
            PropertyTypeNotFoundException;

    /**
     * Shortcut for finding a PropertyType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param meshTypeWithProperties the name of the owning MeshTypeWithProperties
     * @param propertyTypeName the name of the PropertyType
     * @return the found PropertyType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
     * @throws MeshTypeWithPropertiesNotFoundException thrown if the MeshTypeWithProperties cannot be found
     * @throws PropertyTypeNotFoundException thrown if the PropertyType cannot be found
     */
    public abstract PropertyType findPropertyType(
            String subjectAreaName,
            String meshTypeWithProperties,
            String propertyTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            MeshTypeWithPropertiesNotFoundException,
            PropertyTypeNotFoundException;

    /**
      * Find a PropertyType by name and its MeshTypeWithProperties.
      *
      * @param meshTypeWithProperties the MeshTypeWithProperties within which we are looking for the PropertyType
      * @param propertyTypeName the name of the PropertyType
      * @return the found PropertyType, or null if not found
      */
    public abstract PropertyType findPropertyTypeOrNull(
            MeshTypeWithProperties meshTypeWithProperties,
            String                 propertyTypeName );

    /**
     * Shortcut for finding a PropertyType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param meshTypeWithPropertiesName the name of the owning MeshTypeWithProperties
     * @param propertyTypeName the name of the PropertyType
     * @return the found PropertyType, or null if not found
     */
    public abstract PropertyType findPropertyTypeOrNull(
            String subjectAreaName,
            String meshTypeWithPropertiesName,
            String propertyTypeName );

    /**
     * Find a RoleType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found RoleType
     * @return the found RoleType
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a RoleType with this Identifier cannot be found
     */
    public abstract RoleType findRoleType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find a RoleType by its unique identifier.
     *
     * @param identifierAsString the Identifier of the RoleType, as String for convenience
     * @return the found RoleType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a RoleType with this Identifier cannot be found
     */
    public abstract RoleType findRoleType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException;

    /**
     * Find a RoleType by its unique identifier.
     *
     * @param identifier Identifier of the to-be-found RoleType
     * @return the RoleType, or null
     */
    public abstract RoleType findRoleTypeOrNull(
            MeshTypeIdentifier identifier );

    /**
     * Find a RoleType by its unique identifier.
     *
     * @param identifierAsString the Identifier of the RoleType, as String for convenience
     * @return the RoleType, or null
     */
    public abstract RoleType findRoleTypeOrNull(
            String identifierAsString );

    /**
      * Find a RoleType by name and its containing SubjectArea.
      *
      * @param subjectArea the SubjectArea in which we are looking for the RoleType
      * @param roleTypeName the name of the RoleType
      * @return the found RoleType
      * @throws RoleTypeNotFoundException thrown if the RoleType cannot be found
      */
    public abstract RoleType findRoleType(
            SubjectArea subjectArea,
            String      roleTypeName )
        throws
            RoleTypeNotFoundException;

    /**
      * Shortcut for finding a RoleType directly.
      *
      * @param subjectAreaName the fully-qualified name of the SubjectArea
      * @param roleTypeName the name of the RoleType
      * @return the found RoleType
     * @throws ParseException thrown if the identifier String could not be parsed into a MeshTypeIdentifier
      * @throws SubjectAreaNotFoundException thrown if the SubjectArea cannot be found
      * @throws RoleTypeNotFoundException thrown if the RelationshipType cannot be found
      */
    public abstract RoleType findRoleType(
            String subjectAreaName,
            String roleTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            RoleTypeNotFoundException;

    /**
      * Find a RoleType by name and its containing SubjectArea.
      *
      * @param subjectArea the SubjectArea in which we are looking for the RoleType
      * @param roleTypeName the name of the RoleType
      * @return the found RoleType, or null if not found
      */
    public abstract RoleType findRoleTypeOrNull(
            SubjectArea subjectArea,
            String      roleTypeName );

    /**
     * Shortcut for finding a RoleType directly.
     *
     * @param subjectAreaName the fully-qualified name of the SubjectArea
     * @param roleTypeName the name of the RoleType
     * @return the found RoleType, or null if not found
     */
    public abstract RoleType findRoleTypeOrNull(
            String subjectAreaName,
            String roleTypeName );

    /**
     * Load a model from this stream.
     *
     * @param xmlResource the name of the XML resource, for error messages
     * @param stream the InputStream from which to load
     * @param classLoader the ClassLoader to use
     * @return the SubjectAreas that were loaded
     * @throws ModelLoadingException loading the model failed
     * @throws IOException thrown if an I/O error occurred during reading
     */
    public abstract SubjectArea [] loadModel(
            String         xmlResource,
            InputStream    stream,
            ClassLoader    classLoader )
        throws
            ModelLoadingException,
            IOException;

    /**
     * Load a model from this stream.
     *
     * @param xmlResource the name of the XML resource, for error messages
     * @param stream the InputStream from which to load
     * @param classLoader the ClassLoader to use
     * @param now the time the current run was started
     * @return the SubjectAreas that were loaded
     * @throws ModelLoadingException loading the model failed
     * @throws IOException thrown if an I/O error occurred during reading
     */
    public abstract SubjectArea [] loadModel(
            String         xmlResource,
            InputStream    stream,
            ClassLoader    classLoader,
            TimeStampValue now )
        throws
            ModelLoadingException,
            IOException;

    /**
     * Check the correctness and integrity of this SubjectArea. There is usually no need
     * to call this for the application developer.
     *
     * @param candidate the SubjectArea that shall be checked
     * @throws IllegalArgumentException thrown if the check fails
     */
    public void checkSubjectArea(
            SubjectArea candidate )
        throws
            IllegalArgumentException;

    /**
     * Check the correctness and integrity of this EntityType. There is usually no need
     * to call this for the application developer.
     *
     * @param candidate the EntityType that shall be checked
     * @throws IllegalArgumentException thrown if the check fails
     */
    public void checkEntityType(
            EntityType candidate )
        throws
            IllegalArgumentException;

    /**
     * Check the correctness and integrity of this RelationshipType. There is usually no need
     * to call this for the application developer.
     *
     * @param candidate the RelationshipType that shall be checked
     * @throws IllegalArgumentException thrown if the check fails
     */
    public void checkRelationshipType(
            RelationshipType candidate )
        throws
            IllegalArgumentException;

    /**
     * Check the correctness and integrity of this PropertyType. There is usually no need
     * to call this for the application developer.
     *
     * @param candidate the PropertyType that shall be checked
     * @throws IllegalArgumentException thrown if the check fails
     */
    public void checkPropertyType(
            PropertyType candidate )
        throws
            IllegalArgumentException;

    /**
     * Check the correctness and integrity of this PropertyTypeGroup. There is usually no need
     * to call this for the application developer.
     *
     * @param candidate the PropertyTypeGroup that shall be checked
     * @throws IllegalArgumentException thrown if the check fails
     */
    public void checkPropertyTypeGroup(
            PropertyTypeGroup candidate )
        throws
            IllegalArgumentException;

    /**
     * Check the correctness and integrity of this RoleType.
     *
     * @param candidate the candidate RoleType that shall be checked
     * @throws IllegalArgumentException thrown if the check fails
     */
    public void checkRoleType(
            RoleType candidate )
        throws
            IllegalArgumentException;

    /**
     * Add a MeshTypeLifecycleEventListener.
     * This listener is added directly to the listener list, which prevents the
     * listener from being garbage-collected before this Object is being garbage-collected.
     *
     * @param newListener the to-be-added MeshTypeLifecycleEventListener
     * @see #addWeakMeshTypeLifecycleEventListener
     * @see #addSoftMeshTypeLifecycleEventListener
     * @see #removeMeshTypeLifecycleEventListener
     */
    public void addDirectMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener );

    /**
     * Add a MeshTypeLifecycleEventListener.
     * This listener is added to the listener list using a <code>java.lang.ref.SoftReference</code>,
     * which allows the listener to be garbage-collected before this Object is being garbage-collected
     * according to the semantics of Java references.
     *
     * @param newListener the to-be-added MeshTypeLifecycleEventListener
     * @see #addDirectMeshTypeLifecycleEventListener
     * @see #addSoftMeshTypeLifecycleEventListener
     * @see #removeMeshTypeLifecycleEventListener
     */
    public void addWeakMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener );

    /**
     * Add a MeshTypeLifecycleEventListener.
     * This listener is added to the listener list using a <code>java.lang.ref.WeakReference</code>,
     * which allows the listener to be garbage-collected before this Object is being garbage-collected
     * according to the semantics of Java references.
     *
     * @param newListener the to-be-added MeshTypeLifecycleEventListener
     * @see #addDirectMeshTypeLifecycleEventListener
     * @see #addWeakMeshTypeLifecycleEventListener
     * @see #removeMeshTypeLifecycleEventListener
     */
    public void addSoftMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener );

    /**
     * Remove a PropertyChangeListener.
     *
     * @param oldListener the to-be-removed PropertyChangeListener
     * @see #addDirectMeshTypeLifecycleEventListener
     * @see #addWeakMeshTypeLifecycleEventListener
     * @see #addSoftMeshTypeLifecycleEventListener
     */
    public void removeMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener oldListener );
}
