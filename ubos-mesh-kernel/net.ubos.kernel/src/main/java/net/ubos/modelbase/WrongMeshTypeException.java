//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.util.logging.Dumper;

/**
 * Thrown if a different subtype of MeshType was expected than was found.
 */
public class WrongMeshTypeException
        extends
            MeshTypeWithIdentifierNotFoundException
{
    /**
     * Constructor.
     * 
     * @param identifier the Identifier of the MeshType to be found
     * @param foundType the type that was found
     */
    public WrongMeshTypeException(
            MeshTypeIdentifier        identifier,
            Class<? extends MeshType> foundType )
    {
        super( identifier );

        theFoundType = foundType;
    }
    
    /**
     * Obtain the type that was found. This will be an interface such as
     * <code>EntityType.class</code>, not a subclass such as <code>MEntityType.class</code>.
     * 
     * @return the found type
     */
    public Class<? extends MeshType> getFoundType()
    {
        return theFoundType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theIdentifier, theFoundType };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theIdentifier",
                    "theFoundType"
                },
                new Object[] {
                    theIdentifier,
                    theFoundType
                });
    }

    /**
     * The type that was actually found.
     */
    protected transient Class<? extends MeshType> theFoundType;
}
