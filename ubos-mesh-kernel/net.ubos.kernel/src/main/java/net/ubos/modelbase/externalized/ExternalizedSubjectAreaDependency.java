//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

/**
 * This is data wanting to become a SubjectArea dependency, during reading.
 */
public class ExternalizedSubjectAreaDependency
        extends
            ExternalizedDependency
{}
