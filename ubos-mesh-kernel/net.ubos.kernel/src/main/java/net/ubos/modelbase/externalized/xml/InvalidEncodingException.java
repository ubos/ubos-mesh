//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * The value of a DataType could not be parsed from the provided String.
 */
public class InvalidEncodingException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param type the DataType whose value was supposed to be parsed
     * @param encodedValue the String that was being parsed
     * @param locator knows the location of the problem
     */
    public InvalidEncodingException(
            DataType type,
            String   encodedValue,
            Locator  locator )
    {
        super( locator );

        theType         = type;
        theEncodedValue = encodedValue;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theType,
            theEncodedValue
        };
    }

    /**
     * The DataType whose value was supposed to be parsed.
     */
    protected final DataType theType;
    
    /**
     * The String that was being parsed.
     */
    protected final String theEncodedValue;
}
