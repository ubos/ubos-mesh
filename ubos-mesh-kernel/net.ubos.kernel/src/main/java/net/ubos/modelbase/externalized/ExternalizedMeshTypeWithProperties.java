//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.ArrayList;

/**
 * This is data wanting to become an MeshTypeWithProperties, during reading.
 */
public abstract class ExternalizedMeshTypeWithProperties
    extends
        ExternalizedCollectableMeshType
{
    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addPropertyType(
            ExternalizedPropertyType newValue )
    {
        propertyTypes.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<ExternalizedPropertyType> getPropertyTypes()
    {
        return propertyTypes;
    }

    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addPropertyTypeGroup(
            ExternalizedPropertyTypeGroup newValue )
    {
        propertyTypeGroups.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<ExternalizedPropertyTypeGroup> getPropertyTypeGroups()
    {
        return propertyTypeGroups;
    }

    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addSuperTypeIdentifier(
            String newValue )
    {
        theSuperTypes.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<String> getSuperTypeIdentifiers()
    {
        return theSuperTypes;
    }

    /**
     * List of ExternalizedPropertyType.
     */
    protected final ArrayList<ExternalizedPropertyType> propertyTypes = new ArrayList<>();

    /**
     * List of ExternalizedPropertyTypeGroups.
     */
    protected final ArrayList<ExternalizedPropertyTypeGroup> propertyTypeGroups = new ArrayList<>();

    /**
     * List of Identifiers identifying the supertypes.
     */
    protected final ArrayList<String> theSuperTypes = new ArrayList<>();
}
