//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.SubjectArea;

/**
 * This Exception indicates that an MeshTypeWithProperties could not be found.
 */
public class MeshTypeWithPropertiesNotFoundException
        extends
            CollectableMeshTypeNotFoundException
{
    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which an AttributableMeshObjectType could not be found
     * @param amtName the name of the MeshTypeWithProperties that could not be found
     */
    public MeshTypeWithPropertiesNotFoundException(
            SubjectArea sa,
            String      amtName )
    {
        super( sa, amtName );
    }

    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which an AttributableMeshObjectType could not be found
     * @param amtName the name of the MeshTypeWithProperties that could not be found
     * @param cause the Throwable that caused this Exception
     */
    public MeshTypeWithPropertiesNotFoundException(
            SubjectArea sa,
            String      amtName,
            Throwable   cause )
    {
        super( sa, amtName, cause );
    }
}
