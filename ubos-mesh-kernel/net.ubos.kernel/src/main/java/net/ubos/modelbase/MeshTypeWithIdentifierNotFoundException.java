//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * This Exception indicates that a MeshType of any kind, with this Identifier,
 * could not be found.
 */
public class MeshTypeWithIdentifierNotFoundException
        extends
            MeshTypeNotFoundException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     * 
     * @param identifier the Identifier of the MeshType that could not be found
     */
    public MeshTypeWithIdentifierNotFoundException(
            MeshTypeIdentifier identifier )
    {
        super();

        theIdentifier = identifier;
    }

    /**
     * Constructor.
     * 
     * @param identifier the Identifier of the MeshType that could not be found
     * @param cause the Throwable that caused this Exception
     */
    public MeshTypeWithIdentifierNotFoundException(
            MeshTypeIdentifier identifier,
            Throwable          cause )
    {
        super( cause );

        theIdentifier = identifier;
    }

    /**
     * Obtain the Identifier that was not found.
     *
     * @return the Identifier
     */
    public MeshTypeIdentifier getIdentifier()
    {
        return theIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "Identifier"
                },
                new Object[] {
                    theIdentifier
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return super.toString() + "{ id: " + theIdentifier + " }";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{ theIdentifier };
    }

    /**
     * The Identifier of the MeshType that could not be found.
     */
    protected MeshTypeIdentifier theIdentifier;
}
