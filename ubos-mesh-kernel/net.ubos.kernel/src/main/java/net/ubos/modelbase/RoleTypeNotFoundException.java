//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.SubjectArea;

/**
 * This Exception indicates that a RoleType could not be found.
 */
public class RoleTypeNotFoundException
        extends
            MeshTypeWithPropertiesNotFoundException
{
    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which a RoleType could not be found
     * @param roleTypeName the name of the RoleType that could not be found
     */
    public RoleTypeNotFoundException(
            SubjectArea sa,
            String      roleTypeName )
    {
        super( sa, roleTypeName );
    }

    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which a RoleType could not be found
     * @param roleTypeName the name of the RoleType that could not be found
     * @param cause the Throwable that caused this Exception
     */
    public RoleTypeNotFoundException(
            SubjectArea sa,
            String      roleTypeName,
            Throwable   cause )
    {
        super( sa, roleTypeName, cause );
    }
}

