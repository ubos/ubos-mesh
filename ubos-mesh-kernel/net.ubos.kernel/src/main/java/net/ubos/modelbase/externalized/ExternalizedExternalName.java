//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

/**
 * This is data wanting to become a ExternalName, during reading.
 */
public class ExternalizedExternalName
{
    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setValue(
            String newValue )
    {
        if( theValue != null ) {
            throw new IllegalStateException( "Have value already: " + theValue + " vs " + newValue );
        }
        theValue = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public String getValue()
    {
        return theValue;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setPreferred(
            boolean newValue )
    {
        if( thePreferred != null ) {
            throw new IllegalStateException( "Have preferred already: " + thePreferred + " vs " + newValue );
        }
        thePreferred = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public boolean getPreferred()
    {
        return thePreferred == null ? false : thePreferred;
    }

    /**
     * The value of the external name
     */
    protected String theValue;

    /**
     * This external name is preferred.
     */
    protected Boolean thePreferred;
}
