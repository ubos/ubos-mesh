//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.MeshType;
import net.ubos.util.event.AbstractEvent;

/**
  * <p>This is the abstract supertype for all events affecting the life cycle of
  * a MeshType.
  *
  * <p>Note: this is a fairly infrequent occurrence, as the model
  * tends to be constant for relatively long times.
  */
public abstract class MeshTypeLifecycleEvent
       extends
           AbstractEvent<ModelBase,MeshType>
{
    /**
      * Construct one.
      *
      * @param theSender the ModelBase in which the event occurred
      * @param theObject the MeshType whose lifecycle was affected
      */
    protected MeshTypeLifecycleEvent(
            ModelBase theSender,
            MeshType  theObject )
    {
        super( theSender, theObject );
    }
}
