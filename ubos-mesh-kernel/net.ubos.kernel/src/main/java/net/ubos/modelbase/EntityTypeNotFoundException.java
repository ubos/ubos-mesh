//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.SubjectArea;

/**
 * This Exception indicates that an EntityType could not be found.
 */
public class EntityTypeNotFoundException
        extends
            MeshTypeWithPropertiesNotFoundException
{
    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which a EntityType could not be found
     * @param entityTypeName the name of the EntityType that could not be found
     */
    public EntityTypeNotFoundException(
            SubjectArea sa,
            String      entityTypeName )
    {
        super( sa, entityTypeName );
    }

    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which a EntityType could not be found
     * @param entityTypeName the name of the EntityType that could not be found
     * @param cause the Throwable that caused this Exception
     */
    public EntityTypeNotFoundException(
            SubjectArea sa,
            String      entityTypeName,
            Throwable   cause )
    {
        super( sa, entityTypeName, cause );
    }
}
