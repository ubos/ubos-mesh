//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 *A field was specified for a DataType, but it could not be found.
 */
public class CannotFindDeclaredFieldException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param type the DataType on which the field could not be found
     * @param fieldName the name of the field
     * @param locator knows the location of the problem
     */
    public CannotFindDeclaredFieldException(
            Class<? extends DataType> type,
            String                    fieldName,
            Locator                   locator )
    {
        super( locator );

        theType      = type;
        theFieldName = fieldName;
    }    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theType,
            theFieldName
        };
    }

    /**
     * The DataType on which the field could not be found.
     */
    protected final Class<? extends DataType> theType;
    
    /**
     * The name of the field.
     */
    protected final String theFieldName;
}
