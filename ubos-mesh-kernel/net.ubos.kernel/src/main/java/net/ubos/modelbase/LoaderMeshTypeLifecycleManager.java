//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import java.util.ArrayList;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.model.primitives.MeshTypeWithProperties;

/**
 * A MeshTypeLifecycleManager that delegates to another, but that also keeps track
 * of the MeshTypes that it has instantiated.
 */
public class LoaderMeshTypeLifecycleManager
        implements
            MeshTypeLifecycleManager
{
    /**
     * Constructor.
     *
     * @param delegate the underlying TypeLifecycleManager to which we delegate
     */
    public LoaderMeshTypeLifecycleManager(
            MeshTypeLifecycleManager delegate )
    {
        theDelegate = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType createEntityType(
            MeshTypeIdentifier      identifier,
            StringValue             theName,
            L10PropertyValueMap     theUserNames,
            L10PropertyValueMap     theUserDescriptions,
            BlobValue               theIcon,
            SubjectArea             theSubjectArea,
            EntityType []           supertypes,
            MeshTypeIdentifier []   synonyms,
            BooleanValue            isAbstract,
            BooleanValue            mayBeUsedAsForwardReference )
    {
        EntityType ret = theDelegate.createEntityType(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theIcon,
                theSubjectArea,
                supertypes,
                synonyms,
                isAbstract,
                mayBeUsedAsForwardReference );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier  identifier,
            StringValue         theName, // full name
            L10PropertyValueMap theUserNames,
            L10PropertyValueMap theUserDescriptions,
            SubjectArea         theSubjectArea,
            MultiplicityValue   sourceMultiplicity,
            MultiplicityValue   destinationMultiplicity,
            EntityType          source,
            EntityType          destination,
            RoleType []         sourceSuperRoleTypes,
            RoleType []         destinationSuperRoleTypes,
            BooleanValue        isAbstract )
    {
        RelationshipType ret = theDelegate.createRelationshipType(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theSubjectArea,
                sourceMultiplicity,
                destinationMultiplicity,
                source,
                destination,
                sourceSuperRoleTypes,
                destinationSuperRoleTypes,
                isAbstract );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier  identifier,
            StringValue         theName, // full name
            L10PropertyValueMap theUserNames,
            L10PropertyValueMap theUserDescriptions,
            SubjectArea         theSubjectArea,
            MultiplicityValue   sourceMultiplicity,
            MultiplicityValue   destinationMultiplicity,
            EntityType          source,
            EntityType          destination,
            RoleType            sourceSuperRoleType,
            RoleType            destinationSuperRoleType,
            BooleanValue        isAbstract )
    {
        RelationshipType ret = theDelegate.createRelationshipType(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theSubjectArea,
                sourceMultiplicity,
                destinationMultiplicity,
                source,
                destination,
                sourceSuperRoleType,
                destinationSuperRoleType,
                isAbstract );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceDestMultiplicity,
            EntityType                sourceDest,
            RoleType []               sourceDestSuperRoleTypes,
            BooleanValue              isAbstract )
    {
        RelationshipType ret = theDelegate.createRelationshipType(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theSubjectArea,
                sourceDestMultiplicity,
                sourceDest,
                sourceDestSuperRoleTypes,
                isAbstract );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceDestMultiplicity,
            EntityType                sourceDest,
            RoleType                  sourceDestSuperRoleType,
            BooleanValue              isAbstract )
    {
        RelationshipType ret = theDelegate.createRelationshipType(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theSubjectArea,
                sourceDestMultiplicity,
                sourceDest,
                sourceDestSuperRoleType,
                isAbstract );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType createPropertyType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties    theParent,
            SubjectArea               theSubjectArea,
            DataType                  theDataType,
            PropertyValue             theDefaultValue,
            BooleanValue              isOptional,
            BooleanValue              isReadOnly,
            FloatValue                theSequenceNumber )
    {
        PropertyType ret = theDelegate.createPropertyType(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theParent,
                theSubjectArea,
                theDataType,
                theDefaultValue,
                isOptional,
                isReadOnly,
                theSequenceNumber );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType createOverridingPropertyType(
            PropertyType []           toOverride,
            MeshTypeIdentifier        overriddenIdentifier,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties    theParent,
            SubjectArea               theSubjectArea,
            DataType                  theDataType,
            PropertyValue             theDefaultValue,
            BooleanValue              isOptional,
            BooleanValue              isReadOnly )
        throws
            InheritanceConflictException
    {
        PropertyType ret = theDelegate.createOverridingPropertyType(
                toOverride,
                overriddenIdentifier,
                theUserDescriptions,
                theParent,
                theSubjectArea,
                theDataType,
                theDefaultValue,
                isOptional,
                isReadOnly );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyTypeGroup createPropertyTypeGroup(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties      theParent,
            SubjectArea               theSubjectArea,
            PropertyType []           theMembers,
            FloatValue                theSequenceNumber )
    {
        PropertyTypeGroup ret = theDelegate.createPropertyTypeGroup(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theParent,
                theSubjectArea,
                theMembers,
                theSequenceNumber );
        theInstances.add( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea createSubjectArea(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea []            theSubjectAreaDependencies,
            ClassLoader               theClassLoader )
    {
        SubjectArea ret = theDelegate.createSubjectArea(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theSubjectAreaDependencies,
                theClassLoader );
        theInstances.add( ret );
        return ret;
    }

    /**
     * Obtain the MeshTypes that have been created by this LoaderTypeLifecycleManager.
     *
     * @return the created MeshTypes
     */
    public MeshType [] getInstantiated()
    {
        return ArrayHelper.copyIntoNewArray( theInstances, MeshType.class );
    }

    /**
     * The MeshTypeLifecycleManager to which we delegate.
     */
    protected MeshTypeLifecycleManager theDelegate;

    /**
     * The MeshTypes that have been created by this LoaderTypeLifecycleManager so far.
     */
    protected ArrayList<MeshType> theInstances = new ArrayList<>();
}
