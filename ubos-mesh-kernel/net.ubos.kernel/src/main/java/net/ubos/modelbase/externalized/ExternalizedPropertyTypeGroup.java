//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.ArrayList;
import net.ubos.model.primitives.FloatValue;

/**
 * This is data wanting to become a PropertyTypeGroup, during reading.
 */
public class ExternalizedPropertyTypeGroup
    extends
        ExternalizedMeshType
{
    /**
     * Add to property.
     *
     * @param newValue the new value
     */
    public void addGroupMemberIdentifier(
            String newValue )
    {
        if( theGroupMembers == null ) {
            theGroupMembers = new ArrayList<>();
        }
        theGroupMembers.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<String> getGroupMembers()
    {
        return theGroupMembers;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setSequenceNumber(
            FloatValue newValue )
    {
        if( theSequenceNumber != null ) {
            throw new IllegalStateException( "Have sequenceNumber already: " + theSequenceNumber + " vs " + newValue );
        }
        theSequenceNumber = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public FloatValue getSequenceNumber()
    {
        return theSequenceNumber== null ? FloatValue.ZERO : theSequenceNumber;
    }

    /**
     * The list of Identifiers of PropertyTypes that this PropertyTypeGroup contains.
     */
    protected ArrayList<String> theGroupMembers;

    /**
     * SequenceNumber.
     */
    protected FloatValue theSequenceNumber;
}
