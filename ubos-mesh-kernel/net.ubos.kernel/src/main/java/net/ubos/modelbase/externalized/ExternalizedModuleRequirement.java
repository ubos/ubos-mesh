//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

/**
 * This is data wanting to become a ModuleRequirement, during reading.
 */
public class ExternalizedModuleRequirement
        extends
            ExternalizedDependency
{}
