//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import net.ubos.modelbase.externalized.ExternalizedPropertyType;
import org.xml.sax.Locator;

/**
 * A simple String was given as a default value for a BlobValue, and that
 * is insufficient. We don't want to get the MIME types wrong, for example.
 */
public class UnderspecifiedBlobValueException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param value the underspecified String value
     * @param pt the PropertyType for which this was supposed to be a default value
     * @param locator knows the location of the problem
     */
    public UnderspecifiedBlobValueException(
            String                   value,
            ExternalizedPropertyType pt,
            Locator                  locator )
    {
        super( locator );
        
        theValue        = value;
        thePropertyType = pt;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theValue,
            thePropertyType.getIdentifier() == null ? thePropertyType.getName() : thePropertyType.getIdentifier()
        };
    }

    /**
     * The underspecified String vavlue
     */
    protected final String theValue;
    
    /**
     * The PropertyType for which this was supposed to be a default value
     */
    protected final ExternalizedPropertyType thePropertyType;
}