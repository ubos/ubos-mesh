//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.m;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierDeserializer;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.MeshTypeSynonymDictionary;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.util.logging.Log;

/**
 * An in-memory implementation of MeshTypeSynonymDictionary. This implementation
 * assumes that the ModelBase is set prior to invoking any of the find methods.
 */
public class MMeshTypeSynonymDictionary
        implements
            MeshTypeSynonymDictionary
{
    private static final Log log = Log.getLogInstance( MMeshTypeSynonymDictionary.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @return the created MMeshTypeSynonymDictionary
     */
    public static MMeshTypeSynonymDictionary create()
    {
        return new MMeshTypeSynonymDictionary();
    }

    /**
     * Private constructor, use factory method instead.
     */
    protected MMeshTypeSynonymDictionary()
    {
        // nothing
    }

    /**
     * Set the ModelBase that this MeshTypeSynonymDictionary delegates to.
     *
     * @param modelBase the ModelBase
     */
    public void setModelBase(
            ModelBase modelBase )
    {
        theModelBase = modelBase;
    }

    /**
     * Obtain the ModelBase that this MeshTypeSynonymDictionary delegates to.
     *
     * @return the ModelBase
     */
    @Override
    public ModelBase getModelBase()
    {
        return theModelBase;
    }

    /**
     * Obtain the MeshType for this identifier.
     *
     * @param externalIdentifier the Identifier
     * @return the MeshType
     * @throws MeshTypeNotFoundException thrown if this Identifier was not known
     */
    @Override
    public MeshType findMeshTypeByIdentifier(
            MeshTypeIdentifier externalIdentifier )
        throws
            MeshTypeNotFoundException
    {
        MeshTypeIdentifier internalIdentifier = theMappingTable.get( externalIdentifier );
        if( internalIdentifier == null ) {
            throw new MeshTypeWithIdentifierNotFoundException( externalIdentifier );
        }

        MeshType ret = theModelBase.findMeshType( internalIdentifier );
        return ret;
    }

    /**
     * Add a mapping to the internal table.
     *
     * @param externalIdentifier the identifier as known externally
     * @param internalIdentifier the canonical identifier
     */
    public void addToTable(
            MeshTypeIdentifier externalIdentifier,
            MeshTypeIdentifier internalIdentifier )
    {
        MeshTypeIdentifier found = theMappingTable.put( externalIdentifier, internalIdentifier );
        if( found != null ) {
            throw new IllegalArgumentException( "Redefining external identifier " + externalIdentifier + ", was " + found );
        }
    }

    /**
     * Load a property file containing external/internal mappings.
     *
     * @param inStream the incoming stream
     * @param idSerializer knows how to parse MeshTypeIdentifiers
     * @throws IOException thrown if a input/output problem occurred
     */
    public void loadMappings(
            InputStream                    inStream,
            MeshTypeIdentifierDeserializer idSerializer )
        throws
            IOException
    {
        Properties props = new Properties();
        props.load( inStream );

        for( Object key : props.keySet() ) {

            try {
                Object value = props.get( key );

                MeshTypeIdentifier realKey   = idSerializer.fromExternalForm( (String) key );
                MeshTypeIdentifier realValue = idSerializer.fromExternalForm( (String) value );

                Object found = theMappingTable.put( realKey, realValue );
                if( found != null ) {
                    log.warn( this + ".loadMappings(...): have pair already: " + key + " -> " + found );
                }
            } catch( ParseException ex ) {
                log.error( ex );
            }
        }
    }

    /**
     * The internal table mapping external identifiers to canonical identifiers.
     */
    protected Map<MeshTypeIdentifier,MeshTypeIdentifier> theMappingTable
            = new HashMap<>();

    /**
     * The ModelBase to delegate to.
     */
    protected ModelBase theModelBase;
}
