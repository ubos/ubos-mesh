//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.m;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Arrays;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleClassLoader;
import org.diet4j.core.ModuleException;
import org.diet4j.core.ModuleMeta;
import org.diet4j.core.ModuleNotFoundException;
import org.diet4j.core.ModuleRegistry;
import org.diet4j.core.ModuleRequirement;
import org.diet4j.core.ModuleResolutionCandidateNotUniqueException;
import org.diet4j.core.ModuleResolutionException;
import org.diet4j.core.NoModuleResolutionCandidateException;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierDeserializer;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.modelbase.MeshTypeWithPropertiesNotFoundException;
import net.ubos.modelbase.CollectableMeshTypeNotFoundException;
import net.ubos.modelbase.EntityTypeNotFoundException;
import net.ubos.modelbase.MeshTypeLifecycleEventListener;
import net.ubos.modelbase.MeshTypeLifecycleManager;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.ModelLoader;
import net.ubos.modelbase.ModelLoadingException;
import net.ubos.modelbase.PropertyTypeNotFoundException;
import net.ubos.modelbase.RelationshipTypeNotFoundException;
import net.ubos.modelbase.RoleTypeNotFoundException;
import net.ubos.modelbase.SubjectAreaNotFoundException;
import net.ubos.modelbase.WrongMeshTypeException;
import net.ubos.modelbase.externalized.xml.XmlModelLoader;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;

/**
  * Implementation of ModelBase that holds its content in memory only.
  */
public class MModelBase
        implements
            ModelBase
{
    private static final Log log = Log.getLogInstance( MModelBase.class ); // our own, private logger

    /**
     * Factory method with defaults.
     *
     * @return the created MModelBase
     */
    public static ModelBase create()
    {
        MMeshTypeLifecycleManager                lifecycleManager  = MMeshTypeLifecycleManager.create();
        DefaultMMeshTypeIdentifierBothSerializer bothSerializer    = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;
        MMeshTypeSynonymDictionary               synonymDictionary = MMeshTypeSynonymDictionary.create();

        return new MModelBase( lifecycleManager, bothSerializer, bothSerializer, synonymDictionary );
    }

    /**
     * Factory method.
     *
     * @param lifecycleManager allows the creation of MeshTypes
     * @param userIdDeserializer knows how to deserialize the Strings provided in lieu of MeshTypeIdentifiers
     * @param moduleAwareIdSerializer knows how to serialize MeshTypeIdentifiers into module names for dynamic Model loads
     * @param synonymDictionary the dictionary for MeshTypeIdentifier synonyms to use
     * @return the created MModelBase
     */
    public static MModelBase create(
            MMeshTypeLifecycleManager      lifecycleManager,
            MeshTypeIdentifierDeserializer userIdDeserializer,
            MeshTypeIdentifierSerializer   moduleAwareIdSerializer,
            MMeshTypeSynonymDictionary     synonymDictionary )
    {
        return new MModelBase( lifecycleManager, userIdDeserializer, moduleAwareIdSerializer, synonymDictionary );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param lifecycleManager allows the creation of MeshTypes
     * @param userIdDeserializer knows how to deserialize the Strings provided in lieu of MeshTypeIdentifiers
     * @param moduleAwareIdSerializer knows how to serialize MeshTypeIdentifiers into module names for dynamic Model loads
     * @param synonymDictionary the dictionary for MeshTypeIdentifier synonyms to use
     */
    protected MModelBase(
            MMeshTypeLifecycleManager      lifecycleManager,
            MeshTypeIdentifierDeserializer userIdDeserializer,
            MeshTypeIdentifierSerializer   moduleAwareIdSerializer,
            MMeshTypeSynonymDictionary     synonymDictionary )
    {
        theLifecycleManager        = lifecycleManager;
        theUserIdDeserializer      = userIdDeserializer;
        theModuleAwareIdSerializer = moduleAwareIdSerializer;
        theSynonymDictionary       = synonymDictionary;

        theLifecycleManager.setModelBase( this );
        theSynonymDictionary.setModelBase( this );

        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "constructor" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeLifecycleManager getMeshTypeLifecycleManager()
    {
        return theLifecycleManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshType> iterator()
    {
        return theCluster.iterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea [] getLoadedSubjectAreas()
    {
        return theCluster.getLoadedSubjectAreas();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea [] getOrderedLoadedSubjectAreas()
    {
        SubjectArea [] ret = getLoadedSubjectAreas();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_CASE_INSENSITIVE_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MMeshTypeSynonymDictionary getSynonymDictionary()
    {
        return theSynonymDictionary;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshType findMeshType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshType findMeshType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        MeshType ret = findMeshTypeOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshType findMeshTypeOrNull(
            MeshTypeIdentifier identifier )
    {
        if( identifier == null ) {
            throw new NullPointerException( "Null Identifier given" );
        }

        MeshType ret = theCluster.findLoadedMeshTypeByIdentifier( identifier );
        if( ret == null ) {
            try {
                MeshTypeIdentifier saIdentifier = theUserIdDeserializer.fromExternalForm( identifier.getSubjectAreaPart() );

                attemptToLoadSubjectArea( saIdentifier );
                ret = theCluster.findLoadedMeshTypeByIdentifier( identifier );

            } catch( SubjectAreaNotFoundException ex ) {
                log.info( ex );
                // that's fine
            } catch( ParseException ex ) {
                log.error( ex );
            } catch( IOException ex ) {
                log.error( ex );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshType findMeshTypeOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            MeshType ret = findMeshTypeOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea findSubjectArea(
            MeshTypeIdentifier identifier )
        throws
            SubjectAreaNotFoundException
    {
        SubjectArea ret = findSubjectAreaOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new SubjectAreaNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea findSubjectArea(
            String identifierAsString )
        throws
            ParseException,
            SubjectAreaNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        SubjectArea ret = findSubjectAreaOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new SubjectAreaNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea findSubjectAreaOrNull(
            MeshTypeIdentifier identifier )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "findSubjectAreaOrNull", identifier );
        }

        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret != null ) {
            try {
                return checkType( identifier, ret, SubjectArea.class );
            } catch( WrongMeshTypeException ex ) {
                log.error( ex );
                return null;
            }
        }

        try {
            ret = attemptToLoadSubjectArea( identifier );

        } catch( SubjectAreaNotFoundException ex ) {
            // nothing
        } catch( IOException ex ) {
            log.warn( ex );
        }
        return (SubjectArea) ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea findSubjectAreaOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            SubjectArea ret = findSubjectAreaOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        CollectableMeshType ret = findCollectableMeshTypeOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        CollectableMeshType ret = findCollectableMeshTypeOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeOrNull(
            MeshTypeIdentifier identifier )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "findCollectableMeshTypeOrNull", identifier );
        }

        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret == null ) {
            return null;
        }

        try {
            return checkType( identifier, ret, CollectableMeshType.class );

        } catch( WrongMeshTypeException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            CollectableMeshType ret = findCollectableMeshTypeOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshType(
            SubjectArea sa,
            String      entityTypeName )
        throws
            CollectableMeshTypeNotFoundException
    {
        CollectableMeshType ret = findCollectableMeshTypeOrNull( sa, entityTypeName );
        if( ret != null ) {
            return ret;
        }
        throw new CollectableMeshTypeNotFoundException( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshType(
            String saName,
            String entityTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            CollectableMeshTypeNotFoundException
    {
        SubjectArea sa = findSubjectArea( saName );
        return findCollectableMeshType( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeOrNull(
            SubjectArea sa,
            String      entityTypeName )
    {
        if( sa == null ) {
            throw new IllegalArgumentException( "SubjectArea cannot be null" );
        }
        CollectableMeshType ret = theCluster.findCollectableMeshType( sa, entityTypeName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeOrNull(
            String subjectAreaName,
            String entityTypeName )
    {
        SubjectArea sa = findSubjectAreaOrNull( subjectAreaName );
        if( sa == null ) {
            return null;
        }
        return findCollectableMeshTypeOrNull( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithProperties(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeWithProperties ret = findMeshTypeWithPropertiesOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithProperties(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        MeshTypeWithProperties ret = findMeshTypeWithPropertiesOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            MeshTypeIdentifier identifier )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "findMeshTypeWithPropertiesOrNull", identifier );
        }

        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret == null ) {
            return null;
        }

        try {
            return checkType(identifier, ret, MeshTypeWithProperties.class );

        } catch( WrongMeshTypeException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            MeshTypeWithProperties ret = findMeshTypeWithPropertiesOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithProperties(
            SubjectArea subjectArea,
            String      entityTypeName )
        throws
            MeshTypeWithPropertiesNotFoundException
    {
        MeshTypeWithProperties ret = findMeshTypeWithPropertiesOrNull( subjectArea, entityTypeName );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithPropertiesNotFoundException( subjectArea, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithProperties(
            String subjectAreaName,
            String entityTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            MeshTypeWithPropertiesNotFoundException
    {
        SubjectArea sa = findSubjectAreaOrNull( subjectAreaName );
        if( sa == null ) {
            return null;
        }
        return findMeshTypeWithPropertiesOrNull( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            SubjectArea subjectArea,
            String      entityTypeName )
    {
        if( subjectArea == null ) {
            throw new IllegalArgumentException( "SubjectArea cannot be null" );
        }
        CollectableMeshType ret = theCluster.findCollectableMeshType( subjectArea, entityTypeName );
        if( ret instanceof MeshTypeWithProperties ) {
            return (MeshTypeWithProperties) ret;
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties findMeshTypeWithPropertiesOrNull(
            String subjectAreaName,
            String entityTypeName )
    {
        SubjectArea sa = findSubjectAreaOrNull( subjectAreaName );
        if( sa == null ) {
            return null;
        }
        return findMeshTypeWithPropertiesOrNull( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        EntityType ret = findEntityTypeOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        EntityType ret = findEntityTypeOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityTypeOrNull(
            MeshTypeIdentifier identifier )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "findEntityTypeOrNull", identifier );
        }

        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret == null ) {
            return null;
        }

        try {
            return checkType( identifier, ret, EntityType.class );

        } catch( WrongMeshTypeException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityTypeOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            EntityType ret = findEntityTypeOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityType(
            SubjectArea sa,
            String      entityTypeName )
        throws
            EntityTypeNotFoundException
    {
        EntityType ret = findEntityTypeOrNull( sa, entityTypeName );
        if( ret != null ) {
            return ret;
        }
        throw new EntityTypeNotFoundException( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityType(
            String saName,
            String entityTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            EntityTypeNotFoundException
    {
        SubjectArea sa = findSubjectArea( saName );
        return findEntityType( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityTypeOrNull(
            SubjectArea sa,
            String      entityTypeName )
    {
        if( sa == null ) {
            throw new IllegalArgumentException( "SubjectArea cannot be null" );
        }
        CollectableMeshType ret = theCluster.findCollectableMeshType( sa, entityTypeName );
        if( ret instanceof EntityType ) {
            return (EntityType) ret;
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType findEntityTypeOrNull(
            String subjectAreaName,
            String entityTypeName )
    {
        SubjectArea sa = findSubjectAreaOrNull( subjectAreaName );
        if( sa == null ) {
            return null;
        }
        return findEntityTypeOrNull( sa, entityTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        RelationshipType ret = findRelationshipTypeOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        RelationshipType ret = findRelationshipTypeOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipTypeOrNull(
            MeshTypeIdentifier identifier )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "findRelationshipTypeOrNull", identifier );
        }

        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret == null ) {
            return null;
        }

        try {
            return checkType( identifier, ret, RelationshipType.class );

        } catch( WrongMeshTypeException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipTypeOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            RelationshipType ret = findRelationshipTypeOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipType(
            SubjectArea subjectArea,
            String      relationshipTypeName )
        throws
            RelationshipTypeNotFoundException
    {
        RelationshipType ret = findRelationshipTypeOrNull( subjectArea, relationshipTypeName );
        if( ret != null ) {
            return ret;
        }
        throw new RelationshipTypeNotFoundException( subjectArea, relationshipTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipType(
            String saName,
            String relationshipTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            RelationshipTypeNotFoundException
    {
        SubjectArea sa = findSubjectAreaOrNull( saName );
        return findRelationshipType( sa, relationshipTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipTypeOrNull(
            SubjectArea subjectArea,
            String      relationshipTypeName )
    {
        if( subjectArea == null ) {
            throw new IllegalArgumentException( "SubjectArea cannot be null" );
        }
        CollectableMeshType ret = theCluster.findCollectableMeshType( subjectArea, relationshipTypeName );
        if( ret instanceof RelationshipType ) {
            return (RelationshipType) ret;
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType findRelationshipTypeOrNull(
            String subjectAreaName,
            String relationshipTypeName )
    {
        SubjectArea sa = findSubjectAreaOrNull( subjectAreaName );
        if( sa == null ) {
            return null;
        }
        return findRelationshipTypeOrNull( sa, relationshipTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        PropertyType ret = findPropertyTypeOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        PropertyType ret = findPropertyTypeOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyTypeOrNull(
            MeshTypeIdentifier identifier )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "findPropertyTypeOrNull", identifier );
        }

        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret == null ) {
            return null;
        }

        try {
            return checkType( identifier, ret, PropertyType.class );

        } catch( WrongMeshTypeException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyTypeOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            PropertyType ret = findPropertyTypeOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyType(
            MeshTypeWithProperties meshTypeWithProperties,
            String                 propertyTypeName )
        throws
            PropertyTypeNotFoundException
    {
        PropertyType ret = findPropertyTypeOrNull( meshTypeWithProperties, propertyTypeName );
        if( ret != null ) {
            return ret;
        }
        throw new PropertyTypeNotFoundException( meshTypeWithProperties, propertyTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyType(
            String subjectAreaName,
            String meshTypeWithPropertiesName,
            String propertyTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            MeshTypeWithPropertiesNotFoundException,
            PropertyTypeNotFoundException
    {
        MeshTypeWithProperties amo = findMeshTypeWithProperties( subjectAreaName, meshTypeWithPropertiesName );
        PropertyType ret = findPropertyType( amo, propertyTypeName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyTypeOrNull(
            MeshTypeWithProperties meshTypeWithProperties,
            String                 propertyTypeName )
    {
        if( meshTypeWithProperties == null ) {
            throw new IllegalArgumentException( "MeshTypeWithProperties cannot be null" );
        }
        PropertyType ret = theCluster.findPropertyType( meshTypeWithProperties, propertyTypeName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType findPropertyTypeOrNull(
            String subjectAreaName,
            String meshTypeWithPropertiesName,
            String propertyTypeName )
    {
        MeshTypeWithProperties amo = findMeshTypeWithPropertiesOrNull( subjectAreaName, meshTypeWithPropertiesName );
        if( amo == null ) {
            return null;
        }
        PropertyType ret = findPropertyTypeOrNull( amo, propertyTypeName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleType(
            MeshTypeIdentifier identifier )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        RoleType ret = findRoleTypeOrNull( identifier );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleType(
            String identifierAsString )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

        RoleType ret = findRoleTypeOrNull( identifierAsString );
        if( ret != null ) {
            return ret;
        }
        throw new MeshTypeWithIdentifierNotFoundException( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleTypeOrNull(
            MeshTypeIdentifier identifier )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "findRoleTypeOrNull", identifier );
        }

        MeshType ret = findMeshTypeOrNull( identifier );
        if( ret == null ) {
            return null;
        }

        try {
            return checkType( identifier, ret, RoleType.class );

        } catch( WrongMeshTypeException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleTypeOrNull(
            String identifierAsString )
    {
        try {
            MeshTypeIdentifier identifier = theUserIdDeserializer.fromExternalForm( identifierAsString );

            RoleType ret = findRoleTypeOrNull( identifier );
            return ret;

        } catch( ParseException ex ) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleType(
            SubjectArea subjectArea,
            String      roleTypeName )
        throws
            RoleTypeNotFoundException
    {
        RoleType ret = findRoleTypeOrNull( subjectArea, roleTypeName );
        if( ret != null ) {
            return ret;
        }
        throw new RoleTypeNotFoundException( subjectArea, roleTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleType(
            String subjectAreaName,
            String roleTypeName )
        throws
            ParseException,
            SubjectAreaNotFoundException,
            RoleTypeNotFoundException
    {
        SubjectArea sa = findSubjectAreaOrNull( subjectAreaName );
        return findRoleType( sa, roleTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleTypeOrNull(
            SubjectArea subjectArea,
            String      roleTypeName )
    {
        if( subjectArea == null ) {
            throw new IllegalArgumentException( "SubjectArea cannot be null" );
        }
        CollectableMeshType ret = theCluster.findCollectableMeshType( subjectArea, roleTypeName );
        if( ret instanceof RoleType ) {
            return (RoleType) ret;
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType findRoleTypeOrNull(
            String subjectAreaName,
            String roleTypeName )
    {
        SubjectArea sa = findSubjectAreaOrNull( subjectAreaName );
        if( sa == null ) {
            return null;
        }
        return findRoleTypeOrNull( sa, roleTypeName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea [] loadModel(
            String         xmlResource,
            InputStream    stream,
            ClassLoader    classLoader )
        throws
            ModelLoadingException,
            IOException
    {
        return loadModel( xmlResource, stream, classLoader, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea [] loadModel(
            String         xmlResource,
            InputStream    stream,
            ClassLoader    classLoader,
            TimeStampValue now )
        throws
            ModelLoadingException,
            IOException
    {
        if( now == null ) {
            now = TimeStampValue.now();
        }

        ModelLoader theLoader = new XmlModelLoader(
                            this,
                            stream,
                            xmlResource,
                            classLoader, // FIXME?
                            classLoader );
        SubjectArea [] ret = theLoader.loadAndCheckModel( getMeshTypeLifecycleManager(), now );
        return ret;
    }

    /**
     * This internal method is called when there is an attempt to access a SubjectArea
     * which is not present in the working model. This method will attempt to load it, and returns
     * true if it was successful. This must only be called if this SubjectArea has not been loaded before.
     *
     * @param saId the identifier of the SubjectArea
     * @return the found SubjectArea
     * @throws SubjectAreaNotFoundException thrown if the SubjectArea was not found
     * @throws IOException thrown if the file could not be read
     */
    public SubjectArea attemptToLoadSubjectArea(
            MeshTypeIdentifier saId )
        throws
            SubjectAreaNotFoundException,
            IOException
    {
        SubjectArea ret;
        ClassLoader cl = getClass().getClassLoader();
        if( cl instanceof ModuleClassLoader && ((ModuleClassLoader)cl).getModuleRegistry() != null ) {
            try {
               ret = attemptToLoadSubjectAreaWithModuleRegistry( ((ModuleClassLoader)cl).getModuleRegistry(), saId );

            } catch( ModuleException ex ) {
                throw new SubjectAreaNotFoundException( saId, ex );
            } catch( ParseException ex ) {
                throw new SubjectAreaNotFoundException( saId, ex );
            }
        } else {
            try {
                ret = attemptToLoadSubjectAreaWithoutModuleRegistry( saId );

            } catch( ModelLoadingException ex ) {
                throw new SubjectAreaNotFoundException( saId, ex );
            }
        }
        return ret;
    }

    /**
     * This internal method is called when there is an attempt to access a SubjectArea
     * which is not present in the working model. This method will attempt to load it without the
     * Module Framework. It returns true if it was successful. This must only be called
     * if this SubjectArea has not been loaded before.
     *
     * @param saId the identifier of the SubjectArea
     * @return the found SubjectArea
     * @throws ModelLoadingException loading the model failed
     * @throws IOException thrown if the file could not be read
     */
    protected SubjectArea attemptToLoadSubjectAreaWithoutModuleRegistry(
            MeshTypeIdentifier saId )
        throws
            ModelLoadingException,
            IOException
    {
        String saName = theModuleAwareIdSerializer.toExternalForm( saId );

        StringBuilder path = new StringBuilder();
        path.append( "models/" );
        path.append( saName );
        path.append( ".xml" );

        String realPath = path.toString();

        ClassLoader cl     = getClass().getClassLoader();
        InputStream stream = cl.getResourceAsStream( realPath );

        if( stream == null ) {
            return null;
        }

        ModelLoader theLoader = new XmlModelLoader(
                            this,
                            stream,
                            realPath,
                            cl, // FIXME?
                            cl );
        SubjectArea [] ret = theLoader.loadAndCheckModel( getMeshTypeLifecycleManager(), TimeStampValue.now() );
        return ret[0];
    }

    /**
     * This internal method is called when there is an attempt to access a SubjectArea
     * which is not present in the working model. This method will attempt to load it using the
     * Module Framework. It returns true if it was successful. This must only be called
     * if this SubjectArea has not been loaded before.
     *
     * @param registry the ModuleRegistry to use
     * @param saId the identifier of the SubjectArea
     * @return the found SubjectArea
     * @throws ModuleNotFoundException thrown if the ModelModule was not found
     * @throws ModuleResolutionException thrown if the found ModelModule's dependencies could not be resolved
     * @throws ModuleActivationException thrown if the found ModelModule could not be activated
     * @throws ModuleResolutionCandidateNotUniqueException thrown if a dependency could not be uniquely resolved
     * @throws NoModuleResolutionCandidateException thrown if a dependency could not be resolved at all
     * @throws ParseException thrown if the saName had invalid syntax
     */
    protected SubjectArea attemptToLoadSubjectAreaWithModuleRegistry(
            ModuleRegistry     registry,
            MeshTypeIdentifier saId )
        throws
            ModuleNotFoundException,
            ModuleResolutionException,
            ModuleActivationException,
            ModuleResolutionCandidateNotUniqueException,
            NoModuleResolutionCandidateException,
            ParseException
    {
        String saModuleName = theModuleAwareIdSerializer.toExternalForm( saId );

        ModuleRequirement saRequirement = ModuleRequirement.parse( saModuleName );
        ModuleMeta        saCandidate   = registry.determineSingleResolutionCandidate( saRequirement );

        Module saModule = registry.resolve( saCandidate, true );

        saModule.activateRecursively();

        return theCluster.findSubjectArea( saId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkSubjectArea(
            SubjectArea candidate )
        throws
            IllegalArgumentException
    {
        if( candidate.getIdentifier() == null ) {
            throw new IllegalArgumentException( "SubjectArea has no Identifier: " + candidate );
        }
        if( candidate.getName() == null ) {
            throw new IllegalArgumentException( "SubjectArea has no Name: " + candidate );
        }
        if( candidate.getUserVisibleName() == null ) {
            throw new IllegalArgumentException( "SubjectArea has no UserName: " + candidate );
        }
        if( candidate.getCollectableMeshTypes() == null ) {
            throw new IllegalArgumentException( "SubjectArea has no CollectableMeshTypes: " + candidate );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkEntityType(
            EntityType candidate )
        throws
            IllegalArgumentException
    {
        if( candidate.getIdentifier() == null ) {
            throw new IllegalArgumentException( "EntityType has no Identifier: " + candidate );
        }
        if( candidate.getName() == null ) {
            throw new IllegalArgumentException( "EntityType has no Name: " + candidate );
        }
        if( candidate.getUserVisibleName() == null ) {
            throw new IllegalArgumentException( "EntityType has no UserName: " + candidate );
        }
        if( candidate.getSubjectArea() == null ) {
            throw new IllegalArgumentException( "EntityType has no SubjectArea: " + candidate );
        }
        if( candidate.getIsAbstract() == null ) {
            throw new IllegalArgumentException( "EntityType has no IsAbstract: " + candidate );
        }
        if( candidate.getDirectSubtypes() == null ) {
            throw new IllegalArgumentException( "EntityType has no direct subtypes: " + candidate );
        }
        if( candidate.getDirectSupertypes() == null ) {
            throw new IllegalArgumentException( "EntityType has no direct supertypes: " + candidate );
        }
        if( candidate.getLocalPropertyTypeGroups() == null ) {
            throw new IllegalArgumentException( "EntityType has no local PropertyTypeGroups: " + candidate );
        }
        if( candidate.getLocalPropertyTypes() == null ) {
            throw new IllegalArgumentException( "EntityType has no local PropertyTypes: " + candidate );
        }
        if( candidate.getOverridingLocalPropertyTypes() == null ) {
            throw new IllegalArgumentException( "EntityType has no overriding PropertyTypes: " + candidate );
        }
        if( candidate.getLocalRoleTypes() == null ) {
            throw new IllegalArgumentException( "EntityType has no local RoleTypes: " + candidate );
        }
        // check that getAllPropertyTypes does not produce an exception -- if it does, something is wrong
        candidate.getPropertyTypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkRelationshipType(
            RelationshipType candidate )
        throws
            IllegalArgumentException
    {
        if( candidate.getIdentifier() == null ) {
            throw new IllegalArgumentException( "RelationshipType has no Identifier: " + candidate );
        }
        if( candidate.getName() == null ) {
            throw new IllegalArgumentException( "RelationshipType has no Name: " + candidate );
        }
        if( candidate.getUserVisibleName() == null ) {
            throw new IllegalArgumentException( "RelationshipType has no UserName: " + candidate );
        }
        if( candidate.getSubjectArea() == null ) {
            throw new IllegalArgumentException( "RelationshipType has no SubjectArea: " + candidate );
        }
        if( candidate.getIsAbstract() == null ) {
            throw new IllegalArgumentException( "RelationshipType has no IsAbstract: " + candidate );
        }
        if( candidate.getSource() == null ) {
            throw new IllegalArgumentException( "RelationshipType has no Source: " + candidate );
        }
        if( candidate.getDestination() == null ) {
            throw new IllegalArgumentException( "RelationshipType has no Destination: " + candidate );
        }

        checkRoleType( candidate.getSource() );
        checkRoleType( candidate.getDestination() );
    }

    /**
     * Helper method to check whether the right subtype of MeshType is being returned. If not, throw exception.
     *
     * @param identifier the Identifier of the found type
     * @param type the MeshType
     * @param clazz the Class of MeshType expected
     * @return the subtype of MeshType corresponding to clazz, or null if does not
     * @throws WrongMeshTypeException a MeshType of the wrong type was found
     * @param <T> the type parameter
     */
    @SuppressWarnings(value={"unchecked"})
    protected <T> T checkType(
            MeshTypeIdentifier identifier,
            MeshType           type,
            Class<T>           clazz )
        throws
            WrongMeshTypeException
    {
        if( clazz.isInstance( type )) {
            return (T) type;
        } else {
            Class<? extends MeshType> foundType = null;

            if( type instanceof EntityType ) {
                foundType = EntityType.class;
            } else if( type instanceof RelationshipType ) {
                foundType = RelationshipType.class;
            } else if( type instanceof SubjectArea ) {
                foundType = SubjectArea.class;
            } else if( type instanceof PropertyTypeGroup ) {
                foundType = PropertyTypeGroup.class;
            } else if( type instanceof PropertyType ) {
                foundType = PropertyType.class;
            } else {
                log.error( "unexpected type: " + type );
            }
            throw new WrongMeshTypeException( identifier, foundType );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPropertyType(
            PropertyType candidate )
        throws
            IllegalArgumentException
    {
        if( candidate.getIdentifier() == null ) {
            throw new IllegalArgumentException( "PropertyType has no Identifier: " + candidate );
        }
        if( candidate.getName() == null ) {
            throw new IllegalArgumentException( "PropertyType has no Name: " + candidate );
        }
        if( candidate.getUserVisibleName() == null ) {
            throw new IllegalArgumentException( "PropertyType has no UserName: " + candidate );
        }
        if( candidate.getSubjectArea() == null ) {
            throw new IllegalArgumentException( "PropertyType has no SubjectArea: " + candidate );
        }
        if( candidate.getDataType() == null ) {
            throw new IllegalArgumentException( "PropertyType has no DataType: " + candidate );
        }
        if( candidate.getIsOptional() == null ) {
            throw new IllegalArgumentException( "PropertyType has no IsOptional: " + candidate );
        }
        if( candidate.getIsReadOnly() == null ) {
            throw new IllegalArgumentException( "PropertyType has no IsReadOnly: " + candidate );
        }
        if( candidate.getMeshTypeWithProperties() == null ) {
            throw new IllegalArgumentException( "PropertyType has no MeshTypeWithProperties: " + candidate );
        }
        if( candidate.getSequenceNumber() == null ) {
            throw new IllegalArgumentException( "PropertyType has no SequenceNumber: " + candidate );
        }
        // FIXME: check datatype wrt overridden PropertyTypes and default value
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPropertyTypeGroup(
            PropertyTypeGroup candidate )
        throws
            IllegalArgumentException
    {
        if( candidate.getIdentifier() == null ) {
            throw new IllegalArgumentException( "PropertyTypeGroup has no Identifier: " + candidate );
        }
        if( candidate.getName() == null ) {
            throw new IllegalArgumentException( "PropertyTypeGroup has no Name: " + candidate );
        }
        if( candidate.getUserVisibleName() == null ) {
            throw new IllegalArgumentException( "PropertyTypeGroup has no UserName: " + candidate );
        }
        if( candidate.getContainedPropertyTypes() == null ) {
            throw new IllegalArgumentException( "PropertyTypeGroup contains no PropertyTypes: " + candidate );
        }
        if( candidate.getSequenceNumber() == null ) {
            throw new IllegalArgumentException( "PropertyTypeGroup has no SequenceNumber: " + candidate );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkRoleType(
            RoleType candidate )
    {
        if( candidate.getDirectSubtypes() == null ) {
            throw new IllegalArgumentException( "RoleType has no direct subtypes: " + candidate );
        }
        if( candidate.getDirectSupertypes() == null ) {
            throw new IllegalArgumentException( "RoleType has no direct supertypes: " + candidate );
        }
        if( candidate.getLocalPropertyTypeGroups() == null ) {
            throw new IllegalArgumentException( "RoleType has no local PropertyTypeGroups: " + candidate );
        }
        if( candidate.getLocalPropertyTypes() == null ) {
            throw new IllegalArgumentException( "RoleType has no local PropertyTypes: " + candidate );
        }
        if( candidate.getOverridingLocalPropertyTypes() == null ) {
            throw new IllegalArgumentException( "RoleType has no overriding PropertyTypes: " + candidate );
        }
        // check that getAllPropertyTypes does not produce an exception -- if it does, something is wrong
        candidate.getPropertyTypes();
    }

    /**
     * Remove a MeshType. This only exists for testing instrumentation. Do not invoke.
     *
     * @param toRemove the MeshType to remove
     */
    public void remove(
            MeshType toRemove )
    {
        theCluster.remove( toRemove );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDirectMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener )
    {
        theCluster.addDirectMeshTypeLifecycleEventListener( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addWeakMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener )
    {
        theCluster.addWeakMeshTypeLifecycleEventListener( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addSoftMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener )
    {
        theCluster.addSoftMeshTypeLifecycleEventListener( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener oldListener )
    {
        theCluster.removeMeshTypeLifecycleEventListener( oldListener );
    }

    /**
     * For a MeshType by its unique identifier if it is already in the working Model.
     * Do not attempt to load it.
     *
     * @param identifier Identifier of the to-be-found MeshType
     * @return the found MeshType, or null
     */
    protected MeshType findLoadedMeshTypeOrNull(
            MeshTypeIdentifier identifier )
    {
        return theCluster.findLoadedMeshTypeByIdentifier( identifier );
    }

    /**
      * The life cycle manager -- allocated smartly.
      */
    protected final MMeshTypeLifecycleManager theLifecycleManager;

    /**
     * Knows how to convert MeshTypeIdentifiers into Module names, for on-demand loading of Models.
     */
    protected final MeshTypeIdentifierSerializer theModuleAwareIdSerializer;

    /**
     * Used for finding MeshTypes by String name.
     */
    protected final MeshTypeIdentifierDeserializer theUserIdDeserializer;

    /**
      * The actual storage.
      */
    protected final MMeshTypeStore theCluster = new MMeshTypeStore( this );

    /**
     * Dictionary of synonyms.
     */
    protected final MMeshTypeSynonymDictionary theSynonymDictionary;
}
