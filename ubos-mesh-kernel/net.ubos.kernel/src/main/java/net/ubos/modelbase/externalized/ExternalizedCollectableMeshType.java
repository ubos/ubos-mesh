//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

/**
 * This is data wanting to become an CollectableMeshType, during reading.
 */
public abstract class ExternalizedCollectableMeshType
    extends
        ExternalizedMeshType
{}
