//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.HashMap;
import java.util.Map;
import net.ubos.model.primitives.PropertyValue;

/**
 * This is data wanting to become an EnumeratedValue, during reading.
 */
public class ExternalizedEnum
{
    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setValue(
            String newValue )
    {
        if( theValue != null ) {
            throw new IllegalStateException( "Have value already: " + theValue + " vs " + newValue );
        }
        theValue = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public String getValue()
    {
        return theValue;
    }

    /**
     * Add to property.
     *
     * @param key the locale
     * @param newValue the new value in the given locale
     */
    public void addUserName(
            String        key,
            PropertyValue newValue )
    {
        if( theUserNames == null ) {
            theUserNames = new HashMap<>();
        }
        theUserNames.put( key, newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public Map<String,PropertyValue> getUserNames()
    {
        return theUserNames;
    }

    /**
     * Add to property.
     *
     * @param key the locale
     * @param newValue the new value in the given locale
     */
    public void addUserDescription(
            String        key,
            PropertyValue newValue )
    {
        if( theUserDescriptions == null ) {
            theUserDescriptions = new HashMap<>();
        }
        theUserDescriptions.put( key, newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public Map<String,PropertyValue> getUserDescriptions()
    {
        return theUserDescriptions;
    }

    /**
     * Convert to String, for user error messages.
     *
     * @return String form of this object
     */
    @Override
    public String toString()
    {
        return "Enumerated value: " + theValue;
    }

    /**
      * The actual value.
      */
    protected String theValue = null;

    /**
     * The set of localized user names, keyed by locale, if any.
     */
    protected HashMap<String,PropertyValue> theUserNames;

    /**
     * The set of localized user descriptions, keyed by locale, if any.
     */
    protected HashMap<String,PropertyValue> theUserDescriptions;
}
