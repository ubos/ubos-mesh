//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import net.ubos.model.primitives.BooleanValue;

/**
 * This is data wanting to become a RelationshipType, during reading.
 */
public class ExternalizedRelationshipType
    extends
        ExternalizedCollectableMeshType
{
    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setSource(
            ExternalizedRoleType newValue )
    {
        if( theSource != null ) {
            throw new IllegalStateException( "Have source already: " + theSource + " vs " + newValue );
        }
        theSource = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ExternalizedRoleType getSource()
    {
        return theSource;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setDestination(
            ExternalizedRoleType newValue )
    {
        if( theDestination != null ) {
            throw new IllegalStateException( "Have distination already: " + theDestination + " vs " + newValue );
        }
        theDestination = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ExternalizedRoleType getDestination()
    {
        return theDestination;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setSourceDestination(
            ExternalizedRoleType newValue )
    {
        if( theSourceDestination != null ) {
            throw new IllegalStateException( "Have source-destination already: " + theSourceDestination + " vs " + newValue );
        }
        theSourceDestination = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ExternalizedRoleType getSourceDestination()
    {
        return theSourceDestination;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setIsAbstract(
            BooleanValue newValue )
    {
        if( theIsAbstract != null ) {
            throw new IllegalStateException( "Have isAbstract already: " + theIsAbstract + " vs " + newValue );
        }
        theIsAbstract = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public BooleanValue getIsAbstract()
    {
        return theIsAbstract == null ? BooleanValue.FALSE : theIsAbstract;
    }

    /**
     * This describes the source RoleType.
     */
    protected ExternalizedRoleType theSource;

    /**
     * This describes the source RoleType.
     */
    protected ExternalizedRoleType theDestination;

    /**
     * If this is a undirected RelationshipType (but only then), we use this instead of src and/or dest.
     */
    protected ExternalizedRoleType theSourceDestination;

    /**
     * IsAbstract.
     */
    protected BooleanValue theIsAbstract;
}
