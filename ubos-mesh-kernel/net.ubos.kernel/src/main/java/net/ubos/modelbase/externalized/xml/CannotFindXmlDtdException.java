//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * An XML DTD was specified but could not be found.
 */
public class CannotFindXmlDtdException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param path the path to the DTD.
     * @param locator knows the location of the problem
     */
    public CannotFindXmlDtdException(
            String path,
            Locator locator )
    {
        super( locator );

        thePath = path;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            thePath
        };
    }

    /**
     * The path to the DTD.
     */
    protected final String thePath;
}
