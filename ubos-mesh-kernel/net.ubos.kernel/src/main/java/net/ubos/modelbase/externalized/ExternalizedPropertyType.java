//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.ArrayList;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;

/**
 * This is data wanting to become a PropertyType, during reading.
 */
public class ExternalizedPropertyType
    extends
        ExternalizedMeshType
{
    /**
     * Add to property.
     *
     * @param newValue the new value
     */
    public void addToOverride(
            String newValue )
    {
        if( toOverride == null ) {
            toOverride = new ArrayList<>();
        }
        toOverride.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<String> getToOverrides()
    {
        return toOverride;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setDataType(
            DataType newValue )
    {
        if( theDataType != null ) {
            throw new IllegalStateException( "Have DataType already: " + theDataType + " vs " + newValue );
        }
        theDataType = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public DataType getDataType()
    {
        return theDataType;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setDefaultValue(
            PropertyValue newValue )
    {
        if( theDefaultValue != null ) {
            throw new IllegalStateException( "Have default value already: " + theDefaultValue + " vs " + newValue );
        }
        theDefaultValue = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public PropertyValue getDefaultValue()
    {
        return theDefaultValue;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setIsOptional(
            BooleanValue newValue )
    {
        if( theIsOptional != null ) {
            throw new IllegalStateException( "Have isOptional already: " + theIsOptional + " vs " + newValue );
        }
        theIsOptional = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public BooleanValue getIsOptional()
    {
        return theIsOptional == null ? BooleanValue.FALSE : theIsOptional;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setIsReadOnly(
            BooleanValue newValue )
    {
        if( theIsReadOnly != null ) {
            throw new IllegalStateException( "Have isReadOnly already: " + theIsReadOnly + " vs " + newValue );
        }
        theIsReadOnly = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public BooleanValue getIsReadOnly()
    {
        return theIsReadOnly == null ? BooleanValue.FALSE : theIsReadOnly;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setSequenceNumber(
            FloatValue newValue )
    {
        if( theSequenceNumber != null ) {
            throw new IllegalStateException( "Have sequenceNumber already: " + theSequenceNumber + " vs " + newValue );
        }
        theSequenceNumber = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public FloatValue getSequenceNumber()
    {
        return theSequenceNumber == null ? FloatValue.ZERO : theSequenceNumber;
    }

    /**
     * The list of Identifiers of PropertyTypes that this one overrides.
     */
    protected ArrayList<String> toOverride;

    /**
     * The DataType of this PropertyType.
     */
    protected DataType theDataType;

    /**
     * DefaultValue if given.
     */
    protected PropertyValue theDefaultValue;

    /**
     * IsOptional.
     */
    protected BooleanValue theIsOptional;

    /**
     * IsReadOnly.
     */
    protected BooleanValue theIsReadOnly;

    /**
     * SequenceNumber.
     */
    protected FloatValue theSequenceNumber;
}
