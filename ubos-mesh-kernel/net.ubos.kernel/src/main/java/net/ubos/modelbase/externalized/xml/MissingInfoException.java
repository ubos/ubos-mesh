//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * Some information was missing.
 * A SubjectArea's MeshTypeIdentifier was not specified.
 */
public abstract class MissingInfoException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     *
     * @param locator knows the location of the problem
     */
    protected MissingInfoException(
            Locator locator )
    {
        super( locator );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber()
        };
    }

    /**
     * The identifier for a SubjectArea was missing.
     */
    public static class SubjectAreaIdentifier
        extends
            MissingInfoException
    {
        /**
         * Constructor.
         *
         * @param locator knows the location of the problem
         */
        protected SubjectAreaIdentifier(
                Locator locator )
        {
            super( locator );
        }
    }

    /**
     * The identifier of a SubjectArea dependency was missing.
     */
    public static class SubjectAreaDependencyIdentifier
        extends
            MissingInfoException
    {
        /**
         * Constructor.
         *
         * @param locator knows the location of the problem
         */
        protected SubjectAreaDependencyIdentifier(
                Locator locator )
        {
            super( locator );
        }
    }
}
