//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * A field with this name does not have the correct DataType on the provided DataType.
 */
public class InvalidFieldException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param type the DataType on which the field exists
     * @param fieldName the name of the field
     * @param fieldValue the value of the field that was invalid
     * @param locator knows the location of the problem
     */
    public InvalidFieldException(
            Class<? extends DataType> type,
            String                    fieldName,
            Object                    fieldValue,
            Locator locator )
    {
        super( locator );

        theType       = type;
        theFieldName  = fieldName;
        theFieldValue = fieldValue;
    }    

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theType,
            theFieldName,
            theFieldValue
        };
    }

    /**
     * The DataType on which the field exists,
     */
    protected final Class<? extends DataType> theType;
    
    /**
     * The name of the field.
     */
    protected final String theFieldName;
    
    /**
     * The value of the field that was invalid.
     */
    protected final Object theFieldValue;
}
