//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.logging.Dumper;

/**
 * This Exception indicates that a CollectableMeshObject could not be found.
 */
public class CollectableMeshTypeNotFoundException
    extends
        MeshTypeNotFoundException
{
    /**
     * Constructor.
     *
     * @param subjectArea the SubjectArea that does not contain the CollectableMeshObject
     * @param name the name of the CollectableMeshObject
     */
    public CollectableMeshTypeNotFoundException(
            SubjectArea subjectArea,
            String      name )
    {
        super();

        theSubjectArea = subjectArea;
        theName        = name;
    }

    /**
     * Constructor.
     *
     * @param subjectArea the SubjectArea that does not contain the CollectableMeshObject
     * @param name the name of the CollectableMeshObject
     * @param cause the underlying cause
     */
    public CollectableMeshTypeNotFoundException(
            SubjectArea subjectArea,
            String      name,
            Throwable   cause )
    {
        super( cause );

        theSubjectArea = subjectArea;
        theName        = name;
    }

    /**
     * Obtain the SubjectArea in which the CollectableMeshObject was not found.
     *
     * @return the SubjectArea
     */
    public SubjectArea getSubjectArea()
    {
        return theSubjectArea;
    }

    /**
     * Obtain the name of the CollectableMeshObject that was not found.
     *
     * @return the name
     */
    public String getName()
    {
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                        "SubjectArea",
                        "Name"
                },
                new Object[] {
                        theSubjectArea,
                        theName
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return super.toString() + "{ id: " + theSubjectArea.getIdentifier() + ", " + theName + " }";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
                theSubjectArea,
                theName
        };
    }

    /**
     * The SubjectArea in which the CollectableMeshObject could not be found.
     */
    protected SubjectArea theSubjectArea;

    /**
     * The name of the CollectableMeshObject that could not be found.
     */
    protected String theName;
}
