//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.regex.Pattern;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.l10.L10StringMapImpl;

/**
 * This is data wanting to become a regular expression, during reading.
 */
public class ExternalizedRegex
{
    /**
     * Set property.
     *
     * @param newValue the new thePattern
     */
    public void setRegexString(
            String newValue )
    {
        if( thePattern != null ) {
            throw new IllegalStateException( "Have pattern already: " + thePattern + " vs " + newValue );
        }
        thePattern = newValue;
    }

    /**
     * Get property.
     *
     * @return the thePattern
     */
    public String getRegexString()
    {
        return thePattern;
    }

    /**
     * Set default thePattern.
     *
     * @param newValue the new thePattern
     */
    public void setDefaultValue(
            String newValue )
    {
        if( theDefaultValue != null ) {
            throw new IllegalStateException( "Have defaeult value already: " + theDefaultValue + " vs " + newValue );
        }
        theDefaultValue = newValue;
    }

    /**
     * Get the default thePattern.
     *
     * @return the thePattern
     */
    public String getDefaultValue()
    {
        return theDefaultValue;
    }

    /**
     * Add an error message in a particular locale.
     *
     * @param newLocale the locale
     * @param newValue the new error message
     */
    public void addRegexErrorString(
            String newLocale,
            String newValue )
    {
        if( theErrorMessages == null ) {
            theErrorMessages = L10StringMapImpl.create( null );
        }
        if( newLocale == null || newLocale.length() == 0 ) {
            theErrorMessages.setDefault( newValue );
        } else {
            theErrorMessages.put( newLocale, newValue );
        }
    }

    /**
     * Get property as a Pattern.
     *
     * @return the Pattern
     */
    public Pattern getAsPattern()
    {
        if( thePattern != null ) {
            Pattern ret = Pattern.compile( thePattern );
            return ret;
        } else {
            return null;
        }
    }

    /**
     * Get default thePattern as StringValue.
     *
     * @return the StringValue
     */
    public StringValue getDefaultAsStringValue()
    {
        if( theDefaultValue != null ) {
            return StringValue.create( theDefaultValue );
        }else {
            return null;
        }
    }

    /**
     * Get as StringDataType.
     *
     * @return the StringDataType
     */
    public StringDataType getAsStringDataType()
    {
        StringDataType ret;
        if( thePattern == null ) {
            if( theDefaultValue == null ) {
                ret = StringDataType.create();
            } else {
                ret = StringDataType.create( StringValue.create( theDefaultValue ));
            }
        } else {
            Pattern p = Pattern.compile( thePattern );

            if( theDefaultValue != null ) {
                ret = StringDataType.create( p, StringValue.create( theDefaultValue ), theErrorMessages );
            } else {
                throw new IllegalArgumentException( "Cannot create a StringDataType that has a regex but no default value" );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "Regular expression " + thePattern;
    }

    /**
      * The actual pattern.
      */
    protected String thePattern;

    /**
     * The default value.
     */
    protected String theDefaultValue;

    /**
     * Error messages, by locale.
     */
    protected L10StringMapImpl theErrorMessages;
}
