//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.SubjectArea;

/**
 * This Exception indicates that a RelationshipType could not be found.
 */
public class RelationshipTypeNotFoundException
        extends
            MeshTypeWithPropertiesNotFoundException
{
    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which a RelationshipType could not be found
     * @param relationshipTypeName the name of the RelationshipType that could not be found
     */
    public RelationshipTypeNotFoundException(
            SubjectArea sa,
            String relationshipTypeName )
    {
        super( sa, relationshipTypeName );
    }

    /**
     * Constructor.
     *
     * @param sa the SubjectArea within which a RelationshipType could not be found
     * @param relationshipTypeName the name of the RelationshipType that could not be found
     * @param cause the Throwable that caused this Exception
     */
    public RelationshipTypeNotFoundException(
            SubjectArea sa,
            String      relationshipTypeName,
            Throwable   cause )
    {
        super( sa, relationshipTypeName, cause );
    }
}
