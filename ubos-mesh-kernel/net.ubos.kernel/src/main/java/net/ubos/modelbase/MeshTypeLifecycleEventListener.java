//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

/**
  * This interface is supported by all objects that are interested in
  * being notified of MeshTypeLifecycleEvents.
  *
  * The deletion or change of of MeshTypes is currently not supported.
  */
public interface MeshTypeLifecycleEventListener
{
    /**
      * Indicates that a MeshType has been created in this ModelBase.
      *
      * @param theEvent event indicating the creation of a MeshType
      */
    public void objectCreated(
            MeshTypeLifecycleEvent theEvent );
}
