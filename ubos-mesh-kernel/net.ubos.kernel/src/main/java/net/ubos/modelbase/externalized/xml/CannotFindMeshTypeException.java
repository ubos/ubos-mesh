//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * A MeshType could not be found.
 */
public class CannotFindMeshTypeException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param id identifier of the MeshType that could not be found
     * @param locator knows the location of the problem
     */
    public CannotFindMeshTypeException(
            MeshTypeIdentifier id,
            Locator            locator )
    {
        super( locator );

        theIdentifier = id;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theIdentifier
        };
    }

    /**
     * The identifier of the MeshType that could not be found.
     */
    protected final MeshTypeIdentifier theIdentifier;
}
