//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.m;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierNotUniqueException;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.m.MMeshTypeWithProperties;
import net.ubos.model.primitives.m.MEntityType;
import net.ubos.model.primitives.m.MPropertyType;
import net.ubos.model.primitives.m.MPropertyTypeGroup;
import net.ubos.model.primitives.m.MRelationshipType;
import net.ubos.model.primitives.m.MRoleType;
import net.ubos.model.primitives.m.MSubjectArea;
import net.ubos.modelbase.InheritanceConflictException;
import net.ubos.modelbase.MeshTypeLifecycleManager;
import net.ubos.util.ArrayHelper;
import net.ubos.model.primitives.MeshTypeWithProperties;

/**
 * The MeshTypeLifecycleManager implementation for the in-memory implementation
 * of ModelBase.
 */
public class MMeshTypeLifecycleManager
        implements
            MeshTypeLifecycleManager
{
    /**
     * Factory method.
     *
     * @return the created MMeshTypeLifecycleManager
     */
    public static MMeshTypeLifecycleManager create()
    {
        return new MMeshTypeLifecycleManager();
    }

    /**
      * Constructor.
      */
    protected MMeshTypeLifecycleManager()
    {
        // nothing
    }

    /**
      * Set the ModelBase that this MeshTypeLifecycleManager belongs to.
      *
      * @param mb the ModelBase that this MeshTypeLifecycleManager belongs to
      */
    public void setModelBase(
            MModelBase mb )
    {
        this.theModelBase = mb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType createEntityType(
            MeshTypeIdentifier      identifier,
            StringValue             theName,
            L10PropertyValueMap     theUserNames,
            L10PropertyValueMap     theUserDescriptions,
            BlobValue               theIcon,
            SubjectArea             theSubjectArea,
            EntityType []           supertypes,
            MeshTypeIdentifier []   synonyms,
            BooleanValue            isAbstract,
            BooleanValue            mayBeUsedAsForwardReference )
    {
        if( identifier == null ) {
            throw new IllegalArgumentException( "To-be-created EntityType cannot have null Identifier" );
        }
        if( theName == null ) {
            throw new IllegalArgumentException( "To-be-created EntityType with Identifier " + identifier + " cannot have null Name" );
        }
        if( theUserNames == null || theUserNames.getDefault() == null ) {
            throw new IllegalArgumentException( "To-be-created EntityType with Identifier " + identifier + " cannot have empty UserNames" );
        }
        if( theSubjectArea == null ) {
            throw new IllegalArgumentException( "To-be-created EntityType with Identifier " + identifier + " cannot have null SubjectArea" );
        }
        if( supertypes == null ) {
            supertypes = new MEntityType[0];
        }
        if( synonyms == null ) {
            synonyms = new MeshTypeIdentifier[0];
        }
        if( isAbstract == null ) {
            throw new IllegalArgumentException( "To-be-created EntityType with Identifier " + identifier + " cannot have null IsAbstract" );
        }

        MeshType already = theModelBase.findLoadedMeshTypeOrNull( identifier );
        if( already != null ) {
            throw new MeshTypeIdentifierNotUniqueException( already );
        }

        MSubjectArea   realSubjectArea = (MSubjectArea) theSubjectArea;
        MEntityType [] realSupertypes  = copyIntoNewArray( supertypes, MEntityType.class );

        MEntityType ret = new MEntityType( (MMeshTypeIdentifier) identifier );

        ret.setName( theName );
        ret.setUserVisibleNameMap( theUserNames );
        ret.setUserVisibleDescriptionMap( theUserDescriptions );

        if( theIcon != null ) {
            ret.setIcon( theIcon );
        }
        realSubjectArea.addCollectableMeshType( ret );
        ret.setSubjectArea( realSubjectArea );

        ret.setDirectSupertypes( realSupertypes );

        if( supertypes != null ) {
            for( int i=0 ; i<supertypes.length ; ++i ) {
                if( supertypes[i] == null ) {
                    throw new IllegalArgumentException( "To-be-created EntityType with Identifier " + identifier + " cannot have null supertype" );
                }
                realSupertypes[i].addDirectSubtype( ret );
            }
        }
        ret.setSynonyms( synonyms );

        ret.setIsAbstract( isAbstract );
        ret.setMaybeUsedAsForwardReference( mayBeUsedAsForwardReference );

        theModelBase.theCluster.addObject( ret );

        for( MeshTypeIdentifier current : synonyms ) {
            theModelBase.theSynonymDictionary.addToTable( current, identifier );
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName, // full name
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceMultiplicity,
            MultiplicityValue         destinationMultiplicity,
            EntityType                source,
            EntityType                destination,
            RoleType []               sourceSuperRoleTypes,
            RoleType []               destinationSuperRoleTypes,
            BooleanValue              isAbstract )
    {
        if( identifier == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType cannot have null Identifier" );
        }
        if( theName == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null Name" );
        }
        if( theUserNames == null || theUserNames.getDefault() == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have empty UserNames" );
        }
        if( theSubjectArea == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null SubjectArea" );
        }
        if( sourceMultiplicity == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null SourceMultiplicity" );
        }
        if( destinationMultiplicity == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null DestinationMultiplicity" );
        }
        if( sourceSuperRoleTypes == null ) {
            sourceSuperRoleTypes = new RoleType[0];
        }
        if( destinationSuperRoleTypes == null ) {
            destinationSuperRoleTypes = new RoleType[0];
        }
        if( sourceSuperRoleTypes.length != destinationSuperRoleTypes.length ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have source super-RoleTypes and destination super-RoleTypes of different lengths" );
        }

        // they need to override the same relationship types
        for( int i=0 ; i<sourceSuperRoleTypes.length ; ++i ) {
            if( sourceSuperRoleTypes[i].getRelationshipType() != destinationSuperRoleTypes[i].getRelationshipType() ) {
                throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot override RoleTypes from different RelationshipTypes simultaneously" );
            }
        }
        // cannot widen the multiplicity
        for( int i=0 ; i<sourceSuperRoleTypes.length ; ++i ) {
            if( !sourceMultiplicity.isSubrangeOf( sourceSuperRoleTypes[i].getMultiplicity() )) {
                throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot override source RoleType with Identifier " + sourceSuperRoleTypes[i].getIdentifier() + " with a narrower multiplicity" );
            }
        }
        for( int i=0 ; i<destinationSuperRoleTypes.length ; ++i ) {
            if( !destinationMultiplicity.isSubrangeOf( destinationSuperRoleTypes[i].getMultiplicity() )) {
                throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot override destination RoleType with Identifier " + destinationSuperRoleTypes[i].getIdentifier() + " with a narrower multiplicity" );
            }
        }

        if( isAbstract == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null IsAbstract" );
        }

        MeshType already = theModelBase.findLoadedMeshTypeOrNull( identifier );
        if( already != null ) {
            throw new MeshTypeIdentifierNotUniqueException( already );
        }

        MEntityType  realSource                    = (MEntityType) source;
        MEntityType  realDestination               = (MEntityType) destination;
        MSubjectArea realSubjectArea               = (MSubjectArea) theSubjectArea;
        MRoleType [] realSourceSuperRoleTypes      = copyIntoNewArray( sourceSuperRoleTypes,      MRoleType.class );
        MRoleType [] realDestinationSuperRoleTypes = copyIntoNewArray( destinationSuperRoleTypes, MRoleType.class );

        MRelationshipType ret = new MRelationshipType(
                (MMeshTypeIdentifier) identifier,
                theName,
                realSource,
                realDestination,
                sourceMultiplicity,
                destinationMultiplicity );

        ret.setUserVisibleNameMap( theUserNames );
        ret.setUserVisibleDescriptionMap( theUserDescriptions );

        realSubjectArea.addCollectableMeshType( ret );
        ret.setSubjectArea( realSubjectArea );

        MRoleType sourceRole      = ret.getSource();
        MRoleType destinationRole = ret.getDestination();

        sourceRole.setSubjectArea( realSubjectArea );
        destinationRole.setSubjectArea( realSubjectArea );

        realSubjectArea.addCollectableMeshType( sourceRole );
        realSubjectArea.addCollectableMeshType( destinationRole );

        // we have to add it both in the roles and in the relationship
        sourceRole.setDirectSupertypes( realSourceSuperRoleTypes );
        destinationRole.setDirectSupertypes( realDestinationSuperRoleTypes );

        ArrayList<MRelationshipType> supertypes = new ArrayList<>(
                sourceSuperRoleTypes.length + destinationSuperRoleTypes.length );

        for( int i=0 ; i<sourceSuperRoleTypes.length ; ++i ) {
            if( sourceSuperRoleTypes[i] == null ) {
                throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null source super-RoleType" );
            }
            MRoleType sourceSuperRoleType      = (MRoleType) sourceSuperRoleTypes[i];
            MRoleType destinationSuperRoleType = (MRoleType) destinationSuperRoleTypes[i];

            sourceSuperRoleType.addDirectSubtype( sourceRole );
            destinationSuperRoleType.addDirectSubtype( destinationRole );
        }

        ret.setIsAbstract( isAbstract );

        theModelBase.theCluster.addObject( ret );
        theModelBase.theCluster.addObject( sourceRole );
        theModelBase.theCluster.addObject( destinationRole );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName, // full name
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceMultiplicity,
            MultiplicityValue         destinationMultiplicity,
            EntityType                source,
            EntityType                destination,
            RoleType                  sourceSuperRoleType,
            RoleType                  destinationSuperRoleType,
            BooleanValue              isAbstract )
    {
        return createRelationshipType(
                identifier,
                theName,
                theUserNames,
                theUserDescriptions,
                theSubjectArea,
                sourceMultiplicity,
                destinationMultiplicity,
                source,
                destination,
                sourceSuperRoleType      != null ? new RoleType[] { sourceSuperRoleType      } : new RoleType[0],
                destinationSuperRoleType != null ? new RoleType[] { destinationSuperRoleType } : new RoleType[0],
                isAbstract );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceDestMultiplicity,
            EntityType                sourceDest,
            RoleType                  sourceDestSuperRoleType,
            BooleanValue              isAbstract )
     {
         return createRelationshipType(
                 identifier,
                 theName,
                 theUserNames,
                 theUserDescriptions,
                 theSubjectArea,
                 sourceDestMultiplicity,
                 sourceDest,
                 new RoleType[] { sourceDestSuperRoleType },
                 isAbstract );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName, // full name
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceDestMultiplicity,
            EntityType                sourceDest,
            RoleType []               sourceDestSuperRoleTypes,
            BooleanValue              isAbstract )
    {
        if( identifier == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType cannot have null Identifier" );
        }
        if( theName == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null Name" );
        }
        if( theUserNames == null || theUserNames.getDefault() == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have empty UserNames" );
        }
        if( theSubjectArea == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null SubjectArea" );
        }
        if( sourceDestMultiplicity == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null SourceAndDestinationMultiplicity" );
        }

        // cannot widen the multiplicity
        for( int i=0 ; i<sourceDestSuperRoleTypes.length ; ++i ) {
            if( !sourceDestMultiplicity.isSubrangeOf( sourceDestSuperRoleTypes[i].getMultiplicity() )) {
                throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot override sourcedest RoleType with Identifier " + sourceDestSuperRoleTypes[i].getIdentifier() + " with a narrower multiplicity" );
            }
        }
        if( isAbstract == null ) {
            throw new IllegalArgumentException( "To-be-created RelationshipType with Identifier " + identifier + " cannot have null IsAbstract" );
        }

        MeshType already = theModelBase.findLoadedMeshTypeOrNull( identifier );
        if( already != null ) {
            throw new MeshTypeIdentifierNotUniqueException( already );
        }

        MEntityType  realSourceAndDestination = (MEntityType) sourceDest;
        MRoleType [] realSuperRoles           = copyIntoNewArray( sourceDestSuperRoleTypes, MRoleType.class );
        MSubjectArea realSubjectArea          = (MSubjectArea) theSubjectArea;

        MRelationshipType ret = new MRelationshipType(
                (MMeshTypeIdentifier) identifier,
                theName,
                realSourceAndDestination,
                sourceDestMultiplicity );

        ret.setUserVisibleNameMap( theUserNames );
        ret.setUserVisibleDescriptionMap( theUserDescriptions );
        ret.getSource().setDirectSupertypes( realSuperRoles );

        realSubjectArea.addCollectableMeshType( ret );
        ret.setSubjectArea( realSubjectArea );

        ret.setIsAbstract( isAbstract );

        realSubjectArea.addCollectableMeshType( ret.getSource() );
        ret.getSource().setSubjectArea( realSubjectArea );

        theModelBase.theCluster.addObject( ret );
        theModelBase.theCluster.addObject( ret.getSource() );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType createPropertyType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties    theParent,
            SubjectArea               theSubjectArea,
            DataType                  theDataType,
            PropertyValue             theDefaultValue,
            BooleanValue              isOptional,
            BooleanValue              isReadOnly,
            FloatValue                theSequenceNumber )
    {
        if( identifier == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType cannot have null Identifier" );
        }
        if( theName == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have null Name" );
        }
        if( theUserNames == null || theUserNames.getDefault() == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have empty UserNames" );
        }
        if( theParent == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have null parent" );
        }
        if( theSubjectArea == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have null SubjectArea" );
        }
        if( theDataType == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have null DataType" );
        }
        if( isOptional == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have null IsOptional" );
        }
        if( isReadOnly == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have null IsReadOnly" );
        }
        if( theSequenceNumber == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + identifier + " cannot have null SequenceNumber" );
        }

        MeshType already = theModelBase.findLoadedMeshTypeOrNull( identifier );
        if( already != null ) {
            throw new MeshTypeIdentifierNotUniqueException( already );
        }

        if( theParent.findPropertyTypeByName( theName.value() ) != null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier has a PropertyType with Name " + theName + " already" );
        }
        MMeshTypeWithProperties realParent      = (MMeshTypeWithProperties) theParent;
        MSubjectArea            realSubjectArea = (MSubjectArea) theSubjectArea;

        MPropertyType ret = new MPropertyType( (MMeshTypeIdentifier) identifier );

        ret.setName( theName );
        ret.setUserVisibleNameMap( theUserNames );
        ret.setUserVisibleDescriptionMap( theUserDescriptions );
        ret.setDataType( theDataType );

        // FIXME: we should check whether default value and data type are compatible
        if( theDefaultValue != null ) {
            ret.setDefaultValue( theDefaultValue );
        }
        ret.setIsOptional( isOptional );

        ret.setIsReadOnly( isReadOnly );
        ret.setSequenceNumber( theSequenceNumber );

        realParent.addPropertyType( ret );

        ret.setMeshTypeWithProperties( realParent );
        realSubjectArea.addCollectableMeshType( ret );
        ret.setSubjectArea( realSubjectArea );

        theModelBase.theCluster.addObject( ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType createOverridingPropertyType(
            PropertyType []           toOverride,
            MeshTypeIdentifier        overriddenIdentifier,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties    theParent,
            SubjectArea               theSubjectArea,
            DataType                  theDataType,
            PropertyValue             theDefaultValue,
            BooleanValue              isOptional,
            BooleanValue              isReadOnly )
        throws
            InheritanceConflictException
    {
        if( overriddenIdentifier == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType cannot have null overridden Identifier" );
        }
        if( theParent == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + overriddenIdentifier + " cannot have null parent" );
        }
        if( theSubjectArea == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + overriddenIdentifier + " cannot have null SubjectArea" );
        }
        if( theDataType == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + overriddenIdentifier + " cannot have null DataType" );
        }
        if( isOptional == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + overriddenIdentifier + " cannot have null IsOptional" );
        }
        if( isReadOnly == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyType with Identifier " + overriddenIdentifier + " cannot have null IsReadOnly" );
        }

        MeshType already = theModelBase.findLoadedMeshTypeOrNull( overriddenIdentifier );
        if( already != null ) {
            throw new MeshTypeIdentifierNotUniqueException( already );
        }

        if( toOverride == null || toOverride.length == 0 ) {
            throw new IllegalArgumentException( "Cannot override empty PropertyType list" );
        }
        MMeshTypeWithProperties realParent      = (MMeshTypeWithProperties) theParent;
        MSubjectArea          realSubjectArea = (MSubjectArea) theSubjectArea;
        MPropertyType []      realOverride    = copyIntoNewArray( toOverride, MPropertyType.class );

        MPropertyType ret = new MPropertyType( (MMeshTypeIdentifier) overriddenIdentifier );

        ret.setOverride( realOverride );
        ret.setName( realOverride[0].getName() ); // use any
        ret.setUserVisibleNameMap( realOverride[0].getUserVisibleNameMap() ); // use any
        ret.setUserVisibleDescriptionMap( theUserDescriptions );

        for( int i=0 ; i<realOverride.length ; ++i ) {
            if( ! realOverride[i].getDataType().isSupersetOfOrEquals( theDataType )) {
                throw new IllegalArgumentException(
                        "Cannot override Dataype "
                        + realOverride[i].getDataType()
                        + " with a different DataType or a larger domain: "
                        + theDataType
                        + ", MeshTypeWithProperties: "
                        + realParent
                        + ", PropertyType: "
                        + realOverride[i] );
            }
            if( ! realOverride[i].getIsReadOnly().value() && isReadOnly.value() ) {
                throw new IllegalArgumentException(
                        "Cannot override PropertyType from read-write to read-only, MeshTypeWithProperties: "
                        + realParent
                        + ", PropertyType: "
                        + realOverride[i] );
            }

            if( !realOverride[i].getIsOptional().value() && isOptional.value() ) {
                throw new IllegalArgumentException(
                        "Cannot make mandatory PropertyType optional in subtype, MeshTypeWithProperties: "
                        + realParent
                        + ", PropertyType: "
                        + realOverride[i] );
            }

            if( ! realOverride[i].getIsReadOnly().value() && isReadOnly.value() ) {
                throw new IllegalArgumentException(
                        "Cannot override PropertyType from read-write to read-only, MeshTypeWithProperties: "
                        + realParent
                        + ", PropertyType: "
                        + realOverride[i] );
            }

            int distance = theParent.getDistanceToSupertype( realOverride[i].getMeshTypeWithProperties() );
            if( distance <= 0 ) {
                throw new IllegalArgumentException(
                        "Cannot override PropertyType "
                        + realOverride[i]
                        + " of non-supertype in "
                        + realParent
                        + ", distance is "
                        + distance );
            }
        }

        ret.setDataType( theDataType );

        // FIXME: we should check whether default value and data type are compatible
        if( theDefaultValue != null ) {
            ret.setDefaultValue( theDefaultValue );
        }
        ret.setIsOptional( isOptional );
        if( ! isOptional.value() && theDefaultValue == null ) {
            throw new IllegalArgumentException(
                    "Default value must be given if PropertyType is mandatory: PropertyType "
                    + ret
                    + " on MeshTypeWithProperties "
                    + realParent );
        }

        ret.setIsReadOnly( isReadOnly );
        ret.setSequenceNumber( realOverride[0].getSequenceNumber() ); // use any, all the same

        realParent.addOverridingPropertyType( ret );

        ret.setMeshTypeWithProperties( realParent );
        realSubjectArea.addCollectableMeshType( ret );
        ret.setSubjectArea( realSubjectArea );

        // This overridden PropertyType gets added to the cluster under the name of its
        // "parent" AMO -- not the name of the AMO of the overridden MA.
        // Not sure that this meets all requirements under all circumstances?!?
        //
        // It has to be in the cluster, otherwise we can't find it, and so we would get the properties
        // of the overridden PropertyTypes if we were looking for it.

        theModelBase.theCluster.addObject( ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyTypeGroup createPropertyTypeGroup(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties    theParent,
            SubjectArea               theSubjectArea,
            PropertyType []           theMembers,
            FloatValue                theSequenceNumber )
    {
        if( identifier == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyTypeGroup cannot have null Identifier" );
        }
        if( theName == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyTypeGroup with Identifier " + identifier + " cannot have null Name" );
        }
        if( theUserNames == null || theUserNames.getDefault() == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyTypeGroup with Identifier " + identifier + " cannot have empty UserNames" );
        }
        if( theParent == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyTypeGroup with Identifier " + identifier + " cannot have null parent" );
        }
        if( theMembers == null || theMembers.length < 2 ) {
            throw new IllegalArgumentException( "To-be-created PropertyTypeGroup with Identifier " + identifier + " cannot have less than 2 Members" );
        }
        if( theSequenceNumber == null ) {
            throw new IllegalArgumentException( "To-be-created PropertyTypeGroup with Identifier " + identifier + " cannot have null SequenceNumber" );
        }

        MeshType already = theModelBase.findLoadedMeshTypeOrNull( identifier );
        if( already != null ) {
            throw new MeshTypeIdentifierNotUniqueException( already );
        }

        MMeshTypeWithProperties realParent      = (MMeshTypeWithProperties) theParent;
        MSubjectArea          realSubjectArea = (MSubjectArea) theSubjectArea;

        MPropertyTypeGroup ret = new MPropertyTypeGroup( (MMeshTypeIdentifier) identifier );

        ret.setName( theName );
        ret.setUserVisibleNameMap( theUserNames );
        ret.setUserVisibleDescriptionMap( theUserDescriptions );

        realParent.addPropertyTypeGroup( ret );

        ret.setMeshTypeWithProperties( realParent );
        ret.setContainedPropertyTypes( theMembers );
        realSubjectArea.addCollectableMeshType( ret );
        ret.setSequenceNumber( theSequenceNumber );

        ret.setSubjectArea( realSubjectArea );

        theModelBase.theCluster.addObject( ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea createSubjectArea(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea []            theSubjectAreaDependencies,
            ClassLoader               theClassLoader )
    {
        if( identifier == null ) {
            throw new IllegalArgumentException( "To-be-created SubjectArea cannot have null Identifier" );
        }
        if( theName == null ) {
            throw new IllegalArgumentException( "To-be-created SubjectArea with Identifier " + identifier + " cannot have null Name" );
        }
//        if( theVersion == null ) {
//            throw new IllegalArgumentException( "To-be-created SubjectArea with Identifier " + identifier + " cannot have null Version" );
//        }
        if( theUserNames == null || theUserNames.getDefault() == null ) {
            throw new IllegalArgumentException( "To-be-created SubjectArea with Identifier " + identifier + " cannot have empty UserNames" );
        }
        if( theSubjectAreaDependencies == null ) {
            theSubjectAreaDependencies = new SubjectArea[0];
        }

        MeshType already = theModelBase.findLoadedMeshTypeOrNull( identifier );
        if( already != null ) {
            throw new MeshTypeIdentifierNotUniqueException( already );
        }

        MSubjectArea ret = new MSubjectArea( (MMeshTypeIdentifier) identifier );

        ret.setName( theName );
        ret.setUserVisibleNameMap( theUserNames );
        ret.setUserVisibleDescriptionMap( theUserDescriptions );
        ret.setSubjectAreaDependencies( theSubjectAreaDependencies );
        ret.setClassLoader( theClassLoader );

        theModelBase.theCluster.addObject( ret );

        return ret;
    }

    /**
     * Helper method to copy into an array.
     *
     * @param oldArray the old array
     * @param arrayComponentType the type of the new array
     * @return the new array with the old content
     * @param <T> type parameter
     */
    @SuppressWarnings(value={"unchecked"})
    protected <T> T [] copyIntoNewArray(
            Object [] oldArray,
            Class<T>  arrayComponentType )
    {
        T [] ret = ArrayHelper.createArray( arrayComponentType, oldArray.length );
        for( int i=0 ; i<oldArray.length ; ++i ) {
            ret[i] = (T) oldArray[i];
        }
        return ret;
    }

    /**
     * Helper method to copy into an array.
     *
     * @param oldCollection the old Collection
     * @param arrayComponentType the type of the new array
     * @return the new array with the old content
     * @param <T> type parameter
     */
    @SuppressWarnings(value={"unchecked"})
    protected <T> T [] copyIntoNewArray(
            Collection<?> oldCollection,
            Class<T>      arrayComponentType )
    {
        T [] ret = ArrayHelper.createArray( arrayComponentType, oldCollection.size() );
        Iterator<?> iter = oldCollection.iterator();
        int         max  = oldCollection.size();
        for( int i=0 ; i<max ; ++i ) {
            ret[i] = (T) iter.next();
        }
        return ret;
    }

    /**
      * The MModelBase that this belongs to.
      */
    protected MModelBase theModelBase;
}
