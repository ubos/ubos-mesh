//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.logging.Dumper;
import net.ubos.model.primitives.MeshTypeWithProperties;

/**
 * This Exception indicates that a PropertyType could not be found.
 */
public class PropertyTypeNotFoundException
        extends
            MeshTypeNotFoundException
{
    /**
     * Constructor.
     *
     * @param amt the MeshTypeWithProperties within which a PropertyType could not be found
     * @param propertyTypeName the name of the PropertyType that could not be found
     */
    public PropertyTypeNotFoundException(
            MeshTypeWithProperties amt,
            String                 propertyTypeName )
    {
        super();

        if( amt != null ) {
            theSubjectArea       = amt.getSubjectArea();
            theSubjectAreaName   = amt.getSubjectArea().getIdentifier();
            theAmtName           = amt.getIdentifier();
        } else {
            theSubjectArea       = null;
            theSubjectAreaName   = null;
            theAmtName           = null;
        }
        thePropertyTypeName  = propertyTypeName;
    }

    /**
     * Constructor.
     *
     * @param amt the MeshTypeWithProperties within which a PropertyType could not be found
     * @param propertyTypeName the name of the PropertyType that could not be found
     * @param cause the Throwable that caused this Exception
     */
    public PropertyTypeNotFoundException(
            MeshTypeWithProperties amt,
            String                 propertyTypeName,
            Throwable              cause )
    {
        super( cause );

        if( amt != null ) {
            theSubjectArea       = amt.getSubjectArea();
            theSubjectAreaName   = amt.getSubjectArea().getIdentifier();
            theAmtName           = amt.getIdentifier();
        } else {
            theSubjectArea       = null;
            theSubjectAreaName   = null;
            theAmtName           = null;
        }
        thePropertyTypeName  = propertyTypeName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String [] {
                    "SubjectAreaName",
                    "MeshTypeWithProperties",
                    "PropertyType"
                },
                new Object[] {
                    theSubjectAreaName,
                    theAmtName,
                    thePropertyTypeName
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{ theSubjectAreaName, theAmtName, thePropertyTypeName };
    }

    /**
     * The SubjectArea in which the MeshTypeWithProperties resides in which the PropertyType could not be found.
     */
    protected volatile SubjectArea theSubjectArea;
    
    /**
     * The Identifier of the SubjectArea in which the MeshTypeWithProperties resides in which the
     * PropertyType could not be found.
     */
    protected MeshTypeIdentifier theSubjectAreaName;
    
    /**
     * The MeshTypeWithProperties in which the PropertyType could not be found.
     */
    protected volatile MeshTypeWithProperties theAmt;
    
    /**
     * The Identifier of the MeshTypeWithProperties in which the PropertyType could not be found. This
     * is stored as MeshTypeIdentifier rather than MeshTypeWithProperties in order to support serialization.
     */
    protected MeshTypeIdentifier theAmtName;

    /**
     * The name of the PropertyType that could not be found.
     */
    protected String thePropertyTypeName;
}
