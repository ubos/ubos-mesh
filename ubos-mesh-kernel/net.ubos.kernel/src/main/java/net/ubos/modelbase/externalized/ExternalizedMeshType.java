//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.HashMap;
import java.util.Map;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;

/**
 * Convenience superclass for most of the other ExternalizedXXX. Roughly resembles MeshType.
 */
public class ExternalizedMeshType
{
    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setIdentifier(
            String newValue )
    {
        if( theIdentifier != null ) {
            throw new IllegalStateException( "Have identifier already: " + theIdentifier + " vs " + newValue );
        }
        theIdentifier = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public String getIdentifier()
    {
        return theIdentifier;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setName(
            StringValue newValue )
    {
        if( theName != null ) {
            throw new IllegalStateException( "Have name already: " + theName + " vs " + newValue );
        }
        theName = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public StringValue getName()
    {
        return theName;
    }

    /**
     * Add to property.
     *
     * @param key the locale
     * @param newValue the new value in this locale
     */
    public void addUserName(
            String        key,
            PropertyValue newValue )
    {
        if( userNames == null ) {
            userNames = new HashMap<>();
        }
        userNames.put( key, newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public Map<String,PropertyValue> getUserNames()
    {
        return userNames;
    }

    /**
     * Add to property.
     *
     * @param key the locale
     * @param newValue the new value in this locale
     */
    public void addUserDescription(
            String        key,
            PropertyValue newValue )
    {
        if( userDescriptions == null ) {
            userDescriptions = new HashMap<>();
        }
        userDescriptions.put( key, newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public Map<String,PropertyValue> getUserDescriptions()
    {
        return userDescriptions;
    }

    /**
     * Convert to String, for user error messages.
     *
     * @return String form of this object
     */
    @Override
    public String toString()
    {
        final String PREFIX = "net.ubos.modelbase.externalized.Externalized";

        String name = getClass().getName();
        if( name.startsWith( PREFIX )) {
            name = name.substring( PREFIX.length() );
        }

        return name + ": " + theIdentifier;
    }

    /**
     * Identifier, if specified.
     */
    protected String theIdentifier;

    /**
     * Name.
     */
    protected StringValue theName;

    /**
     * The set of localized user names, keyed by locale, if any.
     */
    protected HashMap<String,PropertyValue> userNames;

    /**
     * The set of localized user descriptions, keyed by locale, if any.
     */
    protected HashMap<String,PropertyValue> userDescriptions;
}
