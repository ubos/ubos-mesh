//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import java.text.ParseException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.externalized.ExternalizedEntityType;
import net.ubos.modelbase.externalized.ExternalizedPropertyType;
import net.ubos.modelbase.externalized.ExternalizedPropertyTypeGroup;
import net.ubos.modelbase.externalized.ExternalizedRelationshipType;
import net.ubos.modelbase.externalized.ExternalizedSubjectArea;
import net.ubos.modelbase.externalized.ExternalizedSubjectAreaDependency;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import org.xml.sax.Locator;

/**
 * Extends the default MeshTypeSerializer with the default rules for
 * incompletely or only partially specified identifiers in model.xml files.
 * @author jernst
 */
public class ModelLoaderIdConstructor
    extends
        DefaultMMeshTypeIdentifierBothSerializer
{
    /**
     * Singleton instance.
     */
    public static final ModelLoaderIdConstructor SINGLETON = new ModelLoaderIdConstructor();

    /**
     * Given what is known about a SubjectArea, create a suitable MeshTypeIdentifier for it.
     *
     * @param sa the externalized Subject Area
     * @param locator the location in the parsed file
     * @return the constructed MeshTypeIdentifier
     * @throws ParseException the provided identifier string could not be parsed
     * @throws MissingInfoException.SubjectAreaIdentifier no identifier string was provided
     */
    public MeshTypeIdentifier forSubjectArea(
            ExternalizedSubjectArea sa,
            Locator                 locator )
        throws
            ParseException,
            MissingInfoException.SubjectAreaIdentifier
    {
        if( sa.getIdentifier() != null ) {
            return fromExternalForm( sa.getIdentifier());
        } else {
            throw new MissingInfoException.SubjectAreaIdentifier( locator );
        }
    }

    /**
     * Given what's known about a Subject Area dependency, create a suitable MeshTypeIdentifier for the
     * Subject Area referenced by the dependency.
     *
     * @param sadep the externalized Subject Area dependency
     * @return the constructed MeshTypeIdentifier
     * @throws ParseException the provided identifier string could not be parsed
     * @throws MissingInfoException.SubjectAreaDependencyIdentifier no identifier string was provided
     */
    public MeshTypeIdentifier forSubjectAreaDependency(
            ExternalizedSubjectAreaDependency sadep,
            Locator                           locator )
        throws
            ParseException,
            MissingInfoException.SubjectAreaDependencyIdentifier
    {
        if( sadep.getName() != null ) {
            return fromExternalForm( sadep.getName().value() );
        } else {
            throw new MissingInfoException.SubjectAreaDependencyIdentifier( locator );
        }
    }

    /**
     * Create a MeshTypeIdentifier for a supertype, in the context of the provided SubjectArea.
     *
     * @param sa the SubjectArea context
     * @param s the identifier as expressed in the model file
     * @return the constructed MeshTypeIdentifier
     * @throws ParseException the provided identifier string could not be parsed
     */
    public MeshTypeIdentifier forSupertypeReference(
            SubjectArea sa,
            String      s )
        throws
            ParseException
    {
        return forReference( sa, s );
    }

    /**
     * Create a MeshTypeIdentifier for a synonym, in the context of the provided SubjectArea.
     *
     * @param sa the SubjectArea context
     * @param s the identifier as expressed in the model file
     * @return the constructed MeshTypeIdentifier
     * @throws ParseException the provided identifier string could not be parsed
     */
    public MeshTypeIdentifier forSynonym(
            SubjectArea sa,
            String      s )
        throws
            ParseException
    {
        return forReference( sa, s );
    }

    /**
     * Given what's known about an EntityType, in the context of a SubjectArea, create a
     * suitable MeshTypeIdentifier.
     *
     * @param sa the SubjectArea context
     * @param et the externalized EntitytType
     * @return the constructed MeshTypeIdentifier
     * @throws ParseException the provided identifier string could not be parsed
     */
    public MeshTypeIdentifier forEntityType(
            SubjectArea            sa,
            ExternalizedEntityType et )
        throws
            ParseException
    {
        String s = et.getIdentifier();
        if( s == null ) {
            s = et.getName().value();
        }
        return fromExternalForm( sa.getIdentifier(), s );
    }

   /**
     * Create a MeshTypeIdentifier for a source or destination EntityType, in the context
     * of the provided SubjectArea.
     *
     * @param sa the SubjectArea context
     * @param s the identifier as expressed in the model file
     * @return the constructed MeshTypeIdentifier
     */
    public MeshTypeIdentifier forSourceDest(
            SubjectArea sa,
            String      s )
        throws
            ParseException
    {
        return forReference( sa, s );
    }

    /**
     * Given what's known about a RelationshipType, and its source and destination EntityTypes,
     * in the context of a SubjectArea, create a suitable MeshTypeIdentifier.
     *
     * @param sa the SubjectArea context
     * @param rt the externalized RelationshipType
     * @return the constructed MeshTypeIdentifier
     * @throws ParseException the provided identifier string could not be parsed
     */
    public MeshTypeIdentifier forRelationshipType(
            SubjectArea                  sa,
            EntityType                   source,
            EntityType                   destination,
            ExternalizedRelationshipType rt )
        throws
            ParseException
    {
        String s = rt.getIdentifier();
        if( s == null ) {
            s = rt.getName().value();
        }
        return fromExternalForm( sa.getIdentifier(), s );
    }

    /**
     * Given what's known about a PropertyType, in the context of its parent MeshTypeWithProperties,
     * create a suitable MeshTypeIdentifier.
     *
     * @param mtwp the MeshTypeWithProperties context
     * @param pt the externalized PropertyType
     * @return the constructed MeshTypeIdentifier
     */
    public MeshTypeIdentifier forPropertyType(
            MeshTypeWithProperties   mtwp,
            ExternalizedPropertyType pt )
        throws
            ParseException
    {
        String s = pt.getIdentifier();
        if( s == null ) {
            s = pt.getName().value();
        }
        return fromExternalNamePropertyContext( mtwp, s );
    }

    /**
     * Create a MeshTypeIdentifier for a reference to an overridden PropertyType, in the
     * context of its parent MeshTypeWithProperties.
     *
     * @param mtwp the MeshTypeWithProperties context
     * @param s the identifier as expressed in the model file
     * @return the constructed MeshTypeIdentifier
     */
    public MeshTypeIdentifier forOverriddenPropertyTypeReference(
            MeshTypeWithProperties mtwp,
            String                 s )
        throws
            ParseException
    {
        return fromExternalForm( s ); // need absolute path, cannot guess
    }

    /**
     * Given what's known about a PropertyTypeGroup, in the context of its parent MeshTypeWithProperties,
     * create a suitable MeshTypeIdentifier.
     *
     * @param mtwp the MeshTypeWithProperties context
     * @param ptg the externalized PropertyTypeGroup
     * @return the constructed MeshTypeIdentifier
     */
    public MeshTypeIdentifier forPropertyTypeGroup(
            MeshTypeWithProperties        mtwp,
            ExternalizedPropertyTypeGroup ptg )
        throws
            ParseException
    {
        String s = ptg.getIdentifier();
        if( s == null ) {
            s = ptg.getName().value();
        }
        return fromExternalNamePropertyContext( mtwp, s );
    }

    /**
     * Create a MeshTypeIdentifier for a reference to a PropertyType to be included in a
     * PropertyTypeGroup, in the context of its parent MeshTypeWithProperties.
     *
     * @param mtwp the MeshTypeWithProperties context
     * @param s the identifier as expressed in the model file
     * @return the constructed MeshTypeIdentifier
     */
    public MeshTypeIdentifier forGroupedPropertyTypeReference(
            MeshTypeWithProperties mtwp,
            String                 s )
        throws
            ParseException
    {
        return fromExternalNamePropertyContext( mtwp, s );
    }

    /**
     * Code used for references.
     *
     * @param sa the SubjectArea context
     * @param s the identifier as string
     * @return the constructed MeshTypeIdentifier
     * @throws ParseException the provided identifier string could not be parsed
     */
    protected MeshTypeIdentifier forReference(
            SubjectArea sa ,
            String      s )
        throws
            ParseException
    {
        if( s == null ) {
            return null; // reference to "Any"
        } else {
            return fromExternalForm( sa.getIdentifier(), s );
        }
    }

    /**
     * Construct a MeshTypeIdentifier from a property name, and its
     * enclosing MeshTypeWithProperties object.
     *
     * @throws ParseException the provided identifier string could not be parsed
     */
    protected MeshTypeIdentifier fromExternalNamePropertyContext(
            MeshTypeWithProperties mtwp,
            String                 s )
        throws
            ParseException
    {
        if( s.indexOf( PTSEP ) >= 0 ) {
            return fromExternalForm( mtwp.getSubjectArea().getIdentifier(), s );

        } else {
            String saPart    = mtwp.getIdentifier().getSubjectAreaPart();
            String localPart = mtwp.getIdentifier().getLocalPart();

            return fromExternalForm( saPart + SASEP + localPart + PTSEP + s );
        }
    }

//
//    /**
//     * Determine a MeshTypeIdentifier appropriate for a CollectableMeshType.
//     *
//     * @param sa the SubjectArea in which this CollectableMeshType lives
//     * @param cmt the CollectableMeshType
//     * @return a suitable Identifier
//     */
//    public MeshTypeIdentifier constructIdentifierFor(
//            SubjectArea                     sa,
//            ExternalizedCollectableMeshType cmt )
//        throws
//            ParseException
//    {
//        if( cmt.getIdentifier() != null ) {
//            return theIdDeserializer.fromExternalForm(
//                    sa.getIdentifier(),
//                    cmt.getIdentifier());
//        } else {
//            return theIdDeserializer.fromExternalForm(
//                    sa.getIdentifier(),
//                    cmt.getName().value());
//        }
//    }
//
//    /**
//     * Determine a MeshTypeIdentifier appropriate for a PropertyType.
//     *
//     * @param amo the AttributableMeshObject in which this PropertyType lives
//     * @param pt the PropertyType
//     * @return a suitable Identifier
//     */
//    public MeshTypeIdentifier constructIdentifierFor(
//            MeshTypeWithProperties   amo,
//            ExternalizedPropertyType pt )
//        throws
//            ParseException
//    {
//        if( pt.getIdentifier() != null ) {
//            return theIdDeserializer.fromExternalForm(
//                    amo.getIdentifier(),
//                    pt.getIdentifier() );
//        } else {
//            return theIdDeserializer.fromExternalForm(
//                    amo.getIdentifier(),
//                    pt.getName().value() );
//        }
//    }
//
//    /**
//     * Determine a MeshTypeIdentifier appropriate for a PropertyTypeGroup.
//     *
//     * @param amo the AttributableMeshObject in which this PropertyTypeGroup lives
//     * @param pt the PropertyTypeGroup
//     * @return a suitable Identifier
//     */
//    public MeshTypeIdentifier constructIdentifierFor(
//            MeshTypeWithProperties          amo,
//            ExternalizedPropertyTypeGroup pt )
//        throws
//            ParseException
//    {
//        if( pt.getIdentifier() != null ) {
//            return theIdDeserializer.fromExternalForm(
//                    amo.getIdentifier(),
//                    pt.getIdentifier() );
//        } else {
//            return theIdDeserializer.fromExternalForm(
//                    amo.getIdentifier(),
//                    pt.getName().value() );
//        }
//    }
//
//    /**
//     * Complete a MeshTypeIdentifier that might be relative to a SubjectArea.
//     *
//     * @param saIdentifier Identified of the the SubjectArea relative to which this MeshTypeWithProperties is named
//     * @param identifier the identifier
//     * @return the complete identifier
//     */
//    public MeshTypeIdentifier completeIdentifier(
//            MeshTypeIdentifier saIdentifier,
//            String             identifier )
//        throws
//            ParseException
//    {
//        if( identifier != null ) {
//            return theIdDeserializer.fromExternalForm( saIdentifier, identifier );
//        } else {
//            return null;
//        }
//    }
}
