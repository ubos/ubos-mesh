//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * Thrown when during parsing of a model XML file, an XML tag was found
 * that is not allowed as a child of the enclosing element.
 */
public class InvalidChildException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param parentObject the parent object
     * @param tagName the XML tag name of the child that was not allowed
     * @param locator knows the location of the problem
     */
    public InvalidChildException(
            Object parentObject,
            String tagName,
            Locator locator )
    {
        super( locator );

        theParentObject = parentObject;
        theTagName      = tagName;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theParentObject,
            theTagName
        };
    }

    /**
     * The parent object.
     */
    protected final Object theParentObject;
    
    /**
     * The XML tag name of the child that was not allowed.
     */
    protected final String theTagName;
}

