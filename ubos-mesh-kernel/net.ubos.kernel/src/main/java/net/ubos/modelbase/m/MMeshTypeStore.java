//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.m;

import java.util.HashMap;
import java.util.Iterator;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.Pair;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.modelbase.MeshTypeCreatedEvent;
import net.ubos.modelbase.MeshTypeLifecycleEvent;
import net.ubos.modelbase.MeshTypeLifecycleEventListener;
import net.ubos.util.listenerset.FlexibleListenerSet;

/**
  * This stores a working model, in memory. A working model is the set of SubjectAreas currently needed
  * by some application.
  */
public class MMeshTypeStore
{
    private static final Log log = Log.getLogInstance( MMeshTypeStore.class ); // our own, private logger

    /**
      * Construct one.
      *
      * @param base the MModelBase to which we belong
      */
    public MMeshTypeStore(
            MModelBase base )
    {
        this.theModelBase = base;
    }

    /**
     * Obtain an iterator that iterates over all known MeshTypes.
     *
     * @return the iterator
     */
    public CursorIterator<MeshType> iterator()
    {
        return MapCursorIterator.createForValues( allMeshTypes );
    }

    /**
     * Get all currently loaded SubjectAreas.
     *
     * @return the Set
     */
    public SubjectArea [] getLoadedSubjectAreas()
    {
        SubjectArea [] ret = new SubjectArea[ theSubjectAreas.size() ];
        theSubjectAreas.values().toArray( ret );

        return ret;
    }

    /**
     * Let MMeshTypeLifecycleManager add a SubjectArea.
     *
     * @param theObject the SubjectArea to be added
     */
    void addObject(
            SubjectArea theObject )
    {
        theSubjectAreas.put( theObject.getIdentifier(), theObject );
        allMeshTypes.put( theObject.getIdentifier(), theObject );

        notifyElementAdded( theObject );
    }

    /**
     * Let MMeshTypeLifecycleManager add a CollectableMeshType.
     *
     * @param theObject the CollectableMeshType to be added
     */
    void addObject(
            CollectableMeshType theObject )
    {
        SubjectArea sa = theObject.getSubjectArea();

        Pair<SubjectArea,String> theKey = new Pair<>(
                sa,
                theObject.getName().value() );

        theCollectableMeshTypes.put( theKey, theObject );
        allMeshTypes.put( theObject.getIdentifier(), theObject );

        notifyElementAdded( theObject );
    }

    /**
     * Let MMeshTypeLifecycleManager add a PropertyType.
     *
     * @param theObject the PropertyType to be added
     */
    void addObject(
            PropertyType theObject )
    {
        MeshTypeWithProperties amo = theObject.getMeshTypeWithProperties();

        Pair<MeshTypeWithProperties,String> theKey = new Pair<>(
                amo,
                theObject.getName().value() );

        thePropertyTypes.put( theKey, theObject );
        allMeshTypes.put( theObject.getIdentifier(), theObject );

        notifyElementAdded( theObject );
    }

    /**
     * Let MMeshTypeLifecycleManager add a PropertyTypeGroup.
     *
     * @param theObject the PropertyTypeGroup to be added
     */
    void addObject(
            PropertyTypeGroup theObject )
    {
        allMeshTypes.put( theObject.getIdentifier(), theObject );
    }

    /**
     * Find a SubjectArea. No wildcards etc. are allowed.
     *
     * @param saId the identifier of the SubjectArea
     * @return the found SubjectArea, or null
     */
    SubjectArea findSubjectArea(
            MeshTypeIdentifier saId )
    {
        return theSubjectAreas.get( saId );
    }

    /**
     * Find a CollectableMeshType by name within a SubjectArea.
     *
     * @param theSubjectArea the SubjectArea within which the search shall be performed
     * @param theCollectableMeshTypeName the programmatic name of the CollectableMeshType
     * @return the found CollectableMeshType, or null
     */
    CollectableMeshType findCollectableMeshType(
            SubjectArea theSubjectArea,
            String      theCollectableMeshTypeName )
    {
        if( theSubjectArea == null ) {
            throw new IllegalArgumentException( "null SubjectArea" );
        }
        if( theCollectableMeshTypeName == null ) {
            throw new IllegalArgumentException( "null CollectableMeshType name" );
        }
        Pair<SubjectArea,String> theKey = new Pair<>(
                theSubjectArea,
                theCollectableMeshTypeName );

        return theCollectableMeshTypes.get( theKey );
    }

    /**
     * Find a PropertyType. No wildcards etc. are allowed.
     *
     * @param cmt the MeshTypeWithProperties within which the search shall be performed
     * @param ptName the programmatic name of the PropertyType
     * @return the found PropertyType, or null
     */
    PropertyType findPropertyType(
            MeshTypeWithProperties cmt,
            String                 ptName )
    {
        if( cmt == null ) {
            throw new IllegalArgumentException( "null MeshTypeWithProperties" );
        }
        if( ptName == null ) {
            throw new IllegalArgumentException( "null PropertyType name" );
        }
        Pair<MeshTypeWithProperties,String> theKey = new Pair<>(
                cmt,
                ptName );

        PropertyType ret = thePropertyTypes.get( theKey );
        if( ret != null ) {
            return ret;
        }

        MeshTypeWithProperties [] theSupertypes = cmt.getDirectSupertypes();
        for( int i=0 ; i<theSupertypes.length ; ++i ) {
            ret = findPropertyType( theSupertypes[i], ptName );
            if( ret != null ) {
                return ret;
            }
        }
        return null;
    }

    /**
     * Find a MeshType by its globally unique identifier in the working model, but do not attempt to
     * load.
     *
     * @param identifier the Identifier of the MeshType to be found
     * @return the found MeshType, or null
     */
    public MeshType findLoadedMeshTypeByIdentifier(
            MeshTypeIdentifier identifier )
    {
        MeshType ret = allMeshTypes.get( identifier );
        return ret;
    }

    /**
     * Remove a MeshType. This only exists for testing instrumentation. Do not invoke.
     *
     * @param toRemove the MeshType to remove
     */
    public void remove(
            MeshType toRemove )
    {
        // won't hurt if removed where it does not exist
        theSubjectAreas.remove( toRemove.getIdentifier() );
        allMeshTypes.remove( toRemove.getIdentifier() );

        if( toRemove instanceof CollectableMeshType ) {
            CollectableMeshType realToRemove = (CollectableMeshType) toRemove;

            theCollectableMeshTypes.remove( new Pair<>( realToRemove.getSubjectArea(), realToRemove.getName().value() ));
        }
        if( toRemove instanceof PropertyType ) {
            PropertyType realToRemove = (PropertyType) toRemove;

            thePropertyTypes.remove( new Pair<>( realToRemove.getMeshTypeWithProperties(), realToRemove.getName().value() ));
        }
    }

    /**
      * Internal helper method that notifies MeshTypeLifecycleEventListeners of an
      * "added" event.
      *
      * @param theObject the MeshType that was added
      */
    protected void notifyElementAdded(
            MeshType theObject )
    {
        theListeners.fireEvent( new MeshTypeCreatedEvent( theModelBase, theObject));
    }

    /**
     * Add a MeshTypeLifecycleEventListener.
     * This listener is added directly to the listener list, which prevents the
     * listener from being garbage-collected before this Object is being garbage-collected.
     *
     * @param newListener the to-be-added MeshTypeLifecycleEventListener
     * @see #addWeakMeshTypeLifecycleEventListener
     * @see #addSoftMeshTypeLifecycleEventListener
     * @see #removeMeshTypeLifecycleEventListener
     */
    public void addDirectMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener )
    {
        theListeners.addDirect( newListener );
    }

    /**
     * Add a MeshTypeLifecycleEventListener.
     * This listener is added to the listener list using a <code>java.lang.ref.SoftReference</code>,
     * which allows the listener to be garbage-collected before this Object is being garbage-collected
     * according to the semantics of Java references.
     *
     * @param newListener the to-be-added MeshTypeLifecycleEventListener
     * @see #addDirectMeshTypeLifecycleEventListener
     * @see #addSoftMeshTypeLifecycleEventListener
     * @see #removeMeshTypeLifecycleEventListener
     */
    public void addWeakMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener )
    {
        theListeners.addWeak( newListener );
    }

    /**
     * Add a MeshTypeLifecycleEventListener.
     * This listener is added to the listener list using a <code>java.lang.ref.WeakReference</code>,
     * which allows the listener to be garbage-collected before this Object is being garbage-collected
     * according to the semantics of Java references.
     *
     * @param newListener the to-be-added MeshTypeLifecycleEventListener
     * @see #addDirectMeshTypeLifecycleEventListener
     * @see #addWeakMeshTypeLifecycleEventListener
     * @see #removeMeshTypeLifecycleEventListener
     */
    public void addSoftMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener newListener )
    {
        theListeners.addSoft( newListener );
    }

    /**
     * Remove a PropertyChangeListener.
     *
     * @param oldListener the to-be-removed PropertyChangeListener
     * @see #addDirectMeshTypeLifecycleEventListener
     * @see #addWeakMeshTypeLifecycleEventListener
     * @see #addSoftMeshTypeLifecycleEventListener
     */
    public void removeMeshTypeLifecycleEventListener(
            MeshTypeLifecycleEventListener oldListener )
    {
        theListeners.remove( oldListener );
    }

    /**
     * The MModelBase that this MMeshTypeStore belongs to.
     */
    protected MModelBase theModelBase;

    /**
     * The SubjectAreas contained by this MMeshTypeStore.
     * key is identifier
     */
    protected HashMap<MeshTypeIdentifier,SubjectArea> theSubjectAreas
            = new HashMap<>( 20 ); // fudge

    /**
     * The CollectableMeshTypes contained by this MMeshTypeStore.
     * key is { subjectArea, name }
     */
    protected HashMap<Pair<SubjectArea,String>,CollectableMeshType> theCollectableMeshTypes
            = new HashMap<>();

    /**
     * The PropertyTypes contained by this MMeshTypeStore.
     * key is { amo, name }
     */
    protected HashMap<Pair<MeshTypeWithProperties,String>,PropertyType> thePropertyTypes
            = new HashMap<>();

    /**
     * An additional (and redundant) table that maps from the MeshType's
     * Identifier to the MeshType directly.
     */
    protected HashMap<MeshTypeIdentifier, MeshType> allMeshTypes
            = new HashMap<>();

    /**
      * The listeners.
      */
    protected FlexibleListenerSet<MeshTypeLifecycleEventListener,MeshTypeLifecycleEvent,Void> theListeners
            = new FlexibleListenerSet<MeshTypeLifecycleEventListener,MeshTypeLifecycleEvent,Void>() {
                    @Override
                    protected void fireEventToListener(
                            MeshTypeLifecycleEventListener listener,
                            MeshTypeLifecycleEvent         event,
                            Void                           parameter )
                    {
                        listener.objectCreated( event );
                    }
            };

    /**
      * This implements the Iterator interface for this MMeshTypeStore.
      */
    protected class ClusterIterator
            implements Iterator<MeshType>
    {
        /**
          * Construct one.
          */
        ClusterIterator()
        {
            currentIter = theSubjectAreas.values().iterator();
            goNext();
        }

        /**
         * Tests if this Iterator can return more elements.
         *
         * @return if true, there are more elements
         */
        @Override
        public boolean hasNext()
        {
            return nextElement != null;
        }

        /**
         * Returns the next element of this Iterator.
         *
         * @return the next element
         */
        @Override
        public MeshType next()
        {
            MeshType ret = nextElement;
            goNext();
            return ret;
        }

        /**
         * The remove operation is unsupported.
         *
         * @throws UnsupportedOperationException always
         */
        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }

        /**
         * Internal advance to the next element.
         */
        final protected void goNext()
        {
            while( true ) {
                if( currentIter.hasNext() ) {
                    nextElement = currentIter.next();
                    return;
                }

                switch( state ) {
                    case 0:
                        state = 1;
                        currentIter = theCollectableMeshTypes.values().iterator();
                        break;
                    case 1:
                        state = 2;
                        currentIter = thePropertyTypes.values().iterator();
                        break;
                    case 2:
                        nextElement = null;
                        return;
                }
            }
        }

        /**
         * The state that we are in. The value 0 means we're returning SubjectAreas,
         * 1 MeshTypeWithProperties, 2 PropertyTypes and 3 other CollectableMeshTypes
         */
        protected int state = 0;

        /**
         * The next element we will be returning.
         */
        protected MeshType nextElement;

        /**
         * The current Iterator that we are delegating to.
         */
        protected Iterator<? extends MeshType> currentIter;
    }
}
