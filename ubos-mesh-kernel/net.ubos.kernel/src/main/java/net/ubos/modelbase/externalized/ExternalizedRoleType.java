//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MultiplicityValue;

/**
 * This is data wanting to become a RoleType, during reading.
 */
public class ExternalizedRoleType
    extends
        ExternalizedMeshTypeWithProperties
{
    /**
     * Constructor.
     *
     * @param rel the ExternalizedRelationshipType to which this ExternalizedRoleType belongs
     * @param source if true, this is the source ExternalizedRoleType
     * @param dest if true, this is the destination ExternalizedRoleType
     */
    public ExternalizedRoleType(
            ExternalizedRelationshipType rel,
            boolean                      source,
            boolean                      dest )
    {
        theRelationship  = rel;
        theIsSource      = source;
        theIsDestination = dest;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setEntityTypeIdentifier(
            String newValue )
    {
        if( theEntityTypeIdentifier != null ) {
            throw new IllegalStateException( "Have entity type identifier already: " + theEntityTypeIdentifier + " vs " + newValue );
        }
        theEntityTypeIdentifier = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public String getEntityTypeIdentifier()
    {
        return theEntityTypeIdentifier;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setMultiplicity(
            MultiplicityValue newValue )
    {
        if( theMultiplicity != null ) {
            throw new IllegalStateException( "Have multiplicity already: " + theMultiplicity + " vs " + newValue );
        }
        theMultiplicity = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public MultiplicityValue getMultiplicity()
    {
        return theMultiplicity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        if( theIsSource ) {
            return "Source RoleType of RelationshipType: " + theRelationship.getIdentifier();
        } else if( theIsDestination ) {
            return "Destination RoleType of RelationshipType: " + theRelationship.getIdentifier();
        } else {
            return "RoleType of RelationshipType: " + theRelationship.getIdentifier();
        }
    }

    /**
     * The RelationshipType to which this RoleType belongs.
     */
    protected ExternalizedRelationshipType theRelationship;

    /**
     * True if this is the source RoleType.
     */
    protected boolean theIsSource;

    /**
     * True if this is the destination RoleType.
     */
    protected boolean theIsDestination;

    /**
     * MeshObjectIdentifier identifying the EntityType playing this RoleType.
     */
    protected String theEntityTypeIdentifier;

    /**
     * Multiplicity on this RoleType.
     */
    protected MultiplicityValue theMultiplicity;
}
