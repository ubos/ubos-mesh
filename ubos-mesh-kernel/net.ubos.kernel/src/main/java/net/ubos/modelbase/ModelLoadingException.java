//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

/**
 * Thrown if loading a Model failed. There are specific subclasses.
 */
public class ModelLoadingException
    extends
        Exception
{
    /**
     * Constructor with the underlying cause for the problem.
     * 
     * @param cause the cause
     */
    public ModelLoadingException(
            Throwable cause )
    {
        super( null, cause );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        Throwable cause = getCause();
        return cause.getLocalizedMessage();
    }
}
