//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.MeshType;

/**
  * This event indicates the creation of a MeshType in a ModelBase.
  */
public class MeshTypeCreatedEvent
        extends
            MeshTypeLifecycleEvent
{
    /**
      * Construct one.
      *
      * @param theSender the ModelBase in which the MeshType was created
      * @param theObject the MeshType that was created
      */
    public MeshTypeCreatedEvent(
            ModelBase theSender,
            MeshType  theObject )
    {
        super( theSender, theObject );
    }
}
