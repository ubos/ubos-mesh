//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import java.io.IOException;
import java.text.ParseException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.util.logging.Log;

/**
 * <p>An abstract supertype for classes that know how to load
 * a model, or part of a model.
 *
 * <p>Note: subtypes of this class need to have the same constructor as this class
 * for the Class.forName()-based instantiation to work.
 */
public abstract class ModelLoader
{
    /**
     * Constructor for subclasses only.
     *
     * @param modelBase the ModelBase into which the new model will be merged
     */
    protected ModelLoader(
            ModelBase modelBase )
    {
        theModelBase = modelBase;
    }

    /**
     * Instruct this ModelLoader to instantiate and check its model.
     *
     * @param theInstantiator the MeshTypeLifecycleManager whose methods shall be used to instantiate
     * @param now the time the current run was started
     * @return the SubjectAreas that were instantiated
     * @throws ModelLoadingException loading the model failed
     * @throws IOException thrown if reading the model failed
     */
    public final SubjectArea [] loadAndCheckModel(
            MeshTypeLifecycleManager theInstantiator,
            TimeStampValue           now )
        throws
            ModelLoadingException,
            IOException
    {
        try {
            LoaderMeshTypeLifecycleManager delegate = new LoaderMeshTypeLifecycleManager( theInstantiator );

            SubjectArea [] ret = loadModel( delegate, now );

            MeshType [] inst = delegate.getInstantiated();
            for( int i=0 ; i<inst.length ; ++i ) {
                if( inst[i] instanceof EntityType ) {
                    theModelBase.checkEntityType( (EntityType) inst[i] );
                } else if( inst[i] instanceof RelationshipType ) {
                    theModelBase.checkRelationshipType( (RelationshipType) inst[i] );
                } else if( inst[i] instanceof PropertyType ) {
                    theModelBase.checkPropertyType( (PropertyType) inst[i] );
                } else if( inst[i] instanceof PropertyTypeGroup ) {
                    theModelBase.checkPropertyTypeGroup( (PropertyTypeGroup) inst[i] );
                } else if( inst[i] instanceof SubjectArea ) {
                    theModelBase.checkSubjectArea( (SubjectArea) inst[i] );
                } else {
                    getLog().error( "unexpected " + inst[i] );
                }
            }
            return ret;

        } catch( MeshTypeNotFoundException ex ) {
            throw new ModelLoadingException( ex );

        } catch( ParseException ex ) {
            throw new ModelLoadingException( ex );
        }
    }

    /**
     * Instruct this ModelLoader to instantiate its model.
     *
     * @param theInstantiator the MeshTypeLifecycleManager whose method shall be used to instantiate
     * @param now the time the current run was started
     * @return the SubjectAreas that were instantiated
     * @throws MeshTypeNotFoundException thrown if there was an undeclared dependency in the model that could not be resolved
     * @throws ModelLoadingException loading the model failed
     * @throws ParseException parsing an identifier failed
     * @throws IOException thrown if reading the model failed
     */
    protected abstract SubjectArea [] loadModel(
            MeshTypeLifecycleManager theInstantiator,
            TimeStampValue           now )
        throws
            MeshTypeNotFoundException,
            ModelLoadingException,
            ParseException,
            IOException;

    /**
     * Obtain the correct logger of the subclass.
     *
     * @return the logger of the subclass
     */
    protected final Log getLog()
    {
        return Log.getLogInstance( getClass() );
    }

    /**
     * The ModelBase that we work on.
     */
    protected ModelBase theModelBase;
}
