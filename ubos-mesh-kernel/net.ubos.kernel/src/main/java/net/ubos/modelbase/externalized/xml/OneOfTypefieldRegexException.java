//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * Must not specify regex and typefield simultaneously.
 */
public class OneOfTypefieldRegexException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param locator knows the location of the problem
     */
    public OneOfTypefieldRegexException(
            Locator locator )
    {
        super( locator );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber()
        };
    }
}
