//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.MeshTypeIdentifier;

/**
 * This Exception indicates that a SubjectArea could not be found.
 */
public class SubjectAreaNotFoundException
        extends
            MeshTypeWithIdentifierNotFoundException
{
    /**
     * Constructor.
     *
     * @param identifier identifier of the SubjectArea
     */
    public SubjectAreaNotFoundException(
            MeshTypeIdentifier identifier )
    {
        super( identifier );
    }

    /**
     * Constructor.
     *
     * @param identifier identifier of the SubjectArea
     * @param cause the Throwable that caused this Exception
     */
    public SubjectAreaNotFoundException(
            MeshTypeIdentifier identifier,
            Throwable          cause )
    {
        super( identifier, cause );
    }
}
