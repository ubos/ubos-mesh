//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.externalized.xml.UnknownTokenException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanDataType;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorDataType;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyDataType;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.ExtentDataType;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatDataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerDataType;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.L10PropertyValueMapImpl;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MultiplicityDataType;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointDataType;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.TimePeriodDataType;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampDataType;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.UnknownEnumeratedValueException;
import net.ubos.modelbase.MeshTypeLifecycleManager;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.externalized.ExternalizedAttributes;
import net.ubos.modelbase.externalized.ExternalizedEntityType;
import net.ubos.modelbase.externalized.ExternalizedEnum;
import net.ubos.modelbase.externalized.ExternalizedModuleRequirement;
import net.ubos.modelbase.externalized.ExternalizedPropertyType;
import net.ubos.modelbase.externalized.ExternalizedPropertyTypeGroup;
import net.ubos.modelbase.externalized.ExternalizedRegex;
import net.ubos.modelbase.externalized.ExternalizedRelationshipType;
import net.ubos.modelbase.externalized.ExternalizedRoleType;
import net.ubos.modelbase.externalized.ExternalizedSubjectArea;
import net.ubos.modelbase.externalized.ExternalizedSubjectAreaDependency;
import net.ubos.util.logging.Log;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.MimeTypeNotInDomainException;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.externalized.xml.PropertyValueXmlDecoderHandler;

/**
 * This is the handler for the SAX callbacks during model XML parsing.
 */
public class XmlModelLoaderHandler
        extends
            PropertyValueXmlDecoderHandler
{
    private static final Log log = Log.getLogInstance(XmlModelLoaderHandler.class); // our own, private logger

    /**
      * Constructor.
      *
      * @param inst the MeshTypeLifecycleManager to use to instantiate the model
      * @param modelBase the ModelBase into which to instantiate the model
      * @param idConstructor knows how to construct MeshTypeIdentifiers
      * @param catalogClassLoader the ClassLoader for the XML catalog
      * @param now the time the current run was started
      */
    public XmlModelLoaderHandler(
            MeshTypeLifecycleManager inst,
            ModelBase                modelBase,
            ModelLoaderIdConstructor idConstructor,
            ClassLoader              catalogClassLoader,
            TimeStampValue           now )
    {
        theInstantiator       = inst;
        theModelBase          = modelBase;
        theIdConstructor      = idConstructor;
        theCatalogClassLoader = catalogClassLoader;
        theNow                = now;
    }

    /**
     * This causes us to instantiate the MeshTypes that we have read and buffered.
     *
     * @param theClassLoader the ClassLoader to use
     * @return the instantiated SubjectAreas
     * @throws MissingInfoException no identifier was specified for a SubjectArea
     * @throws MeshTypeNotFoundException thrown if a required MeshType could not be found
     */
    public SubjectArea [] instantiateExternalizedObjects(
            ClassLoader theClassLoader )
        throws
            ParseException,
            MissingInfoException,
            MeshTypeNotFoundException
    {
        SubjectArea [] ret = new SubjectArea[ theSubjectAreas.size() ];

        for( int i=theSubjectAreas.size()-1 ; i>=0 ; --i ) {
            ret[i] = instantiateSubjectArea( theSubjectAreas.get(i), theClassLoader );
        }
        return ret;
    }

    /**
     * SAX says a new element starts.
     *
     * @param namespaceURI URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @param attrs the Attributes at this element
     * @throws SAXException a parsing problem occurred
     */
    @Override
    @SuppressWarnings( "null" )
    public void startElement(
            String     namespaceURI,
            String     localName,
            String     qName,
            Attributes attrs )
        throws
            SAXException
    {
        if( log.isTraceEnabled() ) {
            if( attrs != null && attrs.getValue( "ID" ) != null ) {
                log.traceMethodCallEntry( this, "startElement", namespaceURI, localName, qName, attrs.getValue( "ID") );
            } else {
                log.traceMethodCallEntry( this, "startElement", namespaceURI, localName, qName );
            }
        }

        theCharacters = null;

        int token = XmlModelTokens.getTokenFromKeyword( namespaceURI, qName );

        ExternalizedSubjectArea       theSubjectArea;
        ExternalizedEntityType        theEntityType;
        ExternalizedRelationshipType  theRelationshipType;
        ExternalizedRoleType          theRoleType;
        ExternalizedPropertyType      thePropertyType;
        ExternalizedPropertyTypeGroup thePropertyTypeGroup;

        Object temp;

        switch( token ) {

            case XmlModelTokens.SUBJECT_AREA_TOKEN:
                theSubjectArea = new ExternalizedSubjectArea();
                theSubjectArea.setIdentifier( attrs.getValue( XmlModelTokens.IDENTIFIER_KEYWORD ));
                theStack.push( theSubjectArea );
                break;
            case XmlModelTokens.DEPENDSON_TOKEN:
                // noop
                break;
            case XmlModelTokens.SUBJECT_AREA_REFERENCE_TOKEN:
                theStack.push( new ExternalizedSubjectAreaDependency());
                break;
            case XmlModelTokens.ENTITY_TYPE_TOKEN:
                theSubjectArea = (ExternalizedSubjectArea) theStack.peek();
                theEntityType = new ExternalizedEntityType();
                theEntityType.setIdentifier( attrs.getValue( XmlModelTokens.IDENTIFIER_KEYWORD ));
                theSubjectArea.addEntityType( theEntityType );
                theStack.push( theEntityType );
                break;
            case XmlModelTokens.RELATIONSHIP_TYPE_TOKEN:
                theSubjectArea = (ExternalizedSubjectArea) theStack.peek();
                theRelationshipType = new ExternalizedRelationshipType();
                theRelationshipType.setIdentifier( attrs.getValue( XmlModelTokens.IDENTIFIER_KEYWORD ));
                theSubjectArea.addRelationshipType( theRelationshipType );
                theStack.push( theRelationshipType );
                break;
            case XmlModelTokens.PROPERTY_TYPE_TOKEN:
                temp = theStack.peek();
                thePropertyType = new ExternalizedPropertyType();
                thePropertyType.setIdentifier( attrs.getValue( XmlModelTokens.IDENTIFIER_KEYWORD ));
                if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.addPropertyType( thePropertyType );
                } else if( temp instanceof ExternalizedRoleType ) {
                    theRoleType = (ExternalizedRoleType) temp;
                    theRoleType.addPropertyType( thePropertyType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                theStack.push( thePropertyType );
                break;
            case XmlModelTokens.PROPERTY_TYPE_GROUP_TOKEN:
                temp = theStack.peek();
                thePropertyTypeGroup = new ExternalizedPropertyTypeGroup();
                thePropertyTypeGroup.setIdentifier( attrs.getValue( XmlModelTokens.IDENTIFIER_KEYWORD ));
                if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.addPropertyTypeGroup( thePropertyTypeGroup );
                } else if( temp instanceof ExternalizedRoleType ) {
                    theRoleType = (ExternalizedRoleType) temp;
                    theRoleType.addPropertyTypeGroup( thePropertyTypeGroup );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                theStack.push( thePropertyTypeGroup );
                break;
            case XmlModelTokens.PROPERTY_TYPE_GROUP_MEMBER_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.NAME_TOKEN:
                // noop
                break;
            case XmlModelTokens.USERNAME_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.USERDESCRIPTION_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.SUPERTYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.SYNONYM_TOKEN:
                // noop
                break;
            case XmlModelTokens.IS_ABSTRACT_TOKEN:
                // noop
                break;
            case XmlModelTokens.MAYBE_USED_AS_FORWARD_REFERENCE:
                // noop
                break;
            case XmlModelTokens.IS_OPTIONAL_TOKEN:
                // noop
                break;
            case XmlModelTokens.IS_READONLY_TOKEN:
                // noop
                break;
            case XmlModelTokens.DEFAULT_VALUE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.SOURCE_TOKEN:
                theRelationshipType     = (ExternalizedRelationshipType) theStack.peek();
                theRoleType             = new ExternalizedRoleType( theRelationshipType, true, false );
                theRelationshipType.setSource( theRoleType );
                theStack.push( theRoleType );
                break;
            case XmlModelTokens.DESTINATION_TOKEN:
                theRelationshipType      = (ExternalizedRelationshipType) theStack.peek();
                theRoleType              = new ExternalizedRoleType( theRelationshipType, false, true );
                theRelationshipType.setDestination( theRoleType );
                theStack.push( theRoleType );
                break;
            case XmlModelTokens.SOURCE_DESTINATION_TOKEN:
                theRelationshipType         = (ExternalizedRelationshipType) theStack.peek();
                theRoleType                 = new ExternalizedRoleType( theRelationshipType, false, false );
                theRelationshipType.setSourceDestination( theRoleType );
                theStack.push( theRoleType );
                break;
            case XmlModelTokens.ENTITY_TOKEN:
                // noop
                break;
            case XmlModelTokens.TO_OVERRIDE_TOKEN:
                // noop
                break;
            case XmlModelTokens.SEQUENCE_NUMBER_TOKEN:
                // noop
                break;
            case XmlModelTokens.ICON_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.BLOB_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.BOOLEAN_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.COLOR_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.CURRENCY_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.ENUMERATED_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                theStack.push( new ArrayList<>() ); // we put our domain elements into there
                break;
            case XmlModelTokens.EXTENT_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.FLOAT_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.FLOAT_MATRIX_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.INTEGER_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.MULTIPLICITY_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.POINT_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.STRING_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                theStack.push( new ExternalizedRegex() );
                break;
            case XmlModelTokens.TIME_PERIOD_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.TIME_STAMP_DATATYPE_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;
            case XmlModelTokens.MULTIPLICITY_VALUE_TOKEN:
                // noop
                break;
            case XmlModelTokens.ENUM_TOKEN:
                theStack.push( new ExternalizedEnum());
                break;
            case XmlModelTokens.REGEX_TOKEN:
                // noop
                break;
            case XmlModelTokens.REGEX_ERROR_TOKEN:
                theStack.push( new ExternalizedAttributes( attrs ));
                break;

            default:
                throw new UnknownTokenException( namespaceURI, qName, theLocator );
        }
    }

    /**
     * SAX says an element ends.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @throws SAXException a parsing problem occurred
     */
    @Override
    @SuppressWarnings( "UnusedAssignment" )
    public void endElement(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "endElement", namespaceURI, localName, qName );
        }

        int token = XmlModelTokens.getTokenFromKeyword( namespaceURI, qName );

        ExternalizedSubjectArea            theSubjectArea;
        ExternalizedSubjectAreaDependency  theSubjectAreaDependency;
        ExternalizedModuleRequirement      theModuleRequirement;
        ExternalizedEntityType             theEntityType;
        ExternalizedRelationshipType       theRelationshipType;
        ExternalizedRoleType               theRoleType;
        ExternalizedPropertyType           thePropertyType;
        ExternalizedPropertyTypeGroup      thePropertyTypeGroup;
        ExternalizedAttributes             theAttributes;
        ExternalizedEnum                   theEnum;
        ExternalizedRegex                  theRegex;

        ArrayList<Object>  theCollection;
        DataType           theDataType;
        Object             temp;
        String             typeId;

        switch( token ) {
            case XmlModelTokens.SUBJECT_AREA_TOKEN:
                theSubjectArea = (ExternalizedSubjectArea) theStack.pop();
                theSubjectAreas.add( theSubjectArea );
                break;
            case XmlModelTokens.DEPENDSON_TOKEN:
                // noop
                break;
            case XmlModelTokens.SUBJECT_AREA_REFERENCE_TOKEN:
                temp = theStack.pop();
                if( temp instanceof ExternalizedSubjectAreaDependency ) {
                    theSubjectAreaDependency = (ExternalizedSubjectAreaDependency) temp;
                } else {
                    theSubjectAreaDependency = (ExternalizedSubjectAreaDependency) theStack.pop();
                }
                theSubjectArea           = (ExternalizedSubjectArea) theStack.peek();
                theSubjectArea.addSubjectAreaDependency( theSubjectAreaDependency );
                break;
            case XmlModelTokens.ENTITY_TYPE_TOKEN:
                theEntityType  = (ExternalizedEntityType) theStack.pop();
                break;
            case XmlModelTokens.RELATIONSHIP_TYPE_TOKEN:
                theRelationshipType = (ExternalizedRelationshipType) theStack.pop();
                break;
            case XmlModelTokens.PROPERTY_TYPE_TOKEN:
                thePropertyType = (ExternalizedPropertyType) theStack.pop();
                break;
            case XmlModelTokens.PROPERTY_TYPE_GROUP_TOKEN:
                thePropertyTypeGroup = (ExternalizedPropertyTypeGroup) theStack.pop();
                break;
            case XmlModelTokens.PROPERTY_TYPE_GROUP_MEMBER_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                temp = theStack.peek();
                if( temp instanceof ExternalizedPropertyTypeGroup ) {
                    thePropertyTypeGroup = (ExternalizedPropertyTypeGroup) temp;
                    typeId = theAttributes.getValue( XmlModelTokens.IDENTIFIER_KEYWORD );
                    if( typeId != null ) {
                        thePropertyTypeGroup.addGroupMemberIdentifier( typeId );
                    } else {
                        throw new AttributeRequiredException( qName, XmlModelTokens.IDENTIFIER_KEYWORD, theLocator );
                    }
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.NAME_TOKEN:
                temp = theStack.peek();
                if( temp instanceof ExternalizedSubjectArea ) {
                    theSubjectArea = (ExternalizedSubjectArea) temp;
                    theSubjectArea.setName( createStringValueFrom( theCharacters ));
                } else if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.setName( createStringValueFrom( theCharacters ));
                } else if( temp instanceof ExternalizedRelationshipType ) {
                    theRelationshipType = (ExternalizedRelationshipType) temp;
                    theRelationshipType.setName( createStringValueFrom( theCharacters ));
                } else if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setName( createStringValueFrom( theCharacters ));
                } else if( temp instanceof ExternalizedPropertyTypeGroup ) {
                    thePropertyTypeGroup = (ExternalizedPropertyTypeGroup) temp;
                    thePropertyTypeGroup.setName( createStringValueFrom( theCharacters ));
                } else if( temp instanceof ExternalizedEnum ) {
                    theEnum = (ExternalizedEnum) temp;
                    theEnum.setValue( theCharacters.toString());
                } else if( temp instanceof ExternalizedSubjectAreaDependency )  {
                    theSubjectAreaDependency = (ExternalizedSubjectAreaDependency) temp;
                    theSubjectAreaDependency.setName( createStringValueFrom( theCharacters ));
                } else if( temp instanceof ExternalizedModuleRequirement )  {
                    theModuleRequirement = (ExternalizedModuleRequirement) temp;
                    theModuleRequirement.setName( createStringValueFrom( theCharacters ));
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.USERNAME_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                temp          = theStack.peek();
                if( temp instanceof ExternalizedSubjectArea ) {
                    theSubjectArea = (ExternalizedSubjectArea) temp;
                    theSubjectArea.addUserName( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createStringValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.addUserName( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createStringValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedRelationshipType ) {
                    theRelationshipType = (ExternalizedRelationshipType) temp;
                    theRelationshipType.addUserName( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createStringValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.addUserName( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createStringValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedPropertyTypeGroup ) {
                    thePropertyTypeGroup = (ExternalizedPropertyTypeGroup) temp;
                    thePropertyTypeGroup.addUserName( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createStringValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedRoleType ) {
                    theRoleType = (ExternalizedRoleType) temp;
                    theRoleType.addUserName( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createStringValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedEnum ) {
                    theEnum = (ExternalizedEnum) temp;
                    theEnum.addUserName( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createStringValueFrom( theCharacters.toString() ));
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.USERDESCRIPTION_TOKEN:
                theAttributes   = (ExternalizedAttributes) theStack.pop();

                temp = theStack.peek();
                if( temp instanceof ExternalizedSubjectArea ) {
                    theSubjectArea = (ExternalizedSubjectArea) temp;
                    theSubjectArea.addUserDescription( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createHtmlBlobValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.addUserDescription( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createHtmlBlobValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedRelationshipType ) {
                    theRelationshipType = (ExternalizedRelationshipType) temp;
                    theRelationshipType.addUserDescription( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createHtmlBlobValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedPropertyType )  {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.addUserDescription( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createHtmlBlobValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedPropertyTypeGroup ) {
                    thePropertyTypeGroup = (ExternalizedPropertyTypeGroup) temp;
                    thePropertyTypeGroup.addUserDescription( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createHtmlBlobValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedRoleType ) {
                    theRoleType = (ExternalizedRoleType) temp;
                    theRoleType.addUserDescription( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createHtmlBlobValueFrom( theCharacters.toString() ));
                } else if( temp instanceof ExternalizedEnum ) {
                    theEnum = (ExternalizedEnum) temp;
                    theEnum.addUserDescription( theAttributes.getValue( XmlModelTokens.LOCALE_KEYWORD ), createHtmlBlobValueFrom( theCharacters.toString() ));
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.SUPERTYPE_TOKEN:
                typeId = theCharacters.toString();
                theStack.pop(); // the ExternalizedAttributes -- ignored because we have a string identifying the supertype
                temp = theStack.peek();
                if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.addSuperTypeIdentifier( typeId );
                } else if( temp instanceof ExternalizedRoleType ) {
                    theRoleType = (ExternalizedRoleType) theStack.peek();
                    theRoleType.addSuperTypeIdentifier( typeId );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.SYNONYM_TOKEN:
                typeId =  theCharacters.toString();
                temp = theStack.peek();
                if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.addSynonym( typeId );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.IS_ABSTRACT_TOKEN:
                temp = theStack.peek();
                if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.setIsAbstract( BooleanValue.TRUE );
                } else if( temp instanceof ExternalizedRelationshipType ) {
                    theRelationshipType = (ExternalizedRelationshipType) temp;
                    theRelationshipType.setIsAbstract( BooleanValue.TRUE );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.MAYBE_USED_AS_FORWARD_REFERENCE:
                temp = theStack.peek();
                if( temp instanceof ExternalizedEntityType ) {
                    theEntityType = (ExternalizedEntityType) temp;
                    theEntityType.setMayBeUsedAsForwardReference( BooleanValue.TRUE );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.IS_OPTIONAL_TOKEN:
                temp = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setIsOptional( BooleanValue.TRUE );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.IS_READONLY_TOKEN:
                temp = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setIsReadOnly( BooleanValue.TRUE );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.DEFAULT_VALUE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDefaultValue( constructDefaultValue( theCharacters != null ? theCharacters.toString() : "", thePropertyType, theNow ));
                } else if( temp instanceof ExternalizedRegex ) {
                    theRegex = (ExternalizedRegex) temp;
                    theRegex.setDefaultValue( theCharacters != null ? theCharacters.toString() : "" );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.SOURCE_TOKEN:
                theRoleType        = (ExternalizedRoleType) theStack.pop();
                break;
            case XmlModelTokens.DESTINATION_TOKEN:
                theRoleType        = (ExternalizedRoleType) theStack.pop();
                break;
            case XmlModelTokens.SOURCE_DESTINATION_TOKEN:
                theRoleType        = (ExternalizedRoleType) theStack.pop();
                break;
            case XmlModelTokens.ENTITY_TOKEN:
                theRoleType        = (ExternalizedRoleType) theStack.peek();
                theRoleType.setEntityTypeIdentifier( theCharacters.toString() );
                break;
            case XmlModelTokens.TO_OVERRIDE_TOKEN:
                temp               = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.addToOverride( theCharacters.toString() );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.SEQUENCE_NUMBER_TOKEN:
                temp            = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setSequenceNumber( createFloatValueFrom( theCharacters ));
                } else if( temp instanceof ExternalizedPropertyTypeGroup ) {
                    thePropertyTypeGroup = (ExternalizedPropertyTypeGroup) temp;
                    thePropertyTypeGroup.setSequenceNumber( createFloatValueFrom( theCharacters ));
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.ICON_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                temp = theStack.peek();
                if( temp instanceof ExternalizedEntityType ) {
                    // use file extension to determine mime type
                    String path = theAttributes.getValue( XmlModelTokens.PATH_KEYWORD );
                    int lastPeriod = path.lastIndexOf( '.' );
                    if( lastPeriod > 0 ) {
                        theEntityType = (ExternalizedEntityType) temp;
                        theEntityType.setRelativeIconPath( path );
                        theEntityType.setIconMimeType( "image/" + path.substring( lastPeriod+1 ));
                    }
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.BLOB_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = determineDataType( BlobDataType.class, theAttributes, XmlModelTokens.TYPEFIELD_KEYWORD, BlobDataType.theDefault );
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.BOOLEAN_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = BooleanDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.COLOR_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = ColorDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.CURRENCY_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = CurrencyDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.ENUMERATED_DATATYPE_TOKEN:
                theCollection = toArrayList(             theStack.pop());
                theAttributes = (ExternalizedAttributes) theStack.pop();
                if( !theCollection.isEmpty() ) {
                    String [] domain           = new String[ theCollection.size() ];
                    L10PropertyValueMap [] userNames        = new L10PropertyValueMap[ domain.length ];
                    L10PropertyValueMap [] userDescriptions = new L10PropertyValueMap[ domain.length ];

                    for( int i=0 ; i<domain.length ; ++i ) {
                        theEnum = (ExternalizedEnum) theCollection.get( i );
                        domain[i]           = theEnum.getValue();
                        userNames[i]        = L10PropertyValueMapImpl.create( theEnum.getUserNames(),        StringValue.create( theEnum.getValue() ) );
                        userDescriptions[i] = L10PropertyValueMapImpl.create( theEnum.getUserDescriptions(), null );
                    }
                    theDataType = EnumeratedDataType.create( domain, userNames, userDescriptions, EnumeratedDataType.theDefault );
                } else {
                    theDataType = EnumeratedDataType.theDefault; // should not really happen
                }
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.EXTENT_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = ExtentDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.FLOAT_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = determineDataType( FloatDataType.class, theAttributes, XmlModelTokens.TYPEFIELD_KEYWORD, FloatDataType.theDefault );
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.INTEGER_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = determineDataType( IntegerDataType.class, theAttributes, XmlModelTokens.TYPEFIELD_KEYWORD, IntegerDataType.theDefault );
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.MULTIPLICITY_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = MultiplicityDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.POINT_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = PointDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.STRING_DATATYPE_TOKEN:
                theRegex      = (ExternalizedRegex)      theStack.pop();
                theAttributes = (ExternalizedAttributes) theStack.pop();
                if( theRegex.getRegexString() != null ) {
                    if( theAttributes.getValue( XmlModelTokens.TYPEFIELD_KEYWORD ) != null ) {
                        throw new OneOfTypefieldRegexException( theLocator );
                    }
                    theDataType = theRegex.getAsStringDataType();
                } else {
                    theDataType = determineDataType( StringDataType.class, theAttributes, XmlModelTokens.TYPEFIELD_KEYWORD, StringDataType.theDefault );
                }
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.TIME_PERIOD_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = TimePeriodDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.TIME_STAMP_DATATYPE_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theDataType   = TimeStampDataType.theDefault;
                temp          = theStack.peek();
                if( temp instanceof ExternalizedPropertyType ) {
                    thePropertyType = (ExternalizedPropertyType) temp;
                    thePropertyType.setDataType( theDataType );
                } else {
                    throw new InvalidChildException( temp, qName, theLocator );
                }
                break;
            case XmlModelTokens.MULTIPLICITY_VALUE_TOKEN:
                theRoleType      = (ExternalizedRoleType) theStack.peek();
                theRoleType.setMultiplicity( createMultiplicityValueFrom( theCharacters ));
                break;
            case XmlModelTokens.ENUM_TOKEN:
                theEnum          = (ExternalizedEnum) theStack.pop();
                theCollection    = toArrayList( theStack.peek() );
                theCollection.add( theEnum );
                break;
            case XmlModelTokens.REGEX_TOKEN:
                theRegex         = (ExternalizedRegex) theStack.peek();
                theRegex.setRegexString( theCharacters.toString() );
                break;
            case XmlModelTokens.REGEX_ERROR_TOKEN:
                theAttributes = (ExternalizedAttributes) theStack.pop();
                theRegex      = (ExternalizedRegex) theStack.peek();
                theRegex.addRegexErrorString( theAttributes.getValue(  XmlModelTokens.LOCALE_KEYWORD ), theCharacters.toString() );
                break;

            default:
                throw new UnknownTokenException( namespaceURI, qName, theLocator );
        }
    }

    /**
     * We need this helper method to isolate a non-safe cast.
     *
     * @param o the Object to cast
     * @return the same Object, cast
     */
    @SuppressWarnings("unchecked")
    private static ArrayList<Object> toArrayList(
            Object o )
    {
        return (ArrayList<Object>) o;
    }

    /**
      * SAX wants us to resolve an XML entity.
      *
      * @param publicId the public identifier of the entity
      * @param systemId the system identifier of the entity
      * @return an InputSource to the XML entity, or null if it could not be resolved
      * @throws SAXException a parsing problem occurred
      */
    @Override
    public InputSource resolveEntity(
            String publicId,
            String systemId )
        throws
            SAXException
    {
        if( theCatalogClassLoader != null && XmlModelTokens.PUBLIC_ID_DTD.equals( publicId )) {
            InputStream modelStream = theCatalogClassLoader.getResourceAsStream( XmlModelTokens.LOCAL_MODEL_DTD_PATH );

            if( modelStream != null ) {
                return new InputSource( modelStream );
            }
            throw new CannotFindXmlDtdException( XmlModelTokens.LOCAL_MODEL_DTD_PATH, theLocator );
        }
        return null; // default behavior
    }

    /**
     * Create a StringValue from a String.
     *
     * @param raw the String
     * @return the created StringValue
     */
    protected StringValue createStringValueFrom(
            String raw )
    {
        return StringValue.create( raw );
    }

    /**
     * Create a StringValue from a StringBuilder.
     *
     * @param raw the StringBuilder
     * @return the created StringValue
     */
    protected StringValue createStringValueFrom(
            StringBuilder raw )
    {
        return StringValue.create( raw.toString().trim() );
    }

    /**
     * Create a FloatValue from a String.
     *
     * @param raw the String
     * @return the created FloatValue
     */
    protected FloatValue createFloatValueFrom(
            String raw )
    {
        double d = Double.parseDouble( raw );
        return FloatValue.create( d );
    }

    /**
     * Create a FloatValue from a StringBuilder.
     *
     * @param raw the StringBuilder
     * @return the created FloatValue
     */
    protected FloatValue createFloatValueFrom(
            StringBuilder raw )
    {
        return createFloatValueFrom( raw.toString() );
    }

    /**
     * Create a BlobValue from a String.
     *
     * @param raw the String
     * @return the created BlobValue
     */
    protected BlobValue createHtmlBlobValueFrom(
            String raw )
    {
        if( raw == null || raw.length() == 0 ) {
            return null;
        }
        return BlobDataType.theTextHtmlType.createBlobValue( raw, SelectableMimeType.TEXT_HTML.getMimeType() );
    }

    /**
     * Create a MultiplicityValue from a String.
     *
     * @param raw the String
     * @return the created MultiplicityValue
     */
    protected MultiplicityValue createMultiplicityValueFrom(
            String raw )
    {
        int colon = raw.indexOf( ':' );
        String minString = raw.substring( 0, colon );
        String maxString = raw.substring( colon+1 );

        int min;
        int max;

        if( "N".equalsIgnoreCase( minString )) {
            min = MultiplicityValue.N;
        } else {
            min = Integer.parseInt( raw.substring( 0, colon ));
        }

        if( "N".equalsIgnoreCase( maxString )) {
            max = MultiplicityValue.N;
        } else {
            max = Integer.parseInt( raw.substring( colon+1 ));
        }
        return MultiplicityValue.create( min, max );
    }

    /**
     * Create a MultiplicityValue from a StringBuilder.
     *
     * @param raw the StringBuilder
     * @return the created MultiplicityValue
     */
    protected MultiplicityValue createMultiplicityValueFrom(
            StringBuilder raw )
    {
        return createMultiplicityValueFrom( raw.toString() );
    }

    /**
      * Construct a PropertyValue as a default value given this String
 representation and what we know about the PropertyType at this time.
      *
      * @param raw the String
      * @param pt the ExternalizedPropertyType
      * @param now the time when the run was started
      * @return the created PropertyValue
      * @throws SAXException a parsing problem occurred
      */
    protected PropertyValue constructDefaultValue(
            String                   raw,
            ExternalizedPropertyType pt,
            TimeStampValue           now )
        throws
            SAXException
    {
        PropertyValue ret;
        DataType      type = pt.getDataType();

        if( type instanceof BlobDataType ) {
            BlobDataType realType = (BlobDataType) type;

            try {
                if( raw.startsWith(XmlModelTokens.BLOB_DEFAULTVALUE_MIME_TAG )) {
                    String raw2  = raw.substring( XmlModelTokens.BLOB_DEFAULTVALUE_MIME_TAG.length() );
                    int    blank = raw2.indexOf( ' ' );
                    String mime  = raw2.substring( 0, blank ).trim();

                    if( raw2.regionMatches(blank+1, XmlModelTokens.BLOB_DEFAULTVALUE_LOAD_FROM_TAG, 0, XmlModelTokens.BLOB_DEFAULTVALUE_LOAD_FROM_TAG.length() )) {
                        ret = realType.createBlobValueByLoadingFrom(
                                getClass().getClassLoader(),
                                raw2.substring( blank + 1 + XmlModelTokens.BLOB_DEFAULTVALUE_LOAD_FROM_TAG.length() ),
                                mime );

                    } else if( raw2.regionMatches(blank+1, XmlModelTokens.BLOB_DEFAULTVALUE_BYTES_TAG, 0, XmlModelTokens.BLOB_DEFAULTVALUE_BYTES_TAG.length() )) {
                        byte [] bytes = Base64.getDecoder().decode( raw2.substring( blank + 1 + XmlModelTokens.BLOB_DEFAULTVALUE_BYTES_TAG.length() ));
                        ret = realType.createBlobValue(
                                bytes,
                                mime );

                    } else {
                        ret = realType.createBlobValue( raw, mime );
                    }
                } else if( realType.getAllowsTextMimeType() ) {

                    if( raw.startsWith(XmlModelTokens.BLOB_DEFAULTVALUE_STRING_TAG )) {
                        ret = realType.createBlobValue(raw.substring( XmlModelTokens.BLOB_DEFAULTVALUE_STRING_TAG.length() ), realType.getDefaultTextMimeType() );
                    } else {
                        ret = realType.createBlobValue( raw, realType.getDefaultTextMimeType() );
                    }

                } else {
                    throw new UnderspecifiedBlobValueException( raw, pt, theLocator );
                }
            } catch( MimeTypeNotInDomainException ex ) {
                throw new InvalidEncodingException( type, raw, theLocator );
            }

        } else if( type instanceof BooleanDataType ) {
            char firstChar = raw.charAt( 0 );
            switch( firstChar ) {
                case 'T':
                case 't':
                    ret = BooleanValue.create( true );
                    break;
                case 'F':
                case 'f':
                    ret = BooleanValue.create( false );
                    break;
                default:
                    throw new InvalidEncodingException( type, raw, theLocator );
            }

        } else if( type instanceof ColorDataType ) {
            int value = Integer.parseInt( raw );
            ret = ColorValue.create( value );

        } else if( type instanceof CurrencyDataType ) {
            try {
                ret = CurrencyValue.parseCurrencyValue( raw );
            } catch( ParseException ex ) {
                throw new InvalidEncodingException( type, raw, theLocator );
            }

        } else if( type instanceof EnumeratedDataType ) {
            try {
                ret = ((EnumeratedDataType)type).select( raw );

            } catch( UnknownEnumeratedValueException.Key ex ) {
                throw new InvalidEncodingException( type, raw, theLocator );
            }

        } else if( type instanceof ExtentDataType ) {
            int colon = raw.indexOf( ':' );
            double a = Double.parseDouble( raw.substring( 0, colon ));
            double b = Double.parseDouble( raw.substring( colon+1 ));
            ret = ExtentValue.create( a, b );

        } else if( type instanceof FloatDataType ) {
            ret = FloatValue.create( Double.parseDouble( raw ));

        } else if( type instanceof IntegerDataType ) {
            ret = IntegerValue.create( Integer.parseInt( raw ));

        } else if( type instanceof MultiplicityDataType ) {
            int colon = raw.indexOf( ':' );
            String aString = raw.substring( 0, colon ).trim();
            String bString = raw.substring( colon+1 ).trim();
            int a = Integer.parseInt( aString );
            int b = ( MultiplicityValue.N_SYMBOL.equals( bString ) || "N".equals( bString )) ? MultiplicityValue.N : Integer.parseInt( bString );
            ret = MultiplicityValue.create( a, b );

        } else if( type instanceof PointDataType ) {
            int colon = raw.indexOf( ':' );
            int a = Integer.parseInt( raw.substring( 0, colon ));
            int b = Integer.parseInt( raw.substring( colon+1 ));
            ret = PointValue.create( a, b );

        } else if( type instanceof StringDataType ) {
            ret = StringValue.create( raw );

        } else if( type instanceof TimePeriodDataType ) {
            try {
                int [] values = new int[6];
                int oldSlash = 0;
                int slash;
                int index;

                for( index=0 ; index<values.length ; ++index ) {
                    slash = raw.indexOf( '/', oldSlash );
                    if( slash < 0 ) {
                        values[index] = Integer.parseInt( raw.substring( oldSlash ));
                        break;
                    }
                    values[index] = Integer.parseInt( raw.substring( oldSlash, slash ));
                    oldSlash = slash+1;
                }
                // FIXME -- using integer for float seconds
                ret = TimePeriodValue.create(
                        (short) ((index >=5 ) ? values[ index-5 ] : 0 ),
                        (short) ((index >=4 ) ? values[ index-4 ] : 0 ),
                        (short) ((index >=3 ) ? values[ index-3 ] : 0 ),
                        (short) ((index >=2 ) ? values[ index-2 ] : 0 ),
                        (short) ((index >=1 ) ? values[ index-1 ] : 0 ),
                        (float) ((index >=0 ) ? values[ index ] : 0 ) );

            } catch( NumberFormatException ex ) {
                throw new InvalidEncodingException( type, raw, theLocator );
            }

        } else if( type instanceof TimeStampDataType ) {
            if( "NOW".equalsIgnoreCase( raw )) {
                return now;
            }
            try {
                ret = TimeStampValue.createFromRfc3339( raw );

            } catch( ParseException ex ) {
                throw new InvalidEncodingException( type, raw, theLocator );
            }

        } else {
            ret = null;
            log.error( "Unknown data type " + type );
        }

        return ret;
    }

    /**
     * Internal helper to determine the correct instanceof a subtype of DataType, given the parameters.
     *
     * @param theDataTypeClass the Class whose instance we are looking for
     * @param theAttributes the ExternalizedAttributes containing information about this DataType
     * @param nameOfParameter the name of the parameter
     * @param theDefault the default DataType
     * @return the created / found DataType
      * @throws SAXException a parsing problem occurred
     */
    protected DataType determineDataType(
            Class<? extends DataType> theDataTypeClass,
            ExternalizedAttributes    theAttributes,
            String                    nameOfParameter,
            DataType                  theDefault )
        throws
            SAXException
    {
        String fieldName = theAttributes.getValue( nameOfParameter );
        if( fieldName == null ) {
            return theDefault;
        }

        // use reflection to resolve to the specified default
        try {
            Field theField = theDataTypeClass.getDeclaredField( fieldName );
            if( theField == null ) {
                throw new CannotFindDeclaredFieldException( theDataTypeClass, fieldName, theLocator );
            }

            Object fieldValue = theField.get( null ); // must be static
            if( fieldValue instanceof DataType ) {
                return (DataType) fieldValue;
            } else {
                throw new InvalidFieldException( theDataTypeClass, fieldName, fieldValue, theLocator );
            }
        } catch( NoSuchFieldException ex ) {
            throw new CannotFindDeclaredFieldException( theDataTypeClass, fieldName, theLocator );

        } catch( IllegalAccessException ex ) {
            log.error( ex ); // good enough for the time being, FIXME
            return theDefault;
        }
    }

    /**
     * Internal helper that instantiates all the data in this SubjectArea.
     *
     * @param esa the to-be-instantiated SubjectArea in buffered form
     * @param cl the ClassLoader to use for this SubjectArea
     * @return the instantiated SubjectArea
     * @throws MissingInfoException no identifier was specified for the SubjectArea
     * @throws MeshTypeNotFoundException thrown if a required MeshType could not be found
     */
    protected SubjectArea instantiateSubjectArea(
            ExternalizedSubjectArea esa,
            ClassLoader             cl )
        throws
            ParseException,
            MissingInfoException,
            MeshTypeNotFoundException
    {
        SubjectArea [] saDependencies = new SubjectArea[ esa.getSubjectAreaDependencies().size() ];

        int i=0;
        for( ExternalizedSubjectAreaDependency theExternalizedSubjectAreaDependency
                : esa.getSubjectAreaDependencies() )
        {
            saDependencies[i++] = theModelBase.findSubjectArea(
                    theIdConstructor.forSubjectAreaDependency(
                            theExternalizedSubjectAreaDependency,
                            theLocator ));
        }

        SubjectArea ret = theInstantiator.createSubjectArea(
                theIdConstructor.forSubjectArea( esa, theLocator ),
                esa.getName(),
                userTableToL10Map( esa.getUserNames(),        esa.getName(), StringDataType.theDefault ),
                userTableToL10Map( esa.getUserDescriptions(), null,          BlobDataType.theTextAnyType ),
                saDependencies,
                cl );

        for( ExternalizedEntityType theExternalizedEntityType
                : esa.getEntityTypes() )
        {
            instantiateEntityType( theExternalizedEntityType, ret );
        }
        for( ExternalizedRelationshipType theExternalizedRelationshipType
                : esa.getRelationshipTypes() )
        {
            instantiateRelationshipType( theExternalizedRelationshipType, ret );
        }
        return ret;
    }

    /**
     * Internal helper that instantiates one EntityType.
     *
     * @param eet the to-be-instantiated object in buffered form
     * @param sa the SubjectArea in which the newly instantiated object lives
     * @returns the instantiated EntityType
     * @throws MeshTypeNotFoundException a required MeshType could not be found
     */
    protected EntityType instantiateEntityType(
            ExternalizedEntityType  eet,
            SubjectArea             sa )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        EntityType [] theSupertypes
                = new EntityType[ eet.getSuperTypeIdentifiers().size() ];
        for( int i=0 ; i<theSupertypes.length ; ++i ) {
            MeshTypeIdentifier currentRef = theIdConstructor.forSupertypeReference(
                    sa,
                    eet.getSuperTypeIdentifiers().get( i ));

            theSupertypes[i] = theModelBase.findEntityType( currentRef );
        }

        BlobValue icon;
        if( eet.getRelativeIconPath() != null ) {
            icon = BlobDataType.theJdkSupportedBitmapType.createBlobValueByLoadingFrom(
                    sa.getClassLoader(),
                    eet.getRelativeIconPath(),
                    eet.getIconMimeType() );
        } else {
            icon = null;
        }

        MeshTypeIdentifier [] synomyms = new MeshTypeIdentifier[ eet.getSynonyms().length ];
        for( int i=0 ; i<synomyms.length ; ++i ) {
            synomyms[i] = theIdConstructor.forSynonym( sa, eet.getSynonyms()[i] );
        }
        EntityType ret = theInstantiator.createEntityType(
                    theIdConstructor.forEntityType( sa, eet ),
                    eet.getName(),
                    userTableToL10Map( eet.getUserNames(),        eet.getName(), StringDataType.theDefault ),
                    userTableToL10Map( eet.getUserDescriptions(), null,                                BlobDataType.theTextAnyType ),
                    icon,
                    sa,
                    theSupertypes,
                    synomyms,
                    eet.getIsAbstract(),
                    eet.getMayBeUsedAsForwardReference() );

        for( ExternalizedPropertyType theExternalizedPropertyType
                : eet.getPropertyTypes() )
        {
            instantiatePropertyType(
                    theExternalizedPropertyType,
                    ret,
                    sa );
        }
        return ret;
    }

    /**
     * Internal helper that instantiates one RelationshipType and all it contains.
     *
     * @param ert the to-be-instantiated object in buffered form
     * @param sa the SubjectArea in which the newly instantiated object lives
     * @return the instantiated RelationshipType
     * @throws MeshTypeNotFoundException a required MeshType could not be found
     */
    protected RelationshipType instantiateRelationshipType(
            ExternalizedRelationshipType ert,
            SubjectArea                  sa )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        RelationshipType ret;

        if( ert.getSourceDestination() == null ) {
            // normal case: directed RelationshipType
            RoleType [] theSourceSupertypes      = new RoleType[ ert.getSource().getSuperTypeIdentifiers().size() ];
            RoleType [] theDestinationSupertypes = new RoleType[ ert.getDestination().getSuperTypeIdentifiers().size() ];

            for( int i=0 ; i<theSourceSupertypes.length ; ++i ) {
                MeshTypeIdentifier superId = theIdConstructor.forSupertypeReference(
                        sa,
                        ert.getSource().getSuperTypeIdentifiers().get( i ));
                theSourceSupertypes[i] = theModelBase.findRoleType( superId );
            }
            for( int i=0 ; i<theDestinationSupertypes.length ; ++i ) {
                MeshTypeIdentifier superId = theIdConstructor.forSupertypeReference(
                        sa,
                        ert.getDestination().getSuperTypeIdentifiers().get( i ));
                theDestinationSupertypes[i] = theModelBase.findRoleType( superId );
            }

            MeshTypeIdentifier srcName  = theIdConstructor.forSourceDest(
                    sa,
                    ert.getSource().getEntityTypeIdentifier());
            MeshTypeIdentifier destName = theIdConstructor.forSourceDest(
                    sa,
                    ert.getDestination().getEntityTypeIdentifier());

            EntityType src  = null;
            EntityType dest = null;

            if( srcName != null ) {
                src = theModelBase.findEntityType( srcName );
            }
            if( destName != null ) {
                dest = theModelBase.findEntityType( destName );
            }

            ret = theInstantiator.createRelationshipType(
                    theIdConstructor.forRelationshipType( sa, src, dest, ert ),
                    ert.getName(),
                    userTableToL10Map( ert.getUserNames(),        ert.getName(), StringDataType.theDefault ),
                    userTableToL10Map( ert.getUserDescriptions(), null,          BlobDataType.theTextAnyType ),
                    sa,
                    ert.getSource().getMultiplicity(),
                    ert.getDestination().getMultiplicity(),
                    src,
                    dest,
                    theSourceSupertypes,
                    theDestinationSupertypes,
                    ert.getIsAbstract() );

            for( ExternalizedPropertyType theExternalizedPropertyType
                    : ert.getSource().getPropertyTypes() )
            {
                instantiatePropertyType(
                        theExternalizedPropertyType,
                        ret.getSource(),
                        sa );
            }
            if(    ert.getSource().getPropertyTypeGroups() != null
                && !ert.getSource().getPropertyTypeGroups().isEmpty() )
            {
                for( ExternalizedPropertyTypeGroup theExternalizedPropertyTypeGroup
                        : ert.getSource().getPropertyTypeGroups() )
                {
                    instantiatePropertyTypeGroup(
                            theExternalizedPropertyTypeGroup,
                            ret.getSource(),
                            sa );
                }
            }

            for( ExternalizedPropertyType theExternalizedPropertyType
                    : ert.getDestination().getPropertyTypes() )
            {
                instantiatePropertyType(
                        theExternalizedPropertyType,
                        ret.getDestination(),
                        sa );
            }
            if(    ert.getDestination().getPropertyTypeGroups() != null
                && !ert.getDestination().getPropertyTypeGroups().isEmpty() )
            {
                for( ExternalizedPropertyTypeGroup theExternalizedPropertyTypeGroup
                        : ert.getDestination().getPropertyTypeGroups() )
                {
                    instantiatePropertyTypeGroup(
                            theExternalizedPropertyTypeGroup,
                            ret.getDestination(),
                            sa );
                }
            }
        } else {
            // undirected RelationshipType

            RoleType [] theSrcDestSupertypes = new RoleType[ ert.getSourceDestination().getSuperTypeIdentifiers().size() ];

            for( int i=0 ; i<theSrcDestSupertypes.length ; ++i ) {
                MeshTypeIdentifier current = theIdConstructor.forSourceDest(
                        sa,
                        ert.getSourceDestination().getSuperTypeIdentifiers().get( i ));
                theSrcDestSupertypes[i] = theModelBase.findRoleType( current );
            }

            EntityType srcdest = (EntityType) theModelBase.findMeshType( ert.getSourceDestination().getEntityTypeIdentifier() );
                    // may throw

            ret = theInstantiator.createRelationshipType(
                    theIdConstructor.forRelationshipType( sa, srcdest, srcdest, ert ),
                    ert.getName(),
                    userTableToL10Map( ert.getUserNames(),        ert.getName(), StringDataType.theDefault ),
                    userTableToL10Map( ert.getUserDescriptions(), null,                                      BlobDataType.theTextAnyType ),
                    sa,
                    ert.getSourceDestination().getMultiplicity(),
                    srcdest,
                    theSrcDestSupertypes,
                    ert.getIsAbstract() );

            for( ExternalizedPropertyType theExternalizedPropertyType
                    : ert.getSourceDestination().getPropertyTypes() )
            {
                instantiatePropertyType(
                        theExternalizedPropertyType,
                        ret.getDestination(),
                        sa );
            }
            if(    ert.getSourceDestination().getPropertyTypeGroups() != null
                && !ert.getSourceDestination().getPropertyTypeGroups().isEmpty() )
            {
                for( ExternalizedPropertyTypeGroup theExternalizedPropertyTypeGroup
                        : ert.getSourceDestination().getPropertyTypeGroups() )
                {
                    instantiatePropertyTypeGroup(
                            theExternalizedPropertyTypeGroup,
                            ret.getDestination(),
                            sa );
                }
            }
        }
        return ret;
    }

    /**
     * Internal helper that instantiates one PropertyType.
     *
     * @param ept the to-be-instantiated object in buffered form
     * @param mtwp the MeshTypeWithProperties to which the newly instantiated PropertyType belongs
     * @param sa the SubjectArea in which the newly instantiated object lives
     * @return the instantiated PropertyType
     * @throws MeshTypeNotFoundException a required MeshType could not be found
     */
    protected PropertyType instantiatePropertyType(
            ExternalizedPropertyType  ept,
            MeshTypeWithProperties    mtwp,
            SubjectArea               sa )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        MeshTypeIdentifier identifier = theIdConstructor.forPropertyType( mtwp, ept );
        PropertyType       ret;

        if(    ept.getToOverrides() == null
            || ept.getToOverrides().isEmpty() )
        {
            // no override
            ret = theInstantiator.createPropertyType(
                    identifier,
                    ept.getName(),
                    userTableToL10Map( ept.getUserNames(),        ept.getName(), StringDataType.theDefault ),
                    userTableToL10Map( ept.getUserDescriptions(), null,                                  BlobDataType.theTextAnyType ),
                    mtwp,
                    sa,
                    ept.getDataType(),
                    ept.getDefaultValue(),
                    ept.getIsOptional(),
                    ept.getIsReadOnly(),
                    ept.getSequenceNumber() );
        } else {
            // override
            PropertyType [] toOverride = new PropertyType[ ept.getToOverrides().size() ];
            for( int i=0 ; i<toOverride.length ; ++i )
            {
                String             current    = ept.getToOverrides().get( i );
                MeshTypeIdentifier currentRef = theIdConstructor.forOverriddenPropertyTypeReference( mtwp, current );

                toOverride[i] = theModelBase.findPropertyType( currentRef );
            }
            ret = theInstantiator.createOverridingPropertyType(
                    toOverride,
                    identifier,
                    userTableToL10Map( ept.getUserDescriptions(), null, BlobDataType.theTextAnyType ),
                    mtwp,
                    sa,
                    ept.getDataType(),
                    ept.getDefaultValue(),
                    ept.getIsOptional(),
                    ept.getIsReadOnly() );
        }

        return ret;
    }

    /**
     * Internal helper that instantiates one PropertyTypeGroup.
     *
     * @param eptg the to-be-instantiated object in buffered form
     * @param mtwp the MeshTypeWithProperties to which the newly instantiated PropertyTypeGroup belongs
     * @param sa the SubjectArea in which the newly instantiated object lives
     * @return the instantiated PropertyTypeGroup
     * @throws MeshTypeNotFoundException a required MeshType could not be found
     */
    protected PropertyTypeGroup instantiatePropertyTypeGroup(
            ExternalizedPropertyTypeGroup    eptg,
            MeshTypeWithProperties           mtwp,
            SubjectArea                      sa )
        throws
            ParseException,
            MeshTypeNotFoundException
    {
        MeshTypeIdentifier identifier = theIdConstructor.forPropertyTypeGroup( mtwp, eptg );

        PropertyType [] mas = new PropertyType[ eptg.getGroupMembers().size() ];
        for( int i=0 ; i<mas.length ; ++i ) {
            MeshTypeIdentifier currentRef = theIdConstructor.forGroupedPropertyTypeReference(
                    mtwp,
                    eptg.getGroupMembers().get( i ));

            mas[i] = theModelBase.findPropertyType( currentRef ); // may throw
        }

        PropertyTypeGroup ret = theInstantiator.createPropertyTypeGroup(
                identifier,
                eptg.getName(),
                userTableToL10Map( eptg.getUserNames(),        eptg.getName(), StringDataType.theDefault ),
                userTableToL10Map( eptg.getUserDescriptions(), null,                                       BlobDataType.theTextAnyType ),
                mtwp,
                sa,
                mas,
                eptg.getSequenceNumber() );

        return ret;
    }

    /**
     * Internal helper to convert a HashMap of locale-value mappings into an L10Map.
     *
     * @param theMap the HashMap of locale-value mappings
     * @param defaultValue the default value of the PropertyType
     * @param theType the data type of the PropertyType
     * @return appropriate L10Map
     */
    protected L10PropertyValueMap userTableToL10Map(
            Map<String,PropertyValue> theMap,
            PropertyValue             defaultValue,
            DataType                  theType )
    {
        return L10PropertyValueMapImpl.create( theMap, defaultValue );
    }

    /**
     * Obtain current content of the parse stack (for debugging).
     *
     * @return String representation of the current parse stack
     */
    public String parseStackToString()
    {
        StringBuilder buf = new StringBuilder( 100 ); // fudge
        Iterator<Object> theIter = theStack.iterator();
        int i=0;
        while( theIter.hasNext() ) {
            ++i;
            buf.append( "\n    <" );
            buf.append( i );
            buf.append( "> " );
            buf.append( theIter.next() );
        }
        return buf.toString();
    }

    /**
     * The MeshTypeLifecycleManager that we use to instantiate the objects we find.
     */
    protected final MeshTypeLifecycleManager theInstantiator;

    /**
     * Our ModelBase into which we ultimately write and against which we resolve.
     */
    protected final ModelBase theModelBase;

    /**
     * Deserializer for MeshTypeIdentifiers found.
     */
    protected final ModelLoaderIdConstructor theIdConstructor;

    /**
     * The ClassLoader which we use to load the XML catalog, if any.
     */
    protected final ClassLoader theCatalogClassLoader;

    /**
     * The set of SubjectAreas that we have parsed and found.
     */
    protected final ArrayList<ExternalizedSubjectArea> theSubjectAreas = new ArrayList<>();

    /**
     * The time the current run was started.
     */
    protected final TimeStampValue theNow;

    /**
     * The errors during parsing.
     */
    protected final List<String> theErrors = new ArrayList<>();
}
