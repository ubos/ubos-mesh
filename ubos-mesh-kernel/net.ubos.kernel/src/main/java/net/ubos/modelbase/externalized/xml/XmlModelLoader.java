//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.modelbase.MeshTypeLifecycleManager;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.ModelLoader;
import net.ubos.modelbase.ModelLoadingException;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
  * This class loads a model from an XML InputStream.
  */
public class XmlModelLoader
        extends
            ModelLoader
{
    private static final Log log = Log.getLogInstance(XmlModelLoader.class); // our own, private logger

    /**
     * Constructor.
     *
     * @param modelBase the ModelBase into which we load the model
     * @param s the stream that contains the meta-model in XML format
     * @param systemId SystemId for error reporting purposes
     * @param catalogClassLoader the ClassLoader from which we load the XML catalog
     * @param codeClassLoader the ClassLoader from which we load the code
     */
    public XmlModelLoader(
            ModelBase    modelBase,
            InputStream  s,
            String       systemId,
            ClassLoader  catalogClassLoader,
            ClassLoader  codeClassLoader )
    {
        super( modelBase );

        if( s == null ) {
            throw new NullPointerException( "No stream for the Model XML file" );
        }

        theStream             = s;
        theSystemId           = systemId;
        theCatalogClassLoader = catalogClassLoader;
        theCodeClassLoader    = codeClassLoader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea [] loadModel(
            MeshTypeLifecycleManager theInstantiator,
            TimeStampValue           now )
        throws
            ModelLoadingException,
            IOException
    {
        synchronized( LOCK ) {
            if( theStream == null ) {
                throw new IOException( "no stream from XML file available" );
            }

            try {
                // initialize if we have not done so already
                if( theParser == null ) {
                    // The documentation says we are supposed to be doing this:
                    //      SAXParserFactory.newInstance();
                    // However, this "very smart" implementation tries to use the context class loader to load
                    // that class (and does not give us an option to specify a different class loader) and
                    // that won't work. So we cut through all the clutter and instantiate the thing directly:

                    SAXParserFactory fact = SAXParserFactory.newInstance(); // FIXME? new SAXParserFactoryImpl();
                    fact.setValidating( true );

                    theParser = fact.newSAXParser();
                }

                theHandler = new XmlModelLoaderHandler(
                        theInstantiator,
                        theModelBase,
                        ModelLoaderIdConstructor.SINGLETON,
                        theCatalogClassLoader,
                        now );
                theParser.parse( theStream, theHandler, theSystemId );

                SubjectArea [] ret = theHandler.instantiateExternalizedObjects( theCodeClassLoader );

                return ret;

            } catch( ParserConfigurationException ex ) {
                log.error( ex );
                return new SubjectArea[0]; // FIXME?

            } catch( ParseException ex ) {
                throw new ModelLoadingException( ex );

            } catch( MeshTypeNotFoundException ex ) {
                throw new ModelLoadingException( ex );

            } catch( SAXParseException ex ) {
                throw new ModelLoadingException( ex );

            } catch( SAXException ex ) {
                log.warn( ex, ex );
                throw new FixedIOException( "Could not read model", ex );
            }
        }
    }

    /**
     * The InputStream from which we read the XML data.
     */
    protected InputStream theStream;

    /**
     * The SystemId for error reporting purposes.
     */
    protected String theSystemId;

    /**
     * The ClassLoader from which we can find the DTD catalog, if any.
     */
    protected ClassLoader theCatalogClassLoader;

    /**
     * The ClassLoader through which the classes can be found that implement
     * the loaded meta-model.
     */
    protected ClassLoader theCodeClassLoader;

    /**
     * Our callback handler for SAX callbacks.
     */
    protected XmlModelLoaderHandler theHandler;

    /**
     * Static lock to avoid that we have more than one thread in this class (unlikely anyway).
     */
    protected static final Object LOCK = new Object();

    /**
     * We allocate only one XML parser etc.
     */
    protected static SAXParser theParser = null;

    /**
     * Java IOException's constructor is broken. This attempts to fix it.
     */
    public static class FixedIOException
            extends
                IOException
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
         * Constructor.
         *
         * @param message error message, if any
         * @param cause underlying cause, if any
         */
        public FixedIOException(
                String    message,
                Throwable cause )
        {
            super( message );

            initCause( cause );
        }
    }
}
