//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.m;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;

/**
 * The default "both" serializer for MeshTypeIdentifiers. This is strict, meaning that the String form can only
 * get deserialized if it exactly matches the syntax,
 */
public class DefaultMMeshTypeIdentifierBothSerializer
    implements
        MeshTypeIdentifierBothSerializer
{
    private static final Log log = Log.getLogInstance( DefaultMMeshTypeIdentifierBothSerializer.class );

    /**
     * Singleton instance.
     */
    public static final DefaultMMeshTypeIdentifierBothSerializer SINGLETON
        = new DefaultMMeshTypeIdentifierBothSerializer();

    /**
     * Private constructor, use factory method.
     */
    protected DefaultMMeshTypeIdentifierBothSerializer()
    {}

    /**
     * Factory method for pre-parsed invocation. There is redundancy in the arguments, but it makes things clearer and faster.
     *
     * @param subjectAreaPart the part of the identifier that identifies the SubjectArea
     * @param collectablePart the part of the identifier that identifies the CollectableMeshObject uniquely within
     *                        the parent SubjectArea (if any).
     * @return the created MMeshTypeIdentifier
     */
    public MMeshTypeIdentifier create(
            String subjectAreaPart,
            String collectablePart )
    {
        return new MMeshTypeIdentifier( subjectAreaPart, collectablePart );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MMeshTypeIdentifier fromExternalForm(
            String externalForm )
        throws
            ParseException
    {
        Matcher m = theIdentifierPattern.matcher( externalForm );
        if( !m.matches() ) {
            throw new ParseException( "Invalid MeshTypeIdentifier syntax: " + externalForm, 0 );
        }
        String localPart = m.group( 2 );
        if( localPart != null && localPart.isEmpty() ) {
            localPart = null;
        }
        return new MMeshTypeIdentifier( m.group( 1 ), localPart );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toExternalForm(
            MeshTypeIdentifier identifier )
    {
        if( identifier.getLocalPart() == null ) {
            return identifier.getSubjectAreaPart();
        } else {
            return identifier.getSubjectAreaPart() + SASEP + identifier.getLocalPart();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MMeshTypeIdentifier fromExternalForm(
            MeshTypeIdentifier contextIdentifier,
            String             externalForm )
        throws
            ParseException
    {
        MMeshTypeIdentifier ret = null;

        if( contextIdentifier == null ) {
            ret = fromExternalForm( externalForm );

        } else {
            Matcher m = theIdentifierPattern.matcher( externalForm );
            if( m.matches() ) {
                String localPart = m.group( 2 );
                if( localPart != null && localPart.isEmpty() ) {
                    localPart = null;
                }
                if( localPart == null ) {
                    ret = new MMeshTypeIdentifier( contextIdentifier.getSubjectAreaPart(), externalForm );
                } else {
                    // is fully qualified itself
                    ret = new MMeshTypeIdentifier( m.group( 1 ), localPart );
                }
            } else {
                ret = new MMeshTypeIdentifier( contextIdentifier.getSubjectAreaPart(), externalForm );
            }
        }

        return ret;
    }

    /**
     * The Separator between the SubjectArea part and the CollectableMeshType part.
     */
    public static final char SASEP = '/';

    /**
     * The separator between the CollectableMeshType part and the PropertyType part.
     */
    public static final char PTSEP = '_';

    /**
     * Regular expression for MeshTypeIdentifiers.
     */
    private static final Pattern theIdentifierPattern = Pattern.compile(
              "^"
            + "([a-zA-Z0-9\\._]+)"      // SubjectArea
            + "(?:/"                    // optionally: followed by slash
                + "([-a-zA-Z0-9_]+)"          // CollectableMeshType
            + ")?"
            + "$" );
}
