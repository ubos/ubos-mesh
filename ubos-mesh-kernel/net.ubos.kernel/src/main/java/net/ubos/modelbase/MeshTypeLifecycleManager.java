//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.MeshTypeWithProperties;

/**
  * <p>A life cycle manager for MeshTypes created and stored in a ModelBase.</p>
  * <p>At this point, we do not support the obsoletion of MeshTypes. Likely, we never will.</p>
  */
public interface MeshTypeLifecycleManager
{
    /**
     * Create an EntityType.
     *
     * @param identifier globally unique identifier of this EntityType
     * @param theName programmatic name of this EntityType
     * @param theUserNames internationalized names of this EntityType
     * @param theUserDescriptions internationalized descriptions of this EntityType
     * @param theIcon an icon that can be shown to the user that represents this EntityType
     * @param theSubjectArea the SubjectArea in which this EntityType is defined
     * @param supertypes the zero or more or EntityTypes that are the direct supertypes of this EntityType
     * @param synonyms the alternate MeshTypeIdentifiers identifying this EntityType
     * @param isAbstract if BooleanValue.TRUE, this EntityType cannot be instantiated and a non-abstract subtype must be instantiated instead
     * @param mayBeUsedAsForwardReference if BooleanValue.TRUE, this EntityType may be used as a ForwardReference
     * @return the newly created EntityType
     */
    public EntityType createEntityType(
            MeshTypeIdentifier      identifier,
            StringValue             theName,
            L10PropertyValueMap     theUserNames,
            L10PropertyValueMap     theUserDescriptions,
            BlobValue               theIcon,
            SubjectArea             theSubjectArea,
            EntityType []           supertypes,
            MeshTypeIdentifier []   synonyms,
            BooleanValue            isAbstract,
            BooleanValue            mayBeUsedAsForwardReference );

    /**
     * Create a binary RelationshipType.
     *
     * @param identifier globally unique identifier of this RelationshipType
     * @param theName programmatic name of this RelationshipType
     * @param theUserNames internationalized names of this RelationshipType
     * @param theUserDescriptions internationalized descriptions of this RelationshipType
     * @param theSubjectArea the SubjectArea in which this RelationshipType is defined
     * @param sourceMultiplicity the maximum number of Entities that can participate as the destination
     *        of an instance of this RelationshipType for a given source Entity
     * @param destinationMultiplicity the maximum number of Entities that can participate as the source
     *        of an instance of this RelationshipType for a given destination Entity
     * @param source the source EntityType of this RelationshipType
     * @param destination the destination EntityType of this RelationshipType
     * @param sourceSuperRoleTypes the RoleTypes that are refined by the source RoleTypes of this RelationshipType
     * @param destinationSuperRoleTypes the RoleTypes that are refined by the destination RoleTypes of this RelationshipType
     * @param isAbstract if BooleanValue.TRUE, this RelationshipType cannot be instantiated and a non-abstract subtype must be instantiated instead
     * @return the newly created RelationshipType
     */
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier  identifier,
            StringValue         theName, // full name
            L10PropertyValueMap theUserNames,
            L10PropertyValueMap theUserDescriptions,
            SubjectArea         theSubjectArea,
            MultiplicityValue   sourceMultiplicity,
            MultiplicityValue   destinationMultiplicity,
            EntityType          source,
            EntityType          destination,
            RoleType []         sourceSuperRoleTypes,
            RoleType []         destinationSuperRoleTypes,
            BooleanValue        isAbstract );

    /**
     * Create a binary RelationshipType. This is a convenience method for those RelationshipTypes that
     * refine exactly one RoleType as source and one RoleType as destination, i.e. those RelationshipType
     * that use single (relationship) inheritance.
     *
     * @param identifier globally unique identifier of this RelationshipType
     * @param theName programmatic name of this RelationshipType
     * @param theUserNames internationalized names of this RelationshipType
     * @param theUserDescriptions internationalized descriptions of this RelationshipType
     * @param theSubjectArea the SubjectArea in which this RelationshipType is defined
     * @param sourceMultiplicity the maximum number of Entities that can participate as the destination
     *        of an instance of this RelationshipType for a given source Entity
     * @param destinationMultiplicity the maximum number of Entities that can participate as the source
     *        of an instance of this RelationshipType for a given destination Entity
     * @param source the source EntityType of this RelationshipType
     * @param destination the destination EntityType of this RelationshipType
     * @param sourceSuperRoleType the RoleType that is refined by the source RoleType of this RelationshipType
     * @param destinationSuperRoleType the RoleType that is refined by the destination RoleType of this RelationshipType
     * @param isAbstract if BooleanValue.TRUE, this RelationshipType cannot be instantiated and a non-abstract subtype must be instantiated instead
     * @return the newly created RelationshipType
     */
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName, // full name
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceMultiplicity,
            MultiplicityValue         destinationMultiplicity,
            EntityType                source,
            EntityType                destination,
            RoleType                  sourceSuperRoleType,
            RoleType                  destinationSuperRoleType,
            BooleanValue              isAbstract );

    /**
     * Create an undirected RelationshipType.
     *
     * @param identifier globally unique identifier of this RelationshipType
     * @param theName programmatic name of this RelationshipType
     * @param theUserNames internationalized names of this RelationshipType
     * @param theUserDescriptions internationalized descriptions of this RelationshipType
     * @param theSubjectArea the SubjectArea in which this RelationshipType is defined
     * @param sourceDestMultiplicity the maximum number of Entities that can participate in one
     *        instance of this RelationshipType
     * @param sourceDest the EntityType attached to this RelationshipType
     * @param sourceDestSuperRoleTypes the RoleTypes that are refined by the RoleTypes of this RelationshipType
     * @param isAbstract if BooleanValue.TRUE, this RelationshipType cannot be instantiated and a non-abstract subtype must be instantiated instead
     * @return the newly created RelationshipType
     */
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceDestMultiplicity,
            EntityType                sourceDest,
            RoleType []               sourceDestSuperRoleTypes,
            BooleanValue              isAbstract );

    /**
     * Create a undirected RelationshipType.
     *
     * @param identifier globally unique identifier of this RelationshipType
     * @param theName programmatic name of this RelationshipType
     * @param theUserNames internationalized names of this RelationshipType
     * @param theUserDescriptions internationalized descriptions of this RelationshipType
     * @param theSubjectArea the SubjectArea in which this RelationshipType is defined
     * @param sourceDestMultiplicity the maximum number of Entities that can participate in one
     *        instance of this RelationshipType
     * @param sourceDest the EntityType attached to this RelationshipType
     * @param sourceDestSuperRoleType the RoleType that is refined by the RoleType of this RelationshipType
     * @param isAbstract if BooleanValue.TRUE, this RelationshipType cannot be instantiated and a non-abstract subtype must be instantiated instead
     * @return the newly created RelationshipType
     */
    public RelationshipType createRelationshipType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea               theSubjectArea,
            MultiplicityValue         sourceDestMultiplicity,
            EntityType                sourceDest,
            RoleType                  sourceDestSuperRoleType,
            BooleanValue              isAbstract );

    /**
     * Create a PropertyType.
     *
     * @param identifier globally unique identifier of this PropertyType
     * @param theName programmatic name of this PropertyType
     * @param theUserNames internationalized names of this PropertyType
     * @param theUserDescriptions internationalized descriptions of this PropertyType
     * @param theParent the parent MeshTypeWithProperties in which this PropertyType is defined
     * @param theSubjectArea the SubjectArea in which this PropertyType is defined
     * @param theDataType the data type for this PropertyType
     * @param theDefaultValue the value with which instances of this PropertyType are initialized. This must
     *                        be non-null for non-optional PropertyType
     * @param isOptional if true, this PropertyType can have value null; if false, this PropertyType cannot have the null value
     * @param isReadOnly if true, this PropertyType can only be read, not written
     * @param theSequenceNumber determines the sequence of display in things like property sheets etc.
     * @return the newly created PropertyType
     */
    public PropertyType createPropertyType(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties    theParent,
            SubjectArea               theSubjectArea,
            DataType                  theDataType,
            PropertyValue             theDefaultValue,
            BooleanValue              isOptional,
            BooleanValue              isReadOnly,
            FloatValue                theSequenceNumber );

    /**
     * Create a PropertyType that overrides another PropertyType or set of PropertyTypes.
     *
     * @param toOverride set of PropertyTypes to override. All PropertyTypes given must have the same ancestor PropertyType,
     *        and each PropertyType must be declared in a different supertype of our parent MeshTypeWithProperties that uses
     *        multiple inheritance. For single inheritance, the length of this array must be 1.
     * @param overriddenIdentifier the unique identifier of this overridden PropertyType
     * @param theUserDescriptions internationalized descriptions of this PropertyType
     * @param theParent the parent MeshTypeWithProperties in which this PropertyType is defined
     * @param theSubjectArea the SubjectArea in which this PropertyType is defined
     * @param theDataType the data type for this PropertyType
     * @param theDefaultValue the value with which instances of this PropertyType are initialized. This must
     *                        be non-null for non-optional PropertyType
     * @param isOptional if true, this PropertyType can have value null; if false, this PropertyType cannot have the null value
     * @param isReadOnly if true, this PropertyType can only be read, not written
     * @return the newly created PropertyType
     * @throws InheritanceConflictException thrown if the overridden PropertyType are wrong
     */
    public PropertyType createOverridingPropertyType(
            PropertyType []           toOverride,
            MeshTypeIdentifier        overriddenIdentifier,
            L10PropertyValueMap       theUserDescriptions,
            MeshTypeWithProperties    theParent,
            SubjectArea               theSubjectArea,
            DataType                  theDataType,
            PropertyValue             theDefaultValue,
            BooleanValue              isOptional,
            BooleanValue              isReadOnly )
        throws
            InheritanceConflictException;

    /**
     * Create a PropertyTypeGroup.
     *
     * @param identifier globally unique identifier of this PropertyTypeGroup
     * @param theName programmatic name of this PropertyTypeGroup
     * @param theUserNames internationalized names of this PropertyTypeGroup
     * @param theUserDescriptions internationalized descriptions of this PropertyTypeGroup
     * @param theParent the parent MeshTypeWithProperties in which this PropertyTypeGroup is defined
     * @param theSubjectArea the SubjectArea in which this PropertyTypeGroup is defined
     * @param theMembers the member PropertyTypes of this PropertyTypeGroup. All PropertyTypes must be defined in the parent
     *        MeshTypeWithProperties, or any of its supertypes.
     * @param theSequenceNumber determines the sequence of display in things like property sheets etc.
     * @return the newly created PropertyTypeGroup
     */
    public PropertyTypeGroup createPropertyTypeGroup(
            MeshTypeIdentifier     identifier,
            StringValue            theName,
            L10PropertyValueMap    theUserNames,
            L10PropertyValueMap    theUserDescriptions,
            MeshTypeWithProperties theParent,
            SubjectArea            theSubjectArea,
            PropertyType []        theMembers,
            FloatValue             theSequenceNumber );

    /**
     * Create a SubjectArea.
     *
     * @param identifier globally unique identifier of this SubjectArea
     * @param theName programmatic name of this SubjectArea
     * @param theUserNames internationalized names of this SubjectArea
     * @param theUserDescriptions internationalized descriptions of this SubjectArea
     * @param theSubjectAreaDependencies the set of SubjectAreas on whose content members of this SubjectArea depend
     * @param theClassLoader the ClassLoader to use to load resources needed in this SubjectArea
     * @return the newly created SubjectArea
     */
    public SubjectArea createSubjectArea(
            MeshTypeIdentifier        identifier,
            StringValue               theName,
            L10PropertyValueMap       theUserNames,
            L10PropertyValueMap       theUserDescriptions,
            SubjectArea []            theSubjectAreaDependencies,
            ClassLoader               theClassLoader );
}
