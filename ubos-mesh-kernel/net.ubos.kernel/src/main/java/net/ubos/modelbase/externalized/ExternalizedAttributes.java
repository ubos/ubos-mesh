//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.HashMap;
import java.util.Iterator;
import org.xml.sax.Attributes;

/**
 * This contains a list of XML node attributes. We can't store the XML Attributes object directly,
 * because it is mutable (apparently, after a lot of debugging).
 */
public class ExternalizedAttributes
{
    /**
      * Constructor.
      *
      * @param raw theAttributes object from which we take our content
      */
    public ExternalizedAttributes(
            Attributes raw )
    {
        int i = raw.getLength();
        if( i>0 ) {
            // optimization
            theMap = new HashMap<>( i );

            for( --i ; i>=0 ; --i ) {
                theMap.put( raw.getQName( i ), raw.getValue( i ));
            }
        }
    }

    /**
     * Obtain the value of an attribute with this name.
     *
     * @param keyword the name of the attribute
     * @return the value of the attribute, or null if not present
     */
    public String getValue(
            String keyword )
    {
        if( theMap != null ) {
            return theMap.get( keyword );
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder( 100 ); // fudge
        buf.append( super.toString() );
        buf.append( "{" );
        if( theMap != null ) {
            Iterator<String> theIter = theMap.keySet().iterator();
            while( theIter.hasNext() ) {
                String currentKey = theIter.next();

                buf.append( " " );
                buf.append( currentKey );
                buf.append( "=" );
                buf.append( theMap.get( currentKey ));
            }
        } else {
            buf.append( " empty" );
        }
        buf.append( " }" );
        return buf.toString();
    }

    /**
     * Internally, we use a HashMap.
     */
    protected HashMap<String,String> theMap;
}
