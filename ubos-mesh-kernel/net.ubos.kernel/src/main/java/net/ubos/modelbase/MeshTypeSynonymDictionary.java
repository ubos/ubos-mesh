//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;

/**
 * This interface is supported by objects that know how to translate an identifier
 * other than the MeshType's primary identifier into the right MeshType. This is
 * useful if external namespace authorities identify concepts that are synonyms
 * with concepts defined in models, but at a different unique identifier.
 */
public interface MeshTypeSynonymDictionary
{
    /**
     * Obtain the ModelBase that this MeshTypeSynonymDictionary delegates to.
     *
     * @return the ModelBase
     */
    public ModelBase getModelBase();

    /**
     * Obtain the MeshType for this identifier.
     *
     * @param externalIdentifier the Identifier
     * @return the MeshType
     * @throws MeshTypeNotFoundException thrown if this Identifier was not known
     */
    public MeshType findMeshTypeByIdentifier(
            MeshTypeIdentifier externalIdentifier )
        throws
            MeshTypeNotFoundException;
}
