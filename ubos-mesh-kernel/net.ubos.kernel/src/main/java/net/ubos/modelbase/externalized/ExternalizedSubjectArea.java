//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.ArrayList;

/**
 * This is data wanting to become a SubjectArea, during reading.
 */
public class ExternalizedSubjectArea
    extends
        ExternalizedMeshType
{
    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addEntityType(
            ExternalizedEntityType newValue )
    {
        theEntityTypes.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<ExternalizedEntityType> getEntityTypes()
    {
        return theEntityTypes;
    }

    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addRelationshipType(
            ExternalizedRelationshipType newValue )
    {
        theRelationshipTypes.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<ExternalizedRelationshipType> getRelationshipTypes()
    {
        return theRelationshipTypes;
    }

    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addSubjectAreaDependency(
            ExternalizedSubjectAreaDependency newValue )
    {
        theSubjectAreaDependencies.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<ExternalizedSubjectAreaDependency> getSubjectAreaDependencies()
    {
        return theSubjectAreaDependencies;
    }

    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addModuleRequirement(
            ExternalizedModuleRequirement newValue )
    {
        theModuleRequirements.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public ArrayList<ExternalizedModuleRequirement> getModuleRequirements()
    {
        return theModuleRequirements;
    }

    /**
     * List of ExternalizedEntityTypes, as they are being read in.
     */
    protected final ArrayList<ExternalizedEntityType> theEntityTypes = new ArrayList<>();

    /**
     * List of ExternalizedRelationshipTypes, as they are being read in.
     */
    protected final ArrayList<ExternalizedRelationshipType> theRelationshipTypes = new ArrayList<>();

    /**
     * List of SubjectAreas that we are dependent on, as they are being read in.
     */
    protected final ArrayList<ExternalizedSubjectAreaDependency> theSubjectAreaDependencies = new ArrayList<>();

    /**
     * List of Modules, beyond the SubjectAreas, that we are dependent on, as they are being read in.
     */
    protected final ArrayList<ExternalizedModuleRequirement> theModuleRequirements = new ArrayList<>();
}
