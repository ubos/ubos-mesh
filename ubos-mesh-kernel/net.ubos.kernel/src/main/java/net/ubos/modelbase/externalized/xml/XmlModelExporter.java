//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Locale;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanDataType;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.ColorDataType;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyDataType;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.ExtentDataType;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatDataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerDataType;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.MultiplicityDataType;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointDataType;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.TimePeriodDataType;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampDataType;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.modelbase.ModelBase;
import net.ubos.util.logging.Log;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;

/**
 * This class knows how to export the current model in a ModelBase.
 */
public class XmlModelExporter
{
    private static final Log log = Log.getLogInstance( XmlModelExporter.class ); // our own, private logger

    /**
     * Factory method with defaults.
     *
     * @return the created XmlModelExporter
     */
    public static XmlModelExporter create()
    {
        return new XmlModelExporter( DefaultMMeshTypeIdentifierBothSerializer.SINGLETON );
    }

    /**
     * Constructor.
     *
     * @param idSerializer knows how to serialize MeshTypeIdentifiers suitably
     */
    protected XmlModelExporter(
            MeshTypeIdentifierSerializer idSerializer )
    {
        theIdSerializer = idSerializer;
    }

    /**
     * Export the full content of this ModelBase to an XML stream.
     *
     * @param modelBase the ModelBase whose content is being exported
     * @param theStream the OutputStream to which the data is sent
     * @throws IOException a write error occurred
     */
    public void exportToXML(
            ModelBase    modelBase,
            OutputStream theStream )
        throws
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "exportToXML", modelBase, theStream );
        }

        SubjectArea [] theSubjectAreas = modelBase.getLoadedSubjectAreas();

        exportToXML( theSubjectAreas, theStream );
    }

    /**
     * Export one SubjectArea to an XML stream.
     *
     * @param theSubjectArea the SubjectArea that is being exported
     * @param theStream the OutputStream to which the data is sent
     * @throws IOException a write error occurred
     */
    public void exportToXML(
            SubjectArea  theSubjectArea,
            OutputStream theStream )
        throws
            IOException
    {
        exportToXML( new SubjectArea[] { theSubjectArea }, theStream );
    }

    /**
     * Export a set of SubjectAreas to an XML stream.
     *
     * @param theSubjectAreas the SubjectAreas that are being exported
     * @param theStream the OutputStream to which the data is sent
     * @throws IOException a write error occurred
     */
    public void exportToXML(
            SubjectArea [] theSubjectAreas,
            OutputStream   theStream )
        throws
            IOException
    {
        Writer theWriter = new OutputStreamWriter( theStream, StandardCharsets.UTF_8 );

        writeXmlHeader( theWriter, StandardCharsets.UTF_8.name() );

        for( int s=0 ; s<theSubjectAreas.length ; ++s ) {
            SubjectArea currentSa = theSubjectAreas[s];

            // sa keyword
            doIndent( 0, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUBJECT_AREA_TOKEN ));
            theWriter.write( " " );
            theWriter.write( XmlModelTokens.IDENTIFIER_KEYWORD );
            theWriter.write( "=\"" );
            theWriter.write( theIdSerializer.toExternalForm( currentSa.getIdentifier() ));
            theWriter.write( "\">\n" );

            // sa.name keyword
            doIndent( 1, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
            theWriter.write( ">" );
            writeValue( currentSa.getName(), false, theWriter );
            theWriter.write( "</" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
            theWriter.write( ">\n" );

            // sa.username keyword
            exportToXMLUserVisibleNameMap( 1, theWriter, currentSa );

            // sa.userdescription keyword
            exportToXMLUserVisibleDescriptionMap( 1, theWriter, currentSa );

            if( currentSa.getSubjectAreaDependencies().length > 0 ) {

                // sa.dependson keyword
                doIndent( 1, theWriter );
                theWriter.write( "<" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.DEPENDSON_TOKEN ));
                theWriter.write( ">\n" );

                SubjectArea [] saDependencies = currentSa.getSubjectAreaDependencies();
                for( int i=0 ; i<saDependencies.length ; ++i ) {
                    // sa.dependson.sareference keyword
                    doIndent( 2, theWriter );
                    theWriter.write( "<" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUBJECT_AREA_REFERENCE_TOKEN ));
                    theWriter.write( ">\n" );

                    // sa.dependson.sareference.name keyword
                    doIndent( 3, theWriter );
                    theWriter.write( "<" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
                    theWriter.write( ">" );
                    writeValue( saDependencies[i].getName(), false, theWriter );
                    theWriter.write( "</" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
                    theWriter.write( ">\n" );

                    // /sa.dependson.sareference keyword
                    doIndent( 2, theWriter );
                    theWriter.write( "</" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUBJECT_AREA_REFERENCE_TOKEN ));
                    theWriter.write( ">\n" );
                }

                doIndent( 1, theWriter );
                theWriter.write( "</" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.DEPENDSON_TOKEN ));
                theWriter.write( ">\n" );
            }

            CollectableMeshType [] cmos = currentSa.getCollectableMeshTypes();

            for( int i=0 ; i<cmos.length ; ++i ) {
                if( cmos[i] instanceof EntityType ) {
                    EntityType currentMe = (EntityType) cmos[i];

                    // sa.me keyword
                    doIndent( 1, theWriter );
                    theWriter.write( "<" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENTITY_TYPE_TOKEN ));
                    theWriter.write( " " );
                    theWriter.write( XmlModelTokens.IDENTIFIER_KEYWORD );
                    theWriter.write( "=\"" );
                    theWriter.write( theIdSerializer.toExternalForm( currentMe.getIdentifier() ));
                    theWriter.write( "\">\n" );

                    // sa.me.name keyword
                    doIndent( 2, theWriter );
                    theWriter.write( "<" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
                    theWriter.write( ">" );
                    writeValue( currentMe.getName(), false, theWriter );
                    theWriter.write( "</" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
                    theWriter.write( ">\n" );

                    // me.username keyword
                    exportToXMLUserVisibleNameMap( 2, theWriter, currentMe );

                    // me.userdescription keyword
                    exportToXMLUserVisibleDescriptionMap( 2, theWriter, currentMe );

                    // sa.me.icon keyword
                    if( currentMe.getIcon() != null ) {
                        doIndent( 2, theWriter );
                        theWriter.write( "<" );
                        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ICON_TOKEN ));
                        theWriter.write( " " );
                        theWriter.write( XmlModelTokens.PATH_KEYWORD );
                        theWriter.write( "=\"" );
                        writeValue( currentMe.getIcon(), false, theWriter );
                        theWriter.write( "\">\n" );
                    }

                    // sa.me.supertype
                    MeshTypeWithProperties [] supertypes = currentMe.getDirectSupertypes();
                    for( int j=0 ; j<supertypes.length ; ++j ) {
                        doIndent( 2, theWriter );
                        theWriter.write( "<" );
                        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUPERTYPE_TOKEN ));
                        theWriter.write( ">" );
                        theWriter.write( theIdSerializer.toExternalForm( supertypes[j].getIdentifier() ));
                        theWriter.write( "</" );
                        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUPERTYPE_TOKEN ));
                        theWriter.write( ">\n" );
                    }

                    // sa.me.isabstract keyword
                    if( currentMe.getIsAbstract().value() ) {
                        doIndent( 2, theWriter );
                        theWriter.write( "<" );
                        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.IS_ABSTRACT_TOKEN ));
                        theWriter.write( "/>\n" );
                    }

                    // sa.pt... pt,ppt,ptgroup keyword
                    PropertyType [] pts = currentMe.getLocalPropertyTypes();
                    for( int j=0 ; j<pts.length ; ++j ) {
                        exportToXMLPropertyType( 2, theWriter, pts[j] );
                    }

                    pts = currentMe.getOverridingLocalPropertyTypes();
                    for( int j=0 ; j<pts.length ; ++j ) {
                        exportToXMLPropertyType( 2, theWriter, pts[j] );
                    }

                    PropertyTypeGroup [] ptGroups = currentMe.getLocalPropertyTypeGroups();
                    for( int j=0 ; j<ptGroups.length ; ++j ) {
                        exportToXMLPropertyTypeGroup( 2, theWriter, ptGroups[j] );
                    }
                    // sa./me keyword
                    doIndent( 1, theWriter );
                    theWriter.write( "</" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENTITY_TYPE_TOKEN ));
                    theWriter.write( ">\n" );

                } else if( cmos[i] instanceof RelationshipType ) {

                    RelationshipType currentRt = (RelationshipType) cmos[i];

                    // sa.mr keyword
                    doIndent( 1, theWriter );
                    theWriter.write( "<" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.RELATIONSHIP_TYPE_TOKEN ));
                    theWriter.write( " " );
                    theWriter.write( XmlModelTokens.IDENTIFIER_KEYWORD );
                    theWriter.write( "=\"" );
                    theWriter.write( theIdSerializer.toExternalForm( currentRt.getIdentifier() ));
                    theWriter.write( "\">\n" );

                    // sa.mr.name keyword
                    doIndent( 2, theWriter );
                    theWriter.write( "<" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
                    theWriter.write( ">" );
                    writeValue( currentRt.getName(), false, theWriter );
                    theWriter.write( "</" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
                    theWriter.write( ">\n" );

                    // sa.username keyword
                    exportToXMLUserVisibleNameMap( 2, theWriter, currentRt );

                    // sa.userdescription keyword
                    exportToXMLUserVisibleDescriptionMap( 2, theWriter, currentRt );

                    // sa.mr.src / dest / srcdest keywords
                    if( currentRt.getSource().isSymmetric() ) {
                        // top singleton
                        exportToXMLSrcDestRoleType( 2, theWriter, currentRt.getSource() );

                    } else {
                        // normal relationship type
                        exportToXMLRoleType(
                                2,
                                theWriter,
                                currentRt.getSource(),
                                XmlModelTokens.getKeywordFromToken( XmlModelTokens.SOURCE_TOKEN ));
                        exportToXMLRoleType(
                                2,
                                theWriter,
                                currentRt.getDestination(),
                                XmlModelTokens.getKeywordFromToken( XmlModelTokens.DESTINATION_TOKEN ));
                    }

                    // sa.mr.isabstract keyword
                    if( currentRt.getIsAbstract().value() ) {
                        doIndent( 2, theWriter );
                        theWriter.write( "<" );
                        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.IS_ABSTRACT_TOKEN ));
                        theWriter.write( "/>\n" );
                    }

                    // sa./mr keyword
                    doIndent( 1, theWriter );
                    theWriter.write( "</" );
                    theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.RELATIONSHIP_TYPE_TOKEN ));
                    theWriter.write( ">\n" );

                } else {
                     // nothing
                }
            }
            // /sa
            doIndent( 0, theWriter );
            theWriter.write( "</" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUBJECT_AREA_TOKEN ));
            theWriter.write( ">\n" );
        }

        theWriter.flush();
    }

    /**
     * Internal helper to write a PropertyType to an XML export stream.
     *
     * @param indent the indent level
     * @param theWriter the Writer we write to
     * @param current the PropertyType to export
     * @throws IOException a write error occurred
     */
    protected void exportToXMLPropertyType(
            int                  indent,
            Writer               theWriter,
            PropertyType         current )
        throws
            IOException
    {
        // pt/ppt keyword
        doIndent( indent, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.PROPERTY_TYPE_TOKEN ));
        theWriter.write( " " );
        theWriter.write( XmlModelTokens.IDENTIFIER_KEYWORD );
        theWriter.write( "=\"" );
        theWriter.write( theIdSerializer.toExternalForm( current.getIdentifier() ));
        theWriter.write( "\">\n" );

        // pt.name keyword
        doIndent( indent+1, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
        theWriter.write( ">" );
        writeValue( current.getName(), false, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
        theWriter.write( ">\n" );

        // pt.username keyword
        exportToXMLUserVisibleNameMap( indent+1, theWriter, current );

        // pt.userdescription keyword
        exportToXMLUserVisibleDescriptionMap( indent+1, theWriter, current );

        // pt.tooverride keyword
        if( current.getOverride() != null ) {

            PropertyType [] toOverride = current.getOverride();
            for( int j=0 ; j<toOverride.length ; ++j ) {

                doIndent( indent+1, theWriter );
                theWriter.write( "<" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.TO_OVERRIDE_TOKEN ));
                theWriter.write( ">" );
                theWriter.write( theIdSerializer.toExternalForm( toOverride[j].getIdentifier() ));
                theWriter.write( "</" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.TO_OVERRIDE_TOKEN ));
                theWriter.write( ">\n" );
            }
        }

        // pt.datatype keyword
        exportToXMLDataType( indent+1, theWriter, current.getDataType() );

        // pt.defaultvalue keyword
        if( current.getDefaultValue() != null ) {

            doIndent( indent+1, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.DEFAULT_VALUE_TOKEN ));
            theWriter.write( ">" );
            writeValue( current.getDefaultValue(), true, theWriter ); // FIXME? false?
            theWriter.write( "</" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.DEFAULT_VALUE_TOKEN ));
            theWriter.write( ">\n" );
        }

        // pt.isoptional keyword
        if( current.getIsOptional().value() ) {

            doIndent( indent+1, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.IS_OPTIONAL_TOKEN ));
            theWriter.write( "/>\n" );
        }

        // pt.isreadonly keyword
        if( current.getIsReadOnly().value() ) {

            doIndent( indent+1, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.IS_READONLY_TOKEN ));
            theWriter.write( "/>\n" );
        }

        // ppt.sequencenumber keyword
        doIndent( indent+1, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SEQUENCE_NUMBER_TOKEN ));
        theWriter.write( ">" );
        writeValue( current.getSequenceNumber(), false, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SEQUENCE_NUMBER_TOKEN ));
        theWriter.write( ">\n" );

        // pt//ppt keyword
        doIndent( indent, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.PROPERTY_TYPE_TOKEN ));
        theWriter.write( ">\n" );
    }

    /**
     * Internal helper to write a PropertyTypeGroup to an XML export stream.
     *
     * @param indent the indent level
     * @param theWriter the Writer we write to
     * @param currentGroup the PropertyTypeGroup to export
     * @throws IOException a write error occurred
     */
    protected void exportToXMLPropertyTypeGroup(
            int                   indent,
            Writer                theWriter,
            PropertyTypeGroup     currentGroup )
        throws
            IOException
    {
        // magroup keyword
        doIndent( indent, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.PROPERTY_TYPE_GROUP_TOKEN ));
        theWriter.write( " " );
        theWriter.write( XmlModelTokens.IDENTIFIER_KEYWORD );
        theWriter.write( "=\"" );
        theWriter.write( theIdSerializer.toExternalForm( currentGroup.getIdentifier() ));
        theWriter.write( "\">\n" );

        // ptgroup.name keyword
        doIndent( indent+1, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
        theWriter.write( ">" );
        writeValue( currentGroup.getName(), false, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.NAME_TOKEN ));
        theWriter.write( ">\n" );

        // ptgroup.username keyword
        exportToXMLUserVisibleNameMap( indent+1, theWriter, currentGroup );

        // ptgroup.userdescription keyword
        exportToXMLUserVisibleDescriptionMap( indent+1, theWriter, currentGroup );

        PropertyType [] pts = currentGroup.getContainedPropertyTypes();
        for( int j=0 ; j<pts.length ; ++j ) {

            // ptgroup.ptgroupmember keyword
            doIndent( indent+1, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.PROPERTY_TYPE_GROUP_MEMBER_TOKEN ));
            theWriter.write( " " );
            theWriter.write( XmlModelTokens.IDENTIFIER_KEYWORD );
            theWriter.write( "=\"" );
            theWriter.write( theIdSerializer.toExternalForm( pts[j].getIdentifier() ));
            theWriter.write( "\"/>\n" );
        }

        // ptgroup.sequencenumber keyword
        doIndent( indent+1, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SEQUENCE_NUMBER_TOKEN ));
        theWriter.write( ">" );
        writeValue( currentGroup.getSequenceNumber(), false, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SEQUENCE_NUMBER_TOKEN ));
        theWriter.write( ">\n" );

        // /ptgroup keyword
        doIndent( indent, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.PROPERTY_TYPE_GROUP_TOKEN ));
        theWriter.write( ">\n" );
    }

    /**
     * Internal helper to write a RoleType of a undirected RelationshipType to an XML export stream.
     *
     * @param indent the indent level
     * @param theWriter the Writer we write to
     * @param top the RoleType to export
     * @throws IOException a write error occurred
     */
    protected void exportToXMLSrcDestRoleType(
            int      indent,
            Writer   theWriter,
            RoleType top )
        throws
            IOException
    {
        // srcdest keyword
        doIndent( indent, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SOURCE_DESTINATION_TOKEN ));
        theWriter.write( ">\n" );

        // e keyword
        doIndent( indent+1, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENTITY_TOKEN ));
        theWriter.write( ">" );
        theWriter.write( theIdSerializer.toExternalForm( top.getEntityType().getIdentifier() ));
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENTITY_TOKEN ));
        theWriter.write( ">\n" );

        // MultiplicityValue keyword
        doIndent( indent+1, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.MULTIPLICITY_VALUE_TOKEN ));
        theWriter.write( ">" );
        writeValue( top.getMultiplicity(), false, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.MULTIPLICITY_VALUE_TOKEN ));
        theWriter.write( ">\n" );

        // /srcdest keyword
        doIndent( indent, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SOURCE_DESTINATION_TOKEN ));
        theWriter.write( ">\n" );
    }

    /**
     * Internal helper to write a normal RoleType to an XML export stream.
     *
     * @param indent the indent level
     * @param theWriter the Writer we write to
     * @param role the RoleType to export
     * @param keyword the XML keyword to use for this element
     * @throws IOException a write error occurred
     */
    protected void exportToXMLRoleType(
            int                   indent,
            Writer                theWriter,
            RoleType              role,
            String                keyword )
        throws
            IOException
    {
        // src / dest keyword
        doIndent( indent, theWriter );
        theWriter.write( "<" );
        theWriter.write( keyword );
        theWriter.write( ">\n" );

        // e keyword
        if( role.getEntityType() != null ) {
            doIndent( indent+1, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENTITY_TOKEN ));
            theWriter.write( ">" );
            theWriter.write( theIdSerializer.toExternalForm( role.getEntityType().getIdentifier() ));
            theWriter.write( "</" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENTITY_TOKEN ));
            theWriter.write( ">\n" );
        }

        // refines keyword
        RoleType [] supertypes = role.getDirectSupertypes();
        for( int i=0 ; i<supertypes.length ; ++i ) {

            doIndent( indent+1, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUPERTYPE_TOKEN ));
            theWriter.write( ">" );
            theWriter.write( theIdSerializer.toExternalForm( supertypes[i].getIdentifier() ));
            theWriter.write( "</" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.SUPERTYPE_TOKEN ));
            theWriter.write( ">\n" );
        }

        // MultiplicityValue keyword
        doIndent( indent+1, theWriter );
        theWriter.write( "<" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.MULTIPLICITY_VALUE_TOKEN ));
        theWriter.write( ">" );
        writeValue( role.getMultiplicity(), false, theWriter );
        theWriter.write( "</" );
        theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.MULTIPLICITY_VALUE_TOKEN ));
        theWriter.write( ">\n" );

        // sa.me... pt,ppt,ptgroup keyword
        PropertyType [] pts = role.getLocalPropertyTypes();
        for( int j=0 ; j<pts.length ; ++j ) {
            exportToXMLPropertyType( 3, theWriter, pts[j] );
        }
        pts = role.getOverridingLocalPropertyTypes();
        for( int j=0 ; j<pts.length ; ++j ) {
            exportToXMLPropertyType( 3, theWriter, pts[j] );
        }
        PropertyTypeGroup [] ptGroups = role.getLocalPropertyTypeGroups();
        for( int j=0 ; j<ptGroups.length ; ++j ) {
            exportToXMLPropertyTypeGroup( 3, theWriter, ptGroups[j] );
        }

        // /src / /dest keyword
        doIndent( indent, theWriter );
        theWriter.write( "</" );
        theWriter.write( keyword );
        theWriter.write( ">\n" );
    }

    /**
     * Internal helper to write a DataType to an XML export stream.
     *
     * @param indent the indent level
     * @param theWriter the Writer we write to
     * @param type the DataType to export
     * @throws IOException a write error occurred
     */
    protected void exportToXMLDataType(
            int                  indent,
            Writer               theWriter,
            DataType             type )
        throws
            IOException
    {
        String qualifier;
        if( type instanceof BlobDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.BLOB_DATATYPE_TOKEN ));

            qualifier = determineDataTypeQualifier( BlobDataType.class, type );

            if( qualifier != null ) {
                theWriter.write( qualifier );
            }
            theWriter.write( "/>\n" );

        } else if( type instanceof BooleanDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.BOOLEAN_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else if( type instanceof ColorDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.COLOR_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else if( type instanceof CurrencyDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.CURRENCY_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else if( type instanceof EnumeratedDataType ) {
            EnumeratedDataType realType = (EnumeratedDataType) type;
            EnumeratedValue [] domain   = realType.getDomain();

            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENUMERATED_DATATYPE_TOKEN ));
            theWriter.write( ">\n" );
            for( int i=0 ; i<domain.length ; ++i ) {
                doIndent( indent+1, theWriter );
                theWriter.write( "<" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENUM_TOKEN ));
                theWriter.write( ">" );
                writeValue( domain[i], false, theWriter );
                theWriter.write( "</" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENUM_TOKEN ));
                theWriter.write( ">\n" );
            }
            doIndent( indent, theWriter );
            theWriter.write( "</" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.ENUMERATED_DATATYPE_TOKEN ));
            theWriter.write( ">\n" );

        } else if( type instanceof ExtentDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.EXTENT_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else if( type instanceof FloatDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.FLOAT_DATATYPE_TOKEN ));

            qualifier = determineDataTypeQualifier( FloatDataType.class, type );

            if( qualifier != null ) {
                theWriter.write( qualifier );
            }
            theWriter.write( "/>\n" );

        } else if( type instanceof IntegerDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.INTEGER_DATATYPE_TOKEN ));

            qualifier = determineDataTypeQualifier( IntegerDataType.class, type );

            if( qualifier != null ) {
                theWriter.write( qualifier );
            }
            theWriter.write( "/>\n" );

        } else if( type instanceof MultiplicityDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.MULTIPLICITY_DATATYPE_TOKEN ));

            qualifier = determineDataTypeQualifier( MultiplicityDataType.class, type );

            if( qualifier != null ) {
                theWriter.write( qualifier );
            }
            theWriter.write( "/>\n" );

        } else if( type instanceof PointDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.POINT_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else if( type instanceof StringDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.STRING_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else if( type instanceof TimePeriodDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.TIME_PERIOD_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else if( type instanceof TimeStampDataType ) {
            doIndent( indent, theWriter );
            theWriter.write( "<" );
            theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.TIME_STAMP_DATATYPE_TOKEN ));
            theWriter.write( "/>\n" );

        } else {
            log.error( "unknown type: " + type );
        }
    }

    /**
     * Internal helper to write a UserVisibleName map to an XML export stream.
     *
     * @param indent the indent level
     * @param theWriter the Writer we write to
     * @param mo the MeshType whose UserVisibeName map is being exported
     * @throws IOException a write error occurred
     */
    protected void exportToXMLUserVisibleNameMap(
            int      indent,
            Writer   theWriter,
            MeshType mo )
        throws
            IOException
    {
        L10PropertyValueMap theMap = mo.getUserVisibleNameMap();

        if( theMap != null ) {
            PropertyValue v = theMap.getDefault();
            if( v != null ) {
                doIndent( indent, theWriter );
                theWriter.write( "<" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERNAME_TOKEN ));
                theWriter.write( ">" );
                writeValue( v, false, theWriter );
                theWriter.write( "</" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERNAME_TOKEN ));
                theWriter.write( ">\n" );
            }

            Iterator theIter = theMap.keyIterator();
            while( theIter.hasNext() ) {
                Locale key = (Locale) theIter.next();
                v = theMap.get( key );

                doIndent( indent, theWriter );
                theWriter.write( "<" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERNAME_TOKEN ));
                theWriter.write( " " );
                theWriter.write( XmlModelTokens.LOCALE_KEYWORD );
                theWriter.write( "=\"" );
                theWriter.write( key.toString() );
                theWriter.write( "\">" );
                writeValue( v, false, theWriter );
                theWriter.write( "</" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERNAME_TOKEN ));
                theWriter.write( ">\n" );
            }
        }
    }

    /**
     * Internal helper to write a UserVisibleDescription map to an XML export stream.
     *
     * @param indent the indent level
     * @param theWriter the Writer we write to
     * @param mo the MeshType whose UserVisibleDescription map is being exported
     * @throws IOException a write error occurred
     */
    protected void exportToXMLUserVisibleDescriptionMap(
            int      indent,
            Writer   theWriter,
            MeshType mo )
        throws
            IOException
    {
        L10PropertyValueMap theMap = mo.getUserVisibleDescriptionMap();

        if( theMap != null ) {
            PropertyValue v = theMap.getDefault();

            if( v != null ) {
                doIndent( indent, theWriter );
                theWriter.write( "<" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERDESCRIPTION_TOKEN ));
                theWriter.write( ">" );
                writeValue( v, false, theWriter );
                theWriter.write( "</" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERDESCRIPTION_TOKEN ));
                theWriter.write( ">\n" );
            }

            Iterator theIter = theMap.keyIterator();
            while( theIter.hasNext() ) {
                Locale key = (Locale) theIter.next();
                v = theMap.get( key );

                doIndent( indent, theWriter );
                theWriter.write( "<" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERDESCRIPTION_TOKEN ));
                theWriter.write( " " );
                theWriter.write( XmlModelTokens.LOCALE_KEYWORD );
                theWriter.write( "=\"" );
                theWriter.write( key.toString() );
                theWriter.write( "\">" );
                writeValue( v, false, theWriter );
                theWriter.write( "</" );
                theWriter.write( XmlModelTokens.getKeywordFromToken( XmlModelTokens.USERDESCRIPTION_TOKEN ));
                theWriter.write( ">\n" );
            }
        }
    }

    /**
      * Internal helper to write any PropertyValue to an XML stream.
      *
      * @param value the PropertyValue to serialize
      * @param writeTag if true, tag the value with the type
      * @param theWriter the Writer to serialize to
      * @throws IOException thrown if a write error occurred
      */
    public void writeValue(
            PropertyValue value,
            boolean       writeTag,
            Writer        theWriter )
        throws
            IOException
    {
        if( value == null ) {
            theWriter.write( "NULL" );
            return;
        }

        if( value instanceof BlobValue ) {
            BlobValue realValue = (BlobValue) value;
            if( realValue.delayedLoadingFrom() != null ) {
                String mt = realValue.getMimeType();
                if( writeTag ) {
                    theWriter.write( "BlobDelayed:" );
                    if( mt != null ) {
                        theWriter.write( escape( mt ) );
                    } else {
                        theWriter.write( escape( "?/?" ));
                    }
                    theWriter.write( ":" );
                }
                theWriter.write( "<![CDATA[" );
                theWriter.write( escape( realValue.delayedLoadingFrom() ));
                theWriter.write( "]]>" );
                return;
            } else {
                String mt = realValue.getMimeType();
                if( writeTag ) {
                    theWriter.write( "Blob:" );
                    if( mt != null ) {
                        theWriter.write( escape( mt ) );
                    } else {
                        theWriter.write( escape( "?/?" ));
                    }
                    theWriter.write( ":" );
                }

                if( mt != null && ! mt.startsWith( "text" )) {
                    theWriter.write("x\'");
                    theWriter.write(BlobValue.encodeHex(realValue.value() ) );
                    theWriter.write("\'");
                } else {
                    theWriter.write( "<![CDATA[" );
                    theWriter.write( escape( realValue.getAsString() ));
                    theWriter.write( "]]>" );
                }
                return;
            }
        }

        if( value instanceof BooleanValue ) {
            if( writeTag ) {
                theWriter.write( "Boolean:" );
            }
            if( ((BooleanValue)value).value() ) {
                theWriter.write( "TRUE" );
            } else {
                theWriter.write( "FALSE" );
            }
            return;
        }

        if( value instanceof ColorValue ) {
            if( writeTag ) {
                theWriter.write( "Color:" );
            }
            theWriter.write( String.valueOf( ((ColorValue)value).getRGB() ));
            return;
        }

        if( value instanceof CurrencyValue ) {
            CurrencyValue realValue = (CurrencyValue) value;
            if( writeTag ) {
                theWriter.write( "Currency:" );
            }
            theWriter.write(realValue.value() );
            return;
        }

        if( value instanceof EnumeratedValue ) {
            if( writeTag ) {
                theWriter.write( "Enumerated:" );
            }
            theWriter.write(escape(((EnumeratedValue)value).value() ));
            return;
        }

        if( value instanceof ExtentValue ) {
            if( writeTag ) {
                theWriter.write( "Extent:" );
            }
            theWriter.write( String.valueOf( ((ExtentValue)value).getWidth() ));
            theWriter.write( ";" );
            theWriter.write( String.valueOf( ((ExtentValue)value).getHeight() ));
            return;
        }

        if( value instanceof FloatValue ) {
            if( writeTag ) {
                theWriter.write( "Float:" );
            }
            theWriter.write(String.valueOf(((FloatValue)value).value() ));
            return;
        }

        if( value instanceof IntegerValue ) {
            if( writeTag ) {
                theWriter.write( "Integer:" );
            }
            theWriter.write(String.valueOf(((IntegerValue)value).value() ));
            return;
        }

        if( value instanceof MultiplicityValue ) {
            MultiplicityValue theValue = (MultiplicityValue) value;
            if( writeTag ) {
                theWriter.write( "Multiplicity:" );
            }
            theWriter.write( ( theValue.getMinimum() < 0 ) ? "N" : String.valueOf( theValue.getMinimum() ) );
            theWriter.write( ':' );
            theWriter.write( ( theValue.getMaximum() < 0 ) ? "N" : String.valueOf( theValue.getMaximum() ) );
            return;
        }

        if( value instanceof PointValue ) {
            PointValue theValue = (PointValue) value;
            if( writeTag ) {
                theWriter.write( "Point:" );
            }
            theWriter.write( String.valueOf( theValue.getX() ));
            theWriter.write( ";" );
            theWriter.write( String.valueOf( theValue.getY() ));
            return;
        }

        if( value instanceof StringValue ) {
            if( writeTag ) {
                theWriter.write( "String:" );
            }
            theWriter.write(escape(((StringValue)value).value() ));
            return;
        }

        if( value instanceof TimePeriodValue ) {
            TimePeriodValue realValue = (TimePeriodValue) value;
            if( writeTag ) {
                theWriter.write( "TimePeriod:" );
            }
            theWriter.write( String.valueOf( realValue.getYear() ));
            theWriter.write( '/' );
            theWriter.write( String.valueOf( realValue.getMonth() ));
            theWriter.write( '/' );
            theWriter.write( String.valueOf( realValue.getDay() ));
            theWriter.write( ' ' );
            theWriter.write( String.valueOf( realValue.getHour() ));
            theWriter.write( ':' );
            theWriter.write( String.valueOf( realValue.getMinute() ));
            theWriter.write( ':' );
            theWriter.write( String.valueOf( realValue.getSecond() ));
            return;
        }

        if( value instanceof TimeStampValue ) {
            TimeStampValue realValue = (TimeStampValue) value;
            if( writeTag ) {
                theWriter.write( "TimeStamp:" );
            }
            theWriter.write( realValue.getAsRfc3339String() );
            return;
        }

        theWriter.write( "?" );
    }

    /**
     * This writes the XML file header to a stream.
     *
     * @param theWriter the Writer we write to
     * @param encoding the encoding to use
     * @throws IOException a write error occurred
     */
    protected static void writeXmlHeader(
            Writer theWriter,
            String encoding )
        throws
            IOException
    {
        theWriter.write( "<?xml version=\"1.0\"" );
        if( encoding != null ) {
            theWriter.write( " encoding=\"" );
            theWriter.write( encoding );
            theWriter.write( "\"" );
        }
        theWriter.write( "?>\n" );
        theWriter.write( "<!DOCTYPE model" );
        theWriter.write( " PUBLIC '-//ubos.net//UBOS Data Mesh Model//EN' '" );
        theWriter.write( XmlModelTokens.PUBLIC_MODEL_DTD_URL );
        theWriter.write( "'>\n" );
        theWriter.write( "\n" );
    }

    /**
     * Helper method to determine the qualifier string to be added to a DataType which
     * indicates which instance variable to use. Returns null if default.
     *
     * @param theDataTypeClass the Class of the DataType
     * @param theDataTypeInstance the instance of the DataType
     * @return the name of the member variable in theDataTypeClass
     */
    protected String determineDataTypeQualifier(
            Class    theDataTypeClass,
            DataType theDataTypeInstance )
    {
        // we use reflection to find which instance this is

        Field [] allFields = theDataTypeClass.getFields();
        for( int i=0 ; i<allFields.length ; ++i ) {
            try {
                if( ! Modifier.isStatic( allFields[i].getModifiers() )) {
                    continue;
                }

                if( theDataTypeInstance == allFields[i].get( null )) {
                    // found
                    String fieldName = allFields[i].getName();
                    if( "theDefault".equals( fieldName )) {
                        return null;
                    } else {
                        return " " + XmlModelTokens.TYPEFIELD_KEYWORD + "=\"" + fieldName + "\"";
                    }
                }
            } catch( IllegalAccessException ex ) {
                log.error( ex );
            }
        }
        // not found
        log.error( "could not find " + theDataTypeInstance + " in class " + theDataTypeClass.getName() );
        return null;
    }

    /**
     * This helper takes arbitrary strings and escapes them in a way so that they
     * fit into XML. FIXME This is not complete at all.
     *
     * @param org the original String
     * @return the escaped String
     */
    protected static String escape(
            String org )
    {
        StringBuilder ret = new StringBuilder( org.length() + org.length()/5 ); // make a little larger

        for( int i=0 ; i<org.length() ; ++i ) {
            char c = org.charAt( i );
            switch( c ) {
                case '\'':
                case '\"':
                case '\t':
                // case '\n':
                case '\\':
                case '&':
                // case '#':
                case '<':
                // case '>':
                // case '/':
                    {
                        ret.append( "&#x" );
                        // needs to have a least four hex digits according to the XML spec
                        int digit;
                        for( int j=3 ; j>=0 ; --j ) {
                            digit = c >> (j*4);
                            digit &= 0xf;
                            ret.append( charTable[ digit ] );
                        }

                        ret.append( ";" );
                        break;
                    }
                default:
                    ret.append( c );
                    break;
            }
        }
        return new String( ret );
    }

    /**
     * Helper method to indent by n steps.
     *
     * @param n the number of steps to indent
     * @param theWriter the Writer we write to
     * @throws IOException a write error occurred
     */
    protected static void doIndent(
            int    n,
            Writer theWriter )
        throws
            IOException
    {
        if( n <= 0 ) {
            return;
        }

        int nBlanks = 2*n;
        if( nBlanks <= spaces.length ) {
            theWriter.write( spaces, 0, nBlanks );
        } else {
            theWriter.write( spaces );
            doIndent( nBlanks - spaces.length, theWriter );
        }
    }

    /**
     * The indent space buffer.
     */
    protected static final char [] spaces = new char[ 16 ];
    static {
        for( int i=0 ; i<spaces.length ; ++ i ) {
            spaces[i] = ' ';
        }
    }

    /**
     * For conversion speed, a character table.
     */
    protected static char[] charTable = {
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    /**
     * Knows how to serialize MeshTypeIdentifiers suitably.
     */
    protected final MeshTypeIdentifierSerializer theIdSerializer;
}
