//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.util.exception.AbstractLocalizedRuntimeException;

/**
 * This Exception is thrown if the specified PropertyType overriding was inconsistent
 * with the model. For example, an EntityType cannot override a PropertyType in another
 * EntityType that is not one of its supertypes.
 */
public class InheritanceConflictException
        extends
            AbstractLocalizedRuntimeException
{
    /**
     * Construct one.
     *
     * @param amt the MeshTypeWithProperties where the problem occurred
     * @param pts the PropertyType that are in conflict
     */
    public InheritanceConflictException(
            MeshTypeWithProperties amt,
            PropertyType []        pts )
    {
        theAmt = amt;
        thePts = pts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getLocalizationParameters()
    {
        return new Object[] {
            theAmt,
            thePts
        };
    }

    /**
     * The MeshTypeWithProperties where the problem occurred.
     */
    protected final MeshTypeWithProperties theAmt;

    /**
     * The conflicting PropertyTypes.
     */
    protected final PropertyType [] thePts;
}
