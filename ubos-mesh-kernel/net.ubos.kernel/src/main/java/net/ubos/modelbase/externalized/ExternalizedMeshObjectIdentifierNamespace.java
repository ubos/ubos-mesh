//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.ArrayList;
import java.util.List;
import net.ubos.util.logging.Log;

/**
 * This is data wanting to become a MeshObjectIdentifierNamespace, during reading.
 */
public class ExternalizedMeshObjectIdentifierNamespace
{
    private static final Log log = Log.getLogInstance( ExternalizedMeshObjectIdentifierNamespace.class );

    /**
     * Set the name for this namespace within the scope of this externalization.
     *
     * @param newValue the new value
     */
    public void setLocalName(
            String newValue )
    {
        if( theLocalName != null ) {
            throw new IllegalStateException( "Cannot reset local name: " + newValue + ", was" + theLocalName );
        }
        theLocalName = newValue;
    }

    /**
     * Get the name for this namespace within the scope of this externalization.
     *
     * @return the value
     */
    public String getLocalName()
    {
        return theLocalName;
    }

    /**
     * Add an external name.
     *
     * @param toAdd to add
     */
    public void addExternalName(
            ExternalizedExternalName toAdd )
    {
        if( theExternalNames.contains( toAdd.getValue() )) {
            throw new IllegalStateException( "Adding same external name again: " + toAdd.getValue() );
        }
        if( toAdd.getPreferred() ) {
            if( thePreferredExternalName == null ) {
                thePreferredExternalName = toAdd.getValue();
                theExternalNames.add( 0, toAdd.getValue() ); // put at beginning of list
            } else {
                theExternalNames.add( toAdd.getValue() );
                log.warn( "More than one preferred external name:", thePreferredExternalName, toAdd.getValue() );
            }
        } else {
            theExternalNames.add( toAdd.getValue() );
        }
    }

    /**
     * Add a preferred external name, alternate invocation.
     *
     * @param toAdd to add
     */
    public void addPreferredExternalName(
            String toAdd )
    {
        if( thePreferredExternalName == null ) {
            thePreferredExternalName = toAdd;
            theExternalNames.add( 0, toAdd ); // put at beginning of list
        } else {
            theExternalNames.add( toAdd );
            log.warn( "More than one preferred external name:", thePreferredExternalName, toAdd );
        }
    }

    /**
     * Add a non-preferred external name, alternate invocation.
     * @param toAdd to add
     */
    public void addExternalName(
            String toAdd )
    {
        theExternalNames.add( toAdd );
    }

    /**
     * Get the preferred external name.
     *
     * @return the external name
     */
    public String getPreferredExternalName()
    {
        return thePreferredExternalName;
    }

    /**
     * Get the non-preferred external names.
     *
     * @return the external names
     */
    public List<String> getExternalNames()
    {
        return theExternalNames;
    }

    /**
     * The name of this namespace within the scope of this externalization
     */
    protected String theLocalName;

    /**
     * The preferred ExternalName of this namespace.
     */
    protected String thePreferredExternalName;

    /**
     * All ExternalNames of this namespace.
     */
    protected final List<String> theExternalNames = new ArrayList<>();
}
