//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.m;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.util.StringHelper;

/**
 * MeshTypeIdentifier implementation for MModelBase.
 */
public class MMeshTypeIdentifier
        implements
             MeshTypeIdentifier
{
    /**
     * Constructor.
     *
     * @param subjectAreaPart the part of the identifier that identifies the SubjectArea
     * @param localPart the part of the identifier that identifies the CollectableMeshObject uniquely within
     *                        the parent SubjectArea (if any). This is null for SubjectAreas only.
     */
    protected MMeshTypeIdentifier(
            String subjectAreaPart,
            String localPart )
    {
        if( subjectAreaPart == null ) {
            throw new NullPointerException();
        }
        theSubjectAreaPart = subjectAreaPart;
        theLocalPart       = localPart;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSubjectAreaPart()
    {
        return theSubjectAreaPart;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalPart()
    {
        return theLocalPart;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        if( theLocalPart == null ) {
            return "{ " + getClass().getName() + ": " + theSubjectAreaPart + " }";
        } else {
            return "{ " + getClass().getName() + ": " + theSubjectAreaPart + "/" + theLocalPart + " }";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = theSubjectAreaPart.hashCode();
        if( theLocalPart != null ) {
            ret ^= theLocalPart.hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MMeshTypeIdentifier )) {
            return false;
        }

        MMeshTypeIdentifier realOther = (MMeshTypeIdentifier) other;

        if( StringHelper.compareTo(theLocalPart, realOther.theLocalPart ) != 0 ) {
            return false;
        }
        if( StringHelper.compareTo( theSubjectAreaPart, realOther.theSubjectAreaPart ) != 0 ) {
            return false;
        }
        return true;
    }

    /**
     * Let us create MesnTypeIdentifiers for RoleTypes.
     *
     * @param postfix the RoleType postfix
     * @return the created MesnTypeIdentifiers
     */
    public MMeshTypeIdentifier createRoleTypeIdentifier(
            RoleTypeDirection postfix )
    {
        return new MMeshTypeIdentifier( theSubjectAreaPart, theLocalPart + postfix.getPostfix() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            MeshTypeIdentifier o )
    {
        int ret = theSubjectAreaPart.compareTo( o.getSubjectAreaPart() );
        if( ret == 0 ) {
            ret = theLocalPart.compareTo( o.getLocalPart());
        }
        return ret;
    }

    /**
     * The underlying String identifying the first component (SubjectArea).
     */
    protected final String theSubjectAreaPart;

    /**
     * The underlying String identifying the second component (CollectableMeshType),
     * if any.
     */
    protected final String theLocalPart;
}
