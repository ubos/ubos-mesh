//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase;

import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.logging.CanBeDumped;

/**
 * This Exception is thrown if a type lookup was unsuccessful.
 * Sub-classes provide more detail.
 */
public abstract class MeshTypeNotFoundException
        extends
            AbstractLocalizedException
        implements
            CanBeDumped
{
    /**
     * Protected constructor, use subclasses only.
     */
    protected MeshTypeNotFoundException()
    {
        super();
    }

    /**
     * Protected constructor, use subclasses only.
     *
     * @param cause the Throwable that caused this Exception
     */
    protected MeshTypeNotFoundException(
            Throwable cause )
    {
        super( cause );
    }

    /**
     * Protected constructor, use subclasses only.
     *
     * @param message the message
     */
    protected MeshTypeNotFoundException(
            String message )
    {
        super( message );
    }

    /**
     * Protected constructor, use subclasses only.
     *
     * @param message the message
     * @param cause the Throwable that caused this Exception
     */
    protected MeshTypeNotFoundException(
            String    message,
            Throwable cause )
    {
        super( message, cause );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return getClass().getName() + ": ";
    }
}
