//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized;

import java.util.ArrayList;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.util.ArrayHelper;

/**
 * This is data wanting to become a EntityType, during reading.
 */
public class ExternalizedEntityType
        extends
            ExternalizedMeshTypeWithProperties
{
    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setRelativeIconPath(
            String newValue )
    {
        if( theRelativeIconPath != null ) {
            throw new IllegalStateException( "Have relative icon path already: " + theRelativeIconPath + " vs " + newValue );
        }
        theRelativeIconPath = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public String getRelativeIconPath()
    {
        return theRelativeIconPath;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setIconMimeType(
            String newValue )
    {
        if( theIconMimeType != null ) {
            throw new IllegalStateException( "Have icon MIME type already: " + theIconMimeType + " vs " + newValue );
        }
        theIconMimeType = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public String getIconMimeType()
    {
        return theIconMimeType;
    }

    /**
     * Add to the property.
     *
     * @param newValue the new value
     */
    public void addSynonym(
            String newValue )
    {
        theSynonyms.add( newValue );
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public String [] getSynonyms()
    {
        return ArrayHelper.copyIntoNewArray( theSynonyms, String.class );
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setMayBeUsedAsForwardReference(
            BooleanValue newValue )
    {
        if( theMayBeUsedAsForwardReference != null ) {
            throw new IllegalStateException( "Have may be used as forward reference already: " + theMayBeUsedAsForwardReference + " vs " + newValue );
        }
        theMayBeUsedAsForwardReference = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public BooleanValue getMayBeUsedAsForwardReference()
    {
        return theMayBeUsedAsForwardReference == null ? BooleanValue.FALSE : theMayBeUsedAsForwardReference;
    }

    /**
     * Set property.
     *
     * @param newValue the new value
     */
    public void setIsAbstract(
            BooleanValue newValue )
    {
        if( theIsAbstract != null ) {
            throw new IllegalStateException( "Have isAbstract already: " + theIsAbstract + " vs " + newValue );
        }
        theIsAbstract = newValue;
    }

    /**
     * Get property.
     *
     * @return the value
     */
    public BooleanValue getIsAbstract()
    {
        return theIsAbstract == null ? BooleanValue.FALSE : theIsAbstract;
    }

    /**
      * Relative icon path, if any.
      */
    protected String theRelativeIconPath;

    /**
     * Mime type of the icon, if any.
     */
    protected String theIconMimeType;

    /**
     * List of Strings containing the identifiers of synonyms.
     */
    protected final ArrayList<String> theSynonyms = new ArrayList<>();

    /**
     * MayBeUsedAsForwardReference.
     */
    protected BooleanValue theMayBeUsedAsForwardReference;

    /**
     * IsAbstract.
     */
    protected BooleanValue theIsAbstract;
}
