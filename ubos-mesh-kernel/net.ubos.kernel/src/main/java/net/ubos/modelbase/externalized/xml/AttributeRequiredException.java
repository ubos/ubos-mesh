//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.modelbase.externalized.xml;

import net.ubos.model.primitives.externalized.xml.LocalizedSAXParseException;
import org.xml.sax.Locator;

/**
 * A tag did not carry a required attribute.
 */
public class AttributeRequiredException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     * 
     * @param tagName the name of the tag that did not carry the attribute
     * @param requiredAttribute the name of the attribute that was required
     * @param locator knows the location of the problem
     */
    public AttributeRequiredException(
            String  tagName,
            String  requiredAttribute,
            Locator locator )
    {
        super( locator );

        theTagName           = tagName;
        theRequiredAttribute = requiredAttribute;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[]{
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theTagName,
            theRequiredAttribute
        };
    }

    /**
     * The name of the tag that did not carry the attribute.
     */
    protected final String theTagName;
    
    /**
     * The name of the attribute that was required.
     */
    protected final String theRequiredAttribute;
}
