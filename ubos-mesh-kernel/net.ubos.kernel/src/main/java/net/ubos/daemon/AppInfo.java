//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.daemon;

/**
 * Capture information about this AppConfiguration.
 */
public class AppInfo
{
    /**
     * Factory method.
     *
     * @param fullAppName the full name of the main App at this AppConfiguration
     * @param shortAppName the short name of the main App at this AppConfiguration
     * @return the created AppInfo object
     */
    public static AppInfo create(
            String fullAppName,
            String shortAppName )
    {
        return new AppInfo( fullAppName, shortAppName );
    }

    /**
     * Constructor.
     * @param fullAppName the full name of the main App at this AppConfiguration
     * @param shortAppName the short name of the main App at this AppConfiguration
     */
    protected AppInfo(
            String fullAppName,
            String shortAppName )
    {
        theFullAppName  = fullAppName;
        theShortAppName = shortAppName;
    }

    /**
     * Obtain the full name of the main App at this AppConfiguration.
     *
     * @return the name
     */
    public String getFullAppName()
    {
        return theFullAppName;
    }

    /**
     * Obtain the short name of the main App at this AppConfiguration.
     *
     * @return the name
     */
    public String getShortAppName()
    {
        return theShortAppName;
    }

    /**
     * The full name of the main App at this AppConfiguration.
     */
    protected final String theFullAppName;

    /**
     * The short name of the main App at this AppConfiguration.
     */
    protected final String theShortAppName;
}
