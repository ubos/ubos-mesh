//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.daemon;

import java.util.ArrayList;
import net.ubos.bot.Bot;
import net.ubos.license.LicenseManager;
import net.ubos.license.PermitAllLicenseManager;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.text.StringRepresentationSchemeDirectory;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseNameServer;
import net.ubos.meshbase.m.MMeshBaseNameServer;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Globally known objects when running UBOS Mesh as a daemon.
 */
public abstract class Daemon
{
    /**
     * Keep this static only.
     */
    protected Daemon() {}

    /**
     * Obtain information about this AppConfiguration.
     *
     * @return the info
     */
    public static AppInfo getAppInfo()
    {
        return theAppInfo;
    }

    /**
     * Set information about this AppConfiguration.
     *
     * @param info the info
     */
    public static void setAppInfo(
            AppInfo info )
    {
        theAppInfo = info;
    }

    /**
     * Get the main MeshBase name server.
     *
     * @return the name server
     */
    public static MeshBaseNameServer getMeshBaseNameServer()
    {
        return theMeshBaseNameServer;
    }

    /**
     * Enables the Module that defines the primary MeshBase tells us what it is.
     *
     * @param mainMb the main MeshBase
     */
    public static void setMainMeshBase(
            MeshBase mainMb )
    {
        theMeshBaseNameServer.setDefaultMeshBase( mainMb );
    }

    /**
     * Get the primary MeshObjectIdentifierNamespaceMap.
     *
     * @return the map
     */
    public static PrimaryMeshObjectIdentifierNamespaceMap getPrimaryNamespaceMap()
    {
        return theMeshBaseNameServer.getDefaultMeshBase().getPrimaryNamespaceMap();
    }

    /**
     * Determine the configured StringRepresentationSchemeDirectory.
     *
     * @return the StringRepresentationSchemeDirectory
     */
    public static StringRepresentationSchemeDirectory getStringRepresentationSchemeDirectory()
    {
        return theStringRepresentationSchemeDirectory;
    }

    /**
     * Register a bot.
     *
     * @param toAdd the new bot
     */
    public static void registerBot(
            Bot toAdd )
    {
        theBots.add( toAdd );
    }

    /**
     * Unregister a bot.
     *
     * @param toRemove the old bot
     */
    public static void unregisterBot(
            Bot toRemove )
    {
        theBots.remove( toRemove );
    }

    /**
     * Obtain an iterator over all registered bots.
     *
     * @return the iterator
     */
    public static CursorIterator<Bot> botIterator()
    {
        return ArrayListCursorIterator.create( theBots );
    }

    /**
     * Obtain the currently set LicenseManager.
     *
     * @return the LicenseManager
     */
    public static LicenseManager getLicenseManager()
    {
        return theLicenseManager;
    }

    /**
     * Set a new LicenseManager.
     *
     * @param newValue the new LicenseManager
     */
    public static void setLicenseManager(
            LicenseManager newValue )
    {
        if( newValue == null ) {
            throw new NullPointerException( "Null LicenseManager not permitted" );
        }
        theLicenseManager = newValue;
    }

    /**
     * Info about the App.
     */
    protected static AppInfo theAppInfo;

    /**
     * The name server for finding MeshBases.
     */
    protected static MMeshBaseNameServer theMeshBaseNameServer = MMeshBaseNameServer.create();

    /**
     * Directory of string representations.
     */
    protected static StringRepresentationSchemeDirectory theStringRepresentationSchemeDirectory;

    /**
     * Manages software licenses.
     */
    protected static LicenseManager theLicenseManager = PermitAllLicenseManager.create(); // FIXME needs different default

    /**
     * The Set of known bots.
     */
    protected static final ArrayList<Bot> theBots = new ArrayList<>();
}
