//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.license;

import net.ubos.mesh.security.ThreadIdentityManager;

/**
 * A LicenseManager that permits use of all packages by anybody who is authenticated. Denies use to all
 * anonymous users.
 */
public class PermitAuthenticatedAllLicenseManager
    implements
        LicenseManager
{
    /**
     * Factory method for consistency with other managers
     *
     * @return the created object
     */
    public static PermitAuthenticatedAllLicenseManager create()
    {
        return new PermitAuthenticatedAllLicenseManager();
    }

    /**
     * Private constructor, use factory method,
     */
    protected PermitAuthenticatedAllLicenseManager() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPermittedToUsePackage(
            String packageName )
    {
        boolean ret = ThreadIdentityManager.getWho() != null;
        return ret;
    }
}
