//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.license;

/**
 * This interface is supported by classes that know how to evaluate whether a current caller is permitted
 * to use the code and resources in a certain package.
 */
public interface LicenseManager
{
    /**
     * Convenience method to determine whether the current caller is permitted use of the named package.
     *
     * @param packageName name of the package
     * @return true or false
     */
    public boolean isPermittedToUsePackage(
            String packageName );
}
