//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.license;

/**
 * A LicenseManager that permits use of all packages by anybody.
 */
public class PermitAllLicenseManager
    implements
        LicenseManager
{
    /**
     * Factory method for consistency with other managers
     *
     * @return the created object
     */
    public static PermitAllLicenseManager create()
    {
        return new PermitAllLicenseManager();
    }

    /**
     * Private constructor, use factory method,
     */
    protected PermitAllLicenseManager() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPermittedToUsePackage(
            String packageName )
    {
        return true;
    }
}
