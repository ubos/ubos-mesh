//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot;

import net.ubos.meshbase.transaction.ChangeList;

/**
 * This type is supported by all objects that act as bots on UBOS Mesh.
 */
public interface Bot
{
    /**
     * Notify the bot that a set of changes has been made to a MeshBase.
     *
     * @param changes the changes
     */
    public void notifyChanges(
            ChangeList changes );
}
