//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.bot;

import java.util.LinkedList;
import net.ubos.daemon.Daemon;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.quit.QuitListener;
import net.ubos.meshbase.transaction.TransactionListener;

/**
 * The BotRunner subscribes to Transaction events of a MeshBase, and then invokes
 * the Bots registered with the Daemon.
 */
public class BotRunner
    extends
        Thread
    implements
        TransactionListener,
        QuitListener
{
    private static final Log log = Log.getLogInstance( BotRunner.class );

    /**
     * Constructor.
     */
    public BotRunner()
    {
        super( BotRunner.class.getSimpleName() );

        isDead = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run()
    {
        while( !isDead ) {
            ChangeList toWorkOn = null;

            try {
                synchronized( this ) {
                    if( !theQueue.isEmpty() ) {
                        toWorkOn = theQueue.removeFirst();
                        notify();
                    } else {
                        wait();
                    }
                }
            } catch( InterruptedException ex ) {
                // no op
            }

            if( toWorkOn != null ) {
                CursorIterator<Bot> botIter = Daemon.botIterator();
                while( botIter.hasNext() ) {
                    Bot bot = botIter.next();

                    try {
                        log.info( "Notifying Bot of", toWorkOn.size(), "changes:", bot.getClass().getName() );
                        bot.notifyChanges( toWorkOn );
                        log.info( "Done notifying Bot of changes:", bot.getClass().getName() );

                    } catch( Throwable t ) {
                        log.error( t );
                    }
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void transactionCommitted(
            HeadTransaction tx )
    {
        theQueue.add( tx.getChangeList() );
        notify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepareForQuit()
    {
        isDead = true;
        // clean up properly -- FIXME
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void die()
    {
        // clean up properly -- FIXME
    }

    /**
     * Flag indicating when to stop processing.
     */
    protected boolean isDead;

    /**
     * The queue of incoming ChangeLists not yet processed by the Bots.
     */
    protected final LinkedList<ChangeList> theQueue = new LinkedList<>();
}
