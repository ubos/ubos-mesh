//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.util.exception.AbstractLocalizedRuntimeException;

/**
 * Thrown if an operation is executed on MeshObjects that are contained
 * in different MeshBaseViews. For example, it is thrown when a
 * MeshObject from the HEAD MeshBaseView is attempted to be put into the same MeshObjectSet
 * as one from a historical MeshBaseView.
 *
 * Inner subclasses distinguish whether it was different MeshBaseViews of the same
 * MeshBase, or of different MeshBases.
 */
public abstract class WrongMeshBaseViewException
        extends
            AbstractLocalizedRuntimeException
{
    /**
     * Checks that the two provided MeshBaseViews are compatible. Throws exception if not.
     * The MeshBaseViews are compatible if they are the same, or if one is a MeshBaseView
     * of the MeshBase provided as the other.
     *
     * @param one the first MeshBaseView
     * @param two the second MeshBaseView
     * @throws WrongMeshBaseViewException thrown if the MeshBaseViews are not compatible
     */
    public static void checkCompatible(
            MeshBaseView one,
            MeshBaseView two )
        throws
            WrongMeshBaseViewException
    {
        checkCompatible( one, two, null );
    }

    /**
     * Checks that the two provided MeshBaseViews are compatible. Throws exception if not.
     * The MeshBaseViews are compatible if they are the same, or if one is a MeshBaseView
     * of the MeshBase provided as the other.
     *
     * @param one the first MeshBaseView
     * @param two the second MeshBaseView
     * @param msg the message
     * @throws WrongMeshBaseViewException thrown if the MeshBaseViews are not compatible
     */
    public static void checkCompatible(
            MeshBaseView one,
            MeshBaseView two,
            String       msg )
        throws
            WrongMeshBaseViewException
    {
        if( one == null ) {
            return;
        }
        if( two == null ) {
            return;
        }
        if( one instanceof MeshBase ) {
            if( two instanceof MeshBase ) {
                if( one != two ) {
                    throw new DifferentMeshBases( one, two, msg );
                }
            } else if( one != two.getMeshBase() ) {
                throw new DifferentMeshBases( one, two, msg );
            }
        } else if( two instanceof MeshBase ) {
            if( one.getMeshBase() != two ) {
                throw new DifferentMeshBases( one, two, msg );
            }
        } else if( one != two ) {
            throw new SameMeshBase( one, two, msg );
        }
    }

    /**
     * Factory method that instantiates the right subclass.
     *
     * @param one the first MeshBaseView
     * @param two the second, different, MeshBaseView
     * @return the created WrongMeshBaseViewException
     */
    public static WrongMeshBaseViewException create(
            MeshBaseView one,
            MeshBaseView two )
    {
        return create( one, two, null );
    }

    /**
     * Factory method that instantiates the right subclass.
     *
     * @param one the first MeshBaseView
     * @param two the second, different, MeshBaseView
     * @param msg the message
     * @return the created WrongMeshBaseViewException
     */
    public static WrongMeshBaseViewException create(
            MeshBaseView one,
            MeshBaseView two,
            String       msg )
    {
        if( one.getMeshBase() == two.getMeshBase() ) {
            return new SameMeshBase( one, two, null );
        } else {
            return new DifferentMeshBases( one, two, null );
        }
    }

    /**
     * Constructor.
     *
     * @param one the first MeshBaseView
     * @param two the second, different, MeshBaseView
     * @param msg the message
     */
    protected WrongMeshBaseViewException(
            MeshBaseView one,
            MeshBaseView two,
            String       msg )
    {
        super( msg );

        theOne = one;
        theTwo = two;
    }

    /**
     * Obtain the first MeshBaseView.
     *
     * @return the first MeshBaseView
     */
    public MeshBaseView getFirstMeshBaseView()
    {
        return theOne;
    }

    /**
     * Obtain the second MeshBaseView.
     *
     * @return the second MeshBaseView
     */
    public MeshBaseView getSecondMeshBaseView()
    {
        return theTwo;
    }

    /**
     * Obtain parameters for the internationalization.
     *
     * @return the parameters
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theOne, theTwo };
    }

    /**
     * The first MeshBaseView.
     */
    protected transient MeshBaseView theOne;

    /**
     * The second MeshBaseView.
     */
    protected MeshBaseView theTwo;

    /**
     * Indicates that the different MeshBaseViews were nevertheless MeshBaseViews of the same MeshBase.
     */
    public static class SameMeshBase
        extends
            WrongMeshBaseViewException
    {
        /**
         * Constructor.
         *
         * @param one the first MeshBaseView
         * @param two the second, different, MeshBaseView
         * @param msg the message
         */
        protected SameMeshBase(
                MeshBaseView one,
                MeshBaseView two,
                String       msg )
        {
            super( one, two, msg );
        }
    }

    /**
     * Indicates that the different MeshBaseViews were also MeshBaseViews of different MeshBases.
     */
    public static class DifferentMeshBases
        extends
            WrongMeshBaseViewException
    {
        /**
         * Constructor.
         *
         * @param one the first MeshBaseView
         * @param two the second, different, MeshBaseView
         * @param msg the message
         */
        protected DifferentMeshBases(
                MeshBaseView one,
                MeshBaseView two,
                String       msg )
        {
            super( one, two, msg );
        }
    }
}
