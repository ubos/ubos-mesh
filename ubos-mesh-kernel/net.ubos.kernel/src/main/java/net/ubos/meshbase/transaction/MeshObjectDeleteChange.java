//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectDeleteChange;

/**
 * This event indicates that a MeshObject was semantically deleted.
 */
public class MeshObjectDeleteChange
        extends
            AbstractMeshObjectLifecycleChange
{
    /**
     * Constructor.
     *
     * @param changedObj the MeshObject whose lifecycle changed
     * @param changedObjId the identifier of the MeshObject whose lifecycle changed
     */
    public MeshObjectDeleteChange(
            MeshObject           changedObj,
            MeshObjectIdentifier changedObjId )
    {
        super( changedObj, changedObjId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject otherObject = base.findMeshObjectByIdentifier( theChangedObjectIdentifier );

        base.getMeshBase().deleteMeshObject( otherObject );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectDeleteChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject = mbv.findMeshObjectByIdentifier( theChangedObjectIdentifier );

        return new MeshObjectDeleteChange(
                updatedMeshObject,
                theChangedObjectIdentifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectCreateChange inverse()
    {
        return new MeshObjectCreateChange(
                theChangedObject,
                theChangedObjectIdentifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectCreateChange )) {
            return false;
        }
        MeshObjectCreateChange realCandidate = (MeshObjectCreateChange) candidate;

        if( !theChangedObjectIdentifier.equals( realCandidate.theChangedObjectIdentifier )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectDeleteChange )) {
            return false;
        }
        MeshObjectDeleteChange realOther = (MeshObjectDeleteChange) other;

        if( !theChangedObjectIdentifier.equals( realOther.theChangedObjectIdentifier )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theChangedObjectIdentifier.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectDeleteChange asExternalized()
    {
        return new ExternalizedMeshObjectDeleteChange(
                getChangedMeshObjectIdentifier() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectDeleteChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectDeleteChange(
                translation.get( theChangedObject.getIdentifier() ),
                theChangedObjectIdentifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        for( int i=index-1 ; i>=0 ; --i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof MeshObjectCreateChange )) {
                continue;
            }
            // can eliminate both
            list.blankChangeAt( i );
            list.blankChangeAt( index );
            break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theChangedObjectIdentifier );
        return ret.toString();
    }
}
