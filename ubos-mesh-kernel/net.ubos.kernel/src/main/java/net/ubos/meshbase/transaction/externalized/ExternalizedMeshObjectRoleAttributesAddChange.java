//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributesAddChange;
import net.ubos.meshbase.transaction.TransactionException;

/**
 *
 */
public class ExternalizedMeshObjectRoleAttributesAddChange
    extends
        AbstractExternalizedMeshObjectRoleAttributesChange
{
    public ExternalizedMeshObjectRoleAttributesAddChange(
            MeshObjectIdentifier sourceIdentifier,
            String []            oldValues,
            String []            deltaValues,
            String []            newValues,
            MeshObjectIdentifier neighborIdentifier )
    {
        super( sourceIdentifier, oldValues, deltaValues, newValues, neighborIdentifier );
    }

    public String [] getAdded()
    {
        return theDeltaValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject source   = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );
            MeshObject neighbor = base.findMeshObjectByIdentifierOrThrow( theNeighborIdentifier );

            source.setRoleAttributeValues( neighbor, theDeltaValues, new Serializable[ theDeltaValues.length ] );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributesAddChange internalizeWith(
            MeshObject [] affectedMeshObjects )
    {
        if( affectedMeshObjects.length != 2 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectRoleAttributesAddChange( affectedMeshObjects[0], theOldValues, theDeltaValues, theNewValues, affectedMeshObjects[1] );
    }
}
