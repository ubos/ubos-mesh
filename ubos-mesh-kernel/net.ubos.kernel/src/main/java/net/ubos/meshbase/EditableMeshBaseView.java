//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.text.ParseException;
import net.ubos.mesh.IsAbstractException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;

/**
 * A MeshBaseView that is also editable.
 */
public interface EditableMeshBaseView
    extends
        MeshBaseView
{
    /**
     * Helper to check that we are on a Transaction, and it is the right Transaction.
     *
     * @return the current Transaction
     * @throws TransactionException throw if this has been invoked outside of proper Transaction boundaries
     */
    public abstract Transaction checkTransaction()
        throws
            TransactionException;

    /**
     * <p>Create a new MeshObject without a type  and an automatically created MeshObjectIdentifier.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @return the created MeshObject
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObject()
        throws
            TransactionException,
            NotPermittedException;

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and an automatically created MeshObjectIdentifier.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param type the EntityType with which the MeshObject will be blessed
     * @return the created MeshObject
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObject(
            EntityType type )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>Create a new MeshObject without a type and with a provided MeshObjectIdentifier.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is to be created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @return the created MeshObject
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObject(
            MeshObjectIdentifier identifier )
        throws
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and a provided MeshObjectIdentifier.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @param type the EntityType with which the MeshObject will be blessed
     * @return the created MeshObject
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObject(
            MeshObjectIdentifier identifier,
            EntityType           type )
        throws
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>Create a new MeshObject without a type and with a provided MeshObjectIdentifier.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is to be created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public default MeshObject createMeshObject(
            String identifier )
        throws
            ParseException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( getMeshBase().meshObjectIdentifierFromExternalForm( identifier ));
    }

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and a provided MeshObjectIdentifier.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @param type the EntityType with which the MeshObject will be blessed
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public default MeshObject createMeshObject(
            String     identifier,
            EntityType type )
        throws
            ParseException,
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( getMeshBase().meshObjectIdentifierFromExternalForm( identifier ), type );
    }

    /**
     * <p>Create a new MeshObject without a type and an automatically created MeshObjectIdentifier
     * that is in the same name space and "below" the MeshObjectIdentifier of another MeshObject, by
     * appending a trailing String.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param base the base MeshObject
     * @return the created MeshObject
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject base )
        throws
            TransactionException,
            NotPermittedException;

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and an automatically created MeshObjectIdentifier that is in the same name space and "below"
     * the MeshObjectIdentifier of another MeshObject, by appending a trailing String.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param base the base MeshObject
     * @param type the EntityType with which the MeshObject will be blessed
     * @return the created MeshObject
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject base,
            EntityType type )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>Create a new MeshObject without a type and with a MeshObjectIdentifier that is in the same name space
     * and "below" the MeshObjectIdentifier of another MeshObject, by appending a trailing String.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is to be created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param base the base MeshObject
     * @param trailingId the string to be appended to the MeshObjectIdentifier of the base MeshObject
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject base,
            String     trailingId )
        throws
            ParseException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and a MeshObjectIdentifier that is in the same name space
     * and "below" the MeshObjectIdentifier of another MeshObject, by appending a trailing String.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param base the base MeshObject
     * @param trailingId the string to be appended to the MeshObjectIdentifier of the base MeshObject
     * @param type the EntityType with which the MeshObject will be blessed
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject base,
            String     trailingId,
            EntityType type )
        throws
            ParseException,
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>Semantically delete a MeshObject.
     *
     * <p>This call is a "semantic delete", which means that an existing
     * MeshObject will go away in all its replicas. Due to time lag, the MeshObject
     * may still exist in certain replicas in other places for a while, but
     * the request to deleteMeshObjects all objects is in the queue and will get there
     * eventually.
     *
     * @param theObject the MeshObject to be semantically deleted
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void deleteMeshObject(
            MeshObject theObject )
        throws
            TransactionException,
            NotPermittedException;

    /**
     * <p>Semantically delete several MeshObjects at the same time.
     *
     * <p>This call is a "semantic delete", which means that an existing
     * MeshObject will go away in all its replicas. Due to time lag, the MeshObject
     * may still exist in certain replicas in other places for a while, but
     * the request to deleteMeshObjects all objects is in the queue and will get there
     * eventually.
     *
     * <p>This call either succeeds or fails in total: if one or more of the specified MeshObject cannot be
     *    deleted for some reason, none of the other MeshObjects will be deleted either.
     *
     * @param theObjects the MeshObjects to be semantically deleted
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void deleteMeshObjects(
            MeshObject [] theObjects )
        throws
            TransactionException,
            NotPermittedException;

    /**
     * <p>Semantically delete all MeshObjects in a MeshObjectSet at the same time.
     *
     * <p>This call is a "semantic delete", which means that an existing
     * MeshObject will go away in all its replicas. Due to time lag, the MeshObject
     * may still exist in certain replicas in other places for a while, but
     * the request to deleteMeshObjects all objects is in the queue and will get there
     * eventually.
     *
     * <p>This call either succeeds or fails in total: if one or more of the specified MeshObject cannot be
     *    deleted for some reason, none of the other MeshObjects will be deleted either.
     *
     * @param theSet the set of MeshObjects to be semantically deleted
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void deleteMeshObjects(
            MeshObjectSet theSet )
        throws
            TransactionException,
            NotPermittedException;
}
