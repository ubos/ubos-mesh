//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.history;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * A MeshBaseView that represents a point in the history of the MeshBase, and not the HEAD.
 */
public interface HistoricMeshBaseView
    extends
        MeshBaseView
{
    /**
     * Obtain the MeshBaseState at which this MeshBaseView was current.
     *
     * @return theMeshBaseState
     */
    public abstract MeshBaseState getMeshBaseState();

    /**
     * Convenience method.
     *
     * @return the time this MeshBaseView was updated.
     */
    public abstract long getTimeUpdated();

    /**
     * Obtain an Iterator over all MeshObjects that were changed in the Transaction that led
     * to this MeshBaseView.
     *
     * @return the Iterator
     */
    public abstract CursorIterator<MeshObject> changedMeshObjectsIterator();

    /**
     * Purge all MeshObjects in this HistoricMeshBaseView that are mere clones (without changes) Of
     * previously changed MeshObjects. This saves memory, but also enables removes now potentially
     * inconsistent MeshObjects when a HistoryTransaction is committed.
     */
    public abstract void purgeClonedMeshObjectsCache();
}
