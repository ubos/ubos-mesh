//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.security;

import java.util.function.BiPredicate;
import java.util.function.Predicate;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;

/**
 * A CursorIterator that filters the returned MeshObjects by the access that the caller has
 * per the provided AccessManager.
 */
public class ByAccessFilteringCursorIterator
    extends
        FilteringCursorIterator<MeshObject>
{
    /**
     * Factory method.
     *
     * @param delegate the delegate CursorIterator
     * @param accessMgr the AccessManager that determines the access rules
     * @return the created FilteringCursorIterator
     */
    public static ByAccessFilteringCursorIterator create(
            CursorIterator<MeshObject> delegate,
            AccessManager              accessMgr )
    {
        Predicate<MeshObject> filter = (MeshObject obj) -> {
            try {
                accessMgr.checkPermittedAccess( obj );
                return true;

            } catch( NotPermittedException ex ) {
                return false;
            }
        };
        return new ByAccessFilteringCursorIterator(
                delegate,
                filter,
                new DEFAULT_EQUALS<>() );
    }
    /**
     * Constructor.
     *
     * @param delegate the delegate CursorIterator
     * @param filter the Predicate to use that determines which elements from the underlying CursorIterator to select
     * @param equals the equality operation for E's
     */
    protected ByAccessFilteringCursorIterator(
            CursorIterator<MeshObject>         delegate,
            Predicate<MeshObject>              filter,
            BiPredicate<MeshObject,MeshObject> equals )
    {
        super( delegate, filter, equals );
    }
}
