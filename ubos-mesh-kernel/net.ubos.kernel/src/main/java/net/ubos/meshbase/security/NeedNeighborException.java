//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.security;

import net.ubos.mesh.MeshObjectIdentifier;

/**
 * This is an internal exception that is thrown if the AccessManager was consulted
 * with a neighbor MeshObjectIdentifier only, but the AccessManager could not
 * provide the answer without the neighbor MeshObject itself.
 * 
 * If this exception is thrown, it turns permission check into a two-stage process,
 * but with the benefit that for many AccessManager implementations, the neighbor
 * MeshObject does not need to be retrieved from disk.
 */
public class NeedNeighborException
    extends
        RuntimeException
{
    /**
     * Constructor.
     * 
     * @param neighborId the MeshObjectIdentifier of the neighbor MeshObject that needed to be provided
     */
    public NeedNeighborException(
            MeshObjectIdentifier neighborId )
    {
        theNeighborId = neighborId;
    }
    
    /**
     * The MeshObjectIdentifier of the neighbor MeshObject that needed to be provided.
     */
    protected final MeshObjectIdentifier theNeighborId;
}
