//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.a;

import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.MeshBaseStateHistory;

/**
 * Adds a smart factory method to MeshBaseStateHistory, so we can load and restore
 * histories.
 */
public interface AMeshBaseStateHistory
    extends
        MeshBaseStateHistory
{
    /**
     * Obtain the element that was recorded at exactly the provided time. If it does
     * not exist yet, create it.
     *
     * @param t the time
     * @return the element at that time, or null if nothing was recorded at that time
     */
    public MeshBaseState obtainAt(
            long t );
}
