//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

/**
 * A Transaction that modifies the head version of the graph.
 */
public interface HeadTransaction
        extends
            Transaction
{
}
