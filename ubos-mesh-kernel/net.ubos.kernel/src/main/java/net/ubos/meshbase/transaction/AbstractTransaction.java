//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.AbstractMeshBase;
import net.ubos.meshbase.AbstractMeshBaseView;
import net.ubos.util.logging.Log;

/**
 * Functionality common to Transaction implementations.
 */
public abstract class AbstractTransaction
    implements
        Transaction
{
    private static final Log log = Log.getLogInstance( AbstractTransaction.class); // our own, private logger

    /**
     * Private constructor for subclasses only
     *
     * @param mbv the MeshBaseView that the Transaction guards
     */
    protected AbstractTransaction(
            AbstractEditableMeshBaseView mbv )
    {
        theMeshBaseView = mbv;

        myThread       = Thread.currentThread();
        theTimeStarted = System.currentTimeMillis();

        theStatus = Status.TRANSACTION_STARTED;

        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "constructor" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeshBase getMeshBase()
    {
        return theMeshBaseView.getMeshBase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeshBaseView getMeshBaseView()
    {
        return theMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean owns(
            Thread t )
    {
        return myThread == t;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Status getStatus()
    {
        return theStatus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeStarted()
    {
        return theTimeStarted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeEnded()
    {
        return theTimeEnded;
    }

    /**
      * The Thread on which this Transaction was started and on which it
      * needs to be executed.
      */
    protected final Thread myThread;

    /**
      * The current status of this Transaction.
      */
    protected Status theStatus;

    /**
      * The MeshBaseView on which this Transaction is executed. This may be either a
      * HistoryMeshBaseView or a HeadMeshBaseView.
      */
    protected final AbstractEditableMeshBaseView theMeshBaseView;

    /**
     * The time the transaction was started.
     */
    protected final long theTimeStarted;

    /**
     * The time the transaction was committed or rolled back.
     */
    protected long theTimeEnded = -1L;
}
