//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributesRemoveChange;
import net.ubos.util.ArrayHelper;

/**
 * <p>This event indicates that at least one Attribute was removed from a MeshObject.
 */
public class MeshObjectAttributesRemoveChange
        extends
            AbstractMeshObjectAttributesChange
{
    /**
     * Pass-through constructor for subclasses.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old values of the names of the Attributes, prior to the event
     * @param deltaValues the names of the Attributes that changed
     * @param newValues the new values of the names of the Attributes, after the event
     */
    public MeshObjectAttributesRemoveChange(
            MeshObject source,
            String []  oldValues,
            String []  deltaValues,
            String []  newValues )
    {
        super(  source,
                oldValues,
                deltaValues,
                newValues );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject otherObject    = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        String []  attributeNames = getDeltaValue();

        otherObject.deleteAttributes( attributeNames );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesRemoveChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject   = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        String []  oldAttributeNames   = updatedMeshObject.getAttributeNames();
        String []  deltaAttributeNames = theDeltaValue;
        String []  newAttributeNames   = ArrayHelper.remove( oldAttributeNames, deltaAttributeNames, true, String.class );

        return new MeshObjectAttributesRemoveChange(
                updatedMeshObject,
                oldAttributeNames,
                deltaAttributeNames,
                newAttributeNames );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesAddChange inverse()
    {
        return new MeshObjectAttributesAddChange(
                getSource(),
                getNewValue(),
                getDeltaValue(),
                getOldValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectAttributesAddChange )) {
            return false;
        }

        MeshObjectAttributesAddChange realCandidate = (MeshObjectAttributesAddChange) candidate;
        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realCandidate.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectAttributesRemoveChange )) {
            return false;
        }
        MeshObjectAttributesRemoveChange realOther = (MeshObjectAttributesRemoveChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realOther.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = theSource.getIdentifier().hashCode();
        for( String name : theDeltaValue ) {
            ret ^= name.hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectAttributesRemoveChange asExternalized()
    {
        return new ExternalizedMeshObjectAttributesRemoveChange(
                getSource().getIdentifier(),
                getOldValue(),
                getDeltaValue(),
                getNewValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesRemoveChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectAttributesRemoveChange(
                translation.get( theSource.getIdentifier() ),
                theOldValue,
                theDeltaValue,
                theNewValue );
    }
//
//    public void blankIneffectiveFromList(
//            ChangeList.SubjectChangeList list,
//            int                          index )
//    {
//        list.blankIneffective(
//                index,
//                ( Change candidate ) -> {
//
//                } );
//    }
}
