//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributeChange;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.event.PropertyChange;

/**
  * <p>This event indicates that one of the Attributes of a MeshObject has changed its value.
  */
public class MeshObjectAttributeChange
        extends
            PropertyChange<MeshObject,String,Serializable>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the object that is the source of the event
     * @param attributeName the name of the Attribute
     * @param oldValue the old value of the Attribute, prior to the event
     * @param newValue the new value of the Attribute, after the event
     */
    public MeshObjectAttributeChange(
            MeshObject   source,
            String       attributeName,
            Serializable oldValue,
            Serializable newValue )
    {
        super(  source,
                attributeName,
                oldValue,
                newValue,
                newValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject[] { getSource() };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject otherObject = base.findMeshObjectByIdentifier( getSource().getIdentifier() );

        String       attributeName = getProperty();
        Serializable newValue      = getNewValue();

        otherObject.setAttributeValue(
                attributeName,
                newValue );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributeChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        String     attributeName     = getProperty();

        return new MeshObjectAttributeChange(
                updatedMeshObject,
                attributeName,
                updatedMeshObject.getAttributeValue( attributeName ),
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributeChange inverse()
    {
        return new MeshObjectAttributeChange(
                getSource(),
                getProperty(),
                getNewValue(),
                getOldValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectAttributeChange )) {
            return false;
        }
        MeshObjectAttributeChange realCandidate = (MeshObjectAttributeChange) candidate;

        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }
        if( ! theProperty.equals( realCandidate.theProperty )) {
            return false;
        }

        Object one = getOldValue();
        Object two = realCandidate.getNewValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        one = getNewValue();
        two = realCandidate.getOldValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectAttributeChange )) {
            return false;
        }
        MeshObjectAttributeChange realOther = (MeshObjectAttributeChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !theProperty.equals( realOther.theProperty )) {
            return false;
        }
        if( !ArrayHelper.equals( theNewValue, realOther.theNewValue )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(
                theSource.getIdentifier(),
                theProperty,
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectAttributeChange asExternalized()
    {
        return new ExternalizedMeshObjectAttributeChange(
                getSource().getIdentifier(),
                getProperty(),
                getOldValue(),
                getNewValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributeChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectAttributeChange(
                translation.get( theSource.getIdentifier() ),
                theProperty,
                theOldValue,
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        for( int i=index-1 ; i>=0 ; --i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof MeshObjectAttributeChange )) {
                continue;
            }
            MeshObjectAttributeChange realCurrentChange = (MeshObjectAttributeChange) currentChange;
            if( !theProperty.equals( realCurrentChange.getProperty())) {
                continue;
            }

            if( Objects.equals( realCurrentChange.theOldValue, theNewValue )) {
                // can eliminate both
                list.blankChangeAt( i );
                list.blankChangeAt( index );

            } else {
                // can simplify -- we blank ourselves and replace the other
                Change newCurrentChange = new MeshObjectAttributeChange( theSource, theProperty, realCurrentChange.theOldValue, theNewValue );

                list.replaceChangeAt( i, newCurrentChange);
                list.blankChangeAt( index );
            }
            break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( ": " );
        ret.append( theProperty );
        ret.append( " = " );
        ret.append( theOldValue );
        ret.append( " >> " );
        ret.append( theNewValue );
        return ret.toString();
    }
}
