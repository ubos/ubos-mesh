//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import net.ubos.meshbase.externalized.MeshBaseTransactionEncoder;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;

/**
 *
 */
public final class MeshBaseTransactionJsonEncoder
    extends
        AbstractMeshBaseTransactionJsonEncoder
    implements
        MeshBaseTransactionEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static MeshBaseTransactionJsonEncoder create()
    {
        return new MeshBaseTransactionJsonEncoder( DefaultMMeshTypeIdentifierBothSerializer.SINGLETON );
    }

    /**
     * Constructor, use factory method.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected MeshBaseTransactionJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        super( typeIdSerializer );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_BASE_TRANSACTION_JSON_ENCODING_ID;
    }
}
