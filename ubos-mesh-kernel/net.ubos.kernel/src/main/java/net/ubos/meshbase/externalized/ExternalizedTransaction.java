//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.util.List;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;

/**
 * Representation of a successful Transaction that can be easily serialized and deserialized.
 */
public interface ExternalizedTransaction
{
    /**
     * Obtain the time the Transaction was committed.
     *
     * @return the time
     */
    public long getTimeUpdated();

    /**
     * Obtain the Changes made during the Transaction.
     *
     * @return the Changes
     */
    public List<ExternalizedChange> getChanges();
}
