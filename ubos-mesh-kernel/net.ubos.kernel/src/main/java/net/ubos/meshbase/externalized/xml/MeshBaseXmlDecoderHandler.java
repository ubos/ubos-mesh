//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.mesh.externalized.xml.MeshObjectXmlDecoderHandler;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.StringValue;
import net.ubos.modelbase.externalized.ExternalizedExternalName;
import net.ubos.modelbase.externalized.ExternalizedMeshObjectIdentifierNamespace;
import net.ubos.modelbase.externalized.ExternalizedSubjectAreaDependency;
import net.ubos.model.primitives.externalized.xml.UnknownTokenException;
import net.ubos.util.logging.Log;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * Factored-out SAX callback handler.
 */
public class MeshBaseXmlDecoderHandler
    extends
        MeshObjectXmlDecoderHandler
    implements
        MeshBaseXmlTags
{
    private static final Log log = Log.getLogInstance(MeshBaseXmlDecoderHandler.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @param mb the MeshBase we import into
     */
    public MeshBaseXmlDecoderHandler(
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
    {
        super( idDeserializer );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startElement2(
            String     namespaceURI,
            String     localName,
            String     qName,
            Attributes attrs )
        throws
            SAXException
    {
        switch( qName ) {
            case UBOS_MESH_TAG:
                // noop
                break;

            case HEADER_TAG:
                // noop
                break;

            case FORMAT_TAG:
                // FIXME
                break;

            case VERSION_TAG:
                // FIXME
                break;

            case MESHOBJECTIDENTIFIERNAMESPACES_TAG:
                // noop
                break;

            case MESHOBJECTIDENTIFIERNAMESPACE_TAG:
                theNamespace = new ExternalizedMeshObjectIdentifierNamespace();
                theNamespace.setLocalName( attrs.getValue( IDENTIFIER_TAG ));
                break;

            case EXTERNALNAME_TAG:
                theExternalName = new ExternalizedExternalName();
                theExternalName.setPreferred( EXTERNALNAME_PREFERRED_TRUE_TAG_VALUE.equals( attrs.getValue( EXTERNALNAME_PREFERRED_TAG )));
                break;

            case SUBJECTAREAS_TAG:
                // noop
                break;

            case SUBJECTAREA_TAG:
                theSubjectAreaDependency = new ExternalizedSubjectAreaDependency();
                theSubjectAreaDependency.setName( StringValue.createOrNull( attrs.getValue( IDENTIFIER_TAG )));
                break;

            case BODY_TAG:
                // noop
                break;

            default:
                startElement3( namespaceURI, localName, qName, attrs );
                break;
        }
    }

    /**
     * Allows subclasses to add to parsing.
     *
     * @param namespaceURI URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @param attrs the Attributes at this element
     * @throws SAXException thrown if a parsing error occurrs
     */
    protected void startElement3(
            String     namespaceURI,
            String     localName,
            String     qName,
            Attributes attrs )
        throws
            SAXException
    {
        throw new UnknownTokenException( namespaceURI, qName, theLocator );
    }

    /**
     * Overrides parsing behavior of the superclass.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @throws SAXException thrown if a parsing error occurrs
     */
    @Override
    protected void endElement1(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        // Override to pull out "return value" of the superclass' endElement1
        switch( qName ) {
            case MESHOBJECT_TAG:
                theParsedExternalizedMeshObjects.add( theMeshObjectBeingParsed );
                theMeshObjectBeingParsed = null;
                break;

            default:
                super.endElement1( namespaceURI, localName, qName );
                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void endElement2(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        switch( qName ) {
            case UBOS_MESH_TAG:
                // noop
                break;

            case HEADER_TAG:
                // FIXME
                break;

            case FORMAT_TAG:
                // FIXME
                break;

            case VERSION_TAG:
                // FIXME
                break;

            case MESHOBJECTIDENTIFIERNAMESPACES_TAG:
                // noop
                break;

            case MESHOBJECTIDENTIFIERNAMESPACE_TAG:
                ((ImporterMeshObjectIdentifierDeserializer)theIdDeserializer).addNamespace( theNamespace );
                theNamespace = null;
                break;

            case EXTERNALNAME_TAG:
                theNamespace.addExternalName( theExternalName );
                theExternalName = null;
                break;

            case SUBJECTAREAS_TAG:
                // noop
                break;

            case SUBJECTAREA_TAG:
                theParsedSubjectAreaDependencies.add( theSubjectAreaDependency );
                theSubjectAreaDependency = null;
                break;

            case BODY_TAG:
                // noop
                break;

            default:
                endElement3( namespaceURI, localName, qName );
                break;
        }
    }

    /**
     * Allows subclasses to add to parsing.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @throws SAXException thrown if a parsing error occurrs
     */
    protected void endElement3(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        throw new UnknownTokenException( namespaceURI, qName, theLocator );
    }

    /**
     * Obtain the collection of MeshObjects that we have found during parsing.
     *
     * @return the collection
     */
    public final List<ParserFriendlyExternalizedMeshObject> getParsedExternalizedMeshObjects()
    {
        return theParsedExternalizedMeshObjects;
    }

    /**
     * Obtain the collection of SubjectAreaFerences that we have found during parsing.
     *
     * @return the collection
     */
    public final Collection<ExternalizedSubjectAreaDependency> getParsedSubjectAreaDependencies()
    {
        return theParsedSubjectAreaDependencies;
    }

    /**
     * The set of SubjectAreaReferences that we have foudn so far.
     */
    protected ArrayList<ExternalizedSubjectAreaDependency> theParsedSubjectAreaDependencies
            = new ArrayList<>();

    /**
     * The set of parsed ExternalizedMeshObjects that we have found so far.
     */
    protected ArrayList<ParserFriendlyExternalizedMeshObject> theParsedExternalizedMeshObjects
            = new ArrayList<>();

    /**
     * The SubjectAreaDependency currently being parsed.
     */
    protected ExternalizedSubjectAreaDependency theSubjectAreaDependency = null;

    /**
     * The MeshObjectIdentifierNamespace currently being parsed.
     */
    protected ExternalizedMeshObjectIdentifierNamespace theNamespace = null;

    /**
     * The ExternalName currently being parsed.
     */
    protected ExternalizedExternalName theExternalName = null;
}
