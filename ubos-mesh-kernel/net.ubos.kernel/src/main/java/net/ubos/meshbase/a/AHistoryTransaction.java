//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.a;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.meshbase.AbstractEditableHistoricMeshBaseView;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.meshbase.history.AbstractEditableHistoricMeshBaseState;
import net.ubos.meshbase.history.EditableHistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.ChangeUtils;
import net.ubos.meshbase.transaction.IllegalTransactionThreadException;
import net.ubos.meshbase.transaction.MeshObjectCreateChange;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
 * "A" implementaton of HistoryTransaction.
 */
public class AHistoryTransaction
    extends
        ATransaction
    implements
        HistoryTransaction,
        CanBeDumped
{
    private static final Log log = Log.getLogInstance( AHistoryTransaction.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView we work on
     */
    public AHistoryTransaction(
            AbstractEditableHistoricMeshBaseView mbv )
    {
        super( mbv );

        theActiveMeshBaseViewForThisTransaction = mbv; // where we start

        theChanges.put( mbv, AChangeList.create( mbv )); // always have this one
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addChange(
            Change toAdd )
    {
        ChangeList found = theChanges.get( theActiveMeshBaseViewForThisTransaction );
        if( found == null ) {
            found = AChangeList.create( theActiveMeshBaseViewForThisTransaction );
            theChanges.put( theActiveMeshBaseViewForThisTransaction, found );
        }
        found.addChange( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ChangeList getChangeList()
    {
        return theChanges.get( theMeshBaseView ); // always return the primary one, not the rippled ones
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractEditableMeshBaseView getDefaultMeshBaseViewForTransaction()
    {
        return theActiveMeshBaseViewForThisTransaction;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkTransaction(
            EditableMeshBaseView mbv )
        throws
            IllegalTransactionThreadException,
            WrongMeshBaseViewException
    {
        if( theStatus == Status.HISTORY_TRANSACTION_RIPPLING_FORWARD ) {
            // we can work on some other MeshBaseView of the same MeshBase
            if( theMeshBaseView.getMeshBase() != mbv.getMeshBase() ) {
                throw WrongMeshBaseViewException.create( theMeshBaseView, mbv );
            }
        } else {
            if( theMeshBaseView != mbv ) {
                throw WrongMeshBaseViewException.create( theMeshBaseView, mbv );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkTransaction(
            EditableHistoricMeshBaseView mbv )
        throws
            IllegalTransactionThreadException,
            WrongMeshBaseViewException
    {
        if( !owns( Thread.currentThread() )) {
            throw new IllegalTransactionThreadException( mbv, this );
        }
        if( theStatus == Status.HISTORY_TRANSACTION_RIPPLING_FORWARD ) {
            return;
        }
        if( theMeshBaseView != mbv ) {
            throw WrongMeshBaseViewException.create( theMeshBaseView, mbv );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getWhen()
    {
        return getMeshBaseView().getMeshBaseState().getTimeUpdated();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractEditableHistoricMeshBaseView getMeshBaseView()
    {
        return (AbstractEditableHistoricMeshBaseView) theMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getEffectiveCommitTime()
    {
        return getWhen();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commitTransaction(
            boolean normalizeChangeList )
    {
        if( theStatus != Status.TRANSACTION_STARTED ) {
            log.error( "Illegal state for transaction:", theStatus );
        }

        ThreadIdentityManager.sudo();

        try {
            if( theChanges.size() != 1 ) {
                throw new IllegalStateException( "Did not expect more than 1 ChangeSet here" );
            }
            for( ChangeList changeList : theChanges.values() ) { // not quite a loop, but easiest to write this way
                if( normalizeChangeList ) {
                    changeList.normalize(); // try not to create unnecessary work for rippleForward
                }
                checkValidGraph( changeList );
                updateMeshObjectTimestamps( changeList, getWhen() );

                putIntoMeshObjectHistories( changeList );
            }

            theStatus    = Status.HISTORY_TRANSACTION_RIPPLING_FORWARD;
            rippleForward( normalizeChangeList );

            for( ChangeList changeList : theChanges.values() ) {
                if( normalizeChangeList ) {
                    changeList.normalize(); // try not to create unnecessary work for rippleForward
                }
                checkValidGraph( changeList );
            }

            theStatus    = Status.TRANSACTION_COMMITTED;
            theTimeEnded = System.currentTimeMillis();

            for( var entry : theChanges.entrySet() ) {
                AbstractEditableMeshBaseView mbv = entry.getKey();
                if( !( mbv instanceof AbstractEditableHistoricMeshBaseView )) {
                    continue;
                }
                AbstractEditableHistoricMeshBaseView realMbv    = (AbstractEditableHistoricMeshBaseView) mbv;
                ChangeList                           changeList = entry.getValue();

                updateMeshObjectTimestamps( changeList, realMbv.getTimeUpdated() );
            }

        } finally {
            ThreadIdentityManager.sudone();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rollbackTransaction(
            Throwable t )
    {
        if( theStatus != Status.TRANSACTION_STARTED ) {
            log.error( "illegal state for transaction: ", theStatus );
        }
//
//        ThreadIdentityManager.sudo();
//
//        try {
//            // go backwards in the change set
//            ArrayList<Change> changes = theChangeList.getChanges();
//
//            for( int i=changes.size()-1 ; i>=0 ; --i ) {
//                Change current = changes.get( i );
//
//                try {
//                    Change inverted = current.inverse();
//
//                    if( inverted == null ) {
//                        log.error( "Could not invert change", current );
//                        continue;
//                    }
//
//                    inverted.applyTo( theMeshBaseView.getMeshBase() );
//
//                } catch( CannotApplyChangeException ex ) {
//                    Throwable cause = ex.getCause();
//
//                    if(    !( cause instanceof PropertyReadOnlyException )
//                        && !( cause instanceof IllegalPropertyValueException )
//                        && !( cause instanceof RoleTypeNotBlessedException )
//                        && !( cause instanceof RoleTypeBlessedAlreadyException ))
//                    {
//                        log.error( ex );
//                        // that's the best we can do
//                    }
//
//                } catch( MeshObjectGraphModificationException ex ) {
//                    log.error( ex );
//                    // that's the best we can do
//                } catch( TransactionException ex ) {
//                    log.error( ex );
//                    // that's the best we can do
//                } catch( Throwable ex ) {
//                    log.error( ex );
//                    // that's the best we can do
//                }
//            }
//
//            theStatus    = Status.TRANSACTION_ROLLEDBACK;
//            theTimeEnded = System.currentTimeMillis();
//
//        } finally {
//            ThreadIdentityManager.sudone();
//        }
    }

    protected void putIntoMeshObjectHistories(
            ChangeList changeList )
    {
        Set<MeshObject> affecteds = changeList.getAffectedMeshObjects();
        for( MeshObject affected : affecteds ) {
            MeshObjectHistory affectedHistory = theMeshBaseView.getMeshBase().obtainMeshObjectHistory( affected.getIdentifier() );
            affectedHistory.putOrThrow( affected );
        }
    }

    /**
     * Given this set of MeshObjects affected by this HistoryTransaction, ripple the consequences
     * forward in time.
     *
     * Stuff can go wrong. For example, if this HistoryTransaction creates a relationship a2.FOO.b1,
     * which is valid at this historical point in time, but then in the future, a relationship a1.FOO.b1
     * is being created, if RelationshipType FOO has a maximum multiplicity of 1, this is invalid.
     *
     * @param normalizeChangeList
     */
    protected void rippleForward(
            boolean normalizeChangeList )
        throws
            CannotApplyChangeException
    {
        ChangeList changeList = getChangeList();
        if( changeList.isEmpty() ) {
            return;
        }

        AMeshBase mb = (AMeshBase) getMeshBaseView().getMeshBase();

        CursorIterator<MeshBaseState> mbsIter = mb.getHistory().iterator();
        mbsIter.moveToAfter( getMeshBaseView().getMeshBaseState() );

        Set<MeshObjectIdentifier> workingList = new HashSet<>(); // all MeshObjects that may need to be patched
        workingList.addAll( changeList.getAffectedMeshObjectIdentifiers() );

        while( mbsIter.hasNext() ) {
            AbstractEditableHistoricMeshBaseState currentMbs                = (AbstractEditableHistoricMeshBaseState) mbsIter.next();
            AbstractEditableHistoricMeshBaseView  currentMbv                = currentMbs.getMeshBaseView();
            ChangeList                            currentChangesBeforePatch = currentMbs.getChangeList();
            ChangeList                            currentChangesAfterPatch  = theMeshBaseView.getMeshBase().createChangeList();

            currentMbv.purgeClonedMeshObjectsCache();

            theActiveMeshBaseViewForThisTransaction = currentMbs.getMeshBaseView();

            Map<MeshObjectIdentifier,ExternalizedMeshObject> currentBeforePatch = new HashMap<>(); // record what it was before
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges = new HashSet<>();

            // We are going to re-apply the Changes in the transaction that led to this MeshBaseView,

            for( MeshObject current : currentMbv.changedMeshObjectsIterator() ) {
                // only pay attention to MeshObjects that were changed in this MBV, they are the only ones we need
                // to change
                currentBeforePatch.put( current.getIdentifier(), current.asExternalized() ); // keep old state, so we can compare

                MeshObjectHistory currentHistory  = mb.meshObjectHistory( current.getIdentifier() );
                MeshObject        currentPrevious = currentHistory.before( currentMbs.getTimeUpdated() );

                if( currentPrevious == null ) {
                    // was created here, so could not be affected by the past
                    continue;
                }

                ChangeList undoChanges = makeLikePrevious( current, currentPrevious, neighborsAlreadyConsideredForRoleChanges );
                for( Change undoChange : undoChanges ) {
                    undoChange.applyTo( currentMbv );
                }
            }

            // now re-apply the current changes, fixing the "old" value in the process
            for( Change currentChangeBeforePatch : currentChangesBeforePatch ) {
                Change currentChangeAfterPatch = currentChangeBeforePatch.createButFrom( currentMbv );

                if( !( currentChangeAfterPatch instanceof MeshObjectCreateChange )) {
                    // need this because we didn't delete created MeshObjects (currentPrevious == null) a few lines above
                    currentChangeAfterPatch.applyTo( currentMbv );
                }
                currentChangesAfterPatch.addChange( currentChangeAfterPatch );
            }

            for( Map.Entry<MeshObjectIdentifier,ExternalizedMeshObject> beforePatch : currentBeforePatch.entrySet() ) {
                MeshObjectIdentifier id = beforePatch.getKey();
                MeshObject afterPatch = currentMbv.findMeshObjectByIdentifier( id );
                if( beforePatch.equals( afterPatch.asExternalized() )) {
                    // all changes have been superseded
                    workingList.remove( id );
                }
            }
            currentMbs.updateChangeList( currentChangesAfterPatch );

            if( workingList.isEmpty() ) {
                // We are done. There are no more consequences of the HistoryTransaction further in the future
                break;
            }

        }
        if( !workingList.isEmpty() ) {
            // fix HEAD
            theActiveMeshBaseViewForThisTransaction = theMeshBaseView.getMeshBase().getHeadMeshBaseView();

            for( MeshObjectIdentifier id : workingList ) {
                AMeshObject       current         = (AMeshObject) mb.findMeshObjectByIdentifier( id );
                MeshObjectHistory currentHistory  = mb.meshObjectHistory( id );
                AMeshObject       currentPrevious = (AMeshObject) currentHistory.current();

                if( current == null ) {
                    current = currentPrevious.createCopyForMeshBaseView( theActiveMeshBaseViewForThisTransaction );
                    theActiveMeshBaseViewForThisTransaction.putNewMeshObject( current );
                }
            }

            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges = new HashSet<>();
            for( MeshObjectIdentifier id : workingList ) {
                AMeshObject       current         = (AMeshObject) mb.findMeshObjectByIdentifier( id );
                MeshObjectHistory currentHistory  = mb.meshObjectHistory( id );
                AMeshObject       currentPrevious = (AMeshObject) currentHistory.current();

                ChangeList makeChanges = makeLikePrevious( current, currentPrevious, neighborsAlreadyConsideredForRoleChanges );
                for( Change makeChange : makeChanges ) {
                    makeChange.applyTo( mb );
                }
            }
        }
    }

    /**
     * Helper method.
     *
     * @param changeThis change this MeshObject
     * @param fromThis using the values from this MeshObject
     * @param neighborsAlreadyConsideredForRoleChanges track to avoid applying the same Role change on both sides
     * @return the Changes that need to be made
     */
    protected ChangeList makeLikePrevious(
            MeshObject                changeThis,
            MeshObject                fromThis,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        AMeshBase  mb      = (AMeshBase) getMeshBaseView().getMeshBase();
        ChangeList changes = mb.createChangeList();

        if( changeThis == null ) {
            changeThis = mb.createMeshObject( fromThis.getIdentifier());
        }
        ChangeUtils.appendDiffAttributes(
                changeThis,
                fromThis,
                changes );

        ChangeUtils.appendDiffEntityTypesProperties(
                changeThis,
                fromThis,
                changes );

        ChangeUtils.appendDiffNeighbors(
                changeThis,
                fromThis,
                changes,
                neighborsAlreadyConsideredForRoleChanges );

        return changes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void transactionCommitted()
    {
        getMeshBaseView().getMeshBase().historyTransactionCommitted( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "mbv",
                    "activeMbv",
                    "myThread",
                    "timeStarted"
                },
                new Object[] {
                    theMeshBaseView,
                    theActiveMeshBaseViewForThisTransaction,
                    myThread,
                    theTimeStarted
                });
    }

    /**
     * The MeshBaseView that's the default for us. This changes as the status of the Transaction
     * moves into ripple.
     */
    protected AbstractEditableMeshBaseView theActiveMeshBaseViewForThisTransaction;

    /**
     * The Changes made by this HistoryTransaction either directly on the relevant MeshBaseState,
     * or rippled forward into the future from here, keyed by the corresponding MeshBaseView.
     */
    protected Map<AbstractEditableMeshBaseView,ChangeList> theChanges = new HashMap<>();
}
