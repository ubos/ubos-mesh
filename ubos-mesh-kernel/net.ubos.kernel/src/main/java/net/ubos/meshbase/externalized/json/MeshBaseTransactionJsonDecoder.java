//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import net.ubos.meshbase.externalized.MeshBaseTransactionDecoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class MeshBaseTransactionJsonDecoder
    extends
        AbstractMeshBaseTransactionJsonDecoder
    implements
        MeshBaseTransactionDecoder
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static MeshBaseTransactionJsonDecoder create()
    {
        return new MeshBaseTransactionJsonDecoder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_BASE_TRANSACTION_JSON_ENCODING_ID;
    }
}
