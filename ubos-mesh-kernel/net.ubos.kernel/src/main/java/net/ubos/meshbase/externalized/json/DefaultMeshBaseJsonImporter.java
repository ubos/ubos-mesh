//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import com.google.gson.stream.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import net.ubos.importer.Importer;
import net.ubos.importer.ImporterException;
import net.ubos.importer.ImporterScore;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.externalized.MeshBaseDecoder;
import static net.ubos.meshbase.externalized.json.MeshBaseJsonTags.HEADER_SECTION_TAG;
import static net.ubos.meshbase.externalized.json.MeshBaseJsonTags.MESHOBJECTS_SECTION_TAG;
import static net.ubos.meshbase.externalized.json.MeshBaseJsonTags.TRANSACTIONS_SECTION_TAG;
import net.ubos.meshbase.history.EditableHistoryMeshBase;

/**
 * Separate class to avoid coding mistakes.
 */
public final class DefaultMeshBaseJsonImporter
    extends
        AbstractMeshBaseJsonDecoder
    implements
        MeshBaseDecoder,
        Importer
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static DefaultMeshBaseJsonImporter create()
    {
        return new DefaultMeshBaseJsonImporter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return "Default MeshBaseJsonImporter";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getIdentifier()
    {
        return getClass().getSimpleName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_BASE_JSON_ENCODING_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String analyze(
            File importCandidate )
        throws
            IOException
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImporterScore importTo(
            File                    toBeImported,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            ImporterException,
            IOException
    {
        if( !toBeImported.isFile() ) {
            throw new ImporterException.NotAFile( this );
        }
        return importTo( new InputStreamReader( new FileInputStream( toBeImported ), StandardCharsets.UTF_8 ), mb );
    }

    /**
     * Import data into this MeshBase.
     *
     * @param s read the data from here
     * @param mb the MeshBase to import into
     * @return the score
     * @throws ImporterException thrown if a parsing problem occurred; details are in the cause
     * @throws IOException an I/O problem occurred
     */
    public ImporterScore importTo(
            InputStream s,
            MeshBase    mb )
        throws
            ImporterException,
            IOException
    {
        return importTo( new InputStreamReader( s ), mb );
    }

    /**
     * Import data into this MeshBase.
     *
     * @param r read the data from here
     * @param mb the MeshBase to import into
     * @return the score
     * @throws ImporterException thrown if a parsing problem occurred; details are in the cause
     * @throws IOException an I/O problem occurred
     */
    public ImporterScore importTo(
            Reader   r,
            MeshBase mb )
        throws
            ImporterException,
            IOException
    {
        return importTo( new JsonReader( r ), mb );
    }

    /**
     * Import data into this MeshBase.
     *
     * @param jr read the data from here
     * @param mb the MeshBase to import into
     * @return the score
     * @throws ImporterException thrown if a parsing problem occurred; details are in the cause
     * @throws IOException an I/O problem occurred
     */
    public ImporterScore importTo(
            JsonReader jr,
            MeshBase   mb )
        throws
            ImporterException,
            IOException
    {
        MContextualMeshObjectIdentifierNamespaceMap nsMap = MContextualMeshObjectIdentifierNamespaceMap.create(
                mb.getPrimaryNamespaceMap() );

        ImporterMeshObjectIdentifierDeserializer idDeserializer = ImporterMeshObjectIdentifierDeserializer.create(
                mb,
                nsMap );

        try {
            List<ParserFriendlyExternalizedMeshObject> foundMeshObjects = null;
            jr.beginObject();

            while( jr.hasNext() ) {
                switch( jr.nextName() ) {
                    case HEADER_SECTION_TAG:
                        decodeHeader( jr, idDeserializer );
                        break;

                    case MESHOBJECTS_SECTION_TAG:
                        foundMeshObjects = decodeMeshObjects( jr, idDeserializer );
                        break;

                    case TRANSACTIONS_SECTION_TAG:
                        decodeTransactions( jr, idDeserializer ); // ignore result
                        break;
                }
            }

            jr.endObject();

            mb.discardAllAndBulkLoad( foundMeshObjects, nsMap.getDefaultNamespace() );

        } catch( Throwable ex ) {
            throw new ImporterException.Failure( this, ex );

        }

        return new ImporterScore( this, ImporterScore.BEST, null );
    }
}
