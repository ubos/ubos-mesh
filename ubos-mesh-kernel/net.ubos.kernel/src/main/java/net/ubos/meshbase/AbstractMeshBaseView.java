//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.util.ArrayHelper;

/**
 * Factors out common functionality between all implementations of MeshBase and MeshBaseView.
 */
public abstract class AbstractMeshBaseView
        implements
            MeshBaseView
{
    /**
     * Constructor for subclasses only.
     *
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBaseView
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     */
    public AbstractMeshBaseView(
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        if( identifierFactory == null ) {
            throw new NullPointerException();
        }
        this.theMeshObjectIdentifierFactory = identifierFactory;
        this.theIdDeserializer              = idDeserializer;
    }

// MeshObjectIdentifierFactory

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifierNamespace getDefaultNamespace()
    {
        return theMeshObjectIdentifierFactory.getDefaultNamespace();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrimaryMeshObjectIdentifierNamespaceMap getPrimaryNamespaceMap()
    {
        return theMeshObjectIdentifierFactory.getPrimaryNamespaceMap();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getHomeMeshObjectIdentifier()
    {
        return theMeshObjectIdentifierFactory.getHomeMeshObjectIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getHomeObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns )
    {
        return theMeshObjectIdentifierFactory.getHomeObjectIdentifierIn( ns );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifier()
    {
        return theMeshObjectIdentifierFactory.createRandomMeshObjectIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifier(
            String localId )
    {
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifier( localId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns )
    {
        return theMeshObjectIdentifierFactory.createRandomMeshObjectIdentifierIn( ns );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierIn(
            MeshObjectIdentifierNamespace ns,
            String                        localId )
    {
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifierIn( ns, localId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String               append )
        throws
            ParseException
    {
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifierBelow( baseIdentifier, append );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String []            append )
        throws
            ParseException
    {
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifierBelow( baseIdentifier, append );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String               append,
            String ...           appendMore )
        throws
            ParseException
    {
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifierBelow( baseIdentifier, append, appendMore );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier )
    {
        return theMeshObjectIdentifierFactory.createRandomMeshObjectIdentifierBelow( baseIdentifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            MeshObjectIdentifier baseIdentifier,
            String ...           append )
        throws
            ParseException
    {
        return theMeshObjectIdentifierFactory.createRandomMeshObjectIdentifierBelow( baseIdentifier, append );
    }

// MeshObjectIdentifierDeserializer

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            String externalForm )
        throws
            ParseException
    {
        return theIdDeserializer.meshObjectIdentifierFromExternalForm( externalForm );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            MeshObjectIdentifier contextIdentifier,
            String               externalForm )
        throws
            ParseException
    {
        return theIdDeserializer.meshObjectIdentifierFromExternalForm( contextIdentifier, externalForm );
    }

// MeshBaseView

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract AbstractMeshBase getMeshBase();

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            String baseIdentifier,
            String append )
        throws
            ParseException
    {
        MeshObjectIdentifier baseId = meshObjectIdentifierFromExternalForm( baseIdentifier );
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifierBelow( baseId, append );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            String    baseIdentifier,
            String [] append )
        throws
            ParseException
    {
        MeshObjectIdentifier baseId = meshObjectIdentifierFromExternalForm( baseIdentifier );
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifierBelow( baseId, append );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            String     baseIdentifier,
            String     append,
            String ... appendMore )
        throws
            ParseException
    {
        MeshObjectIdentifier baseId = meshObjectIdentifierFromExternalForm( baseIdentifier );
        return theMeshObjectIdentifierFactory.createMeshObjectIdentifierBelow( baseId, append, appendMore );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            String baseIdentifier )
        throws
            ParseException
    {
        MeshObjectIdentifier baseId = meshObjectIdentifierFromExternalForm( baseIdentifier );
        return theMeshObjectIdentifierFactory.createRandomMeshObjectIdentifierBelow( baseId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            String     baseIdentifier,
            String ... append )
        throws
            ParseException
    {
        MeshObjectIdentifier baseId = meshObjectIdentifierFromExternalForm( baseIdentifier );
        return theMeshObjectIdentifierFactory.createRandomMeshObjectIdentifierBelow( baseId, append );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] findMeshObjectsByIdentifier(
            MeshObjectIdentifier [] identifiers )
    {
        MeshObject [] ret = createArray( identifiers.length );
        for( int i=0 ; i<identifiers.length ; ++i ) {
            ret[i] = findMeshObjectByIdentifier( identifiers[i] );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject findMeshObjectByIdentifierOrThrow(
            MeshObjectIdentifier identifier )
        throws
            MeshObjectsNotFoundException
    {
        MeshObject ret = findMeshObjectByIdentifier( identifier );
        if( ret == null ) {
            throw new MeshObjectsNotFoundException( this, identifier );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] findMeshObjectsByIdentifierOrThrow(
            MeshObjectIdentifier [] identifiers )
        throws
            MeshObjectsNotFoundException
    {
        MeshObject []           ret      = createArray( identifiers.length );
        MeshObjectIdentifier [] notFound = null; // allocated when needed
        int                     count    = 0;

        for( int i=0 ; i<identifiers.length ; ++i ) {
            ret[i] = findMeshObjectByIdentifier( identifiers[i] );
            if( ret[i] == null ) {
                if( notFound == null ) {
                    notFound = new MeshObjectIdentifier[ identifiers.length ];
                }
                notFound[ count++ ] = identifiers[i];
            }
        }
        if( count == 0 ) {
            return ret;
        }
        if( count < notFound.length ) {
            notFound = ArrayHelper.copyIntoNewArray( notFound, 0, count, MeshObjectIdentifier.class );
        }
        throw new MeshObjectsNotFoundException( this, notFound );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet findCommonNeighbors(
            MeshObject one,
            MeshObject two )
    {
        return findCommonNeighbors( new MeshObject[] { one, two } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet findCommonNeighbors(
            MeshObject [] all )
    {
        MeshObjectSet ret = all[0].traverseToNeighbors();
        for( int i=1 ; i<all.length ; ++i ) {
            MeshObjectSet found = all[i].traverseToNeighbors();
            ret = ret.intersection( found );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet findCommonNeighbors(
            MeshObject one,
            RoleType   oneType,
            MeshObject two,
            RoleType   twoType )
    {
        return findCommonNeighbors( new MeshObject[] { one, two }, new RoleType[] { oneType, twoType } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet findCommonNeighbors(
            MeshObject [] all,
            RoleType []   allTypes )
    {
        MeshObjectSet ret = all[0].traverse( allTypes[0] );
        for( int i=1 ; i<all.length ; ++i ) {
            MeshObjectSet found = all[i].traverse(  allTypes[i] );
            ret = ret.intersection( found );
        }
        return ret;
    }

    /**
     * Allocate an array of the the right subtype of MeshObject.
     *
     * @param len the length of the to-be-allocated array
     * @return the created array
     */
    public abstract MeshObject [] createArray(
            int len );

    /**
     * The factory for MeshObjectIdentifiers.
     */
    protected final MeshObjectIdentifierFactory theMeshObjectIdentifierFactory;

    /**
     * Knows how to deserialize MeshObjectIdentifiers provided as Strings.
     */
    protected final MeshObjectIdentifierDeserializer theIdDeserializer;
}
