//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.security;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;

/**
 * This abstract class makes it easier to implement AccessManagers whose policy never
 * requires resolving neighbor MeshObjects; it can make all its decisions by only
 * having the neighbor MeshObjectIdentifier.
 */
public abstract class AbstractNoNeedToResolveNeighborsAccessManager
    implements
        AccessManager
{
    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedCreateRoleAttribute(
            MeshObject obj,
            MeshObject neighbor,
            String     name )
        throws
            NotPermittedException
    {
        checkPermittedCreateRoleAttribute( obj, neighbor.getIdentifier(), name );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedGetRoleAttribute(
            MeshObject obj,
            MeshObject neighbor,
            String     name )
        throws
            NotPermittedException
    {
        checkPermittedGetRoleAttribute( obj, neighbor.getIdentifier(), name );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedSetRoleAttribute(
            MeshObject   obj,
            MeshObject   neighbor,
            String       name,
            Serializable newValue )
        throws
            NotPermittedException
    {
        checkPermittedSetRoleAttribute( obj, neighbor.getIdentifier(), name, newValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedDeleteRoleAttribute(
            MeshObject obj,
            MeshObject neighbor,
            String     name )
        throws
            NotPermittedException
    {
        checkPermittedDeleteRoleAttribute( obj, neighbor.getIdentifier(), name );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedBless(
            MeshObject  obj,
            RoleType [] thisEnds,
            MeshObject  neighbor )
        throws
            NotPermittedException
    {
        checkPermittedBless( obj, thisEnds, neighbor.getIdentifier() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedUnbless(
            MeshObject  obj,
            RoleType [] thisEnds,
            MeshObject  neighbor )
        throws
            NotPermittedException
    {
        checkPermittedUnbless( obj, thisEnds, neighbor.getIdentifier() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedTraverse(
            MeshObject obj,
            RoleType   toTraverse,
            MeshObject neighbor )
        throws
            NotPermittedException
    {
        checkPermittedTraverse( obj, toTraverse, neighbor.getIdentifier() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedGetRoleProperty(
            MeshObject   obj,
            MeshObject   neighbor,
            PropertyType pt )
        throws
            NotPermittedException
    {
        checkPermittedGetRoleProperty( obj, neighbor.getIdentifier(), pt );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedSetRoleProperty(
            MeshObject    obj,
            MeshObject    neighbor,
            PropertyType  pt,
            PropertyValue newValue )
        throws
            NotPermittedException
    {
        checkPermittedSetRoleProperty( obj, neighbor.getIdentifier(), pt, newValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checkPermittedIsRelated(
            MeshObject obj,
            MeshObject neighbor )
        throws
            NotPermittedException
    {
        checkPermittedIsRelated( obj, neighbor.getIdentifier() );
    }
}
