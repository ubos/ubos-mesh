//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.MeshBase;

/**
  * <p>This TransactionException is thrown to indicate that the current Thread reached a time out for
  * trying to create a Transaction "asap". While there may be genuine congestion in an application,
  * the most likely cause of this Exception is that the developer forgot to complete a previously
  * opened Transaction, in which case the next Transaction cannot be started.</p>
  * <p>For a good pattern on how to manage Transactions, please refer to the documentation
  * of {@link MeshBase}.</p>  
  */
public class TransactionTimeoutException
        extends
            TransactionException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param trans the MeshBase that was affected
     * @param blockingTransaction the Transaction that blocked the new Transaction
     */
    public TransactionTimeoutException(
            MeshBase    trans,
            Transaction blockingTransaction )
    {
        super( trans, blockingTransaction );
    }
}
