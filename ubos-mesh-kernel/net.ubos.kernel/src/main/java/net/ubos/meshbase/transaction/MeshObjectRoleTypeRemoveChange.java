//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import java.util.Objects;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleTypeRemoveChange;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.util.ArrayHelper;

/**
 * <p>This event indicates that a relationship between the MeshObject and
 * another MeshObject was unblessed.
 */
public class MeshObjectRoleTypeRemoveChange
        extends
            AbstractMeshObjectRoleTypeChange
{
    /**
     * Pass-through constructor for subclasses.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old values of the RoleType, prior to the event
     * @param deltaValues the RoleTypes that changed
     * @param newValues the new values of the RoleType, after the event
     * @param neighbor the neighbor MeshObject
     */
    public MeshObjectRoleTypeRemoveChange(
            MeshObject  source,
            RoleType [] oldValues,
            RoleType [] deltaValues,
            RoleType [] newValues,
            MeshObject  neighbor )
    {
        super(  source,
                oldValues,
                deltaValues,
                newValues,
                neighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject  otherObject        = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject  relatedOtherObject = base.findMeshObjectByIdentifier( getNeighbor().getIdentifier() );
        RoleType [] roleTypes          = getDeltaValue();

        otherObject.unblessRole( roleTypes, relatedOtherObject );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleTypeRemoveChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject  updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject  updatedNeighbor   = mbv.findMeshObjectByIdentifier( theNeighbor.getIdentifier() );
        RoleType [] oldTypes          = updatedMeshObject.getRoleTypes( theNeighbor );
        RoleType [] deltaTypes        = theDeltaValue;
        RoleType [] newTypes          = ArrayHelper.remove( oldTypes, deltaTypes, true, RoleType.class );

        return new MeshObjectRoleTypeRemoveChange(
                updatedMeshObject,
                oldTypes,
                deltaTypes,
                newTypes,
                updatedNeighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleTypeAddChange inverse()
    {
        return new MeshObjectRoleTypeAddChange(
                getSource(),
                getNewValue(),
                getDeltaValue(),
                getOldValue(),
                getNeighbor() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectRoleTypeAddChange )) {
            return false;
        }

        MeshObjectRoleTypeAddChange realCandidate = (MeshObjectRoleTypeAddChange) candidate;
        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }
        if( !getNeighbor().equals( realCandidate.getNeighbor())) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(),  realCandidate.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectRoleTypeRemoveChange )) {
            return false;
        }
        MeshObjectRoleTypeRemoveChange realOther = (MeshObjectRoleTypeRemoveChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !theNeighbor.equals(realOther.theNeighbor )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realOther.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = Objects.hash(
                theSource.getIdentifier(),
                theNeighbor.getIdentifier());

        for( RoleType rt : getDeltaValue() ) {
            ret ^= rt.getIdentifier().hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectRoleTypeRemoveChange asExternalized()
    {
        return new ExternalizedMeshObjectRoleTypeRemoveChange(
                getSource().getIdentifier(),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getOldValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getDeltaValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getNewValue() ),
                theNeighbor.getIdentifier());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleTypeRemoveChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectRoleTypeRemoveChange(
                translation.get( theSource.getIdentifier() ),
                theOldValue,
                theDeltaValue,
                theNewValue,
                translation.get( theNeighbor.getIdentifier() ));
    }
}
