//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.a;

import java.util.ArrayList;
import net.ubos.mesh.AbstractMeshObject;
import net.ubos.mesh.IllegalPropertyValueException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.RoleTypeBlessedAlreadyException;
import net.ubos.mesh.RoleTypeNotBlessedException;
import net.ubos.mesh.security.PropertyReadOnlyException;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.AbstractHeadMeshBaseView;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.meshbase.history.EditableHistoricMeshBaseView;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.IllegalTransactionThreadException;
import net.ubos.meshbase.transaction.MeshObjectCreateChange;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
 * HeadTransaction implementation for the AMeshBase implementation.
 */
public class AHeadTransaction
    extends
        ATransaction
    implements
        HeadTransaction,
        CanBeDumped
{
    protected static final Log log = Log.getLogInstance( AHeadTransaction.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param mbv the AMeshBase that created this Transaction, and which is
     *            guarded by this Transaction
     */
    protected AHeadTransaction(
            AbstractHeadMeshBaseView mbv )
    {
        super( mbv );

        theChangeList = AChangeList.create( mbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addChange(
            Change toAdd )
    {
        theChangeList.addChange( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractEditableMeshBaseView getDefaultMeshBaseViewForTransaction()
    {
        return theMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ChangeList getChangeList()
    {
        return theChangeList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkTransaction(
            EditableMeshBaseView mbv )
        throws
            IllegalTransactionThreadException,
            WrongMeshBaseViewException
    {
        if( !owns( Thread.currentThread() )) {
            throw new IllegalTransactionThreadException( mbv, this );
        }
        WrongMeshBaseViewException.checkCompatible( theMeshBaseView, mbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkTransaction(
            EditableHistoricMeshBaseView mbv )
        throws
            IllegalTransactionThreadException,
            WrongMeshBaseViewException
    {
        throw WrongMeshBaseViewException.create( theMeshBaseView, mbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getEffectiveCommitTime()
    {
        return theTimeEnded;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commitTransaction(
            boolean normalizeChangeList )
    {
        if( theStatus != Transaction.Status.TRANSACTION_STARTED ) {
            log.error( "Illegal state for transaction:", theStatus );
        }

        ThreadIdentityManager.sudo();

        try {
            checkValidGraph( theChangeList );

            if( normalizeChangeList ) {
                theChangeList.normalize();
            }

            theTimeEnded = System.currentTimeMillis();

            updateMeshObjectTimestamps(theChangeList, theTimeEnded );

            theStatus    = Transaction.Status.TRANSACTION_COMMITTED;

        } finally {
            ThreadIdentityManager.sudone();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rollbackTransaction(
            Throwable thrown )
    {
        if( theStatus != Status.TRANSACTION_STARTED ) {
            log.error( "illegal state for transaction: ", theStatus );
        }

        ThreadIdentityManager.sudo();

        try {
            // go backwards in the change set
            ArrayList<Change> changes = theChangeList.getChanges();

            for( int i=changes.size()-1 ; i>=0 ; --i ) {
                Change current = changes.get( i );

                try {
                    Change inverted = current.inverse();

                    if( inverted == null ) {
                        log.error( "Could not invert change", current );
                        continue;
                    }

                    inverted.applyTo( theMeshBaseView.getMeshBase() );

                } catch( CannotApplyChangeException ex ) {
                    Throwable cause = ex.getCause();

                    if(    !( cause instanceof PropertyReadOnlyException )
                        && !( cause instanceof IllegalPropertyValueException )
                        && !( cause instanceof RoleTypeNotBlessedException )
                        && !( cause instanceof RoleTypeBlessedAlreadyException ))
                    {
                        log.error( ex );
                        // that's the best we can do
                    }

                } catch( MeshObjectGraphModificationException ex ) {
                    log.error( ex );
                    // that's the best we can do
                } catch( TransactionException ex ) {
                    log.error( ex );
                    // that's the best we can do
                } catch( Throwable ex ) {
                    log.error( ex );
                    // that's the best we can do
                }
            }

            theStatus    = Status.TRANSACTION_ROLLEDBACK;
            theTimeEnded = System.currentTimeMillis();

        } finally {
            ThreadIdentityManager.sudone();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void transactionCommitted()
    {
        getMeshBase().headTransactionCommitted( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "mbv",
                    "changes",
                    "myThread",
                    "timeStarted"
                },
                new Object[] {
                    theMeshBaseView,
                    theChangeList,
                    myThread,
                    theTimeStarted
                });
    }

    /**
     * The list of Changes accumulated during this Transaction so far.
     * If this is a HeadTransaction, this is obvious.
     * If this is a HistoryTransaction, this instance is nevertheless different from the ChangeList held by
     * HistoryMeshBase's MeshBaseState: this here contains the changes made on the current Transaction, which
     * may (or may not, if there is a rollback) be added to potentially already-existing Changes on the
     * MeshBaseState.
     */
    protected final AChangeList theChangeList;
}
