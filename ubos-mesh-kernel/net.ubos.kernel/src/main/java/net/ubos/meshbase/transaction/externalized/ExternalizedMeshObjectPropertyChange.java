//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectPropertyChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;

/**
 *
 */
public class ExternalizedMeshObjectPropertyChange
    extends
        AbstractExternalizedChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier the identifier of the MeshObject that is the source of the event
     * @param propertyTypeIdentifier the identifier of the PropertyType
     * @param oldValue the old value of the Property, prior to the event
     * @param newValue the new value of the Property, after the event
     */
    public ExternalizedMeshObjectPropertyChange(
            MeshObjectIdentifier sourceIdentifier,
            MeshTypeIdentifier   propertyTypeIdentifier,
            PropertyValue        oldValue,
            PropertyValue        newValue )
    {
        super( sourceIdentifier );

        thePropertyTypeIdentifier = propertyTypeIdentifier;
        theOldValue = oldValue;
        theNewValue = newValue;
    }

    public MeshTypeIdentifier getPropertyTypeIdentifier()
    {
        return thePropertyTypeIdentifier;
    }

    public PropertyValue getOldValue()
    {
        return theOldValue;
    }

    public PropertyValue getNewValue()
    {
        return theNewValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject    source = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );
            PropertyType  pt     = ModelBase.SINGLETON.findPropertyType( thePropertyTypeIdentifier );

            source.setPropertyValue( pt, theNewValue );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectPropertyChange internalizeWith(
            MeshObject [] affectedMeshObjects )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        if( affectedMeshObjects.length != 1 ) {
            throw new IllegalArgumentException();
        }
        PropertyType propertyType = ModelBase.SINGLETON.findPropertyType( thePropertyTypeIdentifier );

        return new MeshObjectPropertyChange( affectedMeshObjects[0], propertyType, theOldValue, theNewValue );
    }

    protected final MeshTypeIdentifier thePropertyTypeIdentifier;
    protected final PropertyValue theOldValue;
    protected final PropertyValue theNewValue;
}
