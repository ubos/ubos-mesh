//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import net.ubos.meshbase.transaction.externalized.json.AbstractChangeListJsonEncoder;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.meshbase.transaction.ChangeList;

/**
 *
 */
public abstract class AbstractMeshBaseTransactionJsonEncoder
    extends
        AbstractChangeListJsonEncoder
    implements
        MeshBaseTransactionJsonTags
{
    /**
     * Constructor.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected AbstractMeshBaseTransactionJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        super( typeIdSerializer );
    }

    /**
     * Write a MeshBaseState to a Writer.
     *
     * @param timeUpdated the time when the transaction was successful
     * @param changes the Changes made during the transaction
     * @param idSerializer the serializer to use
     * @param w the Writer to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    public void writeTransaction(
            long                           timeUpdated,
            ChangeList                     changes,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException
    {
        JsonWriter jw = new JsonWriter( w );
        jw.setSerializeNulls( true );
        jw.setIndent( " " );

        writeTransaction( timeUpdated, changes, idSerializer, jw );

        jw.flush();
    }

    public void writeTransaction(
            long                           timeUpdated,
            ChangeList                     changes,
            MeshObjectIdentifierSerializer idSerializer,
            JsonWriter                     jw )
        throws
            EncodingException,
            IOException
    {

        jw.beginObject();

        jw.name( CHANGE_TIME_TAG ).value( timeUpdated );

        jw.name( CHANGES_TAG );
        writeChangeList( changes, idSerializer, true, true, jw ); // emitOldValues=true, emitNewValues=true

        jw.endObject();
    }
}
