//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.SubjectArea;

/**
  * This is the abstract supertype for all events indicating
  * a lifecycle event in the life of a MeshObject.
  */
public abstract class AbstractMeshObjectLifecycleChange
    implements
        Change
{
    /**
     * Private constructor, use subclasses.
     *
     * @param changedObj the MeshObject whose lifecycle changed
     * @param changedObjId the identifier of the MeshObject whose lifecycle changed
     */
    protected AbstractMeshObjectLifecycleChange(
            MeshObject           changedObj,
            MeshObjectIdentifier changedObjId )
    {
        theChangedObject           = changedObj;
        theChangedObjectIdentifier = changedObjId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return theChangedObjectIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject[] { theChangedObject };
    }

    /**
     * Obtain the changed MeshObject.
     *
     * @return the MeshObject
     */
    public MeshObject getChangedMeshObject()
    {
        return theChangedObject;
    }

    /**
     * Obtain the identifier of the changed MeshObject.
     *
     * @return the identifier of the MeshObject
     */
    public MeshObjectIdentifier getChangedMeshObjectIdentifier()
    {
        return theChangedObjectIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        // no op
    }

    /**
     * The MeshObject that changed.
     */
    protected final MeshObject theChangedObject;

    /**
     * Identifier of the MeshObject that changed. We keep this separately because in case of deletion,
     * the MeshObject may not be around any more.
     */
    protected final MeshObjectIdentifier theChangedObjectIdentifier;
}
