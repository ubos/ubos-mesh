//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.security;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;

/**
 * A simplistic AccessManager implementation that allows all operations under all circumstances
 * and does nothing to assign owners when asked.
 * This may not be very useful in production applications, but is useful for testing purposes.
 */
public class PermitAllAccessManager
        extends
            AbstractNoNeedToResolveNeighborsAccessManager
{
    /**
     * Factory method.
     *
     * @return the created PermitAllAccessManager
     */
    public static PermitAllAccessManager create()
    {
        return new PermitAllAccessManager();
    }

    /**
     * Constructor.
     */
    protected PermitAllAccessManager()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assignOwner(
            MeshObject toBeOwned,
            MeshObject newOwner )
        throws
            TransactionException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedCreate(
            MeshBaseView         mbv,
            MeshObjectIdentifier identifier )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedAccess(
            MeshObject obj )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedCreateAttribute(
            MeshObject obj,
            String name )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetAttribute(
            MeshObject   obj,
            String       name,
            Serializable newValue )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedDeleteAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException
    {
        // no op
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedBlessedBy(
            MeshObject obj,
            EntityType type )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedBless(
            MeshObject    obj,
            EntityType [] types )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedUnbless(
            MeshObject    obj,
            EntityType [] types )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetProperty(
            MeshObject    obj,
            PropertyType  thePropertyType,
            PropertyValue newValue )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetProperty(
            MeshObject   obj,
            PropertyType thePropertyType )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedCreateRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name,
            Serializable         newValue )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedDeleteRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedBless(
            MeshObject           obj,
            RoleType []          thisEnds,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedUnbless(
            MeshObject           obj,
            RoleType []          thisEnds,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedTraverse(
            MeshObject           obj,
            RoleType             toTraverse,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetRoleProperty(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            PropertyType         pt )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetRoleProperty(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            PropertyType         pt,
            PropertyValue        newValue )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedIsRelated(
            MeshObject           obj,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedDelete(
            MeshObject obj )
        throws
            NotPermittedException
    {
        // no op
    }
}
