//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

/**
 * Defines the MeshObjectIdentifiers of well-known MeshObjects in the MeshBase,
 * and related information.
 * Not all of them may exist in a given MeshBase. Some other well-known MeshObjects
 * may be defined by other packages, e.g. bots.
 */
public interface MeshBaseWellKnown
{
    /**
     * Separator between hierarchical elements.
     */
    public static final String SEP = "/";

    /**
     * Identifier of the "directory" that contains the well-known MeshObjects.
     */
    public static final String WELL_KNOWN_DIR = "-";

    /**
     * Identifier of the "directory" that contains the well-known MeshObjects
     * with trailing separator.
     */
    public static final String WELL_KNOWN_DIR_WITH_SEP = WELL_KNOWN_DIR + SEP;

    /**
     * Identifier of the "directory" that contains the MeshObjects paralleling the model.
     */
    public static final String MODEL_DIR = WELL_KNOWN_DIR_WITH_SEP + "model";

    /**
     * Identifier of the "directory" that contains the MeshObjects paralleling the model
     * with trailing separator.
     */
    public static final String MODEL_DIR_WITH_SEP = MODEL_DIR + SEP;

    /**
     * Identifier of the MeshObject that collects the nonblessed MeshObjects.
     */
    public static final String UNBLESSED_INDEX = WELL_KNOWN_DIR_WITH_SEP + "nonblessed";

    /**
     * Identifier of the "directory" that contains the MeshObjects collecting indexed terms.
     */
    public static final String TERMS_INDEX_DIR = WELL_KNOWN_DIR_WITH_SEP + "terms";

    /**
     * Identifier of the "directory" that contains the MeshObjects collecting indexed terms
     * with trailing separator.
     */
    public static final String TERMS_INDEX_DIR_WITH_SEP = TERMS_INDEX_DIR + SEP;

    /**
     * Identifier of the "directory" that contains Account MeshObjects used for access control to this MeshBase.
     */
    public static final String ACCOUNTS_DIR = WELL_KNOWN_DIR_WITH_SEP + "account";

    /**
     * Identifier of the "directory" that contains Account MeshObjects used for access control to this MeshBase
     * with trailing separator.
     */
    public static final String ACCOUNTS_DIR_WITH_SEP = ACCOUNTS_DIR + SEP;

    /**
     * Identifier of the MeshObject that represents the owner Account of this MeshBase.
     */
    public static final String OWNER_ACCOUNT = ACCOUNTS_DIR_WITH_SEP + "owner";

    /**
     * Identifier of the "directory" that contains Principal MeshObjects used for access control to this MeshBase.
     */
    public static final String PRINCIPAL_DIR = WELL_KNOWN_DIR_WITH_SEP + "principal";
    
    /**
     * Identifier of the "directory" that contains Principal MeshObjects used for access control to this MeshBase
     * with trailing separator.
     */
    public static final String PRINCIPAL_DIR_WITH_SEP = WELL_KNOWN_DIR_WITH_SEP + SEP;
    
    /**
     * Name of the Role Attribute on the side of the collecting index MeshObject that indicates
     * the number of occurrences in the neighbor MeshObject that it indexes.
     */
    public static final String INDEX_OCCURRENCES_ROLE_ATTRIBUTE = "index:occ";
}
