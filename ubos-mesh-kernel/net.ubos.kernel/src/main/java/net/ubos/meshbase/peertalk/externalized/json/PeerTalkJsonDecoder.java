//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.peertalk.externalized.json;

import net.ubos.meshbase.peertalk.externalized.PeerTalkDecoder;

/**
 * Separate class to avoid coding mistakes.
 */
public class PeerTalkJsonDecoder
    extends
        AbstractPeerTalkJsonDecoder
    implements
        PeerTalkDecoder
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static PeerTalkJsonDecoder create()
    {
        return new PeerTalkJsonDecoder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return PEER_TALK_JSON_ENCODING_ID;
    }
}
