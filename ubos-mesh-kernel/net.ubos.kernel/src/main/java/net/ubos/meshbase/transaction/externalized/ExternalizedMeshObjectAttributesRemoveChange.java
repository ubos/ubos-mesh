//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectAttributesRemoveChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;

/**
 *
 */
public class ExternalizedMeshObjectAttributesRemoveChange
    extends
        AbstractExternalizedMeshObjectAttributesChange
{
    public ExternalizedMeshObjectAttributesRemoveChange(
            MeshObjectIdentifier sourceIdentifier,
            String []            oldValues,
            String []            deltaValues,
            String []            newValues )
    {
        super( sourceIdentifier, oldValues, deltaValues, newValues );
    }

    public String [] getRemoved()
    {
        return theDeltaValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject source = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );

            source.deleteAttributes( theDeltaValues );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesRemoveChange internalizeWith(
            MeshObject [] affectedMeshObjects )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        if( affectedMeshObjects.length != 1 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectAttributesRemoveChange( affectedMeshObjects[0], theOldValues, theDeltaValues, theNewValues );
    }
}
