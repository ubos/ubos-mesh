//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.text.ParseException;
import net.ubos.mesh.IsAbstractException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;

/**
 * Intermediate class.
 */
public abstract class AbstractEditableMeshBaseView
    extends
        AbstractMeshBaseView
    implements
        EditableMeshBaseView
{
    /**
     * Constructor for subclasses only.
     *
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBaseView
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     */
    public AbstractEditableMeshBaseView(
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        super( identifierFactory, idDeserializer );
    }

    /**
     * Set the MeshBase that this MeshBaseView belongs to.
     *
     * @param mb the MeshBase
     */
    public void setMeshBase(
            AbstractMeshBase mb )
    {
        if( theMeshBase != null ) {
            throw new IllegalArgumentException( "MeshBase set already" );
        }
        theMeshBase = mb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject()
        throws
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObject( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject(
            EntityType type )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObject( type, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject(
            MeshObjectIdentifier identifier )
        throws
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObject( identifier, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject(
            MeshObjectIdentifier identifier,
            EntityType           type )
        throws
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObject( identifier, type, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base )
        throws
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObjectBelow( base, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base,
            EntityType type )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObjectBelow( base, type, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base,
            String     trailingId )
        throws
            ParseException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObjectBelow( base, trailingId, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base,
            String     trailingId,
            EntityType type )
        throws
            ParseException,
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBase.getLifecycleManager().createMeshObjectBelow( base, trailingId, type, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMeshObject(
            MeshObject obj )
        throws
            TransactionException,
            NotPermittedException
    {
        theMeshBase.getLifecycleManager().deleteMeshObject( obj, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMeshObjects(
            MeshObject[] objs )
        throws
            TransactionException,
            NotPermittedException
    {
        theMeshBase.getLifecycleManager().deleteMeshObjects( objs, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMeshObjects(
            MeshObjectSet objs )
        throws
            TransactionException,
            NotPermittedException
    {
        theMeshBase.getLifecycleManager().deleteMeshObjects( objs, this );
    }

    /**
     * Enable Transactions to tell us that a new MeshObject exists.
     *
     * @param created the newly created MeshObject
     */
    public abstract void putNewMeshObject(
            MeshObject created );

    /**
     * Enable Transactions to tell us to remove a MeshObject.
     *
     * @param objId the identifier of the to-be-removed MeshObject
     */
    public abstract void removeMeshObject(
            MeshObjectIdentifier objId );

    /**
     * The MeshBase that this MeshBaseView belongs to.
     */
    protected AbstractMeshBase theMeshBase;
}
