//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.ubos.mesh.BlessedAlreadyException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotBlessedException;
import net.ubos.mesh.RoleTypeBlessedAlreadyException;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * <p>A ChangeList is the set of {@link Change Changes} (createCopy, update, delete)
  * that are to be or have been performed during a Transaction.</p>
  *
  * When normalized, any transaction that leads from a before to an after MeshBaseState
  * has the same ChangeList
  */
public abstract class ChangeList
        implements
            Iterable<Change>,
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( ChangeList.class );

    /**
     * Determine the MeshBase that emits these Changes.
     *
     * @return the MeshBase
     */
    public MeshBase getMeshBase()
    {
        return getMeshBaseView().getMeshBase();
    }

    /**
     * Determine the MeshBaseView that was created as a result of these Changes.
     *
     * @return the MeshBaseView
     */
    public abstract MeshBaseView getMeshBaseView();

    /**
     * Obtain Iterator over the Changes in this ChangeList.
     *
     * @return Iterator over the Changes in this ChangeList
     */
    @Override
    public CursorIterator<Change> iterator()
    {
        return ArrayListCursorIterator.create( theChanges );
    }

    /**
     * Obtain the Changes in this set, in sequence.
     *
     * @return the Changes in this set
     */
    public ArrayList<Change> getChanges()
    {
        return theChanges;
    }

    /**
     * Obtain the number of Changes in the ChangeList.
     *
     * @return the number of Changes in the ChangeList
     */
    public int size()
    {
        return getChanges().size(); // FIXME can be made better
    }

    /**
     * Normalize this ChangeList, i.e. remove intermediate Changes that are overwritten
     * by other Changes later in the same ChangeList, and arrange the Changes in
     * canonical order.
     */
    public synchronized void normalize()
    {
        // 1. sort by MeshObject
        // 2. normalize each list of Changes

        Map<MeshObjectIdentifier,SubjectChangeList> sortedChanges = new HashMap<>();
        int i = 0;
        for( Change c : theChanges ) {
            MeshObjectIdentifier sourceId = c.getSourceMeshObjectIdentifier();
            SubjectChangeList changesForSource = sortedChanges.get( sourceId );
            if( changesForSource == null ) {
                changesForSource = new SubjectChangeList();
                sortedChanges.put( sourceId, changesForSource );
            }
            changesForSource.add( new ChangeWithIndex( c, i ));
            ++i;
        }

        //

        for( SubjectChangeList changesForSource : sortedChanges.values() ) {
            changesForSource.normalize();
        }

        //
        ArrayList<Change> newChanges = new ArrayList<>();
        for( Change c : theChanges ) {
            if( c != null ) {
                newChanges.add( c );
            }
        }
        theChanges = newChanges;
    }

    /**
      * This allows additional Changes to be appended to the ChangeList.
      *
      * @param newChange the Change to be added to the ChangeList
      */
    public void addChange(
            Change newChange )
    {
        theChanges.add( newChange );
    }

    /**
      * This allows additional Changes to be appended to the ChangeList.
      *
      * @param newChanges the ChangeList of changes to be added to the ChangeList
      */
    public void append(
            ChangeList newChanges )
    {
        theChanges.addAll( newChanges.theChanges );
    }

    /**
      * Apply the changes in the ChangeList to the provided EditableMeshBaseView. This method silently
      * ignores most Exceptions in order to apply as many Changes as possible.
      *
      * @param mbv the MeshBaseView to which to apply the Changes
      * @throws MeshObjectGraphModificationException thrown if at commit time, the graph did not
      *         conform to the model
      * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
      */
    public void applyChangeListTo(
            EditableMeshBaseView mbv )
        throws
            MeshObjectGraphModificationException,
            TransactionException
    {
        for( Change current : theChanges ) {

            try {
                current.applyTo( mbv );

            } catch( CannotApplyChangeException ex ) {
                Throwable cause = ex.getCause();

                if( cause instanceof RoleTypeBlessedAlreadyException ) {
                    // do nothing, it's the other end of the same relationship
                } else if( cause instanceof BlessedAlreadyException ) {
                    log.warn( ex );

                } else if( cause instanceof NotBlessedException ) {
                    // do nothing
                } else {
                    log.error( ex );
                }
            }
        }
    }

    /**
     * Determine the set of MeshObjects affected by the Changes in this ChangeList.
     *
     * @return the set
     */
    public Set<MeshObject> getAffectedMeshObjects()
    {
        HashSet<MeshObject> ret = new HashSet<>();

        List<Change> changes = getChanges();
        for( Change change : changes ) {
            for( MeshObject affected : change.getAffectedMeshObjects() ) {
                ret.add( affected );
            }
        }
        return ret;
    }

    /**
     * Determine the identifiers of the set of MeshObjects affected by the Changes in this ChangeList.
     *
     * @return the set
     */
    public Set<MeshObjectIdentifier> getAffectedMeshObjectIdentifiers()
    {
        HashSet<MeshObjectIdentifier> ret = new HashSet<>();

        List<Change> changes = getChanges();
        for( Change change : changes ) {
            for( MeshObject affected : change.getAffectedMeshObjects() ) {
                ret.add( affected.getIdentifier() );
            }
        }
        return ret;
    }

    /**
     * Obtain in externalized form.
     *
     * @return the externalized form
     */
    public List<ExternalizedChange> asExternalized()
    {
        List<ExternalizedChange> ret = new ArrayList<>( theChanges.size() );
        for( Change current : theChanges ) {
            ret.add( current.asExternalized() );
        }
        return ret;
    }

    /**
     * Determine whether this ChangeList is empty.
     *
     * @return true if this ChangeList is empty
     */
    public boolean isEmpty()
    {
        return theChanges.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract ChangeList clone();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof ChangeList ) {
            ChangeList realOther = (ChangeList) other;

            return theChanges.equals( realOther.theChanges );
        }
        return false;
    }

    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theChanges"
                },
                new Object[] {
                    theChanges
                });
    }

    /**
     * Contains the various Changes.
     */
    protected ArrayList<Change> theChanges = new ArrayList<>();

    /**
     * Helper class that lets us normalize more easily. This is the list of Changes
     * with the same source. We also store a pointer into theChanges array to where the
     * Change came from, so we know what to patch/remove.
     */
    public class SubjectChangeList
    {
        /**
         * Normalize this SubjectChangeList and the respective parts of the underlying ChangeList.
         */
        public void normalize()
        {
            int max = theChangesWithIndex.size();
            for( int i=max-1 ; i>=0 ; --i ) {
                ChangeWithIndex currentPair = theChangesWithIndex.get( i );
                if( currentPair == null ) {
                    continue; // has been blanked already
                }
                Change currentChange = currentPair.theChange;

                currentChange.normalizePreviousInChangeList( this, i );
            }
        }

        /**
         * Obtain an element.
         *
         * @param index the index
         * @return the element
         */
        public ChangeWithIndex get(
                int index )
        {
            return theChangesWithIndex.get( index );
        }

        /**
         * Add an element at the end
         *
         * @param toAdd the element
         */
        public void add(
                ChangeWithIndex toAdd )
        {
            theChangesWithIndex.add( toAdd );
        }

        /**
         * We do not need the Change at index any more. Also update the underlying ChangeList.
         *
         * @param index the index
         */
        public void blankChangeAt(
                int index )
        {
            int changesIndex = theChangesWithIndex.get( index ).theChangesIndex;
            theChanges.set( changesIndex, null );
            theChangesWithIndex.set( index, null );
        }

        /**
         * Replace the Change at index with a different Change.
         *
         * @param index the index
         * @param newChange the new Change
         */
        public void replaceChangeAt(
                int    index,
                Change newChange )
        {
            int changesIndex = theChangesWithIndex.get( index ).theChangesIndex;
            theChanges.set( changesIndex, newChange );
            theChangesWithIndex.set( index, new ChangeWithIndex( newChange, changesIndex ));
        }

        protected List<ChangeWithIndex> theChangesWithIndex = new ArrayList<>();
    }

    /**
     * Captures a Change, and its location in the theChanges array.
     */
    static class ChangeWithIndex
    {
        public ChangeWithIndex(
                Change change,
                int    changesIndex )
        {
            theChange       = change;
            theChangesIndex = changesIndex;
        }

        protected final Change theChange;
        protected final int    theChangesIndex;
    }
}
