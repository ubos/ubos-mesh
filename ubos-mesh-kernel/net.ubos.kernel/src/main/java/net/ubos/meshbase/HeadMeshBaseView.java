//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.TransactionAction0;
import net.ubos.meshbase.transaction.TransactionAction1;
import net.ubos.meshbase.transaction.TransactionActionException;

/**
 * A MeshBaseView that reflects the HEAD version of the MeshBase.
 */
public interface HeadMeshBaseView
    extends
        EditableMeshBaseView
{
    /**
     * Obtain the HeadTransaction currently active on this EditableHistoricMeshBaseView (if any).
     *
     * @return the currently active HeadTransaction, or null if there is none
     */
    public abstract HeadTransaction getCurrentHeadTransaction();

    /**
     * Perform this TransactionAction within an automatically generated HeadTransaction
     * as soon as possible.
     *
     * @param act the TransactionAction
     * @throws TransactionActionException a problem occurred when executing the TransactionAction
     */
    public abstract void execute(
            TransactionAction0 act )
        throws
            TransactionActionException;

    /**
     * Perform this TransactionAction as root within an automatically generated HeadTransaction
     * as soon as possible.
     *
     * @param act the TransactionAction
     * @throws TransactionActionException a problem occurred when executing the TransactionAction
     */
    public abstract void sudoExecute(
            TransactionAction0 act )
        throws
            TransactionActionException;

    /**
     * Perform this TransactionAction within an automatically generated HeadTransaction
     * as soon as possible.
     *
     * @param act the TransactionAction
     * @return a TransactionAction-specific return value
     * @throws TransactionActionException a problem occurred when executing the TransactionAction
     * @param <T> the type of return value
     */
    public abstract <T> T execute(
            TransactionAction1<T> act )
        throws
            TransactionActionException;

    /**
     * Perform this TransactionAction as root within an automatically generated HeadTransaction
     * as soon as possible.
     *
     * @param act the TransactionAction
     * @return a TransactionAction-specific return value
     * @throws TransactionActionException a problem occurred when executing the TransactionAction
     * @param <T> the type of return value
     */
    public abstract <T> T sudoExecute(
            TransactionAction1<T> act )
        throws
            TransactionActionException;
}
