//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.MeshBase;

/**
  * This TransactionException is thrown to indicate that a Transaction is
  * already active and thus the asked-for new Transaction cannot be created.
  */
public class TransactionActiveAlreadyException
        extends
            TransactionException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param trans the MeshBase that was affected
     * @param blockingTransaction the Transaction that blocked the new Transaction
     */
    public TransactionActiveAlreadyException(
            MeshBase    trans,
            Transaction blockingTransaction )
    {
        super( trans, blockingTransaction );
    }
}
