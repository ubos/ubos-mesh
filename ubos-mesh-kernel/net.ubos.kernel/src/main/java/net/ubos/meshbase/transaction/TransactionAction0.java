//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

/**
 * An action that is performed within Transaction boundaries.
 */
@FunctionalInterface
public interface TransactionAction0
{
    /**
     * Execute the action. This will be invoked within valid Transaction
     * boundaries.
     *
     * @throws Throwable this declaration makes it easy to implement this method
     */
    public abstract void execute()
        throws
            Throwable;

    /**
     * Overridable callback executed just prior to attempting to perform a commit.
     *
     * @param tx the Transaction
     */
    default void preCommitTransaction(
            Transaction tx )
    {
        // no op
    }

    /**
     * Overridable callback executed just after having been successful performing a commit.
     *
     * @param tx the Transaction
     */
    default void postCommitTransaction(
            Transaction tx )
    {
        // no op
    }

    /**
     * Overridable callback executed just prior to attempting to perform a rollback.
     *
     * @param tx the Transaction
     * @param causeForRollback the Throwable that was the cause for this rollback
     */
    default void preRollbackTransaction(
            Transaction tx,
            Throwable   causeForRollback )
    {
        // no op
    }

    /**
     * Overridable callback executed just after having been successful performing a rollback.
     *
     * @param tx the Transaction
     * @param causeForRollback the Throwable that was the cause for this rollback
     */
    default void postRollbackTransaction(
            Transaction tx,
            Throwable   causeForRollback )
    {
        // no op
    }
}
