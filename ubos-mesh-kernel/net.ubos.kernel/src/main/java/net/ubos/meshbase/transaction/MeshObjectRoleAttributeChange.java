//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributeChange;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.event.PropertyChange;

/**
 * <p>This event indicates that one of the Attributes of a Role between a MeshObject and another
 * has changed its value.
 */
public class MeshObjectRoleAttributeChange
        extends
            PropertyChange<MeshObject,String,Serializable>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the object that is the source of the event
     * @param attributeName the name of the RoleAttribute
     * @param oldValue the old value of the RoleAttribute, prior to the event
     * @param newValue the new value of the RoleAttribute, after the event
     * @param neighbor the neighbor MeshObject
     */
    public MeshObjectRoleAttributeChange(
            MeshObject   source,
            String       attributeName,
            Serializable oldValue,
            Serializable newValue,
            MeshObject   neighbor )
    {
        super(  source,
                attributeName,
                oldValue,
                newValue,
                newValue );

        theNeighbor = neighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject[] { getSource(), theNeighbor };
    }

    /**
     * Obtain the neighbor MeshObject.
     *
     * @return the neighbor MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject   otherObject       = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject   otherNeighbor     = base.findMeshObjectByIdentifier( getNeighbor().getIdentifier() );
        String       roleAttributeName = getProperty();
        Serializable newValue          = getNewValue();

        otherObject.setRoleAttributeValue(
                otherNeighbor,
                roleAttributeName,
                newValue );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributeChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject updatedNeighbor   = mbv.findMeshObjectByIdentifier( theNeighbor.getIdentifier() );

        String attributeName = getProperty();

        return new MeshObjectRoleAttributeChange(
                updatedMeshObject,
                attributeName,
                updatedMeshObject.hasAttribute( attributeName ) ? updatedMeshObject.getAttributeValue( attributeName ) : null,
                theNewValue,
                updatedNeighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributeChange inverse()
    {
        return new MeshObjectRoleAttributeChange(
                getSource(),
                getProperty(),
                getNewValue(),
                getOldValue(),
                getNeighbor() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectRoleAttributeChange )) {
            return false;
        }
        MeshObjectRoleAttributeChange realCandidate = (MeshObjectRoleAttributeChange) candidate;

        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }

        if( ! theProperty.equals( realCandidate.theProperty )) {
            return false;
        }

        if( !theNeighbor.equals( realCandidate.theNeighbor )) {
            return false;
        }

        Object one = getOldValue();
        Object two = realCandidate.getNewValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        one = getNewValue();
        two = realCandidate.getOldValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectRoleAttributeChange )) {
            return false;
        }
        MeshObjectRoleAttributeChange realOther = (MeshObjectRoleAttributeChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !theProperty.equals( realOther.theProperty )) {
            return false;
        }
        if( !theNeighbor.equals(realOther.theNeighbor )) {
            return false;
        }
        if( !ArrayHelper.equals( theNewValue, realOther.theNewValue )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(
                theSource.getIdentifier(),
                theProperty,
                theNeighbor.getIdentifier(),
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectRoleAttributeChange asExternalized()
    {
        return new ExternalizedMeshObjectRoleAttributeChange(
                getSource().getIdentifier(),
                getProperty(),
                getOldValue(),
                getNewValue(),
                theNeighbor.getIdentifier() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributeChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectRoleAttributeChange(
                translation.get( theSource.getIdentifier() ),
                theProperty,
                theOldValue,
                theNewValue,
                translation.get( theNeighbor.getIdentifier() ) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        for( int i=index-1 ; i>=0 ; --i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof MeshObjectRoleAttributeChange )) {
                continue;
            }
            MeshObjectRoleAttributeChange realCurrentChange = (MeshObjectRoleAttributeChange) currentChange;
            if(    !theProperty.equals( realCurrentChange.getProperty())
                || !theNeighbor.equals( realCurrentChange.theNeighbor ))
            {
                continue;
            }

            if( Objects.equals( realCurrentChange.theOldValue, theNewValue )) {
                // can eliminate both
                list.blankChangeAt( i );
                list.blankChangeAt( index );

            } else {
                // can simplify -- we blank ourselves and replace the other
                Change newCurrentChange = new MeshObjectRoleAttributeChange( theSource, theProperty, realCurrentChange.theOldValue, theNewValue, theNeighbor );

                list.replaceChangeAt( i, newCurrentChange);
                list.blankChangeAt( index );
            }
            break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( " -> " );
        ret.append( theNeighbor.getIdentifier() );
        ret.append( ": " );
        ret.append( theProperty );
        ret.append( " = " );
        ret.append( theOldValue );
        ret.append( " >> " );
        ret.append( theNewValue );
        return ret.toString();
    }

    /**
     * The neighbor MeshObject
     */
    protected final MeshObject theNeighbor;
}
