//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m;

import java.util.function.Function;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseNameServer;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.nameserver.MNameServer;
import net.ubos.util.nameserver.WritableNameServer;

/**
 * In-memory implementation of a NameServer for MeshBases.
 */
public class MMeshBaseNameServer
        implements
            MeshBaseNameServer,
            WritableNameServer<String,MeshBase>
{
    private static final Log log = Log.getLogInstance( MMeshBaseNameServer.class ); // our own, private logger

    /**
     * Factory method. Don't set a default MeshBase.
     *
     * @return the created MMeshBaseNameServer
     */
    public static MMeshBaseNameServer create()
    {
        MNameServer<String,MeshBase> delegate = MNameServer.create();

        MMeshBaseNameServer ret = new MMeshBaseNameServer(
                null,
                delegate );

        return ret;
    }

    /**
     * Factory method. Set a default MeshBase
     *
     * @param defaultMeshBase the default MeshBase
     * @return the created MMeshBaseNameServer
     */
    public static MMeshBaseNameServer create(
            MeshBase defaultMeshBase )
    {
        MNameServer<String,MeshBase> delegate = MNameServer.create();

        MMeshBaseNameServer ret = new MMeshBaseNameServer(
                defaultMeshBase,
                delegate );

        ret.put( "", defaultMeshBase );
        return ret;
    }

    /**
     * Constructor for subclasses only, use factory method.
     *
     * @param defaultMeshBase the default MeshBase
     * @param delegate the underlying NameServer to which we delegate
     */
    protected MMeshBaseNameServer(
            MeshBase                            defaultMeshBase,
            WritableNameServer<String,MeshBase> delegate )
    {
        theDefaultMeshBase = defaultMeshBase;
        theDelegate        = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase getDefaultMeshBase()
    {
        return theDefaultMeshBase;
    }

    /**
     * Set or change the default MeshBase.
     *
     * @param mainMb the new default MeshBase
     */
    public void setDefaultMeshBase(
            MeshBase mainMb )
    {
        if( theDefaultMeshBase != null ) {
            throw new IllegalStateException( "Already have a default MeshBase" );
        }
        theDefaultMeshBase = mainMb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> keyIterator()
    {
        return theDelegate.keyIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshBase> valueIterator()
    {
        return theDelegate.valueIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase get(
            String key )
    {
        return theDelegate.get( key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        return theDelegate.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        return theDelegate.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            String   key,
            MeshBase value )
    {
        theDelegate.put( key, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            String key )
    {
        theDelegate.remove( key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase remove(
            String                  key,
            Function<MeshBase,Void> cleanupCode )
    {
        return theDelegate.remove( key, cleanupCode );
    }

    /**
     * The default MeshBase. It is also in theDelegate name server, but kept
     * here as well for faster access.
     */
    protected MeshBase theDefaultMeshBase;

    /**
     * The underlying NameServer.
     */
    protected final WritableNameServer<String,MeshBase> theDelegate;
}
