//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.meshbase.history.EditableHistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.transaction.TransactionAction0;
import net.ubos.meshbase.transaction.TransactionAction1;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.meshbase.transaction.NotWithinTransactionBoundariesException;
import net.ubos.meshbase.transaction.Transaction;

/**
 * Default implementation of historical MeshBaseViews. This is editable, because there is no harm if the default
 * implementation is editable. We can always pass the non-editable interface.
 */
public abstract class AbstractEditableHistoricMeshBaseView
    extends
        AbstractEditableMeshBaseView
    implements
        EditableHistoricMeshBaseView
{
    /**
     * Constructor for subclasses only.
     *
     * @param mb the MeshBase this MeshBaseView belongs to
     */
    protected AbstractEditableHistoricMeshBaseView(
            AbstractMeshBase     mb )
    {
        super( mb, mb );

        theMeshBase = mb;
    }

    /**
     * Set the MeshBaseState with which this view goes.
     *
     * @param newState the new MeshBaseState
     */
    public void setMeshBaseState(
            MeshBaseState newState )
    {
        if( theMeshBaseState != null ) {
            throw new IllegalStateException( "Already have a MeshBaseState" );
        }
        theMeshBaseState = newState;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBaseState getMeshBaseState()
    {
        return theMeshBaseState;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeUpdated()
    {
        return theMeshBaseState.getTimeUpdated();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryTransaction checkTransaction()
        throws
            TransactionException
    {
        HistoryTransaction tx = getCurrentHistoryTransaction();
        if( tx == null ) {
            throw new NotWithinTransactionBoundariesException( this );
        }
        return tx;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void sudoExecute(
            TransactionAction0 act )
        throws
            TransactionActionException
    {
        theMeshBase.sudoExecuteAt( theMeshBaseState.getTimeUpdated(), act );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T sudoExecute(
            TransactionAction1<T> act )
        throws
            TransactionActionException
    {
        return theMeshBase.sudoExecuteAt( theMeshBaseState.getTimeUpdated(), act );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFrozen()
    {
        return theMeshBase.isFrozen();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryTransaction getCurrentHistoryTransaction()
    {
        Transaction tx = theMeshBase.getCurrentTransaction();
        if( tx == null ) {
            return null;
        }
        if( ! tx.owns( Thread.currentThread() )) {
            return null;
        }
        if( tx instanceof HistoryTransaction ) {
            return (HistoryTransaction) tx;
        } else {
            return null;
        }
    }

    /**
     * The MeshBaseState at which this is a historic MeshBaseView.
     */
    protected MeshBaseState theMeshBaseState;
}
