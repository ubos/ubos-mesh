//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.MeshBaseStateHistory;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.StringHelper;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;

/**
 * Parses an JSON stream of MeshObjects.
 */
public abstract class AbstractMeshBaseJsonEncoder
    extends
        AbstractMeshBaseTransactionJsonEncoder
    implements
        MeshBaseJsonTags
{
    /**
     * Constructor.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected AbstractMeshBaseJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        super( typeIdSerializer );
    }

    /**
     * Bulk write the content of a MeshBase.
     *
     * @param mb the MeshBase
     * @param w the Writer to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    public void bulkWrite(
            MeshBase                                   mb,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            Writer                                     w )
        throws
            IOException,
            EncodingException
    {
        JsonWriter jw = new JsonWriter( w );
        jw.setSerializeNulls( true );
        jw.setIndent( " " );

        bulkWrite( mb, nsMap, jw );

        jw.flush();
    }

    /**
     * Bulk write the content of a MeshBase.
     *
     * @param mb the MeshBase
     * @param jw the JsonWriter to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    public void bulkWrite(
            MeshBase                                   mb,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            JsonWriter                                 jw )
        throws
            IOException,
            EncodingException
    {
        ContextualMeshObjectIdentifierBothSerializer idSerializer
                = ContextualMeshObjectIdentifierBothSerializer.create( mb, nsMap );

        jw.beginObject();

        jw.name( HEADER_SECTION_TAG );
        writeHeader( mb, nsMap, jw );

        jw.name( MESHOBJECTS_SECTION_TAG );
        writeMeshObjects( mb, idSerializer, jw );

        if( theExportTransactions ) {
            jw.name( TRANSACTIONS_SECTION_TAG );
            writeTransactions( mb, idSerializer, jw );
        }
        jw.endObject();
    }

    /**
     * Write the header of the export file. This also produces a map
     * of MeshObjectIdentifierNamespace to local name, which is used
     * in the main body of the export for MeshObjectIdentifiers
     *
     * @param mb the MeshBase
     * @param nsMap track the namespaces and their local names here
     * @param jw the JsonWriter to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    protected void writeHeader(
            MeshBase                                   mb,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            JsonWriter                                 jw )
        throws
            IOException,
            EncodingException
    {
        jw.beginObject();
        jw.name( FORMAT_TAG ).value( FORMAT_TAG_VALUE );
        jw.name( VERSION_TAG ).value( VERSION_TAG_VALUE );

        MeshBaseStateHistory history = mb.getHistory();
        if( history != null ) {
            long mostRecentUpdated = history.current().getTimeUpdated();
            jw.name( MOST_RECENT_UPDATE_TAG ).value( mostRecentUpdated );
        }

        // This can certainly be done more efficiently wrt memory
        HashSet<SubjectArea> sas = new HashSet<>();

        nsMap.register( ContextualMeshObjectIdentifierNamespaceMap.DEFAULT_LOCAL_NAME, mb.getDefaultNamespace() ); // keep the default namespace of the MeshBase in the export

        for( MeshObject current : mb ) {
            for( EntityType current2 : current.getEntityTypes() ) {
                sas.add( current2.getSubjectArea() );
            }
            for( RoleType current2 : current.getRoleTypes() ) {
                sas.add( current2.getSubjectArea() );
            }
            MeshObjectIdentifierNamespace ns = current.getIdentifier().getNamespace();
            nsMap.obtainLocalNameFor( ns ); // registers if needed
        }

        CursorIterator<String> localNameIter = nsMap.localNameIterator();
        if( localNameIter.hasNext() ) {
            jw.name( MESHOBJECTIDENTIFIERNAMESPACES_TAG );
            jw.beginObject();

            while( localNameIter.hasNext() ) {
                String                        localName = localNameIter.next();
                MeshObjectIdentifierNamespace ns        = nsMap.findByLocalName( localName );

                jw.name( localName );
                jw.beginObject();

                if( ns.getPreferredExternalName() != null ) {
                    jw.name( PREFERRED_EXTERNALNAME_TAG );
                    jw.value( ns.getPreferredExternalName() );
                }

                if( ns.getExternalNames().size() > 1 ) {
                    jw.name( EXTERNALNAMES_TAG );
                    jw.beginArray();
                    for( String externalName : ns.getExternalNames()) {
                        if( StringHelper.compareTo( externalName, ns.getPreferredExternalName() ) != 0 ) {
                            jw.value( externalName );
                        }
                    }
                    jw.endArray();
                }
                jw.endObject();
            }
            jw.endObject();
        }
        if( !sas.isEmpty() ) {
            jw.name( SUBJECTAREAS_TAG );
            jw.beginObject();

            for( SubjectArea current : sas ) {
                jw.name( theTypeIdSerializer.toExternalForm( current.getIdentifier() ));
                jw.beginObject();
                // we want the ability to put more stuff there, like SubjectArea version
                jw.endObject();
            }
            jw.endObject();
        }
        jw.endObject();
    }

    /**
     * Write the body of the export file that contains the MeshObjects.
     *
     * @param mb the MeshBase
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param jw the JsonWriter to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    protected void writeMeshObjects(
            MeshBase                       mb,
            MeshObjectIdentifierSerializer idSerializer,
            JsonWriter                     jw )
        throws
            IOException,
            EncodingException
    {
        jw.beginObject();

        CursorIterator<MeshObjectHistory> meshObjectHistoriesIter = mb.meshObjectHistoriesIterator();

        if( theExportHistory && meshObjectHistoriesIter != null ) {
            for( MeshObjectHistory history : meshObjectHistoriesIter ) {
                MeshObjectIdentifier         id      = history.getMeshObjectIdentifier();
                MeshObject                   current = mb.findMeshObjectByIdentifier( id );
                List<ExternalizedMeshObject> oldExt  = new ArrayList<>( history.getLength() );

                HistoryCursorIterator<MeshObject> historyIter = history.getIterator();
                while( historyIter.hasNext() ) {
                    MeshObject histCurrent = historyIter.next();
                    if( !histCurrent.getIsDead() && !historyIter.hasNext() ) { // skip the last one if that's in HEAD
                        break;
                    }
                    oldExt.add( histCurrent.asExternalized() );
                }

                jw.name( idSerializer.toExternalForm( id ));

                writeExternalizedMeshObject(
                        current == null ? null : current.asExternalized(),
                        false,
                        oldExt.isEmpty() ? null : oldExt,
                        idSerializer,
                        jw );
            }
        } else {
            for( MeshObject current : mb ) {
                jw.name( idSerializer.toExternalForm( current.getIdentifier() ));

                writeExternalizedMeshObject( current.asExternalized(), false, null, idSerializer, jw );
            }
        }
        jw.endObject();
    }

    /**
     * Write the body of the export file that contains the Transactions.
     *
     * @param mb the MeshBase
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param jw the JsonWriter to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    protected void writeTransactions(
            MeshBase                       mb,
            MeshObjectIdentifierSerializer idSerializer,
            JsonWriter                     jw )
        throws
            IOException,
            EncodingException
    {
        jw.beginArray();

        for( MeshBaseState state : mb.getHistory() ) {
            writeTransaction( state.getTimeUpdated(), state.getChangeList(), idSerializer, jw );
        }

        jw.endArray();
    }

    /**
     * Do we export MeshObjectHistories.
     */
    protected boolean theExportHistory = false;

    /**
     * Do we export Transactions.
     */
    protected boolean theExportTransactions = false;


    /**
     * Knows how to serialize MeshTypeIdentifiers.
     */
    protected static final MeshTypeIdentifierSerializer theTypeIdSerializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;
}
