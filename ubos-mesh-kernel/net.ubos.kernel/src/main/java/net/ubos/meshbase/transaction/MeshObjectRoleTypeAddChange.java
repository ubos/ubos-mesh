//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import java.util.Objects;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleTypeAddChange;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.util.ArrayHelper;

/**
 * <p>This event indicates that a relationship between the MeshObject and
 * another MeshObject was blessed.
 */
public class MeshObjectRoleTypeAddChange
        extends
            AbstractMeshObjectRoleTypeChange
{
    /**
     * Pass-through constructor for subclasses.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old values of the RoleType, prior to the event
     * @param deltaValues the RoleTypes that changed
     * @param newValues the new values of the RoleType, after the event
     * @param neighbor the MeshObject that identifies the other end of the affected relationship
     */
    public MeshObjectRoleTypeAddChange(
            MeshObject  source,
            RoleType [] oldValues,
            RoleType [] deltaValues,
            RoleType [] newValues,
            MeshObject  neighbor )
    {
        super(  source,
                oldValues,
                deltaValues,
                newValues,
                neighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject  otherObject        = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject  relatedOtherObject = base.findMeshObjectByIdentifier( getNeighbor().getIdentifier() );
        RoleType [] roleTypes          = getDeltaValue();

        otherObject.blessRole( roleTypes, relatedOtherObject );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleTypeAddChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject  updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject  updatedNeighbor   = mbv.findMeshObjectByIdentifier( theNeighbor.getIdentifier() );
        RoleType [] oldTypes          = updatedMeshObject.getRoleTypes( theNeighbor );
        RoleType [] deltaTypes        = theDeltaValue;
        RoleType [] newTypes          = ArrayHelper.append( oldTypes, deltaTypes, RoleType.class );

        return new MeshObjectRoleTypeAddChange(
                updatedMeshObject,
                oldTypes,
                deltaTypes,
                newTypes,
                updatedNeighbor );
    }

    /**
     * <p>Create a Change that undoes this Change.
     *
     * @return the inverse Change, or null if no inverse Change could be constructed.
     */
    @Override
    public MeshObjectRoleTypeRemoveChange inverse()
    {
        return new MeshObjectRoleTypeRemoveChange(
                getSource(),
                getNewValue(),
                getDeltaValue(),
                getOldValue(),
                getNeighbor() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectRoleTypeRemoveChange )) {
            return false;
        }

        MeshObjectRoleTypeRemoveChange realCandidate = (MeshObjectRoleTypeRemoveChange) candidate;
        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }
        if( !getNeighbor().equals( realCandidate.getNeighbor())) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(),  realCandidate.getDeltaValue(), true )) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectRoleTypeAddChange )) {
            return false;
        }
        MeshObjectRoleTypeAddChange realOther = (MeshObjectRoleTypeAddChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !theNeighbor.equals(realOther.theNeighbor )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realOther.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = Objects.hash(
                theSource.getIdentifier(),
                theNeighbor.getIdentifier());

        for( RoleType rt : getDeltaValue() ) {
            ret ^= rt.getIdentifier().hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectRoleTypeAddChange asExternalized()
    {
        return new ExternalizedMeshObjectRoleTypeAddChange(
                getSource().getIdentifier(),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getOldValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getDeltaValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getNewValue() ),
                theNeighbor.getIdentifier());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleTypeAddChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectRoleTypeAddChange(
                translation.get( theSource.getIdentifier() ),
                theOldValue,
                theDeltaValue,
                theNewValue,
                translation.get( theNeighbor.getIdentifier() ) );
    }
}
