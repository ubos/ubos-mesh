//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.io.IOException;
import java.io.Writer;
import net.ubos.mesh.externalized.Encoder;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;

/**
 * Knows how to encode/decode a transaction performed on a MeshBase
 */
public interface MeshBaseTransactionEncoder
    extends
        Encoder
{
    /**
     * Write a MeshBaseState to a Writer.
     *
     * @param timeUpdated the time when the transaction was successful
     * @param changes the Changes made during the transaction
     * @param idSerializer the serializer to use
     * @param w the Writer to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    public void writeTransaction(
            long                           timeUpdated,
            ChangeList                     changes,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            EncodingException,
            IOException;
}
