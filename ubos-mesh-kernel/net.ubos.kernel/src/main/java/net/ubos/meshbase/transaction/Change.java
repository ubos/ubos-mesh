//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.model.primitives.SubjectArea;

/**
 * This interface represents the concept of a Change that may occur to a
 * {@link net.ubos.mesh.meshbase.MeshBase} during a {@link Transaction}. Any instance of Change
 * represents an "elemental" change, there are no composites.
 */
public interface Change
{
    /**
     * Obtain the identifier of the source MeshObject.
     *
     * @return the source MeshObjectIdentifier
     */
    public abstract MeshObjectIdentifier getSourceMeshObjectIdentifier();

    /**
     * Obtain the MeshObjects affected by this Change. Generally, relationship changes
     * affect two MeshObjects. Most other Changes affect one MeshObject.
     *
     * @return obtain the MeshObjects affected by this Change
     */
    public abstract MeshObject [] getAffectedMeshObjects();

    /**
     * Obtain the identifiers of the MeshObjects affected by this Change.
     * This is provided because when MeshObjects are deleted, and recreated
     * (e.g. during rollback of a transaction), new instances with the
     * same identifier are created.
     *
     * @return the identifiers of the MeshObjects affected by this Change
     */
    public default MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        MeshObject []           affected = getAffectedMeshObjects();
        MeshObjectIdentifier [] ret      = new MeshObjectIdentifier[ affected.length ];

        for( int i=0 ; i<affected.length ; ++i ) {
            ret[i] = affected[i].getIdentifier();
        }
        return ret;
    }

    /**
     * Determine whether this Change affects the MeshObject with the provided identifier.
     *
     * @param identifier the MeshObjectIdentifier of the potentially affected MeshObject
     * @return true if this Change affects the MeshObject with this identifier
     */
    public default boolean affects(
            MeshObjectIdentifier identifier )
    {
        MeshObject [] affecteds = getAffectedMeshObjects();
        for( MeshObject affected : affecteds ) {
            if( affected.getIdentifier().equals( identifier )) {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>Apply this Change to a MeshObject in this EditableMeshBaseView. This method
     *    is intended to make it easy to reproduce Changes that were made in
     *    one MeshBase to MeshObjects in another MeshBase.
     *
     * <p>This method will attempt to create a Transaction if none is present on the
     * current Thread.
     *
     * @param base the EditableMeshBaseView in which to apply the Change
     * @return the MeshObject to which the Change was applied
     * @throws CannotApplyChangeException thrown if the Change could not be applied, e.g because
     *         the affected MeshObject did not exist in MeshBase base
     * @throws MeshObjectGraphModificationException thrown if at commit time, the graph did not
     *         conform to the model
     * @throws TransactionException thrown if a Transaction didn't exist on this Thread and
     *         could not be created
     */
    public abstract MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException;

    /**
     * Create a new instance of the same subtype of this Change that represents the same Change,
     * but starting with MeshObjects in the provided MeshBaseView. This is used when rippling
     * historical changes forward, where suddenly the old version of a MeshObject is different
     * than what the Change expected. This will keep as much of the original meaning of the
     * Change as possible.
     *
     * @param mbv the MeshBaseView containing the MeshObjects on which this Change is supposed to be applied now
     * @return the new Change
     */
    public Change createButFrom(
           MeshBaseView mbv );

    /**
     * <p>Create a Change that undoes this Change.
     *
     * @return the inverse Change, or null if no inverse Change could be constructed.
     */
    public abstract Change inverse();

    /**
     * Determine whether a given Change is the inverse of this Change.
     *
     * @param candidate the candidate Change
     * @return true if the candidate Change is the inverse of this Change
     */
    public abstract boolean isInverse(
            Change candidate );

    /**
     * Obtain the same Change as ExternalizedChange so it can be easily serialized.
     *
     * @return this Change as ExternalizedChange
     */
    public abstract ExternalizedChange asExternalized();

    /**
     * Add the SubjectAreas referenced by this Change to the provided Set.
     * This is so we can declare the set of required SubjectAreas before a node attempts
     * to process an incoming Chnage.
     *
     * @param sas the Set in which the SubjectAreas are collected
     */
    public abstract void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas );

    /**
     * Obtain an equivalent Change, but swapping out the instances of affected MeshObjects.
     * This operation is only used to insert "historic" MeshObjects into the Changes
     * kept by a MeshBaseStateHistory, instead of referring to the "head" MeshObjects.
     *
     * @param translation a map from the MeshObjectIdentifiers of the (old) affected MeshObjects,
     *                    to the MeshObjects to be used instead
     * @return the equivalent Change
     */
    public Change withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation );

    /**
     * Implementation method that allows this Change to remove other Changes prior to itself (and potentially itself)
     * or replace those Changes in order to normalize a ChangeList.
     *
     * @param list the change list for a given subject
     * @param index the index of this Change in the list
     */
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index );
}
