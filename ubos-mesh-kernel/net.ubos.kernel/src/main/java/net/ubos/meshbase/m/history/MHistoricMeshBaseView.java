//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.AbstractEditableHistoricMeshBaseView;
import net.ubos.meshbase.a.AMeshBase;
import net.ubos.meshbase.security.ByAccessFilteringCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;

/**
 * A memory-only implementation of historical MeshBaseViews.
 */
public class MHistoricMeshBaseView
    extends
        AbstractEditableHistoricMeshBaseView
{
    private static final Log log = Log.getLogInstance( MHistoricMeshBaseView.class );

    /**
     * Factory method.
     *
     * @param mb the MeshBase this MeshBaseView belongs to
     * @return the created object
     */
    public static MHistoricMeshBaseView create(
            AMeshBase mb )
    {
        MHistoricMeshBaseViewRepository meshObjectRepo = new MHistoricMeshBaseViewRepository();

        MHistoricMeshBaseView ret = new MHistoricMeshBaseView( meshObjectRepo, mb );
        meshObjectRepo.setMeshBaseView( ret );

        return ret;
    }

    /**
     * Constructor, use factory method.
     *
     * @param meshObjectRepo smartly contains the MeshObjects in this MeshBaseView
     * @param mb the MeshBase this MeshBaseView belongs to
     */
    protected MHistoricMeshBaseView(
            MHistoricMeshBaseViewRepository meshObjectRepo,
            AMeshBase                       mb )
    {
        super( mb );

        theRepository = meshObjectRepo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getHomeObject()
    {
        MeshObject ret = theRepository.get( getHomeMeshObjectIdentifier() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        boolean ret = theRepository.containsKey( identifier );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject findMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        MeshObject ret = theRepository.get( identifier );

        try {
            getMeshBase().getAccessManager().checkPermittedAccess( ret );
            return ret;

        } catch( NotPermittedException ex ) {
            log.debug( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putNewMeshObject(
            MeshObject toAdd )
    {
        theRepository.put( toAdd.getIdentifier(), toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeMeshObject(
            MeshObjectIdentifier objId )
    {
        theRepository.remove( objId );
    }

    /**
     * Access the underlying repository
     *
     * @return the underlying MHistoricMeshBaseViewCache
     */
    public MHistoricMeshBaseViewRepository getMeshObjectStorage()
    {
        return theRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> changedMeshObjectsIterator()
    {
        ByAccessFilteringCursorIterator ret = ByAccessFilteringCursorIterator.create(
                theRepository.changedMeshObjectsIterator(),
                getMeshBase().getAccessManager() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void purgeClonedMeshObjectsCache()
    {
        theRepository.purgeClonedMeshObjectsCache();
    }

    /**
     * For debugging.
     *
     * @return String representation
     */
    @Override
    public String toString()
    {
        return getClass().getName()
                + "(timeUpdated:"
                + ( theMeshBaseState == null ? "???" : theMeshBaseState.getTimeUpdated())
                + ")";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> iterator()
    {
        ByAccessFilteringCursorIterator ret = ByAccessFilteringCursorIterator.create(
                theRepository.valueIterator(),
                getMeshBase().getAccessManager() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject[] createArray(
            int len )
    {
        return theMeshBase.createArray( len );
    }

    /**
     * The storage for the MeshBaseView.
     */
    protected final MHistoricMeshBaseViewRepository theRepository;
}
