//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.AbstractHeadMeshBaseView;
import net.ubos.meshbase.security.ByAccessFilteringCursorIterator;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * The MeshBaseView that contains the head version of the MeshObjects in a MeshBase.
 */
public class MHeadMeshBaseView
    extends
        AbstractHeadMeshBaseView
{
    private static final Log log = Log.getLogInstance( MHeadMeshBaseView.class );

    /**
     * Factory method.
     *
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBaseView
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     * @return the created MHeadMeshBaseView
     */
    public static MHeadMeshBaseView create(
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        MHeadMeshBaseView ret = new MHeadMeshBaseView( identifierFactory, idDeserializer );
        return ret;
    }

    /**
     * Constructor.
     *
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBaseView
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     */
    public MHeadMeshBaseView(
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        super( identifierFactory, idDeserializer );
    }

    /**
     * Delete all content. This is (very) destructive.
     */
    protected void clear()
    {
        theStorage.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putNewMeshObject(
            MeshObject obj )
    {
        theStorage.put( obj.getIdentifier(), obj );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeMeshObject(
            MeshObjectIdentifier objId )
    {
        theStorage.remove( objId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getHomeObject()
    {
        MeshObject home = theStorage.get( getHomeMeshObjectIdentifier() );
        return home;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        return theStorage.containsKey( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject findMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        MeshObject ret = theStorage.get( identifier );
        try {
            getMeshBase().getAccessManager().checkPermittedAccess( ret );
            return ret;

        } catch( NotPermittedException ex ) {
            log.debug( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> iterator()
    {
        ByAccessFilteringCursorIterator ret = ByAccessFilteringCursorIterator.create(
                theStorage.valueIterator(),
                getMeshBase().getAccessManager() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject[] createArray(
            int len )
    {
        return theMeshBase.createArray( len );
    }

    /**
     * The underlying storage.
     */
    protected final SmartMap<MeshObjectIdentifier,MeshObject> theStorage = MSmartMap.create();
}
