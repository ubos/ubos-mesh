//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.history;

import net.ubos.meshbase.AbstractEditableHistoricMeshBaseView;
import net.ubos.meshbase.transaction.ChangeList;

/**
 * A MeshBaseState that can be changed after the fact.
 * This should not be invoked by the application programmer.
 */
public abstract class AbstractEditableHistoricMeshBaseState
    extends
        MeshBaseState
{
    /**
     * Constructor.
     *
     * @param timeUpdated the time when this state begun, in System.currentTimeMillis() format
     * @param changes the Changes that led to this state, if known
     * @param meshBaseView the content of the MeshBase at this time
     */
    protected AbstractEditableHistoricMeshBaseState(
            long                                 timeUpdated,
            ChangeList                           changes,
            AbstractEditableHistoricMeshBaseView meshBaseView )
    {
        super( timeUpdated, changes );

        theMeshBaseView = meshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractEditableHistoricMeshBaseView getMeshBaseView()
    {
        return theMeshBaseView;
    }

    /**
     * Set a new ChangeList.
     *
     * @param newChanges the new ChangeList
     */
    public void updateChangeList(
            ChangeList newChanges )
    {
        theChanges = newChanges;
    }

    /**
     * The MeshObjects at this time.
     */
    protected final AbstractEditableHistoricMeshBaseView theMeshBaseView;
}
