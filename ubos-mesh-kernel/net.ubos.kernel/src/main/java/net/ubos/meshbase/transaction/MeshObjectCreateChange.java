//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import net.ubos.mesh.AbstractMeshObject;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectCreateChange;

/**
 * This event indicates that a new MeshObject was created.
 */
public class MeshObjectCreateChange
        extends
            AbstractMeshObjectLifecycleChange
{
    /**
     * Constructor.
     *
     * @param createdObject the MeshObject that was created
     * @param createdObjectId the identifier of the MeshObject that was created
     */
    public MeshObjectCreateChange(
            MeshObject           createdObject,
            MeshObjectIdentifier createdObjectId )
    {
        super( createdObject, createdObjectId );
    }

    /**
     * Constructor.
     *
     * @param identifier the identifier of the MeshObject that was created
     */
    public MeshObjectCreateChange(
            MeshObjectIdentifier identifier )
    {
        super( null, identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject newObject = base.createMeshObject( theChangedObjectIdentifier );

        if( theChangedObject != null ) {
            AbstractMeshObject realNewObject = (AbstractMeshObject) newObject;

            realNewObject.setTimeCreated( theChangedObject.getTimeCreated());
            realNewObject.setTimeUpdated( theChangedObject.getTimeUpdated());
        }

        return newObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectCreateChange createButFrom(
            MeshBaseView mbv )
    {
        return new MeshObjectCreateChange(
                null,
                theChangedObjectIdentifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectDeleteChange inverse()
    {
        return new MeshObjectDeleteChange(
                theChangedObject,
                theChangedObjectIdentifier );
    }

    /**
     * Determine whether a given Change is the inverse of this Change.
     *
     * @param candidate the candidate Change
     * @return true if the candidate Change is the inverse of this Change
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectDeleteChange )) {
            return false;
        }
        MeshObjectDeleteChange realCandidate = (MeshObjectDeleteChange) candidate;

        if( !theChangedObjectIdentifier.equals( realCandidate.theChangedObjectIdentifier )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectCreateChange )) {
            return false;
        }
        MeshObjectCreateChange realOther = (MeshObjectCreateChange) other;

        if( !theChangedObjectIdentifier.equals( realOther.theChangedObjectIdentifier )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theChangedObjectIdentifier.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectCreateChange asExternalized()
    {
        return new ExternalizedMeshObjectCreateChange(
                getChangedMeshObjectIdentifier() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectCreateChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectCreateChange(
                translation.get( theChangedObject.getIdentifier() ),
                theChangedObjectIdentifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        for( int i=index-1 ; i>=0 ; --i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof MeshObjectDeleteChange )) {
                continue;
            }
            // can eliminate both
            list.blankChangeAt( i );
            list.blankChangeAt( index );
            break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theChangedObjectIdentifier );
        return ret.toString();
    }
}
