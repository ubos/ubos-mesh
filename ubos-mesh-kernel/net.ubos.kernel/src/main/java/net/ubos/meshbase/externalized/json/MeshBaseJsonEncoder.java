//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import net.ubos.meshbase.externalized.MeshBaseEncoder;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;

/**
 * Separate class to avoid coding mistakes.
 */
public final class MeshBaseJsonEncoder
    extends
        AbstractMeshBaseJsonEncoder
    implements
        MeshBaseEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static MeshBaseJsonEncoder create()
    {
        return new MeshBaseJsonEncoder( DefaultMMeshTypeIdentifierBothSerializer.SINGLETON );
    }

    /**
     * Constructor, use factory method.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected MeshBaseJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        super( typeIdSerializer );
    }

    /**
     * Set whether we export the history of MeshObjects.
     *
     * @param exportHistory true or false
     */
    public void setExportHistory(
            boolean exportHistory )
    {
        theExportHistory = exportHistory;
    }

    /**
     * Set whether we export Transactions.
     *
     * @param exportTransactions true or false
     */
    public void setExportTransactions(
            boolean exportTransactions )
    {
        theExportTransactions = exportTransactions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_BASE_JSON_ENCODING_ID;
    }
}
