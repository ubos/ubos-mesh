//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectAttributesAddChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;

/**
 *
 */
public class ExternalizedMeshObjectAttributesAddChange
    extends
        AbstractExternalizedMeshObjectAttributesChange
{
    public ExternalizedMeshObjectAttributesAddChange(
            MeshObjectIdentifier sourceIdentifier,
            String []            oldValues,
            String []            deltaValues,
            String []            newValues )
    {
        super( sourceIdentifier, oldValues, deltaValues, newValues );
    }

    public String [] getAdded()
    {
        return theDeltaValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject source = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );

            source.setAttributeValues( theDeltaValues, new Serializable[ theDeltaValues.length ] );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesAddChange internalizeWith(
            MeshObject [] affectedMeshObjects )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        if( affectedMeshObjects.length != 1 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectAttributesAddChange( affectedMeshObjects[0], theOldValues, theDeltaValues, theNewValues );
    }
}
