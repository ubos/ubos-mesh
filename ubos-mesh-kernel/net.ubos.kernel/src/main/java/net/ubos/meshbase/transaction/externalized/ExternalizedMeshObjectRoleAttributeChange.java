//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributeChange;
import net.ubos.meshbase.transaction.TransactionException;

/**
 *
 */
public class ExternalizedMeshObjectRoleAttributeChange
    extends
        AbstractExternalizedChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier the identifier of the MeshObject that is the source of the event
     * @param attributeName the name of the RoleAttribute
     * @param oldValue the old value of the RoleAttribute, prior to the event
     * @param newValue the new value of the RoleAttribute, after the event
     * @param neighborIdentifier the identifier of the neighbor MeshObject
     */
    public ExternalizedMeshObjectRoleAttributeChange(
            MeshObjectIdentifier sourceIdentifier,
            String               attributeName,
            Serializable         oldValue,
            Serializable         newValue,
            MeshObjectIdentifier neighborIdentifier )
    {
        super( sourceIdentifier );

        theAttributeName      = attributeName;
        theOldValue           = oldValue;
        theNewValue           = newValue;
        theNeighborIdentifier = neighborIdentifier;
    }

    public String getAttributeName()
    {
        return theAttributeName;
    }

    public Serializable getOldValue()
    {
        return theOldValue;
    }

    public Serializable getNewValue()
    {
        return theNewValue;
    }

    public MeshObjectIdentifier getNeighborIdentifier()
    {
        return theNeighborIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier, theNeighborIdentifier };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject source   = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );
            MeshObject neighbor = base.findMeshObjectByIdentifierOrThrow( theNeighborIdentifier );

            source.setRoleAttributeValue( neighbor, theAttributeName, theNewValue );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributeChange internalizeWith(
            MeshObject [] affectedMeshObjects )
    {
        if( affectedMeshObjects.length != 2 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectRoleAttributeChange( affectedMeshObjects[0], theAttributeName, theOldValue, theNewValue, affectedMeshObjects[1] );
    }

    protected final String theAttributeName;
    protected final Serializable theOldValue;
    protected final Serializable theNewValue;
    protected final MeshObjectIdentifier theNeighborIdentifier;
}
