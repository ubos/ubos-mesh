//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.peertalk.externalized.json;

import net.ubos.meshbase.transaction.externalized.json.ChangeListJsonTags;

/**
 * JSON tags for PeerTalk over JSON
 */
public interface PeerTalkJsonTags
    extends
        ChangeListJsonTags
{
    /** Encoding ID */
    public static final String PEER_TALK_JSON_ENCODING_ID
            = PeerTalkJsonTags.class.getPackage().getName();

    /** Tag indicating the header section. */
    public static final String HEADER_TAG = "Meta";

    /** Tag indicating the item in the header that is the file's type. */
    public static final String FORMAT_TAG = "Format";

    /** Value for the format. */
    public static final String FORMAT_TAG_VALUE = "PeerTalk";

    /** Tag indicating the item in the header that is the file's version */
    public static final String VERSION_TAG = "Version";

    /** Value for the current version. */
    public static final String VERSION_TAG_VALUE = "0.1";

    /** Tag indicating the section in the header that lists the SubjectAreas. */
    public static final String SUBJECTAREAS_TAG = "SubjectAreas";

    /** Tag indicating the section in the header that lists the MeshObjectIdentifierNamespaces */
    public static final String MESHOBJECTIDENTIFIERNAMESPACES_TAG = "MeshObjectIdentifierNamespaces";

    /** Tag indicating the preferred external name of a MeshObjectIdentifierNamespace */
    public static final String PREFERRED_EXTERNALNAME_TAG = "preferredExternalName";

    /** Tag indicating the array of external names of a MeshObjectIdentifierNamespace */
    public static final String EXTERNALNAMES_TAG = "externalNames";
}
