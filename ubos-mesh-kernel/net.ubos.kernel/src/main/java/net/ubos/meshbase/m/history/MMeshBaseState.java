//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m.history;

import net.ubos.meshbase.AbstractEditableHistoricMeshBaseView;
import net.ubos.meshbase.history.AbstractEditableHistoricMeshBaseState;
import net.ubos.meshbase.transaction.ChangeList;

/**
 * Implementation of MeshBaseState for MMeshBase.
 */
public class MMeshBaseState
    extends
        AbstractEditableHistoricMeshBaseState
{
    /**
     * Factory method, for the case where the MeshBaseState is created as part of a new Transaction.
     *
     * @param time the time when this state begun, in System.currentTimeMillis() format
     * @param changes the Changes that led to this state
     * @param meshBaseView the content of the MeshBase at this time
     * @return the created MeshBaseState
     */
    public static MMeshBaseState createWithChanges(
            long                         time,
            ChangeList                   changes,
            AbstractEditableHistoricMeshBaseView meshBaseView )
    {
        return new MMeshBaseState( time, changes, meshBaseView );
    }

    /**
     * Constructor.
     *
     * @param timeUpdated the time when this state begun, in System.currentTimeMillis() format
     * @param changes the Changes that led to this state, if known
     * @param meshBaseView the content of the MeshBase at this time
     */
    protected MMeshBaseState(
            long                         timeUpdated,
            ChangeList                   changes,
            AbstractEditableHistoricMeshBaseView meshBaseView )
    {
        super( timeUpdated, changes, meshBaseView );
    }

    /**
     * Add some Changes. This is an internal method.
     *
     * @param toAdd changes to add
     */
    public void appendChanges(
            ChangeList toAdd )
    {
        ensureChangesResolved();
        theChanges.append( toAdd );
    }

    /**
     * Set the Changes. This is an internal method.
     *
     * @param toSet changes to set
     */
    public void setChanges(
            ChangeList toSet )
    {
        theChanges = toSet;
    }
}
