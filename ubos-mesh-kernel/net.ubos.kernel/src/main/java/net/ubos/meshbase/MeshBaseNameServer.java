//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.util.nameserver.NameServer;

/**
 * This interface is implemented by objects that can look up MeshBases from
 * their MeshBaseIdentifier.
 */
public interface MeshBaseNameServer
    extends
        NameServer<String,MeshBase>
{
    /**
     * Obtain the default MeshBase
     *
     * @return the default MeshBase
     */
    public abstract MeshBase getDefaultMeshBase();
}
