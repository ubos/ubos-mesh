//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.peertalk.externalized.json;

import net.ubos.meshbase.peertalk.externalized.PeerTalkEncoder;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;

/**
 * Separate class to avoid coding mistakes.
 */
public final class PeerTalkJsonEncoder
    extends
        AbstractPeerTalkJsonEncoder
    implements
        PeerTalkEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static PeerTalkJsonEncoder create()
    {
        return new PeerTalkJsonEncoder( DefaultMMeshTypeIdentifierBothSerializer.SINGLETON );
    }

    /**
     * Constructor, use factory method.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected PeerTalkJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        super( typeIdSerializer );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return PEER_TALK_JSON_ENCODING_ID;
    }
}
