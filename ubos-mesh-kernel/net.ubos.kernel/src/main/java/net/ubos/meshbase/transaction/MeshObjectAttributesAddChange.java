//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.io.Serializable;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributesAddChange;
import net.ubos.util.ArrayHelper;

/**
 * <p>This event indicates that at least one Attribute was added to a MeshObject.
 */
public class MeshObjectAttributesAddChange
        extends
            AbstractMeshObjectAttributesChange
{
    /**
     * Pass-through constructor for subclasses.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old values of the names of the Attributes, prior to the event
     * @param deltaValues the names of the Attributes that changed
     * @param newValues the new values of the names of the Attributes, after the event
     */
    public MeshObjectAttributesAddChange(
            MeshObject source,
            String []  oldValues,
            String []  deltaValues,
            String []  newValues )
    {
        super(  source,
                oldValues,
                deltaValues,
                newValues );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject      otherObject    = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        String []       attributeNames = getDeltaValue();
        Serializable [] initialValues  = new Serializable[ attributeNames.length ];

        otherObject.setAttributeValues( attributeNames, initialValues );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesAddChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject   = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        String []  oldAttributeNames   = updatedMeshObject.getAttributeNames();
        String []  deltaAttributeNames = theDeltaValue;
        String []  newAttributeNames   = ArrayHelper.append( oldAttributeNames, deltaAttributeNames, String.class );

        return new MeshObjectAttributesAddChange(
                updatedMeshObject,
                oldAttributeNames,
                deltaAttributeNames,
                newAttributeNames );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesRemoveChange inverse()
    {
        return new MeshObjectAttributesRemoveChange(
                getSource(),
                getNewValue(),
                getDeltaValue(),
                getOldValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectAttributesRemoveChange )) {
            return false;
        }

        MeshObjectAttributesRemoveChange realCandidate = (MeshObjectAttributesRemoveChange) candidate;
        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }
        if( !getDeltaValue().equals( realCandidate.getDeltaValue())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectAttributesAddChange )) {
            return false;
        }
        MeshObjectAttributesAddChange realOther = (MeshObjectAttributesAddChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realOther.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = theSource.getIdentifier().hashCode();
        for( String name : theDeltaValue ) {
            ret ^= name.hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectAttributesAddChange asExternalized()
    {
        return new ExternalizedMeshObjectAttributesAddChange(
                getSource().getIdentifier(),
                getOldValue(),
                getDeltaValue(),
                getNewValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributesAddChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectAttributesAddChange(
                translation.get( theSource.getIdentifier() ),
                theOldValue,
                theDeltaValue,
                theNewValue );
    }
}
