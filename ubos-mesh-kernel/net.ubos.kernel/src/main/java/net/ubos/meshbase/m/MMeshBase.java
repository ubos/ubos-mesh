//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.a.AMeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObjectIdentifierFactory;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.m.ImmutableMMeshObjectSetFactory;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.AbstractHeadMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.a.AChangeList;
import net.ubos.meshbase.a.AMeshBase;
import net.ubos.meshbase.a.AMeshBaseLifecycleManager;
import net.ubos.meshbase.a.ATransaction;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.m.history.MHistoricMeshBaseView;
import net.ubos.meshbase.m.history.MMeshBaseState;
import net.ubos.meshbase.m.history.MMeshBaseStateMeshObjectHistory;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.security.PermitAllAccessManager;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.ChangeUtils;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;
import net.ubos.util.logging.Log;

/**
  * This MeshBase is only held in memory. It has no persistence whatsoever.
  */
public class MMeshBase
        extends
            AMeshBase
{
    private static final Log log = Log.getLogInstance( MMeshBase.class ); // our own, private logger

    /**
     * Builder class, use to instantiate MMeshBase.
     */
    public static class Builder
    {
        /**
         * Create the Builder.
         *
         * @return the Builder.
         */
        public static Builder create()
        {
            return new Builder();
        }

        protected Builder() {}

        /**
         * MMeshBase factory method.
         *
         * @return the created MMeshBase
         */
        public MMeshBase build()
        {
            AMeshBaseLifecycleManager       life       = AMeshBaseLifecycleManager.create();
            ImmutableMMeshObjectSetFactory  setFactory = ImmutableMMeshObjectSetFactory.create( AMeshObject.class, AMeshObjectIdentifier.class );

            if( theNamespaceMap == null ) {
                theNamespaceMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();
            }
            if( theDefaultNamespace == null ) {
                theDefaultNamespace = theNamespaceMap.getPrimary();
            } else {
                if( theNamespaceMap.getLocalNameFor( theDefaultNamespace ) == null ) {
                    throw new IllegalArgumentException( "Default namespace not known by namespace map" );
                }
            }
            AMeshObjectIdentifierFactory idFact = AMeshObjectIdentifierFactory.create( theNamespaceMap, theDefaultNamespace );

            if( theIdDeserializer == null ) {
                theIdDeserializer = ExternalNameHashMeshObjectIdentifierBothSerializer.create( theNamespaceMap, idFact );
            }

            MHeadMeshBaseView headMeshBaseView = MHeadMeshBaseView.create( idFact, theIdDeserializer );

            MMeshBaseStateMeshObjectHistory history;
            if( theHistory ) {
                history = MMeshBaseStateMeshObjectHistory.create();
            } else {
                history = null;
            }

            MMeshBase ret = new MMeshBase(
                    headMeshBaseView,
                    idFact,
                    theIdDeserializer,
                    setFactory,
                    life,
                    theAccessManager,
                    theIsHistoryEditable,
                    theNormalizeChangeList,
                    history );

            setFactory.setMeshBase( ret );
            idFact.setMeshBase( ret );
            headMeshBaseView.setMeshBase( ret );

            if( theHistory ) {
                history.setMeshBase( ret );
            }

            if( theInitializeHomeObject ) {
                ret.initializeHomeObject();
            }

            if( log.isDebugEnabled() ) {
                log.debug( "created " + ret );
            }

            return ret;
        }

        /**
         * Specify an AccessManager
         *
         * @param value the AccessManager
         * @return
         */
        public Builder accessManager(
                AccessManager value )
        {
            if( value == null ) {
                throw new NullPointerException();
            }
            theAccessManager = value;
            return this;
        }

        /**
         * Specify a PrimaryMeshObjectIdentifierNamespaceMap
         *
         * @param value the PrimaryMeshObjectIdentifierNamespaceMap
         * @return
         */
        public Builder namespaceMap(
                PrimaryMeshObjectIdentifierNamespaceMap value )
        {
            theNamespaceMap = value;
            return this;
        }

        /**
         * Specify a default MeshObjectIdentifierNamespace
         *
         * @param value the default MeshObjectIdentifierNamespace
         * @return
         */
        public Builder defaultNamespace(
                MeshObjectIdentifierNamespace value )
        {
            theDefaultNamespace = value;
            return this;
        }

        /**
         * Is this history of this MeshBase editable. Default is false.
         *
         * @param isHistoryEditable the new value
         * @return
         */
        public Builder isHistoryEditable(
                boolean isHistoryEditable )
        {
            theIsHistoryEditable = isHistoryEditable;
            return this;
        }

        /**
         * Normalize the ChangeList of a Transaction during commit. Default is true.
         *
         * @param normalizeChangeList the new value
         * @return
         */
        public Builder normalizeChangeList(
                boolean normalizeChangeList )
        {
            theNormalizeChangeList = normalizeChangeList;
            return this;
        }

        /**
         * Activate storing a meshObjectHistory.
         *
         * @param value if true, store a meshObjectHistory
         * @return
         */
        public Builder history(
                boolean value )
        {
            theHistory = value;
            return this;
        }

        protected AccessManager                           theAccessManager = PermitAllAccessManager.create(); // FIXME change default
        protected PrimaryMeshObjectIdentifierNamespaceMap theNamespaceMap;
        protected MeshObjectIdentifierNamespace           theDefaultNamespace; // in which new MeshObjects are created if no id is given
        protected MeshObjectIdentifierDeserializer        theIdDeserializer;
        protected boolean                                 theHistory;
        protected boolean                                 theInitializeHomeObject = true;
        protected boolean                                 theIsHistoryEditable    = false;
        protected boolean                                 theNormalizeChangeList  = true;
    }

    /**
     * Private constructor, use Builder pattern.
     *
     * @param headMeshBaseView stores the MeshObjects in this MeshBase
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBase
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     * @param setFactory the factory for MeshObjectSets appropriate for this MeshBase
     * @param life the MeshBaseLifecycleManager to use
     * @param accessMgr the AccessManager that controls access to this MeshBase
     * @param isHistoryEditable if true, the history of the MeshBase is editable
     * @param normalizeChangeList if true, normalize the ChangeList of a Transaction during commit
     * @param history contains the MeshBase's historical information, or null
     */
    protected MMeshBase(
            MHeadMeshBaseView                headMeshBaseView,
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer,
            MeshObjectSetFactory             setFactory,
            AMeshBaseLifecycleManager        life,
            AccessManager                    accessMgr,
            boolean                          isHistoryEditable,
            boolean                          normalizeChangeList,
            MMeshBaseStateMeshObjectHistory  history )
    {
        super( identifierFactory, idDeserializer, setFactory, life, accessMgr, isHistoryEditable, normalizeChangeList );

        theHeadMeshBaseView = headMeshBaseView;
        theHistory          = history;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractHeadMeshBaseView getHeadMeshBaseView()
    {
        return theHeadMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MMeshBaseStateMeshObjectHistory getHistory()
    {
        return theHistory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectHistory meshObjectHistory(
            MeshObjectIdentifier identifier )
    {
        if( theHistory != null ) {
            return theHistory.history( identifier );
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectHistory obtainMeshObjectHistory(
            MeshObjectIdentifier identifier )
    {
        if( theHistory != null ) {
            return theHistory.obtainHistory( identifier );
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObjectHistory> meshObjectHistoriesIterator()
    {
        if( theHistory != null ) {
            return theHistory.meshObjectHistoriesIterator();
        } else {
            return ZeroElementCursorIterator.create();
        }
    }

    /**
     * Add a newly created MeshObject to the right MMeshBaseView
     *
     * @param created the created MeshObject
     */
    public void putNewMeshObject(
            MeshObject created )
    {
        AbstractEditableMeshBaseView mbv = findMeshBaseViewForCurrentTransaction();
        mbv.putNewMeshObject( created );
    }

    /**
     * Remove a MeshObject from the right MMeshBaseView
     *
     * @param identifier identiifer of the MeshObject
     */
    public void removeMeshObject(
            MeshObjectIdentifier identifier )
    {
        AbstractEditableMeshBaseView mbv = findMeshBaseViewForCurrentTransaction();
        mbv.removeMeshObject( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearMemoryCache()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        MeshBaseView mbv = findMeshBaseViewForCurrentTransaction();
        return mbv.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void headTransactionCommitted(
            HeadTransaction tx )
    {
        if( theHistory != null ) {
            theHistory.headTransactionCommitted( tx );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void historyTransactionCommitted(
            HistoryTransaction tx )
    {
        if( theHistory != null ) {
            theHistory.historyTransactionCommitted( tx );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractEditableMeshBaseView findMeshBaseViewForCurrentTransaction()
    {
        AbstractEditableMeshBaseView ret;
        ATransaction                 tx = getCurrentTransaction();

        if( tx == null ) {
            ret = theHeadMeshBaseView;
        } else {
            ret = tx.getDefaultMeshBaseViewForTransaction();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void discardAllAndBulkLoad(
            Collection<? extends ExternalizedMeshObject> data,
            MeshObjectIdentifierNamespace                newNamespace )
    {
        theHeadMeshBaseView.clear();

        if( newNamespace != null ) {
            ((AMeshObjectIdentifierFactory)theMeshObjectIdentifierFactory).setDefaultNamespace( newNamespace );
        }

        AMeshBaseLifecycleManager life = (AMeshBaseLifecycleManager) theMeshBaseLifecycleManager;

        if( theHistory == null ) {
            // only load the head version
            for( ExternalizedMeshObject currentExt : data ) {
                AMeshObject current = life.recreateMeshObject( currentExt, this );
                theHeadMeshBaseView.putNewMeshObject( current );
            }

        } else {
            // load history as well
            theHistory.clear();

            // Need to do this in two steps: create the graphs, and then recreate the Transactions.
            // We can't do it in one step, because to diff, we need to have all MeshObjects present.

            for( ExternalizedMeshObject currentExt : data ) {
                MeshObjectHistory history = theHistory.obtainHistory( currentExt.getIdentifier() );

                ExternalizedMeshObject [] historicalExts = currentExt.getHistorical();
                if( historicalExts != null ) {

                    for( ExternalizedMeshObject historyExt : historicalExts ) {
                        MeshBaseState         historyMbs = theHistory.obtainAt( historyExt.getTimeUpdated() );
                        MHistoricMeshBaseView historyMbv = (MHistoricMeshBaseView) historyMbs.getMeshBaseView();

                        AMeshObject historyObj = life.recreateMeshObject( historyExt, historyMbv );
                        historyMbv.putNewMeshObject( historyObj ); // add even when dead to history

                        history.putOrThrow( historyObj );
                    }
                }
                if( currentExt.hasSomethingBeenSet() ) { // may not be present in HEAD version
                    MeshBaseState         currentMbs = theHistory.obtainAt( currentExt.getTimeUpdated() );
                    MHistoricMeshBaseView currentMbv = (MHistoricMeshBaseView) currentMbs.getMeshBaseView();

                    AMeshObject currentObj = life.recreateMeshObject( currentExt, currentMbv );

                    currentMbv.putNewMeshObject( currentObj );
                    history.putOrThrow( currentObj );

                    theHeadMeshBaseView.putNewMeshObject( currentObj );
                }
            }

            Set<MeshObjectIdentifier> roleTypesConsideredAlready = new HashSet<>();
                // RoleType changes come in pairs (MeshObject, and neighbor) and we create them together because
                // they need to be related. Which means by the time we look at it from the other direction,
                // we can skip.

            for( MeshObjectHistory history : meshObjectHistoriesIterator()) {
                MeshObject previousObj = null;

                for( MeshObject currentObj : history.iterator() ) {
                    MMeshBaseState historyMbs = theHistory.obtainAt( currentObj.getTimeUpdated() );

                    ChangeList changes = diff( previousObj, currentObj, roleTypesConsideredAlready );
                    // This list can be empty, e.g. when a RoleAttribute was added on the "other" side

                    historyMbs.appendChanges( changes );

                    previousObj = currentObj;
                }
            }

            for( MeshBaseState mbs : getHistory() ) {
                mbs.getChangeList().normalize();
            }
        }
    }

    /**
     * Helper to determine the Changes needed to transform the old into the new version of the MeshObject.
     *
     * @param oldVersion the old version of the MeshObject
     * @param newVersion the new version of the MeshObject
     * @param neighborsAlreadyConsideredForRoleChanges keep track of the neighbors whose RoleType changes we have processed already
     * @return the list of Changes
     */
    protected AChangeList diff(
            MeshObject                oldVersion,
            MeshObject                newVersion,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        AChangeList ret = createChangeList();

        if( oldVersion == null || oldVersion.getIsDead() ) {
            ChangeUtils.appendCreateBare(           newVersion, ret );

            ChangeUtils.appendCreateAttributes(     newVersion, ret );
            ChangeUtils.appendSetAttributes(        newVersion, ret );

            ChangeUtils.appendBless(                newVersion, ret );
            ChangeUtils.appendSetProperties(        newVersion, ret );

            ChangeUtils.appendCreateRoleAttributes( newVersion, ret );
            ChangeUtils.appendSetRoleAttributes(    newVersion, ret );

            ChangeUtils.appendBlessRoles(           newVersion, ret, neighborsAlreadyConsideredForRoleChanges );
            ChangeUtils.appendSetRoleProperties(    newVersion, ret );

            neighborsAlreadyConsideredForRoleChanges.add( newVersion.getIdentifier() );

        } else if( newVersion == null || newVersion.getIsDead() ) {
            ChangeUtils.appendUnsetAttributes(      oldVersion, ret );
            ChangeUtils.appendRemoveAttributes(     oldVersion, ret );

            ChangeUtils.appendUnsetRoleAttributes(  oldVersion, ret );
            ChangeUtils.appendRemoveRoleAttributes( oldVersion, ret );

            ChangeUtils.appendUnsetRoleProperties(  oldVersion, ret );
            ChangeUtils.appendUnblessRoles(         oldVersion, ret, neighborsAlreadyConsideredForRoleChanges );

            ChangeUtils.appendUnsetProperties(      oldVersion, ret );
            ChangeUtils.appendUnbless(              oldVersion, ret );

            ChangeUtils.appendDeleteBare(           oldVersion, ret );

        } else {
            ChangeUtils.appendDiffAttributes(
                    oldVersion,
                    newVersion,
                    ret );

            ChangeUtils.appendDiffEntityTypesProperties(
                    oldVersion,
                    newVersion,
                    ret );

            ChangeUtils.appendDiffNeighbors(
                    oldVersion,
                    newVersion,
                    ret,
                    neighborsAlreadyConsideredForRoleChanges );
        }

        return ret;
    }

    /**
     * All functionality related to the HEAD view.
     */
    protected final MHeadMeshBaseView theHeadMeshBaseView;

    /**
     * All functionality related to histories, if any.
     */
    protected final MMeshBaseStateMeshObjectHistory theHistory;
}
