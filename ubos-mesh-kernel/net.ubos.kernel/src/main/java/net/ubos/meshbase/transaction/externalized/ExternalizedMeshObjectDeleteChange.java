//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectDeleteChange;
import net.ubos.meshbase.transaction.TransactionException;

/**
 *
 */
public class ExternalizedMeshObjectDeleteChange
    extends
        AbstractExternalizedChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier the identifier of the MeshObject that is the source of the event
     */
    public ExternalizedMeshObjectDeleteChange(
            MeshObjectIdentifier sourceIdentifier )
    {
        super( sourceIdentifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject source = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );

            base.deleteMeshObject( source );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectDeleteChange internalizeWith(
            MeshObject [] affectedMeshObjects )
    {
        if( affectedMeshObjects.length != 1 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectDeleteChange( affectedMeshObjects[0], affectedMeshObjects[0].getIdentifier() );
    }
}
