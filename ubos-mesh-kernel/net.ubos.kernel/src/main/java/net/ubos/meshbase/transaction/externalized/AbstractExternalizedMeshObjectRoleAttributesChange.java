//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObjectIdentifier;

/**
 *
 */
public abstract class AbstractExternalizedMeshObjectRoleAttributesChange
    extends
        AbstractExternalizedChange
{
    public AbstractExternalizedMeshObjectRoleAttributesChange(
            MeshObjectIdentifier sourceIdentifier,
            String []            oldValues,
            String []            deltaValues,
            String []            newValues,
            MeshObjectIdentifier neighborIdentifier )
    {
        super( sourceIdentifier );

        theOldValues   = oldValues;
        theDeltaValues = deltaValues;
        theNewValues   = newValues;
        theNeighborIdentifier = neighborIdentifier;
    }

    public String [] getOldValue()
    {
        return theOldValues;
    }

    public String [] getDeltaValue()
    {
        return theDeltaValues;
    }

    public String [] getNewValue()
    {
        return theNewValues;
    }

    public MeshObjectIdentifier getNeighborIdentifier()
    {
        return theNeighborIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier, theNeighborIdentifier };
    }

    protected final String [] theOldValues;
    protected final String [] theDeltaValues;
    protected final String [] theNewValues;
    protected final MeshObjectIdentifier theNeighborIdentifier;
}
