//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.history;

import net.ubos.meshbase.MeshBase;
import net.ubos.util.history.History;

/**
 * The historical record of a MeshBase's evolution over time.
 * This could also be called A MeshBase's Transaction Log.
 */
public interface MeshBaseStateHistory
    extends
        History<MeshBaseState>
{
    /**
     * Obtain the MeshBase with this MeshBaseStateHistory.
     *
     * @return the MeshBase
     */
    public MeshBase getMeshBase();
}
