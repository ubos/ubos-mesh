//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.meshbase.externalized.ParserFriendlyExternalizedTransaction;
import net.ubos.meshbase.transaction.externalized.json.AbstractChangeListJsonDecoder;
import net.ubos.model.primitives.externalized.DecodingException;

/**
 */
public abstract class AbstractMeshBaseTransactionJsonDecoder
    extends
        AbstractChangeListJsonDecoder
    implements
        MeshBaseTransactionJsonTags
{
    /**
     * Parse a serialized Transaction.
     *
     * @param r read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the MeshBaseState
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public ParserFriendlyExternalizedTransaction decodeExternalizedTransaction(
            Reader                           r,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        JsonReader jr = new JsonReader( r );
        return AbstractMeshBaseTransactionJsonDecoder.this.decodeExternalizedTransaction( jr, idDeserializer );
    }

    /**
     * Parse a serialized Transaction.
     *
     * @param jr read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the MeshBaseState
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public ParserFriendlyExternalizedTransaction decodeExternalizedTransaction(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        ParserFriendlyExternalizedTransaction ret = new ParserFriendlyExternalizedTransaction();

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case CHANGE_TIME_TAG:
                    ret.setTimeUpdated( jr.nextLong() );
                    break;

                case CHANGES_TAG:
                    ret.setChanges( decodeChanges( jr, idDeserializer ));
                    break;
            }
        }
        jr.endObject();

        return ret;
    }
}
