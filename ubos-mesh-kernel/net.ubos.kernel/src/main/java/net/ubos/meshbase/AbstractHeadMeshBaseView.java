//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.meshbase.a.AHistoryTransaction;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.NotWithinTransactionBoundariesException;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionAction0;
import net.ubos.meshbase.transaction.TransactionAction1;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;

/**
 * Functionality common to MeshBaseView implementations that represent the HEAD version of a MeshBase.
 * @author jernst
 */
public abstract class AbstractHeadMeshBaseView
    extends
        AbstractEditableMeshBaseView
    implements
        HeadMeshBaseView
{
    /**
     * Constructor.
     *
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBaseView
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     */
    public AbstractHeadMeshBaseView(
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        super( identifierFactory, idDeserializer );
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFrozen()
    {
        return theMeshBase.isFrozen();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Transaction checkTransaction()
        throws
            TransactionException
    {
        Transaction tx = getCurrentHeadTransaction();
        if( tx == null ) {
            tx = theMeshBase.getCurrentTransaction();

            if( tx == null ) {
                throw new NotWithinTransactionBoundariesException( this );
            }
            if( !( tx instanceof AHistoryTransaction )) {
                throw new NotWithinTransactionBoundariesException( this );
            }
            AHistoryTransaction realTx = (AHistoryTransaction) tx;
            if( realTx.getStatus() != Transaction.Status.HISTORY_TRANSACTION_RIPPLING_FORWARD ) {
                throw new NotWithinTransactionBoundariesException( this );
            }
        }
        return tx;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HeadTransaction getCurrentHeadTransaction()
    {
        HeadTransaction tx = theMeshBase.getCurrentHeadTransaction();
        if( tx != null && tx.getMeshBaseView() == this ) {
            return tx;
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(
            TransactionAction0 act )
        throws
            TransactionActionException
    {
        theMeshBase.execute( act );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sudoExecute(
            TransactionAction0 act )
        throws
            TransactionActionException
    {
        theMeshBase.sudoExecute( act );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T execute(
            TransactionAction1<T> act )
        throws
            TransactionActionException
    {
        return theMeshBase.execute( act );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T sudoExecute( TransactionAction1<T> act )
            throws TransactionActionException
    {
        return theMeshBase.sudoExecute( act );
    }
}
