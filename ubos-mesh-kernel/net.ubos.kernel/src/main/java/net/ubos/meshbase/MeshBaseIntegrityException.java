//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;

/**
 * Thrown if something is wrong about the integrity of the MeshBase.
 * Inner classes determime details.
 */
public abstract class MeshBaseIntegrityException
    extends
        RuntimeException
{
    /**
     * Constructor for subclasses only.
     */
    protected MeshBaseIntegrityException() {}

    /**
     * A MeshObject is supposed to have a neighbor MeshObject with a certain identifier,
     * but the neighbor could not be found.
     */
    public static class MeshObjectMissing
         extends
             MeshBaseIntegrityException
    {
        /**
         * Constructor.
         *
         * @param obj the MeshObject whose neighbor is missing
         * @param neighborIds the identifiers of all neighbors
         * @param neighbors the neighbors, in the same sequence, resolved or not
         */
        public MeshObjectMissing(
                MeshObject              obj,
                MeshObjectIdentifier [] neighborIds,
                MeshObject           [] neighbors )
        {
            theObject      = obj;
            theNeighborIds = neighborIds;
            theNeighbors   = neighbors;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getLocalizedMessage()
        {
            MeshObjectIdentifier firstNotFound = null;
            int                  notFoundCount = 0;
            for( int i=0 ; i<theNeighborIds.length ; ++i ) {
                if( theNeighbors[i] == null ) {
                    ++notFoundCount;
                    if( firstNotFound == null ) {
                        firstNotFound = theNeighborIds[i];
                    }
                }
            }

            StringBuilder buf = new StringBuilder();
            buf.append( getClass().getName() );
            buf.append( ": MeshObject " ).append( theObject.getIdentifier() );
            buf.append( " with supposedly " ).append( theNeighborIds.length ).append( " neighbors," );
            buf.append( " cannot find " ).append( notFoundCount ).append(  " neighbors, starting with " );
            buf.append( firstNotFound );

            return buf.toString();
        }

        protected final MeshObject theObject;
        protected final MeshObjectIdentifier [] theNeighborIds;
        protected final MeshObject [] theNeighbors;
    }
}
