//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Thrown if a Transaction commit failed and was automatically rolled back.
 * This typically happens if a semantic constraint (such as a multiplicity on
 * a RelationshipType) was found to be violated at commit time.
 */
public class CommitFailedException
    extends
        AbstractLocalizedException
{
    /**
     * Constructor.
     * 
     * @param tx the Transaction that failed
     * @param cause the underlying cause
     */
    public CommitFailedException(
            Transaction tx,
            Throwable   cause )
    {
        super( cause );
        
        theTransaction = tx;
    }
    
    /**
     * Obtain the Transaction that failed.
     * 
     * @return the Transaction
     */
    public Transaction getTransaction()
    {
        return theTransaction;
    }
    
    /**
     * Obtain resource parameters for the internationalization.
     *
     * @return the resource parameters
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            theTransaction,
            theTransaction.getMeshBase()
        };
    }
    
    /**
     * The Transaction that failed.
     */
    protected Transaction theTransaction;
}
