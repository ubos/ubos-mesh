//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.xml;

import net.ubos.mesh.externalized.xml.MeshObjectXmlTags;

/**
 * XML tags for the BulkLoader.
 */
public interface MeshBaseXmlTags
        extends
            MeshObjectXmlTags
{
    /** Encoding ID */
    public static final String MESH_BASE_XML_ENCODING_ID
            = MeshBaseXmlTags.class.getPackage().getName();

    /** Tag indicating a full UBOS Data Mesh file. */
    public static final String UBOS_MESH_TAG = "Mesh";

    /** Tag indicating the header section */
    public static final String HEADER_TAG = "Meta";

    /** Tag indicating the item in the header that is the file's type. */
    public static final String FORMAT_TAG = "Format";

    /** Value for the format. */
    public static final String FORMAT_TAG_VALUE = "UBOS Mesh";

    /** Tag indicating the item in the header that is the file's version */
    public static final String VERSION_TAG = "Version";

    /** Value for the current version. */
    public static final String VERSION_TAG_VALUE = "1.0";

    /** Tag indicating the section in the header that lists the SubjectAreas. */
    public static final String SUBJECTAREAS_TAG = "SubjectAreas";

    /** Tag indicating the section in the header that lists one SubjectArea. */
    public static final String SUBJECTAREA_TAG = "SubjectArea";

    /** Tag indicating the section in the header that lists the MeshObjectIdentifierNamespaces */
    public static final String MESHOBJECTIDENTIFIERNAMESPACES_TAG = "MeshObjectIdentifierNamespaces";

    /** Tag indicating the section in the header that lists the MeshObjectIdentifierNamespaces */
    public static final String MESHOBJECTIDENTIFIERNAMESPACE_TAG = "MeshObjectIdentifierNamespace";

    /** Tag indicating one value for an external name in a MeshObjectIdentifierNamespace */
    public static final String EXTERNALNAME_TAG = "ExternalName";

    /** Tag indicating the preferred value for an external name in a MeshObjectIdentifierNamespace */
    public static final String EXTERNALNAME_PREFERRED_TAG = "preferred";

    /** Value for the preferred external name if it is preferred */
    public static final String EXTERNALNAME_PREFERRED_TRUE_TAG_VALUE = "true";

    /** Tag indicating the body content with the MeshObjects. */
    public static final String BODY_TAG = "MeshObjects";
}
