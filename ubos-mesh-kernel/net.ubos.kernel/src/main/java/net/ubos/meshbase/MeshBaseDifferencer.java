//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.util.HashSet;
import java.util.Set;
import net.ubos.mesh.BlessedAlreadyException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotBlessedException;
import net.ubos.mesh.RoleTypeBlessedAlreadyException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.ChangeUtils;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * <p>Provides the functionality to perform a diff on any two MeshBaseViews.
  *
  * <p>This class creates two MeshObjectNeighborChangeEvents for each changed relationships
  * (one for the source MeshObject, and one for the destination MeshObject). Strictly speaking,
  * only one is necessary and the other is redundant. However, overall it appears more efficient
  * (and is certainly easier to program) to generate and process both (i.e. ignore the second
  * one), so that's what we do.
  */
public class MeshBaseDifferencer
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( MeshBaseDifferencer.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param baseline the MeshBase that forms the "base" against which compare
     * @return the differencer
     */
    public static MeshBaseDifferencer create(
            MeshBaseView baseline )
    {
        return new MeshBaseDifferencer( baseline );
    }

    /**
     * Constructor.
     *
     * @param baseline the MeshBaseView that forms the "base" against which compare
     */
    protected MeshBaseDifferencer(
            MeshBaseView baseline )
    {
        theBaseline = baseline;
    }

    /**
     * Obtain the base MeshBase against which we compare
     *
     * @return the base MeshBase
     */
    public MeshBaseView getBaseline()
    {
        return theBaseline;
    }

    /**
     * This method performs the diff between the baseline MeshBase (set in the constructor)
     * and the comparison MeshBase (provided as argument to this call). The differences between
     * the MeshObjects in comparison MeshBase as compared to the MeshObjects in the baseline
     * MeshBase are returned as a ChangeList. The ChangeList includes all necessary changes that,
     * when applied on the baseline MeshBase in the sequence that they are given, transform it
     * into the comparison MeshBase.
     *
     * @param comparisonBase the MeshBase to compare against
     * @return the list of changes between the baseline MeshBase and the comparison MeshBase
     */
    public ChangeList determineChangeList(
            MeshBaseView comparisonBase )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "determineChangeList" );
        }

        ChangeList changes = theBaseline.getMeshBase().createChangeList();

        Set<MeshObjectIdentifier> roleTypesConsideredAlready = new HashSet<>();
            // RoleType changes come in pairs (MeshObject, and neighbor) and we create them together because
            // they need to be related. Which means by the time we look at it from the other direction,
            // we can skip.

        // iterate through all the objects in COMPARISON
        for( MeshObject objInComp : comparisonBase ) {

            MeshObjectIdentifier identifier = objInComp.getIdentifier();
            MeshObject           objInBase  = theBaseline.findMeshObjectByIdentifier( identifier );

            // If the MeshObject from COMPARISON does not exist in BASE, add a MeshObjectCreatedEvent to the ChangeList
            if( objInBase == null ) {

                ChangeUtils.appendCreateBare(           objInComp, changes );

                ChangeUtils.appendCreateAttributes(     objInComp, changes );
                ChangeUtils.appendSetAttributes(        objInComp, changes );

                ChangeUtils.appendBless(                objInComp, changes );
                ChangeUtils.appendSetProperties(        objInComp, changes );
            }
        }

        for( MeshObject objInBase : theBaseline ) {
            MeshObjectIdentifier identifier = objInBase.getIdentifier();
            MeshObject           objInComp  = comparisonBase.findMeshObjectByIdentifier( identifier );

            if( objInComp != null ) {
                // If the MeshObject from BASE exists on COMPARISON,
                // we need to check for any changes made to it.

                ChangeUtils.appendDiffAttributes(
                        objInBase,
                        objInComp,
                        changes );

                ChangeUtils.appendDiffEntityTypesProperties(
                        objInBase,
                        objInComp,
                        changes );

                ChangeUtils.appendDiffNeighbors(
                        objInBase,
                        objInComp,
                        changes,
                        roleTypesConsideredAlready );

                roleTypesConsideredAlready.add( identifier );
            }
        }

        // iterate through all the objects in COMPARISON
        for( MeshObject objInComp : comparisonBase ) {

            MeshObjectIdentifier identifier = objInComp.getIdentifier();
            MeshObject           objInBase  = theBaseline.findMeshObjectByIdentifier( identifier );

            // If the MeshObject from COMPARISON does not exist in BASE, add a MeshObjectCreatedEvent to the ChangeList
            if( objInBase == null ) {

                ChangeUtils.appendCreateRoleAttributes( objInComp, changes );
                ChangeUtils.appendSetRoleAttributes(    objInComp, changes );

                ChangeUtils.appendBlessRoles(           objInComp, changes, roleTypesConsideredAlready );
                ChangeUtils.appendSetRoleProperties(    objInComp, changes );

                roleTypesConsideredAlready.add( identifier );
            }
        }

        for( MeshObject objInBase : theBaseline ) {
            MeshObjectIdentifier identifier = objInBase.getIdentifier();
            MeshObject           objInComp  = comparisonBase.findMeshObjectByIdentifier( identifier );

            if( objInComp == null ) {
                ChangeUtils.appendUnsetAttributes(      objInBase, changes );
                ChangeUtils.appendRemoveAttributes(     objInBase, changes );

                ChangeUtils.appendUnsetRoleAttributes(  objInBase, changes );
                ChangeUtils.appendRemoveRoleAttributes( objInBase, changes );

                ChangeUtils.appendUnsetRoleProperties(  objInBase, changes );
                ChangeUtils.appendUnblessRoles(         objInBase, changes, roleTypesConsideredAlready );

                ChangeUtils.appendUnsetProperties(      objInBase, changes );
                ChangeUtils.appendUnbless(              objInBase, changes );
                ChangeUtils.appendDeleteBare(           objInBase, changes );

                roleTypesConsideredAlready.add( identifier );
            }
        }

        changes.normalize();
        return changes;
    }

    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theBaseline"
                },
                new Object[] {
                    theBaseline
                });
    }

    /**
     * The MeshBaseView against which we are comparing, and to which we apply any changes.
     */
    protected MeshBaseView theBaseline;
}
