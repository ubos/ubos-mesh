//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.externalized.ParserFriendlyExternalizedTransaction;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.modelbase.externalized.ExternalizedMeshObjectIdentifierNamespace;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleClassLoader;
import org.diet4j.core.ModuleException;
import org.diet4j.core.ModuleMeta;
import org.diet4j.core.ModuleRegistry;
import org.diet4j.core.ModuleRequirement;

/**
 * Parses JSON and imports it into a MeshBase
 */
public abstract class AbstractMeshBaseJsonDecoder
    extends
        AbstractMeshBaseTransactionJsonDecoder
    implements
        MeshBaseJsonTags
{
    /**
     * Handle the header section.
     *
     * @param jr read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @throws DecodingException thrown if a problem occurred during decoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void decodeHeader(
            JsonReader                               jr,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        jr.beginObject();

        while( jr.hasNext() ) {
            switch( jr.nextName() ) {
                case FORMAT_TAG:
                    String format = jr.nextString();
                    if( !FORMAT_TAG_VALUE.equals( format )) {
                        throw new DecodingException.Syntax( "Wrong format: need " + FORMAT_TAG_VALUE + ", have " + format );
                    }
                    break;

                case VERSION_TAG:
                    String version = jr.nextString();
                    if( !VERSION_TAG_VALUE.equals( version )) {
                        throw new DecodingException.Syntax( "Wrong version: need " + VERSION_TAG_VALUE + ", have " + version );
                    }
                    break;

                case MOST_RECENT_UPDATE_TAG:
                    Long ignored = jr.nextLong();
                    break;

                case MESHOBJECTIDENTIFIERNAMESPACES_TAG:
                    decodeHeaderNamespaces( jr, idDeserializer );
                    break;

                case SUBJECTAREAS_TAG:
                    decodeSubjectAreas( jr );
                    break;
            }
        }

        jr.endObject();
    }

    /**
     * Handle the MeshObjectIdentifier namespaces section in the header
     * @param jr read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @throws DecodingException thrown if a problem occurred during decoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void decodeHeaderNamespaces(
            JsonReader                               jr,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        jr.beginObject();

        while( jr.hasNext() ) {
            String shortName = jr.nextName();

            ExternalizedMeshObjectIdentifierNamespace namespace = new ExternalizedMeshObjectIdentifierNamespace();
            namespace.setLocalName( shortName );

            jr.beginObject();

            while( jr.hasNext() ) {
                switch( jr.nextName() ) {
                    case PREFERRED_EXTERNALNAME_TAG:
                        namespace.addPreferredExternalName( jr.nextString() );
                        break;

                    case EXTERNALNAMES_TAG:
                        jr.beginArray();
                        while( jr.hasNext() ) {
                            namespace.addExternalName( jr.nextString() );
                        }
                        jr.endArray();
                        break;
                }
            }

            jr.endObject();

            idDeserializer.addNamespace( namespace );
        }

        jr.endObject();
    }

    /**
     * Handle the SubjectAreas section in the header.
     *
     * @param jr read the data from here
     * @throws DecodingException thrown if a problem occurred during decoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected void decodeSubjectAreas(
            JsonReader jr )
        throws
            DecodingException,
            IOException
    {
        ModuleRegistry registry = ((ModuleClassLoader)getClass().getClassLoader()).getModuleRegistry();

        jr.beginObject();

        while( jr.hasNext() ) {
            String saName = jr.nextName();
            jr.beginObject();
            // ignore everything in between -- should not be anything for now
            jr.endObject();

            try {
                ModuleRequirement req    = ModuleRequirement.parse( saName );
                ModuleMeta        meta   = registry.determineSingleResolutionCandidate( req );
                Module            module = registry.resolve( meta );

                module.activateRecursively();

            } catch( ParseException ex ) {
                throw new DecodingException.Syntax( ex );

            } catch( ModuleException ex ) {
                throw new DecodingException.Installation( ex );
            }
        }

        jr.endObject();
    }

    /**
     * Handle the body section containing the MeshObjects.
     *
     * @param jr read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the parsed MeshObjects
     * @throws DecodingException thrown if a problem occurred during decoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected List<ParserFriendlyExternalizedMeshObject> decodeMeshObjects(
            JsonReader                               jr,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        jr.beginObject();

        List<ParserFriendlyExternalizedMeshObject> foundMeshObjects = new ArrayList<>();

        while( jr.hasNext() ) {
            try {
                MeshObjectIdentifier id = idDeserializer.meshObjectIdentifierFromExternalForm(jr.nextName());

                ParserFriendlyExternalizedMeshObject found = decodeExternalizedMeshObject( jr, id, idDeserializer );
                foundMeshObjects.add( found );

            } catch( ParseException ex ) {
                throw new DecodingException.Syntax( ex );
            }
        }
        jr.endObject();

        return foundMeshObjects;
    }

    /**
     * Handle the body section containing the Transactions.
     *
     * @param jr read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the parsed Transactions
     * @throws DecodingException thrown if a problem occurred during decoding
     * @throws IOException thrown if a problem occurred during writing the output
     */
    protected List<ParserFriendlyExternalizedTransaction> decodeTransactions(
            JsonReader                               jr,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        List<ParserFriendlyExternalizedTransaction> ret = new ArrayList<>();

        jr.beginArray();

        while( jr.hasNext() ) {
            ParserFriendlyExternalizedTransaction found = decodeExternalizedTransaction( jr, idDeserializer );

            ret.add( found );
        }

        jr.endArray();

        return ret;
    }
}
