//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.history.transaction;

import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.history.EditableHistoricMeshBaseView;

/**
 * A Transaction that modifies a point in the history of the graph, not the head version.
 */
public interface HistoryTransaction
        extends
            Transaction
{
    /**
     * Obtain the time when the history is being modified.
     *
     * @return the time
     */
    public long getWhen();

    /**
     * Obtain the MeshBaseView that is being modified.
     *
     * @return the MeshBaseView
     */
    public EditableHistoricMeshBaseView getMeshBaseView();
}

