//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import net.ubos.mesh.externalized.json.MeshObjectJsonTags;

/**
 * JSON Tags for the serialization of Meshbases.
 */
public interface MeshBaseJsonTags
        extends
            MeshBaseTransactionJsonTags
{
    /** Encoding ID */
    public static final String MESH_BASE_JSON_ENCODING_ID
            = MeshBaseJsonTags.class.getPackage().getName();

    /** Tag indicating the header section. */
    public static final String HEADER_SECTION_TAG = "Meta";

    /** Tag indicating the item in the header that is the file's type. */
    public static final String FORMAT_TAG = "Format";

    /** Value for the format. */
    public static final String FORMAT_TAG_VALUE = "UBOS Mesh";

    /** Tag indicating the item in the header that is the file's version. */
    public static final String VERSION_TAG = "Version";

    /** Current value for the version. */
    public static final String VERSION_TAG_VALUE = "1.0";

    /** Tag indicating the most recent update of data in this MeshBase. */
    public static final String MOST_RECENT_UPDATE_TAG = "LastUpdated";

    /** Tag indicating the section in the header that lists the SubjectAreas. */
    public static final String SUBJECTAREAS_TAG = "SubjectAreas";

    /** Tag indicating the section in the header that lists the MeshObjectIdentifierNamespaces */
    public static final String MESHOBJECTIDENTIFIERNAMESPACES_TAG = "MeshObjectIdentifierNamespaces";

    /** Tag indicating the preferred external name of a MeshObjectIdentifierNamespace */
    public static final String PREFERRED_EXTERNALNAME_TAG = "preferredExternalName";

    /** Tag indicating the array of external names of a MeshObjectIdentifierNamespace */
    public static final String EXTERNALNAMES_TAG = "externalNames";

    /** Tag indicating the body section with the MeshObjects. */
    public static final String MESHOBJECTS_SECTION_TAG = "MeshObjects";

    /** Tag indicating the body section with the Transactions. */
    public static final String TRANSACTIONS_SECTION_TAG = "Transactions";
}
