//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.a.AChangeList;
import net.ubos.meshbase.a.AMeshBaseStateHistory;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.history.m.MHistory;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * Implementation class for everything related to histories for the MMeshBase.
 *
 * It inherits from MeshBaseStateHistory, and implements that, but it also implements the histories of
 * MeshObjects. Which is why we call it MMeshBaseStateMeshObjectHistory instead of MMeshBaseStateHistory.
 *
 * Compare StoreMeshBaseStateMeshObjectHistory
 */
public class MMeshBaseStateMeshObjectHistory
    extends
        MHistory<MeshBaseState>
    implements
        AMeshBaseStateHistory
{
    private static final Log log = Log.getLogInstance( MMeshBaseStateMeshObjectHistory.class );

    /**
     * Factory method.
     *
     * @return the created object
     */
    public static MMeshBaseStateMeshObjectHistory create()
    {
        return new MMeshBaseStateMeshObjectHistory( MSmartMap.create());
    }

    /**
     * Constructor.
     *
     * @param meshObjectHistories store the MeshObjectHistories here
     */
    protected MMeshBaseStateMeshObjectHistory(
            SmartMap<MeshObjectIdentifier,MeshObjectHistory> meshObjectHistories )
    {
        theMeshObjectHistories = meshObjectHistories;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MMeshBaseState obtainAt(
            long t )
    {
        int createIndex = -1;

        MMeshBaseState ret = null;
        for( int i=0 ; i<theStorage.size() ; ++i ) {
            long found = theStorage.get( i ).getTimeUpdated();
            if( t == found ) {
                ret = (MMeshBaseState) theStorage.get( i );
                break;
            } else if( t < found ) {
                createIndex = i;
                break;
            }
        }

        if( ret == null ) {
            MHistoricMeshBaseView view = MHistoricMeshBaseView.create( theMeshBase );
            ret = MMeshBaseState.createWithChanges( t, AChangeList.create( theMeshBase ), view );
            view.setMeshBaseState( ret );

            if( createIndex == -1 ) {
                theStorage.add( ret );
            } else {
                theStorage.add( createIndex, ret );
            }
        }
        return ret;
    }

    /**
     * Set the MeshBase after initialization.
     *
     * @param mb the meshBase of which this is the MeshBaseHistory
     */
    public void setMeshBase(
            MMeshBase mb )
    {
        if( theMeshBase != null ) {
            throw new IllegalStateException( "Cannot set MeshBase more than once" );
        }
        theMeshBase = mb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MMeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * Callback from the MMeshBase informing us that a Transaction was committed.
     *
     * @param tx the committed Transaction
     */
    public void headTransactionCommitted(
            HeadTransaction tx )
    {
        // compare StoreMeshBaseStateMeshObjectHistory.

        // Algorithm:
        // 1. We look at all MeshObjects affected by the Transaction, and
        //    a. create copies
        //    b. insert those copies into the MeshBaseView
        //    c. and into the MeshObjectHistories
        // 2. We create new Change events for the Changes stored in the MeshBaseState
        //    that refer to those copies, not the head version. Because we need those
        //    MeshObjects for the Changes, and the MeshObjects need to have a MeshBaseView,
        //    we inserted those into the MeshBaseView in 1.b instead of simply have
        //    a MeshBaseView cache miss recreate it
        // 3. All other MeshObjects in the MeshBaseView will be demand-created when
        //    somebody actually looks for them. Those will be verbatim copies of the
        //    most recent version of the MeshObjectHistory but not be part of the
        //    MeshObjectHistory

        ChangeList changeList = tx.getChangeList();
        if( changeList.isEmpty() ) {
            return; // do nothing
        }
        MHistoricMeshBaseView newView = MHistoricMeshBaseView.create( theMeshBase );

        AChangeList newChanges = AChangeList.createCopyWithMeshBaseView( changeList, newView );

        MMeshBaseState newState = MMeshBaseState.createWithChanges(
                            tx.getTimeEnded(),
                            newChanges,
                            newView );
        newView.setMeshBaseState( newState );
        putOrThrow( newState );

        for( MeshObject affected : newChanges.getAffectedMeshObjects() ) {
            MeshObjectHistory currentMeshObjectHistory = obtainHistory( affected.getIdentifier() );
            currentMeshObjectHistory.putOrThrow( affected );

            newView.putNewMeshObject( affected );
        }
    }

    /**
     * Callback from the MMeshBase informing us that a Transaction was committed.
     *
     * @param tx the committed Transaction
     */
    public void historyTransactionCommitted(
            HistoryTransaction tx )
    {
        // compare StoreMeshBaseStateMeshObjectHistory.

        ChangeList changeList = tx.getChangeList();
        if( changeList.isEmpty() ) {
            theMeshBase.getHistory().removeIgnorePrevious( tx.getWhen() );

            return;
        }
        MHistoricMeshBaseView mbv = (MHistoricMeshBaseView) tx.getMeshBaseView();
        MMeshBaseState        mbs = (MMeshBaseState) mbv.getMeshBaseState();
        mbs.appendChanges( changeList );

        for( MeshObject affected : changeList.getAffectedMeshObjects() ) {
            MeshObjectHistory currentMeshObjectHistory = obtainHistory( affected.getIdentifier() );
            if( !currentMeshObjectHistory.containsAt( affected.getTimeUpdated())) {
                currentMeshObjectHistory.putOrThrow( affected );
            }

            if( !mbv.existsMeshObjectByIdentifier( affected.getIdentifier())) {
                mbv.putNewMeshObject( affected );
            }
        }
    }

    /**
     * Obtain the historical record for a MeshObject with a given identifier.
     *
     * @param identifier the MeshObject's identifier
     * @return the history of the MeshObject with this identifier, or null
     */
    public MeshObjectHistory history(
            MeshObjectIdentifier identifier )
    {
        return theMeshObjectHistories.get( identifier );
    }

    /**
     * Obtain the historical record for a MeshObject with a given identifier.
     * Create it if it does not exist.
     *
     * @param identifier the MeshObject's identifier
     * @return the history of the MeshObject with this identifier
     */
    public synchronized MeshObjectHistory obtainHistory(
            MeshObjectIdentifier identifier )
    {
        MeshObjectHistory ret = theMeshObjectHistories.get( identifier );
        if( ret == null ) {
            ret = new MMeshObjectHistory( theMeshBase, identifier );
            theMeshObjectHistories.put( identifier, ret );
        }
        return ret;
    }

    /**
     * Obtain an iterator over all MeshObjectHistories this MeshBaseHistory knows about.
     *
     * @return the CursorIterator
     */
    public CursorIterator<MeshObjectHistory> meshObjectHistoriesIterator()
    {
        return theMeshObjectHistories.valueIterator();
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            MeshBaseState toAdd )
        throws
            IllegalArgumentException
    {
        MeshBaseState already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same MeshBaseState:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "MeshBaseState exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        theMeshObjectHistories.clear();
        super.clear();
    }

    /**
     * The MeshBase whose history implementation this is.
     */
    protected MMeshBase theMeshBase;

    /**
     * The histories of the individual MeshObjects, keyed by their identifiers.
     */
    protected final SmartMap<MeshObjectIdentifier,MeshObjectHistory> theMeshObjectHistories;
}
