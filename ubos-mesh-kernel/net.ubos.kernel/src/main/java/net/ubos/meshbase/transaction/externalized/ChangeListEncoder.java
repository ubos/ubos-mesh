//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import java.io.IOException;
import java.io.Writer;
import net.ubos.mesh.externalized.Encoder;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;

/**
 * Knows how to encode a list of Changes, such as in one transaction.
 */
public interface ChangeListEncoder
    extends
        Encoder
{
    /**
     * Write a list of changes to a Writer.
     *
     * @param changes the ChangeList
     * @param idSerializer use this to serialize MeshObjectIdentifiers
     * @param emitOldValues if true, always emit old values even if they are redundant
     * @param emitNewValues if true, always emit new values even if they are redundant
     * @param w the Writer to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    public void writeChangeList(
            ChangeList                     changes,
            MeshObjectIdentifierSerializer idSerializer,
            boolean                        emitOldValues,
            boolean                        emitNewValues,
            Writer                         w )
        throws
            EncodingException,
            IOException;
}
