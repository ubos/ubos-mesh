//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifier;

/**
 *
 */
public abstract class AbstractExternalizedMeshObjectTypeChange
    extends
        AbstractExternalizedChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier MeshObjectIdentifier of the primary affected MeshObject
     * @param oldValue the old MeshTypes, prior to the event
     * @param deltaValue the MeshTypes that are different
     * @param newValue the new MeshTypes, after the event
     */
    public AbstractExternalizedMeshObjectTypeChange(
            MeshObjectIdentifier  sourceIdentifier,
            MeshTypeIdentifier [] oldValue,
            MeshTypeIdentifier [] deltaValue,
            MeshTypeIdentifier [] newValue )
    {
        super( sourceIdentifier );

        theOldValue   = oldValue;
        theDeltaValue = deltaValue;
        theNewValue   = newValue;
    }

    public MeshTypeIdentifier [] getOldValue()
    {
        return theOldValue;
    }

    public MeshTypeIdentifier [] getDeltaValue()
    {
        return theDeltaValue;
    }

    public MeshTypeIdentifier [] getNewValue()
    {
        return theNewValue;
    }

    protected final MeshTypeIdentifier [] theOldValue;
    protected final MeshTypeIdentifier [] theDeltaValue;
    protected final MeshTypeIdentifier [] theNewValue;
}
