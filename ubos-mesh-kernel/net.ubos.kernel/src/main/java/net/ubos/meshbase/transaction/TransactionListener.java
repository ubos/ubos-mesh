//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.history.transaction.HistoryTransaction;

/**
  * This interface is implemented by objects that wish to listen to Transaction-related events.
  */
public interface TransactionListener
{
    /**
      * Indicates that a HeadTransaction has been started.
      *
      * @param tx the HeadTransaction that was started
      */
    public default void transactionStarted(
            HeadTransaction tx )
    {}

    /**
      * Indicates that a HeadTransaction has been committed.
      *
      * @param tx the HeadTransaction that was committed
      */
    public default void transactionCommitted(
            HeadTransaction tx )
    {}

    /**
     * Indicates that a HeadTransaction has been rolled back.
     *
     * @param tx the HeadTransaction that was rolled back
     */
    public default void transactionRolledback(
            HeadTransaction tx )
    {}

    /**
      * Indicates that a HistoryTransaction has been started.
      *
      * @param tx the HistoryTransaction that was started
      */
    public default void transactionStarted(
            HistoryTransaction tx )
    {}

    /**
      * Indicates that a HistoryTransaction has been committed.
      *
      * @param tx the HistoryTransaction that was committed
      */
    public default void transactionCommitted(
            HistoryTransaction tx )
    {}

    /**
     * Indicates that a HistoryTransaction has been rolled back.
     *
     * @param tx the HistoryTransaction that was rolled back
     */
    public default void transactionRolledback(
            HistoryTransaction tx )
    {}
};
