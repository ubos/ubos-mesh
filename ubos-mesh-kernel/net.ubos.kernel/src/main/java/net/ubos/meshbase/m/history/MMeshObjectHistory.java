//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.MeshBase;
import net.ubos.util.history.m.MHistory;
import net.ubos.util.history.m.MHistoryCursorIterator;
import net.ubos.util.logging.Log;

/**
 * In-memory implementation of MeshObjectHistory.
 */
public class MMeshObjectHistory
        extends
            MHistory<MeshObject>
        implements
            MeshObjectHistory
{
    private static final Log log = Log.getLogInstance( MMeshObjectHistory.class );

    /**
     * Constructor.
     *
     * @param meshBase the MeshBase to which this history belongs
     * @param id the MeshObjectIdentifier of the MeshObject whose history this is
     */
    public MMeshObjectHistory(
            MeshBase             meshBase,
            MeshObjectIdentifier id )
    {
        theMeshBase   = meshBase;
        theIdentifier = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getMeshObjectIdentifier()
    {
        return theIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MHistoryCursorIterator<MeshObject> iterator()
    {
        return new MHistoryCursorIterator<>( this, 0 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            MeshObject toAdd )
    {
        // So we can set a breakpoint
        super.putIgnorePrevious( toAdd );
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            MeshObject toAdd )
        throws
            IllegalArgumentException
    {
        MeshObject already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same MeshObject:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "MeshObject exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * The MeshBase to which this history belongs
     */
    protected final MeshBase theMeshBase;

    /**
     * The MeshObjectIdentifier of the MeshObject whose history this is.
     */
    protected final MeshObjectIdentifier theIdentifier;
}
