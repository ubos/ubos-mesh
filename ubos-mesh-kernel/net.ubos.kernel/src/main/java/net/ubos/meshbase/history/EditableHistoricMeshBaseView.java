//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.history;

import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.meshbase.transaction.TransactionAction0;
import net.ubos.meshbase.transaction.TransactionAction1;

/**
 * A historic MeshBaseView that can be edited. This means MeshObjects can be added, removed,
 * changed etc. as if the change had happened in the past.
 *
 * MeshObjects looked up in this MeshBaseView will be looked up, and traversals will be made
 * based on that historical point in time. Executed Transactions will modify this
 * MeshBaseView, and adjust the ChangeSets leading to this MeshBaseView, and from this
 * MeshBaseView to the next one, to be internally consistent.
 */
public interface EditableHistoricMeshBaseView
    extends
        EditableMeshBaseView,
        HistoricMeshBaseView
{
    /**
     * Obtain the HistoryTransaction currently active on this EditableHistoricMeshBaseView (if any).
     *
     * @return the currently active HistoryTransaction, or null if there is none
     */
    public abstract HistoryTransaction getCurrentHistoryTransaction();

    /**
     * Perform this TransactionAction as root within an automatically generated HistoryTransaction
     * as soon as possible.
     *
     * @param act the TransactionAction
     * @throws TransactionActionException a problem occurred when executing the TransactionAction
     */
    public abstract void sudoExecute(
            TransactionAction0 act )
        throws
            TransactionActionException;

    /**
     * Perform this TransactionAction within an automatically generated HistoryTransaction
     * as soon as possible.
     *
     * @param act the TransactionAction
     * @return a TransactionAction-specific return value
     * @throws TransactionActionException a problem occurred when executing the TransactionAction
     * @param <T> the type of return value
     */
    public abstract <T> T sudoExecute(
            TransactionAction1<T> act )
        throws
            TransactionActionException;
}
