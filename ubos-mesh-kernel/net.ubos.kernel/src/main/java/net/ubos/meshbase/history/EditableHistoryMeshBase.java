//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.history;

import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.meshbase.transaction.TransactionAction0;
import net.ubos.meshbase.transaction.TransactionAction1;

/**
 * A MeshBase whose history is editable.
 */
public interface EditableHistoryMeshBase
    extends
        MeshBase
{
    /**
     * Execute a HistoryTransaction at the given time in the past.
     *
     * @param when the time
     * @param act the code to run in the transaction
     * @throws TransactionActionException thrown if an error occurred
     */
    public abstract void executeAt(
            long               when,
            TransactionAction0 act )
        throws
            TransactionActionException;

    /**
     * Execute a HistoryTransaction at the given time in the past and return
     * a result.
     *
     * @param when the time
     * @param act the code to run in the transaction
     * @return the result
     * @throws TransactionActionException thrown if an error occurred
     * @param <T> the type of result
     */
    public abstract <T> T executeAt(
            long                  when,
            TransactionAction1<T> act )
        throws
            TransactionActionException;

    /**
     * Execute a HistoryTransaction as root at the given time in the past.
     *
     * @param when the time
     * @param act the code to run in the transaction
     * @throws TransactionActionException thrown if an error occurred
     */
    public abstract void sudoExecuteAt(
            long               when,
            TransactionAction0 act )
        throws
            TransactionActionException;

    /**
     * Execute a HistoryTransaction as root at the given time in the past.
     *
     * @param when the time
     * @param act the code to run in the transaction
     * @return the result
     * @throws TransactionActionException thrown if an error occurred
     * @param <T> the type of result
     */
    public abstract <T> T sudoExecuteAt(
            long                  when,
            TransactionAction1<T> act )
        throws
            TransactionActionException;
}
