//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.security;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.security.CallerHasInsufficientPermissionsException;
import net.ubos.mesh.security.ReadOnlyMeshBaseViewException;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;

/**
 * An AccessManager that permits read-only access to all MeshObjects to authenticated users. Root gets to
 * do anything.
 */
public class PermitAuthenticatedAllReadOnlyAccessManager
    extends
        AbstractNoNeedToResolveNeighborsAccessManager
{
    /**
     * Factory method.
     *
     * @return the created PermitAllAccessManager
     */
    public static PermitAuthenticatedAllReadOnlyAccessManager create()
    {
        return new PermitAuthenticatedAllReadOnlyAccessManager();
    }

    /**
     * Constructor.
     */
    protected PermitAuthenticatedAllReadOnlyAccessManager()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public void assignOwner(
            MeshObject toBeOwned,
            MeshObject newOwner )
        throws
            NotPermittedException,
            TransactionException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( toBeOwned.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedCreate(
            MeshBaseView         mb,
            MeshObjectIdentifier identifier )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( mb );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedAccess(
            MeshObject obj )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( obj.isHomeObject() ) {
            return; // always allow access to the home object
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedCreateAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetAttribute(
            MeshObject   obj,
            String       name,
            Serializable newValue )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedDeleteAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedBlessedBy(
            MeshObject obj,
            EntityType type )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedBless(
            MeshObject    obj,
            EntityType [] types )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedUnbless(
            MeshObject    obj,
            EntityType [] types )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetProperty(
            MeshObject   obj,
            PropertyType thePropertyType )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetProperty(
            MeshObject    obj,
            PropertyType  thePropertyType,
            PropertyValue newValue )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedCreateRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name,
            Serializable         newValue )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedDeleteRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedBless(
            MeshObject           obj,
            RoleType []          thisEnds,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedUnbless(
            MeshObject           obj,
            RoleType []          thisEnds,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedTraverse(
            MeshObject           obj,
            RoleType             toTraverse,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedGetRoleProperty(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            PropertyType         thePropertyType )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedSetRoleProperty(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            PropertyType         thePropertyType,
            PropertyValue        newValue )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedIsRelated(
            MeshObject           obj,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        if( ThreadIdentityManager.getCaller() != null ) {
            return;
        }
        throw new CallerHasInsufficientPermissionsException( obj.getMeshBaseView(), obj, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkPermittedDelete(
            MeshObject obj )
        throws
            NotPermittedException
    {
        if( ThreadIdentityManager.isSu() ) {
            return;
        }
        throw new ReadOnlyMeshBaseViewException( obj.getMeshBaseView() );
    }

}
