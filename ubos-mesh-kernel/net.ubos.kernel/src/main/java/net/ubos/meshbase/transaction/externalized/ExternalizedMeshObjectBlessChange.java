//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectBlessChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.modelbase.MeshTypeNotFoundException;

/**
 *
 */
public class ExternalizedMeshObjectBlessChange
    extends
        AbstractExternalizedMeshObjectTypeChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier the identifier of the MeshObject whose MeshTypes changed
     * @param oldValue the old MeshTypes, prior to the event
     * @param deltaValue the added MeshTypes
     * @param newValue the new MeshTypes, after the event
     */
    public ExternalizedMeshObjectBlessChange(
            MeshObjectIdentifier  sourceIdentifier,
            MeshTypeIdentifier [] oldValue,
            MeshTypeIdentifier [] deltaValue,
            MeshTypeIdentifier [] newValue )
    {
        super( sourceIdentifier, oldValue, deltaValue, newValue );
    }

    public MeshTypeIdentifier [] getAdded()
    {
        return theDeltaValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject    source = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );
            EntityType [] types  = findEntityTypes( theDeltaValue );
            source.bless( types );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectBlessChange internalizeWith(
            MeshObject [] affectedMeshObjects )
        throws
            MeshTypeNotFoundException
    {
        if( affectedMeshObjects.length != 1 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectBlessChange(
                affectedMeshObjects[0],
                findEntityTypes( theOldValue ),
                findEntityTypes( theDeltaValue ),
                findEntityTypes( theNewValue ));
    }
}
