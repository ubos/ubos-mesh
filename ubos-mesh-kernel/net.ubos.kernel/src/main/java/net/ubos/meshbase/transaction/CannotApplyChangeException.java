//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.MeshBase;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Thrown if a Change could not be applied. The cause provides more detail.
 */
public class CannotApplyChangeException
        extends
            RuntimeException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param mb the MeshBase to which the Change could not be applied
     * @param cause the cause for this Exception
     */
    public CannotApplyChangeException(
            MeshBase  mb,
            Throwable cause )
    {
        super( null, cause );

        theMeshBase = mb;
    }

    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "meshBase"
                },
                new Object[] {
                    theMeshBase
                } );
    }

    /**
     * The MeshBase to which the Change could not be applied.
     */
    protected final MeshBase theMeshBase;
}
