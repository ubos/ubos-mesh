//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.util.List;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;

/**
 * This implementation of ExternalizedTransaction is fully initialized in the
 * factory method.
 */
public class SimpleExternalizedTransaction
    implements
        ExternalizedTransaction
{
    // selfdoc
    public static SimpleExternalizedTransaction create(
             long                     timeUpdated,
             List<ExternalizedChange> changes )
    {
        return new SimpleExternalizedTransaction( timeUpdated, changes );
    }

    // selfdoc
    protected SimpleExternalizedTransaction(
             long                     timeUpdated,
             List<ExternalizedChange> changes )
    {
        theTimeUpdated = timeUpdated;
        theChanges     = changes;
    }

    // selfdoc
    @Override
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    // selfdoc
    @Override
    public List<ExternalizedChange> getChanges()
    {
        return theChanges;
    }

    // selfdoc
    protected long theTimeUpdated;

    // selfdoc
    protected List<ExternalizedChange> theChanges;
}
