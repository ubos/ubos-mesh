//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.util.text.DefaultMessageFormatter;
import net.ubos.util.text.StringifierException;
import net.ubos.util.text.StringifierParameters;

/**
 * Thrown if one or more MeshObjects could not be found.
 */
public class MeshObjectsNotFoundException
        extends
            AbstractLocalizedException
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( MeshObjectsNotFoundException.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView that threw this Exception
     * @param identifier the identifier of the MeshObject that was not found
     */
    public MeshObjectsNotFoundException(
            MeshBaseView         mbv,
            MeshObjectIdentifier identifier )
    {
        this( mbv, new MeshObjectIdentifier[] { identifier } );
    }

    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView that threw this Exception
     * @param identifiers the identifiers of the MeshObjects some of which were not found
     */
    public MeshObjectsNotFoundException(
            MeshBaseView            mbv,
            MeshObjectIdentifier [] identifiers )
    {
        theMeshBaseView         = mbv;
        theAttemptedIdentifiers = identifiers;
    }

    /**
     * Determine the identifiers of the MeshObjects that could not be found.
     *
     * @return the identifiers
     */
    public MeshObjectIdentifier [] getMeshObjectIdentifiers()
    {
        return theAttemptedIdentifiers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
                theAttemptedIdentifiers
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String [] {
                    "attempted"
                },
                new Object[] {
                    theAttemptedIdentifiers
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        try {
            String ret = DefaultMessageFormatter.TEXT_PLAIN.format(
                    findResourceHelperForLocalizedMessage().getResourceString(
                            theAttemptedIdentifiers.length > 1 ? PLURAL_STRING_REPRESENTATION_KEY : SINGULAR_STRING_REPRESENTATION_KEY ),
                    StringifierParameters.EMPTY,
                    getLocalizationParameters() );

            return ret;

        } catch( StringifierException ex ) {
            log.error( ex );
            return getClass().getName() + ": error converting to localized message";
        }
    }

    /**
     * The MeshBaseView in which this Exception occurred.
     */
    protected MeshBaseView theMeshBaseView;

    /**
     * The identifiers of the MeshObjects that were attempted to be accessed.
     */
    protected MeshObjectIdentifier [] theAttemptedIdentifiers;

    /**
     * Key into the ResourceHelper that helps with stringifying the singular case.
     */
    public static final String SINGULAR_STRING_REPRESENTATION_KEY = "PlainSingularString";

    /**
     * Key into the ResourceHelper that helps with stringifying the plural case.
     */
    public static final String PLURAL_STRING_REPRESENTATION_KEY = "PlainPluralString";
}
