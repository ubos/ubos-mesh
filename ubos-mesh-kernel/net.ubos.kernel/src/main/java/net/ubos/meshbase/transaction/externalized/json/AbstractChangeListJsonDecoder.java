//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized.json;

import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import net.ubos.mesh.externalized.json.AbstractMeshObjectJsonDecoder;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributeChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributesAddChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributesRemoveChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectCreateChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectDeleteChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectPropertyChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributeChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributesAddChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributesRemoveChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRolePropertyChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleTypeAddChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleTypeRemoveChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectBlessChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectUnblessChange;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.util.ArrayHelper;
import net.ubos.model.primitives.externalized.DecodingException;

/**
 * Knows how to decode a set of Changes from JSON.
 */
public abstract class AbstractChangeListJsonDecoder
    extends
        AbstractMeshObjectJsonDecoder
    implements
        ChangeListJsonTags
{
    /**
     * Handle the list of changes.
     *
     * @param r read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the changes contained in the stream
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public List<ExternalizedChange> handleChanges(
            Reader                                   r,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        return decodeChanges( new JsonReader( r ), idDeserializer );
    }

    /**
     * Decode a list of changes.
     *
     * @param r read the data from here
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return the changes contained in the stream
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public List<ExternalizedChange> decodeChanges(
            Reader                           r,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        JsonReader jr = new JsonReader( r );
        return decodeChanges( jr, idDeserializer );
    }

    /**
     * Decode a list of changes.
     *
     * @param jr read the data from here
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return the changes contained in the stream
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public List<ExternalizedChange> decodeChanges(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        ArrayList<ExternalizedChange> ret = new ArrayList<>();

        jr.beginArray();
        while( jr.hasNext() ) {
            jr.beginObject();

            ExternalizedChange change;

            String name = jr.nextName();
            switch( name ) {
                case CHANGE_TYPE_TAG:
                    String changeType = jr.nextString();
                    change = handleChangeRemainder( changeType, jr, idDeserializer );
                    break;

                default:
                    // not what we expected
                    throw new DecodingException.Syntax( "Looking for name " + CHANGE_TYPE_TAG + ", found: " + name );
            }

            ret.add( change );

            jr.endObject();
        }
        jr.endArray();

        return ret;
    }

    /**
     * Handle the remainder of the Json Object representing a Change
     *
     * @param changeType the type of change
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleChangeRemainder(
            String                           changeType,
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        ExternalizedChange ret;

        switch( changeType ) {
            case ATTRIBUTE_CHANGE_TAG:
                ret = handleAttributeChangeRemainder( jr, idDeserializer );
                break;

            case ATTRIBUTES_ADD_TAG:
                ret = handleAttributesAddRemainder( jr, idDeserializer );
                break;

            case ATTRIBUTES_REMOVE_TAG:
                ret = handleAttributesRemoveRemainder( jr, idDeserializer );
                break;

            case CREATE_TAG:
                ret = handleCreateChangeRemainder( jr, idDeserializer );
                break;

            case DELETE_TAG:
                ret = handleDeleteChangeRemainder( jr, idDeserializer );
                break;

            case PROPERTY_CHANGE_TAG:
                ret = handlePropertyChangeRemainder( jr, idDeserializer );
                break;

            case ROLE_ATTRIBUTE_CHANGE_TAG:
                ret = handleRoleAttributeChangeRemainder( jr, idDeserializer );
                break;

            case ROLE_ATTRIBUTES_ADD_TAG:
                ret = handleRoleAttributesAddRemainder( jr, idDeserializer );
                break;

            case ROLE_ATTRIBUTES_REMOVE_TAG:
                ret = handleRoleAttributesRemoveRemainder( jr, idDeserializer );
                break;

            case ROLE_PROPERTY_CHANGE_TAG:
                ret = handleRolePropertyChangeRemainder( jr, idDeserializer );
                break;

            case ROLE_TYPE_ADD_TAG:
                ret = handleRoleTypeAddRemainder( jr, idDeserializer );
                break;

            case ROLE_TYPE_REMOVE_TAG:
                ret = handleRoleTypeRemoveRemainder( jr, idDeserializer );
                break;

            case TYPE_ADD_TAG:
                ret = handleTypeAddRemainder( jr, idDeserializer );
                break;

            case TYPE_REMOVE_TAG:
                ret = handleTypeRemoveRemainder( jr, idDeserializer );
                break;

            default:
                throw new DecodingException.Syntax( "Unexpected change type: " + changeType );
        }
        return ret;
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleAttributeChangeRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String meshObject    = null;
            String attributeName = null;
            String oldValue      = null;
            String newValue      = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case ATTRIBUTE_NAME_TAG:
                        attributeName = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValue = jr.nextString();
                        break;

                    case NEW_VALUE_TAG:
                        newValue = jr.nextString();
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectAttributeChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    attributeName,
                    base64StringAsSerializable( oldValue ),
                    base64StringAsSerializable( newValue ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleAttributesAddRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String    meshObject  = null;
            String [] oldValues   = null;
            String [] deltaValues = null;
            String [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleStringArray( jr );
                        break;

                    case ADDED_VALUE_TAG:
                        deltaValues = handleStringArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleStringArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectAttributesAddChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues );

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleAttributesRemoveRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String    meshObject  = null;
            String [] oldValues   = null;
            String [] deltaValues = null;
            String [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleStringArray( jr );
                        break;

                    case REMOVED_VALUE_TAG:
                        deltaValues = handleStringArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleStringArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectAttributesRemoveChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues );

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleCreateChangeRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String meshObject = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectCreateChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleDeleteChangeRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String meshObject = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectDeleteChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handlePropertyChangeRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String        meshObject   = null;
            PropertyType  propertyType = null;
            PropertyValue oldValue     = null;
            PropertyValue newValue     = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case PROPERTY_TYPE_TAG:
                        propertyType = findPropertyType( jr.nextString() );
                        break;

                    case OLD_VALUE_TAG:
                        oldValue = parsePropertyValue( propertyType.getDataType(), jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValue = parsePropertyValue( propertyType.getDataType(), jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectPropertyChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    propertyType.getIdentifier(),
                    oldValue,
                    newValue );

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleRoleAttributeChangeRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String meshObject    = null;
            String neighbor      = null;
            String attributeName = null;
            String oldValue      = null;
            String newValue      = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case NEIGHBOR_TAG:
                        neighbor = jr.nextString();
                        break;

                    case ATTRIBUTE_NAME_TAG:
                        attributeName = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValue = jr.nextString();
                        break;

                    case NEW_VALUE_TAG:
                        newValue = jr.nextString();
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectRoleAttributeChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    attributeName,
                    base64StringAsSerializable( oldValue ),
                    base64StringAsSerializable( newValue ),
                    idDeserializer.meshObjectIdentifierFromExternalForm( neighbor ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleRoleAttributesAddRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String    meshObject  = null;
            String    neighbor    = null;
            String [] oldValues   = null;
            String [] deltaValues = null;
            String [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case NEIGHBOR_TAG:
                        neighbor = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleStringArray( jr );
                        break;

                    case ADDED_VALUE_TAG:
                        deltaValues = handleStringArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleStringArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectRoleAttributesAddChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues,
                    idDeserializer.meshObjectIdentifierFromExternalForm( neighbor ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleRoleAttributesRemoveRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String    meshObject  = null;
            String    neighbor    = null;
            String [] oldValues   = null;
            String [] deltaValues = null;
            String [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case NEIGHBOR_TAG:
                        neighbor = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleStringArray( jr );
                        break;

                    case ADDED_VALUE_TAG:
                        deltaValues = handleStringArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleStringArray( jr );
                        break;

                    case REMOVED_VALUE_TAG:
                        deltaValues = handleStringArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectRoleAttributesRemoveChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues,
                    idDeserializer.meshObjectIdentifierFromExternalForm( neighbor ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleRolePropertyChangeRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String        meshObject   = null;
            String        neighbor     = null;
            PropertyType  propertyType = null;
            PropertyValue oldValue     = null;
            PropertyValue newValue     = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case NEIGHBOR_TAG:
                        neighbor = jr.nextString();
                        break;

                    case PROPERTY_TYPE_TAG:
                        propertyType = findPropertyType( jr.nextString() );
                        break;

                    case OLD_VALUE_TAG:
                        oldValue = parsePropertyValue( propertyType.getDataType(), jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValue = parsePropertyValue( propertyType.getDataType(), jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectRolePropertyChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    propertyType.getIdentifier(),
                    oldValue,
                    newValue,
                    idDeserializer.meshObjectIdentifierFromExternalForm( neighbor ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleRoleTypeAddRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String                meshObject  = null;
            String                neighbor    = null;
            MeshTypeIdentifier [] oldValues   = null;
            MeshTypeIdentifier [] deltaValues = null;
            MeshTypeIdentifier [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case NEIGHBOR_TAG:
                        neighbor = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case ADDED_VALUE_TAG:
                        deltaValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectRoleTypeAddChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues,
                    idDeserializer.meshObjectIdentifierFromExternalForm( neighbor ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleRoleTypeRemoveRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String                meshObject  = null;
            String                neighbor    = null;
            MeshTypeIdentifier [] oldValues   = null;
            MeshTypeIdentifier [] deltaValues = null;
            MeshTypeIdentifier [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case NEIGHBOR_TAG:
                        neighbor = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case ADDED_VALUE_TAG:
                        deltaValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectRoleTypeRemoveChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues,
                    idDeserializer.meshObjectIdentifierFromExternalForm( neighbor ));

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleTypeAddRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String                meshObject  = null;
            MeshTypeIdentifier [] oldValues   = null;
            MeshTypeIdentifier [] deltaValues = null;
            MeshTypeIdentifier [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case ADDED_VALUE_TAG:
                        deltaValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectBlessChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues );

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse a particular type of ExternalizedChange from the remainder of a JsonObject
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the found ExternalizedChange
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected ExternalizedChange handleTypeRemoveRemainder(
            JsonReader                       jr,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        try {
            String                meshObject  = null;
            MeshTypeIdentifier [] oldValues   = null;
            MeshTypeIdentifier [] deltaValues = null;
            MeshTypeIdentifier [] newValues   = null;

            while( jr.hasNext() ) {
                String name = jr.nextName();
                switch( name ) {
                    case MESHOBJECT_TAG:
                        meshObject = jr.nextString();
                        break;

                    case OLD_VALUE_TAG:
                        oldValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case REMOVED_VALUE_TAG:
                        deltaValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    case NEW_VALUE_TAG:
                        newValues = handleMeshTypeIdentifierArray( jr );
                        break;

                    default:
                        throw new DecodingException.Syntax( "Unexpected tag: " + name );
                }
            }

            return new ExternalizedMeshObjectUnblessChange(
                    idDeserializer.meshObjectIdentifierFromExternalForm( meshObject ),
                    oldValues,
                    deltaValues,
                    newValues );

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Helper to parse a String array.
     *
     * @param jr where to read from
     * @return the String array
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected String [] handleStringArray(
            JsonReader jr )
        throws
            DecodingException,
            IOException
    {
        ArrayList<String> tmp = new ArrayList<>();
        jr.beginArray();
        while( jr.hasNext() ) {
            tmp.add( jr.nextString() );
        }
        jr.endArray();

        return ArrayHelper.copyIntoNewArray( tmp, String.class );
    }

    /**
     * Helper to parse a String array into an array of MeshTypeIdentifier
     *
     * @param jr where to read from
     * @return the MeshTypeIdentifiers
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected MeshTypeIdentifier [] handleMeshTypeIdentifierArray(
            JsonReader jr )
        throws
            DecodingException,
            IOException
    {
        try {
            ArrayList<MeshTypeIdentifier> tmp = new ArrayList<>();
            jr.beginArray();
            while( jr.hasNext() ) {
                tmp.add( theTypeIdDeserializer.fromExternalForm( jr.nextString() ));
            }
            jr.endArray();

            return ArrayHelper.copyIntoNewArray( tmp, MeshTypeIdentifier.class );

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Look up a PropertyType based on its identifier.
     *
     * @param raw the String identifier
     * @return the PropertyType
     * @throws DecodingException thrown if decoding some serialized data failed
     */
    protected PropertyType findPropertyType(
            String raw )
        throws
            DecodingException
    {
        try {
            PropertyType ret = ModelBase.SINGLETON.findPropertyType( theTypeIdDeserializer.fromExternalForm( raw ));
            return ret;

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new DecodingException.Installation( ex );
        }
    }
}
