//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.util.event.PropertyChange;

/**
  * This indicates a change in the state of a MeshObject, such as
  * "the MeshObject died". Subclasses implement specific state changes.
  */
public abstract class MeshObjectStateChange
        extends
            PropertyChange<MeshObject,String,MeshObjectStateChange.MeshObjectState>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValue the old value of the MeshObjectState, prior to the event
     * @param newValue the new value of the MeshObjectState, after the event
     */
    protected MeshObjectStateChange(
            MeshObject      source,
            MeshObjectState oldValue,
            MeshObjectState newValue )
    {
        super(  source,
                EVENT_NAME,
                oldValue,
                newValue, // delta = new
                newValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject[] { getSource() };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( ": " );
        ret.append( theNewValue );
        return ret.toString();
    }

    /**
     * Common super-interface for all MeshObjectStates.
     */
    public static interface MeshObjectState
    {}

    /**
     * Name of this event.
     */
    public static final String EVENT_NAME = "MeshObjectStateChanged";
}
