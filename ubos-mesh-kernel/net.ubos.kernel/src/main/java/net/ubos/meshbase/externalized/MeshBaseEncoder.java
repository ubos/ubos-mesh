//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.io.IOException;
import java.io.Writer;
import net.ubos.mesh.externalized.Encoder;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.externalized.EncodingException;

/**
 *
 */
public interface MeshBaseEncoder
    extends
        Encoder
{
    /**
     * Bulk write the content of a MeshBase.
     *
     * @param mb the MeshBase
     * @param nsMap the namespace map to use
     * @param w the Writer to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    public void bulkWrite(
            MeshBase                                   mb,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            Writer                                     w )
        throws
            IOException,
            EncodingException;
}
