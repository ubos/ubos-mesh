//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.event.PropertyChange;

/**
 * <p>This indicates a change in the number of RoleAttributes of this MeshObject
 * with respect to a neighbor MeshObject.
 *
 * <p>This extends PropertyChange so we can keep the well-known JavaBeans
 event generation model that programmers are used to.
 */
public abstract class AbstractMeshObjectRoleAttributesChange
        extends
            PropertyChange<MeshObject,String,String[]>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old values of the names of the RoleAttributes, prior to the event
     * @param deltaValues the names of the RoleAttributes that changed
     * @param newValues the new values of the names of the RoleAttributes, after the event
     * @param neighbor the neighbor MeshOBject
     */
    protected AbstractMeshObjectRoleAttributesChange(
            MeshObject source,
            String []  oldValues,
            String []  deltaValues,
            String []  newValues,
            MeshObject neighbor )
    {
        super(  source,
                EVENT_NAME,
                ArrayHelper.checkNoNullArrayMembers( oldValues ),
                ArrayHelper.checkNoNullArrayMembers( deltaValues ),
                ArrayHelper.checkNoNullArrayMembers( newValues ));

        theNeighbor = neighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject[] { getSource(), theNeighbor };
    }

    /**
     * Obtain the neighbor MeshObject
     *
     * @return the neighbor MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * Obtain the names of the affected RoleAttributes.
     *
     * @return the names
     */
    public String [] getAffectedRoleAttributes()
    {
        return getDeltaValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        // We consolidate everything into (at most) one MeshObjectAttributesAddChange event (as early as possible)
        // and one MeshObjectAttributesRemoveChange (as late a possible). We can do this by simply
        // looking at the earliest "old" value and the latest "new" value

        String [] oldValue = null; // this will not remain null, because we also look at ourselves
        int       earliest = -1;   // latest removed = index

        for( int i=0 ; i<=index ; ++i ) { // include ourselves at index
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof AbstractMeshObjectRoleAttributesChange )) {
                continue;
            }
            AbstractMeshObjectRoleAttributesChange realCurrentChange = (AbstractMeshObjectRoleAttributesChange) currentChange;
            if( !theNeighbor.equals( realCurrentChange.theNeighbor )) {
                continue;
            }
            oldValue = realCurrentChange.getOldValue();
            earliest = i;
            break;
        }
        ArrayHelper.Difference<String> diff = ArrayHelper.determineDifference( oldValue, theNewValue, true, String.class );

        if( diff.getAdditions().length > 0 ) {
            MeshObjectRoleAttributesAddChange newAddChange = new MeshObjectRoleAttributesAddChange(
                    theSource,
                    oldValue,
                    diff.getAdditions(),
                    ArrayHelper.append( oldValue, diff.getAdditions(), String.class ),
                    theNeighbor );
            list.replaceChangeAt( earliest, newAddChange);
        } else if( earliest > -1 && earliest < index ) {
            list.blankChangeAt( earliest );
        }
        if( diff.getRemovals().length > 0 ) {
            MeshObjectRoleAttributesRemoveChange newRemoveChange = new MeshObjectRoleAttributesRemoveChange(
                    theSource,
                    ArrayHelper.append( theNewValue, diff.getRemovals(), String.class ),
                    diff.getRemovals(),
                    theNewValue,
                    theNeighbor );
            list.replaceChangeAt( index, newRemoveChange);
        } else if( earliest > -1 && earliest < index ) {
            list.blankChangeAt( index );
        }

        for( int i=earliest+1 ; i<index ; ++i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( currentChange instanceof AbstractMeshObjectRoleAttributesChange ) {
                list.blankChangeAt( i );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( " -> " );
        ret.append( theNeighbor.getIdentifier() );
        ret.append( ": " );

        String sep = "";
        for( String att : theDeltaValue ) {
            ret.append( sep );
            ret.append( att );
            sep = ", ";
        }
        return ret.toString();
    }

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * Name of this event.
     */
    public static final String EVENT_NAME = "MeshObjectRoleAttributesChange";
}
