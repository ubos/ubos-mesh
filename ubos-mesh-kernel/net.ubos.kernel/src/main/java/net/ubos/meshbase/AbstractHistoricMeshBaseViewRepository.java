//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.smartmap.AbstractSmartMap;

/**
 * In-memory store for MeshObjects in a historical MeshBaseView. Looks up / creates
 * MeshObject instances if needed to fill in when a MeshObject didn't change in
 * this MeshBaseView.
 */
public abstract class AbstractHistoricMeshBaseViewRepository
    extends
        AbstractSmartMap<MeshObjectIdentifier,MeshObject>
{
     /**
     * Set the MeshBaseView after initialization.
     *
     * @param mbv the meshBaseView to which we belong.
     */
    public void setMeshBaseView(
            AbstractEditableHistoricMeshBaseView mbv )
    {
        if( theMeshBaseView != null ) {
            throw new IllegalStateException( "Cannot set MeshBaseView more than once" );
        }
        theMeshBaseView = mbv;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            MeshObjectIdentifier key,
            MeshObject           value )
    {
        throw new UnsupportedOperationException( "Must not be invoked on " + this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObjectIdentifier> keyIterator()
    {
        CursorIterator<MeshObjectIdentifier> ret = new MappingCursorIterator<>(
                theMeshBaseView.getMeshBase().meshObjectHistoriesIterator(),
                new MappingCursorIterator.Mapper<>() {
                        @Override
                        public MeshObjectIdentifier mapDelegateValueToHere(
                                MeshObjectHistory delegateValue )
                        {
                            return delegateValue.getMeshObjectIdentifier();
                        }

                        @Override
                        public MeshObjectHistory mapHereToDelegateValue(
                                MeshObjectIdentifier value )
                        {
                            return theMeshBaseView.getMeshBase().meshObjectHistory( value );
                        }
               } );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> valueIterator()
    {
        CursorIterator<MeshObject> meshObjectOrNullIter = new MappingCursorIterator<>(
                theMeshBaseView.getMeshBase().meshObjectHistoriesIterator(),
                new MappingCursorIterator.Mapper<>() {
                        @Override
                        public MeshObject mapDelegateValueToHere(
                                MeshObjectHistory delegateValue )
                        {
                            MeshObject ret = get( delegateValue.getMeshObjectIdentifier() );
                            return ret;
                        }

                        @Override
                        public MeshObjectHistory mapHereToDelegateValue(
                                MeshObject value )
                        {
                            throw new UnsupportedOperationException( "Should not be invoked on " + this );
                        }
               } );

        CursorIterator<MeshObject> actuallyPresentIter = FilteringCursorIterator.create(
                meshObjectOrNullIter,
                ( MeshObject candidate ) -> candidate != null && !candidate.getIsDead());

        return actuallyPresentIter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueUpdated(
            MeshObjectIdentifier key,
            MeshObject           value )
    {
        throw new UnsupportedOperationException( "Must not be invoked on " + this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        CursorIterator<MeshObject> iter = valueIterator();

        int ret = iter.moveToAfterLast();

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        return false; // there's always a home object
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            MeshObjectIdentifier key )
    {
        throw new UnsupportedOperationException( "Must not be invoked on " + this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        throw new UnsupportedOperationException( "Must not be invoked on " + this );
    }

    /**
     * The historical MeshBaseView for which this is a cache
     */
    protected AbstractEditableHistoricMeshBaseView theMeshBaseView;
}
