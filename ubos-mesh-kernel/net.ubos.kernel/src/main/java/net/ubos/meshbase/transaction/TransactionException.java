//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.MeshBaseView;

/**
  * A superclass for all {@link Transaction}-related Exceptions.
  */
public abstract class TransactionException
        extends
            RuntimeException
{
    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView that raised this TransactionException
     * @param tx the Transaction that raised this TransactionException
     */
    protected TransactionException(
            MeshBaseView mbv,
            Transaction  tx )
    {
        theMeshBaseView = mbv;
        theTransaction  = tx;
    }

    /**
     * Obtain the MeshBaseView that raised this TransactionException.
     *
     * @return the MeshBaseView that that raised this TransactionException
     */
    public MeshBaseView getMeshBaseView()
    {
        return theMeshBaseView;
    }

    /**
     * Obtain the Transaction that raised this TransactionException.
     *
     * @return the Transaction that raised this TransactionException
     */
    public Transaction getTransaction()
    {
        return theTransaction;
    }

    /**
     * Convert this into a string, for debugging.
     *
     * @return this instance as a string
     */
    @Override
    public String toString()
    {
        return super.toString() + " -- MBV: " + theMeshBaseView + ", TX: " + theTransaction;
    }

    /**
     * The MeshBaseView the raised this TransactionException.
     */
    protected final MeshBaseView theMeshBaseView;

    /**
     * The Transaction that raised this TransactionException.
     */
    protected final Transaction theTransaction;
}
