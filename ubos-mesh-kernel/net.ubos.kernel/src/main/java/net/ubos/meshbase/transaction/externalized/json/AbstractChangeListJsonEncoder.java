//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized.json;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import net.ubos.mesh.externalized.json.AbstractMeshObjectJsonEncoder;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributeChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributesAddChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectAttributesRemoveChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectCreateChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectDeleteChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectPropertyChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributeChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributesAddChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributesRemoveChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRolePropertyChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleTypeAddChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleTypeRemoveChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectBlessChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectUnblessChange;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;

/**
 * Knows how to encode lists of changes in JSON.
 */
public abstract class AbstractChangeListJsonEncoder
    extends
        AbstractMeshObjectJsonEncoder
    implements
        ChangeListJsonTags
{
    /**
     * Constructor.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected AbstractChangeListJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        theTypeIdSerializer = typeIdSerializer;
    }

    /**
     * Write a list of changes to a Writer.
     *
     * @param changes the ChangeList
     * @param idSerializer use this to serialize MeshObjectIdentifiers
     * @param emitOldValues if true, always emit old values even if they are redundant
     * @param emitNewValues if true, always emit new values even if they are redundant
     * @param w the Writer to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    public void writeChangeList(
            ChangeList                     changes,
            MeshObjectIdentifierSerializer idSerializer,
            boolean                        emitOldValues,
            boolean                        emitNewValues,
            Writer                         w )
        throws
            EncodingException,
            IOException
    {
        JsonWriter jw = new JsonWriter( w );
        jw.setSerializeNulls( true );
        jw.setIndent( " " );

        writeChangeList( changes, idSerializer, emitOldValues, emitNewValues, jw );

        jw.flush();
    }

    /**
     * Write the list of changes.
     *
     * @param changes the changes
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param emitOldValues if true, always emit old values even if they are redundant
     * @param emitNewValues if true, always emit new values even if they are redundant
     * @param jw the JsonWriter to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    protected void writeChangeList(
            ChangeList                     changes,
            MeshObjectIdentifierSerializer idSerializer,
            boolean                        emitOldValues,
            boolean                        emitNewValues,
            JsonWriter                     jw )
        throws
            IOException,
            EncodingException
    {
        jw.beginArray();
        for( Change change : changes ) {
            writeChange( change.asExternalized(), idSerializer, emitOldValues, emitNewValues, jw );
        }
        jw.endArray();
    }

    /**
     * Serialize a Change to a JsonWriter.
     *
     * @param change the Change
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param emitOldValues if true, always emit old values even if they are redundant
     * @param emitNewValues if true, always emit new values even if they are redundant
     * @param jw the JsonWriter to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    public void writeChange(
            ExternalizedChange             change,
            MeshObjectIdentifierSerializer idSerializer,
            boolean                        emitOldValues,
            boolean                        emitNewValues,
            JsonWriter                     jw )
        throws
            EncodingException,
            IOException
    {
        if( change instanceof ExternalizedMeshObjectAttributeChange ) {
            ExternalizedMeshObjectAttributeChange realChange = (ExternalizedMeshObjectAttributeChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ATTRIBUTE_CHANGE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( ATTRIBUTE_NAME_TAG ).value( realChange.getAttributeName() );
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                jw.value( serializableAsBase64String( realChange.getOldValue()));
            }
            jw.name( NEW_VALUE_TAG );
            jw.value( serializableAsBase64String( realChange.getNewValue()));
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectAttributesAddChange ) {
            ExternalizedMeshObjectAttributesAddChange realChange = (ExternalizedMeshObjectAttributesAddChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ATTRIBUTES_ADD_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeAttributeNames( realChange.getOldValue(), jw );
            }
            jw.name( ADDED_VALUE_TAG );
            writeAttributeNames( realChange.getAdded(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeAttributeNames( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectAttributesRemoveChange ) {
            ExternalizedMeshObjectAttributesRemoveChange realChange = (ExternalizedMeshObjectAttributesRemoveChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ATTRIBUTES_REMOVE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeAttributeNames( realChange.getOldValue(), jw );
            }
            jw.name( REMOVED_VALUE_TAG );
            writeAttributeNames( realChange.getRemoved(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeAttributeNames( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectCreateChange ) {
            ExternalizedMeshObjectCreateChange realChange = (ExternalizedMeshObjectCreateChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value(CREATE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectDeleteChange ) {
            ExternalizedMeshObjectDeleteChange realChange = (ExternalizedMeshObjectDeleteChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( DELETE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectPropertyChange ) {
            ExternalizedMeshObjectPropertyChange realChange = (ExternalizedMeshObjectPropertyChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( PROPERTY_CHANGE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( PROPERTY_TYPE_TAG ).value( theTypeIdSerializer.toExternalForm( realChange.getPropertyTypeIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writePropertyValue( realChange.getOldValue(), jw );
            }
            jw.name( NEW_VALUE_TAG );
            writePropertyValue( realChange.getNewValue(), jw );
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectRoleAttributeChange ) {
            ExternalizedMeshObjectRoleAttributeChange realChange = (ExternalizedMeshObjectRoleAttributeChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ROLE_ATTRIBUTE_CHANGE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( NEIGHBOR_TAG ).value( idSerializer.toExternalForm( realChange.getNeighborIdentifier()));
            jw.name( ATTRIBUTE_NAME_TAG ).value( realChange.getAttributeName() );
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                jw.value( serializableAsBase64String( realChange.getOldValue()));
            }
            jw.name( NEW_VALUE_TAG );
            jw.value( serializableAsBase64String( realChange.getNewValue()));
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectRoleAttributesAddChange ) {
            ExternalizedMeshObjectRoleAttributesAddChange realChange = (ExternalizedMeshObjectRoleAttributesAddChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ROLE_ATTRIBUTES_ADD_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( NEIGHBOR_TAG ).value( idSerializer.toExternalForm( realChange.getNeighborIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeAttributeNames( realChange.getOldValue(), jw );
            }
            jw.name( ADDED_VALUE_TAG );
            writeAttributeNames( realChange.getAdded(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeAttributeNames( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectRoleAttributesRemoveChange ) {
            ExternalizedMeshObjectRoleAttributesRemoveChange realChange = (ExternalizedMeshObjectRoleAttributesRemoveChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ROLE_ATTRIBUTES_REMOVE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( NEIGHBOR_TAG ).value( idSerializer.toExternalForm( realChange.getNeighborIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeAttributeNames( realChange.getOldValue(), jw );
            }
            jw.name( REMOVED_VALUE_TAG );
            writeAttributeNames( realChange.getRemoved(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeAttributeNames( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectRolePropertyChange ) {
            ExternalizedMeshObjectRolePropertyChange realChange = (ExternalizedMeshObjectRolePropertyChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ROLE_PROPERTY_CHANGE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( NEIGHBOR_TAG ).value( idSerializer.toExternalForm( realChange.getNeighborIdentifier()));
            jw.name( PROPERTY_TYPE_TAG ).value( theTypeIdSerializer.toExternalForm( realChange.getPropertyTypeIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writePropertyValue( realChange.getOldValue(), jw );
            }
            jw.name( NEW_VALUE_TAG );
            writePropertyValue( realChange.getNewValue(), jw );
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectRoleTypeAddChange ) {
            ExternalizedMeshObjectRoleTypeAddChange realChange = (ExternalizedMeshObjectRoleTypeAddChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ROLE_TYPE_ADD_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( NEIGHBOR_TAG ).value( idSerializer.toExternalForm( realChange.getNeighborIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeTypeIdentifiers( realChange.getOldValue(), jw );
            }
            jw.name( ADDED_VALUE_TAG );
            writeTypeIdentifiers( realChange.getAdded(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeTypeIdentifiers( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectRoleTypeRemoveChange ) {
            ExternalizedMeshObjectRoleTypeRemoveChange realChange = (ExternalizedMeshObjectRoleTypeRemoveChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( ROLE_TYPE_REMOVE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            jw.name( NEIGHBOR_TAG ).value( idSerializer.toExternalForm( realChange.getNeighborIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeTypeIdentifiers( realChange.getOldValue(), jw );
            }
            jw.name( ADDED_VALUE_TAG );
            writeTypeIdentifiers( realChange.getRemoved(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeTypeIdentifiers( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectBlessChange ) {
            ExternalizedMeshObjectBlessChange realChange = (ExternalizedMeshObjectBlessChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( TYPE_ADD_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeTypeIdentifiers( realChange.getOldValue(), jw );
            }
            jw.name( ADDED_VALUE_TAG );
            writeTypeIdentifiers( realChange.getAdded(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeTypeIdentifiers( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else if( change instanceof ExternalizedMeshObjectUnblessChange ) {
            ExternalizedMeshObjectUnblessChange realChange = (ExternalizedMeshObjectUnblessChange) change;

            jw.beginObject();
            jw.name( CHANGE_TYPE_TAG ).value( TYPE_REMOVE_TAG );
            jw.name( MESHOBJECT_TAG ).value( idSerializer.toExternalForm( realChange.getSourceIdentifier()));
            if( emitOldValues ) {
                jw.name( OLD_VALUE_TAG );
                writeTypeIdentifiers( realChange.getOldValue(), jw );
            }
            jw.name( REMOVED_VALUE_TAG );
            writeTypeIdentifiers( realChange.getRemoved(), jw );
            if( emitNewValues ) {
                jw.name( NEW_VALUE_TAG );
                writeTypeIdentifiers( realChange.getNewValue(), jw );
            }
            jw.endObject();

        } else  {
            jw.value(  "?" );
        }
    }

    /**
     * Helper to emit an array of Attribute names to a JsonWriter.
     *
     * @param names the names of the Attributes
     * @param out the JsonWriter to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    protected void writeAttributeNames(
            String []  names,
            JsonWriter out )
        throws
            EncodingException,
            IOException
    {
        out.beginArray();
        for( String name : names ) {
            out.value( name );
        }
        out.endArray();
    }

    /**
     * Helper to emit an array of Identifiers to a JsonWriter.
     *
     * @param ids the identifiers
     * @param out the JsonWriter to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    protected void writeTypeIdentifiers(
            MeshTypeIdentifier [] ids,
            JsonWriter            out )
        throws
            EncodingException,
            IOException
    {
        out.beginArray();
        for( MeshTypeIdentifier id : ids ) {
            out.value( theTypeIdSerializer.toExternalForm( id ) );
        }
        out.endArray();
    }

    /**
     * Knows how to serialize MeshTypeIdentifiers.
     */
    protected final MeshTypeIdentifierSerializer theTypeIdSerializer;
}
