//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized.json;

/**
 * JSON tags for expressing Changes in JSON.
 */
public interface ChangeListJsonTags
{
    /** Encoding ID */
    public static final String CHANGE_SET_JSON_ENCODING_ID
            = ChangeListJsonTags.class.getPackage().getName();

    /** Tag indicating the body content with the Changes. */
    public static final String CHANGES_TAG = "Changes";

    /** Key identifying the type of Change represented. */
    public static final String CHANGE_TYPE_TAG = "CT";

    /** Value identifying a Change as an Attribute Change Event. */
    public static final String ATTRIBUTE_CHANGE_TAG = "AC";

    /** Value identifying a Change as an Attributes Add Event. */
    public static final String ATTRIBUTES_ADD_TAG = "AA";

    /** Value identifying a Change as an Attributes Remove Event. */
    public static final String ATTRIBUTES_REMOVE_TAG = "AR";

    /** Value identifying a Change as a MeshObject Create Event. */
    public static final String CREATE_TAG = "C";

    /** Value identifying a Change as a MeshObject Delete Event. */
    public static final String DELETE_TAG = "D";

    /** Value identifying a Change as a PropertyChangeEvent. */
    public static final String PROPERTY_CHANGE_TAG = "PC";

    /** Value identifying a Change as a Role Attribute Change Event. */
    public static final String ROLE_ATTRIBUTE_CHANGE_TAG = "RAC";

    /** Value identifying a Change as a Role Attribute Add Event. */
    public static final String ROLE_ATTRIBUTES_ADD_TAG = "RAA";

    /** Value identifying a Change as a Role Attribute Remove Event. */
    public static final String ROLE_ATTRIBUTES_REMOVE_TAG = "RAR";

    /** Value identifying a Change as a Role PropertyChangeEvent. */
    public static final String ROLE_PROPERTY_CHANGE_TAG = "RPC";

    /** Value identifying a Change as a Role Type Add Event. */
    public static final String ROLE_TYPE_ADD_TAG = "RTA";

    /** Value identifying a Change as a Role Type Remove Event. */
    public static final String ROLE_TYPE_REMOVE_TAG = "RTR";

    /** Value identifying a Change as a Type Add Event. */
    public static final String TYPE_ADD_TAG = "TA";

    /** Value identifying a Change as a Type Remove Event. */
    public static final String TYPE_REMOVE_TAG = "TR";

    /** Key identifying the identifying of the affected neighbor MeshObject. */
    public static final String NEIGHBOR_TAG = "N";

    /** Key identifying the name of an Attribute. */
    public static final String ATTRIBUTE_NAME_TAG = "AN";

    /** Key identifying he identifier of a PropertyType. */
    public static final String PROPERTY_TYPE_TAG = "PT";

    /** Key identifying the old value or values. */
    public static final String OLD_VALUE_TAG = "OV";

    /** Key identifying the new value or values. */
    public static final String NEW_VALUE_TAG = "NV";

    /** Key identifying the added value or values. */
    public static final String ADDED_VALUE_TAG = "AV";

    /** Key identifying the removed value or values. */
    public static final String REMOVED_VALUE_TAG = "RV";
}
