//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.a;

import java.util.HashSet;
import java.util.Set;
import net.ubos.mesh.AbstractMeshObject;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.IllegalPropertyValueException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MultiplicityException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.transaction.AbstractTransaction;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectCreateChange;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.logging.Log;

/**
 * "A" implementation of Transaction
 */
public abstract class ATransaction
    extends
        AbstractTransaction
{
    private static final Log log = Log.getLogInstance( ATransaction.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param mbv the MeshBase that the Transaction guards
     */
    protected ATransaction(
            AbstractEditableMeshBaseView mbv )
    {
        super( mbv );
    }

    /**
     * Find the default MeshBaseView given this Thread and this Transaction. This moves from
     * one MeshBaseView to another during the HistoryTransaction's ripple.
     *
     * @return the MeshBaseView
     */
    public abstract AbstractEditableMeshBaseView getDefaultMeshBaseViewForTransaction();

    /**
     * Add a Change to the ChangeList.
     *
     * @param toAdd the Change to add
     */
    public abstract void addChange(
            Change toAdd );

    /**
     * Commit a transaction.
     *
     * @param normalizeChangeList if true, normalize the ChangeList of a Transaction during commit
     */
    public abstract void commitTransaction(
            boolean normalizeChangeList );

    /**
     * Roll back a transaction.
     *
     * @param t the reason for the rollback
     */
    public abstract void rollbackTransaction(
            Throwable t );

    /**
     * Determine the MeshObjects affected by the Changes in this ChangeList.
     *
     * @param changeList the ChangeList
     * @return the affected MeshObjects
     */
    protected Set<AMeshObject> determineAffectedMeshObject(
            ChangeList changeList )
    {
        HashSet<AMeshObject> affectedSet = new HashSet<>( changeList.size()/2 ); // guess size

        for( Change change : changeList ) {
            MeshObject [] affecteds = change.getAffectedMeshObjects();
            for( MeshObject affected : affecteds ) {
                if( affected.getIsDead() ) {
                    continue;
                }
                affectedSet.add( (AMeshObject) affected );
            }
        }
        return affectedSet;
    }

    /**
     * Check that all MeshObjects affected by the Changes in this ChangeList are still
     * meeting all semantic constraints.
     *
     * @param changeList the ChangeList
     * @throws MultiplicityException a RoleType's multiplicity was violated
     * @throws IllegalPropertyValueException a Property was set to an illegal value, e.g. null in case
     *         of a PropertyType that is not optional
     * @throws NotPermittedException pass-on upwards, should not happen
     */
    protected void checkValidGraph(
            ChangeList changeList )
        throws
            MultiplicityException,
            IllegalPropertyValueException,
            NotPermittedException
    {
        Set<MeshObject> toCheck = changeList.getAffectedMeshObjects();

        // go through the changes, and make sure all MeshObjects touched still meet all the constraints
        for( MeshObject affected : toCheck ) {
            if( affected.getIsDead() ) {
                continue;
            }
            for( EntityType entityType : affected.getEntityTypes()) {
                for( RoleType roleType : entityType.getRoleTypes()) {
                    MeshObject [] others = affected.traverse( roleType ).getMeshObjects();
                    roleType.checkMultiplicity( affected, others );
                }
                for( PropertyType propType : entityType.getPropertyTypes() ) {
                    try {
                        propType.checkValue( affected );
                    } catch( IllegalPropertyTypeException ex ) {
                        log.error( ex );
                    }
                }
            }
        }
    }

    /**
     * This hook is invoked to update the timestamps on affected MeshObjects, so they can have
     * the same timeCreated and timeUpdated as the commit time of the Transaction.
     *
     * @param changeList the ChangeList
     * @param commitTime the time to update to
     */
    protected void updateMeshObjectTimestamps(
            ChangeList changeList,
            long       commitTime )
    {
        // we might set this more than once for the same MeshObject, but that's harmless and cheap
        for( Change c : changeList ) {
            MeshObject [] affecteds = c.getAffectedMeshObjects();

            if( c instanceof MeshObjectCreateChange ) {
                for( MeshObject affected : affecteds ) {
                    if( affected.getIsDead() ) {
                        continue;
                    }
                    AbstractMeshObject realAffected = (AbstractMeshObject) affected;
                    realAffected.setTimeCreated( commitTime );

                    for( MeshObject neighbor : realAffected.traverseToNeighbors()) {
                        AbstractMeshObject realNeighbor = (AbstractMeshObject) neighbor;
                        realNeighbor.neighborTimeCreatedWasUpdated( realAffected );
                    }
                }
            }

            for( MeshObject affected : affecteds ) {
                ((AbstractMeshObject)affected).setTimeUpdated( commitTime );
            }
        }
    }

    /**
     * Helper method.
     *
     * @param obj the MeshObject to put
     */
    public void putCreatedMeshObjectIntoMeshBaseView(
            MeshObject obj )
    {
        theMeshBaseView.putNewMeshObject( obj );
    }

    /**
     * Helper method.
     *
     * @param objId the MeshObject to remobr
     */
    public void removeFromMeshBaseView(
            MeshObjectIdentifier objId )
    {
        theMeshBaseView.removeMeshObject( objId );
    }

    /**
     * Callback notifying that the Transaction is now committed.
     */
    public abstract void transactionCommitted();
}
