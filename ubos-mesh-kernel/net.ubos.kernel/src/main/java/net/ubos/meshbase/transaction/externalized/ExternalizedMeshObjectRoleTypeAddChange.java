//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectRoleTypeAddChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.RoleType;
import net.ubos.modelbase.MeshTypeNotFoundException;

/**
 *
 */
public class ExternalizedMeshObjectRoleTypeAddChange
    extends
        AbstractExternalizedMeshObjectRoleTypeChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier the identifier of the MeshObject that is the source of the event
     * @param oldValue the identifiers of the source RoleTypes, prior to the end
     * @param deltaValue the identifiers of the source RoleTypes that changed during the event
     * @param newValue identifiers of the source RoleTypes, after the event
     * @param neighborIdentifier the identifier of the neighbor MeshObject
     */
    public ExternalizedMeshObjectRoleTypeAddChange(
            MeshObjectIdentifier  sourceIdentifier,
            MeshTypeIdentifier [] oldValue,
            MeshTypeIdentifier [] deltaValue,
            MeshTypeIdentifier [] newValue,
            MeshObjectIdentifier  neighborIdentifier )
    {
        super( sourceIdentifier, oldValue, deltaValue, newValue, neighborIdentifier );
    }

    public MeshTypeIdentifier [] getAdded()
    {
        return theDeltaValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject source   = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );
            MeshObject neighbor = base.findMeshObjectByIdentifierOrThrow( theNeighborIdentifier );

            RoleType [] rts = findRoleTypes(theDeltaValues );

            source.blessRole( rts, neighbor );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );

        } catch( MeshTypeNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleTypeAddChange internalizeWith(
            MeshObject [] affectedMeshObjects )
        throws
            MeshTypeNotFoundException
    {
        if( affectedMeshObjects.length != 2 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectRoleTypeAddChange(
                affectedMeshObjects[0],
                findRoleTypes( theOldValues ),
                findRoleTypes( theDeltaValues ),
                findRoleTypes( theNewValues ),
                affectedMeshObjects[1] );
    }
}
