//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized.json;

import net.ubos.meshbase.transaction.externalized.ChangeListDecoder;

/**
 * Separate class to avoid coding mistakes.
 */
public class ChangeListJsonDecoder
    extends
        AbstractChangeListJsonDecoder
    implements
        ChangeListDecoder
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static ChangeListJsonDecoder create()
    {
        return new ChangeListJsonDecoder();
    }

    @Override
    public String getEncodingId()
    {
        return CHANGE_SET_JSON_ENCODING_ID;
    }
}
