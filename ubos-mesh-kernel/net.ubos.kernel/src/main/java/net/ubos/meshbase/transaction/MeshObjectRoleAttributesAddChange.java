//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRoleAttributesAddChange;
import net.ubos.util.ArrayHelper;

/**
 * <p>This event indicates that at least one RoleAttribute was added to a MeshObject's relationship with another.
 * This is emitted by the MeshObject on whose Role in the Relationship the Attribute was added.
 * @see MeshObjectInverseRoleAttributesAddEvent
 */
public class MeshObjectRoleAttributesAddChange
        extends
            AbstractMeshObjectRoleAttributesChange
{
    /**
     * Pass-through constructor for subclasses.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old values of the names of the RoleAttributes, prior to the event
     * @param deltaValues the names of the RoleAttributes that changed
     * @param newValues the new values of the names of the RoleAttributes, after the event
     * @param neighbor the neighbor MeshObject
     */
    public MeshObjectRoleAttributesAddChange(
            MeshObject source,
            String []  oldValues,
            String []  deltaValues,
            String []  newValues,
            MeshObject neighbor )
    {
        super(  source,
                oldValues,
                deltaValues,
                newValues,
                neighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject      otherObject        = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject      relatedOtherObject = base.findMeshObjectByIdentifier( getNeighbor().getIdentifier() );
        String []       roleAttributeNames = getDeltaValue();
        Serializable [] initialValues      = new Serializable[ roleAttributeNames.length ];

        otherObject.setRoleAttributeValues( relatedOtherObject, roleAttributeNames, initialValues );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributesAddChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject   = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject updatedNeighbor     = mbv.findMeshObjectByIdentifier( theNeighbor.getIdentifier() );
        String []  oldAttributeNames   = updatedMeshObject.getRoleAttributeNames( updatedNeighbor );
        String []  deltaAttributeNames = theDeltaValue;
        String []  newAttributeNames   = ArrayHelper.append( oldAttributeNames, deltaAttributeNames, String.class );

        return new MeshObjectRoleAttributesAddChange(
                updatedMeshObject,
                oldAttributeNames,
                deltaAttributeNames,
                newAttributeNames,
                updatedNeighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributesRemoveChange inverse()
    {
        return new MeshObjectRoleAttributesRemoveChange(
                getSource(),
                getNewValue(),
                getDeltaValue(),
                getOldValue(),
                getNeighbor() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectRoleAttributesRemoveChange )) {
            return false;
        }

        MeshObjectRoleTypeRemoveChange realCandidate = (MeshObjectRoleTypeRemoveChange) candidate;
        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }
        if( !getNeighbor().equals( realCandidate.getNeighbor())) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realCandidate.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectRoleAttributesAddChange )) {
            return false;
        }
        MeshObjectRoleAttributesAddChange realOther = (MeshObjectRoleAttributesAddChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !theNeighbor.equals(realOther.theNeighbor )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realOther.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

   /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = Objects.hash(
                theSource.getIdentifier(),
                theNeighbor.getIdentifier());
        for( String name : getDeltaValue() ) {
            ret ^= name.hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectRoleAttributesAddChange asExternalized()
    {
        return new ExternalizedMeshObjectRoleAttributesAddChange(
                getSource().getIdentifier(),
                getOldValue(),
                getDeltaValue(),
                getNewValue(),
                getNeighbor().getIdentifier());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRoleAttributesAddChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectRoleAttributesAddChange(
                translation.get( theSource.getIdentifier() ),
                theOldValue,
                theDeltaValue,
                theNewValue,
                translation.get( theNeighbor.getIdentifier() ) );
    }
}
