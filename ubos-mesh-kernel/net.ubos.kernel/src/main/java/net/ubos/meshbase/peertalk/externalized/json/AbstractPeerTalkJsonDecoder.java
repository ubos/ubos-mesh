//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.peertalk.externalized.json;

import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.List;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleClassLoader;
import org.diet4j.core.ModuleException;
import org.diet4j.core.ModuleMeta;
import org.diet4j.core.ModuleRegistry;
import org.diet4j.core.ModuleRequirement;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.util.logging.Log;
import net.ubos.modelbase.externalized.ExternalizedMeshObjectIdentifierNamespace;
import net.ubos.meshbase.transaction.externalized.json.AbstractChangeListJsonDecoder;
import net.ubos.model.primitives.externalized.DecodingException;

/**
 * Knows how to decode a PeerTalk message from JSON.
 */
public abstract class AbstractPeerTalkJsonDecoder
    extends
        AbstractChangeListJsonDecoder
    implements
        PeerTalkJsonTags
{
    private static final Log log = Log.getLogInstance( AbstractPeerTalkJsonDecoder.class );

    /**
     * Parse and process an entire file with header and body.
     *
     * @param r read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the changes contained in the stream
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public List<ExternalizedChange> decodeHeadBody(
            Reader                                   r,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        JsonReader jr = new JsonReader( r );

        return decodeHeadBody( jr, idDeserializer );
    }

    /**
     * Parse and process an entire file with header and body.
     *
     * @param jr read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the changes contained in the stream
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public List<ExternalizedChange> decodeHeadBody(
            JsonReader                               jr,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        List<ExternalizedChange> changes = null;

        jr.beginObject();
        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case HEADER_TAG:
                    decodeHeader( jr, idDeserializer );
                    break;

                case CHANGES_TAG:
                    changes = decodeChanges( jr, idDeserializer );
                    break;

                default:
                    jr.skipValue();
                    log.error( "Unknown name " + name );
                    break;
            }
        }
        jr.endObject();

        return changes;
    }

    /**
     * Parse and process the header of the file.
     *
     * @param jr where to read from
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodeHeader(
            JsonReader                               jr,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        jr.beginObject();
        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case FORMAT_TAG:
                    String format = jr.nextString();
                    if( !FORMAT_TAG_VALUE.equals( format )) {
                        throw new DecodingException.Syntax( "Wrong format: need " + FORMAT_TAG_VALUE + ", have " + format );
                    }
                    break;

                case VERSION_TAG:
                    String version = jr.nextString();
                    if( !VERSION_TAG_VALUE.equals( version )) {
                        throw new DecodingException.Syntax( "Wrong version: need " + VERSION_TAG_VALUE + ", have " + version );
                    }
                    break;

                case MESHOBJECTIDENTIFIERNAMESPACES_TAG:
                    decodeHeaderNamespaces( jr, idDeserializer );
                    break;

                case SUBJECTAREAS_TAG:
                    decodeSubjectAreas( jr );
                    break;

                default:
                    jr.skipValue();
                    throw new DecodingException.Syntax( "Unknown name " + name );
            }
        }
        jr.endObject();
    }

    /**
     * Handle the MeshObjectIdentifier namespaces section in the header
     * @param jr read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodeHeaderNamespaces(
            JsonReader                               jr,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException
    {
        jr.beginObject();

        while( jr.hasNext() ) {
            String shortName = jr.nextName();

            ExternalizedMeshObjectIdentifierNamespace namespace = new ExternalizedMeshObjectIdentifierNamespace();
            namespace.setLocalName( shortName );

            jr.beginObject();

            while( jr.hasNext() ) {
                switch( jr.nextName() ) {
                    case PREFERRED_EXTERNALNAME_TAG:
                        namespace.addPreferredExternalName( jr.nextString() );
                        break;

                    case EXTERNALNAMES_TAG:
                        jr.beginArray();
                        while( jr.hasNext() ) {
                            namespace.addExternalName( jr.nextString() );
                        }
                        jr.endArray();
                        break;
                }
            }

            jr.endObject();

            idDeserializer.addNamespace( namespace );
        }

        jr.endObject();
    }

    /**
     * Handle the SubjectAreas section in the header.
     *
     * @param jr read the data from here
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected void decodeSubjectAreas(
            JsonReader jr )
        throws
            DecodingException,
            IOException
    {
        ModuleRegistry registry = ((ModuleClassLoader)getClass().getClassLoader()).getModuleRegistry();

        jr.beginObject();

        while( jr.hasNext() ) {
            String saName = jr.nextName();
            jr.beginObject();
            // ignore everything in between -- should not be anything for now
            jr.endObject();

            try {
                ModuleRequirement req    = ModuleRequirement.parse( saName );
                ModuleMeta        meta   = registry.determineSingleResolutionCandidate( req );
                Module            module = registry.resolve( meta );

                module.activateRecursively();

            } catch( ParseException ex ) {
                throw new DecodingException.Syntax( ex );

            } catch( ModuleException ex ) {
                throw new DecodingException.Installation( ex );
            }
        }

        jr.endObject();
    }
}
