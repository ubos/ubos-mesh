//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleException;
import org.diet4j.core.ModuleMeta;
import org.diet4j.core.ModuleRegistry;
import org.diet4j.core.ModuleRequirement;
import net.ubos.importer.Importer;
import net.ubos.importer.ImporterException;
import net.ubos.importer.ImporterScore;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.mesh.externalized.xml.AbstractMeshObjectXmlDecoder;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.LoggingMeshBaseErrorListener;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.modelbase.externalized.ExternalizedSubjectAreaDependency;
import org.diet4j.core.ModuleClassLoader;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Parses an XML stream of MeshObjects.
 */
public abstract class AbstractMeshBaseXmlDecoder
    extends
        AbstractMeshObjectXmlDecoder
    implements
        MeshBaseXmlTags
{
}
