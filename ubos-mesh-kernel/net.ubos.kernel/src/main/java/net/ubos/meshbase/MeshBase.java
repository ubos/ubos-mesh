//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.text.ParseException;
import java.util.Collection;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObjectFactory;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.history.MeshBaseStateHistory;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.livedead.LiveDeadObject;
import net.ubos.util.quit.QuitListener;
import net.ubos.meshbase.transaction.TransactionListener;

/**
  * <p>MeshBase represents the place where MeshObjects live. MeshBases collect MeshObjects
  * for management purposes. A MeshBase may be persistent (i.e. retain the contained
  * MeshObjects after reboot), or it may be volatile; however, this distinction is not
  * visible by a client of a MeshBase other than the result of the
  * {@link #isPersistent method}.
  *
  * <p>A MeshBase's home object is a single MeshObject in the MeshBase
  * that is "known" to the outside world. This might often be some sort of
  * "directory object", or an object which is a logical entry point into the
  * information held by the MeshBase. The home object cannot be deleted, but
  * it may be updated (e.g. blessed, properties set, related etc.)
  *
  * <p>All operations potentially modifying information held in a MeshBase must be performed
  *    within a {@link Transaction}.
  */
public interface MeshBase
        extends
            LiveDeadObject,
            HeadMeshBaseView,
            ParserFriendlyExternalizedMeshObjectFactory,
            QuitListener
{
    /**
     * Obtain the head MeshBaseView.
     *
     * @return the head MeshBaseView
     */
    public abstract EditableMeshBaseView getHeadMeshBaseView();

    /**
     * Obtain this MeshBase's MeshBaseIndex, if one has been set.
     *
     * @return the MeshBaseIndex, or null
     */
    public abstract MeshBaseIndex getMeshBaseIndex();

    /**
     * Obtain the history of this MeshBase, aka the transaction log.
     * This may not be implemented by all MeshBases.
     *
     * @return the MeshBaseHistory, or null
     */
    public abstract MeshBaseStateHistory getHistory();

    /**
     * Obtain the historical record for a MeshObject with a given identifier. Some MeshBase
     * implementations may not store a historical record, so this will return null. Some
     * other MeshBases may truncate the record that they store, for example, to save storage
     * space.
     *
     * @param identifier the MeshObject's identifier
     * @return the meshObjectHistory of the MeshObject with this identifier
     */
    public abstract MeshObjectHistory meshObjectHistory(
            MeshObjectIdentifier identifier );

    /**
     * Obtain the historical record for a MeshObject with a given identifier. Some MeshBase
     * implementations may not store a historical record, so this will return null. Some
     * other MeshBases may truncate the record that they store, for example, to save storage
     * space.
     *
     * @param identifier the MeshObject's identifier
     * @return the meshObjectHistory of the MeshObject with this identifier
     * @throws ParseException thrown if an error occurred when parsing the identifier
     */
    public default MeshObjectHistory meshObjectHistory(
            String identifier )
        throws
            ParseException
    {
        return meshObjectHistory( meshObjectIdentifierFromExternalForm( identifier ));
    }

    /**
     * Obtain an iterator over all MeshObjectHistories in this MeshObject. This
     * also returns MeshObjectHistories that, at the current time, do not contain
     * a MeshObject.
     *
     * @return the iterator
     */
    public CursorIterator<MeshObjectHistory> meshObjectHistoriesIterator();

    /**
      * Determine whether this is a persistent MeshBase.
      * A MeshBase is persistent if the information stored in it last longer
      * than the lifetime of the virtual machine running this MeshBase.
      *
      * @return true if this is a persistent MeshBase.
      */
    public abstract boolean isPersistent();

    /**
     * Obtain the AccessManager associated with this MeshBase, if any.
     * The AccessManager controls access to the MeshObjects in this MeshBase.
     *
     * @return the AccessManager, if any
     */
    public abstract AccessManager getAccessManager();

    /**
     * Clear the in-memory cache, if this MeshBase has one. This method only makes any sense
     * if the MeshBase is persistent. Any MeshBase may implement this as a no op.
     * This must only be invoked if no clients hold references to MeshObjects in the cache.
     */
    public abstract void clearMemoryCache();

    /**
     * Obtain the Transaction that is currently active on this MeshBaseView (if any).
     *
     * @return the currently active Transaction, or null if there is none
     */
    public abstract Transaction getCurrentTransaction();

    /**
     * Discard all data currently in this MeshBase, and bulk-load new data into this MeshBase.
     * This is a very dangerous operation!
     *
     * @param data the externalized data to instantiate
     */
    public abstract void discardAllAndBulkLoad(
            Collection<? extends ExternalizedMeshObject> data );

    /**
     * Discard all data currently in this MeshBase, and bulk-load new data into this MeshBase.
     * Also set a new default namespace.
     * This is a very dangerous operation!
     *
     * @param data the externalized data to instantiate
     * @param newDefaultNamespace the new default namespace
     */
    public abstract void discardAllAndBulkLoad(
            Collection<? extends ExternalizedMeshObject> data,
            MeshObjectIdentifierNamespace                newDefaultNamespace );

    /**
     * Act as a factory for ChangeList objects.
     *
     * @return a created, empty ChangeList
     */
    public abstract ChangeList createChangeList();

    /**
      * Add a TransactionListener to this MeshBase, without using a Reference.
      *
      * @param newListener the to-be-added TransactionListener
      * @see #addWeakTransactionListener
      * @see #addSoftTransactionListener
      * @see #removeTransactionListener
      */
    public abstract void addDirectTransactionListener(
            TransactionListener newListener );

    /**
      * Add a TransactionListener to this MeshBase, using a WeakReference.
      *
      * @param newListener the to-be-added TransactionListener
      * @see #addDirectTransactionListener
      * @see #addSoftTransactionListener
      * @see #removeTransactionListener
      */
    public abstract void addWeakTransactionListener(
            TransactionListener newListener );

    /**
      * Add a TransactionListener to this MeshBase, using a SoftReference.
      *
      * @param newListener the to-be-added TransactionListener
      * @see #addWeakTransactionListener
      * @see #addDirectTransactionListener
      * @see #removeTransactionListener
      */
    public abstract void addSoftTransactionListener(
            TransactionListener newListener );

    /**
      * Remove a TransactionListener from this MeshBase.
      *
      * @param oldListener the to-be-removed TransactionListener
      * @see #addWeakTransactionListener
      * @see #addSoftTransactionListener
      * @see #addDirectTransactionListener
      */
    public abstract void removeTransactionListener(
            TransactionListener oldListener );

    /**
     * Subscribe to events indicating errors.
     *
     * @param newListener the to-be-added MeshBaseErrorListener
     * @see #removeErrorListener
     * @see #addWeakErrorListener
     * @see #addSoftErrorListener
     * @see #removeErrorListener
     */
    public abstract void addDirectErrorListener(
            MeshBaseErrorListener newListener );

    /**
     * Subscribe to events indicating errors.
     *
     * @param newListener the to-be-added MeshBaseErrorListener
     * @see #removeErrorListener
     * @see #addSoftErrorListener
     * @see #addDirectErrorListener
     * @see #removeErrorListener
     */
    public abstract void addWeakErrorListener(
            MeshBaseErrorListener newListener );

    /**
     * Subscribe to events indicating errors.
     *
     * @param newListener the to-be-added MeshBaseErrorListener
     * @see #removeErrorListener
     * @see #addWeakErrorListener
     * @see #addDirectErrorListener
     * @see #removeErrorListener
     */
    public abstract void addSoftErrorListener(
            MeshBaseErrorListener newListener );

    /**
     * Unsubscribe from events indicating errors.
     *
     * @param oldListener the to-be-removed MeshBaseErrorListener
     * @see #addMeshBaseErrorListener
     * @see #addWeakMeshBaseErrorListener
     * @see #addSoftMesBaseErrorListener
     * @see #addDirectMeshBaseErrorListener
     */
    public abstract void removeErrorListener(
            MeshBaseErrorListener oldListener );

    /**
     * Tell this MeshBase that we don't need it any more.
     *
     * @param isPermanent if true, this MeshBase will go away permanmently; if false,
     *         it may come alive again some time later
     * @throws IsDeadException thrown if this object is dead already
     */
    public void die(
             boolean isPermanent )
         throws
             IsDeadException;
}
