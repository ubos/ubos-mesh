//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import net.ubos.importer.Importer;
import net.ubos.importer.ImporterException;
import net.ubos.importer.ImporterScore;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.LoggingMeshBaseErrorListener;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.externalized.MeshBaseDecoder;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.modelbase.externalized.ExternalizedSubjectAreaDependency;
import org.diet4j.core.ModuleClassLoader;
import org.diet4j.core.ModuleException;
import org.diet4j.core.ModuleMeta;
import org.diet4j.core.ModuleRegistry;
import org.diet4j.core.ModuleRequirement;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Separate class to avoid coding mistakes.
 */
public final class DefaultMeshBaseXmlImporter
    extends
        AbstractMeshBaseXmlDecoder
    implements
        MeshBaseDecoder,
        Importer
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static DefaultMeshBaseXmlImporter create()
    {
        return new DefaultMeshBaseXmlImporter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return "Default MeshBaseXmlImporter";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getIdentifier()
    {
        return getClass().getSimpleName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_BASE_XML_ENCODING_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String analyze(
            File importCandidate )
        throws
            IOException
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImporterScore importTo(
            File                    toBeImported,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            ImporterException,
            IOException
    {
        if( !toBeImported.isFile() ) {
            throw new ImporterException.NotAFile( this );
        }
        return importTo( new InputStreamReader( new FileInputStream( toBeImported ), StandardCharsets.UTF_8 ), mb );
    }

    /**
     * Import from a Reader.
     *
     * @param toBeRead read from here
     * @param mb the MeshBase to import into
     * @return the score
     * @throws ImporterException thrown if a parsing problem occurred; details are in the cause
     * @throws IOException an I/O problem occurred
     */
    public ImporterScore importTo(
            Reader   toBeRead,
            MeshBase mb )
        throws
            ImporterException,
            IOException
    {
        MContextualMeshObjectIdentifierNamespaceMap nsMap = MContextualMeshObjectIdentifierNamespaceMap.create(
                mb.getPrimaryNamespaceMap() );

        ImporterMeshObjectIdentifierDeserializer idDeserializer = ImporterMeshObjectIdentifierDeserializer.create(
                mb,
                nsMap );

        MeshBaseXmlDecoderHandler myHandler = new MeshBaseXmlDecoderHandler( idDeserializer );
        myHandler.setParent( this );

        LoggingMeshBaseErrorListener listener = new LoggingMeshBaseErrorListener();
        mb.addDirectErrorListener( listener );

        ModuleRegistry registry = ((ModuleClassLoader)getClass().getClassLoader()).getModuleRegistry();
        try {
            theParser.parse( new InputSource( toBeRead ), myHandler );

            Collection<ExternalizedSubjectAreaDependency> foundSaRefs = myHandler.getParsedSubjectAreaDependencies();
            for( ExternalizedSubjectAreaDependency current : foundSaRefs ) {

                ModuleRequirement req    = ModuleRequirement.parse( current.getName().value() );
                ModuleMeta        meta   = registry.determineSingleResolutionCandidate( req );
                org.diet4j.core.Module            module = registry.resolve( meta );

                module.activateRecursively();
            }

            List<ParserFriendlyExternalizedMeshObject> foundMeshObjects = myHandler.getParsedExternalizedMeshObjects();

            mb.discardAllAndBulkLoad( foundMeshObjects, nsMap.getDefaultNamespace() );

        } catch( SAXException ex ) {
            throw new ImporterException.Failure( this, new DecodingException.Syntax( ex ));

        } catch( ParseException ex ) {
            throw new ImporterException.Failure( this, new DecodingException.Syntax( ex ));

        } catch( ModuleException ex ) {
            throw new ImporterException.Failure( this, new DecodingException.Installation( ex ));

        } finally {
            mb.removeErrorListener( listener );
        }
        if( listener.hasError() ) {
            throw new ImporterException.Failure(
                    this,
                    new DecodingException.Syntax(
                            "Errors (" + listener.getErrors().size() + ") during import,"
                            + " starting with " + listener.getErrors().get( 0 )));
        }

        return new ImporterScore( this, ImporterScore.BEST, null );
    }
}
