//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.io.IOException;
import java.io.Reader;
import net.ubos.mesh.externalized.Decoder;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;

/**
 * Knows how to deserialize a transaction performed on a MeshBase.
 */
public interface MeshBaseTransactionDecoder
    extends
        Decoder
{
    /**
     * Parse a serialized Transaction.
     *
     * @param r read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the MeshBaseState
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public ExternalizedTransaction decodeExternalizedTransaction(
            Reader                           r,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException;
}
