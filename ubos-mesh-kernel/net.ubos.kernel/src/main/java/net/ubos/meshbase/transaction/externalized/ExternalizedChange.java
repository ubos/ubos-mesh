//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.modelbase.MeshTypeNotFoundException;

/**
 * Representation of a Change that can be easily serialized and deserialized.
 */
public interface ExternalizedChange
{
    /**
     * Obtain the identifiers of the MeshObjects affected by this Change.
     * This is provided because when MeshObjects are deleted, and recreated
     * (e.g. during rollback of a transaction), new instances with the
     * same identifier are created.
     *
     * @return the identifiers of the MeshObjects affected by this Change
     */
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers();

    /**
     * <p>Apply this Change to a MeshObject in this MeshBase. This method
     *    is intended to make it easy to reproduce Changes that were made in
     *    one MeshBase to MeshObjects in another MeshBase.
     *
     * <p>This method will attempt to create a Transaction if none is present on the
     * current Thread.
     *
     * @param base the MeshBase in which to apply the Change
     * @return the MeshObject to which the Change was applied
     * @throws CannotApplyChangeException thrown if the Change could not be applied, e.g because
     *         the affected MeshObject did not exist in MeshBase base
     * @throws MeshObjectGraphModificationException thrown if at commit time, the graph did not
     *         conform to the model
     * @throws TransactionException thrown if a Transaction didn't exist on this Thread and
     *         could not be created
     */
    public abstract MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException;

    /**
     * Obtain the same ExternalizedChange as Change so it can be easily deserialized.
     * Use the provided MeshObjects as the resolution of the affected MeshobjectIdentifiers
     * (in the same sequence).
     *
     * @param affectedMeshObjects the MeshObjects to use as the resolution of the affected MeshobjectIdentifiers
     * @param modelBase resolve against this ModelBase
     * @throws MeshTypeNotFoundException a MeshType could not be resolved
     * @return this ExternalizedChange as Change
     */
    public Change internalizeWith(
            MeshObject [] affectedMeshObjects )
        throws
            MeshTypeNotFoundException;
}
