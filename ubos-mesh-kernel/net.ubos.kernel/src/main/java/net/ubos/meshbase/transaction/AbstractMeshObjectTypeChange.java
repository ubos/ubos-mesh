//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.event.PropertyChange;

/**
 * This event indicates that a MeshObject has changed its type, i.e. by supporting
 * one more or one less EntityType.
 */
public abstract class AbstractMeshObjectTypeChange
        extends
            PropertyChange<MeshObject,String,EntityType[]>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old set of EntityTypes, prior to the event
     * @param deltaValues the EntityTypes that changed
     * @param newValues the new set of EntityTypes, after the event
     */
    protected AbstractMeshObjectTypeChange(
            MeshObject    source,
            EntityType [] oldValues,
            EntityType [] deltaValues,
            EntityType [] newValues )
    {
        super(  source,
                EVENT_NAME,
                ArrayHelper.checkNoNullArrayMembers( oldValues ),
                ArrayHelper.checkNoNullArrayMembers( deltaValues ),
                ArrayHelper.checkNoNullArrayMembers( newValues ));
    }

     /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

   /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject[] { getSource() };
    }

    /**
     * Obtain the EntityTypes involved in this Change.
     *
     * @return obtain the EntityTypes involved in this Change.
     */
    public EntityType [] getEntityTypes()
    {
        return theDeltaValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        for( EntityType type : theDeltaValue ) {
            sas.add( type.getSubjectArea() );
        }
        // inverse RoleTypes are in the same SubjectArea
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        // We consolidate everything into (at most) one MeshObjectAttributesAddChange event (as early as possible)
        // and one MeshObjectAttributesRemoveChange (as late a possible). We can do this by simply
        // looking at the earliest "old" value and the latest "new" value

        EntityType [] oldValue = null; // this will not remain null, because we also look at ourselves
        int           earliest = -1;   // latest removed = index

        for( int i=0 ; i<=index ; ++i ) { // include ourselves at index
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof AbstractMeshObjectTypeChange )) {
                continue;
            }
            AbstractMeshObjectTypeChange realCurrentChange = (AbstractMeshObjectTypeChange) currentChange;
            oldValue = realCurrentChange.getOldValue();
            earliest = i;
            break;
        }
        ArrayHelper.Difference<EntityType> diff = ArrayHelper.determineDifference( oldValue, theNewValue, true, EntityType.class );

        if( diff.getAdditions().length > 0 ) {
            MeshObjectBlessChange newAddChange = new MeshObjectBlessChange(
                    theSource,
                    oldValue,
                    diff.getAdditions(),
                    ArrayHelper.append( oldValue, diff.getAdditions(), EntityType.class ));
            list.replaceChangeAt( earliest, newAddChange);
        } else if( earliest > -1 && earliest < index ) {
            list.blankChangeAt( earliest );
        }
        if( diff.getRemovals().length > 0 ) {
            MeshObjectUnblessChange newRemoveChange = new MeshObjectUnblessChange(
                    theSource,
                    ArrayHelper.append( theNewValue, diff.getRemovals(), EntityType.class ),
                    diff.getRemovals(),
                    theNewValue );
            list.replaceChangeAt( index, newRemoveChange);
        } else if( earliest > -1 && earliest < index ) {
            list.blankChangeAt( index );
        }

        for( int i=earliest+1 ; i<index ; ++i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( currentChange instanceof AbstractMeshObjectTypeChange ) {
                list.blankChangeAt( i );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( ": " );

        String sep = "";
        for( EntityType t : theDeltaValue ) {
            ret.append( sep );
            ret.append( t.getIdentifier() );
            sep = ", ";
        }
        return ret.toString();
    }

    /**
     * Name of this event.
     */
    public static final String EVENT_NAME = "MeshObjectTypeChange";
}
