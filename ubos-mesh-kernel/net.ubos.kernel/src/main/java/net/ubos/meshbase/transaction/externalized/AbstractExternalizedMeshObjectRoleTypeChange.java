//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifier;

/**
 *
 */
public abstract class AbstractExternalizedMeshObjectRoleTypeChange
    extends
        AbstractExternalizedChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier the identifier of the MeshObject that is the source of the event
     * @param oldValue the identifiers of the source RoleTypes, prior to the end
     * @param deltaValue the identifiers of the source RoleTypes that changed during the event
     * @param newValue identifiers of the source RoleTypes, after the event
     * @param neighborIdentifier the identifier of the neighbor MeshObject
     */
    public AbstractExternalizedMeshObjectRoleTypeChange(
            MeshObjectIdentifier  sourceIdentifier,
            MeshTypeIdentifier [] oldValue,
            MeshTypeIdentifier [] deltaValue,
            MeshTypeIdentifier [] newValue,
            MeshObjectIdentifier  neighborIdentifier )
    {
        super( sourceIdentifier );

        theOldValues   = oldValue;
        theDeltaValues = deltaValue;
        theNewValues   = newValue;
        theNeighborIdentifier = neighborIdentifier;
    }

    public MeshTypeIdentifier [] getOldValue()
    {
        return theOldValues;
    }

    public MeshTypeIdentifier [] getDeltaValue()
    {
        return theDeltaValues;
    }

    public MeshTypeIdentifier [] getNewValue()
    {
        return theNewValues;
    }

    public MeshObjectIdentifier getNeighborIdentifier()
    {
        return theNeighborIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier, theNeighborIdentifier };
    }

    protected final MeshTypeIdentifier [] theOldValues;
    protected final MeshTypeIdentifier [] theDeltaValues;
    protected final MeshTypeIdentifier [] theNewValues;
    protected final MeshObjectIdentifier theNeighborIdentifier;
}
