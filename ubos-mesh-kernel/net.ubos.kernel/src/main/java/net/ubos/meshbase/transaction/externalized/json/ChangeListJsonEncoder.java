//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized.json;

import net.ubos.meshbase.transaction.externalized.ChangeListEncoder;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;

/**
 * Separate class to avoid coding mistakes.
 */
public final class ChangeListJsonEncoder
    extends
        AbstractChangeListJsonEncoder
    implements
        ChangeListEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static ChangeListJsonEncoder create()
    {
        return new ChangeListJsonEncoder( DefaultMMeshTypeIdentifierBothSerializer.SINGLETON );
    }

    /**
     * Constructor, use factory method.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected ChangeListJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        super( typeIdSerializer );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return CHANGE_SET_JSON_ENCODING_ID;
    }
}
