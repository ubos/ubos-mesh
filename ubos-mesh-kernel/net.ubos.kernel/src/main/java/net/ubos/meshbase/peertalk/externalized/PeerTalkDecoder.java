//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.peertalk.externalized;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import net.ubos.importer.ImporterException;
import net.ubos.mesh.externalized.Decoder;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.model.primitives.externalized.DecodingException;

/**
 *
 */
public interface PeerTalkDecoder
    extends
        Decoder
{
    /**
     * Parse and process an entire PeerTalk file with header and body.
     *
     * @param r read the data from here
     * @param idDeserializer first learns from the header how to deserialize, then does it
     * @return the changes contained in the stream
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public List<ExternalizedChange> decodeHeadBody(
            Reader                                   r,
            ImporterMeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException;
}
