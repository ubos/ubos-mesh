//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.util.List;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;

/**
 * A temporary buffer for a to-be-deserialized MeshBaseState.
 */
public class ParserFriendlyExternalizedTransaction
    implements
        ExternalizedTransaction
{
    /**
     * Set the timeUpdated.
     *
     * @param newValue the new value
     */
    public void setTimeUpdated(
            long newValue )
    {
        theTimeUpdated = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * Set the Changes that let to this state.
     *
     * @param newValue the changes
     */
    public void setChanges(
            List<ExternalizedChange> newValue )
    {
        theChanges = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExternalizedChange> getChanges()
    {
        return theChanges;
    }

    /**
     * The timeUpdated.
     */
    protected long theTimeUpdated;

    /**
     * The Changes.
     */
    protected List<ExternalizedChange> theChanges;
}
