//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.MeshBaseView;

/**
  * This TransactionException is thrown if a (potentially) modifying operation
  * is invoked by a Thread that does not belong to the currently active Transaction.
  */
public class IllegalTransactionThreadException
        extends
            TransactionException
{
    /**
     * Constructor.
     *
     * @param mbv the MeshBaseView that was affected
     * @param tx the current transaction
     */
    public IllegalTransactionThreadException(
            MeshBaseView mbv,
            Transaction  tx )
    {
        super( mbv, tx );
    }
}
