//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.peertalk.externalized;

import java.io.IOException;
import java.io.Writer;
import net.ubos.mesh.externalized.Encoder;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.externalized.EncodingException;

/**
 *
 */
public interface PeerTalkEncoder
    extends
        Encoder
{
    /**
     * Write a ChangeList to a Writer as a full PeerTalk file.
     *
     * @param changes the ChangeList
     * @param emitOldValues if true, always emit old values even if they are redundant
     * @param emitNewValues if true, always emit new values even if they are redundant
     * @param nsMap use this namespace map
     * @param w the Writer to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    public void writePeerTalk(
            ChangeList                                 changes,
            boolean                                    emitOldValues,
            boolean                                    emitNewValues,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            Writer                                     w )
        throws
            EncodingException,
            IOException;
}
