//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.exception.AbstractLocalizedRuntimeException;

/**
 * Thrown if a problem occurred during the execution of a TransactionAction.
 */
public abstract class TransactionActionException
    extends
        AbstractLocalizedRuntimeException
{
    /**
     * Constructor, for subclasses defined in this file only.
     */
    private TransactionActionException()
    {
        super();
    }

    /**
     * Constructor, for subclasses defined in this file only.
     *
     * @param message the message, if any
     * @param cause the cause, if any
     */
    private TransactionActionException(
            String    message,
            Throwable cause )
    {
        super( message, cause );
    }

    /**
     * Indicates that the Transaction needs to be rolled back, and that
     * a new attempt to execute the TransactionAction should not be
     * attempted.
     */
    public static class Rollback
            extends
                TransactionActionException
    {
        /**
         * Default constructor.
         */
        public Rollback()
        {
            // nothing
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object[] getLocalizationParameters()
        {
            return new String[] { getMessage() };
        }
    }

    /**
     * Indicates that the Transaction could not be committed, such as because
     * a model constraint was violated.
     */
    public static class CannotCommit
            extends
                TransactionActionException
    {
        /**
         * Constructor.
         *
         * @param cause the cause
         */
        public CannotCommit(
                Throwable cause )
        {
            super( null, cause );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object[] getLocalizationParameters()
        {
            return new String[] { getMessage(), getCause().getLocalizedMessage() };
        }
    }

    /**
     * Indicates that a different problem occurred that could not be remedied from
     * within the TransactionAction.
     */
    public static class Error
            extends
                TransactionActionException
    {
        /**
         * Constructor.
         *
         * @param cause the cause
         */
        public Error(
                Throwable cause )
        {
            super( null, cause );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object[] getLocalizationParameters()
        {
            return new String[] { getMessage(), getCause().getLocalizedMessage() };
        }
    }

    /**
     * Indicates that a Transaction could not be created, in spite of trying.
     */
    public static class FailedToCreateTransaction
            extends
                TransactionActionException
    {
        /**
         * Constructor.
         *
         * @param cause the cause
         */
        public FailedToCreateTransaction(
                TransactionException cause )
        {
            super( null, cause );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object[] getLocalizationParameters()
        {
            return new String[] { getMessage(), getCause().getLocalizedMessage() };
        }
    }
}
