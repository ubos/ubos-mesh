//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.index;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.OrderedMeshObjectSetWithValue;
import net.ubos.mesh.set.m.ImmutableMMeshObjectSetFactory;
import net.ubos.mesh.set.m.OrderedMMeshObjectSetWithValue;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseWellKnown;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.meshbase.transaction.TransactionListener;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.model.traverse.ByRoleAttributeTraverseSpecification;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.ArrayHelper;
import net.ubos.util.OrderedMSetWithValue;
import net.ubos.util.OrderedSetWithValue;
import net.ubos.util.ResourceHelper;
import net.ubos.util.logging.Log;

/**
 * Provides a facade to the index information in a MeshBase (assuming a given
 * MeshBase does indeed contain such index information).
 * 
 * This is just a view with no local storage; you can create as many as you like.
 */
public class MeshBaseIndex
    implements
        MeshBaseWellKnown,
        TransactionListener
{
    private static final Log log = Log.getLogInstance( MeshBaseIndex.class );

    /**
     * Factory method.
     * 
     * @param mb the MeshBase for which this is the index
     * @return the index
     */
    public static MeshBaseIndex create(
            MeshBase mb )
    {
        return new MeshBaseIndex( mb );
    }
    
    /**
     * Constructor.
     *
     * @param mb the MeshBase to which this is the index
     */
    protected MeshBaseIndex(
            MeshBase mb )
    {
        theMeshBase = mb;

        theMeshBase.addWeakTransactionListener( this );
    }

    /**
     * Helper to find the MeshObject that represents a certain EntityType.
     * 
     * @param type the EntityType
     * @return the MeshObject, or null if not found
     */
    public MeshObject findTypeObject(
            EntityType type )
    {
        MeshObjectIdentifier objId = identifierForType( type );
        MeshObject           ret   = theMeshBase.findMeshObjectByIdentifier( objId );
        
        return ret;
    }
    
    /**
     * Helper to find, or create the MeshObject that represents a certain EntityType.
     * 
     * @param type the EntityType
     * @return the MeshObject
     * @throws TransactionException thrown if the MeshObject needs to be created, and not invoked from within a Transaction
     */
    public MeshObject obtainTypeObject(
            EntityType type )
        throws
            TransactionException
    {
        MeshObjectIdentifier objId = identifierForType( type );
        MeshObject           ret   = theMeshBase.findMeshObjectByIdentifier( objId );
        
        if( ret == null ) {
            ret = theMeshBase.createMeshObject( objId );
        }
        return ret;
    }
    
    /**
     * Determine the EntityType represented by this type MeshObject.
     * 
     * @param obj the MeshObject
     * @return the EntityType, or null if not a type MeshObject
     */
    public EntityType entityTypeOfTypeObject(
            MeshObject obj )
    {
        String localId = obj.getIdentifier().getLocalId();
        if( localId.startsWith( MeshBaseWellKnown.MODEL_DIR_WITH_SEP )) {
            String typeId = localId.substring( MeshBaseWellKnown.MODEL_DIR_WITH_SEP.length() );

            try {
                EntityType ret = ModelBase.SINGLETON.findEntityType( typeId );
                return ret;

            } catch( ParseException | MeshTypeWithIdentifierNotFoundException ex ) {
                return null;
            }

        } else {
            return null;
        }
    }

    /**
     * Helper to find the MeshObject that represents a certain word.
     * 
     * @param word the word
     * @return the MeshObject, or null if not found
     */
    public MeshObject findWordObject(
            String word )
    {
        MeshObjectIdentifier objId = identifierForWord( word );
        MeshObject           ret   = theMeshBase.findMeshObjectByIdentifier( objId );
        
        return ret;
    }
    
    /**
     * Helper to find, or create the MeshObject that represents a certain word.
     * 
     * @param word the word
     * @return the MeshObject
     * @throws TransactionException thrown if the MeshObject needs to be created, and not invoked from within a Transaction
     */
    public MeshObject obtainWordObject(
            String word )
        throws
            TransactionException
    {
        MeshObjectIdentifier objId = identifierForWord( word );
        MeshObject           ret   = theMeshBase.findMeshObjectByIdentifier( objId );
        
        if( ret == null ) {
            ret = theMeshBase.createMeshObject( objId );
        }
        return ret;
    }
    
    /**
     * Determine the word represented by this word MeshObject.
     * 
     * @param obj the MeshObject
     * @return the word, or null if not a word MeshObject
     */
    public String wordOfWordObject(
            MeshObject obj )
    {
        String localId = obj.getIdentifier().getLocalId();
        if( localId.startsWith( MeshBaseWellKnown.TERMS_INDEX_DIR_WITH_SEP )) {
            String ret = localId.substring( MeshBaseWellKnown.TERMS_INDEX_DIR_WITH_SEP.length() );
            return ret;
        } else {
            return null;
        }
    }

    /**
     * Construct the MeshObjectIdentifier of a type MeshObject.
     * 
     * @param type the EntityType
     * @return the MeshObjectIdentifier
     */
    protected MeshObjectIdentifier identifierForType(
            EntityType type )
    {
        try {
            StringBuilder buf = new StringBuilder();
            
            if( type == null ) {
                buf.append( MeshBaseWellKnown.UNBLESSED_INDEX );
            } else {
                buf.append( MeshBaseWellKnown.MODEL_DIR_WITH_SEP );
                buf.append( MESHTYPEIDENTIFIER_SERIALIZER.toExternalForm( type.getIdentifier() ));
            }

            MeshObjectIdentifier ret = theMeshBase.meshObjectIdentifierFromExternalForm( buf.toString() );
            return ret;

        } catch( ParseException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Construct the MeshObjectIdentifier of a word MeshObject.
     * 
     * @param word the word
     * @return the MeshObjectIdentifier
     */
    protected MeshObjectIdentifier identifierForWord(
            String word )
    {
        try {
            StringBuilder buf = new StringBuilder();
            buf.append( MeshBaseWellKnown.TERMS_INDEX_DIR_WITH_SEP );
            buf.append( word );

            MeshObjectIdentifier ret = theMeshBase.meshObjectIdentifierFromExternalForm( buf.toString() );
            return ret;

        } catch( ParseException ex ) {
            log.error( ex );
            return null;
        }
    }
    
    /**
     * Obtain the set of MeshObjects that match the provided search String.
     *
     * @param searchString the search string
     * @return the found results, in order of quality
     * @throws ParseException thrown if the search expression cannot be parsed
     */
    public OrderedMeshObjectSetWithValue<Long> searchFullTextWithCount(
            String searchString )
        throws
            ParseException
    {
        String [] terms = parseSearchExpression( searchString );

        OrderedMeshObjectSetWithValue<Long> found = searchForWordWithCount( terms[0] );
        for( int i=1 ; i<terms.length ; ++i ) {
            OrderedMeshObjectSetWithValue<Long> found2   = searchForWordWithCount( terms[i] );
            OrderedMeshObjectSetWithValue<Long> newFound = OrderedMMeshObjectSetWithValue.createLargeToSmall( ImmutableMMeshObjectSetFactory.SINGLETON );

            for( MeshObject current : found ) {
                Long value  = found.getValueFor( current );
                Long value2 = found2.getValueFor( current );
                
                if( value2 != null ) {
                    newFound.put( current, value + value2 ); // addition?
                }
            }
            found = newFound;
        }

        return found;
    }

    /**
     * Obtain the set of MeshObjects that has a String or text Blob Property
     * that contains an occurrence of this word. You cannot search for
     * null or for stop words. The value of the map is the number of
     * occurrences of this word for this MeshObject.
     *
     * @param word the word to look for
     * @return the MeshObject with non-zero occurrences, and the respective number of occurrences, or null
     */
    public OrderedMeshObjectSetWithValue<Long> searchForWordWithCount(
            String word )
    {
        MeshObject wordObj = findWordObject( word );
        if( wordObj == null ) {
            return null;
        }
        MeshObjectSet objectsWithWord = wordObj.traverse(
                ByRoleAttributeTraverseSpecification.createSource( MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE ));

        OrderedMeshObjectSetWithValue<Long> ret = OrderedMMeshObjectSetWithValue.createLargeToSmall( ImmutableMMeshObjectSetFactory.SINGLETON );
        for( MeshObject current : objectsWithWord ) {
            ret.put( current, (Long) wordObj.getRoleAttributeValue( current, MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE ));
        }
        return ret;
    }

    /**
     * Obtain the set of MeshObjects that has a String or text Blob Property
     * that contains an occurrence of this word. You cannot search for
     * null or for stop words.
     *
     * @param word the word to look for
     * @return the MeshObjects with non-zero occurrences
     */
    public MeshObjectSet searchForWord(
            String word )
    {
        MeshObject wordObj = findWordObject( word );
        if( wordObj == null ) {
            return MeshObjectSet.DEFAULT_EMPTY_SET;
        }
        MeshObjectSet objectsWithWord = wordObj.traverse(
                ByRoleAttributeTraverseSpecification.createSource( MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE ));
        
        return objectsWithWord;
    }

    /**
     * Obtain the set of MeshObjects that has been blessed with (at least)
     * this exact EntityType. This will not return MeshObjects that have
     * been blessed with a subtype of this EntityType.
     *
     * @param type the EntityType to look for
     * @return the set
     */
    public MeshObjectSet searchForBlessedWithEntityType(
            EntityType type )
    {
        MeshObject wordObj = findTypeObject( type );
        if( wordObj == null ) {
            return MeshObjectSet.DEFAULT_EMPTY_SET;
        }
        MeshObjectSet objectsWithWord = wordObj.traverse(
                ByRoleAttributeTraverseSpecification.createSource( MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE ));
        
        return objectsWithWord;
    }

    /**
     * Obtain all the words with their number of occurrences.
     * 
     * @return the words with occurrences
     */
    public OrderedSetWithValue<String,Long> wordsWithCount()
    {
        return wordsWithCount( Integer.MAX_VALUE );
    }

    /**
     * Obtain all the words with their number of occurrences.
     * Limit the number of returned results to the top-max. This may return more
     * elements than max, the the number of occurrences of some word is the same;
     * all words with the same number of occurrences will be returned.
     * 
     * @param max the maximum number of elements to return
     * @return the words with occurrences
     */
    public OrderedSetWithValue<String,Long> wordsWithCount(
            int max )
    {
        try {
            OrderedSetWithValue<String,Long> ret = OrderedMSetWithValue.createLargeToSmall( max );

            for( MeshObject current : theMeshBase.iterator( MeshBaseWellKnown.TERMS_INDEX_DIR_WITH_SEP )) {
                Long sum  = current.aggregateRoleAttributes(
                        MeshBaseWellKnown.INDEX_OCCURRENCES_ROLE_ATTRIBUTE,
                        (Long) 0L,
                        ( Long state, Serializable newValue ) -> (Long) ( state + (Long) newValue ));

                String word = wordOfWordObject( current );
                if( word != null ) {
                    ret.put( word, sum );
                }  else {
                    log.error( "Word not found: ", current );
                }
            }
            
            return ret;

        } catch( ParseException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Parse a search expression into words.
     *
     * @param s the search expression
     * @return the words
     * @throws ParseException thrown if the expression could not be parsed
     */
    protected String [] parseSearchExpression(
            String s )
        throws
            ParseException
    {
        String [] terms = s.trim().toLowerCase().split( "\\s+" );
        if( terms.length == 0 ) {
            throw new ParseException( "No search terms given", 0 );
        }

        HashSet<String> almost = new HashSet<>();

        for( String candidate : terms ) {
            Matcher m = MeshBaseIndex.WORD_TO_INDEX_PATTERN.matcher( candidate );
            if( !m.matches() ) {
                throw new ParseException( "Cannot search for this word: " + candidate, 0 );
            }
            String word = m.group(1);
            word = word.replaceAll( "-", "" );

            if( MeshBaseIndex.STOP_WORDS.contains( word )) {
                throw new ParseException( "Cannot search for stop word: " + word, 0 );
            }

            almost.add( word );
        }
        return ArrayHelper.copyIntoNewArray( almost, String.class );
    }

    /**
     * The MeshBase that we index.
     */
    protected final MeshBase theMeshBase;

    /**
     * Convert the MeshTypeIdentifier of the EntityTypes to the String used in
     * creation of the MeshObjectIdentifier for the type MeshObject.
     */
    protected static final MeshTypeIdentifierBothSerializer MESHTYPEIDENTIFIER_SERIALIZER
            = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

    /**
     * Regex matching words in text to be indexed. At least 2 chars. Lowercased'd already. May have certain leading
     * and trailing punctuation.
     */
    public static final Pattern WORD_TO_INDEX_PATTERN = Pattern.compile( "[-\\(\\[]?([a-z0-9][-a-z0-9]*[a-z0-9])[-\\.:;,\\)\\]]?" );

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( MeshBaseIndex.class );

    /**
     * The set of stop words.
     */
    public static final HashSet<String> STOP_WORDS = new HashSet<>();
    static {
        String [] stopWords = theResourceHelper.getResourceStringOrDefault( "StopWords", "a the" ).trim().split( "\\s+" );
        STOP_WORDS.addAll( Arrays.asList( stopWords ));
    }
}
