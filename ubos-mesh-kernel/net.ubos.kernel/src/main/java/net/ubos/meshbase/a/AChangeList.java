//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.a;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.meshbase.AbstractMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;

/**
 * Implementation of ChangeList for the "A" MeshObject implementation.
 */
public class AChangeList
    extends
        ChangeList
{
    /**
     * Factory method.
     *
     * @param mbv the MeshBaseView that resulted as a result of these changes.
     * @return the created ChangeList
     */
    public static AChangeList create(
            MeshBaseView mbv )
    {
        return new AChangeList( mbv );
    }

    /**
     * Create a new instance of ChangeList with the same content as this one,
     * except that all affected MeshObjects are being cloned with the
     * provided MeshBaseView instead.
     *
     * @param oldChangeList the old changes
     * @param newView the MeshBaseView to use instead
     * @return the new ChangeList
     */
    public static AChangeList createCopyWithMeshBaseView(
            ChangeList           oldChangeList,
            AbstractMeshBaseView newView )
    {
        AChangeList ret = new AChangeList( newView );

        HashMap<MeshObjectIdentifier,MeshObject> newObjects = new HashMap<>();

        for( Change oldChange : oldChangeList ) {
            cloneAffectedMeshObjectsFor( oldChange, newView, newObjects );
            Change newChange = oldChange.withAlternateAffectedMeshObjects( newObjects );
            ret.addChange( newChange );
        }
        return ret;
    }

    /**
     * Helper method to clone all affected MeshObjects with a different MeshBaseView.
     *
     * @param change the Change that holds the affected MeshObjects
     * @param newView the new MeshBaseView
     * @param clones deposit result here, if it isn't there yet
     */
    protected static void cloneAffectedMeshObjectsFor(
            Change                               change,
            AbstractMeshBaseView                 newView,
            Map<MeshObjectIdentifier,MeshObject> clones )
    {
        MeshObject [] oldAffecteds = change.getAffectedMeshObjects();

        for( MeshObject oldAffected : oldAffecteds ) {
            MeshObject newAffected = clones.get( oldAffected.getIdentifier() );
            if( newAffected == null ) {
                newAffected = ((AMeshObject)oldAffected).createCopyForMeshBaseView( newView );
                clones.put( newAffected.getIdentifier(), newAffected );
            }
        }
    }

    /**
     * Private constructor, use factory method instead.
     *
     * @param mbv the MeshBaseView that resulted as a result of these changes.
     */
    protected AChangeList(
            MeshBaseView mbv )
    {
        theMeshBaseView = mbv;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBaseView getMeshBaseView()
    {
        return theMeshBaseView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ChangeList clone()
    {
        AChangeList ret = new AChangeList( theMeshBaseView );
        ret.append( this );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        int count = 0;
        for( Change c : theChanges ) {
            buf.append( String.format( "%2d: %s\n", count, c.toString() ));
            ++count;
            if( count > 20 ) {
                buf.append( String.format( "... (%d changes total)\n", theChanges.size() ));
                break;
            }
        }
        return buf.toString();
    }

    /**
     * The MeshBaseView that resulted as a result of these Changes.
     */
    protected final MeshBaseView theMeshBaseView;
}
