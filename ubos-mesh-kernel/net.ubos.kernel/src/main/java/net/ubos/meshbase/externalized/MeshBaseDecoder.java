//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.io.IOException;
import java.io.Reader;
import net.ubos.importer.ImporterException;
import net.ubos.importer.ImporterScore;
import net.ubos.mesh.externalized.Decoder;
import net.ubos.meshbase.MeshBase;

/**
 *
 */
public interface MeshBaseDecoder
    extends
        Decoder
{
    /**
     * Import data into this MeshBase.
     *
     * @param r read the data from here
     * @param mb the MeshBase to import into
     * @return the score
     * @throws ImporterException thrown if a parsing problem occurred; details are in the cause
     * @throws IOException an I/O problem occurred
     */
    public ImporterScore importTo(
            Reader   r,
            MeshBase mb )
        throws
            ImporterException,
            IOException;
}
