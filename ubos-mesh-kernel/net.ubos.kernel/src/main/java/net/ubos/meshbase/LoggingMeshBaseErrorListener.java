//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple implementation of MeshBaseErrorListener that only logs what happened.
 */
public class LoggingMeshBaseErrorListener
    implements
        MeshBaseErrorListener
{
    /**
     * {@inheritDoc}
     */
    @Override
    public void unresolveableEntityType(
            MeshBaseError.UnresolvableEntityType error )
    {
        logError( error );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unresolveableRoleType(
            MeshBaseError.UnresolvableRoleType error )
    {
        logError( error );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unresolveablePropertyType(
            MeshBaseError.UnresolvablePropertyType error )
    {
        logError( error );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incompatibleDataType(
            MeshBaseError.IncompatibleDataType error )
    {
        logError( error );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propertyNotOptional(
            MeshBaseError.PropertyNotOptional error )
    {
        logError( error );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void otherError(
            MeshBaseError.OtherError error )
    {
        logError( error );
    }

    /**
     * Obtain the collected errors.
     *
     * @return the errors
     */
    public List<MeshBaseError> getErrors()
    {
        return theLog;
    }

    /**
     * Determine whether at at least one error has been logged.
     *
     * @return true or false
     */
    public boolean hasError()
    {
        return !theLog.isEmpty();
    }

    /**
     * Overridable helper to log one error.
     *
     * @param error the error to log
     */
    protected void logError(
            MeshBaseError error )
    {
        theLog.add( error );
    }

    /**
     * The collected errors.
     */
    protected final ArrayList<MeshBaseError> theLog = new ArrayList<>();
}
