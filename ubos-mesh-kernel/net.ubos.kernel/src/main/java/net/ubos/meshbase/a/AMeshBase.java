//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.a;

import java.util.function.Predicate;
import java.util.function.Supplier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.meshbase.AbstractEditableHistoricMeshBaseView;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.AbstractMeshBase;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.NotWithinTransactionBoundariesException;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionAction1;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.meshbase.transaction.TransactionTimeoutException;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.util.ResourceHelper;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.logging.Log;

/**
 * The subclass of MeshBase suitable for the AMeshObject implementation.
 */
public abstract class AMeshBase
        extends
            AbstractMeshBase
{
    private static final Log log = Log.getLogInstance( AMeshBase.class ); // our own, private logger

    /**
     * Constructor for subclasses only. This does not initialize content.
     *
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBase
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     * @param setFactory the factory for MeshObjectSets appropriate for this MeshBase
     * @param life the MeshBaseLifecycleManager to use
     * @param accessMgr the AccessManager that controls access to this MeshBase
     * @param isHistoryEditable if true, the history of this MeshBase may be edited
     * @param normalizeChangeList if true, normalize the ChangeList of a Transaction during commit
     */
    protected AMeshBase(
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer,
            MeshObjectSetFactory             setFactory,
            AMeshBaseLifecycleManager        life,
            AccessManager                    accessMgr,
            boolean                          isHistoryEditable,
            boolean                          normalizeChangeList )
    {
        super( identifierFactory, idDeserializer, setFactory, life, accessMgr );

        theIsHistoryEditable   = isHistoryEditable;
        theNormalizeChangeList = normalizeChangeList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract AMeshBaseStateHistory getHistory();

    /**
     * {@inheritDoc}
     */
    @Override
    public final ATransaction getCurrentTransaction()
    {
        return theCurrentTransaction;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final AHeadTransaction getCurrentHeadTransaction()
    {
        if( theCurrentTransaction == null || !( theCurrentTransaction instanceof AHeadTransaction )) {
            return null;
        } else {
            return (AHeadTransaction) theCurrentTransaction;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T execute(
            TransactionAction1<T> act )
        throws
            TransactionActionException
    {
        return execute(
                act,
                () -> new AHeadTransaction( getHeadMeshBaseView() ),
                ( Transaction reuseCandidate ) -> {
                    if( !( reuseCandidate instanceof HeadTransaction )) {
                        return false;
                    }
                    if( !reuseCandidate.owns( Thread.currentThread() )) {
                        return false;
                    }
                    return true;
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T executeAt(
            long                  when,
            TransactionAction1<T> act )
        throws
            TransactionActionException
    {
        if( when >= System.currentTimeMillis() ) {
            throw new IllegalArgumentException( "Cannot execute HistoryTransaction in the future" );
        }
        return execute(
                act,
                () -> {
                    MeshBaseState                mbs = getHistory().obtainAt( when );
                    AbstractEditableHistoricMeshBaseView mbv = (AbstractEditableHistoricMeshBaseView) mbs.getMeshBaseView();
                    return (Transaction) new AHistoryTransaction( mbv );
                },
                ( Transaction reuseCandidate ) -> {
                    if( !( reuseCandidate instanceof HistoryTransaction )) {
                        return false;
                    }
                    if( !reuseCandidate.owns( Thread.currentThread() )) {
                        return false;
                    }

                    HistoryTransaction realCandidate = (HistoryTransaction) reuseCandidate;
                    return realCandidate.getWhen() == when;
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected <T> T execute(
            TransactionAction1<T>            act,
            Supplier<? extends Transaction>  txFactory,
            Predicate<Transaction>           canReuseTransaction )
    {
        // This is invoked in several scenarios:
        // * HeadTransaction vs HistoryTransaction
        // * this may already be inside a Transaction: piggyback on that one, we silently ignore sub-transactions
        //   but of course they need to be compatible: eg can't do a HeadTransaction instead of a HistoryTransaction

        T                ret                 = null;
        ATransaction     existingTransaction = null;
        ATransaction     createdTx           = null; // if a Transaction is created in this method, it will be here
        ATransaction     validTx             = null; // if a Transaction is created, or a valid Transaction is found, it will be here
        Throwable        thrown              = null; // any Throwable thrown by the TransactionAction
        RuntimeException toThrow             = null; // any Throwable to be thrown as result of this invocation

        // we try to get a Transaction for nAsapTries with some time delay each, then we give up: some other Transaction won't go away.
        for( int counter = 0 ; counter < nAsapTries ; ++counter ) {
            synchronized( this ) {
                existingTransaction = theCurrentTransaction;
                if( theCurrentTransaction == null ) {
                    createdTx             = (ATransaction) txFactory.get();
                    validTx               = createdTx;
                    theCurrentTransaction = createdTx;

                } else if( canReuseTransaction.test( theCurrentTransaction )) {
                    validTx = theCurrentTransaction;
                } // else do nothing
            }
            if( validTx != null ) {
                break; // found one
            }

            // wait a bit and try again
            try {
                Thread.sleep( asapRetryInterval );
            } catch( InterruptedException ex ) {
                // just continue on
            }
        }
        if( validTx == null ) {
            // timeed out, cannot create transaction
            throw new TransactionTimeoutException( this, existingTransaction );
        }

        // have valid transaction -- try to commit
        if( createdTx != null ) {
            fireTransactionStartedEvent( createdTx );
        }

        try {
            ret = act.execute( theCurrentTransaction );

            if( createdTx != null ) {
                // only if this is not a sub-transaction
                ATransaction oldTransaction = theCurrentTransaction;

                synchronized( this ) {
                    act.preCommitTransaction( oldTransaction );

                    try {
                        oldTransaction.commitTransaction( theNormalizeChangeList );

                    } finally {
                        theCurrentTransaction = null; // commitTransaction may throw
                    }
                    act.postCommitTransaction( oldTransaction );
                }
                oldTransaction.transactionCommitted();
                fireTransactionCommittedEvent( oldTransaction );
            }

            return ret;

        } catch( TransactionActionException.Rollback ex ) {
            thrown = ex;

        } catch( NotWithinTransactionBoundariesException ex ) {
            thrown  = ex;
            toThrow = ex;

        } catch( TransactionException ex ) {
            thrown  = ex;
            toThrow = new TransactionActionException.FailedToCreateTransaction( ex );

        } catch( TransactionActionException.Error ex ) {
            thrown  = ex;
            toThrow = ex;

        } catch( Throwable ex ) {
            thrown  = ex; // User code did this, e.g. an invalid graph modificiation
            toThrow = new TransactionActionException.Error( ex );

        } finally {
            if( thrown != null && createdTx != null && createdTx.getStatus() != Transaction.Status.TRANSACTION_ROLLEDBACK ) {
                try {
                    act.preRollbackTransaction( createdTx, thrown );
                } catch( Throwable t ) {
                    log.error( t );
                }
                try {
                    theCurrentTransaction = createdTx;
                    theCurrentTransaction.rollbackTransaction( thrown );
                } catch( Throwable t ) {
                    log.error( t );
                } finally {
                    theCurrentTransaction = null;
                }
                try {
                    act.postRollbackTransaction( createdTx, thrown );
                } catch( Throwable t ) {
                    log.error( t );
                }
                transactionRolledbackHook( createdTx );
                fireTransactionRolledbackEvent( createdTx );
            }
        }
        if( toThrow != null ) {
            throw toThrow;
        }
        return ret;
    }

    /**
     * This method may be overridden by subclasses to perform suitable actions when a
     * Transaction was rolled back.
     *
     * @param tx Transaction the Transaction that was rolled back
     */
    protected void transactionRolledbackHook(
            Transaction tx )
    {
        // noop
    }

    /**
     * Recreate the AMeshObject instance from this externalized representation, but do not add
     * to the MeshBaseView. This is an internal implementation method.
     *
     * @param externalized external form of the to-be-recreated MeshObject
     * @param mbv use this MeshBaseView for the restored MeshObject
     * @return the recreated MeshObject
     */
    public AMeshObject recreateMeshObject(
            ExternalizedMeshObject       externalized,
            AbstractEditableMeshBaseView mbv )
    {
        return ((AMeshBaseLifecycleManager)theMeshBaseLifecycleManager).recreateMeshObject( externalized, mbv );
    }

    /**
     * Determine whether the history of this MeshBase is editable.
     *
     * @return true or false
     */
    public boolean isHistoryEditable()
    {
        return theIsHistoryEditable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AChangeList createChangeList()
    {
        return AChangeList.create( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AMeshObject[] createArray(
            int len )
    {
        return new AMeshObject[ len ];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void die(
             boolean isPermanent )
         throws
             IsDeadException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "die", isPermanent );
        }

        // let current transaction finish for no more than 5 seconds
        int count = 25;
        while( count> 0 && theCurrentTransaction != null ) {
            try {
                Thread.sleep( asapRetryInterval );
                --count;

            } catch( InterruptedException ex ) {
                // ignore
            }
        }

        // let's die even if the transaction is not done yet
        super.die( isPermanent );
    }

    /**
      * The current Transaction, if any.
      */
    protected ATransaction theCurrentTransaction;

    /**
     * Our way to find our resources.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( AMeshBase.class );

    /**
      * The number of tries before "asap" Transactions time out.
      */
    protected final int nAsapTries = theResourceHelper.getResourceIntegerOrDefault(
            "NTriesForAsapTransactions",
            10 );

    /**
      * The interval in milliseconds before an "asap" Transaction is retried.
      */
    protected final int asapRetryInterval = theResourceHelper.getResourceIntegerOrDefault(
            "AsapTransactionRetryInterval",
            200 );

    /**
     * If true, the history of this MeshBase is editable.
     */
    protected final boolean theIsHistoryEditable;

    /**
     * If true, normalize a Transaction's ChangeList during commit.
     */
    protected final boolean theNormalizeChangeList;
}
