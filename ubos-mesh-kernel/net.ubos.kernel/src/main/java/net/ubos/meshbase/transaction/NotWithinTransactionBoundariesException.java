//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.MeshBaseView;

/**
  * This TransactionException is thrown if a (potentially) modifying operation
  * is invoked outside of Transaction boundaries.
  */
public class NotWithinTransactionBoundariesException
        extends
            TransactionException
{
    /**
     * Constructor.
     *
     * @param trans the MeshBase that was affected
     */
    public NotWithinTransactionBoundariesException(
            MeshBaseView trans )
    {
        super( trans, null );
    }
}
