//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.json;

import net.ubos.mesh.externalized.json.MeshObjectJsonTags;
import net.ubos.meshbase.transaction.externalized.json.ChangeListJsonTags;

/**
 *
 */
public interface MeshBaseTransactionJsonTags
    extends
        MeshObjectJsonTags,
        ChangeListJsonTags
{
    /** Encoding ID */
    public static final String MESH_BASE_TRANSACTION_JSON_ENCODING_ID
            = MeshObjectJsonTags.class.getPackage().getName();

    /** Tag indicating the Transaction. */
    public static final String TRANSACTION_TAG = "TX";

    /** Tag indicating the time of a change. */
    public static final String CHANGE_TIME_TAG = "T";
}
