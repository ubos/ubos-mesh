//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.xml;

import net.ubos.meshbase.externalized.MeshBaseEncoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class MeshBaseXmlEncoder
    extends
        AbstractMeshBaseXmlEncoder
    implements
        MeshBaseEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static MeshBaseXmlEncoder create()
    {
        return new MeshBaseXmlEncoder();
    }

    /**
     * Constructor, use factory method.
     */
    protected MeshBaseXmlEncoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return MESH_BASE_XML_ENCODING_ID;
    }
}
