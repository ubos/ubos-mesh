//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.event.PropertyChange;

/**
 * <p>This indicates a change in a MeshObject's participation in a role of
 * a certain RoleType. In other words, a relationship between the MeshObject and
 * another MeshObject was blessed or unblessed.
 *
 * <p>This extends PropertyChange so we can keep the well-known JavaBeans
 event generation model that programmers are used to.
 */
public abstract class AbstractMeshObjectRoleTypeChange
        extends
            PropertyChange<MeshObject,String,RoleType[]>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the MeshObject that is the source of the event
     * @param oldValues the old values of the RoleType, prior to the event
     * @param deltaValues the RoleTypes that changed
     * @param newValues the new values of the RoleType, after the event
     * @param neighbor the MeshObject at the end of the affected relationship
     */
    protected AbstractMeshObjectRoleTypeChange(
            MeshObject  source,
            RoleType [] oldValues,
            RoleType [] deltaValues,
            RoleType [] newValues,
            MeshObject  neighbor )
    {
        super(  source,
                EVENT_NAME,
                ArrayHelper.checkNoNullArrayMembers( oldValues ),
                ArrayHelper.checkNoNullArrayMembers( deltaValues ),
                ArrayHelper.checkNoNullArrayMembers( newValues ));

        theNeighbor = neighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject [] { getSource(), theNeighbor };
    }

    /**
     * Obtain the affected RoleTypes.
     *
     * @return the RoleTypes
     */
    public RoleType [] getAffectedRoleTypes()
    {
        return getDeltaValue();
    }

    /**
     * Obtain the neighbor MeshObject.
     *
     * @return the neighbor MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        for( RoleType rt : theDeltaValue ) {
            sas.add( rt.getSubjectArea() );
        }
        // inverse RoleTypes are in the same SubjectArea
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        // We consolidate everything into (at most) one MeshObjectAttributesAddChange event (as early as possible)
        // and one MeshObjectAttributesRemoveChange (as late a possible). We can do this by simply
        // looking at the earliest "old" value and the latest "new" value

        RoleType [] oldValue = null; // this will not remain null, because we also look at ourselves
        int         earliest = -1;   // latest removed = index

        for( int i=0 ; i<=index ; ++i ) { // include ourselves at index
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof AbstractMeshObjectRoleTypeChange )) {
                continue;
            }
            AbstractMeshObjectRoleTypeChange realCurrentChange = (AbstractMeshObjectRoleTypeChange) currentChange;
            if( !theNeighbor.equals( realCurrentChange.theNeighbor )) {
                continue;
            }
            oldValue = realCurrentChange.getOldValue();
            earliest = i;
            break;
        }
        ArrayHelper.Difference<RoleType> diff = ArrayHelper.determineDifference( oldValue, theNewValue, true, RoleType.class );

        if( diff.getAdditions().length > 0 ) {
            MeshObjectRoleTypeAddChange newAddChange = new MeshObjectRoleTypeAddChange(
                    theSource,
                    oldValue,
                    diff.getAdditions(),
                    ArrayHelper.append( oldValue, diff.getAdditions(), RoleType.class ),
                    theNeighbor );
            list.replaceChangeAt( earliest, newAddChange);
        } else if( earliest > -1 && earliest < index ) {
            list.blankChangeAt( earliest );
        }
        if( diff.getRemovals().length > 0 ) {
            MeshObjectRoleTypeRemoveChange newRemoveChange = new MeshObjectRoleTypeRemoveChange(
                    theSource,
                    ArrayHelper.append( theNewValue, diff.getRemovals(), RoleType.class ),
                    diff.getRemovals(),
                    theNewValue,
                    theNeighbor );
            list.replaceChangeAt( index, newRemoveChange);
        } else if( earliest > -1 && earliest < index ) {
            list.blankChangeAt( index );
        }

        for( int i=earliest+1 ; i<index ; ++i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( currentChange instanceof AbstractMeshObjectRoleTypeChange ) {
                list.blankChangeAt( i );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( " -> " );
        ret.append( theNeighbor == null ? "???" : theNeighbor.getIdentifier() );
        ret.append( ": " );

        String sep = "";
        for( RoleType t : theDeltaValue ) {
            ret.append( sep );
            ret.append( t.getIdentifier() );
            sep = ", ";
        }
        return ret.toString();
    }

    /**
     * The neighbor MeshObject.
     */
    protected final MeshObject theNeighbor;

    /**
     * Name of this event.
     */
    public static final String EVENT_NAME = "MeshObjectRoleTypesChange";
}
