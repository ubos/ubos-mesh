//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.m.history;

import java.util.HashMap;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.AbstractHistoricMeshBaseViewRepository;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.smartmap.GentleSmartMap;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * In-memory repository for MeshObjects in a historical MeshBaseView. Looks up / creates
 * MeshObject instances if needed to fill in when a MeshObject didn't change in
 * this MeshBaseView.
 */
public class MHistoricMeshBaseViewRepository
    extends
        AbstractHistoricMeshBaseViewRepository
{
    /**
     * Constructor.
     */
    public MHistoricMeshBaseViewRepository()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            MeshObjectIdentifier key )
    {
        if( theChangedMeshObjects.containsKey( key )) {
            return true;
        }
        if( theClonedMeshObjects.containsKey( key )) {
            return true;
        }

        // could be containing
        MeshObjectHistory history = theMeshBaseView.getMeshBase().meshObjectHistory( key );
        if( history == null ) {
            return false;
        }

        MeshObject found = history.atOrBefore( theMeshBaseView.getMeshBaseState().getTimeUpdated() );
        if( found == null ) {
            return false;
        }
        return !found.getIsDead();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject get(
            MeshObjectIdentifier key )
    {
        MeshObject ret = theChangedMeshObjects.get( key );
        if( ret == null ) {
            ret = theClonedMeshObjects.get( key );
            if( ret == null ) {
                MeshObjectHistory history = theMeshBaseView.getMeshBase().meshObjectHistory( key );
                if( history != null ) {
                    AMeshObject found = (AMeshObject) history.atOrBefore( theMeshBaseView.getMeshBaseState().getTimeUpdated() );
                    if( found != null ) {
                        ret = found.createCopyForMeshBaseView( theMeshBaseView );
                    }
                }
                if( ret != null ) {
                    theClonedMeshObjects.put( key, ret );
                }
            }
        }

        if( ret == null || ret.getIsDead() ) {
            return null;
        } else {
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject put(
            MeshObjectIdentifier key,
            MeshObject           value )
    {
        return theChangedMeshObjects.put( key, value ); // this is only invoked for changed MeshObjects, never for clone MeshObjects
    }

    /**
     * Obtain an iterator over the primary MeshObjects.
     *
     * @return the iterator.
     */
    public CursorIterator<MeshObject> changedMeshObjectsIterator()
    {
        return theChangedMeshObjects.valueIterator();
    }

    /**
     * Obtain an iterator over the closed MeshObjects.
     *
     * @return the iterator.
     */
    public CursorIterator<MeshObject> cloneMeshObjectsIterator()
    {
        return theClonedMeshObjects.valueIterator();
    }

    /**
     * Purge all MeshObjects that are mere clones (without changes) Of
     * previously changed MeshObjects. This saves memory, but also enables removes now potentially
     * inconsistent MeshObjects when a HistoryTransaction is committed.
     */
    public void purgeClonedMeshObjectsCache()
    {
        theClonedMeshObjects.clear();
    }

    /**
     * The MeshObjects that changed in this MeshBaseView, which means they cannot be recreated.
     */
    protected final SmartMap<MeshObjectIdentifier,MeshObject> theChangedMeshObjects = MSmartMap.create();

    /**
     * The MeshObjects that are part of this MeshBaseView, but are mere clones of MeshObjects in an earlier
     * MeshObjectView. They can be recreated by cloning from the preceding MeshBaseView
     */
    protected final SmartMap<MeshObjectIdentifier,MeshObject> theClonedMeshObjects = new GentleSmartMap.Weak<>( new HashMap<>() );
}
