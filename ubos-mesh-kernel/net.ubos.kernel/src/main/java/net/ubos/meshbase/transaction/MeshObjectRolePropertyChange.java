//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectRolePropertyChange;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.event.PropertyChange;

/**
  * <p>This event indicates that one of the properties of a Role between a MeshObject and another
  * has changed its value.
  */
public class MeshObjectRolePropertyChange
        extends
            PropertyChange<MeshObject,PropertyType,PropertyValue>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the object that is the source of the event
     * @param property an object representing the property of the event
     * @param oldValue the old value of the property, prior to the event
     * @param newValue the new value of the property, after the event
     * @param neighbor the identifier of the other side of this relationship
     */
    public MeshObjectRolePropertyChange(
            MeshObject    source,
            PropertyType  property,
            PropertyValue oldValue,
            PropertyValue newValue,
            MeshObject    neighbor )
    {
        super(  source,
                property,
                oldValue,
                newValue,
                newValue );

        theNeighbor = neighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject [] { getSource() };
    }

    /**
     * Obtain the neighbor MeshObject with which this MeshObject is in a relationship
     * whose RoleProperty changed.
     *
     * @return identifier of the neighbor MeshObject
     */
    public MeshObject getNeighbor()
    {
        return theNeighbor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject    otherObject      = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject    otherNeighbor    = base.findMeshObjectByIdentifier( getNeighbor().getIdentifier() );
        PropertyType  affectedProperty = getProperty();
        PropertyValue newValue         = getNewValue();

        otherObject.setRolePropertyValue(
                otherNeighbor,
                affectedProperty,
                newValue );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRolePropertyChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        MeshObject updatedNeighbor   = mbv.findMeshObjectByIdentifier( theNeighbor.getIdentifier() );

        PropertyType pt = getProperty();

        return new MeshObjectRolePropertyChange(
                updatedMeshObject,
                pt,
                updatedMeshObject.getPropertyValue( pt ),
                theNewValue,
                updatedNeighbor );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRolePropertyChange inverse()
    {
        return new MeshObjectRolePropertyChange(
                getSource(),
                getProperty(),
                getNewValue(),
                getOldValue(),
                getNeighbor() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectRolePropertyChange )) {
            return false;
        }
        MeshObjectRolePropertyChange realCandidate = (MeshObjectRolePropertyChange) candidate;

        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }

        if( theProperty != realCandidate.theProperty ) {
            return false;
        }

        if( !theNeighbor.equals( realCandidate.theNeighbor )) {
            return false;
        }

        Object one = getOldValue();
        Object two = realCandidate.getNewValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        one = getNewValue();
        two = realCandidate.getOldValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectRolePropertyChange )) {
            return false;
        }
        MeshObjectRolePropertyChange realOther = (MeshObjectRolePropertyChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( theProperty != realOther.theProperty ) {
            return false;
        }
        if( !theNeighbor.equals( realOther.theNeighbor )) {
            return false;
        }
        if( !ArrayHelper.equals( theNewValue, realOther.theNewValue )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(
                theSource.getIdentifier(),
                theProperty.getIdentifier(),
                theNeighbor.getIdentifier(),
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectRolePropertyChange asExternalized()
    {
        return new ExternalizedMeshObjectRolePropertyChange(
                getSource().getIdentifier(),
                getProperty().getIdentifier(),
                getOldValue(),
                getNewValue(),
                theNeighbor.getIdentifier());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        sas.add( theProperty.getSubjectArea() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectRolePropertyChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectRolePropertyChange(
                translation.get( theSource.getIdentifier() ),
                theProperty,
                theOldValue,
                theNewValue,
                translation.get( theNeighbor.getIdentifier() ) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        for( int i=index-1 ; i>=0 ; --i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof MeshObjectRolePropertyChange )) {
                continue;
            }
            MeshObjectRolePropertyChange realCurrentChange = (MeshObjectRolePropertyChange) currentChange;
            if(    !theProperty.equals( realCurrentChange.getProperty())
                || !theNeighbor.equals( realCurrentChange.theNeighbor ))
            {
                continue;
            }

            if( Objects.equals( realCurrentChange.theOldValue, theNewValue )) {
                // can eliminate both
                list.blankChangeAt( i );
                list.blankChangeAt( index );

            } else {
                // can simplify -- we blank ourselves and replace the other
                Change newCurrentChange = new MeshObjectRolePropertyChange( theSource, theProperty, realCurrentChange.theOldValue, theNewValue, theNeighbor );

                list.replaceChangeAt( i, newCurrentChange);
                list.blankChangeAt( index );
            }
            break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( " -> " );
        ret.append( theNeighbor.getIdentifier() );
        ret.append( ": " );
        ret.append( theProperty.getIdentifier() );
        ret.append( " = " );
        ret.append( theOldValue );
        ret.append( " >> " );
        ret.append( theNewValue );
        return ret.toString();
    }

    /**
     * The MeshObject on the other side of this relationship.
     */
    protected final MeshObject theNeighbor;
}
