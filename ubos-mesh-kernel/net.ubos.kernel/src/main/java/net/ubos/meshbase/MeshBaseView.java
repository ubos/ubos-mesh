//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.util.cursoriterator.CursorIterable;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;

/**
 * The view into a MeshBase as of a particular time.
 */
public interface MeshBaseView
        extends
            MeshObjectIdentifierFactory,
            MeshObjectIdentifierDeserializer,
            CursorIterable<MeshObject>
{
    /**
     * Obtain the MeshBase which is viewed by this MeshBaseView.
     *
     * @return the MeshBase that is viewed by this MeshBaseView.
     */
    @Override
    public abstract MeshBase getMeshBase();

    /**
     * Determine whether MeshObjects in this MeshBaseView can be modified. Generally, only
     * the MeshObjects in the HEAD MeshBaseView can be modified.
     *
     * @return true if the view is frozen
     */
    public abstract boolean isFrozen();

    /**
     * Obtain the MeshBaseView's home object. The home object is the only well-known object in
     * a MeshBaseView. It is guaranteed to exist and cannot be deleted.
     *
     * @return the MeshObject that is this MeshBaseView's home object
     */
    public abstract MeshObject getHomeObject();

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a trailing String. None of the provided Strings may contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append the String to append
     * @return the created MeshObjectIdentifier
     * @throws ParseException thrown if any of the identifier segments were invalid
     */
    public abstract MeshObjectIdentifier createMeshObjectIdentifierBelow(
            String baseIdentifier,
            String append )
        throws
            ParseException;

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending one or more trailing Strings. None of the provided Strings may contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append the Strings to append
     * @return the created MeshObjectIdentifier
     * @throws ParseException thrown if any of the identifier segments were invalid
     */
    public abstract MeshObjectIdentifier createMeshObjectIdentifierBelow(
            String    baseIdentifier,
            String [] append )
        throws
            ParseException;

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a trailing String and potentially another sequence of trailing Strings.
     * None of the provided Strings may contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append the String to append
     * @param appendMore more Strings to append
     * @return the created MeshObjectIdentifier
     * @throws ParseException thrown if any of the identifier segments were invalid
     */
    public MeshObjectIdentifier createMeshObjectIdentifierBelow(
            String     baseIdentifier,
            String     append,
            String ... appendMore )
        throws
            ParseException;

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a random trailing String. None of the provided Strings may contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @return the created Identifier
     * @throws ParseException thrown if any of the identifier segments were invalid
     */
    public abstract MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            String baseIdentifier )
        throws
            ParseException;

    /**
     * Create a MeshObjectIdentifier that's "below" another MeshObjectIdentifier in the same name space, by
     * appending a sequence of Strings and then a random trailing String.
     * None of the provided Strings may contain a slash.
     *
     * @param baseIdentifier the MeshObjectIdentifier "above" the to-be-created one
     * @param append Strings to append
     * @return the created Identifier
     * @throws ParseException thrown if any of the identifier segments were invalid
     */
    public abstract MeshObjectIdentifier createRandomMeshObjectIdentifierBelow(
            String     baseIdentifier,
            String ... append )
        throws
            ParseException;

    /**
     * <p>Determine whether a MeshObject exists in this MeshBaseView with this identifier.
     * This operation may be cheaper than actually finding the MeshObject.
     * Note: this operation is not filtered by the AccessManager
     *
     * @param identifier the identifier of the MeshObject that shall be found
     * @return true or false
     */
    public abstract boolean existsMeshObjectByIdentifier(
            MeshObjectIdentifier identifier );

    /**
     * <p>Find a MeshObject in this MeshBaseView by its identifier.
     *
     * @param identifier the identifier of the MeshObject that shall be found
     * @return the found MeshObject, or null if not found
     * @see #findMeshObjectByIdentifierOrThrow
     */
    public abstract MeshObject findMeshObjectByIdentifier(
            MeshObjectIdentifier identifier );

    /**
     * <p>Find a MeshObject in this MeshBaseView by its identifier.
     *
     * @param identifier the identifier of the MeshObject that shall be found
     * @return the found MeshObject, or null if not found
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @see #findMeshObjectByIdentifierOrThrow
     */
    public default MeshObject findMeshObjectByIdentifier(
            String identifier )
        throws
            ParseException
    {
        return findMeshObjectByIdentifier( meshObjectIdentifierFromExternalForm( identifier ) );
    }

    /**
     * <p>Find a set of MeshObjects in this MeshBaseView by their identifiers at the same time.
     * <p>If one or more of the MeshObjects could not be found, returns <code>null</code> at
     *    the respective index in the returned array.
     *
     * @param identifiers the identifiers of the MeshObjects that shall be found
     * @return the found MeshObjects, which may contain null values for MeshObjects that were not found
     */
    public abstract MeshObject [] findMeshObjectsByIdentifier(
            MeshObjectIdentifier [] identifiers );

    /**
     * <p>Find a set of MeshObjects in this MeshBaseView by their identifiers at the same time.
     * <p>If one or more of the MeshObjects could not be found, returns <code>null</code> at
     *    the respective index in the returned array.
     *
     * @param identifiers the identifiers of the MeshObjects that shall be found
     * @return the found MeshObjects, which may contain null values for MeshObjects that were not found
     * @throws ParseException thrown if an error occurred when parsing the identifier
     */
    public default MeshObject [] findMeshObjectsByIdentifier(
            String [] identifiers )
        throws
            ParseException
    {
        MeshObjectIdentifier [] realIds = new MeshObjectIdentifier[ identifiers.length ];
        for( int i=0 ; i<identifiers.length ; ++i ) {
            realIds[i] = meshObjectIdentifierFromExternalForm( identifiers[i] );
        }
        return findMeshObjectsByIdentifier( realIds );
    }

    /**
     * <p>Find a MeshObject in this MeshBaseView by its identifier.
     *
     * @param identifier the identifier of the MeshObject that shall be found
     * @return the found MeshObject
     * @throws MeshObjectsNotFoundException if the MeshObject was not found
     */
    public abstract MeshObject findMeshObjectByIdentifierOrThrow(
            MeshObjectIdentifier identifier )
        throws
            MeshObjectsNotFoundException;

    /**
     * <p>Find a MeshObject in this MeshBaseView by its identifier.
     *
     * @param identifier the identifier of the MeshObject that shall be found
     * @return the found MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws MeshObjectsNotFoundException if the MeshObject was not found
     */
    public default MeshObject findMeshObjectByIdentifierOrThrow(
            String identifier )
        throws
            ParseException,
            MeshObjectsNotFoundException
    {
        return findMeshObjectByIdentifierOrThrow( meshObjectIdentifierFromExternalForm( identifier ));
    }

    /**
     * <p>Find a set of MeshObjects in this MeshBaseView by their identifiers at the same time.
     *
     * <p>If one or more of the MeshObjects could not be found, throws
     *    {@link MeshObjectsNotFoundException MeshObjectsNotFoundException}.</p>
     *
     * @param identifiers the identifiers of the MeshObjects that shall be found
     * @return the found MeshObjects
     * @throws MeshObjectsNotFoundException if one or more of the MeshObjects were not found
     */
    public abstract MeshObject [] findMeshObjectsByIdentifierOrThrow(
            MeshObjectIdentifier [] identifiers )
        throws
            MeshObjectsNotFoundException;

    /**
     * <p>Find a set of MeshObjects in this MeshBaseView by their identifiers at the same time.
     *
     * <p>If one or more of the MeshObjects could not be found, throws
     *    {@link MeshObjectsNotFoundException MeshObjectsNotFoundException}.
     *
     * @param identifiers the identifiers of the MeshObjects that shall be found
     * @return the found MeshObjects
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws MeshObjectsNotFoundException if one or more of the MeshObjects were not found
     */
    public default MeshObject [] findMeshObjectsByIdentifierOrThrow(
            String [] identifiers )
        throws
            ParseException,
            MeshObjectsNotFoundException
    {
        MeshObjectIdentifier [] realIds = new MeshObjectIdentifier[ identifiers.length ];
        for( int i=0 ; i<identifiers.length ; ++i ) {
            realIds[i] = meshObjectIdentifierFromExternalForm( identifiers[i] );
        }
        return findMeshObjectsByIdentifierOrThrow( realIds );
    }

    /**
     * Returns a CursorIterator over the MeshObjects in this MeshBaseView whose
     * identifier starts with this identifier.
     *
     * @param startsWith the String the identifier starts with
     * @return a CursorIterator.
     */
    public default CursorIterator<MeshObject> iterator(
            MeshObjectIdentifier startsWith )
    {
        FilteringCursorIterator<MeshObject> ret = FilteringCursorIterator.create(
                iterator(),
                (MeshObject o) ->    o.getIdentifier().getNamespace().equals( startsWith.getNamespace() )
                                  && o.getIdentifier().getLocalId().startsWith( startsWith.getLocalId() ));

        return ret;
    }

    /**
     * Returns a CursorIterator over the MeshObjects in this MeshBaseView whose
     * identifier starts with this identifier.
     *
     * @param startsWith the String the identifier starts with
     * @return a CursorIterator.
     * @throws ParseException thrown if an error occurred when parsing the identifier
     */
    public default CursorIterator<MeshObject> iterator(
            String startsWith )
        throws
            ParseException
    {
        return iterator( meshObjectIdentifierFromExternalForm( startsWith ));
    }

    /**
     * Returns a CursorIterator over the MeshObjects in this MeshBaseView whose
     * identifier starts with this identifier.
     *
     * @param startsWith the String the identifier starts with
     * @return a CursorIterator.
     */
    public default CursorIterator<MeshObject> getIterator(
            MeshObjectIdentifier startsWith )
    {
        return iterator( startsWith );
    }

    /**
     * Returns a CursorIterator over the MeshObjects in this MeshBaseView whose
     * identifier starts with this identifier.
     *
     * @param startsWith the String the identifier starts with
     * @return a CursorIterator.
     * @throws ParseException thrown if an error occurred when parsing the identifier
     */
    public default CursorIterator<MeshObject> getIterator(
            String startsWith )
        throws
            ParseException
    {
        return iterator( startsWith );
    }

    /**
     * Determine the number of MeshObjects in this MeshBaseView.
     *
     * @return the number of MeshObjects in this MeshBaseView
     * @see #getSize()
     */
    public default int size()
    {
        CursorIterator<MeshObject> iter = iterator();
        int ret = iter.moveToAfterLast();
        return ret;
    }

    /**
     * Determine the number of MeshObjects in this MeshBaseView. This redundant method
     * is provided to make life easier for JavaBeans-aware software.
     *
     * @return the number of MeshObjects in this MeshBaseView
     * @see #size()
     */
    public default int getSize()
    {
        return size();
    }

    /**
     * Determine the set of MeshObjects that are neighbors of both of the passed-in MeshObjects.
     * This is a convenience method that can have substantial performance benefits, depending on
     * the underlying implementation of MeshObject.
     *
     * @param one the first MeshObject
     * @param two the second MeshObject
     * @return the set of MeshObjects that are neighbors of both MeshObject one and MeshObject two
     */
    public abstract MeshObjectSet findCommonNeighbors(
            MeshObject one,
            MeshObject two );

    /**
     * Determine the set of MeshObjects that are neighbors of all of the passed-in MeshObjects.
     * This is a convenience method that can have substantial performance benefits, depending on
     * the underlying implementation of MeshObject.
     *
     * @param all the MeshObjects whose common neighbors we seek.
     * @return the set of MeshObjects that are neighbors of all MeshObjects
     */
    public abstract MeshObjectSet findCommonNeighbors(
            MeshObject [] all );

    /**
     * Determine the set of MeshObjects that are neighbors of both of the passed-in MeshObjects
     * while playing particular RoleTypes.
     * This is a convenience method that can have substantial performance benefits, depending on
     * the underlying implementation of MeshObject.
     *
     * @param one the first MeshObject
     * @param oneType the RoleType to be played by the first MeshObject with the to-be-found MeshObjects
     * @param two the second MeshObject
     * @param twoType the RoleType to be played by the second MeshObject with the to-be-found MeshObjects
     * @return the set of MeshObjects that are neighbors of both MeshObject one and MeshObject two
     */
    public abstract MeshObjectSet findCommonNeighbors(
            MeshObject one,
            RoleType   oneType,
            MeshObject two,
            RoleType   twoType );

    /**
     * Determine the set of MeshObjects that are neighbors of all of the passed-in MeshObjects
     * while playing particular RoleTypes.
     * This is a convenience method that can have substantial performance benefits, depending on
     * the underlying implementation of MeshObject.
     *
     * @param all the MeshObjects whose common neighbors we seek.
     * @param allTypes the RoleTypes to be played by the MeshObject at the same position in the array
     *        with the to-be-found MeshObjects
     * @return the set of MeshObjects that are neighbors of all MeshObjects
     */
    public abstract MeshObjectSet findCommonNeighbors(
            MeshObject [] all,
            RoleType []   allTypes );
}
