//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectBlessChange;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.util.ArrayHelper;

/**
 * Event that indicates a MeshObject was blessed with one or more EntityTypes.
 */
public class MeshObjectBlessChange
        extends
            AbstractMeshObjectTypeChange
{
    /**
     * Constructor.
     *
     * @param source the MeshObject whose type changed
     * @param oldValues the old set of EntityTypes, prior to the event
     * @param deltaValues the EntityTypes that were added
     * @param newValues the new set of EntityTypes, after the event
     */
    public MeshObjectBlessChange(
            MeshObject    source,
            EntityType [] oldValues,
            EntityType [] deltaValues,
            EntityType [] newValues )
    {
        super(  source,
                oldValues,
                deltaValues,
                newValues );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject otherObject = base.findMeshObjectByIdentifier( getSource().getIdentifier() );

        EntityType [] types = getDeltaValue();
        otherObject.bless( types );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectBlessChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject    updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        EntityType [] oldTypes          = updatedMeshObject.getEntityTypes();
        EntityType [] deltaTypes        = theDeltaValue;
        EntityType [] newTypes          = ArrayHelper.append( oldTypes, deltaTypes, EntityType.class );

        return new MeshObjectBlessChange(
                updatedMeshObject,
                oldTypes,
                deltaTypes,
                newTypes );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectUnblessChange inverse()
    {
        return new MeshObjectUnblessChange(
                getSource(),
                getNewValue(),
                getDeltaValue(),
                getOldValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectUnblessChange )) {
            return false;
        }
        MeshObjectUnblessChange realCandidate = (MeshObjectUnblessChange) candidate;
        if( !getSource().equals( realCandidate.getSource() )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realCandidate.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectBlessChange )) {
            return false;
        }

        MeshObjectBlessChange realOther = (MeshObjectBlessChange) other;
        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realOther.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = theSource.getIdentifier().hashCode();

        for( EntityType et : getDeltaValue() ) {
            ret ^= et.getIdentifier().hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectBlessChange asExternalized()
    {
        return new ExternalizedMeshObjectBlessChange(
                getSource().getIdentifier(),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getOldValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getDeltaValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getNewValue() ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectBlessChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectBlessChange(
                translation.get( theSource.getIdentifier() ),
                theOldValue,
                theDeltaValue,
                theNewValue );
    }
}
