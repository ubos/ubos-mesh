//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectUnblessChange;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.util.ArrayHelper;

/**
 * Event that indicates a MeshObject was unblessed from one or more EntityTypes.
 */
public class MeshObjectUnblessChange
        extends
            AbstractMeshObjectTypeChange
{
    /**
     * Constructor.
     *
     * @param source the MeshObject whose type changed
     * @param oldValues the old set of EntityTypes, prior to the event
     * @param deltaValues the EntityTypes that were added
     * @param newValues the new set of EntityTypes, after the event
     */
    public MeshObjectUnblessChange(
            MeshObject    source,
            EntityType [] oldValues,
            EntityType [] deltaValues,
            EntityType [] newValues )
    {
        super(  source,
                oldValues,
                deltaValues,
                newValues );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject otherObject = base.findMeshObjectByIdentifier( getSource().getIdentifier() );

        EntityType [] types = getDeltaValue();
        otherObject.unbless( types );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectUnblessChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject    updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );
        EntityType [] oldTypes          = updatedMeshObject.getEntityTypes();
        EntityType [] deltaTypes        = theDeltaValue;
        EntityType [] newTypes          = ArrayHelper.remove( oldTypes, deltaTypes, true, EntityType.class );

        return new MeshObjectUnblessChange(
                updatedMeshObject,
                oldTypes,
                deltaTypes,
                newTypes );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectBlessChange inverse()
    {
        return new MeshObjectBlessChange(
                getSource(),
                getNewValue(),
                getDeltaValue(),
                getOldValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectBlessChange )) {
            return false;
        }

        MeshObjectBlessChange realCandidate = (MeshObjectBlessChange) candidate;
        if( !getSource().equals( realCandidate.getSource())) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realCandidate.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectUnblessChange )) {
            return false;
        }
        MeshObjectUnblessChange realOther = (MeshObjectUnblessChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( !ArrayHelper.hasSameContentOutOfOrder( getDeltaValue(), realOther.getDeltaValue(), true )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = theSource.getIdentifier().hashCode();

        for( EntityType et : getDeltaValue() ) {
            ret ^= et.getIdentifier().hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectUnblessChange asExternalized()
    {
        return new ExternalizedMeshObjectUnblessChange(
                getSource().getIdentifier(),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getOldValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getDeltaValue() ),
                MeshTypeUtils.meshTypeIdentifiersOrNull( getNewValue() ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectUnblessChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectUnblessChange(
                translation.get( theSource.getIdentifier() ),
                theOldValue,
                theDeltaValue,
                theNewValue );
    }
}
