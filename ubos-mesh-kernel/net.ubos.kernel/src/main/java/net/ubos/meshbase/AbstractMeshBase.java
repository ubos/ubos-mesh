//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import net.ubos.mesh.IsAbstractException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.externalized.ParserFriendlyExternalizedMeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.NotWithinTransactionBoundariesException;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Log;
import net.ubos.util.quit.QuitManager;
import net.ubos.meshbase.transaction.TransactionAction0;
import net.ubos.meshbase.transaction.TransactionAction1;
import net.ubos.meshbase.transaction.TransactionListener;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.listenerset.FlexibleListenerSet;
import net.ubos.util.livedead.LiveDeadListener;
import net.ubos.util.logging.Dumper;

/**
 * This abstract, partial implementation of MeshBase provides
 * functionality that is common to all (or at least most) implementations of MeshBase.
 */
public abstract class AbstractMeshBase
        extends
            AbstractMeshBaseView
        implements
            EditableHistoryMeshBase,
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( AbstractMeshBase.class ); // our own, private logger

    /**
     * Constructor for subclasses only. This does not initialize content.
     *
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBase
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     * @param setFactory the factory for MeshObjectSets appropriate for this MeshBase
     * @param life the MeshBaseLifecycleManager to use
     * @param accessMgr the AccessManager that controls access to this MeshBase
     */
    protected AbstractMeshBase(
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer,
            MeshObjectSetFactory             setFactory,
            AbstractMeshBaseLifecycleManager life,
            AccessManager                    accessMgr )
    {
        super( identifierFactory, idDeserializer );

        if( life == null ) {
            throw new NullPointerException();
        }
        // accessMgr may be null

        this.theSetFactory               = setFactory;
        this.theMeshBaseLifecycleManager = life;
        this.theAccessManager            = accessMgr;

        this.theMeshBaseLifecycleManager.setMeshBase( this );

        QuitManager.addWeakQuitListener( this );
    }

    /**
     * Obtain the MeshObjectSetFactory suitable for this MeshBase.
     *
     * @return the MeshObjectSetFactory
     */
    public MeshObjectSetFactory getMeshObjectSetFactory()
    {
        return theSetFactory;
    }

    /**
     * Obtain the lifecycle manager.
     *
     * @return the AbstractMeshBaseLifecycleManager
     */
    public AbstractMeshBaseLifecycleManager getLifecycleManager()
    {
        return theMeshBaseLifecycleManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract AbstractHeadMeshBaseView getHeadMeshBaseView();

    /**
     * Enables a HeadTransaction to tell us that it is complete.
     *
     * @param tx the completed Transaction
     */
    public abstract void headTransactionCommitted(
            HeadTransaction tx );

    /**
     * Enables a HistoryTransaction to tell us that it is complete.
     *
     * @param tx the completed Transaction
     */
    public abstract void historyTransactionCommitted(
            HistoryTransaction tx );

    // FIXME not sure this should be here
    public abstract MeshObjectHistory obtainMeshObjectHistory(
            MeshObjectIdentifier id );

    /**
     * Find the correct MeshBaseView given this Thread and the current Transaction.
     *
     * @return the MeshBaseView
     */
    protected abstract AbstractEditableMeshBaseView findMeshBaseViewForCurrentTransaction();

// from MeshBase

    /**
     * {@inheritDoc}
     */
    @Override
    public Transaction checkTransaction()
        throws
            TransactionException
    {
        Transaction tx = getCurrentTransaction();
        if( tx == null ) {
            throw new NotWithinTransactionBoundariesException( this );
        }
        AbstractEditableMeshBaseView mbv = findMeshBaseViewForCurrentTransaction();

        tx.checkTransaction( mbv );
        return tx;
    }

//   from LiveDeadObject

//   from MeshBaseLifecycleManager

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeshBase getMeshBase()
    {
        return this;
    }

// CursorIterable<MeshObject>

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> iterator()
    {
        MeshBaseView mbv = findMeshBaseViewForCurrentTransaction();
        return mbv.iterator();
    }

// MeshBaseView

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getHomeObject()
    {
        MeshBaseView mbv = findMeshBaseViewForCurrentTransaction();
        return mbv.getHomeObject();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject()
        throws
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObject( findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject(
            EntityType type )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObject( type, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject(
            MeshObjectIdentifier identifier )
        throws
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObject( identifier, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObject(
            MeshObjectIdentifier identifier,
            EntityType           type )
        throws
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObject( identifier, type, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base )
        throws
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObjectBelow( base, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base,
            EntityType type )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObjectBelow( base, type, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base,
            String     trailingId )
        throws
            ParseException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObjectBelow( base, trailingId, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject base,
            String     trailingId,
            EntityType type )
        throws
            ParseException,
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return theMeshBaseLifecycleManager.createMeshObjectBelow( base, trailingId, type, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMeshObject(
            MeshObject theObject )
        throws
            TransactionException,
            NotPermittedException
    {
        theMeshBaseLifecycleManager.deleteMeshObject( theObject, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMeshObjects(
            MeshObject [] theObjects )
        throws
            TransactionException,
            NotPermittedException
    {
        theMeshBaseLifecycleManager.deleteMeshObjects( theObjects, findMeshBaseViewForCurrentTransaction() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMeshObjects(
            MeshObjectSet theSet )
        throws
            TransactionException,
            NotPermittedException
    {
        theMeshBaseLifecycleManager.deleteMeshObjects( theSet, findMeshBaseViewForCurrentTransaction() );
    }

//   from MeshBaseView,

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFrozen()
    {
        return false;
    }

//   from ParserFriendlyExternalizedMeshObjectFactory,

    /**
     * {@inheritDoc}
     */
    @Override
    public ParserFriendlyExternalizedMeshObject createParserFriendlyExternalizedMeshObject()
    {
        return new ParserFriendlyExternalizedMeshObject();
    }

//   from QuitListener


    /**
     * {@inheritDoc}
     */
    @Override
    public void prepareForQuit()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void die()
        throws
            IsDeadException
    {
        die( false );
    }

// from MeshBase

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBaseIndex getMeshBaseIndex()
    {
        if( theMeshBaseIndex == null ) {
            theMeshBaseIndex = MeshBaseIndex.create( this );
        }
        return theMeshBaseIndex;
    }

    /**
     * Set the MeshBaseIndex. This can only be performed once.
     *
     * @param index the new index
     */
    public void setMeshBaseIndex(
            MeshBaseIndex index )
    {
        theMeshBaseIndex = index;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        MeshBaseView mbv = findMeshBaseViewForCurrentTransaction();
        return mbv.existsMeshObjectByIdentifier( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject findMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        MeshBaseView mbv = findMeshBaseViewForCurrentTransaction();
        MeshObject   ret = mbv.findMeshObjectByIdentifier( identifier );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AccessManager getAccessManager()
    {
        return theAccessManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(
            TransactionAction0 act )
        throws
            TransactionActionException
    {
        // simple wrapper
        execute(( Transaction tx ) -> {
            act.execute();
            return null;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sudoExecute(
            TransactionAction0 act )
        throws
            TransactionActionException
    {
        try {
            ThreadIdentityManager.sudo();

            execute( act );

        } finally {
            ThreadIdentityManager.sudone();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T sudoExecute(
            TransactionAction1<T> act )
        throws
            TransactionActionException
    {
        try {
            ThreadIdentityManager.sudo();

            return execute( act );

        } finally {
            ThreadIdentityManager.sudone();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void executeAt(
            long               when,
            TransactionAction0 act )
        throws
            TransactionActionException
    {
        executeAt(   when,
                   ( Transaction tx ) -> {
                        act.execute();
                        return null;
                    });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sudoExecuteAt(
            long               when,
            TransactionAction0 act )
        throws
            TransactionActionException
    {
        try {
            ThreadIdentityManager.sudo();

            executeAt( when, act );

        } finally {
            ThreadIdentityManager.sudone();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T sudoExecuteAt(
            long                  when,
            TransactionAction1<T> act )
        throws
            TransactionActionException
    {
        try {
            ThreadIdentityManager.sudo();

            return executeAt( when, act );

        } finally {
            ThreadIdentityManager.sudone();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void discardAllAndBulkLoad(
            Collection<? extends ExternalizedMeshObject> data )
    {
        discardAllAndBulkLoad( data, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void discardAllAndBulkLoad(
            Collection<? extends ExternalizedMeshObject> data,
            MeshObjectIdentifierNamespace                newDefaultNamespace )
    {
        throw new UnsupportedOperationException( "Cannot bulk load into this MeshBase" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final synchronized void addDirectTransactionListener(
            TransactionListener newListener )
    {
        instantiateTransactionListenersIfNeeded();

        theTransactionListeners.addDirect( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final synchronized void addWeakTransactionListener(
            TransactionListener newListener )
    {
        instantiateTransactionListenersIfNeeded();

        theTransactionListeners.addWeak( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final synchronized void addSoftTransactionListener(
            TransactionListener newListener )
    {
        instantiateTransactionListenersIfNeeded();

        theTransactionListeners.addSoft( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final synchronized void removeTransactionListener(
            TransactionListener oldListener )
    {
        theTransactionListeners.remove( oldListener );
        if( theTransactionListeners.isEmpty() ) {
            theTransactionListeners = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDirectErrorListener(
            MeshBaseErrorListener newListener )
    {
        instantiateErrorListenersIfNeeded();

        theErrorListeners.addDirect( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addWeakErrorListener(
            MeshBaseErrorListener newListener )
    {
        instantiateErrorListenersIfNeeded();

        theErrorListeners.addWeak( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addSoftErrorListener(
            MeshBaseErrorListener newListener )
    {
        instantiateErrorListenersIfNeeded();

        theErrorListeners.addSoft( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeErrorListener(
            MeshBaseErrorListener oldListener )
    {
        theErrorListeners.remove( oldListener );
        if( theErrorListeners.isEmpty() ) {
            theErrorListeners = null;
        }
    }

    /**
     * Internal helper to instantiate theTransactionListeners if needed.
     */
    protected void instantiateTransactionListenersIfNeeded()
    {
        if( theTransactionListeners == null ) {
            theTransactionListeners = new FlexibleListenerSet<TransactionListener,Transaction,Integer>() {
                @Override
                public void fireEventToListener(
                        TransactionListener listener,
                        Transaction         tx,
                        Integer             flag )
                {
                    if( tx instanceof HeadTransaction ) {
                        HeadTransaction realTx = (HeadTransaction) tx;

                        if( flag == 0 ) {
                            listener.transactionStarted( realTx );
                        } else if( flag == 1 ) {
                            listener.transactionCommitted( realTx );
                        } else {
                            listener.transactionRolledback( realTx );
                        }
                    } else {
                        HistoryTransaction realTx = (HistoryTransaction) tx;

                        if( flag == 0 ) {
                            listener.transactionStarted( realTx );
                        } else if( flag == 1 ) {
                            listener.transactionCommitted( realTx );
                        } else {
                            listener.transactionRolledback( realTx );
                        }
                    }
                }
            };
        }
    }

    /**
     * Internal helper to instantiate theErrorListeners if needed.
     */
    protected void instantiateErrorListenersIfNeeded()
    {
        if( theErrorListeners == null ) {
            theErrorListeners = new FlexibleListenerSet<MeshBaseErrorListener,MeshBaseError,Void>() {
                @Override
                public void fireEventToListener(
                        MeshBaseErrorListener listener,
                        MeshBaseError         event,
                        Void                  flag )
                {
                    if( event instanceof MeshBaseError.UnresolvableEntityType ) {
                        listener.unresolveableEntityType( (MeshBaseError.UnresolvableEntityType) event );
                    } else if( event instanceof MeshBaseError.UnresolvableRoleType ) {
                        listener.unresolveableRoleType( (MeshBaseError.UnresolvableRoleType) event );
                    } else if( event instanceof MeshBaseError.UnresolvablePropertyType ) {
                        listener.unresolveablePropertyType( (MeshBaseError.UnresolvablePropertyType) event );
                    } else if( event instanceof MeshBaseError.IncompatibleDataType ) {
                        listener.incompatibleDataType( (MeshBaseError.IncompatibleDataType) event );
                    } else if( event instanceof MeshBaseError.PropertyNotOptional ) {
                        listener.propertyNotOptional( (MeshBaseError.PropertyNotOptional) event );
                    } else if( event instanceof MeshBaseError.OtherError ) {
                        listener.otherError( (MeshBaseError.OtherError) event );
                    } else {
                        log.error( "Unexpected event: " + event );
                    }
                }
            };
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void die(
             boolean isPermanent )
         throws
             IsDeadException
    {
        makeDead();

        internalDie( isPermanent );

        theTransactionListeners = null;
        // let's not set the storage to null, we want to know that it is collected at the same time

        QuitManager.removeQuitListener( this );
    }

    /**
      * This overridable method is invoked during die() and lets subclasses of AbstractMeshBase
      * clean up on their own. This implementation does nothing.
      *
      * @param isPermanent if true, this MeshBase will go away permanmently; if false, it may come alive again some time later
      */
    protected void internalDie(
            boolean isPermanent )
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isDead()
    {
        return isDead;
    }

    /**
      * Simple checking method to create IsDeadExceptions when necessary.
      *
      * @throws IsDeadException thrown if this LiveDeadObject is dead already; do nothing otherwise
      */
    public final void checkDead()
        throws
            IsDeadException
    {
        if( isDead ) {
            throw new IsDeadException( this );
        }
    }

    /**
     * This convenience method for our subclasses will set the isDead flag to false,
     * and continue, or throw an exception if we are dead already.
     *
     * @throws IsDeadException thrown if this object is dead already
     */
    protected synchronized void makeDead()
        throws
            IsDeadException
    {
        checkDead();

        isDead = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void addLiveDeadListener(
            LiveDeadListener newListener )
    {
        if( theLiveDeadListeners == null ) {
            theLiveDeadListeners = new ArrayList<>();
        }
        theLiveDeadListeners.add( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void removeLiveDeadListener(
            LiveDeadListener oldListener )
    {
        theLiveDeadListeners.remove( oldListener );
    }

    /**
     * This allows subclasses to fire off an event "we died".
     */
    protected final void fireObjectDied()
    {
        Iterator<LiveDeadListener> theIter;
        synchronized( this ) {
            if( theLiveDeadListeners == null || theLiveDeadListeners.isEmpty() ) {
                return;
            }

            theIter = new ArrayList<>( theLiveDeadListeners ).iterator();
        }

        while( theIter.hasNext() ) {
            LiveDeadListener current = theIter.next();
            current.objectDied( this );
        }
    }

// from CanBeDumped

    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "nObjects"
                },
                new Object[] {
                    this.size()
                });
    }

// internal

    /**
     * Enable subclasses to initialize the MeshBase's Home Object after the constructor has
     * returned.
     */
    protected void initializeHomeObject()
    {
        MeshObjectIdentifier homeObjectIdentifier = theMeshObjectIdentifierFactory.getHomeMeshObjectIdentifier();

        if( !existsMeshObjectByIdentifier( homeObjectIdentifier )) {
            // do not attempt load -- the MeshTypes needed to deserialize may not have been loaded yet
            sudoExecute( () -> createMeshObject( homeObjectIdentifier ));
        }
    }

    /**
     * Internal method to actually perform a transaction.
     *
     * @param act the code to be run in the Transaction
     * @param txFactory creates the actual Transaction object
     * @param canReuseTransaction checks whether an existing Transaction can be reused, instead of creating a new one
     * @return the result of the transaction, if any
     * @param <T> the type of return value
     */
    protected abstract <T> T execute(
            TransactionAction1<T>            act,
            Supplier<? extends Transaction>  txFactory,
            Predicate<Transaction>           canReuseTransaction );

    /**
      * Notify TransactionListeners that a Transaction has been started.
      * This shall not be invoked by the application programmer.
      *
      * @param tx the Transaction that has been started
      */
    public final void fireTransactionStartedEvent(
            Transaction tx )
    {
        FlexibleListenerSet<TransactionListener,Transaction,Integer> listeners = theTransactionListeners;
        if( listeners != null ) {
            listeners.fireEvent( tx, 0 );
        }
    }

    /**
      * Notify TransactionListeners that a Transaction has been committed.
      * This shall not be invoked by the application programmer.
      *
      * @param tx the Transaction that has been committed
      */
    public void fireTransactionCommittedEvent(
            Transaction tx )
    {
        FlexibleListenerSet<TransactionListener,Transaction,Integer> listeners = theTransactionListeners;
        if( listeners != null ) {
            listeners.fireEvent( tx, 1 );
        }
    }

    /**
      * Notify TransactionListeners that a Transaction has been rolled back.
      * This shall not be invoked by the application programmer.
      *
      * @param tx the Transaction that has been rolled back
      */
    public void fireTransactionRolledbackEvent(
            Transaction tx )
    {
        FlexibleListenerSet<TransactionListener,Transaction,Integer> listeners = theTransactionListeners;
        if( listeners != null ) {
            listeners.fireEvent( tx, 2 );
        }
    }

    /**
     * Notify MeshBaseErrorListeners that a MeshBaseError occurred.
     * This shall not be invoked by the application programmer.
     *
     * @param event the MeshBaseError
     */
    public final void notifyMeshBaseError(
            MeshBaseError event )
    {
        FlexibleListenerSet<MeshBaseErrorListener,MeshBaseError,Void> listeners = theErrorListeners;

        if( listeners == null ) {
            log.error( event );
        } else {
            listeners.fireEvent( event );
        }
    }

    /**
     * The factory for MeshObjects that we use.
     */
    protected final MeshObjectSetFactory theSetFactory;

    /**
     * The lifecycle manager.
     */
    protected final AbstractMeshBaseLifecycleManager theMeshBaseLifecycleManager;

    /**
     * The AccessManager that enforces access control, if any.
     */
    protected final AccessManager theAccessManager;

    /**
     * The MeshBaseIndex, if one has been set.
     */
    protected MeshBaseIndex theMeshBaseIndex;

    /**
      * The TransactionListeners (if any).
      */
    private FlexibleListenerSet<TransactionListener,Transaction,Integer> theTransactionListeners = null;

    /**
     * The ErrorListeners (if any).
     */
    private FlexibleListenerSet<MeshBaseErrorListener,MeshBaseError,Void> theErrorListeners = null;

    /**
     * Flag indicating whether we are dead already.
     */
    private boolean isDead;

    /**
     * The current list of listeners to our live dead state, allocated as needed.
     */
    private ArrayList<LiveDeadListener> theLiveDeadListeners = null;
}
