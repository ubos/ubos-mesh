//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import net.ubos.mesh.externalized.Decoder;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;

/**
 * Knows how to deserialize a list of Changes.
 */
public interface ChangeListDecoder
    extends
        Decoder
{
    /**
     * Decode a list of changes.
     *
     * @param r read the data from here
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return the changes contained in the stream
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public List<ExternalizedChange> decodeChanges(
            Reader                           r,
            MeshObjectIdentifierDeserializer idDeserializer )
        throws
            DecodingException,
            IOException;
}
