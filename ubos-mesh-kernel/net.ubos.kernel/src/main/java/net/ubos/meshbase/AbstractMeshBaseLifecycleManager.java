//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

import java.text.ParseException;
import net.ubos.mesh.EntityBlessedAlreadyException;
import net.ubos.mesh.IsAbstractException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.util.logging.Log;

/**
 * Collects implementation methods common to various implementations of
 * {@link MeshBaseLifecycleManager}.
 */
public abstract class AbstractMeshBaseLifecycleManager
{
    private static final Log log = Log.getLogInstance( AbstractMeshBaseLifecycleManager.class ); // our own, private logger

    /**
     * Enable the AbstractMeshBase to tell this MeshBaseLifecycleManager that they are working together.
     *
     * @param meshBase the AbstractMeshBase with which this MeshBaseLifecycleManager works
     */
    void setMeshBase(
            AbstractMeshBase meshBase )
    {
        if( theMeshBase != null ) {
            throw new IllegalStateException( "Have MeshBase already: " + theMeshBase );
        }
        theMeshBase = meshBase;
    }

    /**
     * Obtain the MeshBase that this MeshBaseLifecycleManager belongs to.
     *
     * @return the MeshBase
     */
    public AbstractMeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * <p>Create a new MeshObject without a type and an automatically created MeshObjectIdentifier
     * in the provided MeshBaseView.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public MeshObject createMeshObject(
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException
    {
        MeshObjectIdentifier identifier = mbv.createRandomMeshObjectIdentifier();

        try {
            return createMeshObject( identifier, mbv );

        } catch( MeshObjectIdentifierNotUniqueException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and an automatically created MeshObjectIdentifier in the provided MeshBaseView
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @param type the EntityType with which the MeshObject will be blessed
     * @return the created MeshObject
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public MeshObject createMeshObject(
            EntityType                   type,
            AbstractEditableMeshBaseView mbv )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        MeshObject ret = createMeshObject( mbv );
        try {
            ret.bless( type );

        } catch( EntityBlessedAlreadyException ex ) {
            log.error( ex );
        }
        return ret;
    }

    /**
     * <p>Create a new MeshObject without a type and with a provided MeshObjectIdentifier
     * in the provided MeshBaseView.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is to be created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObject(
            MeshObjectIdentifier         identifier,
            AbstractEditableMeshBaseView mbv )
        throws
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and a provided MeshObjectIdentifier in the provided MeshBaseView
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @param type the EntityType with which the MeshObject will be blessed
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public MeshObject createMeshObject(
            MeshObjectIdentifier         identifier,
            EntityType                   type,
            AbstractEditableMeshBaseView mbv )
        throws
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        MeshObject ret = createMeshObject( identifier, mbv );
        if( type != null ) {
            try {
                ret.bless( type );

            } catch( EntityBlessedAlreadyException ex ) {
                log.error( ex );
            }
        }
        return ret;
    }

    /**
     * <p>Create a new MeshObject without a type and with a provided MeshObjectIdentifier
     * in the provided MeshBaseView.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is to be created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public MeshObject createMeshObject(
            String                       identifier,
            AbstractEditableMeshBaseView mbv )
        throws
            ParseException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( getMeshBase().meshObjectIdentifierFromExternalForm( identifier ), mbv );
    }

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and a provided MeshObjectIdentifier in the provided MeshBaseView
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param identifier the identifier of the to-be-created MeshObject. If this is null,
     *                        automatically create a suitable MeshObjectIdentifier.
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @param type the EntityType with which the MeshObject will be blessed
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public MeshObject createMeshObject(
            String                       identifier,
            EntityType                   type,
            AbstractEditableMeshBaseView mbv )
        throws
            ParseException,
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( getMeshBase().meshObjectIdentifierFromExternalForm( identifier ), type, mbv );
    }

    /**
     * <p>Create a new MeshObject without a type and an automatically created MeshObjectIdentifier
     * that is in the same name space and "below" the MeshObjectIdentifier of another MeshObject, by
     * appending a trailing String, in the provided MeshBaseView.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param base the base MeshObject
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject                   base,
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException;

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and an automatically created MeshObjectIdentifier that is in the same name space and "below"
     * the MeshObjectIdentifier of another MeshObject, by appending a trailing String,
     * in the provided MeshBaseView.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param base the base MeshObject
     * @param type the EntityType with which the MeshObject will be blessed
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws TransactionException thrown if this method is invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject                   base,
            EntityType                   type,
            AbstractEditableMeshBaseView mbv )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>Create a new MeshObject without a type and with a MeshObjectIdentifier that is in the same name space
     * and "below" the MeshObjectIdentifier of another MeshObject, by appending a trailing String.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is to be created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.>/p>
     *
     * @param base the base MeshObject
     * @param trailingId the string to be appended to the MeshObjectIdentifier of the base MeshObject
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject                   base,
            String                       trailingId,
            AbstractEditableMeshBaseView mbv )
        throws
            ParseException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>This is a convenience method to create a MeshObject with exactly one EntityType
     * and a MeshObjectIdentifier that is in the same name space
     * and "below" the MeshObjectIdentifier of another MeshObject, by appending a trailing String.
     * This call is a "semantic create" which means that a new, semantically distinct object
     * is created.
     *
     * <p>Before this operation can be successfully invoked, a Transaction must be active
     * on this Thread.
     *
     * @param base the base MeshObject
     * @param trailingId the string to be appended to the MeshObjectIdentifier of the base MeshObject
     * @param type the EntityType with which the MeshObject will be blessed
     * @param mbv the MeshBaseView in which to create the MeshObject
     * @return the created MeshObject
     * @throws ParseException thrown if an error occurred when parsing the identifier
     * @throws IsAbstractException thrown if the EntityType is abstract and cannot be instantiated
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified identifier
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract MeshObject createMeshObjectBelow(
            MeshObject                   base,
            String                       trailingId,
            EntityType                   type,
            AbstractEditableMeshBaseView mbv )
        throws
            ParseException,
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException;

    /**
     * <p>Semantically delete a MeshObject.
     *
     * <p>This call is a "semantic delete", which means that an existing
     * MeshObject will go away in all its replicas. Due to time lag, the MeshObject
     * may still exist in certain replicas in other places for a while, but
     * the request to deleteMeshObjects all objects is in the queue and will get there
     * eventually.
     *
     * @param obj the MeshObject to be semantically deleted
     * @param mbv the MeshBaseView in which the MeshObject shall be deleted
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public void deleteMeshObject(
            MeshObject                   obj,
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException
    {
        MeshObject [] array = theMeshBase.createArray( 1 );
        array[0] = obj;
        deleteMeshObjects( array, mbv );
    }

    /**
     * <p>Semantically delete several MeshObjects at the same time.
     *
     * <p>This call is a "semantic delete", which means that an existing
     * MeshObject will go away in all its replicas. Due to time lag, the MeshObject
     * may still exist in certain replicas in other places for a while, but
     * the request to deleteMeshObjects all objects is in the queue and will get there
     * eventually.
     *
     * <p>This call either succeeds or fails in total: if one or more of the specified MeshObject cannot be
     *    deleted for some reason, none of the other MeshObjects will be deleted either.
     *
     * @param objs the MeshObjects to be semantically deleted
     * @param mbv the MeshBaseView in which the MeshObjects shall be deleted
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void deleteMeshObjects(
            MeshObject []                objs,
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException;

    /**
     * <p>Semantically delete all MeshObjects in a MeshObjectSet at the same time.
     *
     * <p>This call is a "semantic delete", which means that an existing
     * MeshObject will go away in all its replicas. Due to time lag, the MeshObject
     * may still exist in certain replicas in other places for a while, but
     * the request to deleteMeshObjects all objects is in the queue and will get there
     * eventually.
     *
     * <p>This call either succeeds or fails in total: if one or more of the specified MeshObject cannot be
     *    deleted for some reason, none of the other MeshObjects will be deleted either.
     *
     * @param set the set of MeshObjects to be semantically deleted
     * @param mbv the MeshBaseView in which the MeshObjects shall be deleted
     * @throws TransactionException thrown if this method was invoked outside of proper Transaction boundaries
     * @throws NotPermittedException thrown if the caller is not authorized to perform this operation
     */
    public abstract void deleteMeshObjects(
            MeshObjectSet                set,
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException;

    /**
     * Assign owner MeshObject to a newly created MeshObject. For subclasses only.
     *
     * @param obj the newly created MeshObject
     * @throws TransactionException thrown if invoked outside of proper Transaction boundaries
     */
    protected void assignOwner(
            MeshObject obj )
        throws
            TransactionException
    {
        AccessManager accessMgr = theMeshBase.getAccessManager();
        if( accessMgr == null ) {
            return; // no AccessManager
        }

        MeshObject caller = ThreadIdentityManager.getCaller();
        if( caller == null ) {
            return; // no owner
        }

        try {
            accessMgr.assignOwner( obj, caller );
        } catch( NotPermittedException ex ) {
            log.error( ex );
        }
    }

    /**
     * The MeshBase that we work on.
     */
    protected AbstractMeshBase theMeshBase;
}
