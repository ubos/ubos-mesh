//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized;

import java.util.List;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.modelbase.externalized.ExternalizedMeshObjectIdentifierNamespace;
import net.ubos.util.logging.Log;

/**
 * A MeshObjectIdentifierDeserializer suitable for used with importers. It learns
 * how to deserialize first, based on information in the header of the import file.
 */
public class ImporterMeshObjectIdentifierDeserializer
    extends
        ContextualMeshObjectIdentifierBothSerializer
{
    private static final Log log = Log.getLogInstance( ImporterMeshObjectIdentifierDeserializer.class );

    /**
     * Factory method.
     *
     * @param idFact the MeshObjectIdentifierFactory to use
     * @param map the ContextualMeshObjectIdentifierNamespaceMap that knows the local names
     * @return the created instance
     */
    public static ImporterMeshObjectIdentifierDeserializer create(
            MeshObjectIdentifierFactory                idFact,
            ContextualMeshObjectIdentifierNamespaceMap map )
    {
        return new ImporterMeshObjectIdentifierDeserializer( idFact, map );
    }

    /**
     * Constructor.
     *
     * @param idFact the MeshObjectIdentifierFactory to use
     * @param map the ContextualMeshObjectIdentifierNamespaceMap that knows the local names
     */
    protected ImporterMeshObjectIdentifierDeserializer(
            MeshObjectIdentifierFactory                idFact,
            ContextualMeshObjectIdentifierNamespaceMap map )
    {
        super( idFact, map );
    }

    /**
     * During parsing, a namespace declaration has been found.
     *
     * @param extNamespace the ExternalizedMeshObjectIdentifierNamespace that has been found
     */
    public void addNamespace(
            ExternalizedMeshObjectIdentifierNamespace extNamespace )
    {
        PrimaryMeshObjectIdentifierNamespaceMap primary = theMap.getPrimaryMap();

        List<String>                  externalNames = extNamespace.getExternalNames();
        String                        localName     = extNamespace.getLocalName();
        MeshObjectIdentifierNamespace ns            = theMap.findByLocalName( localName );

        if( ns != null ) {
            throw new IllegalArgumentException( "Duplicate local name: " + localName );
        }

        if( externalNames.isEmpty() ) {
            ns = primary.getPrimary();
        } else {
            ns = primary.obtainByExternalName( externalNames.get( 0 )); // will also set first as preferred when newly allocated
        }

        theMap.register( localName, ns );

        // add extra external names, and sanity check
        for( int i=1 ; i<externalNames.size() ; ++i ) {
            String current = externalNames.get( i );

            if( !ns.getExternalNames().contains( current )) {

                if( primary.findByExternalName( current ) != null ) {
                    throw new IllegalArgumentException( "To-be-added MeshObjectIdentifierNamespace has conflicting external names with existing namespaces" );
                    // this needs better error reporting
                } else {
                    ns.addExternalName( current );
                }
            }
        }
    }
}
