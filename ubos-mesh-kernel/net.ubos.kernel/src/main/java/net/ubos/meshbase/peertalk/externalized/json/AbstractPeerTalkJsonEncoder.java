//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.peertalk.externalized.json;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.util.StringHelper;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.meshbase.transaction.externalized.json.AbstractChangeListJsonEncoder;

/**
 * Knows how to PeerTalk in JSON.
 */
public abstract class AbstractPeerTalkJsonEncoder
    extends
        AbstractChangeListJsonEncoder
    implements
        PeerTalkJsonTags
{
    /**
     * Constructor.
     *
     * @param typeIdSerializer knows how to serialize MeshTypeIdentifiers
     */
    protected AbstractPeerTalkJsonEncoder(
            MeshTypeIdentifierSerializer typeIdSerializer )
    {
        super( typeIdSerializer );
    }

    /**
     * Write a ChangeList to a Writer as a full PeerTalk file.
     *
     * @param changes the ChangeList
     * @param emitOldValues if true, always emit old values even if they are redundant
     * @param emitNewValues if true, always emit new values even if they are redundant
     * @param nsMap use this namespace map
     * @param w the Writer to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    public void writePeerTalk(
            ChangeList                                 changes,
            boolean                                    emitOldValues,
            boolean                                    emitNewValues,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            Writer                                     w )
        throws
            EncodingException,
            IOException
    {
        JsonWriter jw = new JsonWriter( w );
        jw.setSerializeNulls( true );
        jw.setIndent( " " );

        writePeerTalk( changes, emitOldValues, emitNewValues, nsMap, jw );

        jw.flush();
    }

    /**
     * Serialize a list of Changes to a JsonWriter in PeerTalk format.
     *
     * @param changes the ChangeList
     * @param emitOldValues if true, always emit old values even if they are redundant
     * @param emitNewValues if true, always emit new values even if they are redundant
     * @param nsMap knows about namespaces
     * @param jw the JsonWriter to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    public void writePeerTalk(
            ChangeList                                 changes,
            boolean                                    emitOldValues,
            boolean                                    emitNewValues,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            JsonWriter                                 jw )
        throws
            EncodingException,
            IOException
    {
        ContextualMeshObjectIdentifierBothSerializer idSerializer
                = ContextualMeshObjectIdentifierBothSerializer.create( changes.getMeshBase(), nsMap );

        jw.beginObject();
        jw.name( HEADER_TAG );
        writeHeader( changes, nsMap, jw );

        jw.name( CHANGES_TAG );
        writeChangeList( changes, idSerializer, emitOldValues, emitNewValues, jw );
        jw.endObject();
    }

    /**
     * Write the header of the export file. This also produces a map
     * of MeshObjectIdentifierNamespace to local name, which is used
     * in the main body of the export for MeshObjectIdentifiers
     *
     * @param changes the list of changes
     * @param nsMap track the namespaces and their local names here
     * @param jw the JsonWriter to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    protected void writeHeader(
            ChangeList                                  changes,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            JsonWriter                                 jw )
        throws
            IOException,
            EncodingException
    {
        jw.beginObject();
        jw.name( FORMAT_TAG ).value( FORMAT_TAG_VALUE );
        jw.name( VERSION_TAG ).value( VERSION_TAG_VALUE );

        MeshBase                      mb        = changes.getMeshBase();
        MeshObjectIdentifierNamespace defaultNs = mb.getDefaultNamespace();

        nsMap.register( ContextualMeshObjectIdentifierNamespaceMap.DEFAULT_LOCAL_NAME, defaultNs ); // keep the default namespace of the MeshBase in the export

        HashSet<SubjectArea> sas = new HashSet<>();
        for( Change current : changes ) {
            current.addReferencedSubjectAreasTo( sas );

            for( MeshObjectIdentifier affectedId : current.getAffectedMeshObjectIdentifiers() ) {
                MeshObjectIdentifierNamespace ns = affectedId.getNamespace();

                nsMap.obtainLocalNameFor( ns ); // registers if needed
            }
        }

        CursorIterator<String> localNameIter = nsMap.localNameIterator();
        if( localNameIter.hasNext() ) {
            jw.name( MESHOBJECTIDENTIFIERNAMESPACES_TAG );
            jw.beginObject();

            while( localNameIter.hasNext() ) {
                String                        localName = localNameIter.next();
                MeshObjectIdentifierNamespace ns        = nsMap.findByLocalName( localName );

                jw.name( localName );
                jw.beginObject();

                if( ns.getPreferredExternalName() != null ) {
                    jw.name( PREFERRED_EXTERNALNAME_TAG );
                    jw.value( ns.getPreferredExternalName() );
                }

                if( ns.getExternalNames().size() > 1 ) {
                    jw.name( EXTERNALNAMES_TAG );
                    jw.beginArray();
                    for( String externalName : ns.getExternalNames()) {
                        if( StringHelper.compareTo( externalName, ns.getPreferredExternalName() ) != 0 ) {
                            jw.value( externalName );
                        }
                    }
                    jw.endArray();
                }

                jw.endObject();
            }
            jw.endObject();
        }
        if( !sas.isEmpty() ) {
            jw.name( SUBJECTAREAS_TAG );
            jw.beginObject();

            for( SubjectArea current : sas ) {
                jw.name( theTypeIdSerializer.toExternalForm( current.getIdentifier() ) );
                jw.beginObject();
                // for now. Add things like version later
                jw.endObject();
            }
            jw.endObject();
        }
        jw.endObject();
    }
}
