//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObjectIdentifier;

/**
 *
 */
public abstract class AbstractExternalizedMeshObjectAttributesChange
    extends
        AbstractExternalizedChange
{
    public AbstractExternalizedMeshObjectAttributesChange(
            MeshObjectIdentifier sourceIdentifier,
            String []            oldValues,
            String []            deltaValues,
            String []            newValues )
    {
        super( sourceIdentifier );

        theOldValues   = oldValues;
        theDeltaValues = deltaValues;
        theNewValues   = newValues;
    }

    public String [] getOldValue()
    {
        return theOldValues;
    }

    public String [] getDeltaValue()
    {
        return theDeltaValues;
    }

    public String [] getNewValue()
    {
        return theNewValues;
    }

    protected final String [] theOldValues;
    protected final String [] theDeltaValues;
    protected final String [] theNewValues;
}
