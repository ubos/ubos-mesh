//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.a;

import java.io.Serializable;
import java.text.ParseException;
import net.ubos.mesh.AbstractMeshObject;
import net.ubos.mesh.AttributesMap;
import net.ubos.mesh.IsAbstractException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.PropertiesMap;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.a.AMeshObjectIdentifier;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.security.MustNotDeleteHomeObjectException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.AbstractMeshBaseLifecycleManager;
import net.ubos.meshbase.MeshBaseError;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.transaction.MeshObjectCreateChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;
import net.ubos.mesh.externalized.HasTypesPropertiesAttributes;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.AbstractMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.transaction.MeshObjectDeleteChange;

/**
 * A MeshBaseLifecycleManager appropriate for the AMeshBase implementation of MeshBase.
 */
public class AMeshBaseLifecycleManager
        extends
            AbstractMeshBaseLifecycleManager
{
    private static final Log log = Log.getLogInstance( AMeshBaseLifecycleManager.class ); // our own, private logger

    /**
     * Factory method. The application developer should not call this or a subclass constructor; use
     * MeshBase.getMeshObjectLifecycleManager() instead.
     *
     * @return the created AMeshBaseLifecycleManager
     */
    public static AMeshBaseLifecycleManager create()
    {
        return new AMeshBaseLifecycleManager();
    }

    /**
     * Constructor, for subclasses only.
     */
    protected AMeshBaseLifecycleManager()
    {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized AMeshObject createMeshObject(
            MeshObjectIdentifier         identifier,
            AbstractEditableMeshBaseView here )
    {
        AMeshObjectIdentifier realIdentifier = (AMeshObjectIdentifier) identifier;

        ATransaction tx  = (ATransaction) theMeshBase.checkTransaction();

        checkPermittedCreate( identifier, here );

        AccessManager access = theMeshBase.getAccessManager();
        access.checkPermittedCreate( here, identifier );

        AMeshObject ret = instantiateMeshObjectImplementation( realIdentifier, tx, here );

        tx.putCreatedMeshObjectIntoMeshBaseView( ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject                   base,
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( theMeshBase.createRandomMeshObjectIdentifierBelow( base.getIdentifier() ), mbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject                   base,
            EntityType                   type,
            AbstractEditableMeshBaseView mbv )
        throws
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( theMeshBase.createRandomMeshObjectIdentifierBelow( base.getIdentifier() ), type, mbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject                   base,
            String                       trailingId,
            AbstractEditableMeshBaseView mbv )
        throws
            ParseException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( theMeshBase.createMeshObjectIdentifierBelow( base.getIdentifier(), trailingId ), mbv );
    }

    @Override
    public MeshObject createMeshObjectBelow(
            MeshObject                   base,
            String                       trailingId,
            EntityType                   type,
            AbstractEditableMeshBaseView mbv )
        throws
            ParseException,
            IsAbstractException,
            MeshObjectIdentifierNotUniqueException,
            TransactionException,
            NotPermittedException
    {
        return createMeshObject( theMeshBase.createMeshObjectIdentifierBelow( base.getIdentifier(), trailingId ), type, mbv );
    }

    /**
     * Recreate the AMeshObject instance from this externalized representation, but do not add
     * to the MeshBaseView. This is an internal implementation method.
     *
     * @param theObjectBeingParsed external form of the to-be-recreated MeshObject
     * @param mbv the MeshBaseView it will belong to
     * @return the recreated MeshObject
     */
    public AMeshObject recreateMeshObject(
            ExternalizedMeshObject theObjectBeingParsed,
            AbstractMeshBaseView   mbv )
    {
        AMeshObjectIdentifier   realIdentifier = (AMeshObjectIdentifier) theObjectBeingParsed.getIdentifier(); // thrown ClassCast immediately
        MeshTypeIdentifier []   meshTypeNames = theObjectBeingParsed.getTypeIdentifiers();
        EntityType         []   types         = new EntityType[ meshTypeNames.length ];

        int typeCounter = 0;
        for( int i=0 ; i<meshTypeNames.length ; ++i ) {
            try {
                // it's possible that the schema changed since we read this object. Try to recover.
                types[typeCounter] = (EntityType) ModelBase.SINGLETON.findMeshType( meshTypeNames[i] );
                typeCounter++; // make sure we do the increment after an exception might have been thrown

            } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                theMeshBase.notifyMeshBaseError( new MeshBaseError.UnresolvableEntityType( theMeshBase, theObjectBeingParsed, meshTypeNames[i] ));
            } catch( Exception ex ) {
                theMeshBase.notifyMeshBaseError( new MeshBaseError.OtherError( theMeshBase, theObjectBeingParsed, ex ));
            }
        }
        if( typeCounter < types.length ) {
            types = ArrayHelper.copyIntoNewArray( types, 0, typeCounter, EntityType.class );
        }

        // Attributes

        AttributesMap attributes = checkInsertAttributes(
                theObjectBeingParsed.getAttributeNames(),
                theObjectBeingParsed.getAttributeValues(),
                theObjectBeingParsed );

        // properties

        PropertiesMap properties = checkInsertProperties(
                types,
                theObjectBeingParsed.getPropertyTypes(),
                theObjectBeingParsed.getPropertyValues(),
                theObjectBeingParsed );


        // relationships and role properties

        HasTypesPropertiesAttributes [] thisEnds             = theObjectBeingParsed.getThisEnds();
        AMeshObjectIdentifier []        neighborIdentifiers  = null;
        long []                         neighborTimeCreateds = null;
        RoleType [][]                   roleTypes            = null;
        AttributesMap []                roleAttributes       = null;
        PropertiesMap []                roleProperties       = null;

        if( thisEnds.length > 0 ) {

            neighborIdentifiers  = new AMeshObjectIdentifier[ thisEnds.length ];
            neighborTimeCreateds = new long[ thisEnds.length ];
            roleTypes            = new RoleType[ thisEnds.length ][];
            roleAttributes       = new AttributesMap[ thisEnds.length ];
            roleProperties       = new PropertiesMap[ thisEnds.length ];

            for( int i=0 ; i<thisEnds.length ; ++i ) {
                neighborIdentifiers[i]  = (AMeshObjectIdentifier) thisEnds[i].getIdentifier();
                neighborTimeCreateds[i] = thisEnds[i].getTimeCreated();

                MeshTypeIdentifier [] currentRoleTypes = thisEnds[i].getTypeIdentifiers();

                roleTypes[i] = new RoleType[ currentRoleTypes.length ];
                typeCounter = 0;

                for( int j=0 ; j<currentRoleTypes.length ; ++j ) {
                    try {
                        roleTypes[i][typeCounter] = ModelBase.SINGLETON.findRoleType( currentRoleTypes[j] );
                        typeCounter++; // make sure we do the increment after an exception might have been thrown

                    } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                        theMeshBase.notifyMeshBaseError( new MeshBaseError.UnresolvableRoleType( theMeshBase, theObjectBeingParsed, currentRoleTypes[j] ));
                    } catch( Exception ex ) {
                        theMeshBase.notifyMeshBaseError( new MeshBaseError.OtherError( theMeshBase, theObjectBeingParsed, ex ));
                    }
                }
                if( typeCounter < roleTypes[i].length ) {
                    roleTypes[i] = ArrayHelper.copyIntoNewArray( roleTypes[i], 0, typeCounter, RoleType.class );
                }

                // role attributes

                roleAttributes[i] = checkInsertAttributes(
                        thisEnds[i].getAttributeNames(),
                        thisEnds[i].getAttributeValues(),
                        theObjectBeingParsed );

                // role properties

                roleProperties[i] = checkInsertProperties(
                        roleTypes[i],
                        thisEnds[i].getPropertyTypes(),
                        thisEnds[i].getPropertyValues(),
                        theObjectBeingParsed );
            }
        }

        // instantiate

        AMeshObject ret = instantiateRecreatedMeshObject(
                realIdentifier,
                mbv,
                attributes,
                properties,
                types,
                neighborIdentifiers,
                neighborTimeCreateds,
                roleAttributes,
                roleTypes,
                roleProperties,
                theObjectBeingParsed,
                theObjectBeingParsed.getIsDead() || !theObjectBeingParsed.hasSomethingBeenSet()
                        ? AbstractMeshObject.State.DEAD
                        : AbstractMeshObject.State.ALIVE );

        ret.setTimeCreated( theObjectBeingParsed.getTimeCreated() );
        ret.setTimeUpdated( theObjectBeingParsed.getTimeUpdated() );

        return ret;
    }

    /**
     * Factored-out helper method to check and insert values for Attributes for both
     * MeshObjects and Roles.
     *
     * @param attributeNames
     * @param attributeValues
     * @param theObjectBeingParsed
     * @return
     */
    protected AttributesMap checkInsertAttributes(
            String []              attributeNames,
            Serializable []        attributeValues,
            ExternalizedMeshObject theObjectBeingParsed )
    {
        AttributesMap ret;
        if( attributeNames != null && attributeNames.length > 0 ) {
            ret = new AttributesMap();

            for( int i=0 ; i<attributeNames.length ; ++i ) {
                ret.put( attributeNames[i], attributeValues[i] );
            }
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * Factored-out helper method to check and insert values for properties for
     * both EntityTypes and RoleTypes.
     *
     * @param types
     * @param propertyTypeNames
     * @param propertyValues
     * @param theObjectBeingParsed external form of the to-be-recreated MeshObject
     * @return a PropertiesMap, or null if no properties
     */
    protected PropertiesMap checkInsertProperties(
            MeshTypeWithProperties [] types,
            MeshTypeIdentifier []     propertyTypeNames,
            PropertyValue []          propertyValues,
            ExternalizedMeshObject    theObjectBeingParsed )
    {
        PropertiesMap ret = new PropertiesMap();

        // set defaults first
        for( MeshTypeWithProperties type : types ) {
            for( PropertyType propertyType : type.getPropertyTypes() ) {
                if( propertyType.getDefaultValue() == null ) {
                    continue;
                }
                ret.put( propertyType, propertyType.getDefaultValue() );
            }
        }

        for( int i=0 ; i<propertyTypeNames.length ; ++i ) {
            try {
                // it's possible that the schema changed since we read this object. Try to recover.
                PropertyType propertyType = (PropertyType) ModelBase.SINGLETON.findMeshType( propertyTypeNames[i] );

                if( propertyValues[i] != null ) {
                    if( propertyType.getDataType().conforms( propertyValues[i] ) != 0 ) {
                        theMeshBase.notifyMeshBaseError( new MeshBaseError.IncompatibleDataType( theMeshBase, theObjectBeingParsed, propertyType, propertyValues[i] ));
                        continue;
                    }
                } else if( !propertyType.getIsOptional().value() ) {
                    theMeshBase.notifyMeshBaseError( new MeshBaseError.PropertyNotOptional( theMeshBase, theObjectBeingParsed, propertyType ));
                    propertyValues[i] = propertyType.getDefaultValue();
                }
                // now we patch EnumeratedValues
                DataType type = propertyType.getDataType();
                if( type instanceof EnumeratedDataType && propertyValues[i] instanceof EnumeratedValue ) {
                    EnumeratedDataType realType  = (EnumeratedDataType) type;
                    EnumeratedValue    realValue = realType.select( ((EnumeratedValue)propertyValues[i]).value() );
                    ret.put( propertyType, realValue );
                } else {
                    ret.put( propertyType, propertyValues[i] );
                }

            } catch( MeshTypeWithIdentifierNotFoundException ex ) {
                theMeshBase.notifyMeshBaseError( new MeshBaseError.UnresolvablePropertyType( theMeshBase, theObjectBeingParsed, propertyTypeNames[i] ));
            } catch( Exception ex ) {
                theMeshBase.notifyMeshBaseError( new MeshBaseError.OtherError( theMeshBase, theObjectBeingParsed, ex ));
            }
        }

        // make sure all mandatory properties have values. This can happen if a mandatory PropertyType
        // has been added to the model after the instance was written to disk. (not that one should change
        // a model that radically, but it happens.)

        for( MeshTypeWithProperties current : types ) {
            for( PropertyType current2 : current.getPropertyTypes() ) {
                if( !current2.getIsOptional().value() ) {
                    PropertyValue found = ret.get( current2 );
                    if( found == null ) {
                        ret.put( current2, current2.getDefaultValue() );
                    }
                }
            }
        }

        if( ret.isEmpty() ) {
            return null;
        } else {
            return ret;
        }
    }

    /**
     * Check whether it is permitted to create a MeshObject with the specified parameters.
     *
     * @param identifier the identifier of the to-be-created MeshObject. This must not be null.
     * @param mbv the MeshBaseView in which to check
     * @throws MeshObjectIdentifierNotUniqueException a MeshObject exists already in this MeshBase with the specified Identifier
     * @throws NotPermittedException thrown if the combination of arguments was not permitted
     * @throws IllegalArgumentException thrown if an illegal argument was specified
     */
    protected void checkPermittedCreate(
            MeshObjectIdentifier identifier,
            MeshBaseView         mbv )
        throws
            MeshObjectIdentifierNotUniqueException,
            NotPermittedException,
            IllegalArgumentException
    {
        if( identifier == null ) {
            throw new IllegalArgumentException( "null MeshObjectIdentifier" );
        }

        MeshObject existing = mbv.findMeshObjectByIdentifier( identifier );
        if( existing != null ) {
            throw new MeshObjectIdentifierNotUniqueException( existing );
        }

        if( mbv instanceof HistoricMeshBaseView ) {
            HistoricMeshBaseView realMbv = (HistoricMeshBaseView) mbv;

            MeshObjectHistory history = realMbv.getMeshBase().meshObjectHistory( identifier );
            if( history == null ) {
                return;
            }

            existing = history.after( realMbv.getTimeUpdated() );
            if( existing != null ) {
                throw new MeshObjectIdentifierNotUniqueException( existing );
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void deleteMeshObjects(
            MeshObject []                objs,
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException
    {
        ATransaction tx = (ATransaction) mbv.checkTransaction();

        AccessManager accessMgr = getMeshBase().getAccessManager();

        MeshObject home = mbv.getHomeObject();

        for( int i=0 ; i<objs.length ; ++i ) {
            if( objs[i] == null ) {
                throw new NullPointerException( "MeshObject at index " + i + " is null" );
            }
            if( objs[i].getMeshBaseView() != mbv ) {
                throw new IllegalArgumentException( "cannot delete MeshObjects in a different MeshBases" );
            }
            if( objs[i] == home ) {
                throw new MustNotDeleteHomeObjectException( home.getMeshBaseView() );
            }
        }
        for( int i=0 ; i<objs.length ; ++i ) {
            accessMgr.checkPermittedDelete( objs[i] );
        }
        for( int i=0 ; i<objs.length ; ++i ) {
            AMeshObject current = (AMeshObject) objs[i];
            if( !current.getIsDead() ) {
                // this may be a loop, or this object was deleted already as part of a cascading delete performed
                // earlier in theObjects

                current.delete();

                tx.addChange( new MeshObjectDeleteChange( current, current.getIdentifier() ) );

                tx.removeFromMeshBaseView( current.getIdentifier() );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMeshObjects(
            MeshObjectSet                set,
            AbstractEditableMeshBaseView mbv )
        throws
            TransactionException,
            NotPermittedException
    {
        deleteMeshObjects( set.getMeshObjects(), mbv );
    }

    /**
     * Helper method to instantiate the right subclass of MeshObject. This makes the creation
     * of subclasses of AMeshBase and this class much easier.
     *
     * @param identifier the identifier of the to-be-created MeshObject. This must not be null.
     * @param tx the current Transaction to which we need to add all the Changes we made
     * @param mbv use this MeshBaseView as the MeshObject's
     * @return the created MeshObject
     */
    protected AMeshObject instantiateMeshObjectImplementation(
            AMeshObjectIdentifier identifier,
            ATransaction          tx,
            AbstractMeshBaseView  mbv )
    {
        AMeshObject ret = new AMeshObject(
                identifier,
                mbv,
                AbstractMeshObject.State.ALIVE );

        tx.addChange( new MeshObjectCreateChange( ret, ret.getIdentifier() ));

        return ret;
    }

    /**
     * Factored out method to instantiate a recreated MeshObject. This exists to make
     * it easy to override it in subclasses.
     *
     * @param identifier the identifier of the MeshObject
     * @param mbv the MeshBaseView where to instantiate the MeshObject
     * @param attributes the Attributes of the MeshObject
     * @param properties the properties of the MeshObject
     * @param types the EntityTypes of the MeshObject
     * @param neighborIdentifiers the identifiers of the MeshObject's neighbors, if any
     * @param neighborTimeCreateds the timeCreateds of the MeshObject's neighbors
     * @param roleAttributes the RoleAttributes in which this MeshObject participates with its neighbors
     * @param roleTypes the RoleTypes in which this MeshObject participates with its neighbors
     * @param roleProperties the Role Properties in sequence of the neighbors
     * @param objectBeingParsed the externalized representation of the MeshObject
     * @param state the State of the MeshObject
     * @return the recreated AMeshObject
     */
    protected AMeshObject instantiateRecreatedMeshObject(
            AMeshObjectIdentifier        identifier,
            AbstractMeshBaseView         mbv,
            AttributesMap                attributes,
            PropertiesMap                properties,
            EntityType []                types,
            AMeshObjectIdentifier []     neighborIdentifiers,
            long []                      neighborTimeCreateds,
            AttributesMap []             roleAttributes,
            RoleType [][]                roleTypes,
            PropertiesMap []             roleProperties,
            ExternalizedMeshObject       objectBeingParsed,
            AbstractMeshObject.State     state )
    {
        AMeshObject ret = new AMeshObject(
                identifier,
                mbv,
                attributes,
                properties,
                types,
                neighborIdentifiers,
                neighborTimeCreateds,
                roleAttributes,
                roleTypes,
                roleProperties,
                state );

        return ret;
    }

}
