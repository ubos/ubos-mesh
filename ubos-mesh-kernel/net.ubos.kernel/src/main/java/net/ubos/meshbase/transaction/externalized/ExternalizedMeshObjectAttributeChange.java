//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.CannotApplyChangeException;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;

/**
 *
 */
public class ExternalizedMeshObjectAttributeChange
    extends
        AbstractExternalizedChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier the identifier of the MeshObject that is the source of the event
     * @param attributeName the name of the Attribute
     * @param oldValue the old value of the Attribute, prior to the event
     * @param newValue the new value of the Attribute, after the event
     */
    public ExternalizedMeshObjectAttributeChange(
            MeshObjectIdentifier sourceIdentifier,
            String               attributeName,
            Serializable         oldValue,
            Serializable         newValue )
    {
        super( sourceIdentifier );

        theAttributeName = attributeName;
        theOldValue      = oldValue;
        theNewValue      = newValue;
    }

    public String getAttributeName()
    {
        return theAttributeName;
    }

    public Serializable getOldValue()
    {
        return theOldValue;
    }

    public Serializable getNewValue()
    {
        return theNewValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier [] getAffectedMeshObjectIdentifiers()
    {
        return new MeshObjectIdentifier[] { theSourceIdentifier };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            MeshBase base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        try {
            MeshObject source = base.findMeshObjectByIdentifierOrThrow( theSourceIdentifier );

            source.setAttributeValue( theAttributeName, theNewValue );

            return source;

        } catch( MeshObjectsNotFoundException ex ) {
            throw new CannotApplyChangeException( base, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectAttributeChange internalizeWith(
            MeshObject [] affectedMeshObjects )
        throws
            MeshTypeWithIdentifierNotFoundException
    {
        if( affectedMeshObjects.length != 1 ) {
            throw new IllegalArgumentException();
        }

        return new MeshObjectAttributeChange( affectedMeshObjects[0], theAttributeName, theOldValue, theNewValue );
    }

    protected final String theAttributeName;
    protected final Serializable theOldValue;
    protected final Serializable theNewValue;
}
