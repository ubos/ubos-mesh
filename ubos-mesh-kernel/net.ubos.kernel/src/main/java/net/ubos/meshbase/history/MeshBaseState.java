//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.history;

import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.util.history.HasTimeUpdated;

/**
 * The state of a MeshBase at some time during its history. It carries
 * both "stock" and "flow" information -- the MeshBaseView of how the
 * MeshBase was at that time, and the Changes that were made during the
 * Transaction that let to this MeshBaseState.
 *
 * It is constructed to be subclassable, so that StoreMeshBaseState can
 * determine the list of changes lazily.
 */
public abstract class MeshBaseState
    implements
        HasTimeUpdated
{
    /**
     * Constructor.
     *
     * @param timeUpdated the time when this state begun, in System.currentTimeMillis() format
     * @param changes the Changes that led to this state, if known
     */
    protected MeshBaseState(
            long       timeUpdated,
            ChangeList changes )
    {
        theTimeUpdated  = timeUpdated;
        theChanges      = changes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * The MeshBase of which this is a MeshBaseState.
     *
     * @return the MeshBase
     */
    public MeshBase getMeshBase()
    {
        return getMeshBaseView().getMeshBase();
    }

    /**
     * Obtain the Changes that were applied to this MeshBase prior to this MeshBaseState,
     * which lead to this MeshBaseState.
     *
     * @return the changes
     */
    public ChangeList getChangeList()
    {
        ensureChangesResolved();
        return theChanges;
    }

    /**
     * Obtain the MeshBaseView that contains the MeshObjects at this MeshBaseState.
     *
     * @return the view
     */
    public abstract HistoricMeshBaseView getMeshBaseView();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof MeshBaseState ) {
            MeshBaseState realOther = (MeshBaseState) other;

            return getMeshBase() == realOther.getMeshBase() && theTimeUpdated == realOther.theTimeUpdated;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return ((int) theTimeUpdated ) ^ getMeshBase().hashCode();
    }

    /**
     * Internal helper to make sure the externalized Changes have been resolved.
     * Does nothing on this level.
     */
    protected void ensureChangesResolved()
    {}

    /**
     * The time when this MeshBaseState begun.
     */
    protected final long theTimeUpdated;

    /**
     * The Changes that led to this MeshBaseState, once they have been resolved
     */
    protected ChangeList theChanges;
}
