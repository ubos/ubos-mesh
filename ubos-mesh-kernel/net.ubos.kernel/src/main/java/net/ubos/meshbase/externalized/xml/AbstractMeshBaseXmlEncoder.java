//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.externalized.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.externalized.xml.AbstractMeshObjectXmlEncoder;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifierSerializer;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Parses an XML stream of MeshObjects.
 */
public abstract class AbstractMeshBaseXmlEncoder
    extends
        AbstractMeshObjectXmlEncoder
    implements
        MeshBaseXmlTags
{
    /**
     * Bulk write the content of a MeshBase.
     *
     * @param mb the MeshBase
     * @param w the Writer to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    public void bulkWrite(
            MeshBase                                   mb,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            Writer                                     w )
        throws
            IOException,
            EncodingException
    {
        ContextualMeshObjectIdentifierBothSerializer idSerializer
                = ContextualMeshObjectIdentifierBothSerializer.create( mb, nsMap );

        w.write( "<" );
        w.write( UBOS_MESH_TAG );
        w.write( ">\n" );

        w.write( " <" );
        w.write( HEADER_TAG );
        w.write( ">\n" );

        writeHeader( mb, nsMap, w );

        w.write( " </" );
        w.write( HEADER_TAG );
        w.write( ">\n" );

        w.write( " <" );
        w.write( BODY_TAG );
        w.write( ">\n" );

        writeBody( mb, idSerializer, w );

        w.write( " </" );
        w.write( BODY_TAG );
        w.write( ">\n" );

        w.write( "</" );
        w.write( UBOS_MESH_TAG );
        w.write( ">\n" );
    }

    /**
     * Write the header of the export file.
     *
     * @param mb the MeshBase
     * @param nsMap track the namespaces and their local names here
     * @param w the Writer to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    protected void writeHeader(
            MeshBase                                   mb,
            ContextualMeshObjectIdentifierNamespaceMap nsMap,
            Writer                                     w )
        throws
            IOException,
            EncodingException
    {
        w.write( "  <" );
        w.write( FORMAT_TAG );
        w.write( ">" );
        w.write( FORMAT_TAG_VALUE );
        w.write( "</" );
        w.write( FORMAT_TAG );
        w.write( ">\n" );

        w.write( "  <" );
        w.write( VERSION_TAG );
        w.write( ">" );
        w.write( VERSION_TAG_VALUE );
        w.write( "</" );
        w.write( VERSION_TAG );
        w.write( ">\n" );

        // This can certainly be done more efficiently wrt memory
        HashSet<SubjectArea> sas = new HashSet<>();

        nsMap.register( ContextualMeshObjectIdentifierNamespaceMap.DEFAULT_LOCAL_NAME, mb.getDefaultNamespace() ); // keep the default namespace of the MeshBase in the export

        for( MeshObject current : mb ) {
            for( EntityType current2 : current.getEntityTypes() ) {
                sas.add( current2.getSubjectArea() );
            }
            for( RoleType current2 : current.getRoleTypes() ) {
                sas.add( current2.getSubjectArea() );
            }
            MeshObjectIdentifierNamespace ns = current.getIdentifier().getNamespace();
            nsMap.obtainLocalNameFor( ns ); // registers if needed
        }

        CursorIterator<String> localNameIter = nsMap.localNameIterator();
        if( localNameIter.hasNext() ) {
            w.write( "  <" );
            w.write( MESHOBJECTIDENTIFIERNAMESPACES_TAG );
            w.write( ">\n" );

            while( localNameIter.hasNext() ) {
                String                        localName = localNameIter.next();
                MeshObjectIdentifierNamespace ns        = nsMap.findByLocalName( localName );

                w.write( "   <" );
                w.write( MESHOBJECTIDENTIFIERNAMESPACE_TAG );
                w.write( " " );
                w.write( IDENTIFIER_TAG );
                w.write( "=\"" );
                w.write( localName );
                w.write( "\"/>\n" );

                for( String externalName : ns.getExternalNames()) {
                    w.write( "    <" );
                    w.write( EXTERNALNAME_TAG );
                    if( externalName.equals( ns.getPreferredExternalName() )) {
                        w.write( " " );
                        w.write( EXTERNALNAME_PREFERRED_TAG );
                        w.write( "\"" );
                        w.write( EXTERNALNAME_PREFERRED_TRUE_TAG_VALUE );
                        w.write( "\"" );
                    }
                    w.write( ">" );
                    w.write( externalName );
                    w.write( "</" );
                    w.write( EXTERNALNAME_TAG );
                    w.write( ">\n" );
                }

                w.write( "   </" );
                w.write( MESHOBJECTIDENTIFIERNAMESPACE_TAG );
                w.write( "\"/>\n" );
            }

            w.write( "  </" );
            w.write( MESHOBJECTIDENTIFIERNAMESPACES_TAG );
            w.write( ">\n" );
        }
        if( !sas.isEmpty() ) {
            w.write( "  <" );
            w.write( SUBJECTAREAS_TAG );
            w.write( ">\n" );

            for( SubjectArea current : sas ) {
                w.write( "   <" );
                w.write( SUBJECTAREA_TAG );
                w.write( " ID=\"" );
                w.write( theTypeIdSerializer.toExternalForm( current.getIdentifier()));
                w.write( "\"/>\n" );
            }

            w.write( "  </" );
            w.write( SUBJECTAREAS_TAG );
            w.write( ">\n" );
        }
    }

    /**
     * Write the body of the export file.
     *
     * @param mb the MeshBase
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param w the Writer to write to
     * @throws IOException thrown if an I/O error occurred
     * @throws EncodingException thrown if an Encoding problem occurred
     */
    protected void writeBody(
            MeshBase                       mb,
            MeshObjectIdentifierSerializer idSerializer,
            Writer                         w )
        throws
            IOException,
            EncodingException
    {
        for( MeshObject current : mb ) {
            writeMeshObject( current, idSerializer, w );
        }
    }

    /**
     * Knows how to serialize MeshTypeIdentifiers.
     */
    protected static final MeshTypeIdentifierSerializer theTypeIdSerializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;
}
