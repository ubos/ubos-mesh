//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.io.Serializable;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StringHelper;

/**
 * Collects reusable code fragments for generating and processing ChangeLists.
 */
public abstract class ChangeUtils
{
    /**
     * Static only.
     */
    private ChangeUtils() {}

    /**
     * Append, to the ChangeList, a Change reflecting the creation of the provided MeshObject.
     *
     * @param obj the created MeshObject
     * @param appendHere append the Change to this ChangeList
     */
    public static void appendCreateBare(
            MeshObject obj,
            ChangeList appendHere )
    {
        appendHere.addChange( new MeshObjectCreateChange( obj, obj.getIdentifier() ));
    }

    /**
     * Append, to the ChangeList, a Change reflecting the deletion of the provided MeshObject.
     *
     * @param obj the deleted MeshObject
     * @param appendHere append the Change to this ChangeList
     */
    public static void appendDeleteBare(
            MeshObject obj,
            ChangeList appendHere )
    {
        appendHere.addChange( new MeshObjectDeleteChange( obj, obj.getIdentifier() ));
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the blessings of the provided MeshObject
     * with all the EntityTypes it is currently blessed with.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendBless(
            MeshObject obj,
            ChangeList appendHere )
    {
        EntityType [] types = obj.getEntityTypes();
        if( types.length > 0 ) {
            appendHere.addChange( new MeshObjectBlessChange(
                    obj,
                    EntityType.EMPTY_ARRAY,
                    types,
                    types ));
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the unblessings of the provided MeshObject
     * from all the EntityTypes it is currently blessed with.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendUnbless(
            MeshObject obj,
            ChangeList appendHere )
    {
        EntityType [] types = obj.getEntityTypes();
        if( types.length > 0 ) {
            appendHere.addChange( new MeshObjectUnblessChange(
                    obj,
                    types,
                    types,
                    EntityType.EMPTY_ARRAY ));
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the setting of all non-default PropertyValues
     * on the provided MeshObject.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendSetProperties(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( PropertyType pt : obj.getPropertyTypes() ) {
            PropertyValue value = obj.getPropertyValue( pt );
            if( PropertyValue.compare( pt.getDefaultValue(), value ) != 0 ) {
                appendHere.addChange( new MeshObjectPropertyChange(
                        obj,
                        pt,
                        null,
                        value ));
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the unsetting, back to their default value,
     * of all non-default PropertyValueson the provided MeshObject.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendUnsetProperties(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( PropertyType pt : obj.getPropertyTypes() ) {
            PropertyValue value = obj.getPropertyValue( pt );
            if( PropertyValue.compare( pt.getDefaultValue(), value ) != 0 ) {
                appendHere.addChange( new MeshObjectPropertyChange(
                        obj,
                        pt,
                        value,
                        pt.getDefaultValue() ));
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the creation on the provided MeshObject
     * of all Attributes the MeshObject currently carries.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendCreateAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        String [] attributeNames = obj.getAttributeNames();
        if( attributeNames.length > 0 ) {
            appendHere.addChange( new MeshObjectAttributesAddChange(
                    obj,
                    StringHelper.EMPTY_ARRAY,
                    attributeNames,
                    attributeNames ));
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the removal from the provided MeshObject
     * of all Attributes the MeshObject currently carries.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendRemoveAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        String [] attributeNames = obj.getAttributeNames();
        if( attributeNames.length > 0 ) {
            appendHere.addChange( new MeshObjectAttributesRemoveChange(
                    obj,
                    attributeNames,
                    attributeNames,
                    StringHelper.EMPTY_ARRAY ));
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the setting on the provided MeshObject
     * of all non-null values of the Attributes the MeshObject currently carries.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendSetAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( String attributeName : obj.getAttributeNames() ) {
            Serializable value = obj.getAttributeValue( attributeName );
            if( value != null ) {
                appendHere.addChange( new MeshObjectAttributeChange(
                        obj,
                        attributeName,
                        null,
                        value ));
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the unsetting on the provided MeshObject
     * to null values of all the non-null the Attributes the MeshObject currently carries.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendUnsetAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( String attributeName : obj.getAttributeNames() ) {
            Serializable value = obj.getAttributeValue( attributeName );
            if( value != null ) {
                appendHere.addChange( new MeshObjectAttributeChange(
                        obj,
                        attributeName,
                        value,
                        null ));
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the blessing of Roles on the provided MeshObject
     * with all of its current neighbors, without considering the neighbors provided in neighborsAlreadyConsideredForRoleChanges.
     * (This is to avoid duplicate events from both ends of the relationship)
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     * @param neighborsAlreadyConsideredForRoleChanges keep track of the neighbors whose RoleType changes we have processed already
     */
    public static void appendBlessRoles(
            MeshObject                obj,
            ChangeList                appendHere,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            if( neighborsAlreadyConsideredForRoleChanges == null || !neighborsAlreadyConsideredForRoleChanges.contains( neighbor.getIdentifier() )) {
                RoleType [] rts = obj.getRoleTypes( neighbor );
                if( rts.length > 0 ) {
                    appendHere.addChange( new MeshObjectRoleTypeAddChange(
                            obj,
                            RoleType.EMPTY_ARRAY,
                            rts,
                            rts,
                            neighbor ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the unblessing of Roles from the provided MeshObject
     * with all of its current neighbors, without considering the neighbors provided in neighborsAlreadyConsideredForRoleChanges.
     * (This is to avoid duplicate events from both ends of the relationship)
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     * @param neighborsAlreadyConsideredForRoleChanges keep track of the neighbors whose RoleType changes we have processed already
     */
    public static void appendUnblessRoles(
            MeshObject                obj,
            ChangeList                appendHere,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            if( neighborsAlreadyConsideredForRoleChanges == null || !neighborsAlreadyConsideredForRoleChanges.contains( neighbor.getIdentifier() )) {
                RoleType [] rts = obj.getRoleTypes( neighbor );
                if( rts.length > 0 ) {
                    appendHere.addChange( new MeshObjectRoleTypeRemoveChange(
                            obj,
                            rts,
                            rts,
                            RoleType.EMPTY_ARRAY,
                            neighbor ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the setting of all RoleProperties on the provided MeshObject
     * with all of its current neighbors to non-default values.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendSetRoleProperties(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            for( PropertyType pt : obj.getRolePropertyTypes( neighbor )) {
                PropertyValue value = obj.getRolePropertyValue( neighbor, pt );
                if( PropertyValue.compare( pt.getDefaultValue(), value ) != 0 ) {
                    appendHere.addChange( new MeshObjectRolePropertyChange(
                            obj,
                            pt,
                            pt.getDefaultValue(),
                            value,
                            neighbor ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the unsetting of all RoleProperties from the provided MeshObject
     * with all of its current neighbors that have non-default values.
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendUnsetRoleProperties(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            for( PropertyType pt : obj.getRolePropertyTypes( neighbor )) {
                PropertyValue value = obj.getRolePropertyValue( neighbor, pt );
                if( PropertyValue.compare( pt.getDefaultValue(), value ) != 0 ) {
                    appendHere.addChange( new MeshObjectRolePropertyChange(
                            obj,
                            pt,
                            value,
                            pt.getDefaultValue(),
                            neighbor ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the addition of all RoleAttributes on the provided MeshObject
     * with all of its current neighbors .
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendCreateRoleAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            String [] roleAttributeNames = obj.getRoleAttributeNames( neighbor );
            if( roleAttributeNames.length > 0 ) {
                appendHere.addChange( new MeshObjectRoleAttributesAddChange(
                        obj,
                        StringHelper.EMPTY_ARRAY,
                        roleAttributeNames,
                        roleAttributeNames,
                        neighbor ));
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the removal of all RoleAttributes from the provided MeshObject
     * with all of its current neighbors .
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendRemoveRoleAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            String [] roleAttributeNames = obj.getRoleAttributeNames( neighbor );
            if( roleAttributeNames.length > 0 ) {
                appendHere.addChange( new MeshObjectRoleAttributesRemoveChange(
                        obj,
                        roleAttributeNames,
                        roleAttributeNames,
                        StringHelper.EMPTY_ARRAY,
                        neighbor ));
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the setting of all non-null RoleAttribute values
     * on the provided MeshObject with all of its current neighbors .
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendSetRoleAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            String [] roleAttributeNames = obj.getRoleAttributeNames( neighbor );
            for( String roleAttributeName : roleAttributeNames ) {
                Serializable value = obj.getRoleAttributeValue( neighbor, roleAttributeName );
                if( value != null ) {
                    appendHere.addChange( new MeshObjectRoleAttributeChange(
                            obj,
                            roleAttributeName,
                            null,
                            value,
                            neighbor ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the unsetting of all non-null RoleAttribute values
     * on the provided MeshObject with all of its current neighbors .
     *
     * @param obj the applicable MeshObject
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendUnsetRoleAttributes(
            MeshObject obj,
            ChangeList appendHere )
    {
        for( MeshObject neighbor : obj.traverseToNeighbors() ) {
            String [] roleAttributeNames = obj.getRoleAttributeNames( neighbor );
            for( String roleAttributeName : roleAttributeNames ) {
                Serializable value = obj.getRoleAttributeValue( neighbor, roleAttributeName );
                if( value != null ) {
                    appendHere.addChange( new MeshObjectRoleAttributeChange(
                            obj,
                            roleAttributeName,
                            value,
                            null,
                            neighbor ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the blessing and unblessing of
     * the baseline MeshObject, so it has the same EntityTypes as the comparison MeshObject, as well
     * as setting/unsetting Properties.
     *
     * @param objInBase the MeshObject in the baseline
     * @param objInComp the MeshObject in the comparison
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendDiffEntityTypesProperties(
            MeshObject objInBase,
            MeshObject objInComp,
            ChangeList appendHere )
    {
        ArrayHelper.Difference<EntityType> typeDiff = ArrayHelper.determineDifference(
                objInBase.getEntityTypes(),
                objInComp.getEntityTypes(),
                false,
                EntityType.class );

        EntityType [] hypotheticalAllTypes = ArrayHelper.append( objInBase.getEntityTypes(), typeDiff.getAdditions(), EntityType.class );
        if( typeDiff.getAdditions().length > 0 ) {
            appendHere.addChange( new MeshObjectBlessChange(
                    objInBase,
                    objInBase.getEntityTypes(),
                    typeDiff.getAdditions(),
                    hypotheticalAllTypes ));
        }

        for( PropertyType pt : objInComp.getPropertyTypes() ) {
            if( objInBase.hasProperty( pt )) {
                PropertyValue propertyValueInBase = objInBase.getPropertyValue( pt );
                PropertyValue propertyValueInComp = objInComp.getPropertyValue( pt );

                if( PropertyValue.compare( propertyValueInBase, propertyValueInComp ) != 0 ) {
                    appendHere.addChange( new MeshObjectPropertyChange(
                            objInBase,
                            pt,
                            propertyValueInBase,
                            propertyValueInComp ));
                }
            } else {
                PropertyValue propertyValueInBase = pt.getDefaultValue();
                PropertyValue propertyValueInComp = objInComp.getPropertyValue( pt );

                if( PropertyValue.compare( propertyValueInBase, propertyValueInComp ) != 0 ) {
                    appendHere.addChange( new MeshObjectPropertyChange(
                            objInBase,
                            pt,
                            propertyValueInBase,
                            propertyValueInComp ));
                }
            }
        }
        for( PropertyType pt : objInBase.getPropertyTypes() ) {
            if( !objInComp.hasProperty( pt )) {
                PropertyValue propertyValueInBase = objInBase.getPropertyValue( pt );

                if( PropertyValue.compare( propertyValueInBase, pt.getDefaultValue() ) != 0 ) {
                    // reset to default value
                    appendHere.addChange( new MeshObjectPropertyChange(
                            objInBase,
                            pt,
                            propertyValueInBase,
                            pt.getDefaultValue() ));
                }
            }
        }

        if( typeDiff.getRemovals().length > 0 ) {
            appendHere.addChange( new MeshObjectUnblessChange(
                    objInBase,
                    hypotheticalAllTypes,
                    typeDiff.getRemovals(),
                    ArrayHelper.remove( hypotheticalAllTypes, typeDiff.getRemovals(), false, EntityType.class ) ));
        }

        // nothing to do for same
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting the creation, deletion and
     * setting the values of all Attributes of the baseline MeshObject, so it has the same Attributes
     * and AttributeValues as the comparison MeshObject.
     *
     * @param objInBase the MeshObject in the baseline
     * @param objInComp the MeshObject in the comparison
     * @param appendHere append the Changes to this ChangeList
     */
    public static void appendDiffAttributes(
            MeshObject objInBase,
            MeshObject objInComp,
            ChangeList appendHere )
    {
        // now Attributes
        ArrayHelper.Difference<String> meshObjectAttributeNamesDifference
                = ArrayHelper.determineDifference(
                        objInBase.getAttributeNames(),
                        objInComp.getAttributeNames(),
                        true,
                        String.class );

        if( meshObjectAttributeNamesDifference.getAdditions().length > 0 ) {
            appendHere.addChange( new MeshObjectAttributesAddChange(
                    objInBase,
                    objInBase.getAttributeNames(),
                    meshObjectAttributeNamesDifference.getAdditions(),
                    ArrayHelper.append( objInBase.getAttributeNames(), meshObjectAttributeNamesDifference.getAdditions(), String.class ) ));

            for( String attributeName : meshObjectAttributeNamesDifference.getAdditions() ) {
                Serializable newValue = objInComp.getAttributeValue( attributeName );

                if( newValue != null ) {
                    appendHere.addChange( new MeshObjectAttributeChange(
                            objInBase,
                            attributeName,
                            null,
                            newValue));
                }
            }
        }

        if( meshObjectAttributeNamesDifference.getRemovals().length > 0 ) {
            for( String attributeName : meshObjectAttributeNamesDifference.getRemovals() ) {
                Serializable value = objInBase.getAttributeValue( attributeName );

                if( value != null ) {
                    // set back to null
                    appendHere.addChange( new MeshObjectAttributeChange(
                            objInBase,
                            attributeName,
                            value,
                            null ));
                }
            }
            appendHere.addChange( new MeshObjectAttributesRemoveChange(
                    objInBase,
                    objInBase.getAttributeNames(),
                    meshObjectAttributeNamesDifference.getRemovals(),
                    ArrayHelper.remove( objInBase.getAttributeNames(), meshObjectAttributeNamesDifference.getRemovals(), true, String.class ) ));
        }

        for( String attributeName : meshObjectAttributeNamesDifference.getSame() ) {
            Serializable attributeValueInBase = objInBase.getAttributeValue( attributeName );
            Serializable attributeValueInComp = objInComp.getAttributeValue( attributeName );

            if(    ( attributeValueInBase == null && attributeValueInComp != null )
                || ( attributeValueInBase != null && !attributeValueInBase.equals( attributeValueInComp )))
            {
                appendHere.addChange( new MeshObjectAttributeChange(
                        objInBase,
                        attributeName,
                        attributeValueInBase,
                        attributeValueInComp ));
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting all operations necessary so the
     * base MeshObject has the same neighbor MeshObjects including all RoleTypes, RoleProperty values,
     * Attributes and Attribute values as the comparison MeshObject has with its neighbors.
     *
     * @param objInBase the MeshObject in the baseline
     * @param objInComp the MeshObject in the comparison
     * @param appendHere append the Changes to this ChangeList
     * @param neighborsAlreadyConsideredForRoleChanges keep track of the neighbors whose RoleType changes we have processed already
     */
    public static void appendDiffNeighbors(
            MeshObject                objInBase,
            MeshObject                objInComp,
            ChangeList                appendHere,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        MeshObjectSet neighborsInBase = objInBase.traverseToNeighbors();
        MeshObjectSet neighborsInComp = objInComp.traverseToNeighbors();

        ArrayHelper.Difference<MeshObject> neighborsDiff = ArrayHelper.determineDifference(
                neighborsInBase.getMeshObjects(),
                neighborsInComp.getMeshObjects(),
                true, // compare via identifiers: they are in different MeshBases
                MeshObject.class );

        // added neighbors
        for( MeshObject neighborInComp : neighborsDiff.getAdditions() ) {
            handleAddedNeighbor(
                    objInComp,
                    neighborInComp,
                    appendHere,
                    neighborsAlreadyConsideredForRoleChanges );
        }

        // removed neighbors
        for( MeshObject neighborInBase : neighborsDiff.getRemovals() ) {
            handleRemovedNeighbor(
                    objInBase,
                    neighborInBase,
                    appendHere,
                    neighborsAlreadyConsideredForRoleChanges );
        }

        // same neighbors
        for( MeshObject neighborInBase : neighborsDiff.getSame() ) {
            MeshObject neighborInComp = objInComp.getMeshBaseView().findMeshObjectByIdentifier( neighborInBase.getIdentifier() );

            handleSameNeighbor(
                    objInBase,
                    neighborInBase,
                    objInComp,
                    neighborInComp,
                    appendHere,
                    neighborsAlreadyConsideredForRoleChanges );
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting all operations necessary so the
     * provided MeshObject has a new neighbor MeshObject including adding/setting all RoleTypes, RoleProperty values,
     * Attributes and Attribute values that it has with that neighbor.
     *
     * @param obj the MeshObject
     * @param neighbor the new neighbor MeshObject
     * @param appendHere append the Changes to this ChangeList
     * @param neighborsAlreadyConsideredForRoleChanges keep track of the neighbors whose RoleType changes we have processed already
     */
    static void handleAddedNeighbor(
            MeshObject                obj,
            MeshObject                neighbor,
            ChangeList                appendHere,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        String [] namesInComp = obj.getRoleAttributeNames( neighbor );
        handleAddRoleAttributes(
                obj,
                neighbor,
                namesInComp,
                appendHere );

        if( !neighborsAlreadyConsideredForRoleChanges.contains( neighbor.getIdentifier() )) {
            RoleType [] roleTypesInComp = obj.getRoleTypes( neighbor );
            handleAddRoleTypes(
                    obj,
                    neighbor,
                    RoleType.EMPTY_ARRAY,
                    roleTypesInComp,
                    appendHere );
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting all operations necessary so the
     * provided MeshObject ceases to have this neighbor MeshObject including removal/unsetting of all RoleTypes, RoleProperty values,
     * Attributes and Attribute values that it has with that neighbor.
     *
     * @param obj the MeshObject
     * @param neighbor the new neighbor MeshObject
     * @param appendHere append the Changes to this ChangeList
     * @param neighborsAlreadyConsideredForRoleChanges keep track of the neighbors whose RoleType changes we have processed already
     */
    static void handleRemovedNeighbor(
            MeshObject                objInBase,
            MeshObject                neighborInBase,
            ChangeList                appendHere,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        String [] namesInBase = objInBase.getRoleAttributeNames( neighborInBase );
        handleRemoveRoleAttributes(
                objInBase,
                neighborInBase,
                namesInBase,
                appendHere );

        if( !neighborsAlreadyConsideredForRoleChanges.contains( neighborInBase.getIdentifier() )) {
            RoleType [] roleTypesInBase = objInBase.getRoleTypes( neighborInBase );
            handleRemoveRoleTypes(
                    objInBase,
                    neighborInBase,
                    RoleType.EMPTY_ARRAY,
                    roleTypesInBase,
                    appendHere );
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes reflecting all neighbor-related operations necessary so the
     * base MeshObject with a given neighbor is transformed into the comparison MeshObject with its given neighbor,
     * including setting/unsetting/blessing/unblessing etc all RoleTypes, RoleProperty values, Attributes and Attribute
     * values.
     *
     * @param objInBase the MeshObject in the base
     * @param neighborInBase the neighbor in the base
     * @param objInComp the MeshObject in the comparison
     * @param neighborInComp the neighbor in the comparison
     * @param appendHere append the Changes to this ChangeList
     * @param neighborsAlreadyConsideredForRoleChanges keep track of the neighbors whose RoleType changes we have processed already
     */
    static void handleSameNeighbor(
            MeshObject                objInBase,
            MeshObject                neighborInBase,
            MeshObject                objInComp,
            MeshObject                neighborInComp,
            ChangeList                appendHere,
            Set<MeshObjectIdentifier> neighborsAlreadyConsideredForRoleChanges )
    {
        String [] namesInBase = objInBase.getRoleAttributeNames( neighborInBase );
        String [] namesInComp = objInComp.getRoleAttributeNames( neighborInComp );

        ArrayHelper.Difference<String> roleAttributesDiff = ArrayHelper.determineDifference(
                namesInBase,
                namesInComp,
                true,
                String.class );

        handleAddRoleAttributes(
                objInComp,
                neighborInComp,
                roleAttributesDiff.getAdditions(),
                appendHere );

        handleRemoveRoleAttributes(
                objInBase,
                neighborInBase,
                roleAttributesDiff.getRemovals(),
                appendHere );

        handleChangeRoleAttributes(
                objInBase,
                neighborInBase,
                objInComp,
                neighborInComp,
                roleAttributesDiff.getSame(),
                appendHere );

        RoleType [] roleTypesInBase = objInBase.getRoleTypes( neighborInBase );
        RoleType [] roleTypesInComp = objInComp.getRoleTypes( neighborInComp );

        ArrayHelper.Difference<RoleType> roleTypesDiff = ArrayHelper.determineDifference(
                roleTypesInBase,
                roleTypesInComp,
                false,
                RoleType.class );

        if( !neighborsAlreadyConsideredForRoleChanges.contains( neighborInBase.getIdentifier() )) {

            handleAddRoleTypes(
                    objInComp,
                    neighborInComp,
                    roleTypesInBase,
                    roleTypesDiff.getAdditions(),
                    appendHere );

            handleRemoveRoleTypes(
                    objInBase,
                    neighborInBase,
                    roleTypesInComp,
                    roleTypesDiff.getRemovals(),
                    appendHere );
        }

        handleChangeRoleProperties(
                objInBase,
                neighborInBase,
                objInComp,
                neighborInComp,
                roleTypesDiff.getSame(),
                appendHere );
    }

    /**
     * Append, to the ChangeList, a sequence of Changes adding the named RoleAttributes to the
     * base MeshObject with a given neighbor.
     *
     * @param obj the MeshObject
     * @param neighbor the neighbor MeshObject
     * @param names names of the RoleAttributes that were added
     * @param appendHere append the Changes to this ChangeList
     */
    static void handleAddRoleAttributes(
            MeshObject obj,
            MeshObject neighbor,
            String []  names,
            ChangeList appendHere )
    {
        if( names.length > 0 ) {
            appendHere.addChange( new MeshObjectRoleAttributesAddChange(
                    obj,
                    StringHelper.EMPTY_ARRAY,
                    names,
                    names,
                    neighbor ));

            for( String name : names ) {
                Serializable value = obj.getRoleAttributeValue( neighbor, name );
                if( value != null ) {
                    appendHere.addChange( new MeshObjectRoleAttributeChange(
                            obj,
                            name,
                            null,
                            value,
                            neighbor ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes removing the named RoleAttributes from the
     * base MeshObject with a given neighbor.
     *
     * @param obj the MeshObject
     * @param neighbor the neighbor MeshObject
     * @param names names of the RoleAttributes that were removed
     * @param appendHere append the Changes to this ChangeList
     */
    static void handleRemoveRoleAttributes(
            MeshObject obj,
            MeshObject neighbor,
            String []  names,
            ChangeList appendHere )
    {
        if( names.length > 0 ) {
            for( String name : names ) {
                Serializable value = obj.getRoleAttributeValue( neighbor, name );
                if( value != null ) {
                    appendHere.addChange( new MeshObjectRoleAttributeChange(
                            obj,
                            name,
                            value,
                            null,
                            neighbor ));
                }
            }
            appendHere.addChange( new MeshObjectRoleAttributesRemoveChange(
                    obj,
                    names,
                    names,
                    StringHelper.EMPTY_ARRAY,
                    neighbor ));

        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes setting the values of the named RoleAttributes on the
     * relationship from the base MeshObject to a given neighbor to the values they have in the comparison.
     *
     * @param objInBase MeshObject the object in question in the base MeshBase
     * @param neighborInBase the neighbor of the object in question in the base MeshBase
     * @param objInComp MeshObject the object in question in the comparison MeshBase
     * @param neighborInComp the neighbor of the object in question in the comparison MeshBase
     * @param names names of the RoleAttributes that were potentially changed
     * @param appendHere append the Changes to this ChangeList
     */
    static void handleChangeRoleAttributes(
            MeshObject objInBase,
            MeshObject neighborInBase,
            MeshObject objInComp,
            MeshObject neighborInComp,
            String []  names,
            ChangeList appendHere )
    {
        if( names.length > 0 ) {
            for( String name : names ) {
                Serializable valueInBase = objInBase.getRoleAttributeValue( neighborInBase, name );
                Serializable valueInComp = objInComp.getRoleAttributeValue( neighborInComp, name );

                if(    ( valueInBase == null && valueInComp != null )
                    || ( valueInBase != null && !valueInBase.equals( valueInComp )))
                {
                    appendHere.addChange( new MeshObjectRoleAttributeChange(
                            objInBase,
                            name,
                            valueInBase,
                            valueInComp,
                            neighborInBase ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes adding the provided RoleTypes on the
     * relationship from the base MeshObject to the given neighbor, assuming a set of
     * RoleTypes at the beginning.
     *
     * @param obj MeshObject the object in question
     * @param neighbor the neighbor of the object in question
     * @param startingRoleTypes the RoleTypes before any were added
     * @param addedRoleTypes the RoleTypes that were added
     * @param appendHere append the Changes to this ChangeList
     */
    static void handleAddRoleTypes(
            MeshObject  obj,
            MeshObject  neighbor,
            RoleType [] startingRoleTypes,
            RoleType [] addedRoleTypes,
            ChangeList  appendHere )
    {
        if( addedRoleTypes.length > 0 ) {
            MeshObjectRoleTypeAddChange addEvent = new MeshObjectRoleTypeAddChange(
                    obj,
                    startingRoleTypes,
                    addedRoleTypes,
                    ArrayHelper.append( startingRoleTypes, addedRoleTypes, RoleType.class ),
                    neighbor );
            appendHere.addChange( addEvent );

            PropertyType [] propertyTypes = obj.getRolePropertyTypes( neighbor );
            for( PropertyType propertyType : propertyTypes ) {
                PropertyValue newValue = obj.getRolePropertyValue( neighbor, propertyType );

                if( PropertyValue.compare( newValue, propertyType.getDefaultValue()) != 0 ) {
                    appendHere.addChange( new MeshObjectRolePropertyChange(
                            obj,
                            propertyType,
                            propertyType.getDefaultValue(),
                            newValue,
                            neighbor ));
                }
            }

            PropertyType [] neighborPropertyTypes = neighbor.getRolePropertyTypes( obj );
            for( PropertyType neighborPropertyType : neighborPropertyTypes ) {
                PropertyValue newValue = neighbor.getRolePropertyValue( obj, neighborPropertyType );

                if( PropertyValue.compare( newValue, neighborPropertyType.getDefaultValue()) != 0 ) {
                    appendHere.addChange( new MeshObjectRolePropertyChange(
                            neighbor,
                            neighborPropertyType,
                            neighborPropertyType.getDefaultValue(),
                            newValue,
                            obj ));
                }
            }
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes removing the provided RoleTypes on the
     * relationship from the base MeshObject to the given neighbor, assuming a set of
     * RoleTypes at the end.
     *
     * @param obj MeshObject the object in question
     * @param neighbor the neighbor of the object in question
     * @param endingRoleTypes the RoleTypes that remained after the removal
     * @param removedRoleTypes the RoleTypes that were removed
     * @param appendHere append the Changes to this ChangeList
     */
    static void handleRemoveRoleTypes(
            MeshObject  obj,
            MeshObject  neighbor,
            RoleType [] endingRoleTypes,
            RoleType [] removedRoleTypes,
            ChangeList  appendHere )
    {
        if( removedRoleTypes.length > 0 ) {
            PropertyType [] propertyTypes = obj.getRolePropertyTypes( neighbor );
            for( PropertyType propertyType : propertyTypes ) {
                PropertyValue newValue = obj.getRolePropertyValue( neighbor, propertyType );

                if( PropertyValue.compare( newValue, propertyType.getDefaultValue()) != 0 ) {
                    appendHere.addChange( new MeshObjectRolePropertyChange(
                            obj,
                            propertyType,
                            newValue,
                            propertyType.getDefaultValue(),
                            neighbor ));
                }
            }

            PropertyType [] neighborPropertyTypes = neighbor.getRolePropertyTypes( obj );
            for( PropertyType neighborPropertyType : neighborPropertyTypes ) {
                PropertyValue newValue = neighbor.getRolePropertyValue( obj, neighborPropertyType );

                if( PropertyValue.compare( newValue, neighborPropertyType.getDefaultValue()) != 0 ) {
                    appendHere.addChange( new MeshObjectRolePropertyChange(
                            neighbor,
                            neighborPropertyType,
                            newValue,
                            neighborPropertyType.getDefaultValue(),
                            obj ));
                }
            }

            MeshObjectRoleTypeRemoveChange removeEvent = new MeshObjectRoleTypeRemoveChange(
                    obj,
                    ArrayHelper.append( endingRoleTypes, removedRoleTypes, RoleType.class ),
                    removedRoleTypes,
                    endingRoleTypes,
                    neighbor );

            appendHere.addChange( removeEvent );
        }
    }

    /**
     * Append, to the ChangeList, a sequence of Changes setting the values of the RoleProperties defined
     * by the provided ToleTypes on the base MeshObject to the given neighbor to the values they have in the comparison.
     *
     * @param objInBase MeshObject the object in question in the base MeshBase
     * @param neighborInBase the neighbor of the object in question in the base MeshBase
     * @param objInComp MeshObject the object in question in the comparison MeshBase
     * @param neighborInComp the neighbor of the object in question in the comparison MeshBase
     * @param roleTypes the RoleTypes whose RoleProperties were potentially changed
     * @param appendHere append the Changes to this ChangeList
     */
    static void handleChangeRoleProperties(
            MeshObject  objInBase,
            MeshObject  neighborInBase,
            MeshObject  objInComp,
            MeshObject  neighborInComp,
            RoleType [] roleTypes,
            ChangeList  appendHere )
    {
        if( roleTypes.length > 0 ) {
            for( RoleType roleType : roleTypes ) {
                PropertyType [] rolePropertyTypes = roleType.getPropertyTypes();

                for( PropertyType rolePropertyType : rolePropertyTypes ) {
                    PropertyValue valueInBase = objInBase.getRolePropertyValue( neighborInBase, rolePropertyType );
                    PropertyValue valueInComp = objInComp.getRolePropertyValue( neighborInComp, rolePropertyType );

                    if(    ( valueInBase == null && valueInComp != null )
                        || ( valueInBase != null && !valueInBase.equals( valueInComp )))
                    {
                        appendHere.addChange( new MeshObjectRolePropertyChange(
                                objInBase,
                                rolePropertyType,
                                valueInBase,
                                valueInComp,
                                neighborInBase ));
                    }
                }
            }
        }
    }
}
