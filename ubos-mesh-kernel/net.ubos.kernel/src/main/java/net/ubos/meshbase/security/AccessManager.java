//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.security;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;

/**
 * <p>Manages access control rights to MeshObjects, aspects of MeshObjects, and relationships in a MeshBase.
 * Implementations typically use ThreadIdentityManager to determine the current caller.
 */
public interface AccessManager
{
    /**
     * Assign the second MeshObject to be the owner of the first MeshObject. This
     * must only be called if the current Thread has an open Transaction.
     *
     * @param toBeOwned the MeshObject to be owned by the new owner
     * @param newOwner the MeshObject that is the new owner.
     * @throws NotPermittedException thrown if it is not permitted
     * @throws TransactionException thrown if this is invoked outside of proper transaction boundaries
     */
    public void assignOwner(
            MeshObject toBeOwned,
            MeshObject newOwner )
        throws
            NotPermittedException,
            TransactionException;

    /**
     * Check whether it is permitted to create a MeshObject with this MeshObjectIdentifier.
     *
     * @param mbv the MeshBase in which the MeshObject is supposed to be created
     * @param identifier the MeshObjectIdentifier
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedCreate(
            MeshBaseView         mbv,
            MeshObjectIdentifier identifier )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to access this MeshObject. If it is not permitted, it appears to the caller
     * as if the MeshObject did not exist.
     *
     * @param obj the MeshObject to be accessed
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedAccess(
            MeshObject obj )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to create this Attribute.
     *
     * @param obj the MeshObject that will carry the Attribute
     * @param name the name of the Attribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedCreateAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to read this Attribute.
     *
     * @param obj the MeshObject that may carry the Attribute
     * @param name the name of the Attribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedGetAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to write this Attribute.
     *
     * @param obj the MeshObject that will carry the Attribute
     * @param name the name of the Attribute
     * @param newValue the new value of the Attribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedSetAttribute(
            MeshObject   obj,
            String       name,
            Serializable newValue )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to delete this Attribute.
     *
     * @param obj the MeshObject that carries the Attribute
     * @param name the name of the Attribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedDeleteAttribute(
            MeshObject obj,
            String     name )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to determine whether or not a MeshObject is blessed with
     * the given type. If the caller asks for all EntityTypes of a MeshObject, the set will be
     * filtered to those EntityTypes permitted by this method.
     *
     * @param obj the MeshObject
     * @param type the EntityType whose blessing we wish to check
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedBlessedBy(
            MeshObject obj,
            EntityType type )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to bless a MeshObject with the given EntityTypes.
     *
     * @param obj the MeshObject
     * @param types the EntityTypes with which to bless
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedBless(
            MeshObject    obj,
            EntityType [] types )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to unbless a MeshObject from the given EntityTypes.
     *
     * @param obj the MeshObject
     * @param types the EntityTypes from which to unbless
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedUnbless(
            MeshObject    obj,
            EntityType [] types )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to obtain the value of MeshObject's given property.
     *
     * @param obj the MeshObject
     * @param pt the PropertyType identifying the property to be read
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedGetProperty(
            MeshObject   obj,
            PropertyType pt )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to set a MeshObject's given property to the given
     * value.
     *
     * @param obj the MeshObject
     * @param pt the PropertyType identifying the property to be modified
     * @param newValue the proposed new value for the property
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedSetProperty(
            MeshObject    obj,
            PropertyType  pt,
            PropertyValue newValue )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to create this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighborId the identifier of the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedCreateRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to create this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedCreateRoleAttribute(
            MeshObject obj,
            MeshObject neighbor,
            String     name )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to read this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighborId the identifier of the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedGetRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to read this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedGetRoleAttribute(
            MeshObject obj,
            MeshObject neighbor,
            String     name )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to write this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighborId the identifier of the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @param newValue the new value of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedSetRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name,
            Serializable         newValue )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to write this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @param newValue the new value of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedSetRoleAttribute(
            MeshObject   obj,
            MeshObject   neighbor,
            String       name,
            Serializable newValue )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to delete this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighborId the identifier of the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedDeleteRoleAttribute(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            String               name )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to delete this RoleAttribute.
     *
     * @param obj the MeshObject on whose side of the Relationship the RoleAttribute is located
     * @param neighbor the neighbor MeshObject
     * @param name the name of the RoleAttribute
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedDeleteRoleAttribute(
            MeshObject obj,
            MeshObject neighbor,
            String     name )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to bless the relationship to the neighbor with the
     * provided RoleTypes.
     *
     * @param obj the MeshObject
     * @param thisEnds the RoleTypes to bless the relationship with
     * @param neighborId the identifier of the neighbor MeshObject
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedBless(
            MeshObject           obj,
            RoleType []          thisEnds,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to bless the relationship to the neighbor with the
     * provided RoleTypes.
     *
     * @param obj the MeshObject
     * @param thisEnds the RoleTypes to bless the relationship with
     * @param neighbor neighbor to which this MeshObject is related, if it could be resolved
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedBless(
            MeshObject  obj,
            RoleType [] thisEnds,
            MeshObject  neighbor )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to unbless the relationship to the neighbor from the
     * provided RoleTypes.
     *
     * @param obj the MeshObject
     * @param thisEnds the RoleTypes to unbless the relationship from
     * @param neighborId the identifier of the neighbor MeshObject
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedUnbless(
            MeshObject           obj,
            RoleType []          thisEnds,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to unbless the relationship to the neighbor from the
     * provided RoleTypes.
     *
     * @param obj the MeshObject
     * @param thisEnds the RoleTypes to unbless the relationship from
     * @param neighbor neighbor to which this MeshObject is related, if it could be resolved
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedUnbless(
            MeshObject  obj,
            RoleType [] thisEnds,
            MeshObject  neighbor )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to traverse the given RoleType from this MeshObject to the
     * given MeshObject.
     *
     * @param obj the MeshObject
     * @param toTraverse the RoleType to traverse
     * @param neighborId the identifier of the neighbor MeshObject
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedTraverse(
            MeshObject           obj,
            RoleType             toTraverse,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to traverse the given RoleType from this MeshObject to the
     * given MeshObject.
     *
     * @param obj the MeshObject
     * @param toTraverse the RoleType to traverse
     * @param neighbor neighbor to which this traversal leads
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedTraverse(
            MeshObject obj,
            RoleType   toTraverse,
            MeshObject neighbor )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to obtain this pair of MeshObjects' given role property. Subclasses
     * may override this.
     *
     * @param obj the start MeshObject
     * @param neighborId the identifier of the neighbor MeshObject
     * @param pt the PropertyType identifying the property to be read
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedGetRoleProperty(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            PropertyType         pt )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to obtain this pair of MeshObjects' given role property. Subclasses
     * may override this.
     *
     * @param obj the start MeshObject
     * @param neighbor the neighbor MeshObject
     * @param pt the PropertyType identifying the property to be read
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedGetRoleProperty(
            MeshObject   obj,
            MeshObject   neighbor,
            PropertyType pt )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to set this pair of MeshObjects' given role property to the given
     * value. Subclasses may override this.
     *
     * @param obj the start MeshObject
     * @param neighborId the identifier of the neighbor MeshObject
     * @param pt the PropertyType identifying the property to be modified
     * @param newValue the proposed new value for the property
     * @throws NotPermittedException thrown if it is not permitted
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedSetRoleProperty(
            MeshObject           obj,
            MeshObjectIdentifier neighborId,
            PropertyType         pt,
            PropertyValue        newValue )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted to set this pair of MeshObjects' given role property to the given
     * value. Subclasses may override this.
     *
     * @param obj the start MeshObject
     * @param neighbor the neighbor MeshObject
     * @param pt the PropertyType identifying the property to be modified
     * @param newValue the proposed new value for the property
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedSetRoleProperty(
            MeshObject    obj,
            MeshObject    neighbor,
            PropertyType  pt,
            PropertyValue newValue )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted for the caller to know that the two MeshObjects are related.
     *
     * Note: the check should be consistent with the permissions on RoleType and RoleAttribute-based
     * traversal. In other words, if the caller isn't permitted to know any of the RoleTypes between
     * the two MeshObjects, and none of the RoleAttributes either, they should not know that the
     * two MeshObjects are related either. So this could be implemented in exactly that fashion, but
     * that would make the implementation rather expensive, and so we factor it out.
     *
     * @param obj the start MeshObject
     * @param neighborId the identifier of the neighbor MeshObject
     * @throws NeedNeighborException thrown if the neighbor must be resolved and provided to be able to answer this question
     */
    public void checkPermittedIsRelated(
            MeshObject           obj,
            MeshObjectIdentifier neighborId )
        throws
            NotPermittedException,
            NeedNeighborException;

    /**
     * Check whether it is permitted for the caller to know that the two MeshObjects are related.
     *
     * Note: the check should be consistent with the permissions on RoleType and RoleAttribute-based
     * traversal. In other words, if the caller isn't permitted to know any of the RoleTypes between
     * the two MeshObjects, and none of the RoleAttributes either, they should not know that the
     * two MeshObjects are related either. So this could be implemented in exactly that fashion, but
     * that would make the implementation rather expensive, and so we factor it out.
     *
     * @param obj the start MeshObject
     * @param neighbor the neighbor MeshObject
     */
    public void checkPermittedIsRelated(
            MeshObject obj,
            MeshObject neighbor )
        throws
            NotPermittedException;

    /**
     * Check whether it is permitted to delete this MeshObject. This checks both whether the
     * MeshObject itself may be deleted, and whether the relationships it participates in may
     * be deleted (which in turn depends on whether the relationships may be unblessed).
     *
     * @param obj the MeshObject
     * @throws NotPermittedException thrown if it is not permitted
     */
    public void checkPermittedDelete(
            MeshObject obj )
        throws
            NotPermittedException;
}
