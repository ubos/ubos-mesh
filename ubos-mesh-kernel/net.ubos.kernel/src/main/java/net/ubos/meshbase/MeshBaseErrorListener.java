//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase;

/**
 * Listener to errors encountered by a MeshBase
 */
public interface MeshBaseErrorListener
{
    /**
     * The identifier of an EntityType was encountered when reading a MeshObject
     * from disk that could not be resolved.
     *
     * @param error the MeshBaseError
     */
    public void unresolveableEntityType(
            MeshBaseError.UnresolvableEntityType error );

    /**
     * The identifier of an RoleType was encountered when reading a MeshObject
     * from disk that could not be resolved.
     *
     * @param error the MeshBaseError
     */
    public void unresolveableRoleType(
            MeshBaseError.UnresolvableRoleType error );

    /**
     * The identifier of an PropertyType was encountered when reading a MeshObject
     * from disk that could not be resolved.
     *
     * @param error the MeshBaseError
     */
    public void unresolveablePropertyType(
            MeshBaseError.UnresolvablePropertyType error );

    /**
     * A PropertyValue did not match the DataType of the Property to which it was
     * supposed to be assigned when reading a MeshObject from disk.
     *
     * @param error the MeshBaseError
     */
    public void incompatibleDataType(
            MeshBaseError.IncompatibleDataType error );

    /**
     * A null value was found on disk for a Property that was not optional.
     *
     * @param error the MeshBaseError
     */
    public void propertyNotOptional(
            MeshBaseError.PropertyNotOptional error );

    /**
     * Some other uncategorized error occurred.
     *
     * @param error the MeshBaseError
     */
    public void otherError(
            MeshBaseError.OtherError error );
}
