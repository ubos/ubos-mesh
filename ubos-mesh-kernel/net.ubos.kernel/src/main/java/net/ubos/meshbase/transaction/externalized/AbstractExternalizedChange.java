//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction.externalized;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.RoleType;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;

/**
 *
 */
public abstract class AbstractExternalizedChange
    implements
        ExternalizedChange
{
    /**
     * Constructor.
     *
     * @param sourceIdentifier MeshObjectIdentifier of the primary affected MeshObject
     */
    public AbstractExternalizedChange(
            MeshObjectIdentifier sourceIdentifier )
    {
        if( sourceIdentifier == null ) {
            throw new NullPointerException( "Source MeshObjectIdentifier must be provided" );
        }
        theSourceIdentifier = sourceIdentifier;
    }

    /**
     * Obtain the identifier of the primary affected MeshObject
     *
     * @return the identifier
     */
    public MeshObjectIdentifier getSourceIdentifier()
    {
        return theSourceIdentifier;
    }

    /**
     * Helper method to resolve an array of MeshTypeIdentifier into an array of EntityType.
     *
     * @param ids the MeshTypeIdentifiers
     * @return the EntityTypes identified by the MeshTypeIdentifiers
     * @throws MeshTypeNotFoundException thrown if a MeshType could not be found
     */
    protected EntityType [] findEntityTypes(
            MeshTypeIdentifier [] ids )
        throws
            MeshTypeNotFoundException
    {
        EntityType [] ret = new EntityType[ ids.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = ModelBase.SINGLETON.findEntityType( ids[i] );
        }
        return ret;
    }

    /**
     * Helper method to resolve an array of MeshTypeIdentifier into an array of RoleType.
     *
     * @param ids the MeshTypeIdentifiers
     * @return the RoleType identified by the MeshTypeIdentifiers
     * @throws MeshTypeNotFoundException thrown if a MeshType could not be found
     */
    protected RoleType [] findRoleTypes(
            MeshTypeIdentifier [] ids )
        throws
            MeshTypeNotFoundException
    {
        RoleType [] ret = new RoleType[ ids.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = ModelBase.SINGLETON.findRoleType( ids[i] );
        }
        return ret;
    }

    /**
     * MeshObjectIdentifier of the primary affected MeshObject.
     */
    protected final MeshObjectIdentifier theSourceIdentifier;
}
