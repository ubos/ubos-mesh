//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectGraphModificationException;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.transaction.externalized.ExternalizedMeshObjectPropertyChange;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.event.PropertyChange;

/**
  * <p>This event indicates that one of a MeshObject's properties has changed its value
  */
public class MeshObjectPropertyChange
        extends
            PropertyChange<MeshObject,PropertyType,PropertyValue>
        implements
            Change
{
    /**
     * Constructor.
     *
     * @param source the object that is the source of the event
     * @param property an object representing the property of the event
     * @param oldValue the old value of the property, prior to the event
     * @param newValue the new value of the property, after the event
     */
    public MeshObjectPropertyChange(
            MeshObject    source,
            PropertyType  property,
            PropertyValue oldValue,
            PropertyValue newValue )
    {
        super(  source,
                property,
                oldValue,
                newValue,
                newValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getSourceMeshObjectIdentifier()
    {
        return getSource().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject [] getAffectedMeshObjects()
    {
        return new MeshObject[] { getSource() };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject applyTo(
            EditableMeshBaseView base )
        throws
            CannotApplyChangeException,
            MeshObjectGraphModificationException,
            TransactionException
    {
        MeshObject    otherObject      = base.findMeshObjectByIdentifier( getSource().getIdentifier() );
        PropertyType  affectedProperty = getProperty();
        PropertyValue newValue         = getNewValue();

        otherObject.setPropertyValue(
                affectedProperty,
                newValue );

        return otherObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectPropertyChange createButFrom(
            MeshBaseView mbv )
    {
        MeshObject updatedMeshObject = mbv.findMeshObjectByIdentifier( getSource().getIdentifier() );

        PropertyType pt = getProperty();

        return new MeshObjectPropertyChange(
                updatedMeshObject,
                pt,
                updatedMeshObject.getPropertyValue( pt ),
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectPropertyChange inverse()
    {
        return new MeshObjectPropertyChange(
                getSource(),
                getProperty(),
                getNewValue(),
                getOldValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInverse(
            Change candidate )
    {
        if( !( candidate instanceof MeshObjectPropertyChange )) {
            return false;
        }
        MeshObjectPropertyChange realCandidate = (MeshObjectPropertyChange) candidate;

        if( theSource != realCandidate.theSource ) {
            return false;
        }

        if( theProperty != realCandidate.theProperty ) {
            return false;
        }

        Object one = getOldValue();
        Object two = realCandidate.getNewValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        one = getNewValue();
        two = realCandidate.getOldValue();

        if( one == null ) {
            if( two != null ) {
                return false;
            }
        } else if( !one.equals( two )) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof MeshObjectPropertyChange )) {
            return false;
        }
        MeshObjectPropertyChange realOther = (MeshObjectPropertyChange) other;

        if( !getSource().equals( realOther.getSource() )) {
            return false;
        }
        if( theProperty != realOther.theProperty ) {
            return false;
        }
        if( !ArrayHelper.equals( theNewValue, realOther.theNewValue )) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(
                theSource.getIdentifier(),
                theProperty.getIdentifier(),
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalizedMeshObjectPropertyChange asExternalized()
    {
        return new ExternalizedMeshObjectPropertyChange(
                getSource().getIdentifier(),
                getProperty().getIdentifier(),
                getOldValue(),
                getNewValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReferencedSubjectAreasTo(
            Set<SubjectArea> sas )
    {
        sas.add( theProperty.getSubjectArea() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectPropertyChange withAlternateAffectedMeshObjects(
            Map<MeshObjectIdentifier,MeshObject> translation )
    {
        return new MeshObjectPropertyChange(
                translation.get( theSource.getIdentifier() ),
                theProperty,
                theOldValue,
                theNewValue );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void normalizePreviousInChangeList(
            ChangeList.SubjectChangeList list,
            int                          index )
    {
        for( int i=index-1 ; i>=0 ; --i ) {
            ChangeList.ChangeWithIndex currentChangeWithIndex = list.get( i );
            if( currentChangeWithIndex == null ) {
                continue;
            }
            Change currentChange = currentChangeWithIndex.theChange;

            if( !( currentChange instanceof MeshObjectPropertyChange )) {
                continue;
            }
            MeshObjectPropertyChange realCurrentChange = (MeshObjectPropertyChange) currentChange;
            if( !theProperty.equals( realCurrentChange.getProperty())) {
                continue;
            }

            if( Objects.equals( realCurrentChange.theOldValue, theNewValue )) {
                // can eliminate both
                list.blankChangeAt( i );
                list.blankChangeAt( index );

            } else {
                // can simplify -- we blank ourselves and replace the other
                Change newCurrentChange = new MeshObjectPropertyChange( theSource, theProperty, realCurrentChange.theOldValue, theNewValue );

                list.replaceChangeAt( i, newCurrentChange);
                list.blankChangeAt( index );
            }
            break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        ret.append( getClass().getSimpleName() );
        ret.append( ": " );
        ret.append( theSource.getIdentifier() );
        ret.append( ": " );
        ret.append( theProperty.getIdentifier() );
        ret.append( " = " );
        ret.append( theOldValue );
        ret.append( " >> " );
        ret.append( theNewValue );
        return ret.toString();
    }
}
