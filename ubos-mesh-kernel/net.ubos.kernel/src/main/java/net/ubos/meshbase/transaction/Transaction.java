//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.transaction;

import net.ubos.meshbase.EditableMeshBaseView;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.meshbase.history.EditableHistoricMeshBaseView;

/**
 * <p>The concept of a Transaction on a MeshBase. This is an abstract class;
 *    specific implementations of MeshBase provide concrete subclasses.
 * <p>Transactions can either be HeadTransactions (modifying the "head" version of the graph) or
 *    HistoryTransactions (modifying some aspect of the history of the graph)
 */
public interface Transaction
{
    /**
     * Obtain the MeshBase to which this Transaction belongs.
     *
     * @return the MeshBase
     */
    public MeshBase getMeshBase();

    /**
     * Obtain the MeshBaseView to which this Transaction belongs. For a HeadTransaction,
     * this is the same as getMeshBase(); for a HistoryTransaction, this is the historic MeshBaseView.
     *
     * @return the MeshBaseView
     */
    public MeshBaseView getMeshBaseView();

    /**
      * Obtain the set of Changes that have been made as part of this
      * Transaction so far. Depending on the state of the Transaction,
      * the ChangeList might be complete, frozen or not.
      *
      * @return the set of Changes up to this point
      */
    public ChangeList getChangeList();

    /**
     * Determine whether the calling Thread, and the provided MeshBaseView, are
     * compatible with this Transaction.
     *
     * @param mbv the MeshBaseView the Transaction is supposed to be applied to
     * @throws IllegalTransactionThreadException if the calling Thread is not
     *         compatible with this Transaction
     * @throws WrongMeshBaseViewException if the MeshBaseView is not compatible
     *         with this Transaction
     */
    public void checkTransaction(
            EditableMeshBaseView mbv )
        throws
            IllegalTransactionThreadException,
            WrongMeshBaseViewException;

    /**
     * Determine whether the calling Thread, and the provided MeshBase, are
     * compatible with this Transaction.
     *
     * @param mbv the MeshBase the Transaction is supposed to be applied to
     * @throws IllegalTransactionThreadException if the calling Thread is not
     *         compatible with this Transaction
     * @throws WrongMeshBaseViewException if the MeshBaseView is not compatible
     *         with this Transaction
     */
    public void checkTransaction(
            EditableHistoricMeshBaseView mbv )
        throws
            IllegalTransactionThreadException,
            WrongMeshBaseViewException;

    /**
     * Determine whether the passed-in Thread owns this Transaction.
     *
     * @param t the Thread to test
     * @return true if t owns this Transaction
     */
    public boolean owns(
            Thread t );

    /**
      * Obtain the current status of this Transaction.
      *
      * @return the current status of this Transaction
      */
    public Status getStatus();

    /**
     * Obtain the wall-clock time when the Transaction was started. This is actual wall-clock time
     * when the API was invoked.
     *
     * @return the time the Transaction was started
     */
    public long getTimeStarted();

    /**
     * Obtain the wall-clock time when the Transaction was committed or rolled back. This is actual
     * wall-clock time when the API was invoked.
     *
     * @return the time the Transaction was committed or rolled back
     */
    public long getTimeEnded();

    /**
     * Obtain the effective time when the Transaction was committed that will be used to set
     * timestamps on the affected MeshObjects. This is the same as timeEnded for HeadTransactions,
     * but in the past for HistoryTransactions.
     *
     * @return the time
     */
    public long getEffectiveCommitTime();

    /**
     * Defines the values for a transaction status. FIXME: Not all of these values
     * have been implemented so far.
     */
    public static enum Status
    {
        /**
         * Indicates that this Transaction has been started but not voted on.
         */
        TRANSACTION_STARTED,

        /**
         * Indicates that this Transaction has been committed.
         */
        TRANSACTION_COMMITTED,

        /**
         * Indicates that this Transaction has been rolled back.
         */
        TRANSACTION_ROLLEDBACK,

        /**
         * Indicates that this HistoryTransaction is currently in its "ripple forward" state.
         */
        HISTORY_TRANSACTION_RIPPLING_FORWARD
    }
}
