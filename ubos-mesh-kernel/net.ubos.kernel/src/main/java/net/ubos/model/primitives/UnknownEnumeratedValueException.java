//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * Thrown if a non-existing EnumeratedValue within an EnumeratedDataType is selected.
 */
public abstract class UnknownEnumeratedValueException
        extends
            NotInDomainException
{
    /**
     * Private constructor for subclasses only.
     *
     * @param type the EnumeratedDataType in which it could not be found
     */
    protected UnknownEnumeratedValueException(
            EnumeratedDataType type )
    {
        super( type );
    }

    /**
     * Subclass that indicates the key could not be found.
     */
    public static class Key
            extends
                UnknownEnumeratedValueException
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
         * Constructor.
         *
         * @param type the EnumeratedDataType in which it could not be found
         * @param key the key that could not be found
         */
        public Key(
                EnumeratedDataType type,
                String             key )
        {
            super( type );

            theKey = key;
        }

        /**
         * Obtain the key that was not found.
         *
         * @return the key
         */
        public String getKey()
        {
            return theKey;
        }

        /**
         * Obtain resource parameters for the internationalization.
         *
         * @return the resource parameters
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[] { theType, theKey };
        }

        /**
         * The key that was not found.
         */
        protected String theKey;
    }

    /**
     * Subclass that indicates that the user-visible name could not be found.
     */
    public static class UserVisible
            extends
                UnknownEnumeratedValueException
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
         * Constructor.
         *
         * @param type the EnumeratedDataType in which it could not be found
         * @param userVisible the user-visible name that could not be found
         */
        public UserVisible(
                EnumeratedDataType type,
                String             userVisible )
        {
            super( type );

            theUserVisibleName = userVisible;
        }

        /**
         * Obtain the user-visible name that was not found.
         *
         * @return the user-visible name
         */
        public String getUserVisibleName()
        {
            return theUserVisibleName;
        }

        /**
         * Obtain resource parameters for the internationalization.
         *
         * @return the resource parameters
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[] { theType, theUserVisibleName };
        }

        /**
         * The user-visible name that was not found.
         */
        protected String theUserVisibleName;
    }
}
