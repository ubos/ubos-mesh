//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import net.ubos.util.logging.Dumper;

/**
  * A floating-point number DataType for PropertyValues. 
  * Explicit minimum and maximum values may be specified. Minimum and
  * maximum are included in the domain. It can also carry a unit.
  *
  * FIXME: units not tested.
  */
public class FloatDataType
        extends
            DataType
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * This is the default instance of this class. It represents
     * a float data type with no domain restriction.
     */
    public static final FloatDataType theDefault
            = new FloatDataType(
                    FloatValue.create( Float.NEGATIVE_INFINITY ),
                    FloatValue.create( Float.POSITIVE_INFINITY ),
                    null,
                    null );

    /**
     * This is another default instance of this class. It represents
     * a float data type which can only be positive or zero.
     */
    public static final FloatDataType thePositiveDefault
            = new FloatDataType(
                    FloatValue.create( 0.f ),
                    FloatValue.create( Float.POSITIVE_INFINITY ),
                    null,
                    theDefault );

    /**
     * Construct one with minimum and maximum values (inclusive).
     * Use constants defined in java.lang.Float to represent infinity.
     *
     * @param min the smallest allowed value (may be negative infinity)
     * @param max the largest allowed value (may be infinity)
     * @param superType the DataType that we refine, if any
     * @return the created FloatDataType
     */
    public static FloatDataType create(
            FloatValue min,
            FloatValue max,
            DataType   superType )
    {
        return new FloatDataType( min, max, null, superType );
    }

     /**
      * Construct one with minimum and maximum values (inclusive) and a unit family.
      * This allows one to specify that a certain meta-attribute can only hold
      * values with a specific unit, eg a length.
      * Use constants defined in java.lang.Float to represent infinity.
      *
      * @param min the smallest allowed value (may be negative infinity)
      * @param max the largest allowed value (may be infinity)
      * @param u the UnitFamily, if any
      * @param superType the DataType that we refine, if any
      * @return the created FloatDataType
      */
    public static FloatDataType create(
            FloatValue min,
            FloatValue max,
            UnitFamily u,
            DataType   superType )
    {
        return new FloatDataType( min, max, u, superType );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param min the smallest allowed value (may be negative infinity)
     * @param max the largest allowed value (may be infinity)
     * @param u the UnitFamily, if any
     * @param superType the DataType that we refine, if any
     */
    private FloatDataType(
            FloatValue min,
            FloatValue max,
            UnitFamily u,
            DataType   superType )
    {
        super( superType );

        this.theMin = min;
        this.theMax = max;

        this.theUnitFamily = u;

        if( theUnitFamily == null ) {
            if( theMin.getUnit() != null ) {
                throw new IllegalArgumentException( "Min value has unit, data type has none" );
            }
            if( theMax.getUnit() != null ) {
                throw new IllegalArgumentException( "Max value has unit, data type has none" );
            }
        } else {
            if( ! theMin.getUnit().getFamily().equals( theUnitFamily )) {
                throw new IllegalArgumentException( "Min value has wrong unit family" );
            }
            if( ! theMax.getUnit().getFamily().equals( theUnitFamily )) {
                throw new IllegalArgumentException( "Max value has wrong unit family" );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof FloatDataType ) {
            FloatDataType realOther = (FloatDataType) other;
            if(    ( ! theMin.equals( realOther.theMin ))
                || ( ! theMax.equals( realOther.theMax ))) {
                return false;
            }

            if( theUnitFamily == null ) {
                return( realOther.theUnitFamily == null );
            } else {
                return theUnitFamily.equals( realOther.theUnitFamily );
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        if( theMin != null ) {
            ret ^= theMin.hashCode();
        }
        if( theMax != null ) {
            ret ^= theMax.hashCode();
        }
        if( theUnitFamily != null ) {
            ret ^= theUnitFamily.hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSupersetOfOrEquals(
            DataType other )
    {
        if( other instanceof FloatDataType ) {
            FloatDataType realOther = (FloatDataType) other;
            return    ( theMin.isSmallerOrEquals( realOther.theMin ))
                   && ( realOther.theMax.isSmallerOrEquals( theMax ))
                   && (    ( theUnitFamily == null && realOther.theUnitFamily == null )
                        || ( theUnitFamily != null && theUnitFamily.equals( realOther.theUnitFamily )));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getPerformDomainCheck()
    {
        return    ( theMin.theValue != Float.NEGATIVE_INFINITY )
               || ( theMax.theValue != Float.POSITIVE_INFINITY );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        FloatValue realValue = (FloatValue) value; // may throw

        if( theMin.theValue > realValue.value() ) {
            return -1;
        }
        if( theMax.theValue < realValue.value() ) {
            return +1;
        }
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaDomainCheckExpression(
            String varName )
    {
        if( theMin.theValue == Float.NEGATIVE_INFINITY ) {
            if( theMax.theValue == Float.POSITIVE_INFINITY ) {
                return "true";
            } else {
                return "( " + varName + ".value() <= " + String.valueOf(theMax.value() ) + " )";
            }
        } else {
            if( theMax.theValue == Float.POSITIVE_INFINITY ) {
                return "( " + varName + ".value() >= " + String.valueOf(theMin.value() ) + " )";
            } else {
                return "(    ( " + varName + ".value() >= " + String.valueOf(theMin.value() ) + " )"
                   +    " && ( " + varName + ".value() <= " + String.valueOf(theMax.value() ) + " ) )";
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<FloatValue> getCorrespondingJavaClass()
    {
        return FloatValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FloatValue getDefaultValue()
    {
        if( theUnitFamily == null ) {
            if( theMin.theValue <= 0.0 && theMax.theValue > 0.0 ) {
                return FloatValue.create( 0.0 );
            }
            return FloatValue.create( theMin.theValue );
        } else {
            // FIXME we should use a "primary unit" here, but we don't know the concept
            if( theMin.theValue <= 0.0 && theMax.theValue > 0.0 ) {
                return FloatValue.create( 0.0, theUnitFamily.getUnitsInFamily()[0] );
            }
            return FloatValue.create( theMin.theValue, theUnitFamily.getUnitsInFamily()[0] );
        }
    }

    /**
     * Obtain minimum allowed value.
     *
     * @return the smallest allowed value (may be negative infinity)
     */
    public FloatValue getMinimum()
    {
        return theMin;
    }

    /**
     * Obtain maximum allowed value.
     *
     * @return the largest allowed value (may be infinity)
     */
    public FloatValue getMaximum()
    {
        return theMax;
    }

    /**
     * Obtain the UnitFamily.
     *
     * @return the UnitFamily, if any
     */
    public UnitFamily getUnitFamily()
    {
        return theUnitFamily;
    }

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else if( this.equals( thePositiveDefault )) {
            return thePositiveDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        if( this == theDefault ) {
            return className + DEFAULT_STRING;
        } else if( this == thePositiveDefault ) {
            return className + ".thePositiveDefault";
        } else {
            StringBuilder ret = new StringBuilder( className );
            ret.append( CREATE_STRING );

            if( theMin != null ) {
                ret.append( theMin.getJavaConstructorString( classLoaderVar, null ));
            } else {
                ret.append( NULL_STRING );
            }

            ret.append( COMMA_STRING );

            if( theMax != null ) {
                ret.append( theMax.getJavaConstructorString( classLoaderVar, null ));
            } else {
                ret.append( NULL_STRING );
            }

            ret.append( COMMA_STRING );

            if( theUnitFamily != null ) {
                ret.append( theUnitFamily.getJavaConstructorString( classLoaderVar ));
            } else {
                ret.append( NULL_STRING );
            }
            ret.append( COMMA_STRING );

            if( theSupertype != null ) {
                ret.append( theSupertype.getJavaConstructorString( classLoaderVar ));
            } else {
                ret.append( NULL_STRING );
            }

            ret.append( CLOSE_PARENTHESIS_STRING );
            return ret.toString();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FloatValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        try {
            FloatValue ret = FloatValue.create( Double.parseDouble( raw ) );
            return ret;

        } catch( NumberFormatException ex ) {
            throw new PropertyValueParsingException( this, raw, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMin",
                    "theMax"
                },
                new Object[] {
                    theMin,
                    theMax
                });
    }

    /**
      * The value for the minimum.
      */
    protected FloatValue theMin;

    /**
      * The value for the maximum.
      */
    protected FloatValue theMax;

    /**
      * The unit family (if any).
      */
    protected UnitFamily theUnitFamily;
}
