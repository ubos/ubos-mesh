//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse.xpath;

import net.ubos.model.traverse.TraverseTranslatorException;

/**
 * Thrown by the XpathTraverseTranslator.
 */
public class XpathTraverseTranslatorException
    extends
        TraverseTranslatorException
{
    /**
     * Constructor.
     *
     * @param term the term that was given
     * @param index index into the term
     */
    public XpathTraverseTranslatorException(
            String term,
            int    index )
    {
        super( term );

        theIndex = index;
    }

    /**
     * Constructor with a message.
     *
     * @param term the term that was given
     * @param index index into the term
     * @param msg the message
     */
    public XpathTraverseTranslatorException(
            String term,
            int    index,
            String msg )
    {
        super( term, msg );

        theIndex = index;
    }

    /**
     * Constructor with no message but a cause.
     *
     * @param term the term that was given
     * @param index index into the term
     * @param cause the Throwable that caused this Exception
     */
    public XpathTraverseTranslatorException(
            String    term,
            int       index,
            Throwable cause )
    {
        super( term, cause );

        theIndex = index;
    }

    /**
     * Constructor with a message and a cause.
     *
     * @param term the term that was given
     * @param index index into the term
     * @param msg the message
     * @param cause the Exception that caused this Exception
     */
    public XpathTraverseTranslatorException(
            String    term,
            int       index,
            String    msg,
            Throwable cause )
    {
        super( term, msg, cause );

        theIndex = index;
    }

    /**
     * The index into the term where the problem was detected.
     */
    protected int theIndex;
}
