//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import java.text.ParseException;
import net.ubos.model.primitives.PointValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies PointValue.
 */
public class PointValueStringifier
        extends
            AbstractStringifier<PointValue>
{
    /**
     * Factory method.
     *
     * @return the created PointValueStringifier
     */
    public static PointValueStringifier create()
    {
        return new PointValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected PointValueStringifier()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            PointValue            arg,
            StringifierParameters pars,
            String                soFar )
    {
        return arg.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (PointValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            PointValue ret = PointValue.parsePointValue( rawString );
            return ret;

        } catch( ParseException ex ) {
            throw new StringifierParseException( this, null, ex );
        }
    }
}
