//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import java.text.ParseException;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies CurrencyValues.
 */
public class CurrencyValueStringifier
        extends
            AbstractStringifier<CurrencyValue>
{
    /**
     * Factory method.
     *
     * @return the created EnumeratedValueStringifier
     */
    public static CurrencyValueStringifier create()
    {
        return new CurrencyValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected CurrencyValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            CurrencyValue         arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = arg.value();

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (CurrencyValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CurrencyValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            CurrencyValue ret = CurrencyValue.parseCurrencyValue( rawString );
            return ret;

        } catch( ParseException ex ) {
            throw new StringifierParseException( this, null, ex );
        }
    }
}