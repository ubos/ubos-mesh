//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.StringHelper;

/**
  * This is a string value for PropertyValues. StringValues are arbitary-length.
  */
public final class StringValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method.
     *
     * @param value the value
     * @return the created StringValue
     * @throws IllegalArgumentException if null is given as argument
     */
    public static StringValue create(
            String value )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new StringValue( value );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the value
     * @return the created StringValue, or null
     * @throws IllegalArgumentException if null is given as argument
     */
    public static StringValue createOrNull(
            String value )
    {
        if( value == null ) {
            return null;
        }
        return new StringValue( value );
    }

    /**
     * Factory method.
     *
     * @param value the value
     * @return the created StringValue
     * @throws IllegalArgumentException if null is given as argument
     */
    public static StringValue create(
            StringBuilder value )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new StringValue( value.toString() );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the value
     * @return the created StringValue, or null
     * @throws IllegalArgumentException if null is given as argument
     */
    public static StringValue createOrNull(
            StringBuilder value )
    {
        if( value == null ) {
            return null;
        }
        return new StringValue( value.toString() );
    }

    /**
     * Helper method to create an array of StringValues from Strings.
     *
     * @param raw the array of Strings, or null
     * @return the corresponding array of StringValue, or null
     */
    public static StringValue [] createMultiple(
            String [] raw )
    {
        if( raw == null ) {
            return null;
        }
        StringValue [] ret = new StringValue[ raw.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = StringValue.create( raw[i] );
        }
        return ret;
    }

    /**
      * Private constructor, use factory methods
      *
      * @param value the value
      */
    private StringValue(
            String value )
    {
        this.theValue = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue()
    {
        return value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringDataType getDataType()
    {
        return StringDataType.theDefault;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof StringValue ) {
            return theValue.equals( ((StringValue) otherValue).theValue );
        }
        if( otherValue instanceof String ) {
            return theValue.equals( (String) otherValue );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theValue.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder( theValue.length() + 2 );
        buf.append( "\'" );
        buf.append( theValue );
        buf.append( "\'" );
        return buf.toString();
    }

    /**
     * Obtain as String, mostly for JavaBeans-aware software.
     *
     * @return as String
     */
    public String getAsString()
    {
        return value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        StringBuilder buf = new StringBuilder( theValue.length() + 30 ); // fudge
        buf.append( getClass().getName() );
        buf.append( DataType.CREATE_STRING );
        buf.append( "\"" );
        buf.append( StringHelper.stringToJavaString( theValue ));
        buf.append( "\" )" );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        StringValue realOther = (StringValue) o;

        return theValue.compareTo( realOther.theValue );
    }

    /**
     * This is a convenience comparison method in case one of the arguments may be null.
     *
     * @param one the first PropertyValue or null to be compared
     * @param two the second PropertyValue or null to be compared
     * @return 0 if the two values are equal, +1/-1 if the second is smaller/larger than the first,
     *         +2/-2 if one of them is null
     */
    public static int compareCaseInsensitive(
            StringValue one,
            StringValue two )
    {
        if( one == null ) {
            if( two == null ) {
                return 0;
            } else {
                return -2;
            }
        } else {
            if( two == null ) {
                return +2;
            } else {
                int temp = one.compareCaseInsensitiveTo( two );
                if( temp > 0 ) {
                    return +1;
                } else if( temp == 0 ) {
                    return 0;
                } else {
                    return -1;
                }
            }
        }
    }

    /**
     * Compare, but without considering case.
     *
     * @param o the other StringValue
     * @return 0 if the two values are equal, +1/-1 if the second is smaller/larger than the first,
     *         +2/-2 if one of them is null
     */
    public int compareCaseInsensitiveTo(
            StringValue o )
    {
        return theValue.toLowerCase().compareTo( o.theValue.toLowerCase() );
    }

    /**
     * The real value.
     */
    protected String theValue;
}
