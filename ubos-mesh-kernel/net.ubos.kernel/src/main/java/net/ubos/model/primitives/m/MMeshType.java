//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import java.util.Locale;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.StringValue;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.modelbase.m.MMeshTypeIdentifier;

/**
  * The root of the type inheritance hierarchy.
  * In-memory implementation.
  */
public abstract class MMeshType
        implements
            MeshType
{
    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     */
    public MMeshType(
            MMeshTypeIdentifier identifier )
    {
        this.theIdentifier = identifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MMeshTypeIdentifier getIdentifier()
    {
        return theIdentifier;
    }

    /**
     * Set the Name property.
     *
     * @param newValue the new value for the property
     * @see #getName
     */
    public final void setName(
            StringValue newValue )
    {
        this.theName = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final StringValue getName()
    {
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringValue getUserVisibleName()
    {
        if( theUserName == null && theUserNameMap != null ) {
            theUserName = (StringValue) theUserNameMap.get( Locale.getDefault() );
        }
        return theUserName;
    }

    /**
     * Set the localized map of user-visible names.
     *
     * @param newValue the new value for the map
     * @see #getUserVisibleNameMap
     */
    public final void setUserVisibleNameMap(
            L10PropertyValueMap newValue )
    {
        theUserNameMap = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final L10PropertyValueMap getUserVisibleNameMap()
    {
        return theUserNameMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final BlobValue getUserVisibleDescription()
    {
        if( theUserDescription == null && theUserDescriptionMap != null ) {
            theUserDescription = (BlobValue) theUserDescriptionMap.get( Locale.getDefault() );
        }

        return theUserDescription;
    }

    /**
     * Set the localized map of user-visible descriptions.
     *
     * @param newValue the new value for the map
     * @see #getUserVisibleDescriptionMap
     */
    public final void setUserVisibleDescriptionMap(
            L10PropertyValueMap newValue )
    {
        theUserDescriptionMap = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final L10PropertyValueMap getUserVisibleDescriptionMap()
    {
        return theUserDescriptionMap;
    }

    /**
      * Set the Icon property.
      *
      * @param newValue the new value for the property
      * @see #getIcon
      */
    public final void setIcon(
            BlobValue newValue )
    {
        this.theIcon = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BlobValue getIcon()
    {
        return theIcon;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean equals(
            Object other )
    {
        if( other instanceof MMeshType ) {
            MMeshType realOther = (MMeshType) other;
            return theIdentifier.equals( realOther.getIdentifier() );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int hashCode()
    {
        return theIdentifier.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( super.toString() );
        buf.append( "{ id: " );
        buf.append( DefaultMMeshTypeIdentifierBothSerializer.SINGLETON.toExternalForm( theIdentifier ));
        buf.append( " }" );
        return buf.toString();
    }

    /**
      * Value of the Identifier property.
      */
    private final MMeshTypeIdentifier theIdentifier;

    /**
      * Value of the Name property.
      */
    private StringValue theName;

    /**
     * Our user-visible Name -- allocated as needed.
     * This is protected, not private, so our subclasses can easily reassign it to something more specific.
     */
    protected transient StringValue theUserName;

    /**
     * Our user-visible Description -- allocated as needed.
     */
    private transient BlobValue theUserDescription;

    /**
     * The Icon. This is protected instead of private so subclasses can easily modify it.
     */
    protected BlobValue theIcon;

    /**
     * The map of localized user names.
     */
    private L10PropertyValueMap theUserNameMap;

    /**
     * The map of localized user descriptions.
     */
    private L10PropertyValueMap theUserDescriptionMap;
};
