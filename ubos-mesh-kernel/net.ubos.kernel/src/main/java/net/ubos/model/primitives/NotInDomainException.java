//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * Thrown if a value was not in the domain specified by its DataType.
 */
public abstract class NotInDomainException
        extends
            InvalidPropertyValueException
{
    /**
     * Constructor, for subclasses only.
     *
     * @param type the DataType whose domain was violated
     */
    protected NotInDomainException(
            DataType type )
    {
        theType = type;
    }

    /**
     * Obtain the DataType whose domain was violated.
     *
     * @return the DataType
     */
    public DataType getDataType()
    {
        return theType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theType };
    }

    /**
     * The DataType whose domain was violated
     */
    protected DataType theType;
}
