//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import java.text.ParseException;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies ExtentValue.
 */
public class ExtentValueStringifier
        extends
            AbstractStringifier<ExtentValue>
{
    /**
     * Factory method.
     *
     * @return the created ExtentValueStringifier
     */
    public static ExtentValueStringifier create()
    {
        return new ExtentValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected ExtentValueStringifier()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            ExtentValue           arg,
            StringifierParameters pars,
            String                soFar )
    {
        return arg.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (ExtentValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtentValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            ExtentValue ret = ExtentValue.parseExtentValue( rawString );
            return ret;

        } catch( ParseException ex ) {
            throw new StringifierParseException( this, null, ex );
        }
    }
}
