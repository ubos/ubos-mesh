//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.util.Locale;

/**
  * This represents an enumerated value for PropertyValues.
  *
  * This does not have a public constructor as only EnumeratedDataType gets to instantiate this.
  */
public final class EnumeratedValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method, to be used only by EnumeratedDataType. Use those factory methods and select
     * methods instead, do not instantiate EnumeratedValue using this factory method.
     *
     * @param type the EnumeratedDataType to which this EnumeratedValue belongs
     * @param value the programmatic value of this EnumeratedValue
     * @param userNameMap user-visible, internationalized names for this value
     * @param userDescriptionMap user-visible, internalizationalized descriptions for this value
     * @return the created EnumeratedValue
     */
    public static EnumeratedValue create(
            EnumeratedDataType type,
            String             value,
            L10PropertyValueMap userNameMap,
            L10PropertyValueMap userDescriptionMap )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new EnumeratedValue( type, value, userNameMap, userDescriptionMap );
    }

    /**
      * Private constructor.
      *
     * @param type the EnumeratedDataType to which this EnumeratedValue belongs
      * @param value the programmatic value of this EnumeratedValue
      * @param userNameMap user-visible, internationalized names for this value
      * @param userDescriptionMap user-visible, internalizationalized descriptions for this value
      */
    private EnumeratedValue(
            EnumeratedDataType  type,
            String              value,
            L10PropertyValueMap userNameMap,
            L10PropertyValueMap userDescriptionMap )
    {
        this.theDataType           = type;
        this.theValue              = value;
        this.theUserNameMap        = userNameMap;
        this.theUserDescriptionMap = userDescriptionMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnumeratedDataType getDataType()
    {
        return theDataType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return getUserVisibleName().getAsString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue()
    {
        return value();
    }

    /**
     * Obtain a user-visible, internationalized representation of this value.
     *
     * @return user-visible, internationalized representation of this value
     */
    public StringValue getUserVisibleName()
    {
        if( theUserName == null && theUserNameMap != null ) {
            theUserName = (StringValue) theUserNameMap.get( Locale.getDefault() );
        }
        if( theUserName == null ) {
            return StringValue.create( theValue );
        }
        return theUserName;
    }

    /**
     * Obtain the map of user-visible, internationalized representations of this value.
     *
     * @return map of user-visible, internationalized representations of this value
     */
    public L10PropertyValueMap getUserVisibleNameMap()
    {
        return theUserNameMap;
    }

    /**
     * Obtain a user-visible, internationalized description of this value.
     *
     * @return user-visible, internationalized description of this value
     */
    public BlobValue getUserVisibleDescription()
    {
        if( theUserDescription == null && theUserDescriptionMap != null ) {
            theUserDescription = (BlobValue) theUserDescriptionMap.get( Locale.getDefault() );
        }
        return theUserDescription;
    }

    /**
     * Obtain the map of user-visible, internationalized descriptions of this value.
     *
     * @return map of user-visible, internationalized descriptions of this value
     */
    public L10PropertyValueMap getUserVisibleDescriptionMap()
    {
        return theUserDescriptionMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof EnumeratedValue ) {
            return this == otherValue;
        } else if( otherValue instanceof String ) {
            return theValue.equals( otherValue );
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        ret ^= theDataType != null ? theDataType.hashCode() : 0;
        ret ^= theValue.hashCode();

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "#" + theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        return typeVar + ".select( \"" + theValue + "\" )";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        // typecast to ensure ClassCastException
        if( this == (EnumeratedValue) o ) {
            return 0;
        } else {
            return +2; // not comparable convention: +2
        }
    }

    /**
     * The EnumeratedDataType to which this instance belongs.
     */
    protected EnumeratedDataType theDataType;
    
    /**
      * The real value.
      */
    protected String theValue;

    /**
     * The user-visible string in the current locale -- allocated as needed.
     */
    protected transient StringValue theUserName;

    /**
     * The user-visible description in the current locale -- allocated as needed.
     */
    protected transient BlobValue theUserDescription;

    /**
     * The user name localization map.
     */
    protected L10PropertyValueMap theUserNameMap;

    /**
     * The user description localization map.
     */
    protected L10PropertyValueMap theUserDescriptionMap;
}
