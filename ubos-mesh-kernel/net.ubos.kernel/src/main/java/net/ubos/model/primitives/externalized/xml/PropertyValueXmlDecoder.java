//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.xml;

import net.ubos.model.primitives.externalized.PropertyValueDecoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class PropertyValueXmlDecoder
    extends
        AbstractPropertyValueXmlDecoder
    implements
        PropertyValueDecoder
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static PropertyValueXmlDecoder create()
    {
        return new PropertyValueXmlDecoder();
    }

    /**
     * Constructor. Use factory method.
     */
    protected PropertyValueXmlDecoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return PROPERTY_VALUE_XML_ENCODING_ID;
    }
}
