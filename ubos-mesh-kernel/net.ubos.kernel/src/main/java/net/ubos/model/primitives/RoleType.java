//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MultiplicityException;
import net.ubos.model.traverse.TraverseSpecification;

/**
  * <p>The role played by a EntityType in a RelationshipType. One can consider a RoleType to be the "end"
  *    of a RelationshipType.
  *
  * <p>RelationshipType inheritance is modeled through RoleTypes. By allowing a RoleType to refine another
  *    RoleType, the RelationshipType is subtyped. Of course, there are constraints on the topology of
  *    such refinement relationships. Given that models support multiple inheritance for EntityTypes
  *    and for RelationshipTypes, any RoleType may refine N RoleTypes, which thereby identify the
  *    supertype RelationshipTypes.
  *
  * <p>A RelationshipType usually has different source and destination RoleTYpes, indicating the directionality
  *    of the RelationshipType. However, a RelationshipType may also use the same RoleType for source and
  *    destination, indicating a symmetric RelationshipType in which source and destination are semantically
  *    the same. For example, the social media RelationshipType "follows" is directed: the entity that follows
  *    plays a semantically distinct role from the entity that is being followed; while the social media
  *    RelationshipType "is friends with" is undirected: the two related entities play the same RoleType
  *    within this relationship.
  *
  * <p>RoleTypes carry a multiplicity, which, counter-intuitively, is the opposite of what is commonly written
  *    next to the line in an entity-relationship or class-association diagram. For example, if this is the
  *    diagram:
  * <pre>
  *    +-------+ 0..1        1..* +-------+
  *    |   A   |<---------------->|   B   |
  *    +-------+ (source)  (dest) +-------+
  * </pre>
  * <p>then the source RoleType carries the 1..*, while the destination RoleType carries the 0..1. While this is
  *    a little counter-intuitive diagrammatically, it makes a lot more sense semantically and programmatically.
  */
public interface RoleType
        extends
            MeshTypeWithProperties,
            TraverseSpecification
{
    /**
     * Obtain the RelationshipType that this RoleType belongs to.
     *
     * @return the RelationshipType that this RoleType belongs to
     */
    public RelationshipType getRelationshipType();

    /**
     * Obtain the EntityType that plays this RoleType.
     *
     * @return the EntityType that this RoleType belongs to
     */
    public EntityType getEntityType();

    /**
     * Obtain the "other" RoleType of this RelationshipType. A RelationshipType has a source
     * and a destination RoleType. If this is applied on a source RoleType, this will obtain
     * the destination RoleType, and vice versa.
     *
     * @return the RoleType on the other end of the RelationshipType that this RoleType belongs to
     */
    public RoleType getInverseRoleType();

    /**
     * Obtain the EntityType on the other end of this RelationshipType. A RelationshipType has a source
     * and a destination RoleType. If this is applied on a source RoleType, this will obtain
     * the EntityType related to the destination RoleType, and vice versa.
     *
     * @return the EntityType on the other end of the RelationshipType that this RoleType belongs to
     */
    public EntityType getOtherEntityType();

    /**
     * Obtain the multiplicity of this RoleType. The multiplicity of a RoleType is the number of
     * EntityType instances that can be reached from a start EntityType instance by traversing
     * the RoleType. Multiplicities have a minimum and a maximum value. Note that multiplicities
     * on RoleType are the opposite numbers of what is written next to a line in an entity-relationship
     * (or class-association) diagram.
     *
     * @return the multiplicity of this RoleType
     */
    public MultiplicityValue getMultiplicity();

    /**
     * Determine whether this RoleType is symmetric, i.e. the same as its inverse.
     *
     * @return true if this is symmetric
     */
    public boolean isSymmetric();

    /**
     * Given this MeshObject and this set of other sides for the RoleType,
     * check that it conforms to the range specified in the RoleType's Multiplicity.
     *
     * @param affected the MeshObject
     * @param others the other MeshObjects
     * @throws MultiplicityException thrown if the number of others is out of range
     */
    public void checkMultiplicity(
            MeshObject    affected,
            MeshObject [] others )
        throws
            MultiplicityException;

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getDirectSupertypes();

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getSupertypes();

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getDirectSubtypes();

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getSubtypes();

    /**
     * Convenience constant.
     */
    public static final RoleType [] EMPTY_ARRAY = {};
}
