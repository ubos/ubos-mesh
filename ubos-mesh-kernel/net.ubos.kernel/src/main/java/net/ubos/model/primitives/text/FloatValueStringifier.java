//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.FloatValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies FloatValues.
 */
public class FloatValueStringifier
        extends
            AbstractStringifier<FloatValue>
{
    /**
     * Factory method.
     *
     * @return the created FloatValueStringifier
     */
    public static FloatValueStringifier create()
    {
        return new FloatValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected FloatValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            FloatValue            arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = String.valueOf( arg.value() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (FloatValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FloatValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            return FloatValue.parseFloatValue( rawString );

        } catch( NumberFormatException ex ) {
            throw new StringifierParseException( this, rawString, ex );
        }
    }
}

