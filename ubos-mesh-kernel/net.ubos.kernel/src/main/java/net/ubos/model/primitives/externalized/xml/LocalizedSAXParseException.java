//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.xml;

import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.exception.LocalizedException;
import org.xml.sax.Locator;
import org.xml.sax.SAXParseException;

/**
 * Adds localization to the SAXParseException, and fixes the constructor,
 * so we can pass in a cause
 */
public abstract class LocalizedSAXParseException
    extends
        SAXParseException
    implements
        LocalizedException
{
    /**
     * Constructor.
     *
     * @param locator indicates the location of the error in the stream
     * @param cause the underlying cause, if any
     */
    public LocalizedSAXParseException(
            Locator   locator,
            Exception cause )
    {
        super( null, locator, cause );
        initCause( cause );
    }
    
    /**
     * Constructor.
     *
     * @param locator indicates the location of the error in the stream
     */
    public LocalizedSAXParseException(
            Locator locator )
    {
        super( null, locator );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        return AbstractLocalizedException.assembleDefaultLocalizedMessage( this );
    }

    /**
     * A subclass that delegates to another Throwable.
     */
    public static class Delegate
            extends
                LocalizedSAXParseException
    {
        /**
         * Constructor.
         *
         * @param locator indicates the location of the error in the stream
         * @param cause the underlying cause, if any
         */
        public Delegate(
                Locator   locator,
                Exception cause )
        {
            super( locator, cause );
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public String getLocalizedMessage()
        {
            return getCause().getLocalizedMessage();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[0];
        }
    }
}
