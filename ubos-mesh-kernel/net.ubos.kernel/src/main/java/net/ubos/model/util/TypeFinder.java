//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.util;

import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;


/**
 * This utility class makes it easy for the code generator to look up types.
 */
public abstract class TypeFinder
{
    private static final Log log = Log.getLogInstance( TypeFinder.class ); // our own, private logger

    /**
     * Find an EntityType by its unique identifierString. If needed, attempt to load additional
     * ModelModules to resolve the request. This uses String instead of MeshObjectIdentifier to
     * simplify the API for the code generator purpose.
     *
     * @param identifierString identifier of the to-be-found MeshType
     * @return the found MeshType, or null if not found
     */
    public static EntityType findEntityType(
            String identifierString )
    {
        try {
            MeshTypeIdentifier identifier = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON.fromExternalForm( identifierString );

            EntityType ret = ModelBase.SINGLETON.findEntityType( identifier );

            return ret;

        } catch( Throwable ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Find a RelationshipType by its unique identifier. If needed, attempt to load additional
     * ModelModules to resolve the request. This uses String instead of MeshObjectIdentifier to
     * simplify the API for the code generator purpose.
     *
     *
     * @param identifierString Identifier of the to-be-found MeshType
     * @return the found MeshType, or null if not found
     */
    public static RelationshipType findRelationshipType(
            String identifierString )
    {
        try {
            MeshTypeIdentifier identifier = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON.fromExternalForm( identifierString );

            RelationshipType ret = ModelBase.SINGLETON.findRelationshipType( identifier );

            return ret;

        } catch( Throwable ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Find a PropertyType by its unique identifier. If needed, attempt to load additional
     * ModelModules to resolve the request. This uses String instead of MeshObjectIdentifier to
     * simplify the API for the code generator purpose.
     *
     * @param identifierString Identifier of the to-be-found MeshType
     * @return the found MeshType, or null if not found
     */
    public static PropertyType findPropertyType(
            String identifierString )
    {
        try {
            MeshTypeIdentifier identifier = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON.fromExternalForm( identifierString );

            PropertyType ret = ModelBase.SINGLETON.findPropertyType( identifier );

            return ret;

        } catch( Throwable ex ) {
            log.error( ex );
            return null;
        }
    }
    }
