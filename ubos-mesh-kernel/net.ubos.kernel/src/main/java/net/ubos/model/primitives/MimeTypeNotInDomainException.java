//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * Thrown if a MIME type of a BlobValue was not allowed in the set of MIME types
 * defined by its BlobDataType.
 */
public class MimeTypeNotInDomainException
        extends
            NotInDomainException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor, for subclasses only.
     *
     * @param type the DataType whose domain was violated
     * @param mime the disallowed MIME type
     */
    public MimeTypeNotInDomainException(
            BlobDataType type,
            String       mime )
    {
        super( type );

        theMime = mime;
    }

    /**
     * Obtain the disallowed MIME type.
     *
     * @return the MIME type
     */
    public String getMimeType()
    {
        return theMime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BlobDataType getDataType()
    {
        return (BlobDataType) theType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theType, theMime, ((BlobDataType)theType).getMimeTypeRegexes() };
    }

    /**
     * The disallowed MIME type.
     */
    protected final String theMime;
}
