//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSelector;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;

/**
 * This TraverseSpecification qualifies another TraverseSpecification by first taking
 * a subset of the start set as the point of departure for the qualified TraverseSpecification,
 * and then taking another subset of the result. Either (but not both) of the MeshObjectSelectors
 * is optional.
 */
public class SelectiveTraverseSpecification
        extends
            AbstractTraverseSpecification
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method to create one without the start selector.
     *
     * @param theQualified the TraverseSpecification that this SelectiveTraverseSpecification qualifies
     * @param theEndSelector determines which of the found MeshObjects of the qualified
     *        TraverseSpecification are returned
     * @return the created SelectiveTraverseSpecification
     * @throws IllegalArgumentException if one of the arguments is null
     */
    public static SelectiveTraverseSpecification create(
            TraverseSpecification theQualified,
            MeshObjectSelector     theEndSelector )
    {
        // we are paranoid, so we check
        if( theQualified == null ) {
            throw new IllegalArgumentException( "empty qualified TraverseSpecification" );
        }
        if( theEndSelector == null ) {
            throw new IllegalArgumentException( "end selector cannot be null" );
        }
        return new SelectiveTraverseSpecification( null, theQualified, theEndSelector );
    }

    /**
     * Factory method to create one without the start selector.
     *
     * @param theStartSelector determines with which of the start MeshObjects we start our traversal
     * @param theQualified the TraverseSpecification that this SelectiveTraverseSpecification qualifies
     * @param theEndSelector determines which of the found MeshObjects of the qualified
     *        TraverseSpecification are returned
     * @return the created SelectiveTraverseSpecification
     * @throws IllegalArgumentException if theQualified or either theStartSelector or theEndSelector are null
     */
    public static SelectiveTraverseSpecification create(
            MeshObjectSelector    theStartSelector,
            TraverseSpecification theQualified,
            MeshObjectSelector    theEndSelector )
    {
        // we are paranoid, so we check
        if( theQualified == null ) {
            throw new IllegalArgumentException( "empty qualified TraverseSpecification" );
        }
        if( theStartSelector == null && theEndSelector == null ) {
            throw new IllegalArgumentException( "both selectors cannot be null" );
        }
        return new SelectiveTraverseSpecification( theStartSelector, theQualified, theEndSelector );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param startSelector determines the subset of the start MeshObjects
     * @param qualified the TraverseSpecification from the start MeshObjectSet's subset
     * @param endSelector determines the subset of the destination MeshObjects
     */
    protected SelectiveTraverseSpecification(
            MeshObjectSelector    startSelector,
            TraverseSpecification qualified,
            MeshObjectSelector    endSelector )
    {
        theStartSelector = startSelector;
        theQualified     = qualified;
        theEndSelector   = endSelector;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObject start )
    {
        if( theStartSelector != null && ! theStartSelector.accepts( start )) {
            return MeshObjectSet.DEFAULT_EMPTY_SET;
        }

        MeshObjectSet s = theQualified.traverse( start );

        if( theEndSelector != null ) {
            s = s.subset( theEndSelector );
        }
        return s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObjectSet theSet )
    {
        MeshObjectSetFactory setFactory = theSet.getFactory();

        MeshObjectSet realStart;
        if( theStartSelector != null ) {
            realStart = setFactory.createImmutableMeshObjectSet( theSet, theStartSelector );
        } else {
            realStart = theSet;
        }
        MeshObjectSet s = theQualified.traverse( realStart );

        if( theEndSelector != null ) {
            return setFactory.createImmutableMeshObjectSet( s, theEndSelector );
        } else {
            return s;
        }
    }

    /**
     * Determine equality.
     *
     * @param other the Object to compare to
     * @return true if the two objects are equal
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof SelectiveTraverseSpecification ) {
            SelectiveTraverseSpecification realOther = (SelectiveTraverseSpecification) other;

            if( ! theQualified.equals( realOther.theQualified )) {
                return false;
            }

            if( theStartSelector != null ) {
                if( ! theStartSelector.equals( realOther.theStartSelector )) {
                    return false;
                }
            } else if( realOther.theStartSelector != null ) {
                return false;
            }

            if( theEndSelector != null ) {
                if( ! theEndSelector.equals( realOther.theEndSelector )) {
                    return false;
                }
            } else if( realOther.theEndSelector != null ) {
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * Hash code.
     *
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        int ret = theQualified.hashCode();
        if( theStartSelector != null ) {
            ret ^= theStartSelector.hashCode();
        }
        if( theEndSelector != null ) {
            ret ^= theEndSelector.hashCode();
        }
        return ret;
    }

    /**
     * Determine whether a given event, with a source of from where we traverse the
     * TraverseSpecification, may affect the result of the traversal.
     *
     * @param meshBase the MeshBase in which we consider the event
     * @param theEvent the event that we consider
     * @return true if this event may affect the result of traversing from the MeshObject
     *         that sent this event
     */
    @Override
    public boolean isAffectedBy(
            MeshBase                  meshBase,
            AbstractMeshObjectRoleTypeChange theEvent )
    {
        MeshObject source = theEvent.getSource();
        if( theStartSelector != null ) {
            if( ! theStartSelector.accepts( source )) {
                return false;
            }
        }

        MeshObject otherSide = theEvent.getNeighbor();

        if( !theEndSelector.accepts( otherSide )) {
            return false;
        }

        return theQualified.isAffectedBy( meshBase, theEvent );
    }

    /**
     * Obtain the TraverseSpecification that we qualify.
     *
     * @return the TraverseSpecification that we qualify
     */
    public TraverseSpecification getQualifiedTraverseSpecification()
    {
        return theQualified;
    }

    /**
     * Obtain our start MeshObjectSelector, if any.
     *
     * @return the start MeshObjectSelector
     */
    public MeshObjectSelector getStartSelector()
    {
        return theStartSelector;
    }

    /**
     * Obtain our end MeshObjectSelector, if any.
     *
     * @return the end MeshObjectSelector
     */
    public MeshObjectSelector getEndSelector()
    {
        return theEndSelector;
    }

    /**
     * The TraverseSpecification that we qualify.
     */
    protected TraverseSpecification theQualified;

    /**
     * The selector that knows which of the start MeshObjects we should traverse from.
     */
    protected MeshObjectSelector theStartSelector;

    /**
     * The selector that knows which of the destination MeshObjects we should actually return.
     */
    protected MeshObjectSelector theEndSelector;
}
