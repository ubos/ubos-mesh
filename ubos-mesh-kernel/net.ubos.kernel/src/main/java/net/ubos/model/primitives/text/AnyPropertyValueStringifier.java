//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import java.util.HashMap;
import java.util.Map;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.Stringifier;
import net.ubos.util.text.StringifierException;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies any PropertyValue.
 */
public class AnyPropertyValueStringifier
        extends
            AbstractStringifier<PropertyValue>
{
    /**
     * Factory method.
     *
     * @return the created AnyPropertyValueStringifier
     */
    public static AnyPropertyValueStringifier create()
    {
        return new AnyPropertyValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected AnyPropertyValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            PropertyValue         arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        if( arg == null ) {
            return theNullStringifier.format( arg, pars, soFar );
        }
        for( Map.Entry<Class<? extends PropertyValue>, Stringifier<? extends PropertyValue>> current : theDelegates.entrySet() ) {
            if( current.getKey().isInstance( arg )) {
                return current.getValue().attemptFormat( arg, pars, soFar );
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException
    {
        return format( (PropertyValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CurrencyValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Delegate Stringifiers.
     */
    protected static final Map<Class<? extends PropertyValue>, Stringifier<? extends PropertyValue>> theDelegates
            = new HashMap<>();
    static {
        theDelegates.put( BlobValue.class,         BlobValueStringifier.create() );
        theDelegates.put( BooleanValue.class,      BooleanValueStringifier.create() );
        theDelegates.put( ColorValue.class,        ColorValueStringifier.create() );
        theDelegates.put( CurrencyValue.class,     CurrencyValueStringifier.create() );
        theDelegates.put( EnumeratedValue.class,   EnumeratedValueStringifier.create( true ) );
        theDelegates.put( ExtentValue.class,       ExtentValueStringifier.create() );
        theDelegates.put( FloatValue.class,        FloatValueStringifier.create() );
        theDelegates.put( IntegerValue.class,      IntegerValueStringifier.create() );
        theDelegates.put( MultiplicityValue.class, MultiplicityValueStringifier.create() );
        theDelegates.put( PointValue.class,        PointValueStringifier.create() );
        theDelegates.put( StringValue.class,       StringValueStringifier.create() );
        theDelegates.put( TimePeriodValue.class,   TimePeriodValueStringifier.create() );
        theDelegates.put( TimeStampValue.class,    TimeStampValueStringifier.create() );
    }

    /**
     * Null is special.
     */
    protected static final NullValueStringifier theNullStringifier = NullValueStringifier.create();
}
