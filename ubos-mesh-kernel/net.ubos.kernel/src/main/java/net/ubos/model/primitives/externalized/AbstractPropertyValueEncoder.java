//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;

/**
 * Collects functionality useful for many implementations of PropertyValueEncoder.
 */
public abstract class AbstractPropertyValueEncoder
    implements
        PropertyValueEncoder
{
    /**
     * Encode a Serializable through Serialization.
     *
     * @param value the Serializable
     * @return the Serializable, Base64 encoded
     * @throws IOException thrown if an I/O error occurred
     */
    protected String serializableAsBase64String(
            Serializable value )
        throws
            IOException
    {
        try( ByteArrayOutputStream stream = new ByteArrayOutputStream()) {

            ObjectOutputStream stream2 = new ObjectOutputStream( stream );
            stream2.writeObject( value );

            String encoded = Base64.getEncoder().encodeToString( stream.toByteArray() );
            return encoded;
        }
    }
}
