//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.logging.Dumper;

/**
  * An enumerated DataType for PropertyValues. It requires the explicit specification of
  * a domain.
  */
public class EnumeratedDataType
        extends
            DataType
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
      * Default instance.
      */
    public static final EnumeratedDataType theDefault = new EnumeratedDataType();

    /**
     * Factory method.
     *
     * @param domain the programmatic domain of this EnumeratedDataType
     * @param userNameMap in the same sequence as the domain, the domain in internationalized form
     * @param userDescriptionMap in the same sequence as the domain, user-displayable descriptions of the various values
     * @param superType the DataType that we refine, if any
     * @return the created EnumeratedDataType
     */
    public static EnumeratedDataType create(
            String []              domain,
            L10PropertyValueMap [] userNameMap,
            L10PropertyValueMap [] userDescriptionMap,
            DataType               superType )
    {
        return new EnumeratedDataType( domain, userNameMap, userDescriptionMap, superType );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param domain the programmatic domain of this EnumeratedDataType
     * @param userNameMap in the same sequence as the domain, the domain in internationalized form
     * @param userDescriptionMap in the same sequence as the domain, user-displayable descriptions of the various values
     * @param superType the DataType that we refine, if any
     */
    private EnumeratedDataType(
            String []              domain,
            L10PropertyValueMap [] userNameMap,
            L10PropertyValueMap [] userDescriptionMap,
            DataType               superType )
    {
        super( superType );

        if( domain == null || domain.length == 0 ) {
            throw new IllegalArgumentException( "Cannot have empty domain for EnumeratedDataType" );
        }
        if( userNameMap == null || userNameMap.length != domain.length ) {
            throw new IllegalArgumentException( "UserNameMap must match length of EnumeratedDataType domain" );
        }
        if( userDescriptionMap != null && userDescriptionMap.length != domain.length ) {
            throw new IllegalArgumentException( "UserDescriptionMap must either be null, or match length of EnumeratedDataType domain" );
        }
        theDomain = new EnumeratedValue[ domain.length ];
        for( int i=0 ; i<domain.length ; ++i ) {
            L10PropertyValueMap nameMap        = userNameMap[i]; // may not be null
            L10PropertyValueMap descriptionMap = userDescriptionMap != null ? userDescriptionMap[i] : null;

            theDomain[i] = EnumeratedValue.create( this, domain[i], nameMap, descriptionMap );
        }
    }

    /**
     * Special constructor for the supertype singleton.
     */
    private EnumeratedDataType()
    {
        super( null );
        
        theDomain = new EnumeratedValue[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( this == other ) {
            return true;
        }

        if( other instanceof EnumeratedDataType ) {
            EnumeratedDataType realOther = (EnumeratedDataType) other;
            if( theDomain == null ) {
                if( realOther.theDomain == null ) {
                    return true; // FIXME? Why doesn't the first line of this method catch this? Need singleton deserialization support ...?!?
                } else {
                    return false;
                }
            }
            if( realOther.theDomain == null ) {
                return false;
            }

            // quick test first
            if( theDomain.length != realOther.theDomain.length ) {
                return false;
            }

            // then look at all values
            for( int i=0 ; i<realOther.theDomain.length ; ++i ) {
                boolean found = false;
                for( int j=0 ; j<theDomain.length ; ++j ) {
                    if( realOther.theDomain[i].equals( theDomain[j] )) {
                        found = true;
                        break;
                    }
                }
                if( ! found ) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSupersetOfOrEquals(
            DataType other )
    {
        if( other instanceof EnumeratedDataType ) {
            EnumeratedDataType realOther = (EnumeratedDataType) other;

            // quick test first
            if( theDomain.length < realOther.theDomain.length ) {
                return false;
            }
            // then look at all values
            for( int i=0 ; i<realOther.theDomain.length ; ++i ) {
                boolean found = false;
                for( int j=0 ; j<theDomain.length ; ++j ) {
                    if( realOther.theDomain[i].equals( theDomain[j] )) {
                        found = true;
                        break;
                    }
                }
                if( ! found ) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        EnumeratedValue realValue = (EnumeratedValue) value; // may throw

        for( int i=0 ; i<theDomain.length ; ++i ) {
            if( realValue.equals( (Object) theDomain[i].value() )) { // EnumeratedValue compared to String is okay
                return 0;
            }
        }
        return Integer.MAX_VALUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getPerformDomainCheck()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaDomainCheckExpression(
            String varName )
    {
        StringBuilder ret = new StringBuilder( theDomain.length * 10 );
        ret.append( "( " );
        for( int i=0 ; i<theDomain.length ; ++i ) {
            if( i>0 ) {
                ret.append( " || " );
            }
            ret.append( varName );
            ret.append( ".equals( \"" );
            ret.append( theDomain[i].value() );
            ret.append( "\" )" );
        }
        ret.append( CLOSE_PARENTHESIS_STRING );
        return ret.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<EnumeratedValue> getCorrespondingJavaClass()
    {
        return EnumeratedValue.class;
    }

    /**
      * Obtain the domain with programmatic values.
      *
      * @return the domain with programmatic values
      */
    public EnumeratedValue [] getDomain()
    {
        return theDomain;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnumeratedValue getDefaultValue()
    {
        return theDomain[0];
    }

    /**
     * Given a programmatic string as a key, this selects the right EnumeratedValue from our domain.
     *
     * @param key the selector to find the right element of this domain
     * @return the found EnumeratedValue
     * @throws UnknownEnumeratedValueException.Key if the EnumeratedValue cannot be found
     */
    public EnumeratedValue select(
            String key )
        throws
            UnknownEnumeratedValueException.Key
    {
        EnumeratedValue ret = selectOrNull( key );
        if( ret != null ) {
            return ret;
        } else {
            throw new UnknownEnumeratedValueException.Key( this, key );
        }
    }

    /**
     * Given a programmatic string as a key, this selects the right EnumeratedValue from our domain,
     * or returns null if not found.
     *
     * @param key the selector to find the right element of this domain
     * @return the found EnumeratedValue, or null
     */
    public EnumeratedValue selectOrNull(
            String key )
    {
        for( int i=0 ; i<theDomain.length ; ++i ) {
            if( theDomain[i].equals( (Object) key )) { // compare EnumeratedValue with String is okay
                return theDomain[i];
            }
        }
        for( int i=0 ; i<theDomain.length ; ++i ) {
            if( theDomain[i].toString().equals( key )) {
                return theDomain[i];
            }
        }
        return null;
    }

    /**
     * Given a user-visible name as a key, this selects the right EnumeratedValue from our domain.
     *
     * @param userVisibleName the selector to find the right element of this domain
     * @return the found EnumeratedValue
     * @throws UnknownEnumeratedValueException.UserVisible if the EnumeratedValue cannot be found
     */
    public EnumeratedValue selectByUserVisibleName(
            String userVisibleName )
        throws
            UnknownEnumeratedValueException.UserVisible
    {
        EnumeratedValue ret = selectByUserVisibleNameOrNull( userVisibleName );
        if( ret != null ) {
            return ret;
        } else {
            throw new UnknownEnumeratedValueException.UserVisible( this, userVisibleName );
        }
    }

    /**
     * Given a user-visible name as a key, this selects the right EnumeratedValue from our domain,
     * or returns null if not found.
     *
     * @param userVisibleName the selector to find the right element of this domain
     * @return the found EnumeratedValue, or null
     */
    public EnumeratedValue selectByUserVisibleNameOrNull(
            String userVisibleName )
    {
        for( int i=0 ; i<theDomain.length ; ++i ) {
            if( userVisibleName.equals( theDomain[i].getUserVisibleName().value() )) {
                return theDomain[i];
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        if( this == theDefault ) {
            return className + DEFAULT_STRING;
        } else {
            // we make the assumption that there is a domain

            StringBuilder ret = new StringBuilder( className );
            ret.append( CREATE_STRING );
            ret.append( "new String[] { " );
            for( int i=0 ; i<theDomain.length ; ++i ) {
                ret.append( QUOTE_STRING );
                ret.append( theDomain[i].value() );
                ret.append( QUOTE_STRING );
                if( i<theDomain.length-1 ) {
                    ret.append( COMMA_STRING );
                }
            }
            ret.append( " }, new " ).append( L10PropertyValueMap.class.getName() ).append( "[] { " );
            for( int i=0 ; i<theDomain.length ; ++i ) {
                if( theDomain[i].getUserVisibleNameMap() != null ) {
                    ret.append( theDomain[i].getUserVisibleNameMap().getJavaConstructorString(
                            classLoaderVar,
                            StringDataType.class.getName() + ".create" ));
                } else {
                    ret.append( NULL_STRING );
                }
                if( i<theDomain.length-1 ) {
                    ret.append( COMMA_STRING );
                }
            }
            ret.append( " }, new " ).append( L10PropertyValueMap.class.getName() ).append( "[] { " );
            for( int i=0 ; i<theDomain.length ; ++i ) {
                if( theDomain[i].getUserVisibleDescriptionMap() != null ) {
                    ret.append( theDomain[i].getUserVisibleDescriptionMap().getJavaConstructorString(
                            classLoaderVar,
                            BlobDataType.class.getName() + ".theTextAnyType" ));
                } else {
                    ret.append( NULL_STRING );
                }
                if( i<theDomain.length-1 ) {
                    ret.append( COMMA_STRING );
                }
            }
            ret.append( " }" );
            ret.append( COMMA_STRING );

            if( theSupertype != null ) {
                ret.append( theSupertype.getJavaConstructorString( classLoaderVar ));
            } else {
                ret.append( NULL_STRING );
            }
            ret.append( CLOSE_PARENTHESIS_STRING );

            return ret.toString();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnumeratedValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        try {
            EnumeratedValue ret = select( raw );
            return ret;

        } catch( UnknownEnumeratedValueException.Key ex ) {
            throw new PropertyValueParsingException( this, raw );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theDomain"
                },
                new Object[] {
                    theDomain
                });

    }

    /**
      * The value of the domain.
      */
    protected EnumeratedValue [] theDomain;
}
