//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import java.io.Serializable;
import java.util.function.Predicate;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;

/**
 * A TraverseSpecification to traverse to neighbor MeshObjects with whom the start object has a
 * RoleAttribute with a certain name, and potentially, a certain value
 */
public class ByRoleAttributeTraverseSpecification
    extends
        AbstractTraverseSpecification
{
    /**
     * Factory method for a RoleAttributeTraverseSpecification that matches
     * RoleAttributes with a certain name at the source.
     *
     * @param roleAttributeName name of the RoleAttribute
     * @return the created object
     */
    public static ByRoleAttributeTraverseSpecification createSource(
            String roleAttributeName )
    {
        return new ByRoleAttributeTraverseSpecification( roleAttributeName, null, null, null );
    }

    /**
     * Factory method for a RoleAttributeTraverseSpecification that matches
     * RoleAttributes with a certain name at the source.
     *
     * @param roleAttributeName name of the RoleAttribute
     * @param sourceAttributePredicate evaluates whether the value of the RoleAttribute at the source matches the requirement, or null
     * @return the created object
     */
    public static ByRoleAttributeTraverseSpecification createSource(
            String                  roleAttributeName,
            Predicate<Serializable> sourceAttributePredicate )
    {
        return new ByRoleAttributeTraverseSpecification( roleAttributeName, sourceAttributePredicate, null, null );
    }

    /**
     * Factory method for a RoleAttributeTraverseSpecification that matches
     * RoleAttributes with a certain name at the neighbor.
     *
     * @param roleAttributeName name of the RoleAttribute
     * @return the created object
     */
    public static ByRoleAttributeTraverseSpecification createDestination(
            String roleAttributeName )
    {
        return new ByRoleAttributeTraverseSpecification( null, null, roleAttributeName, null );
    }

    /**
     * Factory method for a RoleAttributeTraverseSpecification that matches
     * RoleAttributes with a certain name at the neighbor.
     *
     * @param roleAttributeName name of the RoleAttribute
     * @param neighborAttributePredicate evaluates whether the value of the RoleAttribute at the neighbor matches the requirement, or null
     * @return the created object
     */
    public static ByRoleAttributeTraverseSpecification createDestination(
            String                  roleAttributeName,
            Predicate<Serializable> neighborAttributePredicate )
    {
        return new ByRoleAttributeTraverseSpecification( null, null, roleAttributeName, neighborAttributePredicate );
    }

    /**
     * Factory method for a RoleAttributeTraverseSpecification that matches
     * RoleAttributes with a certain name at the source and one at the neighbor.
     *
     * @param sourceRoleAttributeName name of the required RoleAttribute at the source, or null
     * @param sourceAttributePredicate evaluates whether the value of the RoleAttribute at the source matches the requirement, or null
     * @param neighborRoleAttributeName name of the required RoleAttribute at the neighbor, or null
     * @param neighborAttributePredicate evaluates whether the value of the RoleAttribute at the neighbor matches the requirement, or null
     * @return the created object
     */
    public static ByRoleAttributeTraverseSpecification createBoth(
            String                  sourceRoleAttributeName,
            Predicate<Serializable> sourceAttributePredicate,
            String                  neighborRoleAttributeName,
            Predicate<Serializable> neighborAttributePredicate )
    {
        return new ByRoleAttributeTraverseSpecification( sourceRoleAttributeName, sourceAttributePredicate, neighborRoleAttributeName, neighborAttributePredicate );
    }

    /**
     * Constructor.
     *
     * @param sourceRoleAttributeName name of the required RoleAttribute at the source, or null
     * @param sourceAttributePredicate evaluates whether the value of the RoleAttribute at the source matches the requirement, or null
     * @param neighborRoleAttributeName name of the required RoleAttribute at the neighbor, or null
     * @param neighborAttributePredicate evaluates whether the value of the RoleAttribute at the neighbor matches the requirement, or null
     */
    protected ByRoleAttributeTraverseSpecification(
            String                  sourceRoleAttributeName,
            Predicate<Serializable> sourceAttributePredicate,
            String                  neighborRoleAttributeName,
            Predicate<Serializable> neighborAttributePredicate )
    {
        theSourceName                 = sourceRoleAttributeName;
        theSourceAttributePredicate   = sourceAttributePredicate;
        theDestName                   = neighborRoleAttributeName;
        theNeighborAttributePredicate = neighborAttributePredicate;
    }

    /**
     * Obtain the source RoleAttribute, if any.
     * 
     * @return the name of the RoleAttribute
     */
    public String getSourceRoleAttributeName()
    {
        return theSourceName;
    }

    /**
     * Obtain the destination RoleAttribute, if any.
     * 
     * @return the name of the RoleAttribute
     */
    public String getDestinationRoleAttributeName()
    {
        return theDestName;
    }

    /**
     * Convenience method to determine whether this ByRoleAttributeTraverseSpecification
     * only looks at the name of the source RoleAttribute.
     * 
     * @return true if all filter criteria are null other than the name of the source RoleAttribute
     */
    public boolean onlySelectsBySourceRoleAttributeName()
    {
        return     theSourceName != null
                && theSourceAttributePredicate == null
                && theDestName == null
                && theNeighborAttributePredicate == null;
    }

    /**
     * Convenience method to determine whether this ByRoleAttributeTraversalSpecification
     * only looks at the name of the destination RoleAttribute.
     * 
     * @return true if all filter criteria are null other than the name of the destination RoleAttribute
     */
    public boolean onlySelectsByDestinationRoleAttributeName()
    {
        return     theSourceName == null
                && theSourceAttributePredicate == null
                && theDestName != null
                && theNeighborAttributePredicate == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObject start )
    {
        MeshObjectSet ret = start.traverseToNeighbors();
        if( theSourceName != null ) {
            ret = ret.subset( (MeshObject neighbor) -> start.hasRoleAttribute( neighbor, theSourceName ));
            if( theSourceAttributePredicate != null ) {
                ret = ret.subset( (MeshObject neighbor) -> theSourceAttributePredicate.test( start.getRoleAttributeValue( neighbor, theSourceName )));
            }
        }
        if( theDestName != null ) {
            ret = ret.subset( (MeshObject neighbor) -> neighbor.hasRoleAttribute( start, theDestName ));
            if( theNeighborAttributePredicate != null ) {
                ret = ret.subset( (MeshObject neighbor) -> theNeighborAttributePredicate.test( neighbor.getRoleAttributeValue( start, theDestName )));
            }
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObjectSet set )
    {
        MeshObjectSet ret = MeshObjectSet.DEFAULT_EMPTY_SET;
        for( MeshObject start : set ) {
            MeshObjectSet toAdd = start.traverse( this );
            if( ret == MeshObjectSet.DEFAULT_EMPTY_SET ) {
                ret = toAdd;
            } else {
                ret = ret.union( toAdd );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAffectedBy(
            MeshBase meshBase,
            AbstractMeshObjectRoleTypeChange theEvent )
    {
        return false;
    }

    protected final String theSourceName;
    protected final Predicate<Serializable> theSourceAttributePredicate;
    protected final String theDestName;
    protected final Predicate<Serializable> theNeighborAttributePredicate;
}
