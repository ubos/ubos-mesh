//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.mesh.MeshObject;

/**
  * <p>A MeshType that does not depend on the existence of others, that can participate in
  * inheritance relationships, and may be attributed. This is often called Class (or MetaClass)
  * or Entity in the literature.
  *
  * <p>An EntityType may be abstract, which means that it cannot be instantiated.
  */
public interface EntityType
        extends
            MeshTypeWithProperties
{
    /**
     * Obtain the identifiers by which this EntityType is also known.
     *
     * @return the identifiers by which this EntityType is also known
     */
    public MeshTypeIdentifier [] getSynonyms();

    /**
     * Obtain all local RoleTypes in which this EntityType participates in the
     * working model. Local RoleTypes are those which identify this EntityType
     * as the source or destination of a RelationshipType, rather than one of its
     * supertypes.
     *
     * By definition (see definition of "working model"), not all RoleTypes of an
     * EntityType may be known at any point in time; only the ones
     * currently known in the working model are returned.
     *
     * @return the local RoleTypes in which this EntityType participates in the working model
     */
    public RoleType [] getLocalRoleTypes();

    /**
     * Obtain all RoleTypes in which this EntityType participates in the
     * working model.
     * This includes all RoleTypes which identify this EntityType,
     * or one of its supertypes as the source or destination of a RelationshipType.
     *
     * By definition (see definition of "working model"), not all RoleTypes of an
     * EntityType may be known at any point in time; only the ones
     * currently known in the working model are returned.
     *
     * @return all RoleTypes in which this EntityType or one of its supertypes
     *         participates in the working model
     */
    public RoleType [] getRoleTypes();

    /**
     * Obtain all RoleTypes in which this EntityType participates in the
     * working model, ordered by user-visible name.
     * This includes all RoleTypes which identify this EntityType,
     * or one of its supertypes as the source or destination of a RelationshipType.
     *
     * By definition (see definition of "working model"), not all RoleTypes of an
     * EntityType may be known at any point in time; only the ones
     * currently known in the working model are returned.
     *
     * @return all RoleTypes in which this EntityType or one of its supertypes
     *         participates in the working model
     */
    public RoleType [] getOrderedRoleTypes();

    /**
     * Obtain all RoleTypes in which this EntityType participates in the
     * working model, and that belong to RelationshipTypes which are not abstract.
     * This includes all RoleTypes which identify this EntityType, or one of its supertypes
     * as the source or destination of a RelationshipType.
     *
     * By definition (see definition of "working model"), not all RoleTypes of an
     * EntityType may be known at any point in time; only the ones
     * currently known in the working model are returned.
     *
     * @return all RoleTypes in which this EntityType or one of its supertypes
     *         participates in the working model,
     *         and which belong to RelationshipTypes which are not abstract
     */
    public RoleType [] getConcreteRoleTypes();

    /**
     * Obtain all RoleTypes in which this EntityType participates in the
     * working model, and that belong to RelationshipTypes which are not abstract,
     * ordered by UserVisibleName.
     * This includes all RoleTypes which identify this EntityType, or one of its supertypes
     * as the source or destination of a RelationshipType.
     *
     * By definition (see definition of "working model"), not all RoleTypes of an
     * EntityType may be known at any point in time; only the ones
     * currently known in the working model are returned.
     *
     * @return all RoleTypes in which this EntityType or one of its supertypes
     *         participates in the working model,
     *         and which belong to RelationshipTypes which are not abstract
     */
    public RoleType [] getOrderedConcreteRoleTypes();

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getDirectSupertypes();

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getSupertypes();

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getDirectSubtypes();

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getSubtypes();

    /**
     * Obtain the transitive closure of all subtypes of this EntityType
     * in the working model, and return those that are instantiable (as opposed
     * to those that are abstract).
     *
     * By definition (see definition of "working model"), not all subtypes of the
     * EntityType may be known at any point in time; only the ones
     * currently known in the working model are returned.
     *
     * @return the transitive closure of all known subtypes of this EntityType
     */
    public EntityType [] getConcreteSubtypes();

    /**
     * Determine whether this EntityType may be used as a ForwardReference.
     *
     * @return true if this may be used as a ForwardReference
     */
    public BooleanValue getMayBeUsedAsForwardReference();

    /**
      * Obtain the value of the IsAbstract property.
      *
      * @return the value of the IsAbstract property
      */
    public BooleanValue getIsAbstract();

    /**
     * Determine whether the passed-in MeshObject is an instance of this EntityType
     * or one of its subtypes. This is modeled after a similar method on java.lang.Class.
     *
     * @param candidate MeshObject the MeshObject to test against
     * @return true if candidate is an instance of this EntityTypes, or one of its subtypes
     */
    public boolean isInstance(
            MeshObject candidate );

    /**
     * Convenience constant.
     */
    public static final EntityType [] EMPTY_ARRAY = {};
}
