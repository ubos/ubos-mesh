//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;
import net.ubos.util.ArrayHelper;

/**
 * This implements a compound TraverseSpecification as a sequence of TraverseSpecifications
 * that are being traversed in sequence.
 */
public class SequentialCompoundTraverseSpecification
        extends
            AbstractTraverseSpecification
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method to construct a SequentialCompoundTraverseSpecification
     * from a sequence of TraverseSpecifications.
     *
     * @param steps the sequence of TraverseSpecifications
     * @return the created SequentialCompoundTraverseSpecification
     */
    public static SequentialCompoundTraverseSpecification create(
            TraverseSpecification [] steps )
    {
        // we are paranoid, so we check
        if( steps == null || steps.length < 2 ) {
            throw new IllegalArgumentException( ArrayHelper.arrayToString( steps ) + " must be at least of length 2" );
        }

        return new SequentialCompoundTraverseSpecification( steps );
    }

    /**
     * Convenience factory method for a sequence of two TraverseSpecifications.
     *
     * @param step1 the first traversal
     * @param step2 the first traversal
     * @return the created SequentialCompoundTraverseSpecification
     */
    public static SequentialCompoundTraverseSpecification create(
            TraverseSpecification step1,
            TraverseSpecification step2 )
    {
        return create( new TraverseSpecification[] { step1, step2 } );
    }

    /**
     * Convenience factory method for a sequence of three TraverseSpecifications.
     *
     * @param step1 the first traversal
     * @param step2 the first traversal
     * @param step3 the first traversal
     * @return the created SequentialCompoundTraverseSpecification
     */
    public static SequentialCompoundTraverseSpecification create(
            TraverseSpecification step1,
            TraverseSpecification step2,
            TraverseSpecification step3 )
    {
        return create( new TraverseSpecification[] { step1, step2, step3 } );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param steps the sequence of TraverseSpecifications
     */
    private SequentialCompoundTraverseSpecification(
            TraverseSpecification [] steps )
    {
        for( int i=0 ; i<steps.length ; ++i ) {
            if( steps[i] == null ) {
                throw new NullPointerException( "Step " + i + " in SequentialCompoundTraverseSpecification is null" );
            }
        }
        theSteps = steps;
    }

    /**
     * Obtain the first step.
     *
     * @return the first step
     */
    public TraverseSpecification getFirstStep()
    {
        return theSteps[0];
    }

    /**
     * Obtain all steps.
     *
     * @return all steps
     */
    public TraverseSpecification [] getSteps()
    {
        return theSteps;
    }

    /**
     * Obtain the number of steps in this SequentialComopoundTraverseSpecification.
     *
     * @return the number of steps in this SequentialComopoundTraverseSpecification
     */
    public int getStepCount()
    {
        return theSteps.length;
    }

    /**
     * Obtain another TraverseSpecification that is just like this
     * but without the first step.
     *
     * @return the same TraverseSpecification as this one without the first step
     */
    public TraverseSpecification withoutFirstStep()
    {
        switch( theSteps.length ) {
            case 2:
                return theSteps[1];

            default:
                return new SequentialCompoundTraverseSpecification(
                        ArrayHelper.copyIntoNewArray(
                                theSteps,
                                1,
                                theSteps.length,
                                TraverseSpecification.class ));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObject start )
    {
        MeshObjectSet current = theSteps[0].traverse( start );
        for( int i=1 ; i<theSteps.length ; ++i ) {
                current = current.traverse( theSteps[i] );
            }
        return current;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObjectSet theSet )
    {
        MeshObjectSet current = theSet;

        for( int i=0 ; i<theSteps.length ; ++i ) {
            current = current.traverse( theSteps[i] );
        }
        return current;
    }

    /**
     * Determine equality.
     *
     * @param other the object to test against
     * @return true if the two objects are equal
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof SequentialCompoundTraverseSpecification ) {
            SequentialCompoundTraverseSpecification realOther = (SequentialCompoundTraverseSpecification) other;

            if( theSteps.length != realOther.theSteps.length ) {
                return false;
            }

            for( int i=0 ; i<theSteps.length ; ++i ) {
                if( !theSteps[i].equals( realOther.theSteps[i] )) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Hash code.
     *
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        for( TraverseSpecification current : theSteps ) {
            ret ^= current.hashCode();
        }
        return ret;
    }

    /**
     * Determine whether a given event, with a source of from where we traverse the
     * TraverseSpecification, may affect the result of the traversal.
     *
     * @param theEvent the event that we consider
     * @return true if this event may affect the result of traversing from the MeshObject
     *         that sent this event
     */
    @Override
    public boolean isAffectedBy(
            MeshBase                  meshBase,
            AbstractMeshObjectRoleTypeChange theEvent )
    {
        return getFirstStep().isAffectedBy( meshBase, theEvent );
    }

    /**
     * The sequence of steps to traverse.
     */
    protected TraverseSpecification [] theSteps;
}
