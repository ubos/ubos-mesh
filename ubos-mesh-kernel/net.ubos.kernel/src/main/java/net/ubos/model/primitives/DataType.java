//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.Serializable;
import net.ubos.util.logging.CanBeDumped;

/**
  * This represents a data type for properties. This is an abstract class;
  * a more suitable subclass needs to be used instead.
  * A DataType can refine another DataType and usually does.
  *
  * Note: This is different from a value for properties:
  * Instances of (a subtype of) DataType represent the notion of a type,
  * such as "a positive integer", while instances of (a subtype of)
  * PropertyValue represent a value, such as "7".
  */
public abstract class DataType
        implements
            CanBeDumped,
            Serializable
{
    /**
     * Constructor for subclasses only.
     *
     * @param supertype the DataType that we refine, if any
     */
    protected DataType(
            DataType supertype )
    {
        theSupertype = supertype;
    }

    /**
     * Obtain the name of the DataType.
     *
     * @return the name of the DataType
     */
    public final String getName()
    {
        return this.getClass().getName();
    }

    /**
      * Test whether this DataType is a superset of or equals the argument
      * DataType. This is useful to test whether an assignment of values
      * is legal prior to attempting to do it. By default, this delegates
      * to the equals method.
      *
      * @param other the DataType we test against
      * @return if true, this DataType is a superset or equals the argument DataType
      */
    public boolean isSupersetOfOrEquals(
            DataType other )
    {
        return equals( other );
    }

    /**
     * Determine whether this PropertyValue conforms to the constraints of this
     * instance of DataType.
     *
     * @param value the candidate PropertyValue
     * @return 0 if the candidate PropertyValue conforms to this type. Non-zero values depend
     *         on the DataType; generally constructed by analogy with the return value of strcmp.
     * @throws ClassCastException if this PropertyValue has the wrong type (e.g.
     *         the PropertyValue is a StringValue, and the DataType an IntegerDataType)
     */
    public abstract int conforms(
            PropertyValue value )
        throws
            ClassCastException;

    /**
     * Determine whether a domain check shall be performed on
     * assignments. Default to false.
     *
     * @return if true, a domain check shall be performed prior to performing assignments
     */
    public boolean getPerformDomainCheck()
    {
        return false;
    }

    /**
     * Return a boolean expression in the Java language that uses
     * varName as an argument and that evaluates whether the content
     * of variable varName is assignable to a value of this data type.
     * For example, on an integer data type with minimum and maximum
     * values, one would call it with argument "theValue", and it could
     * return: "( theValue > 2 && theValue < 7 )", where "2" and "7" are
     * the values of the min and max attributes of the integer data type.
     *
     * This is used primarily for code-generation purposes.
     *
     * @param varName the name of the variable containing the value
     * @return the Java language  expression
     */
    public String getJavaDomainCheckExpression(
            String varName )
    {
        return "true";
    }

    /**
      * Obtain the Java class that can hold values of this data type.
      *
      * @return the Java class that can hold values of this data type
      */
    public abstract Class<? extends PropertyValue> getCorrespondingJavaClass();

    /**
     * Obtain a value expression in the Java language that invokes the constructor
     * of factory method of the underlying concrete class, thereby creating or
     * reusing an instance of the underlying concrete class that is identical
     * to the instance on which this method was invoked.
     *
     * This is used primarily for code-generation purposes.
     *
     * @param classLoaderVar name of a variable containing the class loader to be used to initialize this value
     * @return the Java language expression
     */
    public abstract String getJavaConstructorString(
            String classLoaderVar );

    /**
      * Instantiate this data type into a PropertyValue with a
      * reasonable default value.
      *
      * @return a PropertyValue with a reasonable default value that is an instance of this DataType
      */
    public final PropertyValue instantiate()
    {
        return getDefaultValue();
    }

    /**
     * Obtain the default value of this DataType.
     *
     * @return the default value of this DataType
     */
    public abstract PropertyValue getDefaultValue();

    /**
     * If this DataType is a refinement of another, find which DataType it refines.
     *
     * @return the refined DataType, if any
     */
    public final DataType getSupertype()
    {
        return theSupertype;
    }

    /**
     * Parse a user-visible String representation of a corresponding PropertyValue.
     * This must match the various PropertyValue.toUserVisibleString() implementations.
     * 
     * @param raw the String
     * @param mime the MIME type of the raw data. This is only used for BlobDataTypes.
     * @param pt the PropertyType whose instance is being parsed
     * @return the PropertyValue
     * @throws PropertyValueParsingException thrown if the string could not be parsed
     */
    public abstract PropertyValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException;

    /**
     * The supertype of this DataType (if any).
     */
    protected DataType theSupertype;

    /**
     * String constant used in our subclasses in order to avoid using up more memory than necessary.
     */
    public static final String CREATE_STRING = ".create( ";

    /**
     * String constant used in our subclasses in order to avoid using up more memory than necessary.
     */
    public static final String COMMA_STRING = ", ";

    /**
     * String constant used in our subclasses in order to avoid using up more memory than necessary.
     */
    public static final String NULL_STRING = "null";

    /**
     * String constant used in our subclasses in order to avoid using up more memory than necessary.
     */
    public static final String QUOTE_STRING = "\"";

    /**
     * String constant used in our subclasses in order to avoid using up more memory than necessary.
     */
    public static final String DEFAULT_STRING = ".theDefault";

    /**
     * String constant used in our subclasses in order to avoid using up more memory than necessary.
     */
    public static final String CLOSE_PARENTHESIS_STRING = " )";

    /**
     * The default entry in the resource files for a DataType, prefixed by the StringRepresentation's prefix.
     */
    public static final String DEFAULT_ENTRY = "Type";
}
