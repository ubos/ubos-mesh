//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
  * An integer number PropertyValue. It can carry a unit.
  *
  * FIXME: units not tested.
  */
public final class IntegerValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method.
     *
     * @param value the integer value
     * @return the created IntegerValue
     */
    public static IntegerValue create(
            long value )
    {
        return new IntegerValue( value, null );
    }

    /**
     * Factory method.
     *
     * @param value the integer value
     * @param u the unit for the integer value
     * @return the created IntegerValue
     */
    public static IntegerValue create(
            long  value,
            Unit  u )
    {
        return new IntegerValue( value, u );
    }

    /**
     * Factory method.
     *
     * @param value the integer value
     * @return the created IntegerValue
     * @throws IllegalArgumentException if null is given as argument
     */
    public static IntegerValue create(
            Number value )
        throws
            IllegalArgumentException
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new IntegerValue( value.longValue(), null );
    }

    /**
     * Factory method.
     *
     * @param value the integer value
     * @param u the unit for the integer value
     * @return the created IntegerValue
     * @throws IllegalArgumentException if null is given as argument for the value
     */
    public static IntegerValue create(
            Number value,
            Unit   u )
        throws
            IllegalArgumentException
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new IntegerValue( value.longValue(), u );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the integer value
     * @return the created IntegerValue
     */
    public static IntegerValue createOrNull(
            Number value )
    {
        if( value == null ) {
            return null;
        }
        return new IntegerValue( value.longValue(), null );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the integer value
     * @param u the unit for the integer value
     * @return the created IntegerValue
     */
    public static IntegerValue createOrNull(
            Number value,
            Unit   u )
    {
        if( value == null ) {
            return null;
        }
        return new IntegerValue( value.longValue(), u );
    }

    /**
      * Private constructor, use factory methods.
      *
      * @param value the integer value
      * @param u the unit for the integer value
      */
    private IntegerValue(
            long value,
            Unit u )
    {
        this.theValue = value;
        this.theUnit  = u;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getValue()
    {
        return value();
    }

    /**
     * Convert value back to long.
     *
     * @return the value as long
     */
    public long longValue()
    {
        return theValue;
    }

    /**
     * Convenience method along the lines of Number.doubleValue.
     *
     * @return the value as double
     */
    public Double doubleValue()
    {
        return Double.valueOf( theValue );
    }

    /**
     * Obtain Unit, if any.
     *
     * @return the Unit, if any
     */
    public Unit getUnit()
    {
        return theUnit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegerDataType getDataType()
    {
        return IntegerDataType.theDefault;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( ! ( otherValue instanceof IntegerValue )) {
            return false;
        }

        IntegerValue realOtherValue = (IntegerValue) otherValue;

        if( theUnit == null ) {
            if( realOtherValue.theUnit != null ) {
                return false;
            }
            return theValue == realOtherValue.theValue;

        } else {
            if( realOtherValue.theUnit == null ) {
                return false;
            }

            if( ! theUnit.getFamily().equals( realOtherValue.theUnit.getFamily() )) {
                return false;
            }

            return theValue * theUnit.getPrefix() == realOtherValue.theValue * realOtherValue.theUnit.getPrefix();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        // auto-generated by NetBeans
        int hash = 7;
        hash = 41 * hash + (int) (theValue ^ ( theValue >>> 32 ));
        hash = 41 * hash + ( theUnit != null ? theUnit.hashCode() : 0 );
        return hash;
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is smaller than, or the same as otherValue
     */
    public boolean isSmallerOrEquals(
            IntegerValue otherValue )
    {
        if( theUnit == null ) {
            if( otherValue.theUnit != null ) {
                return false;
            }

            return theValue <= otherValue.theValue;
        } else {
            if( otherValue.theUnit == null ) {
                return false;
            }

            if( ! theUnit.getFamily().equals( otherValue.theUnit.getFamily() )) {
                return false;
            }

            return theValue * theUnit.getPrefix() <= otherValue.theValue * otherValue.theUnit.getPrefix();
        }
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is smaller than otherValue
     */
    public boolean isSmaller(
            IntegerValue otherValue )
    {
        if( theUnit == null ) {
            if( otherValue.theUnit != null ) {
                return false;
            }
            return theValue < otherValue.theValue;

        } else {
            if( otherValue.theUnit == null ) {
                return false;
            }

            if( ! theUnit.getFamily().equals( otherValue.theUnit.getFamily() )) {
                return false;
            }

            return theValue * theUnit.getPrefix() < otherValue.theValue * otherValue.theUnit.getPrefix();
        }
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is larger, or the same, as otherValue
     */
    public boolean isLargerOrEquals(
            IntegerValue otherValue )
    {
        return otherValue.isSmallerOrEquals( this );
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is larger than otherValue
     */
    public boolean isLarger(
            IntegerValue otherValue )
    {
        return otherValue.isSmaller( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        if( theUnit != null ) {
            return String.valueOf( theValue ) + " " + theUnit.toString();
        } else {
            return String.valueOf( theValue );
        }
    }

    /**
     * This attempts to parse a string and turn it into a integer value similarly
     * to Integer.parseInt().
     *
     * FIXME: need to deal with unit
     *
     * @param theString the string that shall be parsed
     * @return the created IntegerValue
     * @throws NumberFormatException thrown if theString does not follow the correct syntax
     */
    public static IntegerValue parseIntegerValue(
            String theString )
        throws
            NumberFormatException
    {
        return IntegerValue.create( Long.parseLong( theString ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        StringBuilder buf = new StringBuilder( 64 );
        buf.append( getClass().getName() );
        buf.append( ".create( " );
        buf.append( theValue );
        buf.append( "L" ); // be safe
        if( theUnit != null ) {
            buf.append( ", " );
            buf.append( theUnit.getJavaConstructorString() );
        }
        buf.append( " )" );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        IntegerValue realOther = (IntegerValue) o;

        if( theValue < realOther.theValue ) {
            return -1;
        } else if( theValue == realOther.theValue ) {
            return 0;
        } else {
            return +1;
        }
    }

    /**
      * The actual value.
      */
    protected long theValue;

    /**
      * The Unit, if any.
      */
    protected Unit theUnit;
}
