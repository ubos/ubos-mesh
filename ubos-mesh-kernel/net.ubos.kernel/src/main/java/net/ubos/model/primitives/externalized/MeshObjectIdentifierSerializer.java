//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import net.ubos.mesh.MeshObjectIdentifier;

/**
 * Knows how to serialize a MeshObjectIdentifier into a String.
 */
public interface MeshObjectIdentifierSerializer
{
    /**
     * Convert the MeshObjectIdentifier to String form.
     *
     * @param identifier the MeshObjectIdentifier to serialize
     * @return String form
     */
    public String toExternalForm(
            MeshObjectIdentifier identifier );
}
