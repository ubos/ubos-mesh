//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.IntegerValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies IntegerValues.
 */
public class IntegerValueStringifier
        extends
            AbstractStringifier<IntegerValue>
{
    /**
     * Factory method.
     *
     * @return the created StringValueStringifier
     */
    public static IntegerValueStringifier create()
    {
        return new IntegerValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected IntegerValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            IntegerValue          arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = String.valueOf( arg.value() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (IntegerValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegerValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            return IntegerValue.parseIntegerValue( rawString );

        } catch( NumberFormatException ex ) {
            throw new StringifierParseException( this, rawString, ex );
        }
    }
}

