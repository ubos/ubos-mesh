//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import java.io.IOException;
import java.io.Reader;
import net.ubos.mesh.externalized.Decoder;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.PropertyValue;

/**
 * Type supported by objects that know how to decode PropertyValues.
 */
public interface PropertyValueDecoder
    extends
        Decoder
{
    /**
     * Decode a PropertyValue from a Reader
     *
     * @param type the DataType for which this will be a value
     * @param r where to read from
     * @return the PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException thrown if an I/O problem occurred
     */
    public PropertyValue parsePropertyValue(
            DataType type,
            Reader   r )
        throws
            DecodingException,
            IOException;
}
