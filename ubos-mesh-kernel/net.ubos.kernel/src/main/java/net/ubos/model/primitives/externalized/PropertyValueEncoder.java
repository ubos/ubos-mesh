//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import java.io.IOException;
import java.io.Writer;
import net.ubos.mesh.externalized.Encoder;
import net.ubos.model.primitives.PropertyValue;

/**
 * Type supported by objects that know how to encode PropertyValues.
 */
public interface PropertyValueEncoder
    extends
        Encoder
{
    /**
     * Serialize a PropertyValue to a Writer.
     *
     * @param value the PropertyValue
     * @param w the Writer to which to write the PropertyValue
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    public void writePropertyValue(
            PropertyValue value,
            Writer        w )
        throws
            EncodingException,
            IOException;
}
