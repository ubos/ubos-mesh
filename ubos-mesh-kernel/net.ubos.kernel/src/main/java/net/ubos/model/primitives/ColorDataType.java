//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.util.logging.Dumper;

/**
  * A DataType representing colors.
  */
public final class ColorDataType
        extends
            DataType
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
      * This is the default instance of this class.
      */
    public static final ColorDataType theDefault = new ColorDataType();

    /**
     * Factory method. Always returns the same instance.
     *
     * @return the default instance of this class
     */
    public static ColorDataType create()
    {
        return theDefault;
    }

    /**
      * Private constructor, there is no reason to instantiate this more than once.
      */
    private ColorDataType()
    {
        super( null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        return other instanceof ColorDataType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        ColorValue realValue = (ColorValue) value; // may throw

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<ColorValue> getCorrespondingJavaClass()
    {
        return ColorValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ColorValue getDefaultValue()
    {
        return theDefaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        return className + DEFAULT_STRING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ColorValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        ColorValue ret;

        raw = raw.trim();
        if( raw.startsWith( "#" )) {
            ret = ColorValue.create( Integer.parseInt( raw.substring( 1 )));

        } else {
            Matcher m = PARSING_REGEX.matcher( raw );
            if( !m.matches() ) {
                throw new PropertyValueParsingException( this, raw );

            } else if( m.groupCount() == 3 ) {
                ret = ColorValue.create(
                        Integer.parseInt( m.group(1) ),
                        Integer.parseInt( m.group(2) ),
                        Integer.parseInt( m.group(3) ));
            } else {
                ret = ColorValue.create(
                        Integer.parseInt( m.group(1) ),
                        Integer.parseInt( m.group(2) ),
                        Integer.parseInt( m.group(3) ),
                        Integer.parseInt( m.group(4) ));
            }
        }
        return ret;
    }

    /**
     * The default value.
     */
    private static final ColorValue theDefaultValue = ColorValue.create( 0 );
    
    /**
     * Parsing pattern.
     */
    protected static final Pattern PARSING_REGEX = Pattern.compile(
            "(?:\\(\\s*)?"   // optional leading ( + white
            + "(\\d+)"
            + "\\s*"
            + "[,;]"
            + "\\s*"
            + "(\\d+)"
            + "\\s*"
            + "[,;]"
            + "\\s*"
            + "(\\d+)"
            + "(?:"
                + "\\s*"
                + "[,;]"
                + "\\s*"
                + "(\\d+)"
            + ")"
            + "(?:\\))?" ); // optional trailing white + )
}
