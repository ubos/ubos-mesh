//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.Serializable;

/**
  * This is the abstract supertype for values of all supported DataTypes.
  *
  * This also implements Comparable. In addition to the conventions about return values from
  * the Comparable.compareTo method, subclasses of PropertyValues return 2 if the two
  * comparison operands are not comparable.
  */
public abstract class PropertyValue
        implements
            Serializable,
            Comparable<PropertyValue>
{
    /**
     * Obtain the default user-visible String for this value.
     * 
     * @return the String
     */
    public abstract String getUserVisibleString();

    /**
     * Obtain a string which is the Java-language constructor expression reflecting this value.
     * For example, a StringValue with value "my foo" would return
     *     new StringValue( "my foo" )
     * as the result.
     * This is mainly for code-generation purposes.
     *
     * @param classLoaderVar name of a variable containing the class loader to be used to initialize this value
     * @param typeVar  name of the variable containing the DataType that goes with the to-be-created instance.
     * @return the Java-language constructor expression
     */
    public abstract String getJavaConstructorString(
            String classLoaderVar,
            String typeVar );

    /**
     * Obtain the underlying value. This is implemented with more specific subtypes in subclasses.
     *
     * @return the underlying value
     */
    public abstract Object value();

    /**
     * Obtain the underlying value. This is implemented with more specific subtypes in subclasses.
     * Same as value(), for JavaBeans-aware software.
     *
     * @return the underlying value
     */
    public abstract Object getValue();

    /**
     * Obtain the DataType to which this PropertyValue belongs. This may return a wider,
     * less restrictive DataType than expected.
     *
     * @return the DataType
     */
    public abstract DataType getDataType();

    /**
     * This is a convenience comparison method in case one of the arguments may be null.
     *
     * @param one the first PropertyValue or null to be compared
     * @param two the second PropertyValue or null to be compared
     * @return 0 if the two values are equal, +1/-1 if the second is smaller/larger than the first,
     *         +2/-2 if one of them is null
     * @throws ClassCastException if the PropertyValue are of a different type
     */
    public static int compare(
            PropertyValue one,
            PropertyValue two )
        throws
            ClassCastException
    {
        if( one == null ) {
            if( two == null ) {
                return 0;
            } else {
                return -2;
            }
        } else {
            if( two == null ) {
                return +2;
            } else {
                int temp = one.compareTo( two );
                if( temp > 0 ) {
                    return +1;
                } else if( temp == 0 ) {
                    return 0;
                } else {
                    return -1;
                }
            }
        }
    }

    /**
     * Convenience constant.
     */
    public static final PropertyValue [] EMPTY_ARRAY = {};
}
