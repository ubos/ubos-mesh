//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.Stringifier;
import net.ubos.util.text.StringifierException;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies the domain of EnumeratedDataTypes.
 */
public class EnumeratedDataTypeDomainStringifier
        extends
            AbstractStringifier<EnumeratedDataType>
{
    /**
     * Factory method.
     *
     * @param delegate the Stringifier responsible for stringifying the contained EnumeratedValues
     * @return the created EnumeratedValueStringifier
     */
    public static EnumeratedDataTypeDomainStringifier create(
            Stringifier<EnumeratedValue> delegate )
    {
        return new EnumeratedDataTypeDomainStringifier( delegate, null, null, null );
    }

    /**
     * Factory method.
     *
     * @param middle the string to insert in the middle
     * @param delegate the Stringifier responsible for stringifying the contained EnumeratedValues
     * @return the created EnumeratedValueStringifier
     */
    public static EnumeratedDataTypeDomainStringifier create(
            Stringifier<EnumeratedValue> delegate,
            String                       middle )
    {
        return new EnumeratedDataTypeDomainStringifier( delegate, null, middle, null );
    }

    /**
     * Factory method.
     *
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     * @param delegate the Stringifier responsible for stringifying the contained EnumeratedValues
     * @return the created EnumeratedValueStringifier
     */
    public static EnumeratedDataTypeDomainStringifier create(
            Stringifier<EnumeratedValue> delegate,
            String                       start,
            String                       middle,
            String                       end )
    {
        return new EnumeratedDataTypeDomainStringifier( delegate, start, middle, end );
    }

    /**
     * Private constructor for subclasses only, use factory method.
     *
     * @param delegate the Stringifier responsible for stringifying the contained EnumeratedValues
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     */
    protected EnumeratedDataTypeDomainStringifier(
            Stringifier<EnumeratedValue> delegate,
            String                       start,
            String                       middle,
            String                       end )
    {
        theDelegate = delegate;
        theStart    = start;
        theMiddle   = middle;
        theEnd      = end;
    }

    /**
     * Obtain the start String, if any.
     *
     * @return the start String, if any
     */
    public String getStart()
    {
        return theStart;
    }

    /**
     * Obtain the middle String, if any.
     *
     * @return the middle String, if any
     */
    public String getMiddle()
    {
        return theMiddle;
    }

    /**
     * Obtain the end String, if any.
     *
     * @return the end String, if any
     */
    public String getEnd()
    {
        return theEnd;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            EnumeratedDataType    arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        EnumeratedValue [] values = arg.getDomain();
        StringBuilder      ret    = new StringBuilder();
        String             sep    = theStart;

        for( int i=0 ; i<values.length ; ++i ) {
            if( sep != null ) {
                ret.append( sep );
            }
            String childInput = theDelegate.format( values[i], pars, soFar + ret.toString() ); // presumably shorter, but we don't know
            if( childInput != null ) {
                ret.append( childInput );
            }
            sep = theMiddle;
        }
        if( theEnd != null ) {
            ret.append( theEnd );
        }
        return potentiallyShorten( ret.toString(), pars );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException
    {
        return format( (EnumeratedDataType) arg, pars, soFar );
    }

    /**
     * The underlying Stringifier responsible for stringifying the contained EnumeratedValues.
     */
    protected Stringifier<EnumeratedValue> theDelegate;

    /**
     * The String to insert at the beginning. May be null.
     */
    protected String theStart;

    /**
     * The String to insert when joining two elements. May be null.
     */
    protected String theMiddle;

    /**
     * The String to append at the end. May be null.
     */
    protected String theEnd;
}
