//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.xml;

import org.xml.sax.Locator;

/**
 * Thrown when an unknown XML token was encountered during parsing of a Model or Mesh XML file.
 */
public class UnknownTokenException
    extends
        LocalizedSAXParseException
{
    /**
     * Constructor.
     *
     * @param namespaceUri the namespace URI of the unknown token
     * @param token the unknown token
     * @param locator knows the location of the problem
     */
    public UnknownTokenException(
            String  namespaceUri,
            String  token,
            Locator locator )
    {
        super( locator );

        theNamespaceUri = namespaceUri;
        theToken        = token;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            getSystemId(),
            getPublicId(),
            getLineNumber(),
            getColumnNumber(),
            theNamespaceUri,
            theToken
        };
    }

    /**
     * The namespace URI of the unknown token
     */
    protected final String theNamespaceUri;

    /**
     * The unknown token.
     */
    protected final String theToken;
}
