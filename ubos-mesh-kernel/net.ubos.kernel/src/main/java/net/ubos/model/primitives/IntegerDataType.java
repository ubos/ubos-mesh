//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import net.ubos.util.logging.Dumper;

/**
  * An integer number DataType for PropertyValues. Explicit minimum and maximum values
  * may be specified. This can carry a unit.
  */
public class IntegerDataType
        extends
            DataType
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
      * This is the default instance of this class. It represents
      * an int data type with no domain restriction.
      */
    public static final IntegerDataType theDefault
            = new IntegerDataType(
                    IntegerValue.create( Long.MIN_VALUE ),
                    IntegerValue.create( Long.MAX_VALUE ),
                    null,
                    null );

    /**
      * This is a default instance of this class. It represents
      * a positive or zero int data type.
      */
    public static final IntegerDataType thePositiveOrZeroDefault
            = new IntegerDataType(
                    IntegerValue.create( 0 ),
                    IntegerValue.create( Long.MAX_VALUE ),
                    null,
                    theDefault );

    /**
      * This is a default instance of this class. It represents
      * a positive int data type excluding zero.
      */
    public static final IntegerDataType thePositiveDefault
            = new IntegerDataType(
                    IntegerValue.create( 1 ),
                    IntegerValue.create( Long.MAX_VALUE ),
                    null,
                    thePositiveOrZeroDefault );

    /**
      * Construct one with minimum and maximum values (inclusive).
      * Use constants defined in java.lang.Integer to represent infinity.
      *
      * @param min the smallest allowed value
      * @param max the largest allowed value
      * @param superType the DataType that we refine, if any
      * @return the created IntegerDataType
      */
     public static IntegerDataType create(
            IntegerValue min,
            IntegerValue max,
            DataType     superType )
    {
        return new IntegerDataType( min, max, null, superType );
    }

    /**
      * Construct one with minimum and maximum values (inclusive) and a unit.
      * Use constants defined in java.lang.Integer to represent infinity.
      *
      * @param min the smallest allowed value
      * @param max the largest allowed value
      * @param u the unit
      * @param superType the DataType that we refine, if any
      * @return the created IntegerDataType
      */
     public static IntegerDataType create(
            IntegerValue min,
            IntegerValue max,
            UnitFamily   u,
            DataType     superType )
    {
        return new IntegerDataType( min, max, u, superType );
    }

    /**
     * Private constructor, use factory method instead.
     *
     * @param min the smallest allowed value
     * @param max the largest allowed value
     * @param u the unit
     * @param superType the DataType that we refine, if any
     */
    private IntegerDataType(
            IntegerValue min,
            IntegerValue max,
            UnitFamily   u,
            DataType     superType )
    {
        super( superType );

        this.theMin = min;
        this.theMax = max;

        this.theUnitFamily = u;

        if( theUnitFamily == null ) {
            if( theMin.getUnit() != null ) {
                throw new IllegalArgumentException( "Min value has unit, data type has none" );
            }
            if( theMax.getUnit() != null ) {
                throw new IllegalArgumentException( "Max value has unit, data type has none" );
            }
        } else {
            if( ! theMin.getUnit().getFamily().equals( theUnitFamily )) {
                throw new IllegalArgumentException( "Min value has wrong unit family" );
            }
            if( ! theMax.getUnit().getFamily().equals( theUnitFamily )) {
                throw new IllegalArgumentException( "Max value has wrong unit family" );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof IntegerDataType ) {
            IntegerDataType realOther = (IntegerDataType) other;
            return    ( theMin.equals( realOther.theMin ))
                   && ( theMax.equals(  realOther.theMax ))
                   && (   ( theUnitFamily == null && realOther.theUnitFamily == null )
                       || ( theUnitFamily != null && theUnitFamily.equals( realOther.theUnitFamily )));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        if( theMin != null ) {
            ret ^= theMin.hashCode();
        }
        if( theMax != null ) {
            ret ^= theMax.hashCode();
        }
        if( theUnitFamily != null ) {
            ret ^= theUnitFamily.hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSupersetOfOrEquals(
            DataType other )
    {
        if( other instanceof IntegerDataType ) {
            IntegerDataType realOther = (IntegerDataType) other;
            return    ( theMin.isSmallerOrEquals( realOther.theMin ))
                   && ( realOther.theMax.isSmallerOrEquals( theMax ))
                   && (    ( theUnitFamily == null && realOther.theUnitFamily == null )
                        || ( theUnitFamily != null && theUnitFamily.equals( realOther.theUnitFamily )));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        IntegerValue realValue = (IntegerValue) value; // may throw

        if( theMin.theValue > realValue.value() ) {
            return -1;
        }
        if( theMax.theValue < realValue.value() ) {
            return +1;
        }
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getPerformDomainCheck()
    {
        return ( theMin.theValue != Long.MIN_VALUE ) || ( theMax.theValue != Long.MAX_VALUE );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaDomainCheckExpression(
            String varName )
    {
        if( theMin.theValue == Long.MIN_VALUE ) {
            if( theMax.theValue == Long.MAX_VALUE ) {
                return "true";
            } else {
                return "( " + varName + ".value() <= " + String.valueOf( theMax ) + " )";
            }
        } else {
            if( theMax.theValue == Long.MAX_VALUE ) {
                return "( " + varName + ".value() >= " + String.valueOf(theMin.value() ) + " )";
            } else {
                return "(    ( " + varName + ".value() >= " + String.valueOf(theMin.value() ) + " )"
                   +    " && ( " + varName + ".value() <= " + String.valueOf(theMax.value() ) + " ) )";
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<IntegerValue> getCorrespondingJavaClass()
    {
        return IntegerValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegerValue getDefaultValue()
    {
        if( theUnitFamily == null ) {
            if( theMin.theValue <= 0 && theMax.theValue >= 0 ) {
                return IntegerValue.create( 0 );
            }
            return IntegerValue.create( theMin.theValue );
        } else {
            if( theMin.theValue <= 0 && theMax.theValue >= 0 ) {
                return IntegerValue.create( 0, theUnitFamily.getUnitsInFamily()[0] );
            }
            return IntegerValue.create( theMin.theValue, theUnitFamily.getUnitsInFamily()[0] );
        }
    }

    /**
      * Obtain the smallest allowed value.
      *
      * @return the smallest allowed value
      */
    public IntegerValue getMinimum()
    {
        return theMin;
    }

    /**
      * Obtain the largest allowed value.
      *
      * @return the largest allowed value
      */
    public IntegerValue getMaximum()
    {
        return theMax;
    }

    /**
      * Obtain the unit family, if any.
      *
      * @return the UnitFamily
      */
    public UnitFamily getUnitFamily()
    {
        return theUnitFamily;
    }

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else if( this.equals( thePositiveDefault )) {
            return thePositiveDefault;
        } else if( this.equals( thePositiveOrZeroDefault )) {
            return thePositiveOrZeroDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        if( this == theDefault ) {
            return className + DEFAULT_STRING;

        } else if( this == thePositiveDefault ) {
            return className + ".thePositiveDefault";

        } else if( this == thePositiveOrZeroDefault ) {
            return className + ".thePositiveOrZeroDefault";

        } else {
            StringBuilder ret = new StringBuilder( className );
            ret.append( CREATE_STRING );

            if( theMin != null ) {
                ret.append( theMin.getJavaConstructorString( classLoaderVar, null ));
            } else {
                ret.append( NULL_STRING );
            }
            ret.append( COMMA_STRING );

            if( theMax != null ) {
                ret.append( theMax.getJavaConstructorString( classLoaderVar, null ));
            } else {
                ret.append( NULL_STRING );
            }

            ret.append( COMMA_STRING );

            if( theUnitFamily != null ) {
                ret.append( theUnitFamily.getJavaConstructorString( classLoaderVar ));
            } else {
                ret.append( NULL_STRING );
            }

            ret.append( COMMA_STRING );

            if( theSupertype != null ) {
                ret.append( theSupertype.getJavaConstructorString( classLoaderVar ));
            } else {
                ret.append( NULL_STRING );
            }

            ret.append( CLOSE_PARENTHESIS_STRING );
            return ret.toString();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntegerValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        try {
            IntegerValue ret = IntegerValue.create( Integer.parseInt( raw ) );
            return ret;

        } catch( NumberFormatException ex ) {
            throw new PropertyValueParsingException( this, raw, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theMin",
                    "theMax"
                },
                new Object[] {
                    theMin,
                    theMax
                });

    }

    /**
      * The value for the minimum.
      */
    protected IntegerValue theMin;

    /**
      * The value for the maximum.
      */
    protected IntegerValue theMax;

    /**
      * The unit family (if any).
      */
    protected UnitFamily theUnitFamily;
}
