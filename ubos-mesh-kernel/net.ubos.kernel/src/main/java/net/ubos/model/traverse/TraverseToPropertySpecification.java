//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.model.primitives.PropertyType;
import net.ubos.util.ArrayHelper;

/**
 * <p>This specifies the value of one or more specific properties at a destination
 *    MeshObject that is obtained by traversing a TraverseSpecification. In spite of the
 *    somewhat similar name, this is NOT a TraverseSpecification, as we don't arrive at a
 *    set of MeshObjects when we use it, but at the values of certain of their properties.</p>
 *
 * <p>A TraverseToPropertySpecification consists of a TraverseSpecification, and
 *    a set of PropertyTypes. We find the properties
 *    by first traversing the TraverseSpecification, and the selecting the specified properties
 *    at the reached MeshObject (or MeshObjects). The TraverseSpecification is optional, indicating
 *    properties at the current MeshObject.</p>
 *
 * <p>Just like a TraverseSpecification, this class does not specify the start MeshObject as
 *    it is the specification for a traversal, not the result of an actual traversal that took
 *    place from a certain start MeshObject.
 */
public class TraverseToPropertySpecification
{
    /**
     * Factory method. This will create a TraversalToPropertySpecification that
     * leads to a single property after traversing a TraverseSpecification.
     *
     * @param traversalSpec the TraverseSpecification to be traversed first
     * @param destinationPropertyType the PropertyType that indicates the destination property
     * @return the created TraversalToPropertySpecification
     */
    public static TraverseToPropertySpecification create(
            TraverseSpecification traversalSpec,
            PropertyType          destinationPropertyType )
    {
        return new TraverseToPropertySpecification(
                traversalSpec,
                new PropertyType[] { destinationPropertyType } );
    }

    /**
     * Factory method. This will create a TraverseToPropertySpecification that
     * leads to a single property of the start MeshObject.
     *
     * @param destinationPropertyType the PropertyType that indicates the destination property
     * @return the created TraversalToPropertySpecification
     */
    public static TraverseToPropertySpecification create(
            PropertyType destinationPropertyType )
    {
        return new TraverseToPropertySpecification(
                null,
                new PropertyType[] { destinationPropertyType } );
    }

    /**
     * Factory method. This will create a TraverseToPropertySpecification that
     * leads to several properties after traversing a TraverseSpecification.
     *
     * @param traversalSpec the TraverseSpecification to be traversed first
     * @param destinationPropertyTypes the PropertyTypes that indicate the destination properties
     * @return the created TraversalToPropertySpecification
     */
    public static TraverseToPropertySpecification create(
            TraverseSpecification traversalSpec,
            PropertyType []       destinationPropertyTypes )
    {
        return new TraverseToPropertySpecification(
                traversalSpec,
                destinationPropertyTypes );
    }

    /**
     * Factory method. This will create a TraversalToPropertySpecification that
     * leads to several properties of the start MeshObject.
     *
     * @param destinationPropertyTypes the PropertyTypes that indicate the destination properties
     * @return the created TraversalToPropertySpecification
     */
    public static TraverseToPropertySpecification create(
            PropertyType [] destinationPropertyTypes )
    {
        return new TraverseToPropertySpecification(
                null,
                destinationPropertyTypes );
    }

    /**
     * Private constructor, use factory methods.
     *
     * @param traversalSpec the TraverseSpecification to be traversed first
     * @param destinationPropertyTypes the PropertyTypes that indicate the destination properties
     */
    protected TraverseToPropertySpecification(
            TraverseSpecification traversalSpec,
            PropertyType []       destinationPropertyTypes )
    {
        theTraverseSpecification   = traversalSpec;
        theDestinationPropertyTypes = destinationPropertyTypes;
    }

    /**
     * Obtain the TraverseSpecification (if any).
     *
     * @return the TraverseSpecification (if any)
     */
    public TraverseSpecification getTraverseSpecification()
    {
        return theTraverseSpecification;
    }

    /**
     * Obtain the PropertyTypes at the destination.
     *
     * @return the PropertyTypes at the destination
     */
    public PropertyType [] getPropertyTypes()
    {
        return theDestinationPropertyTypes;
    }

    /**
     * Determine equality. Two TraversalToPropertySpecification
     * are equal if their components are equal.
     *
     * @param other the Object to compare against
     * @return true if the objects are equal
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof TraverseToPropertySpecification ) {

            TraverseToPropertySpecification realOther = (TraverseToPropertySpecification) other;

            if( theTraverseSpecification == null ) {
                if( realOther.theTraverseSpecification != null ) {
                    return false;
                }
            } else if( ! theTraverseSpecification.equals( realOther.theTraverseSpecification )) {
                return false;
            }

            if( theDestinationPropertyTypes.length != realOther.theDestinationPropertyTypes.length ) {
                return false;
            }

            return ArrayHelper.hasSameContentOutOfOrder( theDestinationPropertyTypes, realOther.theDestinationPropertyTypes, true );
        }
        return false;
    }

    /**
     * Hash code.
     * 
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        int ret = 17; // random
        if( theTraverseSpecification != null ) {
            ret ^= theTraverseSpecification.hashCode();
        }
        for( PropertyType current : theDestinationPropertyTypes ) {
            ret ^= current.hashCode();
        }
        return ret;
    }

    /**
     * Our TraverseSpecification (if any).
     */
    protected TraverseSpecification theTraverseSpecification;

    /**
     * The PropertyTypes at the destination end.
     */
    protected PropertyType [] theDestinationPropertyTypes;
}
