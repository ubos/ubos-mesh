//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
  * A floating point number PropertyValue. Internally, it uses double.
  * It can also carry a Unit.
  *
  * FIXME: units not tested.
  */
public final class FloatValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Convenience constant like Float.NaN.
     */
    public static final FloatValue NaN = create( Float.NaN );

    /**
     * Convenience constant for zero.
     */
    public static final FloatValue ZERO = create( 0. );

    /**
     * Factory method.
     *
     * @param value the float value
     * @return the created FloatValue
     */
    public static FloatValue create(
            double value )
    {
        return new FloatValue( value, null );
    }

    /**
     * Factory method.
     *
     * @param value the float value
     * @param u the unit for the float value
     * @return the created FloatValue
     */
    public static FloatValue create(
            double value,
            Unit   u )
    {
        return new FloatValue( value, u );
    }

    /**
     * Factory method.
     *
     * @param value the float value
     * @return the created FloatValue
     * @throws IllegalArgumentException if null is given as argument
     */
    public static FloatValue create(
            Number value )
        throws
            IllegalArgumentException
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new FloatValue( value.doubleValue(), null );
    }

    /**
     * Factory method.
     *
     * @param value the float value
     * @param u the unit for the float value
     * @return the created FloatValue
     * @throws IllegalArgumentException if null is given as argument for the value
     */
    public static FloatValue create(
            Number value,
            Unit   u )
        throws
            IllegalArgumentException
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new FloatValue( value.doubleValue(), u );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the float value
     * @return the created FloatValue
     */
    public static FloatValue createOrNull(
            Number value )
    {
        if( value == null ) {
            return null;
        }
        return new FloatValue( value.doubleValue(), null );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the float value
     * @param u the unit for the float value
     * @return the created FloatValue
     */
    public static FloatValue createOrNull(
            Number value,
            Unit   u )
    {
        if( value == null ) {
            return null;
        }
        return new FloatValue( value.doubleValue(), u );
    }

    /**
      * Private constructor, use factory methods.
      *
      * @param value the value
      * @param u the Unit for the value
      */
    private FloatValue(
            double value,
            Unit   u )
    {
        this.theValue = value;
        this.theUnit  = u;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double value()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double getValue()
    {
        return value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FloatDataType getDataType()
    {
        return FloatDataType.theDefault;
    }

    /**
     * Determine Unit, if any.
     *
     * @return the Unit, if any
     */
    public Unit getUnit()
    {
        return theUnit;
    }

    /**
     * Convenience method along the lines of Number.doubleValue.
     *
     * @return the value as double
     */
    public Double doubleValue()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( ! ( otherValue instanceof FloatValue )) {
            return false;
        }

        FloatValue realOtherValue = (FloatValue) otherValue;

        if( theUnit == null ) {
            if( realOtherValue.theUnit != null ) {
                return false;
            }

            return theValue == realOtherValue.theValue;
        } else {
            if( realOtherValue.theUnit == null ) {
                return false;
            }

            if( ! theUnit.getFamily().equals( realOtherValue.theUnit.getFamily() )) {
                return false;
            }

            return theValue * theUnit.getPrefix() == realOtherValue.theValue * realOtherValue.theUnit.getPrefix();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = (int) theValue;

        if( theUnit != null ) {
            ret ^= theUnit.hashCode();
        }
        return ret;
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is smaller than, or the same as otherValue
     */
    public boolean isSmallerOrEquals(
            FloatValue otherValue )
    {
        if( theUnit == null ) {
            if( otherValue.theUnit != null ) {
                return false;
            }

            return theValue <= otherValue.theValue;
        } else {
            if( otherValue.theUnit == null ) {
                return false;
            }

            if( ! theUnit.getFamily().equals( otherValue.theUnit.getFamily() )) {
                return false;
            }

            return theValue * theUnit.getPrefix() <= otherValue.theValue * otherValue.theUnit.getPrefix();
        }
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is smaller than otherValue
     */
    public boolean isSmaller(
            FloatValue otherValue )
    {
        if( theUnit == null ) {
            if( otherValue.theUnit != null ) {
                return false;
            }
            return theValue < otherValue.theValue;
        } else {
            if( otherValue.theUnit == null ) {
                return false;
            }

            if( ! theUnit.getFamily().equals( otherValue.theUnit.getFamily() )) {
                return false;
            }
            return theValue * theUnit.getPrefix() < otherValue.theValue * otherValue.theUnit.getPrefix();
        }
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is larger, or the same, as otherValue
     */
    public boolean isLargerOrEquals(
            FloatValue otherValue )
    {
        return otherValue.isSmallerOrEquals( this );
    }

    /**
     * Determine relationship between two values.
     *
     * @param otherValue the value to test against
     * @return returns true if this object is larger than otherValue
     */
    public boolean isLarger(
            FloatValue otherValue )
    {
        return otherValue.isSmaller( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        if( theUnit != null ) {
            return String.valueOf( theValue ) + " " + theUnit.toString();
        } else {
            return String.valueOf( theValue );
        }
    }

    /**
      * This attempts to parse a string and turn it into a float value similarly
      * to Float.parseFloat().
      *
      * FIXME: need to deal with unit
      *
      * @param theString the string that shall be parsed
      * @return the created FloatValue
      * @throws NumberFormatException thrown if theString does not follow the correct syntax
      */
    public static FloatValue parseFloatValue(
            String theString )
        throws
            NumberFormatException
    {
        return FloatValue.create( Double.parseDouble( theString ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        StringBuilder buf = new StringBuilder( 128 );
        buf.append( getClass().getName() );
        buf.append( DataType.CREATE_STRING );
        buf.append( theValue );
        
        if( theUnit != null ) {
            buf.append( ", " );
            buf.append( theUnit.getJavaConstructorString() );
        }
        buf.append( DataType.CLOSE_PARENTHESIS_STRING );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        FloatValue realOther = (FloatValue) o;

        if( theValue < realOther.theValue ) {
            return -1;
        } else if( theValue == realOther.theValue ) {
            return 0;
        } else {
            return +1;
        }
    }

    /**
      * The actual value.
      */
    protected double theValue;

    /**
      * The Unit, if any.
      */
    protected Unit theUnit;
}
