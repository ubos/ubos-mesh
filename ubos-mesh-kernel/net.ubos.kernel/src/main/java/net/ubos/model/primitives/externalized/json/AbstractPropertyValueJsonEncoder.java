//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.json;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Base64;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.AbstractPropertyValueEncoder;
import net.ubos.model.primitives.externalized.EncodingException;

/**
 * Knows how to encode a PropertyValue to JSON.
 */
public abstract class AbstractPropertyValueJsonEncoder
    extends
        AbstractPropertyValueEncoder
    implements
        PropertyValueJsonTags
{
    /**
     * {@inheritDoc}
     */
    @Override
    public void writePropertyValue(
            PropertyValue value,
            Writer        w )
        throws
            EncodingException,
            IOException
    {
        JsonWriter jw = new JsonWriter( w );
        jw.setSerializeNulls(  true );

        writePropertyValue( value, jw );

        jw.flush();
    }

    /**
     * Serialize a PropertyValue to a JsonWriter.
     *
     * @param value the PropertyValue
     * @param out the JsonWriter to write to
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException an I/O problem occurred
     */
    public void writePropertyValue(
            PropertyValue value,
            JsonWriter    out )
        throws
            EncodingException,
            IOException
    {
        if( value == null ) {
            out.nullValue();

        } else if( value instanceof BlobValue ) {
            BlobValue realValue = (BlobValue) value;
            String    mt        = realValue.getMimeType();

            if( mt == null ) {
                mt = "?/?";
            }

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( BLOB_VALUE_TAG );
            out.name( BLOB_VALUE_MIME_TAG ).value( mt );

            // inline value, even if delayed loading -- hard to keep track of which ClassLoader to use
            if( mt.startsWith( "text/" )) {
                out.name( VALUE_TAG ).value( realValue.getAsString() );
            } else {
                out.name( VALUE_TAG ).value( new String( Base64.getEncoder().encode( realValue.value() )));
            }
            out.endObject();

        } else if( value instanceof BooleanValue ) {
            BooleanValue realValue = (BooleanValue)value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( BOOLEAN_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.value() );
            out.endObject();

        } else if( value instanceof ColorValue ) {
            ColorValue realValue = (ColorValue)value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( COLOR_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.getRGB() );
            out.endObject();

        } else if( value instanceof CurrencyValue ) {
            CurrencyValue realValue = (CurrencyValue)value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( CURRENCY_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.value() );
            out.endObject();

        } else if( value instanceof EnumeratedValue ) {
            EnumeratedValue realValue = (EnumeratedValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( ENUMERATED_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.value() );
            out.endObject();

        } else if( value instanceof ExtentValue ) {
            ExtentValue realValue = (ExtentValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( EXTENT_VALUE_TAG );
            out.name( EXTENT_VALUE_WIDTH_TAG ).value( realValue.getWidth() );
            out.name( EXTENT_VALUE_HEIGHT_TAG ).value( realValue.getHeight() );
            out.endObject();

        } else if( value instanceof FloatValue ) {
            FloatValue realValue = (FloatValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( FLOAT_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.value() );
            out.endObject();

        } else if( value instanceof IntegerValue ) {
            IntegerValue realValue = (IntegerValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( INTEGER_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.value() );
            out.endObject();

        } else if( value instanceof MultiplicityValue ) {
            MultiplicityValue realValue = (MultiplicityValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( MULTIPLICITY_VALUE_TAG );
            out.name( MULTIPLICITY_VALUE_MIN_TAG ).value( realValue.getMinimumAsString() );
            out.name( MULTIPLICITY_VALUE_MAX_TAG ).value( realValue.getMinimumAsString() );
            out.endObject();

        } else if( value instanceof PointValue ) {
            PointValue realValue = (PointValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( POINT_VALUE_TAG );
            out.name( POINT_VALUE_X_TAG ).value( realValue.getX() );
            out.name( POINT_VALUE_Y_TAG ).value( realValue.getY() );
            out.endObject();

        } else if( value instanceof StringValue ) {
            StringValue realValue = (StringValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( STRING_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.value() );
            out.endObject();

        } else if( value instanceof TimePeriodValue ) {
            TimePeriodValue realValue = (TimePeriodValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG        ).value( TIME_PERIOD_VALUE_TAG );
            out.name( TIME_PERIOD_YEAR_TAG   ).value( realValue.getYear() );
            out.name( TIME_PERIOD_MONTH_TAG  ).value( realValue.getMonth() );
            out.name( TIME_PERIOD_DAY_TAG    ).value( realValue.getDay() );
            out.name( TIME_PERIOD_HOUR_TAG   ).value( realValue.getHour() );
            out.name( TIME_PERIOD_MINUTE_TAG ).value( realValue.getMinute() );
            out.name( TIME_PERIOD_SECOND_TAG ).value( realValue.getSecond() );
            out.endObject();

        } else if( value instanceof TimeStampValue ) {
            TimeStampValue realValue = (TimeStampValue) value;

            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( TIME_STAMP_VALUE_TAG );
            out.name( VALUE_TAG ).value( realValue.getAsMillis() );
            out.endObject();

        } else {
            out.beginObject();
            out.name( VALUE_CLASS_TAG ).value( "?" );
            out.endObject();
        }
    }
}
