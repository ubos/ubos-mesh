//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.json;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.Base64;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BooleanDataType;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorDataType;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyDataType;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.ExtentDataType;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatDataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerDataType;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MultiplicityDataType;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointDataType;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodDataType;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampDataType;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.AbstractPropertyValueDecoder;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.util.logging.Log;

/**
 * Knows how to decode a PropertyValue from JSON.
 */
public abstract class AbstractPropertyValueJsonDecoder
    extends
        AbstractPropertyValueDecoder
    implements
        PropertyValueJsonTags
{
    private static final Log log = Log.getLogInstance(AbstractPropertyValueJsonDecoder.class );

    /**
     * Parse a PropertyValue.
     *
     * @param type the DataType for which this will be a value
     * @param r where to read from
     * @return the PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public PropertyValue parsePropertyValue(
            DataType type,
            Reader   r )
        throws
            DecodingException,
            IOException
    {
        JsonReader jr = new JsonReader( r );
        return parsePropertyValue( type, jr );
    }

    /**
     * Helper to parse a PropertyValue.
     *
     * @param type the DataType for which this will be a value
     * @param jr where to read from
     * @return the PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    public PropertyValue parsePropertyValue(
            DataType     type,
            JsonReader   jr )
        throws
            DecodingException,
            IOException
    {
        PropertyValue ret;

        if( JsonToken.NULL.equals( jr.peek())) {
            ret = null;
            jr.nextNull();

        } else if( type instanceof BlobDataType ) {
            ret = parseBlobValue( (BlobDataType) type, jr );

        } else if( type instanceof BooleanDataType ) {
            ret = parseBooleanValue( (BooleanDataType) type, jr );

        } else if( type instanceof ColorDataType ) {
            ret = parseColorValue( (ColorDataType) type, jr );

        } else if( type instanceof CurrencyDataType ) {
            ret = parseCurrencyValue( (CurrencyDataType) type, jr );

        } else if( type instanceof EnumeratedDataType ) {
            ret = parseEnumeratedValue( (EnumeratedDataType) type, jr );

        } else if( type instanceof ExtentDataType ) {
            ret = parseExtentValue( (ExtentDataType) type, jr );

        } else if( type instanceof FloatDataType ) {
            ret = parseFloatValue( (FloatDataType) type, jr );

        } else if( type instanceof IntegerDataType ) {
            ret = parseIntegerValue( (IntegerDataType) type, jr );

        } else if( type instanceof MultiplicityDataType ) {
            ret = parseMultiplicityValue( (MultiplicityDataType) type, jr );

        } else if( type instanceof PointDataType ) {
            ret = parsePointValue( (PointDataType) type, jr );

        } else if( type instanceof StringDataType ) {
            ret = parseStringValue( (StringDataType) type, jr );

        } else if( type instanceof TimePeriodDataType ) {
            ret = parseTimePeriodValue( (TimePeriodDataType) type, jr );

        } else if( type instanceof TimeStampDataType ) {
            ret = parseTimeStampValue( (TimeStampDataType) type, jr );

        } else {
            log.error( "Unexpected DataType: " + type );
            ret = null;
        }

        return ret;
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseBlobValue(
            BlobDataType dt,
            JsonReader   jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        String mime  = null;
        String value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !BLOB_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting BlobValue" );
                    }
                    break;

                case BLOB_VALUE_MIME_TAG:
                    mime = jr.nextString();
                    break;

                case VALUE_TAG:
                    value = jr.nextString();
                    break;
            }
        }

        jr.endObject();

        PropertyValue ret;
        if( mime.startsWith( "text/" )) {
            ret = dt.createBlobValue( value, mime );
        } else {
            ret = dt.createBlobValue( Base64.getDecoder().decode( value), mime );
        }
        return ret;
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseBooleanValue(
            BooleanDataType dt,
            JsonReader      jr )
        throws
            DecodingException,
            IOException
    {
        String  type;
        Boolean value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !BOOLEAN_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting BooleanValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextBoolean();
                    break;
            }
        }

        jr.endObject();

        if( value ) {
            return BooleanValue.TRUE;
        } else {
            return BooleanValue.FALSE;
        }
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseColorValue(
            ColorDataType dt,
            JsonReader    jr )
        throws
            DecodingException,
            IOException
    {
        String  type;
        Integer value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !COLOR_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting ColorValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextInt();
                    break;
            }
        }

        jr.endObject();

        return ColorValue.create( value );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseCurrencyValue(
            CurrencyDataType dt,
            JsonReader       jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        String value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !CURRENCY_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting CurrencyValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextString();
                    break;
            }
        }

        jr.endObject();

        try {
            return CurrencyValue.parseCurrencyValue( value );

        } catch( ParseException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseEnumeratedValue(
            EnumeratedDataType dt,
            JsonReader         jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        String value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !ENUMERATED_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting EnumeratedValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextString();
                    break;
            }
        }

        jr.endObject();

        return dt.select( value );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseExtentValue(
            ExtentDataType dt,
            JsonReader     jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        Double width  = null;
        Double height = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !EXTENT_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting ExtentValue" );
                    }
                    break;

                case EXTENT_VALUE_WIDTH_TAG:
                    width = jr.nextDouble();
                    break;

                case EXTENT_VALUE_HEIGHT_TAG:
                    height = jr.nextDouble();
                    break;
            }
        }

        jr.endObject();

        return ExtentValue.create( width, height );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseFloatValue(
            FloatDataType dt,
            JsonReader    jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        Double value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !FLOAT_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting FloatValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextDouble();
                    break;
            }
        }

        jr.endObject();

        return FloatValue.create( value );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseIntegerValue(
            IntegerDataType dt,
            JsonReader      jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        Long   value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !INTEGER_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting IntegerValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextLong();
                    break;
            }
        }

        jr.endObject();

        return IntegerValue.create( value );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseMultiplicityValue(
            MultiplicityDataType dt,
            JsonReader           jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        String min = null;
        String max = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !MULTIPLICITY_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting MultiplicityValue" );
                    }
                    break;

                case MULTIPLICITY_VALUE_MIN_TAG:
                    min = jr.nextString();
                    break;

                case MULTIPLICITY_VALUE_MAX_TAG:
                    max = jr.nextString();
                    break;
            }
        }

        jr.endObject();

        return MultiplicityValue.create(
                MultiplicityValue.N_SYMBOL.equals( min ) ? MultiplicityValue.N : Integer.parseInt( min ),
                MultiplicityValue.N_SYMBOL.equals( max ) ? MultiplicityValue.N : Integer.parseInt( max ) );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parsePointValue(
            PointDataType dt,
            JsonReader    jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        Double x = null;
        Double y = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !POINT_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting PointValue" );
                    }
                    break;

                case POINT_VALUE_X_TAG:
                    x = jr.nextDouble();
                    break;

                case POINT_VALUE_Y_TAG:
                    y = jr.nextDouble();
                    break;
            }
        }

        jr.endObject();

        return PointValue.create( x, y );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseStringValue(
            StringDataType dt,
            JsonReader     jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        String value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !STRING_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting StringValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextString();
                    break;
            }
        }

        jr.endObject();

        return StringValue.create( value );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseTimePeriodValue(
            TimePeriodDataType dt,
            JsonReader         jr )
        throws
            DecodingException,
            IOException
    {
        String  type;
        Integer year   = null;
        Integer month  = null;
        Integer day    = null;
        Integer hour   = null;
        Integer minute = null;
        Double  second = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !TIME_PERIOD_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting TimePeriodValue" );
                    }
                    break;

                case TIME_PERIOD_YEAR_TAG:
                    year = jr.nextInt();
                    break;

                case TIME_PERIOD_MONTH_TAG:
                    month = jr.nextInt();
                    break;

                case TIME_PERIOD_DAY_TAG:
                    day = jr.nextInt();
                    break;

                case TIME_PERIOD_HOUR_TAG:
                    hour = jr.nextInt();
                    break;

                case TIME_PERIOD_MINUTE_TAG:
                    month = jr.nextInt();
                    break;

                case TIME_PERIOD_SECOND_TAG:
                    second = jr.nextDouble();
                    break;
            }
        }

        jr.endObject();

        return TimePeriodValue.create(
                (short) (int) year,
                (short) (int) month,
                (short) (int) day,
                (short) (int) hour,
                (short) (int) minute,
                (float) (double) second );
    }

    /**
     * Helper to parse a particular type of PropertyValue.
     *
     * @param dt the DataType for which this will be a value
     * @param jr where to read from
     * @return the found PropertyValue
     * @throws DecodingException thrown if decoding some serialized data failed
     * @throws IOException an I/O problem occurred
     */
    protected PropertyValue parseTimeStampValue(
            TimeStampDataType dt,
            JsonReader        jr )
        throws
            DecodingException,
            IOException
    {
        String type;
        String value = null;

        jr.beginObject();

        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case VALUE_CLASS_TAG:
                    type = jr.nextString();
                    if( !TIME_STAMP_VALUE_TAG.equals( type )) {
                        throw new DecodingException.Syntax( "Inconsistent types: " + type + " vs expecting TimeStampValue" );
                    }
                    break;

                case VALUE_TAG:
                    value = jr.nextString();
                    break;
            }
        }

        jr.endObject();

        return TimeStampValue.create( Long.parseLong( value ));
    }
}
