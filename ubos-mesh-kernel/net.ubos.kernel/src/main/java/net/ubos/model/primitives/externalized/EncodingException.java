//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

/**
 * Thrown if encoding of something failed.
 */
public class EncodingException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param message the message
     */
    public EncodingException(
            String message )
    {
        super( message );
    }

    /**
     * Constructor.
     *
     * @param cause the underlying cause
     */
    public EncodingException(
            Throwable cause )
    {
        super( cause );
    }

    /**
     * Constructor.
     *
     * @param message the message
     * @param cause the underlying cause
     */
    public EncodingException(
            String    message,
            Throwable cause )
    {
        super( message, cause );
    }
}
