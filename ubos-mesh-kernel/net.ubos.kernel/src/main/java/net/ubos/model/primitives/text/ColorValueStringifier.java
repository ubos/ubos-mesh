//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.ColorValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies ColorValues.
 */
public class ColorValueStringifier
        extends
            AbstractStringifier<ColorValue>
{
    /**
     * Factory method.
     *
     * @return the created ColorValueStringifier
     */
    public static ColorValueStringifier create()
    {
        return new ColorValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected ColorValueStringifier()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            ColorValue            arg,
            StringifierParameters pars,
            String                soFar )
    {
        return arg.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (ColorValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ColorValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            ColorValue ret = ColorValue.parseColorValue( rawString );
            return ret;

        } catch( NumberFormatException ex ) {
            throw new StringifierParseException( this, null, ex );
        }
    }
}
