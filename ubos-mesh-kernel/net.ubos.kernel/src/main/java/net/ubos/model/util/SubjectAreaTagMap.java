//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.util;

import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.CollectableMeshTypeNotFoundException;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Maps short names (tags) to SubjectAreas.
 */
public interface SubjectAreaTagMap
{
    /**
     * Put a value into the map.
     *
     * @param tag the tag
     * @param sa the SubjectArea
     * @throws TagConflictException if there was already a mapping
     */
    public void put(
            String      tag,
            SubjectArea sa )
        throws
            TagConflictException;

    /**
     * Determine whether this SubjectAreaTagMap is empty.
     *
     * @return true if it is empty
     */
    public boolean isEmpty();

    /**
     * Obtain the SubjectArea for this tag.
     *
     * @param tag the tag
     * @return the SubjectArea, or null
     */
    public SubjectArea getSubjectAreaFor(
            String tag );

    /**
     * Obtain the tag for this SubjectArea. If the SubjectArea does not have a tag,
     * return null.
     *
     * @param sa the Subject Area
     * @return the tag, or null
     */
    public String getTagFor(
            SubjectArea sa );

    /**
     * Obtain the tag for this SubjectArea. If the SubjectArea does not have a
     * tag, create a new one.
     *
     * @param sa the Subject Area
     * @return the tag
     */
    public String obtainTagFor(
            SubjectArea sa );

    /**
     * Obtain an Iterator over all tag contained in this map.
     *
     * @return the Iterator
     */
    public CursorIterator<String> tagIterator();

    /**
     * Convenience method to look up a CollectableMeshType using the SubjectArea tag name.
     *
     * @param identifier short identifier of the CollectableMeshType using the tag name
     * @return the CollectableMeshType
     * @throws CollectableMeshTypeNotFoundException the CollectableMeshType could not be found
     */
    public CollectableMeshType findCollectableMeshType(
            String identifier )
        throws
            CollectableMeshTypeNotFoundException;
}
