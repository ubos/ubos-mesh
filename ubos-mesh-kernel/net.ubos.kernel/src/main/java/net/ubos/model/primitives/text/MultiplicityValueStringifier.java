//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import java.text.ParseException;
import net.ubos.model.primitives.MultiplicityDataType;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies MultiplicityValues.
 */
public class MultiplicityValueStringifier
        extends
            AbstractStringifier<MultiplicityValue>
{
    /**
     * Factory method.
     *
     * @return the created MultiplicityStringifier
     */
    public static MultiplicityValueStringifier create()
    {
        return new MultiplicityValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected MultiplicityValueStringifier()
    {
    }

    /**
     * Format an Object using this Stringifier.
     *
     * @param soFar the String so far, if any
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the formatted String
     */
    @Override
    public String format(
            MultiplicityValue     arg,
            StringifierParameters pars,
            String                soFar )
    {
        return arg.toString();
    }

    /**
     * Format an Object using this Stringifier. This may be null.
     *
     * @param soFar the String so far, if any
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the formatted String
     * @throws ClassCastException thrown if this Stringifier could not format the provided Object
     *         because the provided Object was not of a type supported by this Stringifier
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (MultiplicityValue) arg, pars, soFar );
    }

    /**
     * Parse out the Object in rawString that were inserted using this Stringifier.
     *
     * @param rawString the String to parse
     * @param factory the factory needed to create the parsed values, if any
     * @return the found Object
     * @throws StringifierParseException thrown if a parsing problem occurred
     */
    @Override
    public MultiplicityValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        MultiplicityDataType type = (MultiplicityDataType) factory;

        MultiplicityValue ret;
        try {
            ret = MultiplicityValue.parseMultiplicityValue( rawString );

        } catch( ParseException ex ) {
            throw new StringifierParseException( this, null, ex );
        }
        return ret;
    }
}
