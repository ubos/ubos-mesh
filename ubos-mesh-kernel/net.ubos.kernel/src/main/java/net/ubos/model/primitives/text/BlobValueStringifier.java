//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.BlobValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies BlobValues.
 */
public class BlobValueStringifier
        extends
            AbstractStringifier<BlobValue>
{
    /**
     * Factory method.
     *
     * @return the created BlobValueStringifier
     */
    public static BlobValueStringifier create()
    {
        return new BlobValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected BlobValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            BlobValue             arg,
            StringifierParameters pars,
            String                soFar )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (BlobValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BlobValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }
}
