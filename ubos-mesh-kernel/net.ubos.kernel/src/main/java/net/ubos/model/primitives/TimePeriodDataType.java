//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.util.logging.Dumper;

/**
  * A time period PropertyType. A time period is a "delta" value in time.
  */
public final class TimePeriodDataType
        extends
            DataType
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * This is the default instance of this class.
     */
    public static final TimePeriodDataType theDefault = new TimePeriodDataType();

    /**
     * Factory method. Always returns the same instance.
     *
     * @return the default instance of this class
     */
    public static TimePeriodDataType create()
    {
        return theDefault;
    }

    /**
     * Private constructor, there is no reason to instantiate this more than once.
     */
    private TimePeriodDataType()
    {
        super( null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof TimePeriodDataType ) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return TimePeriodDataType.class.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        TimePeriodValue realValue = (TimePeriodValue) value; // may throw

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<TimePeriodValue> getCorrespondingJavaClass()
    {
        return TimePeriodValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimePeriodValue getDefaultValue()
    {
        return theDefaultValue;
    }

    /**
     * The default value.
     */
    private static final TimePeriodValue theDefaultValue = TimePeriodValue.create( (short) 0, (short) 0, (short) 0, (short) 0, (short) 0, (float) 0 );

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        return className + DEFAULT_STRING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimePeriodValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        try {
            Matcher m = PARSING_REGEX.matcher( raw.trim() );
            if( m.matches() ) {
                TimePeriodValue ret = TimePeriodValue.create(
                        Short.parseShort( m.group(1) ),
                        Short.parseShort( m.group(2) ),
                        Short.parseShort( m.group(3) ),
                        Short.parseShort( m.group(4) ),
                        Short.parseShort( m.group(5) ),
                        Float.parseFloat( m.group(6) ));
                return ret;

            } else {
            throw new PropertyValueParsingException( this, raw );
            }

        } catch( NumberFormatException ex ) {
            throw new PropertyValueParsingException( this, raw );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }

    /**
     * Parsing pattern.
     * 1/2/3 4:5:6.7
     */
    protected static final Pattern PARSING_REGEX = Pattern.compile(
            "(\\d*)"
            + "\\s*(?:[/,:]\\s*)?"
            + "(\\d*)"
            + "\\s*(?:[/,:]\\s*)?"
            + "(\\d*)"
            + "\\s*(?:[/,:]\\s*)?"
            + "(\\d*)"
            + "\\s*(?:[/,:]\\s*)?"
            + "(\\d*)"
            + "\\s*(?:[/,:]\\s*)?"
            + "(\\d*(?:\\.\\d*)?)"
            + "" );
}
