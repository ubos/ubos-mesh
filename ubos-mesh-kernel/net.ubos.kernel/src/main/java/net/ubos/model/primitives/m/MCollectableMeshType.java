//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.m.MMeshTypeIdentifier;

/**
  * A MeshType that is defined in a SubjectArea.
  * In-memory implementation.
  */
public abstract class MCollectableMeshType
        extends
            MMeshType
        implements
            CollectableMeshType
{
    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     */
    public MCollectableMeshType(
            MMeshTypeIdentifier identifier )
    {
        super( identifier );
    }

    /**
      * Set the SubjectArea property.
      *
      * @param newValue the new value for the property
      * @see #getSubjectArea
      */
    public final void setSubjectArea(
            SubjectArea newValue )
    {
        this.theSubjectArea = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final SubjectArea getSubjectArea()
    {
        return theSubjectArea;
    }

    /**
      * The SubjectArea in which this CollectableMeshType is defined.
      */
    private SubjectArea theSubjectArea;
}
