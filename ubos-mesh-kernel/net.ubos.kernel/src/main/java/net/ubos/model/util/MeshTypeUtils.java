//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.util;

import java.util.Comparator;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.livedead.IsDeadException;
import net.ubos.util.logging.Log;

/**
 * Collection of utility methods for MeshTypes.
 */
public abstract class MeshTypeUtils
{
    private static final Log log = Log.getLogInstance( MeshTypeUtils.class ); // our own, private logger

    /**
     * Private constructor, this class cannot be instantiated.
     */
    private MeshTypeUtils()
    {
        // noop
    }

    /**
     * Construct an array of MeshTypeIdentifiers from an array of MeshTypes.
     *
     * @param types the MeshTypeIdentifiers
     * @return the MeshTypeIdentifiers of the MeshTypes
     */
    public static MeshTypeIdentifier [] meshTypeIdentifiersOrNull(
            MeshType [] types )
    {
        if( types == null ) {
            return null;
        }
        MeshTypeIdentifier [] ret = new MeshTypeIdentifier[ types.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = types[i].getIdentifier();
        }
        return ret;
    }

    /**
     * Construct an array of MeshTypeIdentifiers from the MeshTypes by which
     * a MeshObject is blessed.
     *
     * @param obj the MeshObject
     * @return the MeshTypeIdentifiers of the MeshTypes of the MeshObject
     */
    public static MeshTypeIdentifier [] meshTypeIdentifiersOrNull(
            MeshObject obj )
    {
        if( obj == null ) {
            return null;
        }
        try {
            MeshType           [] types = obj.getEntityTypes();
            MeshTypeIdentifier [] ret = new MeshTypeIdentifier[ types.length ];
            for( int i=0 ; i<ret.length ; ++i ) {
                ret[i] = types[i].getIdentifier();
            }
            return ret;
        } catch( IsDeadException ex ) {
            log.warn( ex );
        }
        return null;
    }


    /**
     * Sorter by UserVisibleName.
     */
    public static final Comparator<MeshType> BY_USER_VISIBLE_NAME_COMPARATOR
            = (MeshType one, MeshType two) -> {
                StringValue name1 = one.getUserVisibleName();
                StringValue name2 = two.getUserVisibleName();

                return PropertyValue.compare( name1, name2 );
            };

    /**
     * Sorted by UserVisibleName, but case insensitive.
     */
    public static final Comparator<MeshType> BY_USER_VISIBLE_NAME_CASE_INSENSITIVE_COMPARATOR
            = (MeshType one, MeshType two) -> {
                StringValue name1 = one.getUserVisibleName();
                StringValue name2 = two.getUserVisibleName();

                return StringValue.compareCaseInsensitive( name1, name2 );
            };
}
