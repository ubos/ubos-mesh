//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.model.util.SubjectAreaTagMap;

/**
 * Knows how to chop Traverse strings into sequential pieces, and pass those to another TraverseTranslator.
 */
public class SpaceMakesSequentialTraverseTranslator
     implements
        TraverseTranslator
{
    /**
     * Factory method.
     *
     * @param delegate: delegate to this TranversalTranslator if needed
     * @return the created instance
     */
    public static SpaceMakesSequentialTraverseTranslator create(
            TraverseTranslator delegate )
    {
        return new SpaceMakesSequentialTraverseTranslator( delegate );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param delegate: delegate to this TranversalTranslator if needed
     */
    protected SpaceMakesSequentialTraverseTranslator(
            TraverseTranslator delegate )
    {
        theDelegate = delegate;
    }

    @Override
    public TraverseSpecification translateTraverseSpecification(
            MeshObject        startObject,
            SubjectAreaTagMap tagMap,
            String            traversalTerm )
        throws
            TraverseTranslatorException
    {
        String [] terms = traversalTerm.trim().split( "\\s+" );

        TraverseSpecification ret;
        if( terms.length == 1 ) {
            ret = theDelegate.translateTraverseSpecification( startObject, tagMap, traversalTerm );
        } else {
            TraverseSpecification [] steps = new TraverseSpecification[ terms.length ];
            for( int i=0 ; i<terms.length ; ++i ) {
                steps[i] = theDelegate.translateTraverseSpecification( startObject, tagMap, terms[i] );
            }
            ret = SequentialCompoundTraverseSpecification.create( steps );
        }
        return ret;
    }

    /**
     * The delegate.
     */
    protected final TraverseTranslator theDelegate;
}
