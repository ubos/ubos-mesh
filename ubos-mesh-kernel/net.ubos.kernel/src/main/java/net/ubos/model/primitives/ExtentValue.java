//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.awt.geom.Dimension2D;
import java.text.ParseException;
import net.ubos.util.DoubleDimension;

/**
  * This is a graphical extent value for PropertyValues.
  */
public final class ExtentValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method.
     *
     * @param value the Dimension2D version of this value
     * @return the created ExtentValue
     */
    public static ExtentValue create(
            Dimension2D value )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new ExtentValue( value.getWidth(), value.getHeight() );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the Dimension2D version of this value
     * @return the created ExtentValue, or null
     */
    public static ExtentValue createOrNull(
            Dimension2D value )
    {
        if( value == null ) {
            return null;
        }
        return new ExtentValue( value.getWidth(), value.getHeight() );
    }

    /**
     * Factory method.
     *
     * @param width the theWidth of the extent
     * @param height the theHeight of the extent
     * @return the created ExtentValue
     */
    public static ExtentValue create(
            double width,
            double height )
    {
        return new ExtentValue( width, height );
    }

    /**
      * Private constructor, use factory method.
      *
      * @param width the theWidth of the extent
      * @param height the theHeight of the extent
      */
    private ExtentValue(
            double width,
            double height )
    {
        this.theWidth  = width;
        this.theHeight = height;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Dimension2D value()
    {
        return new DoubleDimension( theWidth, theHeight );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Dimension2D getValue()
    {
        return value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtentDataType getDataType()
    {
        return ExtentDataType.theDefault;
    }

    /**
     * Determine the width of the extent.
     *
     * @return the width of the extent
     */
    public double getWidth()
    {
        return theWidth;
    }

    /**
     * Determine the height of the extent.
     *
     * @return the the height of the extent
     */
    public double getHeight()
    {
        return theHeight;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof ExtentValue ) {
            return (theHeight == ((ExtentValue)otherValue).theHeight)
                && (theWidth  == ((ExtentValue)otherValue).theWidth);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return (int) ( theHeight * theWidth );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "[" + theWidth + ";" + theHeight + "]";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        StringBuilder buf = new StringBuilder( 64 );
        buf.append( getClass().getName());
        buf.append( DataType.CREATE_STRING );
        buf.append( theWidth  );
        buf.append( ", " );
        buf.append( theHeight  );
        buf.append( DataType.CLOSE_PARENTHESIS_STRING );
        return buf.toString();
    }

    /**
      * This attempts to parse a string and turn it into a ExtentValue value similarly
 to Integer.parseInt().
      *
      * @param s the string that shall be parsed
      * @return the created ExtentValue
      * @throws ParseException thrown if theString does not follow the correct syntax
      */
    public static ExtentValue parseExtentValue(
            String s )
        throws
            ParseException
    {
        s = s.trim();
        if( s.startsWith( "[" ) && s.endsWith( "]" )) {
            int semi = s.indexOf( ';', 1 );
            if( semi > 1 ) {
                double x = Double.parseDouble( s.substring( 1, semi ));
                double y = Double.parseDouble( s.substring( semi+1, s.length()-1 ));

                return new ExtentValue( x, y );
            }
        }
        throw new ParseException( s, 0 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        ExtentValue realOther = (ExtentValue) o;

        if( theWidth < realOther.theWidth ) {
            if( theHeight < realOther.theHeight ) {
                return -1;
            } else {
                return +2; // not comparable convention: +2
            }
        } else if( theWidth == realOther.theWidth ) {
            if( theHeight == realOther.theHeight ) {
                return 0;
            } else {
               return +2; // not comparable convention: +2
            }
        } else {
            if( theHeight > realOther.theHeight ) {
                return +1;
            } else {
                return +2; // not comparable convention: +2
            }
        }
    }

    /**
      * The actual value for the width.
      */
    protected double theWidth;

    /**
      * The actual value for the height.
      */
    protected double theHeight;
}
