//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.model.util.SubjectAreaTagMap;

/**
 * <p>This interface is supported by classes that know how to translate one or more
 * Strings (representing a TraverseSpecification, or several TraverseSpecifications) into the actual
 * TraverseSpecification(s). Many different implementations are possible.</p>
 *
 * <p>Note: there is no requirement that the same set of terms translate into the same
 * TraverseSpecification(s) when applied to different MeshObjects.</p>
 */
public interface TraverseTranslator
{
    /**
     * Translate the String form of a single TraverseSpecification into an
     * actual TraverseSpecification.
     *
     * @param startObject the start MeshObject for the traversal
     * @param tagMap map defining the tags that might have been used
     * @param traversalTerm the term to be translated
     * @return the TraverseSpecification, or null if not found.
     * @throws TraversalTranslatorException thrown if the translation failed
     */
    public abstract TraverseSpecification translateTraverseSpecification(
            MeshObject        startObject,
            SubjectAreaTagMap tagMap,
            String            traversalTerm )
        throws
            TraverseTranslatorException;
}
