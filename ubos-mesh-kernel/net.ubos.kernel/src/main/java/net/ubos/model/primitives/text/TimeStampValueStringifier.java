//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.TimeStampValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies TimeStampValues.
 */
public class TimeStampValueStringifier
        extends
            AbstractStringifier<TimeStampValue>
{
    /**
     * Factory method.
     *
     * @return the created TimePeriodValueStringifier
     */
    public static TimeStampValueStringifier create()
    {
        return new TimeStampValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected TimeStampValueStringifier()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            TimeStampValue        arg,
            StringifierParameters pars,
            String                soFar )
    {
        return arg.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (TimeStampValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeStampValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }
}
