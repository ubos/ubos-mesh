//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.ResourceHelper;
import net.ubos.util.exception.AbstractLocalizedRuntimeException;

/**
 * Indicates that a PropertyValue was illegal.
 */
public abstract class InvalidPropertyValueException
        extends
            AbstractLocalizedRuntimeException
{
    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceHelper findResourceHelperForLocalizedMessage()
    {
        return findResourceHelperForLocalizedMessageViaEnclosingClass();
    }
}
