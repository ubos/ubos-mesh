//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
  * A boolean PropertyValue.
  */
public final class BooleanValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * This is a convenience function, given that this class only has two instances.
     *
     * @param value the corresponding boolean for which we want to determine BooleanValue
     * @return the corresponding BooleanValue
     */
    public static BooleanValue create(
            boolean value )
    {
        if( value ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This is a convenience function, given that this class only has two instances.
     *
     * @param value the corresponding boolean for which we want to determine BooleanValue
     * @return the corresponding BooleanValue
     */
    public static BooleanValue create(
            Boolean value )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        if( value ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This is a convenience function, given that this class only has two instances.
     *
     * @param value the corresponding boolean for which we want to determine BooleanValue
     * @return the corresponding BooleanValue, or null
     */
    public static BooleanValue createOrNull(
            Boolean value )
    {
        if( value == null ) {
            return null;
        }

        if( value ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Private constructor as all instances of this class are pre-defined here.
     *
     * @param value the underlying boolean value
     */
    private BooleanValue(
            boolean value )
    {
        this.theValue = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean getValue()
    {
        return value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BooleanDataType getDataType()
    {
        return BooleanDataType.theDefault;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof BooleanValue ) {
            return this == (BooleanValue) otherValue;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theValue ? 2 : 1; // make up something
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        if( theValue ) {
            return "True";
        } else {
            return "False";
        }
    }

    /**
     * Logical inversion.
     *
     * @return BooleanValue.TRUE if the value is BooleanValue.FALSE, and vice versa
     */
    public BooleanValue not()
    {
        if( theValue ) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        String className = BooleanValue.class.getName();

        if( theValue ) {
            return className + ".TRUE";
        } else {
            return className + ".FALSE";
        }
    }

    /**
     * Special treatment for serialization in order to keep the singleton properties of TRUE and FALSE.
     *
     * @return the read object
     */
    public Object readResolve()
    {
        if( theValue ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        BooleanValue realValue = (BooleanValue) o;

        if( theValue ) {
            if( realValue.theValue ) {
                return 0;
            } else {
                return +1;
            }
        } else {
            if( realValue.theValue ) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    /**
      * The actual value.
      */
    protected boolean theValue;

    /**
      * Pre-defined TRUE value.
      */
    public static final BooleanValue TRUE = new BooleanValue( true );

    /**
      * Pre-defined FALSE value.
      */
    public static final BooleanValue FALSE = new BooleanValue( false );
}
