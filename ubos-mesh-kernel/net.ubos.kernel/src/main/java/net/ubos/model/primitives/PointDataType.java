//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.util.logging.Dumper;

/**
  * A two-dimensional point DataType for PropertyValues.
  */
public class PointDataType
        extends
            DataType
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
      * This is the default instance of this class.
      */
    public static final PointDataType theDefault = new PointDataType();

    /**
     * Factory method. Always returns the same instance.
     *
     * @return the default instance of this class
     */
    public static PointDataType create()
    {
        return theDefault;
    }

    /**
      * Private constructor, there is no reason to instantiate this more than once.
      */
    private PointDataType()
    {
        super( null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof PointDataType ) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return PointDataType.class.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        PointValue realValue = (PointValue) value; // may throw

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<PointValue> getCorrespondingJavaClass()
    {
        return PointValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointValue getDefaultValue()
    {
        return PointValue.create( 0., 0. );
    }

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        return className + DEFAULT_STRING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        Matcher m = DEFAULT_PARSE_PATTERN.matcher( raw.trim() );
        if( m.matches() ) {
            double a = Double.parseDouble( m.group( 1 ));
            double b = Double.parseDouble( m.group( 2 ));

            PointValue ret = PointValue.create( a, b );

            return ret;
        } else {
            throw new PropertyValueParsingException( this, raw );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }
    
    /**
     * The Pattern to parse the default format. We are pretty lenient.
     */
    public static final Pattern DEFAULT_PARSE_PATTERN = Pattern.compile(
            "(?:[\\[\\(])?"
            + "(-?\\d+(?:\\.\\d*)?)"
            + "\\s*"
            + "(?:[:;,]\\s*)?"
            + "(-?\\d+(?:\\.\\d*)?)"
            + "(?:[\\]\\)])?" );
}
