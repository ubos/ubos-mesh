//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.modelbase.InheritanceConflictException;
import net.ubos.util.ArrayHelper;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.modelbase.m.MMeshTypeIdentifier;
import net.ubos.util.logging.Log;

/**
  * <p>A MeshType that can have features, called PropertyTypes, and may participate in an
  *    inheritance hierarchy.
  *
  * <p>In-memory implementation.
  */
public abstract class MMeshTypeWithProperties
        extends
            MCollectableMeshType
        implements
            MeshTypeWithProperties
{
    private static final Log log = Log.getLogInstance( MMeshTypeWithProperties.class );

    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     */
    public MMeshTypeWithProperties(
            MMeshTypeIdentifier identifier )
    {
        super( identifier );
    }

    /**
     * Obtain the value of the Icon property. However, if there is none,
     * return a supertype's Icon instead.
     *
     * @return the value of the Icon property
     */
    @Override
    public final BlobValue getIcon()
    {
        if( theIcon == null ) {
            MeshTypeWithProperties [] supertypes = getDirectSupertypes();
            for( int i=0 ; i<supertypes.length ; ++i ) {
                theIcon = supertypes[i].getIcon();
                if( theIcon != null ) {
                    break;
                }
            }
        }
        return theIcon;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyType [] getLocalPropertyTypes()
    {
        return thePropertyTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyType [] getOverridingLocalPropertyTypes()
    {
        return theOverridingPropertyTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyType [] getInheritedPropertyTypes()
    {
        if( inheritedPropertyTypes == null ) {
            try {
                ArrayList<MPropertyType> almostRet = internalGetInheritedPropertyTypes(); // can throw InheritanceConflictException

                inheritedPropertyTypes = ArrayHelper.copyIntoNewArray(
                        almostRet,
                        MPropertyType.class );
            } finally {
                if( inheritedPropertyTypes == null ) {
                    inheritedPropertyTypes = new MPropertyType[0];
                }
            }
        }
        return inheritedPropertyTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyType [] getPropertyTypes()
    {
        if( allPropertyTypes == null ) {
            try {
                ArrayList<MPropertyType> almostRet = internalGetAllPropertyTypes(); // can throw InheritanceConflictException

                allPropertyTypes = ArrayHelper.copyIntoNewArray(
                        almostRet,
                        MPropertyType.class );
            } finally {
                if( allPropertyTypes == null ) {
                    allPropertyTypes = new MPropertyType[0];
                }
            }
        }
        return allPropertyTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PropertyType [] getOrderedPropertyTypes()
    {
        PropertyType [] ret = getPropertyTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * Add a PropertyType.
     *
     * @param attr the PropertyType to add
     */
    public final void addPropertyType(
            PropertyType attr )
    {
        for( int i=0 ; i<thePropertyTypes.length ; ++i ) {
            if( thePropertyTypes[i].equals( attr )) {
                throw new IllegalArgumentException( "Cannot add: " + attr.getIdentifier() );
            }
        }
        if( attr.getOverride().length != 0 ) {
            throw new IllegalArgumentException( "Cannot add: " + attr.getIdentifier() );
        }

        thePropertyTypes = ArrayHelper.append(
                thePropertyTypes,
                (MPropertyType) attr,
                MPropertyType.class );

        zeroCaches();
    }

    /**
     * Add an overriding PropertyType.
     *
     * @param attr the overriding PropertyType to add
     * @throws InheritanceConflictException thrown if provided PropertyType is not defined in a supertype of
     *          this MeshTypeWithProperties
     */
    public final void addOverridingPropertyType(
            PropertyType attr )
        throws
            InheritanceConflictException
    {
        for( int i=0 ; i<theOverridingPropertyTypes.length ; ++i ) {
            if( theOverridingPropertyTypes[i].equals( attr )) {
                throw new IllegalArgumentException( "Cannot add: " + attr.getIdentifier() );
            }
        }

        PropertyType [] overrides = attr.getOverride();
        if( overrides.length == 0 ) {
            throw new IllegalArgumentException( "Cannot add: " + attr.getIdentifier() );
        }

        // make sure we are not overriding someone else's PropertyType, and that
        // we are not skipping overriding PropertyTypes in supertypes "in between"

        for( int i=0 ; i<overrides.length ; ++i ) {
            boolean found = false;
            for( int j=0 ; j<theSupertypes.length ; ++j ) {
                if( hasNonOverridden( theSupertypes[j], overrides[i] )) {
                    found = true;
                    break;
                }
            }
            if( !found ) {
                throw new InheritanceConflictException( this, new PropertyType[] { attr, overrides[i] } );
            }
        }

        theOverridingPropertyTypes = ArrayHelper.append(
                theOverridingPropertyTypes,
                (MPropertyType) attr,
                MPropertyType.class );
        zeroCaches();
    }

    /**
     * Internal helper to determine whether this MeshTypeWithProperties has
     * this PropertyType locally, or inherited directly (without override).
     *
     * @param amo the MeshTypeWithProperties
     * @param attr the PropertyType to be tested
     * @return true if attr is defined locally in amo, or inherited without override, or overridden locally
     */
    protected static boolean hasNonOverridden(
            MeshTypeWithProperties amo,
            PropertyType           attr )
    {
        PropertyType [] mas;

        // check local PropertyTypes
        mas = amo.getLocalPropertyTypes();
        for( int i=0 ; i<mas.length ; ++i ) {
            if( mas[i] == attr ) {
                return true;
            }
        }

        // check local overridden PropertyTypes
        mas = amo.getOverridingLocalPropertyTypes();
        for( int i=0 ; i<mas.length ; ++i ) {
            if( mas[i] == attr ) {
                return true;
            }
        }

        // check supertypes
        MeshTypeWithProperties [] supertypes = amo.getDirectSupertypes();
        for( int i=0 ; i<supertypes.length ; ++i ) {
            if( hasNonOverridden( supertypes[i], attr )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyType findPropertyTypeByName(
            String name )
    {
        PropertyType [] all = getPropertyTypes();

        for( int i=0 ; i<all.length ; ++i ) {
            if( name.equals( all[i].getName().getAsString())) {
                return all[i];
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyTypeGroup [] getLocalPropertyTypeGroups()
    {
        return thePropertyTypeGroups;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyTypeGroup [] getPropertyTypeGroups()
    {
        ArrayList<MPropertyTypeGroup> almostRet = internalGetAllPropertyTypeGroups();

        // FIXME? need to deal with duplicates in case of multiple inheritance?

        return ArrayHelper.copyIntoNewArray(
                almostRet,
                MPropertyTypeGroup.class );
    }

    /**
     * Add a local PropertyTypeGroup.
     *
     * @param mag the PropertyTypeGroup to be added
     */
    public final void addPropertyTypeGroup(
            MPropertyTypeGroup mag )
    {
        thePropertyTypeGroups = ArrayHelper.append(
                thePropertyTypeGroups,
                mag,
                MPropertyTypeGroup.class );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties [] getDirectSupertypes()
    {
        return theSupertypes;
    }

    /**
     * Set the direct supertypes of this MeshTypeWithProperties.
     *
     * @param supertypes the direct supertypes of this MeshTypeWithProperties
     * @see #getDirectSupertypes
     */
    public abstract void setDirectSupertypes(
            MMeshTypeWithProperties [] supertypes );

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public MMeshTypeWithProperties [] getSupertypes()
    {
        if( allSupertypes == null ) {
            ArrayList<? extends MMeshTypeWithProperties> almostRet = internalGetAllSupertypes();

            allSupertypes = ArrayHelper.<MMeshTypeWithProperties>copyIntoNewArray(almostRet,
                    (Class<MMeshTypeWithProperties>) getClass() );
        }
        return allSupertypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshTypeWithProperties [] getDirectSubtypes()
    {
        return theSubtypes;
    }

    /**
     * Add subtype to this MeshTypeWithProperties if not already there.
     *
     * @param subtype the new subtype to be added
     * @see #getDirectSubtypes
     */
    public abstract void addDirectSubtype(
            MeshTypeWithProperties subtype );

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public MMeshTypeWithProperties [] getSubtypes()
    {
        ArrayList<MMeshTypeWithProperties> almostRet = new ArrayList<>();
        for( int i=0 ; i<theSubtypes.length ; ++i ) {
            MeshTypeWithProperties [] temp = theSubtypes[i].getSubtypes();
            for( int j=0 ; j<temp.length ; ++j ) {
                if( ! almostRet.contains((MMeshTypeWithProperties) temp[j] )) {
                    almostRet.add((MMeshTypeWithProperties) temp[j] );
                }
            }
            almostRet.add( theSubtypes[i] );
        }
        return ArrayHelper.copyIntoNewArray(almostRet, (Class<MMeshTypeWithProperties>) getClass() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean equalsOrIsSupertype(
            MeshTypeWithProperties other )
    {
        return getDistanceToSupertype( other ) >= 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int getDistanceToSupertype(
            MeshTypeWithProperties other )
    {
        if( this == other || equals( other ) ) {
            return 0;
        }

        int bestMatch = Integer.MAX_VALUE;
        for( int i=0 ; i<theSupertypes.length ; ++i ) {
            int current = theSupertypes[i].getDistanceToSupertype( other );
            if( current >= 0 && current < bestMatch ) {
                bestMatch = current;
            }
        }
        if( bestMatch != Integer.MAX_VALUE ) {
            return bestMatch + 1; // one more than zero
        } else {
            return -1;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isSubtypeOfOrEquals(
            MeshTypeWithProperties other )
    {
        return getDistanceToSupertype( other ) >=0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isSubtypeOfDoesNotEqual(
            MeshTypeWithProperties other )
    {
        return getDistanceToSupertype( other ) >0;
    }

    /**
     * Obtain transitive closure of PropertyTypes, with duplicates but
     * without overridden PropertyTypes in supertypes.
     *
     * @return an ArrayList containing a transitive closure of PropertyTypes
     */
    protected final ArrayList<MPropertyType> internalGetAllPropertyTypes()
    {
        ArrayList<MPropertyType> ret = new ArrayList<>( 20 ); // fudge factor
        for( int i=0 ; i<thePropertyTypes.length ; ++i ) {
            ret.add( thePropertyTypes[i] );
        }
        for( int i=0 ; i<theOverridingPropertyTypes.length ; ++i ) {
            ret.add( theOverridingPropertyTypes[i] );
        }

        for( int i=0 ; i<theSupertypes.length ; ++i ) {
            PropertyType [] inherited = theSupertypes[i].getPropertyTypes();

            for( int j=0 ; j<inherited.length ; ++j ) {
                PropertyType overrideRet = null;

                Iterator<MPropertyType> theIter = ret.iterator();
                while( theIter.hasNext() ) {
                    PropertyType currentRet = theIter.next();
                    if( currentRet.getOverrideAncestor() == inherited[j].getOverrideAncestor() ) {
                        overrideRet = currentRet;
                        break;
                    }
                }
                if( overrideRet == null ) {
                    // no override
                    ret.add( (MPropertyType) inherited[j] );
                } else if( overrideRet == inherited[j] ) {
                    // same from two sides, skip
                } else if( overrideRet.overrides( inherited[j] )) {
                   // correct override, skip
                } else if( inherited[j].overrides( overrideRet )) {
                    // correct override -- remove old, put new, unless it's a locally overridden thing in which
                    // case we have an error
                    if( ArrayHelper.isIn( overrideRet, theOverridingPropertyTypes, false )) {
                        log.error( "Inheritance conflict", this, overrideRet );
                    }

                    ret.remove( (MPropertyType) overrideRet );
                    ret.add( (MPropertyType) inherited[j] );
                } else {
                    // incorrect override, error
                    log.error( "Incorrect override", this, overrideRet );
                }
            }
        }

        return ret;
    }

    /**
     * Obtain transitive closure of PropertyTypeGroups, with duplicates.
     *
     * @return an ArrayList containing the transitive closure of PropertyTypeGroups
     */
    protected final ArrayList<MPropertyTypeGroup> internalGetAllPropertyTypeGroups()
    {
        ArrayList<MPropertyTypeGroup> ret = new ArrayList<>( 20 ); // fudge factor
        for( int i=0 ; i<thePropertyTypeGroups.length ; ++i ) {
            ret.add( thePropertyTypeGroups[i] );
        }

        for( int i=0 ; i<theSupertypes.length ; ++i ) {
            PropertyTypeGroup [] inherited = theSupertypes[i].getPropertyTypeGroups();

            for( int j=0 ; j<inherited.length ; ++j ) {
                if( !ret.contains( (MPropertyTypeGroup) inherited[j] )) {
                    ret.add( (MPropertyTypeGroup) inherited[j] );
                }
            }
        }
        return ret;
    }

    /**
     * Obtain transitive closure of inherited (but not local) PropertyTypes, with duplicates but
     * without overridden PropertyTypes in supertypes.
     *
     * @return an ArrayList containing a transitive closure of PropertyTypes
     */
    protected final ArrayList<MPropertyType> internalGetInheritedPropertyTypes()
    {
        ArrayList<MPropertyType> ret = new ArrayList<>( 20 ); // fudge factor

        for( int i=0 ; i<theSupertypes.length ; ++i ) {
            PropertyType [] inherited = theSupertypes[i].getPropertyTypes();

            for( int j=0 ; j<inherited.length ; ++j ) {
                PropertyType overrideRet = null;

                Iterator<MPropertyType> theIter = ret.iterator();
                while( theIter.hasNext() ) {
                    PropertyType currentRet = theIter.next();
                    if( currentRet.getOverrideAncestor() == inherited[j].getOverrideAncestor() ) {
                        overrideRet = currentRet;
                        break;
                    }
                }
                if( overrideRet == null ) {
                    // no override
                    ret.add( (MPropertyType) inherited[j] );
                } else if( overrideRet == inherited[j] ) {
                    // same from two sides, skip
                } else if( overrideRet.overrides( inherited[j] )) {
                   // correct override, skip
                } else if( inherited[j].overrides( overrideRet )) {
                    // correct override -- remove old, put new, unless it's a locally overridden thing in which
                    // case we have an error
                    if( ArrayHelper.isIn( overrideRet, theOverridingPropertyTypes, false )) {
                        log.error( "Inheritance conflict", this, overrideRet );
                    }

                    ret.remove( (MPropertyType) overrideRet );
                    ret.add( (MPropertyType) inherited[j] );
                } else {
                    // incorrect override, error
                    log.error( "Incorrect override", this, overrideRet );
                }
            }
        }

        return ret;
    }

    /**
     * Obtain transitive closure of all supertypes.
     *
     * @return an ArrayList containing all supertypes.
     */
    protected final ArrayList<MMeshTypeWithProperties> internalGetAllSupertypes()
    {
        ArrayList<MMeshTypeWithProperties> ret = new ArrayList<>( 20 ); // fudge factor

        for( int i=0 ; i<theSupertypes.length ; ++i ) {
            MeshTypeWithProperties [] inherited = theSupertypes[i].getSupertypes();

            for( int j=0 ; j<inherited.length ; ++j ) {
                if( !ret.contains((MMeshTypeWithProperties) inherited[j] )) {
                    ret.add((MMeshTypeWithProperties) inherited[j] );
                }
            }
            if( !ret.contains( theSupertypes[i] )) {
                ret.add( theSupertypes[i] );
            }
        }
        return ret;
    }

    /**
     * This internal helper zeros out cached values. It is invoked in case the inheritance
     * hierarchy changes, such as when loading additional SubjectAreas at run-time..
     */
    protected void zeroCaches()
    {
        allPropertyTypes = null;
        allSupertypes    = null;

        for( int i=0 ; i<theSubtypes.length ; ++i ) {
            theSubtypes[i].zeroCaches();
        }
    }

    /**
     * The PropertyTypes locally defined on this MeshTypeWithProperties.
     */
    private MPropertyType [] thePropertyTypes = new MPropertyType[0];

    /**
     * The PropertyTypes overriding inherited PropertyTypes for this MeshTypeWithProperties.
     */
    private MPropertyType [] theOverridingPropertyTypes = new MPropertyType[0];

    /**
     * The PropertyTypeGroups locally defined in this MeshTypeWithProperties.
     */
    private MPropertyTypeGroup [] thePropertyTypeGroups = new MPropertyTypeGroup[0];

    /**
     * This caches the set of all inherited PropertyTypes.
     * This goes with method zeroCaches().
     */
    private transient MPropertyType [] inheritedPropertyTypes = null;

    /**
     * This caches the set of all PropertyTypes, local and inherited. If we don't cache
     * this, we'll have to recalculate every time we determine the set of all
     * (local and inherited) PropertyTypes and that takes a while.
     * This goes with method zeroCaches().
     */
    private transient MPropertyType [] allPropertyTypes = null;

    /**
     * The set of direct supertypes of this MeshTypeWithProperties.
     * This is actually never an array of MeshTypeWithProperties, but always either
     * an array of EntityType or RoleType.
     */
    protected MMeshTypeWithProperties [] theSupertypes = ArrayHelper.createArray( getClass(), 0 );

    /**
     * The set of direct subtypes of this MeshTypeWithProperties in the working model.
     * This is actually never an array of MeshTypeWithProperties, but always either
     * an array of EntityType or RoleType.
     */
    protected MMeshTypeWithProperties [] theSubtypes = ArrayHelper.createArray( getClass(), 0 );

    /**
     * This caches the set of all supertypes. If we don't cache this,
     * we'll have to recalculate every time we determine the set of all (direct and
     * indirect) supertypes and that takes a while. This goes with method zeroCaches().
     */
    private transient MMeshTypeWithProperties [] allSupertypes = null;
}
