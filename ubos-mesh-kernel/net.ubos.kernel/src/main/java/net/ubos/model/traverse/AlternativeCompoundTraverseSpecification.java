//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.meshbase.MeshBase;
import net.ubos.util.ArrayHelper;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;

/**
 * Implementation of a CompoundTraverseSpecification that traverses several
 * other TraverseSpecifications in parallel. In other words, the result of a traversal,
 * using this AlternativeCompoundTraverseSpecification from a certain MeshObject, is
 * the unification of all results of traversing from the same MeshObject using the
 * component TraverseSpecifications.
 */
public class AlternativeCompoundTraverseSpecification
        extends
            AbstractTraverseSpecification
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method.
     *
     * @param alt the components of this AlternativeCompoundTraverseSpecification
     * @return the created AlternativeCompoundTraverseSpecification
     */
    public static AlternativeCompoundTraverseSpecification create(
            TraverseSpecification [] alt )
    {
        // we are paranoid, so we check
        if( alt == null || alt.length < 2 ) {
            throw new IllegalArgumentException( ArrayHelper.arrayToString( alt ) + " must be at least of length 2" );
        }

        for( int i=0 ; i<alt.length ; ++i ) {
            if( alt[i] == null ) {
                throw new IllegalArgumentException( "Alternative TraverseSpecification is null: " + ArrayHelper.arrayToString( alt ));
            }
        }
        return new AlternativeCompoundTraverseSpecification( alt );
    }

    /**
     * Convenience factory method.
     *
     * @param alt1 the first traversal alternative
     * @param alt2 the second traversal alternative
     * @return the created AlternativeCompoundTraverseSpecification
     */
    public static AlternativeCompoundTraverseSpecification create(
            TraverseSpecification alt1,
            TraverseSpecification alt2 )
    {
        return create( new TraverseSpecification[] { alt1, alt2 } );
    }

    /**
     * Convenience factory method.
     *
     * @param alt1 the first traversal alternative
     * @param alt2 the second traversal alternative
     * @param alt3 the third traversal alternative
     * @return the created AlternativeCompoundTraverseSpecification
     */
    public static AlternativeCompoundTraverseSpecification create(
            TraverseSpecification alt1,
            TraverseSpecification alt2,
            TraverseSpecification alt3 )
    {
        return create( new TraverseSpecification[] { alt1, alt2, alt3 } );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param alt the components of this AlternativeCompoundTraverseSpecification
     */
    private AlternativeCompoundTraverseSpecification(
            TraverseSpecification [] alt )
    {
        theAlternatives = alt;
    }

    /**
     * Obtain the component alternatives.
     *
     * @return the component alternatives
     */
    public TraverseSpecification [] getAlternatives()
    {
        return theAlternatives;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObject start )
    {
        MeshObjectSet ret = theAlternatives[0].traverse( start );

        for( int i=1 ; i<theAlternatives.length ; ++i ) {
            ret = ret.union( theAlternatives[i].traverse( start ));
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObjectSet theSet )
    {
        MeshObjectSetFactory setFactory = theSet.getFactory();

        MeshObjectSet [] sets = new MeshObjectSet[ theAlternatives.length ];
        for( int i=0 ; i<sets.length ; ++i ) {
            sets[i] = theAlternatives[i].traverse( theSet );
        }
        return setFactory.createImmutableMeshObjectSetUnification( sets );
    }

    /**
     * Determine equality.
     *
     * @param other the object to compare against
     * @return true if the two objects are equal
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof AlternativeCompoundTraverseSpecification ) {

            AlternativeCompoundTraverseSpecification realOther = (AlternativeCompoundTraverseSpecification) other;

            if( theAlternatives.length != realOther.theAlternatives.length ) {
                return false;
            }

            for( int i=0 ; i<theAlternatives.length ; ++i ) {
                if( !theAlternatives[i].equals( realOther.theAlternatives[i] )) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Hash code.
     *
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        for( TraverseSpecification current : theAlternatives ) {
            ret ^= current.hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAffectedBy(
            MeshBase                  meshBase,
            AbstractMeshObjectRoleTypeChange theEvent )
    {
        for( int i=0 ; i<theAlternatives.length ; ++i ) {

            if( theAlternatives[i].isAffectedBy( meshBase, theEvent )) {
                return true;
            }
        }
        return false;
    }

    /**
     * The alternatives to traverse.
     */
    protected TraverseSpecification [] theAlternatives;
}
