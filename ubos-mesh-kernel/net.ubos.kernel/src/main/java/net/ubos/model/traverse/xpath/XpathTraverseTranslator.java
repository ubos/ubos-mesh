//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse.xpath;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.ByTypeMeshObjectSelector;
import net.ubos.mesh.set.MeshObjectSelector;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.traverse.AllNeighborsTraverseSpecification;
import net.ubos.model.traverse.AlternativeCompoundTraverseSpecification;
import net.ubos.model.traverse.SelectiveTraverseSpecification;
import net.ubos.model.traverse.SequentialCompoundTraverseSpecification;
import net.ubos.model.traverse.StayRightHereTraverseSpecification;
import net.ubos.model.traverse.TraverseSpecification;
import net.ubos.model.traverse.TraverseTranslator;
import net.ubos.model.traverse.TraverseTranslatorException;
import net.ubos.model.util.SubjectAreaTagMap;
import net.ubos.modelbase.CollectableMeshTypeNotFoundException;
import net.ubos.util.logging.Log;

import java.util.ArrayList;

/**
 * <p>A TraverseTranslator that uses XPath-style expressions.</p>
 */
public class XpathTraverseTranslator
    implements
        TraverseTranslator
{
    private static final Log log = Log.getLogInstance( XpathTraverseTranslator.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @return the created XpathTraversalTranslator
     */
    public static XpathTraverseTranslator create()
    {
        return new XpathTraverseTranslator();
    }

    /**
     * Constructor.
     */
    protected XpathTraverseTranslator() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public TraverseSpecification translateTraverseSpecification(
            MeshObject        startObject,
            SubjectAreaTagMap tagMap,
            String            traversalTerm )
        throws
            TraverseTranslatorException
    {
        ArrayList<String> components = new ArrayList<>();

        int start            = 0;
        int parenthesisLevel = 0;
        boolean foundAxis = false;
        boolean foundAlt  = false;

        for( int i=0 ; i<traversalTerm.length() ; ++i ) {
            switch( traversalTerm.charAt( i ) ) {
                case PARENTHESIS_OPEN:
                    ++parenthesisLevel;
                    break;
                case PARENTHESIS_CLOSE:
                    --parenthesisLevel;
                    if( parenthesisLevel < 0 ) {
                        throw new XpathTraverseTranslatorException( traversalTerm, i, "Cannot close more parentheses than are open" );
                    }
                    break;
                case AXIS_SEPARATOR:
                    if( parenthesisLevel == 0 ) {
                        if( foundAlt ) {
                            throw new XpathTraverseTranslatorException( traversalTerm, i, "Use parenthesis to disambiguate precedence between " + AXIS_SEPARATOR + " and " + ALTERNATIVE_SEPARATOR );
                        }
                        foundAxis = true;
                        components.add( subexpression( traversalTerm, start, i ) );
                        start = i+1;
                    }
                    break;
                case ALTERNATIVE_SEPARATOR:
                    if( parenthesisLevel == 0 ) {
                        if( foundAxis ) {
                            throw new XpathTraverseTranslatorException( traversalTerm, i, "Use parenthesis to disambiguate precedence between " + AXIS_SEPARATOR + " and " + ALTERNATIVE_SEPARATOR );
                        }
                        foundAlt = true;
                        components.add( subexpression( traversalTerm, start, i ) );
                        start = i+1;
                    }
                    break;
                default:
                    // no op
                    break;
            }
        }
        components.add( subexpression( traversalTerm, start, traversalTerm.length() ));

        if( components.size() > 1 ) {
            TraverseSpecification [] steps = new TraverseSpecification[ components.size() ];
            for( int i=0 ; i<steps.length ; ++i ) {
                steps[i] = translateTraverseSpecification( startObject, tagMap, components.get( i ));
            }
            if( foundAxis ) {
                return SequentialCompoundTraverseSpecification.create( steps );

            } else if( foundAlt ) {
                return AlternativeCompoundTraverseSpecification.create( steps );

            } else {
                log.error( "Unexpected in " + traversalTerm );
                return null;
            }
        }

        String    singleComponent = components.get( 0 );
        String [] triple          = parseSinglePathComponent( singleComponent, traversalTerm, 0 );

        if( triple[0] != null || triple[2] != null ) {
            MeshObjectSelector     startSelector = triple[0] != null ? qualifierToMeshObjectSelector( triple[0], traversalTerm, 0, tagMap ) : null;
            MeshObjectSelector     endSelector   = triple[2] != null ? qualifierToMeshObjectSelector( triple[2], traversalTerm, 0, tagMap ) : null;
            TraverseSpecification qualified     = translateTraverseSpecification( startObject, tagMap, triple[1] );

            return SelectiveTraverseSpecification.create(
                    startSelector,
                    qualified,
                    endSelector );
        }
        if( ALL_NEIGHBORS.equals( triple[1] )) {
            return AllNeighborsTraverseSpecification.create();
        }
        if( STAY_RIGHT_HERE.equals( triple[1] )) {
            return StayRightHereTraverseSpecification.create();
        }

        return findRoleType( traversalTerm, traversalTerm, 0, tagMap );
    }

    /**
     * Helper method to clean up a sub-expression string.
     *
     * @param raw the raw String
     * @param start start index (inclusive)
     * @param end end index (exclusive)
     * @return cleaned-up version
     */
    protected static String subexpression(
            String raw,
            int    start,
            int    end )
    {
        String ret = raw.substring( start, end );
        ret = ret.trim();
        if( ret.charAt( 0 ) == PARENTHESIS_OPEN && ret.charAt( ret.length()-1 ) == PARENTHESIS_CLOSE ) {
            ret = ret.substring( 1, ret.length()-1 );
            ret = ret.trim();
        }
        return ret;
    }

    /**
     * Parse a pre-condition/node/post-condition triple.
     *
     * @param toParse the String
     * @param traversalTerm the term being parsed
     * @param index the index in the term
     * @return array of 3 elements
     * @throws TraversalTranslatorException thrown if the translation failed
     */
    protected static String[] parseSinglePathComponent(
            String toParse,
            String traversalTerm,
            int    index )
        throws
            TraverseTranslatorException
    {
        toParse = toParse.trim();

        String [] ret = new String[3];
        int currentSection = -1;
        int bracketLevel   = 0;
        int start          = 0;

        for( int i=0 ; i<toParse.length() ; ++i ) {
            switch( toParse.charAt( i )) {
                case QUALIFIER_OPEN:
                    ++bracketLevel;
                    if( currentSection == -1 ) {
                        currentSection = 0;
                    } else if( currentSection == 1 ) {
                        currentSection = 2;
                        ret[1] = toParse.substring( start, i );
                        start = i+1;
                    }
                    break;
                case QUALIFIER_CLOSE:
                    if( currentSection == -1 || currentSection == 1 ) {
                        throw new XpathTraverseTranslatorException( traversalTerm, index+i, "Cannot close ] before open [" );
                    } else if( currentSection == 0 ) {
                        --bracketLevel;
                        if( bracketLevel < 0 ) {
                            throw new XpathTraverseTranslatorException( traversalTerm, index+i, "Cannot close more brackets than are open" );
                        } else if( bracketLevel == 0 ) {
                            currentSection = 1;
                            ret[0] = toParse.substring( start, i );
                            start = i+1;
                        }
                    } else if( currentSection == 2 ) {
                        --bracketLevel;
                        if( bracketLevel < 0 ) {
                            throw new XpathTraverseTranslatorException( traversalTerm, index+i, "Cannot close more brackets than are open" );
                        } else if( bracketLevel == 0 ) {
                            currentSection = 3;
                            ret[2] = toParse.substring( start, i );
                            start = i+1;
                            if( start < toParse.length() ) {
                                throw new XpathTraverseTranslatorException( traversalTerm, index+i, "Leftover characters at end of qualifier" );
                            }
                        }
                    }
                    break;
                default:
                    if( currentSection == -1 ) {
                        currentSection = 1;
                    }
            }
        }
        if( currentSection == 1 ) {
            ret[1] = toParse.substring( start );
        }
        return ret;
    }

    /**
     * Convert a qualifier expression back into a MeshObjectSelector.
     *
     * @param toParse the expression
     * @param traversalTerm the term being parsed
     * @param index the index in the term
     * @param tagMap the interpretation of the prefixes contained in the pseudoXpath
     * @return the MeshObjectSelector
     * @throws TraversalTranslatorException thrown if the translation failed
     */
    public static MeshObjectSelector qualifierToMeshObjectSelector(
            String            toParse,
            String            traversalTerm,
            int               index,
            SubjectAreaTagMap tagMap )
        throws
            TraverseTranslatorException
    {
        toParse = toParse.trim();

        if( toParse.startsWith( HAS_TYPE_FUNCTION + PARENTHESIS_OPEN ) && toParse.charAt( toParse.length()-1 ) == PARENTHESIS_CLOSE ) {
            String middle = toParse.substring( HAS_TYPE_FUNCTION.length() + 1, toParse.length()-1 ).trim();
            if( middle.startsWith( ".," )) {
                String     typeString = middle.substring( 2 );
                EntityType type       = findEntityType( typeString, traversalTerm, index, tagMap );

                return ByTypeMeshObjectSelector.create( type, true );
            }
        } else if( toParse.startsWith( HAS_EXACT_TYPE_FUNCTION + PARENTHESIS_OPEN ) && toParse.charAt( toParse.length()-1 ) == PARENTHESIS_CLOSE ) {
            String middle = toParse.substring( HAS_EXACT_TYPE_FUNCTION.length() + 1, toParse.length()-1 ).trim();
            if( middle.startsWith( ".," )) {
                String     typeString = middle.substring( 2 );
                EntityType type       = findEntityType( typeString, traversalTerm, index, tagMap );

                return ByTypeMeshObjectSelector.create( type, false );
            }
        }
        throw new XpathTraverseTranslatorException( traversalTerm, index, "Sorry, can't understand your MeshObjectSelector (this is not a very complete implementation)" );
    }

    /**
     * Helper method to find an EntityType given as String.
     *
     * @param toParse the String
     * @param traversalTerm the term being parsed
     * @param index the index in the term
     * @param tagMap the interpretation of the prefixes contained in the pseudoXpath
     * @return the MeshType, or null
     * @throws TraversalTranslatorException thrown if the translation failed
     */
    protected static EntityType findEntityType(
            String            toParse,
            String            traversalTerm,
            int               index,
            SubjectAreaTagMap tagMap )
        throws
            TraverseTranslatorException
    {
        try {
            EntityType ret = (EntityType) tagMap.findCollectableMeshType( toParse );
            if( ret == null ) {
                throw new XpathTraverseTranslatorException( traversalTerm, index, "Cannot find EntityType" );
            }
            return ret;

        } catch( CollectableMeshTypeNotFoundException ex ) {
            throw new XpathTraverseTranslatorException( traversalTerm, index, "EntityType cannot be found", ex );

        } catch( ClassCastException ex ) {
            throw new XpathTraverseTranslatorException( traversalTerm, index, "Does not refer to an EntityType", ex );
        }
    }

    /**
     * Helper method to find a RoleType given as String.
     *
     * @param toParse the String
     * @param traversalTerm the term being parsed
     * @param index the index in the term
     * @param tagMap the interpretation of the prefixes contained in the pseudoXpath
     * @return the MeshType, or null
     * @throws TraversalTranslatorException thrown if the translation failed
     */
    protected static RoleType findRoleType(
            String            toParse,
            String            traversalTerm,
            int               index,
            SubjectAreaTagMap tagMap )
        throws
            TraverseTranslatorException
    {
        try {
            RoleType ret = (RoleType) tagMap.findCollectableMeshType( toParse );
            if( ret == null ) {
                throw new XpathTraverseTranslatorException( traversalTerm, index, "Cannot find RoleType" );
            }
            return ret;

        } catch( CollectableMeshTypeNotFoundException ex ) {
            throw new XpathTraverseTranslatorException( traversalTerm, index, "RoleType cannot be found", ex );

        } catch( ClassCastException ex ) {
            throw new XpathTraverseTranslatorException( traversalTerm, index, "Does not refer to a RoleType", ex );
        }
    }

    /**
     * Separates prefix and name.
     */
    public static final char COLON = ':';

    /**
     * Indicates any traversal.
     */
    public static final String ALL_NEIGHBORS = "*";

    /**
     * Indicates traversal to self.
     */
    public static final String STAY_RIGHT_HERE = ".";

    /**
     * Separates the components of the Xpath.
     */
    public static final char AXIS_SEPARATOR = '/';

    /**
     * Separates the alternatives in a step.
     */
    public static final char ALTERNATIVE_SEPARATOR = '|';

    /**
     * Opens up a qualifier.
     */
    public static final char QUALIFIER_OPEN = '[';

    /**
     * Closes a qualifier.
     */
    public static final char QUALIFIER_CLOSE = ']';

    /**
     * Open parenthesis.
     */
    public static final char PARENTHESIS_OPEN = '(';

    /**
     * Close parenthesis.
     */
    public static final char PARENTHESIS_CLOSE = ')';

//    /**
//     * Logical inversion.
//     */
//    public static final char NOT = '!';

    /**
     * Has-type function.
     */
    public static final String HAS_TYPE_FUNCTION = "has-type";

    /**
     * Has-exact-type function.
     */
    public static final String HAS_EXACT_TYPE_FUNCTION = "has-exact-type";
}
