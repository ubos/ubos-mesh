//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.StringValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies StringValues.
 */
public class StringValueStringifier
        extends
            AbstractStringifier<StringValue>
{
    /**
     * Factory method.
     *
     * @return the created StringValueStringifier
     */
    public static StringValueStringifier create()
    {
        return new StringValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected StringValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            StringValue           arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = arg.value();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (StringValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        return StringValue.create( rawString );
    }
}
