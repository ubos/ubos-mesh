//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * Thrown if the specified MultiplicityValue is illegal.
 */
public class InvalidMultiplicityException
        extends
            InvalidPropertyValueException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     * 
     * @param min the minimum value specified
     * @param max the maximum value specified
     */
    public InvalidMultiplicityException(
            int min,
            int max )
    {
        theMin = min;
        theMax = max;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            theMin != MultiplicityValue.N ? String.valueOf( theMin ) : MultiplicityValue.N_SYMBOL,
            theMax != MultiplicityValue.N ? String.valueOf( theMax ) : MultiplicityValue.N_SYMBOL,
        };
    }
    
    /**
     * The minimum value.
     */
    protected int theMin;
    
    /**
     * The maximum value.
     */
    protected int theMax;
}
