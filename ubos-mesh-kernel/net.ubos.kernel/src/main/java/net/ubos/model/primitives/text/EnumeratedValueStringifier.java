//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies EnumeratedValues.
 */
public class EnumeratedValueStringifier
        extends
            AbstractStringifier<EnumeratedValue>
{
    /**
     * Factory method.
     *
     * @return the created EnumeratedValueStringifier
     * @param emitUserVisible if false, emit computable name; if true, emit user-visible name.
     */
    public static EnumeratedValueStringifier create(
            boolean emitUserVisible )
    {
        return new EnumeratedValueStringifier( emitUserVisible );
    }

    /**
     * Private constructor for subclasses only, use factory method.
     *
     * @param emitUserVisible if false, emit computable name; if true, emit user-visible name.
     */
    protected EnumeratedValueStringifier(
            boolean emitUserVisible )
    {
        theEmitUserVisible = emitUserVisible;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            EnumeratedValue       arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret;
        if( theEmitUserVisible ) {
            ret = arg.getUserVisibleName().value();
        } else {
            ret = arg.value();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (EnumeratedValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnumeratedValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        EnumeratedDataType type = (EnumeratedDataType) factory;

        for( EnumeratedValue candidate : type.getDomain() ) {
            String candidateName = candidate.value();

            if( candidateName.equals( rawString )) {
                return candidate;
            }
        }
        return null;
    }

    /**
     * If false, emit computable name. If true, emit user-visible name.
     */
    protected boolean theEmitUserVisible;
}
