//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.model.util.SubjectAreaTagMap;
import net.ubos.modelbase.ModelBase;

/**
 * Knows how to look up RoleTypes, and if something isn't, delegate to another TraverseTranslator.
 */
public class RoleTypeTraverseTranslator
     implements
        TraverseTranslator
{
    /**
     * Factory method.
     *
     * @return the created instance
     */
    public static RoleTypeTraverseTranslator create()
    {
        return new RoleTypeTraverseTranslator( null );
    }

    /**
     * Factory method.
     *
     * @param delegate: delegate to this TranversalTranslator if needed
     * @return the created instance
     */
    public static RoleTypeTraverseTranslator create(
            TraverseTranslator delegate )
    {
        return new RoleTypeTraverseTranslator( delegate );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param delegate: delegate to this TranversalTranslator if needed
     */
    protected RoleTypeTraverseTranslator(
            TraverseTranslator delegate )
    {
        theDelegate = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TraverseSpecification translateTraverseSpecification(
            MeshObject        startObject,
            SubjectAreaTagMap tagMap,
            String            traversalTerm )
        throws
            TraverseTranslatorException
    {
        TraverseSpecification ret = ModelBase.SINGLETON.findRoleTypeOrNull( traversalTerm );
        if( ret == null ) {
            if( theDelegate == null ) {
                throw new TraverseTranslatorException( traversalTerm );
            } else {
                ret = theDelegate.translateTraverseSpecification( startObject, tagMap, traversalTerm );
            }
        }
        return ret;
    }

    /**
     * The delegate.
     */
    protected final TraverseTranslator theDelegate;
}
