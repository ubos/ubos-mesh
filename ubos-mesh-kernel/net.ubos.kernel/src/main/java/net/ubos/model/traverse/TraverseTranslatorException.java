//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Thrown if a TraverseSpecification could not be translated. This may be overridden by subclasses.
 */
public class TraverseTranslatorException
    extends
        AbstractLocalizedException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param term the term that was given
     */
    public TraverseTranslatorException(
            String term )
    {
        theTerm = term;
    }

    /**
     * Constructor with a message.
     *
     * @param term the term that was given
     * @param msg the message
     */
    public TraverseTranslatorException(
            String term,
            String msg )
    {
        super( msg );

        theTerm = term;
    }

    /**
     * Constructor with no message but a cause.
     *
     * @param term the term that was given
     * @param cause the Throwable that caused this Exception
     */
    public TraverseTranslatorException(
            String    term,
            Throwable cause )
    {
        super( cause );

        theTerm = term;
    }

    /**
     * Constructor with a message and a cause.
     *
     * @param term the term that was given
     * @param msg the message
     * @param cause the Exception that caused this Exception
     */
    public TraverseTranslatorException(
            String    term,
            String    msg,
            Throwable cause )
    {
        super( msg, cause );

        theTerm = term;
    }

    /**
     * Obtain the traversal term that was given.
     *
     * @return the term
     */
    public String getTerm()
    {
        return theTerm;
    }

    /**
     * Obtain resource parameters for the internationalization.
     *
     * @return the resource parameters
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theTerm };
    }

    /**
     * The term that was given.
     */
    protected String theTerm;
}
