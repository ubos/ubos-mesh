//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.ResourceHelper;
import net.ubos.util.StringHelper;

/**
 * Captures everything we know about a MIME type, and the MIME types we know.
 */
public class SelectableMimeType
{
    /**
     * Constructor.
     *
     * @param name the programmatic name
     * @param mime the actual MIME type
     * @param extensions the typical file name extensions for this MIME type
     */
    public SelectableMimeType(
            String name,
            String mime,
            String [] extensions )
    {
        theName = name;
        theMime = mime;
        theExtensions = extensions;
    }

    /**
     * Obtain the programmatic name of the MIME type.
     *
     * @return the name
     */
    public String getName()
    {
        return theName;
    }

    /**
     * Obtain the user-visible name of the MIME type.
     *
     * @return the user-visible name
     */
    public String getUserVisibleName()
    {
        return theResourceHelper.getResourceString( theName + "Name" );
    }

    /**
     * Obtain the actual MIME type.
     *
     * @return the MIME type
     */
    public String getMimeType()
    {
        return theMime;
    }

    /**
     * Obtain the typical file name extensions for this MIME type.
     *
     * @return the extensions
     */
    public String [] getExtensions()
    {
        return theExtensions;
    }

    /**
     * Find the right MIME type given this file name extensions.
     *
     * @param ext the extension
     * @return the SelectableMimeType or OCTET_STREAM
     */
    public static SelectableMimeType findByExtension(
            String ext )
    {
        // This can be made faster
        for( SelectableMimeType candidate : KNOWN_MIME_TYPES ) {
            for( String candidateExt : candidate.getExtensions() ) {
                if( ext.equals( candidateExt )) {
                    return candidate;
                }
            }
        }
        return OCTET_STREAM;
    }

    /**
     * The programmatic name of the MIME type, to use as key for ResourceHelper lookup.
     */
    protected final String theName;

    /**
     * The actual MIME type, such as "text/plain".
     */
    protected final String theMime;

    /**
     * The typical file name extensions for this MIME type.
     */
    protected final String [] theExtensions;

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( SelectableMimeType.class );

    /**
     * Pre-defined MIME type for text/plain.
     */
    public static final SelectableMimeType TEXT_PLAIN
            = new SelectableMimeType( "TextPlain", "text/plain", new String[] { "txt" } );

    /**
     * Pre-defined MIME type for text/html.
     */
    public static final SelectableMimeType TEXT_HTML
            = new SelectableMimeType( "TextHtml", "text/html", new String[] { "html", "htm" } );

    /**
     * Pre-defined MIME type for image/gif.
     */
    public static final SelectableMimeType IMAGE_GIF
            = new SelectableMimeType( "ImageGif", "image/gif", new String[] { "gif" } );

    /**
     * Pre-defined MIME type for image/jpeg.
     */
    public static final SelectableMimeType IMAGE_JPEG
            = new SelectableMimeType( "ImageJpeg", "image/jpeg", new String[] { "jpeg", "jpg" } );

    /**
     * Pre-defined MIME type for image/png.
     */
    public static final SelectableMimeType IMAGE_PNG
            = new SelectableMimeType( "ImagePng", "image/png", new String[] { "png" } );

    public static final SelectableMimeType VIDEO_MP4
            = new SelectableMimeType( "ImageMp4", "video/mp4", new String[] { "mp4" } );

    /**
     * Pre-defined MIME type for favicons.
     */
    public static final SelectableMimeType IMAGE_FAVICON
            = new SelectableMimeType( "TextFavicon", "image/x-icon", new String[] { "ico" } );

    /**
     * Pre-defined MIME type for XML.
     */
    public static final SelectableMimeType TEXT_XML
            = new SelectableMimeType( "TextXml", "text/xml", new String[] { "xml" } );

    /**
     * Pre-defined MIME type for XML.
     */
    public static final SelectableMimeType APPLICATION_XML
            = new SelectableMimeType( "ApplicationXml", "application/xml", new String[] { "xml" });

    /**
     * Pre-defined MIME type for JSON.
     */
    public static final SelectableMimeType TEXT_JSON
            = new SelectableMimeType( "TextJson", "text/json", new String[] { "json" } );

    /**
     * Pre-defined MIME type for comma-separated values.
     */
    public static final SelectableMimeType TEXT_CSV
            = new SelectableMimeType( "TextCsv", "text/csv", new String[] { "csv" } );

    /**
     * Pre-defined MIME type for unknown.
     */
    public static final SelectableMimeType OCTET_STREAM
            = new SelectableMimeType( "OctetStream", "application/octet-stream", StringHelper.EMPTY_ARRAY );

    /**
     * The set of all known MIME types. FIXME, insert longer list.
     */
    protected static final SelectableMimeType [] KNOWN_MIME_TYPES = {
        TEXT_PLAIN,
        TEXT_HTML,
        IMAGE_GIF,
        IMAGE_JPEG,
        IMAGE_PNG,
        IMAGE_FAVICON,
        VIDEO_MP4,
        TEXT_XML,
        APPLICATION_XML,
        TEXT_JSON,
        TEXT_CSV,
        OCTET_STREAM
    };
}
