//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Base64;

/**
 * Collects functionality useful for many implementations of PropertyValueDecoder.
 */
public abstract class AbstractPropertyValueDecoder
    implements
        PropertyValueDecoder
{
    /**
     * Parse a Serializable from its String representation back.
     *
     * @param s the String
     * @return the value
     * @throws DecodingException thrown if decoding failed
     * @throws IOException an I/O problem occurred
     */
    public Serializable base64StringAsSerializable(
            String s )
        throws
            DecodingException,
            IOException
    {
        if( s == null ) {
            return null;
        }
        byte [] decoded = Base64.getDecoder().decode( s );

        try( ByteArrayInputStream stream = new ByteArrayInputStream( decoded  )) {

            ObjectInputStream stream2 = new ObjectInputStream( stream );
            Serializable ret = (Serializable) stream2.readObject();

            return ret;

        } catch( ClassNotFoundException ex ) {
            throw new DecodingException.Installation( ex );
        }
    }

}
