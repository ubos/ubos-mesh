//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
  * The root of the inheritance hierarchy for the type objects. All classes whose
  * instances represent model elements inherit from here.
  */
public interface MeshType
{
    /**
      * Obtain the Identifier of this MeshType.
      *
      * @return the Identifier
      */
    public MeshTypeIdentifier getIdentifier();

    /**
      * Obtain the value of the Name property.
      *
      * @return value of the Name property
      */
    public StringValue getName();

    /**
     * Obtain a localized, user-visible name of this MeshType in the current locale.
     *
     * @return value of the UserVisibleName property
     */
    public StringValue getUserVisibleName();

    /**
     * Obtain the localized map of user-visible names.
     *
     * @return the localized map of user-visible names
     */
    public L10PropertyValueMap getUserVisibleNameMap();

    /**
     * Obtain a localized, user-visible description text for this MeshType in the current locale.
     *
     * @return the value of the UserVisibleDescription property
     */
    public BlobValue getUserVisibleDescription();

    /**
     * Obtain the localized map of user-visible descriptions.
     *
     * @return the localized map of user-visible descriptions
     */
    public L10PropertyValueMap getUserVisibleDescriptionMap();

    /**
     * Obtain the value of the Icon property.
     *
     * @return the value of the Icon property
     */
    public BlobValue getIcon();
};
