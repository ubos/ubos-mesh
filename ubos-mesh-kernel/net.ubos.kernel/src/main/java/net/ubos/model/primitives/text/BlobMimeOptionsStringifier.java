//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.SimpleStringifierParameters;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies the allowed MIME types of a BlobDataType.
 */
public class BlobMimeOptionsStringifier
        extends
            AbstractStringifier<BlobDataType>
{
    /**
     * Factory method.
     *
     * @param begin String to print prior to a value if the value is not selected.
     * @param beginSelected String to print prior to a value if the value is selected.
     * @param middle String to print between values
     * @param end String to print after a value if the value is not selected.
     * @param endSelected String to print after a value if the value is selected.
     * @return the created EnumeratedValueStringifier
     */
    public static BlobMimeOptionsStringifier create(
            String begin,
            String beginSelected,
            String middle,
            String end,
            String endSelected )
    {
        return new BlobMimeOptionsStringifier( begin, beginSelected, middle, end, endSelected );
    }

    /**
     * Private constructor for subclasses only, use factory method.
     *
     * @param begin String to print prior to a value if the value is not selected.
     * @param beginSelected String to print prior to a value if the value is selected.
     * @param middle String to print between values
     * @param end String to print after a value if the value is not selected.
     * @param endSelected String to print after a value if the value is selected.
     */
    protected BlobMimeOptionsStringifier(
            String begin,
            String beginSelected,
            String middle,
            String end,
            String endSelected )
    {
        theBeginString         = begin;
        theBeginStringSelected = beginSelected;
        theMiddleString        = middle;
        theEndString           = end;
        theEndStringSelected   = endSelected;
    }

    /**
     * Obtain the String to print prior to a value if the value is not selected.
     *
     * @return the String, if any
     */
    public String getBeginString()
    {
        return theBeginString;
    }

    /**
     * Obtain the String to print prior to a value if the value is selected.
     *
     * @return the String, if any
     */
    public String getBeginStringSelected()
    {
        return theBeginStringSelected;
    }

    /**
     * Obtain the String to print between values.
     *
     * @return the String, if any
     */
    public String getMiddleString()
    {
        return theMiddleString;
    }

    /**
     * Obtain the String to print after a value if the value is not selected.
     *
     * @return the String, if any
     */
    public String getEndString()
    {
        return theEndString;
    }

    /**
     * Obtain the String to print after a value if the value is selected.
     *
     * @return the String, if any
     */
    public String getEndStringSelected()
    {
        return theEndStringSelected;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            BlobDataType          arg,
            StringifierParameters pars,
            String                soFar )
    {
        String selectedMime;
        BlobValue selected = (BlobValue) pars.get( CURRENT_VALUE );
        if( selected != null ) {
            selectedMime = selected.getMimeType();
        } else {
            selectedMime = null;
        }

        SelectableMimeType [] mimeTypes = arg.getUserSelectableMimeTypes();
        StringBuilder         ret           = new StringBuilder();

        String sep = null;
        for( int i=0 ; i<mimeTypes.length ; ++i ) {
            SelectableMimeType current = mimeTypes[i];
            if( sep != null ) {
                ret.append( sep );
            }
            if( current.getName().equals( selectedMime )) {
                if( theBeginStringSelected != null ) {
                    ret.append( theBeginStringSelected );
                }
                ret.append( current.getUserVisibleName() );
                if( theEndStringSelected != null ) {
                    ret.append( theEndStringSelected );
                }
            } else {
                if( theBeginString != null ) {
                    ret.append( theBeginString );
                }
                ret.append( current.getUserVisibleName() );
                if( theEndString != null ) {
                    ret.append( theEndString );
                }
            }
            sep = theMiddleString;
        }

        return potentiallyShorten( ret.toString(), pars );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg instanceof BlobDataType ) {
            BlobDataType realArg = (BlobDataType) arg;

            return format( realArg, pars, soFar );

        } else if( arg instanceof PropertyType ) {
            PropertyType realArg  = (PropertyType) arg;
            BlobDataType realType = (BlobDataType) realArg.getDataType();

            return format( realType, pars, soFar );

        } else if( arg instanceof BlobValue ) {
            BlobValue    realArg  = (BlobValue) arg;
            BlobDataType realType = realArg.getDataType();

            SimpleStringifierParameters realPars = SimpleStringifierParameters.create( pars );
            realPars.put( CURRENT_VALUE, realArg );

            return format( realType, realPars, soFar );

        } else {
            throw new ClassCastException( "Cannot stringify " + arg );
        }
    }

    /**
     * The String to print prior to a value if the value is not selected.
     */
    protected String theBeginString;

    /**
     * The String to print prior to a value if the value is selected.
     */
    protected String theBeginStringSelected;

    /**
     * The String to print between values.
     */
    protected String theMiddleString;

    /**
     * The String to print after a value if the value is not selected.
     */
    protected String theEndString;

    /**
     * The String to print after a value if the value is selected.
     */
    protected String theEndStringSelected;

    /**
     * Key for the current BlobValue, if any.
     */
    public static final String CURRENT_VALUE = "current-value";
}
