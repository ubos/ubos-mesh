//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
 * A DataType for boolean values.
 */
public final class BooleanDataType
        extends
            DataType
{
    private static final Log  log              = Log.getLogInstance( BooleanDataType.class ); // our own, private logger
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
      * This is the default instance of this class.
      */
    public static final BooleanDataType theDefault = new BooleanDataType();

    /**
     * Factory method. Always returns the same instance.
     *
     * @return the default instance of this class
     */
    public static BooleanDataType create()
    {
        return theDefault;
    }

    /**
      * Private constructor, there is no reason to instantiate this more than once.
      */
    private BooleanDataType()
    {
        super( null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        return other instanceof BooleanDataType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        BooleanValue realValue = (BooleanValue) value; // may throw

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<BooleanValue> getCorrespondingJavaClass()
    {
        return BooleanValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BooleanValue getDefaultValue()
    {
        return BooleanValue.FALSE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        return className + DEFAULT_STRING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BooleanValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        if( "true".equalsIgnoreCase( raw )) {
            return BooleanValue.TRUE;
        } else if( "yes".equalsIgnoreCase( raw ) ) {
            return BooleanValue.TRUE;
        } else {
            return BooleanValue.FALSE;
        }
    }
}
