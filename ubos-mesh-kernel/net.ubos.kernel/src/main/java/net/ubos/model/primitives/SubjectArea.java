//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.modelbase.CollectableMeshTypeNotFoundException;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;

/**
 * SubjectAreas are the packaging construct for the model. They are similar
 * to the concept of a package in UML, or in Java. SubjectAreas have names
 * using the reverse DNS name convention (same as in Java), and version numbers.
 * Any SubjectArea depends on zero or more SubjectAreas.
 */
public interface SubjectArea
        extends
            MeshType
{
    /**
     * Obtain the set of SubjectAreas that this SubjectArea depends on.
     *
     * @return the set of SubjectAreas that this SubjectArea depends on
     */
    public SubjectArea [] getSubjectAreaDependencies();

    /**
      * Obtain the CollectableMeshTypes collected by this SubjectArea. This
      * is the method by which the "content" of this SubjectArea can be determined.
      *
      * @return the CollectableMeshTypes in this SubjectArea
      */
    public CollectableMeshType [] getCollectableMeshTypes();

    /**
     * Convenience method to obtain the EntityTypes collected by this SubjectArea.
     *
     * @return the EntityTypes in this SubjectArea.
     */
    public EntityType [] getEntityTypes();

    /**
     * Convenience method to obtain the EntityTypes collected by this SubjectArea,
     * ordered by user-visible name.
     *
     * @return the EntityTypes in this SubjectArea.
     */
    public EntityType [] getOrderedEntityTypes();

    /**
     * Convenience method to obtain the RelationshipType collected by this SubjectArea.
     *
     * @return the RelationshipType in this SubjectArea.
     */
    public RelationshipType [] getRelationshipTypes();

    /**
     * Convenience method to obtain the RelationshipType collected by this SubjectArea,
     * ordered by user-visible name.
     *
     * @return the RelationshipType in this SubjectArea.
     */
    public RelationshipType [] getOrderedRelationshipTypes();

    /**
     * Obtain a ClassLoader that can load the appropriate Java classes to instantiate
     * all MeshTypeWithProperties in this SubjectArea.
     *
     * @return the ClassLoader for this SubjectArea
     */
    public ClassLoader getClassLoader();

    /**
     * Find a CollectableMeshType in this SubjectArea by name.
     *
     * @param name the name of the CollectableMeshType
     * @return the found CollectableMeshType
     * @throws CollectableMeshTypeNotFoundException thrown if the CollectableMeshType
     * could not be found
     */
    public CollectableMeshType findCollectableMeshTypeByName(
            String name )
        throws
            CollectableMeshTypeNotFoundException;

    /**
     * Find a CollectableMeshType in this SubjectArea by name.
     *
     * @param name the name of the CollectableMeshType
     * @return the found CollectableMeshType, or null if not found
     */
    public CollectableMeshType findCollectableMeshTypeByNameOrNull(
            String name );

    /**
     * Find a CollectableMeshType in this SubjectArea by the local part of its identifier,
     * i.e. the part after the slash.
     *
     * @param localId the local part of the identifier
     * @return the found CollectableMeshType
     * @throws CollectableMeshTypeNotFoundException thrown if the CollectableMeshType
     * could not be found
     */
    public CollectableMeshType findCollectableMeshTypeByLocalIdentifier(
            String localId )
        throws
            CollectableMeshTypeNotFoundException;

    /**
     * Find a CollectableMeshType in this SubjectArea by the local part of its identifier,
     * i.e. the part after the slash.
     *
     * @param localId the local part of the identifier
     * @return the found CollectableMeshType
     */
    public CollectableMeshType findCollectableMeshTypeByLocalIdentifierOrNull(
            String localId );
}
