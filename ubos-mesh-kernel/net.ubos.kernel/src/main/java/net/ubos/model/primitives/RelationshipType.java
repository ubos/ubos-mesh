//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
  * <p>A MeshType that has a source and a destination, that can participate in
  * inheritance relationships, and may be attributed. This is often called Association
  * (or MetaAssociation), Relationship or Relation in the literature.</p>
  * 
  * <p>A RelationshipType may be abstract, which means it cannot be instantiated.</p>
  */
public interface RelationshipType
        extends
            CollectableMeshType
{
    /**
      * Obtain the source RoleType of this RelationshipType.
      *
      * @return the source RoleType of this RelationshipType
      */
    public RoleType getSource();

    /**
      * Obtain the destination RoleType of this RelationshipType.
      *
      * @return the destination RoleType of this RelationshipType
      */
    public RoleType getDestination();

    /**
      * Obtain the value of the IsAbstract property.
      *
      * @return the value of the IsAbstract property
      */
    public BooleanValue getIsAbstract();
}
