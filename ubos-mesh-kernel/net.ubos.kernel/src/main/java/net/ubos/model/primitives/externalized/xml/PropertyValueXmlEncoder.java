//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.xml;

import net.ubos.model.primitives.externalized.PropertyValueEncoder;
import net.ubos.model.primitives.externalized.json.PropertyValueJsonEncoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class PropertyValueXmlEncoder
    extends
        AbstractPropertyValueXmlEncoder
    implements
        PropertyValueEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static PropertyValueXmlEncoder create()
    {
        return new PropertyValueXmlEncoder();
    }

    /**
     * Constructor, use factory method.
     */
    protected PropertyValueXmlEncoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return PROPERTY_VALUE_XML_ENCODING_ID;
    }
}
