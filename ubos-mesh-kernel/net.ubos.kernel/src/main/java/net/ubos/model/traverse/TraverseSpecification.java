//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;

/**
 * <p>This interface is supported by all classes that can be
 * used to specify how to traverse from one MeshObject to
 * another (or a set of others).</p>
 *
 * <p>For example, the method MeshObject.traverse takes a TraverseSpecification
 * as an argument. The most basic TraverseSpecification is a RoleType, but
 * more complex ones can be created.</p>
 *
 * <p>In order to support polymorphism on TraverseSpecification, the
 *    MeshObject.traverse method then typically invokes TraverseSpecification.traverse.
 */
public interface TraverseSpecification
        extends
            Serializable
{
    /**
     * Use this TraverseSpecification to traverse from the passed-in start MeshObject
     * to related MeshObjects. This method is defined on TraverseSpecification, so
     * different implementations of TraverseSpecification can implement different ways
     * of doing this.
     *
     * @param start the start MeshObject for the traversal
     * @return the result of the traversal
     */
    public abstract MeshObjectSet traverse(
            MeshObject start );

    /**
      * Use this TraverseSpecification to traverse from the passed-in start MeshObjectSet
      * to related MeshObjects. This method is defined on TraverseSpecification, so
      * different implementations of TraverseSpecification can implement different ways
      * of doing this.
      *
      * @param theSet the start MeshObjectSet for the traversal
      * @return the result of the traversal
      */
    public abstract MeshObjectSet traverse(
            MeshObjectSet theSet );

    /**
     * Determine whether a given event, with a source of from where we traverse the
     * TraverseSpecification, may affect the result of the traversal.
     *
     * @param meshBase the MeshBase in which we consider the event
     * @param theEvent the event that we consider
     * @return true if this event may affect the result of traversing from the MeshObject
     *         that sent this event
     */
    public abstract boolean isAffectedBy(
            MeshBase                  meshBase,
            AbstractMeshObjectRoleTypeChange theEvent );

    /**
     * All implementations of this interface need an equals method.
     *
     * @param other the Object to compare against
     * @return true if the two objects are equal
     */
    @Override
    public abstract boolean equals(
            Object other );
}
