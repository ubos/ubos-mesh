//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import java.text.ParseException;
import net.ubos.modelbase.MeshTypeNotFoundException;
import org.diet4j.core.ModuleException;
import org.xml.sax.SAXException;

/**
 * Thrown if decoding of the serialized representation of something failed.
 * Inner classes provided more information.
 */
public abstract class DecodingException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param message the message
     * @param cause the underlying cause
     */
    public DecodingException(
            String    message,
            Throwable cause )
    {
        super( cause );
    }

    /**
     * The software running was missing some code that is necessary to decode.
     */
    public static class Installation
        extends
            DecodingException
    {
        /**
         * Constructor.
         *
         * @param ex the Exception indicating a requires Class could not be found.
         */
        public Installation(
                ClassNotFoundException ex )
        {
            super( null, ex );
        }

        /**
         * Constructor.
         *
         * @param ex the Exception indicating that a MeshType could not be found.
         */
        public Installation(
               MeshTypeNotFoundException ex )
        {
            super( null, ex );
        }

        /**
         * Constructor.
         *
         * @param ex the Exception indicating that a Module could not be found or resolved
         */
        public Installation(
                ModuleException ex )
        {
            super( null, ex );
        }

        /**
         * Constructor.
         *
         * @param msg a message
         */
        public Installation(
                String msg )
        {
            super( msg, null );
        }
    }

    /**
     * Syntax of the serialized representation was not as expected.
     */
    public static class Syntax
        extends
            DecodingException
    {
        /**
         * Constructor.
         *
         * @param ex the Exception indicating the parsing problem
         */
        public Syntax(
                ParseException ex )
        {
            super( null, ex );
        }

        /**
         * Constructor.
         *
         * @param ex the Exception indicating the parsing problem
         */
        public Syntax(
                SAXException ex )
        {
            super( null, ex );
        }

        /**
         * Constructor.
         *
         * @param message message explaining the problem
         */
        public Syntax(
                String message )
        {
            super( message, null );
        }
    }
}
