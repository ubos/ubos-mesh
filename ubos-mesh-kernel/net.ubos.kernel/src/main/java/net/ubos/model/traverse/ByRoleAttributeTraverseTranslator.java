//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.model.util.SubjectAreaTagMap;

/**
 * Knows how to look for RoleAttributes, and if something isn't, delegate to another TraverseTranslator.
 */
public class ByRoleAttributeTraverseTranslator
     implements
        TraverseTranslator
{
    /**
     * Factory method.
     *
     * @return the created instance
     */
    public static ByRoleAttributeTraverseTranslator create()
    {
        return new ByRoleAttributeTraverseTranslator( null );
    }

    /**
     * Factory method.
     *
     * @param delegate: delegate to this TranversalTranslator if needed
     * @return the created instance
     */
    public static ByRoleAttributeTraverseTranslator create(
            TraverseTranslator delegate )
    {
        return new ByRoleAttributeTraverseTranslator( delegate );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param delegate: delegate to this TranversalTranslator if needed
     */
    protected ByRoleAttributeTraverseTranslator(
            TraverseTranslator delegate )
    {
        theDelegate = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TraverseSpecification translateTraverseSpecification(
            MeshObject        startObject,
            SubjectAreaTagMap tagMap,
            String            traversalTerm )
        throws
            TraverseTranslatorException
    {
        TraverseSpecification ret;
        if( traversalTerm.startsWith( ROLE_ATTRIBUTE_NAME_SOURCE )) {
            ret = ByRoleAttributeTraverseSpecification.createSource( traversalTerm.substring( ROLE_ATTRIBUTE_NAME_SOURCE.length() ));

        } else if( traversalTerm.startsWith( ROLE_ATTRIBUTE_NAME_DESTINATION )) {
            ret = ByRoleAttributeTraverseSpecification.createDestination( traversalTerm.substring( ROLE_ATTRIBUTE_NAME_DESTINATION.length() ));

        } else if( theDelegate == null ) {
            throw new TraverseTranslatorException( traversalTerm );

        } else {
            ret = theDelegate.translateTraverseSpecification( startObject, tagMap, traversalTerm );
        }

        return ret;
    }

    /**
     * The delegate.
     */
    protected final TraverseTranslator theDelegate;

    /**
     * Indicates the name of a required RoleAttribute at the source.
     */
    public static final String ROLE_ATTRIBUTE_NAME_SOURCE = "roleAttribute-S:";

    /**
     * Indicates the name of a required RoleAttribute at the destination.
     */
    public static final String ROLE_ATTRIBUTE_NAME_DESTINATION = "roleAttribute-D:";
}
