//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import java.util.ArrayList;
import java.util.Arrays;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.modelbase.m.MMeshTypeIdentifier;

/**
  * <p>A MeshType that does not depend on the existence of others, that can participate in
  * inheritance relationships, and may be attributed. This is often called Class (or MetaClass)
  * or Entity in the literature.
  *
  * <p>In-memory implementation.
  */
public class MEntityType
        extends
            MMeshTypeWithProperties
        implements
            EntityType,
            CanBeDumped
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     */
    public MEntityType(
            MMeshTypeIdentifier identifier )
    {
        super( identifier );
    }

    /**
     * Set the identifiers by which this EntityType is also known.
     *
     * @param newValue the synonym identifiers
     */
    public final void setSynonyms(
            MeshTypeIdentifier [] newValue )
    {
        theSynonyms = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshTypeIdentifier [] getSynonyms()
    {
        return theSynonyms;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final RoleType [] getLocalRoleTypes()
    {
        return theRoles;
    }

    /**
     * Enable a RoleType to add itself to this EntityType.
     *
     * @param theNewRoleType the RoleType to be added to this EntityType
     */
    final void addLocalRoleType(
            MRoleType theNewRoleType )
    {
        theRoles = ArrayHelper.append( theRoles, theNewRoleType, MRoleType.class );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final RoleType [] getRoleTypes()
    {
        return getAllRoleTypesInternal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoleType [] getOrderedRoleTypes()
    {
        RoleType [] ret = getRoleTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final RoleType [] getConcreteRoleTypes()
    {
        return getAllConcreteRoleTypesInternal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final RoleType [] getOrderedConcreteRoleTypes()
    {
        RoleType [] ret = getConcreteRoleTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MEntityType [] getDirectSupertypes()
    {
        return (MEntityType []) super.getDirectSupertypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MEntityType [] getSupertypes()
    {
        return (MEntityType []) super.getSupertypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MEntityType [] getDirectSubtypes()
    {
        return (MEntityType []) super.getDirectSubtypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MEntityType [] getSubtypes()
    {
        return (MEntityType []) super.getSubtypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final EntityType [] getConcreteSubtypes()
    {
        EntityType [] allSubtypes = (EntityType []) getSubtypes();
        int count = 0;
        for( int i=0 ; i<allSubtypes.length ; ++i ) {
            if( ! allSubtypes[i].getIsAbstract().value() ) {
                ++count;
            }
        }
        EntityType [] ret = new EntityType[ count ];
        for( int i=allSubtypes.length-1 ; i>=0 ; --i )
        {
            if( ! allSubtypes[i].getIsAbstract().value() ) {
                ret[--count] = allSubtypes[i];
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void setDirectSupertypes(
            MMeshTypeWithProperties [] supertypes )
    {
        if( this.theSupertypes.length != 0 ) {
            throw new IllegalArgumentException();
        }
        if( supertypes.getClass().getComponentType() != MEntityType.class ) {
            throw new IllegalArgumentException();
        }

        this.theSupertypes = supertypes;

        zeroCaches();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public final void addDirectSubtype(
            MeshTypeWithProperties subtype )
    {
        if( subtype.getClass() != MEntityType.class ) {
            throw new IllegalArgumentException();
        }
        for( int i=0 ; i<theSubtypes.length ; ++i ) {
            if( theSubtypes[i].equals( subtype )) {
                return;
            }
        }

        theSubtypes = ArrayHelper.append(
                (MEntityType []) theSubtypes,
                (MEntityType) subtype,
                MEntityType.class );

        zeroCaches();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final BooleanValue getMayBeUsedAsForwardReference()
    {
        return theMaybeUsedAsForwardReference;
    }

    /**
     * Set the value of the MaybeUsedAsForwardReference property.
     *
     * @param newValue the new value of the property
     * @see #getMaybeUsedAsForwardReference
     */
    public final void setMaybeUsedAsForwardReference(
            BooleanValue newValue )
    {
        this.theMaybeUsedAsForwardReference = newValue;
    }

    /**
     * Set the IsAbstract property.
     *
     * @param newValue the new value for the property
     * @see #getIsAbstract
     */
    public final void setIsAbstract(
            BooleanValue newValue )
    {
        this.theIsAbstract = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final BooleanValue getIsAbstract()
    {
        return theIsAbstract;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isInstance(
            MeshObject candidate )
    {
        MeshTypeWithProperties [] other = candidate.getEntityTypes();
        for( int i=0 ; i<other.length ; ++i ) {
            if( other[i].equalsOrIsSupertype( this )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theIdentifier",
                },
                new Object[] {
                    getIdentifier()
                });
    }

    /**
      * Internal helper to determine all RoleTypes related to this EntityType, inherited or local.
      *
      * @return all RoleTypes related to this EntityType, inherited or local
      */
    protected final RoleType [] getAllRoleTypesInternal()
    {
        ArrayList<MRoleType> ret = new ArrayList<>();
        if( theRoles != null ) {
            for( int i=0 ; i<theRoles.length ; ++i ) {
                ret.add( theRoles[i] );
            }
        }

        MeshTypeWithProperties [] supertypes = super.getDirectSupertypes();
        for( int i=0 ; i<supertypes.length ; ++i ) {
            if( supertypes[i] instanceof EntityType ) { // don't want MeshObjectType
                RoleType [] temp = ((MEntityType)supertypes[i]).getAllRoleTypesInternal();
                for( int j=0 ; j<temp.length ; ++j ) {
                    ret.add( (MRoleType) temp[j] );
                }
            }
        }
        return ArrayHelper.copyIntoNewArray( ret, MRoleType.class );
    }

    /**
      * Internal helper to determine all RoleTypes related to this EntityType, inherited or local,
      * that are associated with concrete RelationshipTypes.
      *
      * @return all RoleTypes related to this EntityType, inherited or local, that are associated
      * with concrete RelationshipTypes
      */
    protected final RoleType [] getAllConcreteRoleTypesInternal()
    {
        ArrayList<MRoleType> ret = new ArrayList<>();
        if( theRoles != null ) {
            for( int i=0 ; i<theRoles.length ; ++i ) {
                ret.add( theRoles[i] );
            }
        }

        MeshTypeWithProperties [] supertypes = super.getDirectSupertypes();
        for( int i=0 ; i<supertypes.length ; ++i ) {
            if( supertypes[i] instanceof EntityType ) { // don't want MeshObject
                RoleType [] temp = ((MEntityType)supertypes[i]).getAllRoleTypesInternal();
                for( int j=0 ; j<temp.length ; ++j ) {
                    RelationshipType currentMetaRel = temp[j].getRelationshipType();
                    if( ! currentMetaRel.getIsAbstract().value() ) {
                        ret.add( (MRoleType) temp[j] );
                    }
                }
            }
        }

        return ArrayHelper.copyIntoNewArray( ret, MRoleType.class );
    }

    /**
     * The RoleTypes that this EntityType plays.
     */
    private MRoleType [] theRoles = new MRoleType[0];

    /**
     * The value of the MaybeUsedAsForwardReference property.
     */
    private BooleanValue theMaybeUsedAsForwardReference;

    /**
     * The identifiers by which this EntityType is also known.
     */
    private MeshTypeIdentifier [] theSynonyms;

    /**
      * The value of the IsAbstract property.
      */
    private BooleanValue theIsAbstract;
}
