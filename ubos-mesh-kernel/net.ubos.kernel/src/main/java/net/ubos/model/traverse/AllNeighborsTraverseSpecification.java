//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;

/**
 * A TraverseSpecification that traverses to all neighbor MeshObjects.
 */
public class AllNeighborsTraverseSpecification
        extends
            AbstractTraverseSpecification
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method always returns the same singleton instance.
     *
     * @return the singleton instance
     */
    public static AllNeighborsTraverseSpecification create()
    {
        return SINGLETON;
    }

    /**
     * Private constructor, use factory method.
     */
    private AllNeighborsTraverseSpecification()
    {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObject start )
    {
        MeshObjectSet ret = start.traverseToNeighbors( );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObjectSet theSet )
    {
        MeshObjectSet ret = MeshObjectSet.DEFAULT_EMPTY_SET;

        for( MeshObject current : theSet ) {
            MeshObjectSet toAdd = current.traverseToNeighbors();
            if( ret == MeshObjectSet.DEFAULT_EMPTY_SET ) {
                ret = toAdd;
            } else {
                ret = ret.union( toAdd );
            }
        }
        return ret;
    }

    /**
     * Determine whether a given event, with a source of from where we traverse the
     * TraverseSpecification, may affect the result of the traversal.
     *
     * @param meshBase the MeshBase in which we consider the event
     * @param theEvent the event that we consider
     * @return true if this event may affect the result of traversing from the MeshObject
     *         that sent this event
     */
    @Override
    public boolean isAffectedBy(
            MeshBase                  meshBase,
            AbstractMeshObjectRoleTypeChange theEvent )
    {
        return false;
    }

    /**
     * All implementations of this interface need an equals method.
     *
     * @param other the Object to compare against
     * @return true if the two objects are equal
     */
    @Override
    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    public boolean equals(
            Object other )
    {
        return this == other; // singleton instance
    }

    /**
     * Determine hash code. Make editor happy that otherwise indicates a warning.
     *
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    /**
     * The singleton instance of this class.
     */
    private static final AllNeighborsTraverseSpecification SINGLETON = new AllNeighborsTraverseSpecification();
}
