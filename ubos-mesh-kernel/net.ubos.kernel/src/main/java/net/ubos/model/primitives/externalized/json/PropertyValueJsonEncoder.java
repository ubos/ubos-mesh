//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.json;

import net.ubos.model.primitives.externalized.PropertyValueEncoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class PropertyValueJsonEncoder
    extends
        AbstractPropertyValueJsonEncoder
    implements
        PropertyValueEncoder
{
    /**
     * Factory method.
     *
     * @return the created encoder
     */
    public static PropertyValueJsonEncoder create()
    {
        return new PropertyValueJsonEncoder();
    }

    /**
     * Constructor, use factory method.
     */
    protected PropertyValueJsonEncoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return PROPERTY_VALUE_JSON_ENCODING_ID;
    }
}
