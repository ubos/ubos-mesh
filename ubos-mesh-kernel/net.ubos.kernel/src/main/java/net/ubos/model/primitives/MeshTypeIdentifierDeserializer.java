//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.text.ParseException;

/**
 * Deserializer for MeshTypeIdentifiers
 */
public interface MeshTypeIdentifierDeserializer
{
    /**
     * Construct an Identifier from its representation as a String.
     *
     * @param externalForm the external form to deserialize
     * @return the identifier
     * @throws ParseException thrown if the externalForm could not be parsed
     */
    public MeshTypeIdentifier fromExternalForm(
            String externalForm )
       throws
            ParseException;

    /**
     * Construct an Identifier from its representation as a String.
     *
     * @param contextIdentifier if the externalForm is given as a relative expression, it is relative to this Identifier
     * @param externalForm the external form to deserialize
     * @return the identifier
     * @throws ParseException thrown if the externalForm could not be parsed
     */
    public MeshTypeIdentifier fromExternalForm(
            MeshTypeIdentifier contextIdentifier,
            String             externalForm )
       throws
            ParseException;
}
