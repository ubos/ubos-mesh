//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

/**
 * Abstract base class for different TraverseSpecification implementations.
 */
public abstract class AbstractTraverseSpecification
        implements
            TraverseSpecification
{
}
