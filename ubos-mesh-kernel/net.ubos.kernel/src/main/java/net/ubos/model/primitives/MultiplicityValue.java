//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.text.ParseException;
import net.ubos.util.ResourceHelper;

/**
  * A multiplicity (aka cardinality) PropertyValue used for quantity
  * participation constraints on MetaRelationships.
  *
  * It also pre-defines a number of frequently-used MultiplicityValues that
  * can be used in order to save memory.
  */
public final class MultiplicityValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
      * Represents the "N" or "*" (open ended).
      * This is a very unusual value so we won't randomly do the wrong thing.
      */
    public static final int N = -1423;

    /**
     * This is a pre-defined 0-N multiplicity.
     */
    public static final MultiplicityValue ZERO_N = MultiplicityValue.create( 0, N );

    /**
     * This is a pre-defined 1-N multiplicity.
     */
    public static final MultiplicityValue ONE_N = MultiplicityValue.create( 1, N );

    /**
     * This is a pre-defined 0-1 multiplicity.
     */
    public static final MultiplicityValue ZERO_ONE = MultiplicityValue.create( 0, 1 );

    /**
     * This is a pre-defined 1-1 multiplicity.
     */
    public static final MultiplicityValue ONE_ONE = MultiplicityValue.create( 1, 1 );

    /**
     * Factory method.
     *
     * @param min the minimum number of participations (inclusive)
     * @param max the maximum number of participations (inclusive), may be N
     * @return the created MultiplicityValue
     * @throws IllegalArgumentException if incorrect/inconsistent values are being specified
     */
    public static MultiplicityValue create(
            int min,
            int max )
    {
        if(    ( min < 0 )
            || ( max <= 0 && max != N )
            || ( min > max && max != N ))
        {
            throw new InvalidMultiplicityException( min, max );
        }

        return new MultiplicityValue( min, max );
    }

    /**
     * Private constructor.
     *
     * @param min the minimum number of participations (inclusive)
     * @param max the maximum number of participations (inclusive), may be N
     */
    private MultiplicityValue(
            int min,
            int max )
    {
        this.minimum = min;
        this.maximum = max;
    }

    /**
     * Obtain the minimum number of participations.
     *
     * @return the minimum number of participations
     */
    public int getMinimum()
    {
        return minimum;
    }

    /**
     * Obtain the minimum number of participations as printable string.
     *
     * @return the minimum number of participations
     */
    public String getMinimumAsString()
    {
        return ( minimum == N ) ? N_SYMBOL : String.valueOf( minimum );
    }

    /**
     * Obtain the maximum number of participations.
     *
     * @return the maximum number of participations
     */
    public int getMaximum()
    {
        return maximum;
    }

    /**
     * Obtain the maximum number of participations as printable string.
     *
     * @return the maximum number of participations
     */
    public String getMaximumAsString()
    {
        return ( maximum == N ) ? N_SYMBOL : String.valueOf( maximum );
    }

    /**
     * Determine whether this MultiplicityValue is the same, or a sub range of another
     * MultiplicityValue.
     *
     * @param other the other MultiplicityValue
     * @return true if this MultiplicityValue is the same, or a sub range of the other
     */
    public boolean isSubrangeOf(
            MultiplicityValue other )
    {
        if( minimum < other.minimum ) {
            return false;
        }
        if( other.maximum == N ) {
            return true; // doesn't matter what our is
        }
        if( maximum == N ) {
            return false;
        }
        return maximum <= other.maximum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultiplicityDataType getDataType()
    {
        return MultiplicityDataType.theDefault;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof MultiplicityValue ) {
            return (minimum == ((MultiplicityValue)otherValue).minimum)
                && (maximum == ((MultiplicityValue)otherValue).maximum);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return minimum * maximum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue()
    {
        return value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( getMinimumAsString() );
        buf.append( SEPARATOR );
        buf.append( getMaximumAsString() );
        return buf.toString();
    }

    /**
     * Parse a text string into a MultiplicityValue.
     *
     * @param raw the text String
     * @return the MultiplicityValue
     * @throws ParseException thrown if the string could not be parsed
     */
    public static MultiplicityValue parseMultiplicityValue(
            String raw )
        throws
            ParseException
    {
        int sep = raw.indexOf( SEPARATOR );
        if( sep < 0 ) {
            throw new ParseException( "No separator in string: " + raw, 0 );
        }
        if( sep == 0 && sep == raw.length() - SEPARATOR.length() ) {
            throw new ParseException( "MultiplicityValue must have two components, is: " + raw, 0 );
        }
        String first  = raw.substring( 0, sep );
        String second = raw.substring( sep + SEPARATOR.length() );

        return MultiplicityValue.create(
                N_SYMBOL.equals( first )  ? N : Integer.parseInt( first ),
                N_SYMBOL.equals( second ) ? N : Integer.parseInt( second ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        String className = getClass().getName();
        StringBuilder buf = new StringBuilder( 32 );
        buf.append( className );
        buf.append( DataType.CREATE_STRING );

        if( minimum == N ) {
            buf.append( className );
            buf.append( ".N" );
        } else {
            buf.append( String.valueOf( minimum ));
        }
        buf.append( ", " );
        if( maximum == N ) {
            buf.append( className );
            buf.append( ".N" );
        } else {
            buf.append( String.valueOf( maximum ));
        }
        buf.append( DataType.CLOSE_PARENTHESIS_STRING );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        MultiplicityValue realOther = (MultiplicityValue) o;

        if( minimum == realOther.minimum && maximum == realOther.maximum ) {
            return 0;
        } else {
            return +2; // not comparable convention: +2
        }
    }

    /**
      * The value for the minimum.
      */
    protected int minimum;

    /**
      * The value for the maximum.
      */
    protected int maximum;

    /**
     * Symbol to use when displaying N.
     */
    public static final String N_SYMBOL = ResourceHelper.getInstance( MultiplicityValue.class ).getResourceStringOrDefault(
            "NSymbol",
            "*" );

    /**
     * The separator between the minimum and the maximum.
     */
    public static final String SEPARATOR = "..";
}
