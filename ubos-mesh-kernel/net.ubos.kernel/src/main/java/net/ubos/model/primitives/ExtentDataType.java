//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.util.logging.Dumper;

/**
  * A DataType for PropertyTypes that represents the extent of a graphical object.
  */
public final class ExtentDataType
        extends
            DataType
{
    /**
     * This is the default instance of this class.
     */
    public static final ExtentDataType theDefault = new ExtentDataType();

    /**
     * Private constructor, there is no reason to instantiate this more than once.
     */
    private ExtentDataType()
    {
        super( null );
    }

    /**
     * Factory method. Always returns the same instance.
     *
     * @return the default instance of this class
     */
    public static ExtentDataType create()
    {
        return theDefault;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof ExtentDataType ) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        ExtentValue realValue = (ExtentValue) value; // may throw

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<ExtentValue> getCorrespondingJavaClass()
    {
        return ExtentValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtentValue getDefaultValue()
    {
        return theDefaultValue;
    }

    /**
     * The default value.
     */
    private static final ExtentValue theDefaultValue = ExtentValue.create( 0., 0. );

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        return className + DEFAULT_STRING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtentValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        Matcher m = DEFAULT_PARSE_PATTERN.matcher( raw.trim() );
        if( m.matches() ) {
            double a = Double.parseDouble( m.group( 1 ));
            double b = Double.parseDouble( m.group( 2 ));

            ExtentValue ret = ExtentValue.create( a, b );

            return ret;
        } else {
            throw new PropertyValueParsingException( this, raw );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }
    
    /**
     * The Pattern to parse the default format.
     */
    public static final Pattern DEFAULT_PARSE_PATTERN = PointDataType.DEFAULT_PARSE_PATTERN;
}