//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * An Identifier for MeshTypes.
 */
public interface MeshTypeIdentifier
    extends
        Comparable<MeshTypeIdentifier>
{
    /**
     * Obtain the part of the Identifier that refers to the SubjectArea.
     *
     * @return the part
     */
    public abstract String getSubjectAreaPart();

    /**
     * Obtain the part of the Identifier that's unique within the SubjectArea (if any).
     *
     * @return the part
     */
    public abstract String getLocalPart();

    /**
     * Convenience constant.
     */
    public static final MeshTypeIdentifier [] EMPTY_ARRAY = {};

    /**
     * Captures the directions of RoleTypes.
     */
    public static enum RoleTypeDirection
    {
        /**
         * Appended to the Identifier of a RelationshipType to create a "source" RoleType's Identifier.
         * Note: an application programmer should not depend on this; use provided methods instead to
         * query a RoleType.
         */
        SOURCE_POSTFIX( "-S" ),

        /**
         * Appended to the Identifier of a RelationshipType to create a "destination" RoleType's Identifier.
         * Note: an application programmer should not depend on this; use provided methods instead to
         * query a RoleType.
         */
        DESTINATION_POSTFIX( "-D" ),

        /**
         * Appended to the Identifier of a RelationshipType to create a "Top" RoleType's Identifier.
         * Note: an application programmer should not depend on this; use provided methods instead to
         * query a RoleType.
         */
        TOP_SINGLETON_POSTFIX( "-T" );

        RoleTypeDirection(
                String postfix )
        {
            thePostfix = postfix;
        }

        /**
         * Obtain the postfix in the Identifier string.
         *
         * @return the postfix
         */
        public String getPostfix()
        {
            return thePostfix;
        }

        /**
         * The postfix in the identifier String.
         */
        protected String thePostfix;
    }
}
