//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.util;

import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.exception.AbstractLocalizedRuntimeException;

/**
 * Thrown if a tag existed already.
 */
public class TagConflictException
    extends
        AbstractLocalizedRuntimeException
{
    /**
     * Constructor.
     *
     * @param prefix the prefix
     * @param oldValue the old value
     * @param newValue the new value
     */
    public TagConflictException(
            String      prefix,
            SubjectArea oldValue,
            SubjectArea newValue )
    {
        thePrefix = prefix;
        theOldValue = oldValue;
        theNewValue = newValue;
    }

    /**
     * Obtain resource parameters for the internationalization.
     *
     * @return the resource parameters
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            thePrefix,
            theOldValue,
            theNewValue };
    }

    /**
     * The prefix with a conflict.
     */
    protected String thePrefix;

    /**
     * The old definition of the prefix.
     */
    protected SubjectArea theOldValue;

    /**
     * The new definition of the prefix.
     */
    protected SubjectArea theNewValue;
}
