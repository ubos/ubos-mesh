//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
  * CollectableMeshType is the abstract supertype of all MeshTypes that are
  * organized in SubjectAreas.
  */
public interface CollectableMeshType
        extends
            MeshType
{
    /**
      * Obtain the SubjectArea in which this CollectableMeshType is defined.
      *
      * @return the SubjectArea in which this CollectableMeshType is defined
      */
    public SubjectArea getSubjectArea();
}
