//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies TimePeriodValues.
 */
public class TimePeriodValueStringifier
        extends
            AbstractStringifier<TimePeriodValue>
{
    /**
     * Factory method.
     *
     * @return the created TimePeriodValueStringifier
     */
    public static TimePeriodValueStringifier create()
    {
        return new TimePeriodValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected TimePeriodValueStringifier()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            TimePeriodValue       arg,
            StringifierParameters pars,
            String                soFar )
    {
        return arg.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (TimePeriodValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimePeriodValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }
}
