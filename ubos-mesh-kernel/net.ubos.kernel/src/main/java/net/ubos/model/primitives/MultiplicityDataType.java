//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.logging.Dumper;

/**
  * A DataType for multiplicities.
  */
public class MultiplicityDataType
        extends
            DataType
{
    /**
     * This is the default instance of this class.
     */
    public static final MultiplicityDataType theDefault = new MultiplicityDataType();

    /**
     * Factory method. Always returns the same instance.
     *
     * @return the default instance of this class
     */
    public static MultiplicityDataType create()
    {
        return theDefault;
    }

    /**
     * Private constructor, there is no reason to instantiate this more than once.
     */
    private MultiplicityDataType()
    {
        super( null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof MultiplicityDataType ) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return MultiplicityDataType.class.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        MultiplicityValue realValue = (MultiplicityValue) value; // may throw

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<MultiplicityValue> getCorrespondingJavaClass()
    {
        return MultiplicityValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultiplicityValue getDefaultValue()
    {
        return MultiplicityValue.ZERO_N;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        return className + DEFAULT_STRING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultiplicityValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        try {
            int split = raw.indexOf( MultiplicityValue.SEPARATOR );

            String aString = raw.substring( 0, split ).trim();
            String bString = raw.substring( split + MultiplicityValue.SEPARATOR.length() ).trim();

            int a = Integer.parseInt( aString );
            int b = ( MultiplicityValue.N_SYMBOL.equals( bString ) || "N".equals( bString )) ? MultiplicityValue.N : Integer.parseInt( bString );

            MultiplicityValue ret = MultiplicityValue.create( a, b );
            return ret;

        } catch( StringIndexOutOfBoundsException ex ) {
            throw new PropertyValueParsingException( this, raw );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }
}
