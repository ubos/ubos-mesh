//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import net.ubos.util.StringHelper;
import net.ubos.util.logging.Log;

/**
 * A Binary Large Object (BLOB) PropertyValue.
 * It can use any MIME-type to specify the formatting,
 * but practically only some are supported by the rest of the code.
 *
 * This has several inner classes as subtypes. One uses a byte[] representation,
 * another String, and a third is a "delayed-loading" one.
 */
public abstract class BlobValue
        extends
            PropertyValue
{
    private static final Log  log = Log.getLogInstance( BlobValue.class ); // our own, private logger

    /**
     * Inverse operation of loadFrom. Saves binary data back to a stream.
     *
     * @param theStream the stream to write this object to
     * @throws IOException thrown if a write error occurred
     */
    public abstract void saveTo(
            OutputStream theStream )
        throws
            IOException;

    /**
     * Protected constructor for subclasses only.
     *
     * @param type the BlobDataType to which this BlobDataType belongs
     * @param mimeType the MIME type of the BlobValue, in "abc/def" format
     */
    protected BlobValue(
            BlobDataType type,
            String       mimeType )
    {
        theDataType = type;
        theMimeType = mimeType;
    }

    /**
     * Obtain the content of this BlobValue in byte[] format.
     *
     * @return the content of this BlobValue in byte[] format
     */
    @Override
    public abstract byte[] value();

    /**
     * Obtain the content of this BlobValue in byte[] format.
     *
     * @return the content of this BlobValue in byte[] format
     */
    @Override
    public byte[] getValue()
    {
        return value();
    }

    /**
      * Obtain the content of this BlobValue in String format. This method will
      * interpret the BlobValue as a string in Unicode format, regardless.
      *
      * @return the content of this BlobValue in String format
      */
    public abstract String getAsString();

    /**
     * {@inheritDoc}
     */
    @Override
    public BlobDataType getDataType()
    {
        return theDataType;
    }

    /**
     * Obtain the MIME type of this BlobValue.
     *
     * @return the MIME type of this BlobValue in "abc/def" forma
     */
    public String getMimeType()
    {
        return theMimeType;
    }

    /**
     * Determine whether this BlobValue has any kind of text MIME type.
     *
     * @return true if it has a text MIME type
     */
    public boolean getHasTextMimeType()
    {
        return theMimeType.startsWith( "text/" );
    }

    /**
     * Determine whether this BlobValue has an HTML MIME type.
     *
     * @return true if it has an HTML MIME type
     */
    public boolean getHasTextHtmlMimeType()
    {
        return theMimeType.startsWith( "text/html" ) || theMimeType.startsWith( "text/xhtml" );
    }

    /**
     * Determine whether this BlobValue has a text MIME type that is not HTML.
     *
     * @return true if it is non-HTML text
     */
    public boolean getHasTextNotHtmlMimeType()
    {
        return getHasTextMimeType() && !getHasTextHtmlMimeType();
    }

    /**
     * If this is non-null, this BlobValue is a delayed-loading Blob and the return value
     * is the relative path from which the content shall be loaded.
     *
     * @return the relative path from where the content shall be loaded
     */
    public String delayedLoadingFrom()
    {
        return null;
    }

    /**
     * Determine whether these two BlobValues have the same MIME type.
     *
     * @param other the BlobValue to test against
     * @return if true, this and other have the same MIME type
     */
    public boolean mimeTypesEquals(
            BlobValue other )
    {
        if( theMimeType == null ) {
            return other.theMimeType == null;
        }

        return theMimeType.equals( other.theMimeType );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        // type cast to ensure ClassCastException
        if( equals( (BlobValue) o )) {
            return 0;
        } else {
            return +2; // not comparable convention: +2
        }
    }

   /**
    * Encode byte data into hex format.
    *
    * @param data the data to convert into hex format
    * @return the content of data in hex format
    */
    public static String encodeHex(
            byte[] data )
    {
        StringBuilder almostRet = new StringBuilder( data.length*2 );

        for( int i=0; i<data.length; ++i ) {
            int current = ( data[i] >> 4 ) & 0xf;
            if( current <= 9 ) {
                almostRet.append( (char) ('0' + current ));
            } else {
                almostRet.append( (char) ('a' + current - 10 ));
            }
            current = data[i] & 0xf;
            if( current <= 9 ) {
                almostRet.append( (char) ('0' + current ));
            } else {
                almostRet.append( (char) ('a' + current - 10 ));
            }
        }
        return almostRet.toString();
   }

   /**
    * Decode byte data from hex format.
    *
    * @param raw the data in hex format
    * @return the original data, decoded
    */
    public static byte [] decodeHex(
            String raw )
    {
        byte [] ret = new byte[ raw.length() / 2 ];
        char [] c   = raw.toCharArray();

        for( int i=0 ; i<ret.length ; ++i ) {
            char current = c[2*i];
            if( current >= '0' && current <= '9' ) {
                ret[i] = (byte) ( (( current - '0' ) & 0xf) << 4 );
            } else if( current >= 'a' && current <= 'f' ) {
                ret[i] = (byte) ( (( current - 'a' + 10 ) & 0xf) << 4 );
            } else if( current >= 'A' && current <= 'F' ) {
                ret[i] = (byte) ( (( current - 'A' + 10 ) & 0xf) << 4 );
            } else {
                throw new IllegalArgumentException();
            }
            current = c[2*i+1];
            if( current >= '0' && current <= '9' ) {
                ret[i] |= ( current - '0' ) & 0xf;
            } else if( current >= 'a' && current <= 'f' ) {
                ret[i] |= ( current - 'a' + 10 ) & 0xf;
            } else if( current >= 'A' && current <= 'F' ) {
                ret[i] |= ( current - 'A' + 10 ) & 0xf;
            } else {
                throw new IllegalArgumentException();
            }
        }
        return ret;
    }

    /**
     * The DataType to which this value belongs.
     */
    protected BlobDataType theDataType;

    /**
      * The MIME type of this BlobValue.
      */
    protected String theMimeType;

    /**
     * This private subclass of BlobValue stores the data as a String.
     */
    protected static final class StringBlob
            extends
                BlobValue
    {
        private static final long serialVersionUID = 6861928152343433099L;

        /**
         * Private constructor.
         *
         * @param type the BlobDataType to which this BlobDataType belongs
         * @param value the content of the BlobValue
         * @param mimeType the MIME type of the BlobValue in "abc/def" format
         */
        protected StringBlob(
                BlobDataType type,
                String       value,
                String       mimeType )
        {
            super( type, mimeType );

            this.theValue = value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getUserVisibleString()
        {
            return theValue;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public byte [] value()
        {
            return theValue.getBytes( StandardCharsets.UTF_8 );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getAsString()
        {
            return theValue;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void saveTo(
                OutputStream theStream )
            throws
                IOException
        {
            theStream.write(value() );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(
                Object otherValue )
        {
            // the very cheap one first
            if( this == otherValue ) {
                return true;
            }

            if( otherValue instanceof StringBlob ) {
                StringBlob realOtherValue = (StringBlob) otherValue;

                if( !mimeTypesEquals( realOtherValue )) {
                    return false;
                }

                // they are different if one is null and the other isn't
                if( theValue == null ) {
                    return realOtherValue.theValue == null;
                }
                if( realOtherValue.theValue == null ) {
                    return false;
                }

                return theValue.equals( realOtherValue.theValue );
            }
            return false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode()
        {
            return theValue.hashCode();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getJavaConstructorString(
                String classLoaderVar,
                String typeVar )
        {
            StringBuilder sb = new StringBuilder( 60 ); // fudge
            sb.append( typeVar );
            sb.append( ".createBlobValue( \"" );

            sb.append( StringHelper.stringToJavaString( theValue ));

            sb.append( "\" " );

            if ( theMimeType != null ) {
                sb.append(", \"");
                sb.append( theMimeType );
                sb.append("\"");
            }
            sb.append( " )" );
            return sb.toString();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder( 50 ); // fudge

            sb.append( "\"" );
            sb.append( getAsString() );
            sb.append( "\"" );

            sb.append( "[" ).append( getMimeType()).append( "]" );
            return new String( sb );
        }

        /**
         * The real value.
         */
        private String theValue;
    }

    /**
     * This private subclass of BlobValue stores the data as a byte []. It has hooks to make
     * it really easy to subclass it into a delayed loading.
     */
    protected static class ByteBlob
            extends
                BlobValue
    {
        private static final long serialVersionUID = -515235083305202648L;

        /**
         * Construct one.
         *
         * @param type the BlobDataType to which this BlobDataType belongs
         * @param value the content of the BlobValue
         * @param mimeType the MIME type of the BlobValue in "abc/def" form
         */
        protected ByteBlob(
                BlobDataType type,
                byte []      value,
                String       mimeType )
        {
            super( type, mimeType );

            this.theValue = value;
        }

        /**
         * Constructor for subtypes only.
         *
         * @param type the BlobDataType to which this BlobDataType belongs
         * @param mimeType the MIME type of the BlobValue in "abc/def" form
         */
        protected ByteBlob(
                BlobDataType type,
                String       mimeType )
        {
            super( type, mimeType );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getUserVisibleString()
        {
            return String.format( "Binary data (%d bytes)", theValue.length );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public byte [] value()
        {
            return theValue;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getAsString()
        {
            return new String( theValue );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void saveTo(
                OutputStream theStream )
            throws
                IOException
        {
            theStream.write( theValue );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(
                Object otherValue )
        {
            // the very cheap one first
            if( this == otherValue ) {
                return true;
            }

            if( otherValue instanceof ByteBlob ) {
                ByteBlob realOtherValue = (ByteBlob) otherValue;

                if( !mimeTypesEquals( realOtherValue )) {
                    return false;
                }

                // they are different if one is null and the other isn't
                if( theValue == null ) {
                    return realOtherValue.theValue == null;
                }
                if( realOtherValue.theValue == null ) {
                    return false;
                }

                // they are different if their lengths are different
                if( theValue.length != realOtherValue.theValue.length ) {
                    return false;
                }
                for( int i=0 ; i<theValue.length ; ++i ) {
                    if( theValue[i] != realOtherValue.theValue[i] ) {
                        return false;
                    }
                }
                return true;

            } else if( otherValue instanceof DelayedByteBlob ) {

                DelayedByteBlob realOtherValue = (DelayedByteBlob) otherValue;

                if( !mimeTypesEquals( realOtherValue )) {
                    return false;
                }

                realOtherValue.ensureLoaded();

                // they are different if one is null and the other isn't
                if( theValue == null ) {
                    return realOtherValue.theValue == null;
                }
                if( realOtherValue.theValue == null ) {
                    return false;
                }

                // they are different if their lengths are different
                if( theValue.length != realOtherValue.theValue.length ) {
                    return false;
                }
                for( int i=0 ; i<theValue.length ; ++i ) {
                    if( theValue[i] != realOtherValue.theValue[i] ) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode()
        {
            int ret = 0;
            for( int i=0 ; i<theValue.length ; ++i ) {
                ret ^= theValue[i];
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getJavaConstructorString(
                String classLoaderVar,
                String typeVar )
        {
            StringBuilder sb = new StringBuilder( 60 ); // fudge
            sb.append( typeVar );
            sb.append( ".createBlobValue( " );
            sb.append( "new byte[] { " );

            for ( int i=0; i<theValue.length; ++i ) {
                if( i>0 ) {
                    sb.append( "," );
                }
                sb.append( (int) theValue[i] );
            }
            sb.append( " } " );

            if ( theMimeType != null ) {
                sb.append(", \"");
                sb.append( theMimeType );
                sb.append("\"");
            }
            sb.append( DataType.CLOSE_PARENTHESIS_STRING );

            return sb.toString();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder( 50 ); // fudge

            sb.append( "<" );
            sb.append( getClass().getName() );
            sb.append( "{ " );
            byte [] v = value();
            sb.append( "value: " );
            if( v != null ) {
                sb.append( "\"x\'" );
                sb.append(encodeHex(value() ) );
                sb.append( "\'\"");
            } else {
                sb.append( "null" );
            }

            sb.append( ", [" ).append( getMimeType() ).append( "]" );
            sb.append( " }>" );

            return sb.toString();
        }

        /**
         * The real value.
         */
        protected byte [] theValue;
    }

    /**
     * This private subclass of BlobValue stores the data as a byte [] and loads only on demand.
     */
    protected static final class DelayedByteBlob
            extends
                BlobValue
    {
        private static final Log log = Log.getLogInstance( DelayedByteBlob.class ); // our own, private logger

        private static final long serialVersionUID = 3750605570020464233L;

        /**
         * Private constructor.
         *
         * @param type the BlobDataType to which this BlobDataType belongs
         * @param loader the ClassLoader relative to which we load
         * @param loadFrom the location relative to loader from which we load
         * @param mimeType the MIME type of the BlobValue, in "abc/def" format
         */
        protected DelayedByteBlob(
                BlobDataType type,
                ClassLoader  loader,
                String       loadFrom,
                String       mimeType )
        {
            super( type, mimeType );

            this.theClassLoader = loader;
            this.theLoadFrom    = loadFrom;
            this.theValue       = null; // indicates we have not loaded yet
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getUserVisibleString()
        {
            ensureLoaded();
            return String.format( "Binary data (%d bytes)", theValue.length );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public byte [] value()
        {
            ensureLoaded();
            return theValue;
        }

        /**
         * Obtain the ClassLoader through which we attempt to load.
         *
         * @return the ClassLoader through which we attempt to load
         */
        public ClassLoader getClassLoader()
        {
            return theClassLoader;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getAsString()
        {
            ensureLoaded();
            if( theValue != null ) {
                return new String( theValue );
            } else {
                return null;
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void saveTo(
                OutputStream theStream )
            throws
                IOException
        {
            ensureLoaded();
            theStream.write( theValue );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String delayedLoadingFrom()
        {
            return theLoadFrom;
        }

        /**
         * This hook is overridden here to make sure we have loaded our value.
         */
        protected void ensureLoaded()
        {
            if( theValue != null ) {
                return; // done already
            }
            try {
                // do load
                ClassLoader l         = ( theClassLoader != null ) ? theClassLoader : Thread.currentThread().getContextClassLoader();
                InputStream theStream = l.getResourceAsStream( theLoadFrom );

                if( theStream == null ) {
                    log.error( "Cannot load resource " + theLoadFrom + ", class loader is " + l );
                    return;
                }

                while( true ) {

                    byte [] buf = new byte[ 512 ];
                    int length = theStream.read( buf );

                    if( length < 0 ) {
                        break;
                    }

                    if( theValue != null ) {
                        byte [] newValue = new byte[ theValue.length + length ];
                        int i;
                        for( i=0 ; i<theValue.length ; ++i ) {
                            newValue[i] = theValue[i];
                        }
                        for( int j=0 ; j<length ; ++j, ++i ) {
                            newValue[i] = buf[j];
                        }
                        theValue = newValue;
                    } else {
                        theValue = buf;
                    }

                    if( length > buf.length ) {
                        break;
                    }
                }

            } catch( IOException ex ) {
                log.error( ex );
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getJavaConstructorString(
                String classLoaderVar,
                String typeVar )
        {
            StringBuilder sb = new StringBuilder( 60 ); // fudge
            sb.append( typeVar );
            sb.append( ".createBlobValueByLoadingFrom( " );
            sb.append( classLoaderVar );
            sb.append( ", \"" );
            sb.append( theLoadFrom );
            sb.append( "\"" );

            if ( theMimeType != null ) {
                sb.append(", \"");
                sb.append( theMimeType );
                sb.append("\"");
            }
            sb.append( DataType.CLOSE_PARENTHESIS_STRING );

            return sb.toString();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder( 50 ); // fudge

            sb.append( "<" );
            sb.append( getClass().getName() );
            sb.append( "{ " );

            sb.append( "from: " );
            sb.append( theLoadFrom );

            sb.append( ", value: " );
            sb.append( theValue != null ? theValue.length : "null" );

            sb.append( "bytes, [" ).append( getMimeType()).append( "]" );
            sb.append( " }>" );

            return sb.toString();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(
                Object otherValue )
        {
            // the very cheap one first
            if( this == otherValue ) {
                return true;
            }

            if( otherValue instanceof DelayedByteBlob ) {

                DelayedByteBlob realOtherValue = (DelayedByteBlob) otherValue;

                if( !mimeTypesEquals( realOtherValue )) {
                    return false;
                }

                // they are different if one is null and the other isn't
                if( theValue == null ) {
                    return realOtherValue.theValue == null;
                }
                if( realOtherValue.theValue == null ) {
                    return false;
                }

                if( theLoadFrom.equals( realOtherValue.theLoadFrom )) {
                    return true;
                }

                realOtherValue.ensureLoaded();

                // they are different if their lengths are different
                if( theValue.length != realOtherValue.theValue.length ) {
                    return false;
                }
                for( int i=0 ; i<theValue.length ; ++i ) {
                    if( theValue[i] != realOtherValue.theValue[i] ) {
                        return false;
                    }
                }
                return true;
            }

            if( otherValue instanceof ByteBlob ) {
                ByteBlob realOtherValue = (ByteBlob) otherValue;

                if( !mimeTypesEquals( realOtherValue )) {
                    return false;
                }

                ensureLoaded();

                // they are different if one is null and the other isn't
                if( theValue == null ) {
                    return realOtherValue.theValue == null;
                }
                if( realOtherValue.theValue == null ) {
                    return false;
                }

                // they are different if their lengths are different
                if( theValue.length != realOtherValue.theValue.length ) {
                    return false;
                }
                for( int i=0 ; i<theValue.length ; ++i ) {
                    if( theValue[i] != realOtherValue.theValue[i] ) {
                        return false;
                    }
                }
                return true;

            }
            return false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode()
        {
            ensureLoaded();

            int ret = 0;
            for( int i=0 ; i<theValue.length ; ++i ) {
                ret ^= theValue[i];
            }
            return ret;
        }

        /**
         * Override serialization, in order to remove ClassLoader dependencies.
         *
         * @return a ByteBlob replacing this one for the purpose of serialization
         * @throws ObjectStreamException thrown if a write error occurs.
         */
        private Object writeReplace()
            throws
                ObjectStreamException
        {
            return new ByteBlob( getDataType(), value(), getMimeType() );
        }

        /**
         * The ClassLoader through which we attempt to load.
         */
        private final ClassLoader theClassLoader;

        /**
         * The relative path from which we load the real value.
         */
        private final String theLoadFrom;

        /**
         * The real value.
         */
        protected byte [] theValue;
    }
}
