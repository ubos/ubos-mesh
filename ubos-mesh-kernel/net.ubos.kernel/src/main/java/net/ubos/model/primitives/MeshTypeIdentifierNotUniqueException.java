//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.exception.AbstractLocalizedRuntimeException;

/**
  * <p>This Exception is thrown if an attempt is made to create a new MeshType with a
  *    MeshTypeIdentifier that is used already for a different MeshType. This
  *    typically indicates a programming error by the application programmer.</p>
 */
public class MeshTypeIdentifierNotUniqueException
        extends
            AbstractLocalizedRuntimeException
{
    /**
     * Constructor.
     *
     * @param existing the MeshType that exists already with the same MeshTypeIdentifier
     */
    public MeshTypeIdentifierNotUniqueException(
            MeshType existing )
    {
        theExisting = existing;
    }

    /**
     * Obtain the MeshType that already existed with this MeshTypeIdentifier
     * 
     * @return the already-existing MeshType
     */
    public MeshType getExistingMeshType()
    {
        return theExisting;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theExisting };
    }

    /**
     * The already-existing MeshType.
     */
    protected final MeshType theExisting;
}
