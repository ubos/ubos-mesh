//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.util;


import java.util.ArrayList;
import java.util.Iterator;
import net.ubos.model.primitives.MeshTypeWithProperties;

/**
 * An Iterator that returns the supertypes of an MeshTypeWithProperties
 * in breadth-first order. It will return duplicates in case of multiple inheritance;
 * to avoid that, chain this Iterator with an Iterator that filters out duplicates.
 */
public class BreadthFirstSupertypeIterator
        implements
            Iterator<MeshTypeWithProperties>
{
    /**
     * Constructor.
     *
     * @param startObject the MeshTypeWithProperties whose supertypes we'd like to find
     */
    public BreadthFirstSupertypeIterator(
            MeshTypeWithProperties startObject )
    {
        MeshTypeWithProperties [] theSupertypes = startObject.getDirectSupertypes();

        if( theSupertypes.length > 0 ) {

            currentLayer = new ArrayList<>( theSupertypes.length );
            for( int i=0 ; i<theSupertypes.length ; ++i ) {
                currentLayer.add( theSupertypes[i] );
            }

            currentIter = currentLayer.iterator();
            nextObject  = currentIter.next();
        }
    }

    /**
     * Returns <tt>true</tt> if the iteration has more elements.
     *
     * @return <tt>true</tt> if the iteration has more elements
     */
    @Override
    public boolean hasNext()
    {
        if( nextObject != null ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     */
    @Override
    public MeshTypeWithProperties next()
    {
        MeshTypeWithProperties ret = nextObject;
        if( currentIter.hasNext() ) {
            nextObject = currentIter.next();

        } else {
            currentIter = currentLayer.iterator();
            ArrayList<MeshTypeWithProperties> newLayer = new ArrayList<>();
            while( currentIter.hasNext() ) {

                MeshTypeWithProperties [] s = currentIter.next().getDirectSupertypes();
                for( int i=0 ; i<s.length ; ++i ) {
                    newLayer.add( s[i] );
                }
            }
            currentLayer = newLayer;
            if( currentLayer.isEmpty() ) {
                currentLayer = null;
                currentIter  = null;
                nextObject   = null;

            } else {
                currentIter = currentLayer.iterator();
                if( currentIter.hasNext() ) {
                    nextObject = currentIter.next();
                } else {
                    nextObject = null;
                }
            }
        }
        return ret;
    }

    /**
     * Removes from the underlying collection the last element returned by the
     * iterator (optional operation).
     *
     * @throws UnsupportedOperationException always thrown, not a supported operation
     */
    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * The set of current super- (or super-super- etc) MeshTypeWithProperties.
     */
    protected ArrayList<MeshTypeWithProperties> currentLayer;

    /**
     * The current delegate Iterator.
     */
    protected Iterator<MeshTypeWithProperties> currentIter;

    /**
     * The next MeshTypeWithProperties to return. We leave this as Object in order to
     * avoid doing unnecessary (performance-wise) typecasts.
     */
    protected MeshTypeWithProperties nextObject;
}
