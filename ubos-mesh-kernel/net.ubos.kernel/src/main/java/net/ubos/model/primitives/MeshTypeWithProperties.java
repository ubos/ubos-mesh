//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
  * <p>A MeshType that can have features, called PropertyTypes, and may participate in an
  *    inheritance hierarchy.
  *
  * <p>A given MeshTypeWithProperties has PropertyTypes that are defined locally (called local
  *    PropertyTypes) and those that are inherited from its supertype or supertypes (called
  *    inherited PropertyTypes). All PropertyTypes of the supertype or supertypes
  *    are always inherited to all subtypes.
  *
  * <p>Further, inherited PropertyTypes can be locally overridden. This is useful, for example,
  *    to set different default values for a PropertyType in a subtype.
  *
  * <p>PropertyTypes can be grouped in PropertyTypeGroups. PropertyTypeGroups also are
  *    held by MeshTypeWithProperties.
  *
  * <p>MeshTypeWithProperties can also have one or more supertypes. By having more than one
  *    supertype, an MeshTypeWithProperties can inherit multiple times.
  *
  * <p>MeshTypeWithProperties can be abstract or concrete (i.e. cannot be instantiated, or
  *    can be instantiated).
  */
public interface MeshTypeWithProperties
        extends
            CollectableMeshType
{
    /**
     * Obtain the locally defined PropertyTypes for this MeshTypeWithProperties.
     * This does not return locally overridden PropertyTypes.
     *
     * @return the locally defined PropertyTypes for this MeshTypeWithProperties
     */
    public PropertyType [] getLocalPropertyTypes();

    /**
     * Obtain the inherited, but locally overridden PropertyTypes.
     *
     * @return the inherited, but locally overridden PropertyTypes
     */
    public PropertyType [] getOverridingLocalPropertyTypes();

    /**
     * Obtain the inherited PropertyTypes for this MeshTypeWithProperties.
     *
     * @return the inherited PropertyTypes for this MeshTypeWithProperties
     */
    public PropertyType [] getInheritedPropertyTypes();

    /**
     * Obtain the transitive closure of all local and inherited PropertyTypes of this
     * MeshTypeWithProperties and its supertypes without duplicates.
     * This will return locally overridden PropertyTypes and skip those in
     * supertypes that have been overridden.
     *
     * @return all local and inherited PropertyTypes
     */
    public PropertyType [] getPropertyTypes();

    /**
     * Obtain the transitive closure of all local and inherited PropertyTypes of this
     * MeshTypeWithProperties and its supertypes without duplicates,
     * ordered by user-visible name.
     * This will return locally overridden PropertyTypes and skip those in
     * supertypes that have been overridden.
     *
     * @return all local and inherited PropertyTypes
     */
    public PropertyType [] getOrderedPropertyTypes();

    /**
      * Find one of this MeshTypeWithProperties's local or inherited PropertyTypes by its Name property.
      * This will return the most concrete overridden PropertyType.
      *
      * @param name value of the Name property of the PropertyType
      * @return the found PropertyType, or null if not found.
      */
    public PropertyType findPropertyTypeByName(
            String name );

    /**
     * Obtain the locally defined PropertyTypeGroups for this MeshTypeWithProperties.
     *
     * @return the locally defined PropertyTypeGroups for this MeshTypeWithProperties
     */
    public PropertyTypeGroup [] getLocalPropertyTypeGroups();

    /**
     * Obtain the transitive closure of all local and inherited PropertyTypeGroups of
     * this MeshTypeWithProperties and all of its supertypes without duplicates.
     *
     * @return all local and inherited PropertyTypeGroups
     */
    public PropertyTypeGroup [] getPropertyTypeGroups();

    /**
     * Obtain the direct supertypes for this MeshTypeWithProperties.
     *
     * @return the direct supertypes of this MeshTypeWithProperties
     */
    public MeshTypeWithProperties [] getDirectSupertypes();

    /**
     * Obtain the transitive closure of all supertypes.
     *
     * @return transitive closure of all supertypes of this MeshTypeWithProperties
     */
    public MeshTypeWithProperties [] getSupertypes();

    /**
     * Obtain the direct subtypes for this MeshTypeWithProperties in the working model.
     *
     * By definition (see definition of "working model"), not all subtypes of the
     * MeshTypeWithProperties may be known at any point in time; only the ones
     * currently known in the working model ones are returned.
     *
     * @return the direct subtypes of this MeshTypeWithProperties
     */
    public MeshTypeWithProperties [] getDirectSubtypes();

    /**
     * Obtain the transitive closure of all subtypes of this MeshTypeWithProperties
     * in the working model.
     *
     * By definition (see definition of "working model"), not all subtypes of the
     * MeshTypeWithProperties may be known at any point in time; only the ones
     * currently known in the working model are returned.
     *
     * @return the transitive closure of all known subtypes of this MeshTypeWithProperties
     */
    public MeshTypeWithProperties [] getSubtypes();

    /**
      * Determine whether the passed-in MeshTypeWithProperties equals or is
      * a supertype of this MeshTypeWithProperties.
      *
      * @param other the MeshTypeWithProperties to test against
      * @return true of the passed-in MeshTypeWithProperties equals or is a supertype of this
      *         MeshTypeWithProperties
      */
    public boolean equalsOrIsSupertype(
            MeshTypeWithProperties other );

    /**
      * Determine the distance from this MeshTypeWithProperties to the
      * passed-in (supertype) MeshTypeWithProperties in number of "inheritance steps".
      *
      * @param other the MeshTypeWithProperties to test against
      * @return the number of steps between this MeshTypeWithProperties and other. -1 if
      *         there is no sub-typing relationships between the two
      */
    public int getDistanceToSupertype(
            MeshTypeWithProperties other );

    /**
     * Determine whether this MeshTypeWithProperties is a subtype of the passed-in
     * MeshTypeWithProperties. This also returns true of they are the same.
     *
     * @param other the MeshTypeWithProperties to test against
     * @return true if this MeshTypeWithProperties is a subtype of other
     * @see #isSubtypeOfDoesNotEqual
     */
    public boolean isSubtypeOfOrEquals(
            MeshTypeWithProperties other );

    /**
     * Determine whether this MeshTypeWithProperties is a subtype of the passed-in
     * MeshTypeWithProperties. This returns false if they are the same.
     *
     * @param other the MeshTypeWithProperties to test against
     * @return true if this MeshTypeWithProperties is a subtype of other
     * @see #isSubtypeOfOrEquals
     */
    public boolean isSubtypeOfDoesNotEqual(
            MeshTypeWithProperties other );
}
