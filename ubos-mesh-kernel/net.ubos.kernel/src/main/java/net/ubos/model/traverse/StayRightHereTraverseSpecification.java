//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.m.ImmutableMMeshObjectSetFactory;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;

/**
 * A degenerate TraverseSpecification that does not perform any traversal of any kind.
 */
public class StayRightHereTraverseSpecification
        extends
            AbstractTraverseSpecification
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method always returns the same singleton instance.
     *
     * @return the singleton instance
     */
    public static StayRightHereTraverseSpecification create()
    {
        return SINGLETON;
    }

    /**
     * Private constructor, use factory method.
     */
    private StayRightHereTraverseSpecification()
    {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObject start )
    {
        MeshObjectSet ret = ImmutableMMeshObjectSetFactory.SINGLETON.createSingleMemberImmutableMeshObjectSet( start );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectSet traverse(
            MeshObjectSet theSet )
    {
        return theSet;
    }

    /**
     * Determine whether a given event, with a source of from where we traverse the
     * TraverseSpecification, may affect the result of the traversal.
     *
     * @param meshBase the MeshBase in which we consider the event
     * @param theEvent the event that we consider
     * @return true if this event may affect the result of traversing from the MeshObject
     *         that sent this event
     */
    @Override
    public boolean isAffectedBy(
            MeshBase                  meshBase,
            AbstractMeshObjectRoleTypeChange theEvent )
    {
        return false;
    }

    /**
     * All implementations of this interface need an equals method.
     *
     * @param other the Object to compare against
     * @return true if the two objects are equal
     */
    @Override
    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    public boolean equals(
            Object other )
    {
        return this == other; // singleton instance
    }

    /**
     * Determine hash code. Make editor happy that otherwise indicates a warning.
     *
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    /**
     * The singleton instance of this class.
     */
    private static final StayRightHereTraverseSpecification SINGLETON = new StayRightHereTraverseSpecification();
}
