//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.StringValue;
import net.ubos.modelbase.m.MMeshTypeIdentifier;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * A MeshType that has a source and a destination, that can participate in
  * inheritance relationships, and may be attributed.
  * In-memory implementation.
  */
public final class MRelationshipType
        extends
            MCollectableMeshType
        implements
            RelationshipType,
            CanBeDumped
{
    /**
     * Constructor to create a binary RelationshipType.
     *
     * @param identifier the Identifier of the to-be-created object
     * @param name the name of the to-be-created object
     * @param sourceEntity the source EntityType of the RelationshipType
     * @param destinationEntity the destination EntityType of the RelationshipType
     * @param sourceMultiplicity the multiplicity of the source
     * @param destinationMultiplicity the multiplicity of the destination
     */
    public MRelationshipType(
            MMeshTypeIdentifier identifier,
            StringValue         name,
            MEntityType         sourceEntity,
            MEntityType         destinationEntity,
            MultiplicityValue   sourceMultiplicity,
            MultiplicityValue   destinationMultiplicity )
    {
        super( identifier );

        setName( name );

        sourceRole      = new MRoleType.Source(      this, sourceEntity,      sourceMultiplicity );
        destinationRole = new MRoleType.Destination( this, destinationEntity, destinationMultiplicity );

        sourceRole.setName(      StringValue.create( name.value() + "_S" ));
        destinationRole.setName( StringValue.create( name.value() + "_D" ));
    }

    /**
     * Constructor to create a undirected RelationshipType.
     *
     * @param identifier the Identifier of the to-be-created object
     * @param name the name of the to-be-created object
     * @param sourceAndDestEntity the associated EntityType of the RelationshipType
     * @param sourceAndDestMultiplicity the multiplicity of the single RoleType
     */
    public MRelationshipType(
            MMeshTypeIdentifier identifier,
            StringValue         name,
            MEntityType         sourceAndDestEntity,
            MultiplicityValue   sourceAndDestMultiplicity )
    {
        super( identifier );

        setName( name );

        sourceRole      = new MRoleType.MSrcDest( this, sourceAndDestEntity, sourceAndDestMultiplicity );
        destinationRole = sourceRole;

        sourceRole.setName( StringValue.create( name.value() + "_U" ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MRoleType getSource()
    {
        return sourceRole;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MRoleType getDestination()
    {
        return destinationRole;
    }

    /**
     * Set the IsAbstract property.
     *
     * @param newValue the new value for the property
     * @see #getIsAbstract
     */
    public final void setIsAbstract(
            BooleanValue newValue )
    {
        this.theIsAbstract = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final BooleanValue getIsAbstract()
    {
        return theIsAbstract;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theIdentifier"
                },
                new Object[] {
                    getIdentifier()
                });
    }

    /**
      * The value of the source RoleType.
      */
    private final MRoleType sourceRole;

    /**
      * The value of the destination RoleType.
      */
    private final MRoleType destinationRole;

    /**
      * The value of the IsAbstract property.
      */
    private BooleanValue theIsAbstract;
}
