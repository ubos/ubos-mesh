//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.traverse;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.WrongMeshBaseViewException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
 * <p>Represents a path of traversal from an (implied) start MeshObject,
 * to another or the same MeshObject, via a sequence of zero or more steps. One
 * step is the combination of a TraverseSpecification, and the resulting
 * MeshObject.</p>
 *
 * <p>TraversalPaths are a handy structure to capture a "path" through an MeshObject
 * graph, similar to a path in a file system, but more powerful. For one, one can
 * traverse different types of TraverseSpecification, not just one as in case of
 * the file system.</p>
 *
 * <p>A TraversalPath is immutable. However, one can add a special type of
 * PropertyChangeListener to it, which will be notified of all PropertyChangeEvents
 * of MeshObjects on the TraversalPath. It uses a special subclass of
 * PropertyChangeEvent, TraversalPathPropertyChangeEvent, to indicate which
 * TraversalPath the changed MeshObject was on.</p>
 *
 * <p>TraversalPath is implemented as a recursive data structure.</p>
 */
public class TraversePath
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( TraversePath.class ); // our own, private logger

    /**
     * Factory method for only one step on the TraversalPath.
     *
     * @param traversedSpec the TraverseSpecification that we traversed
     * @param reachedMeshObject the MeshObject that was reached by doing the traversal
     * @return the created TraversalPath
     */
    public static TraversePath create(
            TraverseSpecification traversedSpec,
            MeshObject             reachedMeshObject )
    {
        return create( traversedSpec, reachedMeshObject, null );
    }

    /**
     * Factory method by prepending a step to an already-existing TraversePath.
     *
     * @param traversedSpec the TraverseSpecification that we traversed
     * @param reachedMeshObject the MeshObject that was reached by doing the traversal
     * @param remainder the TraversalPath to which we prepend this step
     * @return the created TraversalPath
     * @throws WrongMeshBaseViewException thrown if a TraversalPath is prepended to a TraversalPath with MeshObjects in different MeshBases
     */
    public static TraversePath create(
            TraverseSpecification traversedSpec,
            MeshObject            reachedMeshObject,
            TraversePath          remainder )
        throws
            WrongMeshBaseViewException
    {
        int size;

        if( remainder != null ) {
            WrongMeshBaseViewException.checkCompatible( reachedMeshObject.getMeshBaseView(), remainder.getMeshBaseView() );

            size = remainder.getSize() + 1;
        } else {
            size = 1;
        }
        TraversePath ret = new TraversePath(
                traversedSpec,
                reachedMeshObject,
                remainder,
                size,
                reachedMeshObject.getMeshBaseView() );

        return ret;
    }

    /**
     * Factory method to construct one as a concatenation of two other TraversePath.
     *
     * @param one the first TraversalPath
     * @param two the second TraversalPath
     * @return the created TraversalPath
     * @throws WrongMeshBaseViewException thrown if a TraversePath is prepended to a TraversePath with MeshObjects in different MeshBases
     */
    public static TraversePath create(
            TraversePath one,
            TraversePath two )
        throws
            WrongMeshBaseViewException
    {
        if( one == null || two == null ) {
            log.error( "argument is null" );
        }
        WrongMeshBaseViewException.checkCompatible( one.getMeshBaseView(), two.getMeshBaseView() );

        TraversePath remainder = recursiveCreate( one.theRemainder, two );

        TraversePath ret = new TraversePath(
                one.theTraversedSpec,
                one.theReached,
                remainder,
                one.theSize + two.theSize,
                one.getMeshBaseView() );

        return ret;
    }

    /**
     * Factory method to construct one by concatenating another step to an existing TravTraversePathersalPath.
     *
     * @param one the first TraversalPath
     * @param spec the TraverseSpecification traversed from the last MeshObject of the first TraversalPath
     * @param reached the MeshObject reached in the process
     * @return the created TraversalPath
     * @throws WrongMeshBaseViewException thrown if a TraversePath is prepended to a TraversePath with MeshObjects in different MeshBases
     */
    public static TraversePath create(
            TraversePath          one,
            TraverseSpecification spec,
            MeshObject            reached )
        throws
            WrongMeshBaseViewException
    {
        return create( one, TraversePath.create( spec, reached ));
    }

    /**
     * Factory method to construct one from equal-length arrays of TraverseSpecification and reached MeshObjects.
     *
     * @param steps the steps taken, in sequence
     * @param reached the MeshObject reached after each step
     * @return the created TraversePath
     */
    public static TraversePath create(
            TraverseSpecification [] steps,
            MeshObject []            reached )
    {
        if( steps.length != reached.length ) {
            throw new IllegalArgumentException( "Steps: " + steps.length + " vs. reached: " + reached.length );
        }
        TraversePath ret = null;
        for( int i=steps.length-1 ; i>=0 ; --i ) {
            ret = create( steps[i], reached[i], ret );
        }
        return ret;
    }

    /**
     * Little helper method to recursively create.
     *
     * @param here the TraversePath that is the first part
     * @param remainder the remainder TraversePath
     * @return the created TraversePath
     */
    private static TraversePath recursiveCreate(
            TraversePath here,
            TraversePath remainder )
    {
        if( here == null ) {
            return remainder;
        } else {
            TraversePath nextStep = recursiveCreate( here.theRemainder, remainder );
            int          size     = ( nextStep != null ) ? ( nextStep.theSize+1 ) : 1;
            return new TraversePath(
                    here.theTraversedSpec,
                    here.theReached,
                    nextStep,
                    size,
                    here.getMeshBaseView() );
        }
    }

    /**
     * Constructor.
     *
     * @param traversedSpec the TraverseSpecification that we traversed
     * @param reachedMeshObject the MeshObject that was reached by doing the traversal
     * @param remainder the remaining TraversalPath (recursive structure)
     * @param size the number of steps in this TraversePath
     * @param mbv the MeshBaseView against which to resolve the MeshObject
     */
    protected TraversePath(
            TraverseSpecification traversedSpec,
            MeshObject            reachedMeshObject,
            TraversePath          remainder,
            int                   size,
            MeshBaseView          mbv )
    {
        theTraversedSpec     = traversedSpec;
        theReached           = reachedMeshObject;
        theReachedIdentifier = reachedMeshObject.getIdentifier();
        theRemainder         = remainder;
        theSize              = size;
        theMeshBaseView      = mbv;
    }

    /**
     * Set the traceMethodCallEntry name. This is to be used for debugging only.
     *
     * @param newValue the new value
     */
    public void setDebugName(
            String newValue )
    {
        theDebugName = newValue;
    }

    /**
     * Obtain the traceMethodCallEntry name. This is to be used for debugging only.
     *
     * @return the name
     */
    public String getDebugName()
    {
        return theDebugName;
    }

    /**
     * Set the MeshBase against which this TraversalPath shall be resolved.
     *
     * @param newValue the new value
     */
    public void setMeshBase(
            MeshBase newValue )
    {
        if( newValue == null || newValue != theMeshBaseView ) {
            theMeshBaseView = newValue;

            theReached = null;
            theRemainder.setMeshBase( newValue );
        }
    }

    /**
     * Obtain the MeshBaseView to which this TraversalPath belongs.
     *
     * @return the MeshBaseView
     */
    public MeshBaseView getMeshBaseView()
    {
        return theMeshBaseView;
    }

    /**
     * Determine the number of steps in this TraversalPath.
     *
     * @return the number of steps in this TraversalPath
     */
    public int getSize()
    {
        return theSize;
    }

    /**
     * Obtain the TraverseSpecification at a certain position in this TraversalPath.
     *
     * @param index the index from which we want to obtain the TraverseSpecification
     * @return the TraverseSpecification at this index
     * @throws ArrayIndexOutOfBoundsException if index is out of range
     */
    public TraverseSpecification getTraverseSpecificationAt(
            int index )
    {
        if( index >= theSize ) {
            throw new ArrayIndexOutOfBoundsException( index );
        }
        if( index == 0 ) {
            return theTraversedSpec;
        }
        return theRemainder.getTraverseSpecificationAt( index-1 );
    }

    /**
     * Obtain the TraverseSpecifications traversed along the entire TraversalPath in sequence.
     *
     * @return the TraverseSpecifications in sequence
     */
    public TraverseSpecification [] getTraverseSpecificationsInSequence()
    {
        TraverseSpecification [] ret     = new TraverseSpecification[ theSize ];
        TraversePath             current = this;
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = current.theTraversedSpec;
            current = current.theRemainder;
        }
        return ret;
    }

    /**
     * Obtain the TraverseSpecification traversed along the entire
     * TraversalPath. If this TraversalPath is longer than one step,
     * the returned TraverseSpecification will be a SequentialCompoundTraverseSpecification.
     *
     * @return the TraverseSpecification
     */
    public TraverseSpecification getTraverseSpecification()
    {
        if( theSize == 1 ) {
            return theTraversedSpec;
        } else {
            SequentialCompoundTraverseSpecification ret
                    = SequentialCompoundTraverseSpecification.create(
                            getTraverseSpecificationsInSequence() );
            return ret;
        }
    }

    /**
     * Obtain the MeshObject at a certain position in this TraversalPath.
     *
     * @param index the index from which we want to obtain the MeshObject
     * @return the MeshObject at this index
     * @throws ArrayIndexOutOfBoundsException if index is out of range
     */
    public MeshObject getMeshObjectAt(
            int index )
    {
        if( index >= theSize ) {
            throw new ArrayIndexOutOfBoundsException( index );
        }
        if( index == 0 ) {
            return theReached;
        }
        return theRemainder.getMeshObjectAt( index-1 );
    }

    /**
     * This convenience method returns all MeshObjects in this TraversalPath in sequence.
     *
     * @return an array of size getSize(), which contains the MeshObject on the TraversalPath
     * @throws MeshObjectsNotFoundException thrown if the MeshObject could not be resolved
     */
    public MeshObject [] getMeshObjectsInSequence()
        throws
            MeshObjectsNotFoundException
    {
        MeshObject [] ret = new MeshObject[ getSize() ];

        TraversePath current = this;
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i]  = current.getFirstMeshObject(); // may throw
            current = current.getNextSegment();
        }
        return ret;
    }

    /**
     * This convenience method returns the identifiers of all MeshObjects in this TraversalPath
     * in sequence.
     *
     * @return an array of size getSize(), which contains the MeshObjectIdentifiers of the MeshObjects on the TraversalPath
     */
    public MeshObjectIdentifier [] getMeshObjectIdentifiersInSequence()
    {
        MeshObjectIdentifier [] ret = new MeshObjectIdentifier[ getSize() ];
        TraversePath            current = this;

        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i]  = current.getFirstMeshObjectIdentifier();
            current = current.getNextSegment();
        }
        return ret;
    }

    /**
     * Obtain the first MeshObject on this TraversalPath.
     *
     * @return the first MeshObject on this TraversalPath
     * @throws MeshObjectsNotFoundException thrown if the MeshObject could not be resolved
     */
    public synchronized MeshObject getFirstMeshObject()
        throws
            MeshObjectsNotFoundException
    {
        if( theReached == null ) {
            theReached = resolveMeshObject( theReachedIdentifier );
        }
        return theReached;
    }

    /**
     * Obtain the identifier of the first MeshObject on this TraversalPath.
     *
     * @return the identifier of the first MeshObject on this TraversalPath
     */
    public MeshObjectIdentifier getFirstMeshObjectIdentifier()
    {
        return theReachedIdentifier;
    }

    /**
     * Obtain the last MeshObject on this TraversalPath.
     *
     * @return the last MeshObject on this TraversalPath
     */
    public MeshObject getLastMeshObject()
    {
        if( theRemainder != null ) {
            return theRemainder.getLastMeshObject();
        } else {
            return theReached;
        }
    }

    /**
     * Obtain the next segment in this TraversalPath.
     *
     * @return obtain the next segment in this TraversalPath, if there is one
     */
    public TraversePath getNextSegment()
    {
        return theRemainder;
    }

    /**
     * Resolve a MeshObject
     *
     * @param identifier the MeshObjectIdentifier to resolve
     * @return the resolved MeshObject
     * @throws MeshObjectsNotFoundException thrown if the MeshObject could not be resolved
     */
    protected MeshObject resolveMeshObject(
            MeshObjectIdentifier identifier )
        throws
            MeshObjectsNotFoundException
    {
        if( theMeshBaseView == null ) {
            throw new NullPointerException();
        }

        MeshObject ret = theMeshBaseView.findMeshObjectByIdentifierOrThrow( identifier );
        return ret;
    }

    /**
     * Determine equality.
     *
     * @param other the Object to compare against
     * @return true if the objects are equal
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof TraversePath ) {

            TraversePath realOther = (TraversePath) other;

            if( theRemainder == null && realOther.theRemainder != null ) {
                return false;
            }
            if( theRemainder != null && realOther.theRemainder == null ) {
                return false;
            }

            if( theTraversedSpec == null ) {
                if( realOther.theTraversedSpec != null ) {
                    return false;
                }
            } else if( !theTraversedSpec.equals( realOther.theTraversedSpec )) {
                return false;
            }

            if( !theReached.equals( realOther.theReached )) {
                return false;
            }

            if( theRemainder != null ) {
                return theRemainder.equals( realOther.theRemainder );
            }
            return true;
        }
        return false;
    }

    /**
     * Obtain the hash code, which is a combination of of the reached MeshObjects' hash codes.
     *
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        if( theTraversedSpec != null ) {
            return theReached.hashCode() ^ theTraversedSpec.hashCode(); // good enough
        } else {
            return theReached.hashCode(); // good enough
        }
    }

    /**
     * Determine whether this TraversalPath starts with all of the TraversalPath elements
     * of the passed-in test path.
     *
     * @param testPath the TraversalPath to test against
     * @return true if testPath is fully contained in this TraversalPath at the beginning
     */
    public boolean startsWith(
            TraversePath testPath )
    {
        if( testPath == null ) {
            return true;
        }

        if( theTraversedSpec == null ) {
            if( testPath.theTraversedSpec != null ) {
                return false;
            }
        } else if( !theTraversedSpec.equals( testPath.theTraversedSpec )) {
            return false;
        }

        if( !theReached.equals( testPath.theReached )) {
            return false;
        }

        if( theRemainder != null ) {
            return theRemainder.startsWith( testPath.theRemainder );
        } else {
            return testPath.theRemainder == null;
        }
    }

    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "debugName",
                    "traversedSpec",
                    "theReachedIdentifier",
                    "remainder"
                },
                new Object[] {
                    theDebugName,
                    theTraversedSpec,
                    theReachedIdentifier,
                    theRemainder
                });
    }

    /**
     * Our name (if any). For debugging only.
     */
    protected String theDebugName;

    /**
     * The first TraverseSpecification that we traversed.
     */
    protected TraverseSpecification theTraversedSpec;

    /**
     * The MeshBaseView against which to resolve the MeshObject.
     */
    protected transient MeshBaseView theMeshBaseView;

    /**
     * The MeshObject that we reached after we traversed the TraverseSpecification.
     */
    protected transient MeshObject theReached;

    /**
     * The identifier of the MeshObject that we reached after we traversed the TraverseSpecification.
     */
    protected MeshObjectIdentifier theReachedIdentifier;

    /**
     * The remainder (aka second part) of this TraversePath.
     */
    protected TraversePath theRemainder;

    /**
     * The length of this TraversePath. We store this to avoid the frequent, recursive
     * calculations.
     */
    protected int theSize;
}
