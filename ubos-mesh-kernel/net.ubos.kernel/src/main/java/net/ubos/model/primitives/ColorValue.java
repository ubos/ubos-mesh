//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.awt.Color;

/**
 * A color PropertyValue.
 */
public final class ColorValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method.
     *
     * @param value the JDK's definition of Color
     * @return the created ColorValue
     * @throws IllegalArgumentException if null was provided
     */
    public static ColorValue create(
            Color value )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new ColorValue( value.getRGB() );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the JDK's definition of Color
     * @return the created ColorValue, or null
     * @throws IllegalArgumentException if null was provided
     */
    public static ColorValue createOrNull(
            Color value )
    {
        if( value == null ) {
            return null;
        }
        return new ColorValue( value.getRGB() );
    }

    /**
     * Factory method.
     *
     * @param value the value as RGB value
     * @return the created ColorValue
     */
    public static ColorValue create(
            int value )
    {
        return new ColorValue( value );
    }

    /**
     * Factory method.
     *
     * @param r the red component
     * @param g the green component
     * @param b the blue component
     * @return the created ColorValue
     */
    public static ColorValue create(
            int r,
            int g,
            int b )
    {
        return create( r, g, b, 255 );
    }

    /**
     * Factory method.
     *
     * @param r the red component
     * @param g the green component
     * @param b the blue component
     * @param a the alpha component
     * @return the created ColorValue
     */
    public static ColorValue create(
            int r,
            int g,
            int b,
            int a )
    {
        @SuppressWarnings("PointlessBitwiseExpression")
        ColorValue ret = new ColorValue(
                  (( a & 0xFF ) << 24 )
                | (( r & 0xFF ) << 16 )
                | (( g & 0xFF ) <<  8 )
                | (( b & 0xFF ) <<  0 ));

        return ret;
    }

    /**
     * Factory method.
     *
     * @param r the red component
     * @param g the green component
     * @param b the blue component
     * @return the created ColorValue
     */
    public static ColorValue create(
            float r,
            float g,
            float b )
    {
        return create( r, g, b, 1.0f );
    }

    /**
     * Factory method.
     *
     * @param r the red component
     * @param g the green component
     * @param b the blue component
     * @param a the alpha component
     * @return the created ColorValue
     */
    public static ColorValue create(
            float r,
            float g,
            float b,
            float a )
    {
        return create(
                (int) (r*255+0.5),
                (int) (g*255+0.5),
                (int) (b*255+0.5));
    }

    /**
     * Initialize to specified value.
     *
     * @param value the JDK's definition of Color
     */
    private ColorValue(
            int value )
    {
        this.theValue = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
      * Convert back to the JDK's definition of this Color.
      *
      * @return the JDK's definition of this Color.
      */
    @Override
    public Color value()
    {
        return new Color( theValue );
    }

    /**
      * Convert back to the JDK's definition of this Color.
      *
      * @return the JDK's definition of this Color.
      */
    @Override
    public Color getValue()
    {
        return value();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ColorDataType getDataType()
    {
        return ColorDataType.theDefault;
    }

    /**
     * Obtain the value of the alpha component.
     *
     * @return value of the alpha component
     */
    public int getAlpha()
    {
        return ( theValue >> 24 ) & 0xFF;
    }

    /**
      * Obtain value of the red component.
      *
      * @return value of the red component
      */
    public int getRed()
    {
        return ( theValue >> 16 ) & 0xFF;
    }

    /**
     * Obtain value of the blue component.
     *
     * @return value of the blue component
     */
    @SuppressWarnings("PointlessBitwiseExpression")
    public int getBlue()
    {
        return ( theValue >> 0 ) & 0xFF;
    }

    /**
     * Obtain value of the green component.
     *
     * @return value of the green component
     */
    public int getGreen()
    {
        return ( theValue >> 8 ) & 0xFF;
    }

    /**
     * Obtain as RGB value.
     *
     * @return the RGB value
     */
    public int getRGB()
    {
        return theValue;
    }

    /**
     * Obtain as a CSS-style hex string.
     *
     * @return the string
     */
    public String getAsCssHex()
    {
        return String.format( "#%02X%02X%02X", getRed(), getGreen(), getBlue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof ColorValue ) {
            return theValue == ((ColorValue)otherValue).theValue;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        StringBuilder buf = new StringBuilder( 128 );
        buf.append( getClass().getName());
        buf.append( DataType.CREATE_STRING );
        buf.append( theValue );
        buf.append( DataType.CLOSE_PARENTHESIS_STRING );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( "(" );
        buf.append( getRed() );
        buf.append( ";" );
        buf.append( getGreen() );
        buf.append( ";" );
        buf.append( getBlue() );
        buf.append( ";" );
        buf.append( getAlpha() );
        buf.append( ")" );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        ColorValue realValue = (ColorValue) o;

        if( theValue == realValue.theValue ) {
            return 0;
        } else {
            return +2; // not comparable convention: +2
        }
    }

    /**
      * This attempts to parse a string and turn it into a integer value similarly
 to Integer.parseInt().
      *
      * @param theString the string that shall be parsed
      * @return the created IntegerValue
      * @throws NumberFormatException thrown if theString does not follow the correct syntax
      */
    public static ColorValue parseColorValue(
            String theString )
        throws
            NumberFormatException
    {
        return ColorValue.create( Integer.parseInt( theString ));
    }

    /**
      * The value encoded the same way as java.awt.Color.
      */
    protected int theValue;
}
