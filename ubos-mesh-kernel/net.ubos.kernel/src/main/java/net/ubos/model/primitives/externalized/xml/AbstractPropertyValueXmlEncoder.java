//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.Base64;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.AbstractPropertyValueEncoder;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.util.XmlUtils;

/**
 * Knows how to encode a PropertyValue to XML.
 */
public abstract class AbstractPropertyValueXmlEncoder
    extends
        AbstractPropertyValueEncoder
    implements
        PropertyValueXmlTags
{
    /**
     * Serialize a PropertyValue to an OutputStream.
     *
     * @param value the PropertyValue
     * @param w the Writer to which to write the PropertyValue
     * @throws EncodingException thrown if a problem occurred during encoding
     * @throws IOException thrown if an I/O error occurred
     */
    @Override
    public void writePropertyValue(
            PropertyValue value,
            Writer        w )
        throws
            EncodingException,
            IOException
    {
        if( value == null ) {
            w.write( "<" );
            w.write( NULL_VALUE );
            w.write( "/>" );

        } else if( value instanceof BlobValue ) {
            BlobValue realValue = (BlobValue) value;
            String    mt        = realValue.getMimeType();
            if( mt == null ) {
                mt = "?/?";
            }

            w.write( "<" );
            w.write( BLOB_VALUE_TAG );
            w.write( " " );
            w.write( BLOB_VALUE_MIME_TAG );
            w.write( "=\"" );
            w.write( mt );
            w.write( "\">" );

            // inline value, even if delayed loading -- hard to keep track of which ClassLoader to use
            if( mt.startsWith( "text/" )) {
                w.write( "<![CDATA[" );
                w.write( XmlUtils.cdataEscape( realValue.getAsString() ) );
                w.write( "]]>" );
            } else {
                w.write( new String( Base64.getEncoder().encode( realValue.value() ) ));
            }
            w.write( "</" );
            w.write( BLOB_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof BooleanValue ) {
            w.write( "<" );
            w.write( BOOLEAN_VALUE_TAG );
            w.write( ">" );
            if( ((BooleanValue)value).value() ) {
                w.write( BOOLEAN_VALUE_TRUE_TAG );
            } else {
                w.write( BOOLEAN_VALUE_FALSE_TAG );
            }
            w.write( "</" );
            w.write( BOOLEAN_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof ColorValue ) {
            ColorValue realValue = (ColorValue)value;
            w.write( "<" );
            w.write( COLOR_VALUE_TAG );
            w.write( ">" );
            w.write( String.valueOf( realValue.getRGB()) );
            w.write( "</" );
            w.write( COLOR_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof CurrencyValue ) {
            CurrencyValue realValue = (CurrencyValue)value;
            w.write( "<" );
            w.write( CURRENCY_VALUE_TAG );
            w.write( ">" );
            w.write( XmlUtils.escape( realValue.value() ));
            w.write( "</" );
            w.write( CURRENCY_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof EnumeratedValue ) {
            EnumeratedValue realValue = (EnumeratedValue) value;
            w.write( "<" );
            w.write( ENUMERATED_VALUE_TAG );
            w.write( ">" );
            w.write( XmlUtils.escape( realValue.value() ));
            w.write( "</" );
            w.write( ENUMERATED_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof ExtentValue ) {
            ExtentValue realValue = (ExtentValue) value;
            w.write( "<" );
            w.write( EXTENT_VALUE_TAG );
            w.write( " " );
            w.write( EXTENT_VALUE_WIDTH_TAG );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getWidth() ));
            w.write( "\" " );
            w.write( EXTENT_VALUE_HEIGHT_TAG );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getHeight() ));
            w.write( "\"/>" );

        } else if( value instanceof FloatValue ) {
            FloatValue realValue = (FloatValue) value;
            w.write( "<" );
            w.write( FLOAT_VALUE_TAG );
            w.write( ">" );
            w.write( String.valueOf( realValue.value() ));
            w.write( "</" );
            w.write( FLOAT_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof IntegerValue ) {
            IntegerValue realValue = (IntegerValue) value;
            w.write( "<" );
            w.write( INTEGER_VALUE_TAG );
            w.write( ">" );
            w.write( String.valueOf( realValue.value() ));
            w.write( "</" );
            w.write( INTEGER_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof MultiplicityValue ) {
            MultiplicityValue realValue = (MultiplicityValue) value;
            w.write( "<" );
            w.write( MULTIPLICITY_VALUE_TAG );
            w.write( " " );
            w.write( MULTIPLICITY_VALUE_MIN_TAG );
            w.write( "=\"" );
            w.write( realValue.getMinimumAsString() );
            w.write( "\" " );
            w.write( MULTIPLICITY_VALUE_MAX_TAG );
            w.write( "=\"" );
            w.write( realValue.getMaximumAsString() );
            w.write( "\"/>" );

        } else if( value instanceof PointValue ) {
            PointValue realValue = (PointValue) value;
            w.write( "<" );
            w.write( POINT_VALUE_TAG );
            w.write( " " );
            w.write( POINT_VALUE_X_TAG );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getX() ));
            w.write( "\" " );
            w.write( POINT_VALUE_Y_TAG );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getY() ));
            w.write( "\"/>" );

        } else if( value instanceof StringValue ) {
            StringValue realValue = (StringValue) value;
            w.write( "<" );
            w.write( STRING_VALUE_TAG );
            w.write( ">" );
            w.write( XmlUtils.escape( realValue.value() ));
            w.write( "</" );
            w.write( STRING_VALUE_TAG );
            w.write( ">" );

        } else if( value instanceof TimePeriodValue ) {
            TimePeriodValue realValue = (TimePeriodValue) value;
            w.write( "<" );
            w.write( TIME_PERIOD_TAG );
            w.write( " " );
            w.write( TIME_PERIOD_YEAR_TAG   );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getYear() ));
            w.write( "\"" );
            w.write( " " );
            w.write( TIME_PERIOD_MONTH_TAG  );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getMonth() ));
            w.write( "\"" );
            w.write( " " );
            w.write( TIME_PERIOD_DAY_TAG    );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getDay() ));
            w.write( "\"" );
            w.write( " " );
            w.write( TIME_PERIOD_HOUR_TAG   );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getHour() ));
            w.write( "\"" );
            w.write( " " );
            w.write( TIME_PERIOD_MINUTE_TAG );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getMinute() ));
            w.write( "\"" );
            w.write( " " );
            w.write( TIME_PERIOD_SECOND_TAG );
            w.write( "=\"" );
            w.write( String.valueOf( realValue.getSecond() ));
            w.write( "\"/>" );

        } else if( value instanceof TimeStampValue ) {
            TimeStampValue realValue = (TimeStampValue) value;
            w.write( "<" );
            w.write( TIME_STAMP_TAG );
            w.write( ">" );
            w.write( realValue.getAsRfc3339String() );
            w.write( "</" );
            w.write( TIME_STAMP_TAG );
            w.write( ">" );

        } else {
            w.write( "?" );
        }
    }

    /**
     * Encode a String value to it can be used as an attribute value in XML.
     *
     * @param value the String to encode
     * @param w the Writer to write to
     * @throws IOException thrown if an I/O error occurred
     */
    public static void writeQuotedString(
            String value,
            Writer w )
        throws
            IOException
    {
        String escaped = XmlUtils.escape( value );
        w.write( escaped );
    }

    /**
     * Encode a long value.
     *
     * @param value the long value to encode
     * @param w the Writer to write to
     * @throws IOException thrown if an I/O error occurred
     */
    public static void writeLong(
            long   value,
            Writer w )
        throws
            IOException
    {
        w.write( String.valueOf( value ));
    }
}
