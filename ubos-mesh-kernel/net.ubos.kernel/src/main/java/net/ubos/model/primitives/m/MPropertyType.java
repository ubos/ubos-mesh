//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.IllegalPropertyValueException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.modelbase.m.MMeshTypeIdentifier;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
  * Implementation of a value-holding property of an AttributableMeshObjectType.
  * In-memory implementation.
  */
public class MPropertyType
        extends
            MCollectableMeshType
        implements
            PropertyType,
            CanBeDumped
{
    private static final Log  log = Log.getLogInstance( MPropertyType.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     */
    public MPropertyType(
            MMeshTypeIdentifier identifier )
    {
        super( identifier );
    }

    /**
      * Set a value for the DataType property.
      *
      * @param newValue the new value for the property
      * @see #getDataType
      */
    public final void setDataType(
            DataType newValue )
    {
        this.theDataType = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final DataType getDataType()
    {
        return theDataType;
    }

    /**
      * Set a value for the DefaultValue property.
      *
      * @param newValue the new value for the property
      * @see #getDefaultValue
      */
    public final void setDefaultValue(
            PropertyValue newValue )
    {
        this.theDefaultValue = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyValue getDefaultValue()
    {
        return theDefaultValue;
    }

    /**
      * Set a value for the IsOptional property.
      *
      * @param newValue the new value for the property
      * @see #getIsOptional
      */
    public final void setIsOptional(
            BooleanValue newValue )
    {
        this.theIsOptional = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final BooleanValue getIsOptional()
    {
        return theIsOptional;
    }

    /**
      * Set a value for the IsReadOnly property.
      *
      * @param newValue the new value for the property
      * @see #getIsReadOnly
      */
    public final void setIsReadOnly(
            BooleanValue newValue )
    {
        this.theIsReadOnly = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final BooleanValue getIsReadOnly()
    {
        return this.theIsReadOnly;
    }

    /**
      * Set the MeshTypeWithProperties on which this PropertyType is defined.
      *
      * @param newValue the new value of the property
      * @see #getMeshTypeWithProperties
      */
    public final void setMeshTypeWithProperties(
            MeshTypeWithProperties newValue )
    {
        this.theMeshTypeWithProperties = newValue;
    }

    /**
      * Obtain the MeshTypeWithProperties on which this PropertyTypeis defined.
      *
      * @return the MeshTypeWithProperties on which this PropertyType is defined
      * @see #setMeshTypeWithProperties
      */
    @Override
    public final MeshTypeWithProperties getMeshTypeWithProperties()
    {
        return theMeshTypeWithProperties;
    }

    /**
     * Set the value of the SequenceNumber property.
     *
     * @param newValue the new value of the SequenceNumber property
     * @see #getSequenceNumber
     */
    public final void setSequenceNumber(
            FloatValue newValue )
    {
        theSequenceNumber = newValue;
    }

    /**
     * Obtain the value of the SequenceNumber property.
     *
     * @return the value of the SequenceNumber property
     * @see #setSequenceNumber
     */
    @Override
    public final FloatValue getSequenceNumber()
    {
        return theSequenceNumber;
    }

    /**
     * Set the PropertyTypes that we override (if any).
     *
     * @param newValue the PropertyTypes that we override
     * @see #getOverride
     */
    public final void setOverride(
            PropertyType [] newValue )
    {
        theOverride = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyType [] getOverride()
    {
        return theOverride;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final PropertyType getOverrideAncestor()
    {
        if( theOverride.length == 0 ) {
            return this;
        } else {
            return theOverride[0].getOverrideAncestor(); // we can just pick one, they all lead to the same one
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean overrides(
            PropertyType other )
    {
        for( int i=0 ; i<theOverride.length ; ++i ) {
            if( theOverride[i].equals( other )) {
                return true;
            }
        }
        for( int i=0 ; i<theOverride.length ; ++i ) {
            if( theOverride[i].overrides( other )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean equalsOrOverrides(
            PropertyType other )
    {
        if( equals( other )) {
            return true;
        }
        return overrides( other );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkValue(
            MeshObject affected )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException
    {
        if( theIsOptional == BooleanValue.TRUE ) {
            return;
        }
        PropertyValue value = affected.getPropertyValue( this );
        if( value == null ) {
            throw new IllegalPropertyValueException( affected, this, value );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theIdentifier"
                },
                new Object[] {
                    getIdentifier()
                });
    }

    /**
      * The value for the DataType property.
      */
    private DataType theDataType;

    /**
      * Our default value.
      */
    private PropertyValue theDefaultValue;

    /**
     * The value for the IsOptional property.
     */
    private BooleanValue theIsOptional;

    /**
     * The value for the IsReadOnly property.
     */
    private BooleanValue theIsReadOnly;

    /**
      * The MeshTypeWithProperties that this PropertyType is defined in.
      */
    private MeshTypeWithProperties theMeshTypeWithProperties;

    /**
     * The value of the SequenceNumber property.
     */
    private FloatValue theSequenceNumber;

    /**
     * The PropertyTypes that this PropertyType overrides.
     */
    private PropertyType [] theOverride = new PropertyType[0];
}
