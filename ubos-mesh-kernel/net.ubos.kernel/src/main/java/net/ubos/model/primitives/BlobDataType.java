//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import java.util.Base64;
import java.util.regex.Pattern;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StringHelper;
import net.ubos.util.logging.Dumper;

/**
  * A Binary Large Object DataType. It carries a MIME type, identifying its content.
  * The MIME type may be null, indicating that instances of this DataType can hold any data. If the MIME
  * type is non-null, only data of the respective MIME type can be held by instances.
  */
public final class BlobDataType
        extends
            DataType
{
    /**
     * Just a helper variable for initialization
     */
    private static final Pattern [] theKnownMimeTypePatterns;
    static {
        theKnownMimeTypePatterns = new Pattern[ SelectableMimeType.KNOWN_MIME_TYPES.length ];
        for( int i=0 ; i<theKnownMimeTypePatterns.length ; ++i ) {
            theKnownMimeTypePatterns[i] = Pattern.compile( SelectableMimeType.KNOWN_MIME_TYPES[i].getMimeType() );
        }
    }

    /**
     * This DataType allows any MIME type.
     */
    public static final BlobDataType theAnyType = create(
            "",
            SelectableMimeType.KNOWN_MIME_TYPES, // all known ones are offered to the user
            new Pattern[] { Pattern.compile( ".+" ) },
            null );

    /**
     * This DataType allows any known MIME type.
     */
    public static final BlobDataType theAnyKnownType = create(
            "",
            SelectableMimeType.KNOWN_MIME_TYPES, // all known ones are offered to the user
            theKnownMimeTypePatterns,
            null );

    /**
     * This is a text DataType of any text format, plain or formatted.
     */
    public static final BlobDataType theTextAnyType = create(
            "",
            new SelectableMimeType[] {
                    SelectableMimeType.TEXT_PLAIN,
                    SelectableMimeType.TEXT_HTML
            },
            new Pattern[] {
                    Pattern.compile( Pattern.quote( SelectableMimeType.TEXT_PLAIN.getMimeType() )),
                    Pattern.compile( Pattern.quote( SelectableMimeType.TEXT_HTML.getMimeType() ))
            },
            theAnyType );

    /**
     * This is a plain text DataType.
     */
    public static final BlobDataType theTextPlainType = create(
            "",
            new SelectableMimeType [] {
                SelectableMimeType.TEXT_PLAIN
            },
            new Pattern [] {
                Pattern.compile( Pattern.quote( SelectableMimeType.TEXT_PLAIN.getMimeType() ))
            },
            theTextAnyType );

    /**
     * This is an HTML text DataType.
     */
    public static final BlobDataType theTextHtmlType = create(
            "",
            new SelectableMimeType [] {
                SelectableMimeType.TEXT_HTML
            },
            new Pattern [] {
                Pattern.compile( Pattern.quote( SelectableMimeType.TEXT_HTML.getMimeType() ))
            },
            theTextAnyType );

    /**
     * This is an XML text DataType.
     */
    public static final BlobDataType theTextXmlType = create(
            "<xml/>",
            new SelectableMimeType [] {
                    SelectableMimeType.TEXT_XML,
                    SelectableMimeType.APPLICATION_XML
            },
            new Pattern [] {
                    Pattern.compile( Pattern.quote( SelectableMimeType.TEXT_XML.getMimeType() )),
                    Pattern.compile( Pattern.quote( SelectableMimeType.APPLICATION_XML.getMimeType() )),
                    Pattern.compile( "^application/(.+)\\+xml$" )
            },
            theTextAnyType );

    /**
     * This is a JSON text DataType.
     */
    public static final BlobDataType theTextJsonType = create(
            "{}",
            new SelectableMimeType [] {
                    SelectableMimeType.TEXT_JSON
            },
            new Pattern[] {
                    Pattern.compile( Pattern.quote( SelectableMimeType.TEXT_JSON.getMimeType() ))
            },
            theTextAnyType );

    /**
     * Helper variable.
     */
    private static final String packageName;
    static{
        String className = BlobDataType.class.getName();
        packageName      = className.substring( 0, className.lastIndexOf( '.' ) ).replace( '.', '/' );
    }

    /**
     * This is an image DataType whose underlying representation is supported
     * by the JDK. Currently that is GIF, JPG and PNG.
     */
    public static final BlobDataType theJdkSupportedBitmapType = createByLoadingFrom(
            BlobDataType.class.getClassLoader(),
            packageName + "/BlobDefaultValue.gif",
            new SelectableMimeType [] {
                    SelectableMimeType.IMAGE_GIF,
                    SelectableMimeType.IMAGE_JPEG,
                    SelectableMimeType.IMAGE_PNG
            },
            new Pattern [] {
                    Pattern.compile( Pattern.quote( SelectableMimeType.IMAGE_GIF.getMimeType() )),
                    Pattern.compile( Pattern.quote( SelectableMimeType.IMAGE_JPEG.getMimeType() )),
                    Pattern.compile( Pattern.quote( SelectableMimeType.IMAGE_PNG.getMimeType() ))
            },
            theAnyType );

    /**
     * This is a GIF DataType.
     */
    public static final BlobDataType theGifType = createByLoadingFrom(
            BlobDataType.class.getClassLoader(),
            packageName + "/BlobDefaultValue.gif",
            new SelectableMimeType [] {
                SelectableMimeType.IMAGE_GIF
            },
            new Pattern [] {
                Pattern.compile( Pattern.quote( SelectableMimeType.IMAGE_GIF.getMimeType() ))
            },
            theJdkSupportedBitmapType );

    /**
     * This is a JPG DataType.
     */
    public static final BlobDataType theJpgType = createByLoadingFrom(
            BlobDataType.class.getClassLoader(),
            packageName + "/BlobDefaultValue.jpg",
            new SelectableMimeType [] {
                SelectableMimeType.IMAGE_JPEG
            },
            new Pattern [] {
                Pattern.compile( Pattern.quote( SelectableMimeType.IMAGE_JPEG.getMimeType() ))
            },
            theJdkSupportedBitmapType );

    /**
     * This is a PNG DataType.
     */
    public static final BlobDataType thePngType = createByLoadingFrom(
            BlobDataType.class.getClassLoader(),
            packageName + "/BlobDefaultValue.png",
            new SelectableMimeType [] {
                SelectableMimeType.IMAGE_PNG
            },
            new Pattern [] {
                Pattern.compile( Pattern.quote( SelectableMimeType.IMAGE_PNG.getMimeType() ))
            },
            theJdkSupportedBitmapType );

    /**
     * This is a Favicon DataType.
     */
    public static final BlobDataType theFaviconType = createByLoadingFrom(
            BlobDataType.class.getClassLoader(),
            packageName + "/BlobDefaultValue.ico",
            new SelectableMimeType [] {
                SelectableMimeType.IMAGE_FAVICON
            },
            new Pattern [] {
                Pattern.compile( Pattern.quote( SelectableMimeType.IMAGE_FAVICON.getMimeType() ))
            },
            theAnyType );

    /**
      * This is the default instance of this class (plain text).
      */
    public static final BlobDataType theDefault = theAnyType;

    /**
     * Factory method.
     *
     * @param defaultValue default value for instances of this DataType, expressed as String. May be null.
     * @param userSelectableMimeTypes MIME types offered to the user, the first one being the MIME type of the default value
     * @param mimeTypeRegexes the allowed MIME types
     * @param superType supertype of this DataType
     * @return the created BlobDataType
     */
    public static BlobDataType create(
            String                defaultValue,
            SelectableMimeType [] userSelectableMimeTypes,
            Pattern []            mimeTypeRegexes,
            DataType              superType )
    {
        return new BlobDataType( defaultValue, userSelectableMimeTypes, mimeTypeRegexes, superType );
    }

    /**
     * Factory method.
     *
     * @param defaultValue default value for instances of this DataType, expressed as byte array. May be null.
     * @param userSelectableMimeTypes MIME types offered to the user, the first one being the MIME type of the default value
     * @param mimeTypeRegexes the allowed MIME types
     * @param superType supertype of this DataType
     * @return the created BlobDataType
     */
    public static BlobDataType create(
            byte []               defaultValue,
            SelectableMimeType [] userSelectableMimeTypes,
            Pattern []            mimeTypeRegexes,
            DataType              superType )
    {
        return new BlobDataType( defaultValue, userSelectableMimeTypes, mimeTypeRegexes, superType );
    }

    /**
     * Factory method.
     *
     * @param defaultValueLoader the ClassLoader through which the default value is loaded
     * @param defaultValueLoadFrom the location relative to loader from which we load
     * @param userSelectableMimeTypes MIME types offered to the user, the first one being the MIME type of the default value
     * @param mimeTypeRegexes the allowed MIME types
     * @param superType supertype of this DataType
     * @return the created BlobDataType
     */
    public static BlobDataType createByLoadingFrom(
            ClassLoader           defaultValueLoader,
            String                defaultValueLoadFrom,
            SelectableMimeType [] userSelectableMimeTypes,
            Pattern []            mimeTypeRegexes,
            DataType              superType )
    {
        return new BlobDataType( defaultValueLoader, defaultValueLoadFrom, userSelectableMimeTypes, mimeTypeRegexes, superType );
    }

    /**
     * Private constructor, use factory methods.
     *
     * @param defaultValue default value for instances of this DataType, expressed as String. May be null.
     * @param userSelectableMimeTypes MIME types offered to the user, the first one being the MIME type of the default value
     * @param mimeTypeRegexes the allowed MIME types
     * @param superType supertype of this DataType
     */
    private BlobDataType(
            String                defaultValue,
            SelectableMimeType [] userSelectableMimeTypes,
            Pattern []            mimeTypeRegexes,
            DataType              superType )
    {
        super( superType );

        if( mimeTypeRegexes == null || mimeTypeRegexes.length == 0 ) {
            throw new IllegalArgumentException( "Must support at least one MIME type" );
        }
        theMimeTypeRegexes         = mimeTypeRegexes;
        theDefaultValue            = createBlobValueOrNull( defaultValue, userSelectableMimeTypes[0].getMimeType() );
        theUserSelectableMimeTypes = userSelectableMimeTypes;
    }

    /**
     * Private constructor, use factory methods.
     *
     * @param defaultValue default value for instances of this DataType, expressed as a byte array. May be null.
     * @param userSelectableMimeTypes MIME types offered to the user, the first one being the MIME type of the default value
     * @param mimeTypeRegexes the allowed MIME types
     * @param superType supertype of this DataType
     */
    private BlobDataType(
            byte []               defaultValue,
            SelectableMimeType [] userSelectableMimeTypes,
            Pattern []            mimeTypeRegexes,
            DataType              superType )
    {
        super( superType );

        if( mimeTypeRegexes == null || mimeTypeRegexes.length == 0 ) {
            throw new IllegalArgumentException( "Must support at least one MIME type" );
        }
        theMimeTypeRegexes         = mimeTypeRegexes;
        theDefaultValue            = createBlobValueOrNull( defaultValue, userSelectableMimeTypes[0].getMimeType() );
        theUserSelectableMimeTypes = userSelectableMimeTypes;
    }

    /**
     * Private constructor, use factory methods.
     *
     * @param defaultValueLoader the ClassLoader through which the default value is loaded
     * @param defaultValueLoadFrom the location relative to loader from which we load
     * @param defaultValueMimeTypes default MIME types, the first one being the MIME type of the default value
     * @param mimeTypeRegexes the allowed MIME types
     * @param superType supertype of this DataType
     */
    private BlobDataType(
            ClassLoader           defaultValueLoader,
            String                defaultValueLoadFrom,
            SelectableMimeType [] defaultValueMimeTypes,
            Pattern []            mimeTypeRegexes,
            DataType              superType )
    {
        super( superType );

        if( mimeTypeRegexes == null || mimeTypeRegexes.length == 0 ) {
            throw new IllegalArgumentException( "Must support at least one MIME type" );
        }
        theMimeTypeRegexes         = mimeTypeRegexes;
        theDefaultValue            = createBlobValueByLoadingFrom( defaultValueLoader, defaultValueLoadFrom, defaultValueMimeTypes[0].getMimeType() );
        theUserSelectableMimeTypes = defaultValueMimeTypes;
    }

    /**
     * Convenience factory method to construct a BlobValue with MIME Type
     * text/plain.
     *
     * @param value the plain text
     * @return the created BlobValue
     */
    public BlobValue createPlainTextBlobValue(
            String value )
    {
        return createBlobValue( value, SelectableMimeType.TEXT_PLAIN.getMimeType() );
    }

    /**
     * Factory method to construct a BlobValue conforming to this DataType
     * with formatted text according to a certain MIME type in "abc/def" format.
     *
     * @param value the formatted text
     * @param mimeType the MIME type of the text, in "abc/def" format
     * @return the created BlobValue
     */
    public BlobValue createBlobValue(
            String value,
            String mimeType )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        if( mimeType == null ) {
            throw new IllegalArgumentException( "null mimeType" );
        }
        if( !isAllowedMimeType( mimeType )) {
            throw new MimeTypeNotInDomainException( this, mimeType );
        }

        return new BlobValue.StringBlob( this, value, mimeType );
    }

    /**
     * Factory method to construct a BlobValue conforming to this DataType
     * and according to a certain MIME type in "abc/def" format, or to return
     * null if the value argument is null.
     *
     * @param value the formatted text
     * @param mimeType the MIME type of the text, in "abc/def" format
     * @return the created BlobValue, or null
     */
    public BlobValue createBlobValueOrNull(
            String value,
            String mimeType )
    {
        if( value == null ) {
            return null;
        }
        if( mimeType == null ) {
            mimeType = SelectableMimeType.TEXT_PLAIN.getMimeType();
        }
        if( !isAllowedMimeType( mimeType )) {
            throw new MimeTypeNotInDomainException( this, mimeType );
        }

        return new BlobValue.StringBlob( this, value, mimeType );
    }

    /**
     * Factory method to construct a BlobValue conforming to this DataType
     * and the default generic MIME type.
     *
     * @param value the formatted text
     * @return the created BlobValue
     */
    public BlobValue createBlobValue(
            byte [] value )
    {
        return createBlobValue( value, null );
    }

    /**
     * Factory method to construct a BlobValue conforming to this DataType
     * and according to a certain MIME type in "abc/def" format.
     *
     * @param value the formatted text
     * @param mimeType the MIME type of the text, in "abc/def" format
     * @return the created BlobValue
     */
    public BlobValue createBlobValue(
            byte [] value,
            String  mimeType )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        if( mimeType == null ) {
            mimeType = SelectableMimeType.OCTET_STREAM.getMimeType();
        }
        if( !isAllowedMimeType( mimeType )) {
            throw new MimeTypeNotInDomainException( this, mimeType );
        }

        return new BlobValue.ByteBlob( this, value, mimeType );
    }

    /**
     * Factory method to construct a BlobValue conforming to this DataType
     * and according to a certain MIME type in "abc/def" format, or to return null
     * if the argument is null.
     *
     * @param value the formatted text
     * @param mimeType the MIME type of the text, in "abc/def" format
     * @return the created BlobValue, or null
     */
    public BlobValue createBlobValueOrNull(
            byte [] value,
            String  mimeType )
    {
        if( value == null ) {
            return null;
        }
        if( mimeType == null ) {
            mimeType = SelectableMimeType.OCTET_STREAM.getMimeType();
        }
        if( !isAllowedMimeType( mimeType )) {
            throw new MimeTypeNotInDomainException( this, mimeType );
        }

        return new BlobValue.ByteBlob( this, value, mimeType );
    }

    /**
     * Factory method to construct a BlobValue conforming to this DataType
     * by loading it from a certain path relative to a given ClassLoader.
     *
     * @param loader the ClassLoader through which this is loaded
     * @param loadFrom the location relative to loader from which we load
     * @param mimeType the MIME type of the BlobValue, in "abc/def" format
     * @return the created BlobValue
     */
    public BlobValue createBlobValueByLoadingFrom(
            ClassLoader loader,
            String      loadFrom,
            String      mimeType )
    {
        if( loadFrom == null ) {
            throw new IllegalArgumentException( "null loadFrom" );
        }
        if( mimeType == null ) {
            throw new IllegalArgumentException( "null mime type" );
        }
        if( !isAllowedMimeType( mimeType )) {
            throw new MimeTypeNotInDomainException( this, mimeType );
        }

        return new BlobValue.DelayedByteBlob( this, loader, loadFrom, mimeType );
    }

    /**
     * Factory method to construct a BlobValue conforming to this DataType
     * by loading it from a certain path relative to the default ClassLoader.
     *
     * @param loadFrom the location relative to the default ClassLoader
     * @param mimeType the MIME type of the BlobValue, in "abc/def" format
     * @return the created BlobValue
     */
    public BlobValue createBlobValueByLoadingFrom(
            String loadFrom,
            String mimeType )
    {
        if( !isAllowedMimeType( mimeType )) {
            throw new MimeTypeNotInDomainException( this, mimeType );
        }
        return new BlobValue.DelayedByteBlob( this, null, loadFrom, mimeType );
    }

    /**
     * Helper method to create an array of BlobValues from byte arrays.
     *
     * @param raw the array of byte arrays, or null
     * @param mimeType the MIME type of the byte arrays, in the same sequence
     * @return the corresponding array of raw, or null
     */
    public BlobValue [] createMultipleBlobValues(
            byte [][] raw,
            String    mimeType )
    {
        if( raw == null ) {
            return null;
        }
        BlobValue [] ret = new BlobValue[ raw.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            ret[i] = createBlobValue( raw[i], mimeType );
        }
        return ret;
    }

    /**
     * Determine the allowed MIME type regular expressions. If this is null, all
     * MIME types are allowed.
     *
     * @return the MIME types
     */
    public Pattern [] getMimeTypeRegexes()
    {
        return theMimeTypeRegexes;
    }

    /**
     * Determine whether this BlobDataType allows any kind of text MIME type.
     *
     * @return true if it allow a text MIME type
     */
    public boolean getAllowsTextMimeType()
    {
        for( SelectableMimeType current : theUserSelectableMimeTypes ) {

            String s = current.getMimeType();
            if( s.startsWith( "*/" )) {
                return true;
            }
            if( s.startsWith( "text/" )) {
                return true;
            }
        }
        return false;
    }

    /**
     * If this BlobDataType allows a text MIME type, return the default text
     * MIME type.
     *
     * @return the default text MIME type
     */
    public String getDefaultTextMimeType()
    {
        // Return the first text MIME type listed, and text/plain if */*

        for( SelectableMimeType current : theUserSelectableMimeTypes ) {
            String s = current.getMimeType();
            if( s.startsWith( "text/" )) {
                return s;
            }
        }
        for( SelectableMimeType current : theUserSelectableMimeTypes ) {
            String s = current.getMimeType();
            if( s.startsWith( "*/" )) {
                return SelectableMimeType.TEXT_PLAIN.getMimeType();
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof BlobDataType ) {
            BlobDataType realOther = (BlobDataType) other;

            if( !ArrayHelper.hasSameContentOutOfOrder( theMimeTypeRegexes, realOther.theMimeTypeRegexes, true )) {
                return false;
            }

            if( theDefaultValue == null ) {
                return realOther.theDefaultValue == null;
            }

            return theDefaultValue.equals( realOther.theDefaultValue );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        if( theDefaultValue != null ) {
            ret ^= theDefaultValue.hashCode();
        }
        for( int i=0 ; i<theMimeTypeRegexes.length ; ++i ) {
            ret ^= theMimeTypeRegexes[i].hashCode();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSupersetOfOrEquals(
            DataType other )
    {
        if( other instanceof BlobDataType ) {
            BlobDataType realOther = (BlobDataType) other;

            return ArrayHelper.firstHasSecondAsSubset( theMimeTypeRegexes, realOther.theMimeTypeRegexes, true );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        BlobValue realValue = (BlobValue) value;

        String mime = realValue.getMimeType();

        for( int i=0 ; i<theMimeTypeRegexes.length ; ++i ) {
            if( theMimeTypeRegexes[i].matcher( mime ).matches() ) {
                return 0;
            }
        }
        return Integer.MAX_VALUE;
    }

    /**
     * Determine whether a given MIME type is allowed.
     *
     * @param mime the MIME type to test
     * @return true if it is allowed
     */
    public boolean isAllowedMimeType(
            String mime )
    {
        for( int i=0 ; i<theMimeTypeRegexes.length ; ++i ) {
            if( theMimeTypeRegexes[i].matcher( mime ).matches() ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine whether this BlobDataType supports at least one text MIME type.
     *
     * @return true if it supports at least one text MIME type
     */
    public boolean supportsTextMimeType()
    {
        for( int i=0 ; i<theMimeTypeRegexes.length ; ++i ) {
            String regex = theMimeTypeRegexes[i].toString();
            if( regex.equals( ".*" )) {
                return true;
            }
            if( regex.startsWith( "\\Qtext/" )) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine whether this BlobDataType supports at least non non-text MIME type.
     *
     * @return true if it supports at least one MIME type that is not text
     */
    public boolean supportsBinaryMimeType()
    {
        for( int i=0 ; i<theMimeTypeRegexes.length ; ++i ) {
            String regex = theMimeTypeRegexes[i].toString();
            if( regex.equals( ".*" )) {
                return true;
            }
            if( !regex.startsWith( "\\Qtext/" )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getPerformDomainCheck()
    {
        // the old plan:
        // if we have a mime type, we'd like to do a domain check
        // the new plan:
        return true;

        // Reasoning: if we have a PropertyType (eg Content) which
        // accepts any MIME type, but a subtype accepts only a subset (eg
        // HTMLSection) we'd get an IllegalValueException in the subtype, but
        // not in the supertype, and the compiler won't like that ...
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaDomainCheckExpression(
            String varName )
    {
        StringBuilder almostRet = new StringBuilder( 100 );
        almostRet.append( "(" );
        for( int i=0 ; i<theMimeTypeRegexes.length ; ++i ) {
            if( i!=0 ) {
                almostRet.append( " || " );
            }
            almostRet.append( theMimeTypeRegexes[i] );
            almostRet.append( ".matcher( " );
            almostRet.append( varName );
            almostRet.append( " ).matches() )" );
        }
        almostRet.append( CLOSE_PARENTHESIS_STRING );
        return almostRet.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<BlobValue> getCorrespondingJavaClass()
    {
        return BlobValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BlobValue getDefaultValue()
    {
        return theDefaultValue;
    }

    /**
     * Obtain all MIME types of this BlobDataType that are shown to the user
     * in places such as a selectable list.
     *
     * @return the user-selectable MIME types.
     */
    public SelectableMimeType [] getUserSelectableMimeTypes()
    {
        return theUserSelectableMimeTypes;
    }

    /**
     * Obtain the default MIME type of this BlobDataType.
     *
     * @return the default MIME type.
     */
    public String getDefaultMimeType()
    {
        return theUserSelectableMimeTypes[0].getMimeType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return  "BlobDataType: with"
                + ((theDefaultValue == null ) ? "out" : "" )
                + " default, mime: "
                + ( (theMimeTypeRegexes.length == 1 )
                        ? theMimeTypeRegexes[0]
                        : ArrayHelper.arrayToString( theMimeTypeRegexes, "," ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else if( this.equals( theAnyType )) {
            return theAnyType;
        } else if( this.equals( theAnyKnownType )) {
            return theAnyKnownType;
        } else if( this.equals( theGifType )) {
            return theGifType;
        } else if( this.equals( theJpgType )) {
            return theJpgType;
        } else if( this.equals( thePngType )) {
            return thePngType;
        } else if( this.equals( theJdkSupportedBitmapType )) {
            return theJdkSupportedBitmapType;
        } else if( this.equals( theTextAnyType )) {
            return theTextAnyType;
        } else if( this.equals( theTextHtmlType )) {
            return theTextHtmlType;
        } else if( this.equals( theTextPlainType )) {
            return theTextPlainType;
        } else if( this.equals( theTextXmlType )) {
            return theTextXmlType;
        } else if( this.equals( theTextJsonType )) {
            return theTextJsonType;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        if( this == theAnyType ) {
            return className + ".theAnyType";
        } else if( this == theDefault ) {
            return className + DEFAULT_STRING;
        } else if( this == theAnyKnownType ) {
            return className + ".theAnyKnownType";
        } else if( this == theGifType ) {
            return className + ".theGifType";
        } else if( this == theJpgType ) {
            return className + ".theJpgType";
        } else if( this == thePngType ) {
            return className + ".thePngType";
        } else if( this == theJdkSupportedBitmapType ) {
            return className + ".theJdkSupportedBitmapType";
        } else if( this == theTextAnyType ) {
            return className + ".theTextAnyType";
        } else if( this == theTextHtmlType ) {
            return className + ".theTextHtmlType";
        } else if( this == theTextPlainType ) {
            return className + ".theTextPlainType";
        } else if( this == theTextXmlType ) {
            return className + ".theTextXmlType";
        } else if( this == theTextJsonType ) {
            return className + ".theTextJsonType";
        } else if( this == theFaviconType ) {
            return className + ".theFaviconType";
        } else {
            StringBuilder ret = new StringBuilder( className );
            ret.append( CREATE_STRING );
            if( theDefaultValue != null ) {
                ret.append( theDefaultValue.getJavaConstructorString( classLoaderVar, null )); // null is okay here
            } else {
                ret.append( NULL_STRING );
            }
            ret.append( COMMA_STRING );

            if( theMimeTypeRegexes != null ) {
                ret.append( "new java.util.Pattern[] { " );
                for( int i=0 ; i<theMimeTypeRegexes.length ; ++i ) {
                    ret.append( "new java.util.Pattern( " );
                    ret.append( QUOTE_STRING );
                    ret.append( StringHelper.stringToJavaString( theMimeTypeRegexes[i].toString() ));
                    ret.append( QUOTE_STRING );
                    ret.append( " )" );

                    if( i<theMimeTypeRegexes.length-1 ) {
                        ret.append( COMMA_STRING );
                    }
                }
                ret.append( " }" );
            } else {
                ret.append( NULL_STRING );
            }
            ret.append( COMMA_STRING );

            if( theSupertype != null ) {
                ret.append( theSupertype.getJavaConstructorString( classLoaderVar ));
            } else {
                ret.append( NULL_STRING );
            }
            ret.append( " )" );

            return ret.toString();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BlobValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        if( mime == null ) {
            mime = theUserSelectableMimeTypes[0].getMimeType();
        }
        String STRING_TAG = "string:";
        String MIME_TAG   = "mime:";
        String LOADER_TAG = "loader:";
        String BYTES_TAG  = "bytes:";

        BlobValue ret;

        if( raw.startsWith( STRING_TAG )) {
            if( mime == null ) {
                mime = SelectableMimeType.TEXT_PLAIN.getMimeType();
            }
            ret = createBlobValue( raw.substring( STRING_TAG.length() ), mime );

        } else if( raw.startsWith( MIME_TAG )) {
            String raw2  = raw.substring( MIME_TAG.length() );
            int    blank = raw2.indexOf( ' ' );
            String mime2 = raw2.substring( 0, blank ).trim();

            if( !mime2.isBlank()) {
                mime = mime2;
            } else if( mime == null ) {
                mime = theUserSelectableMimeTypes[0].getMimeType();
            }

            if( raw2.regionMatches( blank+1, LOADER_TAG, 0, LOADER_TAG.length() )) {
                ret = createBlobValueByLoadingFrom(
                        getClass().getClassLoader(),
                        raw2.substring( blank+1+LOADER_TAG.length() ),
                        mime );

            } else if( raw2.regionMatches( blank+1, BYTES_TAG, 0, BYTES_TAG.length() )) {
                byte [] bytes = Base64.getDecoder().decode( raw2.substring( blank+1+BYTES_TAG.length() ) );
                ret = createBlobValue(
                        bytes,
                        mime );

            } else {
                ret = createBlobValue( raw, mime );
            }

        } else {
            if( mime == null ) {
                mime = theUserSelectableMimeTypes[0].getMimeType();
            }
            ret = createBlobValue( raw, mime );
        }
        return ret;
    }

    /**
     * The default value that goes with this DataType.
     */
    protected BlobValue theDefaultValue;

    /**
     * The MIME types that goe with this DataType that are shown to the user
     * in places such as a selectable list when creating a new BlobValue.
     * This all of these must obviously be matched by at least one of the
     * Pattersn in theMimeTypeRegexes, but the list may not be exhaustive.
     * For example, BlobDataType that allows any MIME type will have a
     * regex such as ".+", but obviously only a limited number of choices can
     * be offered to the user in a pre-defined list. Those are held here.
     */
    protected SelectableMimeType [] theUserSelectableMimeTypes;

    /**
     * The set of MIME types allowed for BlobValues using this DataType.
     * This must not be null. If any one of the regexes matches, the MIME type is
     * permitted.
     */
    protected Pattern [] theMimeTypeRegexes;
}
