//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.io.ObjectStreamException;
import java.text.ParseException;
import net.ubos.util.logging.Dumper;

/**
  * This is a time stamp DataType for PropertyValues. A time stamp is a point in time.
  */
public final class TimeStampDataType
        extends
            DataType
{
    /**
      * This is the default instance of this class.
      */
    public static final TimeStampDataType theDefault = new TimeStampDataType();

    /**
     * Factory method. Always returns the same instance.
     *
     * @return the default instance of this class
     */
    public static TimeStampDataType create()
    {
        return theDefault;
    }

    /**
      * Private constructor. There is no reason to instantiate this more than once.
      */
    private TimeStampDataType()
    {
        super( null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other instanceof TimeStampDataType ) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return TimeStampDataType.class.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int conforms(
            PropertyValue value )
        throws
            ClassCastException
    {
        TimeStampValue realValue = (TimeStampValue) value; // may throw

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<TimeStampValue> getCorrespondingJavaClass()
    {
        return TimeStampValue.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeStampValue getDefaultValue()
    {
        return TimeStampValue.now();
    }

    /**
     * Correctly deserialize a static instance.
     *
     * @return the static instance if appropriate
     * @throws ObjectStreamException thrown if reading from the stream failed
     */
    public Object readResolve()
        throws
            ObjectStreamException
    {
        if( this.equals( theDefault )) {
            return theDefault;
        } else {
            return this;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar )
    {
        final String className = getClass().getName();

        return className + DEFAULT_STRING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeStampValue parse(
            String       raw,
            String       mime,
            PropertyType pt )
        throws
            PropertyValueParsingException
    {
        if( "NOW".equalsIgnoreCase( raw )) {
            return TimeStampValue.now();
        }
        try {
            TimeStampValue ret = TimeStampValue.createFromRfc3339( raw );
            return ret;

        } catch( ParseException ex ) {
            throw new PropertyValueParsingException( this, raw );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[0],
                new Object[0] );
    }

    /**
     * Entry into the StringRepresentation that represents the current time.
     */
    public static final String NOW_ENTRY = "Now";
}
