//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import java.text.ParseException;
import net.ubos.mesh.MeshObjectIdentifier;

/**
 * A MeshObjectIdentifierBothSerializer that delegates to other MeshObjectIdentifierBothSerializers.
 * For serialization, it uses the first delegate. For deserialization, it tries one after the other
 * until one works.
 */
public class DelegatingMeshObjectIdentifierBothSerializer
    implements
        MeshObjectIdentifierBothSerializer
{
    /**
     * Factory method
     *
     * @param delegates the delegates, in sequence
     * @return the created DelegatingMeshObjectIdentifierBothSerializer
     */
    public static DelegatingMeshObjectIdentifierBothSerializer create(
            MeshObjectIdentifierBothSerializer ... delegates)
    {
        if( delegates.length == 0 ) {
            throw new IllegalArgumentException( "Need at least one delegate" );
        }
        return new DelegatingMeshObjectIdentifierBothSerializer( delegates );
    }

    /**
     * Constructor.
     *
     * @param delegates the delegates, in sequence
     */
    protected DelegatingMeshObjectIdentifierBothSerializer(
            MeshObjectIdentifierBothSerializer [] delegates )
    {
        theDelegates = delegates;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toExternalForm(
            MeshObjectIdentifier identifier )
    {
        String ret = theDelegates[0].toExternalForm( identifier );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            String externalForm )
        throws
            ParseException
    {
        return meshObjectIdentifierFromExternalForm( null, externalForm );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            MeshObjectIdentifier contextIdentifier,
            String               externalForm )
        throws
            ParseException
    {
        MeshObjectIdentifier ret;

        for( int i=0 ; i<theDelegates.length ; ++i ) {
            try {
                ret = theDelegates[i].meshObjectIdentifierFromExternalForm( contextIdentifier, externalForm );
                return ret;
            } catch( ParseException ex ) {
                // no op
            }
        }
        throw new ParseException( "None of the available methods could parse this identifier", 0 );
    }

    /**
     * The delegates.
     */
    protected final MeshObjectIdentifierBothSerializer [] theDelegates;
}
