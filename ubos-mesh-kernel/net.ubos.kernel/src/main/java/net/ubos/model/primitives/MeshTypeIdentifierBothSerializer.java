//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * Knows how to do both.
 */
public interface MeshTypeIdentifierBothSerializer
    extends
        MeshTypeIdentifierSerializer,
        MeshTypeIdentifierDeserializer
{}
