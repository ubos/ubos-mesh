//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

/**
 * Supported by objects to know how to both serialize and deserialize MeshObjectIdentifiers.
 */
public interface MeshObjectIdentifierBothSerializer
    extends
        MeshObjectIdentifierSerializer,
        MeshObjectIdentifierDeserializer
{}
