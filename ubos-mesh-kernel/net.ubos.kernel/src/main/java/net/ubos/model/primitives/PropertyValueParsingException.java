//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.exception.LocalizedException;

/**
 * Thrown if parsing the String representation of a PropertyValue was unsuccessful.
 * This Exception delegates to the cause if there is one and it can be localized.
 */
public class PropertyValueParsingException
        extends
            AbstractLocalizedException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param type the DataType to which the PropertyValue was supposed to conform
     * @param s the String whose parsing was unsuccessful
     */
    public PropertyValueParsingException(
            DataType             type,
            String               s )
    {
        this( type, s, (String) null );
    }

    /**
     * Constructor.
     *
     * @param type the DataType to which the PropertyValue was supposed to conform
     * @param s the String whose parsing was unsuccessful
     * @param formatString the format String used, if any
     */
    public PropertyValueParsingException(
            DataType             type,
            String               s,
            String               formatString )
    {
        theDataType       = type;
        theString         = s;
        theFormatString   = formatString;
    }
    
    /**
     * Constructor.
     *
     * @param type the DataType to which the PropertyValue was supposed to conform
     * @param s the String whose parsing was unsuccessful
     * @param cause the underlying exception
     */
    public PropertyValueParsingException(
            DataType             type,
            String               s,
            Throwable            cause )
    {
        this( type, s, null, cause );
    }

    /**
     * Constructor.
     *
     * @param type the DataType to which the PropertyValue was supposed to conform
     * @param s the String whose parsing was unsuccessful
     * @param formatString the format String used, if any
     * @param cause the underlying exception
     */
    public PropertyValueParsingException(
            DataType             type,
            String               s,
            String               formatString,
            Throwable            cause )
    {
        super( cause );

        theDataType       = type;
        theString         = s;
        theFormatString   = formatString;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        Throwable cause = getCause();
        if( cause instanceof LocalizedException ) {
            return ((LocalizedException)cause).getLocalizedMessage();
        } else {
            return super.getLocalizedMessage();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theDataType, theString, theFormatString };
    }

    /**
     * Obtain the DataType into one of those instances the String was supposed to be parsed.
     *
     * @return the DataType
     */
    public final DataType getDataType()
    {
        return theDataType;
    }

    /**
     * Obtain the String that failed to parse.
     *
     * @return the String
     */
    public String getString()
    {
        return theString;
    }

    /**
     * Obtain the format String used for parsing.
     *
     * @return format String
     */
    public String getFormatString()
    {
        return theFormatString;
    }

    /**
     * The DataType into one of whose instances the String was supposed to be parsed.
     */
    protected DataType theDataType;
    
    /**
     * The String that failed to parse.
     */
    protected String theString;

    /**
     * The format String applied for the parsing.
     */
    protected String theFormatString;
    
    /**
     * Key, into the resource file, for the message in case we have a format string.
     */
    public static final String WITH_FORMAT_STRING_KEY = "WithFormatString";

    /**
     * Key, into the resource file, for the message in case we do not have a format string.
     */
    public static final String WITHOUT_FORMAT_STRING_KEY = "WithoutFormatString";
}
