//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * Serializer for MeshTypeIdentifiers
 */
public interface MeshTypeIdentifierSerializer
{
    /**
     * Convert the Identifier to String form.
     *
     * @param identifier the Identifier to serialize
     * @return String form
     */
    public String toExternalForm(
            MeshTypeIdentifier identifier );
}
