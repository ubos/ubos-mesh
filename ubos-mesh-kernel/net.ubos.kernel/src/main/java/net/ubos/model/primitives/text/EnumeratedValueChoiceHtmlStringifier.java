//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies the domain of an EnumeratedValue's EnumeratedDataType, highlighting the EnumeratedValue.
 */
public class EnumeratedValueChoiceHtmlStringifier
        extends
            AbstractStringifier<EnumeratedValue>
{
    /**
     * Factory method.
     *
     * @param tag the HTML tag to use
     * @return the created EnumeratedValueStringifier
     */
    public static EnumeratedValueChoiceHtmlStringifier create(
            String tag )
    {
        return new EnumeratedValueChoiceHtmlStringifier( tag );
    }

    /**
     * Private constructor for subclasses only, use factory method.
     *
     * @param tag the HTML tag to use
     */
    protected EnumeratedValueChoiceHtmlStringifier(
            String tag )
    {
        theTag = tag;
    }

    /**
     * Obtain the HTML tag.
     *
     * @return the HTML tag
     */
    public String getTag()
    {
        return theTag;
    }

    /**
     * Format an Object using this Stringifier.
     *
     * @param soFar the String so far, if any
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the formatted String
     */
    @Override
    public String format(
            EnumeratedValue       arg,
            StringifierParameters pars,
            String                soFar )
    {
        EnumeratedValue [] values = arg.getDataType().getDomain();
        StringBuilder      ret    = new StringBuilder();

        for( int i=0 ; i<values.length ; ++i ) {
            String name        = values[i].value();
            String userVisible = values[i].getUserVisibleName().value();

            ret.append( "<" ).append( theTag );
            ret.append( " value=\"" ).append( name );

            if( arg == values[i] ) {
                ret.append( "\" selected=\"selected" );
            }
            ret.append( "\">" );
            ret.append( userVisible );
            ret.append( "</" ).append( theTag ).append( ">\n" );

        }
        return potentiallyShorten( ret.toString(), pars );
    }

    /**
     * Format an Object using this Stringifier. This may be null.
     *
     * @param soFar the String so far, if any
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the formatted String
     * @throws ClassCastException thrown if this Stringifier could not format the provided Object
     *         because the provided Object was not of a type supported by this Stringifier
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg instanceof EnumeratedValue ) {
            return format( (EnumeratedValue) arg, pars, soFar );
        } else if( arg instanceof EnumeratedDataType ) {
            return format( ((EnumeratedDataType) arg).getDefaultValue(), pars, soFar );
        } else {
            throw new ClassCastException( "Cannot cast this object to a supported type: " + arg );
        }
    }

    /**
     * The HTML tag to use.
     */
    protected String theTag;
}
