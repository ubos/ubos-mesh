//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.util.function.BiPredicate;

/**
 * Comparison predicates for PropertyValues.
 */
public enum PropertyComparisonPredicate
    implements
        BiPredicate<PropertyValue, PropertyValue>
{
    /**
     * Test whether the values are equal.
     */
    EQUAL {
        @Override
        public boolean test(
                PropertyValue one,
                PropertyValue two )
        {
            int found = PropertyValue.compare( one, two );
            return found == 0;
        }
    },

    /**
     * Test whether the values are not equal.
     */
    NON_EQUAL {
        @Override
        public boolean test(
                PropertyValue one,
                PropertyValue two )
        {
            int found = PropertyValue.compare( one, two );
            return found != 0;
        }
    },

    /**
     * Test whether the first value is greater than the second value.
     */
    GREATER {
        @Override
        public boolean test(
                PropertyValue one,
                PropertyValue two )
        {
            int found = PropertyValue.compare( one, two );
            return found > 0;
        }
    },

    /**
     * Test whether the first value is smaller than the second value.
     */
    SMALLER {
        @Override
        public boolean test(
                PropertyValue one,
                PropertyValue two )
        {
            int found = PropertyValue.compare( one, two );
            return found < 0;
        }
    },

    /**
     * Test whether the first value is greater than or equal to the second value.
     */
    GREATER_OR_EQUALS {
        @Override
        public boolean test(
                PropertyValue one,
                PropertyValue two )
        {
            int found = PropertyValue.compare( one, two );
            return found >= 0;
        }
    },

    /**
     * Test whether the first value is smaller than or equal to the second value.
     */
    SMALLER_OR_EQUALS {
        @Override
        public boolean test(
                PropertyValue one,
                PropertyValue two )
        {
            int found = PropertyValue.compare( one, two );
            return found <= 0;
        }
    };
}
