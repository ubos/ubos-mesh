//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.json;

import net.ubos.model.primitives.externalized.PropertyValueDecoder;

/**
 * Separate class to avoid coding mistakes.
 */
public final class PropertyValueJsonDecoder
    extends
        AbstractPropertyValueJsonDecoder
    implements
        PropertyValueDecoder
{
    /**
     * Factory method.
     *
     * @return the created decoder
     */
    public static PropertyValueJsonDecoder create()
    {
        return new PropertyValueJsonDecoder();
    }

    /**
     * Constructor. Use factory method.
     */
    protected PropertyValueJsonDecoder() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncodingId()
    {
        return PROPERTY_VALUE_JSON_ENCODING_ID;
    }
}
