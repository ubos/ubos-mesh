//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.xml;

import java.io.IOException;
import java.io.Reader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.externalized.AbstractPropertyValueDecoder;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.util.logging.Log;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Knows how to decode a PropertyValue from XML. Implements the SAX interface.
 */
public abstract class AbstractPropertyValueXmlDecoder
    extends
        AbstractPropertyValueDecoder
    implements
        PropertyValueXmlTags
{
    private static final Log log = Log.getLogInstance( AbstractPropertyValueXmlDecoder.class ); // our own, private logger

    /**
     * Parse a PropertyValue.
     *
     * @param type the DataType for which this will be a value
     * @param r where to read from
     * @return the PropertyValue
     * @throws DecodingException thrown if decoding failed
     * @throws IOException an I/O problem occurred
     */
    public PropertyValue parsePropertyValue(
            DataType type,
            Reader   r )
        throws
            DecodingException,
            IOException
    {
        PropertyValueXmlDecoderHandler myHandler = new PropertyValueXmlDecoderHandler() {
            {
                theDataType = type;
            }
        };
        myHandler.setParent( this );

        try {
            theParser.parse( new InputSource( r ), myHandler );

            return myHandler.thePropertyValue;

        } catch( SAXException ex ) {
            throw new DecodingException.Syntax( ex );
        }
    }

    /**
     * Parse an XML DOM attribute into a long, or use default.
     *
     * @param attrs the Attributes on the XML DOM node
     * @param attName name of the Attribute
     * @param defaultValue
     * @return the value
     */
    protected static long parseLong(
            Attributes attrs,
            String     attName,
            long       defaultValue )
    {
        String attValue = attrs.getValue( attName );
        return parseLong( attValue, defaultValue );
    }

    /**
     * Parse an String into a long, or use default.
     *
     * @param s the String
     * @param defaultValue
     * @return the value
     */
    protected static long parseLong(
            String     s,
            long       defaultValue )
    {
        if( s == null || s.length() == 0 ) {
            return defaultValue;
        }
        long ret = Long.parseLong( s );
        return ret;
    }

    /**
     * Parse an XML DOM attribute into an int, or use default.
     *
     * @param attrs the Attributes on the XML DOM node
     * @param attName name of the Attribute
     * @param defaultValue
     * @return the value
     */
    protected static int parseInt(
            Attributes attrs,
            String     attName,
            int        defaultValue )
    {
        String attValue = attrs.getValue( attName );
        return parseInt( attValue, defaultValue );
    }

    /**
     * Parse an String into a int, or use default.
     *
     * @param s the String
     * @param defaultValue
     * @return the value
     */
    protected static int parseInt(
            String     s,
            int        defaultValue )
    {
        if( s == null || s.length() == 0 ) {
            return defaultValue;
        }
        int ret = Integer.parseInt( s );
        return ret;
    }

    /**
     * The SAX parser factory to use.
     */
    protected static final SAXParserFactory theSaxParserFactory;
    static {
        theSaxParserFactory = SAXParserFactory.newInstance();
        theSaxParserFactory.setValidating( true );
    }

    /**
     * Helper method to catch exception when creating a SAX parser.
     *
     * @return the SAXParser
     */
    static SAXParser createSaxParser()
    {
        try {
            return theSaxParserFactory.newSAXParser();

        } catch( Throwable t ) {
            log.error( t );

            return null;
        }
    }

    /**
     * Our SAX parser. All calls to it must be synchronized to it.
     */
    protected final SAXParser theParser = createSaxParser();
}
