//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.util;

import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.CollectableMeshTypeNotFoundException;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;

import java.util.HashMap;

/**
 * Simple implementation of SubjectAreaTagMap.
 */
public class SimpleSubjectAreaTagMap
        implements
            SubjectAreaTagMap
{
    /**
     * Factory method.
     *
     * @return the created SimpleSubjectAreaTagMap
     */
    public static SimpleSubjectAreaTagMap create()
    {
        return new SimpleSubjectAreaTagMap( null );
    }

    /**
     * Factory method with a delegate map for failed lookups
     *
     * @param delegateMap the delegate map
     * @return the created SimpleSubjectAreaTagMap
     */
    public static SimpleSubjectAreaTagMap create(
            SubjectAreaTagMap delegateMap )
    {
        return new SimpleSubjectAreaTagMap( delegateMap );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param delegateMap the delegate map
     */
    protected SimpleSubjectAreaTagMap(
            SubjectAreaTagMap delegateMap )
    {
        theDelegateMap = delegateMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void put(
            String      prefix,
            SubjectArea sa )
        throws
            TagConflictException
    {
        SubjectArea oldSa = theForwardMap.put( prefix, sa );
        if( oldSa != null ) {
            throw new TagConflictException( prefix, sa, oldSa );
        }
        theBackwardMap.put( sa, prefix ); // no need to check for conflicts here
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        return theForwardMap.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubjectArea getSubjectAreaFor(
            String prefix )
    {
        SubjectArea ret = theForwardMap.get( prefix );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTagFor(
            SubjectArea sa )
    {
        String ret = theBackwardMap.get( sa );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String obtainTagFor(
            SubjectArea sa )
    {
        String ret = theBackwardMap.get( sa );
        if( ret == null ) {
            ret = constructNewPrefix();
            theBackwardMap.put( sa, ret );
            theForwardMap.put( ret, sa );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> tagIterator()
    {
        return MapCursorIterator.createForKeys( theForwardMap );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshType(
            String identifier )
        throws
           CollectableMeshTypeNotFoundException
    {
        int sep = identifier.indexOf( SEPARATOR );
        if( sep <= 0 ) {
            return null;
        }
        SubjectArea sa = getSubjectAreaFor( identifier.substring( 0, sep ));
        if( sa == null ) {
            return null;
        }
        return sa.findCollectableMeshTypeByName( identifier.substring( sep+1 ) );
    }

    /**
     * Helper to construct an as-yet unused prefix.
     *
     * @return the unused prefix
     */
    protected String constructNewPrefix()
    {
        // we are counting in hex, that seems easy

        for( int i=0 ; i<26*26 ; ++i ) { // gotta stop at some reasonable number

            String candidate = new String( new char[] { (char) ((i/26) + 'a' ), (char) ((i%26) + 'a' ) } );
            if( theForwardMap.get( candidate ) == null ) {
                return candidate;
            }
        }
        throw new RuntimeException( "Ran out of prefixes" );
    }

    /**
     * Delegate map for fallbacks.
     */
    protected SubjectAreaTagMap theDelegateMap;

    /**
     * Maps in one direction.
     */
    protected HashMap<String,SubjectArea> theForwardMap = new HashMap<>();

    /**
     * Maps in the other direction.
     */
    protected HashMap<SubjectArea,String> theBackwardMap = new HashMap<>();

    /**
     * Separates the tag for the SubjectArea from the rest of the identifier.
     */
    public static char SEPARATOR = ':';
}
