//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import java.awt.geom.Point2D;
import java.text.ParseException;

/**
  * This is a point-in-space value for PropertyValues.
  */
public final class PointValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method.
     *
     * @param value the value as Point2D
     * @return the created PointValue
     * @throws IllegalArgumentException if null value is given
     */
    public static PointValue create(
            Point2D value )
    {
        if( value == null ) {
            throw new IllegalArgumentException( "null value" );
        }
        return new PointValue( value.getX(), value.getY() );
    }

    /**
     * Factory method, or return null if the argument is null.
     *
     * @param value the value as Point2D
     * @return the created PointValue, or null
     * @throws IllegalArgumentException if null value is given
     */
    public static PointValue createOrNull(
            Point2D value )
    {
        if( value == null ) {
            return null;
        }
        return new PointValue( value.getX(), value.getY() );
    }

    /**
     * Factory method.
     *
     * @param xx xx dimension of the point
     * @param yy yy dimension of the point
     * @return the created PointValue
     */
    public static PointValue create(
            double xx,
            double yy )
    {
        return new PointValue( xx, yy );
    }

    /**
      * Private constructor.
      *
      * @param xx xx dimension of the point
      * @param yy yy dimension of the point
      */
    private PointValue(
            double xx,
            double yy )
    {
        this.x = xx;
        this.y = yy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Point2D value()
    {
        return new Point2D.Double( x, y );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Point2D getValue()
    {
        return value();
    }

    /**
     * Determine X coordinate.
     *
     * @return the X coordinate
     */
    public double getX()
    {
        return x;
    }

    /**
     * Determine Y coordinate.
     *
     * @return the Y coordinate
     */
    public double getY()
    {
        return y;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointDataType getDataType()
    {
        return PointDataType.theDefault;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof PointValue ) {
            return (x == ((PointValue)otherValue).x)
                && (y == ((PointValue)otherValue).y);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return (int)(( x + y ) * 1024 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "(" + x + ";" + y + ")";
    }

    /**
      * This attempts to parse a string and turn it into a PointValue value similarly
 to Integer.parseInt().
      *
      * @param s the string that shall be parsed
      * @return the created PointValue
      * @throws ParseException thrown if theString does not follow the correct syntax
      */
    public static PointValue parsePointValue(
            String s )
        throws
            ParseException
    {
        s = s.trim();
        if( s.startsWith( "(" ) && s.endsWith( ")" )) {
            int semi = s.indexOf( ';', 1 );
            if( semi > 1 ) {
                double x = Double.parseDouble( s.substring( 1, semi ));
                double y = Double.parseDouble( s.substring( semi+1, s.length()-1 ));

                return new PointValue( x, y );
            }
        }
        throw new ParseException( s, 0 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        StringBuilder buf = new StringBuilder( 64 );
        buf.append( getClass().getName());
        buf.append( DataType.CREATE_STRING );
        buf.append( x );
        buf.append( ", " );
        buf.append( y );
        buf.append( DataType.CLOSE_PARENTHESIS_STRING );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        PointValue realOther = (PointValue) o;

        if( x < realOther.x ) {
            if( y < realOther.y ) {
                return -1;
            } else {
                return +2; // not comparable convention: +2
            }
        } else if( x == realOther.x ) {
            if( y == realOther.y ) {
                return 0;
            } else {
               return +2; // not comparable convention: +2
            }
        } else {
            if( y > realOther.y ) {
                return +1;
            } else {
                return +2; // not comparable convention: +2
            }
        }
    }

    /**
      * The real value for the x component.
      */
    protected double x;

    /**
      * The real value for the y component.
      */
    protected double y;
}
