//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MultiplicityException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier.RoleTypeDirection;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.ArrayHelper;
import net.ubos.util.ResourceHelper;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;
import net.ubos.modelbase.m.MMeshTypeIdentifier;

/**
  * The RoleType played by an EntityType in a RelationshipType (between another EntityType and itself).
  * In-memory implementation.
  */
public abstract class MRoleType
        extends
            MMeshTypeWithProperties
        implements
            RoleType
{
    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     * @param relationship the RelationshipType to which this RoleType will belong
     * @param entity the EntityType related to this RoleType
     * @param multiplicity the multiplicity of this RoleType
     */
    protected MRoleType(
            MMeshTypeIdentifier identifier,
            MRelationshipType   relationship,
            MEntityType         entity,
            MultiplicityValue   multiplicity )
    {
        super( identifier );

        theRelationshipType = relationship;
        theEntityType       = entity;
        theMultiplicity     = multiplicity;

        if( theEntityType != null ) {
            theEntityType.addLocalRoleType( this );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final RelationshipType getRelationshipType()
    {
        return theRelationshipType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final EntityType getEntityType()
    {
        return theEntityType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final RoleType getInverseRoleType()
    {
        if( theRelationshipType.getSource() == this ) {
            return theRelationshipType.getDestination();
        } else {
            return theRelationshipType.getSource();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType getOtherEntityType()
    {
        return getInverseRoleType().getEntityType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MultiplicityValue getMultiplicity()
    {
        return theMultiplicity;
    }

    /**
     * Use this TraversalSpecification to traverse from the passed-in start MeshObject
     * to related MeshObjects. This method is defined on TraversalSpecification, so
     * different implementations of TraversalSpecification can implement different ways
     * of doing this.
     *
     * @param start the start MeshObject for the traversal
     * @return the result of the traversal
     */
    @Override
    public final MeshObjectSet traverse(
            MeshObject start )
    {
        return start.traverse( this );
    }

    /**
      * Use this TraversalSpecification to traverse from the passed-in start MeshObjectSet
      * to related MeshObjects. This method is defined on TraversalSpecification, so
      * different implementations of TraversalSpecification can implement different ways
      * of doing this.
      *
      * @param theSet the start MeshObjectSet for the traversal
      * @return the result of the traversal
      */
    @Override
    public final MeshObjectSet traverse(
            MeshObjectSet theSet )
    {
        return theSet.traverse( this );
    }

    /**
     * Determine whether a given event, with a source of from where we traverse the
     * TraversalSpecification, may affect the result of the traversal.
     *
     * @param toResolveAgainst the MeshBase to resolve the event against
     * @param theEvent the event that we consider
     * @return true if this event may affect the result of traversing from the Entity
     *         that sent this event
     */
    @Override
    public final boolean isAffectedBy(
            MeshBase                  toResolveAgainst,
            AbstractMeshObjectRoleTypeChange theEvent )
    {
        RoleType [] firedRoles = theEvent.getDeltaValue();

        for( RoleType current : firedRoles ) {
            if( current.isSubtypeOfOrEquals( this )) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public String getUserVisibleString()
    {
        StringValue almost = getUserVisibleName();
        if( almost != null ) {
            return almost.value();
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkMultiplicity(
            MeshObject    affected,
            MeshObject [] others )
        throws
            MultiplicityException
    {
        if( theMultiplicity.getMinimum() > others.length ) {
            throw new MultiplicityException( affected, this, others );
        }
        if( theMultiplicity.getMaximum() != MultiplicityValue.N && others.length > theMultiplicity.getMaximum() ) {
            throw new MultiplicityException( affected, this, others );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MRoleType [] getDirectSupertypes()
    {
        return (MRoleType []) super.getDirectSupertypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MRoleType [] getSupertypes()
    {
        return (MRoleType []) super.getSupertypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MRoleType [] getDirectSubtypes()
    {
        return (MRoleType []) super.getDirectSubtypes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MRoleType [] getSubtypes()
    {
        return (MRoleType []) super.getSubtypes();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public final void setDirectSupertypes(
            MMeshTypeWithProperties [] supertypes )
    {
        if( this.theSupertypes.length != 0 ) {
            throw new IllegalArgumentException();
        }
        if( supertypes.getClass().getComponentType() != MRoleType.class ) {
            throw new IllegalArgumentException();
        }

        this.theSupertypes = supertypes;

        zeroCaches();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public final void addDirectSubtype(
            MeshTypeWithProperties subtype )
    {
        if( ! MRoleType.class.isInstance( subtype ) ) {
            throw new IllegalArgumentException();
        }
        for( int i=0 ; i<theSubtypes.length ; ++i ) {
            if( theSubtypes[i].equals( subtype )) {
                return;
            }
        }

        theSubtypes = ArrayHelper.append(
                (MRoleType []) theSubtypes,
                (MRoleType) subtype,
                MRoleType.class );

        zeroCaches();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder almostRet = new StringBuilder( "<" );
        almostRet.append( super.toString() );
        almostRet.append( "{ for " );
        almostRet.append( theRelationshipType.getIdentifier() );
        almostRet.append( " }>" );
        return almostRet.toString();
    }

    /**
      * The RelationshipType that this RoleType belongs to.
      */
    private final MRelationshipType theRelationshipType;

    /**
      * The EntityType associated with this MetaRole.
      */
    private final MEntityType theEntityType;

    /**
      * The value of the source multiplicity.
      */
    private final MultiplicityValue theMultiplicity;

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( MRoleType.class );

    /**
     * This represents a source RoleType.
     */
    static class Source
            extends
                MRoleType
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
          * Constructor.
          *
          * @param relationship the RelationshipType to which this RoleType will belong
          * @param entity the EntityType related to this RoleType
          * @param multiplicity the multiplicity of this RoleType
          */
        Source(
                MRelationshipType  relationship,
                MEntityType        entity,
                MultiplicityValue  multiplicity )
        {
            super(  relationship.getIdentifier().createRoleTypeIdentifier( RoleTypeDirection.SOURCE_POSTFIX ),
                    relationship,
                    entity,
                    multiplicity );

            setName( StringValue.create(relationship.getName().value() + RoleTypeDirection.SOURCE_POSTFIX.getPostfix() ));
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final StringValue getUserVisibleName()
        {
            StringValue ret = super.getUserVisibleName();
            if( ret == null ) {
                ret = StringValue.create(theUserVisiblePrefix + getRelationshipType().getUserVisibleName().value() );
                theUserName = ret;
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final boolean isSymmetric()
        {
            return false;
        }

        /**
         * The prefix in front of the name to construct our UserVisibleName.
         */
        private static final String theUserVisiblePrefix = theResourceHelper.getResourceString( "ForwardArrow" );
    }

    /**
     * This represents a destination RoleType.
     */
    static class Destination
            extends
                MRoleType
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
          * Constructor.
          *
          * @param relationship the RelationshipType to which this RoleType will belong
          * @param entity the EntityType related to this RoleType
          * @param multiplicity the multiplicity of this RoleType
          */
        Destination(
                MRelationshipType  relationship,
                MEntityType        entity,
                MultiplicityValue  multiplicity )
        {
            super(  relationship.getIdentifier().createRoleTypeIdentifier( RoleTypeDirection.DESTINATION_POSTFIX ),
                    relationship,
                    entity,
                    multiplicity );

            setName(StringValue.create(relationship.getName().value() + RoleTypeDirection.DESTINATION_POSTFIX.getPostfix() ));
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final StringValue getUserVisibleName()
        {
            StringValue ret = super.getUserVisibleName();
            if( ret == null ) {
                ret = StringValue.create(theUserVisiblePrefix + getRelationshipType().getUserVisibleName().value() );
                theUserName = ret;
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final boolean isSymmetric()
        {
            return false;
        }

        /**
         * The prefix in front of the name to construct our UserVisibleName.
         */
        private static final String theUserVisiblePrefix = theResourceHelper.getResourceString( "BackwardArrow" );
    }

    /**
     * This represents a RoleType used for both source and destination of a RelationshipType..
     */
    static class MSrcDest
            extends
                MRoleType
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
          * Constructor.
          *
          * @param relationship the RelationshipType to which this RoleType will belong
          * @param entity the EntityType related to this RoleType
          * @param multiplicity the multiplicity of this RoleType
          */
        MSrcDest(
                MRelationshipType  relationship,
                MEntityType        entity,
                MultiplicityValue  multiplicity )
        {
            super(  relationship.getIdentifier().createRoleTypeIdentifier( RoleTypeDirection.TOP_SINGLETON_POSTFIX ),
                    relationship,
                    entity,
                    multiplicity );
        }

        /**
         * Always throws UnsupportedOperationException
         *
         * @return never returns
         * @throws UnsupportedOperationException always thrown
         */
        @Override
        public final EntityType getOtherEntityType()
        {
            throw new UnsupportedOperationException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final StringValue getUserVisibleName()
        {
            StringValue ret = super.getUserVisibleName();
            if( ret == null ) {
                ret = StringValue.create(theUserVisiblePrefix + getRelationshipType().getUserVisibleName().value() );
                theUserName = ret;
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final boolean isSymmetric()
        {
            return true;
        }

        /**
         * The prefix in front of the name to construct our UserVisibleName.
         */
        private static final String theUserVisiblePrefix = theResourceHelper.getResourceString( "TopArrow" );
    }
}
