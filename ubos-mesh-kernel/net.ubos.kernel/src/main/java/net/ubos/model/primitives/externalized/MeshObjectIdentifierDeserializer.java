//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized;

import java.text.ParseException;
import net.ubos.mesh.MeshObjectIdentifier;

/**
 * Knows how to deserialize MeshObjectIdentifiers from a String
 */
public interface MeshObjectIdentifierDeserializer
{
    /**
     * Construct an MeshObjectIdentifier from its representation as a String.
     *
     * @param externalForm the external form to deserialize
     * @return the MeshObjectIdentifier
     * @throws ParseException thrown if the externalForm could not be parsed
     */
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            String externalForm )
       throws
            ParseException;

    /**
     * Construct an MeshObjectIdentifier from its representation as a String.
     *
     * @param contextIdentifier if the externalForm is given as a relative expression, it is to be interpreted
     *        as being relative to this MeshObjectIdentifier
     * @param externalForm the external form to deserialize
     * @return the MeshObjectIdentifier
     * @throws ParseException thrown if the externalForm could not be parsed
     */
    public MeshObjectIdentifier meshObjectIdentifierFromExternalForm(
            MeshObjectIdentifier contextIdentifier,
            String               externalForm )
       throws
            ParseException;
}
