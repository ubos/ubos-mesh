//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.BooleanValue;
import net.ubos.util.ResourceHelper;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies BooleanValues.
 */
public class BooleanValueStringifier
        extends
            AbstractStringifier<BooleanValue>
{
    /**
     * Factory method.
     *
     * @return the created EnumeratedValueStringifier
     */
    public static BooleanValueStringifier create()
    {
        return new BooleanValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected BooleanValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            BooleanValue          arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret;
        if( arg.value()) {
            ret = theResourceHelper.getResourceString( "True" );
        } else {
            ret = theResourceHelper.getResourceString( "False" );
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (BooleanValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BooleanValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        BooleanValue ret;
        if( theResourceHelper.getResourceString( "True" ).equals( rawString )) {
            ret = BooleanValue.TRUE;

        } else  if( theResourceHelper.getResourceString( "False").equals( rawString )) {
            ret = BooleanValue.FALSE;

        } else {
            throw new StringifierParseException( this, rawString );
        }
        return ret;
    }

    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( BooleanValueStringifier.class );
}
