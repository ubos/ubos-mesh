//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.modelbase.m.MMeshTypeIdentifier;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
  * A PropertyTypeGroup is a group of PropertyTypes, defined by the same AttributableMeshObjectType
  * (or its ancestors in the inheritance hierarchy) that logically belong together.
  * In-memory implementation.
  */
public final class MPropertyTypeGroup
        extends
            MCollectableMeshType
        implements
            PropertyTypeGroup,
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     */
    public MPropertyTypeGroup(
            MMeshTypeIdentifier identifier )
    {
        super( identifier );
    }

    /**
      * Set the MeshTypeWithProperties on which this PropertyTypeGroup is defined.
      *
      * @param newValue the new value of the property
      * @see #getMeshTypeWithProperties
      */
    public final void setMeshTypeWithProperties(
            MeshTypeWithProperties newValue )
    {
        this.theMeshTypeWithProperties = newValue;
    }

    /**
      * Obtain the MeshTypeWithProperties on which this PropertyTypeGroup is defined.
      *
      * @return the MeshTypeWithProperties on which this PropertyTypeGroup is defined
      * @see #setMeshTypeWithProperties
      */
    @Override
    public final MeshTypeWithProperties getMeshTypeWithProperties()
    {
        return theMeshTypeWithProperties;
    }

    /**
     * Set the value of the SequenceNumber property.
     *
     * @param newValue the new value of the SequenceNumber property
     * @see #getSequenceNumber
     */
    public final void setSequenceNumber(
            FloatValue newValue )
    {
        theSequenceNumber = newValue;
    }

    /**
     * Obtain the value of the SequenceNumber property.
     *
     * @return the value of the SequenceNumber property
     * @see #setSequenceNumber
     */
    @Override
    public final FloatValue getSequenceNumber()
    {
        return theSequenceNumber;
    }

    /**
     * Obtain the PropertyTypes that are members of this PropertyTypeGroup.
     *
     * @return the PropertyTypes that are members of this PropertyTypeGroup
     * @see #setContainedPropertyTypes
     */
    @Override
    public final PropertyType [] getContainedPropertyTypes()
    {
        return thePropertyTypes;
    }

    /**
     * Set the PropertyTypes that are members of this PropertyTypeGroup.
     *
     * @param pts the PropertyTypes that are members of this PropertyTypeGroup.
     * @see #getContainedPropertyTypes
     */
    public final void setContainedPropertyTypes(
            PropertyType [] pts )
    {
        thePropertyTypes = pts;
    }

    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump(this,
                new String[] {
                    "id",
                    "name"
                },
                new Object[] {
                    getIdentifier(),
                    getName().value()
                });
    }

    /**
      * The MeshTypeWithProperties that this PropertyTypeGroup is defined in.
      */
    private MeshTypeWithProperties theMeshTypeWithProperties;

    /**
     * The value of the SequenceNumber property.
     */
    private FloatValue theSequenceNumber;

    /**
     * The member PropertyTypes of this PropertyTypeGroup.
     */
    private PropertyType [] thePropertyTypes;
}
