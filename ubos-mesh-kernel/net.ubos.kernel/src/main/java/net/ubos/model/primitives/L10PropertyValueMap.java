//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.util.CanGenerateJavaConstructorString;
import net.ubos.util.l10.L10Map;

/**
 * An L10Map for PropertyValues.
 */
public interface L10PropertyValueMap
        extends
            L10Map<PropertyValue>,
            CanGenerateJavaConstructorString
{
}
