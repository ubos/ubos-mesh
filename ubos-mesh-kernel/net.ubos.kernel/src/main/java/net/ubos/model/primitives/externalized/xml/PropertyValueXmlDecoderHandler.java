//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.externalized.xml;

import java.text.ParseException;
import java.util.Base64;
import java.util.Stack;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.AbstractPropertyValueDecoder;
import net.ubos.util.XmlUtils;
import net.ubos.util.logging.Log;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Factored-out SAX callback handler.
 */
public abstract class PropertyValueXmlDecoderHandler
    extends
        DefaultHandler
    implements
        PropertyValueXmlTags // import namespace
{
    private static final Log log = Log.getLogInstance( PropertyValueXmlDecoderHandler.class );

    /**
     * Set the AbstractPropertyValueDecoder on whose behalf we work.
     *
     * @param parent the AbstractPropertyValueDecoder on whose behalf we work
     */
    public void setParent(
            AbstractPropertyValueDecoder parent )
    {
        theParent = parent;
    }

    /**
     * Override locator method so we know what we are parsing.
     *
     * @param locator the new Locator object
     */
    @Override
    public void setDocumentLocator(
            Locator locator )
    {
        theLocator = locator;
    }

    /**
     * SAX found some characters.
     *
     * @param ch the character array
     * @param start the start index
     * @param length the number of characters
     */
    @Override
    public final void characters(
            char [] ch,
            int     start,
            int     length )
    {
        try {
            if( length > 0 ) {
                if( theCharacters == null ) {
                    theCharacters = new StringBuilder();
                }
                theCharacters.append( ch, start, length );
            }
        } catch( RuntimeException ex ) {
            log.error( ex ); // internal error, no need to throw SAXParseException
        }
    }

    /**
     * SAX found a new element.
     *
     * @param namespaceURI URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @param attrs the Attributes at this element
     * @throws SAXException thrown if a parsing error occurrs
     */
    @Override
    public void startElement(
            String     namespaceURI,
            String     localName,
            String     qName,
            Attributes attrs )
        throws
            SAXException
    {
        if( log.isDebugEnabled() ) {
            log.debug( "SAX startElement: " + namespaceURI + ", " + localName + ", " + qName );
        }
        theCharacters = null; // if we haven't processed them so far, we never will

        String yr;
        String mon;
        String day;
        String hr;
        String min;
        String sec;
        String max;

        switch( qName ) {
            case NULL_VALUE:
                // no op
                break;

            case BLOB_VALUE_TAG:
                String mime      = attrs.getValue( BLOB_VALUE_MIME_TAG );
                String loadFrom  = attrs.getValue( BLOB_VALUE_LOAD_TAG );
                if( mime != null && mime.length() > 0 ) {
                    BlobValue propertyValue;
                    if( theDataType == null ) {
                        if( loadFrom != null && loadFrom.length() > 0 ) {
                            propertyValue = BlobDataType.theAnyType.createBlobValueByLoadingFrom( loadFrom, mime );
                        } else {
                            propertyValue = BlobDataType.theAnyType.createBlobValue( "placeholder", mime ); // that's a big ugly, but why not
                        }
                    } else {
                        if( loadFrom != null && loadFrom.length() > 0 ) {
                            propertyValue = ((BlobDataType)theDataType).createBlobValueByLoadingFrom( loadFrom, mime );
                        } else {
                            propertyValue = ((BlobDataType)theDataType).createBlobValue( "placeholder", mime ); // that's a big ugly, but why not
                        }
                    }
                    thePropertyValue = propertyValue;
                } else {
                    log.error( "empty '" + BLOB_VALUE_MIME_TAG + "' on '" + BLOB_VALUE_TAG + "'" );
                }
                break;

            case BOOLEAN_VALUE_TAG:
                // no op
                break;

            case COLOR_VALUE_TAG:
                // no op
                break;

            case CURRENCY_VALUE_TAG:
                // no op
                break;

            case ENUMERATED_VALUE_TAG:
                // no op
                break;

            case EXTENT_VALUE_TAG:
                String width  = attrs.getValue( EXTENT_VALUE_WIDTH_TAG );
                String height = attrs.getValue( EXTENT_VALUE_HEIGHT_TAG );
                if( width == null || width.length() == 0 ) {
                    log.error( "empty '" + EXTENT_VALUE_WIDTH_TAG + "' on '" + EXTENT_VALUE_TAG + "'" );
                }
                if( height == null || height.length() == 0 ) {
                    log.error( "empty '" + EXTENT_VALUE_HEIGHT_TAG + "' on '" + EXTENT_VALUE_TAG + "'" );
                }   thePropertyValue = ExtentValue.create( Double.parseDouble( width ), Double.parseDouble( height ));
                break;

            case INTEGER_VALUE_TAG:
                // no op
                break;

            case FLOAT_VALUE_TAG:
                // no op
                break;

            case MULTIPLICITY_VALUE_TAG:
                min = attrs.getValue( MULTIPLICITY_VALUE_MIN_TAG );
                max = attrs.getValue( MULTIPLICITY_VALUE_MAX_TAG );
                thePropertyValue = MultiplicityValue.create(
                        ( min != null && !MultiplicityValue.N_SYMBOL.equals( min )) ? Integer.parseInt( min ) : MultiplicityValue.N,
                        ( max != null && !MultiplicityValue.N_SYMBOL.equals( max )) ? Integer.parseInt( max ) : MultiplicityValue.N );
                break;

            case POINT_VALUE_TAG:
                String x = attrs.getValue( POINT_VALUE_X_TAG );
                String y = attrs.getValue( POINT_VALUE_Y_TAG );
                if( x == null || x.length() == 0 ) {
                    log.error( "empty '" + POINT_VALUE_X_TAG + "' on '" + POINT_VALUE_TAG + "'" );
                }
                if( y == null || y.length() == 0 ) {
                    log.error( "empty '" + POINT_VALUE_Y_TAG + "' on '" + POINT_VALUE_TAG + "'" );
                }
                thePropertyValue = PointValue.create( Double.parseDouble( x ), Double.parseDouble( y ));
                break;

            case STRING_VALUE_TAG:
                // no op
                break;

            case TIME_PERIOD_TAG:
                yr  = attrs.getValue( TIME_PERIOD_YEAR_TAG );
                mon = attrs.getValue( TIME_PERIOD_MONTH_TAG );
                day = attrs.getValue( TIME_PERIOD_DAY_TAG );
                hr  = attrs.getValue( TIME_PERIOD_HOUR_TAG );
                min = attrs.getValue( TIME_PERIOD_MINUTE_TAG );
                sec = attrs.getValue( TIME_PERIOD_SECOND_TAG );
                if( yr == null || yr.length() == 0 ) {
                    log.error( "empty '" + TIME_PERIOD_YEAR_TAG + "' on '" + TIME_PERIOD_TAG + "'" );
                }
                if( mon == null || mon.length() == 0 ) {
                    log.error( "empty '" + TIME_PERIOD_MONTH_TAG + "' on '" + TIME_PERIOD_TAG + "'" );
                }
                if( day == null || day.length() == 0 ) {
                    log.error( "empty '" + TIME_PERIOD_DAY_TAG + "' on '" + TIME_PERIOD_TAG + "'" );
                }
                if( hr == null || hr.length() == 0 ) {
                    log.error( "empty '" + TIME_PERIOD_HOUR_TAG + "' on '" + TIME_PERIOD_TAG + "'" );
                }
                if( min == null || min.length() == 0 ) {
                    log.error( "empty '" + TIME_PERIOD_MINUTE_TAG + "' on '" + TIME_PERIOD_TAG + "'" );
                }
                if( sec == null || sec.length() == 0 ) {
                    log.error( "empty '" + TIME_PERIOD_SECOND_TAG + "' on '" + TIME_PERIOD_TAG + "'" );
                }
                thePropertyValue = TimePeriodValue.create(
                        Short.parseShort( yr ),
                        Short.parseShort( mon ),
                        Short.parseShort( day ),
                        Short.parseShort( hr ),
                        Short.parseShort( min ),
                        Float.parseFloat( sec ));
                break;

            case TIME_STAMP_TAG:
                yr  = attrs.getValue( TIME_STAMP_YEAR_TAG );
                mon = attrs.getValue( TIME_STAMP_MONTH_TAG );
                day = attrs.getValue( TIME_STAMP_DAY_TAG );
                hr  = attrs.getValue( TIME_STAMP_HOUR_TAG );
                min = attrs.getValue( TIME_STAMP_MINUTE_TAG );
                sec = attrs.getValue( TIME_STAMP_SECOND_TAG );
                if(    yr  != null && yr.length()  > 0
                    && mon != null && mon.length() > 0
                    && day != null && day.length() > 0
                    && hr  != null && hr.length()  > 0
                    && min != null && min.length() > 0
                    && sec != null && sec.length() > 0 )
                {
                    thePropertyValue = TimeStampValue.create(
                            Short.parseShort( yr ),
                            Short.parseShort( mon ),
                            Short.parseShort( day ),
                            Short.parseShort( hr ),
                            Short.parseShort( min ),
                            Float.parseFloat( sec ));
                } else {
                    thePropertyValue = null; // flag for endElement that the String needs to be parsed
                }
                break;

            default:
                startElement1( namespaceURI, localName, qName, attrs );
                break;
        }
    }

    /**
     * Invoked when no previous start-element parsing rule has matched. Allows subclasses to plus to parsing.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @param attrs the Attributes at this element
     * @throws SAXException thrown if a parsing error occurrs
     */
    protected void startElement1(
            String     namespaceURI,
            String     localName,
            String     qName,
            Attributes attrs )
        throws
            SAXException
    {
        throw new UnknownTokenException( namespaceURI, qName, theLocator );
    }

    /**
     * SAX says an element ends.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @throws SAXException thrown if a parsing error occurrs
     */
    @Override
    public void endElement(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        if( log.isDebugEnabled() ) {
            log.debug( "SAX endElement: " + namespaceURI + ", " + localName + ", " + qName );
        }

        switch( qName ) {
            case NULL_VALUE:
                thePropertyValue = null;
                break;

            case BLOB_VALUE_TAG:
                if( ( thePropertyValue instanceof BlobValue ) && ((BlobValue)thePropertyValue).delayedLoadingFrom() == null ) {
                    if( theDataType == null ) {
                        if( ((BlobValue)thePropertyValue).getMimeType().startsWith( "text/" ) ) {
                            thePropertyValue = BlobDataType.theAnyType.createBlobValue(
                                    theCharacters != null ? XmlUtils.cdataDescape( theCharacters.toString().trim()) : "",
                                    ((BlobValue)thePropertyValue).getMimeType() );
                            // This needs to be patched later once we have the instance of BlobDataType
                        } else if( theCharacters != null ) {
                            thePropertyValue = BlobDataType.theAnyType.createBlobValue(
                                    Base64.getDecoder().decode( theCharacters.toString().trim() ),
                                    ((BlobValue)thePropertyValue).getMimeType() );
                            // This needs to be patched later once we have the instance of BlobDataType
                        } else {
                            thePropertyValue = BlobDataType.theAnyType.createBlobValue(
                                    "",
                                    ((BlobValue)thePropertyValue).getMimeType() );
                        }
                    } else {
                        if( ((BlobValue)thePropertyValue).getMimeType().startsWith( "text/" ) ) {
                            thePropertyValue = ((BlobDataType)theDataType).createBlobValue(
                                    theCharacters != null ? XmlUtils.cdataDescape( theCharacters.toString().trim()) : "",
                                    ((BlobValue)thePropertyValue).getMimeType() );
                        } else if( theCharacters != null ) {
                            thePropertyValue = ((BlobDataType)theDataType).createBlobValue(
                                    Base64.getDecoder().decode( theCharacters.toString().trim() ),
                                    ((BlobValue)thePropertyValue).getMimeType() );
                        } else {
                            thePropertyValue = ((BlobDataType)theDataType).createBlobValue(
                                    "",
                                    ((BlobValue)thePropertyValue).getMimeType() );
                        }
                    }
                }
                break;

            case BOOLEAN_VALUE_TAG:
                if( theCharacters != null ) {
                    thePropertyValue = BooleanValue.create( BOOLEAN_VALUE_TRUE_TAG.equals( theCharacters.toString().trim() ));
                } else {
                    throw new SAXException( "No value given for BooleanValue" );
                }
                break;

            case COLOR_VALUE_TAG:
                if( theCharacters != null ) {
                    thePropertyValue = ColorValue.parseColorValue( theCharacters.toString() );
                } else {
                    throw new SAXException( "No value given for ColorValue" );
                }
                break;

            case CURRENCY_VALUE_TAG:
                if( theCharacters != null ) {
                    try {
                        thePropertyValue = CurrencyValue.parseCurrencyValue( theCharacters.toString() );
                    } catch( ParseException ex ) {
                        throw new SAXException(  "Failed to parse CurrencyValue " + theCharacters.toString() );
                    }
                } else {
                    throw new SAXException( "No value given for ColorValue" );
                }
                break;

            case ENUMERATED_VALUE_TAG:
                if( theCharacters != null ) {
                    if( theDataType == null ) {
                        thePropertyValue = EnumeratedValue.create( null, theCharacters.toString().trim(), null, null );
                        // This needs to be patched later once we have the instance of EnumeratedDataType
                    } else {
                        thePropertyValue = EnumeratedValue.create( (EnumeratedDataType)theDataType, theCharacters.toString().trim(), null, null );
                    }
                } else {
                    throw new SAXException( "No value given for EnumeratedValue" );
                }
                break;

            case EXTENT_VALUE_TAG:
                // no op
                break;

            case INTEGER_VALUE_TAG:
                if( theCharacters != null ) {
                    thePropertyValue = IntegerValue.parseIntegerValue( theCharacters.toString() );
                } else {
                    throw new SAXException( "No value given for IntegerValue" );
                }
                break;

            case FLOAT_VALUE_TAG:
                if( theCharacters != null ) {
                    thePropertyValue = FloatValue.parseFloatValue( theCharacters.toString() );
                } else {
                    throw new SAXException( "No value given for FloatValue" );
                }
                break;

            case MULTIPLICITY_VALUE_TAG:
                // no op
                break;

            case POINT_VALUE_TAG:
                // no op
                break;

            case STRING_VALUE_TAG:
                thePropertyValue = StringValue.create(
                        theCharacters != null ? theCharacters.toString() : "" ); // let's not strip() here
                break;

            case TIME_PERIOD_TAG:
                // no op
                break;

            case TIME_STAMP_TAG:
                if( thePropertyValue == null ) {
                    try {
                        thePropertyValue = TimeStampValue.createFromRfc3339( theCharacters.toString().trim() );
                    } catch( ParseException ex ) {
                        throw new SAXException( "Invalid RFC 3339 time stamp", ex );
                    }
                }
                break;

            default:
                endElement1( namespaceURI, localName, qName );
                break;
        }
    }

    /**
     * Invoked when no previous end-element parsing rule has matched. Allows subclasses to plus to parsing.
     *
     * @param namespaceURI the URI of the namespace
     * @param localName the local name
     * @param qName the qName
     * @throws SAXException thrown if a parsing error occurrs
     */
    protected void endElement1(
            String namespaceURI,
            String localName,
            String qName )
        throws
            SAXException
    {
        throw new UnknownTokenException( namespaceURI, qName, theLocator );
    }

    /**
     * Throw exception in case of an Exception indicating an error.
     *
     * @param ex the Exception
     * @throws org.xml.sax.SAXParseException thrown if a parsing error occurs
     */
    public final void error(
            Exception ex )
        throws
            SAXParseException
    {
        if( ex instanceof SAXParseException ) {
            throw ((SAXParseException)ex);
        } else {
            throw new LocalizedSAXParseException.Delegate( theLocator, ex );
        }
    }

    /**
     * The AbstractPropertyValueDecoder on whose behalf we work.
     */
    protected AbstractPropertyValueDecoder theParent;

    /**
     * The Locator object that tells us where we are in the parsed file.
     */
    protected Locator theLocator;

    /**
     * The parse stack.
     */
    protected Stack<Object> theStack = new Stack<>();

    /**
     * The character String that is currently being parsed, if any.
     */
    protected StringBuilder theCharacters = null;

    /**
     * The PropertyValue that was successfully parsed.
     */
    protected PropertyValue thePropertyValue = null;

    /**
     * The DataType for which we parse a value. This may be null if
     * we don't know it yet (e.g. when parsing default values in model.xml files)
     */
    protected DataType theDataType;
}
