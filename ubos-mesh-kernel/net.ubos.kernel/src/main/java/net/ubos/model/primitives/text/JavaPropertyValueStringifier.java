//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParameters;

/**
 * A Stringifier to stringify PropertyValues into Java syntax. The reverse is currently NOT supported.
 * (FIXME. But: beware of code injection attacks)
 */
public class JavaPropertyValueStringifier
        extends
            AbstractStringifier<PropertyValue>
{
    /**
     * Factory method.
     *
     * @return the created JavaPropertyValueStringifier
     */
    public static JavaPropertyValueStringifier create()
    {
        return new JavaPropertyValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected JavaPropertyValueStringifier()
    {
        // no op
    }

    /**
     * Format an Object using this Stringifier.
     *
     * @param soFar the String so far, if any
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the formatted String
     */
    @Override
    public String format(
            PropertyValue         arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = arg.getJavaConstructorString( "loader", "type" );
        return ret;
    }

    /**
     * Format an Object using this Stringifier. This may be null.
     *
     * @param soFar the String so far, if any
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the formatted String
     * @throws ClassCastException thrown if this Stringifier could not format the provided Object
     *         because the provided Object was not of a type supported by this Stringifier
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg == null ) {
            return "null";
        } else {
            return format( (PropertyValue) arg, pars, soFar );
        }
    }
}
