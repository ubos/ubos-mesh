//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.IllegalPropertyValueException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;

/**
  * <p>This is a value-holding property of an AttributableObjectType.
  *
  * <p>PropertyTypes have DataTypes that constrain the values they can hold.
  *
  * <p>If a PropertyType is optional, it may have the value null. If it is not
  * optional, the null value is not allowed.
  *
  * <p>They can have default values, i.e. the initial value to which they are set
  * once the enclosing AttributableObjectType is instantiated. Default values can
  * be specified either as fixed values, or as code that is to be run when the
  * enclosing AttributableObjectType is instantiated.
  *
  * <p>They may be read-only, i.e. not provide means to set their value.
  *
  * <p>PropertyTypes in subtypes may override PropertyTypes in supertypes, within certain limits.
  * For example, a PropertyType may override a PropertyType in a supertype by
  * specifying a different default value.
  */
public interface PropertyType
        extends
            CollectableMeshType
{
    /**
     * Obtain the MeshTypeWithProperties on which this PropertyType is defined.
     *
     * @return the MeshTypeWithProperties on which this PropertyType is defined
     */
    public MeshTypeWithProperties getMeshTypeWithProperties();

    /**
     * Obtain the value of the SequenceNumber property.
     *
     * @return the value of the SequenceNumber property
     */
    public FloatValue getSequenceNumber();

    /**
      * Obtain the value of the DataType property.
      *
      * @return the value of the DataType property
      */
    public DataType getDataType();

    /**
     * Obtain the value of the DefaultValue property.
     *
     * @return the value of the DefaultValue property
     */
    public PropertyValue getDefaultValue();

    /**
      * Obtain the value of the IsOptional property.
      *
      * @return the value of the IsOptional property
      */
    public BooleanValue getIsOptional();

    /**
     * Obtain the value of the IsReadOnly property.
     *
     * @return the value of the IsReadOnly property
     */
    public BooleanValue getIsReadOnly();

    /**
     * Obtain the MetaAttributes that this PropertyValue overrides, if any.
     *
     * @return the MetaAttributes that this Property overrides
     */
    public PropertyType [] getOverride();

    /**
     * Obtain the ancestor PropertyType of the overriding hierarchy. If this PropertyType
     * does not override any other PropertyType, a call to this method returns this
     * PropertyType. Otherwise it is invoked recursively on the overridden
     * PropertyType, until we reach the top of the overriding hierarchy. Due to
     * topology constraints, it is known that the ancestor PropertyType is the same,
     * regardless which override path was taken in case of multiple inheritance of the
     * enclosing AttributableObjectType.
     *
     * @return the ancestor of the overriding hierarchy
     */
    public PropertyType getOverrideAncestor();

    /**
     * Determine whether this PropertyType overrides the passed-in PropertyType.
     *
     * @param other the PropertyType to test against
     * @return true if this PropertyType overrides the passed-in PropertyType
     */
    public boolean overrides(
            PropertyType other );

    /**
     * Determine whether this PropertyType is the same as, or overrides the passed-in
     * PropertyType. This is the method typically used to determine whether
     * or not an events PropertyType applies to
     * an event subscriber, which generally picks the PropertyType highest in the inheritance
     * hierarchy to compare against.
     *
     * @param other the PropertyType to test against
     * @return true if this PropertyType is the same as, or overrides the passed-in PropertyType
     */
    public boolean equalsOrOverrides(
            PropertyType other );

    /**
     * Given this MeshObject, check that its property conforms to the
     * requirements of this PropertyType.
     *
     * @param affected the MeshObject
     * @throws IllegalPropertyTypeException thrown if this PropertyType is not applicable to the provided MeshObject
     * @throws IllegalPropertyValueException thrown if the value was invalid for this PropertyType
     * @throws NotPermittedException thrown if the caller did not have permission to call this
     */
    public void checkValue(
            MeshObject affected )
        throws
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            NotPermittedException;

    /**
     * Convenience constant.
     */
    public static final PropertyType [] EMPTY_ARRAY = {};
}
