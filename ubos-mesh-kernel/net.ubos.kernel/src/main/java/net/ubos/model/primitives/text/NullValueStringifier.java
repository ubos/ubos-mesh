//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.text;

import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.ResourceHelper;
import net.ubos.util.text.AbstractStringifier;
import net.ubos.util.text.StringifierParseException;
import net.ubos.util.text.StringifierUnformatFactory;
import net.ubos.util.text.StringifierParameters;

/**
 * Stringifies null PropertyValues.
 */
public class NullValueStringifier
        extends
            AbstractStringifier<PropertyValue>
{
    /**
     * Factory method.
     *
     * @return the created NullValueStringifier
     */
    public static NullValueStringifier create()
    {
        return new NullValueStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected NullValueStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            PropertyValue         arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = theResourceHelper.getResourceString( "Null" );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (PropertyValue) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BooleanValue unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        if( theResourceHelper.getResourceString( "Null" ).equals( rawString )) {
            return null;
        } else {
            throw new StringifierParseException( this, rawString );
        }
    }

    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( NullValueStringifier.class );
}
