//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * <p>A PropertyTypeGroup is a group of PropertyTypes, defined by the same AttributableMeshObjectType
 * (or its ancestors in the inheritance hierarchy) that logically belong together.</p>
 *
 * <p>This can be used by things like property sheets, for example, to show related PropertyTypes together.</p>
 */
public interface PropertyTypeGroup
        extends
            CollectableMeshType
{
    /**
     * Obtain the MeshTypeWithProperties on which this PropertyTypeGroup is defined.
     *
     * @return the MeshTypeWithProperties on which this PropertyTypeGroup is defined
     */
    public MeshTypeWithProperties getMeshTypeWithProperties();

    /**
     * Obtain the value of the SequenceNumber property.
     *
     * @return the value of the SequenceNumber property
     */
    public FloatValue getSequenceNumber();

    /**
     * Obtain the PropertyTypes that are members of this PropertyTypeGroup.
     *
     * @return the PropertyTypes that are members of this PropertyTypeGroup
     */
    public PropertyType [] getContainedPropertyTypes();
}
