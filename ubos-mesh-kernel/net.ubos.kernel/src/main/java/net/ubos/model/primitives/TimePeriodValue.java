//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives;

/**
 * A time period PropertyValue.
 */
public final class TimePeriodValue
        extends
            PropertyValue
{
    private final static long serialVersionUID = 1L; // helps with serialization

    /**
     * Factory method.
     *
     * @param year the number of years
     * @param month the number of months
     * @param day the number of days
     * @param hour the number of hours
     * @param minute the number of minutes
     * @param second the number of seconds, plus fractions
     * @return the created TimePeriodValue
     */
    public static TimePeriodValue create(
            short year,
            short month,
            short day,
            short hour,
            short minute,
            float second )
    {
        TimePeriodValue ret = new TimePeriodValue( year, month, day, hour, minute, second );
        return ret;
    }

    /**
     * Factory method.
     *
     * @param millis the number of milliseconds
     * @return the created TimePeriodValue
     */
    public static TimePeriodValue create(
            long millis )
    {
        long minutes = millis / 60000;
        long hours   = minutes / 60;
        long days    = hours / 24;
        long years   = days / 365;
        long months  = ( days - years * 365) / 12;

        days = days - years * 365 - 30 * months;

        float seconds = (millis - minutes * 60000) / 1000.f;

        minutes = minutes % 60;
        hours   = hours % 60;

        TimePeriodValue ret = new TimePeriodValue( (short) years, (short) months, (short) days, (short) hours, (short) minutes, seconds );
        return ret;
    }

    /**
     * Private constructor, use factory method.
     *
     * @param year the number of years
     * @param month the number of months
     * @param day the number of days
     * @param hour the number of hours
     * @param minute the number of minutes
     * @param second the number of seconds, plus fractions
     */
    private TimePeriodValue(
            short year,
            short month,
            short day,
            short hour,
            short minute,
            float second )
    {
        if( month < 0 ) {
            throw new InvalidTimePeriodValueException.Month( year, month, day, hour, minute, second );
        }
        if( day < 0 ) {
            throw new InvalidTimePeriodValueException.Day( year, month, day, hour, minute, second );
        }
        if( hour < 0 ) {
            throw new InvalidTimePeriodValueException.Hour( year, month, day, hour, minute, second );
        }
        if( minute < 0 ) {
            throw new InvalidTimePeriodValueException.Minute( year, month, day, hour, minute, second );
        }
        if( second < 0. ) {
            throw new InvalidTimePeriodValueException.Second( year, month, day, hour, minute, second );
        }

        this.theYear   = year;
        this.theMonth  = month;
        this.theDay    = day;
        this.theHour   = hour;
        this.theMinute = minute;
        this.theSecond = second;
    }

    /**
     * Determine year value.
     *
     * @return the number of years
     */
    public short getYear()
    {
        return theYear;
    }

    /**
     * Determine month value.
     *
     * @return the number of months
     */
    public short getMonth()
    {
        return theMonth;
    }

    /**
     * Determine day value.
     *
     * @return the number of days
     */
    public short getDay()
    {
        return theDay;
    }

    /**
     * Determine hour value.
     *
     * @return the number of hours
     */
    public short getHour()
    {
        return theHour;
    }

    /**
      * Determine minute value.
      *
      * @return the number of minutes
      */
    public short getMinute()
    {
        return theMinute;
    }

    /**
      * Determine second value.
      *
      * @return the number of seconds plus fractions
      */
    public float getSecond()
    {
        return theSecond;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleString()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value()
    {
        return toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue()
    {
        return value();
    }

    /**
     * Obtain the underlying value as number of milliseconds.
     *
     * @return the milliseconds
     */
    public long valueAsMillis()
    {
        long ret = theYear * 365 + theMonth * 12 + theDay;
        ret = ret * 24 + theHour;
        ret = ret * 60 + theMinute;
        ret = ret * 60000 + (long)( theSecond * 1000 );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimePeriodDataType getDataType()
    {
        return TimePeriodDataType.theDefault;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object otherValue )
    {
        if( otherValue instanceof TimePeriodValue ) {
            return (theYear   == ((TimePeriodValue)otherValue).theYear)
                && (theMonth  == ((TimePeriodValue)otherValue).theMonth)
                && (theDay    == ((TimePeriodValue)otherValue).theDay)
                && (theHour   == ((TimePeriodValue)otherValue).theHour)
                && (theMinute == ((TimePeriodValue)otherValue).theMinute)
                && (theSecond == ((TimePeriodValue)otherValue).theSecond);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        ret = (ret << 1) ^ theYear;
        ret = (ret << 1) ^ theMonth;
        ret = (ret << 1) ^ theDay;
        ret = (ret << 1) ^ theHour;
        ret = (ret << 1) ^ theMinute;
        ret = (ret << 1) ^ (int) theSecond;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder( 256 );
        buf.append( theYear );
        buf.append( "/" );
        buf.append( theMonth );
        buf.append( "/" );
        buf.append( theDay );
        buf.append( " " );
        buf.append( theHour );
        buf.append( ":" );
        buf.append( theMinute );
        buf.append( ":" );
        buf.append( theSecond );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar )
    {
        StringBuilder buf = new StringBuilder( 256 );
        buf.append( getClass().getName() );
        buf.append( DataType.CREATE_STRING );
        buf.append( "(short) " );
        buf.append( theYear );
        buf.append( ", (short) " );
        buf.append( theMonth );
        buf.append( ", (short) " );
        buf.append( theDay );
        buf.append( ", (short) " );
        buf.append( theHour );
        buf.append( ", (short) " );
        buf.append( theMinute );
        buf.append( ", (float) " );
        buf.append( theSecond );
        buf.append( DataType.CLOSE_PARENTHESIS_STRING );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(
            PropertyValue o )
    {
        TimePeriodValue realOther = (TimePeriodValue) o;

        return compare( this, realOther );
    }

    /**
     * Compare two instances of this class according to the rules of java.util.Comparator.
     *
     * @param valueOne the first value
     * @param valueTwo the second value
     * @return 0 if both are the same, -1 if the first is smaller than the second, 1 if the second is smaller that the first
     */
    public static int compare(
            TimePeriodValue valueOne,
            TimePeriodValue valueTwo )
    {
        if( valueOne == null ) {
            if( valueTwo == null ) {
                return 0;
            } else {
                return +1;
            }
        } else {
            if( valueTwo == null ) {
                return -1;
            } else {
                int delta;
                delta = valueOne.theYear - valueTwo.theYear;
                if( delta != 0 ) {
                    return delta;
                }
                delta = valueOne.theMonth - valueTwo.theMonth;
                if( delta != 0 ) {
                    return delta;
                }
                delta = valueOne.theDay - valueTwo.theDay;
                if( delta != 0 ) {
                    return delta;
                }
                delta = valueOne.theHour - valueTwo.theHour;
                if( delta != 0 ) {
                    return delta;
                }
                delta = valueOne.theMinute - valueTwo.theMinute;
                if( delta != 0 ) {
                    return delta;
                }

                if( valueOne.theSecond < valueTwo.theSecond ) {
                    return -1;
                } else if( valueOne.theSecond == valueTwo.theSecond ) {
                    return 0;
                } else {
                    return +1;
                }
            }
        }
    }

    /**
      * The real year value.
      */
    protected short theYear;

    /**
      * The real month value.
      */
    protected short theMonth;

    /**
      * The real day value.
      */
    protected short theDay;

    /**
      * The real hour value.
      */
    protected short theHour;

    /**
      * The real minute value.
      */
    protected short theMinute;

    /**
      * The real second value.
      */
    protected float theSecond;
}
