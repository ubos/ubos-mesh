//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.primitives.m;

import java.util.Arrays;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.util.MeshTypeUtils;
import net.ubos.modelbase.CollectableMeshTypeNotFoundException;
import net.ubos.modelbase.m.MMeshTypeIdentifier;
import net.ubos.util.ArrayHelper;


/**
  * The packaging construct for Models.
  * In-memory implementation.
  */
public final class MSubjectArea
        extends
            MMeshType
        implements
            SubjectArea
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param identifier the Identifier of the to-be-created object
     */
    public MSubjectArea(
            MMeshTypeIdentifier identifier )
    {
        super( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final SubjectArea [] getSubjectAreaDependencies()
    {
        return theSubjectAreaDependencies;
    }

    /**
     * Set the set of SubjectAreas that this SubjectArea depends on.
     *
     * @param newValue the set of SubjectAreas that this SubjectArea depends on
     * @see #getSubjectAreaDependencies
     */
    public final void setSubjectAreaDependencies(
            SubjectArea [] newValue )
    {
        theSubjectAreaDependencies = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final synchronized CollectableMeshType [] getCollectableMeshTypes()
    {
        return theCollectableMeshTypes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getEntityTypes()
    {
        int count = 0;
        for( int i=theCollectableMeshTypes.length-1 ; i>=0 ; --i ) {
            if( theCollectableMeshTypes[i] instanceof EntityType ) {
                ++count;
            }
        }
        EntityType [] ret = new EntityType[ count ];
        for( int i=theCollectableMeshTypes.length-1 ; i>=0 ; --i ) {
            if( theCollectableMeshTypes[i] instanceof EntityType ) {
                ret[--count] = (EntityType) theCollectableMeshTypes[i];
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityType [] getOrderedEntityTypes()
    {
        EntityType [] ret = getEntityTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType [] getRelationshipTypes()
    {
        int count = 0;
        for( int i=theCollectableMeshTypes.length-1 ; i>=0 ; --i ) {
            if( theCollectableMeshTypes[i] instanceof RelationshipType ) {
                ++count;
            }
        }
        RelationshipType [] ret = new RelationshipType[ count ];
        for( int i=theCollectableMeshTypes.length-1 ; i>=0 ; --i ) {
            if( theCollectableMeshTypes[i] instanceof RelationshipType ) {
                ret[--count] = (RelationshipType) theCollectableMeshTypes[i];
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelationshipType [] getOrderedRelationshipTypes()
    {
        RelationshipType [] ret = getRelationshipTypes();
        Arrays.sort( ret, MeshTypeUtils.BY_USER_VISIBLE_NAME_COMPARATOR );

        return ret;
    }

    /**
     * Set the ClassLoader that can load the appropriate Java classes to instantiate
     * all MeshTypeWithProperties in this SubjectArea.
     *
     * @param newValue the new value of this property
     * @see #getClassLoader
     */
    public final void setClassLoader(
            ClassLoader newValue )
    {
        theClassLoader = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final ClassLoader getClassLoader()
    {
        return theClassLoader;
    }

    /**
     * {@inheritDoc}
     * @param name
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeByName(
            String name )
        throws
            CollectableMeshTypeNotFoundException
    {
        CollectableMeshType ret = findCollectableMeshTypeByNameOrNull( name );
        if( ret == null ) {
            throw new CollectableMeshTypeNotFoundException( this, name );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     * @param name
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeByNameOrNull(
            String name )
    {
        CollectableMeshType [] cmts = getCollectableMeshTypes();

        for( int i=0 ; i<cmts.length ; ++i ) {
            if( cmts[i].getName().equals( name )) {
                return cmts[i];
            }
        }
        return null;
    }

    /**
      * Add a CollectableMeshType to this SubjectArea.
      *
      * @param cmo the CollectableMeshType to add to this SubjectArea
      */
    public final void addCollectableMeshType(
            CollectableMeshType cmo )
    {
        for( int i=0 ; i<theCollectableMeshTypes.length ; ++i ) {
            if( theCollectableMeshTypes[i].equals( cmo )) {
                return;
            }
        }

        theCollectableMeshTypes = ArrayHelper.append(
                theCollectableMeshTypes,
                cmo,
                CollectableMeshType.class );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeByLocalIdentifier(
            String localId )
        throws
            CollectableMeshTypeNotFoundException
    {
        CollectableMeshType ret = findCollectableMeshTypeByLocalIdentifierOrNull( localId );
        if( ret != null ) {
            return ret;
        } else {
            throw new CollectableMeshTypeNotFoundException( this, localId ); // may not be 100% the right exception
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectableMeshType findCollectableMeshTypeByLocalIdentifierOrNull(
            String localId )
    {
        CollectableMeshType [] cmts = getCollectableMeshTypes();
        for( CollectableMeshType cmt : cmts ) {
            if( localId.equals( cmt.getIdentifier().getLocalPart() )) {
                return cmt;
            }
        }
        return null;
    }

    /**
      * Contained CollectableMeshTypes
      */
    private CollectableMeshType [] theCollectableMeshTypes = new CollectableMeshType[0];

    /**
     * The SubjectAreas that this SubjectArea depends on.
     */
    private SubjectArea [] theSubjectAreaDependencies = new SubjectArea[0];

    /**
     * The ClassLoader that we use in order to load all MeshTypeWithProperties in this SubjectArea.
     */
    private transient ClassLoader theClassLoader;
}
