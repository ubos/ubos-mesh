//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.ArrayFacade;

/**
 * A component in the CompoundStringifier that is a placeholder for a child Stringifier.
 *
 * @param <T> the type of the Objects to be stringified
 */
class CompoundStringifierPlaceholder
        implements
            CompoundStringifierComponent
{
    /**
     * Constructor.
     *
     * @param stringifier the underlying Stringifier
     * @param placeholderIndex the index of the placeholder
     */
    public CompoundStringifierPlaceholder(
            Stringifier<? extends Object> stringifier,
            int            placeholderIndex )
    {
        theStringifier      = stringifier;
        thePlaceholderIndex = placeholderIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            ArrayFacade<Object>   arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        Object [] realArgs = arg.getArray();

        Object localArg = realArgs[ thePlaceholderIndex ];
        String ret = theStringifier.attemptFormat( localArg, pars, soFar );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<? extends StringifierParsingChoice<?>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
         Iterator<? extends StringifierParsingChoice<?>> ret
                 = theStringifier.allParsingChoiceIterator( rawString, startIndex, endIndex, max, factory );
         return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<? extends StringifierParsingChoice<?>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
         Iterator<? extends StringifierParsingChoice<?>> ret
                 = theStringifier.partialParsingChoiceIterator( rawString, startIndex, endIndex, max, factory );
         return ret;
    }

    /**
     * Obtain the underlying Stringifier.
     *
     * @return the underlying Stringifier
     */
    public Stringifier<?> getStringifier()
    {
        return theStringifier;
    }

    /**
     * Obtain the placeholder index.
     *
     * @return the placeholder index
     */
    public int getPlaceholderIndex()
    {
        return thePlaceholderIndex;
    }

    /**
     * The underlying Stringifier.
     */
    protected final Stringifier<?> theStringifier;

    /**
     * The index of the placeholder, e.g. 2 for {2}.
     */
    protected final int thePlaceholderIndex;
}
