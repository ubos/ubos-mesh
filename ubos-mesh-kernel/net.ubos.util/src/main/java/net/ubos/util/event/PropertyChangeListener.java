//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.event;

/**
 * Replacement for the JDK's PropertyChangeListener
 */
public interface PropertyChangeListener
{
    /**
     * An event has occurred.
     * 
     * @param e the event
     */
    public void propertyChange(
            PropertyChange e );
}
