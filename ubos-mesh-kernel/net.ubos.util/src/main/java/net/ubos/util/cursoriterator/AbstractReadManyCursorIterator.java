//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.BiPredicate;

/**
 * Factors out common behaviors of {@link CursorIterator}s.
 * This implementation is for subclasses that are most efficient when reading many elements at a time (e.g. a database)
 *
 * @param <E> the type of element to iterate over
 */
public abstract class AbstractReadManyCursorIterator<E>
        extends
            AbstractCursorIterator<E>
{
    /**
     * Constructor.
     *
     * @param equals the equality operation for E's
     */
    protected AbstractReadManyCursorIterator(
            BiPredicate<E,E> equals )
    {
        super( equals );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
    {
        List<E> found = next( 1 );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
    {
        List<E> found = previous( 1 );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }
}
