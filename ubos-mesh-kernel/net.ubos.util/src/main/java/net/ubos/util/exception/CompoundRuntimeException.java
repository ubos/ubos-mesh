//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * A RuntimeException that collects several other Throwables. This RuntimeException can be thrown
 * if more than one problem occurred simultaneously and all of them should be passed on.
 */
public class CompoundRuntimeException
    extends
        RuntimeException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param ts the Throwables
     */
    public CompoundRuntimeException(
            Throwable [] ts )
    {
        theThrowables = ts;
    }

    /**
     * Obtain the collected Throwables.
     *
     * @return the Exceptions
     */
    public Throwable [] getThrowables()
    {
        return theThrowables;
    }

    /**
     * The collected Throwables.
     */
    protected final Throwable [] theThrowables;
}
