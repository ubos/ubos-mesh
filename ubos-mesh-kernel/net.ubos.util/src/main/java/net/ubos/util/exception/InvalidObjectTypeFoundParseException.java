//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * An ParseException indicating that the while parsing succeeded, the wrong type of
 * object was found.
 */
public class InvalidObjectTypeFoundParseException
    extends
        LocalizedParseException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param string the text that could not be parsed
     * @param rightClass the right type of object expected
     * @param found the found Object
     * @param cause the underlying ClassCastException
     */
    public InvalidObjectTypeFoundParseException(
            String             string,
            Class<?>           rightClass,
            Object             found,
            ClassCastException cause )
    {
        super( string, null, 0, cause );

        theRightClass = rightClass;
        theFound      = found;
    }

    /**
     * Obtain the right Class that should have been found.
     *
     * @return the right Class
     */
    public Class<?> getRightClass()
    {
        return theRightClass;
    }

    /**
     * Obtain the found object.
     *
     * @return the found object
     */
    public Object getFound()
    {
        return theFound;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theString, theRightClass, theFound, getCause() };
    }

    /**
     * The right type of object that should have been found.
     */
    protected final Class<?> theRightClass;

    /**
     * The found object.
     */
    protected final Object theFound;
}
