//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap;

/**
 * A SmartMap that swaps its content in and out.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public interface SwappingSmartMap<K,V>
    extends
        SmartMap<K,V>
{
    /**
     * If this SmartMap uses a local cache, clear it.
     * This is used as test instrumentation.
     */
    public abstract void clearLocalCache();

    /**
     * Add a listener for SmartMap events.
     *
     * @param newListener the new listener
     */
    public void addDirectSmartMapListener(
            SwappingSmartMapListener<K,V> newListener );

    /**
     * Add a listener for SmartMap events.
     *
     * @param newListener the new listener
     */
    public void addWeakSmartMapListener(
            SwappingSmartMapListener<K,V> newListener );

    /**
     * Add a listener for SmartMap events.
     *
     * @param newListener the new listener
     */
    public void addSoftSmartMapListener(
            SwappingSmartMapListener<K,V> newListener );

    /**
     * Remove a listener for SmartMap events.
     *
     * @param oldListener the old listener
     */
    public void removeSmartMapListener(
            SwappingSmartMapListener<K,V> oldListener );
}
