//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.BiPredicate;

/**
 * A CursorIterator that delegates to another, but always reads chunks instead of
 * one element at a time, and keeps those chunks around so it can answer some
 * questions without going back out.
 * 
 * @param <E> the type of element to iterate over
 */
public class BufferingCursorIterator<E>
    implements
        CursorIterator<E>
{
    /**
     * Factory method with defaults.
     * 
     * @param delegate the underlying CursorIterator
     * @return the created object
     * @param <E> the type of element to iterate over
     */
    public static <E> BufferingCursorIterator<E> create(
            CursorIterator<E> delegate )
    {
        return new BufferingCursorIterator<>( DEFAULT_CHUNK_SIZE, delegate, 0, null, false, false, new AbstractCursorIterator.DEFAULT_EQUALS<>() );
    }

    /**
     * Factory method with defaults.
     * 
     * @param delegate the underlying CursorIterator
     * @param equals the equality operation for E's
     * @return the created object
     * @param <E> the type of element to iterate over
     */
    public static <E> BufferingCursorIterator<E> create(
            CursorIterator<E> delegate,
            BiPredicate<E,E>  equals )
    {
        return new BufferingCursorIterator<>( DEFAULT_CHUNK_SIZE, delegate, 0, null, false, false, equals );
    }

    /**
     * Factory method.
     * 
     * @param chunkSize the desired chunk size
     * @param delegate the underlying CursorIterator
     * @return the created object
     * @param <E> the type of element to iterate over
     */
    public static <E> BufferingCursorIterator<E> create(
            int               chunkSize,
            CursorIterator<E> delegate )
    {
        return new BufferingCursorIterator<>( chunkSize, delegate, 0, null, false, false, new AbstractCursorIterator.DEFAULT_EQUALS<>() );
    }

    /**
     * Factory method.
     * 
     * @param chunkSize the desired chunk size
     * @param delegate the underlying CursorIterator
     * @param equals the equality operation for E's
     * @return the created object
     * @param <E> the type of element to iterate over
     */
    public static <E> BufferingCursorIterator<E> create(
            int               chunkSize,
            CursorIterator<E> delegate,
            BiPredicate<E,E>  equals )
    {
        return new BufferingCursorIterator<>( chunkSize, delegate, 0, null, false, false, equals );
    }

    /**
     * Constructor.
     * 
     * @param chunkSize the desired chunk size
     * @param delegate the underlying CursorIterator
     * @param index the index into the current buffer
     * @param buffer the buffer to use, if any
     * @param beforeStart true if we know the delegate is before the start
     * @param afterEnd true if we know the delegate is after the end
     * @param equals the equality operation for E's
     */
    protected BufferingCursorIterator(
            int               chunkSize,
            CursorIterator<E> delegate,
            int               index,
            List<E>           buffer,
            boolean           beforeStart,
            boolean           afterEnd,
            BiPredicate<E,E>  equals )
    {
        theChunkSize             = chunkSize;
        theDelegate              = delegate;
        theIndex                 = index;
        theBuffer                = buffer;
        theDelegateIsBeforeStart = beforeStart;
        theDelegateIsAfterEnd    = afterEnd;
        theEquals                = equals;
    }

    /**
     * Clear the buffer.
     */
    public void clear()
    {
        // set the delegate where we currently are
        if( theBuffer != null && theIndex != theBuffer.size() ) {
            theDelegate.moveBy( theIndex - theBuffer.size() );
        }
        theIndex                 = 0;
        theBuffer                = null;
        theDelegateIsBeforeStart = false;
        theDelegateIsAfterEnd    = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekNext()
    {
        List<E> found = peekNext( 1 );
        if( found.isEmpty() ) {
            throw new NoSuchElementException();
        } else {
            return found.get( 0 );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekPrevious()
    {
        List<E> found = peekPrevious( 1 );
        if( found.isEmpty() ) {
            throw new NoSuchElementException();
        } else {
            return found.get( 0 );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> peekNext(
            int n )
    {
        // FIXME this can be done better
        List<E> ret = createCopy().next( n );
        return ret;
    }

    @Override
    public List<E> peekPrevious( int n )
    {
        // FIXME this can be done better
        List<E> ret = createCopy().previous( n );
        return ret;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
    {
        List<E> found = next( 1 );
        if( found.isEmpty() ) {
            return null;
        } else {
            return found.get( 0 );
        }
    }

    @Override
    public List<E> next(
            int n )
    {
        adjustBufferSoWeHaveNext( n );
        
        int          max = Math.min( n, theBuffer.size() - theIndex );
        int          newIndex = theIndex + max;
        ArrayList<E> ret = new ArrayList<>( max );
        
        ret.addAll( theBuffer.subList( theIndex, newIndex ));
        
        theIndex = newIndex;

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
    {
        List<E> found = previous( 1 );
        if( found.isEmpty() ) {
            return null;
        } else {
            return found.get( 0 );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> previous(
            int n )
    {
        adjustBufferSoWeHavePrevious( n );
        
        int          max      = Math.min( n, theIndex );
        int          newIndex = theIndex - max;

        ArrayList<E> ret = reverse( theBuffer.subList( newIndex, theIndex ));
        
        theIndex = newIndex;

        return ret;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
    {
        clear();
        theDelegate.remove();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<E> createCopy()
    {
        ArrayList<E> copiedBuffer;
        if( theBuffer == null ) {
            copiedBuffer = null;
        } else {
            copiedBuffer = new ArrayList<>( theBuffer.size() );
            copiedBuffer.addAll( theBuffer );
        }

        return new BufferingCursorIterator<>(
                theChunkSize,
                theDelegate.createCopy(),
                theIndex,
                copiedBuffer,
                theDelegateIsBeforeStart,
                theDelegateIsAfterEnd,
                theEquals );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return hasNext( 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        return hasPrevious( 1 );
    }

    @Override
    public boolean hasNext(
            int n )
    {
        adjustBufferSoWeHaveNext( n );
        
        int have = theBuffer.size() - theIndex;
        return have >= n;
    }

    @Override
    public boolean hasPrevious(
            int n )
    {
        adjustBufferSoWeHavePrevious( n );
        
        int have = theIndex;
        return have >= n;
    }

    @Override
    public void moveBy(
            int n )
    {
        if( n == 0 ) {
            return;
        }

        if( theBuffer == null ) {
            theDelegate.moveBy( n );

        } else {
            if( n > 0 ) {
                if( theIndex + n <= theBuffer.size() ) {
                    // within the buffer
                    theIndex += n;
                    return;

                } else {
                    theDelegate.moveBy( n + theIndex - theBuffer.size() );
                }
            } else {
                if( theIndex + n >= 0 ) {
                    // within the buffer
                    theIndex += n;
                    return;
                } else {
                    theDelegate.moveBy( n - theIndex );
                }
            }
            theBuffer                = null;
            theIndex                 = 0;
            theDelegateIsBeforeStart = false;
            theDelegateIsAfterEnd    = false;
        }
    }

    @Override
    public int moveToBefore(
            E pos )
    {
        int ret = 0; // make compiler happy

        if( theBuffer == null ) {
            ret = theDelegate.moveToBefore( pos );

        } else {
            // we whether we have it in the buffer; we don't know whether we move forward or backward
            boolean found = false;
            for( int i=0 ; i<theBuffer.size() ; ++i ) {
                if( theEquals.test( pos, theBuffer.get( i ))) {
                    ret      = i - theIndex;
                    theIndex = i;
                    found    = true;
                    break;
                }
            }
            if( !found ) {
                int delegateRet = theDelegate.moveToBefore( pos );
                ret = delegateRet + theBuffer.size() - theIndex;
                
                theBuffer                = null;
                theIndex                 = 0;
                theDelegateIsBeforeStart = false;
                theDelegateIsAfterEnd    = false;
            }
        }
        
        return ret;
    }

    @Override
    public int moveToAfter(
            E pos )
    {
        int ret = 0; // make compiler happy

        if( theBuffer == null ) {
            ret = theDelegate.moveToAfter( pos );

        } else {
            // we whether we have it in the buffer; we don't know whether we move forward or backward
            boolean found = false;
            for( int i=0 ; i<theBuffer.size() ; ++i ) {
                if( theEquals.test( pos, theBuffer.get( i ))) {
                    ret      = (i+1) - theIndex;
                    theIndex = i+1;
                    found    = true;
                    break;
                }
            }
            if( !found ) {
                int delegateRet = theDelegate.moveToAfter( pos );
                ret = delegateRet + theBuffer.size() - theIndex;
                
                theBuffer                = null;
                theIndex                 = 0;
                theDelegateIsBeforeStart = false;
                theDelegateIsAfterEnd    = false;
            }
        }
        
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret;

        if( theBuffer == null ) {
            ret = theDelegate.moveToBeforeFirst();
            theDelegateIsBeforeStart = true;
            theDelegateIsAfterEnd    = false;

        } else if( theDelegateIsBeforeStart ) {
            ret                      = 0;
            theIndex                 = 0;
            theDelegateIsAfterEnd    = false;

        } else {
            ret       = theDelegate.moveToBeforeFirst();
            ret      -= theIndex;
            theBuffer = null;
            theIndex  = 0;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int ret;

        if( theBuffer == null ) {
            ret = theDelegate.moveToAfterLast();
            theDelegateIsBeforeStart = false;
            theDelegateIsAfterEnd    = true;

        } else if( theDelegateIsAfterEnd ) {
            ret                      = theBuffer.size() - theIndex;
            theIndex                 = theBuffer.size();
            theDelegateIsBeforeStart = false;

        } else {
            ret       = theDelegate.moveToAfterLast();
            ret      += theBuffer.size() - theIndex;
            theBuffer = null;
            theIndex  = 0;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<E> position )
    {
        clear();
        
        theDelegate.setPositionTo( position );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E nextElement()
    {
        return next();
    }

    /**
     * Helper to adjust the buffer so we have n more elements in forward direction (if possible)
     * 
     * @param n the number of elements to have in the buffer from the current position
     */
    protected void adjustBufferSoWeHaveNext(
            int n )
    {
        if( theBuffer == null || theIndex == theBuffer.size() ) {
            // don't have anything in the buffer that is relevant
            
            int     requestLength = Math.max( n, theChunkSize );
            List<E> foundElements = theDelegate.next( requestLength );
            int     foundNumber   = foundElements.size();
            
            theBuffer = new ArrayList<>( foundElements ); // copy
            theIndex  = 0;

            if( foundNumber < n ) {
                // at the end
                theDelegateIsBeforeStart = false;
                theDelegateIsAfterEnd    = true;

            } else {
                theDelegateIsBeforeStart = false;
                theDelegateIsAfterEnd    = false; // we don't know whether we are at the end
            }

        } else {
            // we have a buffer, and at least one element that we need
            if( theIndex + n <= theBuffer.size() ) {
                // can respond from the buffer -- do nothing

            } else {
                // have some in the buffer, but need to get more
                int stillToGet = n - ( theBuffer.size() - theIndex );
                
                int     requestLength = Math.max( stillToGet, theChunkSize );
                List<E> foundElements = theDelegate.next( requestLength );
                int     foundNumber   = foundElements.size();
                
                theBuffer.addAll( foundElements );

                if( foundNumber < stillToGet ) {
                    // at the end
                    theDelegateIsBeforeStart = false;
                    theDelegateIsAfterEnd    = true;

                } else if( foundNumber > stillToGet ) {
                    theDelegateIsBeforeStart = false;
                    theDelegateIsAfterEnd    = false;
                }
            }
        }
        // trimIfNeeded();
    }

    /**
     * Helper to adjust the buffer so we have n more elements in backwards direction (if possible)
     * 
     * @param n the number of elements to have in the buffer from the current position
     */
    void adjustBufferSoWeHavePrevious(
            int n )
    {
        if( theBuffer == null || theIndex == 0 ) {
            // don't have anything in the buffer that is relevant

            if( theBuffer != null ) {
                theDelegate.moveBy( -theBuffer.size() );
            }
            int     requestLength = Math.max( n, theChunkSize );
            List<E> foundElements = theDelegate.peekPrevious( requestLength );
            int     foundNumber   = foundElements.size();
            
            theBuffer = reverse( foundElements );
            theIndex  = theBuffer.size();

            if( foundNumber < n ) {
                // at the beginning
                theDelegateIsBeforeStart = true;
                theDelegateIsAfterEnd    = false;

            } else {
                theDelegateIsBeforeStart = false;
                theDelegateIsAfterEnd    = false;
            }

        } else {
            int haveNumber = theIndex;
            if( n <= haveNumber ) {
                // have already, nothing to do

            } else {
                // have some in the buffer, but need to get more
                int stillToGet = n - theIndex;

                CursorIterator<E> tmp = theDelegate.createCopy(); // keep the delegate where it is
                tmp.moveBy( -theBuffer.size() );

                int     requestLength = Math.max( stillToGet, theChunkSize );
                List<E> foundElements = tmp.previous( requestLength );
                int     foundNumber   = foundElements.size();
                
                List<E> newBuffer = reverse( foundElements );
                newBuffer.addAll( theBuffer );
                theBuffer = newBuffer;
                theIndex  += foundNumber;

                if( foundNumber < stillToGet ) {
                    theDelegateIsBeforeStart = true;
                    theDelegateIsAfterEnd    = false;

                } else {
                    theDelegateIsBeforeStart = false;
                    theDelegateIsAfterEnd    = false;
                }
            }
        }
        // trimIfNeeded();
    }


    /**
     * Trim the buffer if it is too long.
     */
    protected void trimIfNeeded()
    {
        if( theBuffer != null && theBuffer.size() > 2 * theChunkSize ) { // FUDGE factor
            // we assume we usually traverse forward

            List<E> newBuffer = new ArrayList<>( theChunkSize );
            
            int delegateInAdvance = theBuffer.size() - theIndex;
            if( delegateInAdvance > theChunkSize ) {
                newBuffer.addAll( theBuffer.subList( theIndex, theIndex + theChunkSize ));
                theDelegate.moveBy( theChunkSize - delegateInAdvance );
                theIndex = 0;

            } else {
                newBuffer.addAll( theBuffer.subList( theBuffer.size() - theChunkSize, theBuffer.size() ));
                theIndex -= theBuffer.size() - theChunkSize;
            }
            theBuffer = newBuffer;
        }
    }

    /**
     * Helper to reverse a list.
     * 
     * @param list the list
     * @return reversed list
     */
    protected ArrayList<E> reverse(
            List<E> list )
    {
        ArrayList<E> ret = new ArrayList<>( list.size() );
        for( int i=list.size()-1; i>=0 ; --i ) {
            ret.add( list.get( i ));
        }
        return ret;
    }

    /**
     * The chunk size.
     */
    protected final int theChunkSize;

    /**
     *The underlying CursorIterator
     */
    protected final CursorIterator<E> theDelegate;
    
    /**
     * The buffer.
     */
    protected List<E> theBuffer;
    
    /**
     * The current location in the buffer. This points to the element that will be returned by the next next().
     */
    protected int theIndex;

    /**
     * True if we know that the delegate is before the start. (We may be before the start but not know it)
     */
    protected boolean theDelegateIsBeforeStart;
    
    /**
     * True if we know that the delegate is after the end. (We may be after the end but not know it)
     */
    protected boolean theDelegateIsAfterEnd;

    /**
     * Equality operation for E's.
     */
    protected final BiPredicate<E,E> theEquals;

    /**
     * The default size of a chunk that we read from the delegate.
     */
    public static final int DEFAULT_CHUNK_SIZE = 16;
}
