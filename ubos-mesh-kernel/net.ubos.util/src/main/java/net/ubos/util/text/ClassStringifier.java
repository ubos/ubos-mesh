//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies an Java Class name.
 */
public class ClassStringifier
        extends
            AbstractStringifier<Class>
{
    /**
     * Factory method.
     *
     * @return the created ClassStringifier
     */
    public static ClassStringifier create()
    {
        return new ClassStringifier();
    }

    /**
     * No-op constructor. Use factory method.
     */
    protected ClassStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Class                 arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = escape( arg.getName() );
        ret = potentiallyShorten( ret, pars );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg == null ) {
            throw new NullPointerException();

        } else if( arg instanceof Class ) {
            return format( (Class) arg, pars, soFar );

        } else {
            return format( arg.getClass(), pars, soFar );
        }
    }

    /**
     * Overridable method to possibly escape a String first.
     *
     * @param s the String to be escaped
     * @return the escaped String
     */
    protected String escape(
            String s )
    {
        return s;
    }

    /**
     * Overridable method to possibly unescape a String first.
     *
     * @param s the String to be unescaped
     * @return the unescaped String
     */
    protected String unescape(
            String s )
    {
        return s;
    }
}
