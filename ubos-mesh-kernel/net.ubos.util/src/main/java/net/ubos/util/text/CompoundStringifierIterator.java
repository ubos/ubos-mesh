//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.ArrayFacade;
import net.ubos.util.ResourceHelper;

/**
 * Iterator for a CompoundStringifier. This is basically an iterative implementation of a
 * depth-first search algorithm similar to how PROLOG works.
 *
 * @param <T> the type of the Objects to be stringified
 */
class CompoundStringifierIterator
        implements
            Iterator<StringifierParsingChoice<ArrayFacade<Object>>>
{
    /**
     * Constructor.
     *
     * @param stringifier the CompoundStringifier to which this iterator belongs
     * @param components the CompoundStringifierComponent in the CompoundStringifier
     * @param rawString the String to parse
     * @param startIndex the start index (inclusive) of the String to parse
     * @param endIndex the end index (exclusive) of the String to parse
     * @param max the maximum number of elements to return
     * @param matchAll if true, only return StringifierParsingChoice that consume the entire String
     * @param factory the factory needed to create the parsed values, if any
     */
    public CompoundStringifierIterator(
            CompoundStringifier             stringifier,
            CompoundStringifierComponent [] components,
            String                          rawString,
            int                             startIndex,
            int                             endIndex,
            int                             max,
            boolean                         matchAll,
            StringifierUnformatFactory      factory )
    {
        theStringifier  = stringifier;
        theComponents   = components;
        theRawString    = rawString;
        theStartIndex   = startIndex;
        theEndIndex     = endIndex;
        theMax          = max;
        theMatchAll     = matchAll;
        theFactory      = factory;

        initializeIteratorsAndStatus();

        theDepth    = 0;
        theChildMax = Math.max( theMax, theResourceHelper.getResourceIntegerOrDefault( "ChildMax", 20 ) ); // 20 chars for Strings ...

        if( matchAll && theComponents.length == theDepth+1 ) { // first one is the last one
            theIterators[ theDepth ] = theComponents[theDepth].allParsingChoiceIterator(
                    theRawString,
                    0,
                    theRawString.length(),
                    theChildMax,
                    theFactory );
        } else {
            theIterators[ theDepth ] = theComponents[theDepth].partialParsingChoiceIterator(
                    theRawString,
                    0,
                    theRawString.length(),
                    theChildMax,
                    theFactory );
        }
        goNext();
    }

    /**
     * Internal helper method to avoid plastering the SuppressWarnings all over the place.
     */
    @SuppressWarnings(value={"unchecked"})
    protected void initializeIteratorsAndStatus()
    {
        theIterators = new Iterator[theComponents.length];
        theStatus    = new StringifierParsingChoice[theComponents.length];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return theNext != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringifierParsingChoice<ArrayFacade<Object>> next()
    {
        StringifierParsingChoice<ArrayFacade<Object>> ret = theNext;

        goNext();

        return ret;
    }

    /**
     * Iterate to the next element.
     */
    protected void goNext()
    {
        while( theDepth >=0 ) {
            while( theIterators[theDepth] != null && theIterators[theDepth].hasNext() ) {
                theStatus[theDepth] = theIterators[theDepth].next();

                if( theDepth == theComponents.length-1 ) {
                    // last component
                    if( !theMatchAll || theStatus[theDepth].getEndIndex() == theRawString.length() ) {
                        // found
                        theNext = new CompoundStringifierChildChoice(
                                theStringifier,
                                theStatus.clone(),
                                0,
                                theRawString.length() );
                        return;
                    }
                }
                if( theDepth < theComponents.length-1 ) {
                    ++theDepth;
                    if( theMatchAll && ( theDepth == theIterators.length-1 ) ) {
                        theIterators[ theDepth ] = theComponents[theDepth].allParsingChoiceIterator(
                                theRawString,
                                theStatus[theDepth-1].getEndIndex(),
                                theRawString.length(),
                                theChildMax,
                                theFactory );
                    } else {
                        theIterators[ theDepth ] = theComponents[theDepth].partialParsingChoiceIterator(
                                theRawString,
                                theStatus[theDepth-1].getEndIndex(),
                                theRawString.length(),
                                theChildMax,
                                theFactory );
                    }
                }
            }
            --theDepth;
        }
        theNext = null;
    }

    /**
     * Always throws UnsupportedOperationException.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * The Stringifier this Iterator belongs to. Making this explicit seems easier than a non-static inner class
     * with all these generics.
     */
    protected final CompoundStringifier theStringifier;

    /**
     * The CompoundStringifierComponent inside the stringifier to which this Iterator belongs to.
     */
    protected final CompoundStringifierComponent [] theComponents;

    /**
     * The String being parsed.
     */
    protected final String theRawString;

    /**
     * The start index in the String from where to parse (inclusive).
     */
    protected final int theStartIndex;

    /**
     * The end index in the String to where to parse (exclusive).
     */
    protected final int theEndIndex;

    /**
     * The maximum number of elements to return.
     */
    protected final int theMax;

    /**
     * The maximum number of elements to ask our children for.
     */
    protected final int theChildMax;

    /**
     * If true, return only matches that match the entire String between start and end.
     */
    protected final boolean theMatchAll;

    /**
     * The factory needed to create the parsed values, if any.
     */
    protected final StringifierUnformatFactory theFactory;

    /**
     * Reflects the current depth of the state of the search algorithm.
     */
    protected int theDepth;

    /**
     * The current child iterators, ordered from left to right.
     * These are all "partial" (not "all"), except for the last one, which matches the overall
     * "partial" vs "all": it's the last one's responsibility to pick up what's left.
     */
    protected Iterator<? extends StringifierParsingChoice<?>> [] theIterators;

    /**
     * The most-recently returned result of the child iterators, in same sequence as theIterators.
     */
    protected StringifierParsingChoice [] theStatus;

    /**
     * The next result to return from this Iterator.
     */
    protected StringifierParsingChoice<ArrayFacade<Object>> theNext;

    /**
     * The ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( CompoundStringifierIterator.class );
}
