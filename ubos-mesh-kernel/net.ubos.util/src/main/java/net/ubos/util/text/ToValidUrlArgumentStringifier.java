//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.UrlUtils;

/**
 * Stringifies a String by escaping all characters necessary to make the String a valid URL argument.
 * For example, it replaces ? and &.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class ToValidUrlArgumentStringifier<T>
        extends
            AbstractDelegatingStringifier<T>
{
    /**
     * Factory method.
     *
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @return the created StringStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ToValidUrlArgumentStringifier<T> create(
            Stringifier<T> delegate )
    {
        return new ToValidUrlArgumentStringifier<>( delegate );
    }

    /**
     * Private constructor. Use factory method.
     *
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     */
    protected ToValidUrlArgumentStringifier(
            Stringifier<T> delegate )
    {
        super( delegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escape(
            String s )
    {
        String ret = UrlUtils.encodeToValidUrlArgument( s );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String unescape(
            String s )
    {
        String ret = UrlUtils.decodeUrlArgument( s );
        return ret;
    }
}
