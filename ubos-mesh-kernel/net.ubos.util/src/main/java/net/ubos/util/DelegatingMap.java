//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A Map that delegates to other Maps.
 *
 * @param <K> the key type
 * @param <V> the value type
 */
public class DelegatingMap<K,V>
    implements
        Map<K,V>
{
    /**
     * Factory method for any number of delegates.
     *
     * @param delegates the Maps to which this Map delegates
     * @return the created DelegatingMap
     * @param <K> the key type
     * @param <V> the value type
     */
    public static <K,V> DelegatingMap<K,V> create(
            List<Map<K,V>> delegates )
    {
        return new DelegatingMap<>( delegates );
    }

    /**
     * Factory method for any numebr of delegates.
     *
     * @param delegates the Maps to which this Map delegates
     * @return the created DelegatingMap
     * @param <K> the key type
     * @param <V> the value type
     */
    public static <K,V> DelegatingMap<K,V> create(
            Map<K,V> [] delegates )
    {
        ArrayList<Map<K,V>> delegates2 = new ArrayList<>( delegates.length );
        for( int i=0 ; i<delegates.length ; ++i ) {
            delegates2.add( delegates[i] );
        }
        return new DelegatingMap<>( delegates2 );
    }

    /**
     * Factory method for two delegates.
     *
     * @param del1 the first delegate
     * @param del2 the second delegate
     * @return the created DelegatingMap
     * @param <K> the key type
     * @param <V> the value type
     */
    @SuppressWarnings("unchecked")
    public static <K,V> DelegatingMap<K,V> create(
            Map<K,V> del1,
            Map<K,V> del2 )
    {
        ArrayList<Map<K,V>> delegates2 = new ArrayList<>( 2 );
        delegates2.add( del1 );
        delegates2.add( del2 );
        return new DelegatingMap<>( delegates2 );
    }

    /**
     * Constructor.
     *
     * @param delegates the Maps to which this Map delegates
     */
    protected DelegatingMap(
            List<Map<K,V>> delegates )
    {
        theDelegates = delegates;
    }

    /**
     * Obtain the delegates.
     *
     * @return the delegates
     */
    public List<Map<K,V>> getDelegates()
    {
        return theDelegates;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public int size()
    {
        int ret = 0;
        for( Map<K,V> current : theDelegates ) {
            ret += current.size();
        }
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean isEmpty()
    {
        for( Map<K,V> current : theDelegates ) {
            if( !current.isEmpty() ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean containsKey(
            Object key )
    {
        for( Map<K,V> current : theDelegates ) {
            if( current.containsKey( key )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean containsValue(
            Object value )
    {
        for( Map<K,V> current : theDelegates ) {
            if( current.containsValue( value )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V get(
            Object key )
    {
        for( Map<K,V> current : theDelegates ) {
            V ret = current.get( key );
            if( ret != null ) {
                return ret;
            }
        }
        return null;
    }

    // Modification Operations

    /**
     * {@inheritDoc]
     */
    @Override
    public V put(
            K key,
            V value )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V remove(
            Object key )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void putAll(
            Map<? extends K, ? extends V> t )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void clear()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public Set<K> keySet()
    {
        List<Set<K>> newDelegates = new ArrayList<>( theDelegates.size() );

        for( Map<K,V> current : theDelegates ) {
            newDelegates.add( current.keySet() );
        }
        return DelegatingSet.create( newDelegates );
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public Collection<V> values()
    {
        List<Collection<V>> newDelegates = new ArrayList<>( theDelegates.size() );

        for( Map<K,V> current : theDelegates ) {
            newDelegates.add( current.values() );
        }
        return DelegatingCollection.create( newDelegates );
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public Set<Map.Entry<K,V>> entrySet()
    {
        List<Set<Map.Entry<K,V>>> newDelegates = new ArrayList<>( theDelegates.size() );

        for( Map<K,V> current : theDelegates ) {
            newDelegates.add( current.entrySet() );
        }
        return DelegatingSet.create( newDelegates );
    }

    /**
     * The Maps to which this Map delegates.
     */
    protected List<Map<K,V>> theDelegates;
}
