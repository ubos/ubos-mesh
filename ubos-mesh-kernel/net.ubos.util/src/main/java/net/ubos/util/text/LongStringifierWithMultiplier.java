//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;

/**
 * Stringifies a single Long, supporting suitable multipliers such as kilo, Mega, etc.
 * Overriding NumberStringifier does not seem to work.
 */
public class LongStringifierWithMultiplier
        extends
            AbstractStringifier<Long>
{
    /**
     * Factory method.
     *
     * @return the created LongStringifier
     */
    public static LongStringifierWithMultiplier create()
    {
        return new LongStringifierWithMultiplier( true );
    }

    /**
     * Constructor. Use factory method.
     *
     * @param useThousands if true, consider prefixes multiples of 1000; if false, of 1024.
     */
    protected LongStringifierWithMultiplier(
            boolean useThousands )
    {
        theUseThousands = useThousands;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Long                  arg,
            StringifierParameters pars,
            String                soFar )
    {
        // from http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java

        int unit = theUseThousands ? 1000 : 1024;
        String ret;

        if( arg < unit ) {
            ret = String.valueOf( arg ) + " "; // no prefix, but for consistent formatting

        } else {
            int exp = (int) (Math.log( arg ) / Math.log( unit ));
            String pre;
            if( theUseThousands ) {
                pre = theThousandsMultipliers[exp-1];
            } else {
                pre = theTwoToTheTenMultipliers[exp-1];
            }
            double mant     = arg / Math.pow( unit, exp );
            long   mantLong = (long) ( mant + .5d );

            if( mantLong < 100 ) {
                ret = String.format( "%.1f %s", mant, pre );
            } else {
                ret = String.format( "%d %s", mantLong, pre );

            }
        }

        ret = potentiallyShorten( ret, pars );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg instanceof Short ) {
            return format( ((Short)arg).longValue(), pars, soFar );
        } else if( arg instanceof Long ) {
            return format( ((Long)arg), pars, soFar );
        } else {
            return format( ((Integer)arg).longValue(), pars, soFar );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        Matcher m = thePattern.matcher( rawString );
        if( !m.matches() ) {
            throw new StringifierParseException( this, rawString );
        }
        String beforePeriodString = m.group( 1 );
        String afterPeriodString  = m.group( 2 );

        long   beforePeriod = ( beforePeriodString != null && !beforePeriodString.isEmpty() ) ? Integer.parseInt( beforePeriodString ) : 0L;
        long   afterPeriod  = ( afterPeriodString  != null && !afterPeriodString.isEmpty()  ) ? Integer.parseInt( afterPeriodString )  : 0L;
        String prefix       = m.group( 3 );

        if( prefix == null || prefix.isEmpty() ) {
            return beforePeriod;
        }

        double n = Double.parseDouble( String.format( "%d.%d", beforePeriod, afterPeriod ) );

        long factor = 1000L;
        for( int i=0 ; i<theThousandsMultipliers.length ; ++i ) {
            if( theThousandsMultipliers[i].equals( prefix )) {
                return (long) ( n * factor + .5d );
            }
            factor *= 1000L;
        }

        factor = 1024L;
        for( int i=0 ; i<theTwoToTheTenMultipliers.length ; ++i ) {
            if( theTwoToTheTenMultipliers[i].equals( prefix )) {
                return (long) ( n * factor + .5d );
            }
            factor *= 1024L;
        }
        throw new StringifierParseException(  this, rawString );
    }

    /**
     * If true, consider prefixes multipliers of 1000.
     * If false, of 1024.
     */
    protected boolean theUseThousands;

    /**
     * Supported thousands multipliers.
     */
    protected static final String [] theThousandsMultipliers = {
        "k",
        "M",
        "G",
        "T"
    };

    /**
     * Supported 2**10 multipliers.
     */
    protected static final String [] theTwoToTheTenMultipliers = {
        "Ki",
        "Mi",
        "Gi",
        "Ti"
    };

    /**
     * Regular expression that makes it easier to parse.
     */
    protected static final Pattern thePattern = Pattern.compile(
            "^\\s*(\\d*)(?:\\.(\\d*))?(?:\\s*("
            + ArrayHelper.join( "|", theThousandsMultipliers )
            + "|"
            + ArrayHelper.join( "|", theTwoToTheTenMultipliers )
            + "))?\\s*$" );
}
