//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import net.ubos.util.listenerset.FlexibleListenerSet;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SmartMap;

/**
 * Factors out functionality common to History implementation that swap their
 * content in and out.
 *
 * @param <E> the type of thing for which this is a history
 */
public abstract class AbstractSwappingHistory<E extends HasTimeUpdated>
    extends
        AbstractHistory<E>
    implements
        SwappingHistory<E>
{
    private static final Log log = Log.getLogInstance( AbstractSwappingHistory.class );

    /**
     * Constructor.
     *
     * @param localCache the local cache implementation
     */
    protected AbstractSwappingHistory(
            SmartMap<Long,ReferenceWithTimeUpdated<E>> localCache )
    {
        theLocalCache = localCache;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearLocalCache()
    {
        theLocalCache.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsAt(
            long t )
    {
        E value = at( t );
        return value != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized E at(
            long t )
    {
        cleanup();

        ReferenceWithTimeUpdated<E> found = theLocalCache.get( t );
        E ret = found != null ? found.get() : null;

        if( ret == null ) {
            ret = loadValueFromStorage( t );

            if( ret != null ) {
                theLocalCache.put( t, createReference( ret ));
                fireValueLoaded( ret );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void putIgnorePrevious(
            E value )
    {
        cleanup();
        theLocalCache.put( value.getTimeUpdated(),  createReference( value ));

        saveValueToStorage( value );
        fireValueSaved( value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void removeIgnorePrevious(
            long t )
    {
        cleanup();
        theLocalCache.remove( t );

        removeValueFromStorage( t );
        fireValueRemoved( t );
    }

    /**
     * Factory method for a subclass of Reference.
     *
     * @param value the value
     * @return the Reference to the value
     */
    protected abstract ReferenceWithTimeUpdated<E> createReference(
            E value );

    /**
     * Clean up deleted references.
     */
    @SuppressWarnings(value={"unchecked"})
    protected void cleanup()
    {
        while( true ) {
            Reference<? extends E> current = theQueue.poll();
            if( current == null ) {
                break;
            }

            ReferenceWithTimeUpdated<E> realCurrent = (ReferenceWithTimeUpdated<E>) current;

            theLocalCache.remove( realCurrent.getTimeUpdated() );
            fireValuePurged( realCurrent.getTimeUpdated() );
        }
    }

    /**
     * This method is defined by subclasses, to swap in a value that currently
     * is not contained in the local cache.
     *
     * @param t the key whose value should be loaded
     * @return the value that was loaded, or null if none.
     */
    protected abstract E loadValueFromStorage(
            long t );

    /**
     * This method is defined by subclasses, to save a value to storage.
     *
     * @param newValue the new value
     */
    protected abstract void saveValueToStorage(
            E newValue );

    /**
     * This method is defined by subclasses, to remove a value from storage.
     *
     * @param t the key whose value has been removed
     */
    protected abstract void removeValueFromStorage(
            long t );

    /**
     * Fire an "an entry has been purged" event.
     *
     * @param value the timeUpdated whose value has been purged
     */
    protected void fireValuePurged(
            long timeUpdated )
    {
        theListeners.fireEvent( timeUpdated, 0 );
    }

    /**
     * Fire an "a value has been loaded from storage" event.
     *
     * @param value the value that has been loaded
     */
    protected void fireValueLoaded(
            E value )
    {
        theListeners.fireEvent( value, 1 );
    }

    /**
     * Fire an "a value has been saved to storage" event.
     *
     * @param value the value that has been saved
     */
    protected void fireValueSaved(
            E value )
    {
        theListeners.fireEvent( value, 2 );
    }

    /**
     * Fire a "a value has been removed from storage" event.
     *
     * @param t the time whose value has been removed
     */
    protected final void fireValueRemoved(
            long t )
    {
        theListeners.fireEvent( t, 3 );
    }

    /**
     * The local cache.
     */
    protected final SmartMap<Long,ReferenceWithTimeUpdated<E>> theLocalCache;

    /**
     * The queue that into which deleted References are inserted.
     */
    protected final ReferenceQueue<E> theQueue = new ReferenceQueue<>();


    /**
     * Listeners to what is happening with this SmartMap.
     */
    private final FlexibleListenerSet<SwappingHistoryListener<E>,Object,Integer> theListeners
            = new FlexibleListenerSet<SwappingHistoryListener<E>,Object,Integer>() {
                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    @SuppressWarnings("unchecked")
                    protected void fireEventToListener(
                            SwappingHistoryListener<E> listener,
                            Object                     event,
                            Integer                    parameter )
                    {
                        switch( parameter ) {
                            case 0:
                                listener.purged( AbstractSwappingHistory.this, (Long) event );
                                break;
                            case 1:
                                listener.loadedFromStorage( AbstractSwappingHistory.this, (E) event );
                                break;
                            case 2:
                                listener.savedToStorage( AbstractSwappingHistory.this, (E) event );
                                break;
                            case 3:
                                listener.removedFromStorage( AbstractSwappingHistory.this, (Long) event );
                                break;
                            default:
                                log.error( "should not be here: " + parameter );
                        }
                    }
            };

    /**
     * This interface is supported by the WeakReference and SoftReference classes defined below.
     * It should really inherit from Reference, but then, Java won't let us.
     *
     * @param <E>
     */
    public static interface ReferenceWithTimeUpdated<E>
        extends
            HasTimeUpdated
    {
        /**
         * Duplicated from Reference.
         *
         * @return the referent
         */
        public E get();
    }

    /**
     * A SoftReference that also carries a TimeUpdated. We need this so we know the timeUpdated into the
     * history whose values has just been garbage-collected.
     */
    public static class SoftReferenceWithTimeUpdated<E>
        extends
            SoftReference<E>
        implements
            ReferenceWithTimeUpdated<E>
    {
        /**
         * Constructor.
         *
         * @param referent the target of the reference
         * @param queue collecting garbage-collected references
         * @param timeUpdated the time updated.
         */
        public SoftReferenceWithTimeUpdated(
                E                         referent,
                ReferenceQueue<? super E> queue,
                long                      timeUpdated )
        {
            super( referent, queue );

            theTimeUpdated = timeUpdated;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public long getTimeUpdated()
        {
            return theTimeUpdated;
        }

        /**
         * The timeUpdated.
         */
        private final long theTimeUpdated;
    }

    /**
     * A WeakReference that also carries a TimeUpdated. We need this so we know the timeUpdated into the
     * history whose values has just been garbage-collected.
     */
    public static class WeakReferenceWithTimeUpdated<E>
        extends
            WeakReference<E>
        implements
            ReferenceWithTimeUpdated<E>
    {
        /**
         * Constructor.
         *
         * @param referent the target of the reference
         * @param queue collecting garbage-collected references
         * @param timeUpdated the time updated.
         */
        public WeakReferenceWithTimeUpdated(
                E                         referent,
                ReferenceQueue<? super E> queue,
                long                      timeUpdated )
        {
            super( referent, queue );

            theTimeUpdated = timeUpdated;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public long getTimeUpdated()
        {
            return theTimeUpdated;
        }

        /**
         * The timeUpdated.
         */
        private final long theTimeUpdated;
    }
}
