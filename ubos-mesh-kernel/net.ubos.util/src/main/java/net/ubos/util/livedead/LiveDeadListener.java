//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.livedead;

/**
 * This interface is supported by objects who want to notified when another object dies.
 */
public interface LiveDeadListener
{
    /**
      * Indicates that a death has occured.
      *
      * @param theObjectThatDied the LiveDeadObject that died
      */
    public void objectDied(
            LiveDeadObject theObjectThatDied );
}
