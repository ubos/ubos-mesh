//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap;

/**
 * Factors out functionality common to SmartMap implementations.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public abstract class AbstractSmartMap<K,V>
    implements
        SmartMap<K,V>
{
    /**
     * Constructor.
     */
    protected AbstractSmartMap()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        return size() == 0;
    }

    /**
     * Default implementation does nothing.
     *
     * @param key
     * @param value
     */
    @Override
    public void valueUpdated(
        K key,
        V value )
    {}
}
