//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.StringHelper;

/**
 * Takes the output of another Stringifier and makes it valid HTML. For example, this replaces
 * <code>&gt;</code> with <code>&amp;gt;</code>.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class HtmlifyingDelegatingStringifier<T>
        extends
            AbstractDelegatingStringifier<T>
{
    /**
     * Factory method.
     *
     * @return the created HtmlifyingDelegatingStringifier
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> HtmlifyingDelegatingStringifier<T> create(
            Stringifier<T> delegate )
    {
        return new HtmlifyingDelegatingStringifier<>( delegate, null );
    }

    /**
     * Factory method.
     *
     * @return the created HtmlifyingDelegatingStringifier
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @param enclosingTag if non-null, only htmlify what is necessary for "replaceable character data", per W3C.
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> HtmlifyingDelegatingStringifier<T> create(
            Stringifier<T> delegate,
            String         enclosingTag )
    {
        return new HtmlifyingDelegatingStringifier<>( delegate, enclosingTag );
    }

    /**
     * No-op constructor. Use factory method.
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @param enclosingTag if non-null, only htmlify what is necessary for "replaceable character data", per W3C.
     */
    protected HtmlifyingDelegatingStringifier(
            Stringifier<T> delegate,
            String         enclosingTag )
    {
        super( delegate );

        theEnclosingTag = enclosingTag;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escape(
            String s )
    {
        String ret;
        if( theEnclosingTag != null ) {
            ret = StringHelper.stringToReplaceableCharacterDataHtml( s, theEnclosingTag );
        } else {
            ret = StringHelper.stringToHtml( s );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String unescape(
            String s )
    {
        String ret;
        if( theEnclosingTag != null ) {
            ret = StringHelper.replaceableCharacterDataHtmlToString( s, theEnclosingTag );
        } else {
            ret = StringHelper.htmlToString( s );
        }
        return ret;
    }

    /**
     * The enclosing tag, in case of "replaceable character data" mode
     */
    protected final String theEnclosingTag;
}
