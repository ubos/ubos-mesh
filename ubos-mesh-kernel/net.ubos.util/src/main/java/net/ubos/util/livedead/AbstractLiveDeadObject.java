//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.livedead;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * This is a simple, abstract implementation of {@link LiveDeadObject}
 * that can be used as "mixin".
 */
public abstract class AbstractLiveDeadObject
    implements
        LiveDeadObject
{
    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void die()
        throws
            IsDeadException;

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isDead()
    {
        return isDead;
    }

    /**
      * Simple checking method to create IsDeadExceptions when necessary.
      *
      * @throws IsDeadException thrown if this LiveDeadObject is dead already; do nothing otherwise
      */
    public final void checkDead()
        throws
            IsDeadException
    {
        if( isDead ) {
            throw new IsDeadException( this );
        }
    }

    /**
     * This convenience method for our subclasses will set the isDead flag to false,
     * and continue, or throw an exception if we are dead already.
     *
     * @throws IsDeadException thrown if this object is dead already
     */
    protected synchronized void makeDead()
        throws
            IsDeadException
    {
        checkDead();

        isDead = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void addLiveDeadListener(
            LiveDeadListener newListener )
    {
        if( theLiveDeadListeners == null ) {
            theLiveDeadListeners = new ArrayList<>();
        }
        theLiveDeadListeners.add( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void removeLiveDeadListener(
            LiveDeadListener oldListener )
    {
        theLiveDeadListeners.remove( oldListener );
    }

    /**
     * This allows subclasses to fire off an event "we died".
     */
    protected final void fireObjectDied()
    {
        Iterator<LiveDeadListener> theIter;
        synchronized( this ) {
            if( theLiveDeadListeners == null || theLiveDeadListeners.isEmpty() ) {
                return;
            }

            theIter = new ArrayList<>( theLiveDeadListeners ).iterator();
        }

        while( theIter.hasNext() ) {
            LiveDeadListener current = theIter.next();
            current.objectDied( this );
        }
    }

    /**
     * Flag indicating whether we are dead already.
     */
    private boolean isDead;

    /**
     * The current list of listeners to our live dead state, allocated as needed.
     */
    private ArrayList<LiveDeadListener> theLiveDeadListeners = null;
}
