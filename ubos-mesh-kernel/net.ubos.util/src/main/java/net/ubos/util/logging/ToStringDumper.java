//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import net.ubos.util.ResourceHelper;
import net.ubos.util.StringHelper;

/**
 * A simple implementation of Dumper that dumps to a String.
 */
public class ToStringDumper
        extends
            AbstractDumper
        implements
            BufferingDumper
{
    /**
     * Factory method.
     *
     * @return the created ToStringDumper
     */
    public static ToStringDumper create()
    {
        return new ToStringDumper( DEFAULT_MAXLEVEL, DEFAULT_MAXARRAYLENGTH );
    }

    /**
     * Factory method.
     *
     * @param maxLevel the number of object levels to dump
     * @param maxArrayLength the maximum number of array entries to dump
     * @return the created ToStringDumper
     */
    public static ToStringDumper create(
            int maxLevel,
            int maxArrayLength )
    {
        return new ToStringDumper( maxLevel, maxArrayLength );
    }

    /**
     * Constructor.
     *
     * @param maxLevel the number of object levels to dump
     * @param maxArrayLength the maximum number of array entries to dump
     */
    protected ToStringDumper(
            int maxLevel,
            int maxArrayLength )
    {
        super( maxLevel, maxArrayLength );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset()
    {
        theBuffer        = new StringBuilder();
        theAlreadyDumped = new HashSet<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getBuffer()
    {
        return theBuffer.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void emit(
            CharSequence toEmit )
    {
        CharSequence indented = StringHelper.indent( toEmit, INDENT, 0, theLevel );
        theBuffer.append( indented );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void emit(
            char toEmit )
    {
        theBuffer.append( toEmit );

        if( toEmit == '\n' ) {
            for( int i=0 ; i<theLevel ; ++i ) {
                theBuffer.append( INDENT );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean shouldBeDumpedFull(
            Object obj )
    {
        if( obj == null ) {
            return true;

        } else if( theLevel >= theMaxLevel ) {
            return false;

        } else if( theAlreadyDumped.contains( obj )) {
            return false;

        } else if( isEmptyCollection( obj )) {
            return false;

        } else {
            return true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean shouldBeDumpedShort(
            Object obj )
    {
        if( isEmptyCollection( obj )) {
            return false;
        }
        return true;
    }

    /**
     * Determine whether the passed object is an empty collection and thus should not be dumped.
     *
     * @param obj the Object to be tested
     * @return true if this is an empty collection
     */
    protected boolean isEmptyCollection(
            Object obj )
    {
        if( obj instanceof Collection ) {
            Collection realObj = (Collection) obj;
            return realObj.isEmpty();
        }
        if( obj instanceof Object [] ) {
            Object [] realObj = (Object []) obj;
            return realObj.length == 0;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void registerAsDumped(
            Object obj )
    {
        if( obj != null ) {
            theAlreadyDumped.add( obj );
        }
    }

    /**
     * The internal buffer.
     */
    protected StringBuilder theBuffer = new StringBuilder();

    /**
     * The Objects dumped so far.
     */
    protected Set<Object> theAlreadyDumped = new HashSet<>();

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( ToStringDumper.class );

    /**
     * The character String by which child objects are indented.
     */
    public static final String INDENT = theResourceHelper.getResourceStringOrDefault( "Indent", "    " );

    /**
     * The default maxLevel.
     */
    public static final int DEFAULT_MAXLEVEL = theResourceHelper.getResourceIntegerOrDefault( "DefaultMaxLevel", 6 );
    
    /**
     * The default maxArrayLength.
     */
    public static final int DEFAULT_MAXARRAYLENGTH = theResourceHelper.getResourceIntegerOrDefault( "DefaultMaxArrayLength", 8 );
}
