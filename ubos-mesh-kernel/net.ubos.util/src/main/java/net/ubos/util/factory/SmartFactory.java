//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.factory;

import net.ubos.util.exception.ObjectExistsAlreadyFactoryException;
import net.ubos.util.nameserver.WritableNameServer;

/**
 * This interface is implemented by objects that support the smart factory pattern.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 * @param <A> the type of argument
 */
public interface SmartFactory<K,V,A>
        extends
            Factory<K,V,A>,
            WritableNameServer<K,V>
{
    /**
     * Factory method. This method will only be successful if the SmartFactory does not have
     * an object with the key yet; otherwise it throws an ObjectExistsAlreadyFactoryException.
     *
     * @param key the key information required for object creation, if any
     * @param argument any argument-style information required for object creation, if any
     * @return the created object
     * @throws ObjectExistsAlreadyFactoryException an object with this key existed already
     * @throws FactoryException catch-all Exception, consider its cause
     */
    public abstract V obtainNewFor(
            K key,
            A argument )
        throws
            ObjectExistsAlreadyFactoryException,
            FactoryException;

    /**
     * Factory method. This method will only be successful if the SmartFactory does not have
     * an object with the key yet; otherwise it throws an ObjectExistsAlreadyFactoryException.
     * This is equivalent to specifying a null argument.
     *
     * @param key the key information required for object creation, if any
     * @return the created object
     * @throws ObjectExistsAlreadyFactoryException an object with this key existed already
     * @throws FactoryException catch-all Exception, consider its cause
     */
    public default V obtainNewFor(
            K key )
        throws
            ObjectExistsAlreadyFactoryException,
            FactoryException
    {
        return obtainNewFor( key, null );
    }
}
