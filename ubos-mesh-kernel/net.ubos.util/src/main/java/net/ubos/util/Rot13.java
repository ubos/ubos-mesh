//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

/**
 * Simplistic obfuscation via Rot13.
 */
public abstract class Rot13
{
    /**
     * Private constructor to prevent subclassing.
     */
    private Rot13()
    {
        // no op
    }
    
    /**
     * Perform a Rot13 operation
     * 
     * @param in the input String
     * @return return output String
     */
    public static String rot(
            String in )
    {
        StringBuilder tempReturn = new StringBuilder();

        for( int i=0; i < in.length(); ++i ) {
            int abyte = in.charAt(i);
            int cap   = abyte & 32;
            abyte &= ~cap;
            abyte = ((abyte >= 'A') && (abyte <= 'Z') ? ((abyte - 'A' + 13) % 26 + 'A') : abyte) | cap;
            tempReturn.append((char) abyte);
        }
        return tempReturn.toString();
    }
}
