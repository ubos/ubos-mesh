//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

import net.ubos.util.ResourceHelper;

/**
 * <p>This Exception is thrown when a Thread is waiting for some sort of result
 * to a query which requires communication with a remote entity, and
 * the query times out. This Exception is abstract, but two concrete implementations
 * are provided as inner classes.</p>
 *
 * <p>It implements PartialResultException, as it is possible that parts
 * of the results are already available by the time the query times out.</p>
 *
 * <p>We'd really like to make this a parameterized class, but apparently the
 * Java spec does not like that ("A generic class may not extend java.lang.Throwable").</p>
 */
public abstract class RemoteQueryTimeoutException
        extends
            AbstractLocalizedException
        implements
            PartialResultException
{
    /**
     * Constructor.
     *
     * @param localProxy the local proxy object which is the local endpoint of the query
     * @param havePartialResult if true, we have a partial result
     * @param partialResult the partial result
     */
    protected RemoteQueryTimeoutException(
            Object  localProxy,
            boolean havePartialResult,
            Object  partialResult )
    {
        theLocalProxy        = localProxy;
        theHavePartialResult = havePartialResult;
        thePartialResult     = partialResult;
    }

    /**
     * Obtain the local proxy Object which is the local endpoint of the query.
     *
     * @return the local proxy Object
     */
    public Object getLocalProxy()
    {
        return theLocalProxy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPartialResultAvailable()
    {
        return theHavePartialResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getBestEffortResult()
    {
        return thePartialResult;
    }

    /**
     * Determine whether the complete result is now available.
     *
     * @return true of the complete result is now available
     */
    public boolean isCompleteResultAvailable()
    {
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceHelper findResourceHelperForLocalizedMessage()
    {
        return ResourceHelper.getInstance( RemoteQueryTimeoutException.class );
    }

    /**
     * The local proxy object.
     */
    protected Object theLocalProxy;

    /**
     * Flag indicating whether we have a partial result.
     */
    protected boolean theHavePartialResult;

    /**
     * The partial result (if any).
     */
    protected Object thePartialResult;

    /**
     * This subclass of RemoteTimeoutQueryException is being instantiated with
     * a static partial result object, if any.
     */
    public static class QueryIsOver
            extends
                RemoteQueryTimeoutException
    {
        /**
         * Create one.
         *
         * @param localProxy the local proxy object which is the local endpoint of the query
         * @param havePartialResult if true, we have a partial result
         * @param partialResult the partial result
         */
        public QueryIsOver(
                Object  localProxy,
                boolean havePartialResult,
                Object  partialResult )
        {
            super( localProxy, havePartialResult, partialResult );
        }
    }

    /**
     * This subclass of RemoteTimeoutQueryException may obtain a partial, or even
     * complete result, after it has been thrown.
     */
    public static class QueryIsOngoing
            extends
                RemoteQueryTimeoutException
    {
        /**
         * Create one with the partial result of the query at the time of creation, if any.
         * This may change over time from havePartialResult=false to havePartialResult=true
         * and then to haveCompleteResult=true by invocations of the local methods after
         * the constructor has been invoked, as results come in.
         *
         * @param localProxy the local proxy object which is the local endpoint of the query
         * @param havePartialResult if true, we have a partial result
         * @param partialResult the partial result
         */
        public QueryIsOngoing(
                Object  localProxy,
                boolean havePartialResult,
                Object  partialResult )
        {
            super( localProxy, havePartialResult, partialResult );
        }

        /**
         * This can be invoked by the entity that previously threw this Exception
         * in order to indicate that a more complete partial result is now available.
         *
         * @param newResult a new result Object replacing the old partial result
         */
        public void haveMoreCompletePartialResult(
                Object newResult )
        {
            theHavePartialResult = true;
            thePartialResult     = newResult;
        }

        /**
         * This can be invoked by the entity that previous threw this Exception
         * in order to indicate that the complete result is now available.
         *
         * @param newResult the new result Object replacing the old partial result
         */
        public void haveCompleteResult(
                Object newResult )
        {
            theHavePartialResult  = true;
            theHaveCompleteResult = true;
            thePartialResult      = newResult;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isCompleteResultAvailable()
        {
            return theHaveCompleteResult;
        }

        /**
         * Flag set to true when the complete result has arrived.
         */
        protected boolean theHaveCompleteResult = false;
    }
}
