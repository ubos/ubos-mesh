//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

/**
 * A more fully-featured version of <code>java.util.Iterator</code> that can move
 * backwards just as well as forwards, and take N steps at a time instead of just one.
 *
 * @param <E> the type of element to iterate over
 */
public interface CursorIterator<E>
        extends
            CursorIteratorMove<E>,
            Iterator<E>, // Iterator's methods are in-lined here as well, to show the symmetry in the API re next and previous
            Enumeration<E>,
            CursorIterable<E> // allow the CursorIterator to be used wherever a Iterable is required, e.g. in the for( x : y ) loops
{
    /**
     * Obtain the next element, without iterating forward.
     *
     * @return the next element
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    public E peekNext()
        throws
            NoSuchElementException;

    /**
     * Obtain the previous element, without iterating backward.
     *
     * @return the previous element
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    public E peekPrevious()
        throws
            NoSuchElementException;

    /**
     * Obtain the next N elements, without iterating forward. If fewer than N elements are available, return
     * as many elements are available in a shorter array.
     *
     * @param n the number of elements to return
     * @return the next no more than N elements
     */
    public List<E> peekNext(
            int n );

    /**
     * Obtain the previous N elements, without iterating backward. If fewer than N elements are available, return
     * as many elements are available in a shorter array.
     *
     * @param n the number of elements to return
     * @return the previous no more than N elements
     */
    public List<E> peekPrevious(
            int n );

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration.
     * @throws NoSuchElementException iteration has no more elements.
     * @see #previous()
     */
    @Override
    public E next()
        throws
            NoSuchElementException;

    /**
     * <p>Obtain the next N elements. If fewer than N elements are available, return
     * as many elements are available in a shorter array.</p>
     *
     * @param n the number of elements to return
     * @return the next no more than N elements
     * @see #previous(int)
     */
    public List<E> next(
            int n );

    /**
     * Returns the previous element in the iteration.
     *
     * @return the previous element in the iteration.
     * @throws NoSuchElementException iteration has no more elements.
     * @see #next()
     */
    public E previous()
        throws
            NoSuchElementException;

    /**
     * <p>Obtain the previous N elements. If fewer than N elements are available, return
     * as many elements are available in a shorter array.</p>
     *
     * <p>Note that the elements
     * will be ordered in the opposite direction as you might expect: they are
     * returned in the sequence in which the CursorIterator visits them, not in the
     * sequence in which the underlying {@link CursorIterable} stores them.</p>
     *
     * @param n the number of elements to return
     * @return the previous no more than N elements
     * @see #next(int)
     */
    public List<E> previous(
            int  n );

    /**
     * Removes from the underlying collection the last element returned by the
     * iterator (optional operation). This is the same as the current element.
     *
     * @throws UnsupportedOperationException if the <code>remove</code>
     *        operation is not supported by this Iterator.
     * @throws IllegalStateException if the <code>next</code> method has not
     *        yet been called, or the <code>remove</code> method has already
     *        been called after the last call to the <code>next</code>
     *        method.
     */
    @Override
    public void remove()
        throws
            UnsupportedOperationException,
            IllegalStateException;

    /**
     * Clone this position.
     *
     * @return identical new instance
     */
    public CursorIterator<E> createCopy();

    /**
     * {@inheritDoc}
     */
    @Override
    public default CursorIterator<E> iterator()
    {
        return this;
    }

    /**
     * Efficiently iterate by fetching larger sets of objects in batch.
     *
     * @param batchSize the size of the initial batch to fetch
     * @param consumer the consumer of the value
     */
    default public void batchForEach(
            int                 batchSize,
            Consumer<? super E> consumer )
    {
        while( true ) {
            while( batchSize > 0 ) {
                if( hasNext( batchSize ) ) {
                    break;
                }
                batchSize /= 2;
            }
            if( batchSize == 0 ) {
                break;
            }
            List<E> batch = next( batchSize );

            for( E current : batch ) {
                consumer.accept( current );
            }
        }
    }
}
