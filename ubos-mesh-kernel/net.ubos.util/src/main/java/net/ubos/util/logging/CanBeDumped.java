//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

/**
 * This interface is implemented by objects that wish to support logging in a more
 * sophisticated form than via toString().
 */
public interface CanBeDumped
{
    /**
     * Dump this object.
     *
     * @param d the Dumper to dump to
     */
    public void dump(
            Dumper d );
}
