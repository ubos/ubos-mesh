//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.nameserver;

/**
 * A very simple, in-memory only implementation of {@link NameServer}. This NameServer
 * keeps mappings locally, and if not found, it will delegate to (optional) delegate
 * NameServers. This is to instantiate directly; use AbstractMNameServer to subclass.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 */
public class MNameServer<K,V>
    extends
        AbstractMNameServer<K,V>
{
    /**
     * Factory method.
     *
     * @return the created MNameServer
     * @param <K> the type of key
     * @param <V> the type of value
     */
    public static <K,V> MNameServer<K,V> create()
    {
        MNameServer<K,V> ret = new MNameServer<>();
        return ret;
    }

    /**
     * Factory method, specifying a delegate.
     *
     * @param newDelegate the delegate
     * @return the created MNameServer
     * @param <K> the type of key
     * @param <V> the type of value
     */
    public static <K,V> MNameServer<K,V> create(
            NameServer<K,? extends V> newDelegate )
    {
        MNameServer<K,V> ret = new MNameServer<>();
        ret.addDelegate( newDelegate );
        return ret;
    }
}
