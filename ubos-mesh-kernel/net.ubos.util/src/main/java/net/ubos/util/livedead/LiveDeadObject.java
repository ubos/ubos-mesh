//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.livedead;

/**
 * This interface is implemented by certain Objects that need an explicit <code>die()</code>
 * invocation to get rid of allocated resources, subscriptions, etc. It supports a
 * listener mechanism to provide callbacks if/when that happens.
 */
public interface LiveDeadObject
{
    /**
     * Tell this object that we don't need it any more.
     *
     * @throws IsDeadException thrown if this object is dead already
     */
    public void die()
        throws
            IsDeadException;

    /**
     * Determine whether this object is dead already.
     *
     * @return true if this object is dead already
     */
    public boolean isDead();

    /**
     * Add a listener to be notified when this object dies.
     *
     * @param newListener the new listener
     * @see #removeLiveDeadListener
     */
    public void addLiveDeadListener(
            LiveDeadListener newListener );

    /**
     * Remove a listener to be notified when this object dies.
     *
     * @param oldListener the to-be-removed listener
     * @see #addLiveDeadListener
     */
    public void removeLiveDeadListener(
            LiveDeadListener oldListener );
}
