//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.NoSuchElementException;
import java.util.function.BiPredicate;

/**
 * A CursorIterator that pages through a set represented by another CursorIterator.
 *
 * @param <E> the type of element to iterate over
 */
public class PagingCursorIterator<E>
    extends
        AbstractReadOneCursorIterator<E>
{
    /**
     * Factory method. The Pagination starts with the next object of the delegate.
     *
     * @param pageLength the length of one page
     * @param delegate the underlying iterator
     * @param <E> the type of element to iterate over
     * @return the created PagingCursorIterator
     */
    public static <E> PagingCursorIterator<E> create(
            int               pageLength,
            CursorIterator<E> delegate )
    {
        E start;
        if( delegate.hasNext() ) {
            start = delegate.peekNext();
        } else {
            start = null;
        }

        return new PagingCursorIterator<>(
                start,
                0,
                pageLength,
                delegate,
                new DEFAULT_EQUALS<>());
    }

    /**
     * Factory method. The Pagination starts with the next object of the delegate.
     *
     * @param pageLength the length of one page
     * @param delegate the underlying iterator
     * @param equals the equality operation for E's
     * @param <E> the type of element to iterate over
     * @return the created PagingCursorIterator
     */
    public static <E> PagingCursorIterator<E> create(
            int                        pageLength,
            CursorIterator<E>          delegate,
            BiPredicate<Object,Object> equals )
    {
        E start;
        if( delegate.hasNext() ) {
            start = delegate.peekNext();
        } else {
            start = null;
        }

        return new PagingCursorIterator<>(
                start,
                0,
                pageLength,
                delegate,
                new DEFAULT_EQUALS<>() );
    }

    /**
     * Constructor.
     *
     * @param start the start element
     * @param position the current position
     * @param pageLength the length of one page
     * @param delegate the underlying iterator
     * @param equals the equality operation for E's
     */
    @SuppressWarnings("unchecked")
    protected PagingCursorIterator(
            E                 start,
            int               position,
            int               pageLength,
            CursorIterator<E> delegate,
            BiPredicate<E,E>  equals )
    {
        super( equals );

        theStart      = start;
        thePosition   = position;
        thePageLength = pageLength;
        theDelegate   = delegate;
    }

    /**
     * Obtain the page length.
     *
     * @return the page length
     */
    public int getPageLength()
    {
        return thePageLength;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        if( thePosition + n > thePageLength ) {
            return false;
        }
        return theDelegate.hasNext( n );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        if( thePosition < n ) {
            return false;
        }
        return true; // correct by construction
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
        throws
            NoSuchElementException
    {
        if( thePosition < thePageLength ) {
            ++thePosition;
            return theDelegate.next();
        }
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
        throws
            NoSuchElementException
    {
        if( thePosition > 0 ) {
            E ret = theDelegate.previous();
            --thePosition; // only change position after previous succeeded
            return ret;
        }
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret;

        if( theStart != null ) {
            ret = theDelegate.moveToBefore( theStart );
        } else {
            ret = theDelegate.moveToBeforeFirst();
        }
        thePosition = 0;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int steps = thePageLength - thePosition;
        if( theDelegate.hasNext( steps )) {
            theDelegate.moveBy( steps );
            thePosition = thePageLength;
            return steps;

        } else {
            int ret = theDelegate.moveToAfterLast();
            thePosition += ret;
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingCursorIterator<E> createCopy()
    {
        PagingCursorIterator<E> ret = new PagingCursorIterator<>(
                theStart,
                thePosition,
                thePageLength,
                theDelegate.createCopy(),
                theEquals );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException
    {
        // guess we do the slow approach.
        PagingCursorIterator<E> testing = createCopy();
        testing.moveToBeforeFirst();

        E rightHere = position.peekNext();

        while( testing.hasNext() ) {
            E found = testing.next();

            if( found == rightHere ) {
                // found
                testing.previous();
                thePosition = testing.thePosition;
                theDelegate.setPositionTo( testing.theDelegate );

                return;
            }
        }

        throw new IllegalArgumentException();
    }

    /**
     * The underlying CursorIterator.
     */
    protected CursorIterator<E> theDelegate;

    /**
     * The start element.
     */
    protected E theStart;

    /**
     * The maximum number of elements to return.
     */
    protected int thePageLength;

    /**
     * The current index within a page.
     */
    protected int thePosition;
}
