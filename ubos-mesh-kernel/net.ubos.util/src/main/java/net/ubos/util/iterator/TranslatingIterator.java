//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.iterator;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator that translates the values from some other iterator. This little utility is
 * useful too often to not put it into its own class.
 * 
 * @param <E1> the type of returned elements by this iterator
 * @param <E2> the type of returned elements by the delegate iterator
 */
public abstract class TranslatingIterator<E1,E2>
        implements
            Iterator<E1>,
            Enumeration<E1>
{
    /**
     * Constructor for subclasses only.
     * 
     * @param delegate the delegate iterator
     */
    protected TranslatingIterator(
            Iterator<E2> delegate )
    {
        theDelegate = delegate;
    }
    
    /**
     * Subclasses must implement the translation operation.
     * 
     * @param org the object returned by the delegate
     * @return the object to be returned by this iterator
     */
    protected abstract E1 translate(
            E2 org );

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean hasNext()
    {
        return theDelegate.hasNext();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public E1 next()
        throws
            NoSuchElementException
    {
        E2 org = theDelegate.next();
        E1 ret = translate( org );
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void remove()
    {
        theDelegate.remove();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public final boolean hasMoreElements()
    {
        return theDelegate.hasNext();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public final E1 nextElement()
    {
        return next();
    }
    
    /**
     * The underlying delegate iterator.
     */
    protected Iterator<E2> theDelegate;
}
