//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

/**
 * A factory for ToStringDumpers.
 */
public class ToStringDumperFactory
    implements
        BufferingDumperFactory
{
    /**
     * Factory method.
     *
     * @return the created ToStringDumperFactory
     */
    public static ToStringDumperFactory create()
    {
        return new ToStringDumperFactory( ToStringDumper.DEFAULT_MAXLEVEL, ToStringDumper.DEFAULT_MAXARRAYLENGTH );
    }

    /**
     * Factory method.
     *
     * @param maxLevel the number of object levels to dump
     * @return the created ToStringDumperFactory
     * @param maxArrayLength the maximum number of array entries to dump
     */
    public static ToStringDumperFactory create(
            int maxLevel,
            int maxArrayLength )
    {
        return new ToStringDumperFactory( maxLevel, maxArrayLength );
    }

    /**
     * Constructor.
     *
     * @param maxLevel the number of object levels to dump
     * @param maxArrayLength the maximum number of array entries to dump
     */
    protected ToStringDumperFactory(
            int maxLevel,
            int maxArrayLength )
    {
        theMaxLevel       = maxLevel;
        theMaxArrayLength = maxArrayLength;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ToStringDumper obtainFor(
            Object key )
    {
        return ToStringDumper.create( theMaxLevel, theMaxArrayLength );
    }

    /**
     * The maximum number of object levels to dump.
     */
    protected int theMaxLevel;
    
    /**
     * The maximum number of array entries to dump.
     */
    protected int theMaxArrayLength;
}
