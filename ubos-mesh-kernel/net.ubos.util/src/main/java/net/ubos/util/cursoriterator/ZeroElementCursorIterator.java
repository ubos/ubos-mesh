//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * This Iterator pretends to iterate over an empty history.
 * It is surprising how useful this class can be.
 *
 * @param <T> the type of element to iterate over
 */
public final class ZeroElementCursorIterator<T>
    implements
        CursorIterator<T>
{
    /**
     * Factory method.
     *
     * @return the created ZeroElementCursorIterator
     * @param <T> the type of element to iterate over
     */
    public static <T> ZeroElementCursorIterator<T> create()
    {
        return new ZeroElementCursorIterator<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final T nextElement()
    {
        return next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekNext()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekPrevious()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> peekNext(
            int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> peekPrevious(
            int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> next( int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T previous()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> previous(
            int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
        throws
           IllegalStateException
    {
        throw new IllegalStateException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<T> createCopy()
    {
        return new ZeroElementCursorIterator<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            T pos )
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            T pos )
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<T> position )
        throws
            IllegalArgumentException
    {
        Object tmp = (ZeroElementCursorIterator<T>) position; // throw
    }

}
