//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.NoSuchElementException;

/**
 * The part of the CursorIterator interface that moves its position around, without
 * returning any values.
 *
 * @param <E> the type of element to iterate over
 */
public interface CursorIteratorMove<E>
{
    /**
     * Returns <code>true</code> if the iteration has more elements in the forward direction.
     *
     * @return <code>true</code> if the iterator has more elements in the forward direction.
     * @see #hasPrevious()
     * @see #hasPrevious(int)
     * @see #hasNext(int)
     */
    public boolean hasNext();

    /**
     * Returns <code>true</code> if the iteration has more elements in the backward direction.
     *
     * @return <code>true</code> if the iterator has more elements in the backward direction.
     * @see #hasNext()
     * @see #hasPrevious(int)
     * @see #hasNext(int)
     */
    public boolean hasPrevious();

    /**
     * Returns <code>true</code> if the iteration has at least N more elements in the forward direction.
     *
     * @param n the number of elements for which to check
     * @return <code>true</code> if the iterator has at least N more elements in the forward direction.
     * @see #hasNext()
     * @see #hasPrevious()
     * @see #hasPrevious(int)
     */
    public boolean hasNext(
            int n );

    /**
     * Returns <code>true</code> if the iteration has at least N more elements in the backward direction.
     *
     * @param n the number of elements for which to check
     * @return <code>true</code> if the iterator has at least N more elements in the backward direction.
     * @see #hasNext()
     * @see #hasPrevious()
     * @see #hasNext(int)
     */
    public boolean hasPrevious(
            int n );

    /**
     * Move the cursor by N positions. Positive numbers indicate forward movemement;
     * negative numbers indicate backward movement. This can move all the way forward
     * to the position "past last" and all the way backward to the position "before first".
     *
     * @param n the number of positions to move
     * @throws NoSuchElementException thrown if the position does not exist
     */
    public void moveBy(
            int n )
        throws
            NoSuchElementException;

    /**
     * Move the cursor to just before this element, i.e. return this element when
     * {@link #next next} is invoked right afterwards.
     *
     * @param pos the element to move the cursor to
     * @return the number of steps that were taken to move. Positive number means
     *         forward, negative backward
     * @throws NoSuchElementException thrown if this element is not actually part
     *         of the underlying {@link CursorIterable}
     */
    public int moveToBefore(
            E pos )
        throws
            NoSuchElementException;

    /**
     * Move the cursor to just after this element, i.e. return this element when
     * {@link #previous previous} is invoked right afterwards.
     *
     * @param pos the element to move the cursor to
     * @return the number of steps that were taken to move. Positive number means
     *         forward, negative backward
     * @throws NoSuchElementException thrown if this element is not actually part
     *         of the underlying {@link CursorIterable}
     */
    public int moveToAfter(
            E pos )
        throws
            NoSuchElementException;

    /**
     * Move the cursor to just before the first element, i.e. return the first element when
     * {@link #next next} is invoked right afterwards.
     *
     * @return the number of steps that were taken to move. Positive number means
     *         forward, negative backward
     */
    public int moveToBeforeFirst();

    /**
     * Move the cursor to just after the last element, i.e. return the last element when
     * {@link #previous previous} is invoked right afterwards.
     *
     * @return the number of steps that were taken to move. Positive number means
     *         forward, negative backward
     */
    public int moveToAfterLast();

    /**
     * Set this CursorIterator to the position represented by the provided CursorIterator.
     *
     * @param position the position to set this CursorIterator to
     * @throws IllegalArgumentException thrown if the provided CursorIterator does
     *         not work on the same CursorIterable, or the implementations were incompatible.
     */
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException;
}
