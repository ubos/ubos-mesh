//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A very basic implementation of Log that does not require any other libraries.
 */
public class BasicLog
    extends
        Log
{
    /**
     * Constructor.
     *
     * @param name the name of the Log object
     * @param dumperFactory the DumperFactory to use, if any
     */
    public BasicLog(
            String                 name,
            BufferingDumperFactory dumperFactory )
    {
        super( name, dumperFactory );
    }

    /**
     * Obtain the delegate logger.
     *
     * @return the delegate
     */
    public Logger getDelegate()
    {
        ensureDelegate();

        return theDelegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void logFatal(
            String    msg,
            Throwable t )
    {
        ensureDelegate();

        theDelegate.log( Level.SEVERE, msg, t );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void logError(
            String    msg,
            Throwable t )
    {
        ensureDelegate();

        theDelegate.log( Level.SEVERE, msg, t );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void logWarn(
            String    msg,
            Throwable t )
    {
        ensureDelegate();

        theDelegate.log( Level.WARNING, msg, t );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void logInfo(
            String    msg,
            Throwable t )
    {
        ensureDelegate();

        theDelegate.log( Level.INFO, msg, t );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void logDebug(
            String    msg,
            Throwable t )
    {
        ensureDelegate();

        theDelegate.log( Level.FINE, msg, t );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void logTrace(
            String    msg,
            Throwable t )
    {
        ensureDelegate();

        theDelegate.log( Level.FINEST, msg, t );
    }

 /**
     * {@inheritDoc}
     */
    @Override
    public boolean isInfoEnabled()
    {
        ensureDelegate();

        int level = determineLevel( theDelegate );

        return level <= Level.INFO.intValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDebugEnabled()
    {
        ensureDelegate();

        int level = determineLevel( theDelegate );

        return level <= Level.FINE.intValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTraceEnabled()
    {
        ensureDelegate();

        int level = determineLevel( theDelegate );

        return level <= Level.FINEST.intValue();
    }

    /**
     * Internal helper making sure that we have a delegate.
     */
    protected void ensureDelegate()
    {
        if( theDelegate == null ) {
            theDelegate = Logger.getLogger( theName );
        }
    }

    /**
     * Determine the level of this logger.
     */
    protected static int determineLevel(
            Logger log )
    {
        while( log != null ) {
            Level level = log.getLevel();
            if( level != null ) {
                return level.intValue();
            }
            log = log.getParent();
        }
        return 0; // FIXME?
    }

    /**
     * The underlying Java logger.
     */
    protected Logger theDelegate;

    /**
     * The implementor of LogFactory for the basic Logger.
     */
    public static class Factory
        extends
            LogFactory
    {
        /**
         * {@inheritDoc}
         */
        @Override
        public Log create(
                String                 name,
                BufferingDumperFactory dumperFactory )
        {
            return new BasicLog( name, dumperFactory );
        }
    }
}
