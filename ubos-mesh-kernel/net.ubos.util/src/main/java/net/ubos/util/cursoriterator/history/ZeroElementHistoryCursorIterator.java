//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator.history;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * This Iterator pretends to iterate over an empty History.
 *
 * @param <T> the type of element to iterate over
 */
public final class ZeroElementHistoryCursorIterator<T>
    implements
            HistoryCursorIterator<T>
{
    /**
     * Factory method.
     *
     * @return the created ZeroElementCursorIterator
     * @param <T> the type of element to iterate over
     */
    public static <T> ZeroElementHistoryCursorIterator<T> create()
    {
        return new ZeroElementHistoryCursorIterator<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final T nextElement()
    {
        return next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToJustBeforeTime(
            long time )
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToJustAfterTime(
            long time )
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<T> createCopy()
    {
        return new ZeroElementHistoryCursorIterator<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekNext()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekPrevious()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> peekNext(
            int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> peekPrevious(
            int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> next(
            int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    @Override
    public T previous()
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> previous(
            int n )
    {
        return (List<T>) Collections.EMPTY_LIST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
        throws
            IllegalStateException
    {
        throw new IllegalStateException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            T pos )
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            T pos )
        throws
            NoSuchElementException
    {
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        return 0;
    }

    @Override
    public void setPositionTo(
            CursorIterator<T> position )
        throws
            IllegalArgumentException
    {
        Object foo = (ZeroElementHistoryCursorIterator<T>) position; // throws
    }
}
