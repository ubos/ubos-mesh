//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.BiPredicate;

/**
 * A CursorIterator that iterates over the content produced by one or more
 * delegate CursorIterators.
 *
 * @param <E> the type of element to iterate over
 */
public class CompositeCursorIterator<E>
        extends
            AbstractReadOneCursorIterator<E>
        implements
            CursorIterator<E>
{
    /**
     * Factory method.
     *
     * @param delegates the underlying delegate iterators
     * @return the created CompositeCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeCursorIterator<E> create(
            List<CursorIterator<E>> delegates )
    {
        CompositeCursorIterator<E> ret = new CompositeCursorIterator<>(
                delegates,
                0,
                new DEFAULT_EQUALS<>() );
        return ret;
    }

    /**
     * Factory method.
     *
     * @param delegates the underlying delegate iterators
     * @param equals the equality operation for E's
     * @return the created CompositeCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeCursorIterator<E> create(
            List<CursorIterator<E>> delegates,
            BiPredicate<E,E>        equals )
    {
        CompositeCursorIterator<E> ret = new CompositeCursorIterator<>(
                delegates,
                0,
                equals );

        return ret;
    }

    /**
     * Factory method.
     *
     * @param delegates the underlying delegate iterators
     * @return the created CompositeCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeCursorIterator<E> create(
            CursorIterator<E> [] delegates )
    {
        List<CursorIterator<E>> temp = new ArrayList<>();
        for( CursorIterator<E> current : delegates ) {
            temp.add( current );
        }

        CompositeCursorIterator<E> ret = new CompositeCursorIterator<>(
                temp,
                0,
                new DEFAULT_EQUALS<>());

        return ret;
    }

    /**
     * Factory method.
     *
     * @param delegates the underlying delegate iterators
     * @param equals the equality operation for E's
     * @return the created CompositeCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeCursorIterator<E> create(
            CursorIterator<E> [] delegates,
            BiPredicate<E,E>     equals )
    {
        List<CursorIterator<E>> temp = new ArrayList<>();
        for( CursorIterator<E> current : delegates ) {
            temp.add( current );
        }

        CompositeCursorIterator<E> ret = new CompositeCursorIterator<>(
                temp,
                0,
                new DEFAULT_EQUALS<>() );

        return ret;
    }

    /**
     * Factory method.
     *
     * @param delegate1 the first underlying delegate iterator
     * @param delegate2 the second underlying delegate iterator
     * @return the created CompositeCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeCursorIterator<E> create(
            CursorIterator<E> delegate1,
            CursorIterator<E> delegate2 )
    {
        List<CursorIterator<E>> temp = new ArrayList<>();
        temp.add( delegate1 );
        temp.add( delegate2 );

        CompositeCursorIterator<E> ret = new CompositeCursorIterator<>(
                temp,
                0,
                new DEFAULT_EQUALS<>() );

        return ret;
    }

    /**
     * Factory method.
     *
     * @param delegate1 the first underlying delegate iterator
     * @param delegate2 the second underlying delegate iterator
     * @param equals the equality operation for E's
     * @return the created CompositeCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeCursorIterator<E> create(
            CursorIterator<E> delegate1,
            CursorIterator<E> delegate2,
            BiPredicate<E,E>  equals )
    {
        List<CursorIterator<E>> temp = new ArrayList<>();
        temp.add( delegate1 );
        temp.add( delegate2 );

        CompositeCursorIterator<E> ret = new CompositeCursorIterator<>(
                temp,
                0,
                new DEFAULT_EQUALS<>() );

        return ret;
    }

    /**
     * Constructor for subclasses only, use factory method.
     *
     * @param delegates the underlying delegate iterators
     * @param delegateIndex the current index into the delegates array
     * @param equals the equality operation for E's
     */
    protected CompositeCursorIterator(
            List<CursorIterator<E>> delegates,
            int                     delegateIndex,
            BiPredicate<E,E>        equals )
    {
        super( equals );

        theDelegates     = delegates;
        theDelegateIndex = delegateIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
        throws
            NoSuchElementException
    {
        while( true ) {
            if( theDelegateIndex == theDelegates.size() ) {
                throw new NoSuchElementException();
            }
            CursorIterator<? extends E> currentDelegate = theDelegates.get( theDelegateIndex );
            if( currentDelegate.hasNext() ) {
                return currentDelegate.next();
            }
            ++theDelegateIndex;
            if( theDelegateIndex < theDelegates.size() ) {
                currentDelegate = theDelegates.get( theDelegateIndex );
                currentDelegate.moveToBeforeFirst();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
        throws
            NoSuchElementException
    {
        while( true ) {
            if( theDelegateIndex < theDelegates.size() ) {
                CursorIterator<? extends E> currentDelegate = theDelegates.get( theDelegateIndex );

                if( currentDelegate.hasPrevious() ) {
                    return currentDelegate.previous();
                }
            }
            if( theDelegateIndex == 0 ) {
                throw new NoSuchElementException();
            }
            --theDelegateIndex;
            CursorIterator<? extends E> currentDelegate = theDelegates.get( theDelegateIndex );
            currentDelegate.moveToAfterLast();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        if( n == 0 ) {
            return true;
        }
        if( theDelegateIndex >= theDelegates.size() ) {
            return false;
        }

        int count = 0;
        CursorIterator<E> hereIter = theDelegates.get( theDelegateIndex ).createCopy();
        while( hereIter.hasNext() && ++count < n ) {
            hereIter.next();
        }
        if( count == n ) {
            return true;
        }
        // go to next iterators
        for( int index = theDelegateIndex+1 ; index < theDelegates.size() ; ++index ) {
            CursorIterator<E> current = theDelegates.get( index );
            current.moveToBeforeFirst();
            while( current.hasNext() && ++count < n ) {
                current.next();
            }
            if( count == n ) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        if( n == 0 ) {
            return true;
        }
        if( theDelegateIndex == 0 ) {
            return theDelegates.get( 0 ).hasPrevious( n );
        }

        int count = 0;
        if( theDelegateIndex < theDelegates.size() ) {
            // not after the last
            CursorIterator<E> hereIter = theDelegates.get( theDelegateIndex ).createCopy();
            while( hereIter.hasPrevious() && ++count < n ) {
                hereIter.previous();
            }
            if( count == n ) {
                return true;
            }
        }
        // go to next iterators
        for( int index = theDelegateIndex-1 ; index >= 0 ; --index ) {
            CursorIterator<E> current = theDelegates.get( index );
            current.moveToAfterLast();
            while( current.hasPrevious() && ++count < n ) {
                current.previous();
            }
            if( count == n ) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        // need to traverse to construct return value
        int ret = 0;
        while( theDelegateIndex >= 0 ) {
            if( theDelegateIndex == theDelegates.size() ) {
                --theDelegateIndex;
                theDelegates.get( theDelegateIndex ).moveToAfterLast();
            }
            ret += theDelegates.get( theDelegateIndex ).moveToBeforeFirst();
            --theDelegateIndex;
        }
        theDelegateIndex = 0;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        // need to traverse to construct return value
        int ret = 0;
        while( theDelegateIndex < theDelegates.size() ) {
            ret += theDelegates.get( theDelegateIndex ).moveToAfterLast();
            ++theDelegateIndex;
            if( theDelegateIndex < theDelegates.size() ) {
                theDelegates.get( theDelegateIndex ).moveToBeforeFirst();
            }
        }
        theDelegateIndex = theDelegates.size();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompositeCursorIterator<E> createCopy()
    {
        // need a deep copy

        ArrayList<CursorIterator<E>> delegatesCopy = new ArrayList<>();
        for( CursorIterator<E> current : theDelegates ) {
            CursorIterator<E> delegateCopy = current.createCopy();
            delegatesCopy.add( delegateCopy );
        }
        return new CompositeCursorIterator<>( delegatesCopy, theDelegateIndex, theEquals );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings( "unchecked" )
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException
    {
        if( !( position instanceof CompositeCursorIterator )) {
            throw new IllegalArgumentException( "Wrong type of CursorIterator: " + position );
        }
        CompositeCursorIterator<E> realPosition = (CompositeCursorIterator<E>) position;

        if( hasSameContentInOrder( theDelegates, realPosition.theDelegates )) {
            throw new IllegalArgumentException( "Not the same delegates to iterate over" );
        }
        // we know it's the same content, so all casts are safe

        theDelegateIndex = realPosition.theDelegateIndex;
        if( theDelegateIndex < theDelegates.size() ) {
            CursorIterator<E> newPosition = realPosition.theDelegates.get( theDelegateIndex );
            theDelegates.get( theDelegateIndex ).setPositionTo( newPosition );
        }
    }

    /**
     * Helper method to determine whether two Lists have the same content in the same order.
     *
     * @param one the first List
     * @param two the second List
     * @return true they have the same content in order
     * @param <X> the type of objects in the to-be-compared Lists
     */
    protected <X> boolean hasSameContentInOrder(
            List<X> one,
            List<X> two )
    {
        if( one.size() != two.size() ) {
            return false;
        }

        Iterator<X> oneIter = one.iterator();
        Iterator<X> twoIter = two.iterator();

        while( oneIter.hasNext() ) {
            X foundOne = oneIter.next();
            X foundTwo = twoIter.next();

            if( foundOne == null ) {
                if( foundTwo != null ) {
                    return false;
                }
            } else if( !foundOne.equals( foundTwo )) {
                return false;
            }
        }
        return true;
    }

    /**
     * The underlying delegate iterators.
     */
    protected final List<CursorIterator<E>> theDelegates;

    /**
     * Index into the delegate iterators array.
     */
    protected int theDelegateIndex;
}
