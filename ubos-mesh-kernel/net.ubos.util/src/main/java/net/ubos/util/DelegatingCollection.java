//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import net.ubos.util.iterator.CompositeIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * A Collection that delegates to other Collections.
 *
 * @param <E> the content type
 */
public class DelegatingCollection<E>
    implements
        Collection<E>
{
    /**
     * Factory method for any number of delegates.
     *
     * @param delegates the Maps to which this Map delegates
     * @return the created DelegatingCollection
     * @param <E> the content type
     */
    public static <E> DelegatingCollection<E> create(
            List<Collection<E>> delegates )
    {
        return new DelegatingCollection<>( delegates );
    }

    /**
     * Factory method for any number of delegates.
     *
     * @param delegates the Maps to which this Map delegates
     * @return the created DelegatingCollection
     * @param <E> the content type
     */
    public static <E> DelegatingCollection<E> create(
            Set<E> [] delegates )
    {
        List<Collection<E>> delegates2 = new ArrayList<>( delegates.length );
        for( int i=0 ; i<delegates.length ; ++i ) {
            delegates2.add( delegates[i] );
        }
        return new DelegatingCollection<>( delegates2 );
    }

    /**
     * Factory method for two delegates.
     *
     * @param del1 the first delegate
     * @param del2 the second delegate
     * @return the created DelegatingCollection
     * @param <E> the content type
     */
    public static <E> DelegatingCollection<E> create(
            Collection<E> del1,
            Collection<E> del2 )
    {
        List<Collection<E>> delegates2 = new ArrayList<>( 2 );
        delegates2.add( del1 );
        delegates2.add( del2 );

        return new DelegatingCollection<>( delegates2 );
    }

    /**
     * Constructor.
     *
     * @param delegates the Collections to which this Collection delegates
     */
    protected DelegatingCollection(
            List<Collection<E>> delegates )
    {
        theDelegates = delegates;
    }

    /**
     * Obtain the delegates.
     *
     * @return the delegates
     */
    public List<Collection<E>> getDelegates()
    {
        return theDelegates;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        int ret = 0;
        for( Collection<E> current : theDelegates ) {
            ret += current.size();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        for( Collection<E> current : theDelegates ) {
            if( !current.isEmpty() ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(
            Object o )
    {
        for( Collection<E> current : theDelegates ) {
            if( current.contains( o ) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<E> iterator()
    {
        List<Iterator<E>> newDelegates = new ArrayList<>( theDelegates.size() );

        for( Collection<E> current : theDelegates ) {
            newDelegates.add( current.iterator() );
        }

        CompositeIterator<E> ret = CompositeIterator.createFromIterators( newDelegates );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] toArray()
    {
        return toArray( new Object[0] );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T[] toArray(
            T [] a )
    {
        int               count = 0;
        ArrayList<Object> overflow = null;

        for( Collection<E> current : theDelegates ) {
            for( E current2 : current ) {
                if( count < a.length ) {
                    a[count++] = (T) current2;
                } else {
                    if( overflow == null ) {
                        overflow = new ArrayList<>();
                    }
                    overflow.add( current2 );
                }
            }
        }

        T [] ret;
        if( overflow == null || overflow.isEmpty() ) {
            ret = a;
        } else {
            ret = ArrayHelper.createArray( (Class<T>) a.getClass().getComponentType(), a.length + overflow.size() );
            System.arraycopy( a, 0, ret, 0, a.length );
            for( int i=0 ; i<overflow.size() ; ++i ) {
                ret[i + a.length] = (T) overflow.get( i );
            }
        }
        return ret;
    }

    // Modification Operations

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(
            E o )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(
            Object o )
    {
        throw new UnsupportedOperationException();
    }

    // Bulk Operations

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("element-type-mismatch")
    public boolean containsAll(
            Collection<?> c )
    {
        for( Object x : c ) {
            if( !contains( x )) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addAll(
            Collection<? extends E> c )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean retainAll(
            Collection<?> c )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeAll(
            Collection<?> c )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * The Collections to which this Collection delegates.
     */
    protected List<Collection<E>> theDelegates;
}
