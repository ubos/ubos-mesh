//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.util.concurrent.ThreadFactory;

/**
 * A ThreadFactory that names each Thread with a prefix name and a sequential suffix.
 * This can be very helpful for debugging to determine where a particular Thread
 * came from.
 */
public class NamedThreadFactory
        implements
            ThreadFactory
{
    /**
     * Constructor.
     *
     * @param prefix the prefix name
     */
    public NamedThreadFactory(
            String prefix )
    {
        thePrefix = prefix + "-";
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public Thread newThread(
            Runnable r )
    {
        Thread ret = new Thread( r, thePrefix + theCounter++ );
        return ret;
    }

    /**
     * The Prefix for the name of created Threads.
     */
    protected String thePrefix;

    /**
     * The current counter of created Threads.
     */
    protected int theCounter = 0;
}
