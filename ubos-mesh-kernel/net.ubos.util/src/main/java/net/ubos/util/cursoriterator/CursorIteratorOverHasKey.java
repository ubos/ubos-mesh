//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.NoSuchElementException;
import net.ubos.util.HasKey;

/**
 * A CursorIterator over values that have keys. This can make some
 * implementations faster because some operations only require the key, not the entire value
 * that may be large or slow to get.
 *
 * @param <E> the type of element to iterate over
 */
public interface CursorIteratorOverHasKey<E extends HasKey>
    extends
        CursorIterator<E>
{
    /**
     * Move the cursor to this element, i.e. return this element when {@link #next next} is invoked
     * right afterwards.
     *
     * @param key the key of the element to move the cursor to
     * @return the number of steps that were taken to move. Positive number means forward, negative backward
     * @throws NoSuchElementException thrown if this element is not actually part of the collection to iterate over
     */
    public int moveToBefore(
            String key )
        throws
            NoSuchElementException;

    /**
     * Move the cursor to this element, i.e. return this element when {@link #previous previous} is invoked
     * right afterwards.
     *
     * @param key the key of the element to move the cursor to
     * @return the number of steps that were taken to move. Positive number means forward, negative backward
     * @throws NoSuchElementException thrown if this element is not actually part of the collection to iterate over
     */
    public int moveToAfter(
            String key )
        throws
            NoSuchElementException;

}
