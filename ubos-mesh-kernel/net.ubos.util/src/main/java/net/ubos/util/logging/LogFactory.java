//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

/**
 * Interface for objects that know how to create Log instances. This is the interface
 * between UBOS Data Mesh logging and underlying loggers such as <code>Basic</code>.
 */
public abstract class LogFactory
{
    /**
     * Create a new Log object.
     *
     * @param channelName the name of the channel
     * @return the created Log object
     */
    public Log create(
            String channelName )
    {
        return create( channelName, DEFAULT_DUMPER_FACTORY );
    }

    /**
     * Create a new Log object.
     *
     * @param channelClass the class of the channel
     * @return the created Log object
     */
    public Log create(
            Class channelClass )
    {
        return create( channelClass.getName(), DEFAULT_DUMPER_FACTORY );
    }

    /**
     * Obtain a Logger and specify a dumper factory instead of the default.
     *
     * @param clazz the Class for which we are looking for a Log object
     * @param dumperFactory the factory for dumpers
     * @return the Log object
     */
    public Log create(
            Class                  clazz,
            BufferingDumperFactory dumperFactory )
    {
        return create( clazz.getName(), dumperFactory );
    }

    /**
     * Obtain a Logger and specify a dumper factory instead of the default.
     *
     * @param name name of the Log object that we are looking for
     * @param dumperFactory the factory for dumpers
     * @return the Log object
     */
    public abstract Log create(
            String                 name,
            BufferingDumperFactory dumperFactory );

    /**
     * The default DumperFactory.
     */
    protected static BufferingDumperFactory DEFAULT_DUMPER_FACTORY = ToStringDumperFactory.create();
}
