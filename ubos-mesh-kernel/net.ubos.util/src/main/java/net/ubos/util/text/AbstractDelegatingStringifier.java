//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;

/**
 * Factors out functionality common to Stringifiers that delegate to other Stringifiers.
 *
 * @param <T> the type of the Objects to be stringified
 */
public abstract class AbstractDelegatingStringifier<T>
        extends
            AbstractStringifier<T>
{
    /**
     * Private constructor.
     *
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     */
    protected AbstractDelegatingStringifier(
            Stringifier<T> delegate )
    {
        theDelegate = delegate;
    }

    /**
     * Obtain the delegate Stringifier.
     *
     * @return the delegate
     */
    public Stringifier<T> getDelegate()
    {
        return theDelegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            T                     arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        String ret = theDelegate.format( arg, pars, soFar );

        ret = escape( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException
    {
        String ret = theDelegate.attemptFormat( arg, pars, soFar );

        ret = escape( ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        String descaped = unescape( rawString );

        T ret = theDelegate.unformat( descaped, factory );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        String descaped = unescape( rawString );

        Iterator<StringifierParsingChoice<T>> ret = theDelegate.allParsingChoiceIterator( descaped, startIndex, endIndex, max, factory );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        String descaped = unescape( rawString );

        Iterator<StringifierParsingChoice<T>> ret = theDelegate.partialParsingChoiceIterator( descaped, startIndex, endIndex, max, factory );
        return ret;
    }

    /**
     * Overridable method to possibly escape a String first.
     *
     * @param s the String to be escaped
     * @return the escaped String
     */
    protected String escape(
            String s )
    {
        return s;
    }

    /**
     * Overridable method to possibly unescape a String first.
     *
     * @param s the String to be unescaped
     * @return the unescaped String
     */
    protected String unescape(
            String s )
    {
        return s;
    }

    /**
     * The underlying Stringifier that knows how to deal with the real type.
     */
    protected final Stringifier<T> theDelegate;
}
