//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.tree;

import java.util.NoSuchElementException;
import java.util.function.BiPredicate;
import net.ubos.util.cursoriterator.AbstractReadOneCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * <p>Knows how to walk a {@link TreeFacade}. This implements a depth-first algorithm
 * which returns nodes on the way down.
 *
 * <p>To include or exclude certain nodes, chain this
 *    CursorIterator with a {@link net.ubos.util.cursoriterator.FilteringCursorIterator}.
 *
 * @param <T> the type of tree node
 */
public class TreeFacadeCursorIterator<T>
        extends
            AbstractReadOneCursorIterator<T>
{
    /**
     * Factory method.
     *
     * @param facade the tree to traverse
     * @param equals the equality operation for E's
     * @return the created TreeFacadeCursorIterator
     * @param <T> the type of tree node
     */
    public static <T> TreeFacadeCursorIterator<T> create(
            TreeFacade<T>    facade,
            BiPredicate<T,T> equals )
    {
        TreeFacadeCursorIterator<T> ret = new TreeFacadeCursorIterator<>(
                facade,
                facade.getTopNode(),
                new DEFAULT_EQUALS<>() );
        return ret;
    }

    /**
     * Factory method.
     *
     * @param facade the tree to traverse
     * @return the created TreeFacadeCursorIterator
     * @param <T> the type of tree node
     */
    public static <T> TreeFacadeCursorIterator<T> create(
            TreeFacade<T> facade )
    {
        TreeFacadeCursorIterator<T> ret = new TreeFacadeCursorIterator<>(
                facade,
                facade.getTopNode(),
                new DEFAULT_EQUALS<>() );
        return ret;
    }

    /**
     * Constructor for subclasses only, use factory method.
     *
     * @param facade the tree to traverse
     * @param current the start node
     * @param equals the equality operation for E's
     */
    protected TreeFacadeCursorIterator(
            TreeFacade<T>    facade,
            T                current,
            BiPredicate<T,T> equals )
    {
        super( equals );

        theFacade  = facade;
        theCurrent = current;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        T here = theCurrent;
        for( int i=0 ; i<n ; ++i ) {
            if( here == null ) {
                return false;
            }
            try {
                T next = goForward( here );
                here = next;
            } catch( NoSuchElementException ex ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        T here = theCurrent;
        for( int i=0 ; i<n ; ++i ) {
            try {
                T next = goBackward( here );
                if( next == null ) {
                    return false;
                }
                here = next;
            } catch( NoSuchElementException ex ) {
                return false;
            }
        }
        return true;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next()
        throws
            NoSuchElementException
    {
        T ret = theCurrent;
        theCurrent = goForward( theCurrent );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T previous()
        throws
            NoSuchElementException
    {
        theCurrent = goBackward( theCurrent );
        return theCurrent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TreeFacadeCursorIterator<T> createCopy()
    {
        return new TreeFacadeCursorIterator<>( theFacade, theCurrent, theEquals );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<T> position )
        throws
            IllegalArgumentException
    {
        if( position.hasNext() ) {
            T here = position.next();

            theCurrent = here;
        } else {
            theCurrent = null;
        }
    }

    /**
     * Given this current node, return the next node in the iteration.
     *
     * @param start the current node
     * @return the next node
     * @throws NoSuchElementException iteration has no more elements.
     * @see #goBackward
     */
    protected T goForward(
            T start )
        throws
            NoSuchElementException
    {
        if( start == null ) {
            throw new NoSuchElementException();
        }

        // has child, so return child
        if( theFacade.hasChildNodes( start )) {
            T ret = theFacade.getChildNodes( start )[0];
            return ret;
        }

        // no child, but sibling, so return sibling
        T ret = theFacade.getForwardSiblingNode( start );
        if( ret != null ) {
            return ret;
        }

        // neither child nor sibling, so move up and to the next sibling. We might have to do
        // this several times
        T here = start;
        while( true ) {
            if( theEquals.test( theFacade.getTopNode(), here )) {
                // we are done
                return null;
            }
            here = theFacade.getParentNode( here );
            ret = theFacade.getForwardSiblingNode( here );
            if( ret != null ) {
                return ret;
            }
        }
    }

    /**
     * Given this current node, return the previous node in the iteration.
     *
     * @param start the current node
     * @return the previous node
     * @throws NoSuchElementException iteration has no more elements.
     * @see #goForward
     */
    protected T goBackward(
            T start )
    {
        T here;
        if( start != null ) {
            // find the last child of the previous sibling
            here = start;
            if( theEquals.test( theFacade.getTopNode(), here )) {
                throw new NoSuchElementException();
            }

            T sibling = theFacade.getBackwardSiblingNode( here );
            if( sibling != null ) {
                here = sibling;
            } else {
                T ret = theFacade.getParentNode( here );
                return ret;
            }
        } else {
            here = theFacade.getTopNode();
        }
        while( theFacade.hasChildNodes( here )) {
            T [] children = theFacade.getChildNodes( here );
            here = children[ children.length-1 ];
        }
        return here;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret = 0;
        while( hasPrevious() ) {
            previous();
            --ret;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int ret = 0;
        while( hasNext() ) {
            next();
            ++ret;
        }
        return ret;
    }

    /**
     * The tree to traverse.
     */
    protected TreeFacade<T> theFacade;

    /**
     * The current position.
     */
    protected T theCurrent;
}
