//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.livedead;

/**
 * This Exception is thrown if we are trying to apply an operation to a
 * {@link LiveDeadObject} or other Object that is dead already. This
 * class operates on Object rather than LiveDeadObject as sometimes it
 * is too expensive to add LiveDeadObject APIs to some classes.
 */
public class IsDeadException
    extends
        RuntimeException
{
    /**
     * Constructor.
     *
     * @param obj the dead object
     */
    public IsDeadException(
            Object obj )
    {
        theDeadObject = obj;
    }

    /**
     * Constructor.
     *
     * @param obj the dead object
     * @param msg a text message
     */
    public IsDeadException(
            Object obj,
            String msg )
    {
        super( msg );

        theDeadObject = obj;
    }

    /**
     * Obtain the Object that is dead already.
     *
     * @return the object that is dead already.
     */
    public Object getDeadObject()
    {
        return theDeadObject;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public String toString()
    {
        return super.toString() + ": " + theDeadObject;
    }

    /**
     * The Object that is dead.
     */
    protected final Object theDeadObject;
}
