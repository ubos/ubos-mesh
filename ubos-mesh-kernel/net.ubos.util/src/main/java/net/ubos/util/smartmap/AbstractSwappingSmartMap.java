//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap;

import java.lang.ref.Reference;
import java.util.Map;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Factors out functionality common to SmartMap implementations that swap their
 * content in and out.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public abstract class AbstractSwappingSmartMap<K,V>
    extends
        GentleSmartMap<K,V>
{
    /**
     * Constructor.
     *
     * @param localCache the Map to use as a local cache.
     */
    protected AbstractSwappingSmartMap(
            Map<K,Reference<V>> localCache )
    {
        super( localCache );
    }

    @Override
    public abstract CursorIterator<K> keyIterator();

    @Override
    public abstract CursorIterator<V> valueIterator();

    /**
     * This method is defined by subclasses, to swap in a value that currently
     * is not contained in the local cache.
     *
     * @param key the key whose value should be loaded
     * @return the value that was loaded, or null if none.
     */
    @Override
    protected abstract V loadValueFromStorage(
            K key );

    /**
     * This method is defined by subclasses, to save a value to storage.
     *
     * @param key the key whose value was updated
     * @param newValue the new value
     */
    @Override
    protected abstract void saveValueToStorage(
            K key,
            V newValue );

    /**
     * This method is defined by subclasses, to remove a value from storage.
     *
     * @param key the key whose value has been removed
     */
    @Override
    protected abstract void removeValueFromStorage(
            K key );
}
