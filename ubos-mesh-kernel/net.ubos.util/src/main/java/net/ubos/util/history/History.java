//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history;

import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.cursoriterator.CursorIterable;
import net.ubos.util.logging.Log;

/**
 * Implemented by those objects that represent the evolution of a single thing over time.
 *
 * @param <E> the type of thing for which this is a history
 */
public interface History<E extends HasTimeUpdated>
    extends
        CursorIterable<E>
{
    /**
     * Determine whether this History is persistent.
     *
     * @return true if it is persistent
     */
    public boolean isPersistent();

    /**
     * Empty the entire History.
     */
    public void clear();

    /**
     * Determine whether this History contains an element at this time.
     *
     * @param t the time
     * @return true if it does contain it
     */
    public boolean containsAt(
            long t );

    /**
     * Put a new element into the history. Throw an exception if there was an element
     * there previous previously.
     *
     * @param toAdd the new element
     * @throws IllegalArgumentException thrown if the element does not exist
     */
    public void putOrThrow(
            E toAdd )
        throws
            IllegalArgumentException;

    /**
     * Put a new element into the history. Overwrite what was there previously.
     *
     * @param toAdd the new element
     */
    public void putIgnorePrevious(
            E toAdd );

    /**
     * Remove an element from a history. Throw an exception if the element did not exist.
     *
     * @param t the time
     * @throws IllegalArgumentException thrown if the element does not exist
     */
    public default void removeOrThrow(
            long t )
        throws
            IllegalArgumentException
    {
        if( !containsAt( t )) {
            throw new IllegalArgumentException( "No element at time " + t );
        }
        removeIgnorePrevious( t );
    }

    /**
     * Remove an element from the history. Do nothing if there was nothing there.
     *
     * @param t the time
     */
    public void removeIgnorePrevious(
            long t );

    /**
     * Obtain the length of the history, in number of elements available.
     *
     * @return the length
     */
    public int getLength();

    /**
     * Obtain the oldest element in the history.
     *
     * @return the oldest element
     */
    public E oldest();

    /**
     * Obtain the most recent element in the history.
     *
     * @return the youngest element
     */
    public E current();

    /**
     * Obtain the element that was recorded at exactly the provided time.
     *
     * @param t the time
     * @return the element at that time, or null if nothing was recorded at that time
     */
    public E at(
            long t );

    /**
     * Obtain the element that was recorded closest to the provided time,
     * but still before it (exclusive).
     *
     * @param t the time
     * @return the element at that time, or null
     */
    public E before(
            long t );

    /**
     * Obtain the element that was recorded closest to the provided time,
     * either before it or exactly at it (inclusive).
     *
     * @param t the time
     * @return the element at that time, or null
     */
    public E atOrBefore(
            long t );

    /**
     * Obtain the element that was recorded closest to the provided time,
     * either after it or exactly at it (inclusive).
     *
     * @param t the time
     * @return the element at that time, or null
     */
    public E atOrAfter(
            long t );

    /**
     * Obtain the element that was recorded closest to the provided time,
     * but after it (exclusive).
     *
     * @param t the time
     * @return the element at that time, or null
     */
    public E after(
            long t );

    /**
     * Obtain the element that was recorded closest to the provided time,
     * either before, after or exactly at it.
     *
     * @param t the time
     * @return the element at that time, or null
     */
    public E closest(
            long t );

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<E> iterator();

    /**
     * {@inheritDoc}
     */
    @Override
    public default HistoryCursorIterator<E> getIterator()
    {
        return iterator();
    }
}
