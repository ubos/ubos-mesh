//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.Map;
import net.ubos.util.Pair;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.listenerset.FlexibleListenerSet;
import net.ubos.util.logging.Log;

/**
 * A SmartMap that holds on gently to its values, and if they are not
 * referenced any more, they (and the corresponding keys) are removed from the SmartMap.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public abstract class GentleSmartMap<K,V>
    extends
        AbstractSmartMap<K,V>
    implements
        SwappingSmartMap<K,V>
{
    private static final Log log = Log.getLogInstance(GentleSmartMap.class );

    /**
     * Constructor.
     *
     * @param localCache the Map to use as a local cache.
     */
    protected GentleSmartMap(
            Map<K,Reference<V>> localCache )
    {
        theLocalCache = localCache;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearLocalCache()
    {
        theLocalCache.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            K key )
    {
        V value = get( key );
        return value != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized V get(
            K key )
    {
        cleanup();

        Reference<V> found = theLocalCache.get( key );
        V ret = found != null ? found.get() : null;

        if( ret == null ) {
            ret = loadValueFromStorage( key );

            if( ret != null ) {
                theLocalCache.put( key, createReference( key, ret ));
                fireValueLoaded( key, ret );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void putIgnorePrevious(
            K key,
            V value )
    {
        cleanup();
        theLocalCache.put( key, createReference( key, value ));

        saveValueToStorage( key, value );
        fireValueSaved( key, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void removeIgnorePrevious(
            K key )
    {
        cleanup();
        theLocalCache.remove( key );

        removeValueFromStorage( key );
        fireValueRemoved( key );
    }

    /**
     * Factory method for a subclass of Reference.
     *
     * @param key the key
     * @param value the value
     * @return the Reference to the value
     */
    protected abstract Reference<V> createReference(
            K key,
            V value );

    /**
     * Clean up deleted references.
     */
    @SuppressWarnings(value={"unchecked"})
    protected void cleanup()
    {
        while( true ) {
            Reference<? extends V> current = theQueue.poll();
            if( current == null ) {
                break;
            }

            // we know that this queue only contains things that implement this interface, so this cast is safe
            AbstractSwappingSmartMap.EntryReference<K,V> realCurrent = (AbstractSwappingSmartMap.EntryReference<K,V>) current;
            K key   = realCurrent.getKey();
            V value = realCurrent.get();

            theLocalCache.remove( key );
            fireValuePurged( key, value );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        cleanup();

        return MapCursorIterator.createForKeys( theLocalCache );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<V> valueIterator()
    {
        return new MappingCursorIterator<>(
                keyIterator(),
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public V mapDelegateValueToHere(
                            K delegateValue )
                    {
                        return get( delegateValue );
                    }

                    @Override
                    public K mapHereToDelegateValue(
                            V value )
                    {
                        throw new UnsupportedOperationException( "Should not happen" );
                    }
                });
    }

    /**
     * This method may be defined by subclasses, to swap in a value that currently
     * is not contained in the local cache. Does nothing on this level
     *
     * @param key the key whose value should be loaded
     * @return the value that was loaded, or null if none.
     */
    protected V loadValueFromStorage(
            K key )
    {
        return null;
    }

    /**
     * This method may be defined by subclasses, to save a value to storage.
     * Does nothing on this level.
     *
     * @param key the key whose value was updated
     * @param newValue the new value
     */
    protected void saveValueToStorage(
            K key,
            V newValue )
    {}

    /**
     * This method may be defined by subclasses, to remove a value from storage.
     *
     * @param key the key whose value has been removed
     */
    protected void removeValueFromStorage(
            K key )
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDirectSmartMapListener(
            SwappingSmartMapListener<K,V> newListener )
    {
        theListeners.addDirect( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addWeakSmartMapListener(
            SwappingSmartMapListener<K,V> newListener )
    {
        theListeners.addWeak( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addSoftSmartMapListener(
            SwappingSmartMapListener<K,V> newListener )
    {
        theListeners.addSoft( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeSmartMapListener(
            SwappingSmartMapListener<K,V> oldListener )
    {
        theListeners.remove( oldListener );
    }

    /**
     * Fire an "an entry has been purged" event.
     *
     *
     * @param key the key of the element that has been purged
     * @param value the value that has been purged
     */
    protected void fireValuePurged(
            K key,
            V value )
    {
        theListeners.fireEvent( new Pair<>( key, value ), 0 );
    }

    /**
     * Fire an "a value has been loaded from storage" event.
     *
     * @param key the key of the element that has been loaded
     * @param value the value that has been loaded
     */
    protected void fireValueLoaded(
            K key,
            V value )
    {
        theListeners.fireEvent( new Pair<>( key, value ), 1 );
    }

    /**
     * Fire an "a value has been saved to storage" event.
     *
     * @param key the key of the element that has been saved
     * @param value the value that has been saved
     */
    protected void fireValueSaved(
            K key,
            V value )
    {
        theListeners.fireEvent( new Pair<>( key, value ), 2 );
    }

    /**
     * Fire a "a value has been removed from storage" event.
     *
     * @param key the key whose value has been removed
     */
    protected final void fireValueRemoved(
            K key )
    {
        theListeners.fireEvent( new Pair<>( key, null ), 3 );
    }


    /**
     * The local cache.
     */
    protected final Map<K,Reference<V>> theLocalCache;

    /**
     * The queue that into which deleted References are inserted.
     */
    protected final ReferenceQueue<V> theQueue = new ReferenceQueue<>();

    /**
     * Listeners to what is happening with this SmartMap.
     */
    private final FlexibleListenerSet<SwappingSmartMapListener<K,V>,Pair<K,V>,Integer> theListeners
            = new FlexibleListenerSet<SwappingSmartMapListener<K,V>,Pair<K,V>,Integer>() {
                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    protected void fireEventToListener(
                            SwappingSmartMapListener<K,V> listener,
                            Pair<K,V>                     event,
                            Integer                       parameter )
                    {
                        switch( parameter ) {
                            case 0:
                                listener.purged(GentleSmartMap.this, event.getName(), event.getValue() );
                                break;
                            case 1:
                                listener.loadedFromStorage(GentleSmartMap.this, event.getName(), event.getValue() );
                                break;
                            case 2:
                                listener.savedToStorage(GentleSmartMap.this, event.getName(), event.getValue() );
                                break;
                            case 3:
                                listener.removedFromStorage(GentleSmartMap.this, event.getName() );
                                break;
                            default:
                                log.error( "should not be here: " + parameter );
                        }
                    }
            };

    /**
     * Common for SoftEntryReference and WeakEntryReference.
     *
     * @param <K> the type of key
     * @param <V> the type of value
     */
    protected static interface EntryReference<K,V>
    {
        /**
         * Obtain the key.
         *
         * @return the key
         */
        public K getKey();

        /**
         * Obtain the value.
         *
         * @return the value
         */
        public V get();
    }

    /**
     * Override SoftReference to also hold the key.
     *
     * @param <K> the type of key
     * @param <V> the type of value
     */
    protected static class SoftEntryReference<K,V>
            extends
                SoftReference<V>
            implements
                EntryReference<K,V>
    {
        /**
         * Constructor.
         *
         * @param key the key
         * @param value the value
         * @param queue the ReferenceQueue into which the cleared Reference will be inserted
         */
        public SoftEntryReference(
                K                 key,
                V                 value,
                ReferenceQueue<V> queue )
        {
            super( value, queue );

            theKey = key;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K getKey()
        {
            return theKey;
        }

        /**
         * The key.
         */
        protected K theKey;
    }

    /**
     * Override WeakReference to also hold the key.
     *
     * @param <K> the type of key
     * @param <V> the type of value
     */
    protected static class WeakEntryReference<K,V>
            extends
                WeakReference<V>
            implements
                EntryReference<K,V>
    {
        /**
         * Constructor.
         *
         * @param key the key
         * @param value the value
         * @param queue the ReferenceQueue into which the cleared Reference will be inserted
         */
        public WeakEntryReference(
                K                 key,
                V                 value,
                ReferenceQueue<V> queue )
        {
            super( value, queue );

            theKey = key;

            if( key == null ) {
                throw new NullPointerException( "No null key allowed" );
            }
            if( value == null ) {
                throw new NullPointerException( "No null value allowed" );
            }
            if( queue == null ) {
                throw new NullPointerException( "No null queue allowed" );
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K getKey()
        {
            return theKey;
        }

        /**
         * The key.
         */
        protected K theKey;
    }

    /**
     * Default implementation that uses WeakReferences with reasonable defaults.
     *
     * @param <K> the key for the entries in the map
     * @param <V> the value for the entries of the map
     */
    public static class Weak<K,V>
        extends
            GentleSmartMap<K,V>
    {
        /**
         * Constructor.
         *
         * @param localCache the Map to use as a local cache.
         */
        public Weak(
                Map<K,Reference<V>> localCache )
        {
            super( localCache );
        }

        @Override
        protected Reference<V> createReference(
                K key,
                V value )
        {
            return new WeakEntryReference<>( key, value, theQueue );
        }

        @Override
        public boolean isPersistent()
        {
            return false;
        }

        @Override
        public void clear()
        {
            clearLocalCache();
        }

        @Override
        public int size()
        {
            return theLocalCache.size();
        }
    }

    /**
     * Default implementation that uses SoftReferences with reasonable defaults.
     *
     * @param <K> the key for the entries in the map
     * @param <V> the value for the entries of the map
     */
    public static class Soft<K,V>
        extends
            GentleSmartMap<K,V>
    {
        /**
         * Constructor.
         *
         * @param localCache the Map to use as a local cache.
         */
        public Soft(
                Map<K,Reference<V>> localCache )
        {
            super( localCache );
        }

        @Override
        protected Reference<V> createReference(
                K key,
                V value )
        {
            return new SoftEntryReference<>( key, value, theQueue );
        }

        @Override
        public boolean isPersistent()
        {
            return false;
        }

        @Override
        public void clear()
        {
            clearLocalCache();
        }

        @Override
        public int size()
        {
            return theLocalCache.size();
        }
    }
}
