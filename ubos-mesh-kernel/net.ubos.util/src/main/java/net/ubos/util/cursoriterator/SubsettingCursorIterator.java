//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.BiPredicate;

/**
 * A {@link CursorIterator} that iterates over a subset of another <code>CursorIterator</code>.
 *
 * @param <E> the type of element to iterate over
 */
public class SubsettingCursorIterator<E>
        extends
            AbstractCursorIterator<E>
{
    /**
     * Factory method.
     *
     * @param min the minimum index in the array that we return (inclusive).
     * @param max the maximum index in the array that we return (exclusive).
     * @param delegate the underlying iterator
     * @param <E> the type of element to iterate over
     * @return the created SubsettingCursorIterator
     */
    public static <E> SubsettingCursorIterator<E> create(
            E                 min,
            E                 max,
            CursorIterator<E> delegate )
    {
        return new SubsettingCursorIterator<>(
                min,
                max,
                delegate,
                new DEFAULT_EQUALS<>());
    }

    /**
     * Factory method.
     *
     * @param min the minimum index in the array that we return (inclusive).
     * @param max the maximum index in the array that we return (exclusive).
     * @param delegate the underlying iterator
     * @param equals the equality operation for E's
     * @param <E> the type of element to iterate over
     * @return the created SubsettingCursorIterator
     */
    public static <E> SubsettingCursorIterator<E> create(
            E                 min,
            E                 max,
            CursorIterator<E> delegate,
            BiPredicate<E,E>  equals )
    {
        return new SubsettingCursorIterator<>(
                min,
                max,
                delegate,
                new DEFAULT_EQUALS<>() );
    }

    /**
     * Constructor for subclasses only, use factory method.
     *
     * @param min the minimum index in the array that we return (inclusive).
     * @param max the maximum index in the array that we return (exclusive).
     * @param delegate the underlying iterator
     * @param equals the equality operation for E's
     */
    protected SubsettingCursorIterator(
            E                 min,
            E                 max,
            CursorIterator<E> delegate,
            BiPredicate<E,E>  equals )
    {
        super( equals );

        theMin      = min;
        theMax      = max;
        theDelegate = delegate;

        if( theMin != null ) {
            theDelegate.moveToBefore( theMin );
        } else {
            theDelegate.moveToBeforeFirst();
        }
    }

    /**
     * Obtain the minimum index that we return (inclusive).
     *
     * @return the minimum index, or null
     */
    public E getMin()
    {
        return theMin;
    }

    /**
     * Obtain the maximum index that we return (exclusive).
     *
     * @return the maximum index, or null
     */
    public E getMax()
    {
        return theMax;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekNext()
    {
        E ret = theDelegate.peekNext();
        if( ret != theMax ) {
            return ret;
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekPrevious()
    {
        E ret = theDelegate.peekPrevious();
        if( ret != theMin ) {
            return ret;
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        // we don't have any choice other than trying it
        CursorIterator<E> delegateTrial = theDelegate.createCopy();

        List<E> found = delegateTrial.next( n );
        if( found.size() < n ) {
            return false;
        }
        for( E current : found ) {
            if( theMax == current ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        // we don't have any choice other than trying it
        CursorIterator<E> delegateTrial = theDelegate.createCopy();

        List<E> found = delegateTrial.previous( n );
        if( found.size() < n ) {
            return false;
        }
        for( E current : found ) {
            if( theMin == current ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
    {
        E ret = theDelegate.next();
        if( ret == theMax ) {
            theDelegate.previous();
            throw new NoSuchElementException();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> next(
            int n )
    {
        List<E> almost = theDelegate.next( n );

        int count = 0;
        for( E current : almost ) {
            if( theMax == current ) {
                List<E> ret = almost.subList( 0, count );
                theDelegate.moveBy( count - almost.size() );
                return ret;
            }
            ++count;
        }
        return almost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
    {
        E ret = theDelegate.previous();
        if( ret == theMin ) {
            theDelegate.next();
            throw new NoSuchElementException();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> previous(
            int n )
    {
        List<E> almost = theDelegate.previous( n );

        int count = 0;
        for( E current : almost ) {
            if( theMin == current ) {
                List<E> ret = almost.subList( 0, count );
                theDelegate.moveBy( almost.size()-count );
                return ret;
            }
        }
        return almost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        CursorIterator<E> delegateTrial = theDelegate.createCopy();

        for( int i=0 ; i<n ; ++i ) {
            try {
                E found = delegateTrial.next();

                if( theEquals.test( found, theMax )) {
                    // not enough elements in subset
                    throw new NoSuchElementException();
                }
            } catch( NoSuchElementException ex ) {
                // not enough elements in delegate iterator
                throw new NoSuchElementException();
            }
        }
        // did work, move
        theDelegate = delegateTrial;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            E pos )
        throws
            NoSuchElementException
    {
        // FIXME? Is this right?
        CursorIterator<E> delegateTrial1 = theDelegate.createCopy();
        int steps;
        try {
            steps = delegateTrial1.moveToBefore( pos );
        } catch( NoSuchElementException ex ) {
            // delegate does not have enough elements
            throw new NoSuchElementException();
        }

        CursorIterator<E> delegateTrial2 = theDelegate.createCopy();
        List<E> found;
        E    limit;
        if( steps >= 0 ) {
            found = delegateTrial2.next( steps );
            limit = theMax;
        } else {
            found = delegateTrial2.previous( -steps );
            limit = theMin;
        }

        for( E current : found ) {
            if( limit == current ) {
                throw new NoSuchElementException();
            }
        }
        theDelegate = delegateTrial1;
        return steps;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            E pos )
        throws
            NoSuchElementException
    {
        // FIXME? Is this right?
        CursorIterator<E> delegateTrial1 = theDelegate.createCopy();
        int steps;
        try {
            steps = delegateTrial1.moveToAfter( pos );
        } catch( NoSuchElementException ex ) {
            // delegate does not have enough elements
            throw new NoSuchElementException();
        }

        CursorIterator<E> delegateTrial2 = theDelegate.createCopy();
        List<E> found;
        E    limit;
        if( steps >= 0 ) {
            found = delegateTrial2.next( steps );
            limit = theMax;
        } else {
            found = delegateTrial2.previous( -steps );
            limit = theMin;
        }

        for( E current : found ) {
            if( limit == current ) {
                throw new NoSuchElementException();
            }
        }
        theDelegate = delegateTrial1;
        return steps;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
    {
        theDelegate.remove();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubsettingCursorIterator<E> createCopy()
    {
        return new SubsettingCursorIterator<>( theMin, theMax, theDelegate.createCopy(), theEquals );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException
    {
        if( !( position instanceof SubsettingCursorIterator )) {
            throw new IllegalArgumentException( "Wrong type of CursorIterator: " + position );
        }
        SubsettingCursorIterator<E> realPosition = (SubsettingCursorIterator<E>) position;

        if( theMin != realPosition.theMin ) {
            throw new IllegalArgumentException( "Not the same lower bound" );
        }
        if( theMax != realPosition.theMax ) {
            throw new IllegalArgumentException( "Not the same upper bound" );
        }

        theDelegate.setPositionTo( realPosition.theDelegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        return theDelegate.moveToBefore( theMin );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        return theDelegate.moveToAfter( theMax );
    }

    /**
     * The delegate iterator.
     */
    protected CursorIterator<E> theDelegate;

    /**
     * The minimum element to return (inclusive).
     */
    protected E theMin;

    /**
     * The maximum element to return (exclusive).
     */
    protected E theMax;
}
