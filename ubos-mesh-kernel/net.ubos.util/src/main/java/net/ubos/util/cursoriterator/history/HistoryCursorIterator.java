//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator.history;

import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Extends CursorIterator to provide navigation methods based on time, in
 * addition to the sequence/location-based methods provided by CursorIterator.
 *
 * @param <E> the type of element to iterate over
 */
public interface HistoryCursorIterator<E>
        extends
            CursorIterator<E>
{
    /**
     * Move the cursor so that when {@link #next next} is invoked right afterwards,
     * the element at this time or the smallest amount of time larger than
     * the time is returned.
     *
     * @param time the time
     * @return the number of steps that were taken to move. Positive number means forward, negative backward
     */
    public int moveToJustBeforeTime(
            long time );

    /**
     * Move the cursor so that when {@link #previous previous} is invoked right afterwards,
     * the element at this time or the smallest amount of time smaller than
     * the time is returned.
     *
     * @param time the time
     * @return the number of steps that were taken to move. Positive number means forward, negative backward
     */
    public int moveToJustAfterTime(
            long time );

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<E> createCopy();
}
