//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple implementation of StringifierParameters.
 */
public class SimpleStringifierParameters
    implements
        StringifierParameters
{
    /**
     * Factory method.
     *
     * @return the created SimpleStringRepresentationParameters
     */
    public static SimpleStringifierParameters create()
    {
        SimpleStringifierParameters ret = new SimpleStringifierParameters( null );
        return ret;
    }

    /**
     * Factory method.
     *
     * @param map the initial content for this object
     * @return the created SimpleStringRepresentationParameters
     */
    public static SimpleStringifierParameters create(
            Map<String,?> map )
    {
        SimpleStringifierParameters ret = new SimpleStringifierParameters( null );
        ret.putAll( map );
        return ret;
    }

    /**
     * Factory method.
     *
     * @param delegate the delegate, if any
     * @return the created SimpleStringRepresentationParameters
     */
    public static SimpleStringifierParameters create(
            StringifierParameters delegate )
    {
        SimpleStringifierParameters ret = new SimpleStringifierParameters( delegate );
        return ret;
    }

    /**
     * Factory method.
     *
     * @param delegate the delegate, if any
     * @param map the initial content for this object
     * @return the created SimpleStringRepresentationParameters
     */
    public static SimpleStringifierParameters create(
            StringifierParameters delegate,
            Map<String,?>         map )
    {
        SimpleStringifierParameters ret = new SimpleStringifierParameters( delegate );
        ret.putAll( map );
        return ret;
    }

    /**
     * Constructor.
     *
     * @param delegate the delegate, if any
     */
    protected SimpleStringifierParameters(
            StringifierParameters delegate )
    {
        theDelegate = delegate;
    }

    /**
     * Obtain the delegate, if any.
     *
     * @return the delegate, if any
     */
    public StringifierParameters getDelegate()
    {
        return theDelegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        int ret = theStorage.size();
        if( theDelegate != null ) {
            ret += theDelegate.size();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        if( !theStorage.isEmpty() ) {
            return false;
        }
        if( theDelegate != null ) {
            return theDelegate.isEmpty();
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(
            String key )
    {
        Object ret = theStorage.get( key );
        if( ret == null && theDelegate != null ) {
            ret = theDelegate.get( key );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(
            String key,
            Object defaultValue )
    {
        Object ret = theStorage.get( key );
        if( ret == null ) {
            if( theDelegate == null ) {
                ret = defaultValue;
            } else {
                ret = theDelegate.get( key, defaultValue );
            }
        }
        return ret;
    }

    /**
     * Set a named value.
     *
     * @param key the name of the value
     * @param value the value
     */
    public void put(
            String key,
            Object value )
    {
        theStorage.put( key, value );
    }

    /**
     * Put all the members of the map into this object.
     *
     * @param map the map
     */
    public void putAll(
            Map<String,?> map )
    {
        for( String key : map.keySet() ) {
            Object value = map.get( key );
            theStorage.put( key, value );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringifierParameters with(
            String key,
            Object value )
    {
        SimpleStringifierParameters ret = new SimpleStringifierParameters( this );
        ret.put( key, value );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringifierParameters with(
            Map<String,?> map )
    {
        SimpleStringifierParameters ret = new SimpleStringifierParameters( this );
        ret.putAll( map );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SimpleStringifierParameters without(
            String key )
    {
        StringifierParameters newDelegate = theDelegate != null ? theDelegate.without( key ) : null;

        SimpleStringifierParameters ret = new SimpleStringifierParameters( newDelegate );
        for( String current : theStorage.keySet() ) {
            if( !current.equals( key )) {
                ret.put( current, theStorage.get( current ));
            }
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringifierParameters without(
            String [] keys )
    {
        StringifierParameters newDelegate = theDelegate != null ? theDelegate.without( keys ) : null;

        SimpleStringifierParameters ret = new SimpleStringifierParameters( newDelegate );
        for( String current : theStorage.keySet() ) {

            boolean found = false;
            for( int i=0 ; i<keys.length ; ++i ) {
                if( current.equals( keys[i] )) {
                    found = true;
                    break;
                }
            }
            if( !found ) {
                ret.put( current, theStorage.get( current ));
            }
        }

        return ret;
    }

    /**
     * The StringRepresentationParameters to ask if this object does not know of an entry.
     */
    protected final StringifierParameters theDelegate;

    /**
     * Storage.
     */
    protected final HashMap<String,Object> theStorage = new HashMap<>();
}
