//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap.m;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import net.ubos.util.smartmap.OrderedSmartMap;

/**
 * Simple in-memory implementation of OrderedSmartMap.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public class MOrderedSmartMap<K,V>
    extends
        AbstractMapSmartMap<K,V>
    implements
        OrderedSmartMap<K,V>
{
    /**
     * Factory  method.
     *
     * @param equals defines the order of the keys
     * @return the created MOrderedSmartMap
     * @param <K> the key for the entries in the map
     * @param <V> the value for the entries of the map
     */
    public static <K,V> MOrderedSmartMap<K,V> create(
            Comparator<K> equals )
    {
        return new MOrderedSmartMap<>( new TreeMap<>( equals ) );
    }

    /**
     * Constructor.
     *
     * @param storage the Map object to use for storage
     */
    protected MOrderedSmartMap(
            Map<K,V> storage )
    {
        super( storage );
    }
}
