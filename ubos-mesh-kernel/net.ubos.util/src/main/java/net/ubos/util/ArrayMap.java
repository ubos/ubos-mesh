//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import net.ubos.util.iterator.TranslatingIterator;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A Map that uses an array as implementation. This is slow, but it keeps the names in order.
 * 
 * @param <K> the map key type
 * @param <V> the map value type
 */
public class ArrayMap<K,V>
        implements
            Map<K,V>
{
    /**
     * Constructor with default initial size.
     */
    public ArrayMap()
    {
        theStorage = new ArrayList<>();
    }
    
    /**
     * Constructor with specified initial size.
     * 
     * @param size the initial size
     */
    public ArrayMap(
            int size )
    {
        theStorage = new ArrayList<>( size );
    }
    
    /**
     * {@inheritDoc]
     */
    @Override
    public int size()
    {
        return theStorage.size();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean isEmpty()
    {
        return theStorage.isEmpty();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean containsKey(
            Object key )
    {
        synchronized( theStorage ) {
            if( key == null ) {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( current.getKey() == null ) {
                        return true;
                    }
                }
            } else {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( key.equals( current.getKey() )) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean containsValue(
            Object value )
    {
        synchronized( theStorage ) {
            if( value == null ) {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( current.getValue() == null ) {
                        return true;
                    }
                }
            } else {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( value.equals( current.getValue() )) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V get(
            Object key )
    {
        synchronized( theStorage ) {
            if( key == null ) {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( current.getKey() == null ) {
                        return current.getValue();
                    }
                }
            } else {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( key.equals( current.getKey() )) {
                        return current.getValue();
                    }
                }
            }
        }
        return null;
        
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V put(
            K key,
            V value )
    {
        synchronized( theStorage ) {
            if( key == null ) {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( current.getKey() == null ) {
                        V ret = current.getValue();
                        current.setValue( value );
                        return ret;
                    }
                }
            } else {
                for( Map.Entry<K,V> current : theStorage ) {
                    if( key.equals( current.getKey() )) {
                        V ret = current.getValue();
                        current.setValue( value );
                        return ret;
                    }
                }
            }
        }
        theStorage.add( new MyPair<>( key, value ));
        return null;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V remove(
            Object key )
    {
        synchronized( theStorage ) {
            if( key == null ) {
                int i=0;
                for( Map.Entry<K,V> current : theStorage ) {
                    if( current.getKey() == null ) {
                        theStorage.remove( i );
                        return current.getValue();
                    }
                    ++i;
                }
            } else {
                int i=0;
                for( Map.Entry<K,V> current : theStorage ) {
                    if( key.equals( current.getKey() )) {
                        theStorage.remove( i );
                        return current.getValue();
                    }
                    ++i;
                }
            }
        }
        return null;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void putAll(
            Map<? extends K, ? extends V> t )
    {
        for( K key : t.keySet() ) {
            V value = t.get( key );
            
            put( key, value );
        }
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void clear()
    {
        theStorage.clear();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public synchronized Set<K> keySet()
    {
        if( theKeySet == null ) {
            theKeySet = new MyKeySet();
        }
        return theKeySet;
    }

    /**
     * Make method easier to call from JSP.
     * 
     * @return a set view of the mappings contained in this map.
     * @see #keySet
     */
    public Set<K> getKeySet()
    {
        return keySet();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public synchronized Collection<V> values()
    {
        if( theValueCollection == null ) {
            theValueCollection = new MyValueCollection();
        }
        return theValueCollection;
    }

    /**
     * Make method easier to call from JSP.
     * 
     * @return a set view of the mappings contained in this map.
     * @see #values
     */
    public Collection<V> getValues()
    {
        return values();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public synchronized Set<Map.Entry<K,V>> entrySet()
    {
        if( theEntrySet == null ) {
            theEntrySet = new MyEntrySet();
        }
        return theEntrySet;
    }

    /**
     * Make method easier to call from JSP.
     * 
     * @return a set view of the mappings contained in this map.
     * @see #entrySet
     */
    public Set<Map.Entry<K,V>> getEntrySet()
    {
        return entrySet();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean equals(
            Object o )
    {
        if( o instanceof ArrayMap ) {
            @SuppressWarnings("unchecked")
            boolean ret = theStorage.equals( ((ArrayMap<K,V>) o).theStorage );
            return ret;
        }
        return false;
    }
    
    /**
     * {@inheritDoc]
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    /**
     * Storage for the ArrayMap.
     */
    protected final ArrayList<Map.Entry<K,V>> theStorage;
    
    /**
     * View into the entries, allocated as needed.
     */
    protected MyEntrySet theEntrySet;

    /**
     * View on the keys, allocated as needed.
     */
    protected MyKeySet theKeySet;
    
    /**
     * View on the values, allocated as needed.
     */
    protected MyValueCollection theValueCollection;
    
    /**
     * Stores a name-value pair.
     * 
     * @param <K> the map key type
     * @param <V> the map value type
     */
    protected static class MyPair<K,V>
            extends
                Pair<K,V>
            implements
                Map.Entry<K,V>
    {
        /**
          * Constructor.
          *
          * @param name the name
          * @param value the value
          */
        public MyPair(
                K name,
                V value )
        {
            super( name, value );
        }

        /**
         * {@inheritDoc]
         */
        @Override
        public K getKey()
        {
            return super.getName();
        }

        /**
         * {@inheritDoc]
         */
        @Override
        public V setValue(
                V value )
        {
            V ret = theValue;
            theValue = value;
            return ret;
        }
    }
    
    /**
     * View into the set of entries.
     */
    protected class MyEntrySet
            extends
                AbstractSet<Map.Entry<K,V>>
    {
        /**
         * {@inheritDoc]
         */
        @Override
        public Iterator<Map.Entry<K,V>> iterator()
        {
            Iterator<Map.Entry<K,V>> ret = theStorage.iterator();
            return ret;
        }

        /**
         * {@inheritDoc]
         */
        @Override
        public int size()
        {
            return ArrayMap.this.size();
        }
    }
    
    /**
     * View into the set of keys.
     */
    protected class MyKeySet
            extends
                AbstractSet<K>
    {
        /**
         * {@inheritDoc]
         */
        @Override
        public Iterator<K> iterator()
        {
            return new TranslatingIterator<K,Map.Entry<K,V>>( theStorage.iterator() ) {
                @Override
                protected K translate(
                        Entry<K,V> org )
                {
                    return org.getKey();
                }
            };
        }

        /**
         * {@inheritDoc]
         */
        @Override
        public int size()
        {
            return ArrayMap.this.size();
        }
    }
    
    /**
     * View into the collection of values.
     */
    protected class MyValueCollection
            extends
                AbstractCollection<V>
    {
        /**
         * {@inheritDoc]
         */
        @Override
        public Iterator<V> iterator()
        {
            return new TranslatingIterator<V,Map.Entry<K,V>>( theStorage.iterator() ) {
                @Override
                protected V translate(
                        Entry<K,V> org )
                {
                    return org.getValue();
                }
            };
        }
        
        /**
         * {@inheritDoc]
         */
        @Override
        public int size()
        {
            return ArrayMap.this.size();
        }
    }
}
