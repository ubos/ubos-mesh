//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;

/**
 * A drop-in <code>java.io.InputStream</code> that buffers all information that comes by.
 */
public class BufferingInputStream
        extends
            InputStream
{
    /**
     * Constructor.
     * 
     * @param delegate the delegate InputStream
     */
    public BufferingInputStream(
            InputStream delegate )
    {
        this( delegate, DEFAULT_INITIAL_BUFFER_SIZE );
    }
    
    /**
     * Constructor.
     *
     * @param delegate the delegate InputStream
     * @param bufsize initial buffer size
     */
    public BufferingInputStream(
            InputStream delegate,
            int         bufsize )
    {
        if( delegate == null ) {
            throw new IllegalArgumentException();
        }
        theDelegate    = delegate;
        theCacheStream = new ByteArrayOutputStream( bufsize );
    }
    
    /**
     * {@inheritDoc]
     */
    @Override
    public int read()
        throws
            IOException
    {
        int r = theDelegate.read();
        if( r != -1 && theCacheStream != null ) {
            theCacheStream.write( r );
        }
        return r;
    }
    
    /**
     * {@inheritDoc]
     */
    @Override
    public int read(
            byte b[] )
        throws
            IOException
    {
        int r = theDelegate.read( b );
        if( r != -1 && theCacheStream != null ) {
            theCacheStream.write( b, 0, r );
        }
        return r;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public int read(
            byte b[],
            int  off,
            int  len )
        throws
            IOException
    {
        int r = theDelegate.read( b, off, len );
        if( r != -1 && theCacheStream != null ) {
            theCacheStream.write( b, off, r );
        }
        return r;
    }
    
    /**
     * {@inheritDoc]
     */
    @Override
    public long skip(
            long n )
        throws
            IOException
    {
        throw new UnsupportedOperationException( "don't know how to skip" );
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public int available()
        throws
            IOException
    {
        return theDelegate.available();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void close()
        throws
            IOException
    {       
        theDelegate.close();
        
        if( theCacheStream != null ) {
            theCacheStream.close();
        }
    }       
    
    /**
     * {@inheritDoc]
     */
    @Override
    public boolean markSupported()
    {
        return false;
    }   
    
    /**
     * {@inheritDoc]
     */
    @Override
    public void mark(
            int readAheadLimit )
    {       
        throw new UnsupportedOperationException( "don't know how to mark" );
    }   
    
    /**
     * {@inheritDoc]
     */
    @Override
    public void reset()
    {
        throw new UnsupportedOperationException( "don't know how to reset" );
    }

    /**
     * Obtain the buffer content.
     *
     * @return the buffer content
     */
    public byte [] getBuffer()
    {
        return theCacheStream.toByteArray();
    }

    /**
     * The default initial buffer size.
     */
    public static final int DEFAULT_INITIAL_BUFFER_SIZE = 4096;
    
    /**
     * The delegate InputStream.
     */
    protected InputStream theDelegate;

    /**
     * The OutputStream that buffers.
     */
    protected ByteArrayOutputStream theCacheStream;
}
