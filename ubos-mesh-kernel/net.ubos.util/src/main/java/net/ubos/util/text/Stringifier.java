//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;

/**
 * <p>Interface supported by objects that know how to turn an Object into a String,
 *    and parse it again. Many implementations are possible, including:</p>
 * <ul>
 *  <li>implementations that stringify a single, simple object, such as
 *      {@link IntegerStringifier IntegerStringifier}.</li>
 *  <li>implementations that stringify a complex object, such as
 *      {@link MessageStringifier MessageStringifier}.</li>
 * </ul>
 * <p>The original motivation for this interface and its related classes and interfaces were
 *    several perceived deficiencies of the <code>java.text.MessageFormat</code> class. It is mostly
 *    meant as a more powerful and particularly more extensible replacement. For example, and unlike
 *    <code>java.text.MessageFormat</code>, this framework allows the specification of sub-formatters
 *    for specific arguments; they are not hard-coded as in case of <code>java.text.MessageFormat</code>.</p>
 *
 * @param <T> the type of the Objects to be stringified
 */
public interface Stringifier<T>
{
    /**
     * Format an Object using this Stringifier using default parameters.
     *
     * @param arg the Object to format, or null
     * @return the formatted String
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     */
    public default String format(
            T arg )
        throws
            StringifierException
    {
        return format( arg, null, null );
    }

    /**
     * Format an Object using this Stringifier.
     *
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @param soFar the String so far, if any. This may influence stringification
     * @return the formatted String
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     */
    public abstract String format(
            T                     arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException;

    /**
     * Attempt to format an Object using this Stringifier using default parameters.
     * Depending on the actual type of arg provided, this Stringifier may or may not be able to perform the
     * operation and thrown a ClassCastException instead.
     *
     * @param arg the Object to format, or null
     * @return the formatted String
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     * @throws ClassCastException thrown if this Stringifier could not format the provided Object
     *         because the provided Object was not of a type supported by this Stringifier
     */
    public default String attemptFormat(
            Object arg )
        throws
            StringifierException,
            ClassCastException
    {
        return attemptFormat( arg, null, null );
    }

    /**
     * Attempt to format an Object using this Stringifier.
     * Depending on the actual type of arg provided, this Stringifier may or may not be able to perform the
     * operation and thrown a ClassCastException instead.
     *
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @param soFar the String so far, if any
     * @return the formatted String
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     * @throws ClassCastException thrown if this Stringifier could not format the provided Object
     *         because the provided Object was not of a type supported by this Stringifier
     */
    public abstract String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException;

    /**
     * Parse out the Object in rawString that were inserted using this Stringifier.
     *
     * @param rawString the String to parse
     * @return the found Object
     * @throws StringifierParseException thrown if a parsing problem occurred
     */
    public default T unformat(
            String rawString )
        throws
            StringifierParseException
    {
        return unformat( rawString, null );
    }

    /**
     * Parse out the Object in rawString that were inserted using this Stringifier.
     *
     * @param rawString the String to parse
     * @param factory the factory needed to create the parsed values, if any
     * @return the found Object
     * @throws StringifierParseException thrown if a parsing problem occurred
     */
    public abstract T unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException;

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully consume it.
     * The iterator returns zero elements if the String could not be fully parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String rawString )
    {
        return allParsingChoiceIterator( rawString, 0, rawString.length(), Integer.MAX_VALUE, null );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully consume it.
     * The iterator returns zero elements if the String could not be fully parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param max the maximum number of choices to be returned by the Iterator.
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String rawString,
            int    max )
    {
        return allParsingChoiceIterator( rawString, 0, rawString.length(), max, null );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully consume it.
     * The iterator returns zero elements if the String could not be fully parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String                     rawString,
            StringifierUnformatFactory factory )
    {
        return allParsingChoiceIterator( rawString, 0, rawString.length(), Integer.MAX_VALUE, factory );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully consume it.
     * The iterator returns zero elements if the String could not be fully parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            StringifierUnformatFactory factory )
    {
        return allParsingChoiceIterator( rawString, startIndex, endIndex, Integer.MAX_VALUE, factory );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully consume it.
     * The iterator returns zero elements if the String could not be fully parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param max the maximum number of choices to be returned by the Iterator.
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public abstract Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory );

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully or partially consume it.
     * The iterator returns zero elements if the String could not be fully or partially parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String rawString )
    {
        return partialParsingChoiceIterator( rawString, 0, rawString.length(), Integer.MAX_VALUE, null );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully or partially consume it.
     * The iterator returns zero elements if the String could not be fully or partially parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param max the maximum number of choices to be returned by the Iterator.
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String rawString,
            int    max )
    {
        return partialParsingChoiceIterator( rawString, 0, rawString.length(), max, null );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully or partially consume it.
     * The iterator returns zero elements if the String could not be fully or partially parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String                     rawString,
            StringifierUnformatFactory factory )
    {
        return partialParsingChoiceIterator( rawString, 0, rawString.length(), Integer.MAX_VALUE, factory );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully or partially consume it.
     * The iterator returns zero elements if the String could not be fully or partially parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public default Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            StringifierUnformatFactory factory )
    {
        return partialParsingChoiceIterator( rawString, startIndex, endIndex, Integer.MAX_VALUE, factory );
    }

    /**
     * Obtain an iterator that iterates through all the choices that exist for this Stringifier to
     * parse the String and fully or partially consume it.
     * The iterator returns zero elements if the String could not be fully or partially parsed by this Stringifier.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param max the maximum number of choices to be returned by the Iterator.
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public abstract Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory );
}
