//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Factors out common functionality for Stringifiers that process numbers.
 *
 * @param <T> the type of the Objects to be stringified
 */
public abstract class NumberStringifier<T>
        extends
            AbstractStringifier<T>
{
    /**
     * Constructor.
     *
     * @param minDigits the minimum number of digits, or -1 if none
     * @param maxDigits the maximum number of digits, or -1 if none
     * @param radix the radix to use, e.g. 16 for hexadecimal
     */
    protected NumberStringifier(
            int minDigits,
            int maxDigits,
            int radix )
    {
        theMinDigits = minDigits;
        theMaxDigits = maxDigits;
        theRadix     = radix;
    }

    /**
     * Format an Object using this Stringifier. This is for subclasses only.
     *
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @param soFar the String so far, if any
     * @return the formatted String
     */
    protected String format(
            long                  arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret;
        if( theMinDigits <= 0 ) {
            ret = String.valueOf( arg );

        } else {
            boolean negative;

            if( arg >= 0 ) {
                ret = Long.toString( arg, theRadix );
                negative = false;
            } else {
                ret = Long.toString( -arg, theRadix );
                negative = true;
            }
            StringBuilder prepend = new StringBuilder();
            if( negative ) {
                prepend.append( '-' );
            }
            for( int i=ret.length() ; i<theMinDigits ; ++i ) {
                prepend.append( '0' ); // leading zer0
            }
            prepend.append( ret );
            ret = prepend.toString();
        }
        ret = potentiallyShorten( ret, pars );
        return ret;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg instanceof Short ) {
            return format( (Short)arg, pars, soFar );
        } else if( arg instanceof Long ) {
            return format( (Long)arg, pars, soFar );
        } else {
            return format( ((Integer)arg).longValue(), pars, soFar );
        }
    }

    /**
     * Is this a valid char for this Stringifier.
     *
     * @param pos position
     * @param min limits the considered String by this minimum position (inclusive)
     * @param max limits the considered String by this maximum position (exclusive)
     * @param s the String on whose position we find the char
     * @return true or false
     */
    boolean validChar(
            int    pos,
            int    min,
            int    max,
            String s )
    {
        int length = s.length();
        if( max > length ) {
            return false;
        }
        if( pos >= length ) {
            return false;
        }
        if( pos < min ) {
            return false;
        }

        char c = s.charAt( pos );
        c = Character.toLowerCase( c );

        if( pos == min && length > min ) {
            if( c == '+' || c == '-' ) {
                if( validChar( pos+1, min, max, s )) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        boolean ret;
        if( c < '0' ) {
            ret = false;
        } else if( c <= '9' ) {
            if( c - '0' >= theRadix ) {
                ret = false;
            } else {
                ret = true;
            }
        } else if( c < 'a' ) {
            ret = false;
        } else if( c - 'a' + 10 >= theRadix ) {
            ret = false;
        } else {
            ret = true;
        }
        return ret;
    }

    /**
     * The minimum number of digits to emit, and to accept. -1 means "don't pay any attention".
     */
    protected final int theMinDigits;

    /**
     * The maximum number of digital to emit, and to accept. -1 means "don't pay any attention".
     */
    protected final int theMaxDigits;

    /**
     * The radix to use, e.g. 16 for hexadecimal.
     */
    protected final int theRadix;
}
