//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * A delegating Stringifier that prepends and appends Strings.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class PrePostfixDelegatingStringifier<T>
        extends
             AbstractDelegatingStringifier<T>
{
    /**
     * Factory method.
     *
     * @param start the string to insert at the beginning
     * @param end the string to append at the end
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @return the created PrePostfixDelegatingStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> PrePostfixDelegatingStringifier<T> create(
            String         start,
            String         end,
            Stringifier<T> delegate )
    {
        return new PrePostfixDelegatingStringifier<>( start, end, delegate );
    }

    /**
     * Constructor.
     *
     * @param start the string to insert at the beginning
     * @param end the string to append at the end
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     */
    protected PrePostfixDelegatingStringifier(
            String         start,
            String         end,
            Stringifier<T> delegate )
    {
        super( delegate );

        theStart       = start;
        theEnd         = end;
    }

    /**
     * Obtain the start String, if any.
     *
     * @return the start String, if any
     */
    public String getStart()
    {
        return theStart;
    }

    /**
     * Obtain the end String, if any.
     *
     * @return the end String, if any
     */
    public String getEnd()
    {
        return theEnd;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            T                     arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        StringBuilder ret  = new StringBuilder();

        if( theStart != null ) {
            ret.append( theStart );
        }
        String childInput = theDelegate.attemptFormat( arg, pars, soFar );
        ret.append( childInput );

        if( theEnd != null ) {
            ret.append( theEnd );
        }

        return potentiallyShorten( ret.toString(), pars );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException
    {
        return format( (T) arg, pars, soFar );
    }

    /**
     * The String to insert at the beginning. May be null.
     */
    protected final String theStart;

    /**
     * The String to append at the end. May be null.
     */
    protected final String theEnd;
}
