//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

import java.text.ParseException;

/**
 * Subclasses the JDK's ParseException to support the UBOS Data Mesh localization framework.
 */
public abstract class LocalizedParseException
    extends
        ParseException
    implements
        LocalizedException
{
    /**
     * Constructor.
     *
     * @param string the String that could not be parsed
     * @param message the Exception's message, if any
     * @param errorOffset the offset, into the string, where the error occurred
     * @param cause the underlying cause, if any
     */
    protected LocalizedParseException(
            String    string,
            String    message,
            int       errorOffset,
            Throwable cause )
    {
        super( message, errorOffset );
        initCause( cause );

        theString = string;
    }

    /**
     * Obtain the String that could not be parsed.
     *
     * @return the String that could not be parsed
     */
    public String getString()
    {
        return theString;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        return AbstractLocalizedException.assembleDefaultLocalizedMessage( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { getCause().getMessage(), getCause().getLocalizedMessage() };
    }

    /**
     * The String that could not be parsed.
     */
    protected String theString;
}
