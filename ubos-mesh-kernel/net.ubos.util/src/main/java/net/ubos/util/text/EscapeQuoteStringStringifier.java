//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifier for HTML input fields where double-quotes must be escaped.
 */
public class EscapeQuoteStringStringifier
    extends
        StringStringifier
{
    /**
     * Factory method.
     *
     * @return the created EscapeQuoteStringStringifier
     */
    public static EscapeQuoteStringStringifier create()
    {
        return new EscapeQuoteStringStringifier();
    }

    /**
     * No-op constructor. Use factory method.
     */
    protected EscapeQuoteStringStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escape(
            String s )
    {
        return s.replaceAll( "&quot;", "&amp;quot;" ).replaceAll( "\"", "&quot;" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String unescape(
            String s )
    {
        return s.replaceAll( "&quot;", "\"" ).replaceAll( "&amp;quot;", "&quot;" );
    }
}
