//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import net.ubos.util.logging.Log;

/**
  * A collection of String manipulation methods that we found convenient.
  */
public abstract class StringHelper
{
    private static final Log log = Log.getLogInstance( StringHelper.class ); // our own, private logger

    /**
     * Private no-op constructor to keep this abstract.
     */
    private StringHelper()
    {}

    /**
     * Compare two Strings.
     *
     * @param one the first String
     * @param two the second String
     * @return comparison value, according to String.compareTo
     * @param <T> the type, such as String
     */
    public static <T> int compareTo(
            Comparable<T> one,
            T             two )
    {
        int ret;
        if( one == null ) {
            ret = two == null ? 0 : Integer.MIN_VALUE;
        } else if( two == null ) {
            ret = Integer.MAX_VALUE;
        } else {
            ret = one.compareTo( two );
        }
        return ret;
    }

    /**
     * Join the elements returned by an Iterator in String form, like Perl.
     *
     * @param iter the Iterator returning the elements
     * @return the joined String
     * @see net.ubos.util.ArrayHelper#join(java.lang.Object[])
     */
    public static String join(
            Iterator<?> iter )
    {
        return join( ", ", iter );
    }

    /**
     * Join the elements returned by an Iterator in String form, like Perl.
     *
     * @param separator the separator between the data elements
     * @param iter the Iterator returning the elements
     * @return the joined String
     * @see net.ubos.util.ArrayHelper#join(java.lang.String, java.lang.Object[])
     */
    public static String join(
            String      separator,
            Iterator<?> iter )
    {
        return join( separator, "", "", "null", iter );
    }

    /**
     * Join the elements returned by an Iterator in String form, like Perl.
     *
     * @param separator the separator between the data elements
     * @param prefix the prefix, if the data is non-null
     * @param postfix the prefix, if the data is non-null
     * @param ifNull to be written if the data is null
     * @param iter the Iterator returning the elements
     * @return the joined String
     * @see net.ubos.util.ArrayHelper#join(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Object[])
     */
    public static String join(
            String      separator,
            String      prefix,
            String      postfix,
            String      ifNull,
            Iterator<?> iter )
    {
        if( !iter.hasNext() ) {
            return ifNull;
        }
        String       sep = "";
        StringBuilder ret = new StringBuilder();

        ret.append( prefix );
        while( iter.hasNext() ) {
            Object current = iter.next();

            ret.append( sep );
            ret.append( current );
            sep = separator;
        }
        ret.append( postfix );
        return ret.toString();
    }

    /**
     * We use backslashes as escape.
     */
    static final char ESCAPE = '\\';

    /**
      * Tokenize a String into String[] with specified delimiters.
      * We cannot use StringTokenizer as that doesn't allow us to use \ as
      * an escape character.
      *
      * @param raw the input stream
      * @param datokens the tokens for which we look
      * @param quotes the quotes that we respect
      * @return the tokenized String
      */
    public static String [] tokenize(
            String raw,
            String datokens,
            String quotes )
    {
        if( raw == null ) {
            return new String[0];
        }
        ArrayList<String> v = new ArrayList<>();
        int count = 0;

        for( int from = 0; from<raw.length() ; ++from ) {
            int to;
            boolean escapeOn = false;
            boolean quoteOn  = false;
            StringBuilder buf = new StringBuilder( raw.length() );

            for( to = from; to < raw.length() ; ++to ) {
                char c = raw.charAt( to );
                if( c == ESCAPE ) {
                    escapeOn = true;
                    continue;
                }
                if( quotes != null && quotes.indexOf( c ) >= 0 ) {
                    quoteOn = !quoteOn;
                    buf.append( c );
                    continue;
                }
                if( escapeOn ) {
                    buf.append( c );
                    escapeOn = false;

                } else if( quoteOn ) {
                    buf.append( c );

                } else {
                    boolean foundDelim = false;
                    for( int i=0 ; i<datokens.length() ; ++i ) {
                        if( c == datokens.charAt( i )) {
                            foundDelim = true;
                            break;
                        }
                    }
                    if( foundDelim ) {
                        break;
                    } else {
                        buf.append( c );
                    }
                }
            }
            v.add( buf.toString() );
            ++count;
            from = to;
        }

        String [] tokens = new String[ count ];
        v.toArray( tokens );

        if( log.isDebugEnabled() ) {
            log.debug( "parsed \"" + raw + "\" into " + ArrayHelper.arrayToString( tokens ));
        }
        return tokens;
    }

    /**
     * Tokenize a String into String[] with a default datokens.
     *
     * @param raw the input String
     * @return the tokenized String
     */
    public static String [] tokenize(
            String raw )
    {
        return tokenize( raw, DEFAULT_TOKENS, DEFAULT_QUOTES );
    }

    /**
     * Tokenize a String into URL[] with specified delimiters.
     *
     * @param raw the input String
     * @param datokens the tokens for which we look
     * @param quotes the quotes that we respect
     * @return the tokenized URLs
     */
    public static URL [] tokenizeToURL(
            String raw,
            String datokens,
            String quotes )
    {
        String [] stringArray = tokenize( raw, datokens, quotes );

        URL [] ret = new URL[ stringArray.length ];
        for( int i=0 ; i<ret.length ; ++i ) {
            try {
                ret[i] = new URL( stringArray[i] );
            } catch( MalformedURLException ex ) {
                log.error( ex );
                ret[i] = null;
            }
        }
        return ret;
    }

    /**
     * Tokeinze a String into URL [] with a default datokens.
     *
     * @param raw the input String
     * @return the tokenized URLs
     */
    public static URL [] tokenizeToURL(
            String raw )
    {
        return tokenizeToURL( raw, DEFAULT_TOKENS, DEFAULT_QUOTES );
    }

    /**
      * Replace each occurrence of token with replaceWith.
      *
      * @param source the source String
      * @param token the token to replace
      * @param replaceWith the String to substitute
      * @return a String derived from the source String where each occurrence of token
      *         has been replaced with replaceWith
      */
    public static String replace(
            String source,
            String token,
            String replaceWith )
    {
        StringBuilder ret   = new StringBuilder();
        int           start = 0;
        int           end;

        while(( end = source.indexOf( token, start )) != -1 ) {
            ret.append( source.substring( start, end ));
            ret.append( replaceWith );
            start = end + token.length();
        }
        ret.append( source.substring( start, source.length()));
        return ret.toString();
    }

    /**
     * Indent a String.
     *
     * @param input the input String
     * @return the indented String. This returns StringBuilder as that is usually more efficient.
     */
    public static StringBuilder indent(
            CharSequence input )
    {
        return indent( input, "    ", 1, 1 );
    }

    /**
     * Indent a String.
     *
     * @param input the input String
     * @param indent the indent to use
     * @return the indented String. This returns StringBuilder as that is usually more efficient.
     */
    public static StringBuilder indent(
            CharSequence input,
            String       indent )
    {
        return indent( input, indent, 1, 1 );
    }

    /**
     * Indent a String.
     *
     * @param input the input String
     * @param indent the indent to use
     * @param firstLineN the number of indents to use for the first line
     * @param sunsequentN the number of indents to use for subsequent lines
     * @return the indented String. This returns StringBuilder as that is usually more efficient.
     */
    public static StringBuilder indent(
            CharSequence input,
            String       indent,
            int          firstLineN,
            int          sunsequentN )
    {
        StringBuilder buf = new StringBuilder( input.length() + indent.length() * firstLineN ); // one line is sure to fit

        for( int k=0 ; k<firstLineN ; ++k ) {
            buf.append( indent );
        }
        for( int i=0 ; i<input.length() ; ++i ) {
            char c = input.charAt( i );
            switch( c ) {
                case '\n':
                    buf.append( c );
                    for( int k=0 ; k<sunsequentN ; ++k ) {
                        buf.append( indent );
                    }
                    break;
                default:
                    buf.append( c );
                    break;
            }
        }
        return buf;
    }

    /**
     * Obtain N blanks.
     *
     * @param n the number of blanks
     * @param blank the text String to use as blank
     * @return a String with N blanks
     */
    public static String blanks(
            int    n,
            String blank )
    {
        if( n <= 0 ) {
            return "";
        }
        if( n==1 ) {
            return blank;
        }

        StringBuilder ret = new StringBuilder( blank.length() * n );
        for( int i=0 ; i<n ; ++i ) {
            ret.append( blank );
        }
        return ret.toString();
    }

    /**
     * Obtain N blanks.
     *
     * @param n the number of blanks
     * @return a String with N blanks
     */
    public static String blanks(
            int n )
    {
        return blanks( n, " " );
    }

    /**
     * Escape HTML characters in String. Inspired by <code>http://www.rgagnon.com/javadetails/java-0306.html</code>
     *
     * @param s the unescaped String
     * @return the escaped String
     */
    public static String stringToHtml(
            String s )
    {
        if( s == null ) {
            return NULL_STRING;
        }
        StringBuilder sb = new StringBuilder( s.length() * 5 / 4 ); // fudge

        // true if last char was blank
        boolean lastWasBlankChar = false;
        int len = s.length();

        for( int i=0; i<len; ++i ) {
            char c = s.charAt( i );
            switch( c ) {
                case ' ':
                    // blank gets extra work,
                    // this solves the problem you get if you replace all
                    // blanks with &nbsp;, if you do that you loss
                    // word breaking
                    if( lastWasBlankChar ) {
                        lastWasBlankChar = false;
                        sb.append("&nbsp;");
                    } else {
                        lastWasBlankChar = true;
                        sb.append(' ');
                    }
                    break;
                case '\n':
                    sb.append( "<br />\n" );
                    break;
                default:
                    lastWasBlankChar = false;
                    int ci = 0xffff & c;
                    if( ci < 128 ) {
                        // nothing special only 7 Bit

                        boolean done = false;
                        for( int k=0 ; k<htmlChars.length ; ++k ) {
                            if( c == htmlChars[k] ) {
                                sb.append( htmlFrags[k] );
                                done = true;
                            }
                        }
                        if( !done ) {
                            sb.append( c );
                        }
                    } else {
                        // Not 7 Bit use the unicode system
                        sb.append("&#");
                        sb.append( Integer.toString( ci ));
                        sb.append(';');
                    }   break;
            }
        }
        return sb.toString();
    }

    /**
     * Unescape HTML escape characters in String.
     *
     * @param s the escaped String
     * @return the unescaped String
     */
    public static String htmlToString(
            String s )
    {
        StringBuilder sb = new StringBuilder( s.length() );

        int len = s.length();

        for( int i=0; i<len; ++i ) {
            char c = s.charAt( i );
            if( c != '&' ) {
                sb.append( c );
            } else {
                if( i+2 < len && s.charAt( i+1 ) == '#' ) { // +2 because we need at least one after the #
                    int semi = s.indexOf( '#', i+1 );
                    if( semi >= 0 ) {
                        char unicode = Character.forDigit( Integer.parseInt( s.substring( i+2, semi )), 10 );
                        sb.append( unicode );

                    } else {
                        sb.append( c ); // leave everything normal
                    }
                } else {
                    for( int k=0 ; i<htmlFrags.length ; ++k ) {
                        if( s.regionMatches( i, htmlFrags[k], 0, htmlFrags[k].length() )) {
                            sb.append( htmlChars[k] );
                            i += htmlFrags[k].length()-1; // one less, because the loop increments anyway
                            break;
                        }
                    }
                }
            }
        }
        return sb.toString();
    }

    /**
     * Escape those characters in a String that make it "replaceable character data", per W3C.
     * See http://www.w3.org/TR/html-markup/syntax.html#replaceable-character-data
     *
     * @param s the unescaped String
     * @param enclosing the enclosing html tag, e.g. "textarea"
     * @return the escaped String
     */
    public static String stringToReplaceableCharacterDataHtml(
            String s,
            String enclosing )
    {
        if( s == null ) {
            return NULL_STRING;
        }
        StringBuilder sb = new StringBuilder( s.length() * 5 / 4 ); // fudge

        // true if last char was blank
        int len = s.length();

        for( int i=0; i<len; ++i ) {
            char c = s.charAt( i );

            int ci = 0xffff & c;
            if (ci < 128 ) {
                // nothing special only 7 Bit

                if( c == '&' ) {
                    sb.append( "&amp;" );

                } else if( c == '<' && s.substring( i+1 ).toLowerCase().startsWith( "/" + enclosing )) {
                    sb.append( "&lt;" );

                } else {
                    sb.append( c );
                }

            } else {
                // Not 7 Bit use the unicode system
                sb.append("&#");
                sb.append( Integer.toString( ci ));
                sb.append(';');
            }
        }
        return sb.toString();
    }

    /**
     * Unescape those characters in a String that make it "replaceable character data", per W3C.
     *
     * @param s the escaped String
     * @param enclosing the enclosing html tag, e.g. "textarea"
     * @return the unescaped String
     */
    public static String replaceableCharacterDataHtmlToString(
            String s,
            String enclosing )
    {
        StringBuilder sb = new StringBuilder( s.length() );

        int len = s.length();

        for( int i=0; i<len; ++i ) {
            char c = s.charAt( i );
            if( c != '&' ) {
                sb.append( c );
            } else {
                if( i+2 < len && s.charAt( i+1 ) == '#' ) { // +2 because we need at least one after the #
                    int semi = s.indexOf( '#', i+1 );
                    if( semi >= 0 ) {
                        char unicode = Character.forDigit( Integer.parseInt( s.substring( i+2, semi )), 10 );
                        sb.append( unicode );

                    } else {
                        sb.append( c ); // leave everything normal
                    }
                } else if( s.regionMatches( i, "&amp;", 0, 5 )) {
                    sb.append( "&" );
                }
            }
        }
        return sb.toString();
    }

    /**
     * Escape characters in a String so the output becomes a valid URL for an a tag in HTML.
     *
     * @param s the unescaped String
     * @return the escapedString
     */
    public static String stringToHtmlUrl(
            String s )
    {
        String ret = s.replaceAll( "&", "&amp;" );
        return ret;
    }

    /**
     * Escape characters in a String so the output becomes a valid String
     * in JavaScript.
     *
     * @param s the unescaped String
     * @return the escaped String
     */
    public static String stringToJavaString(
            String s )
    {
        if( s == null ) {
            return NULL_STRING;
        }
        StringBuilder sb = new StringBuilder( s.length() * 5 / 4 ); // fudge

        int len = s.length();
        for( int i=0; i<len; ++i ) {
            char c = s.charAt( i );

            switch( c ) {
                case '"':
                    sb.append( "\\\"" );
                    break;

                case '\'':
                    sb.append( "\\'" );
                    break;

                case '\r':
                    sb.append( "\\r" );
                    break;

                case '\n':
                    sb.append( "\\n" );
                    break;

                case '\\':
                    sb.append( "\\\\" );
                    break;

                default:
                    if( Character.isISOControl( c )) {
                        String v = String.valueOf( (int) c );

                        sb.append( "\\u" );
                        for( int j=v.length() ; j<4 ; ++j ) {
                            sb.append( '0' );
                        }
                        sb.append( v );
                        sb.append( c );
                    } else {
                        sb.append( c );
                    }
                    break;
            }
        }
        return sb.toString();
    }

    /**
     * Convert a byte array into an equivalent hexadecimal string.
     *
     * @param in the byte array
     * @return the String
     */
    public static String toHexString(
            byte [] in )
    {
        final String HEXES = "0123456789abcdef";

        StringBuilder ret = new StringBuilder( in.length*2 );
        for( byte b : in ) {
            ret.append( HEXES.charAt( (b & 0xF0) >> 4) );
            ret.append( HEXES.charAt(  b & 0x0F      ) );
        }

        return ret.toString();
    }

    /**
     * Convert a hexadecimal string into an equivalent byte array.
     *
     * @param s the String
     * @return the byte array
     */
    public static byte [] hexStringToBytes(
            String s )
    {
        if( s.length() % 2 == 1 ) {
            s = "0" + s;
        }
        byte [] ret = new byte[ s.length() / 2 ];
        for( int i=0 ; i<ret.length ; ++i ) {
            char c1 = s.charAt( 2*i );
            char c2 = s.charAt( 2*i+1 );
            ret[i] = (byte) (( hexToNibble( c1 ) << 4 ) | hexToNibble( c2 ) );
        }
        return ret;
    }

    /**
     * Helper to convert a hex character to a nibble.
     *
     * @param c the character
     * @return the nibble
     */
    private static byte hexToNibble(
            char c )
    {
        if( c >= '0' && c <= '9' ) {
            return (byte) ( c - '0' );
        }
        if( c >= 'A' && c <= 'F' ) {
            return (byte) ( c - 'A' + 10 );
        }
        if( c >= 'a' && c <= 'f' ) {
            return (byte) ( c - 'a' + 10 );
        }
        log.error( "Invalid hex char:", c );
        return 0;
    }

    /**
     * If a String contains a prefix, strip the prefix. Otherwise return the original String.
     *
     * @param s the String
     * @param prefix the prefix
     * @return the String without the prefix
     */
    public static String stripPrefixIfPresent(
            String s,
            String prefix )
    {
        if( s != null && s.startsWith( prefix )) {
            return s.substring( prefix.length() );
        }
        return s;
    }

    /**
     * The set of characters we replace in HTML.
     */
    protected static final char [] htmlChars = {
            '&',
            '<',
            '>'
    };

    /**
     * The set of HTML fragments we replace the characters with.
     */
    protected static final String [] htmlFrags = {
            "&amp;",
            "&lt;",
            "&gt;"
    };

    static {
        if( htmlChars.length != htmlFrags.length ) {
            throw new IllegalArgumentException( "htmlChars and htmlFrags not the same length" );
        }
    }

    /**
     * Make sure a String is not longer than <code>maxLength</code>. This is accomplished
     * by taking out characters in the middle if needed.
     *
     * @param in the input String
     * @param maxLength the maximally allowed length. -1 means unlimited.
     * @return the String, potentially shortened
     */
    public static String potentiallyShorten(
            String in,
            int    maxLength )
    {
        if( in == null || in.length() == 0 ) {
            return "";
        }
        if( maxLength < 0 ) {
            return in;
        }

        final String insert = "...";
        int          fromEnd = 5; // how many characters we leave at the end

        String ret = in;
        if( maxLength > 0 && ret.length() > maxLength ) {
            fromEnd = Math.min( fromEnd, ret.length() - maxLength );
            ret = ret.substring( 0, maxLength-fromEnd-insert.length() ) + insert + ret.substring( ret.length() - fromEnd );
        }
        return ret;
    }

    /**
     * Find the Capitalization with this name. Return null if not found.
     *
     * @param name the name of the Capitalization
     * @return the Capitalization
     */
    public static Capitalization findCapitalizationOrNull(
            String name )
    {
        Capitalization ret = Capitalization.valueOf( name );
        return ret;
    }

    /**
     * Find the Capitalization with this name. Return the UNCHANGED Capitalization if not found.
     *
     * @param name the name of the Capitalization
     * @return the Capitalization
     */
    public static Capitalization findCapitalizationOrDefault(
            String name )
    {
        Capitalization ret = Capitalization.valueOf( name );
        if( ret == null ) {
            ret = Capitalization.UNCHANGED;
        }
        return ret;
    }

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( StringHelper.class );

    /**
     * The String to return if an argument is null.
     */
    public static final String NULL_STRING = theResourceHelper.getResourceStringOrDefault( "NullString", "null" );

    /**
     * Our default datokens -- used to determine where to split the string when tokenizing.
     */
    public static final String DEFAULT_TOKENS = theResourceHelper.getResourceStringOrDefault( "DefaultTokens", ",;" );

    /**
     * Our default quotes -- used to ignore datokens when tokenizing.
     */
    public static final String DEFAULT_QUOTES = theResourceHelper.getResourceStringOrDefault( "DefaultQuotes", "\"\'" );

    /**
     * Default logging depth.
     */
    public static final int DEFAULT_LOG_DEPTH = 5;

    /**
     * Convenience constant.
     */
    public static final String [] EMPTY_ARRAY = {};

    /**
     * Defines various capitalization strategies.
     */
    public static enum Capitalization
    {
        /**
         * Leave the String as it is. This is the degenerate case.
         */
        UNCHANGED() {
            @Override
            public String capitalize(
                    String text )
            {
                return text;
            }
        },

        /**
         * Make the String all lowercase.
         * So "hail to the Chief of ABC, today and tomorrow" becomes "hail to the chief of abc, today and tomorrow".
         */
        LOWERCASE() {
            @Override
            public String capitalize(
                    String text )
            {
                return text.toLowerCase();
            }
        },

        /**
         * Make the String all uppercase.
         * So "hail to the Chief of ABC, today and tomorrow" becomes "HAIL TO THE CHIEF OF ABC, TODAY AND TOMORROW".
         */
        UPPERCASE() {
            @Override
            public String capitalize(
                    String text )
            {
                return text.toUpperCase();
            }
        },

        /**
         * Capitalize the first letter, leave everything else unchanged.
         * So "hail to the Chief of ABC, today and tomorrow" becomes "Hail to the Chief of ABC, today and tomorrow".
         */
        VERYFIRST() {
            @Override
            public String capitalize(
                    String text )
            {
                return capFirst( text );
            }
        },

        /**
         * Capitalize the first letter of each word, leave everything else unchanged.
         * So "hail to the Chief of ABC, today and tomorrow" becomes "Hail To The Chief Of ABC, Today And Tomorrow".
         */
        FIRST() {
            @Override
            public String capitalize(
                    String text )
            {
                StringBuilder buf = new StringBuilder();
                String        sep = "";
                for( String word : text.split( "\\+s" )) {
                    buf.append( sep );
                    buf.append( capFirst( word ));
                    sep = " ";
                }
                return buf.toString();
            }
        },

        /**
         * Proper English headline capitalization should be attempted.
         * So "hail to the Chief of ABC, today and tomorrow" becomes "Hail to the Chief of ABC, Today and Tomorrow".
         *
         * This code can probably use more work.
         */
        HEADLINE() {
            @Override
            public String capitalize(
                    String text )
            {
                StringBuilder buf = new StringBuilder();
                String        sep = "";

                String [] split = text.split( "\\+s" );
                for( int i=0 ; i<split.length ; ++i ) {
                    buf.append( sep );
                    if( i == 0 || ( i < split.length-1 && !DONT_CAP_IN_HEADLINES.contains( split[i].toLowerCase() ))) {
                        buf.append( capFirst( split[i] ));
                    } else {
                        buf.append( split[i] );
                    }
                    sep = " ";
                }
                return buf.toString();
            }
        };

        /**
         * Perform the capitalization.
         *
         * @param text the text to be capitalized
         * @return the capitalized text
         */
        public abstract String capitalize(
                String text );

        /**
         * Helper method to capitalize the first letter of word.
         *
         * @param word the word
         * @return the word, potentially capitalized
         */
        protected String capFirst(
                String word )
        {
            String ret;
            if( word.length() > 1 ) {
                ret = word.substring( 0, 1 ).toUpperCase() + word.substring( 1 );
            } else {
                ret = word.toUpperCase();
            }
            return ret;
        }

        /**
         * Set of words not to capitalize in headlines in English. This is likely incomplete.
         */
        protected static final Set<String> DONT_CAP_IN_HEADLINES = new HashSet<>();
        static {
            DONT_CAP_IN_HEADLINES.addAll( Arrays.asList(
                    "and",
                    "but",
                    "or",
                    "for",
                    "nor",
                    "in",
                    "of",
                    "the",
                    "a" ));
        }
    }
}
