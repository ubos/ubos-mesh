//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator.history;

import net.ubos.util.cursoriterator.MappingCursorIterator;

/**
 * A HistoryCursorIterator that delegates to another HistoryCursorIterator, but maps the
 * values back and forth.
 *
 * @param <E> the type of element to iterate over
 * @param <D> the type of element the delegate CursorIterator iterates over
 */
public class MappingHistoryCursorIterator<E,D>
    extends
        MappingCursorIterator<E,D>
    implements
        HistoryCursorIterator<E>
{
    /**
     * Constructor.
     *
     * @param delegate the underling Iterator
     * @param mapper defines how to map
     */
    public MappingHistoryCursorIterator(
            HistoryCursorIterator<D> delegate,
            Mapper<E,D>              mapper )
    {
        super( delegate, mapper );
    }

    /**
     * Helper to centralize casting.
     */
    private HistoryCursorIterator<D> getDelegate()
    {
        return (HistoryCursorIterator<D>) theDelegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToJustBeforeTime(
            long time )
    {
        int ret = getDelegate().moveToJustBeforeTime( time );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToJustAfterTime(
            long time )
    {
        int ret = getDelegate().moveToJustAfterTime( time );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MappingHistoryCursorIterator<E,D> createCopy()
    {
        return new MappingHistoryCursorIterator<>( getDelegate().createCopy(), theMapper );
    }
}
