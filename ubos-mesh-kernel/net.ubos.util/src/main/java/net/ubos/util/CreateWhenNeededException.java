//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

/**
 * Wrapper Exception thrown by CreateWhenNeeded.
 */
public class CreateWhenNeededException
        extends
            RuntimeException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param source the CreateWhenNeeded that threw this CreateWhenNeededException
     * @param cause the cause for this CreateWhenNeededException
     */
    public CreateWhenNeededException(
            CreateWhenNeeded<?> source,
            Throwable           cause )
    {
        super( cause );

        theSource = source;
    }

    /**
     * Obtain the CreateWhenNeeded that threw this CreateWhenNeededException.
     * 
     * @return the CreateWhenNeeded
     */
    public CreateWhenNeeded<?> getSource()
    {
        return theSource;
    }

    /**
     * The CreateWhenNeeded object that threw this CreateWhenNeededException.
     */
    protected CreateWhenNeeded<?> theSource;
}
