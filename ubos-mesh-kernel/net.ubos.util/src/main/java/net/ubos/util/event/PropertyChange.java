//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.event;

import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Extends {@link Event} for the specific purpose of communicating
 * the change in a property.
 *
 * @param <S> the type of the event source
 * @param <P> the type of the property
 * @param <V> the type of the value
 */
public abstract class PropertyChange<S,P,V>
        extends
            AbstractEvent<S,V>
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param source the object that is the source of the event
     * @param property an object representing the property of the event
     * @param oldValue the old value of the property, prior to the event
     * @param deltaValue the value that changed
     * @param newValue the new value of the property, after the event
     */
    protected PropertyChange(
            S source,
            P property,
            V oldValue,
            V deltaValue,
            V newValue )
    {
        super( source, deltaValue );

        theProperty = property;
        theOldValue = oldValue;
        theNewValue = newValue;
    }

    /**
     * Obtain the property of the event.
     *
     * @return the property of the event
     */
    public P getProperty()
    {
        return theProperty;
    }

    /**
     * Obtain the old value of the property prior to the event.
     *
     * @return the old value of the property
     */
    public V getOldValue()
    {
        return theOldValue;
    }

    /**
     * Obtain the new value of the property after the event.
     *
     * @return the new value of the property
     */
    public V getNewValue()
    {
        return theNewValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theSource",
                    "theProperty",
                    "theOldValueIdentifier",
                    "theNewValueIdentifier"
                },
                new Object[] {
                    theSource,
                    theProperty,
                    theOldValue,
                    theNewValue
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( other == null ) {
            return false;
        }
        if( !getClass().equals( other.getClass() )) {
            // try to make it work with subclasses
            return false;
        }
        PropertyChange<?,?,?> realOther = (PropertyChange) other;

        if( !ArrayHelper.equals( getSource(), realOther.getSource() )) {
            return false;
        }
        if( !ArrayHelper.equals( getOldValue(), realOther.getOldValue() )) {
            return false;
        }
        if( !ArrayHelper.equals( getNewValue(), realOther.getNewValue() )) {
            return false;
        }
        if( !ArrayHelper.equals( getDeltaValue(), realOther.getDeltaValue() )) {
            return false;
        }
        if( !ArrayHelper.equals( getProperty(), realOther.getProperty() )) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int ret = theSource.hashCode();
        if( theDeltaValue != null ) {
            ret ^= theDeltaValue.hashCode();
        }
        ret ^= theProperty.hashCode();
        return ret;
    }

    /**
     * The old value of the data item whose change triggered the event.
     */
    protected final V theOldValue;

    /**
     * The new value of the data item whose change triggered the event.
     */
    protected final V theNewValue;

    /**
     * The property of the event.
     */
    protected final P theProperty;
}
