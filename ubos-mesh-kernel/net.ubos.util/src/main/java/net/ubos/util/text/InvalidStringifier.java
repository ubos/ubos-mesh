//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;

/**
 * A degenerate Stringifier that throws Exceptions only indicating that the use of this Stringifier
 * is invalid in a particular context.
 *
 * @param <T> can by used for any type
 */
public class InvalidStringifier<T>
        extends
            AbstractStringifier<T>
{
    /**
     * Factory method.
     *
     * @return the created InvalidStringifier
     * @param <T> can by used for any type
     */
    public static <T> InvalidStringifier<T> create()
    {
        return new InvalidStringifier<>();
    }

    /**
     * No-op constructor. Use factory method.
     */
    protected InvalidStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            T                     arg,
            StringifierParameters pars,
            String                soFar )
    {
        throw new UnsupportedOperationException( "Cannot use: InvalidStringifier!" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        throw new UnsupportedOperationException( "Cannot use: InvalidStringifier!" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException( "Cannot use: InvalidStringifier!" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        throw new UnsupportedOperationException( "Cannot use: InvalidStringifier!" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        throw new UnsupportedOperationException( "Cannot use: InvalidStringifier!" );
    }
}
