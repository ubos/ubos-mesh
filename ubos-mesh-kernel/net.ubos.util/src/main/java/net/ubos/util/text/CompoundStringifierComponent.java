//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.ArrayFacade;

/**
 * A component in the CompoundStringifier. This is not quite symmetric: the <code>format</code> argument
 * takes all arguments of the CompoundStringifier (and picks out the ones that apply), while the
 * Iterator only returns this CompoundStringifierComponent's contribution.
 */
public interface CompoundStringifierComponent
{
    /**
     * Format zero or one Objects in the ArrayFacade.
     *
     * @param arg the Object to format
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @param soFar the String so far, if any
     * @return the formatted String
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     */
    public String format(
            ArrayFacade<Object>   arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException;

    /**
     * Obtain an iterator that goes with this CompoundStringifierComponent.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param max the maximum number of choices to be returned by the Iterator.
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public abstract Iterator<? extends StringifierParsingChoice<?>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory );

    /**
     * Obtain an iterator that goes with this CompoundStringifierComponent.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param max the maximum number of choices to be returned by the Iterator.
     * @param factory the factory needed to create the parsed values, if any
     * @return the Iterator
     */
    public abstract Iterator<? extends StringifierParsingChoice<?>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory );
}
