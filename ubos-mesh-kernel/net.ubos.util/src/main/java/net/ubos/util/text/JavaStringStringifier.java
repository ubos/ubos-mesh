//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.StringHelper;

/**
 * A Stringifier to stringify Strings into Java syntax. The reverse is currently NOT supported.
 */
public class JavaStringStringifier
        extends
            StringStringifier
{
    /**
     * Factory method.
     *
     * @return the created JavaStringStringifier
     */
    public static JavaStringStringifier create()
    {
        return new JavaStringStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected JavaStringStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escape(
            String s )
    {
        String ret = StringHelper.stringToJavaString( s );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String unescape(
            String s )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<String>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<String>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        throw new UnsupportedOperationException();
    }
}