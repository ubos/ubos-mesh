//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

/**
 * This interface is supported by those <code>java.lang.Iterable</code>s that return a
 * {@link CursorIterator CursorIterator} instead of just a plain <code>Iterator</code>.
 *
 * @param <T> the type of element to iterate over
 */
public interface CursorIterable<T>
        extends
            Iterable<T>
{
    /**
     * Obtain a @{link CursorIterable} instead of a <code>java.util.Iterator</code>.
     *
     * @return the <code>CursorIterable</code>
     */
    @Override
    public CursorIterator<T> iterator();

    /**
     * Obtain a @{link CursorIterable}. This performs the exact same operation as
     * {@link #iterator iterator}, but is friendlier towards JSPs and other software
     * that likes to use JavaBeans conventions.
     *
     * @return the <code>CursorIterable</code>
     */
    public default CursorIterator<T> getIterator()
    {
        return iterator();
    }
}
