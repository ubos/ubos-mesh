//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Map;

/**
 * Collects parameters that may influence the formatting of a String using Stringifiers.
 */
public interface StringifierParameters
{
    /**
     * Obtain the number of parameters.
     *
     * @return the number of parameters
     */
    public int size();

    /**
     * Returns <code>true</code> if this is empty.
     *
     * @return <code>true</code> if this is empty.
     */
    boolean isEmpty();

    /**
     * Obtain a named value, or null.
     *
     * @param key the name of the value
     * @return the value, if any
     */
    public Object get(
            String key );

    /**
     * Obtain a named value, or the provided default.
     *
     * @param key the name of the value
     * @param defaultValue the default value
     * @return the value
     */
    public Object get(
            String key,
            Object defaultValue );

    /**
     * Create a copy of this instance, but with an additional the named value.
     *
     * @param key the name of the value
     * @param value the value
     * @return copy, with the named value
     */
    public StringifierParameters with(
            String key,
            Object value );

    /**
     * Create a copy of this instance, but with the named values.
     *
     * @param map the named values
     * @return copy, with the named values
     */
    public StringifierParameters with(
            Map<String,?> map );

    /**
     * Create a copy of this instance, but without the named value.
     *
     * @param key the name of the value
     * @return copy, without the named value
     */
    public StringifierParameters without(
            String key );

    /**
     * Create a copy of this instance, but without the named values.
     *
     * @param keys the names of the values
     * @return copy, without the named values
     */
    public StringifierParameters without(
            String [] keys );

//    /**
//     * The key into this object that identifies a directly provided format String, instead of reading
//     * it from a Resource file.
//     */
//    public final String FORMAT_STRING = "formatString";

    /**
     * The key into this object that identifies a MeshObjectsToView.
     */
    public final String MESHOBJECTS_TO_VIEW_KEY = "meshObjectsToView";

    /**
     * The key into this object that identifies the desired maximum length of the produced String.
     */
    public final String MAX_LENGTH_KEY = "maxLength";

    /**
     * The key into this object that identifies whether or not colloquial output is desired.
     */
    public final String COLLOQUIAL_KEY = "colloquial";

//    /**
//     * The key into this object that identifies any additional text to emit.
//     */
//    public final String ADD_TEXT = "addText";
//
//    /**
//     * The key into this object that identifies the variable to which an edited value is
//     * assigned. The meaning of "variable" depends on the user interface technology. For
//     * example, in a web application, it may refer to an HTTP POST parameter. This is only
//     * relevant for StringRepresentations that are intended to be edited.
//     */
//    public final String EDIT_VARIABLE = "variable";
//
//    /**
//     * The key into this object that identifies the index to a property for the same
//     * MeshObject.
//     */
//    public final String EDIT_INDEX = "index";
//
//    /**
//     * The key into this object that identifies the null value to display, if the object to
//     * be rendered is null.
//     */
//    public final String NULL_STRING = "nullString";
//
//    /**
//     * The key into this object that represents a web application's context path relative to /.
//     */
//    public static final String WEB_RELATIVE_CONTEXT_KEY = "web-relative-context-path";
//
//    /**
//     * The key into this object that represents a web application's full context path including http...
//     */
//    public static final String WEB_ABSOLUTE_CONTEXT_KEY = "web-absolute-context-path";

    /**
     * The key into this object that represents a desired target for a link, like in HTML href tags.
     */
    public static final String LINK_TARGET_KEY = "link-target";

    /**
     * The key into this object that represents a title for a link, like in HTML href tags.
     */
    public static final String LINK_TITLE_KEY = "link-title";

    /**
     * The key into this object that represents additional arguments to be appended to an HTML URL link.
     */
    public static final String HTML_URL_ADDITIONAL_ARGUMENTS_KEY = "html-url-additional-arguments";

    /**
     * The key into this object that represents additional CSS classes to be added to the
     * outermost HTML element.
     */
    public static final String HTML_ADDITIONAL_CSS_CLASS_KEY = "html-element-additional-css-class";

    /**
     * Key into this object that represents the name of a flavor to be used.
     */
    public static final String FLAVOR_KEY = "flavor";

    /**
     * Name of the default flavor, if not set.
     */
    public static final String DEFAULT_FLAVOR = "default";

    /**
     * Name of the default flavor for null values, if not set.
     */
    public static final String DEFAULT_NULL_FLAVOR = "defaultNull";

    /**
     * The key into this object that indicates whether an upload button should be shown.
     */
    public static final String UPLOAD_KEY = "upload";

    /**
     * Name of the default value for upload.
     */
    public static final String DEFAULT_UPLOAD_VALUE = "true";

    /**
     * An empty instance of this interface.
     */
    public static final StringifierParameters EMPTY = new StringifierParameters() {

        /**
         * {@inheritDoc}
         */
        @Override
        public int size()
        {
            return 0;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isEmpty()
        {
            return true;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object get(
                String key )
        {
            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object get(
                String key,
                Object defaultValue )
        {
            return defaultValue;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public StringifierParameters with(
                String key,
                Object value )
        {
            SimpleStringifierParameters ret = SimpleStringifierParameters.create();
            ret.put( key, value );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public StringifierParameters with(
                Map<String, ?> map )
        {
            SimpleStringifierParameters ret = SimpleStringifierParameters.create( map );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public StringifierParameters without(
                String key )
        {
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public StringifierParameters without(
                String[] keys )
        {
            return this;
        }
    };
}
