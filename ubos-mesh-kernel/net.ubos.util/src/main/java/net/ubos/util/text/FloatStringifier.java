//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies a single Float.
 */
public class FloatStringifier
        extends
            AbstractStringifier<Float>
{
    /**
     * Factory method.
     *
     * @return the created DoubleStringifier
     */
    public static FloatStringifier create()
    {
        return new FloatStringifier();
    }

    /**
     * Constructor. Use factory method.
     */
    protected FloatStringifier()
    {
        // noop
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Float                 arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = String.valueOf( arg );
        ret = potentiallyShorten( ret, pars );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg instanceof Float ) {
            return format( ((Float)arg), pars, soFar );
        } else {
            return format( ((Number) arg).floatValue(), pars, soFar );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Float unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            Float ret = Float.parseFloat( rawString );

            return ret;

        } catch( NumberFormatException ex ) {
            throw new StringifierParseException( this, rawString, ex );
        }
    }
}