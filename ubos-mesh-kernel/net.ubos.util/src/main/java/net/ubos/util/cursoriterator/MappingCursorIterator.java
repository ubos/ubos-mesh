//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A CursorIterator that delegates to another CursorIterator, but maps the values back and forth.
 * In other words, there is a one-to-one Map between each elements this CursorIterator iterates
 * over, and the elements the delegate CursorIterator iterates over.
 *
 * @param <E> the type of element to iterate over
 * @param <D> the type of element the delegate CursorIterator iterates over
 */
public class MappingCursorIterator<E,D>
    implements
        CursorIterator<E>
{
    /**
     * Constructor.
     *
     * @param delegate the underling Iterator
     * @param mapper defines how to map
     */
    public MappingCursorIterator(
            CursorIterator<D>  delegate,
            Mapper<E,D>        mapper )
    {
        theDelegate = delegate;
        theMapper   = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final E nextElement()
    {
        return next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekNext()
    {
        D value = theDelegate.peekNext();
        E ret   = theMapper.mapDelegateValueToHere( value );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekPrevious()
    {
        D value = theDelegate.peekPrevious();
        E ret   = theMapper.mapDelegateValueToHere( value );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> peekNext(
            int n )
    {
        List<D> values = theDelegate.peekNext( n );
        List<E> ret    = new ArrayList<>( values.size() );

        for( D current : values ) {
            ret.add( theMapper.mapDelegateValueToHere( current ));
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> peekPrevious(
            int n )
    {
        List<D> values = theDelegate.peekPrevious( n );
        List<E> ret    = new ArrayList<>( values.size() );

        for( D current : values ) {
            ret.add( theMapper.mapDelegateValueToHere( current ));
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return theDelegate.hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        return theDelegate.hasPrevious();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        return theDelegate.hasNext( n );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        return theDelegate.hasPrevious( n );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
    {
        D value = theDelegate.next();
        E ret   = theMapper.mapDelegateValueToHere( value );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> next(
            int n )
    {
        List<D> values  = theDelegate.next( n );
        List<E> ret     = new ArrayList<>( values.size() );

        for( D current : values ) {
            ret.add( theMapper.mapDelegateValueToHere( current ));
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
    {
        D value = theDelegate.previous();
        E ret   = theMapper.mapDelegateValueToHere( value );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> previous(
            int  n )
    {
        List<D> values  = theDelegate.previous( n );
        List<E> ret     = new ArrayList<>( values.size() );

        for( D current : values ) {
            ret.add( theMapper.mapDelegateValueToHere( current ));
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        theDelegate.moveBy( n );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            E pos )
        throws
            NoSuchElementException
    {
        D delegateKey = theMapper.mapHereToDelegateValue( pos );

        return theDelegate.moveToBefore( delegateKey );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            E pos )
        throws
            NoSuchElementException
    {
        D delegateKey = theMapper.mapHereToDelegateValue( pos );

        return theDelegate.moveToAfter( delegateKey );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
    {
        theDelegate.remove();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret = theDelegate.moveToBeforeFirst();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int ret = theDelegate.moveToAfterLast();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException
    {
        E here  = position.peekNext();
        D there = theMapper.mapHereToDelegateValue( here );

        theDelegate.moveToBefore( there );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MappingCursorIterator<E,D> createCopy()
    {
        return new MappingCursorIterator<>( theDelegate.createCopy(), theMapper );
    }

    /**
     * The delegate CursorIterator.
     */
    protected final CursorIterator<D> theDelegate;

    /**
     * The Mapper to use.
     */
    protected final Mapper<E,D> theMapper;

    /**
     * The Mapper defines how to map.
     *
     * @param <E> the type of element to iterate over
     * @param <D> the type of element the delegate CursorIterator iterates over
     */
    public static interface Mapper<E,D>
    {
        /**
         * Map from the delegate to this.
         *
         * @param delegateValue the value in the delegate
         * @return the value here
         */
        public abstract E mapDelegateValueToHere(
                D delegateValue );

        /**
         * Map from this to the delegate.
         *
         * @param value the value here
         * @return the value in the delegate
         */
        public abstract D mapHereToDelegateValue(
                E value );
    }
}
