//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.List;

/**
 * A Stringifier that stringifies lists.
 *
 * It delegates to another Stringifier for the members of
 * the list, and can prepend, append and insert between elements constant strings. If the
 * list is empty, a different string is emitted.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class ListStringifier<T>
        extends
             AbstractStringifier<List<T>>
{
    /**
     * Factory method. This creates an ListStringifier that merely appends the
     * individual components after each other.
     *
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @return the created ListStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ListStringifier<T> create(
            Stringifier<T> delegate )
    {
        return new ListStringifier<>( null, null, null, null, delegate );
    }

    /**
     * Factory method. This creates an ListStringifier that joins the
     * individual components after each other with a string in the middle.
     * This is similar to Perl's join.
     *
     * @param middle the string to insert in the middle
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @return the created ListStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ListStringifier<T> create(
            String         middle,
            Stringifier<T> delegate )
    {
        return new ListStringifier<>( null, middle, null, null, delegate );
    }

    /**
     * Factory method. This creates an ListStringifier that joins the
     * individual components after each other with a string in the middle,
     * prepends a start and appends an end.
     *
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @return the created ListStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ListStringifier<T> create(
            String         start,
            String         middle,
            String         end,
            Stringifier<T> delegate )
    {
        return new ListStringifier<>( start, middle, end, null, delegate );
    }

    /**
     * Factory method. This creates an ListStringifier that joins the
     * individual components after each other with a string in the middle,
     * prepends a start and appends an end, or uses a special empty String if
     * the array is empty.
     *
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     * @param empty what to emit instead if the array is empty
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @return the created ListStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ListStringifier<T> create(
            String         start,
            String         middle,
            String         end,
            String         empty,
            Stringifier<T> delegate )
    {
        return new ListStringifier<>( start, middle, end, empty, delegate );
    }

    /**
     * Constructor.
     *
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @param empty what to emit instead if the array is empty
     */
    protected ListStringifier(
            String         start,
            String         middle,
            String         end,
            String         empty,
            Stringifier<T> delegate )
    {
        theStart       = start;
        theMiddle      = middle;
        theEnd         = end;
        theEmptyString = empty;
        theDelegate    = delegate;
    }

    /**
     * Obtain the start String, if any.
     *
     * @return the start String, if any
     */
    public String getStart()
    {
        return theStart;
    }

    /**
     * Obtain the middle String, if any.
     *
     * @return the middle String, if any
     */
    public String getMiddle()
    {
        return theMiddle;
    }

    /**
     * Obtain the end String, if any.
     *
     * @return the end String, if any
     */
    public String getEnd()
    {
        return theEnd;
    }

    /**
     * Obtain the delegate Stringifier.
     *
     * @return the delegate
     */
    public Stringifier<T> getDelegate()
    {
        return theDelegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            List<T>               arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        if( arg == null || arg.isEmpty() ) {
            if( theEmptyString != null ) {
                return theEmptyString;
            } else if( theStart != null ) {
                if( theEnd != null ) {
                    return theStart+theEnd;
                } else {
                    return theStart;
                }
            } else if( theEnd != null ) {
                return theEnd;
            } else {
                return "";
            }
        }

        StringBuilder ret  = new StringBuilder();
        String        sep  = theStart;

        for( Object current : arg ) {
            if( sep != null ) {
                ret.append( sep );
            }
            String childInput = theDelegate.attemptFormat( current, pars, soFar );
            if( childInput != null ) {
                ret.append( childInput );
            }
            sep = theMiddle;
        }
        if( theEnd != null ) {
            ret.append( theEnd );
        }

        return potentiallyShorten( ret.toString(), pars );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException
    {
        return format( (List<T>) arg, pars, soFar );
    }

    /**
     * The String to insert at the beginning. May be null.
     */
    protected final String theStart;

    /**
     * The String to insert when joining two elements. May be null.
     */
    protected final String theMiddle;

    /**
     * The String to append at the end. May be null.
     */
    protected final String theEnd;

    /**
     * The String to emit if the array if empty. May be null, in which case it is assumed to theStart+theEnd.
     */
    protected final String theEmptyString;

    /**
     * The underlying Stringifier that knows how to deal with the real type.
     */
    protected final Stringifier<T> theDelegate;
}
