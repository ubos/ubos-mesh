//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.token;

/**
 * Knows how to create unique tokens.
 *
 * @param <T> the type of token to create
 */
public interface UniqueTokenGenerator<T>
{
    /**
     * Create a unique token.
     *
     * @return the unique token
     */
    public T createUniqueToken();
}
