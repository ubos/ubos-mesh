//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap;

import java.util.HashMap;
import java.util.Map;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * A SmartMap that sits in front of another, and delays updates to it until flushed.
 *
 * @param <K> the key
 * @param <V> the value
 */
public class WriteCachingSmartMap<K,V>
    extends
        AbstractSmartMap<K,V>
{
    /**
     * Factory method.
     *
     * @param delegate the real map underneath
     * @return the created WriteCachingSmartMap
     * @param <K> the key
     * @param <V> the value
     */
    public static <K,V> WriteCachingSmartMap<K,V> create(
            SmartMap<K,V> delegate )
    {
        return new WriteCachingSmartMap<>( delegate );
    }

    /**
     * Constructor.
     *
     * @param delegate the real map underneath
     */
    protected WriteCachingSmartMap(
            SmartMap<K,V> delegate )
    {
        theDelegate = delegate;
    }

    /**
     * Obtain the delegate.
     *
     * @return the delegate
     */
    public SmartMap<K,V> getDelegate()
    {
        return theDelegate;
    }

    /**
     * Flush the cache to the delegate.
     */
    public void sync()
    {
        for( Map.Entry<K,V> current : theDirtyRecords.entrySet() ) {
            K key   = current.getKey();
            V value = current.getValue();

            if( value == null ) {
                if( theDelegate.containsKey( key )) {
                    // may not be there, if we added and removed before we did a sync
                    theDelegate.removeIgnorePrevious( key );
                }
            } else {
                theDelegate.putIgnorePrevious( key, value );
            }
        }
        theDirtyRecords.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        return Math.max( theDirtyRecords.size(), theDelegate.size()); // approximate
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            K key )
    {
        if( theDirtyRecords.containsKey( key )) {
            V found = theDirtyRecords.get( key );
            return found != null;

        } else {
            return theDelegate.containsKey( key );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V get(
            K key )
    {
        if( theDirtyRecords.containsKey( key )) {
            V found = theDirtyRecords.get( key );
            return found;

        } else {
            V found = theDelegate.get( key );
            return found;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            K key,
            V value )
    {
        theDirtyRecords.put( key, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            K key )
    {
        theDirtyRecords.put( key, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        sync(); // the underlying Store may keep history, so it wants to know.

        theDirtyRecords.clear();
        theDelegate.clear();
    }


    /**
     * Determine the number of cached and not yet written values.
     *
     * @return the number
     */
    public int sizeNotWrittenYet()
    {
        return theDelegate.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return false;
    }

    /**
     * Clear the local cache.
     */
    public void clearLocalCache()
    {
        sync(); // the underlying Store may keep history, so it wants to know.
        theDirtyRecords.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        sync();
        return theDelegate.keyIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<V> valueIterator()
    {
        sync();
        return theDelegate.valueIterator();
    }

    /**
     * The real map underneath.
     */
    protected final SmartMap<K,V> theDelegate;

    /**
     * Keeps the dirty records: updates not yet written.
     */
    protected final Map<K,V> theDirtyRecords = new HashMap<>();
}
