//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

import net.ubos.util.ResourceHelper;

/**
 * Superclass for RuntimeExceptions that knows how to internationalize themselves.
 * Given that Exceptions carry all their data, it is a lot easier to to
 * ask the Exception how to internationalize itself, then to write outside
 * code to do so.
 */
public abstract class AbstractLocalizedRuntimeException
    extends
        RuntimeException
    implements
        LocalizedException
{
    /**
     * Constructor with no message.
     */
    public AbstractLocalizedRuntimeException()
    {
    }

    /**
     * Constructor with a message.
     *
     * @param msg the message
     */
    public AbstractLocalizedRuntimeException(
            String msg )
    {
        super( msg );
    }

    /**
     * Constructor with no message but a cause.
     *
     * @param cause the Throwable that caused this Exception
     */
    public AbstractLocalizedRuntimeException(
            Throwable cause )
    {
        super( cause );
    }

    /**
     * Constructor one with a message and a cause.
     *
     * @param msg the message
     * @param cause the Exception that caused this Exception
     */
    public AbstractLocalizedRuntimeException(
            String    msg,
            Throwable cause )
    {
        super( msg, cause );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        return AbstractLocalizedException.assembleDefaultLocalizedMessage( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract Object [] getLocalizationParameters();

    /**
     * Allow subclasses to override which ResourceHelper to use.
     *
     * @return the ResourceHelper to use
     */
    protected ResourceHelper findResourceHelperForLocalizedMessageViaEnclosingClass()
    {
        String className = getClass().getName();
        int    dollar = className.indexOf( '$' );
        if( dollar >= 0 ) {
            className = className.substring( 0, dollar );
        }
        return ResourceHelper.getInstance( className, getClass().getClassLoader() );
    }
}
