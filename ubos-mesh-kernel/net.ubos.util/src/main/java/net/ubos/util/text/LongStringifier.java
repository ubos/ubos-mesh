//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies a single Long.
 */
public class LongStringifier
        extends
            NumberStringifier<Long>
{
    /**
     * Factory method for a decimal LongStringifier.
     *
     * @return the created LongStringifier
     */
    public static LongStringifier createDecimal()
    {
        return new LongStringifier( -1, -1, 10 );
    }

    /**
     * Factory method for a decimal, fixed-length LongStringifier with a fixed number
     * of digits, using leading zeros if needed.
     *
     * @param digits the number of digits
     * @return the created LongStringifier
     */
    public static LongStringifier createDecimal(
            int digits )
    {
        return new LongStringifier( digits, digits, 10 );
    }

    /**
     * Factory method for a decimal, bounded-length LongStringifier with between two
     * numbers of digits, using leading zeros if needed.
     *
     * @param minDigits the minimum number of digits
     * @param maxDigits the maximum number of digits
     * @return the created LongStringifier
     */
    public static LongStringifier createDecimal(
            int minDigits,
            int maxDigits )
    {
        return new LongStringifier( minDigits, maxDigits, 10 );
    }

    /**
     * Factory method for an LongStringifier with arbitrary radix.
     *
     * @param radix the radix to use, e.g. 16 for hexadecimal
     * @return the created IntegerStringifier
     */
    public static LongStringifier createRadix(
            int radix )
    {
        return new LongStringifier( -1, -1, radix );
    }

    /**
     * Factory method for a fixed-length LongStringifier with arbitrary radix, with a fixed number
     * of digits, using leading zeros if needed.
     *
     * @param digits the number of digits
     * @param radix the radix to use, e.g. 16 for hexadecimal
     * @return the created LongStringifier
     */
    public static LongStringifier createRadix(
            int digits,
            int radix )
    {
        return new LongStringifier( digits, digits, radix );
    }

    /**
     * Factory method for a bounded-length LongStringifier with arbitrary radix, with between two
     * numbers of digits, using leading zeros if needed.
     *
     * @param minDigits the minimum number of digits
     * @param maxDigits the maximum number of digits
     * @param radix the radix to use, e.g. 16 for hexadecimal
     * @return the created LongStringifier
     */
    public static LongStringifier createRadix(
            int minDigits,
            int maxDigits,
            int radix )
    {
        return new LongStringifier( minDigits, maxDigits, radix );
    }

    /**
     * Constructor. Use factory method.
     *
     * @param minDigits the minimum number of digits, or -1 if none
     * @param maxDigits the maximum number of digits, or -1 if none
     * @param radix the radix to use, e.g. 16 for hexadecimal
     */
    protected LongStringifier(
            int minDigits,
            int maxDigits,
            int radix )
    {
        super( minDigits, maxDigits, radix );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Long                  arg,
            StringifierParameters pars,
            String                soFar )
    {
        return super.format( arg.longValue(), pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        int len = rawString.length();

        if( theMinDigits != -1 && len < theMinDigits ) {
             throw new StringifierParseException( this, rawString );
        }
        if( theMaxDigits != -1 && len > theMaxDigits ) {
             throw new StringifierParseException( this, rawString );
        }

        try {
            Long ret = Long.parseLong( rawString, theRadix );

            return ret;

        } catch( NumberFormatException ex ) {
            throw new StringifierParseException( this, rawString, ex );
        }
    }
}
