//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.exception.LocalizedParseException;

/**
 * Thrown if a String could not be parsed by a StringRepresentation.
 */
public class StringRepresentationParseException
    extends
        LocalizedParseException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param string the text that could not be parsed
     * @param formatString the format String that defines the syntax of the String to be parsed
     * @param cause the cause of this Exception
     */
    public StringRepresentationParseException(
            String    string,
            String    formatString,
            Throwable cause )
    {
        super( string, null, 0, cause );

        theFormatString = formatString;
    }

    /**
     * Constructor for the case that no format String could be found.
     *
     * @param string the text that could not be parsed
     */
    public StringRepresentationParseException(
            String string )
    {
        super( string, null, 0, null );

        theFormatString = null;
    }

    /**
     * Obtain the format String.
     *
     * @return the format String
     */
    public String getFormatString()
    {
        return theFormatString;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theString, theFormatString };
    }

    /**
     * The format String for the String.
     */
    protected String theFormatString;
}
