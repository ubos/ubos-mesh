//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies a single Integer.
 */
public class IntegerStringifier
        extends
            NumberStringifier<Integer>
{
    /**
     * Factory method for a decimal IntegerStringifier.
     *
     * @return the created IntegerStringifier
     */
    public static IntegerStringifier createDecimal()
    {
        return new IntegerStringifier( -1, -1, 10 );
    }

    /**
     * Factory method for a decimal, fixed-length IntegerStringifier with a fixed number
     * of digits, using leading zeros if needed.
     *
     * @param digits the number of digits
     * @return the created IntegerStringifier
     */
    public static IntegerStringifier createDecimal(
            int digits )
    {
        return new IntegerStringifier( digits, digits, 10 );
    }

    /**
     * Factory method for a decimal, bounded-length IntegerStringifier with between two
     * numbers of digits, using leading zeros if needed.
     *
     * @param minDigits the minimum number of digits
     * @param maxDigits the maximum number of digits
     * @return the created IntegerStringifier
     */
    public static IntegerStringifier createDecimal(
            int minDigits,
            int maxDigits )
    {
        return new IntegerStringifier( minDigits, maxDigits, 10 );
    }

    /**
     * Factory method for an IntegerStringifier with arbitrary radix.
     *
     * @param radix the radix to use, e.g. 16 for hexadecimal
     * @return the created IntegerStringifier
     */
    public static IntegerStringifier createRadix(
            int radix )
    {
        return new IntegerStringifier( -1, -1, radix );
    }

    /**
     * Factory method for a fixed-length IntegerStringifier with arbitrary radix, with a fixed number
     * of digits, using leading zeros if needed.
     *
     * @param digits the number of digits
     * @param radix the radix to use, e.g. 16 for hexadecimal
     * @return the created IntegerStringifier
     */
    public static IntegerStringifier createRadix(
            int digits,
            int radix )
    {
        return new IntegerStringifier( digits, digits, radix );
    }

    /**
     * Factory method for a bounded-length IntegerStringifier with arbitrary radix, with between two
     * numbers of digits, using leading zeros if needed.
     *
     * @param minDigits the minimum number of digits
     * @param maxDigits the maximum number of digits
     * @param radix the radix to use, e.g. 16 for hexadecimal
     * @return the created IntegerStringifier
     */
    public static IntegerStringifier createRadix(
            int minDigits,
            int maxDigits,
            int radix )
    {
        return new IntegerStringifier( minDigits, maxDigits, radix );
    }

    /**
     * Constructor. Use factory method.
     *
     * @param minDigits the minimum number of digits, or -1 if none
     * @param maxDigits the maximum number of digits, or -1 if none
     * @param radix the radix to use, e.g. 16 for hexadecimal
     */
    protected IntegerStringifier(
            int minDigits,
            int maxDigits,
            int radix )
    {
        super( minDigits, maxDigits, radix );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Integer               arg,
            StringifierParameters pars,
            String                soFar )
    {
        return super.format( arg.longValue(), pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        int len = rawString.length();

        if( theMinDigits != -1 && len < theMinDigits ) {
             throw new StringifierParseException( this, rawString );
        }
        if( theMaxDigits != -1 && len > theMaxDigits ) {
             throw new StringifierParseException( this, rawString );
        }

        try {
            Integer ret = Integer.parseInt( rawString, theRadix );

            return ret;

        } catch( NumberFormatException ex ) {
            throw new StringifierParseException( this, rawString, ex );
        }
    }
}
