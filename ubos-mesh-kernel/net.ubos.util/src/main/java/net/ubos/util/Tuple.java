//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

/**
 * A Tuple.
 */
public class Tuple
{
    /**
     * Create a Tuple of two.
     * 
     * @param a element
     * @param b element
     * @return the Tuple
     */
    public static Tuple create(
            Object a,
            Object b )
    {
        Tuple ret = new Tuple( 2 );
        ret.theData[0] = a;
        ret.theData[1] = b;
        return ret;
    }

    /**
     * Create a Tuple of three.
     * 
     * @param a element
     * @param b element
     * @param c element
     * @return the Tuple
     */
    public static Tuple create(
            Object a,
            Object b,
            Object c )
    {
        Tuple ret = new Tuple( 3 );
        ret.theData[0] = a;
        ret.theData[1] = b;
        ret.theData[2] = c;
        return ret;
    }

    /**
     * Create a Tuple of four.
     * 
     * @param a element
     * @param b element
     * @param c element
     * @param d element
     * @return the Tuple
     */
    public static Tuple create(
            Object a,
            Object b,
            Object c,
            Object d )
    {
        Tuple ret = new Tuple( 4 );
        ret.theData[0] = a;
        ret.theData[1] = b;
        ret.theData[2] = c;
        ret.theData[3] = d;
        return ret;
    }

    /**
     * Create a Tuple of five.
     * 
     * @param a element
     * @param b element
     * @param c element
     * @param d element
     * @param e element
     * @return the Tuple
     */
    public static Tuple create(
            Object a,
            Object b,
            Object c,
            Object d,
            Object e )
    {
        Tuple ret = new Tuple( 5 );
        ret.theData[0] = a;
        ret.theData[1] = b;
        ret.theData[2] = c;
        ret.theData[3] = d;
        ret.theData[4] = e;
        return ret;
    }

    /**
     * Private constructor, use factory method.
     *
     * @param n the length of the tuple
     */
    protected Tuple(
            int n )
    {
        theData = new Object[n];
    }

    /**
     * Obtain the nth element.
     * 
     * @param index the index of the element to obtain
     * @return the element
     * @throws ArrayIndexOutOfBoundsException thrown if a the tuple wasn't large enough
     */
    public Object get(
            int index )
        throws
            ArrayIndexOutOfBoundsException
    {
        return theData[index];
    }

    /**
     * Equality is determined by comparing the components.
     *
     * @param other the Object to compare with
     * @return true if the objects are equal
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof Tuple )) {
            return false;
        }
        Tuple realOther = (Tuple) other;

        if( theData.length != realOther.theData.length ) {
            return false;
        }
        for( int i=0 ; i<theData.length ; ++i ) {
            if( theData[i] == null ) {
                if( realOther.theData[i] != null ) {
                    return false;
                }
            } else if( !theData[i].equals( realOther.theData[i] )) {
                return false;
            }
        }
        return true;
    }

    /**
     * Hash is determined from the components.
     *
     * @return hash code
     */
    @Override
    public int hashCode()
    {
        int ret = 0;
        for( int i=0 ; i<theData.length ; ++i ) {
            if( theData[i] != null ) {
                ret ^= theData[i].hashCode();
            }
        }
        return ret;
    }

    /**
     * Holds the data.
     */
    protected final Object [] theData;
}
