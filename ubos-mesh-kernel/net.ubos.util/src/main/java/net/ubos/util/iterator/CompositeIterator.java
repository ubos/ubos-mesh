//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.iterator;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * <p>Iterator that returns the content returned by N delegate Iterators, in sequence.</p>
 * 
 * @param <E> the type of element to iterate over
 */
public class CompositeIterator<E>
        implements
            Iterator<E>,
            Enumeration<E>
{
    /**
     * Factory method.
     *
     * @param delegates the Iterators to delegate to, in sequence
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromIterators(
            List<Iterator<E>> delegates )
    {
        return new CompositeIterator<>( delegates );
    }

    /**
     * Factory method.
     *
     * @param delegates the Iterators to delegate to, in sequence
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromIterators(
            Iterator<E> [] delegates )
    {
        List<Iterator<E>> delegates2 = new ArrayList<>( delegates.length );
        for( int i=0 ; i<delegates.length ; ++i ) {
            delegates2.add( delegates[i] );
        }
        return new CompositeIterator<>( delegates2 );
    }

    /**
     * Factory method.
     *
     * @param delegate1 the first Iterator to delegate to
     * @param delegate2 the second Iterator to delegate to
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromIterators(
            Iterator<E> delegate1,
            Iterator<E> delegate2 )
    {
        List<Iterator<E>> delegates2 = new ArrayList<>( 2 );
        delegates2.add( delegate1 );
        delegates2.add( delegate2 );

        return new CompositeIterator<>( delegates2 );
    }

    /**
     * Factory method.
     *
     * @param delegate1 the first Iterator to delegate to
     * @param delegate2 the second Iterator to delegate to
     * @param delegate3 the third Iterator to delegate to
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromIterators(
            Iterator<E> delegate1,
            Iterator<E> delegate2,
            Iterator<E> delegate3 )
    {
        List<Iterator<E>> delegates2 = new ArrayList<>( 2 );
        delegates2.add( delegate1 );
        delegates2.add( delegate2 );
        delegates2.add( delegate3 );

        return new CompositeIterator<>( delegates2 );
    }

    /**
     * Factory method.
     *
     * @param delegates the Enumerations to delegate to, in sequence
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromEnumerations(
            List<Enumeration<E>> delegates )
    {
        return new CompositeIterator<>( delegates );
    }

    /**
     * Factory method.
     *
     * @param delegates the Enumeration to delegate to, in sequence
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromEnumerations(
            Enumeration<E> [] delegates )
    {
        List<Enumeration<E>> delegates2 = new ArrayList<>( delegates.length );
        for( int i=0 ; i<delegates.length ; ++i ) {
            delegates2.add( delegates[i] );
        }
        return new CompositeIterator<>( delegates2 );
    }

    /**
     * Factory method.
     *
     * @param delegate1 the first Enumeration to delegate to
     * @param delegate2 the second Enumeration to delegate to
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromEnumerations(
            Enumeration<E> delegate1,
            Enumeration<E> delegate2 )
    {
        List<Enumeration<E>> delegates2 = new ArrayList<>( 2 );
        delegates2.add( delegate1 );
        delegates2.add( delegate2 );

        return new CompositeIterator<>( delegates2 );
    }

    /**
     * Factory method.
     *
     * @param delegate1 the first Enumeration to delegate to
     * @param delegate2 the second Enumeration to delegate to
     * @param delegate3 the third Enumeration to delegate to
     * @return the created CompositeIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> CompositeIterator<E> createFromEnumerations(
            Enumeration<E> delegate1,
            Enumeration<E> delegate2,
            Enumeration<E> delegate3 )
    {
        List<Enumeration<E>> delegates2 = new ArrayList<>( 3 );
        delegates2.add( delegate1 );
        delegates2.add( delegate2 );
        delegates2.add( delegate3 );

        return new CompositeIterator<>( delegates2 );
    }

    /**
     * Constructor.
     *
     * @param delegates the Iterators to delegate to, in sequence
     */
    public CompositeIterator(
            List<?> delegates )
    {
        theDelegates = delegates;
        theIndex     = 0;        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        if( theIndex >= theDelegates.size() ) {
            return false;
        } else {
            return hasNext( theDelegates.get( theIndex ) );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public E next()
    {
        if( theIndex >= theDelegates.size() ) {
            throw new NoSuchElementException();
        }

        Object current = theDelegates.get( theIndex );
        E      ret;

        if( current instanceof Enumeration ) {
            ret = (E) ((Enumeration) current ).nextElement();
        } else {
            ret = ((Iterator<E>) current ).next();
        }
        
        goAdvance();
        
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
    {
        if( theIndex >= theDelegates.size() ) {
            throw new IllegalStateException();
        }

        Object current = theDelegates.get( theIndex );

        if( current instanceof Enumeration ) {
            throw new UnsupportedOperationException( "underlying java.util.Enumeration does not support this method" );
        } else {
            ((Iterator) current).remove();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final E nextElement()
    {
        return next();
    }
    
    /**
     * Advance to the next element.
     */
    protected void goAdvance()
    {
        while( true ) {
            if( theIndex >= theDelegates.size() ) {
                break; // we are done, no more
            }
            
            if( hasNext( theDelegates.get( theIndex ) ) ) {
                break; // found one
            }
            
            ++theIndex;
        }
    }

    /**
     * Determine whether this Iterator has a next element.
     *
     * @param candidate the Iterator or Enumeration to consider
     * @return true if it has a next element
     */
    protected boolean hasNext(
            Object candidate )
    {
        if( candidate instanceof Enumeration ) {
            return ((Enumeration) candidate).hasMoreElements();
        } else {
            return ((Iterator) candidate).hasNext();
        }
    }

    /**
     * The delegate Iterators from which the elements are obtained. This is stored as
     * <code>Object</code> so both Iterators and Enumerations are supported.
     */
    protected List<?> theDelegates;
    
    /**
     * Index into theDelegates array that identifies the currently active Iterator.
     */
    protected int theIndex;
}
