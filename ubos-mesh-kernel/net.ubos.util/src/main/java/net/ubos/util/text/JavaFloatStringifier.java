//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * A Stringifier to stringify floats into Java syntax. The reverse is currently NOT supported.
 */
public class JavaFloatStringifier
        extends
            AbstractStringifier<Number>
{
    /**
     * Factory method.
     *
     * @return the created JavaFloatStringifier
     */
    public static JavaFloatStringifier create()
    {
        return new JavaFloatStringifier();
    }

    /**
     * Private constructor for subclasses only, use factory method.
     */
    protected JavaFloatStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Number                arg,
            StringifierParameters pars,
            String                soFar )
    {
        // ignore maxLength, it makes no sense here
        return String.valueOf( arg.floatValue() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (Number) arg, pars, soFar );
    }
}
