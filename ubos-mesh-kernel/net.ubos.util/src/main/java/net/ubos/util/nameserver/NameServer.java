//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.nameserver;

import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Represents a NameServer, i.e. an object that can return objects based on a key.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 * @see net.ubos.util.nameserver.WritableNameServer
 */
public interface NameServer<K,V>
{
    /**
     * Obtain an already existing value for a provided key.
     *
     * @param key the key for which we want to obtain a value
     * @return the already existing value, or null
     */
    public V get(
            K key );

    /**
     * Obtain an CursorIterator over the known keys.
     *
     * @return the CursorIterator
     */
    public CursorIterator<K> keyIterator();

    /**
     * Obtain an CursorIterator over the known values.
     *
     * @return the CursorIterator
     */
    public CursorIterator<V> valueIterator();

    /**
     * Determine whether the number of key-value pairs in this SmartFactory is zero.
     *
     * @return true if there are no key-value pairs in this SmartFactory
     */
    public default boolean isEmpty()
    {
        return size() == 0;
    }

    /**
     * The number of known key-value pairs in this SmartFactory at this time.
     *
     * @return the number of known key-value pairs
     */
    public int size();
}
