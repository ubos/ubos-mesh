//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

import net.ubos.util.ResourceHelper;
import net.ubos.util.logging.Log;
import net.ubos.util.text.DefaultMessageFormatter;
import net.ubos.util.text.StringifierException;
import net.ubos.util.text.StringifierParameters;

/**
 * This is a supertype for Exceptions that knows how to internationalize themselves.
 * Given that Exceptions carry all their data, it is a lot easier to to
 * ask the Exception how to internationalize itself, than to write outside
 * code to do so.
 */
public abstract class AbstractLocalizedException
        extends
            Exception
        implements
            LocalizedException
{
    private static final Log log = Log.getLogInstance( AbstractLocalizedException.class ); // our own, private logger

    /**
     * Constructor with no message.
     */
    public AbstractLocalizedException()
    {
    }

    /**
     * Constructor with a message.
     *
     * @param msg the message
     */
    public AbstractLocalizedException(
            String msg )
    {
        super( msg );
    }

    /**
     * Constructor with no message but a cause.
     *
     * @param cause the Throwable that caused this Exception
     */
    public AbstractLocalizedException(
            Throwable cause )
    {
        super( cause );
    }

    /**
     * Constructor with a message and a cause.
     *
     * @param msg the message
     * @param cause the Exception that caused this Exception
     */
    public AbstractLocalizedException(
            String    msg,
            Throwable cause )
    {
        super( msg, cause );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        return assembleDefaultLocalizedMessage( this );
    }
    
    /**
     * Default implementation for creating a localized message.
     * This is factored-out as a static, so other LocalizedExceptions that
     * cannot inherit from this class can easily use it.
     * 
     * @param ex the LocalizedException
     * @return the localized message
     */
    public static String assembleDefaultLocalizedMessage(
            LocalizedException ex )
    {
        try {
            String resourceKey;
            String className = ex.getClass().getName();
            int    dollar    = className.lastIndexOf( '$' );
            if( dollar >= 0 ) {
                // special rule for inner classes
                resourceKey = "Plain" + className.substring( dollar+1 ) + "String";
            } else {
                resourceKey = "PlainString";
            }

            String ret = DefaultMessageFormatter.TEXT_PLAIN.format(
                    ex.findResourceHelperForLocalizedMessage().getResourceString( resourceKey ),
                    StringifierParameters.EMPTY,
                    ex.getLocalizationParameters() );

            return ret;
        } catch( StringifierException ex2 ) {
            log.error( ex2 );
            return ex.getClass().getName() + ": error converting to localized message";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract Object [] getLocalizationParameters();

    /**
     * Allow subclasses to override which ResourceHelper to use.
     *
     * @return the ResourceHelper to use
     */
    protected ResourceHelper findResourceHelperForLocalizedMessageViaEnclosingClass()
    {
        String className = getClass().getName();
        int    dollar = className.indexOf( '$' );
        if( dollar >= 0 ) {
            className = className.substring( 0, dollar );
        }
        return ResourceHelper.getInstance( className, getClass().getClassLoader() );
    }
}
