//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.ArrayFacade;
import net.ubos.util.logging.Dumper;

/**
 * A StringifierParsingChoice for CompoundStringifiers.
 *
 * @param T the type of the Objects to be stringified
 */
class CompoundStringifierChildChoice
        extends
            StringifierParsingChoice<ArrayFacade<Object>>
{
    /**
     * Constructor.
     *
     * @param stringifier the Stringifier this choice belongs to
     * @param componentChoices the choices of the child Stringifiers, in sequence from left to right
     * @param startIndex the startIndex of the String match (inclusive)
     * @param endIndex the endIndex of the String match (exclusive)
     */
    public CompoundStringifierChildChoice(
            CompoundStringifier              stringifier,
            StringifierParsingChoice []      componentChoices,
            int                              startIndex,
            int                              endIndex )
    {
        super( startIndex, endIndex );

        theStringifier      = stringifier;
        theComponentChoices = componentChoices;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayFacade<Object> unformat()
    {
        return theStringifier.compoundUnformat( theComponentChoices );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theStringifier",
                    "theComponentChoices"
                },
                new Object[] {
                    theStringifier,
                    theComponentChoices
                } );
    }

    /**
     * Pointer back to our CompoundStringifier. Somehow, the non-static inner class thing wasn't working.
     */
    protected final CompoundStringifier theStringifier;

    /**
     * The choices of the children.
     */
    protected final StringifierParsingChoice [] theComponentChoices;
}
