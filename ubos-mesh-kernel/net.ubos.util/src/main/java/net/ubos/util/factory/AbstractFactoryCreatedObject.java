//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.factory;

/**
 * Makes implementing FactoryCreatedObjects a tiny bit simpler.
 * 
 * @param <K> the type of key
 * @param <V> the type of value
 * @param <A> the type of argument
 */
public abstract class AbstractFactoryCreatedObject<K,V,A>
        implements
            FactoryCreatedObject<K,V,A>
{
    /**
     * Constructor for subclasses only.
     */
    protected AbstractFactoryCreatedObject()
    {
        // nothing right now
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFactory(
            Factory<K,V,A> factory )
    {
        theFactory = factory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Factory<K,V,A> getFactory()
    {
        return theFactory;
    }
    
    /**
     * Subclasses can invoke this method whenever they are updated. This will cause
     * the Factory to be notified of the update.
     */
    protected void factoryCreatedObjectUpdated()
    {
        Factory<K,V,A> factory = theFactory; // trick allows us to not use a synchronized
        if( factory != null ) {
            factory.factoryCreatedObjectUpdated( this );
        }
    }

    /**
     * The Factory that created this FactoryCreatedObject.
     */
    private Factory<K,V,A> theFactory;
}
