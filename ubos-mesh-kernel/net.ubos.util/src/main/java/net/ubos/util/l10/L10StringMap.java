//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.l10;

import net.ubos.util.CanGenerateJavaConstructorString;

/**
 * A L10Map for Strings.
 */
public interface L10StringMap
    extends
        L10Map<String>,
        CanGenerateJavaConstructorString
{
    // no op
}
