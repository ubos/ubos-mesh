//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * An ParseException indicating that the String contained one or more
 * invalid characters or character strings.
 */
public class InvalidCharacterParseException
    extends
        LocalizedParseException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param string the text that could not be parsed
     * @param message error message
     * @param errorOffset the position where the error is found while parsing.
     * @param invalid the invalid character or string
     */
    public InvalidCharacterParseException(
            String string,
            String message,
            int    errorOffset,
            String invalid )
    {
        super( string, message, errorOffset, null );

        theInvalid = invalid;
    }

    /**
     * Obtain the invalid sub-String.
     *
     * @return the invalid sub-String
     */
    public String getInvalid()
    {
        return theInvalid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theString, getErrorOffset(), theInvalid };
    }

    /**
     * The invalid sub-String.
     */
    protected final String theInvalid;
}
