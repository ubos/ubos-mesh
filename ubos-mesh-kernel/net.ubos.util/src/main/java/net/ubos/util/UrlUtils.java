//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import net.ubos.util.logging.Log;

/**
 * Collection of utility functions for URLs.
 */
public class UrlUtils
{
    private static final Log log = Log.getLogInstance( UrlUtils.class ); // our own, private logger

    /**
     * Helper method to escape a String in a URL. This allows us to avoid writing
     * all this exception code all over the place.
     *
     * @param s the String
     * @return the escaped String
     * @see #decodeUrl
     */
    public static String encodeToValidUrl(
            String s )
    {
        StringBuilder ret = new StringBuilder( s.length() * 5 / 4 ); // fudge factor

        for( int i=0 ; i<s.length() ; ++i ) {
            char c = s.charAt( i );

            if(    ( c >= 'A' && c <= 'Z')
                || ( c >= 'a' && c <= 'z' )
                || ( c >= '0' && c <= '9' ))
            {
                ret.append( c );

            } else if(    c == '/'
                       || c == '.'
                       || c == '-'
                       || c == '_' ) {
                ret.append( c );
                        // Due to Tomcat 6 and http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-0450
                        // we have to send slashes in clear text
                        // ret = ret.replaceAll( "%2[Ff]", "/" );
            } else {
                // FIXME there must be something more efficient than this
                byte [] utf8 = s.substring( i, i+1 ).getBytes( StandardCharsets.UTF_8 );
                for( int j=0 ; j<utf8.length ; ++j ) {
                    ret.append( "%" );
                    int positive = utf8[j] > 0 ? utf8[0] : ( 256 + utf8[j] );

                    String hex = Integer.toHexString( positive ).toUpperCase();
                    switch( hex.length() ) {
                        case 0:
                            ret.append( "00" );
                            break;
                        case 1:
                            ret.append( "0" ).append( hex );
                            break;
                        case 2:
                            ret.append( hex );
                            break;
                        case 3:
                            log.error( "How did we get here? " + s );
                            break;
                    }
                }
            }
        }
        return ret.toString();
    }

    /**
     * Helper method to unescape a String in a URL. This allows us to avoid writing
     * all this exception code all over the place.
     *
     * @param s the escaped String
     * @return the original String
     * @see #encodeToValidUrl
     */
    public static String decodeUrl(
            String s )
    {
        ByteArrayOutputStream found = new ByteArrayOutputStream();

        for( int i=0 ; i<s.length(); ++i ) {
            char c = s.charAt( i );

            if( c == '%' ) {
                if( i + 2 < s.length() ) {
                    byte b = (byte) Integer.parseInt( s.substring( i+1, i+3 ), 16 );
                    found.write( b );
                    i += 2; // the third comes from the loop
                } else {
                    throw new IllegalArgumentException( "Incomplete % expression in URL: " + s );
                }
            } else if( c == '+' ) {
                found.write( ' ' );
            } else {
                found.write( (byte) c );
            }
        }

        return new String( found.toByteArray(), StandardCharsets.UTF_8 );
    }

    /**
     * Helper method to escape a String suitably before it can be appended to the query parameters
     * in a URL.
     *
     * @param s the String
     * @return the escaped String
     * @see #decodeUrlArgument
     */
    public static String encodeToValidUrlArgument(
            String s )
    {
        return encodeToValidUrl( s );
    }

    /**
     * Helper method to descape a String suitable before it is extracted as one of the query parameters
     * in a URL.
     *
     * @param s the String
     * @return the descaped String
     * @see #encodeToValidUrlArgument
     */
    public static String decodeUrlArgument(
            String s )
    {
        String ret = decodeUrl( s );
        return ret;
    }

    /**
     * Append an argument pair to a URL.
     *
     * @param url the URL to which we append the argument
     * @param argumentPair the already-escaped argument, such as <tt>foo</tt> or <tt>foo=bar</tt>, without ambersand or question mark separators
     * @return the result
     */
    public static URL appendArgumentPairToUrl(
            URL    url,
            String argumentPair )
    {
        try {
            String urlString = url.toExternalForm();
            String ret = appendArgumentPairToUrl( urlString, argumentPair );
            return new URL( ret );
        } catch( MalformedURLException ex ) {
            log.error( ex );
            return url; // some kind of fallback
        }
    }

    /**
     * Append an argument pair to a URL.
     *
     * @param url the URL to which we append the argument
     * @param argumentPair the already-escaped argument, such as <tt>foo</tt> or <tt>foo=bar</tt>, without ambersand or question mark separators
     * @return the result
     */
    public static String appendArgumentPairToUrl(
            String url,
            String argumentPair )
    {
        StringBuilder buf = new StringBuilder( url.length() + argumentPair.length() + 5 ); // fudge
        buf.append( url );
        appendArgumentPairToUrl( buf, argumentPair );
        return buf.toString();
    }

    /**
     * Append an argument pair to a URL.
     *
     * @param url the URL to which we append the argument
     * @param argumentPair the already-escaped argument, such as <tt>foo</tt> or <tt>foo=bar</tt>, without ambersand or question mark separators
     * @return the result
     */
    public static StringBuilder appendArgumentPairToUrl(
            StringBuilder url,
            String        argumentPair )
    {
        if( url.indexOf( "?" ) >= 0 ) {
            url.append(  '&' );
        } else {
            url.append(  '?' );
        }
        url.append( argumentPair );
        return url;
    }

    /**
     * Append an argument to a URL.
     *
     * @param url the URL to which we append the argument
     * @param name the name of the argument, not escaped yet
     * @param value the value of the argument, not escaped yet
     * @return the result
     */
    public static String appendArgumentToUrl(
            String url,
            String name,
            String value )
    {
        StringBuilder buf = new StringBuilder( url.length() + name.length() + value.length() + 10 ); // fudge
        buf.append( url );
        appendArgumentToUrl( buf, name, value );
        return buf.toString();
    }

    /**
     * Append an argument to a URL.
     *
     * @param url the URL to which we append the argument
     * @param name the name of the argument, not escaped yet
     * @param value the value of the argument, not escaped yet
     * @return the result
     */
    public static StringBuilder appendArgumentToUrl(
            StringBuilder url,
            String        name,
            String        value )
    {
        if( url.indexOf( "?" ) >= 0 ) {
            url.append(  '&' );
        } else {
            url.append(  '?' );
        }
        url.append( encodeToValidUrlArgument( name ));
        if( value != null ) {
            url.append( '=' );
            url.append( encodeToValidUrlArgument( value ));
        }
        return url;
    }

    /**
     * Append all the provided arguments to a URL.
     *
     * @param url the URL to which we append the argument
     * @param args the (possibly multi-valued) arguments
     * @return the result
     */
    public static String appendArgumentsToUrl(
            String               url,
            Map<String,String[]> args )
    {
        if( args == null || args.isEmpty() ) {
            return url;
        }
        StringBuilder buf = new StringBuilder( url.length() + args.size() * 10 ); // fudge
        buf.append( url );
        appendArgumentsToUrl( buf, args );
        return buf.toString();
    }

    /**
     * Append all the provided arguments to a URL.
     *
     * @param url the URL to which we append the argument
     * @param args the (possibly multi-valued) arguments
     * @return the result
     */
    public static StringBuilder appendArgumentsToUrl(
            StringBuilder        url,
            Map<String,String[]> args )
    {
        if( args == null || args.isEmpty() ) {
            return url;
        }

        char sep;
        if( url.indexOf( "?" ) >= 0 ) {
            sep = '&' ;
        } else {
            sep = '?';
        }

        for( Map.Entry<String,String[]> current : args.entrySet()) {
            String [] values = current.getValue();
            if( values == null || values.length == 0 ) {
                url.append( sep );
                url.append( encodeToValidUrlArgument( current.getKey() ));
                sep =  '&';

            } else {
                for( String value : values ) {
                    url.append( sep );
                    url.append( encodeToValidUrlArgument( current.getKey() ));
                    url.append( '=' );
                    url.append( encodeToValidUrlArgument( value ));
                    sep =  '&';
                }
            }
        }
        return url;
    }

    /**
     * Append an argument to a URL if it does not exist yet; replace if it exists already.
     *
     * @param url the URL to which we append the argument
     * @param name the name of the argument, not escaped yet
     * @param value the value of the argument, not escaped yet
     * @return the result
     */
    public static String replaceOrAppendArgumentToUrl(
            String url,
            String name,
            String value )
    {
        int q = url.indexOf( '?' );
        if( q < 0 ) {
            return appendArgumentToUrl( url, name, value );
        }
        String    query      = url.substring( q+1 );
        String [] args       = query.split( "&" );
        String    nameEquals = name + "=";

        for( int i=0 ; i<args.length ; ++i ) {
            if( args[i].startsWith( nameEquals )) {
                // replace
                StringBuilder ret = new StringBuilder( url.length() + 10 ); // fudge
                ret.append( url.substring( 0, q ));
                ret.append( '?' );
                for( int j=0 ; j<args.length ; ++j ) {
                    if( j == i ) {
                        ret.append( encodeToValidUrlArgument( name ));
                        if( value != null ) {
                            ret.append( '=' );
                            ret.append( encodeToValidUrlArgument( value ));
                        }
                    } else {
                        ret.append( args[j] );
                    }
                }
                return ret.toString();
            }
        }

        return appendArgumentToUrl( url, name, value );
    }

    /**
     * Append an argument to a URL if it does not exist yet; replace if it exists already.
     *
     * @param url the URL to which we append the argument
     * @param name the name of the argument, not escaped yet
     * @param value the value of the argument, not escaped yet
     */
    public static void replaceOrAppendArgumentToUrl(
            StringBuilder url,
            String        name,
            String        value )
    {
        int q = url.indexOf( "?" );
        if( q < 0 ) {
            appendArgumentToUrl( url, name, value );
            return;
        }
        String    query      = url.substring( q+1 );
        String [] args       = query.split( "&" );
        String    nameEquals = name + "=";

        for( int i=0 ; i<args.length ; ++i ) {
            if( args[i].startsWith( nameEquals )) {
                // replace
                url.delete( q+1, url.length() );
                for( int j=0 ; j<args.length ; ++j ) {
                    if( j == i ) {
                        url.append( encodeToValidUrlArgument( name ));
                        if( value != null ) {
                            url.append( '=' );
                            url.append( encodeToValidUrlArgument( value ));
                        }
                    } else {
                        url.append( args[j] );
                    }
                }
                return;
            }
        }

        appendArgumentToUrl( url, name, value );
    }

    /**
     * Obtain a named argument from a URL.
     *
     * @param u the URL
     * @param arg the name of the argument
     * @return the value of the named argument
     */
    public static String getUrlArgument(
            String u,
            String arg )
    {
        int q = u.indexOf( '?' );
        if( q < 0 ) {
            return null;
        }
        String args = u.substring( q + 1 );
        String [] pairs = args.split( "&" );
        for( int i=0 ; i<pairs.length ; ++i ) {
            String current = pairs[i];
            int    equals  = current.indexOf( '=' );
            if( equals < 0 ) {
                continue; // won't have a value
            }
            String name = decodeUrlArgument( current.substring( 0, equals ));
            if( arg.equals( name )) {
                String value = decodeUrlArgument( current.substring( equals+1 ));
                return value;
            }
        }
        return null;
    }
}
