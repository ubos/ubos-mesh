//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.util.ArrayList;
import java.util.Comparator;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Simple implementation of OrderedSetWithValue.
 * 
 * @param <K> type of object
 * @param <V> type of value
 */
public class OrderedMSetWithValue<K,V>
    implements
        OrderedSetWithValue<K,V>
{
    /**
     * Factory method. Orders from smallest to largest.
     * 
     * @return the created OrderedMSetWithValue
     * @param <KK> type of object
     * @param <VV> type of value
     */
    public static <KK,VV extends Comparable<VV>> OrderedMSetWithValue<KK,VV> createSmallToLarge()
    {
        return new OrderedMSetWithValue<>( Integer.MAX_VALUE, ( VV v1, VV v2 ) -> v1.compareTo( v2 ));
    }

    /**
     * Factory method. Orders from smallest to largest. Don't collect more than the top max values.
     * This may return more elements than max, if the value of several keys is the same.
     * 
     * @param max the maximum number of elements to collect
     * @return the created OrderedMSetWithValue
     * @param <KK> type of object
     * @param <VV> type of value
     */
    public static <KK,VV extends Comparable<VV>> OrderedMSetWithValue<KK,VV> createSmallToLarge(
            int max )
    {
        return new OrderedMSetWithValue<>( max, ( VV v1, VV v2 ) -> v1.compareTo( v2 ));
    }

    /**
     * Factory method. Orders from largest to smallest.
     * 
     * @return the created OrderedMSetWithValue
     * @param <KK> type of object
     * @param <VV> type of value
     */
    public static <KK,VV extends Comparable<VV>> OrderedMSetWithValue<KK,VV> createLargeToSmall()
    {
        return new OrderedMSetWithValue<>( Integer.MAX_VALUE, ( VV v1, VV v2 ) -> v2.compareTo( v1 ));
    }

    /**
     * Factory method. Orders from largest to smallest. Don't collect more than the top max values.
     * This may return more elements than max, if the value of several keys is the same.
     * 
     * @param max the maximum number of elements to collect
     * @return the created OrderedMSetWithValue
     * @param <KK> type of object
     * @param <VV> type of value
     */
    public static <KK,VV extends Comparable<VV>> OrderedMSetWithValue<KK,VV> createLargeToSmall(
            int max )
    {
        return new OrderedMSetWithValue<>( max, ( VV v1, VV v2 ) -> v2.compareTo( v1 ));
    }

    /**
     * Factory method. Orders based on provided comparator.
     * 
     * @param comparator knows how to compare values
     * @return the created OrderedMSetWithValue
     * @param <KK> type of object
     * @param <VV> type of value
     */
    public static <KK,VV extends Comparable<VV>> OrderedMSetWithValue<KK,VV> create(
            Comparator<VV> comparator )
    {
        return new OrderedMSetWithValue<>( Integer.MAX_VALUE, comparator );
    }

    /**
     * Factory method. Orders based on provided comparator. Don't collect more than the top max values.
     * This may return more elements than max, if the value of several keys is the same.
     * 
     * @param max the maximum number of elements to collect
     * @param comparator knows how to compare values
     * @return the created OrderedMSetWithValue
     * @param <KK> type of object
     * @param <VV> type of value
     */
    public static <KK,VV extends Comparable<VV>> OrderedMSetWithValue<KK,VV> create(
            int            max,
            Comparator<VV> comparator )
    {
        return new OrderedMSetWithValue<>( max, comparator );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param max the maximum number of elements to collect
     * @param comparator knows how to compare values
     */
    protected OrderedMSetWithValue(
            int           max,
            Comparator<V> comparator )
    {
        theMax        = max;
        theComparator = comparator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<K> iterator()
    {
        return ArrayListCursorIterator.create( theContent );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        return theContent.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V put(
            K obj,
            V v )
    {
        // either we have the object already, or not
        // if we have it already, we probably have to reorder
        
        V ret = null;

        if( theContent.size() >= theMax || theThreshold == null || theComparator.compare( v, theThreshold ) <= 0 ) {
            // if it isn't: we are full, we skip

            int newIndex = theContent.size();
            for( int i=0 ; i<theContent.size() ; ++i ) {
                if( theComparator.compare( v, (V) theValues.get( i ) ) < 0 ) {
                    newIndex = i;
                    break;
                }
            }

            int foundAt = -1;
            for( int i=0 ; i<theContent.size() ; ++i ) {
                if( obj == theContent.get( i )) {
                    foundAt = i;
                    ret     = (V) theValues.get( i );
                    break;
                }
            }
            if( foundAt >= 0 ) {
                if( newIndex < foundAt ) {
                    theContent.remove( foundAt );
                    theValues.remove(  foundAt );

                    theContent.add( newIndex, obj );
                    theValues.add(  newIndex, v );

                } else if( newIndex > foundAt ) {
                    theContent.add( newIndex, obj );
                    theValues.add(  newIndex, v );

                    theContent.remove( foundAt );
                    theValues.remove(  foundAt );

                } else {
                    theValues.set( newIndex, v ); // might have different value, even in the same place
                }

            } else {
                theContent.add( newIndex, obj );
                theValues.add(  newIndex, v );
            }
            
            if( theContent.size() > theMax ) {
                V candidateThreshold = theValues.get( theMax-1 );
                if( theThreshold == null || theComparator.compare( candidateThreshold, theThreshold ) <= 0 ) {
                    theThreshold = candidateThreshold;
                    
                    for( int i=theMax ; i<theContent.size() ; ++i ) {
                        if( !candidateThreshold.equals( theValues.get( i ))) {
                            // we can discard the rest
                            for( int j=theContent.size()-1 ; j>=i ; --j ) {
                                theContent.remove( j );
                                theValues.remove( j );
                            }
                            break;
                        }
                    }
                }
            }
        }
        return ret;


//
//                    
//                if( count <= max || sum >= threshold ) {
//                    if( count > max ) {
//                        long min = ret.getValueFor( ret.get( max ));
//                        if( min > threshold ) {
//                            threshold = min;
//                        }
//                    }
//                }
//                ++count;
//            }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public V getValueFor(
            K obj )
    {
        for( int i=0 ; i<theContent.size() ; ++i ) {
            if( obj == theContent.get( i )) {
                return theValues.get( i );
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public K getFirst()
    {
        if( theContent.isEmpty() ) {
            return null;
        }
        return theContent.get( 0 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public K getLast()
    {
        if( theContent.isEmpty() ) {
            return null;
        }
        return theContent.get( theContent.size()-1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public K get(
            int n )
    {
        return theContent.get( n ); // will throw
    }

    /**
     * The maximum number of elements to collect.
     */
    protected final int theMax;

    /**
     * Defines the ordering.
     */
    protected final Comparator<V> theComparator;

    /**
     * The current minimum that we accept because we have been given too many
     * elements already. This is null if we aren't there yet.
     */
    protected V theThreshold = null;

    /**
     * The current content of this set.
     */
    protected final ArrayList<K> theContent = new ArrayList<>();

    /**
     * The values, in the same sequence as the content
     */
    protected final ArrayList<V> theValues = new ArrayList<>();
}
