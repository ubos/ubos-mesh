//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

/**
 * This interface is supported by classes that know how to generate Java code
 * that will instantiate an identical object of the current instance.
 */
public interface CanGenerateJavaConstructorString
{
    /**
     * Obtain a string which is the Java-language constructor expression reflecting this value.
     * This is for code-generation purposes.
     *
     * @param classLoaderVar name of a variable containing the class loader to be used to initialize this value
     * @param typeVar  name of the variable containing the DataType that goes with the to-be-created instance.
     * @return the Java-language constructor expression
     */
    public String getJavaConstructorString(
            String classLoaderVar,
            String typeVar );
}
