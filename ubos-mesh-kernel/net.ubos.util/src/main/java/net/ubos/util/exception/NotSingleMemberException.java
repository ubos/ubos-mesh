//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * Thrown if an accessor method expecting a single member of a set was used,
 * but the set contained a different number of elements.
 */
public class NotSingleMemberException
    extends
        IllegalStateException
{
    /**
     * Constructor.
     *
     * @param n the number of elements in the set
     */
    public NotSingleMemberException(
            int n )
    {
        theN = n;
    }

    /**
     * Constructor.
     *
     * @param msg the message
     * @param n the number of elements in the set
     */
    public NotSingleMemberException(
            String msg,
            int    n )
    {
        super( msg != null ? ( msg + ": " + n ) : String.valueOf( n ));

        theN = n;
    }

    /**
     * Constructor.
     *
     * @param msg the message
     * @param n the number of elements in the set
     * @param cause the underlying cause
     */
    public NotSingleMemberException(
            String    msg,
            int       n,
            Throwable cause )
    {
        super( msg != null ? ( msg + ": " + n ) : String.valueOf( n ), cause );

        theN = n;
    }

    /**
     * Obtain the actual number of elements in the set.
     *
     * @return the actual number
     */
    public int getActualNumberInSet()
    {
        return theN;
    }

    /**
     * The number of elements in the set.
     */
    protected int theN;
}
