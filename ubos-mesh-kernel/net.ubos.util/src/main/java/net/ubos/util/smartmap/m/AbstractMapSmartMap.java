//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap.m;

import java.util.Map;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.util.smartmap.AbstractSmartMap;
import net.ubos.util.smartmap.SmartMap;

/**
 * Simple implementation of SmartMap that uses the JDK's Map as storage.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public abstract class AbstractMapSmartMap<K,V>
    extends
        AbstractSmartMap<K,V>
    implements
        SmartMap<K,V>
{
    /**
     * Constructor.
     *
     * @param storage the Map object to use for storage
     */
    protected AbstractMapSmartMap(
            Map<K,V> storage )
    {
        theStorage = storage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        theStorage.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            K key )
    {
        boolean ret = theStorage.containsKey( key );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V get(
            K key )
    {
        V ret = theStorage.get( key );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            K key,
            V value )
    {
        V ignored = theStorage.put( key, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            K key )
    {
        V ignored = theStorage.remove( key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V remove(
            K key )
    {
        V ret = theStorage.remove( key );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        return MapCursorIterator.createForKeys( theStorage );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<V> valueIterator()
    {
        return MapCursorIterator.createForValues( theStorage );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        int ret = theStorage.size();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueUpdated(
            K key,
            V value )
    {
        // no op
    }

    /**
     * The internal representation.
     */
    protected final Map<K,V> theStorage;
}
