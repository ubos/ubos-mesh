//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * An Exception that collects several other Throwables. This Exception can be thrown
 * if more than one problem occurred simultaneously and all of them should be passed on.
 */
public class CompoundException
    extends
        Exception
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param ts the Throwable
     */
    public CompoundException(
            Throwable [] ts )
    {
        theThrowables = ts;
    }

    /**
     * Convenience constructor.
     *
     * @param t1 first Throwable
     * @param t2 second Throwable
     */
    public CompoundException(
            Throwable t1,
            Throwable t2 )
    {
        this( new Throwable[] { t1, t2 } );
    }

    /**
     * Obtain the collected Exceptions.
     *
     * @return the Exceptions
     */
    public Throwable [] getThrowables()
    {
        return theThrowables;
    }

    /**
     * The collected Throwables.
     */
    protected final Throwable [] theThrowables;
}
