//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.listenerset;

import net.ubos.util.logging.Log;

/**
 * Factors out common functionality of sets of listeners.
 * 
 * @param <T> the type of the listener
 * @param <E> the type of the event
 * @param <P> the type of parameter that allows this AbstractListenerSet to invoke the right listener method
 */
public abstract class AbstractListenerSet<T,E,P>
        implements
            ListenerSet<T,E,P>
{
    private static final Log log = Log.getLogInstance( AbstractListenerSet.class ); // our own, private logger

    /**
     * Constructor.
     * 
     * @param catchRuntimeExceptions if true, any event will be fired to all listeners, even if the listeners throw RuntimeExceptions
     */
    protected AbstractListenerSet(
            boolean catchRuntimeExceptions )
    {
        theCatchRuntimeExceptions = catchRuntimeExceptions;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public abstract void addWeak(
            T newListener );
    
    /**
     * {@inheritDoc]
     */
    @Override
    public abstract void addSoft(
            T newListener );
    
    /**
     * {@inheritDoc]
     */
    @Override
    public abstract void addDirect(
            T newListener );

    /**
     * {@inheritDoc]
     */
    @Override
    public abstract void remove(
            T oldListener );
    
    /**
     * {@inheritDoc]
     */
    @Override
    public abstract boolean isEmpty();

    /**
     * {@inheritDoc]
     */
    @Override
    public abstract void clear();
    
    /**
     * Fire an event to all members of this set.
     *
     * @param event the event, such as PropertyChangeEvent
     */
    public void fireEvent(
            E event )
    {
        fireEvent( event, null );
    }

    /**
     * Fire an event to all members of the set. The parameter is helpful for dispatching
     * to the right method in the listener interfaces for subclasses of this class.
     *
     * @param event the event, such as PropertyChangeEvent
     * @param parameter a user-specific paramater that can be used to dispatch to the right method in the fireEventToListener method
     */
    public abstract void fireEvent(
            E event,
            P parameter );

    /**
     * Fire the provided event and parameter to the set of objects in this array.
     *
     * @param realList the real list of listeners
     * @param startIndex the start index in the real list of listeners
     * @param event the event, such as PropertyChangeEvent
     * @param parameter a user-specific paramater that can be used to dispatch to the right method in the fireEventToListener method
     */
    protected void internalFireEvent(
            T [] realList,
            int  startIndex,
            E    event,
            P    parameter )
    {
        // now fire outside of the synchronized block
        if( theCatchRuntimeExceptions ) {
            if( realList.length > 0 ) {
                for( int i=startIndex ; i<realList.length ; ++i ) {
                    try {
                        fireEventToListener( realList[i], event, parameter );
                    } catch( RuntimeException ex ) {
                        log.error( ex );
                    }
                }
            } else {
                try {
                    fireEventIfNoListeners( event, parameter );
                } catch( RuntimeException ex ) {
                    log.error( ex );
                }
            }
        } else {
            if( realList.length > 0 ) {
                for( int i=startIndex ; i<realList.length ; ++i ) {
                    fireEventToListener( realList[i], event, parameter );
                }
            } else {
                fireEventIfNoListeners( event, parameter );
            }
        }
    }
    
    /**
     * If this ListenerSet does not currently have any listeners, invoke this
     * overridable method that can handle that case. By default, this method
     * does nothing.
     * 
     * @param event the event, such as PropertyChangeEvent
     * @param parameter a user-specific paramater that can be used to dispatch to the right method in the fireEventToListener method
     */
    protected void fireEventIfNoListeners(
            E event,
            P parameter )
    {
        // do nothing
    }

    /**
     * Fire the event to one contained object.
     *
     * @param listener the receiver of this event
     * @param event the sent event
     * @param parameter dispatch parameter
     */
    protected abstract void fireEventToListener(
            T listener,
            E event,
            P parameter );

    /**
     * If this is true, we catch all RuntimeExceptions from our listeners.
     */
    protected boolean theCatchRuntimeExceptions;
}
