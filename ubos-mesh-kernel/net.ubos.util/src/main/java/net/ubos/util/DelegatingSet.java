//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import net.ubos.util.iterator.CompositeIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * A Set that delegates to other Sets.
 *
 * @param <E> the content type
 */
public class DelegatingSet<E>
        implements
            Set<E>
{
    /**
     * Factory method for any number of delegates.
     *
     * @param delegates the Maps to which this Map delegates
     * @return the created DelegatingSet
     * @param <E> the content type
     */
    public static <E> DelegatingSet<E> create(
            List<Set<E>> delegates )
    {
        return new DelegatingSet<>( delegates );
    }

    /**
     * Factory method for any number of delegates.
     *
     * @param delegates the Maps to which this Map delegates
     * @return the created DelegatingSet
     * @param <E> the content type
     */
    public static <E> DelegatingSet<E> create(
            Set<E> [] delegates )
    {
        List<Set<E>> delegates2 = new ArrayList<>( delegates.length );
        for( int i=0 ; i<delegates.length ; ++i ) {
            delegates2.add( delegates[i] );
        }
        return new DelegatingSet<>( delegates2 );
    }

    /**
     * Factory method for two delegates.
     *
     * @param del1 the first delegate
     * @param del2 the second delegate
     * @return the created DelegatingSet
     * @param <E> the content type
     */
    public static <E> DelegatingSet<E> create(
            Set<E> del1,
            Set<E> del2 )
    {
        List<Set<E>> delegates2 = new ArrayList<>( 2 );
        delegates2.add( del1 );
        delegates2.add( del2 );

        return new DelegatingSet<>( delegates2 );
    }

    /**
     * Constructor.
     *
     * @param delegates the Sets to which this Set delegates
     */
    protected DelegatingSet(
            List<Set<E>> delegates )
    {
        theDelegates = delegates;
    }

    /**
     * Obtain the delegates.
     *
     * @return the delegates
     */
    public List<Set<E>> getDelegates()
    {
        return theDelegates;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public int size()
    {
        int ret = 0;
        for( Set<E> current : theDelegates ) {
            ret += current.size();
        }
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean isEmpty()
    {
        for( Set<E> current : theDelegates ) {
            if( !current.isEmpty() ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean contains(
            Object o )
    {
        for( Set<E> current : theDelegates ) {
            if( current.contains( o ) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public Iterator<E> iterator()
    {
        List<Iterator<E>> newDelegates = new ArrayList<>( theDelegates.size() );

        for( Set<E> current : theDelegates ) {
            newDelegates.add( current.iterator() );
        }

        CompositeIterator<E> ret = CompositeIterator.createFromIterators( newDelegates );
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public Object [] toArray()
    {
        return toArray( new Object[0] );
    }

    /**
     * {@inheritDoc]
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T[] toArray(
            T [] a )
    {
        int               count = 0;
        ArrayList<Object> overflow = null;

        for( Set<E> current : theDelegates ) {
            for( E current2 : current ) {
                if( count < a.length ) {
                    a[count++] = (T) current2;
                } else {
                    if( overflow == null ) {
                        overflow = new ArrayList<>();
                    }
                    overflow.add( current2 );
                }
            }
        }

        T [] ret;
        if( overflow == null || overflow.isEmpty() ) {
            ret = a;
        } else {
            ret = ArrayHelper.createArray( (Class<T>) a.getClass().getComponentType(), a.length + overflow.size() );
            System.arraycopy( a, 0, ret, 0, a.length );
            for( int i=0 ; i<overflow.size() ; ++i ) {
                ret[i + a.length] = (T) overflow.get( i );
            }
        }
        return ret;
    }

    // Modification Operations

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean add(
            E o )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean remove(
            Object o )
    {
        throw new UnsupportedOperationException();
    }

    // Bulk Operations

    /**
     * {@inheritDoc]
     */
    @Override
    @SuppressWarnings("element-type-mismatch")
    public boolean containsAll(
            Collection<?> c )
    {
        for( Object x : c ) {
            if( !contains( x )) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean addAll(
            Collection<? extends E> c )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean retainAll(
            Collection<?> c )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean removeAll(
            Collection<?> c )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void clear()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * The Sets to which this Set delegates.
     */
    protected List<Set<E>> theDelegates;
}
