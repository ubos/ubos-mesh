//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.listenerset;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * <p>A container for listener objects based on direct containment or references.
 * This needs to be subclassed to add the actual listener invocation method.</p>
 *
 * <p>Experiences shows that if listeners throw (unexpected) RuntimeExceptions,
 * often the whole rest of a piece of software gets confused as events are
 * sent only to some and not all of the listeners. Per default, this
 * class catches all RuntimeExceptions thrown by its listeners. This
 * behavior can be configured through a parameter in the constructor.</p>
 * 
 * @param <T> the type of listener
 * @param <E> the type of event
 * @param <P> the type of parameter
 */
public abstract class FlexibleListenerSet<T,E,P>
        extends
            AbstractListenerSet<T,E,P>
{
    /**
     * Constructor with default initial size.
     */
    protected FlexibleListenerSet()
    {
        this( true );
    }

    /**
     * Constructor with specified initial size.
     *
     * @param size initial size of the collection
     */
    protected FlexibleListenerSet(
            int size )
    {
        this( size, true );
    }

    /**
     * Create one with default initial size, specifying whether or not we want to catch
     * all RuntimeExceptions.
     *
     * @param catchRuntimeExceptions if set to false, firing events will be stopped as soon as an Exception occurs.
     */
    protected FlexibleListenerSet(
            boolean catchRuntimeExceptions )
    {
        super( catchRuntimeExceptions );

        theContent = new ArrayList<>();
    }

    /**
     * Create one with specified initial size, specifying whether or not we want to catch
     * all RuntimeExceptions.
     *
     * @param size initial size of the collection
     * @param catchRuntimeExceptions if set to false, firing events will be stopped as soon as an Exception occurs.
     */
    protected FlexibleListenerSet(
            int     size,
            boolean catchRuntimeExceptions )
    {
        super( catchRuntimeExceptions );

        theContent = new ArrayList<>( size );
    }

    /**
     * Obtain an iterator over this set. This iterator is guaranteed to skip
     * garbage-collected objects.
     *
     * @return the iterator over this set
     */
    public Iterator<T> iterator()
    {
        return new MyIterator<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addWeak(
            T newListener )
    {
        internalAdd( new WeakReference<>( newListener ), newListener );
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void addSoft(
            T newListener )
    {
        internalAdd( new SoftReference<>( newListener ), newListener );
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void addDirect(
            T newListener )
    {
        internalAdd( newListener, newListener );
    }
    
    /**
     * Internal helper to add a new listener.
     * 
     * @param newListenerOrReference the Reference to the listener, or the listener directly
     * @param newListener the new listener
     */
    @SuppressWarnings(value={"unchecked"})
    protected void internalAdd(
            Object newListenerOrReference,
            Object newListener )
    {
        // set this to false for production build if you like
        if( true ) {
            for( int i=theContent.size()-1 ; i>=0 ; --i ) {
                Object found = theContent.get( i );
                if( found instanceof Reference ) {
                    Reference<T> current = (Reference<T>) found;
                    if( current.get() == newListener ) {
                        throw new IllegalArgumentException( "have listener already: " + newListener );
                    }
                } else {
                    if( found == newListener ) {
                        throw new IllegalArgumentException( "have listener already: " + newListener );
                    }
                }
            }
        }
        theContent.add( newListenerOrReference );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public synchronized void remove(
            T oldListener )
    {
        for( int i=theContent.size()-1 ; i>=0 ; --i ) {
            Object found = theContent.get( i );
            if( found instanceof Reference ) {
                Reference<T> current = (Reference<T>) found;
                if( current.get() == oldListener ) {
                    theContent.remove( i );
                    break;
                }
            } else {
                if( found == oldListener ) {
                    theContent.remove( i );
                    break;
                }
            }
        }
    }

    /**
     * Obtain the nth object contained in this set. This may return null if
     * the object has been garbage-collected.
     *
     * @param n specifies the position of the element we want to obtain
     * @return the found listener object at this position, or null.
     */
    @SuppressWarnings(value={"unchecked"})
    public T get(
            int n )
    {
        Object found = theContent.get( n );
        if( found instanceof Reference ) {
            Reference<T> current = (Reference<T>) found;
            return current.get();
        } else {
            return (T) found;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public synchronized boolean isEmpty()
    {
        if( theContent.isEmpty() ) {
            return true;
        }
        int size = theContent.size();
        int firstNull = -1;
        for( int i=size-1 ; i>=0 ; --i ) {
            Object found = theContent.get( i );
            if( found instanceof Reference ) {
                Reference<T> current = (Reference<T>) found;
                if( current.get() == null ) {
                    firstNull = i;
                    break;
                }
            }
        }
        if( firstNull < 0 ) {
            return false;
        }

        ArrayList<Object> newContent = new ArrayList<>( size-1 ); // fits all elements, maybe more
        for( int i=0 ; i<size ; ++i ) {
            Object found = theContent.get( i );
            if( found instanceof Reference ) {
                Reference<T> current = (Reference<T>) found;
                if( current.get() != null ) {
                    newContent.add( current );
                }
            } else {
                newContent.add( found );
            }
        }
        theContent = newContent;
        return theContent.isEmpty();
    }

    /**
     * Obtain the number of elements in this set. Because of garbage collection, the actual
     * number of non-null elements in this set may be lower that the returned value.
     *
     * @return the number of elements in this set (includes recently garbage-collected ones)
     */
    public int size()
    {
        return theContent.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        theContent.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public void fireEvent(
            E event,
            P parameter )
    {
        // build real listener list
        T realList [] = (T []) new Object[ theContent.size() ];
        int realIndex   = realList.length;

        synchronized( this ) {
            // by going backwards, we avoid index confusion
            for( int i=theContent.size()-1 ; i>=0 ; --i ) {
                Object found    = theContent.get( i );
                Object listener;

                if( found instanceof Reference ) {
                    Reference<T> current  = (Reference<T>) found;
                    listener = current.get();
                } else {
                    listener = found;
                }

                if( listener != null ) {
                    realList[--realIndex] = (T) listener;
                } else {
                    theContent.remove( i );
                }
            }
        }

        internalFireEvent( realList, realIndex, event, parameter );
    }
 
    /**
     * Contains the references to the listeners.
     */
    protected ArrayList<Object> theContent;

    /**
     * Iterator implementation for this set.
     * 
     * @param <T> the type of element to iterate over
     */
    class MyIterator<T>
        implements
            Iterator<T>
    {
        /**
         * Construct one. We copy the non-empty content of the set in order to
         * avoid concurrent modifications.
         */
        public MyIterator()
        {
            listeners    = new Object[ size() ];
            currentIndex = listeners.length;
            for( int i=theContent.size()-1 ; i>=0 ; --i ) {
                Object current = get(i);
                if( current != null ) {
                    listeners[ --currentIndex ] = current;
                }
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNext()
        {
            return currentIndex < listeners.length;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @SuppressWarnings(value={"unchecked"})
        public T next()
        {
            return (T) listeners[currentIndex++];
        }

        /**
         * Remove element -- not supported.
         *
         * @throws UnsupportedOperationException always throws this
         */
        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }

        /**
         * Index of the next object to be returned.
         */
        protected int currentIndex;

        /**
         * Copy of the content of the set.
         */
        protected Object [] listeners;
    }
}
