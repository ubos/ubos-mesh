//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

/**
 * Marks those objects that have a key.
 * @author jernst
 */
public interface HasKey
{
    /**
     * Obtain the key.
     *
     * @return the key
     */
    public String getKey();
}
