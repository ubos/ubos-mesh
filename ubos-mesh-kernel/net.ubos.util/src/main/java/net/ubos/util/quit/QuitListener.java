//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.quit;

/**
  * This interface is supported by those objects who are interested in knowing
  * when a quit event occurs so they can save work etc.
  */
public interface QuitListener
{
    /**
      * Announces that a quit is imminent. Note that because of
      * potential communication between various Objects that receive this
      * notification in arbitrary order, the Object still needs to be
      * functional after it has received this message and react to other
      * Objects' requests (if any).
      */
    public abstract void prepareForQuit();

    /**
      * This is the last message the Object will receive before the
      * shutdown of the VM.
      */
    public abstract void die();
}

