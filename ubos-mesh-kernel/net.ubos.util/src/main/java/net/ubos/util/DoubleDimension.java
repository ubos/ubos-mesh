//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.awt.geom.Dimension2D;

/**
  * It seems incredible, but there does not seem to be a "double"
  * implementation of Dimension2D in the JDK, so here is ours.
  */
public class DoubleDimension
        extends
            Dimension2D
        implements
           Cloneable
{
    /**
     * Construct one.
     *
     * @param w width
     * @param h height
     */
    public DoubleDimension(
            double w,
            double h )
    {
        width = w;
        height = h;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getWidth()
    {
        return width;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getHeight()
    {
        return height;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSize(
            double w,
            double h )
    {
        width = w;
        height = h;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone()
    {
        return new DoubleDimension( width, height );
    }

    /**
     * The width.
     */
    protected double width;

    /**
     * The height.
     */
    protected double height;
}
