//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.factory;

/**
 * Exception thrown if a Factory failed to create an object.
 */
public class FactoryException
        extends
            Exception
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param sender the Factory that threw this exception
     * @param cause the actual underlying cause
     */
    public FactoryException(
            Factory<?,?,?> sender,
            Throwable      cause )
    {
        super( null, cause ); // don't automatically create message

        theSender = sender;
    }

    /**
     * Constructor.
     *
     * @param sender the Factory that threw this exception
     * @param message the message
     */
    public FactoryException(
            Factory<?,?,?> sender,
            String         message )
    {
        super( message );

        theSender = sender;
    }

    /**
     * Constructor.
     *
     * @param sender the Factory that threw this exception
     * @param message the message
     * @param cause the actual underlying cause
     */
    public FactoryException(
            Factory<?,?,?> sender,
            String         message,
            Throwable      cause )
    {
        super( message, cause );

        theSender = sender;
    }

    /**
     * Constructor for subclasses only.
     *
     * @param sender the Factory that threw this exception
     */
    protected FactoryException(
            Factory<?,?,?> sender )
    {
        super();

        theSender = sender;
    }

    /**
     * Obtain the Factory that threw this exception.
     *
     * @return the Factory
     */
    public Factory<?,?,?> getSender()
    {
        return theSender;
    }

    /**
     * The Factory that throw this exception.
     */
    protected final Factory<?,?,?> theSender;
}
