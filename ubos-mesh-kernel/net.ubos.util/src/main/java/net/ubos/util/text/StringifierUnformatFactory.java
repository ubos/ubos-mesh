//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * A factory for the values recreated by the unformat method of a Stringifier. This is useful
 * when parsing objects that can only be created via a factory.
 *
 * This is merely a marker interface as there is no uniform factory method. Still useful, however.
 * as a marker.
 */
public interface StringifierUnformatFactory
{
    // nothing
}
