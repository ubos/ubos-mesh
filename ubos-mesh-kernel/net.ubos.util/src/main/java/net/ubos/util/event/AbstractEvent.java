//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.event;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * A general-purpose {@link Event} implementation.
 *
 * @param <S> the type of the event source
 * @param <V> the type of the value
 */
public abstract class AbstractEvent<S,V>
        implements
            Event<S,V>,
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param source the object that is the source of the event, if available
     * @param deltaValue the value that changed, if available
     */
    protected AbstractEvent(
            S source,
            V deltaValue )
    {
        theSource     = source;
        theDeltaValue = deltaValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public S getSource()
    {
        return theSource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final V getDeltaValue()
    {
        return theDeltaValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theSource",
                    "theDeltaValue"
                },
                new Object[] {
                    theSource,
                    theDeltaValue
                });
    }

    /**
     * The source of the event.
     */
    protected final S theSource;

    /**
     * The delta value of the data item whose change triggered the event.
     */
    protected final V theDeltaValue;
}
