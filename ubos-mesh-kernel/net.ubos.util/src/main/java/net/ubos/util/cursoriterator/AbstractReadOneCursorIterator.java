//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * Factors out common behaviors of {@link CursorIterator}s.
 * This implementation is for subclasses that are most efficient when reading one elements at a time (e.g. in-memory)
 *
 * @param <E> the type of element to iterate over
 */
public abstract class AbstractReadOneCursorIterator<E>
        extends
            AbstractCursorIterator<E>
{
    /**
     * Constructor.
     *
     * @param equals the equality operation for E's
     */
    protected AbstractReadOneCursorIterator(
            BiPredicate<E,E> equals )
    {
        super( equals );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> next(
            int n )
    {
        List<E> ret = new ArrayList<>();

        for( int i = 0; i < n && hasNext() ; ++i ) {
            ret.add( next());
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> previous(
            int n )
    {
        List<E> ret = new ArrayList<>();

        for( int i = 0; i < n && hasPrevious() ; ++i ) {
            ret.add( previous());
        }
        return ret;
    }
}
