//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * A PrintWriter suitable to write to a Log.
 */
public abstract class LogPrintWriter
    extends
        PrintWriter
{
    /**
     * Constructor.
     */
    public LogPrintWriter()
    {
        super( new StringWriter() );
    }

    /**
     * Flush the stream.
     */
    @Override
    @SuppressWarnings("SynchronizeOnNonFinalField")
    public void flush()
    {
        super.flush();

        StringWriter w;
        synchronized( lock ) {
            w = (StringWriter) out;
            out = new StringWriter();
        }

        log( w.toString() );
    }

    /**
     * Define this to write to the right log and channel.
     * 
     * @param buffer the content to write
     */
    public abstract void log(
            String buffer );
}
