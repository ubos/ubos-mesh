//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.factory;

/**
 * Defines the UBOS Data Mesh version of the factory pattern.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 * @param <A> the type of argument
 */
public interface Factory<K,V,A>
{
    /**
     * Factory method.
     *
     * @param key the key information required for object creation, if any
     * @param argument any argument-style information required for object creation, if any
     * @return the created object
     * @throws FactoryException catch-all Exception, consider its cause
     */
    public V obtainFor(
            K key,
            A argument )
        throws
            FactoryException;

    /**
     * Factory method. This is equivalent to specifying a null argument.
     *
     * @param key the key information required for object creation, if any
     * @return the created object
     * @throws FactoryException catch-all Exception, consider its cause
     */
    public default V obtainFor(
            K key )
        throws
            FactoryException
    {
        return obtainFor( key, null );
    }

    /**
     * Invoked only by objects that have been created by this Factory, this enables
     * the created objects to indicate to the Factory that they have been updated.
     * Depending on the implementation of the Factory, that may cause a
     * SmartFactory to write changes to disk, for example.
     *
     * @param object the FactoryCreatedObject
     */
    public default void factoryCreatedObjectUpdated(
            FactoryCreatedObject<K,V,A> object )
    {}
}
