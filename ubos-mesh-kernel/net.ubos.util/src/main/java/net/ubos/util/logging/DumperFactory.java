//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

import net.ubos.util.factory.FactoryException;

/**
 * A factory for Dumpers.
 */
public interface DumperFactory
{
    /**
     * Factory method.
     *
     * @param key the key information required for object creation, if any
     * @return the created object
     * @throws FactoryException catch-all Exception, consider its cause
     */
    public abstract Dumper obtainFor(
            Object key )
        throws
            FactoryException;

    /**
     * The default DumperFactory.
     */
    public static final DumperFactory DEFAULT_FACTORY = ToStringDumperFactory.create();
}
