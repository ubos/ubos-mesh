//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Help with date/time conversion.
 */
public abstract class DateTimeUtil
{
    /**
     * Private constructor to keep this abstract.
     */
    private DateTimeUtil() {}
    
    /**
     * Convert a Date to an RFC 3339 String.
     *
     * @param d the Date
     * @return the String
     */
    public static String dateToRfc3339(
            Date d )
    {
        String ret = theRfc3339Format.format( d );

        return ret;
    }

    /**
     * Convert an RFC 3309 String to a Date.
     *
     * @param s the String
     * @return the Date
     * @throws ParseException thrown if a syntax error occurred
     */
    public static Date rfc3339ToDate(
            String s )
        throws
            ParseException
    {
        s = s.toUpperCase();

        Date ret = theRfc3339Format.parse( s );

        return ret;
    }

    /**
     * Convert a Date to a W3C String.
     *
     * @param d the Date
     * @return the String
     */
    public static String dateToW3c(
            Date d )
    {
        String ret = theW3cFormat.format( d );

        return ret;
    }

    /**
     * Convert a W3C String to a Date.
     *
     * @param s the String
     * @return the Date
     * @throws ParseException thrown if a syntax error occurred
     */
    public static Date w3cToDate(
            String s )
        throws
            ParseException
    {
        s = s.toUpperCase();

        Date ret = theW3cFormat.parse( s );

        return ret;
    }

    /**
     * The UTC TimeZone.
     */
    public static final TimeZone UTC = TimeZone.getTimeZone( "UTC" );

    /**
     * Date format to use for RFC 3339.
     */
    public static final SimpleDateFormat theRfc3339Format = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    static {
        theRfc3339Format.setTimeZone( UTC );
    }

    /**
     * Date format to use for W3C.
     */
    public static final SimpleDateFormat theW3cFormat = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss'Z'");
    static {
        theW3cFormat.setTimeZone( UTC );
    }
}
