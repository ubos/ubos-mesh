//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * One alternative how to parse a String using a Stringifier.
 *
 * @param <T> the type of the Objects to be stringified
 */
public abstract class StringifierParsingChoice<T>
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param startIndex the start index (inclusive) in the parsed String
     * @param endIndex the end index (exclusive) in the parsed String
     */
    protected StringifierParsingChoice(
            int    startIndex,
            int    endIndex )
    {
        theStartIndex = startIndex;
        theEndIndex   = endIndex;
    }

    /**
     * Obtain the position (inclusive) from which the match was accomplished.
     *
     * @return the position
     */
    public int getStartIndex()
    {
        return theStartIndex;
    }

    /**
     * Obtain the position (exclusive) through which the match was accomplished.
     *
     * @return the position
     */
    public int getEndIndex()
    {
        return theEndIndex;
    }

    /**
     * Given this StringifierParsingChoice, obtain the Object parsed. This may
     * return null, given that some Stringifiers don't accept Objects into their
     * format method either.
     *
     * @return the Object parsed
     */
    public abstract T unformat();

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theStartIndex",
                    "theEndIndex"
                },
                new Object[] {
                    theStartIndex,
                    theEndIndex
                });
    }

    /**
     * The index of the first character (inclusive) of this choice.
     */
    protected final int theStartIndex;

    /**
     * The index of the last character (exclusive) of this choice.
     */
    protected final int theEndIndex;
}
