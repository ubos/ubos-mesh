//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * This interface is implemented by those Exceptions that, while obviously
 * indicating that something went wrong, still want to return at least
 * a partial result.
 * 
 * @param <T> the type of partial result
 */
public interface PartialResultException<T>
{
    /**
     * Determine whether a partial result is available.
     *
     * @return true if a partial result is available
     */
    public boolean isPartialResultAvailable();

    /**
     * Obtain the partial result, if any.
     * The actual return type is the return type of the method that threw
     * this Exception.
     *
     * @return the partial result
     */
    public T getBestEffortResult();
}
