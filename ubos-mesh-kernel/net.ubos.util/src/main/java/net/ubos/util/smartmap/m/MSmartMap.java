//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap.m;

import java.util.HashMap;
import java.util.Map;
import net.ubos.util.smartmap.SmartMap;

/**
 * Simple in-memory implementation of SmartMap.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public class MSmartMap<K,V>
    extends
        AbstractMapSmartMap<K,V>
    implements
        SmartMap<K,V>
{
    /**
     * Factory  method.
     *
     * @return the created MOrderedSmartMap
     * @param <K> the key for the entries in the map
     * @param <V> the value for the entries of the map
     */
    public static <K,V> MSmartMap<K,V> create()
    {
        return new MSmartMap<>( new HashMap<>() );
    }

    /**
     * Constructor.
     *
     * @param storage the Map object to use for storage
     */
    protected MSmartMap(
            Map<K,V> storage )
    {
        super( storage );
    }
}
