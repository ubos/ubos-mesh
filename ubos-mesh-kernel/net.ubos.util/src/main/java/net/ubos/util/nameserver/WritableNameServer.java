//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.nameserver;

import java.util.function.Function;

/**
 * A {@link NameServer} whose content may be changed.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 */
public interface WritableNameServer<K,V>
        extends
            NameServer<K,V>
{
    /**
     * Add a new key and a new value.
     * Return the overwritten value, or null.
     *
     * @param key the key
     * @param value the value for the key
     * @return the old value at this key, if any
     */
    default public V put(
            K key,
            V value )
    {
        V ret = get( key );
        putIgnorePrevious( key, value );
        return ret;
    }

    /**
     * Add a new key and a new value.
     *
     * @param key the key
     * @param value the value for the key
     */
    public abstract void putIgnorePrevious(
            K key,
            V value );

    /**
     * Remove a key-value pair that was previously created. This does not affect
     * values that are currently still being constructed. The semantics of
     * &quot;remove&quot; for a SmartFactory imply &quot;deletion&quot; of the
     * object as well. Correspondingly, if the removed Object supports the
     * {@link net.ubos.util.livedead.LiveDeadObject LiveDeadObject} object, this
     * implementation will invoke the <code>die()</code> method there.
     *
     * @param key the key of the key-value pair to be removed
     * @return the value of the key-value pair to be removed, if found
     */
    default public V remove(
            K key )
    {
        return remove( key, null );
    }

    /**
     * Remove a key-value pair that was previously created. This does not affect
     * values that are currently still being constructed. The semantics of
     * &quot;remove&quot; for a SmartFactory imply &quot;deletion&quot; of the
     * object as well. The provided cleanupCode can be used to implement those
     * semantics, e.g. in order to invoke the die() method.
     *
     * @param key the key of the key-value pair to be removed
     * @param cleanupCode the cleanup code to run, if any
     * @return the value of the key-value pair to be removed, if found
     */
    public V remove(
            K                key,
            Function<V,Void> cleanupCode );

    /**
     * Remove a key-value pair that was previously created. This does not affect
     * values that are currently still being constructed. The semantics of
     * &quot;remove&quot; for a SmartFactory imply &quot;deletion&quot; of the
     * object as well. Correspondingly, if the removed Object supports the
     * {@link net.ubos.util.livedead.LiveDeadObject LiveDeadObject} object, this
     * implementation will invoke the <code>die()</code> method there.
     *
     * @param key the key of the key-value pair to be removed
     */
    public void removeIgnorePrevious(
            K key );
}
