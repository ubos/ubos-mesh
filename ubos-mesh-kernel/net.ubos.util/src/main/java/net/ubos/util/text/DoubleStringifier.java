//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies a single Double.
 */
public class DoubleStringifier
        extends
            AbstractStringifier<Double>
{
    /**
     * Factory method.
     *
     * @return the created DoubleStringifier
     */
    public static DoubleStringifier create()
    {
        return new DoubleStringifier();
    }

    /**
     * Constructor. Use factory method.
     */
    protected DoubleStringifier()
    {
        // noop
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Double                arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = String.valueOf( arg );
        ret = potentiallyShorten( ret, pars );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg instanceof Double ) {
            return format( (Double) arg, pars, soFar );
        } else {
            return format( ((Number) arg).doubleValue(), pars, soFar );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        try {
            Double ret = Double.parseDouble( rawString );

            return ret;

        } catch( NumberFormatException ex ) {
            throw new StringifierParseException( this, rawString, ex );
        }
    }
}
