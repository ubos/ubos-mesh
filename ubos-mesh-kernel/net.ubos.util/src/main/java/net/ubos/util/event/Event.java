//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.event;

/**
 * <p>Any kind of Event.
 * <p>Similarly to <code>java.util.EventObject</code>, <code>Event</code>s
 *    has a source, of type <code>S</code>. The <code>DeltaValue</code> property
 *    holds the difference that the event indicates, which is of type <code>T</code>.
 *
 * @param <S> the type of the event source
 * @param <V> the type of the value
 */
public interface Event<S,V>
{
    /**
     * Obtain the source of the event.
     *
     * @return the source of the event
     */
    public S getSource();

    /**
     * Obtain the delta value of the data item whose change triggered the event.
     *
     * @return the delta value
     */
    public V getDeltaValue();
}

