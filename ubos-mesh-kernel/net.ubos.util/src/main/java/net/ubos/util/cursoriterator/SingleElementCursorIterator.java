//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.function.BiPredicate;
import net.ubos.util.ArrayHelper;

/**
 * This CursorIterator returns only a single element.
 *
 * @param <T> the type of element to iterate over
 */
public final class SingleElementCursorIterator<T>
        extends
            ArrayCursorIterator<T>
{
    /**
     * Factory method.
     *
     * @param single the single element to iterate over
     * @return the created SingleElementCursorIterator
     * @param <T> the type of element to iterate over
     */
    @SuppressWarnings("unchecked")
    public static <T> SingleElementCursorIterator<T> create(
            T single )
    {
        T [] array = (T []) ArrayHelper.createArray( single.getClass(), 1 );
        array[0] = single;

        return new SingleElementCursorIterator<>(
                array,
                0,
                new DEFAULT_EQUALS<>() );
    }

    /**
     * Constructor.
     *
     * @param array the array to iterate over
     * @param startPosition the start position
     * @param equals the equality operation for E's
     */
    protected SingleElementCursorIterator(
            T []             array,
            int              startPosition,
            BiPredicate<T,T> equals )
    {
        super( array, startPosition, 0, 1, equals );
    }
}
