//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.nameserver;

import net.ubos.util.logging.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CompositeCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;

/**
 * A very simple, in-memory only implementation of {@link NameServer}. This NameServer
 * keeps mappings locally, and if not found, it will delegate to (optional) delegate
 * NameServers. This is for subclasses; use MNameServer to instantiate directly.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 */
public abstract class AbstractMNameServer<K,V>
        implements
            WritableNameServer<K,V>
{
    private static final Log log = Log.getLogInstance( AbstractMNameServer.class ); // our own, private logger

    /**
     * {@inheritDoc]
     */
    @Override
    public V get(
            K key )
    {
        V ret = null;
        try {
            synchronized( this ) {
                ret = theLocalMappings.get( key );
                if( ret != null ) {
                    return ret;
                }
                if( theDelegates != null ) {
                    for( NameServer<K,? extends V> current : theDelegates ) {
                        ret = current.get( key );
                        if( ret != null ) {
                            return ret;
                        }
                    }
                }
                return ret;
            }

        } finally {
            if( log.isDebugEnabled() ) {
                log.debug( this + ".get( " + key + " ) -> " + ret );
            }
        }
    }

    /**
     * Perform a get, but do not consult delegates.
     *
     * @param key the key
     * @return the value
     */
    public synchronized V getLocal(
            K key )
    {
        V ret = theLocalMappings.get( key );
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        CursorIterator<K> ret;

        if( theDelegates != null && !theDelegates.isEmpty() ) {
            ArrayList<CursorIterator<K>> delegateIters = new ArrayList<>( theDelegates.size() + 1 );
            delegateIters.add( MapCursorIterator.createForKeys( theLocalMappings ));

            for( NameServer<K,? extends V> current : theDelegates ) {
                delegateIters.add( current.keyIterator() );
            }
            ret = CompositeCursorIterator.create( delegateIters );

        } else {
            ret = MapCursorIterator.createForKeys( theLocalMappings );
        }
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public CursorIterator<V> valueIterator()
    {
        return new MappingCursorIterator<>(
                keyIterator(),
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public V mapDelegateValueToHere(
                            K delegateValue )
                    {
                        return get( delegateValue );
                    }

                    @Override
                    public K mapHereToDelegateValue(
                            V value )
                    {
                        throw new UnsupportedOperationException( "Should not be invoked" );
                    }
                });
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void putIgnorePrevious(
            K key,
            V value )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "putIgnorePrevious", key, value );
        }
        synchronized( this ) {
            theLocalMappings.put( key, value );
        }
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V put(
            K key,
            V value )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "put", key, value );
        }
        synchronized( this ) {
            return theLocalMappings.put( key, value );
        }
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V remove(
            K key )
    {
        return remove( key, null );
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public synchronized V remove(
            K                key,
            Function<V,Void> cleanupCode )
    {
        V ret = theLocalMappings.remove( key );

        if( cleanupCode != null ) {
            cleanupCode.apply( ret );
        }

        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void removeIgnorePrevious(
            K key )
    {
        remove( key, null );
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public synchronized boolean isEmpty()
    {
        if( !theLocalMappings.isEmpty() ) {
            return false;
        }
        if( theDelegates != null ) {
            for( NameServer<K,? extends V> current : theDelegates ) {
                if( !current.isEmpty() ) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public synchronized int size()
    {
        int ret = theLocalMappings.size();

        if( theDelegates != null ) {
            for( NameServer<K,? extends V> current : theDelegates ) {
                ret += current.size();
            }
        }
        return ret;
    }

    /**
     * Add a delegate.
     *
     * @param newDelegate the new delegate
     */
    public synchronized void addDelegate(
            NameServer<K,? extends V> newDelegate )
    {
        if( theDelegates == null ) {
            theDelegates = new ArrayList<>();
        }
        theDelegates.add( newDelegate );
    }

    /**
     * Remove a delegate.
     *
     * @param oldDelegate the old delegate
     */
    public synchronized void removedDelegate(
            NameServer<K,? extends V> oldDelegate )
    {
        theDelegates.remove( oldDelegate );
    }

    /**
     * Iterator over the delegates.
     *
     * @return the CursorIterator
     */
    public CursorIterator<NameServer<K,? extends V>> delegateIterator()
    {
        if( theDelegates == null || theDelegates.isEmpty() ) {
            return ZeroElementCursorIterator.create();
        } else {
            return ArrayListCursorIterator.create( theDelegates );
        }
    }

    /**
     * The set of local mappings.
     */
    protected final HashMap<K,V> theLocalMappings = new HashMap<>();

    /**
     * The list of delegate NameServers, in sequence.
     */
    protected ArrayList<NameServer<K,? extends V>> theDelegates = null;
}
