//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator.history;

import java.util.function.BiPredicate;
import net.ubos.util.cursoriterator.AbstractReadManyCursorIterator;
import net.ubos.util.history.AbstractHistory;
import net.ubos.util.history.HasTimeUpdated;

/**
 * Factors out functionality common to implementations of HistoryCursorIterator.
 *
 * @param <E> the type of thing whose history it is
 */
public abstract class AbstractFetchManyHistoryCursorIterator<E extends HasTimeUpdated>
        extends
            AbstractReadManyCursorIterator<E>
        implements
            HistoryCursorIterator<E>
{
    /**
     * Constructor.
     *
     * @param history the History over which we iterate
     * @param equals the equality operation for E's
     */
    protected AbstractFetchManyHistoryCursorIterator(
            AbstractHistory<E> history,
            BiPredicate<E,E>   equals )
    {
        super( equals );

        theHistory = history;
    }

    /**
     * Obtain the History over which this iterates.
     *
     * @return the History
     */
    public AbstractHistory<E> getHistory()
    {
        return theHistory;
    }

    /**
     * The History over which we iterate.
     */
    protected final AbstractHistory<E> theHistory;
}
