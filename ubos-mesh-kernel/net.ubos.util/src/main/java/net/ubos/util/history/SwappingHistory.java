//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history;

/**
/**
 * A History that swaps its content in and out.
 *
 * @param <E> the type of thing for which this is a history
 */
public interface SwappingHistory<E extends HasTimeUpdated>
    extends
        History<E>
{
    /**
     * If this History uses a local cache, clear it.
     * This is used as test instrumentation.
     */
    public abstract void clearLocalCache();
}
