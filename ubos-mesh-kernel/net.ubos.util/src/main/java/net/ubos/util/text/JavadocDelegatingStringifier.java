//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * A DelegatingStringifier that escapes star-slash (the Java end-of-comment indicator)
 * so emitted Html is safe to be used in Javadoc.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class JavadocDelegatingStringifier<T>
        extends
            AbstractDelegatingStringifier<T>
{
    /**
     * Factory method.
     *
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     * @return the created JavadocDelegatingStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> JavadocDelegatingStringifier<T> create(
            Stringifier<T> delegate )
    {
        return new JavadocDelegatingStringifier<>( delegate );
    }

    /**
     * Constructor. Use factory method.
     *
     * @param delegate the underlying Stringifier that knows how to deal with the real type
     */
    protected JavadocDelegatingStringifier(
            Stringifier<T> delegate )
    {
        super( delegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String escape(
            String s )
    {
        String ret = s.replaceAll( "\\*/", "&#42;/" );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String unescape(
            String s )
    {
        String ret = s.replaceAll( "&#42;/", "\\*/" );
        return ret;
    }
}
