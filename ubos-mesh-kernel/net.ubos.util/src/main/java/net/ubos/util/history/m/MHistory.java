//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history.m;

import java.util.ArrayList;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.history.AbstractHistory;
import net.ubos.util.history.HasTimeUpdated;
import net.ubos.util.logging.Log;

/**
 * An in-memory implementation of History.
 *
 * @param <E> the type of thing for which this is a history
 */
public class MHistory<E extends HasTimeUpdated>
    extends
        AbstractHistory<E>
{
    private static final Log log = Log.getLogInstance( MHistory.class );

    /**
     * Factory method.
     *
     * @return the created object
     * @param <E> the type of thing for which this is a history
     */
    public static <E extends HasTimeUpdated> MHistory<E> create()
    {
        return new MHistory<>();
    }

    /**
     * Constructor.
     */
    protected MHistory()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        theStorage.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            E toAdd )
    {
        if( toAdd == null ) {
            throw new NullPointerException();
        }

        long when = toAdd.getTimeUpdated();
        for( int i=0 ; i<theStorage.size() ; ++i ) {
            long current = theStorage.get( i ).getTimeUpdated();

            if( when == current ) {
                theStorage.set( i, toAdd );
                return;
            } else if( when < current ) {
                theStorage.add( i, toAdd );
                return;
            }
        }
        theStorage.add( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            long t )
    {
        for( int i=0 ; i<theStorage.size() ; ++i ) {
            if( t == theStorage.get( i ).getTimeUpdated() ) {
                theStorage.remove( i );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<E> iterator()
    {
        return new MHistoryCursorIterator<>( this, 0 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E at(
            long t )
    {
        for( int i=0 ; i<theStorage.size() ; ++i ) {
            if( t == theStorage.get( i ).getTimeUpdated() ) {
                return theStorage.get( i );
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            E toAdd )
        throws
            IllegalArgumentException
    {
        E already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same element:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "Element exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * The storage for the history, in sequence, from oldest to newest.
     */
    protected final ArrayList<E> theStorage = new ArrayList<>();
}
