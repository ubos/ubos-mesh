//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.ArrayFacade;

/**
 * Abstract superclass for compound Stringifiers that delegate to N child Stringifiers.
 * By default, this class assumes that the child Stringifiers are ordered, and that the
 * result of a {@link #format format} operation is a concatenation of the child
 * Stringifiers' results of their respective {@link #format format} operation.
 */
public abstract class CompoundStringifier
        implements
            Stringifier<ArrayFacade<Object>>
{
    /**
     * Constructor, for subclasses only. This does not invoke {@link #compile compile}.
     */
    protected CompoundStringifier()
    {
        // no op
    }

    /**
     * Compile the compound expression into child Stringifiers.
     *
     * @throws CompoundStringifierCompileException thrown if the compilation failed.
     */
    protected synchronized final void compile()
        throws
            CompoundStringifierCompileException
    {
        if( theComponents == null ) {
            theComponents = compileIntoComponents();
        }
    }

    /**
     * Determine the components of this CompoundStringifier.
     *
     * @return the components of this CompoundStringifier
     * @throws CompoundStringifierCompileException thrown if the compilation failed.
     */
    protected abstract CompoundStringifierComponent [] compileIntoComponents()
        throws
            CompoundStringifierCompileException;

    /**
     * Obtain the components of this CompoundStringifier.
     *
     * @return the child components
     */
    public final CompoundStringifierComponent [] getMessageComponents()
    {
        return theComponents;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            ArrayFacade<Object>   arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        StringBuilder ret = new StringBuilder();
        for( int i=0 ; i<theComponents.length ; ++i ) {
            CompoundStringifierComponent current  = theComponents[i];
            String                       soFarArg = soFar != null ? soFar + ret.toString() : ret.toString();

            String found = current.format( arg, pars, soFarArg ); // presumably shorter, but we don't know

            ret.append( found );
        }
        // this should probably invoke:
        // return StringHelper.potentiallyShorten( ret.toString(), maxLength );
        // but we don't do this, as it arbitrarily cuts out HTML. We could subclass this class
        // and define different potentiallyShorten methods for Plain and HTML (and perhaps others),
        // which would be instantiated by different subclasses of StringRepresentation.
        // For right now, this is more complicated than needed. FIXME?
        return ret.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException
    {
        return format( (ArrayFacade<Object>) arg, pars, soFar );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayFacade<Object> unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        Iterator<StringifierParsingChoice<ArrayFacade<Object>>> iter  = allParsingChoiceIterator( rawString, 0, rawString.length(), 1, factory ); // only need one
        StringifierParsingChoice<ArrayFacade<Object>>           found = null;

        while( iter.hasNext() ) {
            StringifierParsingChoice<ArrayFacade<Object>> choice = iter.next();

            if( choice.getEndIndex() == rawString.length() ) {
                // found
                found = choice;
                break;
            }
        }
        if( found == null ) {
            throw new StringifierParseException( this, rawString );
        }

        int max = 0;
        for( int i = 0 ; i<theComponents.length ; ++i ) {
            if( theComponents[i] instanceof CompoundStringifierPlaceholder ) {
                CompoundStringifierPlaceholder pl = (CompoundStringifierPlaceholder) theComponents[i];
                if( pl.getPlaceholderIndex() > max ) {
                    max = pl.getPlaceholderIndex();
                }
            }
        }

        ArrayFacade<Object> ret = ArrayFacade.create( max+1 );
        Object [] values = found.unformat().getArray();

        for( int i = 0 ; i<theComponents.length ; ++i ) {
            if( theComponents[i] instanceof CompoundStringifierPlaceholder ) {
                CompoundStringifierPlaceholder pl = (CompoundStringifierPlaceholder) theComponents[i];
                ret.put( pl.getPlaceholderIndex(), values[i] );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<ArrayFacade<Object>>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        CompoundStringifierIterator ret = new CompoundStringifierIterator(
                this,
                theComponents,
                rawString,
                startIndex,
                endIndex,
                max,
                true,
                factory );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<ArrayFacade<Object>>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        CompoundStringifierIterator ret = new CompoundStringifierIterator(
                this,
                theComponents,
                rawString,
                startIndex,
                endIndex,
                max,
                false,
                factory );

        return ret;
    }

    /**
     * Factory method to instantiate an array of T, given the childrens' choices.
     *
     * @param childChoices the childrens' StringifierParsingChoices
     * @return this CompoundStringifier's choice
     */
    protected abstract ArrayFacade<Object> compoundUnformat(
            StringifierParsingChoice [] childChoices );

    /**
     * The components in the CompoundStringifier.
     */
    protected CompoundStringifierComponent [] theComponents;
}
