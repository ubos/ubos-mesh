//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import java.util.Objects;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;

/**
 * Collects functionality common to many StringifierParsingChoiceIterators.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class DefaultStringifierParsingChoiceIterator<T>
    implements
        Iterator<StringifierParsingChoice<T>>,
        CanBeDumped
{
    private static final Log log = Log.getLogInstance( DefaultStringifierParsingChoiceIterator.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param max the maximum number of choices to be returned by the Iterator.
     * @param factory the factory needed to create the parsed values, if any
     * @param parent the Stringifier on whose behalf the parsing is performed.
     * @return the created iterator
     *
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> DefaultStringifierParsingChoiceIterator<T> create(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory,
            Stringifier<T>             parent )
    {
        return new DefaultStringifierParsingChoiceIterator<>( rawString, startIndex, endIndex, max, factory, parent );
    }

    /**
     * Constructor for subclasses only, use factory method.
     *
     * @param rawString the String to parse
     * @param startIndex the position at which to parse rawString
     * @param endIndex the position at which to end parsing rawString
     * @param max the maximum number of choices to be returned by the Iterator.
     * @param factory the factory needed to create the parsed values, if any
     * @param parent the Stringifier on whose behalf the parsing is performed.
     */
    protected DefaultStringifierParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory,
            Stringifier<T>             parent )
    {
        theRawString = rawString;
        theStartIndex = startIndex;
        theEndIndex   = endIndex;
        theMax        = max;
        theFactory    = factory;
        theParent     = parent;

        theCounter      = 0;
        theCurrentIndex = theStartIndex;

        goNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return theNext != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringifierParsingChoice<T> next()
    {
        StringifierParsingChoice<T> ret;
        if( theNext != null ) {
            ret = theNext;
            goNext();
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
        throws
            UnsupportedOperationException
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    protected void goNext()
    {
        log.traceMethodCallEntry( this, "goNext", this );

        if( theCounter < theMax ) {
            // try until one works
            while( theCurrentIndex <= theEndIndex ) {
                try {
                    T found = theParent.unformat( theRawString.substring( theStartIndex, theCurrentIndex ), theFactory );
                    T previous = ( theNext == null ) ? null : theNext.unformat();

                    if( Objects.equals( found, previous )) {
                        // skip duplicates, e.g. both "1" and "1." return 1.0d
                        ++theCurrentIndex;

                    } else {
                        theNext = new StringifierValueParsingChoice<>( theStartIndex, theCurrentIndex, found );
                        ++theCounter;
                        ++theCurrentIndex;

                        log.traceMethodCallExit( this, "goNext", this );
                        return;
                    }

                } catch( StringifierParseException ex ) {
                    ++theCurrentIndex;
                }
            }
            theNext = null;
        } else {
            theNext = null;
        }
        log.traceMethodCallExit( this, "goNext -- end", this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "rawString",
                    "startIndex",
                    "endIndex",
                    "max",
                    "counter",
                    "currentIndex",
                    "next"
                },
                new Object[] {
                    theRawString,
                    theStartIndex,
                    theEndIndex,
                    theMax,
                    theCounter,
                    theCurrentIndex,
                    theNext
                });
    }

    /**
     * The String to be parsed.
     */
    protected final String theRawString;

    /**
     * WHere to start parsing.
     */
    protected final int theStartIndex;

    /**
     * Where to end parsing.
     */
    protected final int theEndIndex;

    /**
     * Maximum number of elements to return.
     */
    protected final int theMax;

    /**
     * The factory for unformatting.
     */
    protected final StringifierUnformatFactory theFactory;

    /**
     * The Stringifier on whose behalf we do the parsing.
     */
    protected final Stringifier<T> theParent;

    /**
     * Number of elements returned so far.
     */
    protected int theCounter;

    /**
     * The current parsing location.
     */
    protected int theCurrentIndex;

    /**
     * The next element to return.
     */
    protected StringifierParsingChoice<T> theNext;
}
