//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap;

/**
 * A SmartMap whose keys remain ordered.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public interface OrderedSmartMap<K,V>
    extends
        SmartMap<K,V>
{}
