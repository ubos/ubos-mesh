//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.l10;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Locale;
import net.ubos.util.NameValuePair;

/**
 * This is a in implementation of a map between Locale and values of a certain type. It is
 * used to obtain Locale-specific values, such as names and descriptions. It provides
 * methods to obtain the most specific value given for a provided Locale, and
 * failing that, obtains less specific value. For example, if the desired Locale
 * is en-US, it first attempts to find a value for en-US, and failing that, for
 * Locale en. If none of those is found, a global default value is returned.
 * 
 * @param <T> the type of value
 */
public interface L10Map<T>
        extends
            Serializable
{
    /**
     * Determine the number of entries in this L10Map.
     *
     * @return the number of entries in this L10Map
     */
    public int size();

    /**
     * Returns true if this map is empty.
     *
     * @return true of this map is empty
     */
    public boolean isEmpty();

    /**
     * Obtains the value to which this map maps the provided Locale. The implementation
     * must first look for the exact Locale, and return that if it is found. If a
     * value does not exist, the implementation looks for a less and less specific
     * Locale (e.g. from en-US to en). If all fails, the default value is returned.
     *
     * @param l the Locale for which a value shall be determined
     * @return the PropertyValue for this Locale
     */
    public T get(
            Locale l );

    /**
     * Obtains the value to which this map maps the provided Locale. The implementation
     * must first look for the exact Locale, and return that if it is found. If a
     * value does not exist, the implementation looks for a less and less specific
     * Locale (e.g. from en-US to en). If all fails, the default value is returned.
     *
     * @param l the Locale for which a value shall be determined
     * @return the PropertyValue for this Locale
     */
    public T get(
            String l );

    /**
     * Obtains the value for this exact Locale. If a value for this exact Locale does
     * not exist, returns null. It does not attempt to find a default value using a
     * less-specific Locale.
     *
     * @param l the Locale for which a value shall be determined
     * @return the PropertyValue for this Locale
     */
    public T getExact(
            Locale l );

    /**
     * Obtains the value for this exact Locale. If a value for this exact Locale does
     * not exist, returns null. It does not attempt to find a default value using a
     * less-specific Locale.
     *
     * @param l the Locale for which a value shall be determined
     * @return the PropertyValue for this Locale
     */
    public T getExact(
            String l );

    /**
     * Obtain the default value regardless of Locale.
     *
     * @return the default value
     */
    public T getDefault();

    /**
     * Obtain an Iterator over all keys (Locales) known by this L10Map.
     *
     * @return an Iterator over all keys (Locales) known by this L10Map
     */
    public Iterator<String> keyIterator();

    /**
     * Obtain an Iterator over all mappings known by this L10Map.
     * JSP / JavaBeans-friendly version.
     *
     * @return an Iterator over all keys (Locales) known by this L10Map
     */
    public Iterator<NameValuePair<T>> getPairIterator();
}
