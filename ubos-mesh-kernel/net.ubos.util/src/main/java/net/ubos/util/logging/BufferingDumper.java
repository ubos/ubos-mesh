//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

/**
 * Supported by Dumpers that buffer the dumped content.
 */
public interface BufferingDumper
        extends
            Dumper
{
    /**
     * Reset the internal buffer.
     */
    public void reset();

    /**
     * Obtain the result.
     *
     * @return the result
     */
    public String getBuffer();
}
