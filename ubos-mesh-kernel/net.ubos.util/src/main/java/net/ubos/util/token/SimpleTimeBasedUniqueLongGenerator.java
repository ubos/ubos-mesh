//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.token;

/**
 * Uses a very simple, time-based algorithm to create unique longs.
 */
public class SimpleTimeBasedUniqueLongGenerator
    implements
        UniqueTokenGenerator<Long>
{
    /**
     * Factory method.
     *
     * @return the created UniqueIdentifierCreator
     */
    public static SimpleTimeBasedUniqueLongGenerator create()
    {
        return new SimpleTimeBasedUniqueLongGenerator();
    }
    
    /**
     * Constructor, for subclasses only.
     */
    protected SimpleTimeBasedUniqueLongGenerator()
    {
        // noop
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Long createUniqueToken()
    {
        long currentDate = System.currentTimeMillis();
        if( currentDate > theMostRecent ) {
            theMostRecent = currentDate;
        } else {
            ++theMostRecent;
        }
        return theMostRecent;
    }

    /**
     * The most recently returned unique identifier.
     */
    protected long theMostRecent = 0;
}
