//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.StringHelper;
import net.ubos.util.cursoriterator.SingleElementCursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;

/**
 * Collects functionality common to Stringifier implementations.
 *
 * @param <T> the type of the Objects to be stringified
 */
public abstract class AbstractStringifier<T>
        implements
            Stringifier<T>
{
    /**
     * Helper method to potentially shorten output based on a parameter
     * potentially contained in the StringRepresentationParameters.
     *
     * @param s the String potentially to be shortened
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the potentially shortened String
     */
    protected String potentiallyShorten(
            String                s,
            StringifierParameters pars )
    {
        if( pars == null ) {
            return s;
        }
        Number n = (Number) pars.get(StringifierParameters.MAX_LENGTH_KEY );
        if( n == null ) {
            return s;
        }
        String ret = StringHelper.potentiallyShorten( s, n.intValue() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<T>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        try {
            T found = unformat( rawString.substring( startIndex, endIndex ), factory );

            return SingleElementCursorIterator.create(
                    new StringifierValueParsingChoice<>( startIndex, endIndex, found ));

        } catch( StringifierParseException | UnsupportedOperationException ex ) {
            return ZeroElementCursorIterator.create();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<StringifierParsingChoice<T>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        return DefaultStringifierParsingChoiceIterator.create( rawString, startIndex, endIndex, max, factory, this );
    }
}
