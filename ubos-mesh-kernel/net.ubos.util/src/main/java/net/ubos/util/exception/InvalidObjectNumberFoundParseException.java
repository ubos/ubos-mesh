//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * An ParseException indicating that the while parsing succeeded, the wrong number
 * of objects were found.
 */
public class InvalidObjectNumberFoundParseException
    extends
        LocalizedParseException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param string the text that could not be parsed
     * @param rightNumber the number of objects expected
     * @param found the found objects
     */
    public InvalidObjectNumberFoundParseException(
            String    string,
            int       rightNumber,
            Object [] found )
    {
        super( string, null, 0, null );

        theRightNumber = rightNumber;
        theFound       = found;
    }

    /**
     * Obtain the right number of objects that should have been found.
     *
     * @return the right number of objects
     */
    public int getRightNumber()
    {
        return theRightNumber;
    }

    /**
     * Obtain the found objects.
     *
     * @return the found objects
     */
    public Object [] getFound()
    {
        return theFound;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theString, theRightNumber, theFound.length, theFound };
    }

    /**
     * The right number of objects that should have been found.
     */
    protected final int theRightNumber;

    /**
     * The found objects.
     */
    protected final Object [] theFound;
}
