//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Useful File-related utility functions.
 */
public abstract class FileUtils
{
    /**
     * Private constructor to keep this abstract.
     */
    private FileUtils()
    {}

    /**
     * Helper to determine the extension of a File.
     *
     * @param file the File
     * @return the extension, or null
     */
    public static String extensionOf(
            File file )
    {
        return extensionOf( file.getName() );
    }

    /**
     * Helper to determine the extension in a file name.
     *
     * @param name the file name
     * @return the extension, or null
     */
    public static String extensionOf(
            String name )
    {
        int lastDot = name.lastIndexOf( '.' );

        String ret;
        if( lastDot >= 0 ) {
            ret = name.substring( lastDot+1 ).toLowerCase();
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * Determine the path of the second file relative to the first file.
     * The first file must be a directory, and the second file must be inside that directory.
     *
     * @param parent the first file
     * @param child the second file
     * @return the relative path
     * @throws IllegalArgumentException thrown if the second file is not contained in the first directory
     */
    public static String relativePath(
            File parent,
            File child )
    {
        String parentPath = parent.getAbsolutePath();
        String childPath  = child.getAbsolutePath();

        if( parentPath.equals( childPath )) {
            return "";
        }

        if( !parentPath.endsWith( "/" )) {
            parentPath += "/";
        }

        if( childPath.startsWith( parentPath )) {
            return childPath.substring( parentPath.length());
        } else {
            throw new IllegalArgumentException( "Child not within parent path: " + parent + " vs " + child );
        }
    }

    /**
     * Determine the list of all files and directories inside a directory.
     *
     * @param dir the directory
     * @return list of all files and directories
     */
    public static File [] findFiles(
            File dir )
    {
        return findFiles( dir, (File file) -> true );
    }

    /**
     * Determine the list of all files and directories matching a Predicate inside a directory.
     *
     * @param dir the directory
     * @param candidatePred the predicate for whether to include a candidate File
     * @return list of all files and directories
     */
    public static File [] findFiles(
            File            dir,
            Predicate<File> candidatePred )
    {
        if( dir.isDirectory() ) {
            File [] found = dir.listFiles( (File f) -> candidatePred.test( f ) );
            return found;

        } else {
            throw new IllegalArgumentException( "Not a directory:" + dir.getAbsolutePath() );
        }
    }

    /**
     * Determine the list of all files and directories down from a start directory.
     *
     * @param start starting directory
     * @return list of all files and directories
     */
    public static File [] recursivelyFindFiles(
            File start )
    {
        return recursivelyFindFiles( start, (File file) -> true, (File dir) -> true );
    }

    /**
     * Determine the list of all files and directories matching a predicate recursively down
     * from a start directory.
     *
     * @param start starting directory
     * @param candidatePred the predicate for whether to include a candidate File
     * @return list of all files
     */
    public static File [] recursivelyFindFiles(
            File            start,
            Predicate<File> candidatePred )
    {
        return recursivelyFindFiles( start, candidatePred, (File dir) -> true );
    }

    /**
     * Determine the list of all file and directory matching a predicate recursively down
     * from a start directory, and only visiting directories that match another predicate.
     *
     * @param start starting directory
     * @param candidatePred the predicate for whether to include a candidate File
     * @param enterDirPred the predicate for whether to travel into an encountered directory
     * @return list of all files
     */
    public static File [] recursivelyFindFiles(
            File            start,
            Predicate<File> candidatePred,
            Predicate<File> enterDirPred )
    {
        ArrayList<File> ret = new ArrayList<>();

        _recursivelyFindFiles( start, candidatePred, enterDirPred, ret );

        return ArrayHelper.copyIntoNewArray( ret, File.class );
    }

    /**
     * The internal recursive method to look for files.
     *
     * @param start starting directory
     * @param candidatePred the predicate for whether to include a candidate File
     * @param enterDirPred the predicate for whether to travel into an encountered directory
     * @param results add results here
     */
    private static void _recursivelyFindFiles(
            File            start,
            Predicate<File> candidatePred,
            Predicate<File> enterDirPred,
            List<File>      results )
    {
        if( start.isDirectory() ) {
            if( enterDirPred.test( start )) {
                File [] found = start.listFiles();
                for( File f : found ) {
                    if( candidatePred.test( f )) {
                        results.add( f );
                    }
                }
                for( File f : found ) {
                    if( f.isDirectory() && enterDirPred.test( f ) ) {
                        _recursivelyFindFiles( f, candidatePred, enterDirPred, results );
                    }
                }
            }
        } else {
            throw new IllegalArgumentException( "Not a directory:" + start.getAbsolutePath() );
        }
    }
}
