//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Thrown if parsing a String by a Stringifier failed.
 */
public class StringifierParseException
        extends
            StringifierException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param source the Stringifier that raised this exception
     * @param s the String that could not be parsed
     */
    public StringifierParseException(
            Stringifier<?> source,
            String         s )
    {
        super( source );

        theString = s;
    }

    /**
     * Constructor.
     *
     * @param source the Stringifier that raised this exception
     * @param s the String that could not be parsed
     * @param cause the underlying cause of the Exception
     */
    public StringifierParseException(
            Stringifier source,
            String      s,
            Throwable   cause )
    {
        super( source, cause );

        theString = s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theSource, theString };
    }

    /**
     * The String that could not be parsed.
     */
    protected final String theString;
}
