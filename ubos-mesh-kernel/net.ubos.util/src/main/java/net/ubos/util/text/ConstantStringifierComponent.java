//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.ArrayFacade;
import net.ubos.util.cursoriterator.SingleElementCursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;

/**
 * The constant blocks of text inside a (compound) MessageStringifier.
 */
public class ConstantStringifierComponent
        implements
            CompoundStringifierComponent
{
    /**
     * Constructor.
     *
     * @param s the constant String
     */
    public ConstantStringifierComponent(
            String s )
    {
        theString = s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            ArrayFacade<Object>   arg,
            StringifierParameters pars,
            String                soFar )
    {
        // regardless of argument, we always return the same -- this is a constant after all
        return theString;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<? extends StringifierParsingChoice<Object>> allParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        if( rawString.regionMatches( startIndex, theString, 0, theString.length()) && endIndex == rawString.length()) {
            return SingleElementCursorIterator.<StringifierParsingChoice<Object>>create(
                    new StringifierParsingChoice<Object>( startIndex, startIndex + theString.length() ) {
                        @Override
                        public Object unformat() {
                            // this doesn't return any value
                            return null;
                        }
                    }
                );
        } else {
            return ZeroElementCursorIterator.<StringifierParsingChoice<Object>>create();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<? extends StringifierParsingChoice<Object>> partialParsingChoiceIterator(
            String                     rawString,
            int                        startIndex,
            int                        endIndex,
            int                        max,
            StringifierUnformatFactory factory )
    {
        if( rawString.regionMatches( startIndex, theString, 0, theString.length() )) {
            return SingleElementCursorIterator.<StringifierParsingChoice<Object>>create(
                    new StringifierParsingChoice<Object>( startIndex, startIndex + theString.length() ) {
                        @Override
                        public Object unformat() {
                            // this doesn't return any value
                            return null;
                        }
                    }
                );
        } else {
            return ZeroElementCursorIterator.<StringifierParsingChoice<Object>>create();
        }
    }

    /**
     * The constant String.
     */
    protected final String theString;
}
