//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

/**
 * Helper class that instantiates an object when needed, but only once.
 *
 * @param <T> the type of instantiated object
 */
public abstract class CreateWhenNeeded<T>
{
    /**
     * Smartly create the object. The first time this is invoked, the object
     * will be instantiated. All subsequent times, the same instance will
     * be returned.
     *
     * @return the instantiated object
     * @throws CreateWhenNeededException wrapper exception if something went wrong; the original exception is carried as the cause
     */
    public synchronized T obtain()
        throws
            CreateWhenNeededException
    {
        if( theObject == null ) {
            try {
                theObject = instantiate();

            } catch( Throwable t ) {
                throw new CreateWhenNeededException( this, t );
            }
        }
        return theObject;
    }

    /**
     * Determine whether the object has indeed been instantiated so far.
     *
     * @return true the object has been instantiated
     */
    public boolean hasBeenCreated()
    {
        return theObject != null;
    }

    /**
     * Instantiation method.
     *
     * @return the instantiated object
     * @throws Throwable a problem occurred
     */
    protected abstract T instantiate()
        throws
            Throwable;

    /**
     * The instance, once it is allocated.
     */
    private T theObject;
}
