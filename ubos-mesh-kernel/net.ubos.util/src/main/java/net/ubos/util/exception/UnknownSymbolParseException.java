//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

/**
 * A ParseException indicating that the String contained an unknown symbol.
 */
public class UnknownSymbolParseException
    extends
        LocalizedParseException
{
    /**
     * Constructor.
     *
     * @param string the text that could not be parsed
     * @param errorOffset the offset, into the string, where the error occurred
     * @param unknown the unknown symbol
     */
    public UnknownSymbolParseException(
            String string,
            int    errorOffset,
            String unknown )
    {
        super( string, null, errorOffset, null );

        theUnknown = unknown;
    }

    /**
     * Obtain the unknown symbol.
     *
     * @return the unknown symbol
     */
    public String getUnknown()
    {
        return theUnknown;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theString, theUnknown };
    }

    /**
     * The unknown symbol.
     */
    protected String theUnknown;
}
