//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import net.ubos.util.cursoriterator.CursorIterable;

/**
 * A set of objects with a value each, ordered by the ordering of this value.
 * 
 * @param <K> type of object
 * @param <V> type of value
 */
public interface OrderedSetWithValue<K,V>
    extends
        CursorIterable<K>
{
    /**
     * Obtain the size of the set.
     * 
     * @return the size
     */
    public int size();

    /**
     * Add an object and its corresponding value to the set.
     * 
     * @param obj the object
     * @param v the value
     * @return the previous value, if there was one
     */
    public V put(
            K obj,
            V v );
    
    /**
     * Obtain the value for this object.
     * 
     * @param obj the object
     * @return the value, or null if the MeshObject is not in the set
     */
    public V getValueFor(
            K obj );
    
    /**
     * Obtain the key of the first object, i.e. an object with the largest value.
     * There may be other objects with the same value.
     * 
     * @return the key, or null if the set is empty
     */
    public K getFirst();
    
    /**
     * Obtain the key of the last object, i.e. an object with the smallest value.
     * There may be other objects with the same value.
     * 
     * @return the key, or null if the set is empty
     */
    public K getLast();
    
    /**
     * Obtain the key of the nth object, i.e..
     * There may be other objects with the same value.
     * 
     * @param n the index
     * @return the key, or null if the set is empty
     */
    public K get(
            int n );
}
