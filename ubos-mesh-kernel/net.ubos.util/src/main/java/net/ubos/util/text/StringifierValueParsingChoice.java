//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * A simple implementation of StringifierParsingChoice that holds its own values.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class StringifierValueParsingChoice<T>
        extends
            StringifierParsingChoice<T>
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param startIndex the start index (inclusive) in the parsed String
     * @param endIndex the end index (exclusive) in the parsed String
     * @param value the value representing this parsing choice
     */
    public StringifierValueParsingChoice(
            int    startIndex,
            int    endIndex,
            T      value )
    {
        super( startIndex, endIndex );

        theValue = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T unformat()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( super.toString() );
        buf.append( "{ value: " );
        buf.append( theValue );
        buf.append( " }" );
        return buf.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theStartIndex",
                    "theEndIndex",
                    "theValue"
                },
                new Object[] {
                    theStartIndex,
                    theEndIndex,
                    theValue
                });
    }

    /**
     * The value of this choice.
     */
    protected final T theValue;
}
