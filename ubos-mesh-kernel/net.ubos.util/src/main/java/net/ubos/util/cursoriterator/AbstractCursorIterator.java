//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.BiPredicate;

/**
 * Factors out common behaviors of {@link CursorIterator}s. The default implementation
 * is rather inefficient, but works for all CursorIterators with little required work for
 * new implementations. Subclasses may want to override for performance reasons.
 *
 * @param <E> the type of element to iterate over
 */
public abstract class AbstractCursorIterator<E>
        implements
            CursorIterator<E>
{
    /**
     * Constructor.
     *
     * @param equals the equality operation for E's
     */
    protected AbstractCursorIterator(
            BiPredicate<E,E> equals )
    {
        theEquals = equals;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final E nextElement()
    {
        return next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekNext()
        throws
            NoSuchElementException
    {
        CursorIterator<E> clone = createCopy();
        E                 ret   = clone.next();

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekPrevious()
        throws
            NoSuchElementException
    {
        CursorIterator<E> clone = createCopy();
        E                 ret   = clone.previous();

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> peekNext(
            int n )
    {
        CursorIterator<E> clone = createCopy();
        List<E>           ret   = clone.next( n );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> peekPrevious(
            int n )
    {
        CursorIterator<E> clone = createCopy();
        List<E>           ret   = clone.previous( n );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return hasNext( 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        return hasPrevious( 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        CursorIterator<E> copy = createCopy();

        try {
            copy.moveBy( n );
            return true;
        } catch( NoSuchElementException ex ) {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        CursorIterator<E> copy = createCopy();

        try {
            copy.moveBy( -n );
            return true;
        } catch( NoSuchElementException ex ) {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        if( n > 0 ) {
            List<E> temp = next( n );
            if( temp.size() < n ) {
                throw new NoSuchElementException();
            }
        } else if( n < 0 ) {
            List<E> temp = previous( -n );
            if( temp.size() < -n ) {
                throw new NoSuchElementException();
            }
        } // else nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            E pos )
        throws
            NoSuchElementException
    {
        // by default, we have to search.

        CursorIterator<E> searchIter = createCopy();

        // search forward
        int count = 0;
        while( searchIter.hasNext() ) {
            E found = searchIter.peekNext();
            if( theEquals.test( pos, found )) {
                setPositionTo( searchIter );
                return count;
            }
            ++count;
            searchIter.next();
        }

        // search backward
        searchIter = createCopy();
        count = 0;
        while( searchIter.hasPrevious() ) {
            E found = searchIter.previous();
            --count;
            if( theEquals.test( pos, found )) {
                setPositionTo( searchIter );
                return count;
            }
        }

        // otherwise leave position unchanged
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            E pos )
        throws
            NoSuchElementException
    {
        // by default, we have to search.

        CursorIterator<E> currentPosition = createCopy();

        int count = 0;
        while( hasNext() ) {
            E found = next();
            ++count;
            if( theEquals.test( pos, found )) {
                return count;
            }
        }

        setPositionTo( currentPosition );

        count = 0;
        while( hasPrevious() ) {
            E found = peekPrevious();
            if( theEquals.test( pos, found )) {
                return count;
            }
            --count;
            previous();
        }
        throw new NoSuchElementException();
    }

    /**
     * We don't know how to remove on this level.
     *
     * @throws UnsupportedOperationException always thrown
     */
    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * The equality method to use in subclasses when comparing E with E.
     * This overrides E.equals().
     */
    protected final BiPredicate<E,E> theEquals;

    /**
     * Default equality to use. This can't be a static variable because of the <E> parameter.
     *
     * @param <E> type
     */
    public static class DEFAULT_EQUALS<E>
            implements
                BiPredicate<E,E>
    {
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean test(
                E e1,
                E e2 )
        {
            return e1 == null ? e1 == e2 : e1.equals( e2 );
        }
    }
}
