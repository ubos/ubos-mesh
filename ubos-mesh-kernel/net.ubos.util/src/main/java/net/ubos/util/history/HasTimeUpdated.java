//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history;

/**
 * This interface must be supported by objects inserted into a History.
 * It lets us determine the time when this version of the object in the History
 * was created/last updated.
 *
 * Requiring objects to support this interface poses a bit of a burden on
 * storing thing such as plain integers or Strings, because we need to
 * wrap it in <code>HashTimeUpdatedPair&lt;Integer>&gt;</code> or such, but
 * it makes the API much simpler.
 */
public interface HasTimeUpdated
{
    /**
     * Determine the time when the object was last updated.
     *
     * @return time in System.currentTimeMillis() format
     */
    public long getTimeUpdated();
}
