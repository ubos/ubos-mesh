//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history.m;

import java.util.NoSuchElementException;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.history.AbstractFetchOneHistoryCursorIterator;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.history.HasTimeUpdated;

/**
 * Implementation of HistoryCursorIterator for MHistory.
 *
 * @param <E> the type of element to iterate over
 */
public class MHistoryCursorIterator<E extends HasTimeUpdated>
    extends
        AbstractFetchOneHistoryCursorIterator<E>
    implements
        HistoryCursorIterator<E>
{
    /**
     * Constructor.
     *
     * @param history the MHistory object we iterate over
     * @param index the current position
     */
    public MHistoryCursorIterator(
            MHistory<E> history,
            int         index )
    {
        super( history, ( E one, E two ) -> one.getTimeUpdated() == two.getTimeUpdated() );

        theIndex = index;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MHistory<E> getHistory()
    {
        return (MHistory<E>) theHistory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        if( theIndex + n <= theHistory.getLength() ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        if( n <= theIndex ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
        throws
            NoSuchElementException
    {
        MHistory<E> realHistory = getHistory();

        if( theIndex < realHistory.theStorage.size() ) {
            return realHistory.theStorage.get( theIndex++ );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
            throws NoSuchElementException
    {
        MHistory<E> realHistory = getHistory();

        if( theIndex > 0 ) {
            return realHistory.theStorage.get( --theIndex );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret = theIndex;
        theIndex = 0;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        MHistory<E> realHistory = getHistory();

        int ret = realHistory.theStorage.size() - theIndex;
        theIndex = realHistory.theStorage.size();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException
    {
        MHistoryCursorIterator<E> realPosition = (MHistoryCursorIterator<E>) position;

        if( realPosition.theHistory != theHistory ) {
            throw new IllegalArgumentException( "Not working on the same MMeshObjectHistory" );
        }
        theIndex = realPosition.theIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToJustBeforeTime(
            long time )
    {
        MHistory<E> realHistory = getHistory();

        int ret;
        for( int i=0; i < realHistory.theStorage.size() ; ++i ) {
            if( time < realHistory.theStorage.get( i ).getTimeUpdated() ) {
                // i is now 1 step after where we need to be
                int oldIndex = theIndex;
                theIndex = i-1;
                if( theIndex < 0 ) {
                    theIndex = 0;
                }
                ret = theIndex - oldIndex;
                return ret;
            }
        }
        if( time == realHistory.theStorage.get( realHistory.theStorage.size()-1 ).getTimeUpdated() ) {
            ret = realHistory.theStorage.size() - 1 - theIndex;
            theIndex = realHistory.theStorage.size() - 1;

        } else {
            ret = realHistory.theStorage.size() - theIndex;
            theIndex = realHistory.theStorage.size();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToJustAfterTime(
            long time )
    {
        MHistory<E> realHistory = getHistory();

        int ret;
        for( int i=0; i < realHistory.theStorage.size() ; ++i ) {
            if( time < realHistory.theStorage.get( i ).getTimeUpdated()) {
                int oldIndex = theIndex;
                theIndex = i;
                ret = theIndex - oldIndex;
                return ret;
            }
        }
        ret = realHistory.theStorage.size() - theIndex;
        theIndex = realHistory.theStorage.size();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MHistoryCursorIterator<E> createCopy()
    {
        return new MHistoryCursorIterator<>( (MHistory<E>) theHistory, theIndex );
    }

    /**
     * The current position. This is the index into MMeshObjectHistory's theHistory
     */
    protected int theIndex;
}
