//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.ArrayFacade;
import net.ubos.util.DelegatingMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Knows how to emit messages in text based on format strings and parameters.
 * Modules can register new format string tags once they are activated.
 */
public final class DefaultMessageFormatter
{
    /**
     * Keep this in this class.
     *
     * @param name the name of the DefaultMessageFormatter, for debugging purposes
     * @param delegate the DefaultMessageFormatter to delegate to if no local entry was found
     */
    private DefaultMessageFormatter(
            String                  name,
            DefaultMessageFormatter delegate )
    {
        theName     = name;
        theDelegate = delegate;

        if( delegate != null ) {
            theDelegatingChildStringifierMap = DelegatingMap.create(
                    theChildStringifiers,
                    delegate.theDelegatingChildStringifierMap );
        } else {
            theDelegatingChildStringifierMap = theChildStringifiers;
        }
    }

    /**
     * Format a message.
     *
     * @param formatString the format string, such as "Your {0,string} items cost {1,currency}."
     * @param pars any formatting parameters
     * @param args the arguments to the message
     * @return the formatted String
     * @throws StringifierException
     */
    public String format(
            String                formatString,
            StringifierParameters pars,
            Object ...            args )
        throws
            StringifierException
    {
        AnyMessageStringifier s = AnyMessageStringifier.create( formatString, theDelegatingChildStringifierMap );
        return s.format( ArrayFacade.create( args ), pars, "" );
    }

    /**
     * Register child stringifiers.
     *
     * @param toAdd the child stringifiers and their tags to add
     * @throws IllegalArgumentException thrown if the tag exists already
     */
    public void registerChildStringifiers(
            Map<String,Stringifier<?>> toAdd )
    {
        for( String key : toAdd.keySet() ) {
            if( theChildStringifiers.containsKey( key )) {
                throw new IllegalArgumentException( "Cannot add child stringifier, exists already: " + key );
            }
        }
        theChildStringifiers.putAll( toAdd );
    }

    /**
     * Unregister child stringifiers.
     *
     * @param tagsToRemove the child stringifier tags to remove
     * @throws IllegalArgumentException thrown if the tag does not exist
     */
    public void unregisterChildStringifiers(
            Set<String> tagsToRemove )
    {
        for( String key : tagsToRemove ) {
            if( !theChildStringifiers.containsKey( key )) {
                throw new IllegalArgumentException( "Cannot remove child stringifier, does not exist: " + key );
            }
            theChildStringifiers.remove(key);
        }
    }

    /**
     * The singleton instance for text/plain, with the defaults.
     */
    public static final DefaultMessageFormatter TEXT_PLAIN = new DefaultMessageFormatter( "TEXT_PLAIN", null );
    static {
        TEXT_PLAIN.theChildStringifiers.put( "class",             ClassStringifier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "double",            DoubleStringifier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "escapequotestring", EscapeQuoteStringStringifier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "float",             FloatStringifier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "hex",               LongStringifier.createRadix( 16 ) );
        TEXT_PLAIN.theChildStringifiers.put( "hex2",              LongStringifier.createRadix(  2, 16 ) );
        TEXT_PLAIN.theChildStringifiers.put( "hex4",              LongStringifier.createRadix(  4, 16 ) );

        TEXT_PLAIN.theChildStringifiers.put( "int",               LongStringifier.createDecimal() );
        TEXT_PLAIN.theChildStringifiers.put( "int2",              LongStringifier.createDecimal( 2 ) );
        TEXT_PLAIN.theChildStringifiers.put( "int3",              LongStringifier.createDecimal( 3 ) );
        TEXT_PLAIN.theChildStringifiers.put( "int4",              LongStringifier.createDecimal( 4 ) );

        TEXT_PLAIN.theChildStringifiers.put( "intwithmultiplier", LongStringifierWithMultiplier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "htmlsource",        StringStringifier.create() );
        TEXT_PLAIN.theChildStringifiers.put( "htmltextarea",      StringStringifier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "list",              ListStringifier.create( ", ", StringStringifier.create() ));

        TEXT_PLAIN.theChildStringifiers.put( "stacktrace",        StacktraceStringifier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "string",            StringStringifier.create() );
        TEXT_PLAIN.theChildStringifiers.put( "stringarray",       ArrayStringifier.create( ", ", StringStringifier.create() ));

        TEXT_PLAIN.theChildStringifiers.put( "verbatim",          StringStringifier.create() );

        TEXT_PLAIN.theChildStringifiers.put( "urlappend",         AppendToUrlStringifier.create() );
        TEXT_PLAIN.theChildStringifiers.put( "urlargument",       ToValidUrlArgumentStringifier.create( StringStringifier.create() ) );
    }

    /**
     * The singleton instance for text/html, with the defaults.
     */
    public static final DefaultMessageFormatter TEXT_HTML = new DefaultMessageFormatter( "TEXT_HTML", TEXT_PLAIN );
    static {
        // only overrides need to be specified

        TEXT_HTML.theChildStringifiers.put( "class",             PrePostfixDelegatingStringifier.create( "<code>", "</code>", ClassStringifier.create() ));

        TEXT_HTML.theChildStringifiers.put( "htmlsource",        HtmlifyingDelegatingStringifier.create( StringStringifier.create() ));
        TEXT_HTML.theChildStringifiers.put( "htmltextarea",      HtmlifyingDelegatingStringifier.create( StringStringifier.create(), "textarea" ));

        TEXT_HTML.theChildStringifiers.put( "list",              ListStringifier.create( "<li>", "</li>\n<li>", "</li>", "", StringStringifier.create() ));

        TEXT_HTML.theChildStringifiers.put( "stacktrace",        HtmlifyingDelegatingStringifier.create( StacktraceStringifier.create() ));

        TEXT_HTML.theChildStringifiers.put( "string",            HtmlifyingDelegatingStringifier.create( StringStringifier.create() ));
        TEXT_HTML.theChildStringifiers.put( "stringarray",       ArrayStringifier.create( "<li>", "</li>\n<li>", "</li>", "", HtmlifyingDelegatingStringifier.create( StringStringifier.create() ) ));

        TEXT_HTML.theChildStringifiers.put( "urlappend",         HtmlifyingDelegatingStringifier.create( AppendToUrlStringifier.create() ));
        TEXT_HTML.theChildStringifiers.put( "urlargument",       HtmlifyingDelegatingStringifier.create( ToValidUrlArgumentStringifier.create( StringStringifier.create() )));
    }

    /**
     * The singleton instance for Javascript source code, with the defaults.
     */
    public static final DefaultMessageFormatter APPLICATION_JAVASCRIPT = new DefaultMessageFormatter( "APPLICATION_JAVASCRIPT", TEXT_PLAIN );

    static {
        // only overrides need to be specified

        APPLICATION_JAVASCRIPT.theChildStringifiers.put( "double",           JavaDoubleStringifier.create() );

        APPLICATION_JAVASCRIPT.theChildStringifiers.put( "float",            JavaFloatStringifier.create() );

        APPLICATION_JAVASCRIPT.theChildStringifiers.put( "stacktrace",       InvalidStringifier.create() );

        APPLICATION_JAVASCRIPT.theChildStringifiers.put( "string",           JavaStringStringifier.create() );
        APPLICATION_JAVASCRIPT.theChildStringifiers.put( "stringarray",      InvalidStringifier.create() );
    }

    /**
     * The singleton instance for editing text/plain, with the defaults.
     */
    public static final DefaultMessageFormatter TEXT_PLAIN_EDIT = new DefaultMessageFormatter( "TEXT_PLAIN_EDIT", TEXT_PLAIN );
    static {
        // only overrides need to be specified
        // currently none

    }

    /**
     * The singleton instance for editing text/html, with the defaults.
     */
    public static final DefaultMessageFormatter TEXT_HTML_EDIT = new DefaultMessageFormatter( "TEXT_HTML_EDIT", TEXT_PLAIN_EDIT );
    static {
        // only overrides need to be specified

        TEXT_HTML_EDIT.theChildStringifiers.put( "htmlsource",       HtmlifyingDelegatingStringifier.create( StringStringifier.create() ));
        TEXT_HTML_EDIT.theChildStringifiers.put( "htmltextarea",     HtmlifyingDelegatingStringifier.create( StringStringifier.create(), "textarea" ));

    }

    /**
     * The singleton instance for POST'ing to HTTP, with the defaults.
     */
    public static final DefaultMessageFormatter HTTP_POST = new DefaultMessageFormatter( "HTTP_POST", TEXT_PLAIN );
    static {
        // only overrides need to be specified
        // currently none

    }

    /**
     * Name of the DefaultMessageFormatter, for debugging purposes.
     */
    protected final String theName;

    /**
     * The delegate DefaultMessageFormatter, if any
     */
    protected final DefaultMessageFormatter theDelegate;

    /**
     * The currently registered child Stringifiers with their tags on this level
     */
    protected final Map<String,Stringifier<?>> theChildStringifiers = new HashMap<>();

    /**
     * The map we provide to the AnyMessageStringifier that knows how to delegate.
     */
    protected final Map<String,Stringifier<?>> theDelegatingChildStringifierMap;
}
