//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies a URL argument by prepending & or ? correctly.
 */
public class AppendToUrlStringifier
        extends
            StringStringifier
{
    /**
     * Factory method.
     *
     * @return the created StringStringifier
     */
    public static AppendToUrlStringifier create()
    {
        return new AppendToUrlStringifier();
    }

    /**
     * No-op constructor. Use factory method.
     */
    protected AppendToUrlStringifier()
    {
        // no op
    }

    /**
     * Format an Object using this Stringifier.
     *
     * @param soFar the String so far, if any
     * @param arg the Object to format, or null
     * @param pars collects parameters that may influence the String representation. Always provided.
     * @return the formatted String
     */
    @Override
    public String format(
            String                arg,
            StringifierParameters pars,
            String                soFar )
    {
        if( arg == null ) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        if( soFar.indexOf( '?' ) > 0 ) {
            buf.append( '&' );
        } else {
            buf.append( '?' );
        }

        if( arg.startsWith( "&" ) || arg.startsWith( "?" )) {
            arg = arg.substring( 1 );
        }
        buf.append( arg );

        String ret = potentiallyShorten( buf.toString(), pars );
        // not sure this is the best we can do. Use case: show too long URL on screen.

        ret = escape( ret );
        return ret;
    }
}
