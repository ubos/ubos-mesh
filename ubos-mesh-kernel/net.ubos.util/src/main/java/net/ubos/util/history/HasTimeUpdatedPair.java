//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history;

/**
 * Convenience class to easily store basic data types in a History.
 *
 * @param <E> the type of payload
 */
public class HasTimeUpdatedPair<E>
     implements
        HasTimeUpdated
{
    /**
     * Constructor.
     *
     * @param timeUpdated the time the payload was created or last updated
     * @param payload the payload
     */
    public HasTimeUpdatedPair(
            long timeUpdated,
            E    payload )
    {
        theTimeUpdated = timeUpdated;
        thePayload     = payload;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * Obtain the payload.
     *
     * @return the payload
     */
    public E getPayload()
    {
        return thePayload;
    }

    /**
     * The time the payload was last updated.
     */
    protected final long theTimeUpdated;

    /**
     * The payload.
     */
    protected final E thePayload;
}
