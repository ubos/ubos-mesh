//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import net.ubos.util.ResourceHelper;

/**
 * <p>Marker interface for Exceptions that carry localization parameters.
 *
 * <p>Unfortunately in Java, interfaces may not inherit from classes, so we have to
 *    copy-paste all the public method declarations from the Exception class.
 */
public interface LocalizedException
            // extends Exception -- we would love to say this here, but Java won't let us
{
    /**
     * Obtain resource parameters for the internationalization.
     *
     * @return the resource parameters
     */
    public abstract Object [] getLocalizationParameters();

    /**
     * Copied from Throwable.
     *
     * @return the message
     */
    public String getMessage();

    /**
     * Copied from Throwable.
     *
     * @return the localized message
     */
    default String getLocalizedMessage()
    {
        return AbstractLocalizedException.assembleDefaultLocalizedMessage( this );
    }

    /**
     * Default implementation for finding the correct ResourceHelper to use.
     *
     * @return the ResourceHelper to use
     */
    default ResourceHelper findResourceHelperForLocalizedMessage()
    {
        return ResourceHelper.getInstance( getClass() );
    }

    /**
     * Copied from Throwable.
     *
     * @return the cause
     */
    public Throwable getCause();

    /**
     * Copied from Throwable.
     */
    public void printStackTrace();

    /**
     * Copied from Throwable.
     *
     * @param s <code>PrintStream</code> to use for output
     */
    public void printStackTrace(
            PrintStream s );

    /**
     * Copied from Throwable.
     *
     * @param s <code>PrintWriter</code> to use for output
     */
    public void printStackTrace(
            PrintWriter s );
}
