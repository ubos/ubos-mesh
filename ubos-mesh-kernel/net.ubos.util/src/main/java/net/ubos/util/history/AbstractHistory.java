//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.history;

import net.ubos.util.cursoriterator.history.HistoryCursorIterator;

/**
 * Default code for many implementations of History.
 * In many cases, more efficient implementations can be found than these here.
 *
 * @param <E> the type of object for which this is a history
 */
public abstract class AbstractHistory<E extends HasTimeUpdated>
        implements
            History<E>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public int getLength()
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToBeforeFirst();
        int ret = iter.moveToAfterLast();

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E oldest()
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToBeforeFirst();

        E ret = iter.hasNext() ? iter.next() : null;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E current()
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToAfterLast();

        E ret = iter.hasPrevious() ? iter.previous() : null;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsAt(
            long t )
    {
        E found = at( t );
        return found != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E before(
            long t )
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToJustAfterTime(t);

        E ret;
        if( iter.hasPrevious() ) {
            ret = iter.previous();
            if( ret.getTimeUpdated() >= t && iter.hasPrevious() ) {
                ret = iter.previous();
            }
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E atOrBefore(
            long t )
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToJustAfterTime(t);

        E ret;
        if( iter.hasPrevious() ) {
            ret = iter.previous();
            if( ret.getTimeUpdated() > t && iter.hasPrevious() ) {
                ret = iter.previous();
            }
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E atOrAfter(
            long t )
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToJustBeforeTime(t);

        E ret;
        if( iter.hasNext() ) {
            ret = iter.next();
            if( ret.getTimeUpdated() < t && iter.hasNext() ) {
                ret = iter.next();
            }
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E after(
            long t )
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToJustBeforeTime(t);

        E ret;
        if( iter.hasNext() ) {
            ret = iter.next();
            if( ret.getTimeUpdated() <= t && iter.hasNext() ) {
                ret = iter.next();
            }
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E closest(
            long t )
    {
        HistoryCursorIterator<E> iter = iterator();
        iter.moveToJustBeforeTime(t);

        E ret;
        if( iter.hasNext() ) {
            E before = iter.next();
            if( iter.hasNext() ) {
                E after = iter.next();

                long afterDelta  = after.getTimeUpdated() - t;
                long beforeDelta = t - before.getTimeUpdated();

                if( afterDelta < beforeDelta ) {
                    ret = after;
                } else {
                    ret = before;
                }

            } else {
                ret = before;
            }

        } else {
            // defaults to previous
            if( iter.hasPrevious() ) {
                ret = iter.peekPrevious();
            } else {
                ret = null;
            }
        }
        return ret;
    }
}

