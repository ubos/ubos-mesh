//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies the stack trace of a Throwable in plain text.
 */
public class StacktraceStringifier
        extends
            AbstractStringifier<Throwable>
{
    /**
     * Factory method.
     *
     * @return the created StacktraceStringifier
     */
    public static StacktraceStringifier create()
    {
        String start  = "";
        String middle = "\n";
        String end    = "";
        String empty  = "";

        return new StacktraceStringifier( start, middle, end, empty );
    }

    /**
     * Factory method.
     *
     * @param start the String to print prior to the first frame of the stack trace
     * @param middle the String to print between frames of the stack trace
     * @param end the String to print after the last frame of the stack trace
     * @param empty the String to print if the stacktrace is empty
     * @return the created StacktraceStringifier
     */
    public static StacktraceStringifier create(
            String start,
            String middle,
            String end,
            String empty )
    {
        return new StacktraceStringifier( start, middle, end, empty );
    }

    /**
     * Private constructor. Use factory method.
     *
     * @param start the String to print prior to the first frame of the stack trace
     * @param middle the String to print between frames of the stack trace
     * @param end the String to print after the last frame of the stack trace
     * @param empty the String to print if the stacktrace is empty
     */
    protected StacktraceStringifier(
            String start,
            String middle,
            String end,
            String empty )
    {
        theStart  = start;
        theMiddle = middle;
        theEnd    = end;
        theEmpty  = empty;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            Throwable             arg,
            StringifierParameters pars,
            String                soFar )
    {
        StackTraceElement [] elements = arg.getStackTrace();

        StringBuilder buf = new StringBuilder();

        if( elements == null || elements.length == 0 ) {
            buf.append( theEmpty );

        } else {
            String sep = theStart;

            for( StackTraceElement current : elements ) {
                buf.append( sep );
                buf.append( current.toString() );
                sep = theMiddle;
            }
            buf.append( theEnd );
        }

        String ret = potentiallyShorten( buf.toString(), pars );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        return format( (Throwable) arg, pars, soFar );
    }

    /**
     * The String to print prior to the stack trace.
     */
    protected final String theStart;

    /**
     * The String to print between stack trace elements.
     */
    protected final String theMiddle;

    /**
     * The String to print after the stack trace.
     */
    protected final String theEnd;

    /**
     * The String to print if the stack trace is empty.
     */
    protected final String theEmpty;
}
