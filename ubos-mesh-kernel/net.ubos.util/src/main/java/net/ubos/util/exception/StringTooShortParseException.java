//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

import net.ubos.util.logging.Log;

/**
 * A ParseException indicating that a String was too short.
 */
public class StringTooShortParseException
    extends
        LocalizedParseException
{
    private static final Log log = Log.getLogInstance( StringTooShortParseException.class );

    /**
     * Constructor.
     *
     * @param string the text that could not be parsed
     * @param message error message
     * @param minLength the minimally required length
     */
    public StringTooShortParseException(
            String string,
            String message,
            int    minLength )
    {
        super( string, message, 0, null );

        theMinLength = minLength;
    }

    /**
     * Obtain the minimally required length.
     *
     * @return the minimally required length
     */
    public int getRequiredMinLength()
    {
        return theMinLength;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theString, theMinLength };
    }

    /**
     * The minimally required length.
     */
    protected int theMinLength;
}
