//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

import net.ubos.util.factory.FactoryException;

/**
 * A factory for BufferingDumpers.
 */
public interface BufferingDumperFactory
        extends
            DumperFactory
{
    /**
     * {@inheritDoc}
     */
    @Override
    public abstract BufferingDumper obtainFor(
            Object key )
        throws
            FactoryException;
}

