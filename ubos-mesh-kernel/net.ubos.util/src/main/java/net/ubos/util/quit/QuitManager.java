//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.quit;

import net.ubos.util.listenerset.FlexibleListenerSet;
import net.ubos.util.livedead.IsDeadException;

/**
  * <p>This manager acts as a synchronizer to enable a defined "quit".
  * There is only one QuitManager per Java VM.</p>
  *
  * <p>Objects implementing the {@link QuitListener} interface register with the
  * QuitManager. When a quit is requested (by an invocation of {@link #initiateQuit}),
  * the QuitManager will call {@link QuitListener#prepareForQuit} on all registered QuitListeners
  * followed by a final {@link QuitListener#die} on all of them.</p>
  */
public class QuitManager
{
    /**
     * This is only called by the program's main Thread, which is being suspended in this method
     * until some other Thread initiates a quit.
     *
     * @throws InterruptedException if another Thread sent an interrupt
     */
    public static void waitForQuit()
        throws
            InterruptedException
    {
        synchronized( QuitManager.class ) {
            if( !haveInitiatedQuit ) {
               QuitManager.class.wait();
            }
        }
    }

    /**
      * This call causes the quit procedure to be initiated.
      */
    public static void initiateQuit()
    {
        synchronized( QuitManager.class ) {
            if( haveInitiatedQuit ) {
                return;
            }

            haveInitiatedQuit = true;
        }

        theQuitListeners.fireEvent( null, EventType.PREPARE_FOR_QUIT );
        theQuitListeners.fireEvent( null, EventType.DIE );

        synchronized( QuitManager.class ) {
            QuitManager.class.notifyAll();
        }
    }

    /**
     * Add a new listener using a WeakReference.
     *
     * @param newListener the listener to be added to this set
     * @see #addSoftQuitListener
     * @see #addDirectQuitListener
     * @see #removeQuitListener
     */
    public static void addWeakQuitListener(
            QuitListener newListener )
    {
        theQuitListeners.addWeak( newListener );
    }

    /**
     * Add a new listener using a SoftReference.
     *
     * @param newListener the listener to be added to this set
     * @see #addWeakQuitListener
     * @see #addDirectQuitListener
     * @see #removeQuitListener
     */
    public static void addSoftQuitListener(
            QuitListener newListener )
    {
        theQuitListeners.addSoft( newListener );
    }
    
    /**
     * Add a new listener directly, i.e. without using References.
     *
     * @param newListener the new listener
     * @see #addWeakQuitListener
     * @see #addSoftQuitListener
     * @see #removeQuitListener
     */
    public static void addDirectQuitListener(
            QuitListener newListener )
    {
        theQuitListeners.addDirect( newListener );
    }

    /**
     * Remove a listener.
     *
     * @param oldListener the to-be-removed listener
     * @see #addWeakQuitListener
     * @see #addSoftQuitListener
     * @see #addDirectQuitListener
     */
    public static void removeQuitListener(
            QuitListener oldListener )
    {
        theQuitListeners.remove( oldListener );
    }

    /**
     * When this is set to true, we have initiated quit already.
     */
    private static boolean haveInitiatedQuit = false;

    /**
     * The types of events that can be sent.
     */
    protected static enum EventType {
        PREPARE_FOR_QUIT {
            @Override
            public void fireEventToListener(
                    QuitListener listener )
            {
                listener.prepareForQuit();
            }
        },
        DIE {
            @Override
            public void fireEventToListener(
                    QuitListener listener )
            {
                try {
                    listener.die();
                } catch( IsDeadException ex ) {
                    // ignore
                }
            }
        };

        /**
         * Fire the event.
         *
         * @param listener the listener to send the event to
         */
        public abstract void fireEventToListener(
                QuitListener listener );
    }

    /**
      * The current set of quit listeners.
      */
    protected static final FlexibleListenerSet<QuitListener,Void,EventType> theQuitListeners
            = new FlexibleListenerSet<QuitListener,Void,EventType>( true ) {
                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    protected void fireEventToListener(
                            QuitListener listener,
                            Void         event,
                            EventType    parameter )
                    {
                        parameter.fireEventToListener( listener );
                    }

    };
}
