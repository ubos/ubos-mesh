//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Map;
import net.ubos.util.ArrayFacade;

/**
 * A MessageStringifier that works on Objects and does not require more specific subclasses.
 */
public class AnyMessageStringifier
        extends
            MessageStringifier
{
    /**
     * Factory method.
     * 
     * @param formatString the format string, such as "Your {0,string} items cost {1,currency}."
     * @param childStringifiers the mapping between symbolic names (e.g. "string", "currency") to the actual Stringifier
     * @return the AnyMessageStringifier
     * @throws CompoundStringifierCompileException thrown if the format string could not be compiled
     */
    public static AnyMessageStringifier create(
            String                     formatString,
            Map<String,Stringifier<?>> childStringifiers )
        throws
            CompoundStringifierCompileException
    {
        AnyMessageStringifier ret = new AnyMessageStringifier( formatString, childStringifiers );
        ret.compile();
        return ret;
    }
    
    /**
     * Constructor.
     *
     * @param formatString the format string, such as "Your {0,string} items cost {1,currency}."
     * @param childStringifiers the mapping between symbolic names (e.g. "string", "currency") to the actual Stringifier
     */
    protected AnyMessageStringifier(
            String                     formatString,
            Map<String,Stringifier<?>> childStringifiers )
    {
        super( formatString, childStringifiers );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ArrayFacade<Object> compoundUnformat(
            StringifierParsingChoice [] childChoices )
    {
        Object [] almost = new Object[ childChoices.length ];
        for( int i=0 ; i<childChoices.length ; ++i ) {
            almost[i] = childChoices[i].unformat();
        }
        return new ArrayFacade<>( almost );
    }
}
