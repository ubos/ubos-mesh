//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.function.BiPredicate;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * A CursorIterator for ArrayLists.
 *
 * @param <E> the type of element to iterate over
 */
public class ArrayListCursorIterator<E>
        extends
            AbstractReadOneCursorIterator<E>
        implements
            CursorIterator<E>,
            CanBeDumped
{
    /**
     * Factory method. Position cursor at the beginning of the array. Iterate over the
     * entire array.
     *
     * @param array the array to iterate over
     * @return the created ArrayCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> ArrayListCursorIterator<E> create(
            ArrayList<E> array )
    {
        return new ArrayListCursorIterator<>(
                array,
                0,
                0,
                array.size(),
                new DEFAULT_EQUALS<>());
    }

    /**
     * Factory method. Position cursor at a given position of the array. Iterate over the
     * entire array.
     *
     * @param array the array to iterate over
     * @param startPosition the start position
     * @return the created ArrayCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> ArrayListCursorIterator<E> create(
            ArrayList<E> array,
            int          startPosition )
    {
        return new ArrayListCursorIterator<>(
                array,
                startPosition,
                0,
                array.size(),
                new DEFAULT_EQUALS<>() );
    }

    /**
     *  Factory method. Position cursor at a given position of the array. Iterate over the
     * slice of the array defined by lowerBound (inclusive) and upperBound (exclusive).
     *
     * @param array the array to iterate over
     * @param startPosition the start position
     * @param lowerBound the lowest index in the array to return (inclusive)
     * @param upperBound the highest index in the array to return (exclusive)
     * @return the created ArrayCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> ArrayListCursorIterator<E> create(
            ArrayList<E> array,
            int          startPosition,
            int          lowerBound,
            int          upperBound )
    {
        return new ArrayListCursorIterator<>(
                array,
                startPosition,
                lowerBound,
                upperBound,
                new DEFAULT_EQUALS<>());
    }

    /**
     *  Factory method. Position cursor at a given position of the array. Iterate over the
     * slice of the array defined by lowerBound (inclusive) and upperBound (exclusive).
     *
     * @param array the array to iterate over
     * @param startPosition the start position
     * @param lowerBound the lowest index in the array to return (inclusive)
     * @param upperBound the highest index in the array to return (exclusive)
     * @param equals the equality operation for E's
     * @return the created ArrayCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> ArrayListCursorIterator<E> create(
            ArrayList<E>     array,
            int              startPosition,
            int              lowerBound,
            int              upperBound,
            BiPredicate<E,E> equals )
    {
        return new ArrayListCursorIterator<>( array, startPosition, lowerBound, upperBound, equals );
    }

    /**
     * Constructor. Position cursor at a given position of the array. Iterate over the
     * slice of the array defined by lowerBound (inclusive) and upperBound (exclusive).
     *
     * @param array the array to iterate over
     * @param startPosition the start position
     * @param lowerBound the lowest index in the array to return (inclusive)
     * @param upperBound the highest index in the array to return (exclusive)
     * @param equals the equality operation for E's
     */
    protected ArrayListCursorIterator(
            ArrayList<E>     array,
            int              startPosition,
            int              lowerBound,
            int              upperBound,
            BiPredicate<E,E> equals )
    {
        super( equals );

        if( upperBound > array.size() ) {
            throw new IllegalArgumentException( "Upperbound higher than length of array" );
        }
        if( lowerBound < 0 ) {
            throw new IllegalArgumentException( "Lowerbound cannot be negative" );
        }
        if( lowerBound > upperBound ) {
            // they can be the same, in which case this turns into a ZeroElementIterator
            throw new IllegalArgumentException( "Upperbound must be higher than lowerbound" );
        }
        if( startPosition < lowerBound ) {
            throw new IllegalArgumentException( "Start position cannot be lower than lowerbound" );
        }
        if( startPosition > upperBound ) {
            throw new IllegalArgumentException( "Start position cannot be higher than upperbound" );
        }

        theArray      = array;
        thePosition   = startPosition;
        theLowerBound = lowerBound;
        theUpperBound = upperBound;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekNext()
    {
        if( thePosition >= theLowerBound && thePosition < theUpperBound ) {
            return theArray.get( thePosition );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekPrevious()
    {
        if( thePosition > theLowerBound && thePosition <= theUpperBound ) {
            return theArray.get( thePosition-1 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        boolean ret = thePosition + n <= theUpperBound; // in position 0, next(1) should return true if theArray.length is 1
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        boolean ret = thePosition - n >= theLowerBound;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
    {
        if( thePosition >= theUpperBound ) {
            throw new NoSuchElementException();
        }
        E ret = theArray.get( thePosition );
        ++thePosition;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
    {
        if( thePosition <= theLowerBound ) {
            throw new NoSuchElementException();
        }
        --thePosition;

        E ret = theArray.get( thePosition );
        return ret;
    }

    /**
     * Obtain the current position, starting the count with 0 for the first element.
     *
     * @return the current position
     */
    public int getPosition()
    {
        return thePosition;
    }

    /**
     * Set the new position. Throws NoSuchElementException if the position does not exist.
     *
     * @param n the new position
     * @throws NoSuchElementException
     */
    public void setPosition(
            int n )
        throws
            NoSuchElementException
    {
        if( thePosition + n >= theUpperBound || thePosition - n < theLowerBound ) {
            throw new NoSuchElementException();
        }
        thePosition -= n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        int newPosition = thePosition + n;
        if( newPosition > theUpperBound || newPosition < theLowerBound-1 ) {
            throw new NoSuchElementException();
        }
        thePosition = newPosition;
    }

    /**
     * Move the cursor to this element, i.e. return this element when {@link #next next} is invoked
     * right afterwards.
     *
     * @param pos the element to move the cursor to
     * @return the number of steps that were taken to move. Positive number means forward, negative backward
     * @throws NoSuchElementException thrown if this element is not actually part of the collection to iterate over
     */
    public int moveTo(
            E pos )
        throws
            NoSuchElementException
    {
        for( int i=0 ; i<theArray.size() ; ++i ) {
            if( pos == theArray.get( i ) ) {
                int ret = i - thePosition;
                thePosition = i;
                return ret;
            }
        }
        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret = -thePosition;
        thePosition = 0;

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int ret = theArray.size() - thePosition;
        thePosition = theArray.size();

        return ret;

    }

    /**
     * Reset the Iterator to the first position.
     */
    public void reset()
    {
        thePosition = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayListCursorIterator<E> createCopy()
    {
        // keep position
        return new ArrayListCursorIterator<>( theArray, thePosition, theLowerBound, theUpperBound, theEquals );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException
    {
        if( !( position instanceof ArrayListCursorIterator )) {
            throw new IllegalArgumentException( "Wrong type of CursorIterator: " + position );
        }
        ArrayListCursorIterator<E> realPosition = (ArrayListCursorIterator<E>) position;

        if( theArray != realPosition.theArray ) {
            throw new IllegalArgumentException( "Not the same instance of array to iterate over" );
        }
        if( theLowerBound != realPosition.theLowerBound ) {
            throw new IllegalArgumentException( "Not the same lower bound" );
        }
        if( theUpperBound != realPosition.theUpperBound ) {
            throw new IllegalArgumentException( "Not the same upper bound" );
        }

        thePosition = realPosition.thePosition;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "array",
                    "position",
                    "lower",
                    "upper"
                },
                new Object[] {
                    theArray,
                    thePosition,
                    theLowerBound,
                    theUpperBound
                } );
    }

    /**
      * The array that we are iterating over.
      */
    protected ArrayList<E> theArray;

    /**
      * The current position in the array.
      */
    protected int thePosition;

    /**
     * The index of the lowest position in the array that we return (inclusive).
     */
    protected int theLowerBound;

    /**
     * The index of the highest position in the array that we return (exclusive).
     */
    protected int theUpperBound;
}
