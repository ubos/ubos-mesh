//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import java.util.Iterator;
import net.ubos.util.ArrayFacade;

/**
 * A Stringifier that processes arrays. Arrays must be passed as
 * {@link net.ubos.util.ArrayFacade}, as Java generics aren't working well
 * enough without that intermediate class.
 *
 * It delegates to another Stringifier for the members of
 * the array, and can prepend, append and insert between elements constant strings.
 * If the array is empty, a different string is emitted.
 *
 * @param <T> the type of the Objects to be stringified
 */
public class ArrayStringifier<T>
        extends
             AbstractStringifier<ArrayFacade<T>>
{
    /**
     * Factory method. This creates an ArrayStringifier that merely appends the
     * individual components after each other.
     *
     * @param delegate the delegate to which all element stringifications are delegated
     * @return the created ArrayStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ArrayStringifier<T> create(
            Stringifier<T> delegate )
    {
        return new ArrayStringifier<>( null, null, null, null, delegate );
    }

    /**
     * Factory method. This creates an ArrayStringifier that joins the
     * individual components after each other with a string in the middle.
     * This is similar to Perl's join.
     *
     * @param delegate the delegate to which all element stringifications are delegated
     * @param middle the string to insert in the middle
     * @return the created ArrayStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ArrayStringifier<T> create(
            String         middle,
            Stringifier<T> delegate )
    {
        return new ArrayStringifier<>( null, middle, null, null, delegate );
    }

    /**
     * Factory method. This creates an ArrayStringifier that joins the
     * individual components after each other with a string in the middle,
     * prepends a start and appends an end.
     *
     * @param delegate the delegate to which all element stringifications are delegated
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     * @return the created ArrayStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ArrayStringifier<T> create(
            String         start,
            String         middle,
            String         end,
            Stringifier<T> delegate )
    {
        return new ArrayStringifier<>( start, middle, end, null, delegate );
    }

    /**
     * Factory method. This creates an ArrayStringifier that joins the
     * individual components after each other with a string in the middle,
     * prepends a start and appends an end, or uses a special empty String if
     * the array is empty.
     *
     * @param delegate the delegate to which all element stringifications are delegated
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     * @param empty what to emit instead if the array is empty
     * @return the created ArrayStringifier
     * @param <T> the type of the Objects to be stringified
     */
    public static <T> ArrayStringifier<T> create(
            String         start,
            String         middle,
            String         end,
            String         empty,
            Stringifier<T> delegate )
    {
        return new ArrayStringifier<>( start, middle, end, empty, delegate );
    }

    /**
     * Constructor.
     *
     * @param delegate the delegate to which all element stringifications are delegated
     * @param start the string to insert at the beginning
     * @param middle the string to insert in the middle
     * @param end the string to append at the end
     * @param empty what to emit instead if the array is empty
     */
    protected ArrayStringifier(
            String         start,
            String         middle,
            String         end,
            String         empty,
            Stringifier<T> delegate )
    {
        theStart    = start;
        theMiddle   = middle;
        theEnd      = end;
        theEmpty    = empty;
        theDelegate = delegate;
    }

    /**
     * Obtain the start String, if any.
     *
     * @return the start String, if any
     */
    public String getStart()
    {
        return theStart;
    }

    /**
     * Obtain the middle String, if any.
     *
     * @return the middle String, if any
     */
    public String getMiddle()
    {
        return theMiddle;
    }

    /**
     * Obtain the end String, if any.
     *
     * @return the end String, if any
     */
    public String getEnd()
    {
        return theEnd;
    }

    /**
     * Obtain the empty String, if any.
     *
     * @return the empty String, if any
     */
    public String getEmpty()
    {
        return theEmpty;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            ArrayFacade<T>        arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException
    {
        if( arg == null || arg.getArray().length == 0 ) {
            if( theEmpty != null ) {
                return theEmpty;
            } else if( theStart != null ) {
                if( theEnd != null ) {
                    return theStart+theEnd;
                } else {
                    return theStart;
                }
            } else if( theEnd != null ) {
                return theEnd;
            } else {
                return "";
            }
        }

        T []          data = arg.getArray();
        StringBuilder ret  = new StringBuilder();
        String        sep  = theStart;

        for( int i=0 ; i<data.length ; ++i ) {
            if( sep != null ) {
                ret.append( sep );
            }
            String childInput = theDelegate.format( data[i], pars, soFar + ret.toString() ); // presumably shorter, but we don't know
            if( childInput != null ) {
                ret.append( childInput );
            }
            sep = theMiddle;
        }
        if( theEnd != null ) {
            ret.append( theEnd );
        }
        return potentiallyShorten( ret.toString(), pars );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            StringifierException,
            ClassCastException
    {
        if( arg instanceof ArrayFacade ) {
            return format( (ArrayFacade<T>) arg, pars, soFar );
        } else {
            return format( ArrayFacade.<T>create( (T []) arg ), pars, soFar );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayFacade<T> unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        Iterator<StringifierParsingChoice<ArrayFacade<T>>> iter  = allParsingChoiceIterator( rawString, 0, rawString.length(), 1, factory ); // only need one
        StringifierParsingChoice<ArrayFacade<T>>           found = null;

        while( iter.hasNext() ) {
            StringifierParsingChoice<ArrayFacade<T>> choice = iter.next();

            if( choice.getEndIndex() == rawString.length() ) {
                // found
                found = choice;
                break;
            }
        }
        if( found == null ) {
            throw new StringifierParseException( this, rawString );
        }
        ArrayFacade<T> ret = found.unformat();
        return ret;
    }

    /**
     * The String to insert at the beginning. May be null.
     */
    protected final String theStart;

    /**
     * The String to insert when joining two elements. May be null.
     */
    protected final String theMiddle;

    /**
     * The String to append at the end. May be null.
     */
    protected final String theEnd;

    /**
     * The String to emit if the array if empty. May be null, in which case it is assumed to theStart+theEnd.
     */
    protected final String theEmpty;

    /**
     * The delegate Stringifier to which all element stringifications are delegated.
     */
    protected final Stringifier<T> theDelegate;
}
