//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

import java.util.Comparator;

/**
 * Returns the opposite order than a delegate Comparator.
 *
 * @param <T> the type of objects that may be compared by this comparator
 */
public class InverseComparator<T>
    implements
        Comparator<T>
{
    /**
     * Constructor.
     *
     * @param delegate the delegate Comparator whose results are inverted.
     */
    public InverseComparator(
            Comparator<T> delegate )
    {
        theDelegate = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(
            T o1,
            T o2 )
    {
        int ret = theDelegate.compare( o1, o2 );
        return -ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object obj )
    {
        if( !( obj instanceof InverseComparator )) {
            return false;
        }
        InverseComparator realObj = (InverseComparator) obj;

        return theDelegate.equals( realObj.theDelegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 11 * hash + (this.theDelegate != null ? this.theDelegate.hashCode() : 0);
        return hash;
    }

    /**
     * The delegate Comparator whose results are inverted.
     */
    protected final Comparator<T> theDelegate;
}
