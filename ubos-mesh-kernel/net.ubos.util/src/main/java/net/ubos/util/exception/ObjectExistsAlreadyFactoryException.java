//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.exception;

import net.ubos.util.factory.FactoryException;
import net.ubos.util.factory.SmartFactory;

/**
 * Thrown by a SmartFactory if a to-be-newly-created object exists already.
 */
public class ObjectExistsAlreadyFactoryException
        extends
            FactoryException
{
    private static final long serialVersionUID = 1L; // helps with serialization

    /**
     * Constructor.
     *
     * @param sender the SmartFactory that threw this exception
     * @param existing the object that existed already with this key
     */
    public ObjectExistsAlreadyFactoryException(
            SmartFactory<?,?,?> sender,
            Object              existing )
    {
        super( sender );
        
        theExisting = existing;
    }

    /**
     * Constructor.
     *
     * @param sender the SmartFactory that threw this exception
     * @param existing the object that existed already with this key
     * @param cause the actual underlying cause
     */
    public ObjectExistsAlreadyFactoryException(
            SmartFactory<?,?,?> sender,
            Object         existing,
            Throwable      cause )
    {
        super( sender, cause );
        
        theExisting = existing;
    }

    /**
     * Constructor.
     *
     * @param sender the SmartFactory that threw this exception
     * @param existing the object that existed already with this key
     * @param message the message
     */
    public ObjectExistsAlreadyFactoryException(
            SmartFactory<?,?,?> sender,
            Object         existing,
            String         message )
    {
        super( sender, message );
        
        theExisting = existing;
    }

    /**
     * Constructor.
     *
     * @param sender the SmartFactory that threw this exception
     * @param existing the object that existed already with this key
     * @param message the message
     * @param cause the actual underlying cause
     */
    public ObjectExistsAlreadyFactoryException(
            SmartFactory<?,?,?> sender,
            Object         existing,
            String         message,
            Throwable      cause )
    {
        super( sender, message, cause );
        
        theExisting = existing;
    }
    
    /**
     * Obtain the SmartFactory that threw this exception.
     * 
     * @return the SmartFactory
     */
    @Override
    public SmartFactory<?,?,?> getSender()
    {
        return (SmartFactory<?,?,?>) super.getSender();
    }

    /**
     * Obtain the Object that existed already.
     * 
     * @return the Object
     */
    public Object getExisting()
    {
        return theExisting;
    }

    /**
     * The Object that existed already.
     */
    protected Object theExisting;
}
