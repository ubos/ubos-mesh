//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

/**
 * Stringifies a single String.
 */
public class StringStringifier
        extends
            AbstractStringifier<String>
{
    /**
     * Factory method.
     *
     * @return the created StringStringifier
     */
    public static StringStringifier create()
    {
        return new StringStringifier();
    }

    /**
     * No-op constructor. Use factory method.
     */
    protected StringStringifier()
    {
        // no op
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String format(
            String                arg,
            StringifierParameters pars,
            String                soFar )
    {
        String ret = potentiallyShorten( arg, pars );

        ret = escape( ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String attemptFormat(
            Object                arg,
            StringifierParameters pars,
            String                soFar )
        throws
            ClassCastException
    {
        if( arg == null ) {
            return "";
        } else if( arg instanceof String ) {
            return format( (String) arg, pars, soFar );
        } else {
            return format( String.valueOf( arg ), pars, soFar ); // fallback
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String unformat(
            String                     rawString,
            StringifierUnformatFactory factory )
        throws
            StringifierParseException
    {
        String ret = unescape( rawString );
        return ret;
    }

    /**
     * Overridable method to possibly escape a String first.
     *
     * @param s the String to be escaped
     * @return the escaped String
     */
    protected String escape(
            String s )
    {
        return s;
    }

    /**
     * Overridable method to possibly unescape a String first.
     *
     * @param s the String to be unescaped
     * @return the unescaped String
     */
    protected String unescape(
            String s )
    {
        return s;
    }
}
