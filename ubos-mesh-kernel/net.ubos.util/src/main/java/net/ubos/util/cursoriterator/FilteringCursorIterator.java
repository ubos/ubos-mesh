//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.NoSuchElementException;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

/**
 * A {@link CursorIterator} that iterates over a subset of elements returned by a delegate
 * CursorIterator. The position of the Iterator is assumed to be on the next element
 * to be returned going forward, and one after than the next element to be returned
 * going backwards.
 *
 * @param <E> the type of element to iterate over
 */
public class FilteringCursorIterator<E>
        extends
            AbstractReadOneCursorIterator<E>
{
    /**
     * Factory method.
     *
     * @param delegate the delegate CursorIterator
     * @param filter the Predicate to use that determines which elements from the underlying CursorIterator to select
     * @return the created FilteringCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> FilteringCursorIterator<E> create(
            CursorIterator<E> delegate,
            Predicate<E>      filter )
    {
        return new FilteringCursorIterator<>(
                delegate,
                filter,
                new DEFAULT_EQUALS<>() );
    }

    /**
     * Factory method.
     *
     * @param delegate the delegate CursorIterator
     * @param filter the Predicate to use that determines which elements from the underlying CursorIterator to select
     * @param equals the equality operation for E's
     * @return the created FilteringCursorIterator
     * @param <E> the type of element to iterate over
     */
    public static <E> FilteringCursorIterator<E> create(
            CursorIterator<E>          delegate,
            Predicate<E>               filter,
            BiPredicate<Object,Object> equals )
    {
        return new FilteringCursorIterator<>(
                delegate,
                filter,
                new DEFAULT_EQUALS<>() );
    }

    /**
     * Constructor.
     *
     * @param delegate the delegate CursorIterator
     * @param filter the Predicate to use that determines which elements from the underlying CursorIterator to select
     * @param equals the equality operation for E's
     */
    protected FilteringCursorIterator(
            CursorIterator<E> delegate,
            Predicate<E>      filter,
            BiPredicate<E,E>  equals )
    {
        super( equals );

        theDelegate = delegate;
        theFilter   = filter;

        theIsAhead = false;
        theIsBack  = false;

        try {
            if( filter.test( delegate.peekNext() )) {
                theIsAhead = true;
            }
        } catch( NoSuchElementException ex ) {
            // that's fine
            theIsAhead = true;
        }
        try {
            if( filter.test( delegate.peekPrevious() )) {
                theIsBack = true;
            }
        } catch( NoSuchElementException ex ) {
            // that's fine
            theIsBack = true;
        }
    }

    /**
     * Constructor.
     *
     * @param delegate the delegate CursorIterator
     * @param filter the Predicate to use that determines which elements from the underlying CursorIterator to select
     * @param equals the equality operation for E's
     * @param isAhead if true, the delegate iterator is as far ahead as possible.
     * @param isBack if true, the delegate iterator is as far back as possible.
     */
    protected FilteringCursorIterator(
            CursorIterator<E> delegate,
            Predicate<E>      filter,
            BiPredicate<E,E>  equals,
            boolean           isAhead,
            boolean           isBack )
    {
        super( equals );

        theDelegate = delegate;
        theFilter   = filter;

        theIsAhead = isAhead;
        theIsBack  = isBack;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekNext()
    {
        if( !theIsAhead ) {
            moveAhead();
        }
        E ret = theDelegate.peekNext();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E peekPrevious()
    {
        if( !theIsBack ) {
            moveBack();
        }
        E ret = theDelegate.peekPrevious();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        if( !theIsAhead ) {
            moveAhead();
        }
        return theDelegate.hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        if( !theIsBack ) {
            moveBack();
        }
        return theDelegate.hasPrevious();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        // gotta try it out
        if( !theIsAhead ) {
            moveAhead();
        }
        CursorIterator<E> temp = theDelegate.createCopy();

        int count = 0;
        while( count < n && temp.hasNext() ) {
            E current = temp.next();
            if( theFilter.test( current )) {
                ++count;
            }
        }
        if( count == n ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        // gotta try it out
        if( !theIsBack ) {
            moveBack();
        }
        CursorIterator<E> temp = theDelegate.createCopy();

        int count = 0;
        while( count < n && temp.hasPrevious() ) {
            E current = temp.previous();
            if( theFilter.test( current )) {
                ++count;
            }
        }
        if( count == n ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E next()
    {
        if( !theIsAhead ) {
            moveAhead();
        }
        E ret = theDelegate.next();
        theIsAhead = false; // it might be true, but moveAhead will catch this the next time
        return ret;
    }

    // use default implementations for next(int) and previous(int) from superclass

    /**
     * {@inheritDoc}
     */
    @Override
    public E previous()
    {
        if( !theIsBack ) {
            moveBack();
        }
        E ret = theDelegate.previous();
        theIsBack = false; // it might be true, but moveBack will catch this the next time
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<E> createCopy()
    {
        return new FilteringCursorIterator<>(
                theDelegate.createCopy(),
                theFilter,
                theEquals,
                theIsAhead,
                theIsBack );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<E> position )
        throws
            IllegalArgumentException
    {
        if( !( position instanceof FilteringCursorIterator )) {
            throw new IllegalArgumentException( "Wrong type of CursorIterator: " + position );
        }
        FilteringCursorIterator<E> realPosition = (FilteringCursorIterator<E>) position;

        if( theFilter != realPosition.theFilter ) {
            throw new IllegalArgumentException( "Not the same instance of Filter" );
        }

        theDelegate.setPositionTo( realPosition.theDelegate ); // this may throw

        theIsAhead = realPosition.theIsAhead;
        theIsBack  = realPosition.theIsBack;
    }

    /**
     * Move to the next valid element.
     */
    protected void moveAhead()
    {
        theIsAhead = true; // regardless what else happens
        while( theDelegate.hasNext() ) {
            if( theFilter.test( theDelegate.peekNext() )) {
                return;
            }
            theIsBack = false;
            theDelegate.next();
        }
    }

    /**
     * Move to the next previous element.
     */
    protected void moveBack()
    {
        theIsBack = true; // regardless what else happens
        while( theDelegate.hasPrevious() ) {
            if( theFilter.test( theDelegate.peekPrevious() )) {
                return;
            }
            theIsAhead = false;
            theDelegate.previous();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret = 0;
        while( theDelegate.hasPrevious() ) {
            E candidate = theDelegate.previous();
            if( theFilter.test( candidate )) {
                --ret;
            }
        }
        theIsAhead = false;

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int ret = 0;
        while( theDelegate.hasNext() ) {
            E candidate = theDelegate.next();
            if( theFilter.test( candidate )) {
                ++ret;
            }
        }
        theIsBack = false;

        return ret;
    }

    /**
     * The filter.
     */
    protected final Predicate<E> theFilter;

    /**
     * The delegate iterator.
     */
    protected final CursorIterator<E> theDelegate;

    /**
     * If true, the delegate iterator is as far ahead as possible.
     */
    protected boolean theIsAhead;

    /**
     * If true, the delegate iterator is as far back as possible.
     */
    protected boolean theIsBack;
}
