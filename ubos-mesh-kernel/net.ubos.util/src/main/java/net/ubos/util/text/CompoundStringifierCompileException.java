//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.ResourceHelper;

/**
 * Thrown if a CompoundStringifier fails to parse the expression that
 * describes how the CompoundStringifier is composed from parts.
 * Concrete subtypes are defined as inner classes.
 */
public abstract class CompoundStringifierCompileException
        extends
            StringifierException
{
    /**
     * Constructor.
     *
     * @param source the Stringifier that raised this exception
     */
    protected CompoundStringifierCompileException(
            Stringifier source )
    {
        super( source );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceHelper findResourceHelperForLocalizedMessage()
    {
        return findResourceHelperForLocalizedMessageViaEnclosingClass();
    }

    /**
     * Indicates that brackets were unbalanced in the expression.
     */
    public static class UnbalancedBrackets
            extends
                CompoundStringifierCompileException
    {
        /**
         * Constructor.
         *
         * @param source the Stringifier that raised this exception
         * @param formatString the format String that contains the unbalanced brackets
         */
        public UnbalancedBrackets(
                Stringifier source,
                String      formatString )
        {
            super( source );

            theFormatString = formatString;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[] { theSource, theFormatString };
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return getClass().getName()
                    + ": unbalanced brackets in format String '"
                    + theFormatString
                    + "'.";
        }

        /**
         * The format String that had the problem.
         */
        protected final String theFormatString;
    }

    /**
     * Indicates that a parameter definition in an expression was incomplete.
     */
    public static class IncompleteParameter
            extends
                CompoundStringifierCompileException
    {
        /**
         * Constructor.
         *
         * @param source the Stringifier that raised the Exception
         * @param parameterExpression the expression that was incomplete
         */
        public IncompleteParameter(
                Stringifier source,
                String      parameterExpression )
        {
            super( source );

            theParameterExpression = parameterExpression;
        }

        /**
         * Obtain the expression that was incomplete.
         *
         * @return the expression that was incomplete
         */
        public String getParameterExpression()
        {
            return theParameterExpression;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[] { theSource, theParameterExpression };
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return getClass().getName()
                    + ": incomplete parameter use in expression '"
                    + theParameterExpression
                    + "'.";
        }

        /**
         * The expression that was incomplete.
         */
        protected final String theParameterExpression;
    }

    /**
     * Indicates that a symbolic child name, in an expression, was undefined.
     */
    public static class SymbolicChildNameUndefined
            extends
                CompoundStringifierCompileException
    {
        /**
         * Constructor.
         *
         * @param source the Stringifier that raised the Exception
         * @param parameterExpression the expression that contained the undefined symbolic child
         * @param childName the undefined symbolic child name
         */
        public SymbolicChildNameUndefined(
                Stringifier source,
                String      parameterExpression,
                String      childName )
        {
            super( source );

            theParameterExpression = parameterExpression;
            theChildName           = childName;
        }

        /**
         * Obtain the expression that contained the undefined child.
         *
         * @return the expression that contained the undefined child
         */
        public String getParameterExpression()
        {
            return theParameterExpression;
        }

        /**
         * Obtain the symbolic child name name that was undefined.
         *
         * @return the symbolic child name that was undefined
         */
        public String getUndefinedSymbolicChildName()
        {
            return theChildName;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[] { theSource, theParameterExpression, theChildName };
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return getClass().getName()
                    + ": child '"
                    + theChildName
                    + "' undefined in expression '"
                    + theParameterExpression
                    + "'.";
        }

        /**
         * The expression that contained the undefined child.
         */
        protected final String theParameterExpression;

        /**
         * The symbolic child name that was undefined.
         */
        protected final String theChildName;
    }
}
