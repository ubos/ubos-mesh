//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.text;

import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Superclass for Exceptions raised by Stringifiers.
 */
public abstract class StringifierException
        extends
            AbstractLocalizedException
{
    /**
     * Constructor.
     *
     * @param source the Stringifier that raised this exception
     */
    public StringifierException(
            Stringifier<?> source )
    {
        theSource = source;
    }

    /**
     * Constructor.
     *
     * @param source the Stringifier that raised this exception
     * @param cause the underlying cause
     */
    public StringifierException(
            Stringifier<?> source,
            Throwable      cause )
    {
        super( cause );
        theSource = source;
    }

    /**
     * The Stringifier that raised this exception.
     */
    protected final Stringifier<?> theSource;
}
