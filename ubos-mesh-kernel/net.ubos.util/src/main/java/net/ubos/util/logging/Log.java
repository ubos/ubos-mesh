//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

import java.util.Properties;
import net.ubos.util.ArrayHelper;
import net.ubos.util.factory.FactoryException;

/**
  * <p>How to use it (simplest case):
  * <ol>
  *  <li> import this class "<code>import net.ubos.util.logging.Log;</code>"
  *  <li> put a "<code>private static final Log log = Log.getLogInstance( &lt;myClass&gt;.class );</code>"
  *       into your class,<br>
  *       where "<code>&lt;myClass&gt;</code>" is your class
  *  <li> use the logger, eg "<code>log.warn("this is a warning"); <br>
  *       log.traceMethodCallEntry( "Finished parsing file: " + file.getName() );</code>"
  * </ol>
  */
public abstract class Log
{
    /**
     * Configure the Log class with a set of Properties.
     *
     * @param newProperties the properties
     */
    public static void configure(
            Properties newProperties )
    {
        if( newProperties == null || newProperties.isEmpty() ) {
            return;
        }

        String val = newProperties.getProperty( "StacktraceOnError" ); // default is yes
        if( val != null && ( val.startsWith( "n" ) || val.startsWith( "N" ) || val.startsWith( "F" ) || val.startsWith( "f" )) ) {
            logStacktraceOnError = false;
        } else {
            logStacktraceOnError = true;
        }

        val = newProperties.getProperty( "StacktraceOnWarn" ); // default is no
        if( val != null && ( val.startsWith( "y" ) || val.startsWith( "Y" ) || val.startsWith( "T" ) || val.startsWith( "t" )) ) {
            logStacktraceOnWarn = true;
        } else {
            logStacktraceOnWarn = false;
        }

        val = newProperties.getProperty( "StacktraceOnInfo" ); // default is no
        if( val != null && ( val.startsWith( "y" ) || val.startsWith( "Y" ) || val.startsWith( "T" ) || val.startsWith( "t" )) ) {
            logStacktraceOnInfo = true;
        } else {
            logStacktraceOnInfo = false;
        }

        val = newProperties.getProperty( "StacktraceOnDebug" ); // default is no
        if( val != null && ( val.startsWith( "y" ) || val.startsWith( "Y" ) || val.startsWith( "T" ) || val.startsWith( "t" )) ) {
            logStacktraceOnDebug = true;
        } else {
            logStacktraceOnDebug = false;
        }

        val = newProperties.getProperty( "StacktraceOnTraceCall" ); // default is no
        if( val != null && ( val.startsWith( "y" ) || val.startsWith( "Y" ) || val.startsWith( "T" ) || val.startsWith( "t" )) ) {
            logStacktraceOnTraceCall = true;
        } else {
            logStacktraceOnTraceCall = false;
        }
    }

    /**
     * Set a factory for new Log objects.
     *
     * @param newFactory the new LogFactory
     * @throws IllegalArgumentException thrown if a null is provided as a new factory
     */
    public static void setLogFactory(
            LogFactory newFactory )
    {
        if( newFactory == null ) {
            throw new NullPointerException( "Null factory not allowed" );
        }
        theFactory = newFactory;
    }

    /**
     * Obtain a Logger.
     *
     * @param clazz the Class for which we are looking for a Log object
     * @return the Log object
     */
    public static Log getLogInstance(
            Class clazz )
    {
        if( theFactory != null ) {
            return theFactory.create( clazz );
        } else {
            return null;
            // This can happen when Tomcat stops a web app and some classes get re-loaded after having been freed.
            // In the debugger, that will hit NullPointerException breakpoints and we don't like that.
        }
    }

    /**
     * Obtain a Logger.
     *
     * @param name name of the Log object that we are looking for
     * @return the Log object
     */
    public static Log getLogInstance(
            String name )
    {
        return theFactory.create( name );
    }

    /**
     * Obtain a Logger and specify a dumper factory instead of the default.
     *
     * @param clazz the Class for which we are looking for a Log object
     * @param dumperFactory the factory for dumpers
     * @return the Log object
     */
    public static Log getLogInstance(
            Class                  clazz,
            BufferingDumperFactory dumperFactory )
    {
        return theFactory.create( clazz, dumperFactory );
    }

    /**
     * Obtain a Logger.
     *
     * @param name name of the Log object that we are looking for
     * @param dumperFactory the factory for dumpers
     * @return the Log object
     */
    public static Log getLogInstance(
            String                 name,
            BufferingDumperFactory dumperFactory )
    {
        return theFactory.create( name, dumperFactory );
    }

    /**
     * Private constructor, use getLogInstance to get hold of an instance.
     *
     * @param name the name of the Log object
     * @param dumperFactory the DumperFactory to use, if any
     */
    protected Log(
            String                 name,
            BufferingDumperFactory dumperFactory )
    {
        theName          = name;
        theDumperFactory = dumperFactory;
    }

    /**
     * The method to log a fatal error. This must be implemented by subclasses.
     *
     * @param msg the message to log
     * @param t   the Throwable to log. This may be null.
     */
    protected abstract void logFatal(
            String    msg,
            Throwable t );

    /**
     * The method to log an error. This must be implemented by subclasses.
     *
     * @param msg the message to log
     * @param t   the Throwable to log. This may be null.
     */
    protected abstract void logError(
            String    msg,
            Throwable t );

    /**
     * The method to log a warning. This must be implemented by subclasses.
     *
     * @param msg the message to log
     * @param t   the Throwable to log. This may be null.
     */
    protected abstract void logWarn(
            String    msg,
            Throwable t );

    /**
     * The method to log an informational message. This must be implemented by subclasses.
     *
     * @param msg the message to log
     * @param t   the Throwable to log. This may be null.
     */
    protected abstract void logInfo(
            String    msg,
            Throwable t );

    /**
     * The method to log a debug message. This must be implemented by subclasses.
     *
     * @param msg the message to log
     * @param t   the Throwable to log. This may be null.
     */
    protected abstract void logDebug(
            String    msg,
            Throwable t );

    /**
     * The method to log a traceCall message. This must be implemented by subclasses.
     *
     * @param msg the message to log
     * @param t   the Throwable to log. This may be null.
     */
    protected abstract void logTrace(
            String    msg,
            Throwable t );

    /**
     * Determine whether logging to the info channel is enabled.
     *
     * @return true if the info channel is enabled
     */
    public abstract boolean isInfoEnabled();

    /**
     * Determine whether logging to the debug channel is enabled.
     *
     * @return true if the debug channel is enabled
     */
    public abstract boolean isDebugEnabled();

    /**
     * Determine whether logging to the trace channel is enabled.
     *
     * @return true if the trace channel is enabled
     */
    public abstract boolean isTraceEnabled();

    /**
     * If the <code>obj</code> parameter is <code>null</code>, or
     * if the <code>obj</code> is a boolean that is false, then
     * logs <code>args</code> as an error.
     *
     * @param obj the obj to check for
     * @param args the argument list
     */
    public final void assertLog(
            Object    obj,
            Object... args )
    {
        if( obj == null ) {
            error( "Assertion failed: object null", args );
        } else if( obj instanceof Boolean && !((Boolean)obj) ) {
            error( "Assertion failed: flag is false", args );
        }
    }

    /**
     * This logs a fatal error.
     *
     * @param args the argument list
     */
    public final void fatal(
            Object... args )
    {
        String    message = determineMessage(   true, args );
        Throwable t       = determineThrowable( true, args );

        logFatal( message, t );
    }

    /**
     * This logs an error.
     *
     * @param args the argument list
     */
    public final void error(
            Object... args )
    {
        String    message = determineMessage(   logStacktraceOnError, args );
        Throwable t       = determineThrowable( logStacktraceOnError, args );

        logError( message, t );
    }

    /**
     * This logs an error made by the user.
     *
     * @param message the user message
     */
    public final void userError(
            String message )
    {
        logError( message, null );
    }

    /**
     * This logs a warning.
     *
     * @param args the argument list
     */
    public final void warn(
            Object... args )
    {
        String    message = determineMessage(   logStacktraceOnWarn, args );
        Throwable t       = determineThrowable( logStacktraceOnWarn, args );

        logWarn( message, t );
    }

    /**
     * This logs an info message.
     *
     * @param args the argument list
     */
    public final void info(
            Object... args )
    {
        String    message = determineMessage(   logStacktraceOnInfo, args );
        Throwable t       = determineThrowable( logStacktraceOnInfo, args );

        logInfo( message, t );
    }

    /**
     * This logs a debug message.
     *
     * @param args the argument list
     */
    public final void debug(
            Object... args )
    {
        String    message = determineMessage(   logStacktraceOnDebug, args );
        Throwable t       = determineThrowable( logStacktraceOnDebug, args );

        logDebug( message, t );
    }

    /**
     * Logs an info message that represents a method invocation.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     * @param args the arguments to the method call
     */
    public final void infoMethodCallEntry(
            Object     subject,
            String     method,
            Object ... args )
    {
        String    message = determineMethodCallEntryMessage( logStacktraceOnTraceCall, subject, method, args );
        Throwable t       = determineThrowable(              logStacktraceOnTraceCall, args );

        logInfo( message, t );
    }

    /**
     * Logs an info message that represents a return from a method.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     */
    public final void infoMethodCallExit(
            Object     subject,
            String     method )
    {
        String message = determineMethodCallExitMessage( subject, method );
        logInfo( message, null );
    }

    /**
     * Logs a debug message that represents a method invocation.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     * @param args the arguments to the method call
     */
    public final void debugMethodCallEntry(
            Object     subject,
            String     method,
            Object ... args )
    {
        String    message = determineMethodCallEntryMessage( logStacktraceOnTraceCall, subject, method, args );
        Throwable t       = determineThrowable(              logStacktraceOnTraceCall, args );

        logDebug( message, t );
    }

    /**
     * Logs a debug message that represents a return from a method.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     */
    public final void debugMethodCallExit(
            Object     subject,
            String     method )
    {
        String message = determineMethodCallExitMessage( subject, method );
        logDebug( message, null );
    }

    /**
     * Logs a trace message that represents a method invocation.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     * @param args the arguments to the method call
     */
    public final void traceMethodCallEntry(
            Object     subject,
            String     method,
            Object ... args )
    {
        String    message = determineMethodCallEntryMessage( logStacktraceOnTraceCall, subject, method, args );
        Throwable t       = determineThrowable(              logStacktraceOnTraceCall, args );

        logTrace( message, t );
    }

    /**
     * Logs a trace message that represents a return from a method.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     */
    public final void traceMethodCallExit(
            Object     subject,
            String     method )
    {
        String message = determineMethodCallExitMessage( subject, method );
        logTrace( message, null );
    }

    /**
     * Logs a trace message that represents a return from a method.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     * @param ret the return value
     */
    public final void traceMethodCallExit(
            Object     subject,
            String     method,
            Object     ret )
    {
        String message = determineMethodCallExitMessage( subject, method, ret );
        logTrace( message, null );
    }

    /**
     * Logs a trace message that represents a constructor invocation.
     *
     * @param subject the instance that was created by the constructor
     * @param args the arguments to the method call
     */
    public final void traceConstructor(
            Object     subject,
            Object ... args )
    {
        String    message = determineMessage(   logStacktraceOnTraceCall, args );
        Throwable t       = determineThrowable( logStacktraceOnTraceCall, args );

        StringBuilder buf = new StringBuilder();
        buf.append( "Constructed " );
        buf.append( subject );
        if( message != null ) {
            buf.append( ":" );
            buf.append( message );
        }
        logTrace( buf.toString(), t );
    }

    /**
     * Logs a trace message that indicates object finalization.
     *
     * @param subject the instance being finalized
     */
    public final void traceFinalization(
            Object subject )
    {
        StringBuilder buf = new StringBuilder();
        buf.append( "Finalized " );
        buf.append( subject );
        logTrace( buf.toString(), null );
    }

    /**
     * Given an argument list, determine the message.
     *
     * @param logStacktrace log the tracktrace even if no Throwable is given in the argument list
     * @param args the argument list
     * @return the message, if any
     */
    @SuppressWarnings("unchecked")
    protected String determineMessage(
            boolean   logStacktrace,
            Object... args )
    {
        try {
            if( args == null || args.length == 0 ) {
                return "";
            }

            int startIndex = 0;
            BufferingDumperFactory factory;
            if( args[ startIndex ] instanceof BufferingDumperFactory ) {
                factory = (BufferingDumperFactory) args[ startIndex ];
                ++startIndex;
            } else {
                factory = theDumperFactory;
            }

            BufferingDumper dumper = factory.obtainFor( this );
            if( dumper == null ) {
                return "Cannot find Dumper";
            }

            if( args.length == 1 + startIndex && args[startIndex] instanceof String ) {
                return (String) args[startIndex]; // special case of a simple informational message

            } else if( args.length == 2 + startIndex && ( args[startIndex] instanceof String ) && ( args[startIndex+1] instanceof Object[] ) && ((Object[])args[startIndex+1]).length == 0 ) {
                return (String) args[startIndex]; // special case of a method invocation with no arguments

            } else {
                Object [] toDump;
                if( startIndex == 0 ) {
                    toDump = args;
                } else {
                    toDump = ArrayHelper.copyIntoNewArray( args, startIndex, args.length, Object.class );
                }
                if( toDump.length != 1 ) {
                    dumper.dump( toDump );
                } else {
                    dumper.dump( toDump[0] );
                }
                return dumper.getBuffer();
            }

        } catch( FactoryException ex ) {
            error( ex );
            return "Cannot create Dumper";
        }
    }

    /**
     * Given information about a call entry, determine the message.
     *
     * @param logStacktrace log the tracktrace even if no Throwable is given in the argument list
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     * @param args the arguments to the method call
     * @return the message
     */
    protected String determineMethodCallEntryMessage(
            boolean   logStacktrace,
            Object    subject,
            String    method,
            Object... args )
    {
        String normalMessage = determineMessage( logStacktrace, args );

        StringBuilder buf = new StringBuilder();
        buf.append( "Entered " );
        buf.append( subject );
        buf.append( " ." );
        buf.append( method );
        if( normalMessage != null ) {
            buf.append( ":" );
            buf.append( normalMessage );
        }
        return buf.toString();
    }

    /**
     * Given information about a call return, determine the message.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     * @return the message
     */
    protected String determineMethodCallExitMessage(
            Object    subject,
            String    method )
    {
        StringBuilder buf = new StringBuilder();
        buf.append( "Exited " );
        buf.append( subject );
        buf.append( " ." );
        buf.append( method );

        return buf.toString();
    }

    /**
     * Given information about a call return, determine the message.
     *
     * @param subject the object on which the method was invoked
     * @param method the name of the method being invoked
     * @param ret the return valkuye
     * @return the message
     */
    protected String determineMethodCallExitMessage(
            Object    subject,
            String    method,
            Object    ret )
    {
        StringBuilder buf = new StringBuilder();
        buf.append( "Exited " );
        buf.append( subject );
        buf.append( " ." );
        buf.append( method );
        buf.append( ", returning " );

        try {
            BufferingDumper dumper = theDumperFactory.obtainFor( this );
            if( dumper == null ) {
                return "Cannot find Dumper";
            }
            dumper.dump( ret );
            buf.append( dumper.getBuffer() );

            return buf.toString();

        } catch( FactoryException ex ) {
            error( ex );
            return "Cannot create Dumper";
        }
    }

    /**
     * Given an argument list, determine the Throwable to log.
     *
     * @param logStacktrace log the tracktrace even if no Throwable is given in the argument list
     * @param args the argument list
     * @return the Throwable, if any
     */
    protected Throwable determineThrowable(
            boolean   logStacktrace,
            Object... args )
    {
        Object lastArgument;
        if( args != null && args.length > 0 ) {
            lastArgument= args[ args.length-1 ];
        } else {
            lastArgument = null;
        }
        if( lastArgument instanceof Throwable ) {
            return (Throwable) lastArgument;
        }
        if( logStacktrace ) {
            return new Exception( "Logging marker" );
        }
        return null;
    }

    /**
     * The name of this logger.
     */
    protected String theName;

    /**
     * The factory for creating Dumper objects to use for dumping objects, if any.
     */
    protected BufferingDumperFactory theDumperFactory;

    /**
     * The factory for new Log objects.
     */
    private static LogFactory theFactory = new BasicLog.Factory();

    /**
     * This specifies whether a stack trace shall be logged with every error.
     */
    private static boolean logStacktraceOnError = false;

    /**
     * This specifies whether a stack trace shall be logged with every warning.
     */
    private static boolean logStacktraceOnWarn = false;

    /**
     * This specifies whether a stack trace shall be logged with every info.
     */
    private static boolean logStacktraceOnInfo = false;

    /**
     * This specifies whether a stack trace shall be logged with every debug.
     */
    private static boolean logStacktraceOnDebug = false;

    /**
     * This specifies whether a stack trace shall be logged with every traceCall.
     */
    private static boolean logStacktraceOnTraceCall = false;
}
