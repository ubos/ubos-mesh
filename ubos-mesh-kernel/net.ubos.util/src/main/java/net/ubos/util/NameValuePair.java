//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util;

/**
 * A helper class that represents a pair of name and value.
 * 
 * @param <V> the type of value
 */
public class NameValuePair<V>
        extends
            Pair<String,V>
{
    /**
      * Construct one.
      *
      * @param name the name
      * @param value the value
      */
    public NameValuePair(
            String name,
            V      value )
    {
        super( name, value );
    }
}
