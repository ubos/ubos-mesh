//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.smartmap;

import net.ubos.util.cursoriterator.CursorIterator;

/**
 * The concept of the map we use when the amount of data may be too large
 * to fit into memory.
 * It has a variety of implementation classes, which manage the data in memory
 * or dynamically by loading from disk as needed.
 *
 * @param <K> the key for the entries in the map
 * @param <V> the value for the entries of the map
 */
public interface SmartMap<K,V>
{
    /**
     * Determine whether this SmartMap is persistent.
     *
     * @return true if it is persistent
     */
    public abstract boolean isPersistent();

    /**
     * Empty the entire SmartMap.
     */
    public abstract void clear();

    /**
     * Determine whether this SmartMap contains a key.
     *
     * @param key the key
     * @return true of the map contains the key
     */
    public boolean containsKey(
            K key );

    /**
     * Obtain the value for this key, or null if it does not exist.
     *
     * @param key the key
     * @return the value
     */
    public V get(
            K key );

    /**
     * Put a key-value pair into the SmartMap. This will overwrite if
     * the key exists already. Return the overwritten value, or null
     * if the key does not exist
     *
     * @param key the key
     * @param value the value
     */
    default public V put(
            K key,
            V value )
    {
        V ret = get( key );
        putIgnorePrevious( key, value );
        return ret;
    }

    /**
     * Put a key-value pair into the SmartMap. This will overwrite if
     * the key exists already.
     *
     * @param key the key
     * @param value the value
     */
    public void putIgnorePrevious(
            K key,
            V value );

    /**
     * Remove a key and the associated value from this SmartMap.
     * Return the removed value, or null if the key does not exist.
     *
     * @param key the key
     * @return the removed value
     */
    default public V remove(
            K key )
    {
        V ret = get( key );
        removeIgnorePrevious( key );
        return ret;
    }

    /**
     * Remove a key and the associated value from this SmartMap.
     * Does nothing if the key does not exist.
     *
     * @param key the key
     */
    public void removeIgnorePrevious(
            K key );

    /**
     * Obtain a CursorIterator over the keys in this SmartMap.
     *
     * @return the CursorIterator
     */
    public CursorIterator<K> keyIterator();

    /**
     * Obtain a CursorIterator over the values in this SmartMap.
     *
     * @return the CursorIterator
     */
    public CursorIterator<V> valueIterator();

    /**
     * Tell this SmartMap that the value for this key has been updated.
     * This allows a persistent SmartMap to flush the new value to disk.
     *
     * @param key the key
     * @param value the value
     */
    public void valueUpdated(
            K key,
            V value );

    /**
     * Determine the size of the SmartMap.
     *
     * @return the size
     */
    public int size();

    /**
     * Is this SmartMap empty.
     *
     * @return true if empty
     */
    public boolean isEmpty();
}
