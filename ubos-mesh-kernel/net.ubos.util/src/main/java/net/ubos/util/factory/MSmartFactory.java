//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.factory;

import java.util.function.Function;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.exception.ObjectExistsAlreadyFactoryException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SmartMap;

/**
 * A simple {@link SmartFactory} implementation that stores the set of previously created
 * objects in memory. The {@link #obtainFor} method in this
 * class assumes that object creation by the delegate {@link Factory} is fast. If it is not,
 * use {@link PatientSmartFactory PatientSmartFactory} instead of this class.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 * @param <A> the type of argument
 */
public class MSmartFactory<K,V,A>
        implements
            SmartFactory<K,V,A>,
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( MSmartFactory.class ); // our own, private logger

    /**
     * Factory method for SmartFactory that stores its objects in the provided storage object.
     *
     * @param delegateFactory the Factory that knows how to instantiate values
     * @param storage the storage to use
     * @return the created SmartFactory
     * @param <K> the type of key
     * @param <V> the type of value
     * @param <A> the type of argument
     */
    public static <K,V,A> MSmartFactory<K, V, A> create(
            Factory<K,V,A>  delegateFactory,
            SmartMap<K,V>   storage )
    {
        return new MSmartFactory<>( delegateFactory, storage );
    }

    /**
     * Constructor.
     *
     * @param delegateFactory the Factory that knows how to instantiate values
     * @param storage the storage to use for this instance
     */
    public MSmartFactory(
            Factory<K,V,A> delegateFactory,
            SmartMap<K,V>  storage )
    {
        theDelegateFactory  = delegateFactory;
        theKeyValueMap      = storage;
    }

    /**
     * Obtain the delegate Factory that knows how to instantiate values.
     *
     * @return the delegate Factory
     */
    public Factory<K,V,A> getDelegateFactory()
    {
        return theDelegateFactory;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V get(
            K key )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "get", key );
        }
        synchronized( theKeyValueMap ) {
            return theKeyValueMap.get( key );
        }
    }

    /**
     * {@inheritDoc]
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public V obtainFor(
            K key,
            A argument )
        throws
            FactoryException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "obtainFor", key, argument );
        }

        V ret;
        boolean weAreCreating = false;

        synchronized( theKeyValueMap ) {
            ret = theKeyValueMap.get( key );
            if( ret == null && theDelegateFactory != null ) {
                weAreCreating = true;
                ret = theDelegateFactory.obtainFor( key, argument );

                theKeyValueMap.putIgnorePrevious( key, ret );
            }
            if( ret instanceof FactoryCreatedObject ) {
                FactoryCreatedObject<K,V,A> realRet = (FactoryCreatedObject<K,V,A>) ret;
                if( realRet.getFactory() == null ) {
                    realRet.setFactory( this );
                }
            }
        }
        if( weAreCreating ) {
            createdHook( key, ret, argument );
        }
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V obtainNewFor(
            K key,
            A argument )
        throws
            ObjectExistsAlreadyFactoryException,
            FactoryException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "obtainNewFor", key, argument );
        }

        V ret;
        boolean weAreCreating = false;

        synchronized( theKeyValueMap ) {
            ret = theKeyValueMap.get( key );
            if( ret == null && theDelegateFactory != null ) {
                weAreCreating = true;
                ret = theDelegateFactory.obtainFor( key, argument );

                theKeyValueMap.putIgnorePrevious( key, ret );
            } else {
                throw new ObjectExistsAlreadyFactoryException( this, ret );
            }
            if( ret instanceof FactoryCreatedObject ) {
                @SuppressWarnings( "unchecked" )
                FactoryCreatedObject<K,V,A> realRet = (FactoryCreatedObject<K,V,A>) ret;
                if( realRet.getFactory() == null ) {
                    realRet.setFactory( this );
                }
            }
        }
        if( weAreCreating ) {
            createdHook( key, ret, argument );
        }
        return ret;
    }

    /**
     * This overridable method allows our subclasses to invoke particular functionality
     * every time this SmartFactory created a new value by invoking the delegate Factory.
     * It is not invoked for those returned values that are merely retrieved from
     * the storage in the smart factory.
     *
     * @param key the key of the newly created value
     * @param value the newly created value
     * @param argument the argument into the creation of the newly created value
     */
    protected void createdHook(
            K key,
            V value,
            A argument )
    {
        // noop on this level
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void putIgnorePrevious(
            K key,
            V value )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "put", key, value );
        }

        synchronized( theKeyValueMap ) {
            theKeyValueMap.putIgnorePrevious( key, value );
        }
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public V remove(
            K                key,
            Function<V,Void> cleanupCode )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "remove", key, cleanupCode );
        }

        V ret;
        synchronized( theKeyValueMap ) {
            ret = theKeyValueMap.remove( key );

            if( cleanupCode != null && ret != null ) {
                cleanupCode.apply( ret );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void removeIgnorePrevious(
            K key )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "removeIgnorePrevious", key );
        }

        synchronized( theKeyValueMap ) {
            theKeyValueMap.remove( key );
        }
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public boolean isEmpty()
    {
        return theKeyValueMap.isEmpty();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public final int size()
    {
        synchronized( theKeyValueMap ) {
            return theKeyValueMap.size();
        }
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        return theKeyValueMap.keyIterator();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public CursorIterator<V> valueIterator()
    {
        return theKeyValueMap.valueIterator();
    }

    /**
     * {@inheritDoc]
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public void factoryCreatedObjectUpdated(
            FactoryCreatedObject<K,V,A> object )
    {
        theKeyValueMap.valueUpdated( object.getFactoryKey(), (V) object ); // FIXME? This typecast can probably be done better
    }

    /**
     * Obtain the underlying storage CachingMap.
     *
     * @return the underlying storage CachingMap
     */
    public SmartMap<K,V> getStorage()
    {
        return theKeyValueMap;
    }

    /**
     * {@inheritDoc]
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theDelegateFactory",
                    "theKeyValueMap"
                },
                new Object[] {
                    theDelegateFactory,
                    theKeyValueMap
                } );
    }

    /**
     * The delegate Factory.
     */
    protected Factory<K,V,A> theDelegateFactory;

    /**
      * Our current values, keyed by our keys.
      */
    protected final SmartMap<K,V> theKeyValueMap;
}
