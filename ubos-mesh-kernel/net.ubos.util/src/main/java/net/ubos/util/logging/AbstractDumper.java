//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.logging;

import java.lang.ref.Reference;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import net.ubos.util.ArrayHelper;
import net.ubos.util.ResourceHelper;

/**
 * Factors out common functionality of Dumper implementations.
 *
 * It's important that all methods invoked directly dump at least themselves, and check
 * about their children before invoking them to be dumped.
 */
public abstract class AbstractDumper
        implements
            Dumper
{
    /**
     * Constructor, for subclasses only.
     *
     * @param maxLevel the number of object levels to dump
     * @param maxArrayLength the maximum number of array entries to dump
     */
    protected AbstractDumper(
            int maxLevel,
            int maxArrayLength )
    {
        theLevel          = 0;
        theMaxLevel       = maxLevel;
        theMaxArrayLength = maxArrayLength;
    }

    /**
     * Obtain the maximum number of object levels to dump.
     *
     * @return the maximum number
     */
    public int getMaxLevel()
    {
        return theMaxLevel;
    }

    /**
     * Obtain the current object level.
     *
     * @return the current object level
     */
    public int getLevel()
    {
        return theLevel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Object obj )
    {
        if( obj == null )  {
            dumpNull();

        } else if( obj instanceof CanBeDumped ) {
            dumpCanBeDumped( (CanBeDumped) obj );

        } else if( obj instanceof Object[] ) {
            dumpArray( (Object []) obj );

        } else if( obj instanceof Collection ) {
            dumpCollection( (Collection) obj );

        } else if( obj instanceof Map ) {
            dumpMap( (Map) obj );

        } else if( obj instanceof boolean[] ) {
            dumpBooleanArray( (boolean []) obj );

        } else if( obj instanceof byte[] ) {
            dumpByteArray( (byte []) obj );

        } else if( obj instanceof short[] ) {
            dumpShortArray( (short []) obj );

        } else if( obj instanceof int[] ) {
            dumpIntArray( (int []) obj );

        } else if( obj instanceof long[] ) {
            dumpLongArray( (long []) obj );

        } else if( obj instanceof float[] ) {
            dumpFloatArray( (float []) obj );

        } else if( obj instanceof double[] ) {
            dumpDoubleArray( (double []) obj );

        } else if( obj instanceof char[] ) {
            dumpCharArray( (char []) obj );

        } else if( obj instanceof Boolean ) {
            dumpBoolean( (Boolean) obj );

        } else if( obj instanceof Character ) {
            dumpCharacter( (Character) obj );

        } else if( obj instanceof Date ) {
            dumpDate( (Date) obj );

        } else if( obj instanceof Number ) {
            dumpNumber( (Number) obj );

        } else if( obj instanceof CharSequence ) {
            dumpCharSequence( (CharSequence) obj );

        } else if( obj instanceof Reference ) {
            dumpReference( (Reference) obj );

        } else if( obj instanceof Throwable ) {
            dumpThrowable( (Throwable) obj );

        } else {
            dumpObject( obj );
        }
    }

    /**
     * Dump a regular object.
     *
     * @param obj the object to dump
     */
    public void dumpObject(
            Object obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            // let's try what we can
            Field [] fields = obj.getClass().getFields();
            ArrayList<String> names  = new ArrayList<>();
            ArrayList<Object> values = new ArrayList<>();

            for( int i=0 ; i<fields.length ; ++i ) {
                if( Modifier.isStatic( fields[i].getModifiers())) {
                    continue; // we don't want static
                }
                try {
                    Object value = fields[i].get( obj );

                    names.add( fields[i].getName() );
                    values.add( value );

                } catch( Throwable t ) {
                    if( getLog().isDebugEnabled() ) {
                        getLog().debug( t );
                    }
                }
            }

            if( !names.isEmpty() ) {
                String [] names2  = ArrayHelper.copyIntoNewArray( names, String.class );
                Object [] values2 = ArrayHelper.copyIntoNewArray( values, Object.class );

                dump( obj, names2, values2 );
            } else {
                dumpWithToString( obj );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Object    obj,
            String [] fieldNames,
            Object [] fieldValues )
    {
        dump( obj, fieldNames, fieldValues, null );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Object        obj,
            Map<String,?> namedFields )
    {
        dump( obj, null, null, namedFields );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Object        obj,
            String []     fieldNames,
            Object []     fieldValues,
            Map<String,?> namedFields )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emitObjectId( obj );

            emit( "{" );
            int min = Math.min(
                    fieldNames  != null ? fieldNames.length  : 0,
                    fieldValues != null ? fieldValues.length : 0 );

            try {
                ++theLevel;

                String sep = "";
                for( int i=0 ; i<min ; ++i ) {
                    Object value = fieldValues[i];

                    if( shouldBeDumpedFull( value )) {
                        emit( sep );
                        emit( '\n' );
                        emit( fieldNames[i] );
                        emit( ": ");
                        dump( value );

                        sep = ",";

                    } else if( shouldBeDumpedShort( value )) {
                        emit( sep );
                        emit( '\n' );
                        emit( fieldNames[i] );
                        emit( ": ");
                        emitObjectId( value );
                        emit( "..." );

                        sep = ",";
                    }
                }
                if( namedFields != null ) {
                    for( Map.Entry<String,?> namedField : namedFields.entrySet() ) {
                        Object value = namedField.getValue();

                        emit( sep );
                        sep = ",";
                        if( shouldBeDumpedFull( value )) {
                            emit( '\n' );
                            emit( namedField.getKey() );
                            emit( ": ");
                            dump( value );

                        } else if( shouldBeDumpedShort( value )) {
                            emit( '\n' );
                            emit( namedField.getKey() );
                            emit( ": ");
                            emitObjectId( value );
                            emit( "..." );
                        }
                    }
                }

            } finally {
                --theLevel;
            }
            if(    ( fieldNames  != null && fieldNames.length  != min )
                || ( fieldValues != null && fieldValues.length != min ))
            {
                getLog().error( "non-matching field names and values in toString method" );
            }
            emit( "\n}" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpNull()
    {
        emit( "null" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpCanBeDumped(
            CanBeDumped obj )
    {
        obj.dump( this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpArray(
            Object [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emitObjectId( obj );
            if( obj.length == 0 ) {
                emit( "[0] = {}" );
            } else {
                emit( '[' );
                emit( String.valueOf( obj.length ));
                emit( "] = {" );

                try {
                    ++theLevel;

                    int max = Math.min( theMaxArrayLength, obj.length );

                    for( int j=0 ; j<max ; ++j ) {
                        emit( '\n' );
                        dump( obj[j] );

                        if( j < max-1 ) {
                            emit( "," );
                        }
                    }
                    if( max != obj.length ) {
                        emit( "\n..." );
                    }
                } finally {
                    --theLevel;
                }
                emit( "\n}" );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpCollection(
            Collection<?> obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emitObjectId( obj );
            if( obj.isEmpty() ) {
                emit( "[0] = {}" );
            } else {
                emit( '[' );
                emit( String.valueOf( obj.size() ));
                emit( "] = {" );

                try {
                    ++theLevel;

                    int max = Math.min( theMaxArrayLength, obj.size() );
                    int j   = 0;

                    for( Object current : obj ) {
                        emit( '\n' );
                        dump( current );

                        if( ++j >= max ) {
                            break;
                        }
                    }
                    if( max != obj.size() ) {
                        emit( "\n..." );
                    }
                } finally {
                    --theLevel;
                }
                emit( "\n}" );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpMap(
            Map<?,?> obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emitObjectId( obj );

            if( obj.isEmpty() ) {
                emit( "[0] = {}" );
            } else {
                emit( '[' );
                emit( String.valueOf( obj.size() ));
                emit( "] = {" );

                try {
                    ++theLevel;

                    int max = Math.min( theMaxArrayLength, obj.size() );
                    int j   = 0;
                    for( Object from : obj.keySet() ) {
                        emit( '\n' );

                        Object to = obj.get( from );
                        dump( from );
                        emit( " : " );
                        dump( to );

                        if( ++j >= max ) {
                            break;
                        }
                    }
                    if( max != obj.size() ) {
                        emit( "\n..." );
                    }
                } finally {
                    --theLevel;
                }
                emit( "\n}" );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpBooleanArray(
            boolean [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "boolean[" );
            emit( String.valueOf( obj.length ));
            emit( "] = { " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( obj[j] ? "true" : "false" );
                if( j < max-1 ) {
                    emit( ", " );
                }
            }
            if( max < obj.length ) {
                emit( "..." );
            }
            emit( " }" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpByteArray(
            byte [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "byte[" );
            emit( String.valueOf( obj.length ));
            emit( "] = " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( Character.forDigit( ( obj[j] >> 4 ) & 0xf, 16 ));
                emit( Character.forDigit( obj[j] & 0xf, 16 ));
            }
            if( max < obj.length ) {
                emit( "..." );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpShortArray(
            short [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "short[" );
            emit( String.valueOf( obj.length ));
            emit( "] = { " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( String.valueOf( obj[j] ));
                if( j < max-1 ) {
                    emit( ", " );
                }
            }
            if( max < obj.length ) {
                emit( "..." );
            }
            emit( " }" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpIntArray(
            int [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "int[" );
            emit( String.valueOf( obj.length ));
            emit( "] = { " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( String.valueOf( obj[j] ));
                if( j < max-1 ) {
                    emit( ", " );
                }
            }
            if( max < obj.length ) {
                emit( "..." );
            }
            emit( " }" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpLongArray(
            long [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "long[" );
            emit( String.valueOf( obj.length ));
            emit( "] = { " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( String.valueOf( obj[j] ));
                if( j < max-1 ) {
                    emit( ", " );
                }
            }
            if( max < obj.length ) {
                emit( "..." );
            }
            emit( " }" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpFloatArray(
            float [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "float[" );
            emit( String.valueOf( obj.length ));
            emit( "] = { " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( String.valueOf( obj[j] ));
                if( j < max-1 ) {
                    emit( ", " );
                }
            }
            if( max < obj.length ) {
                emit( "..." );
            }
            emit( " }" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpDoubleArray(
            double [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "double[");
            emit( String.valueOf( obj.length ));
            emit( "] = { " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( String.valueOf( obj[j] ));
                if( j < max-1 ) {
                    emit( ", " );
                }
            }
            if( max < obj.length ) {
                emit( "..." );
            }
            emit( " }" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpCharArray(
            char [] obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            registerAsDumped( obj );
            emit( "char[" );
            emit( String.valueOf( obj.length ));
            emit( "] = { " );

            int max = Math.min( theMaxArrayLength, obj.length );
            for( int j=0 ; j<max ; ++j ) {
                emit( '\'' );
                emit( obj[j] );
                emit( '\'' );
                if( j < max-1 ) {
                    emit( ", " );
                }
            }
            if( max < obj.length ) {
                emit( "..." );
            }
            emit( " }" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpBoolean(
            Boolean obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            // don't register as dumped -- it's short enough to do it again
            if( obj.booleanValue() ) {
                emit( "true" );
            } else {
                emit( "false" );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpCharacter(
            Character obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            // don't register as dumped -- it's short enough to do it again
            emit( '\'' );
            emit( obj );
            emit( '\'' );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpDate(
            Date obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            // don't register as dumped -- it's short enough to do it again
            String ret = theDateFormat.format( obj );
            emit( ret );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpNumber(
            Number obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            // don't register as dumped -- it's short enough to do it again
            emit( obj.getClass().getName() );
            emit( ": " );
            emit( obj.toString() );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpCharSequence(
            CharSequence obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            // don't register as dumped -- it's short enough to do it again
            emit( obj.getClass().getName() );
            emit( ": \"" );
            emit( obj );
            emit( "\"" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpReference(
            Reference<?> obj )
    {
        Object found = obj.get();

        dump( obj, new String[] { "ref" }, new Object[] { found } );
    }

    /**
     * Dump an exception.
     *
     * @param obj the object to dump
     */
    public void dumpThrowable(
            Throwable obj )
    {
        if( obj == null ) {
            dumpNull();

        } else {
            // don't register as dumped -- it's short enough to do it again
            String message = obj.getMessage();
            if( message == null ) {
                message = obj.getLocalizedMessage();
            }

            emit( obj.getClass().getName() );
            emit( ": " );
            if( message != null ) {
                emit( message );
            } else {
                emit( "<empty message>" );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dumpWithToString(
            Object obj )
    {
        if( obj == null ) {
            dumpNull();
        } else {
            // don't register as dumped -- it's short enough to do it again
            String to = obj.toString();
            emit( to );
        }
    }

    /**
     * Dump an object identifier.
     *
     * @param obj the Object
     */
    public void emitObjectId(
            Object obj )
    {
        emit( obj.getClass().getName() );
        emit( '@' );
        emit( Integer.toHexString( obj.hashCode() ));
    }

    /**
     * Create an object's object id.
     *
     * @param obj the Object
     * @return the object id
     */
    public String objectId(
            Object obj )
    {
        StringBuilder buf = new StringBuilder();
        buf.append( obj.getClass().getName() );
        buf.append( '@' );
        buf.append( Integer.toHexString( obj.hashCode() ));

        return buf.toString();
    }

    /**
     * Emit a CharSequence that was put together as part of dumping.
     *
     * @param toEmit the String to emit
     */
    protected abstract void emit(
            CharSequence toEmit );

    /**
     * Emit a single character that was put together as part of dumping.
     *
     * @param toEmit the character to emit
     */
    protected abstract void emit(
            char toEmit );

    /**
     * Determine whether an object should be dumped with all details.
     *
     * @param obj the Object to be tested
     * @return true if the Object should be dumped with all details
     */
    protected abstract boolean shouldBeDumpedFull(
            Object obj );

    /**
     * Determine whether an object should be dumped with minimal details.
     *
     * @param obj the Object to be tested
     * @return true if the Object should be dumped with minimum details
     */
    protected abstract boolean shouldBeDumpedShort(
            Object obj );

    /**
     * Register an object as having been dumped already.
     *
     * @param obj the Object to be registered
     */
    protected abstract void registerAsDumped(
            Object obj );

    /**
     * Obtain the logger to use.
     *
     * @return the Log
     */
    protected Log getLog()
    {
         return Log.getLogInstance( getClass() ); // our own, private logger
    }

    /**
     * The maximum level of dumping.
     */
    protected int theMaxLevel;

    /**
     * The maximum number of array entries to dump.
     */
    protected int theMaxArrayLength;

    /**
     * The level of dumping at which the Dumper is currently operating.
     */
    protected int theLevel = 0;

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( AbstractDumper.class );

    /**
     * The date format to use for dumping Dates.
     */
    public static final DateFormat theDateFormat = new SimpleDateFormat(
            theResourceHelper.getResourceStringOrDefault(
                    "DateFormat",
                    "yyyy-MM-dd HH:mm:ss.S" ));
}
