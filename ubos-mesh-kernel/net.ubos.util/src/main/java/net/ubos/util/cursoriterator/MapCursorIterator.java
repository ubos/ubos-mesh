//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.cursoriterator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import net.ubos.util.ArrayHelper;

/**
 * Provides {@link CursorIterator}s for the keys and values of <code>java.util.HashMap</code>s.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 */
public abstract class MapCursorIterator<K,V>
{
    /**
     * Factory method to create an iterator over the keys of a map.
     *
     * @param map the underlying map to iterate over
     * @return the created MapCursorIterator, iterating over the keys
     * @param <K> the type of key
     * @param <V> the type of value
     */
    @SuppressWarnings(value={"unchecked"})
    public static <K,V> Keys<K,V> createForKeys(
            Map<K,? extends V> map )
    {
        Map.Entry<K,? extends V> [] entries = (Map.Entry<K,V> []) ArrayHelper.createArray( Map.Entry.class, map.size() );

        entries = map.entrySet().toArray( entries );

        return new Keys<>( map, entries, ArrayCursorIterator.create( entries ));
    }

    /**
     * Factory method to create an iterator over the values of a map.
     *
     * @param map the underlying map to iterate over
     * @return the created MapCursorIterator, iterating over the values
     * @param <K> the type of key
     * @param <V> the type of value
     */
    @SuppressWarnings(value={"unchecked"})
    public static <K,V> Values<K,V> createForValues(
            Map<K,? extends V> map )
    {
        Map.Entry<K,? extends V> [] entries = (Map.Entry<K,V> []) ArrayHelper.createArray( Map.Entry.class, map.size() );

        entries = map.entrySet().toArray( entries );

        return new Values<>( map, entries, ArrayCursorIterator.create( entries ));
    }

    /**
     * Constructor.
     *
     * @param map the Map to iterate over
     * @param entries the sorted list of entries in the map
     * @param delegate the iterator over the entries
     */
    protected MapCursorIterator(
            Map<K,? extends V>                            map,
            Map.Entry<K,? extends V> []                   entries,
            ArrayCursorIterator<Map.Entry<K,? extends V>> delegate )
    {
        theMap      = map;
        theEntries  = entries;
        theDelegate = delegate;
    }

    /**
     * Obtain the next element, without iterating forward.
     *
     * @return the next element
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    protected Map.Entry<K,? extends V> getPeekNext()
    {
        return theDelegate.peekNext();
    }

    /**
     * Obtain the previous element, without iterating backward.
     *
     * @return the previous element
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    protected Map.Entry<K,? extends V> getPeekPrevious()
    {
        return theDelegate.peekPrevious();
    }

    /**
     * Obtain the next N elements, without iterating forward.
     *
     * @param n the number of elements to return
     * @return the next N elements
     */
    protected List<Map.Entry<K,? extends V>> getPeekNext(
            int n )
    {
        return theDelegate.peekNext( n );
    }

    /**
     * Obtain the previous N elements, without iterating backward.
     *
     * @param n the number of elements to return
     * @return the previous N elements
     */
    protected List<Map.Entry<K,? extends V>> getPeekPrevious(
            int n )
    {
        return theDelegate.peekPrevious( n );
    }

    /**
     * Returns <code>true</code> if the iteration has more elements in the forward direction.
     *
     * @return <code>true</code> if the iterator has more elements in the forward direction.
     * @see #hasPrevious()
     * @see #hasPrevious(int)
     * @see #hasNext(int)
     */
    public boolean hasNext()
    {
        return theDelegate.hasNext();
    }

    /**
     * Returns <code>true</code> if the iteration has more elements in the backward direction.
     *
     * @return <code>true</code> if the iterator has more elements in the backward direction.
     * @see #hasNext()
     * @see #hasPrevious(int)
     * @see #hasNext(int)
     */
    public boolean hasPrevious()
    {
        return theDelegate.hasPrevious();
    }

    /**
     * Returns <code>true</code> if the iteration has at least N more elements in the forward direction.
     *
     * @param n the number of elements for which to check
     * @return <code>true</code> if the iterator has at least N more elements in the forward direction.
     * @see #hasNext()
     * @see #hasPrevious()
     * @see #hasPrevious(int)
     */
    public boolean hasNext(
            int n )
    {
        return theDelegate.hasNext( n );
    }

    /**
     * Returns <code>true</code> if the iteration has at least N more elements in the backward direction.
     *
     * @param n the number of elements for which to check
     * @return <code>true</code> if the iterator has at least N more elements in the backward direction.
     * @see #hasNext()
     * @see #hasPrevious()
     * @see #hasNext(int)
     */
    public boolean hasPrevious(
            int n )
    {
        return theDelegate.hasPrevious( n );
    }

    /**
     * Move the cursor by N positions. Positive numbers indicate forward movemement;
     * negative numbers indicate backward movement.
     *
     * @param n the number of positions to move
     * @throws NoSuchElementException if the position does not exist
     */
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        theDelegate.moveBy( n );
    }

    /**
     * Obtain the current position, starting the count with 0 for the first element.
     *
     * @return the current position
     */
    public int getPosition()
    {
        return theDelegate.getPosition();
    }

    /**
     * Set this CursorIterator to a particular position.
     *
     * @param n the position to set this CursorIterator to
     * @throws NoSuchElementException thrown if this position was not available
     */
    public void setPosition(
            int n )
        throws
            NoSuchElementException
    {
        theDelegate.setPosition( n );
    }


    /**
     * Removes from the underlying collection the last element returned by the
     * iterator (optional operation). This is the same as the current element.
     *
     * @throws UnsupportedOperationException if the <code>remove</code>
     *        operation is not supported by this Iterator.

     * @throws IllegalStateException if the <code>next</code> method has not
     *        yet been called, or the <code>remove</code> method has already
     *        been called after the last call to the <code>next</code>
     *        method.
     */
    public void remove()
    {
        theDelegate.remove();
    }

    /**
      * Do we have more elements?
      *
      * @return true if we have more elements
      */
    public final boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * Obtain the next entry.
     *
     * @return the next entry
     */
    protected Map.Entry<K,? extends V> getNext()
    {
        return theDelegate.next();
    }

    /**
     * Obtain the next N entries.
     *
     * @param n the number of entries to return
     * @return the next N entries
     */
    protected List<Map.Entry<K,? extends V>> getNext(
            int n )
    {
        return theDelegate.next( n );
    }

    /**
     * Obtain the previous entry.
     *
     * @return the previous entry
     */
    protected Map.Entry<K,? extends V> getPrevious()
    {
        return theDelegate.previous();
    }

    /**
     * Obtain the previous N entries.
     *
     * @param n the number of entries to return
     * @return the previous N entries
     */
    protected List<Map.Entry<K,? extends V>> getPrevious(
            int n )
    {
        return theDelegate.previous( n );
    }

    /**
     * Helper method to get the keys of a list of Entries.
     *
     * @param entries the entries
     * @return the keys of the entries
     */
    protected List<K> keysOf(
            List<Map.Entry<K,? extends V>> entries )
    {
        List<K> ret = new ArrayList<>( entries.size() );

        for( Map.Entry<K,? extends V> current : entries ) {
            ret.add( current.getKey() );
        }
        return ret;
    }

    /**
     * Helper method to get the values of a list of Entries.
     *
     * @param entries the entries
     * @return the values of the entries
     */
    protected List<V> valuesOf(
            List<Map.Entry<K,? extends V>> entries )
    {
        List<V> ret = new ArrayList<>( entries.size() );

        for( Map.Entry<K,? extends V> current : entries ) {
            ret.add( current.getValue() );
        }
        return ret;
    }

    /**
     * Move the cursor to just before the first element, i.e. return the first element when
     * {@link CursorIterator#next() next} is invoked right afterwards.
     *
     * @return the number of steps that were taken to move. Positive number means
     *         forward, negative backward
     */
    public int moveToBeforeFirst()
    {
        int ret = theDelegate.moveToBeforeFirst();
        return ret;
    }

    /**
     * Move the cursor to just after the last element, i.e. return the last element when
     * {@link CursorIterator#previous previous} is invoked right afterwards.
     *
     * @return the number of steps that were taken to move. Positive number means
     *         forward, negative backward
     */
    public int moveToAfterLast()
    {
        int ret = theDelegate.moveToAfterLast();
        return ret;
    }

    /**
     * The Map to iterate over.
     */
    protected Map<K,? extends V> theMap;

    /**
     * The Entries in the Map.
     */
    protected Map.Entry<K,? extends V> [] theEntries;

    /**
     * CursorIterator on theEntries.
     */
    protected ArrayCursorIterator<Map.Entry<K,? extends V>> theDelegate;

    /**
     * An iterator over the keys.
     *
     * @param <K> the type of keys
     * @param <V> the type of values
     */
    public static class Keys<K,V>
            extends
                MapCursorIterator<K, V>
            implements
                CursorIterator<K>
    {
        /**
         * Constructor.
         *
         * @param map the Map to iterate over
         * @param entries the sorted list of entries in the map
         * @param delegate the iterator over the entries
         */
        public Keys(
                Map<K,? extends V>                            map,
                Map.Entry<K,? extends V> []                   entries,
                ArrayCursorIterator<Map.Entry<K,? extends V>> delegate )
        {
            super( map, entries, delegate );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K peekNext()
        {
            return getPeekNext().getKey();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K peekPrevious()
        {
            return getPeekPrevious().getKey();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<K> peekNext(
                int n )
        {
            List<K> ret = keysOf( getPeekNext( n ) );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<K> peekPrevious(
                int n )
        {
            List<K> ret = keysOf( getPeekPrevious( n ) );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K next()
        {
            return getNext().getKey();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<K> next(
                int n )
        {
            List<K> ret =  keysOf( getNext( n ) );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K previous()
        {
            return getPrevious().getKey();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<K> previous(
                int n )
        {
            List<K> ret =  keysOf( getPrevious( n ) );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBefore(
                K pos )
            throws
                NoSuchElementException
        {
            // FIXME? Is this right?
            for( int i=0 ; i<theEntries.length ; ++i ) {
                if( pos == theEntries[i].getKey() ) {

                    int ret = i - theDelegate.getPosition() - 1;
                    theDelegate.setPosition( i );
                    return ret;
                }
            }
            throw new NoSuchElementException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfter(
                K pos )
            throws
                NoSuchElementException
        {
            // FIXME? Is this right?
            for( int i=0 ; i<theEntries.length ; ++i ) {
                if( pos == theEntries[i].getKey() ) {

                    int ret = i - theDelegate.getPosition();
                    theDelegate.setPosition( i );
                    return ret;
                }
            }
            throw new NoSuchElementException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final K nextElement()
        {
            return next();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Keys<K,V> createCopy()
        {
            return new Keys<>( theMap, theEntries, theDelegate.createCopy() );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setPositionTo(
                CursorIterator<K> position )
            throws
                IllegalArgumentException
        {
            throw new UnsupportedOperationException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public CursorIterator<K> iterator()
        {
            return this;
        }
    }

    /**
     * An iterator over the values.
     *
     * @param <K> the type of keys
     * @param <V> the type of values
     */
    public static class Values<K,V>
            extends
                MapCursorIterator<K, V>
            implements
                CursorIterator<V>
    {
        /**
         * Constructor.
         *
         * @param map the Map to iterate over
         * @param entries the sorted list of entries in the map
         * @param delegate the iterator over the entries
         */
        protected Values(
                Map<K,? extends V>                            map,
                Map.Entry<K,? extends V> []                   entries,
                ArrayCursorIterator<Map.Entry<K,? extends V>> delegate )
        {
            super( map, entries, delegate );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V peekNext()
        {
            return getPeekNext().getValue();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V peekPrevious()
        {
            return getPeekPrevious().getValue();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<V> peekNext(
                int n )
        {
            List<V> ret = valuesOf( getPeekNext( n ));
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<V> peekPrevious(
                int n )
        {
            List<V> ret = valuesOf( getPeekPrevious( n ));
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V next()
        {
            return getNext().getValue();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<V> next(
                int  n )
        {
            List<V> ret = valuesOf( getNext( n ) );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V previous()
        {
            return getPrevious().getValue();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<V> previous(
                int  n )
        {
            List<V> ret = valuesOf( getPrevious( n ) );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBefore(
                V pos )
            throws
                NoSuchElementException
        {
            // FIXME? Is this right?
            for( int i=0 ; i<theEntries.length ; ++i ) {
                if( pos == theEntries[i].getValue() ) {

                    int ret = i - theDelegate.getPosition() - 1;
                    theDelegate.setPosition( i );
                    return ret;
                }
            }
            throw new NoSuchElementException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfter(
                V pos )
            throws
                NoSuchElementException
        {
            // FIXME? Is this right?
            for( int i=0 ; i<theEntries.length ; ++i ) {
                if( pos == theEntries[i].getValue() ) {

                    int ret = i - theDelegate.getPosition();
                    theDelegate.setPosition( i );
                    return ret;
                }
            }
            throw new NoSuchElementException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public final V nextElement()
        {
            return next();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Values<K,V> createCopy()
        {
            return new Values<>( theMap, theEntries, theDelegate.createCopy() );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setPositionTo(
                CursorIterator<V> position )
            throws
                IllegalArgumentException
        {
            throw new UnsupportedOperationException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public CursorIterator<V> iterator()
        {
            return this;
        }
    }
}
