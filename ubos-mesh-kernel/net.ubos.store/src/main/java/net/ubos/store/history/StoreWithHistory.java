//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

import java.io.IOException;
import net.ubos.util.cursoriterator.CursorIterable;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * A StoreWithHistory is similar to a Store, but instead of allowing changes to the
 * data in the Store, it only allows appending new data with a more recent
 * timeUpdated property.
 *
 * Unlike Store, it directly contains StoreValueHistories, not StoreValues.
 * One can think of them as swim lanes of the historical evolution of StoreValues.
 *
 * We keep the timeCreated around in StoreValues, because ancient histories may be purged and
 * we still may want to know when a value had been created originally.
 *
 * There is no "delete": clients need to make up their own convention, such as
 * setting timeCreated to -1 to indicate that a history ends here.
 *
 * @param <T> parameterization allows implementations that store more than just StoreValueWithTimeUpdated
 */
public interface StoreWithHistory<T extends StoreValueWithTimeUpdated>
    extends
        CursorIterable<StoreValueWithTimeUpdatedHistory<T>>
{
    /**
     * Initialize the HistoryStore. If the HistoryStore was initialized earlier, this will delete all
     * contained information. This operation is similar to unconditionally formatting a hard drive.
     *
     * @throws IOException thrown if an I/O error occurred
     */
    public void initializeHard()
            throws
                IOException;

    /**
     * Initialize the HistoryStore if needed. If the HistoryStore was initialized earlier, this will do
     * nothing. This operation is equivalent to {@link #initializeHard} if and only if
     * the StoreWithHistory had not been initialized earlier.
     *
     * @throws IOException thrown if an I/O error occurred
     */
    public void initializeIfNecessary()
            throws
                IOException;

    /**
     * Put a data element into the StoreWithHistory, or update it if it exists already
     *
     * @param toStore the StoreValue to store
     * @throws IOException thrown if an I/O error occurred
     */
    public void putOrUpdate(
            T toStore )
        throws
            IOException;

    /**
     * Obtain a data element and associated meta-data from the HistoryStore, given a key and a timeUpdated.
     * The timeUpdated must match exactly.
     *
     * @param key the key to the data element in the StoreWithHistory
     * @param timeUpdated the time
     * @return the StoreValue stored in the StoreWithHistory for this key and time; this encapsulates data element and meta-data
     * @throws StoreWithHistoryEntryDoesNotExistException thrown if currently there is no data element in the StoreWithHistory using this key and time
     * @throws IOException thrown if an I/O error occurred
     */
    public T get(
            String key,
            long   timeUpdated )
        throws
            StoreWithHistoryEntryDoesNotExistException,
            IOException;

    /**
     * Obtain a history of the StoreValues for a particular key.
     *
     * @param key the key
     * @return the history
     * @throws StoreWithHistoryKeyDoesNotExistException thrown if currently there is no data element in the StoreWithHistory using this key
     * @throws IOException thrown if an I/O error occurred
     */
    public StoreValueWithTimeUpdatedHistory<T> history(
            String key )
        throws
            StoreWithHistoryKeyDoesNotExistException,
            IOException;

    /**
     * Obtain an iterator over the subset of the StoreValueHistories in the StoreWithHistory whose
     * key starts with this String.
     *
     * @param startsWith the String the key starts with
     * @return the Iterator
     */
    public CursorIterator<StoreValueWithTimeUpdatedHistory<T>> iterator(
            String startsWith );

    /**
     * Obtain an iterator over the subset of the StoreValueHistories in the StoreWithHistory whose
     * key starts with this String.
     *
     * @param startsWith the String the key starts with
     * @return the Iterator
     */
    public default CursorIterator<StoreValueWithTimeUpdatedHistory<T>> getIterator(
            String startsWith )
    {
        return iterator( startsWith );
    }

    /**
     * Determine the number of StoreValueHistories in this HistoryStore (not StoreValues).
     * Some classes implementing this interface may only return an approximation.
     *
     * @return the number of StoreValueHistories in this StoreWithHistory
     * @throws IOException thrown if an I/O error occurred
     */
    public int size()
            throws
                IOException;

    /**
     * Determine the number of StoreValueHistories in this HistoryStore (not StoreValues).
     * Some classes implementing this interface may only return an approximation.
     *
     * @return the number of StoreValueHistories in this StoreWithHistory
     * @throws IOException thrown if an I/O error occurred
     */
    public default int getSize()
            throws
                IOException
    {
        return size();
    }

    /**
     * Determine the number of StoreValueHistories (not StoreValues) in this StoreWithHistory
     * whose key starts with this String.
     *
     * @param startsWith the String the key starts with
     * @return the number of StoreValueHistories in this StoreWithHistory whose key starts with this String
     * @throws IOException thrown if an I/O error occurred
     */
    public int size(
            String startsWith )
        throws
            IOException;

    /**
     * Determine the number of StoreValueHistories (not StoreValues) in this StoreWithHistory
     * whose key starts with this String.
     *
     * @param startsWith the String the key starts with
     * @return the number of StoreValueHistories in this StoreWithHistory whose key starts with this String
     * @throws IOException thrown if an I/O error occurred
     */
    public default int getSize(
            String startsWith )
        throws
            IOException
    {
        return size( startsWith );
    }

    /**
     * Determine whether this StoreWithHistory is empty.
     *
     * @return true if this StoreWithHistory is empty
     * @throws IOException thrown if an I/O error occurred
     */
    public default boolean isEmpty()
        throws
            IOException
    {
        return size() == 0;
    }

    /**
     * Determine whether the set of StoreValueHistories in this StoreWithHistory whose key
     * starts with this String is empty
     *
     * @param startsWith the String the key starts with
     * @return true if the set of StoreValueHistories in this StoreWithHistory whose key starts with this String is empty
     * @throws IOException thrown if an I/O error occurred
     */
    public default boolean isEmpty(
            String startsWith )
        throws
            IOException
    {
        return size( startsWith ) == 0;
    }

    /**
     * Delete the data element that is stored using this key at this time.
     *
     * @param key the key to the data element in the Store
     * @param t the time
     * @throws StoreWithHistoryEntryDoesNotExistException thrown if currently there is no data element in the Store using this key
     * @throws IOException thrown if an I/O error occurred
     */
    public void delete(
            String key,
            long   t )
        throws
            StoreWithHistoryEntryDoesNotExistException,
            IOException;

    /**
     * Remove all data elements with this key.
     *
     * @param key the key
     * @throws IOException thrown if an I/O error occurred
     */
    public void deleteAllKey(
            String key )
        throws
            IOException;

    /**
     * Remove all data in this Store whose keys start with this string.
     *
     * @param startsWith the String the key starts with
     * @return the number of deleted data elements
     * @throws IOException thrown if an I/O error occurred
     */
    public int deleteAll(
            String startsWith )
        throws
            IOException;

    /**
     * Remove all data elements in this Store.
     *
     * @return the number of deleted data elements
     * @throws IOException thrown if an I/O error occurred
     */
    public int deleteAll()
        throws
            IOException;

    /**
      * Add a listener.
      * This listener is added directly to the listener list, which prevents the
      * listener from being garbage-collected before this Object is being garbage-collected.
      *
      * @param newListener the to-be-added listener
      * @see #addSoftHistoryStoreListener
      * @see #addWeakHistoryStoreListener
      * @see #removeHistoryStoreListener
      */
    public abstract void addDirectHistoryStoreListener(
            StoreWithHistoryListener<T> newListener );

    /**
      * Add a listener.
      * This listener is added to the listener list using a <code>java.lang.ref.SoftReference</code>,
      * which allows the listener to be garbage-collected before this Object is being garbage-collected
      * according to the semantics of Java references.
      *
      * @param newListener the to-be-added listener
      * @see #addDirectHistoryStoreListener
      * @see #addWeakHistoryStoreListener
      * @see #removeHistoryStoreListener
      */
    public abstract void addSoftHistoryStoreListener(
           StoreWithHistoryListener<T> newListener );

    /**
      * Add a listener.
      * This listener is added to the listener list using a <code>java.lang.ref.WeakReference</code>,
      * which allows the listener to be garbage-collected before this Object is being garbage-collected
      * according to the semantics of Java references.
      *
      * @param newListener the to-be-added listener
      * @see #addDirectHistoryStoreListener
      * @see #addSoftHistoryStoreListener
      * @see #removeHistoryStoreListener
      */
    public abstract void addWeakHistoryStoreListener(
            StoreWithHistoryListener<T> newListener );

    /**
      * Remove a listener.
      * This method is the same regardless how the listener was subscribed to events.
      *
      * @param oldListener the to-be-removed listener
      * @see #addDirectHistoryStoreListener
      * @see #addSoftHistoryStoreListener
      * @see #addWeakHistoryStoreListener
      */
    public abstract void removeHistoryStoreListener(
            StoreWithHistoryListener<T> oldListener );
}
