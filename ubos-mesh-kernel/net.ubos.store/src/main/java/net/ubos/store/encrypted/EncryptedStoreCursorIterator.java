//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.encrypted;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.store.AbstractStoreValueCursorIterator;
import net.ubos.store.StoreValue;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.store.StoreValueCursorIterator;

/**
 * CursorIterator implementation for the EncryptedStore.
 *
 * @param <T> corresponds to parameterization of the Store
 */
public final class EncryptedStoreCursorIterator<T extends StoreValue>
        extends
            AbstractStoreValueCursorIterator<T>
{
    /**
     * Constructor.
     *
     * @param store the EncryptedStore being iterated over
     * @param delegateIter the underlying Iterator
     */
    public EncryptedStoreCursorIterator(
            EncryptedStore<T>           store,
            StoreValueCursorIterator<T> delegateIter )
    {
        super( store );
        theDelegateIter = delegateIter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EncryptedStore<T> getStore()
    {
        return (EncryptedStore<T>) theStore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekNext()
    {
        boolean success = false;
        T       ret     = null;

        try {
            ret = getStore().decryptStoreValue( theDelegateIter.peekNext() );
            success = true;

        } finally {
            firePeekNextPerformed( null, ret, success );
        }
        
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekPrevious()
    {
        boolean success = false;
        T       ret     = null;

        try {
            ret = getStore().decryptStoreValue( theDelegateIter.peekPrevious() );
            success = true;
        } finally {
            firePeekPreviousPerformed( null, ret, success );
        }
        
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> peekNext(
            int n )
    {
        List<T> delegateValues = theDelegateIter.peekNext( n );
        List<T> ret            = new ArrayList<>( delegateValues.size() );

        EncryptedStore<T> store = getStore();
        for( T current : delegateValues ) {
            ret.add( store.decryptStoreValue( current ));
        }
        
        firePeekNextPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> peekPrevious(
            int n )
    {
        List<T> delegateValues = theDelegateIter.peekPrevious( n );
        List<T> ret            = new ArrayList<>( delegateValues.size() );

        EncryptedStore<T> store = getStore();
        for( T current : delegateValues ) {
            ret.add( store.decryptStoreValue( current ));
        }
        
        firePeekPreviousPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        boolean ret = theDelegateIter.hasNext();
        
        fireHasNextPerformed( null, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        boolean ret =theDelegateIter.hasPrevious();

        fireHasPreviousPerformed( null, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        boolean ret = theDelegateIter.hasNext( n );

        fireHasNextPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        boolean ret = theDelegateIter.hasPrevious( n );

        fireHasPreviousPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next()
    {
        boolean success = false;
        T       ret     = null;
        try {
            ret = getStore().decryptStoreValue( theDelegateIter.next() );
            success = true;
        } finally {
            fireNextPerformed( null, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> next(
            int  n )
    {
        List<T> delegateValues = theDelegateIter.next( n );
        List<T> ret            = new ArrayList<>( delegateValues.size() );

        EncryptedStore<T> store = getStore();
        for( T current : delegateValues ) {
            ret.add( store.decryptStoreValue( current ));
        }
        
        fireNextPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T previous()
    {
        boolean success = false;
        T       ret     = null;

        try {
            ret = getStore().decryptStoreValue( theDelegateIter.previous() );
            success = true;
        } finally {
            firePreviousPerformed( null, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> previous(
            int n )
    {
        List<T> delegateValues = theDelegateIter.previous( n );
        List<T> ret            = new ArrayList<>( delegateValues.size() );

        EncryptedStore<T> store = getStore();
        for( T current : delegateValues ) {
            ret.add( store.decryptStoreValue( current ));
        }

        firePreviousPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        boolean success = false;
        
        try {
            theDelegateIter.moveBy( n );
            success = true;

        } finally {
            fireMoveByPerformed( null, n, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            T pos )
        throws
            NoSuchElementException
    {
        boolean success = false;
        int     ret     = 0;
        
        try {
            ret = theDelegateIter.moveToBefore( pos );
            success = true;

        } finally {
            fireMoveToBeforePerformed( null, pos, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            T pos )
        throws
            NoSuchElementException
    {
        boolean success = false;
        int     ret     = 0;
        
        try {
            ret = theDelegateIter.moveToAfter( pos );
            success = true;

        } finally {
            fireMoveToAfterPerformed( null, pos, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
    {
        boolean success = false;
        
        try {
            theDelegateIter.remove();
            success = true;

        } finally {
            fireRemovePerformed( null, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            String key )
        throws
            NoSuchElementException
    {
        boolean success = false;
        int     ret     = 0;
        
        try {
            ret = theDelegateIter.moveToBefore( key );
            success = true;

        } finally {
            fireMoveToBeforePerformed( null, key, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            String key )
        throws
            NoSuchElementException
    {
        boolean success = false;
        int     ret     = 0;
        
        try {
            ret = theDelegateIter.moveToAfter( key );
            success = true;
            
        } finally {
            fireMoveToAfterPerformed( null, key, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EncryptedStoreCursorIterator<T> createCopy()
    {
        return new EncryptedStoreCursorIterator<>( getStore(), theDelegateIter.createCopy() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<T> position )
        throws
            IllegalArgumentException
    {
        boolean success = false;
        
        try {
            if( position.hasNext() ) {
                moveToBefore( getStore().encryptStoreValue( position.peekNext() ));
            } else {
                moveToAfterLast();
            }
            success = true;
            
        } finally {
            fireSetPositionToPerformed( null, position, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret = theDelegateIter.moveToBeforeFirst();
        
        fireMoveToBeforeFirstPerformed( null, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int ret = theDelegateIter.moveToAfterLast();

        fireMoveToAfterLastPerformed( null, ret );
        return ret;
    }

    /**
     * The underlying Iterator.
     */
    protected final StoreValueCursorIterator<T> theDelegateIter;
}
