//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.prefixing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.store.AbstractStore;
import net.ubos.store.AbstractStoreValueCursorIterator;
import net.ubos.store.Store;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreKeyExistsAlreadyException;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.store.StoreValueCursorIterator;

/**
 * This {@link Store} delegates to another <code>Store</code>, but prefixes all keys with
 * a constant prefix. This is useful to build a hierarchical namespace of <code>Store</code>
 * keys.
 *
 * @param <T> parameterization allows implementations that store more than just StoreValue
 */
public class PrefixingStore<T extends StoreValue>
        extends
            AbstractStore<T>
        implements
            CanBeDumped
{
    /**
     * Factory method.
     *
     * @param prefix the prefix for all keys
     * @param delegate the Store that this PrefixingStore delegates to
     * @return the created PrefixingStore
     * @param <T> parameterization allows implementations that store more than just StoreValue
     */
    public static <T extends StoreValue> PrefixingStore<T> create(
            String   prefix,
            Store<T> delegate )
    {
        if( prefix.contains( DEFAULT_SEPARATOR )) {
            throw new IllegalArgumentException( "Prefix must not contain DEFAULT_SEPARATOR: " + prefix );
        }
        return new PrefixingStore<>( prefix + DEFAULT_SEPARATOR, delegate );
    }

    /**
     * Constructor.
     *
     * @param prefixAndSeparator the prefixAndSeparator for all keys
     * @param delegate the Store that this PrefixingStore delegates to
     */
    protected PrefixingStore(
            String   prefixAndSeparator,
            Store<T> delegate )
    {
        thePrefixAndSeparator = prefixAndSeparator;
        theDelegate           = delegate;
    }

    /**
     * Not supported. Initialize underlying Store instead.
     */
    @Override
    public void initializeHard()
    {
        throw new UnsupportedOperationException( "Cannot initialize PrefixingStore; initialize underlying Store instead." );
    }

    /**
     * Not supported. Initialize underlying Store instead.
     */
    @Override
    public void initializeIfNecessary()
    {
        throw new UnsupportedOperationException( "Cannot initialize PrefixingStore; initialize underlying Store instead." );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void put(
            T toStore )
        throws
            StoreKeyExistsAlreadyException,
            IOException
    {
        String delegatedKey = constructDelegatedKey( toStore.getKey() );

        boolean success = false;

        try {
            theDelegate.put( (T) toStore.copyWithNewKey( delegatedKey ));

            success = true;

        } catch( StoreKeyExistsAlreadyException ex ) {
            throw new PrefixingStoreKeyExistsAlreadyException( this, toStore.getKey(), ex );

        } finally {
            firePutPerformed( toStore, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void update(
            T toUpdate )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        String delegatedKey = constructDelegatedKey( toUpdate.getKey() );

        try {
            theDelegate.update( (T) toUpdate.copyWithNewKey( delegatedKey ));

        } catch( StoreKeyDoesNotExistException ex ) {
            throw new StoreKeyDoesNotExistException( this, toUpdate.getKey(), ex );

        } finally {
            fireUpdatePerformed( toUpdate, true );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean putOrUpdate(
            T toStoreOrUpdate )
        throws
            IOException
    {
        String delegatedKey = constructDelegatedKey( toStoreOrUpdate.getKey() );

        boolean success = false;
        boolean updated = false;
        try {
            updated = theDelegate.putOrUpdate( (T) toStoreOrUpdate.copyWithNewKey( delegatedKey ));

            success = true;

        } finally {
            if( updated ) {
                fireUpdatePerformed( toStoreOrUpdate, success );
            } else {
                firePutPerformed( toStoreOrUpdate, success );
            }
        }

        return updated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(
            String key )
        throws
            PrefixingStoreKeyDoesNotExistException,
            IOException
    {
        String delegatedKey = constructDelegatedKey( key );

        T ret = null;
        try {
            T delegateValue = theDelegate.get( delegatedKey );

            ret = translateDelegateStoreValue( delegateValue );

        } catch( StoreKeyDoesNotExistException ex ) {
            throw new PrefixingStoreKeyDoesNotExistException( this, key, ex );

        } finally {
            fireGetPerformed( key, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(
            String key )
        throws
            PrefixingStoreKeyDoesNotExistException,
            IOException
    {
        String delegatedKey = constructDelegatedKey( key );

        boolean success = false;
        try {
            theDelegate.delete( delegatedKey );

            success = true;

        } catch( StoreKeyDoesNotExistException ex ) {
            throw new PrefixingStoreKeyDoesNotExistException( this, key, ex );

        } finally {
            fireDeletePerformed( key, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll()
        throws
            IOException
    {
        int ret = -1;

        try {
            ret = theDelegate.deleteAll( thePrefixAndSeparator );

        } finally {
            fireDeleteAllPerformed( "", ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll(
            String prefix )
        throws
            IOException
    {
        int ret = -1;

        try {
            ret = theDelegate.deleteAll( thePrefixAndSeparator + prefix );

        } finally {
            fireDeleteAllPerformed( "", ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            String key )
        throws
            IOException
    {
        boolean ret = false;
        try {
            ret = theDelegate.containsKey( thePrefixAndSeparator + key );

        } finally {
            fireContainsKey( key, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueCursorIterator<T> iterator()
    {
        return iterator( "" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueCursorIterator<T> iterator(
            String startsWith )
    {
        return new MyBridgingIterator( new MyFilteringIterator<>( theDelegate.iterator(), thePrefixAndSeparator, startsWith ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size(
            String prefix )
        throws
            IOException
    {
        return theDelegate.size( thePrefixAndSeparator + prefix );
    }

    /**
     * Convert a delegate StoreValue into a StoreValue here.
     *
     * @param delegateValue the delegate StoreValue
     * @return the local StoreValue
     */
    @SuppressWarnings("unchecked")
    protected T translateDelegateStoreValue(
            T delegateValue )
    {
        String newKey = delegateValue.getKey().substring( thePrefixAndSeparator.length() );
        T      ret    = (T) delegateValue.copyWithNewKey( newKey );

        return ret;
    }

    /**
     * Convert a StoreValue into a delegate StoreValue here.
     *
     * @param value the StoreValue
     * @return the delegate StoreValue
     */
    @SuppressWarnings("unchecked")
    protected T translateToDelegateStoreValue(
            T value )
    {
        String newKey = constructDelegatedKey( value.getKey() );
        T      ret    = (T) value.copyWithNewKey( newKey );

        return ret;
    }

    /**
     * Construct the delegate key, given this key and instance data.
     *
     * @param key in key
     * @return the delegate key
     */
    protected String constructDelegatedKey(
            String key )
    {
        StringBuilder ret = new StringBuilder();
        ret.append( thePrefixAndSeparator );
        ret.append( key );
        return ret.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "prefixAndSeparator",
                    "delegate"
                },
                new Object[] {
                    thePrefixAndSeparator,
                    theDelegate
                });
    }

    /**
     * The prefix and separator.
     */
    protected final String thePrefixAndSeparator;

    /**
     * The delegate Store.
     */
    protected final Store<T> theDelegate;

    /**
     * The default separator between the prefix and the key.
     */
    public static final String DEFAULT_SEPARATOR = " ";

    /**
     * Our IterableStoreCursor implementation.
     */
    protected class MyBridgingIterator
            extends
                AbstractStoreValueCursorIterator<T>
    {
        /**
         * Constructor.
         *
         * @param filterIterator an Iterator over the underlying delegate IterableStore
         */
        public MyBridgingIterator(
                MyFilteringIterator<T> filterIterator )
        {
            super( PrefixingStore.this );

            theFilterIterator= filterIterator;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public PrefixingStore<T> getStore()
        {
            return (PrefixingStore<T>) theStore;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T peekNext()
        {
            boolean success = false;
            T       ret     = null;

            try {
                ret = translateDelegateStoreValue( theFilterIterator.peekNext() );
                success = true;
            } finally {
                firePeekNextPerformed( null, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T peekPrevious()
        {
            boolean success = false;
            T       ret     = null;

            try {
                ret = translateDelegateStoreValue( theFilterIterator.peekPrevious() );
                success = true;
            } finally {
                firePeekPreviousPerformed( null, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<T> peekNext(
                int n )
        {
            List<T> delegateValues = theFilterIterator.peekNext( n );
            List<T> ret            = new ArrayList<>( delegateValues.size() );

            for( T current : delegateValues ) {
                ret.add( translateDelegateStoreValue( current ));
            }

            firePeekNextPerformed( null, n, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<T> peekPrevious(
                int n )
        {
            List<T> delegateValues = theFilterIterator.peekPrevious( n );
            List<T> ret            = new ArrayList<>( delegateValues.size() );

            for( T current : delegateValues ) {
                ret.add( translateDelegateStoreValue( current ));
            }

            firePeekPreviousPerformed( null, n, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNext()
        {
            boolean ret = theFilterIterator.hasNext();

            fireHasNextPerformed( null, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasPrevious()
        {
            boolean ret = theFilterIterator.hasPrevious();

            fireHasPreviousPerformed( null, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNext(
                int n )
        {
            boolean ret = theFilterIterator.hasNext( n );

            fireHasNextPerformed( null, n, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasPrevious(
                int n )
        {
            boolean ret = theFilterIterator.hasPrevious( n );

            fireHasPreviousPerformed( null, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T next()
        {
            boolean success = false;
            T       ret     = null;

            try {
                ret = translateDelegateStoreValue( theFilterIterator.next() );
                success = false;
            } finally {
                fireNextPerformed( null, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<T> next(
                int n )
        {
            List<T> delegateValues = theFilterIterator.next( n );
            List<T> ret            = new ArrayList<>( delegateValues.size() );

            for( T current : delegateValues ) {
                ret.add( translateDelegateStoreValue( current ));
            }

            fireNextPerformed( null, n, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T previous()
        {
            boolean success = false;
            T       ret     = null;

            try {
                ret = translateDelegateStoreValue( theFilterIterator.previous() );
                success = true;
            } finally {
                firePreviousPerformed( null, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<T> previous(
                int n )
        {
            List<T> delegateValues = theFilterIterator.previous( n );
            List<T> ret            = new ArrayList<>( delegateValues.size() );

            for( T current : delegateValues ) {
                ret.add( translateDelegateStoreValue( current ));
            }

            firePreviousPerformed( null, n, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void moveBy(
                int n )
            throws
                NoSuchElementException
        {
            boolean success = false;

            try {
                theFilterIterator.moveBy( n );
                success = true;
            } finally {
                fireMoveByPerformed( null, n, success );
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBefore(
                T pos )
            throws
                NoSuchElementException
        {
            boolean success = false;
            int     ret     = 0;

            try {
                T delegatedPos = translateToDelegateStoreValue( pos );
                ret            = theFilterIterator.moveToBefore( delegatedPos );
                success = true;
            } finally {
                fireMoveToBeforePerformed( null, pos, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfter(
                T pos )
            throws
                NoSuchElementException
        {
            boolean success = false;
            int     ret     = 0;

            try {
                T delegatedPos = translateToDelegateStoreValue( pos );
                ret            = theFilterIterator.moveToAfter( delegatedPos );
                success = true;
            } finally {
                fireMoveToAfterPerformed( null, pos, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void remove()
        {
            boolean success = false;
            
            try {
                theFilterIterator.remove();
                success = true;
            } finally {
                fireRemovePerformed( null, success );
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MyBridgingIterator createCopy()
        {
            return new MyBridgingIterator( theFilterIterator.createCopy() );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setPositionTo(
                CursorIterator<T> position )
            throws
                IllegalArgumentException
        {
            boolean success = false;

            try {
                T delegatedPos = translateToDelegateStoreValue( position.next() );

                theFilterIterator.moveToAfter( delegatedPos );
                success = true;
                
            } finally {
                fireSetPositionToPerformed( null, position, success );
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBefore(
                String key )
            throws
                NoSuchElementException
        {
            boolean success = false;
            int     ret     = 0;

            try {
                String delegatedKey = constructDelegatedKey( key );
                ret                 = theFilterIterator.moveToBefore( delegatedKey );
                success = true;

            } finally {
                fireMoveToBeforePerformed( null, key, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfter(
                String key )
            throws
                NoSuchElementException
        {
            boolean success = false;
            int     ret     = 0;

            try {
                String delegatedKey = constructDelegatedKey( key );
                ret                 = theFilterIterator.moveToAfter( delegatedKey );
                success = true;

            } finally {
                fireMoveToAfterPerformed( null, key, ret, success );
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBeforeFirst()
        {
            int ret = theFilterIterator.moveToBeforeFirst();

            fireMoveToBeforeFirstPerformed( null, ret );
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfterLast()
        {
            int ret = theFilterIterator.moveToAfterLast();

            fireMoveToAfterLastPerformed( null, ret );
            return ret;
        }

        /**
         * The underlying FilteringIterator.
         */
        protected final MyFilteringIterator<T> theFilterIterator;
    }

    /**
     * The FilteringIterator that MyBridgingIterator delegates to. This used to be a non-static class,
     * but I ran into a compiler bug that would cause a java.lang.VerifyError at run-time!
     * @param <T> parameterization allows implementations that store more than just StoreValue
     */
    protected static class MyFilteringIterator<T extends StoreValue>
            extends
                FilteringCursorIterator<T>
            implements
                StoreValueCursorIterator<T>
    {
        /**
         * Constructor.
         *
         * @param delegateIter the Iterator over the underlying Store
         * @param prefixAndSeparator copied from the outer class
         * @param startsWith filter by this additional prefix
         */
        public MyFilteringIterator(
                StoreValueCursorIterator<T> delegateIter,
                String      prefixAndSeparator,
                String      startsWith )
        {
            super( delegateIter,
                   ( T v ) -> {
                          String  key = v.getKey();
                          boolean ret = key.startsWith( prefixAndSeparator + startsWith );

                          return ret;
                   },
                   new DEFAULT_EQUALS<>() ); // I think

            thePrefixAndSeparator = prefixAndSeparator;
            theStartsWith = startsWith;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBefore(
                String pos )
            throws
                NoSuchElementException
        {
            // see moveToBefore in superclass

            CursorIterator<T> currentPosition = createCopy();

            int count = 0;
            while( hasNext() ) {
                StoreValue found = peekNext();
                if( pos.equals( found.getKey() )) {
                    return count;
                }
                ++count;
                next();
            }

            setPositionTo( currentPosition );

            count = 0;
            while( hasPrevious() ) {
                StoreValue found = previous();
                --count;
                if( pos.equals( found.getKey() )) {
                    return count;
                }
            }
            throw new NoSuchElementException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfter(
                String pos )
            throws
                NoSuchElementException
        {
            // see moveToBefore in superclass

            CursorIterator<T> currentPosition = createCopy();

            int count = 0;
            while( hasNext() ) {
                StoreValue found = next();
                ++count;
                if( pos.equals( found.getKey() )) {
                    return count;
                }
            }

            setPositionTo( currentPosition );

            count = 0;
            while( hasPrevious() ) {
                StoreValue found = peekPrevious();
                if( pos.equals( found.getKey() )) {
                    return count;
                }
                --count;
                previous();
            }
            throw new NoSuchElementException();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MyFilteringIterator<T> createCopy()
        {
            return new MyFilteringIterator<>( (StoreValueCursorIterator<T>) theDelegate.createCopy(), thePrefixAndSeparator, theStartsWith );
        }

        /**
         * Copied from the outer class.
         */
        protected final String thePrefixAndSeparator;

        /**
         * The additional key prefix to filter by.
         */
        protected final String theStartsWith;
    }
}
