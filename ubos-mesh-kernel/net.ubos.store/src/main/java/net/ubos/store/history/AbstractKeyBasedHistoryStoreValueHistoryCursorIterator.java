//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Collects functionality common to HistoryStoreCursorIterator implementations that work on
 * Stores whose stored data is organized by ordered keys.
 *
 * @param <T> parameterization of the HistoryStore
 */
public abstract class AbstractKeyBasedHistoryStoreValueHistoryCursorIterator<T extends StoreValueWithTimeUpdated>
        implements
            StoreValueWithTimeUpdatedHistoryCursorIterator<T>
{
    /**
     * Constructor, for subclasses only.
     *
     * @param position the key for the current position.
     * @param startsWith only return those elements whose keys starts with this string
     */
    protected AbstractKeyBasedHistoryStoreValueHistoryCursorIterator(
            String position,
            String startsWith )
    {
        thePosition   = position;
        theStartsWith = startsWith;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdatedHistory<T> nextElement()
    {
        return next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdatedHistory<T> peekNext()
    {
        List<StoreValueWithTimeUpdatedHistory<T>> found = findNextIncluding( thePosition, 1, theStartsWith );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Obtain the next key, without iterating forward.
     *
     * @return the next key
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    public String peekNextKey()
    {
        List<String> found = findNextKeyIncluding( thePosition, 1, theStartsWith );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdatedHistory<T> peekPrevious()
    {
        List<StoreValueWithTimeUpdatedHistory<T>> found = findPreviousExcluding( thePosition, 1, theStartsWith );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Obtain the previous key, without iterating backwards.
     *
     * @return the previous key
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    public String peekPreviousKey()
    {
        List<String> found = findPreviousKeyExcluding( thePosition, 1, theStartsWith );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValueWithTimeUpdatedHistory<T>> peekNext(
            int n )
    {
        List<StoreValueWithTimeUpdatedHistory<T>> found = findNextIncluding( thePosition, n, theStartsWith );
        return found;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValueWithTimeUpdatedHistory<T>> peekPrevious(
            int n )
    {
        List<StoreValueWithTimeUpdatedHistory<T>> found = findPreviousExcluding( thePosition, 1, theStartsWith );
        return found;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return hasNext( 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        return hasPrevious( 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        int found = hasNextIncluding( thePosition, theStartsWith );
        if( found >= n ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        int found = hasPreviousExcluding( thePosition, theStartsWith );
        if( found >= n ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdatedHistory<T> next()
    {
        List<StoreValueWithTimeUpdatedHistory<T>> found = next( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Returns the next key in the iteration.
     *
     * @return the next key in the iteration.
     * @throws NoSuchElementException iteration has no more elements.
     */
    public String nextKey()
    {
        List<String> found = nextKey( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValueWithTimeUpdatedHistory<T>> next(
            int n )
    {
        // we have to go forward by one more, so we can set the new position right after what's returned here
        List<StoreValueWithTimeUpdatedHistory<T>> found = findNextIncluding( thePosition, n+1, theStartsWith );
        List<StoreValueWithTimeUpdatedHistory<T>> ret   = found.subList( 0, Math.min( n, found.size() ));

        if( found.size() == n+1 ) {
            thePosition = found.get( found.size()-1 ).getKey();
        } else {
            moveToAfterLast();
        }
        return ret;
    }

    /**
     * <p>Obtain the next N keys. If fewer than N elements are available, return
     * as many keys are available in a shorter array.</p>
     *
     * @param n the number of keys to obtain
     * @return the next no more than N keys
     * @see #previousKey(int)
     */
    public List<String> nextKey(
            int n )
    {
        // we have to go forward by one more, so we can set the new position right after what's returned here
        List<String> found = findNextKeyIncluding( thePosition, n+1, theStartsWith );
        List<String> ret   = found.subList( 0, Math.min( n, found.size() ));

        if( found.size() == n+1 ) {
            thePosition = found.get( found.size()-1 );
        } else {
            moveToAfterLast();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdatedHistory<T> previous()
    {
        List<StoreValueWithTimeUpdatedHistory<T>> found = previous( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Returns the previous key in the iteration.
     *
     * @return the previous key in the iteration.
     * @see #nextKey()
     */
    public String previousKey()
    {
        List<String> found = previousKey( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValueWithTimeUpdatedHistory<T>> previous(
            int n )
    {
        List<StoreValueWithTimeUpdatedHistory<T>> found = findPreviousExcluding( thePosition, n, theStartsWith );
        if( found.size() > 0 ) {
            thePosition = found.get( found.size()-1 ).getKey();
        } else{
            moveToBeforeFirst();
        }
        return found;
    }

    /**
     * <p>Obtain the previous N keys. If fewer than N elements are available, return
     * as many keys are available in a shorter array.</p>
     *
     * <p>Note that the keys
     * will be ordered in the opposite direction as you might expect: they are
     * returned in the sequence in which the CursorIterator visits them, not in the
     * sequence in which the underlying Iterable stores them.</p>
     *
     * @param n the number of keys to obtain
     * @return the previous no more than N keys
     * @see #next(int)
     */
    public List<String> previousKey(
            int n )
    {
        List<String> found = findPreviousKeyExcluding( thePosition, n, theStartsWith );
        if( found.size() > 0 ) {
            thePosition = found.get( found.size()-1 );
        } else{
            moveToBeforeFirst();
        }
        return found;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        if( n == 0 ) {
            return;
        }
        String newPosition = findKeyAt( thePosition, n, theStartsWith );
        thePosition = newPosition;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            StoreValueWithTimeUpdatedHistory<T> pos )
        throws
            NoSuchElementException
    {
        return moveToBefore( pos.getKey() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            StoreValueWithTimeUpdatedHistory<T> pos )
        throws
            NoSuchElementException
    {
        return moveToAfter( pos.getKey() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            String key )
        throws
            NoSuchElementException
    {
        if( thePosition.equals( key )) {
            return 0;
        }

        // FIXME this does not look right
        int distance = determineDistance( thePosition, key, theStartsWith );
        if( distance < 0 ) {
            distance = -determineDistance(key, thePosition, theStartsWith );
            if( distance > 0 ) {
                throw new NoSuchElementException();
            }
        }
        thePosition = key;

        return distance;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            String key )
        throws
            NoSuchElementException
    {
        int ret = moveToBefore( key );
        next();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret;

        try {
            String newPosition = getBeforeFirstPosition( theStartsWith );
            ret                = determineDistance( thePosition, newPosition, theStartsWith );
            thePosition        = newPosition;

        } catch( NoSuchElementException ex ) {
            // empty store
            ret = 0;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        String newPosition = getAfterLastPosition( theStartsWith );
        int    ret         = determineDistance( thePosition, newPosition, theStartsWith );
        thePosition        = newPosition;

        return ret;
    }

    /**
     * Find the next n StoreValues, including the StoreValue for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValue
     * @param n the number of StoreValues to find
     * @param pattern the pattern to filter by
     * @return the found StoreValues
     */
    protected abstract List<StoreValueWithTimeUpdatedHistory<T>> findNextIncluding(
            String key,
            int    n,
            String pattern );

    /**
     * Find the next n keys, including key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key the first key
     * @param n the number of keys to find
     * @param pattern the pattern to filter by
     * @return the found keys
     */
    protected abstract List<String> findNextKeyIncluding(
            String key,
            int    n,
            String pattern );

    /**
     * Find the previous n StoreValueHistory, excluding the StoreValueHistory for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValueWithTimeUpdatedHistory NOT to be returned
     * @param n the number of StoreValueWithTimeUpdatedHistory to find
     * @param pattern the pattern to filter by
     * @return the found StoreValueWithTimeUpdatedHistory
     */
    protected abstract List<StoreValueWithTimeUpdatedHistory<T>> findPreviousExcluding(
            String key,
            int    n,
            String pattern );

    /**
     * Find the previous n keys, excluding the key for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key the first key NOT to be returned
     * @param n the number of keys to find
     * @param pattern the pattern to filter by
     * @return the found keys
     */
    protected abstract List<String> findPreviousKeyExcluding(
            String key,
            int    n,
            String pattern );

    /**
     * Count the number of elements following and including the one with the key.
     *
     * @param key the key
     * @param pattern the pattern to filter by
     * @return the number of elements
     */
    protected abstract int hasNextIncluding(
            String key,
            String pattern );

    /**
     * Count the number of elements preceding and excluding the one with the key.
     *
     * @param key the key
     * @param pattern the pattern to filter by
     * @return the number of elements
     */
    protected abstract int hasPreviousExcluding(
            String key,
            String pattern );

    /**
     * Find the key N elements up or down from the current key.
     *
     * @param key the current key
     * @param delta the number of elements up (positive) or down (negative)
     * @param pattern the pattern to filter by
     * @return the found key, or null
     * @throws NoSuchElementException thrown if the delta went beyond the "after last" or "before first" element
     */
    protected abstract String findKeyAt(
            String key,
            int    delta,
            String pattern )
       throws
            NoSuchElementException;

    /**
     * Helper method to determine the number of elements between two keys.
     *
     * @param from the start key
     * @param to the end key
     * @param pattern the pattern to filter by
     * @return the distance
     */
    protected abstract int determineDistance(
            String from,
            String to,
            String pattern );

    /**
     * Determine the key at the very beginning.
     *
     * @return the key
     * @param pattern the pattern to filter by
     */
    protected abstract String getBeforeFirstPosition(
            String pattern );

    /**
     * Determine the key at the very end.
     *
     * @return the key
     * @param pattern the pattern to filter by
     */
    protected abstract String getAfterLastPosition(
            String pattern );

    /**
     * Only return those elements whose keys match this pattern. The syntax of this
     * pattern is defined by the subclass where it is used.
     */
    protected final String theStartsWith;

    /**
     * The key for the current position.
     */
    protected String thePosition;
}
