//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

/**
 * Thrown to indicate that a key does not exist already in the {@link Store}, for an operation
 * that requires that the key exists already.
 */
public class StoreKeyDoesNotExistException
        extends
            StoreException
{
    /**
     * Constructor.
     *
     * @param store the Store in which the key did not exist
     * @param key the key that does not exist in the Store
     */
    public StoreKeyDoesNotExistException(
            Store<?> store,
            String   key )
    {
        super( store, key, "Key does not exist: " + key, null );
    }

    /**
     * Constructor.
     *
     * @param store the Store in which the key did not exist
     * @param key the key that does not exist in the Store
     * @param cause the underlying cause
     */
    public StoreKeyDoesNotExistException(
            Store<?>  store,
            String    key,
            Throwable cause )
    {
        super( store, key, "Key does not exist: " + key, cause );
    }
}
