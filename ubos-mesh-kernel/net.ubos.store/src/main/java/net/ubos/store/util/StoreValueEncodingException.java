//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

/**
 * Thrown if encoding of a {@link StoreValue} failed.
 */
public class StoreValueEncodingException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param message the error message
     */
    public StoreValueEncodingException(
            String message )
    {
        super( message );
    }

    /**
     * Constructor.
     *
     * @param cause the underlying cause
     */
    public StoreValueEncodingException(
            Throwable cause )
    {
        super( cause );
    }

    /**
     * Constructor.
     *
     * @param message the error message
     * @param cause the underlying cause
     */
    public StoreValueEncodingException(
            String    message,
            Throwable cause )
    {
        super( message, cause );
    }
}
