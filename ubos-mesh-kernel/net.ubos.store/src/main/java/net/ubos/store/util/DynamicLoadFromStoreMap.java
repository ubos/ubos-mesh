//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import java.io.IOException;
import java.util.HashMap;
import net.ubos.store.Store;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.AbstractSmartMap;

/**
 * A {@link net.ubos.util.CachingMap} whose cache is either entirely empty or complete,
 * and if empty, is transparently reloaded from the specified {@link net.ubos.store.Store}.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 * @param <T> parameterization allows implementations that store more than just StoreValue
 */
public abstract class DynamicLoadFromStoreMap<K,V,T extends StoreValue>
        extends
            AbstractSmartMap<K,V>
{
    private static final Log log = Log.getLogInstance( DynamicLoadFromStoreMap.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param store the underlying Store
     * @param storeEntryKey the key used to write the entire content of the Map into the Store
     */
    protected DynamicLoadFromStoreMap(
            Store<T> store,
            String   storeEntryKey )
    {
        theStore         = store;
        theStoreEntryKey = storeEntryKey;
        theDelegate      = null; // initially empty
    }

    /**
     * Clear the local cache.
     */
    public synchronized void clearLocalCache()
    {
        saveToStore();
        theDelegate = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }
        return delegate.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }
        return delegate.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }

        CursorIterator<K> ret = MapCursorIterator.createForKeys( delegate );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<V> valueIterator()
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }

        CursorIterator<V> ret = MapCursorIterator.createForValues( delegate );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            K key )
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }
        return delegate.containsKey( key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V get(
            K key )
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }
        return delegate.get( key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            K key,
            V value )
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }
        delegate.put( key, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            K key )
    {
        HashMap<K,V> delegate = theDelegate; // trick to get around a synchronized statement

        if( delegate == null ) {
            delegate = ensureLoaded();
        }
        delegate.remove( key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void clear()
    {
        theDelegate = null;

        try {
            theStore.deleteAll();

        } catch( IOException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    public boolean equals(
            Object o )
    {
        return o == this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theDelegate.hashCode();
    }

    /**
     * Ensure that the DynamicLoadFromStoreMap is fully loaded. Return the delegate.
     *
     * @return the delegate
     */
    protected synchronized HashMap<K,V> ensureLoaded()
    {
        if( theDelegate == null ) {
            try {
                theDelegate = load();

            } catch( StoreKeyDoesNotExistException ex ) {
                // that's fine
                theDelegate = new HashMap<>(); // empty

            } catch( IOException ex ) {
                throw new RuntimeException( ex );
            }
        }

        return theDelegate;
    }

    /**
     * Save the content of this DynamicLoadFromStoreMap to Store.
     */
    public void saveToStore()
    {
        if( theDelegate != null ) {
            try {
                save();

            } catch( IOException ex ) {
                throw new RuntimeException( ex );
            }
        }
    }

    /**
     * Create a new delegate HashMap by loading from the Store.
     *
     * @return the new delegate HashMap
     * @throws StoreKeyDoesNotExistException thrown if no map content could be found
     * @throws IOException thrown if loading failed
     */
    protected abstract HashMap<K,V> load()
            throws
                StoreKeyDoesNotExistException,
                IOException;

    /**
     * Save the delegate HashMap to the Store.
     *
     * @throws IOException thrown if saving failed
     */
    protected abstract void save()
            throws
                IOException;

    /**
     * The underlying HashMap if currently available.
     */
    protected HashMap<K,V> theDelegate;

    /**
     * The Store to save from/to.
     */
    protected final Store<T> theStore;

    /**
     * The key of the StoreValue into which to write the entire content of this Map.
     */
    protected final String theStoreEntryKey;
}
