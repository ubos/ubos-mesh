//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

/**
 * Abstract superclass for Exceptions thrown by <code>StoreWithHistory</code>.
 */
public abstract class StoreWithHistoryException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param store the StoreWithHistory that threw the Exception
     * @param key the key that caused the Exception
     * @param message a message
     */
    protected StoreWithHistoryException(
            StoreWithHistory<?> store,
            String              key,
            String              message )
    {
        super( message );

        theStore = store;
        theKey   = key;
    }

    /**
     * Constructor.
     *
     * @param store the StoreWithHistory that threw the Exception
     * @param key the key that caused the Exception
     * @param message a message
     * @param cause the underlying cause for this Exception
     */
    protected StoreWithHistoryException(
            StoreWithHistory<?> store,
            String              key,
            String              message,
            Throwable           cause )
    {
        super( message, cause );

        theStore = store;
        theKey   = key;
    }

    /**
     * Obtain the Store that caused this Exception.
     *
     * @return the Store
     */
    public StoreWithHistory<?> getHistoryStore()
    {
        return theStore;
    }

    /**
     * Obtain the key related to which the Exception occurred.
     *
     * @return the key
     */
    public String getKey()
    {
        return theKey;
    }

    /**
     * The StoreWithHistory that threw this Exception.
     */
    protected final StoreWithHistory<?> theStore;

    /**
     * The key related to which this Exception occurred.
     */
    protected final String theKey;
}
