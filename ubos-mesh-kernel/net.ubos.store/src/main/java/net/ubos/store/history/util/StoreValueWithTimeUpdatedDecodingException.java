//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.util;

import net.ubos.store.history.StoreValueWithTimeUpdated;

/**
 * Thrown if decoding of a {@link StoreValueWithTimeUpdated} failed.
 */
public class StoreValueWithTimeUpdatedDecodingException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param value the StoreValueWithTimeUpdated that failed to decode
     * @param message the error message
     */
    public StoreValueWithTimeUpdatedDecodingException(
            StoreValueWithTimeUpdated value,
            String                    message )
    {
        super( message );

        theValue = value;
    }

    /**
     * Constructor.
     *
     * @param value the StoreValueWithTimeUpdated that failed to decode
     * @param cause the underlying cause
     */
    public StoreValueWithTimeUpdatedDecodingException(
            StoreValueWithTimeUpdated value,
            Throwable                 cause )
    {
        super( cause );

        theValue = value;
    }

    /**
     * Constructor.
     *
     * @param value the StoreValue that failed to decode
     * @param message the error message
     * @param cause the underlying cause
     */
    public StoreValueWithTimeUpdatedDecodingException(
            StoreValueWithTimeUpdated value,
            String                    message,
            Throwable                 cause )
    {
        super( message, cause );

        theValue = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( getClass().getName() );
        buf.append( " StoreValue with key: " );
        buf.append( theValue.getKey() );
        if( getCause() != null ) {
            buf.append( ": " );
            buf.append( getCause().getLocalizedMessage() );
        }
        return buf.toString();
    }

    /**
     * The StoreValue that failed to decode.
     */
    protected final StoreValueWithTimeUpdated theValue;
}
