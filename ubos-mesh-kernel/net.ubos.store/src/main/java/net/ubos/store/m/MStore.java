//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.m;

import java.io.IOException;
import java.util.HashMap;
import net.ubos.store.AbstractStore;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreKeyExistsAlreadyException;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.store.StoreValueCursorIterator;

/**
 * A "fake" Store implementation that keeps the {@link net.ubos.store.Store} content in memory only.
 * While there might be few production uses of this class, there are many
 * during software development and for testing.
 */
public class MStore
        extends
            AbstractStore<StoreValue>
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( MStore.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @return the created MStore
     */
    public static MStore create()
    {
        return new MStore();
    }

    /**
     * Constructor.
     */
    protected MStore()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeHard()
            throws
                IOException
    {
        theDelegate.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeIfNecessary()
            throws
                IOException
    {
        // no nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void put(
            StoreValue toStore )
        throws
            StoreKeyExistsAlreadyException,
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "put", toStore );
        }

        checkKey(      toStore.getKey() );
        checkEncoding( toStore.getEncodingId() );
        checkData(     toStore.getData() );

        boolean success = false;
        try {
            StoreValue already = theDelegate.get( toStore.getKey() );
            if( already != null ) {
                throw new StoreKeyExistsAlreadyException( this, toStore.getKey() );
            }
            theDelegate.put( toStore.getKey(), toStore );

            success = true;

        } finally {
            firePutPerformed( toStore, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void update(
            StoreValue toUpdate )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "update", toUpdate );
        }

        checkKey(      toUpdate.getKey() );
        checkEncoding( toUpdate.getEncodingId() );
        checkData(     toUpdate.getData() );

        boolean success = false;
        try {
            StoreValue already = theDelegate.get( toUpdate.getKey() );
            if( already == null ) {
                throw new StoreKeyDoesNotExistException( this, toUpdate.getKey() );
            }
            theDelegate.put( toUpdate.getKey(), toUpdate );

            success = true;

        } finally {
            fireUpdatePerformed( toUpdate, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized boolean putOrUpdate(
            StoreValue toStoreOrUpdate )
        throws
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "putOrUpdate", toStoreOrUpdate );
        }

        checkKey(      toStoreOrUpdate.getKey() );
        checkEncoding( toStoreOrUpdate.getEncodingId() );
        checkData(     toStoreOrUpdate.getData() );

        boolean    success = false;
        StoreValue already = null;
        boolean    ret;

        try {
            already = theDelegate.put( toStoreOrUpdate.getKey(), toStoreOrUpdate );

            success = true;

        } finally {
            if( already != null ) {
                fireUpdatePerformed( toStoreOrUpdate, success );
                ret = true;

            } else {
                firePutPerformed( toStoreOrUpdate, success );
                ret = false;
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized StoreValue get(
            String key )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        checkKey( key );

        StoreValue ret = null;

        try {
            ret = theDelegate.get( key );

        } finally {
            if( log.isDebugEnabled() ) {
                log.debug( this + ".get( " + key + " ) returns " + ret );
            }

            fireGetPerformed( key, ret );
        }

        if( ret == null ) {
            throw new StoreKeyDoesNotExistException( this, key );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(
            String key )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        checkKey( key );

        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "delete", key );
        }

        StoreValue already = null;
        try {
            already = theDelegate.remove( key );

        } finally {
            fireDeletePerformed( key, already != null );
        }

        if( already == null ) {
            throw new StoreKeyDoesNotExistException( this, key );
        }
        // otherwise no harm done
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized int deleteAll()
        throws
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "deleteAll" );
        }
        int n = theDelegate.size();

        theDelegate.clear();

        fireDeleteAllPerformed( "", n );

        return n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized int deleteAll(
            String startsWith )
        throws
            IOException
    {
        checkKey( startsWith );

        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "deleteAll", startsWith );
        }
        int n=0;
        for( String key : theDelegate.keySet() ) {
            if( key.startsWith( startsWith )) {
                theDelegate.remove( key );
                ++n;
            }
        }
        fireDeleteAllPerformed( startsWith, n );

        return n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            String key )
    {
        checkKey( key );

        boolean ret = false;
        try {
            ret = theDelegate.containsKey( key );

        } finally {
            fireContainsKey( key, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueCursorIterator<StoreValue> iterator()
    {
        return iterator( "" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized StoreValueCursorIterator<StoreValue> iterator(
            String startsWith )
    {
        return MStoreCursorIterator.create( theDelegate, startsWith );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized int size(
            String startsWith )
    {
        int count = 0;
        for( String key : theDelegate.keySet() ) {
            if( key.startsWith( startsWith )) {
                ++count;
            }
        }
        return count;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theDelegate"
                },
                new Object[] {
                    theDelegate
                } );
    }

    /**
     * The in-memory storage.
     */
    protected final HashMap<String,StoreValue> theDelegate = new HashMap<>();
}
