//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.m;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import net.ubos.store.history.AbstractStoreWithHistory;
import net.ubos.store.history.StoreWithHistoryEntryDoesNotExistException;
import net.ubos.store.history.StoreWithHistoryKeyDoesNotExistException;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.store.history.StoreValueWithTimeUpdatedHistory;

/**
 * A "fake" HistoryStore implementation that keeps the {@link net.ubos.store.history.HistoryStore}
 * content in memory only. While there might be few production uses of this class, there are many
 * during software development and for testing.
 *
 * @param <T> allow implementations to store more than just StoreValueWithTimeUpdated
 */
public class MStoreWithHistory<T extends StoreValueWithTimeUpdated>
        extends
            AbstractStoreWithHistory<T>
        implements
            CanBeDumped
{
    /**
     * Factory method.
     *
     * @return the created MStore
     * @param <T> allow implementations to store more than just StoreValueWithTimeUpdated
     */
    public static <T extends StoreValueWithTimeUpdated> MStoreWithHistory<T> create()
    {
        return new MStoreWithHistory<>();
    }

    /**
     * Constructor.
     */
    protected MStoreWithHistory()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeHard()
            throws
                IOException
    {
        theDelegate.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeIfNecessary()
            throws
                IOException
    {
        // no nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putOrUpdate(
            T toStore )
    {
        final String key = toStore.getKey();

        synchronized( this ) {
            MStoreValueWithTimeUpdatedHistory<T> history = theDelegate.get( key );
            if( history == null ) {
                history = new MStoreValueWithTimeUpdatedHistory<>( key, this );
                theDelegate.put( key, history );
            }
            history.putOrThrow( toStore );
        }

        firePutPerformed( toStore, true );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(
            String key,
            long   timeUpdated )
        throws
            StoreWithHistoryEntryDoesNotExistException
    {
        T ret;

        synchronized( this ) {
            MStoreValueWithTimeUpdatedHistory<T> history = theDelegate.get( key );
            if( history == null ) {
                throw new StoreWithHistoryEntryDoesNotExistException( this, key, timeUpdated );
            }
            ret = history.at( timeUpdated );
            if( ret == null ) {
                throw new StoreWithHistoryEntryDoesNotExistException( this, key, timeUpdated );
            }
        }

        fireGetPerformed( key, timeUpdated, ret );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdatedHistory<T> history(
            String key )
        throws
            StoreWithHistoryKeyDoesNotExistException
    {
        MStoreValueWithTimeUpdatedHistory<T> history = theDelegate.get( key );
        if( history == null ) {
            throw new StoreWithHistoryKeyDoesNotExistException( this, key );
        }
        return history;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<StoreValueWithTimeUpdatedHistory<T>> iterator()
    {
        return MapCursorIterator.createForValues( theDelegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<StoreValueWithTimeUpdatedHistory<T>> iterator(
            String startsWith )
    {
          return FilteringCursorIterator.create(iterator(),
                  (StoreValueWithTimeUpdatedHistory<T> h) -> h.getKey().startsWith( startsWith ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
        throws
            IOException
    {
        return theDelegate.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size(
            String startsWith )
        throws
            IOException
    {
        long ret = theDelegate.keySet().stream().filter(
                (String key) -> key.startsWith( startsWith ))
                .count();
        return (int) ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(
            String key,
            long   timeUpdated )
        throws
            StoreWithHistoryEntryDoesNotExistException
    {
        MStoreValueWithTimeUpdatedHistory<T> history = theDelegate.get( key );
        if( history == null ) {
            throw new StoreWithHistoryEntryDoesNotExistException( this, key, timeUpdated );
        }
        history.removeIgnorePrevious( timeUpdated );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAllKey(
            String key )
    {
        theDelegate.remove( key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized int deleteAll(
            String startsWith )
    {
        ArrayList<String> toRemove = new ArrayList<>();

        for( var current : theDelegate.entrySet() ) {
            if( current.getKey().startsWith( startsWith )) {
                toRemove.add( current.getKey() );
            }
        }
        for( String current : toRemove ) {
            theDelegate.remove( current );
        }
        return toRemove.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll()
    {
        int ret = theDelegate.size();
        theDelegate.clear();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theDelegate"
                },
                new Object[] {
                    theDelegate
                } );
    }

    /**
     * The in-memory storage.
     */
    protected final HashMap<String,MStoreValueWithTimeUpdatedHistory<T>> theDelegate = new HashMap<>();
}
