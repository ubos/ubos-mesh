//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.encrypted;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.ubos.store.AbstractStore;
import net.ubos.store.Store;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreKeyExistsAlreadyException;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.Log;
import net.ubos.store.StoreValueCursorIterator;

/**
 * <p>A <code>Store</code> that encrypts its data (but not its keys) before
 *    delegating to an underlying <code>Store</code>.</p>
 * <p>Note that the <code>encodingId</code> remains the same in this implementation; subclasses
 *    could change this.</p>
 *
 * @param <T> parameterization allows implementations that store more than just StoreValue
 */
public class EncryptedStore<T extends StoreValue>
        extends
            AbstractStore<T>
{
    private static final Log log = Log.getLogInstance( EncryptedStore.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param transformation the name of the transformation, e.g., DES/CBC/PKCS5Padding, see <code>javax.crypto.Cipher.getInstance(String)</code>
     * @param keySpec the key specification
     * @param iv the init vector to tuse
     * @param delegate the Store that does the actual storing
     * @return the created EncryptedStore
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException thrown if the key is invalid or does not match the specified transformation
     * @throws NoSuchAlgorithmException thrown if the specified transformation is not available in the default provider package or any of the other provider packages that were searched.
     * @throws NoSuchPaddingException thrown if transformation contains a padding scheme that is not available.
     *
     * @param <T> parameterization allows implementations that store more than just StoreValue
     */
    public static <T extends StoreValue> EncryptedStore<T> create(
            String          transformation,
            SecretKeySpec   keySpec,
            IvParameterSpec iv,
            Store<T>        delegate )
        throws
            InvalidAlgorithmParameterException,
            InvalidKeyException,
            NoSuchPaddingException,
            NoSuchAlgorithmException
    {
        Cipher encCipher = Cipher.getInstance( transformation );
        Cipher decCipher = Cipher.getInstance( transformation );

        encCipher.init( Cipher.ENCRYPT_MODE, keySpec, iv );
        decCipher.init( Cipher.DECRYPT_MODE, keySpec, iv );

        return new EncryptedStore<>( encCipher, decCipher, delegate );
    }

    /**
     * Constructor.
     *
     * @param encCipher the Cipher to use for encryption
     * @param decCipher the Cipher to use for decryption
     * @param delegate the Store that does the actual storing
     */
    protected EncryptedStore(
            Cipher   encCipher,
            Cipher   decCipher,
            Store<T> delegate )
    {
        theEncCipher   = encCipher;
        theDecCipher   = decCipher;
        theDelegate    = delegate;
    }

    /**
     * Not supported. Initialize the underlying Store instead.
     */
    @Override
    public void initializeHard()
    {
        throw new UnsupportedOperationException( "Cannot initialize EncryptedStore; initialize underlying Store instead." );
    }

    /**
     * Not supported. Initialize the underlying Store instead.
     */
    @Override
    public void initializeIfNecessary()
    {
        throw new UnsupportedOperationException( "Cannot initialize EncryptedStore; initialize underlying Store instead." );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void put(
            T toStore )
        throws
            StoreKeyExistsAlreadyException,
            IOException
    {
        T encryptedToStore = encryptStoreValue( toStore );

        boolean success = false;
        try {
            theDelegate.put( encryptedToStore );

            success = true;

        } finally {
            firePutPerformed( toStore, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(
            T toUpdate )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        T encryptedToUpdate = encryptStoreValue( toUpdate );

        boolean success = false;
        try {
            theDelegate.update( encryptedToUpdate );

            success = true;

        } finally {
            fireUpdatePerformed( toUpdate, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean putOrUpdate(
            T toStoreOrUpdate )
        throws
            IOException
    {
        T encryptedToStoreOrUpdate = encryptStoreValue( toStoreOrUpdate );

        boolean ret = false;
        try {
            ret = theDelegate.putOrUpdate( encryptedToStoreOrUpdate );

        } finally {
            if( ret ) {
                fireUpdatePerformed( toStoreOrUpdate, true );
            } else {
                firePutPerformed( toStoreOrUpdate, true );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(
            String key )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        T ret = null;
        try {
            ret = decryptStoreValue( theDelegate.get( key ));

        } finally {
            fireGetPerformed( key, ret );
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(
            String key )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        boolean success = false;

        try {
            theDelegate.delete( key );

            success = true;

        } finally {
            fireDeletePerformed( key, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll()
        throws
            IOException
    {
        int ret = -1;
        try {
            ret = theDelegate.deleteAll();

        } finally {
            fireDeleteAllPerformed( "", ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll(
            String startsWith )
        throws
            IOException
    {
        int ret = -1;
        try {
            ret = theDelegate.deleteAll( startsWith );

        } finally {
            fireDeleteAllPerformed( startsWith, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            String key )
        throws
            IOException
    {
        boolean ret = false;
        try {
            ret = theDelegate.containsKey( key );

        } finally {
            fireContainsKey( key, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueCursorIterator<T> iterator()
    {
        StoreValueCursorIterator<T> delegateIter = theDelegate.iterator();

        return new EncryptedStoreCursorIterator<>( this, delegateIter );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueCursorIterator<T> iterator(
            String startsWith )
    {
        StoreValueCursorIterator<T> delegateIter = theDelegate.iterator( startsWith );

        return new EncryptedStoreCursorIterator<>( this, delegateIter );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
        throws
            IOException
    {
        return theDelegate.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size(
            String startsWith )
        throws
            IOException
    {
        return theDelegate.size( startsWith );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
        throws
            IOException
    {
        return theDelegate.isEmpty();
    }

    /**
     * Encrypt a piece of data.
     *
     * @param clear the cleartext
     * @return encrypted data
     */
    protected byte [] encrypt(
            byte [] clear )
    {
        try {
            byte [] ret = theEncCipher.doFinal( clear );
            return ret;

        } catch( Throwable t ) {
            log.error( t );

             // there isn't really anything else we can do
            if( t instanceof RuntimeException ) {
                throw ((RuntimeException) t);
            } else {
                throw new RuntimeException( t );
            }
        }
    }

    /**
     * Decrypt a piece of data.
     *
     * @param encrypted the encrypted data
     * @return decrypted data
     */
    protected byte [] decrypt(
            byte [] encrypted )
    {
        try {
            byte [] ret = theDecCipher.doFinal( encrypted );
            return ret;

        } catch( Throwable t ) {
            log.error( t );

             // there isn't really anything else we can do
            if( t instanceof RuntimeException ) {
                throw ((RuntimeException) t);
            } else {
                throw new RuntimeException( t );
            }
        }
    }

    /**
     * Helper method to encrypt / translate a StoreValue.
     *
     * @param decryptedStoreValue the decrypted StoreValue
     * @return the encrypted StoreValue
     */
    protected T encryptStoreValue(
            T decryptedStoreValue )
    {
        String  encryptedEncodingId = constructEncodingId( decryptedStoreValue.getEncodingId() );
        byte [] encryptedData       = encrypt( decryptedStoreValue.getData() );

        @SuppressWarnings("unchecked")
        T ret = (T) decryptedStoreValue.copyWithNewEncodingData(
                encryptedEncodingId,
                encryptedData );
        return ret;
    }

    /**
     * Helper method to decrypt / translate a StoreValue.
     *
     * @param encryptedStoreValue the encrypted StoreValue
     * @return the decrypted StoreValue
     */
    protected T decryptStoreValue(
            T encryptedStoreValue )
    {
        String  decryptedEncodingId = reconstructEncodingId( encryptedStoreValue.getEncodingId() );
        byte [] decryptedData       = decrypt( encryptedStoreValue.getData() );

        @SuppressWarnings("unchecked")
        T ret = (T) encryptedStoreValue.copyWithNewEncodingData(
                decryptedEncodingId,
                decryptedData );
        return ret;
    }

    /**
     * Construct the encodingId to be used for the delegate. This can be
     * overridden in subclasses.
     *
     * @param original the original encodingId
     * @return the encodingId for the delegate
     */
    protected String constructEncodingId(
            String original )
    {
        return original;
    }

    /**
     * Reconstruct the encodingId from the one used by the delegate. This can be
     * overridden in subclasses.
     *
     * @param change the encodingId used by the delegate
     * @return the original encodingId
     */
    protected String reconstructEncodingId(
            String change )
    {
        return change;
    }

    /**
     * The Cipher to use for encryption.
     */
    protected Cipher theEncCipher;

    /**
     * The Cipher to use for decryption.
     */
    protected Cipher theDecCipher;

    /**
     * The underlying Store.
     */
    protected Store<T> theDelegate;
}
