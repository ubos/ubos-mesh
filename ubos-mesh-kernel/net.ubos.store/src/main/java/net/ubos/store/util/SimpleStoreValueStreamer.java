//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import net.ubos.store.StoreValue;
import net.ubos.util.StreamUtils;

/**
 * Knows how to encode and decode a StoreValue to and from an encoded byte stream for storage.
 */
public class SimpleStoreValueStreamer
        implements
            StoreValueStreamer
{
    /**
     * Factory method.
     *
     * @return the created SimpleStoreValueStreamer
     */
    public static SimpleStoreValueStreamer create()
    {
        if( theSingleton == null ) {
            theSingleton = new SimpleStoreValueStreamer();
        }
        return theSingleton;
    }

    /**
     * Private constructor for subclasses only.
     */
    protected SimpleStoreValueStreamer()
    {
        // noop
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeStoreValue(
            StoreValue   value,
            OutputStream stream )
        throws
            IOException
    {
        Writer w = new OutputStreamWriter( stream, CHARSET );

        w.write( VERSION );
        w.write(  CR );

        w.write( value.getKey() );
        w.write(  CR );

        w.write( value.getEncodingId() );
        w.write(  CR );

        w.flush();

        stream.write( value.getData() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue readStoreValue(
            InputStream in )
        throws
            IOException
    {
        String version = readUntilCr( in );
        if( !VERSION.equals( version )) {
            throw new IOException( "Cannot read this version: " + VERSION + " vs " + version );
        }
        String key         = readUntilCr( in );
        String encodingId  = readUntilCr( in );

        byte [] data = StreamUtils.slurp( in );

        StoreValue ret = new StoreValue( key, encodingId, data );
        return ret;
    }

    /**
     * Helper method to read through the next CR.
     *
     * @param in the stream to read from
     * @return the read String
     * @throws IOException thrown if an I/O error occurred
     */
    protected String readUntilCr(
            InputStream in )
        throws
            IOException
    {
        byte [] buf = new byte[ 1024 ]; // seems reasonable
        int     pos = 0;
        int c;
        while( ( c = in.read()) != -1 ) {
            if( c == CR ) {
                break;
            }
            if( pos == buf.length ) {
                byte [] newBuf = new byte[ buf.length * 2];
                System.arraycopy( buf, 0, newBuf, 0, buf.length );
                buf = newBuf;
                // pos remains the same
            }
            buf[pos++] = (byte) c;
        }

        String ret = new String( buf, 0, pos, CHARSET.name() );
        return ret;
    }

    /**
     * Singleton instance.
     */
    protected static SimpleStoreValueStreamer theSingleton;

    /**
     * The version of this format.
     */
    public static final String VERSION = "1";

    /**
     * The character to use as line separator.
     */
    public final char CR = '\n';

    /**
     * The character set to use.
     */
    public static final Charset CHARSET = Charset.forName( "UTF-8" );
}
