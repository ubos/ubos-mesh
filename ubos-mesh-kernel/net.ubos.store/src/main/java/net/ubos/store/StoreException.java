//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

/**
 * Abstract superclass for Exceptions thrown by <code>Store</code>.
 */
public abstract class StoreException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param store the Store that threw the Exception
     * @param key the key in the Store that caused the Exception
     * @param message a message
     * @param cause the underlying cause for this Exception
     */
    protected StoreException(
            Store<?>  store,
            String    key,
            String    message,
            Throwable cause )
    {
        super( message, cause );

        theStore = store;
        theKey   = key;
    }

    /**
     * Obtain the Store that caused this Exception.
     *
     * @return the Store
     */
    public Store<?> getStore()
    {
        return theStore;
    }

    /**
     * Obtain the key related to which the Exception occurred.
     *
     * @return the key
     */
    public String getKey()
    {
        return theKey;
    }

    /**
     * The Store that threw this Exception.
     */
    protected final Store<?> theStore;

    /**
     * The key related to which this Exception occurred.
     */
    protected final String theKey;
}
