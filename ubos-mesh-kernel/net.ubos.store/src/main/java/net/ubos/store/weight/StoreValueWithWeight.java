//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.weight;

import net.ubos.store.StoreValue;

/**
 * A StoreValue that also carries a weight attribute.
 */
public class StoreValueWithWeight
        extends
            StoreValue
{
    /**
     * Constructor.
     *
     * @param key the key by which this data element was found
     * @param encodingId the id of the encoding that was used to encode the data element. This must be 64 bytes or less.
     * @param weight the weight attribute
     * @param data the data element, expressed as a sequence of bytes
     */
    public StoreValueWithWeight(
            String  key,
            String  encodingId,
            long    weight,
            byte [] data )
    {
        super( key, encodingId, data );

        theWeight = weight;
    }

    /**
     * Obtain the weight.
     *
     * @return the weight
     */
    public long getWeight()
    {
        return theWeight;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithWeight copyWithNewKey(
            String newKey )
    {
        return new StoreValueWithWeight( newKey, theEncodingId, theWeight, theData );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithWeight copyWithNewEncodingData(
            String  newEncoding,
            byte [] newData )
    {
        return new StoreValueWithWeight( theKey, newEncoding, theWeight, newData );
    }

    /**
     * The weight attribute;
     */
    protected final long theWeight;
}
