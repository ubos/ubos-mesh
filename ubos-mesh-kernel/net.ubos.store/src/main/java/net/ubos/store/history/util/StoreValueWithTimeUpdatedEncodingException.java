//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.util;

/**
 * Thrown if encoding of a {@link StoreValueWithTimeUpdated} failed.
 */
public class StoreValueWithTimeUpdatedEncodingException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param message the error message
     */
    public StoreValueWithTimeUpdatedEncodingException(
            String message )
    {
        super( message );
    }

    /**
     * Constructor.
     *
     * @param cause the underlying cause
     */
    public StoreValueWithTimeUpdatedEncodingException(
            Throwable cause )
    {
        super( cause );
    }

    /**
     * Constructor.
     *
     * @param message the error message
     * @param cause the underlying cause
     */
    public StoreValueWithTimeUpdatedEncodingException(
            String    message,
            Throwable cause )
    {
        super( message, cause );
    }
}
