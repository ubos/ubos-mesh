//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

/**
 * Objects supporting this interface can listen to events from StoreWitHistory's.
 *
 * On an implementation level, there are a lot more events, like "found next N StoreValues" etc. But
 * the structure of these events is implementation-specific, and so we can do little here. So for now,
 * we don't do more than these two events.
 *
 * @param <T> parameterization allows implementations that store more than just StoreValueWithTimeUpdated
 */
public interface StoreWithHistoryListener<T extends StoreValueWithTimeUpdated>
{
    /**
     * A put operation was performed.
     *
     * @param store the StoreWithHistory that emitted this event
     * @param value the StoreValueWithTimeUpdated that was put
     * @param success true if the put operation was successful
     */
    public void putPerformed(
            StoreWithHistory<T> store,
            T                   value,
            boolean             success );

    /**
     * An update operation was performed.
     *
     * @param store the StoreWithHistory that emitted this event
     * @param value the StoreValueWithTimeUpdated that was updated
     * @param success true if the put operation was successful
     */
    public void updatePerformed(
            StoreWithHistory<T> store,
            T                   value,
            boolean             success );

    /**
     * A get operation was performed.
     *
     * @param store the StoreWithHistory that emitted this event
     * @param key the key that was attempted
     * @param timeUpdated the timeUpdated that was attempted
     * @param value the StoreValueWithTimeUpdated that was obtained (in case of success), or null in case of failure
     */
    public void getPerformed(
            StoreWithHistory<T> store,
            String              key,
            long                timeUpdated,
            T                   value );

    /**
     * A delete all operation was performed.
     *
     * @param store the StoreWithHistory that emitted this event
     * @param key the key that was attempted
     */
    public void deleteAllPerformed(
            StoreWithHistory<T> store,
            String              key );
}
