//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

import java.util.NoSuchElementException;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Adds a few methods to <code>CursorIterator&lt;StoreValue&gt;</code> that make life easier.
 *
 * @param <T> parameterization of the HistoryStore
 */
public interface StoreValueWithTimeUpdatedHistoryCursorIterator<T extends StoreValueWithTimeUpdated>
        extends
            CursorIterator<StoreValueWithTimeUpdatedHistory<T>>
{
    /**
     * Move the cursor to this element, i.e. return this element when {@link #next next} is invoked
     * right afterwards.
     *
     * @param key the key of the element to move the cursor to
     * @return the number of steps that were taken to move. Positive number means forward, negative backward
     * @throws NoSuchElementException thrown if this element is not actually part of the collection to iterate over
     */
    public int moveToBefore(
            String key )
        throws
            NoSuchElementException;

    /**
     * Move the cursor to this element, i.e. return this element when {@link #previous previous} is invoked
     * right afterwards.
     *
     * @param key the key of the element to move the cursor to
     * @return the number of steps that were taken to move. Positive number means forward, negative backward
     * @throws NoSuchElementException thrown if this element is not actually part of the collection to iterate over
     */
    public int moveToAfter(
            String key )
        throws
            NoSuchElementException;

    /**
     * Clone this position.
     *
     * @return identical new instance
     */
    @Override
    public StoreValueWithTimeUpdatedHistoryCursorIterator<T> createCopy();
}
