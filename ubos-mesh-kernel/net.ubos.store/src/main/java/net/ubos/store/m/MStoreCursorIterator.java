//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.m;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import net.ubos.store.StoreValue;
import net.ubos.util.ArrayHelper;
import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.MapCursorIterator;
import net.ubos.store.StoreValueCursorIterator;

/**
 * CursorIterator implementation for this MStore.
 */
public class MStoreCursorIterator
        extends
            MapCursorIterator.Values<String,StoreValue>
        implements
            StoreValueCursorIterator<StoreValue>
{
    /**
     * Factory method.
     *
     * @param delegate the HashMap that stores the content
     * @param startsWith additional prefix to filter the keys by
     * @return the IterableStoreCursor
     */
    @SuppressWarnings("unchecked")
    static MStoreCursorIterator create(
            HashMap<String,StoreValue> delegate,
            String                     startsWith )
    {
        Map.Entry<String, StoreValue>[] entries = (Map.Entry<String, StoreValue>[]) ArrayHelper.createArray( Map.Entry.class, delegate.size() );
        entries = delegate.entrySet().toArray( entries );

        // filter
        if( !"".equals( startsWith ) ) {
            int count = 0;
            for( int i=0; i<entries.length; ++i ) {
                if( entries[i].getKey().startsWith( startsWith ) ) {
                    entries[count] = entries[i];
                    ++count;
                }
            }
            if( count < entries.length ) {
                entries = ArrayHelper.copyIntoNewArray( entries, 0, count, Map.Entry.class);
            }
        }
        return new MStoreCursorIterator( delegate, entries, ArrayCursorIterator.<Map.Entry<String,? extends StoreValue>>create( entries ));
    }

    /**
     * Constructor.
     *
     * @param delegate the HashMap that stores the content
     * @param entries the entries in the HashMap, in a sequence
     * @param delegateIter the delegate iterator over the entries in a sequence
     */
    protected MStoreCursorIterator(
            Map<String,? extends StoreValue>                            delegate,
            Map.Entry<String,? extends StoreValue> []                   entries,
            ArrayCursorIterator<Map.Entry<String,? extends StoreValue>> delegateIter )
    {
        super( delegate, entries, delegateIter );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            String key )
        throws
            NoSuchElementException
    {
        int oldPos = theDelegate.getPosition();
        theDelegate.reset();
        while( theDelegate.hasNext() ) {
            String found = theDelegate.next().getKey();
            if( key.equals(found) ) {
                theDelegate.previous(); // went one too far
                int newPos = theDelegate.getPosition();
                return newPos - oldPos;
            }
        }
        throw new NoSuchElementException( "No such element: " + key );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            String key )
        throws
            NoSuchElementException
    {
        int oldPos = theDelegate.getPosition();
        theDelegate.reset();
        while( theDelegate.hasNext() ) {
            String found = theDelegate.next().getKey();
            if( key.equals(found) ) {
                // this is the right place, not one too far
                int newPos = theDelegate.getPosition();
                return newPos - oldPos;
            }
        }
        throw new NoSuchElementException("No such element: " + key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MStoreCursorIterator createCopy()
    {
        ArrayCursorIterator<Map.Entry<String,? extends StoreValue>> delegateIter
                = ArrayCursorIterator.<Map.Entry<String,? extends StoreValue>>create( theEntries, theDelegate.getPosition() );
        return new MStoreCursorIterator( theMap, theEntries, delegateIter );
    }
}
