//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.util;

import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.util.history.HasTimeUpdated;

/**
 * Classes implementing this interface know how to map elements of a History
 * into and from a <code>StoreWithHistory</code>.
 *
 * Note that the StoreWithHistory stores N histories, but a History, by definition, is
 * only one, there is a key that needs to be passed around so we pull the data for
 * the right History.
 *
 * We reuse the exceptions from StoredBackedSwappingSmartMap, there didn't seem to be a point
 * in creating copies.
 *
 * @param <V> the type of element of a history
 * @param <T> parameterization allows implementations that store more than just StoreValueWithTimeUpdated
 */
public interface StoreValueWithTimeUpdatedMapper<V extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
{
    /**
     * Obtain the default encoding id of this StoreValueWithTimeUpdatedMapper. This StoreValueWithTimeUpdatedMapper
     * may support multiple encodings -- in fact, it should -- but that's not visible
     * to the outside. This is the encoding id by which it encodes.
     *
     * @return the default encoding id
     */
    public abstract String getDefaultEncodingId();

    /**
     * Map a StoreValueWithTimeUpdated to a value.
     *
     * @param value the StoreValueWithTimeUpdated
     * @return the value
     * @throws StoreValueWithTimeUpdatedDecodingException thrown if the StoreValueWithTimeUpdated could not been decoded
     */
    public abstract V decodeValue(
            T value )
        throws
            StoreValueWithTimeUpdatedDecodingException;

    /**
     * Map a value to a (subclass of) StoreValueWithTimeUpdated.
     *
     * @param key the key of the StoreValueWithTimeUpdates that contains the History this value belongs to.
     * @param value the value
     * @return the byte array
     * @throws StoreValueWithTimeUpdatedEncodingException thrown if the value could not been encoded
     */
    public abstract T encodeValue(
            String key,
            V      value )
        throws
            StoreValueWithTimeUpdatedEncodingException;
}
