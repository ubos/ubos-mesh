//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

import net.ubos.store.StoreValue;
import net.ubos.util.history.HasTimeUpdated;
import net.ubos.util.logging.Dumper;

/**
 * <p>A <code>StoreValueWithTimeUpdated</code> encapsulates into one Object the data and meta-data written to
 * and read from a StoreWithHistory for a given key at a particular time.</p>
 */
public class StoreValueWithTimeUpdated
    extends
        StoreValue
    implements
        HasTimeUpdated
{
    /**
     * Constructor.
     *
     * @param key the key by which this data element was found
     * @param encodingId the id of the encoding that was used to encode the data element. This must be 64 bytes or less.
     * @param timeUpdated the time at which the data element was successfully updated the most recent time
     * @param data the data element, expressed as a sequence of bytes
     */
    public StoreValueWithTimeUpdated(
            String  key,
            String  encodingId,
            long    timeUpdated,
            byte [] data )
    {
        super( key, encodingId, data );

        theTimeUpdated = timeUpdated;
    }

    /**
     * Obtain the time this data element was last updated, in <code>System.currentTimeMillis()</code> format.
     *
     * @return the time this data element was last updated
     */
    @Override
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdated copyWithNewKey(
            String newKey )
    {
        return new StoreValueWithTimeUpdated( newKey, theEncodingId, theTimeUpdated, theData );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdated copyWithNewEncodingData(
            String  newEncoding,
            byte [] newData )
    {
        return new StoreValueWithTimeUpdated( theKey, newEncoding, theTimeUpdated, newData );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof StoreValueWithTimeUpdated )) {
            return false;
        }
        StoreValueWithTimeUpdated realOther = (StoreValueWithTimeUpdated) other;

        if( !theKey.equals( realOther.theKey )) {
            return false;
        }
        if( !theEncodingId.equals( realOther.theEncodingId )) {
            return false;
        }
        if( theTimeUpdated != realOther.theTimeUpdated ) {
            return false;
        }
        if( theData.length != realOther.theData.length ) {
            return false;
        }
        for( int i=0 ; i<theData.length ; ++i ) {
            if( theData[i] != realOther.theData[i] ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theKey.hashCode() + (int) theTimeUpdated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder almost = new StringBuilder();
        almost.append( getClass().getName() );
        almost.append( "{ key: " );
        almost.append( theKey );
        almost.append( " }" );
        return almost.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theKey",
                    "theEncodingId",
                    "theTimeUpdated",
                    "theData",
                    "String(theData)"
                },
                new Object[] {
                    theKey,
                    theEncodingId,
                    theTimeUpdated,
                    theData,
                    new String( theData )
                });
    }

    /**
     * The time the data element was last updated.
     */
    protected final long theTimeUpdated;
}
