//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

import java.io.IOException;
import net.ubos.util.cursoriterator.CursorIterable;

/**
 * <p>A <code>Store</code> is an abstraction for a data store that behaves like a persistent
 * Java <code>java.util.Map</code> keyed with String, and with byte arrays as values.</p>
 *
 * <p>Classes implementing this Store interface may store information in a variety of
 * different ways, including in file systems, in databases, or even on P2P networks.
 * Application developers can usually program directly against this interface, without
 * having to be aware aware of the specific class implementing it.</p>
 * <p><code>StoreListeners</code> may be used to listen to Store operations, e.g. for
 * logging purposes.</p>
 * <p>This interface uses the format defined by <code>System.currentTimeMillis()</code>
 * to encode all time values.</p>
 *
 * @param <T> parameterization allows implementations that store more than just StoreValue
 */
public interface Store<T extends StoreValue>
    extends
        CursorIterable<T>
{
    /**
     * Initialize the Store. If the Store was initialized earlier, this will delete all
     * contained information. This operation is similar to unconditionally formatting a hard drive.
     *
     * @throws IOException thrown if an I/O error occurred
     */
    public void initializeHard()
            throws
                IOException;

    /**
     * Initialize the Store if needed. If the Store was initialized earlier, this will do
     * nothing. This operation is equivalent to {@link #initializeHard} if and only if
     * the Store had not been initialized earlier.
     *
     * @throws IOException thrown if an I/O error occurred
     */
    public void initializeIfNecessary()
            throws
                IOException;

    /**
     * Put a data element into the Store for the first time. Throw a
     * {@link StoreKeyExistsAlreadyException} if a data
     * element has already been store using the same key.
     *
     * @param toStore the StoreValue to store
     * @throws StoreKeyExistsAlreadyException thrown if a data element is already stored in the Store using this key
     * @throws IOException thrown if an I/O error occurred
     *
     * @see #update if a data element with this key exists already
     * @see #putOrUpdate if a data element with this key may exist already
     */
    public void put(
            T toStore )
        throws
            StoreKeyExistsAlreadyException,
            IOException;

    /**
     * Update a data element that already exists in the Store, by overwriting it with a new value. Throw a
     * {@link StoreKeyDoesNotExistException} if a data element with this key does not exist already.
     *
     * @param toUpdate the StoreValue to update
     * @throws StoreKeyDoesNotExistException thrown if no data element exists in the Store using this key
     * @throws IOException thrown if an I/O error occurred
     *
     * @see #put if a data element with this key does not exist already
     * @see #putOrUpdate if a data element with this key may exist already
     */
    public void update(
            T toUpdate )
        throws
            StoreKeyDoesNotExistException,
            IOException;

    /**
     * Put (if does not exist already) or update (if it does exist) a data element in the Store.
     *
     * @param toStoreOrUpdate the StoreValue to store or update
     * @return true if the value was updated, false if it was put
     * @throws IOException thrown if an I/O error occurred
     *
     * @see #put if a data element with this key does not exist already
     * @see #update if a data element with this key exists already
     */
    public boolean putOrUpdate(
            T toStoreOrUpdate )
        throws
            IOException;

    /**
     * Obtain a data element and associated meta-data from the Store, given a key.
     *
     * @param key the key to the data element in the Store
     * @return the StoreValue stored in the Store for this key; this encapsulates data element and meta-data
     * @throws StoreKeyDoesNotExistException thrown if currently there is no data element in the Store using this key
     * @throws IOException thrown if an I/O error occurred
     *
     * @see #put to initially store a data element
     */
    public T get(
            String key )
        throws
            StoreKeyDoesNotExistException,
            IOException;

    /**
     * Delete the data element that is stored using this key.
     *
     * @param key the key to the data element in the Store
     * @throws StoreKeyDoesNotExistException thrown if currently there is no data element in the Store using this key
     * @throws IOException thrown if an I/O error occurred
     */
    public void delete(
            String key )
        throws
            StoreKeyDoesNotExistException,
            IOException;

    /**
     * Remove all data elements in this Store.
     *
     * @return the number of deleted data elements
     * @throws IOException thrown if an I/O error occurred
     */
    public int deleteAll()
        throws
            IOException;

    /**
     * Remove all data in this Store whose keys start with this string.
     *
     * @param startsWith the String the key starts with
     * @return the number of deleted data elements
     * @throws IOException thrown if an I/O error occurred
     */
    public int deleteAll(
            String startsWith )
        throws
            IOException;

    /**
     * Determine whether this Store contains data with this key. Does not
     * load the data.
     *
     * @param key the key to the data element in the Store
     * @return true or false
     * @throws IOException thrown if an I/O error occurred
     */
    public boolean containsKey(
            String key )
        throws
            IOException;

    /**
     * Return a more specific subtype of CursorIterator as an iterator over
     * the entire Store.
     *
     * @return the Iterator
     */
    @Override
    public StoreValueCursorIterator<T> iterator();

    /**
     * Return a more specific subtype of CursorIterator as an iterator over
     * the entire Store.
     *
     * @return the Iterator
     */
    @Override
    public default StoreValueCursorIterator<T> getIterator()
    {
        return iterator();
    }

    /**
     * Obtain an iterator over the subset of the elements in the Store whose
     * key starts with this String.
     *
     * @param startsWith the String the key starts with
     * @return the Iterator
     */
    public StoreValueCursorIterator<T> iterator(
            String startsWith );

    /**
     * Obtain an iterator over the subset of the elements in the Store whose
     * key starts with this String.
     *
     * @param startsWith the String the key starts with
     * @return the Iterator
     */
    public default StoreValueCursorIterator<T> getIterator(
            String startsWith )
    {
        return iterator( startsWith );
    }

    /**
     * Determine the number of data elements in this Store. Some classes implementing
     * this interface may only return an approximation.
     *
     * @return the number of data elements in this Store
     * @throws IOException thrown if an I/O error occurred
     */
    public int size()
            throws
                IOException;

    /**
     * Determine the number of data elements in this Store. Some classes implementing
     * this interface may only return an approximation.
     *
     * @return the number of data elements in this Store
     * @throws IOException thrown if an I/O error occurred
     */
    public default int getSize()
            throws
                IOException
    {
        return size();
    }

    /**
     * Determine the number of StoreValues in this Store whose key starts with this String
     *
     * @param startsWith the String the key starts with
     * @return the number of StoreValues in this Store whose key starts with this String
     * @throws IOException thrown if an I/O error occurred
     */
    public int size(
            String startsWith )
        throws
            IOException;

    /**
     * Determine the number of StoreValues in this Store whose key starts with this String
     *
     * @param startsWith the String the key starts with
     * @return the number of StoreValues in this Store whose key starts with this String
     * @throws IOException thrown if an I/O error occurred
     */
    public default int getSize(
            String startsWith )
        throws
            IOException
    {
        return size( startsWith );
    }

    /**
     * Determine whether this Store is empty.
     *
     * @return true if this Store is empty
     * @throws IOException thrown if an I/O error occurred
     */
    public default boolean isEmpty()
        throws
            IOException
    {
        return size() == 0;
    }

    /**
     * Determine whether the set of elements in this Store whose key
     * starts with this String is empty
     *
     * @param startsWith the String the key starts with
     * @return true if this Store is empty
     * @throws IOException thrown if an I/O error occurred
     */
    public default boolean isEmpty(
            String startsWith )
        throws
            IOException
    {
        return size( startsWith ) == 0;
    }

    /**
      * Add a listener.
      * This listener is added directly to the listener list, which prevents the
      * listener from being garbage-collected before this Object is being garbage-collected.
      *
      * @param newListener the to-be-added listener
      * @see #addSoftStoreListener
      * @see #addWeakStoreListener
      * @see #removeStoreListener
      */
    public void addDirectStoreListener(
            StoreListener<T> newListener );

    /**
      * Add a listener.
      * This listener is added to the listener list using a <code>java.lang.ref.SoftReference</code>,
      * which allows the listener to be garbage-collected before this Object is being garbage-collected
      * according to the semantics of Java references.
      *
      * @param newListener the to-be-added listener
      * @see #addDirectStoreListener
      * @see #addWeakStoreListener
      * @see #removeStoreListener
      */
    public void addSoftStoreListener(
            StoreListener<T> newListener );

    /**
      * Add a listener.
      * This listener is added to the listener list using a <code>java.lang.ref.WeakReference</code>,
      * which allows the listener to be garbage-collected before this Object is being garbage-collected
      * according to the semantics of Java references.
      *
      * @param newListener the to-be-added listener
      * @see #addDirectStoreListener
      * @see #addSoftStoreListener
      * @see #removeStoreListener
      */
    public void addWeakStoreListener(
            StoreListener<T> newListener );

    /**
      * Remove a listener.
      * This method is the same regardless how the listener was subscribed to events.
      *
      * @param oldListener the to-be-removed listener
      * @see #addDirectStoreListener
      * @see #addSoftStoreListener
      * @see #addWeakStoreListener
      */
    public void removeStoreListener(
            StoreListener<T> oldListener );
}
