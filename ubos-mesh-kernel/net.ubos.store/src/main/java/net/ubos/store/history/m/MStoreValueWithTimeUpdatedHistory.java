//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.m;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import net.ubos.store.history.AbstractStoreValueWithTimeUpdatedHistory;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.history.AbstractFetchOneHistoryCursorIterator;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;

/**
 * In-memory implementation of StoreValueHistory.
 *
 * @param <T> allow implementations to store more than just StoreValueWithTimeUpdated
 */
public class MStoreValueWithTimeUpdatedHistory<T extends StoreValueWithTimeUpdated>
    extends
        AbstractStoreValueWithTimeUpdatedHistory<T>
{
    private static final Log log = Log.getLogInstance( MStoreValueWithTimeUpdatedHistory.class );

    /**
     * Constructor.
     *
     * @param key the key for the StoreValues whose history this is
     * @param historyStore the HistoryStore we belong to
     */
    public MStoreValueWithTimeUpdatedHistory(
            String               key,
            MStoreWithHistory<T> historyStore )
    {
        super( key );

        theHistoryStore = historyStore;
        theDelegate     = new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            T toAdd )
    {
        long when = toAdd.getTimeUpdated();
        for( int i=0 ; i<theDelegate.size() ; ++i ) {
            long foundWhen = theDelegate.get( i ).getTimeUpdated();
            if( when == foundWhen ) {
                theDelegate.set( i, toAdd );
                return;
            } else if( when < foundWhen ) {
                theDelegate.add( i, toAdd );
                return;
            }
        }
        theDelegate.add( toAdd );
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            T toAdd )
        throws
            IllegalArgumentException
    {
        T already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same element:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "Element exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            long t )
    {
        for( int i=0 ; i<theDelegate.size() ; ++i ) {
            if( t == theDelegate.get( i ).getTimeUpdated() ) {
                theDelegate.remove( i );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T at(
            long t )
    {
        for( int i=0 ; i<theDelegate.size() ; ++i ) {
            if( t == theDelegate.get( i ).getTimeUpdated() ) {
                return theDelegate.get( i );
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<T> iterator()
    {
        return new MyCursorIterator<>( this, 0 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        theDelegate.clear();
    }

    /**
     * The HistoryStore that we belong to.
     */
    protected final MStoreWithHistory<T> theHistoryStore;

    /**
     * The actual storage.
     */
    protected final ArrayList<T> theDelegate;

    /**
     * Iterator over this MStoreValueWithTimeUpdatedHistory.
     *
     * @param <T> allow implementations to store more than just StoreValueWithTimeUpdated
     */
    static class MyCursorIterator<T extends StoreValueWithTimeUpdated>
        extends
            AbstractFetchOneHistoryCursorIterator<T>
    {
        /**
         * Constructor.
         *
         * @param history the history we iterate over
         * @param index position in the ArrayList
         */
        public MyCursorIterator(
                MStoreValueWithTimeUpdatedHistory<T> history,
                int                   index )
        {
            super( history,
                   (T one, T two ) -> one.getKey().equals( two.getKey() ) && one.getTimeUpdated() == two.getTimeUpdated());

            theIndex = index;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T next()
            throws
                NoSuchElementException
        {
            MStoreValueWithTimeUpdatedHistory<T> realHistory = (MStoreValueWithTimeUpdatedHistory<T>) theHistory;

            if( theIndex < realHistory.theDelegate.size() ) {
                return realHistory.theDelegate.get( theIndex++ );
            } else {
                throw new NoSuchElementException();
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public T previous()
                throws NoSuchElementException
        {
            MStoreValueWithTimeUpdatedHistory<T> realHistory = (MStoreValueWithTimeUpdatedHistory<T>) theHistory;

            if( theIndex > 0 ) {
                return realHistory.theDelegate.get( --theIndex );
            } else {
                throw new NoSuchElementException();
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNext(
                int n )
        {
            MStoreValueWithTimeUpdatedHistory<T> realHistory = (MStoreValueWithTimeUpdatedHistory<T>) theHistory;

            return theIndex + n <= realHistory.theDelegate.size();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasPrevious(
                int n )
        {
            return theIndex >= n;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBeforeFirst()
        {
            int ret = theIndex;
            theIndex = 0;
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfterLast()
        {
            MStoreValueWithTimeUpdatedHistory<T> realHistory = (MStoreValueWithTimeUpdatedHistory<T>) theHistory;

            int ret = realHistory.theDelegate.size() - theIndex;
            theIndex = realHistory.theDelegate.size();
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setPositionTo(
                CursorIterator<T> position )
            throws
                IllegalArgumentException
        {
            if( !( position instanceof MyCursorIterator ) || theHistory != ((MyCursorIterator<T>) position).theHistory ) {
                throw new IllegalArgumentException( "Incompatible CursorIterators" );
            }
            theIndex = ((MyCursorIterator) position).theIndex;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToJustBeforeTime(
                long time )
        {
            MStoreValueWithTimeUpdatedHistory<T> realHistory = (MStoreValueWithTimeUpdatedHistory<T>) theHistory;

            for( int i=0 ; i<realHistory.theDelegate.size() ; ++i ) {
                if( time <= realHistory.theDelegate.get( i ).getTimeUpdated()) {
                    int ret = i - theIndex;
                    theIndex = i;
                    return ret;
                }
            }
            int ret = realHistory.theDelegate.size() - theIndex;
            theIndex = realHistory.theDelegate.size();
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToJustAfterTime(
                long time )
        {
            MStoreValueWithTimeUpdatedHistory<T> realHistory = (MStoreValueWithTimeUpdatedHistory<T>) theHistory;

            for( int i=0 ; i<realHistory.theDelegate.size() ; ++i ) {
                if( time < realHistory.theDelegate.get( i ).getTimeUpdated()) {
                    int ret = i - theIndex;
                    theIndex = i;
                    return ret;
                }
            }
            int ret = realHistory.theDelegate.size() - theIndex;
            theIndex = realHistory.theDelegate.size();
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public HistoryCursorIterator<T> createCopy()
        {
            return new MyCursorIterator<>( (MStoreValueWithTimeUpdatedHistory<T>) theHistory, theIndex );
        }

        /**
         * Location in the ArrayList.
         */
        protected int theIndex;
    }
}
