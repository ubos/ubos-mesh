//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.Log;
import net.ubos.util.text.StringRepresentationParseException;

/**
 * A StoreValueMapper that uses Java Serialization, and thus can store and retrieve arbitrary
 * Java object graphs in a <code>Store</code>. It uses the key's <code>toString</code> method
 * to construct the String key needed by <code>Store</code>.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 */
public abstract class SerializingStoreValueMapper<K,V>
        implements
            StoreValueMapper<K,V,StoreValue>
{
    private static final Log log = Log.getLogInstance( SerializingStoreValueMapper.class ); // our own, private logger

    /**
     * Constructor. Use default class loader.
     */
    public SerializingStoreValueMapper()
    {
        theClassLoader = null;
    }

    /**
     * Constructor.
     *
     * @param cl the ClassLoader to use
     */
    public SerializingStoreValueMapper(
            ClassLoader cl )
    {
        theClassLoader = cl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String keyToString(
            K key )
    {
        String ret;
        if( key != null ) {
            ret = key.toString();
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract K stringToKey(
            String stringKey )
        throws
            StringRepresentationParseException;

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings(value={"unchecked"})
    public V decodeValue(
            StoreValue value )
    {
        try {
            ObjectInputStream in = new ObjectInputStream( value.getDataAsStream() ) {
                    // code similar to the superclass
                    @Override
                    protected Class<?> resolveClass(
                            ObjectStreamClass desc )
                        throws
                            IOException,
                            ClassNotFoundException
                    {
                        String name = desc.getName();
                        if( theClassLoader != null ) {
                            try {
                                return Class.forName( name, false, theClassLoader );
                            } catch (ClassNotFoundException ex) {
                                // noop
                            }
                        }
                        return super.resolveClass( desc );
                    }
            };

            Object obj = in.readObject();
            return (V) obj;

        } catch( Exception ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue encodeValue(
            K key,
            V value )
        throws
            StoreValueEncodingException
    {
        try {
            ByteArrayOutputStream stream    = new ByteArrayOutputStream();
            ObjectOutputStream    objStream = new ObjectOutputStream( stream );

            objStream.writeObject( value );
            objStream.flush();
            objStream.close();

            byte [] ret = stream.toByteArray();

            return new StoreValue( keyToString( key ) , getDefaultEncodingId(), ret );

        } catch( IOException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * The ClassLoader that we use, if any.
     */
    protected final ClassLoader theClassLoader;
}
