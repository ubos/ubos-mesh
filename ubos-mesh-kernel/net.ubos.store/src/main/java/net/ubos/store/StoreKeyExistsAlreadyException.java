//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

/**
 * Thrown to indicate that a key exists already in a {@link Store}, for an operation
 * that requires that the key does not exist already.
 */
public class StoreKeyExistsAlreadyException
        extends
            StoreException
{
    /**
     * Constructor.
     *
     * @param store the Store in which the key did exist already
     * @param key the key that already exists in the Store
     */
    public StoreKeyExistsAlreadyException(
            Store<?> store,
            String   key )
    {
        super( store, key, "Store key exists already: " + key, null );
    }

    /**
     * Constructor.
     *
     * @param store the Store in which the key did exist already
     * @param key the key that already exists in the Store
     * @param cause the underlying cause
     */
    public StoreKeyExistsAlreadyException(
            Store<?>  store,
            String    key,
            Throwable cause )
    {
        super( store, key, "Store key exists already: " + key, cause );
    }
}
