//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.prefixing;

import net.ubos.store.StoreKeyDoesNotExistException;

/**
 * Thrown to indicate that a key does not exist already in the <code>Store</code>, for an operation
 * that requires that the key exists already. This subclass is used by
 * <code>PrefixingStores</code>.
*/
public class PrefixingStoreKeyDoesNotExistException
        extends
            StoreKeyDoesNotExistException
{
    /**
     * Constructor.
     *
     * @param store the PrefixingStore that threw this Exception
     * @param localKey the key that does not exist in the PrefixingStore
     * @param cause the StoreKeyExistsAlreadyException thrown by the Store the PrefixingStore delegates to
     */
    public PrefixingStoreKeyDoesNotExistException(
            PrefixingStore<?>             store,
            String                        localKey,
            StoreKeyDoesNotExistException cause )
    {
        super( store, localKey, cause );
    }

    /**
     * Obtain the PrefixingStore in which the key did not exist.
     *
     * @return the PrefixingStore
     */
    @Override
    public PrefixingStore<?> getStore()
    {
        return (PrefixingStore<?>) super.getStore();
    }

    /**
     * Obtain the underlying exception, which we know to be a StoreKeyDoesNotExistException.
     *
     * @return the underlying StoreKeyDoesNotExistException
     */
    @Override
    public StoreKeyDoesNotExistException getCause()
    {
        return (StoreKeyDoesNotExistException) super.getCause();
    }
}
