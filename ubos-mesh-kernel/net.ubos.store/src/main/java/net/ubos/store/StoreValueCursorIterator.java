//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

import net.ubos.util.cursoriterator.CursorIteratorOverHasKey;

/**
 * Adds a few methods to <code>CursorIterator&lt;StoreValue&gt;</code> that make life easier.
 *
 * @param <T> corresponds to parameterization of the Store
 */
public interface StoreValueCursorIterator<T extends StoreValue>
        extends
            CursorIteratorOverHasKey<T>
{
    /**
     * Clone this position.
     *
     * @return identical new instance
     */
    @Override
    public StoreValueCursorIterator<T> createCopy();
}
