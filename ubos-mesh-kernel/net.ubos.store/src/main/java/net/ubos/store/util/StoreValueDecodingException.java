//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import net.ubos.store.StoreValue;

/**
 * Thrown if decoding of a {@link StoreValue} failed.
 */
public class StoreValueDecodingException
        extends
            Exception
{
    /**
     * Constructor.
     *
     * @param value the StoreValue that failed to decode
     * @param message the error message
     */
    public StoreValueDecodingException(
            StoreValue value,
            String     message )
    {
        super( message );

        theValue = value;
    }

    /**
     * Constructor.
     *
     * @param value the StoreValue that failed to decode
     * @param cause the underlying cause
     */
    public StoreValueDecodingException(
            StoreValue value,
            Throwable  cause )
    {
        super( cause );

        theValue = value;
    }

    /**
     * Constructor.
     *
     * @param value the StoreValue that failed to decode
     * @param message the error message
     * @param cause the underlying cause
     */
    public StoreValueDecodingException(
            StoreValue value,
            String     message,
            Throwable  cause )
    {
        super( message, cause );

        theValue = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( getClass().getName() );
        buf.append( " StoreValue with key: " );
        buf.append( theValue.getKey() );
        if( getCause() != null ) {
            buf.append( ": " );
            buf.append( getCause().getLocalizedMessage() );
        }
        return buf.toString();
    }

    /**
     * The StoreValue that failed to decode.
     */
    protected final StoreValue theValue;
}
