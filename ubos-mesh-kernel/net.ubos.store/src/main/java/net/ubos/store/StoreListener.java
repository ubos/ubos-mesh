//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

import java.util.List;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Objects supporting this interface can listen to events from Stores.
 *
 * @param <T> corresponds to parameterization of the Store
 */
public interface StoreListener<T extends StoreValue>
{
    /**
     * A put operation was performed. This indicates either a
     * <code>Store.put</code> or a <code>Store.putOrUpdate</code> operation
     * in which an actual <code>put</code> was performed.
     *
     * @param store the Store that emitted this event
     * @param value the StoreValue that was put
     * @param success if true, the put was successful
     */
    public void putPerformed(
            Store<T> store,
            T        value,
            boolean  success );

    /**
     * An update operation was performed. This indicates either a
     * <code>Store.update</code> or a <code>Store.putOrUpdate</code> operation
     * in which an actual <code>update</code> was performed.
     *
     * @param store the Store that emitted this event
     * @param value the StoreValue that was updated
     * @param success if true, the update was successful
     */
    public void updatePerformed(
            Store<T> store,
            T        value,
            boolean  success );

    /**
     * A get operation was performed.
     *
     * @param store the Store that emitted this event
     * @param key the key that was attempted
     * @param value the StoreValue that was obtained (in case of success), or null in case of failure
     */
    public void getPerformed(
            Store<T> store,
            String   key,
            T        value );

    /**
     * A delete operation was performed.
     *
     * @param store the Store that emitted this event
     * @param key the key with which the data element was stored
     * @param success if true, the delete was successful
     */
    public void deletePerformed(
            Store<T> store,
            String   key,
            boolean  success );

    /**
     * A delete-all operation was performed.
     *
     * @param store the Store that emitted this event
     * @param prefix if given, indicates the prefix of all keys that were deleted. If null, indicates &quot;all&quot;.
     * @param n the number of keys that were deleted
     */
    public void deleteAllPerformed(
            Store<T> store,
            String   prefix,
            int      n );

    /**
     * A contains-key operation was performed.
     *
     * @param store the Store that emitted this event
     * @param key the key that was looked up
     * @param success if true, the Store contained the key
     */
    public void containsKeyPerformed(
            Store<T> store,
            String   key,
            boolean  success );

    public void peekNextPerformed(
            Store<T> store,
            String   key,
            T        value,
            boolean  success );

    public void peekPreviousPerformed(
            Store<T> store,
            String   key,
            T        value,
            boolean  success );

    public void peekNextPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  value );

    public void peekPreviousPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  value );
    
    public void hasNextPerformed(
            Store<T> store,
            String   key,
            boolean  ret );

    public void hasPreviousPerformed(
            Store<T> store,
            String   key,
            boolean  ret );

    public void hasNextPerformed(
            Store<T> store,
            String   key,
            int      n,
            boolean  ret );

    public void hasPreviousPerformed(
            Store<T> store,
            String   key,
            int      n,
            boolean  ret );

    public void nextPerformed(
            Store<T> store,
            String   key,
            T        ret,
            boolean  success );

    public void previousPerformed(
            Store<T> store,
            String   key,
            T        ret,
            boolean  success );

    public void nextPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  ret );

    public void previousPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  ret );

    public void moveByPerformed(
            Store<T> store,
            String   key,
            int      n,
            boolean  success );

    public void moveToBeforePerformed(
            Store<T> store,
            T        newPos,
            int      ret,
            boolean  success );

    public void moveToAfterPerformed(
            Store<T> store,
            T        newPos,
            int      ret,
            boolean  success );

    public void removePerformed(
            Store<T> store,
            String   key,
            boolean  success );

    public void moveToBeforePerformed(
            Store<T> store,
            String   position,
            String   key,
            int      ret,
            boolean  success );

    public void moveToAfterPerformed(
            Store<T> store,
            String   position,
            String   key,
            int      ret,
            boolean  success );

    public void setPositionToPerformed(
            Store<T>          store,
            String            position,
            CursorIterator<T> newPosition,
            boolean           success );

    public void moveToBeforeFirstPerformed(
            Store<T> store,
            String   position,
            int      ret );

    public void moveToAfterLastPerformed(
            Store<T> store,
            String   position,
            int      ret );
}