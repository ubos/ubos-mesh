//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

/**
 * Trivial implementation of KeyMapper
 */
public class StringKeyMapper
    implements
        KeyMapper<String>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public String keyToString(
            String key )
    {
        return key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String stringToKey(
            String stringKey )
    {
        return stringKey;
    }
}
