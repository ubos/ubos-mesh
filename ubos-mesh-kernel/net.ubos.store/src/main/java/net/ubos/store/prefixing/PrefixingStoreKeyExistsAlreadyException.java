//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.prefixing;

import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreKeyExistsAlreadyException;

/**
 * Thrown to indicate that a key exists already in the <code>Store</code>, for an operation
 * that requires that the key does not exist already. This subclass is used by
 * <code>PrefixingStores</code>.
 */
public class PrefixingStoreKeyExistsAlreadyException
        extends
            StoreKeyExistsAlreadyException
{
    /**
     * Constructor.
     *
     * @param store the PrefixingStore that threw this Exception
     * @param localKey the key that exists already in the PrefixingStore
     * @param cause the StoreKeyExistsAlreadyException thrown by the Store the PrefixingStore delegates to
     */
    public PrefixingStoreKeyExistsAlreadyException(
            PrefixingStore<?>              store,
            String                         localKey,
            StoreKeyExistsAlreadyException cause )
    {
        super( store, localKey, cause );
    }

    /**
     * Obtain the PrefixingStore in which the key did not exist.
     *
     * @return the PrefixingStore
     */
    @Override
    public PrefixingStore<?> getStore()
    {
        return (PrefixingStore<?>) super.getStore();
    }

    /**
     * Obtain the underlying exception, which we know to be a StoreKeyDoesNotExistException.
     *
     * @return the underlying StoreKeyDoesNotExistException
     */
    @Override
    public StoreKeyDoesNotExistException getCause()
    {
        return (StoreKeyDoesNotExistException) super.getCause();
    }
}
