//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

import net.ubos.util.history.AbstractHistory;

/**
 * Common functionality of StoreValueWithTimeUpdatedHistory implementations.
 *
 * @param <T> the type of thing whose history is stored
 */
public abstract class AbstractStoreValueWithTimeUpdatedHistory<T extends StoreValueWithTimeUpdated>
    extends
        AbstractHistory<T>
    implements
        StoreValueWithTimeUpdatedHistory<T>
{
    /**
     * Constructor.
     *
     * @param key the key for the StoreValues whose history this is
     */
    protected AbstractStoreValueWithTimeUpdatedHistory(
            String key )
    {
        theKey = key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getKey()
    {
        return theKey;
    }

    /**
     * The key for the StoreValues whose history this is.
     */
    protected final String theKey;
}
