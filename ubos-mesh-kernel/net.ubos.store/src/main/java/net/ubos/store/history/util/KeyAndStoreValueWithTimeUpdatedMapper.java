//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.util;

import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.util.KeyMapper;
import net.ubos.util.history.HasTimeUpdated;

/**
 * Combines the two mappers into a single interface.
 *
 * @param <K> the type of key
 * @param <V> the type of element of a history
 * @param <T> parameterization allows implementations that store more than just StoreValueWithTimeUpdated
 */
public interface KeyAndStoreValueWithTimeUpdatedMapper<K,V extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
    extends
        StoreValueWithTimeUpdatedMapper<V,T>,
        KeyMapper<K>
{}
