//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

import java.io.IOException;
import java.util.List;
import net.ubos.util.Tuple;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.listenerset.FlexibleListenerSet;
import net.ubos.util.logging.Log;

/**
 * Collects functionality common to Store implementations.
 *
 * @param <T> parameterization allows implementations that store more than just StoreValue
 */
public abstract class AbstractStore<T extends StoreValue>
        implements
            Store<T>
{
    private static final Log log = Log.getLogInstance( AbstractStore.class );

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll()
        throws
            IOException
    {
        return deleteAll( "" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
        throws
            IOException
    {
        return size( "" );
    }

    /**
     * Throw an IllegalArgumentException if an invalid key was handed to the Store API.
     * This may be overridden by subclasses.
     *
     * @param key the candidate key
     * @throws IllegalArgumentException if the candidate key is invalid
     */
    protected void checkKey(
            String key )
        throws
            IllegalArgumentException
    {
        if( key == null ) {
            throw new IllegalArgumentException( "key must not be null" );
        }
    }

    /**
     * Throw an IllegalArgumentException if an invalid encoding was handed to the Store API.
     * This may be overridden by subclasses.
     *
     * @param encoding the candidate encoding
     * @throws IllegalArgumentException if the candidate encoding is invalid
     */
    protected void checkEncoding(
            String encoding )
        throws
            IllegalArgumentException
    {
        if( encoding == null ) {
            throw new IllegalArgumentException( "encoding must not be null" );
        }
    }

    /**
     * Throw an IllegalArgumentException if invalid data was handed to the Store API.
     * This may be overridden by subclasses.
     *
     * @param data the candidate data
     * @throws IllegalArgumentException if the candidate data is invalid
     */
    protected void checkData(
            byte [] data )
        throws
            IllegalArgumentException
    {
        if( data == null ) {
            throw new IllegalArgumentException( "data must not be null" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDirectStoreListener(
            StoreListener<T> newListener )
    {
        theStoreListeners.addDirect( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addSoftStoreListener(
            StoreListener<T> newListener )
    {
        theStoreListeners.addSoft( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addWeakStoreListener(
            StoreListener<T> newListener )
    {
        theStoreListeners.addWeak( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeStoreListener(
            StoreListener<T> oldListener )
    {
        theStoreListeners.remove( oldListener );
    }

    /**
     * Fire a Store put event.
     *
     * @param value the StoreValue that was put
     * @param success if true, the put was successful
     */
    protected void firePutPerformed(
            T       value,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( value, success ), DISPATCH.PUT );
    }

    /**
     * Fire a Store update event.
     *
     * @param value the StoreValue that was updated
     * @param success if true, the update was successful
     */
    protected void fireUpdatePerformed(
            T       value,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( value, success ), DISPATCH.UPDATE );
    }

    /**
     * Fire a Store get event.
     *
     * @param key the key that was attempted
     * @param value the StoreValue that was obtained (in case of success), or Null in case of failure
     */
    protected void fireGetPerformed(
            String key,
            T      value )
    {
        theStoreListeners.fireEvent( Tuple.create( key, value ), DISPATCH.GET );
    }

    /**
     * Fire a Store delete event.
     *
     * @param key the key with which the data element was stored
     * @param success if true, the delete was successful
     */
    protected void fireDeletePerformed(
            String  key,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( key, success ), DISPATCH.DELETE );
    }

    /**
     * Fire a Store deleteAll event.
     *
     * @param prefix if given, indicates the prefix of all keys that were deleted. If null, indicates &quot;all&quot;.
     * @param n the number of keys that were deleted
     */
    protected void fireDeleteAllPerformed(
            String prefix,
            int    n )
    {
        theStoreListeners.fireEvent( Tuple.create( prefix, n ), DISPATCH.DELETE_ALL );
    }

    /**
     * Fire a Store containsKey event.
     *
     * @param key the key that was looked up
     * @param success if true, the Store contained the key
     */
    protected void fireContainsKey(
            String  key,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( key, success ), DISPATCH.CONTAINS_KEY );
    }

    protected void firePeekNextPerformed(
            String  position,
            T       v,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, v, success ), DISPATCH.PEEK_NEXT );
    }


    protected void firePeekPreviousPerformed(
            String  position,
            T       v,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, v, success ), DISPATCH.PEEK_PREVIOUS );
    }
    
    protected void firePeekNextPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, n, ret ), DISPATCH.PEEK_NEXT_N );
    }

    protected void firePeekPreviousPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, n, ret ), DISPATCH.PEEK_PREVIOUS_N );
    }

    protected void fireHasNextPerformed(
            String  position,
            boolean ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, ret ), DISPATCH.HAS_NEXT );
    }

    protected void fireHasPreviousPerformed(
            String  position,
            boolean ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, ret ), DISPATCH.HAS_PREVIOUS );
    }

    protected void fireHasNextPerformed(
            String  position,
            int     n,
            boolean ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, n, ret ), DISPATCH.HAS_NEXT_N );
    }

    protected void fireHasPreviousPerformed(
            String  position,
            int     n,
            boolean ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, n, ret ), DISPATCH.HAS_PREVIOUS_N );
    }

    protected void fireNextPerformed(
            String  position,
            T       ret,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, ret, success ), DISPATCH.NEXT );
    }

    protected void firePreviousPerformed(
            String  position,
            T       ret,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, ret, success ), DISPATCH.PREVIOUS );
    }

    protected void fireNextPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, n, ret ), DISPATCH.NEXT_N );
    }

    protected void firePreviousPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, n, ret ), DISPATCH.PREVIOUS_N );
    }

    protected void fireMoveByPerformed(
            String  position,
            int     n,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, n, success ), DISPATCH.MOVE_BY );
    }

    protected void fireMoveToBeforePerformed(
            String  position,
            T       newPos,
            int     ret,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, newPos, ret, success ), DISPATCH.MOVE_TO_BEFORE );
    }

    protected void fireMoveToAfterPerformed(
            String  position,
            T       newPos,
            int     ret,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, newPos, ret, success ), DISPATCH.MOVE_TO_AFTER );
    }

    protected void fireRemovePerformed(
            String  position,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, success ), DISPATCH.REMOVE );
    }
    
    protected void fireMoveToBeforePerformed(
            String  position,
            String  key,
            int     ret,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, key, ret, success ), DISPATCH.MOVE_TO_BEFORE_KEY );
    }

    protected void fireMoveToAfterPerformed(
            String  position,
            String  key,
            int     ret,
            boolean success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, key, ret, success ), DISPATCH.MOVE_TO_AFTER_KEY );
    }

    protected void fireSetPositionToPerformed(
            String            position,
            CursorIterator<T> newPosition,
            boolean           success )
    {
        theStoreListeners.fireEvent( Tuple.create( position, newPosition, success ), DISPATCH.SET_POSITION_TO );
    }

    protected void fireMoveToBeforeFirstPerformed(
            String position,
            int    ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, ret ), DISPATCH.MOVE_TO_BEFORE_FIRST );
    }

    protected void fireMoveToAfterLastPerformed(
            String position,
            int    ret )
    {
        theStoreListeners.fireEvent( Tuple.create( position, ret ), DISPATCH.MOVE_TO_AFTER_LAST );
    }

    static enum DISPATCH {
        PUT,
        UPDATE,
        GET,
        DELETE,
        DELETE_ALL,
        CONTAINS_KEY,
        PEEK_NEXT,
        PEEK_PREVIOUS,
        PEEK_NEXT_N,
        PEEK_PREVIOUS_N,
        HAS_NEXT,
        HAS_PREVIOUS,
        HAS_NEXT_N,
        HAS_PREVIOUS_N,
        NEXT,
        PREVIOUS,
        NEXT_N,
        PREVIOUS_N,
        MOVE_BY,
        MOVE_TO_BEFORE,
        MOVE_TO_AFTER,
        REMOVE,
        MOVE_TO_BEFORE_KEY,
        MOVE_TO_AFTER_KEY,
        SET_POSITION_TO,
        MOVE_TO_BEFORE_FIRST,
        MOVE_TO_AFTER_LAST
    }

    /**
     * The StoreListeners.
     */
    private final FlexibleListenerSet<StoreListener<T>,Tuple,DISPATCH> theStoreListeners
            = new FlexibleListenerSet<StoreListener<T>,Tuple,DISPATCH>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    protected void fireEventToListener(
                            StoreListener<T> l,
                            Tuple            e,
                            DISPATCH         p )
                    {
                        switch( p ) {
                            case PUT:
                                l.putPerformed( AbstractStore.this, (T) e.get( 0 ), (Boolean) e.get( 1 ));
                                break;
                            
                            case UPDATE:
                                l.updatePerformed( AbstractStore.this, (T) e.get( 0 ), (Boolean) e.get( 1 ));
                                break;
                            
                            case GET:
                                l.getPerformed( AbstractStore.this, (String) e.get( 0 ), (T) e.get( 1 ));
                                break;
                            
                            case DELETE:
                                l.deletePerformed( AbstractStore.this, (String) e.get( 0 ), (Boolean) e.get( 1 ));
                                break;
                            
                            case DELETE_ALL:
                                l.deleteAllPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ));
                                break;
                            
                            case CONTAINS_KEY:
                                l.containsKeyPerformed( AbstractStore.this, (String) e.get( 0 ), (Boolean) e.get( 1 ));
                                break;
                            
                            case PEEK_NEXT:
                                l.peekNextPerformed( AbstractStore.this, (String) e.get( 0 ), (T) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case PEEK_PREVIOUS:
                                l.peekPreviousPerformed( AbstractStore.this, (String) e.get( 0 ), (T) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case PEEK_NEXT_N:
                                l.peekNextPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ), (List<T>) e.get( 2 ));
                                break;
                            
                            case PEEK_PREVIOUS_N:
                                l.peekPreviousPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ), (List<T>) e.get( 2 ));
                                break;
                            
                            case HAS_NEXT:
                                l.hasNextPerformed( AbstractStore.this, (String) e.get( 0 ), (Boolean) e.get( 1 ));
                                break;
                            
                            case HAS_PREVIOUS:
                                l.hasPreviousPerformed( AbstractStore.this, (String) e.get( 0 ), (Boolean) e.get( 1 ));
                                break;
                            
                            case HAS_NEXT_N:
                                l.hasNextPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case HAS_PREVIOUS_N:
                                l.hasPreviousPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case NEXT:
                                l.nextPerformed( AbstractStore.this, (String) e.get( 0 ), (T) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case PREVIOUS:
                                l.previousPerformed( AbstractStore.this, (String) e.get( 0 ), (T) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case NEXT_N:
                                l.nextPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ), (List<T>) e.get( 2 ));
                                break;
                            
                            case PREVIOUS_N:
                                l.previousPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ), (List<T>) e.get( 2 ));
                                break;
                            
                            case MOVE_BY:
                                l.moveByPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case MOVE_TO_BEFORE:
                                l.moveToBeforePerformed( AbstractStore.this, (T) e.get( 0 ), (Integer) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case MOVE_TO_AFTER:
                                l.moveToAfterPerformed( AbstractStore.this, (T) e.get( 0 ), (Integer) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case REMOVE:
                                l.removePerformed( AbstractStore.this, (String) e.get( 0 ), (Boolean) e.get( 1 ));
                                break;
                            
                            case MOVE_TO_BEFORE_KEY:
                                l.moveToBeforePerformed( AbstractStore.this, (String) e.get( 0 ), (String) e.get( 1 ), (Integer) e.get( 2 ), (Boolean) e.get( 3 ));
                                break;
                            
                            case MOVE_TO_AFTER_KEY:
                                l.moveToAfterPerformed( AbstractStore.this, (String) e.get( 0 ), (String) e.get( 1 ), (Integer) e.get( 2 ), (Boolean) e.get( 3 ));
                                break;
                            
                            case SET_POSITION_TO:
                                l.setPositionToPerformed( AbstractStore.this, (String) e.get( 0 ), (CursorIterator<T>) e.get( 1 ), (Boolean) e.get( 2 ));
                                break;
                            
                            case MOVE_TO_BEFORE_FIRST:
                                l.moveToBeforeFirstPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ));
                                break;
                            
                            case MOVE_TO_AFTER_LAST:
                                l.moveToAfterLastPerformed( AbstractStore.this, (String) e.get( 0 ), (Integer) e.get( 1 ));
                                break;
                            
                            default:
                                log.error( "Unexpected value", p );
                               break;
                        }
                    }
    };
}
