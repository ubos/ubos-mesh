//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.util;

import java.io.IOException;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreValueWithTimeUpdatedHistory;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.StoreWithHistoryEntryDoesNotExistException;
import net.ubos.store.history.StoreWithHistoryKeyDoesNotExistException;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.history.AbstractSwappingHistory;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.cursoriterator.history.MappingHistoryCursorIterator;
import net.ubos.util.cursoriterator.history.ZeroElementHistoryCursorIterator;
import net.ubos.util.history.HasTimeUpdated;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * An implementation of History that is backed by a StoreWithHistory.
 * This implementation is quite inefficient with respect to traversing the time line,
 * because it needs to go out to the underlying StoreWithHistory to determine the
 * next or previous element of the history every single time.
 *
 * @param <E> the type of thing for which this is a history
 * @param <T> the type of StoreValueWithTimeUpdated
 */
public abstract class StoreWithHistoryBackedSwappingHistory<E extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
    extends
        AbstractSwappingHistory<E>
{
    private static final Log log = Log.getLogInstance(StoreWithHistoryBackedSwappingHistory.class );

    /**
     * Builder object.
     *
     * @param <E> same parameterization as StoreWithHistoryBackedHistory
     * @param <T> same parameterization as StoreWithHistoryBackedHistory
     */
    public static class Builder<E extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
    {
        /**
         * Factory method for Builder.
         *
         * @param key the key that identifies the History in the StoreWithHistory that contains the data
         * @param mapper the <code>StoreValueMapper</code> to use
         * @param store the underlying Store for the StoreBackedSwappingSmartMap
         * @return the Builder
         * @param <E> same parameterization as StoreWithHistoryBackedHistory
         * @param <T> same parameterization as StoreWithHistoryBackedHistory
         */
        public static <E extends HasTimeUpdated,T extends StoreValueWithTimeUpdated> StoreWithHistoryBackedSwappingHistory.Builder<E,T> create(
                String                               key,
                StoreValueWithTimeUpdatedMapper<E,T> mapper,
                StoreWithHistory<T>                  store )
        {
            return new StoreWithHistoryBackedSwappingHistory.Builder<>( key, mapper, store );
        }

        /**
         * Factory method for Builder.
         *
         * @param key the key that identifies the History in the StoreWithHistory that contains the data
         * @param mapper the <code>StoreValueMapper</code> to use
         * @param store the underlying Store for the StoreBackedSwappingSmartMap
         */
        protected Builder(
                String                               key,
                StoreValueWithTimeUpdatedMapper<E,T> mapper,
                StoreWithHistory<T>                  store )
        {
            theKey    = key;
            theMapper = mapper;
            theStore  = store;
        }

        /**
         * Instantiate WeakReferences, instead of SoftReferences (the default).
         *
         * @return the Builder itself
         */
        public Builder<E,T> useWeak()
        {
            theUseWeak = true;
            return this;
        }

        /**
         * Set the Map to use as a cache, if not using the default HashMap.
         *
         * @param mapAsLocalCache the Map to use
         * @return the Builder itself
         */
        public Builder<E,T> mapAsLocalCache(
                SmartMap<Long,ReferenceWithTimeUpdated<E>> mapAsLocalCache )
        {
            theMapAsLocalCache = mapAsLocalCache;
            return this;
        }

        /**
         * Build the object.
         *
         * @return the built object
         */
        public StoreWithHistoryBackedSwappingHistory<E,T> build()
        {
            if( theMapAsLocalCache == null ) {
                theMapAsLocalCache = MSmartMap.create();
            }
            StoreWithHistoryBackedSwappingHistory<E,T> ret;

            if( theUseWeak ) {
                ret = new Weak<>( theKey, theMapAsLocalCache, theMapper, theStore );
            } else {
                ret = new Soft<>( theKey, theMapAsLocalCache, theMapper, theStore );
            }
            return ret;
        }

        protected final StoreValueWithTimeUpdatedMapper<E,T> theMapper;
        protected final StoreWithHistory<T>                  theStore;
        protected String                                     theKey;
        protected boolean                                    theUseWeak         = false;
        protected SmartMap<Long,ReferenceWithTimeUpdated<E>> theMapAsLocalCache = null;
    }

    /**
     * Constructor.
     *
     * @param key the key that identifies the History in the StoreWithHistory that contains the data
     * @param localCache the local cache to use
     * @param mapper knows how to map StoreValueWithTimeUpdate to T and back
     * @param store the StoreWithHistory that contains the data
     */
    protected StoreWithHistoryBackedSwappingHistory(
            String                                     key,
            SmartMap<Long,ReferenceWithTimeUpdated<E>> localCache,
            StoreValueWithTimeUpdatedMapper<E,T>       mapper,
            StoreWithHistory<T>                        store )
    {
        super( localCache );

        theKey    = key;
        theMapper = mapper;
        theStore  = store;
    }

    /**
     * Obtain the key.
     *
     * @return the key
     */
    public String getKey()
    {
        return theKey;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void clear()
    {
        try {
            theStore.deleteAll();
        } catch( IOException ex ) {
            log.error( ex );
        }
        clearLocalCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<E> iterator()
    {
        StoreValueWithTimeUpdatedHistory<T> delegate;

        try {
            delegate = theStore.history( theKey );

        } catch( IOException ex ) {
            log.error( ex );
            return null;

        } catch( StoreWithHistoryKeyDoesNotExistException ex ) {
            return ZeroElementHistoryCursorIterator.create();
        }

        HistoryCursorIterator<E> ret = new MappingHistoryCursorIterator<E,T>(
                delegate.iterator(),
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public E mapDelegateValueToHere(
                            T delegateValue )
                    {
                        try {
                            E value = theMapper.decodeValue( delegateValue );
                            return value;
                        } catch( StoreValueWithTimeUpdatedDecodingException ex ) {
                            log.error( ex );
                        }
                        return null;
                    }

                    @Override
                    public T mapHereToDelegateValue(
                            E value )
                    {
                        try {
                            T delegateValue = theMapper.encodeValue( theKey, value );
                            return delegateValue;
                        } catch( StoreValueWithTimeUpdatedEncodingException ex ) {
                            log.error( ex );
                        }
                        return null;
                    }
                });

        return ret;
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            E toAdd )
        throws
            IllegalArgumentException
    {
        E already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same element:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "Element exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected E loadValueFromStorage(
            long t )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "loadValueFromStorage", t );
        }

        try {
            T v = theStore.get( theKey, t );

            E ret = theMapper.decodeValue( v );

            return ret;

        } catch( IOException ex ) {
            log.error( this, ".loadValueFromStorage", t, ex );

        } catch( StoreWithHistoryEntryDoesNotExistException ex ) {
            // no op
            if( log.isDebugEnabled() ) {
                log.debug( this, ".loadValueFromStorage", t, ex );
            }

        } catch( StoreValueWithTimeUpdatedDecodingException ex ) {
            log.error( this, ".loadValueFromStorage", t, ex );
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveValueToStorage(
            E newValue )
    {
       if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "saveValueToStorage", newValue );
        }

        try {
            T storeValue = theMapper.encodeValue( theKey, newValue );

            theStore.putOrUpdate( storeValue );

        } catch( IOException ex ) {
            log.error( ex );
        } catch( StoreValueWithTimeUpdatedEncodingException ex ) {
            log.error( ex );
        }    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void removeValueFromStorage(
            long t )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "removeValueFromStorage", t );
        }

        try {
            theStore.delete( theKey, t );

        } catch( IOException ex ) {
            log.error( ex );
        } catch( StoreWithHistoryEntryDoesNotExistException ex ) {
            log.error( ex );
        }
    }

    /**
     * The key of the element in the StoreWithHistory whose history we are interested in.
     */
    protected final String theKey;

    /**
     * Knows how to map values to and from the store.
     */
    protected final StoreValueWithTimeUpdatedMapper<E,T> theMapper;

    /**
     * The underlying StoreWithHistory.
     */
    protected final StoreWithHistory<T> theStore;

    /**
     * Implementation that instantiates SoftReferences.
     *
     * @param <E> same parameterization as StoreWithHistoryBackedHistory
     * @param <T> same parameterization as StoreWithHistoryBackedHistory
     */
    public static class Soft<E extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
        extends
            StoreWithHistoryBackedSwappingHistory<E,T>
    {
        protected Soft(
                String                                     key,
                SmartMap<Long,ReferenceWithTimeUpdated<E>> localCache,
                StoreValueWithTimeUpdatedMapper<E,T>       mapper,
                StoreWithHistory<T>                        store )
        {
            super( key, localCache, mapper, store );
        }

        @Override
        protected ReferenceWithTimeUpdated<E> createReference(
                E value )
        {
            return new SoftReferenceWithTimeUpdated<>( value, theQueue, value.getTimeUpdated() );
        }
    }

    /**
     * Implementation that instantiates WeakReferences.
     *
     * @param <E> same parameterization as StoreWithHistoryBackedHistory
     * @param <T> same parameterization as StoreWithHistoryBackedHistory
     */
    public static class Weak<E extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
        extends
            StoreWithHistoryBackedSwappingHistory<E,T>
    {
        protected Weak(
                String                                     key,
                SmartMap<Long,ReferenceWithTimeUpdated<E>> localCache,
                StoreValueWithTimeUpdatedMapper<E,T>       mapper,
                StoreWithHistory<T>                        store )
        {
            super( key, localCache, mapper, store );
        }

        @Override
        protected ReferenceWithTimeUpdated<E> createReference(
                E value )
        {
            return new WeakReferenceWithTimeUpdated<>( value, theQueue, value.getTimeUpdated() );
        }
    }
}
