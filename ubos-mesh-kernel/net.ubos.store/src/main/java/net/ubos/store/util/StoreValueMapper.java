//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import java.text.ParseException;
import net.ubos.store.StoreValue;

/**
 * Classes implementing this interface know how to map key-value pairs, of
 * parameterized types for both key and value, into and from a <code>Store</code>.
 *
 * @param <K> the type of key
 * @param <V> the type of value
 * @param <T> parameterization allows implementations that store more than just StoreValue
 */
public interface StoreValueMapper<K,V,T extends StoreValue>
    extends
        KeyMapper<K>
{
    /**
     * Obtain the default encoding id of this StoreValueMapper. This StoreValueMapper
     * may support multiple encodings -- in fact, it should -- but that's not visible
     * to the outside. This is the encoding id by which it encodes.
     *
     * @return the default encoding id
     */
    public abstract String getDefaultEncodingId();

    /**
     * Map a StoreValue to a value.
     *
     * @param value the StoreValue
     * @return the value
     * @throws StoreValueDecodingException thrown if the StoreValue could not been decoded
     */
    public abstract V decodeValue(
            T value )
        throws
            StoreValueDecodingException;

    /**
     * Map a value to a (subclass of) StoreValue.
     *
     * @param key the key for the value
     * @param value the value
     * @return the byte array
     * @throws StoreValueEncodingException thrown if the value could not been encoded
     */
    public abstract T encodeValue(
            K key,
            V value )
        throws
            StoreValueEncodingException;
}
