//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import java.io.IOException;
import java.lang.ref.Reference;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.store.Store;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.util.cursoriterator.BufferingCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.AbstractSwappingSmartMap;

/**
 * A SmartMap that is backed by a Store. This SmartMap contains its values via WeakReferences,
 * meaning they (and they corresponding keys) will disappear when not used, and magically
 * re-appear when needed.
 *
 * @param <K> the type of key (e.g. Identifier)
 * @param <V> the type of value
 * @param <T> parameterization allows implementations that store more than just StoreValue
 */
public abstract class StoreBackedSwappingSmartMap<K,V,T extends StoreValue>
    extends
        AbstractSwappingSmartMap<K,V>
    implements
        CanBeDumped
{
    private static final Log log = Log.getLogInstance( StoreBackedSwappingSmartMap.class );

    /**
     * Builder object.
     *
     * @param <K> same parameterization as StoreBackedSwappingSmartMap
     * @param <V> same parameterization as StoreBackedSwappingSmartMap
     * @param <T> same parameterization as StoreBackedSwappingSmartMap
     */
    public static class Builder<K,V,T extends StoreValue>
    {
        /**
         * Factory method for Builder.
         *
         * @param mapper the <code>StoreValueMapper</code> to use
         * @param store the underlying Store for the StoreBackedSwappingSmartMap
         * @return the Builder
         * @param <K> same parameterization as StoreBackedSwappingSmartMap
         * @param <V> same parameterization as StoreBackedSwappingSmartMap
         * @param <T> same parameterization as StoreBackedSwappingSmartMap
         */
        public static <K,V,T extends StoreValue> Builder<K,V,T> create(
                StoreValueMapper<K,V,T> mapper,
                Store<T>                store )
        {
            return new Builder<>( mapper, store );
        }

        /**
         * Factory method for Builder.
         *
         * @param mapper the <code>StoreValueMapper</code> to use
         * @param store the underlying Store for the StoreBackedSwappingSmartMap
         */
        protected Builder(
                StoreValueMapper<K,V,T> mapper,
                Store<T>                store )
        {
            theMapper = mapper;
            theStore  = store;
        }

        /**
         * Instantiate WeakReferences, instead of SoftReferences (the default).
         *
         * @return the Builder itself
         */
        public Builder<K,V,T> useWeak()
        {
            theUseWeak = true;
            return this;
        }

        /**
         * Set the Map to use as a cache, if not using the default HashMap.
         *
         * @param mapAsLocalCache the Map to use
         * @return the Builder itself
         */
        public Builder<K,V,T> mapAsLocalCache(
                Map<K,Reference<V>> mapAsLocalCache )
        {
            theMapAsLocalCache = mapAsLocalCache;
            return this;
        }

        /**
         * Build the object.
         *
         * @return the built object
         */
        public StoreBackedSwappingSmartMap<K,V,T> build()
        {
            if( theMapAsLocalCache == null ) {
                theMapAsLocalCache = new HashMap<>();
            }
            StoreBackedSwappingSmartMap<K,V,T> ret;

            if( theUseWeak ) {
                ret = new Weak<>( theMapAsLocalCache, theMapper, theStore );
            } else {
                ret = new Soft<>( theMapAsLocalCache, theMapper, theStore );
            }
            return ret;
        }

        protected final StoreValueMapper<K,V,T> theMapper;
        protected final Store<T>                theStore;
        protected boolean                       theUseWeak = false;
        protected Map<K,Reference<V>>           theMapAsLocalCache = null;
    }

    /**
     * Constructor.
     *
     * @param localCache the Map to use as local cache
     * @param mapper the <code>StoreValueMapper</code> to use
     * @param store the underlying <code>Store</code>
     */
    protected StoreBackedSwappingSmartMap(
            Map<K,Reference<V>>     localCache,
            StoreValueMapper<K,V,T> mapper,
            Store<T>                store )
    {
        super( localCache );

        theMapper = mapper;
        theStore  = store;
    }

    /**
     * Obtain the <code>Store</code> that backs this <code>StoreBackedSwappingHashMap</code>.
     *
     * @return the Store
     */
    public Store<T> getStore()
    {
        return theStore;
    }

    /**
     * Obtain the StoreValueMapper.
     *
     * @return the StoreValueMapper
     */
    public StoreValueMapper<K,V,T> getStoreValueMapper()
    {
        return theMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void clear()
    {
        try {
            theStore.deleteAll();
        } catch( IOException ex ) {
            log.error( ex );
        }
        clearLocalCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        int ret = Integer.MAX_VALUE; // not sure what else to return, given that we don't know
        try {
            ret = theStore.size();

        } catch( IOException ex ) {
            log.error( ex );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        try {
            boolean ret = theStore.isEmpty();
            return ret;

        } catch( IOException ex ) {
            log.error( ex );
            return true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueUpdated(
            K key,
            V value )
    {
        saveValueToStorage( key, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        CursorIterator<K> unbuffered = new MappingCursorIterator<>(
                theStore.iterator(),
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public K mapDelegateValueToHere(
                            T delegateValue )
                    {
                        try {
                            return theMapper.stringToKey( delegateValue.getKey() );

                        } catch( ParseException ex ) {
                            log.error( ex );
                            return null;
                        }
                    }

                    @Override
                    public T mapHereToDelegateValue(
                            K value )
                    {
                        throw new UnsupportedOperationException( "Should not be called" );
                    }
                });

        BufferingCursorIterator<K> ret = BufferingCursorIterator.create(
                BUFFERING_ITERATOR_BUFSIZE,
                unbuffered );
        
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<V> valueIterator()
    {
        CursorIterator<V> unbuffered = new MappingCursorIterator<>(
                theStore.iterator(), // don't go through the key iterator any more -- this is more efficient
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public V mapDelegateValueToHere(
                            T delegateValue )
                    {
                        try {
                            K key = theMapper.stringToKey( delegateValue.getKey() );

                            V ret = get( key );
                            return ret;

                        } catch( ParseException ex ) {
                            log.error( ex );
                            return null;
                        }
                    }

                    @Override
                    public T mapHereToDelegateValue(
                           V value )
                    {
                        throw new UnsupportedOperationException( "Should not be called" );
                    }
                });
        
        
        BufferingCursorIterator<V> ret = BufferingCursorIterator.create(
                BUFFERING_ITERATOR_BUFSIZE,
                unbuffered );
        
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected V loadValueFromStorage(
            K key )
    {
         if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "loadValueFromStorage", key );
        }

        String stringKey = theMapper.keyToString( key );

        try {
            T v = theStore.get( stringKey );

            V ret = theMapper.decodeValue( v );

            return ret;

        } catch( IOException ex ) {
            log.error( this, ".loadValueFromStorage", key, ex );

        } catch( StoreKeyDoesNotExistException ex ) {
            // no op
            if( log.isDebugEnabled() ) {
                log.debug( this, ".loadValueFromStorage", key, ex );
            }

        } catch( StoreValueDecodingException ex ) {
            log.error( this, ".loadValueFromStorage", key, ex );
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveValueToStorage(
            K key,
            V newValue )
    {
       if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "saveValueToStorage", key, newValue );
        }

        try {
            T storeValue = theMapper.encodeValue( key, newValue );

            theStore.putOrUpdate( storeValue );

        } catch( IOException ex ) {
            log.error( ex );
        } catch( StoreValueEncodingException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void removeValueFromStorage(
            K key )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "removeValueFromStorage", key );
        }

        String stringKey = theMapper.keyToString( key );

        try {
            theStore.delete( stringKey );

        } catch( IOException ex ) {
            log.error( ex );
        } catch( StoreKeyDoesNotExistException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theStore",
                    "theMapper"
                },
                new Object[] {
                    theStore,
                    theMapper
                } );
    }

    /**
     * The <code>Store</code> by which this <code>StoreBackedSwappingHashMap</code> is backed.
     */
    protected final Store<T> theStore;

    /**
     * The Mapper that we use for the main Store,
     */
    protected final StoreValueMapper<K,V,T> theMapper;

    /**
     * Implementation that instantiates SoftReferences.
     *
     * @param <K> the type of key (e.g. Identifier)
     * @param <V> the type of value
     * @param <T> parameterization allows implementations that store more than just StoreValue
     */
    public static class Soft<K,V,T extends StoreValue>
        extends
            StoreBackedSwappingSmartMap<K,V,T>
    {
        protected Soft(
                Map<K,Reference<V>>     localCache,
                StoreValueMapper<K,V,T> mapper,
                Store<T>                store )
        {
            super( localCache, mapper, store );
        }

        @Override
        protected Reference<V> createReference(
                K key,
                V value )
        {
            return new AbstractSwappingSmartMap.SoftEntryReference<>( key, value, theQueue );
        }
    }

    /**
     * Implementation that instantiates WeakReferences.
     *
     * @param <K> the type of key (e.g. Identifier)
     * @param <V> the type of value
     * @param <T> parameterization allows implementations that store more than just StoreValue
     */
    public static class Weak<K,V,T extends StoreValue>
        extends
            StoreBackedSwappingSmartMap<K,V,T>
    {
        protected Weak(
                Map<K,Reference<V>>     localCache,
                StoreValueMapper<K,V,T> mapper,
                Store<T>                store )
        {
            super( localCache, mapper, store );
        }

        @Override
        protected Reference<V> createReference(
                K key,
                V value )
        {
            return new AbstractSwappingSmartMap.WeakEntryReference<>( key, value, theQueue );
        }
    }
    
    /**
     * Size of the cache for the iterator. FIXME?
     */
    public static final int BUFFERING_ITERATOR_BUFSIZE = 50;
}
