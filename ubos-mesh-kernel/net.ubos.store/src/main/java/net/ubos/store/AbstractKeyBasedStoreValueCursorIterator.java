//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Collects functionality common to StoreCursorIterator implementations that work on
 * Stores whose stored data is organized by ordered keys.
 *
 * @param <T> corresponds to parameterization of the Store
 */
public abstract class AbstractKeyBasedStoreValueCursorIterator<T extends StoreValue>
        extends
            AbstractStoreValueCursorIterator<T>
{
    /**
     * Constructor, for subclasses only.
     *
     * @param store the Store to iterate over
     * @param position the key for the current position.
     */
    protected AbstractKeyBasedStoreValueCursorIterator(
            AbstractStore<T> store,
            String           position )
    {
        super( store );

        thePosition = position;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekNext()
    {
        List<T> found = findNextIncluding( thePosition, 1 );

        if( found.size() == 1 ) {
            T ret = found.get( 0 );
            firePeekNextPerformed( thePosition, ret, true );
            return ret;
        } else {
            firePeekNextPerformed( thePosition, null, false );
            throw new NoSuchElementException();
        }
    }

    /**
     * Obtain the next key, without iterating forward.
     *
     * @return the next key
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    public String peekNextKey()
    {
        List<String> found = findNextKeyIncluding( thePosition, 1 );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peekPrevious()
    {
        List<T> found = findPreviousExcluding( thePosition, 1 );

        if( found.size() == 1 ) {
            T ret = found.get( 0 );
            firePeekPreviousPerformed( thePosition, ret, true );
            return ret;
        } else {
            firePeekPreviousPerformed( thePosition, null, false );
            throw new NoSuchElementException();
        }
    }

    /**
     * Obtain the previous key, without iterating backwards.
     *
     * @return the previous key
     * @throws NoSuchElementException iteration has no current element (e.g. because the end of the iteration was reached)
     */
    public String peekPreviousKey()
    {
        List<String> found = findPreviousKeyExcluding( thePosition, 1 );

        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> peekNext(
            int n )
    {
        List<T> found = findNextIncluding( thePosition, n );
        firePeekNextPerformed( thePosition, n, found );
        return found;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> peekPrevious(
            int n )
    {
        List<T> found = findPreviousExcluding( thePosition, n );
        firePeekPreviousPerformed( thePosition, n, found );
        return found;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return hasNext( 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        return hasPrevious( 1 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        int found = hasNextIncluding( thePosition );
        if( found >= n ) {
            fireHasNextPerformed( thePosition, n, true );
            return true;
        } else {
            fireHasNextPerformed( thePosition, n, false );
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        int found = hasPreviousExcluding( thePosition );
        if( found >= n ) {
            fireHasPreviousPerformed( thePosition, n, true );
            return true;
        } else {
            fireHasPreviousPerformed( thePosition, n, false );
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next()
    {
        List<T> found = next( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Returns the next key in the iteration.
     *
     * @return the next key in the iteration.
     * @throws NoSuchElementException iteration has no more elements.
     */
    public String nextKey()
    {
        List<String> found = nextKey( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> next(
            int n )
    {
        // we have to go forward by one more, so we can set the new position right after what's returned here
        List<T> found = findNextIncluding( thePosition, n+1 );
        List<T> ret   = found.subList( 0, Math.min( n, found.size()));

        if( found.size() == n+1 ) {
            thePosition = found.get( found.size()-1 ).getKey();
        } else {
            moveToAfterLast();
        }
        fireNextPerformed( thePosition, n, ret );
        return ret;
    }

    /**
     * <p>Obtain the next N keys. If fewer than N elements are available, return
     * as many keys are available in a shorter array.
     *
     * @param n the number of keys to obtain
     * @return the next no more than N keys
     * @see #previousKey(int)
     */
    public List<String> nextKey(
            int n )
    {
        // we have to go forward by one more, so we can set the new position right after what's returned here
        List<String> found = findNextKeyIncluding( thePosition, n+1 );
        List<String> ret   = found.subList( 0, Math.min( n, found.size()));

        if( found.size() == n+1 ) {
            thePosition = found.get( found.size()-1 );
        } else {
            moveToAfterLast();
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T previous()
    {
        List<T> found = previous( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Returns the previous key in the iteration.
     *
     * @return the previous key in the iteration.
     * @see #nextKey()
     */
    public String previousKey()
    {
        List<String> found = previousKey( 1 );
        if( found.size() == 1 ) {
            return found.get( 0 );
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> previous(
            int n )
    {
        List<T> ret = findPreviousExcluding( thePosition, n );
        if( ret.size() == n ) {
            thePosition = ret.get( ret.size()-1 ).getKey();
        } else{
            moveToBeforeFirst();
        }
        firePreviousPerformed( thePosition, n, ret );
        return ret;
    }

    /**
     * <p>Obtain the previous N keys. If fewer than N elements are available, return
     * as many keys are available in a shorter array.
     *
     * <p>Note that the keys
     * will be ordered in the opposite direction as you might expect: they are
     * returned in the sequence in which the CursorIterator visits them, not in the
     * sequence in which the underlying Iterable stores them.
     *
     * @param n the number of keys to obtain
     * @return the previous no more than N keys
     * @see #next(int)
     */
    public List<String> previousKey(
            int n )
    {
        List<String> found = findPreviousKeyExcluding( thePosition, n );
        if( found.size() == n ) {
            thePosition = found.get( found.size()-1 );
        } else{
            moveToBeforeFirst();
        }
        return found;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        boolean success = false;
        try {
            if( n == 0 ) {
                return;
            }
            String newPosition = findKeyAt( thePosition, n );
            thePosition = newPosition;

        } finally {
            fireMoveByPerformed( thePosition, n, success);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            StoreValue pos )
        throws
            NoSuchElementException
    {
        return moveToBefore( pos.getKey() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            StoreValue pos )
        throws
            NoSuchElementException
    {
        return moveToAfter( pos.getKey() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            String key )
        throws
            NoSuchElementException
    {
        boolean success = false;
        int     ret     = 0;
        
        try {
            if( !thePosition.equals( key )) {
                // FIXME this does not look right
                int distance = determineDistance( thePosition, key );
                if( distance < 0 ) {
                    distance = -determineDistance(key, thePosition );
                    if( distance > 0 ) {
                        throw new NoSuchElementException();
                    }
                }
                ret = distance;
                thePosition = key;
            }
            success = true;
        } finally {
            fireMoveToBeforePerformed( thePosition, key, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            String key )
        throws
            NoSuchElementException
    {
        int ret = moveToBefore( key );
        next();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret;

        try {
            String newPosition = getBeforeFirstPosition();

            ret         = determineDistance( thePosition, newPosition );
            thePosition = newPosition;

        } catch( NoSuchElementException ex ) {
            // empty store
            ret = 0;
        }
        fireMoveToBeforeFirstPerformed( thePosition, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        String newPosition = getAfterLastPosition();
        int    ret         = determineDistance( thePosition, newPosition );

        thePosition = newPosition;

        fireMoveToAfterLastPerformed( thePosition, ret );
 
        return ret;
    }

    /**
     * Find the next n StoreValues, including the StoreValue for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValue
     * @param n the number of StoreValues to find
     * @return the found StoreValues
     */
    protected abstract List<T> findNextIncluding(
            String key,
            int    n );

    /**
     * Find the next n keys, including key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key the first key
     * @param n the number of keys to find
     * @return the found keys
     */
    protected abstract List<String> findNextKeyIncluding(
            String key,
            int    n );

    /**
     * Find the previous n StoreValues, excluding the StoreValue for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValue NOT to be returned
     * @param n the number of StoreValues to find
     * @return the found StoreValues
     */
    protected abstract List<T> findPreviousExcluding(
            String key,
            int    n );

    /**
     * Find the previous n keys, excluding the key for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key the first key NOT to be returned
     * @param n the number of keys to find
     * @return the found keys
     */
    protected abstract List<String> findPreviousKeyExcluding(
            String key,
            int    n );

    /**
     * Count the number of elements following and including the one with the key.
     *
     * @param key the key
     * @return the number of elements
     */
    protected abstract int hasNextIncluding(
            String key );

    /**
     * Count the number of elements preceding and excluding the one with the key.
     *
     * @param key the key
     * @return the number of elements
     */
    protected abstract int hasPreviousExcluding(
            String key );

    /**
     * Find the key N elements up or down from the current key.
     *
     * @param key the current key
     * @param delta the number of elements up (positive) or down (negative)
     * @return the found key, or null
     * @throws NoSuchElementException thrown if the delta went beyond the "after last" or "before first" element
     */
    protected abstract String findKeyAt(
            String key,
            int    delta )
       throws
            NoSuchElementException;

    /**
     * Helper method to determine the number of steps to go from one key to the other.
     *
     * @param from the start key
     * @param to the end key
     * @return the distance
     */
    protected abstract int determineDistance(
            String from,
            String to );

    /**
     * Determine the key at the very beginning.
     *
     * @return the key
     */
    protected abstract String getBeforeFirstPosition();

    /**
     * Determine the key at the very end.
     *
     * @return the key
     */
    protected abstract String getAfterLastPosition();

    /**
     * The key for the current position.
     */
    protected String thePosition;
}
