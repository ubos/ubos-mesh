//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

/**
 * Thrown to indicate that an entry already exists for the key and timeUpdated
 * combination in a {@link StoreWithHistory}, for an operation that requires that such
 * an entry does not exist already.
 */
public class StoreWithHistoryEntryExistsAlreadyException
        extends
            StoreWithHistoryException
{
    /**
     * Constructor.
     *
     * @param store the StoreWithHistory in which the key did exist already
     * @param key the key that already exists in the StoreWithHistory
     * @param timeUpdated the timeUpdated that already exists in the StoreWithHistory
     */
    public StoreWithHistoryEntryExistsAlreadyException(
            StoreWithHistory<?> store,
            String              key,
            long                timeUpdated )
    {
        super( store, key, "HistoryStore key exists already: " + key + " at time " + timeUpdated, null );

        theTimeUpdated = timeUpdated;
    }

    /**
     * Constructor.
     *
     * @param store the StoreWithHistory in which the key did exist already
     * @param key the key that already exists in the StoreWithHistory
     * @param timeUpdated the timeUpdated that already exists in the StoreWithHistory
     * @param cause the underlying cause
     */
    public StoreWithHistoryEntryExistsAlreadyException(
            StoreWithHistory<?> store,
            String              key,
            long                timeUpdated,
            Throwable           cause )
    {
        super( store, key, "HistoryStore key exists already: " + key + " at time " + timeUpdated, cause );

        theTimeUpdated = timeUpdated;
    }

    /**
     * Obtain the timeUpdated related to which the Exception occurred.
     *
     * @return the timeUpdated
     */
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * The timeUpdated related to which this Exception occurred.
     */
    protected final long theTimeUpdated;
}
