//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import net.ubos.util.HasKey;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * <p>A <code>StoreValue</code> encapsulates into one Object the data and meta-data written to
 * and read from a Store for a given key.</p>
 */
public class StoreValue
        implements
            HasKey,
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param key the key by which this data element was found
     * @param encodingId the id of the encoding that was used to encode the data element. This must be 64 bytes or less.
     * @param data the data element, expressed as a sequence of bytes
     */
    public StoreValue(
            String  key,
            String  encodingId,
            byte [] data )
    {
        theKey        = key;
        theEncodingId = encodingId;
        theData       = data;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String getKey()
    {
        return theKey;
    }

    /**
     * Obtain the id of the encoding that was used to encode the data element.
     * This is 64 bytes or less.
     *
     * @return the encoding id
     */
    public final String getEncodingId()
    {
        return theEncodingId;
    }

    /**
     * The data, as a sequence of bytes.
     *
     * @return the data, as a sequence of bytes
     */
    public byte [] getData()
    {
        return theData;
    }

    /**
     * Alternate method to retrieve the data, as an InputStream.
     *
     * @return the data, as an <code>InputStream</code>.
     */
    public InputStream getDataAsStream()
    {
        return new ByteArrayInputStream( theData );
    }

    /**
     * Create a copy of this StoreValue object, but with a different key.
     * If this class is subclassed, this method must return an instance of the subclass, too.
     *
     * @param newKey the new key
     * @return the new StoreValue
     */
    public StoreValue copyWithNewKey(
            String newKey )
    {
        return new StoreValue( newKey, theEncodingId, theData );
    }

    /**
     * Create a copy of this StoreValue object, but with a different encoding and data.
     * If this class is subclassed, this method must return an instance of the subclass, too
     *
     * @param newEncoding the new encoding
     * @param newData the new value
     * @return the new StoreValue
     */
    public StoreValue copyWithNewEncodingData(
            String  newEncoding,
            byte [] newData )
    {
        return new StoreValue( theKey, newEncoding, newData );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(
            Object other )
    {
        if( !( other instanceof StoreValue )) {
            return false;
        }
        StoreValue realOther = (StoreValue) other;

        if( !theKey.equals( realOther.theKey )) {
            return false;
        }
        if( !theEncodingId.equals( realOther.theEncodingId )) {
            return false;
        }
        if( theData.length != realOther.theData.length ) {
            return false;
        }
        for( int i=0 ; i<theData.length ; ++i ) {
            if( theData[i] != realOther.theData[i] ) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return theKey.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        StringBuilder almost = new StringBuilder();
        almost.append( getClass().getName() );
        almost.append( "{ key: " );
        almost.append( theKey );
        almost.append( " }" );
        return almost.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theKey",
                    "theEncodingId",
                    "theData",
                    "String(theData)"
                },
                new Object[] {
                    theKey,
                    theEncodingId,
                    theData,
                    new String( theData )
                });
    }

    /**
     * The key.
     */
    protected final String theKey;

    /**
     * The id of the encoding.
     */
    protected final String theEncodingId;

    /**
     * The data, as a sequence of bytes.
     */
    protected final byte [] theData;
}
