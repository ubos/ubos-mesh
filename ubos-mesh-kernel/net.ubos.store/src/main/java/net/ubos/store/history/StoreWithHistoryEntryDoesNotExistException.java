//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

/**
 * Thrown to indicate that a key-timeUpdated entry does not exist already in the {@link StoreWithHistory},
 * for an operation that requires that the key-timeUpdated entry exists already.
 */
public class StoreWithHistoryEntryDoesNotExistException
        extends
            StoreWithHistoryException
{
    /**
     * Constructor.
     *
     * @param store the Store in which the entry did not exist
     * @param key the key component that does not exist in the Store
     * @param timeUpdated the time updated component that does not exist in the Store
     */
    public StoreWithHistoryEntryDoesNotExistException(
            StoreWithHistory<?> store,
            String              key,
            long                timeUpdated )
    {
        super( store, key, "Key/timeUpdated does not exist: " + key + " at time " + timeUpdated, null );

        theTimeUpdated = timeUpdated;
    }

    /**
     * Constructor.
     *
     * @param store the Store in which the key did not exist
     * @param key the key that does not exist in the Store
     * @param timeUpdated the time updated that does not exist in the Store
     * @param cause the underlying cause
     */
    public StoreWithHistoryEntryDoesNotExistException(
            StoreWithHistory<?> store,
            String              key,
            long                timeUpdated,
            Throwable           cause )
    {
        super( store, key, "Key/timeUpdated does not exist: " + key + " at time " + timeUpdated, cause );

        theTimeUpdated = timeUpdated;
    }

    /**
     * Obtain the timeUpdated related to which the Exception occurred.
     *
     * @return the timeUpdated
     */
    public long getTimeUpdated()
    {
        return theTimeUpdated;
    }

    /**
     * The timeUpdated related to which this Exception occurred.
     */
    protected final long theTimeUpdated;
}
