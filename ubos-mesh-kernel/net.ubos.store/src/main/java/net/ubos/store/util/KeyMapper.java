//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import java.text.ParseException;

/**
 * Classes implementing this interface know how to map keys from and to String form.
 *
 * @param <K> the type of key
 */
public interface KeyMapper<K>
{
    /**
     * Map a key to a String value that can be used for the Store.
     *
     * @param key the key object
     * @return the corresponding String value that can be used for the Store
     */
    public abstract String keyToString(
            K key );

    /**
     * Map to a key a String value that can be used for the Store.
     *
     * @param stringKey key the key in String form
     * @return the corresponding key
     * @throws ParseException thrown if a parsing error occurred
     */
    public abstract K stringToKey(
            String stringKey )
        throws
            ParseException;
}
