//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

import net.ubos.util.Pair;
import net.ubos.util.listenerset.FlexibleListenerSet;

/**
 * Collections functionality common to StoreWithHistory implementations.
 *
 * @param <T> parameterization allows implementations that store more than just StoreValueWithTimeUpdated
 */
public abstract class AbstractStoreWithHistory<T extends StoreValueWithTimeUpdated>
    implements
        StoreWithHistory<T>
{
    /**
     * Throw an IllegalArgumentException if an invalid key was handed to the Store API.
     * This may be overridden by subclasses.
     *
     * @param key the candidate key
     * @throws IllegalArgumentException if the candidate key is invalid
     */
    protected void checkKey(
            String key )
        throws
            IllegalArgumentException
    {
        if( key == null ) {
            throw new IllegalArgumentException( "key must not be null" );
        }
    }

    /**
     * Throw an IllegalArgumentException if an invalid encoding was handed to the HistoryStore API.
     * This may be overridden by subclasses.
     *
     * @param encoding the candidate encoding
     * @throws IllegalArgumentException if the candidate encoding is invalid
     */
    protected void checkEncoding(
            String encoding )
        throws
            IllegalArgumentException
    {
        if( encoding == null ) {
            throw new IllegalArgumentException( "encoding must not be null" );
        }
    }

    /**
     * Throw an IllegalArgumentException if an invalid time stamp was handed to the HistoryStore API.
     * This may be overridden by subclasses.
     *
     * @param time the candidate time stamp
     * @throws IllegalArgumentException if the candidate time stamp is invalid
     */
    protected void checkTime(
            long time )
        throws
            IllegalArgumentException
    {
        if( time < 0 ) {
            throw new IllegalArgumentException( "time stamp must be positive" );
        }
    }

    /**
     * Throw an IllegalArgumentException if invalid data was handed to the HistoryStore API.
     * This may be overridden by subclasses.
     *
     * @param data the candidate data
     * @throws IllegalArgumentException if the candidate data is invalid
     */
    protected void checkData(
            byte [] data )
        throws
            IllegalArgumentException
    {
        if( data == null ) {
            throw new IllegalArgumentException( "data must not be null" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDirectHistoryStoreListener(
            StoreWithHistoryListener<T> newListener )
    {
        theHistoryStoreListeners.addDirect( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addSoftHistoryStoreListener(
           StoreWithHistoryListener<T> newListener )
    {
        theHistoryStoreListeners.addSoft( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addWeakHistoryStoreListener(
            StoreWithHistoryListener<T> newListener )
    {
        theHistoryStoreListeners.addWeak( newListener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeHistoryStoreListener(
            StoreWithHistoryListener<T> oldListener )
    {
        theHistoryStoreListeners.remove( oldListener );
    }

    /**
     * Fire a Store put event.
     *
     * @param value the value put into the Store
     * @param success true if the put operation was successful
     */
    protected void firePutPerformed(
            StoreValueWithTimeUpdated value,
            boolean                   success )
    {
        theHistoryStoreListeners.fireEvent( new Pair<>( value, success ), 0 );
    }

    /**
     * Fire a Store put event.
     *
     * @param value the value put into the Store
     * @param success true if the put operation was successful
     */
    protected void fireUpdatePerformed(
            StoreValueWithTimeUpdated value,
            boolean                   success )
    {
        theHistoryStoreListeners.fireEvent( new Pair<>( value, success ), 1 );
    }

    /**
     * Fire a Store get event.
     *
     * @param key the key that as attempted
     * @param timeUpdated the timeUpdated as attempted
     * @param value the value that was obtained, or null if none
     */
    protected void fireGetPerformed(
            String                    key,
            long                      timeUpdated,
            StoreValueWithTimeUpdated value )
    {
        theHistoryStoreListeners.fireEvent( new Pair<>( new Pair<>( key, timeUpdated ), value ), 2 );
    }

    /**
     * Fire a Store "delete all" event.
     *
     * @param key the key that as attempted
     */
    protected void fireDeleteAllPerformed(
            String key )
    {
        theHistoryStoreListeners.fireEvent( key, 3 );
    }

    /**
     * The HistoryStoreListeners.
     */
    private final FlexibleListenerSet<StoreWithHistoryListener<T>,Object,Integer> theHistoryStoreListeners
            = new FlexibleListenerSet<StoreWithHistoryListener<T>,Object,Integer>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    protected void fireEventToListener(
                            StoreWithHistoryListener<T> l,
                            Object                   e,
                            Integer                     p )
                    {
                        switch( p ) {
                            case 0:
                                l.putPerformed( AbstractStoreWithHistory.this, (T) ((Pair<?,?>)e).getName(), (boolean) ((Pair<?,?>)e).getValue() );
                                break;

                            case 1:
                                l.updatePerformed( AbstractStoreWithHistory.this, (T) ((Pair<?,?>)e).getName(), (boolean) ((Pair<?,?>)e).getValue() );
                                break;

                            case 2:
                                Pair<?,?> e2 = (Pair<?,?>) ((Pair<?,?>)e).getName();
                                l.getPerformed( AbstractStoreWithHistory.this, (String) e2.getName(), (long) e2.getValue(), (T) ((Pair<?,?>)e).getValue() );
                                break;

                            case 3:
                                l.deleteAllPerformed( AbstractStoreWithHistory.this, (String) e );
                        }
                    }
    };
}
