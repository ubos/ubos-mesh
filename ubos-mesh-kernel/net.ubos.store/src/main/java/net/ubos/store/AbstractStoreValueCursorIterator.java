//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store;

import java.util.List;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Provides listener infrastructure for StoreValueCursorIterators.
 * 
 * @param <T> corresponds to parameterization of the Store
 */
public abstract class AbstractStoreValueCursorIterator<T extends StoreValue>
    implements
        StoreValueCursorIterator<T>
{
    /**
     * Constructor.
     * 
     * @param store the Store to iterate over
     */
    protected AbstractStoreValueCursorIterator(
            AbstractStore<T> store )
    {
        theStore = store;
    }

    /**
     * Obtain the right subclass of Store.
     * 
     * @return the EncryptedStore
     */
    public abstract Store<T> getStore();

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasMoreElements()
    {
        return hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final T nextElement()
    {
        return next();
    }

    protected void firePeekNextPerformed(
            String  position,
            T       v,
            boolean success )
    {
        theStore.firePeekNextPerformed( position, v, success );
    }


    protected void firePeekPreviousPerformed(
            String  position,
            T       v,
            boolean success )
    {
        theStore.firePeekPreviousPerformed( position, v, success );
    }
    
    protected void firePeekNextPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStore.firePeekNextPerformed( position, n, ret );
    }

    protected void firePeekPreviousPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStore.firePeekPreviousPerformed( position, n, ret );
    }

    protected void fireHasNextPerformed(
            String  position,
            boolean ret )
    {
        theStore.fireHasNextPerformed( position, ret );
    }

    protected void fireHasPreviousPerformed(
            String  position,
            boolean ret )
    {
        theStore.fireHasPreviousPerformed( position, ret );
    }

    protected void fireHasNextPerformed(
            String  position,
            int     n,
            boolean ret )
    {
        theStore.fireHasNextPerformed( position, n, ret );
    }

    protected void fireHasPreviousPerformed(
            String  position,
            int     n,
            boolean ret )
    {
        theStore.fireHasPreviousPerformed( position, n, ret );
    }

    protected void fireNextPerformed(
            String  position,
            T       ret,
            boolean success )
    {
        theStore.fireNextPerformed( position, ret, success );
    }

    protected void firePreviousPerformed(
            String  position,
            T       ret,
            boolean success )
    {
        theStore.firePreviousPerformed( position, ret, success );
    }

    protected void fireNextPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStore.fireNextPerformed( position, n, ret );
    }

    protected void firePreviousPerformed(
            String  position,
            int     n,
            List<T> ret )
    {
        theStore.firePreviousPerformed( position, n, ret );
    }

    protected void fireMoveByPerformed(
            String  position,
            int     n,
            boolean success )
    {
        theStore.fireMoveByPerformed( position, n, success );
    }

    protected void fireMoveToBeforePerformed(
            String  position,
            T       newPos,
            int     ret,
            boolean success )
    {
        theStore.fireMoveToBeforePerformed( position, newPos, ret, success );        
    }

    protected void fireMoveToAfterPerformed(
            String  position,
            T       newPos,
            int     ret,
            boolean success )
    {
        theStore.fireMoveToAfterPerformed( position, newPos, ret, success );
    }

    protected void fireRemovePerformed(
            String  position,
            boolean success )
    {
        theStore.fireRemovePerformed( position, success );
    }
    
    protected void fireMoveToBeforePerformed(
            String  position,
            String  key,
            int     ret,
            boolean success )
    {
        theStore.fireMoveToBeforePerformed( position, key, ret, success );
    }

    protected void fireMoveToAfterPerformed(
            String  position,
            String  key,
            int     ret,
            boolean success )
    {
        theStore.fireMoveToAfterPerformed( position, key, ret, success );
    }

    protected void fireSetPositionToPerformed(
            String            position,
            CursorIterator<T> newPosition,
            boolean           success )
    {
        theStore.fireSetPositionToPerformed( position, newPosition, success );
    }

    protected void fireMoveToBeforeFirstPerformed(
            String position,
            int    ret )
    {
        theStore.fireMoveToBeforeFirstPerformed( position, ret );
    }

    protected void fireMoveToAfterLastPerformed(
            String position,
            int    ret )
    {
        theStore.fireMoveToAfterLastPerformed( position, ret );
    }

    /**
     * The Store we iterate over.
     */
    protected final AbstractStore<T> theStore;

}
