//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.util;

import java.io.IOException;
import java.lang.ref.Reference;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreValueWithTimeUpdatedHistory;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.StoreWithHistoryKeyDoesNotExistException;
import net.ubos.util.cursoriterator.BufferingCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.history.HasTimeUpdated;
import net.ubos.util.history.History;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.AbstractSwappingSmartMap;
import net.ubos.util.smartmap.SmartMap;

/**
 * This is a map of histories, backed by a StoreWithHistory. On an API level, it simply looks like
 * nesting: a SmartMap whose values are Histories. On an implementation level, we use the same StoreWithHistory
 * for all the histories, which means the first level only deals with keys, not values that are automatically
 * mapped in. So this cannot inherit from StoreBackedSwappingSmartMap, because there is no StoreValue for
 * an entire history.
 *
 * @param <K> the key that identifies the histories
 * @param <V> the type thing whose history is contained in the map
 * @param <T> the type of StoreValueWithTimeUpdated
 */
public abstract class StoreWithHistoryBackedSwappingSmartMapOfHistory<K,V extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
    extends
        AbstractSwappingSmartMap<K,History<V>>
    implements
        SmartMap<K,History<V>>
{
    private static final Log log = Log.getLogInstance( StoreWithHistoryBackedSwappingSmartMapOfHistory.class );

    /**
     * Builder object.
     *
     * @param <K> same parameterization as StoreWithHistoryBackedSwappingSmartMapOfHistory
     * @param <V> same parameterization as StoreWithHistoryBackedSwappingSmartMapOfHistory
     * @param <T> same parameterization as StoreWithHistoryBackedSwappingSmartMapOfHistory
     */
    public static class Builder<K,V extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
    {
        /**
         * Factory method for Builder.
         *
         * @param mapper translates to and from StoreValueWithTimeUpdated used by the underlying Store
         * @param store the underlying Store
         * @return the Builder
         * @param <K> same parameterization as StoreWithHistoryBackedSwappingSmartMapOfHistory
         * @param <V> same parameterization as StoreWithHistoryBackedSwappingSmartMapOfHistory
         * @param <T> same parameterization as StoreWithHistoryBackedSwappingSmartMapOfHistory
         */
        public static <K,V extends HasTimeUpdated,T extends StoreValueWithTimeUpdated> StoreWithHistoryBackedSwappingSmartMapOfHistory.Builder<K,V,T> create(
                KeyAndStoreValueWithTimeUpdatedMapper<K,V,T> mapper,
                StoreWithHistory<T>                          store )
        {
            return new StoreWithHistoryBackedSwappingSmartMapOfHistory.Builder<>( mapper, store );
        }

        /**
         * Constructor.
         *
         * @param mapper translates values inside the histories to the StoreValueWithTimeUpdate dused by the underlying Store
         * @param store the underlying Store
         */
        protected Builder(
                KeyAndStoreValueWithTimeUpdatedMapper<K,V,T> mapper,
                StoreWithHistory<T>                          store )
        {
            theMapper = mapper;
            theStore  = store;
        }

        /**
         * Instantiate SoftReferences, instead of WeakReferences (the default).
         *
         * @return the Builder itself
         */
        public Builder<K,V,T> useSoft()
        {
            theUseWeak = false;
            return this;
        }

        /**
         * Build the object.
         *
         * @return the built object
         */
        public StoreWithHistoryBackedSwappingSmartMapOfHistory<K,V,T> build()
        {
            if( theLocalHistoryCache == null ) {
                theLocalHistoryCache = new HashMap<>();
            }
            StoreWithHistoryBackedSwappingSmartMapOfHistory<K,V,T> ret;

            if( theUseWeak ) {
                ret = new Weak<>( theLocalHistoryCache, theMapper, theStore );
            } else {
                ret = new Soft<>( theLocalHistoryCache, theMapper, theStore );
            }
            return ret;
        }

        protected Map<K,Reference<History<V>>>                       theLocalHistoryCache = null;
        protected boolean                                            theUseWeak           = true;
        protected final KeyAndStoreValueWithTimeUpdatedMapper<K,V,T> theMapper;
        protected final StoreWithHistory<T>                          theStore;
    }

    /**
     * Constructor.
     *
     * @param localHistoryCache the Map to use as a local cache of histories
     * @param mapper translates values inside the histories to the StoreValueWithTimeUpdatedused by the underlying Store
     * @param store the underlying Store
     */
    protected StoreWithHistoryBackedSwappingSmartMapOfHistory(
            Map<K,Reference<History<V>>>                 localHistoryCache,
            KeyAndStoreValueWithTimeUpdatedMapper<K,V,T> mapper,
            StoreWithHistory<T>                          store )
    {
        super( localHistoryCache );

        theMapper = mapper;
        theStore  = store;
    }

    /**
     * Obtain the backing StoreWithHistory.
     *
     * @return the StoreWithHistory
     */
    public StoreWithHistory<T> getBackingStoreWithHistory()
    {
        return theStore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected History<V> loadValueFromStorage(
            K key )
    {
        History<V> ret = StoreWithHistoryBackedSwappingHistory.Builder.create(
                theMapper.keyToString( key ),
                theMapper,
                theStore ).build();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveValueToStorage(
            K          key,
            History<V> newValue )
    {
        // I think it saves itself
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void removeValueFromStorage(
            K key )
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<K> keyIterator()
    {
        BufferingCursorIterator<StoreValueWithTimeUpdatedHistory<T>> bufferingIter = BufferingCursorIterator.create(
                BUFFERING_ITERATOR_BUFSIZE,
                theStore.iterator() );

        return new MappingCursorIterator<>(
                bufferingIter,
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public K mapDelegateValueToHere(
                            StoreValueWithTimeUpdatedHistory<T> delegateValue )
                    {
                        K key;
                        try {
                            String keyString = delegateValue.getKey();
                            key              = theMapper.stringToKey( keyString );
                        } catch( ParseException ex ) {
                            log.error( ex );
                            key = null;
                        }
                        return key;
                    }

                    @Override
                    public StoreValueWithTimeUpdatedHistory<T> mapHereToDelegateValue(
                            K value )
                    {
                        StoreValueWithTimeUpdatedHistory<T> ret;
                        try {
                            String keyString = theMapper.keyToString( value );
                            ret              = theStore.history( keyString );

                        } catch( StoreWithHistoryKeyDoesNotExistException ex ) {
                            //probably fine
                            ret = null;
                        } catch( IOException ex ) {
                            log.error( ex );
                            ret = null;
                        }
                        return ret;
                    }
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<History<V>> valueIterator()
    {
        return new MappingCursorIterator<>(
                keyIterator(),
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public History<V> mapDelegateValueToHere(
                            K delegateValue )
                    {
                        return get( delegateValue );
                    }

                    @Override
                    public K mapHereToDelegateValue(
                            History<V> value )
                    {
                        @SuppressWarnings("unchecked")
                        StoreWithHistoryBackedSwappingHistory<V,T> realValue = (StoreWithHistoryBackedSwappingHistory<V,T>) value;
                        String stringKey = realValue.getKey();
                        K      key;
                        try {
                            key = theMapper.stringToKey( stringKey );

                        } catch( ParseException ex ) {
                            log.error( ex );
                            key = null;
                        }
                        return key;
                    }
                } );
    }

    @Override
    public int size()
    {
        try {
            return theStore.size();

        } catch( IOException ex ) {
            log.error( ex );
            return Integer.MAX_VALUE;
        }
    }

    /**
     * Knows how to map the values. Provided to the individual histories.
     */
    protected final KeyAndStoreValueWithTimeUpdatedMapper<K,V,T> theMapper;

    /**
     * The underlying Store.
     */
    protected final StoreWithHistory<T> theStore;

    /**
     * Implementation that instantiates SoftReferences.
     *
     * @param <K> the type of key (e.g. Identifier)
     * @param <V> the type of value
     * @param <T> parameterization allows implementations that store more than just StoreValue
     */
    public static class Soft<K,V extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
        extends
            StoreWithHistoryBackedSwappingSmartMapOfHistory<K,V,T>
    {
        protected Soft(
                Map<K,Reference<History<V>>>                 localHistoryCache,
                KeyAndStoreValueWithTimeUpdatedMapper<K,V,T> mapper,
                StoreWithHistory<T>                          store )
        {
            super( localHistoryCache, mapper, store );
        }

        @Override
        protected Reference<History<V>> createReference(
                K          key,
                History<V> value )
        {
            return new AbstractSwappingSmartMap.SoftEntryReference<>( key, value, theQueue );
        }
    }

    /**
     * Implementation that instantiates WeakReferences.
     *
     * @param <K> the type of key (e.g. Identifier)
     * @param <V> the type of value
     * @param <T> parameterization allows implementations that store more than just StoreValue
     */
    public static class Weak<K,V extends HasTimeUpdated,T extends StoreValueWithTimeUpdated>
        extends
            StoreWithHistoryBackedSwappingSmartMapOfHistory<K,V,T>
    {
        protected Weak(
                Map<K,Reference<History<V>>>                 localHistoryCache,
                KeyAndStoreValueWithTimeUpdatedMapper<K,V,T> mapper,
                StoreWithHistory<T>                          store )
        {
            super( localHistoryCache, mapper, store );
        }

        @Override
        protected Reference<History<V>> createReference(
                K          key,
                History<V> value )
        {
            return new AbstractSwappingSmartMap.WeakEntryReference<>( key, value, theQueue );
        }
    }
    
    /**
     * Size of the cache for the iterator. FIXME?
     */
    public static final int BUFFERING_ITERATOR_BUFSIZE = 50;
}
