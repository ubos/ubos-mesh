//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.ubos.store.StoreValue;

/**
 * Knows how to stream, and read a StoreValue to and from an encoded byte stream for storage.
 */
public interface StoreValueStreamer
{
    /**
     * Encode a StoreValue to a byte stream.
     *
     * @param value the StoreValue to encode
     * @param stream the OutputStream to which to write the byte [] representation
     * @throws IOException thrown if an I/O error occurred
     */
    public void writeStoreValue(
            StoreValue   value,
            OutputStream stream )
        throws
            IOException;

    /**
     * Encode a StoreValue to a byte stream.
     *
     * @param value the StoreValue to encode
     * @param file the File to which to write the byte [] representation
     * @throws IOException thrown if an I/O error occurred
     */
    default public void writeStoreValue(
            StoreValue   value,
            File         file )
        throws
            IOException
    {
        OutputStream stream = new FileOutputStream( file );
        writeStoreValue( value, stream );
        stream.close();
    }


    /**
     * Decode a StoreValue from a byte stream.
     *
     * @param in the byte [] representation, represented as stream
     * @return the decoded StoreValue
     * @throws IOException thrown if an I/O error occurred
     */
    public StoreValue readStoreValue(
            InputStream in )
        throws
            IOException;

    /**
     * Decode a StoreValue from the content of a File.
     *
     * @param file the File containing the byte [] representation
     * @return the decoded StoreValue
     * @throws IOException thrown if an I/O error occurred
     */
    default public StoreValue readStoreValue(
            File file )
        throws
            IOException
    {
        InputStream stream = new FileInputStream( file );
        StoreValue  ret    = readStoreValue( stream );
        stream.close();
        return ret;
    }
}
