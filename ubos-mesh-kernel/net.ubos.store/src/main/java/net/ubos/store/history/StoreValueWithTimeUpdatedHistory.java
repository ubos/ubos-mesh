//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history;

import net.ubos.util.history.History;

/**
 * A history of StoreValueWithTimeUpdateds.
 *
 * @param <E> the type of thing for which this is a history
 */
public interface StoreValueWithTimeUpdatedHistory<E extends StoreValueWithTimeUpdated>
    extends
        History<E>
{
    /**
     * Determine the key for of the StoreValueWithTimeUpdated for which this is the history.
     *
     * @return the key
     */
    public String getKey();
}
