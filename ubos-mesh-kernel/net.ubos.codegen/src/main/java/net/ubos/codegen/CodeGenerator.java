//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.codegen;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import org.diet4j.core.ModuleClassLoader;
import net.ubos.codegen.intfc.InterfaceGenerator;
import net.ubos.codegen.modelloader.ModelLoaderGenerator;
import net.ubos.codegen.moduleinit.ModuleInitGenerator;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.ModelLoadingException;
import net.ubos.util.ResourceHelper;
import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.exception.AbstractLocalizedRuntimeException;
import net.ubos.util.logging.Log;

/**
 * The UBOS Mesh code generator.
 */
public abstract class CodeGenerator
{
    private static final Log log = Log.getLogInstance( CodeGenerator.class ); // our own, private logger

    /**
     * Cannot be instantiated.
     */
    private CodeGenerator() {}

    /**
     * Main program for the code generator.
     *
     * @param args command-line arguments
     * @return desired exit code
     */
    public static int main(
            String [] args )
    {
        ArrayList<String> modelFiles = new ArrayList<>();
        File              outputDir  = null;
        boolean           isOutput   = false;
        boolean           cleanFirst = false;

        for( int i=0 ; i<args.length ; ++i ) {
            if( "--clean".equals( args[i] )) {
                cleanFirst = true;

            } else if( "--outdir".equals( args[i] )) {
                isOutput = true;

            } else if( isOutput ) {
                isOutput = false;
                if( outputDir != null ) {
                    return usageAndQuit();

                } else {
                    outputDir = new File( args[i] );
                }
            } else {
                modelFiles.add( args[i] );
            }
        }

        ClassLoader cl = CodeGenerator.class.getClassLoader();
        if( !( cl instanceof ModuleClassLoader ) || ((ModuleClassLoader)cl).getModuleRegistry() == null ) {
            return usageAndQuit();
        }

        if( outputDir == null ) {
            return usageAndQuit();
        }

        if( outputDir.exists() ) {
            if( !outputDir.isDirectory() ) {
                log.userError( "Not a directory: " + outputDir );
                return 1;
            }
            if( !outputDir.canWrite() ) {
                log.userError( "Cannot write directory: " + outputDir );
                return 1;
            }
            if( cleanFirst ) {
                // delete everything that's in it
                try {
                    Files.walk( outputDir.toPath() )
                            .sorted( Comparator.reverseOrder() )
                            .map( Path::toFile )
                            // .peek( (f) -> { System.out.println( "Deleting file " + f ); } )
                            .forEach( File::delete );

                } catch( IOException ex ) {
                    log.userError( "Cannot delete all files below: " + outputDir );
                    return 1;
                }
            }
        } else {
            outputDir.mkdirs();
        }

        TimeStampValue now       = TimeStampValue.now();
        String         nowString = now.toString();

        InterfaceGenerator   iGen    = new InterfaceGenerator(   outputDir, nowString );
        ModelLoaderGenerator loadGen = new ModelLoaderGenerator( outputDir, nowString );
        ModuleInitGenerator  initGen = new ModuleInitGenerator(  outputDir, nowString );

        try {
            Iterator<String> iter = modelFiles.iterator();
            while( iter.hasNext() ) {
                File modelFile = new File( iter.next());
                if( ! modelFile.canRead() ) {
                    log.userError( "Cannot read model file " + modelFile.getAbsolutePath() );
                    return 1;
                }
                SubjectArea [] sas = ModelBase.SINGLETON.loadModel(
                        modelFile.getAbsolutePath(),
                        new FileInputStream( modelFile ),
                        CodeGenerator.class.getClassLoader(),
                        now );

                for( SubjectArea sa : sas ) {
                    iGen.generate( sa );
                    loadGen.generate( sa );
                    initGen.generate( sa );
                }
            }
        } catch( ModelLoadingException ex ) {
            log.userError( ex.getLocalizedMessage() );
            return 1;

        } catch( AbstractLocalizedException ex ) {
            log.userError( ex.getLocalizedMessage() );
            return 1;

        } catch( AbstractLocalizedRuntimeException ex ) {
            log.userError( ex.getLocalizedMessage() );
            return 1;

        } catch( IOException ex ) {
            log.error( ex );
            return 1;

        } catch( RuntimeException ex ) {
            log.error( ex );
            return 1;
        }
        return 0;
    }

    /**
     * Print usage information.
     *
     * @return desired exit code
     */
    private static int usageAndQuit()
    {
        if( CodeGenerator.class.getClassLoader() instanceof ModuleClassLoader ) {
            System.err.println( "Mesh code generator usage: [--clean] <model.xml> ... --outdir <outputDir>" );

        } else {
            System.err.println( "Usage: The mesh code generator must be invoked from the diet4j Module Framework, root module: net.ubos.codegen" );
        }

        return 1;
    }
}
