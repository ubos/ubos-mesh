//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.codegen.moduleinit;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import net.ubos.codegen.AbstractGenerator;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.SubjectArea;

/**
 * This class knows how to generate Java source code class that can be used
 * as the diet4j ModuleInit class for a SubjectArea Module.
 */
public class ModuleInitGenerator
    extends
        AbstractGenerator
{
    /**
     * Constructor.
     *
     * @param outputDir the directory into which the code shall be generated
     * @param nowString the timestamp to insert into the generated files
     */
    public ModuleInitGenerator(
            File   outputDir,
            String nowString )
    {
        super( outputDir, nowString );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void generate(
            SubjectArea sa )
        throws
            IOException
    {
        // boilerplate
        try ( PrintWriter w = getCodePrintWriterFor( sa )) {

            generateBoilerplate( w, sa );

            String packageName = sa.getName().value();
            w.println( "package " + packageName + ";" );
            w.println();

            w.println( "import java.io.IOException;" );
            w.println( "import net.ubos.model.primitives.TimeStampValue;" );
            w.println( "import net.ubos.modelbase.MeshTypeNotFoundException;" );
            w.println( "import net.ubos.modelbase.ModelBase;" );
            w.println( "import net.ubos.modelbase.ModelLoadingException;" );
            w.println( "import org.diet4j.core.Module;" );
            w.println( "import org.diet4j.core.ModuleActivationException;" );
            w.println();
            w.println( "import " + packageName + ".SubjectAreaLoader;" );
            w.println();
            w.println();

            w.println( "/**" );
            w.println( " * Activates the module by loading the Subject Area." );
            w.println( " */" );
            w.println( "public class ModuleInit" );
            w.println( "{" );
            w.println( "    /**" );
            w.println( "     * Activate this Module." );
            w.println( "     *" );
            w.println( "     * @param thisModule the current Module" );
            w.println( "     * @return the Subject Area as context object" );
            w.println( "     * @throws ModuleActivationException thrown if the Module could not be activated" );
            w.println( "     */" );
            w.println( "    public static Object moduleActivate(" );
            w.println( "            Module thisModule )" );
            w.println( "        throws" );
            w.println( "            ModuleActivationException" );
            w.println( "    {" );
            w.println( "        try {" );
            w.println( "            SubjectAreaLoader saLoader = new SubjectAreaLoader( ModelBase.SINGLETON, thisModule.getClassLoader() );" );
            w.println();
            w.println( "            return saLoader.loadAndCheckModel( ModelBase.SINGLETON.getMeshTypeLifecycleManager(), TimeStampValue.now());" );
            w.println();
            w.println( "        } catch( ModelLoadingException | IOException ex ) {" );
            w.println( "            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );" );
            w.println( "       }" );
            w.println( "    }" );
            w.println( "}" );
        }
    }

    /**
     * Obtain a PrintWriter to which the code will be written.
     *
     * @param sa the MeshType for whose generated code we want to obtain a PrintWriter
     * @return the PrintWriter for the generated code
     * @throws IOException thrown if an I/O error occurred during code generation
     */
    protected final PrintWriter getCodePrintWriterFor(
            MeshType sa )
        throws
            IOException
    {
        String packageName = sa.getName().value();
        String dirName     = packageName.replace( '.', File.separatorChar );

        StringBuilder buf = new StringBuilder( 256 );
        buf.append( theOutputDir );
        buf.append( File.separator );
        buf.append( dirName );
        buf.append( File.separator );
        buf.append( "ModuleInit.java" );

        File f = new File( buf.toString() );

        if( ! f.getParentFile().exists() ) {
            f.getParentFile().mkdirs();
        }

        return new PrintWriter( f, StandardCharsets.UTF_8 );
    }
}
