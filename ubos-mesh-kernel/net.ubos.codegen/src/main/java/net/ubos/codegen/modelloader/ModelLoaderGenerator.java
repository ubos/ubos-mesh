//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.codegen.modelloader;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import net.ubos.codegen.AbstractGenerator;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyTypeGroup;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;

/**
 * This class knows how to generate Java source code classes that implement
 * ModelLoader for one or more Subject Areas.
 */
public class ModelLoaderGenerator
    extends
        AbstractGenerator
{
    /**
     * Constructor.
     *
     * @param outputDir the directory into which the code shall be generated
     * @param nowString the timestamp to insert into the generated files
     */
    public ModelLoaderGenerator(
            File   outputDir,
            String nowString )
    {
        super( outputDir, nowString );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void generate(
            SubjectArea sa )
        throws
            IOException
    {
        // boilerplate
        try (PrintWriter w = getCodePrintWriterFor( sa )) {
            // boilerplate
            generateBoilerplate( w, sa );

            // package
            String packageName = sa.getName().value();
            w.println( "package " + packageName + ";" );
            w.println();

            // import
            w.println( "import java.io.IOException;" );
            w.println( "import java.text.ParseException;" );
            w.println( "import net.ubos.model.primitives.*;" );
            w.println( "import net.ubos.model.traverse.TraverseSpecification;" );
            w.println( "import net.ubos.model.traverse.TraverseToPropertySpecification;" );
            w.println( "import net.ubos.modelbase.*;" );
            w.println();

            // class and inheritance
            w.println( "/**" );
            w.println( " * This class loads the model of the " + sa.getUserVisibleName().value() + " Subject Area." );
            w.println( " */" );
            w.println( "public class SubjectAreaLoader" );
            w.println( "        extends" );
            w.println( "            ModelLoader" );
            w.println( "{" );

            // constructor
            w.println( "    /**" );
            w.println( "     * Constructor." );
            w.println( "     *" );
            w.println( "     * @param modelBase the ModelBase into which the SubjectArea will be loaded" );
            w.println( "     * @param loader the ClassLoader to be used for resolving resources" );
            w.println( "     */" );
            w.println( "    public SubjectAreaLoader(" );
            w.println( "            ModelBase   modelBase," );
            w.println( "            ClassLoader loader )" );
            w.println( "    {" );
            w.println( "        super( modelBase );" );
            w.println( "        " + LOADER_VAR + " = loader;" );
            w.println( "    }" );
            w.println();

            // load method
            generateLoadAll( w, sa );

            // member variables
            w.println( "    /**" );
            w.println( "     * The ClassLoader to be used for resources." );
            w.println( "     */" );
            w.println( "    protected ClassLoader " + LOADER_VAR + ";" );
            w.println();

            // finish up
            w.println( "}" );
        }
    }

    /**
     * Generate the load method.
     *
     * @param w the PrintWriter to write to
     * @param theSa the SubjectArea that we generate
     * @throws IOException an input/output error occurred
     */
    protected void generateLoadAll(
            PrintWriter w,
            SubjectArea theSa )
        throws
            IOException
    {
        w.println( "    /**" );
        w.println( "     * {@inheritdoc}" );
        w.println( "     */" );
        w.println( "    @Override" );
        w.println( "    protected SubjectArea [] loadModel(" );
        w.println( "            MeshTypeLifecycleManager theInstantiator," );
        w.println( "            TimeStampValue           now )" );
        w.println( "        throws" );
        w.println( "            MeshTypeNotFoundException," );
        w.println( "            ModelLoadingException," );
        w.println( "            ParseException," );
        w.println( "            IOException" );
        w.println( "    {" );
        w.println( "        net.ubos.util.logging.Log log = net.ubos.util.logging.Log.getLogInstance( getClass() );" );
        w.println( "        if( log.isTraceEnabled() ) {" );
        w.println( "            log.traceMethodCallEntry( this, \"loadModel\" );" );
        w.println( "        }" );
        w.println();
        w.println( "        MeshTypeIdentifierDeserializer typeIdSer = net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;" );
        w.println();

        generateLoadSubjectArea( w, theSa );

        CollectableMeshType [] cmos = theSa.getCollectableMeshTypes();

        // first the EntityTypes

        w.println( "        // generateLoadOneEntity section" );
        w.println();

        for( int i=0 ; i<cmos.length ; ++i ) {
            if( cmos[i] instanceof EntityType ) {
                generateLoadOneEntityType( w, "obj" + i, (EntityType) cmos[i] );
            }
        }

        // then the RelationshipTypes

        w.println( "        // generateLoadOneRelationshipType section" );
        w.println();

        for( int i=0 ; i<cmos.length ; ++i ) {
            if( cmos[i] instanceof RelationshipType ) {
                generateLoadOneRelationshipType( w, "obj" + i, (RelationshipType) cmos[i] );
            }
        }

        w.println( "        return new SubjectArea[] { theSa };" );
        w.println( "    }" );
        w.println();
    }

    /**
     * Generate the code for loading the SubjectArea itself.
     *
     * @param w the PrintWriter to write to
     * @param sa the SubjectArea that we generate
     * @throws IOException an input/output error occurred
     */
    protected void generateLoadSubjectArea(
            PrintWriter w,
            SubjectArea sa )
        throws
            IOException
    {
        w.println( "        SubjectArea theSa = theInstantiator.createSubjectArea(" );
        w.println( "                " + getIdentifierString( sa.getIdentifier()                ) + ",   // IdentifierValue           theIdentifier," );
        w.println( "                " + getValueString(      sa.getName()                      ) + ",   // StringValue               theName," );
        w.println( "                " + getValueString(      sa.getUserVisibleNameMap()        ) + ",   // L10Map                    theUserNames," );
        w.println( "                " + getValueString(      sa.getUserVisibleDescriptionMap() ) + ",   // L10Map                    theUserDescriptions," );
        w.println( "                " + getTypeString(       sa.getSubjectAreaDependencies()   ) + ",   // SubjectArea []            theSubjectAreaDependencies," );
        w.println( "                " + LOADER_VAR                                               + " ); // ClassLoader               theClassLoader );" );
        w.println();
    }

    /**
     * Generate the code for loading one EntityType.
     *
     * @param w the PrintWriter to write to
     * @param varName the name of the variable to which we assign this to
     * @param entity the EntityType that we generate
     * @throws IOException an input/output error occurred
     */
    protected void generateLoadOneEntityType(
            PrintWriter w,
            String      varName,
            EntityType  entity )
        throws
            IOException
    {
        w.println( "        EntityType " + varName + " = theInstantiator.createEntityType(" );
        w.println( "                " + getIdentifierString(  entity.getIdentifier()                 ) + ","   ); // IdentifierValue       theIdentifier,
        w.println( "                " + getValueString(       entity.getName()                       ) + ","   ); // StringValue           theName,
        w.println( "                " + getValueString(       entity.getUserVisibleNameMap()         ) + ","   ); // L10Map                theUserNames,
        w.println( "                " + getValueString(       entity.getUserVisibleDescriptionMap()  ) + ","   ); // L10Map                theUserDescriptions,
        w.println( "                " + getValueString(       entity.getIcon(), BlobDataType.theJdkSupportedBitmapType.getJavaConstructorString( LOADER_VAR )) + ","   ); // BlobValue theIcon,
        w.println( "                theSa," );                                                                    // SubjectArea           theSubjectArea,
        w.println( "                " + getTypeString(        entity.getDirectSupertypes()           ) + ","   ); // EntityType []         supertypes,
        w.println( "                " + getIdentifierStrings( entity.getSynonyms()                   ) + ","   ); // MeshTypeIdentifier [] supertypes,
        w.println( "                " + getValueString(       entity.getIsAbstract()                 ) + ","   ); // BooleanValue          isAbstract,
        w.println( "                " + getValueString(       entity.getMayBeUsedAsForwardReference()) + " );" ); // BooleanValue          mayBeUsedAsForwardReference,
        w.println();

        PropertyType [] mas = entity.getLocalPropertyTypes();
        for( int i=0 ; i<mas.length ; ++i ) {
            generateLoadOnePropertyType( w, varName, varName + "_pt" + i, mas[i] );
        }
        PropertyType [] omas = entity.getOverridingLocalPropertyTypes();
        for( int i=0 ; i<omas.length ; ++i ) {
            generateLoadOnePropertyType( w, varName, varName + "_opt" + i, omas[i] );
        }
        PropertyTypeGroup [] mags = entity.getLocalPropertyTypeGroups();
        for( int i=0 ; i<mags.length ; ++i ) {
            generateLoadOnePropertyTypeGroup( w, varName, varName + "_ptg" + i, mags[i] );
        }
    }

    /**
     * Generate the code for loading one RelationshipType.
     *
     * @param w the PrintWriter to write to
     * @param varName the name of the variable to which we assign this to
     * @param relationship the RelationshipType that we generate
     * @throws IOException an input/output error occurred
     */
    protected void generateLoadOneRelationshipType(
            PrintWriter      w,
            String           varName,
            RelationshipType relationship )
        throws
            IOException
    {
        w.println( "        RelationshipType " + varName + " = theInstantiator.createRelationshipType(" );
        w.println( "                " + getIdentifierString( relationship.getIdentifier()                  ) + "," ); // IdentifierValue   theIdentifier,
        w.println( "                " + getValueString( relationship.getName()                             ) + "," ); // StringValue       theName,
        w.println( "                " + getValueString( relationship.getUserVisibleNameMap()               ) + "," ); // L10Map            theUserNames,
        w.println( "                " + getValueString( relationship.getUserVisibleDescriptionMap()        ) + "," ); // L10Map            theUserDescriptions,
        w.println( "                theSa," );                                                                        // SubjectArea       theSubjectArea,

        if( relationship.getSource() == relationship.getDestination() ) {
            // undirected
            w.println( "                " + getValueString( relationship.getSource().getMultiplicity()         ) + "," ); // MultiplicityValue sourceMultiplicity,
            if( relationship.getSource().getEntityType() != null ) {
                w.println( "                theModelBase.findEntityType( " + getIdentifierString( relationship.getSource().getEntityType().getIdentifier() ) + " )," ); // EntityType                source,
            } else {
                w.println( "                null," );
            }
            if( relationship.getSource().getDirectSupertypes() != null ) {
                w.println( "                " + getTypeString(  relationship.getSource().getDirectSupertypes() ) + "," ); // RoleType []       sourceSuperRoleTypes,
            } else {
                w.println( "                null," );
            }
            w.println( "                " + getValueString( relationship.getIsAbstract()                       ) + " );" ); // BooleanValue    isAbstract,
            w.println();

            PropertyType [] srcMas = relationship.getSource().getLocalPropertyTypes();
            for( int i=0 ; i<srcMas.length ; ++i ) {
                generateLoadOnePropertyType( w, varName + ".getSource()", varName + "_src_pt" + i, srcMas[i] );
            }

            PropertyType [] srcOmas = relationship.getSource().getOverridingLocalPropertyTypes();
            for( int i=0 ; i<srcOmas.length ; ++i ) {
                generateLoadOnePropertyType( w, varName + ".getSource()", varName + "_src_opt" + i, srcOmas[i] );
            }
            PropertyTypeGroup [] srcMags = relationship.getSource().getLocalPropertyTypeGroups();
            for( int i=0 ; i<srcMags.length ; ++i ) {
                generateLoadOnePropertyTypeGroup( w, varName + ".getSource()", varName + "_src_ptg" + i, srcMags[i] );
            }

        } else {
            // directed
            w.println( "                " + getValueString( relationship.getSource().getMultiplicity()         ) + "," ); // MultiplicityValue sourceMultiplicity,
            w.println( "                " + getValueString( relationship.getDestination().getMultiplicity()    ) + "," ); // MultiplicityValue destinationMultiplicity,
            if( relationship.getSource().getEntityType() != null ) {
                w.println( "                theModelBase.findEntityType( " + getIdentifierString( relationship.getSource().getEntityType().getIdentifier() ) + " )," ); // EntityType                source,
            } else {
                w.println( "                null," );
            }
            if( relationship.getDestination().getEntityType() != null ) {
                w.println( "                theModelBase.findEntityType( " + getIdentifierString( relationship.getDestination().getEntityType().getIdentifier() ) + " )," ); // EntityType                destination,
            } else {
                w.println( "                null," );
            }
            if( relationship.getSource().getDirectSupertypes() != null ) {
                w.println( "                " + getTypeString(  relationship.getSource().getDirectSupertypes() ) + "," ); // RoleType []       sourceSuperRoleTypes,
            } else {
                w.println( "                null," );
            }
            if( relationship.getDestination().getDirectSupertypes() != null ) {
            w.println( "                " + getTypeString(  relationship.getDestination().getDirectSupertypes()) + "," ); // RoleType []       destinationSuperRoleTypes,
            } else {
                w.println( "                null," );
            }
            w.println( "                " + getValueString( relationship.getIsAbstract()                       ) + " );" ); // BooleanValue    isAbstract,
            w.println();

            PropertyType [] srcMas = relationship.getSource().getLocalPropertyTypes();
            for( int i=0 ; i<srcMas.length ; ++i ) {
                generateLoadOnePropertyType( w, varName + ".getSource()", varName + "_src_pt" + i, srcMas[i] );
            }

            PropertyType [] srcOmas = relationship.getSource().getOverridingLocalPropertyTypes();
            for( int i=0 ; i<srcOmas.length ; ++i ) {
                generateLoadOnePropertyType( w, varName + ".getSource()", varName + "_src_opt" + i, srcOmas[i] );
            }
            PropertyTypeGroup [] srcMags = relationship.getSource().getLocalPropertyTypeGroups();
            for( int i=0 ; i<srcMags.length ; ++i ) {
                generateLoadOnePropertyTypeGroup( w, varName + ".getSource()", varName + "_src_ptg" + i, srcMags[i] );
            }

            PropertyType [] destMas = relationship.getDestination().getLocalPropertyTypes();
            for( int i=0 ; i<destMas.length ; ++i ) {
                generateLoadOnePropertyType( w, varName + ".getDestination()", varName + "_dest_pt" + i, destMas[i] );
            }

            PropertyType [] destOmas = relationship.getDestination().getOverridingLocalPropertyTypes();
            for( int i=0 ; i<destOmas.length ; ++i ) {
                generateLoadOnePropertyType( w, varName + ".getDestination()", varName + "_dest_opt" + i, destOmas[i] );
            }
            PropertyTypeGroup [] destMags = relationship.getDestination().getLocalPropertyTypeGroups();
            for( int i=0 ; i<destMags.length ; ++i ) {
                generateLoadOnePropertyTypeGroup( w, varName + ".getDestination()", varName + "_dest_ptg" + i, destMags[i] );
            }
        }
    }

    /**
     * Generate the code for loading one PropertyType.
     *
     * @param w the PrintWriter to write to
     * @param parentVarName the variable name of the parent MeshTypeWithProperties
     * @param varName the name of the variable to which we assign this to
     * @param propertyType the PropertyType that we generate
     * @throws IOException an input/output error occurred
     */
    protected void generateLoadOnePropertyType(
            PrintWriter  w,
            String       parentVarName,
            String       varName,
            PropertyType propertyType )
        throws
            IOException
    {
        // first deal with the DataType because they may need a separate declaration

        String typeVarName = varName + "_type";
        w.println( "        " + propertyType.getDataType().getClass().getName() + " " + typeVarName + " = " + propertyType.getDataType().getJavaConstructorString( LOADER_VAR ) + ";" );

        boolean isOverriding = propertyType.getOverride() != null && propertyType.getOverride().length > 0;

        if( isOverriding ) {
            w.println( "        PropertyType " + varName + " = theInstantiator.createOverridingPropertyType(" );
            w.println( "                " + getTypeString( propertyType.getOverride() ) + "," );
        } else {
            w.println( "        PropertyType " + varName + " = theInstantiator.createPropertyType(" );
        }

        w.println( "                " + getIdentifierString( propertyType.getIdentifier()           ) + "," ); // IdentifierValue        theIdentifier,
        if( !isOverriding ) {
            w.println( "                " + getValueString( propertyType.getName()                  ) + "," ); // StringValue            theName,
            w.println( "                " + getValueString( propertyType.getUserVisibleNameMap()    ) + "," ); // L10Map                 theUserNames,
        }
        w.println( "                " + getValueString( propertyType.getUserVisibleDescriptionMap() ) + "," ); // L10Map                 theUserDescriptions,
        w.println( "                " + parentVarName + "," );                                                 // MeshTypeWithProperties theParent,
        w.println( "                theSa," );                                                                 // SubjectArea            theSubjectArea,
        w.println( "                " + typeVarName + "," );                                                   // DataType               theDataType,
        w.println( "                " + getValueString( propertyType.getDefaultValue(), typeVarName ) + "," ); // PropertyValue          theDefaultValue,
        w.println( "                " + getValueString( propertyType.getIsOptional()                ) + "," ); // BooleanValue           isOptional,
        w.print(   "                " + getValueString( propertyType.getIsReadOnly()                ));        // BooleanValue           isReadOnly,
        if( !isOverriding ) {
            w.println( "," );
            w.print( "                " + getValueString( propertyType.getSequenceNumber()          ));        // FloatValue                theSequenceNumber );
        }
        w.println( " );" );
        w.println();
    }

    /**
     * Generate the code for loading one PropertyTypeGroup.
     *
     * @param w the PrintWriter to write to
     * @param parentVarName the variable name of the parent MeshTypeWithProperties
     * @param varName the name of the variable to which we assign this to
     * @param group the PropertyTypeGroup that we generate
     * @throws IOException an input/output error occurred
     */
    protected void generateLoadOnePropertyTypeGroup(
            PrintWriter       w,
            String            parentVarName,
            String            varName,
            PropertyTypeGroup group )
        throws
            IOException
    {
        w.println( "        PropertyTypeGroup " + varName + " = theInstantiator.createPropertyTypeGroup(" );
        w.println( "                " + getIdentifierString( group.getIdentifier()              ) + "," );   // IdentifierValue        theIdentifier,
        w.println( "                " + getValueString( group.getName()                         ) + "," );   // StringValue            theName,
        w.println( "                " + getValueString( group.getUserVisibleNameMap()           ) + "," );   // L10Map                 theUserNames,
        w.println( "                " + getValueString( group.getUserVisibleDescriptionMap()    ) + "," );   // L10Map                 theUserDescriptions,
        w.println( "                " + getTypeString(  group.getMeshTypeWithProperties()       ) + "," );   // MeshTypeWithProperties theParent,
        w.println( "                theSa," );                                                               // SubjectArea            theSubjectArea,
        w.println( "                " + getTypeString(  group.getContainedPropertyTypes()       ) + "," );   // PropertyType []        theMembers,
        w.println( "                " + getValueString( group.getSequenceNumber()               ) + " );" ); // FloatValue             theSequenceNumber );
        w.println();
    }

    /**
     * Determine a Java language expression String containing an array of EntityTypes by
     * looking them up.
     *
     * @param types the EntityTypes
     * @return Java String
     */
    protected String getTypeString(
            EntityType [] types )
    {
        StringBuilder ret = new StringBuilder( "new EntityType[] { " );
        for( int i=0 ; i<types.length ; ++i ) {
            ret.append( "theModelBase.findEntityType( " ).append( getIdentifierString( types[i].getIdentifier() )).append( " )" );
            if( i < types.length-1 ) {
                ret.append( "," );
            }
            ret.append( " " );
        }
        ret.append( "}" );
        return ret.toString();
    }

    /**
     * Determine a Java language expression String containing one MeshTypeWithProperties by
     * looking it up.
     *
     * @param amo the MeshTypeWithProperties
     * @return Java String
     */
    protected String getTypeString(
            MeshTypeWithProperties amo )
    {
        StringBuilder ret = new StringBuilder( "theModelBase.findMeshTypeWithProperties( " );
        ret.append( getIdentifierString( amo.getIdentifier() ));
        ret.append( " )" );
        return ret.toString();
    }

    /**
     * Determine a Java language expression String containing an array of RoleTypes by
     * looking them up.
     *
     * @param roles the RoleTypes
     * @return Java String
     */
    protected String getTypeString(
            RoleType [] roles )
    {
        StringBuilder ret = new StringBuilder( "new RoleType[] { " );
        for( int i=0 ; i<roles.length ; ++i ) {
            ret.append( getTypeString( roles[i] ));

            if( i < roles.length-1 ) {
                ret.append( "," );
            }
            ret.append( " " );
        }
        ret.append( "}" );
        return ret.toString();
    }

    /**
     * Determine a Java language expression String containing a RoleType by
     * looking it up.
     *
     * @param role the RoleType
     * @return Java String
     */
    protected String getTypeString(
            RoleType role )
    {
        StringBuilder ret = new StringBuilder( "theModelBase.findRoleType( " );
        ret.append( getIdentifierString( role.getIdentifier() ));
        ret.append( " )" );
        return ret.toString();
    }

    /**
     * Determine a Java language expression String containing an array of PropertyTypes by
     * looking them up.
     *
     * @param mas the PropertyTypes
     * @return Java String
     */
    protected String getTypeString(
            PropertyType [] mas )
    {
        StringBuilder ret = new StringBuilder( "new PropertyType[] { " );
        for( int i=0 ; i<mas.length ; ++i ) {
            ret.append( "theModelBase.findPropertyType( " );
            ret.append( getIdentifierString( mas[i].getIdentifier() ));
            ret.append( " )" );
            if( i < mas.length-1 ) {
                ret.append( "," );
            }
            ret.append( " " );
        }
        ret.append( "}" );
        return ret.toString();
    }

    /**
     * Determine a Java language expression String containing an array of SubjectAreas by
     * looking them up.
     *
     * @param sas the SubjectAreas
     * @return Java String
     */
    protected String getTypeString(
            SubjectArea [] sas )
    {
        StringBuilder ret = new StringBuilder( "new SubjectArea[] { " );
        for( int i=0 ; i<sas.length ; ++i ) {
            ret.append( "theModelBase.findSubjectArea( " );
            ret.append( getIdentifierString( sas[i].getIdentifier() ));
            ret.append( " )" );
            if( i < sas.length-1 ) {
                ret.append( "," );
            }
            ret.append( " " );
        }
        ret.append( "}" );
        return ret.toString();
    }

    /**
     * Format one PropertyValue appropriately and return.
     *
     * @param val the PropertyValue, or null
     * @return String representation
     */
    protected String getValueString(
            PropertyValue val )
    {
        if( val == null ) {
            return "null";
        } else {
            return val.getJavaConstructorString( LOADER_VAR, null );
        }
    }

    /**
     * Format one PropertyValue appropriately and return.
     *
     * @param val the PropertyValue, or null
     * @param typeVar  name of the variable containing the DataType that goes with the to-be-created instance.
     * @return String representation
     */
    protected String getValueString(
            PropertyValue val,
            String        typeVar )
    {
        if( val == null ) {
            return "null";
        } else {
            return val.getJavaConstructorString( LOADER_VAR, typeVar );
        }
    }

    /**
     * Format one L10Map appropriately and return.
     *
     * @param val the L10Map, or null
     * @return String representation
     */
    protected String getValueString(
            L10PropertyValueMap val )
    {
        if( val == null ) {
            return "null";
        } else {
            return val.getJavaConstructorString( LOADER_VAR, BlobDataType.theTextAnyType.getJavaConstructorString( LOADER_VAR ) );
        }
    }

    /**
     * Format an array of StringValues appropriately and return.
     *
     * @param values the StringValues
     * @return String representation
     */
    protected String getValueString(
            StringValue [] values )
    {
        if( values == null ) {
            return "null";
        } else if( values.length == 0 ) {
            return "new StringValue[0]";
        } else {
            StringBuilder buf = new StringBuilder();
            String sep = "new StringValue[] { ";
            for( StringValue val : values ) {
                buf.append( sep );
                buf.append( val.getJavaConstructorString( null, null ));
                sep = ", ";
            }
            buf.append( " }" );
            return buf.toString();
        }
    }

    /**
     * Format an Identifier appropriately and return.
     *
     * @param value the Identifier
     * @return String representation
     */
    protected String getIdentifierString(
            MeshTypeIdentifier value )
    {
        if( value == null ) {
            return "null";
        } else {
            StringBuilder buf = new StringBuilder();
            buf.append( "typeIdSer.fromExternalForm( \"" );
            buf.append( theTypeIdSerializer.toExternalForm( value ));
            buf.append( "\" )" );
            return buf.toString();
        }
    }

    /**
     * Format an array of Identifier appropriately and return.
     *
     * @param value the Identifiers
     * @return String representation
     */
    protected String getIdentifierStrings(
            MeshTypeIdentifier [] value )
    {
        if( value == null ) {
            return "null";
        } else {
            StringBuilder buf = new StringBuilder();
            buf.append( "new MeshTypeIdentifier [] {\n" );

            String sep = "";
            for( MeshTypeIdentifier current : value ) {
                buf.append( sep );
                buf.append( "typeIdSer.fromExternalForm( \"" );
                buf.append( theTypeIdSerializer.toExternalForm( current ));
                buf.append( "\" )" );
                sep = ",";
            }
            buf.append( "}" );
            return buf.toString();
        }
    }

    /**
     * Obtain a PrintWriter to which the code will be written.
     *
     * @param sa the MeshType for whose generated code we want to obtain a PrintWriter
     * @return the PrintWriter for the generated code
     * @throws IOException thrown if an I/O error occurred during code generation
     */
    protected final PrintWriter getCodePrintWriterFor(
            SubjectArea sa )
        throws
            IOException
    {
        String packageName = sa.getName().value();
        String dirName     = packageName.replace( '.', File.separatorChar );

        StringBuilder buf = new StringBuilder( 256 );
        buf.append( theOutputDir );
        buf.append( File.separator );
        buf.append( dirName );
        buf.append( File.separator );
        buf.append( "SubjectAreaLoader.java" );

        File f = new File( buf.toString() );

        if( ! f.getParentFile().exists() ) {
            f.getParentFile().mkdirs();
        }

        return new PrintWriter( f, StandardCharsets.UTF_8 );
    }

    /**
     * Name of the generated member variable holding the ClassLoader.
     */
    private static final String LOADER_VAR = "theLoader";
}
