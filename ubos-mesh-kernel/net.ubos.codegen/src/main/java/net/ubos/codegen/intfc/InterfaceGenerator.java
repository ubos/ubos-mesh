//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.codegen.intfc;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import net.ubos.codegen.AbstractGenerator;
import net.ubos.model.primitives.CollectableMeshType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.util.logging.Log;
import net.ubos.util.text.StringifierException;

/**
 * This code generator generates the Java interfaces for a model.
 */
public class InterfaceGenerator
        extends
            AbstractGenerator
{
    private static final Log log = Log.getLogInstance( InterfaceGenerator.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param outputDir the directory into which the code shall be generated
     * @param nowString the timestamp to insert into the generated files
     */
    public InterfaceGenerator(
            File   outputDir,
            String nowString )
    {
        super( outputDir, nowString );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void generate(
            SubjectArea sa )
        throws
            IOException,
            StringifierException
    {
        generateCode( sa );
        generateDocs( sa );
    }

    /**
     * Does the code generation part.
     *
     * @param sa the SubjectArea to generate code for
     * @throws IOException thrown if an I/O error occurred during code generation
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     */
    protected void generateCode(
            SubjectArea sa )
        throws
            IOException,
            StringifierException
    {
        log.info( "Generating Interface for SubjectArea " + sa.getName() );

        try (PrintWriter w = getCodePrintWriterFor( sa )) {
            generateBoilerplate( w, sa );

            String packageName = sa.getName().value();
            String saShortName = packageName.substring( packageName.lastIndexOf( '.' )+1 );

            saShortName = Character.toUpperCase( saShortName.charAt( 0 )) + saShortName.substring( 1 );
            w.println( "package " + packageName + ";" );
            w.println();

            w.println( "import net.ubos.model.primitives.*;" );
            w.println( "import net.ubos.modelbase.ModelBase;" );
            w.println();

            // JavaDoc comment
            w.println( "/**" );
            w.println( " * <p>Collects the MeshTypes in this Subject Area for simpler access.</p>" );
            w.println( " */" );

            w.println( "public abstract class " + saShortName + "SubjectArea" );
            w.println( "{" );

            w.println( "    /**" );
            w.println( "     * The SubjectArea itself." );
            w.println( "     */" );
            w.println( "    public static final SubjectArea _SA = ModelBase.SINGLETON.findSubjectAreaOrNull( \"" + packageName + "\" );" );
            w.println();

            for( CollectableMeshType type : sa.getCollectableMeshTypes() ) {

                if( type instanceof MeshTypeWithProperties ) {
                    MeshTypeWithProperties realType = (MeshTypeWithProperties) type;

                    String name      = realType.getName().value();
                    String upperName = name.toUpperCase();

                    if( realType instanceof EntityType ) {
                        w.println( "    /**" );
                        if( realType.getUserVisibleDescription() != null ) {
                            w.println( "     * " + formatForComment( realType.getUserVisibleDescription() ));
                        } else {
                            w.println( "     * An undocumented EntityType." );
                        }
                        w.println( "     */" );
                        w.print(   "    public static final EntityType " );
                        w.print( upperName );
                        w.print(  " = ModelBase.SINGLETON.findEntityTypeOrNull( \"" );
                        w.print( theTypeIdSerializer.toExternalForm( realType.getIdentifier()));
                        w.println( "\" );" );
                        w.println();

                    } else if( realType instanceof RoleType ) {
                        w.println( "    /**" );
                        if( realType.getUserVisibleDescription() != null ) {
                            w.println( "     * " + formatForComment( realType.getUserVisibleDescription() ));
                        } else {
                            w.println( "     * An undocumented RoleType." );
                        }
                        w.println( "     */" );
                        w.print( "    public static final RoleType " );
                        w.print( upperName );
                        w.print( " = ModelBase.SINGLETON.findRoleTypeOrNull( \"" );
                        w.print( theTypeIdSerializer.toExternalForm( realType.getIdentifier()));
                        w.println( "\" );" );
                        w.println();

                    } else {
                        log.error( "Unexpected type:", realType );
                        continue;
                    }

                    for( PropertyType propType : realType.getLocalPropertyTypes() ) {
                        String propName          = propType.getName().value();
                        String upperPropName     = propName.toUpperCase();
                        String upperPropFullName = upperName + "_" + upperPropName;

                        w.println( "    /**" );
                        if( propType.getUserVisibleDescription() != null ) {
                            w.println( "     * " + formatForComment( propType.getUserVisibleDescription() ));
                        } else {
                            w.println( "     * An undocumented PropertyType." );
                        }
                        w.println( "     */" );
                        w.print( "    public static final PropertyType " );
                        w.print( upperPropFullName );
                        w.print( " = ModelBase.SINGLETON.findPropertyTypeOrNull( \"" );
                        w.print( theTypeIdSerializer.toExternalForm( propType.getIdentifier()));
                        w.println( "\" );" );
                        w.println();

                        String dataTypeName = propType.getDataType().getClass().getName();
                        dataTypeName = dataTypeName.substring( dataTypeName.lastIndexOf( '.') + 1 );

                        w.println( "    /**" );
                        w.println( "     * DataType for PropertyType " + upperPropFullName + "." );
                        w.println( "     */" );
                        w.println( "    public static final " + dataTypeName + " " + upperPropFullName + "_type = (" + propType.getDataType().getClass().getName() + ") " + upperName + "_" + upperPropName + ".getDataType();" );
                        w.println();

                        if( propType.getDataType() instanceof EnumeratedDataType ) {
                            EnumeratedDataType realDataType = (EnumeratedDataType) propType.getDataType();

                            for( EnumeratedValue current : realDataType.getDomain() ) {
                                w.println( "    /**" );
                                w.println( "     * Member of the enumeration for PropertyType " + upperPropFullName + "." );
                                w.println( "     */" );
                                w.println( "    public static final EnumeratedValue " + upperPropFullName + "_type_" + current.value().toUpperCase() + " = " + upperName + "_" + upperPropName + "_type.select( \"" + current.value() + "\" );" );
                                w.println();
                            }
                        }
                    }

                } else if( type instanceof RelationshipType ) {
                    RelationshipType realType = (RelationshipType) type;

                    String name      = realType.getName().value();
                    String upperName = name.toUpperCase();

                    w.println( "    /**" );
                    if( realType.getUserVisibleDescription() != null ) {
                        w.println( "     * " + formatForComment( realType.getUserVisibleDescription() ));
                    } else {
                        w.println( "     * An undocumented RelationshipType." );
                    }
                    w.println( "     */" );
                    w.print( "    public static final RelationshipType " );
                    w.print( upperName );
                    w.print( " = ModelBase.SINGLETON.findRelationshipTypeOrNull( \"" );
                    w.print( theTypeIdSerializer.toExternalForm( realType.getIdentifier()));
                    w.println( "\" );" );
                    w.println();

                } // else ignore
            }

            w.println( "}" );
        }
    }

    /**
     * Does the documentation generation part.
     *
     * @param sa the SubjectArea to generate documentation for
     * @throws IOException thrown if an I/O error occurred during code generation
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     */
    protected void generateDocs(
            SubjectArea sa )
        throws
            IOException,
            StringifierException
    {
        log.info( "Generating JavaDoc for SubjectArea " + sa.getName() );

        try (PrintWriter w = this.getJavaDocPrintWriterFor( sa )) {
            w.println( "<html>" );
            w.println( " <head>" );
            w.println( "  <title>Documentation for " + theTypeIdSerializer.toExternalForm( sa.getIdentifier()) + "</title>" );
            w.println( " </head>" );
            w.println( " <body>" );

            w.println( formatForComment( sa.getUserVisibleDescription() ));

            w.println( " </body>" );
            w.println( "</html>" );
        }
    }

    /**
     * Generate the JavaDoc for a PropertyType.
     *
     * @param pt the PropertyType for which we generate documentation
     * @param w the PrintWriter to write to
     * @throws StringifierException thrown if there was a problem when attempting to stringify
     */
    protected void generatePropertyTypeJavaDoc(
            PropertyType pt,
            PrintWriter  w )
        throws
            StringifierException
    {
        w.println( "      * <table>" );
        w.println( "      *  <tr><td>Identifier:</td><td><tt>"
                + theTypeIdSerializer.toExternalForm( pt.getIdentifier())
                + "</tt></td></tr>" );
        w.println( "      *  <tr><td>Name:</td><td><tt>"
                + formatForComment( pt.getName() )
                + "</tt></td></tr>" );
        w.println( "      *  <tr><td>DataType:</td><td><tt>"
                + pt.getDataType().getName()
                + "</tt></td></tr>" );
        w.println( "      *  <tr><td>IsOptional:</td><td><tt>"
                + formatForComment( pt.getIsOptional() )
                + "</tt></td></tr>" );
        w.println( "      *  <tr><td>IsReadOnly:</td><td><tt>"
                + formatForComment( pt.getIsReadOnly() )
                + "</tt></td></tr>" );
        w.println( "      *  <tr><td>SequenceNumber:</td><td><tt>"
                + formatForComment( pt.getSequenceNumber() )
                + "</tt></td></tr>" );
        generateL10Map(
                pt.getUserVisibleNameMap(),
                "      *  <tr><td>UserVisibleName:</td><td>",
                "</td></tr>",
                w );
        generateL10Map(
                pt.getUserVisibleDescriptionMap(),
                "      *  <tr><td>UserVisibleDescription:</td><td>",
                "</td></tr>",
                w );
        w.println( "      * </table>" );
    }

    /**
     * A helper that can generate the content of an L10Map in HTML format.
     *
     * @param map the L10Map containing the internationalized content
     * @param prefix prefix to prepend to the line
     * @param postfix postfix to append to the line
     * @param w the PrintWriter to write to
     */
    protected final void generateL10Map(
            L10PropertyValueMap map,
            String              prefix,
            String              postfix,
            PrintWriter         w )
    {
        if( map == null ) {
            return;
        }
        if( map.isEmpty() && map.getDefault() == null ) {
            return;
        }

        w.print( prefix );
        w.print( "<table><tr><td>default locale:</td><td>" );
        w.print( formatForComment( (StringValue) map.getDefault() ));
        w.print( "</td></tr>" );
        Iterator<String> theIter = map.keyIterator();
        while( theIter.hasNext() ) {
            String key = theIter.next();
            w.print( "<tr><td>" );
            w.print( key );
            w.print( "</td><td>" );
            w.print( formatForComment( (StringValue) map.getExact( key )));
            w.print( "</td></tr>" );
        }
        w.print( "</table>" );
        w.println( postfix );
    }

    /**
     * Obtain a PrintWriter to which the code will be written.
     *
     * @param sa the MeshType for whose generated code we want to obtain a PrintWriter
     * @return the PrintWriter for the generated code
     * @throws IOException thrown if an I/O error occurred during code generation
     */
    protected final PrintWriter getCodePrintWriterFor(
            SubjectArea sa )
        throws
            IOException
    {
        String packageName = sa.getName().value();
        String dirName     = packageName.replace( '.', File.separatorChar );

        String shortSaName = packageName.substring( packageName.lastIndexOf( '.' )+1 );
        shortSaName = Character.toTitleCase( shortSaName.charAt( 0 ) ) + shortSaName.substring( 1 ); // first char is uppercase

        StringBuilder buf = new StringBuilder( 256 );
        buf.append( theOutputDir );
        buf.append( File.separator );
        buf.append( dirName );
        buf.append( File.separator );
        buf.append( shortSaName );
        buf.append( "SubjectArea.java" );

        File f = new File( buf.toString() );

        if( ! f.getParentFile().exists() ) {
            f.getParentFile().mkdirs();
        }

        return new PrintWriter( f, StandardCharsets.UTF_8 );
    }

    /**
     * Obtain a PrintWriter to which the JavaDoc will be written.
     *
     * @param sa the MeshType for whose generated JavaDoc we want to obtain a PrintWriter
     * @return the PrintWriter for the generated JavaDoc
     * @throws IOException thrown if an I/O error occurred during code generation
     */
    protected final PrintWriter getJavaDocPrintWriterFor(
            SubjectArea sa )
        throws
            IOException
    {
        String packageName = sa.getName().value();
        String dirName     = packageName.replace( '.', File.separatorChar );

        StringBuilder buf = new StringBuilder( 256 );
        buf.append( theOutputDir );
        buf.append( File.separator );
        buf.append( dirName );
        buf.append( File.separator );
        buf.append( "package.html" );

        File f = new File( buf.toString());

        if( ! f.getParentFile().exists() ) {
            f.getParentFile().mkdirs();
        }

        return new PrintWriter( f, StandardCharsets.UTF_8 );
    }
}
