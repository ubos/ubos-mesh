//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import net.ubos.util.logging.Log;

/**
 * <p>This class wraps the execution of multiple SQL statements in one transaction,
 * and helps us with obtaining a new Connection if an old one fails without having
 * to expose this to the application programmer.
 * It is generally used by subclassing it with an anonymous class.</p>
 *
 * <p>This class is parameterized with the return type of the execution command.
 *    The return value is the value of the last SQL statement that was executed.</p>
 * 
 * @param <R> the type of return value of the execution
 */
public abstract class MultipleSqlExecutionAction<R>
{
    private static final Log log = Log.getLogInstance( MultipleSqlExecutionAction.class ); // our own, private logger

    /**
     * Constructor.
     *
     * @param stms the SqlStorePreparedStatements to execute, in sequence
     */
    protected MultipleSqlExecutionAction(
            SqlPreparedStatement ... stms )
    {
        theStatements = stms;
        
        if( stms.length == 0 ) {
            throw new IllegalArgumentException( "Must have at least one SqlPreparedStatement" );
        }
        SqlDatabase d = null;
        for( SqlPreparedStatement s : stms ) {
            if( d == null ) {
                d = s.getDatabase();
            } else if( d != s.getDatabase() ) {
                throw new IllegalArgumentException( "All SqlPreparedStatements in a SqlExecutionAction must refer to the same database" );
            }
        }
    }

    /**
     * Execute the action.
     *
     * @return the return value, if any
     * @throws SQLException a SQL exception occurred
     */
    public R execute()
        throws
            SQLException
    {
        R ret = execute( null );
        return ret;
    }
    
    /**
     * Execute the action. Use the provided argument as the value indicating
     * whether or not auto-commit should take place.
     *
     * @param autoCommit if given, and false, performs a commit or rollback. If
     *                   not given or true, do nothing after an operation
     * @return the return value, if any
     * @throws SQLException a SQL exception occurred
     */
    public R execute(
            Boolean autoCommit )
        throws
            SQLException
    {
        SqlDatabase   db   = theStatements[0].getDatabase();        
        Connection conn = db.obtainConnection();
         
        synchronized( conn ) {
            track();

            PreparedStatement [] stms  = new PreparedStatement[ theStatements.length ];
            boolean              error = true;
            try {
                for( int i=0 ; i<theStatements.length ; ++i ) {
                    stms[i] = theStatements[i].obtain( conn );
                }
                R ret = perform( stms, conn );
                error = false;
                
                return ret; // we are done

            } catch( SQLException ex ) {
                for( int i=0 ; i<theStatements.length ; ++i ) {
                    theStatements[i].close();
                }

            } finally {
                if( autoCommit == null ) {
                    autoCommit = db.getAutoCommit();
                }
                if( autoCommit != null && !autoCommit ) {
                    try {
                        if( error ) {
                            conn.rollback();
                        } else {
                            conn.commit();
                        }
                    } catch( SQLException ex ) {
                        // do nothing
                    }
                }
            }

            conn = db.obtainNewConnection();

            // Get a new connection and try again. This time pass on any Exceptions.
            error = true;

            try {
                for( int i=0 ; i<theStatements.length ; ++i ) {
                    stms[i] = theStatements[i].obtain( conn );
                }
                R ret = perform( stms, conn );
                error = false;

                return ret;

            } finally {
                // this funny construct is to not having to catch the Exception while
                // still closing the SqlStorePreparedStatement

                if( error ) {
                    for( int i=0 ; i<theStatements.length ; ++i ) {
                        theStatements[i].close();
                    }
                }
                if( autoCommit != null && !autoCommit ) {
                    try {
                        if( error ) {
                            conn.rollback();
                        } else {
                            conn.commit();
                        }
                    } catch( SQLException ex ) {
                        // do nothing
                    }
                }                
            }
        }
    }

    /**
     * Execute the actual SQL.
     * 
     * @param stms the PreparedStatements to execute
     * @param conn the Connection object to use
     * @return the return value
     * @throws SQLException thrown if the SQL could not be executed
     */
    protected abstract R perform(
            PreparedStatement [] stms,
            Connection           conn )
        throws
            SQLException;
    
    /**
     * Helper to track the execution of a SQL statement.
     */
    protected void track()
    {
        if( log.isInfoEnabled() ) {
            log.info( "Executing compound SQL: " + theStatements.length + " pieces, first is: " + theStatements[0].getSql() );
        }
    }
    
    /**
     * The SqlStorePreparedStatements we execute.
     */
    protected SqlPreparedStatement [] theStatements;
}
