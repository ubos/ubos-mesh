//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.sql;

import java.io.IOException;
import java.sql.SQLException;
import net.ubos.util.exception.LocalizedException;
import net.ubos.util.ResourceHelper;

/**
 * <p>An <code>IOException</code> that delegates to a <code>SQLException</code>.</p>
 */
public class SqlDatabaseException
        extends
            IOException
        implements
            LocalizedException
{
    /**
     * Constructor.
     *
     * @param db the SqlDatabase that threw the exception
     * @param operation name of the operation, such as "put"
     * @param cause the cause
     */
    public SqlDatabaseException(
            SqlDatabase  db,
            String       operation,
            SQLException cause )
    {
        super( "SQL Exception", cause );

        theDb        = db;
        theOperation = operation;
    }

    /**
     * Obtain the underlying cause which we know to be a SQLException.
     *
     * @return the cause
     */
    @Override
    public SQLException getCause()
    {
        return (SQLException) super.getCause();
    }

    /**
     * Obtain the SqlDatabase that threw the Exception.
     *
     * @return the SqlDatabase
     */
    public SqlDatabase getSqlDatabase()
    {
        return theDb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
                theDb.getName(),
                theOperation,
                getCause().getMessage(),
                getCause().getLocalizedMessage() };
    }

    /**
     * The SqlDatabase that threw this exception.
     */
    protected SqlDatabase theDb;

    /**
     * The name of the operation that produced this exception.
     */
    protected String theOperation;
}
