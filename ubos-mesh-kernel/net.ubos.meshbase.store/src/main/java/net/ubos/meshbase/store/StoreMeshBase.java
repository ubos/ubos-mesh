//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.a.AMeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObjectIdentifierFactory;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StoreContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.set.MeshObjectSetFactory;
import net.ubos.mesh.set.m.ImmutableMMeshObjectSetFactory;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.AbstractHeadMeshBaseView;
import net.ubos.meshbase.a.AMeshBase;
import net.ubos.meshbase.a.AMeshBaseLifecycleManager;
import net.ubos.meshbase.a.ATransaction;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.security.AccessManager;
import net.ubos.meshbase.security.PermitAllAccessManager;
import net.ubos.meshbase.store.history.StoreMeshBaseStateMeshObjectHistory;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.meshbase.transaction.AbstractMeshObjectAttributesChange;
import net.ubos.meshbase.transaction.AbstractMeshObjectLifecycleChange;
import net.ubos.meshbase.transaction.MeshObjectPropertyChange;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributeChange;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleAttributesChange;
import net.ubos.meshbase.transaction.MeshObjectRolePropertyChange;
import net.ubos.meshbase.transaction.AbstractMeshObjectRoleTypeChange;
import net.ubos.meshbase.transaction.AbstractMeshObjectTypeChange;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;
import net.ubos.util.logging.Log;

/**
  * A MeshBase that delegates persistence to an external Store. All of its intelligence
  * resides either in the factory method, or in the AMeshBase implementation.
  */
public class StoreMeshBase
        extends
            AMeshBase
{
    private static final Log log = Log.getLogInstance( StoreMeshBase.class ); // our own, private logger

    /**
     * Builder class, use to instantiate StoreMeshBase.
     */
    public static class Builder
    {
        /**
         * Create the Builder.
         *
         * @param meshObjectStore
         * @param localNsStore
         * @param primaryMap
         * @return the Builder.
         */
        public static Builder create(
                Store<StoreValue>                       meshObjectStore,
                Store<StoreValue>                       localNsStore,
                PrimaryMeshObjectIdentifierNamespaceMap primaryMap )
        {
            return new Builder( meshObjectStore, localNsStore, primaryMap );
        }

        protected Builder(
                Store<StoreValue>                       meshObjectStore,
                Store<StoreValue>                       localNsStore,
                PrimaryMeshObjectIdentifierNamespaceMap primaryMap )
        {
            if( meshObjectStore == null ) {
                throw new IllegalArgumentException( "Must provide a Store for MeshObjects" );
            }
            if( localNsStore == null ) {
                throw new IllegalArgumentException( "Must provide a Store for local namespace mappings" );
            }
            if( primaryMap == null ) {
                throw new IllegalArgumentException( "Must provide a PrimaryMeshObjectIdentifierNamespaceMap" );
            }
            theMeshObjectStore = meshObjectStore;
            theLocalNsStore    = localNsStore;
            theNamespaceMap    = primaryMap;
        }

        /**
         * MMeshBase factory method.
         *
         * @return the created MMeshBase
         */
        public StoreMeshBase build()
        {
            AMeshBaseLifecycleManager      life       = AMeshBaseLifecycleManager.create();
            ImmutableMMeshObjectSetFactory setFactory = ImmutableMMeshObjectSetFactory.create( AMeshObject.class, AMeshObjectIdentifier.class );

            if( theDefaultNamespace == null ) {
                theDefaultNamespace = theNamespaceMap.getPrimary();
            } else {
                if( theNamespaceMap.getLocalNameFor( theDefaultNamespace ) == null ) {
                    throw new IllegalArgumentException( "Default namespace not known by namespace map" );
                }
            }
            AMeshObjectIdentifierFactory idFact = AMeshObjectIdentifierFactory.create( theNamespaceMap, theDefaultNamespace );

            if( idDeserializer == null ) {
                idDeserializer = ExternalNameHashMeshObjectIdentifierBothSerializer.create( theNamespaceMap, idFact );
            }
            StoreContextualMeshObjectIdentifierNamespaceMap localNsMap = StoreContextualMeshObjectIdentifierNamespaceMap.create(
                    theLocalNsStore,
                    theNamespaceMap );

            if( localNsMap.findByLocalName( ContextualMeshObjectIdentifierNamespaceMap.DEFAULT_LOCAL_NAME ) == null ) {
                // only for restore
                localNsMap.register( ContextualMeshObjectIdentifierNamespaceMap.DEFAULT_LOCAL_NAME, theDefaultNamespace );
            }

            ContextualMeshObjectIdentifierBothSerializer persistenceIdSerializer = ContextualMeshObjectIdentifierBothSerializer.create(
                    idFact,
                    localNsMap );

            MeshObjectStoreValueMapper          meshObjectMapper  = MeshObjectStoreValueMapper.create( persistenceIdSerializer, persistenceIdSerializer );
            StoreMeshBaseSwappingSmartMap       meshObjectStorage = StoreMeshBaseSwappingSmartMap.create( meshObjectMapper, theMeshObjectStore );
            StoreMeshBaseStateMeshObjectHistory history;

            StoreHeadMeshBaseView headMeshBaseView = StoreHeadMeshBaseView.create( meshObjectStorage, idFact, idDeserializer );

            if( theMeshObjectHistoryStore == null ) {
                history = null;
            } else {
                history = StoreMeshBaseStateMeshObjectHistory.create( persistenceIdSerializer, theMeshObjectHistoryStore, theTransactionLogStore );
            }

            StoreMeshBase ret = new StoreMeshBase(
                    headMeshBaseView,
                    idFact,
                    idDeserializer,
                    setFactory,
                    life,
                    theAccessManager,
                    theIsHistoryEditable,
                    theNormalizeChangeList,
                    history,
                    persistenceIdSerializer );

            headMeshBaseView.setMeshBase( ret );
            meshObjectMapper.setMeshBaseView( headMeshBaseView );
            setFactory.setMeshBase( ret );
            idFact.setMeshBase( ret );
            if( history != null ) {
                history.setMeshBase( ret );
            }

            ret.initializeHomeObject();

            if( log.isDebugEnabled() ) {
                log.debug( "created " + ret );
            }

            return ret;
        }

        public Builder accessManager(
                AccessManager value )
        {
            if( value == null ) {
                throw new NullPointerException();
            }
            theAccessManager = value;
            return this;
        }

        public Builder defaultNamespace(
                MeshObjectIdentifierNamespace value )
        {
            theDefaultNamespace = value;
            return this;
        }

        /**
         * Is this history of this MeshBase editable. Default is false.
         *
         * @param isHistoryEditable the new value
         * @return
         */
        public Builder isHistoryEditable(
                boolean isHistoryEditable )
        {
            theIsHistoryEditable = isHistoryEditable;
            return this;
        }

        /**
         * Normalize the ChangeList of a Transaction during commit. Default is true.
         *
         * @param normalizeChangeList the new value
         * @return
         */
        public Builder normalizeChangeList(
                boolean normalizeChangeList )
        {
            theNormalizeChangeList = normalizeChangeList;
            return this;
        }

        public Builder historyStores(
                StoreWithHistory<StoreValueWithTimeUpdated> meshObjectHistoryStore,
                StoreWithHistory<StoreValueWithTimeUpdated> transactionLogStore )
        {
            theMeshObjectHistoryStore = meshObjectHistoryStore;
            theTransactionLogStore    = transactionLogStore;
            return this;
        }

        protected Store<StoreValue>                       theMeshObjectStore;
        protected Store<StoreValue>                       theLocalNsStore;
        protected StoreWithHistory<StoreValueWithTimeUpdated> theMeshObjectHistoryStore;
        protected StoreWithHistory<StoreValueWithTimeUpdated> theTransactionLogStore;

        protected AccessManager                           theAccessManager = PermitAllAccessManager.create(); // FIXME change to a different default
        protected PrimaryMeshObjectIdentifierNamespaceMap theNamespaceMap;
        protected MeshObjectIdentifierNamespace           theDefaultNamespace; // in which new MeshObjects are created if no id is given
        protected MeshObjectIdentifierDeserializer        idDeserializer;
        protected boolean                                 theInitializeHomeObject = true;
        protected boolean                                 theIsHistoryEditable    = false;
        protected boolean                                 theNormalizeChangeList  = true;
    }

    /**
     * Constructor.
     *
     * @param headMeshBaseView the head view
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBase
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     * @param setFactory the factory for MeshObjectSets appropriate for this MeshBase
     * @param life the MeshBaseLifecycleManager to use
     * @param accessMgr the AccessManager that controls access to this MeshBase
     * @param isHistoryEditable if true, history is editable
     * @param normalizeChangeList if true, normalize the ChangeList of a Transaction during commit
     * @param history contains the MeshBase's historical information, or null
     * @param persistenceIdSerializer knows how to serialize identifiers
     */
    protected StoreMeshBase(
            StoreHeadMeshBaseView                        headMeshBaseView,
            MeshObjectIdentifierFactory                  identifierFactory,
            MeshObjectIdentifierDeserializer             idDeserializer,
            MeshObjectSetFactory                         setFactory,
            AMeshBaseLifecycleManager                    life,
            AccessManager                                accessMgr,
            boolean                                      isHistoryEditable,
            boolean                                      normalizeChangeList,
            StoreMeshBaseStateMeshObjectHistory          history,
            ContextualMeshObjectIdentifierBothSerializer persistenceIdSerializer )
    {
        super( identifierFactory, idDeserializer, setFactory, life, accessMgr, isHistoryEditable, normalizeChangeList );

        theHeadMeshBaseView        = headMeshBaseView;
        theHistory                 = history;
        thePersistenceIdSerializer = persistenceIdSerializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractHeadMeshBaseView getHeadMeshBaseView()
    {
        return theHeadMeshBaseView;
    }

    /**
     * Helper method to cast the storage for MeshObjects to the right subtype.
     *
     * @return the meshObjecStorage
     */
    protected StoreMeshBaseSwappingSmartMap getMeshObjectStorage()
    {
        return theHeadMeshBaseView.getStorage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        try {
            return getMeshObjectStorage().getStore().size();

        } catch( IOException ex ) {
            log.error( ex );
            return 0;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreMeshBaseStateMeshObjectHistory getHistory()
    {
        return theHistory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectHistory meshObjectHistory(
            MeshObjectIdentifier identifier )
    {
       if( theHistory != null ) {
           return theHistory.history( identifier );
       } else {
           return null;
       }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectHistory obtainMeshObjectHistory(
            MeshObjectIdentifier identifier )
    {
        if( theHistory != null ) {
            return theHistory.obtainHistory( identifier );
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObjectHistory> meshObjectHistoriesIterator()
    {
        if( theHistory != null ) {
            return theHistory.meshObjectHistoriesIterator();
        } else {
            return ZeroElementCursorIterator.create();
        }
    }

    /**
     * Let the MeshBaseIndex get at the MeshObjectIdentifierSerializer we use for persistence
     * purposes.
     *
     * @return the MeshObjectIdentifierBothSerializer
     */
    public MeshObjectIdentifierBothSerializer getPersistenceIdSerializer()
    {
        return thePersistenceIdSerializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void headTransactionCommitted(
            HeadTransaction tx )
    {
        Map<MeshObjectIdentifier,MeshObject> toWrite = determineObjectsToWriteFromTransaction( tx );

        OnCommitWriteStoreMeshBaseSwappingSmartMap meshObjectStorage = getMeshObjectStorage();

        // write main store
        for( Map.Entry<MeshObjectIdentifier,MeshObject> current : toWrite.entrySet() ) {
            if( current.getValue().getIsDead() ) {
                meshObjectStorage.removeValueFromStorageUponCommit( current.getKey() );
            } else {
                meshObjectStorage.saveValueToStorageUponCommit( current.getKey(), current.getValue() );
            }
        }
        meshObjectStorage.transactionDone();

        if( theHistory != null ) {
            theHistory.headTransactionCommitted( tx );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void historyTransactionCommitted(
            HistoryTransaction tx )
    {
        if( theHistory != null ) {
            theHistory.historyTransactionCommitted( tx );
        }
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void transactionRolledbackHook(
            Transaction tx )
    {
        super.transactionRolledbackHook( tx );

        OnCommitWriteStoreMeshBaseSwappingSmartMap meshObjectStorage = getMeshObjectStorage();

        meshObjectStorage.transactionUndone();
    }

    /**
     * Determine which MeshObjest must be written to the Store, given what happened
     * during the Transaction. This is factored out as a static, so NetStoreMeshBase can also
     * use it without replicating code.
     *
     * @param tx Transaction the Transaction that was committed
     * @return a map of changed MeshObjects
     */
    public static Map<MeshObjectIdentifier,MeshObject> determineObjectsToWriteFromTransaction(
            HeadTransaction tx )
    {
        List<Change> changes = tx.getChangeList().getChanges();

        HashMap<MeshObjectIdentifier,MeshObject> ret = new HashMap<>( changes.size() );

        // we go backwards, that way we don't forget to store MeshObjects that were deleted and recreated within the
        // same Transaction
        for( int i=changes.size()-1 ; i>=0 ; --i ) {

            Change                  currentChange = changes.get( i );
            MeshObjectIdentifier [] affectedNames = currentChange.getAffectedMeshObjectIdentifiers();
            MeshObject []           affecteds     = currentChange.getAffectedMeshObjects();

            for( int j=0 ; j<affectedNames.length ; ++j ) {
                MeshObjectIdentifier affectedName = affectedNames[j];
                MeshObject           affected     = ret.get( affectedName );
                if( affected != null && !affected.getIsDead() ) {
                    continue;
                }
                affected = affecteds[j]; // also use this if the previous affected was dead -- MeshObject was recreated during same Tx

                if( affected == null ) {
                    log.error( "Cannot find affected MeshObject " + affectedName );

                } else if( affected.getIsDead() ) {
                    // need to check for isDead first, otherwise we might update instead of delete, for example
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof AbstractMeshObjectLifecycleChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof AbstractMeshObjectAttributesChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof MeshObjectAttributeChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof AbstractMeshObjectTypeChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof MeshObjectPropertyChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof AbstractMeshObjectRoleAttributesChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof MeshObjectRoleAttributeChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof AbstractMeshObjectRoleTypeChange ) {
                    ret.put( affectedName, affected );

                } else if( currentChange instanceof MeshObjectRolePropertyChange ) {
                    ret.put( affectedName, affected );

                } else {
                    log.error( "Unknown change: " + currentChange );
                }
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractEditableMeshBaseView findMeshBaseViewForCurrentTransaction()
    {
        AbstractEditableMeshBaseView ret;
        ATransaction                 tx = getCurrentTransaction();

        if( tx == null ) {
            ret = theHeadMeshBaseView;
        } else {
            ret = tx.getDefaultMeshBaseViewForTransaction();
        }
        return ret;
    }

    @Override
    public boolean isPersistent()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearMemoryCache()
    {
        theHeadMeshBaseView.clearLocalCache();
        if( theHistory != null ) {
            theHistory.clearLocalCache();
        }
    }

    /**
     * All functionality related to the HEAD view.
     */
    protected final StoreHeadMeshBaseView theHeadMeshBaseView;

    /**
     * All functionality related to histories, if any.
     */
    protected final StoreMeshBaseStateMeshObjectHistory theHistory;

    /**
     * The serializer we use for persistence purposes, so we can provide it to the StoreMeshBaseIndex.
     */
    protected final MeshObjectIdentifierBothSerializer thePersistenceIdSerializer;
}
