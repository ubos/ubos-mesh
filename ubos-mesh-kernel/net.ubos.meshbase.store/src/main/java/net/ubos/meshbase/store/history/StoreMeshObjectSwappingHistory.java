//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.MeshBase;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.util.StoreWithHistoryBackedSwappingHistory;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * Store-based implementation of MeshObjectHistory.
 */
public class StoreMeshObjectSwappingHistory
        extends
            StoreWithHistoryBackedSwappingHistory<MeshObject, StoreValueWithTimeUpdated>
        implements
            MeshObjectHistory
{
    private static final Log log = Log.getLogInstance( StoreMeshObjectSwappingHistory.class );

    /**
     * Factory method.
     *
     * @param id the identifier of the MeshObjects in the MeshObjectHistory
     * @param meshBase the MeshBase that contains this MeshObjectHistory
     * @param mapper knows how to map StoreValueWithTimeUpdate to T and back
     * @param store the Store where to store the MeshObjectHistory
     * @return the created Object
     */
    public static StoreMeshObjectSwappingHistory create(
            MeshObjectIdentifier                        id,
            MeshBase                                    meshBase,
            MeshObjectStoreValueWithTimeUpdatedMapper   mapper,
            StoreWithHistory<StoreValueWithTimeUpdated> store )
    {
        SmartMap<Long,ReferenceWithTimeUpdated<MeshObject>> localCache = MSmartMap.create();

        return new StoreMeshObjectSwappingHistory( id, meshBase, localCache, mapper, store );
    }

    /**
     * Private constructor, us factory method.
     *
     * @param id the identifier of the MeshObjects in the MeshObjectHistory
     * @param meshBase the MeshBase that contains this MeshObjectHistory
     * @param localCache the local cache to use
     * @param mapper knows how to map StoreValueWithTimeUpdate to T and back
     * @param store the Store where to store the MeshObjectHistory
     */
    protected StoreMeshObjectSwappingHistory(
            MeshObjectIdentifier                                id,
            MeshBase                                            meshBase,
            SmartMap<Long,ReferenceWithTimeUpdated<MeshObject>> localCache,
            MeshObjectStoreValueWithTimeUpdatedMapper           mapper,
            StoreWithHistory<StoreValueWithTimeUpdated>         store )
    {
        super( mapper.keyToString( id ), localCache, mapper, store );

        theMeshBase   = meshBase;
        theIdentifier = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier getMeshObjectIdentifier()
    {
        return theIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ReferenceWithTimeUpdated<MeshObject> createReference(
            MeshObject value )
    {
        return new WeakReferenceWithTimeUpdated<>( value, theQueue, value.getTimeUpdated() );
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            MeshObject toAdd )
        throws
            IllegalArgumentException
    {
        MeshObject already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same MeshObject:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "MeshObject exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * The MeshBase to which this history belongs
     */
    protected final MeshBase theMeshBase;

    /**
     * The MeshObjectIdentifier of the MeshObject whose history this is.
     */
    protected final MeshObjectIdentifier theIdentifier;
}
