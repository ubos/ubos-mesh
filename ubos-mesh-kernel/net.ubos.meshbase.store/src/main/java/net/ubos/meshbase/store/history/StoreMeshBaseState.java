//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.history;

import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.history.AbstractEditableHistoricMeshBaseState;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.util.logging.Log;

/**
 * Extends the implementation of MeshBaseState to construct the list of Changes lazily
 * from the externalized list of changes.
 *
 * The externalized list of changes, which will only be converted into the
 * ChangeList on demand, to break an otherwise infinite recursion where restoring
 * a MeshObject in a MeshObjectHistory first requires its MeshBaseView to be restored, but
 * that restoration of the MeshBaseView causes its MeshBaseState to be created, which in turn
 * requires the very same MeshObject to have been restored first as part of its ChangeList.
 */
public class StoreMeshBaseState
    extends
        AbstractEditableHistoricMeshBaseState
{
    private static final Log log = Log.getLogInstance( StoreMeshBaseState.class );

    /**
     * Factory method, for the case where the MeshBaseState is created as part of a new Transaction.
     *
     * @param time the time when this state begun, in System.currentTimeMillis() format
     * @param changes the Changes that led to this state
     * @param meshBaseView the content of the MeshBase at this time
     * @return the created MeshBaseState
     */
    public static StoreMeshBaseState createWithChanges(
            long                      time,
            ChangeList                changes,
            StoreHistoricMeshBaseView meshBaseView )
    {
        return new StoreMeshBaseState( time, changes.asExternalized(), meshBaseView );
    }

    /**
     * Factory method.
     *
     * @param time the time when this state begun, in System.currentTimeMillis() format
     * @param extChanges the still externalized Changes that led to this state
     * @param meshBaseView the content of the MeshBase at this time
     * @return the created MeshBaseState
     */
    public static StoreMeshBaseState createWithLazilyRestoredChanges(
            long                      time,
            List<ExternalizedChange>  extChanges,
            StoreHistoricMeshBaseView meshBaseView )
    {
        return new StoreMeshBaseState( time, extChanges, meshBaseView );
    }

    /**
     * Constructor.
     *
     * @param time the time when this state begun, in System.currentTimeMillis() format
     * @param extChanges the externalized Changes that led to this state, if changes is not given
     * @param meshBaseView the content of the MeshBase at this time
     */
    public StoreMeshBaseState(
            long                      time,
            List<ExternalizedChange>  extChanges,
            StoreHistoricMeshBaseView meshBaseView )
    {
        super( time, null, meshBaseView );

        theExtChanges = extChanges;
    }

    /**
     * Internal helper to make sure the externalized Changes have been resolved.
     */
    @Override
    protected synchronized void ensureChangesResolved()
    {
        if( theChanges == null ) {
            try {
                MeshBase   mb = theMeshBaseView.getMeshBase();
                theChanges = mb.createChangeList();

                for( ExternalizedChange externalizedChange : theExtChanges ) {
                    MeshObjectIdentifier [] affectedIds = externalizedChange.getAffectedMeshObjectIdentifiers();
                    MeshObject           [] affecteds   = theMeshBaseView.createArray( affectedIds.length );

                    for( int i=0 ; i<affectedIds.length ; ++i ) {
                        affecteds[i] = mb.meshObjectHistory( affectedIds[i] ).atOrAfter( theTimeUpdated );
                        if( affecteds[i].getTimeUpdated() < theTimeUpdated ) {
                            affecteds[i] = ((AMeshObject)affecteds[i]).createCopyForMeshBaseView( theMeshBaseView );
                        }
                        ((StoreHistoricMeshBaseView)theMeshBaseView).putNewMeshObject( affecteds[i] );
                    }

                    Change change = externalizedChange.internalizeWith( affecteds );
                    theChanges.addChange( change );
                }
            } catch( MeshTypeNotFoundException ex ) {
                log.error(  ex );
            }
        }
    }

    /**
     * The Changes that led to this MeshBaseState, before they have been resolved.
     */
    protected List<ExternalizedChange> theExtChanges;
}
