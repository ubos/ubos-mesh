//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.history;

import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.externalized.MeshObjectDecoder;
import net.ubos.mesh.externalized.MeshObjectEncoder;
import net.ubos.mesh.externalized.json.MeshObjectJsonDecoder;
import net.ubos.mesh.externalized.json.MeshObjectJsonEncoder;
import net.ubos.mesh.externalized.xml.MeshObjectXmlDecoder;
import net.ubos.mesh.externalized.xml.MeshObjectXmlEncoder;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.store.AbstractMeshObjectMapper;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.util.KeyAndStoreValueWithTimeUpdatedMapper;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedDecodingException;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedEncodingException;

/**
 * Knows how to map MeshObjects in MeshObjectHistories to and from StoreValueWithTimeUpdated.
 */
public class MeshObjectStoreValueWithTimeUpdatedMapper
    extends
        AbstractMeshObjectMapper
    implements
        KeyAndStoreValueWithTimeUpdatedMapper<MeshObjectIdentifier,MeshObject,StoreValueWithTimeUpdated>
{
    /**
     * Constructor.
     *
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     */
    public MeshObjectStoreValueWithTimeUpdatedMapper(
            MeshObjectIdentifierSerializer   idSerializer,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        super( idSerializer, idDeserializer );
    }

    /**
     * Let us find the MeshBase.
     */
    public void setStoreMeshBaseStateMeshObjectHistory(
            StoreMeshBaseStateMeshObjectHistory newValue )
    {
        if( theStoreMeshBaseStateMeshObjectHistory != null ) {
            throw new IllegalStateException( "Must not set more than once" );
        }
        theStoreMeshBaseStateMeshObjectHistory = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDefaultEncodingId()
    {
        return DEFAULT_ENCODER.getEncodingId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MeshObjectEncoder getPreferredEncoder()
    {
        return DEFAULT_ENCODER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MeshObjectDecoder getDecoderFor(
            String encoderId )
    {
        for( MeshObjectDecoder current : theDecoders ) {
            if( encoderId.equals( current.getEncodingId() )) {
                return current;
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject decodeValue(
            StoreValueWithTimeUpdated value )
        throws
            StoreValueWithTimeUpdatedDecodingException
    {
        StoreMeshBase mb  = theStoreMeshBaseStateMeshObjectHistory.getMeshBase();
        MeshBaseView  mbv = mb.getHistory().at( value.getTimeUpdated() ).getMeshBaseView();

        try {
            ExternalizedMeshObject externalized = deserializeValue( value );

            MeshObject ret = mb.recreateMeshObject( externalized, (AbstractEditableMeshBaseView) mbv );

            return ret;

        } catch( DecodingException ex ) {
            throw new StoreValueWithTimeUpdatedDecodingException( value, ex );

        } catch( IOException ex ) {
            throw new StoreValueWithTimeUpdatedDecodingException( value, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdated encodeValue(
            String     key,
            MeshObject value )
        throws
            StoreValueWithTimeUpdatedEncodingException
    {
        try {
            MeshObjectEncoder encoder = getPreferredEncoder();
            byte [] serialized = serializeValue( value, encoder );
            return new StoreValueWithTimeUpdated( key, encoder.getEncodingId(), value.getTimeUpdated(), serialized );

        } catch( EncodingException ex ) {
            throw new StoreValueWithTimeUpdatedEncodingException( ex );

        } catch( IOException ex ) {
            throw new StoreValueWithTimeUpdatedEncodingException( ex );
        }
    }

    /**
     * Lets us find our MeshBase.
     */
    protected StoreMeshBaseStateMeshObjectHistory theStoreMeshBaseStateMeshObjectHistory;

    /**
     * The MeshBaseView in which we
     */
    /**
     * The set of encoders currently known.
     */
    protected static final MeshObjectEncoder[] theEncoders = {
            MeshObjectJsonEncoder.create(),
            MeshObjectXmlEncoder.create()
    };

    /**
     * The set of decoders currently known.
     */
    protected static final MeshObjectDecoder[] theDecoders = {
            MeshObjectJsonDecoder.create(),
            MeshObjectXmlDecoder.create()
    };

    /**
     * The default encoder.
     */
    protected static final MeshObjectEncoder DEFAULT_ENCODER = theEncoders[0];
}
