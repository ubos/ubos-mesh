//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.history;

import java.lang.ref.Reference;
import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.util.StoreWithHistoryBackedSwappingSmartMapOfHistory;
import net.ubos.util.history.History;

/**
 *
 */
public class StoreMeshObjectHistoryMap
    extends
        StoreWithHistoryBackedSwappingSmartMapOfHistory.Weak<MeshObjectIdentifier,MeshObject,StoreValueWithTimeUpdated>
{
    public static StoreMeshObjectHistoryMap create(
            MeshObjectStoreValueWithTimeUpdatedMapper   mapper,
            StoreWithHistory<StoreValueWithTimeUpdated> meshObjectHistoryStore )
    {
        return new StoreMeshObjectHistoryMap(
                new HashMap<>(),
                mapper,
                meshObjectHistoryStore );
    }

    /**
     * @param localHistoryCache the Map to use as a local cache of histories
     * @param mapper translates values inside the histories to the StoreValueWithTimeUpdatedused by the underlying Store
     * @param store the underlying Store
     */
    protected StoreMeshObjectHistoryMap(
            Map<MeshObjectIdentifier,Reference<History<MeshObject>>> localHistoryCache,
            MeshObjectStoreValueWithTimeUpdatedMapper                mapper,
            StoreWithHistory<StoreValueWithTimeUpdated>              store )
    {
        super( localHistoryCache, mapper, store );
    }

    /**
     * Let us find the history.
     *
     * @param newValue the history
     */
    public void setStoreMeshBaseStateMeshObjectHistory(
            StoreMeshBaseStateMeshObjectHistory newValue )
    {
        if( theStoreMeshBaseStateMeshObjectHistory != null ) {
            throw new IllegalStateException( "Must not set more than once" );
        }
        theStoreMeshBaseStateMeshObjectHistory = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected StoreMeshObjectSwappingHistory loadValueFromStorage(
            MeshObjectIdentifier key )
    {
        StoreMeshObjectSwappingHistory ret = StoreMeshObjectSwappingHistory.create(
                key,
                theStoreMeshBaseStateMeshObjectHistory.getMeshBase(),
                (MeshObjectStoreValueWithTimeUpdatedMapper) theMapper,
                theStore );
        if( ret.getLength() > 0 ) {
            return ret;
        } else {
            return null;
        }
    }

    protected StoreMeshBaseStateMeshObjectHistory theStoreMeshBaseStateMeshObjectHistory;
}
