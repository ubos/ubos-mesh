//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store;

import java.util.HashMap;
import java.util.HashSet;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.Log;
import net.ubos.store.util.StoreBackedSwappingSmartMap;
import net.ubos.store.util.StoreValueMapper;

/**
 * Modifies StoredBasedSwappingHashMap to only write to storage upon commit of a Transaction.
 */
public class StoreMeshBaseSwappingSmartMap
    extends
        StoreBackedSwappingSmartMap.Soft<MeshObjectIdentifier,MeshObject,StoreValue>
    implements
        OnCommitWriteStoreMeshBaseSwappingSmartMap
{
    private static final Log log = Log.getLogInstance(StoreMeshBaseSwappingSmartMap.class ); // our own, private logger

    /**
     * Create a <code>StoreBackedSwappingHashMap</code> that uses <code>WeakReferences</code>.
     *
     * @param mapper the <code>StoreValueMapper</code> to use
     * @param store the underlying <code>Store</code>
     * @return the created <code>StoreBackedSwappingHashMap</code>
     */
    public static StoreMeshBaseSwappingSmartMap create(
            StoreValueMapper<MeshObjectIdentifier,MeshObject,StoreValue> mapper,
            Store<StoreValue>                                            store )
    {
        if( mapper == null ) {
            throw new NullPointerException();
        }
        if( store == null ) {
            throw new NullPointerException();
        }

        return new StoreMeshBaseSwappingSmartMap( mapper, store );
    }

    /**
     * Constructor.
     *
     * @param mapper the <code>StoreValueMapper</code> to use
     * @param store the underlying <code>Store</code>
     */
    protected StoreMeshBaseSwappingSmartMap(
            StoreValueMapper<MeshObjectIdentifier,MeshObject,StoreValue> mapper,
            Store<StoreValue>                                            store )
    {
        super( new HashMap<>(), mapper, store );
    }

    /**
     * Unlike the superclass, do not load anything that has been removed.
     *
     * @param key the key whose value should be loaded
     * @return the value that was loaded, or null if none.
     */
    @Override
    protected MeshObject loadValueFromStorage(
            MeshObjectIdentifier key )
    {
        if( theRemoved.contains( key )) {
            return null;
        }
        return super.loadValueFromStorage( key );
    }

    /**
     * Unlike the superclass, don't do anything. Saving occurs only when a Transaction is committed.
     *
     * @param key the key whose value was updated
     * @param newValue the new value
     */
    @Override
    protected void saveValueToStorage(
            MeshObjectIdentifier key,
            MeshObject           newValue )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "saveValueToStorage", key, newValue );
        }
    }

    /**
     * Save value to storage. Invoked by Transaction commit.
     *
     * @param key the key whose value was updated
     * @param newValue the new value
     */
    @Override
    public void saveValueToStorageUponCommit(
            MeshObjectIdentifier key,
            MeshObject           newValue )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "saveValueToStorageUponCommit", key, newValue );
        }
        super.saveValueToStorage( key, newValue );

        theRemoved.remove( key ); // happens if the object was created during the transaction.
                                  // And if not contained, nothing bad happens either.
    }

    /**
     * Unlike the superclass, don't do anything. Removing occurs only when a Transaction is committed.
     *
     * @param key the key whose value has been removed
     */
    @Override
    protected void removeValueFromStorage(
            MeshObjectIdentifier key )
    {
        theRemoved.add( key );
    }

    /**
     * Remove value from storage. Invoked by Transaction commit.
     *
     * @param key the key whose value has been removed
     */
    @Override
    public void removeValueFromStorageUponCommit(
            MeshObjectIdentifier key )
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "removeValueFromStorageUponCommit", key );
        }

        super.removeValueFromStorage( key );
        theRemoved.remove( key );
    }

    /**
     * The transaction is done. Whatever is still in theRemoved is a programming error.
     */
    @Override
    public void transactionDone()
    {
        if( !theRemoved.isEmpty() ) {
            log.error( "theRemoved not empty", theRemoved );
        }
        theRemoved.clear();
    }

    /**
     * The transaction has been undone. Discard theRemoved.
     */
    @Override
    public void transactionUndone()
    {
        theRemoved.clear();
    }

    /**
     * Keep track of MeshObjects that were removed during a Transaction, to avoid recreating them from the storage
     * although they were deleted during a transaction.
     */
    protected HashSet<Object> theRemoved = new HashSet<>();
}
