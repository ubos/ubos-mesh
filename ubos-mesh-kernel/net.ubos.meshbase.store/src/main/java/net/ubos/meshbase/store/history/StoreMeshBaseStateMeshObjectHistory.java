//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.history;

import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.a.AChangeList;
import net.ubos.meshbase.a.AMeshBaseStateHistory;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.transaction.HistoryTransaction;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.util.StoreWithHistoryBackedSwappingHistory;
import net.ubos.store.history.util.StoreWithHistoryBackedSwappingSmartMapOfHistory;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;
import net.ubos.util.history.History;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SmartMap;
import net.ubos.util.smartmap.m.MSmartMap;

/**
 * Implementation class for everything related to histories for the StoreMeshBase.
 * It inherits from MeshBaseStateHistory, and implements that, but it also implements the histories of
 * MeshObjects. Which is why we call it StoreMeshBaseStateMeshObjectHistory instead of StoreMeshBaseStateHistory.
 *
 * This cannot implementation-inherit from StoreWithHistoryBackedHistory&lt;MeshBaseState&gt;
 * because what we are swapping in and out is the ChangeList from the Transaction, not the MeshBaseState.
 *
 * Compare MMeshBaseStateMeshObjectHistory
 */

public class StoreMeshBaseStateMeshObjectHistory
     extends
        StoreWithHistoryBackedSwappingHistory.Weak<MeshBaseState,StoreValueWithTimeUpdated>
     implements
        AMeshBaseStateHistory
{
    private static final Log log = Log.getLogInstance( StoreMeshBaseStateMeshObjectHistory.class ); // our own, private logger

   /**
     * Factory method.
     *
     * @param persistenceIdSerializer use this to serialize and deserialize MeshObjectIdentifiers
     * @param meshObjectHistoriesStore stores the histories of MeshObjects
     * @param transactionStore stores past MeshBase transactions
     * @return the created object
     */
    public static StoreMeshBaseStateMeshObjectHistory create(
            MeshObjectIdentifierBothSerializer          persistenceIdSerializer,
            StoreWithHistory<StoreValueWithTimeUpdated> meshObjectHistoriesStore,
            StoreWithHistory<StoreValueWithTimeUpdated> transactionStore )
    {
        MeshObjectStoreValueWithTimeUpdatedMapper meshObjectMapper
                = new MeshObjectStoreValueWithTimeUpdatedMapper( persistenceIdSerializer, persistenceIdSerializer );

        StoreMeshObjectHistoryMap meshObjectHistories = StoreMeshObjectHistoryMap.create( meshObjectMapper, meshObjectHistoriesStore );

        TransactionToMeshBaseStateMapper transactionToMeshBaseStateMapper
                = new TransactionToMeshBaseStateMapper( persistenceIdSerializer, persistenceIdSerializer );

        StoreMeshBaseStateMeshObjectHistory ret = new StoreMeshBaseStateMeshObjectHistory(
                meshObjectHistories,
                "TX",
                MSmartMap.create(),
                transactionToMeshBaseStateMapper,
                meshObjectMapper,
                transactionStore );

        meshObjectMapper.setStoreMeshBaseStateMeshObjectHistory( ret );
        transactionToMeshBaseStateMapper.setStoreMeshBaseStateMeshObjectHistory( ret );
        meshObjectHistories.setStoreMeshBaseStateMeshObjectHistory( ret );

        return ret;
    }

    /**
     * Constructor.
     *
     * @param meshObjectHistories store the MeshObjectHistories here
     * @param transactionStoreKey use this key to find the applicable transactions in the transactionStore
     * @param localMeshBaseStateCache use this to cache MeshBaseStates in memory
     * @param transactionToMeshBaseStateMapper use this to map entries in the MeshBaseStateStore to/from MeshBaseStates
     * @param meshObjectMapper use this to map entries in the MeshObjectStore to/from MeshObjects
     * @param transactionStore stores past MeshBase transactions
     */
    protected StoreMeshBaseStateMeshObjectHistory(
            StoreWithHistoryBackedSwappingSmartMapOfHistory<MeshObjectIdentifier,MeshObject,StoreValueWithTimeUpdated> meshObjectHistories,
            String                                                                                                     transactionStoreKey,
            SmartMap<Long,ReferenceWithTimeUpdated<MeshBaseState>>                                                     localMeshBaseStateCache,
            TransactionToMeshBaseStateMapper                                                                           transactionToMeshBaseStateMapper,
            MeshObjectStoreValueWithTimeUpdatedMapper                                                                  meshObjectMapper,
            StoreWithHistory<StoreValueWithTimeUpdated>                                                                transactionStore )
    {
        super( transactionStoreKey, localMeshBaseStateCache, transactionToMeshBaseStateMapper, transactionStore );

        theMeshObjectHistories = meshObjectHistories;
        theMeshObjectMapper    = meshObjectMapper;
    }

    /**
     * Set the MeshBase after initialization.
     *
     * @param mb the meshBase of which this is the MeshBaseHistory
     */
    public void setMeshBase(
            StoreMeshBase mb )
    {
        if( theMeshBase != null ) {
            throw new IllegalStateException( "Cannot set MeshBase more than once" );
        }
        theMeshBase = mb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreMeshBase getMeshBase()
    {
        return theMeshBase;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBaseState obtainAt(
            long t )
    {
        MeshBaseState ret = at( t );
        if( ret == null ) {
            StoreHistoricMeshBaseView view = StoreHistoricMeshBaseView.create( theMeshBase );
            ret = StoreMeshBaseState.createWithChanges( t, theMeshBase.createChangeList(), view );
            view.setMeshBaseState( ret );

            putIgnorePrevious( ret );
        }
        return ret;
    }

    /**
     * Callback from the MMeshBase informing us that a Transaction was committed.
     *
     * @param tx the committed Transaction
     */
    public void headTransactionCommitted(
            HeadTransaction tx )
    {
        // compare MMeshBaseStateMeshObjectHistory.

        ChangeList changeList = tx.getChangeList();
        if( changeList.isEmpty() ) {
            return; // do nothing
        }

        StoreHistoricMeshBaseView newView = StoreHistoricMeshBaseView.create( getMeshBase() );

        ChangeList newChanges = AChangeList.createCopyWithMeshBaseView( changeList, newView );

        StoreMeshBaseState newState = StoreMeshBaseState.createWithChanges(
                            tx.getTimeEnded(),
                            newChanges,
                            newView );
        newView.setMeshBaseState( newState );

        for( MeshObject affected : newChanges.getAffectedMeshObjects() ) {
            MeshObjectHistory currentHistory = obtainHistory( affected.getIdentifier() );
            currentHistory.putOrThrow( affected );

            newView.putNewMeshObject( affected );
        }
        putOrThrow( newState ); // do this after dealing with the MeshObjectHistories, so the Changes are resolved
    }

    /**
     * Callback from the MMeshBase informing us that a Transaction was committed.
     *
     * @param tx the committed Transaction
     */
    public void historyTransactionCommitted(
            HistoryTransaction tx )
    {
        // compare StoreMeshBaseStateMeshObjectHistory.
        ChangeList changeList = tx.getChangeList();
        if( changeList.isEmpty() ) {
            theMeshBase.getHistory().removeIgnorePrevious( tx.getWhen() );
        }
    }

    /**
     * Callback from the TransactionToMeshBaseStateMapper to recreate a past MeshBaseState
     * from this externalized representation. This must not insert the MeshBaseState
     * into the cache, but it must create MeshObjects in the historic MeshBaseView.
     *
     * @param timeUpdated the time of the MeshBaseState
     * @param externalizedChanges the Changes that led to this MeshBaseState
     * @return the recreated MeshBaseState
     * @throws MeshTypeNotFoundException restoration failed because a MeshType could not be found
     */
    protected MeshBaseState recreateMeshBaseState(
            long                     timeUpdated,
            List<ExternalizedChange> externalizedChanges )
        throws
            MeshTypeNotFoundException
    {
        StoreHistoricMeshBaseView restoredView = StoreHistoricMeshBaseView.create( theMeshBase );

        MeshBaseState ret = StoreMeshBaseState.createWithLazilyRestoredChanges( timeUpdated, externalizedChanges, restoredView );
        restoredView.setMeshBaseState( ret );

        return ret;
    }

    /**
     * Obtain the historical record for a MeshObject with a given identifier.
     *
     * @param identifier the MeshObject's identifier
     * @return the history of the MeshObject with this identifier
     */
    public MeshObjectHistory history(
            MeshObjectIdentifier identifier )
    {
        return (MeshObjectHistory) theMeshObjectHistories.get( identifier );
    }

    /**
     * Obtain the historical record for a MeshObject with a given identifier.
     * Create it if it does not exist.
     *
     * @param identifier the MeshObject's identifier
     * @return the history of the MeshObject with this identifier
     */
    public synchronized MeshObjectHistory obtainHistory(
            MeshObjectIdentifier identifier )
    {
        MeshObjectHistory ret = (MeshObjectHistory) theMeshObjectHistories.get( identifier );
        if( ret == null ) {
            ret = StoreMeshObjectSwappingHistory.create(
                        identifier,
                        theMeshBase,
                        theMeshObjectMapper,
                        theMeshObjectHistories.getBackingStoreWithHistory() );
            theMeshObjectHistories.put( identifier, ret );
        }
        return ret;
    }

    /**
     * Obtain an iterator over all MeshObjectHistories this MeshBaseHistory knows about.
     *
     * @return the CursorIterator
     */
    public CursorIterator<MeshObjectHistory> meshObjectHistoriesIterator()
    {
        // This mapping is really just a cast, but it seems easier than figuring out how to coerce Java generics into it ...
        return new MappingCursorIterator<>(
                theMeshObjectHistories.valueIterator(),
                new MappingCursorIterator.Mapper<>() {
                    @Override
                    public MeshObjectHistory mapDelegateValueToHere(
                            History<MeshObject> delegateValue )
                    {
                        return (MeshObjectHistory) delegateValue;
                    }

                    @Override
                    public History<MeshObject> mapHereToDelegateValue(
                            MeshObjectHistory value )
                    {
                        return value;
                    }
                });
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            MeshBaseState toAdd )
        throws
            IllegalArgumentException
    {
        MeshBaseState already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same MeshBaseState:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "MeshBaseState exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * The MeshBase whose history implementation this is.
     */
    protected StoreMeshBase theMeshBase;

    /**
     * The histories of the individual MeshObjects, keyed by their identifiers.
     */
    protected final StoreWithHistoryBackedSwappingSmartMapOfHistory<MeshObjectIdentifier,MeshObject,StoreValueWithTimeUpdated> theMeshObjectHistories;

    /**
     * The mapper for MeshObjects.
     */
    protected final MeshObjectStoreValueWithTimeUpdatedMapper theMeshObjectMapper;

}
