//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.externalized.MeshObjectDecoder;
import net.ubos.mesh.externalized.MeshObjectEncoder;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.store.StoreValue;
import net.ubos.store.util.KeyMapper;
import net.ubos.util.logging.Log;

/**
 * Factors out functionality common to mappers that map MeshObjects to/from binary.
 */
public abstract class AbstractMeshObjectMapper
    implements
        KeyMapper<MeshObjectIdentifier>

{
    private static final Log log = Log.getLogInstance(AbstractMeshObjectMapper.class ); // our own, private logger

    /**
     * Constructor, for subclasses only.
     *
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     */
    protected AbstractMeshObjectMapper(
            MeshObjectIdentifierSerializer   idSerializer,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        theIdSerializer   = idSerializer;
        theIdDeserializer = idDeserializer;
    }

    /**
     * Obtain the MeshObjectIdentifierSerializer we use.
     *
     * @return the MeshObjectIdentifierSerializer
     */
    public MeshObjectIdentifierSerializer getMeshObjectIdentifierSerializer()
    {
        return theIdSerializer;
    }

    /**
     * Obttain the MeshObjectIdentifierDeserializer we use.
     *
     * @return the MeshObjectIdentifierDeserializer
     */
    public MeshObjectIdentifierDeserializer getMeshObjectIdentifierDeserializer()
    {
        return theIdDeserializer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String keyToString(
            MeshObjectIdentifier key )
    {
        String ret = theIdSerializer.toExternalForm( key );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectIdentifier stringToKey(
            String key )
        throws
            ParseException
    {
        MeshObjectIdentifier ret = theIdDeserializer.meshObjectIdentifierFromExternalForm( key );
        return ret;
    }

    /**
     * Obtain the preferred MeshObjectEncoder.
     *
     * @return the preferred MeshObjectEncoder, or null
     */
    protected abstract MeshObjectEncoder getPreferredEncoder();

    /**
     * Obtain the MeshObjectDecoder with a certain id.
     *
     * @param encoderId the encoderId
     * @return the MeshObjectDecoder, or null
     */
    protected abstract MeshObjectDecoder getDecoderFor(
            String encoderId );

    /**
     * Deserialize a value.
     *
     * @param value the value
     * @return the deserialized value
     * @throws DecodingException thrown if deserialization failed
     * @throws IOException an I/O error occurred
     */
    public ExternalizedMeshObject deserializeValue(
            StoreValue value )
        throws
            DecodingException,
            IOException
    {
        String encodingId = value.getEncodingId();

        MeshObjectDecoder decoder = getDecoderFor( encodingId );
        if( decoder == null ) {
            throw new DecodingException.Installation( "Unknown encoding ID: " + value.getEncodingId() );
        }

        InputStream stream = value.getDataAsStream();

        ExternalizedMeshObject ret = decoder.decodeExternalizedMeshObject(
                new InputStreamReader( stream, StandardCharsets.UTF_8 ),
                theIdDeserializer );

        return ret;
    }

    /**
     * Serialize a value.
     *
     * @param value the value
     * @param encoder the MeshObjectEncoder to use
     * @return serialized form
     * @throws EncodingException serialization failed
     * @throws IOException an I/O error occurred
     */
    protected byte [] serializeValue(
            MeshObject           value,
            MeshObjectEncoder    encoder )
        throws
            EncodingException,
            IOException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        OutputStreamWriter    w   = new OutputStreamWriter( out );

        encoder.writeMeshObject( value, theIdSerializer, w );

        w.flush();
        w.close();

        return out.toByteArray();
    }

    /**
     * The object that knows how to serialize MeshObjectIdentifiers.
     */
    protected final MeshObjectIdentifierSerializer theIdSerializer;

    /**
     * The object that knows how to deserialize MeshObjectIdentifiers.
     */
    protected final MeshObjectIdentifierDeserializer theIdDeserializer;
}
