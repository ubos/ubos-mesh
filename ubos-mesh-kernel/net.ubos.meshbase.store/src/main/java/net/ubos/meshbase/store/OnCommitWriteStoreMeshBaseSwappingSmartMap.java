//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.util.smartmap.SmartMap;

/**
 * Little hack that lets the InstrumentedStoreMeshBase be easily swapped in.
 */
public interface OnCommitWriteStoreMeshBaseSwappingSmartMap
    extends
        SmartMap<MeshObjectIdentifier,MeshObject>
{
    /**
     * Remove value from storage. Invoked by Transaction commit.
     *
     * @param key the key whose value has been removed
     */
    public void removeValueFromStorageUponCommit(
            MeshObjectIdentifier key );

    /**
     * Save value to storage. Invoked by Transaction commit.
     *
     * @param key the key whose value was updated
     * @param newValue the new value
     */
    public void saveValueToStorageUponCommit(
            MeshObjectIdentifier key,
            MeshObject           newValue );

    /**
     * The transaction is done. Whatever is still in theRemoved is a programming error.
     */
    public void transactionDone();

    /**
     * The transaction has been undone.
     */
    public void transactionUndone();
}
