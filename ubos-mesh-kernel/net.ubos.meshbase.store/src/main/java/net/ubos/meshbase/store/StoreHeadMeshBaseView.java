//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.AbstractHeadMeshBaseView;
import net.ubos.meshbase.security.ByAccessFilteringCursorIterator;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;

/**
 * The MeshBaseView that contains the head version of the MeshObjects in a MeshBase.
 */
public class StoreHeadMeshBaseView
    extends
        AbstractHeadMeshBaseView
{
    private static final Log log = Log.getLogInstance( StoreHeadMeshBaseView.class );

    /**
     * Factory method.
     *
     * @param storage stores the MeshBase content
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBaseView
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     * @return the created StoreHeadMeshBaseView
     */
    public static StoreHeadMeshBaseView create(
            StoreMeshBaseSwappingSmartMap    storage,
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        StoreHeadMeshBaseView ret = new StoreHeadMeshBaseView( storage, identifierFactory, idDeserializer );
        return ret;
    }

    /**
     * Constructor for subclasses only.
     *
     * @param storage stores the MeshBase content
     * @param identifierFactory the factory for MeshObjectIdentifiers appropriate for this MeshBaseView
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers provided as Strings
     */
    public StoreHeadMeshBaseView(
            StoreMeshBaseSwappingSmartMap    storage,
            MeshObjectIdentifierFactory      identifierFactory,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        super( identifierFactory, idDeserializer );

        theStorage = storage;
    }

    /**
     * Obtain the underlying storage.
     *
     * @return the storage
     */
    public StoreMeshBaseSwappingSmartMap getStorage()
    {
        return theStorage;
    }

    /**
     * Delete all content. This is (very) destructive.
     */
    protected void clear()
    {
        theStorage.clear();
    }

    /**
     * If this View uses a local cache, clear it.
     * This is used as test instrumentation.
     */
    public void clearLocalCache()
    {
        theStorage.clearLocalCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putNewMeshObject(
            MeshObject obj )
    {
        theStorage.putIgnorePrevious( obj.getIdentifier(), obj );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeMeshObject(
            MeshObjectIdentifier objId )
    {
        theStorage.removeIgnorePrevious( objId );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getHomeObject()
    {
        MeshObject home = theStorage.get( getHomeMeshObjectIdentifier() );
        return home;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        return theStorage.containsKey( identifier );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject findMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        MeshObject ret = theStorage.get( identifier );
        if( ret == null ) {
            return ret;
        }
        try {
            getMeshBase().getAccessManager().checkPermittedAccess( ret );
            return ret;

        } catch( NotPermittedException ex ) {
            log.debug( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> iterator()
    {
        ByAccessFilteringCursorIterator ret = ByAccessFilteringCursorIterator.create(
                theStorage.valueIterator(),
                getMeshBase().getAccessManager() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject[] createArray(
            int len )
    {
        return theMeshBase.createArray( len );
    }

    /**
     * The underlying storage.
     */
    protected final StoreMeshBaseSwappingSmartMap theStorage;
}
