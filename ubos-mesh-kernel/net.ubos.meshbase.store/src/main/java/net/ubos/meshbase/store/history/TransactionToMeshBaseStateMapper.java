//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.history;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.transaction.externalized.ChangeListDecoder;
import net.ubos.meshbase.transaction.externalized.ChangeListEncoder;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.meshbase.transaction.externalized.json.ChangeListJsonDecoder;
import net.ubos.meshbase.transaction.externalized.json.ChangeListJsonEncoder;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedDecodingException;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedEncodingException;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedMapper;
import net.ubos.util.logging.Log;

/**
 * Knows how to map MeshBaseState -- and associated ChangeList and MeshBaseView -- to and
 * from StoreWithHistory.
 */
public class TransactionToMeshBaseStateMapper
    implements
        StoreValueWithTimeUpdatedMapper<MeshBaseState,StoreValueWithTimeUpdated>
{
    private static final Log log = Log.getLogInstance( TransactionToMeshBaseStateMapper.class );

    /**
     * Constructor, for subclasses only.
     *
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     */
    public TransactionToMeshBaseStateMapper(
            MeshObjectIdentifierSerializer   idSerializer,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        theIdSerializer   = idSerializer;
        theIdDeserializer = idDeserializer;
    }

    /**
     * Obtain the MeshObjectIdentifierSerializer we use.
     *
     * @return the MeshObjectIdentifierSerializer
     */
    public MeshObjectIdentifierSerializer getMeshObjectIdentifierSerializer()
    {
        return theIdSerializer;
    }

    /**
     * Obttain the MeshObjectIdentifierDeserializer we use.
     *
     * @return the MeshObjectIdentifierDeserializer
     */
    public MeshObjectIdentifierDeserializer getMeshObjectIdentifierDeserializer()
    {
        return theIdDeserializer;
    }

    /**
     * Let us find the history.
     *
     * @param newValue the history
     */
    public void setStoreMeshBaseStateMeshObjectHistory(
            StoreMeshBaseStateMeshObjectHistory newValue )
    {
        if( theStoreMeshBaseStateMeshObjectHistory != null ) {
            throw new IllegalStateException( "Must not set more than once" );
        }
        theStoreMeshBaseStateMeshObjectHistory = newValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDefaultEncodingId()
    {
        return DEFAULT_ENCODER.getEncodingId();
    }

    /**
     * Obtain the ChangeListDecoder with a certain id.
     *
     * @param decoderId the decoderId
     * @return the ChangeListDecoder, or null
     */
    protected ChangeListDecoder getDecoderFor(
            String decoderId )
    {
        for( ChangeListDecoder current : theDecoders ) {
            if( decoderId.equals( current.getEncodingId() )) {
                return current;
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshBaseState decodeValue(
            StoreValueWithTimeUpdated value )
        throws
            StoreValueWithTimeUpdatedDecodingException
    {
        try {
            String encodingId = value.getEncodingId();

            ChangeListDecoder decoder = getDecoderFor( encodingId );
            if( decoder == null ) {
                throw new StoreValueWithTimeUpdatedDecodingException( value, "Unknown encoding ID: " + value.getEncodingId() );
            }

            InputStream stream = value.getDataAsStream();

            List<ExternalizedChange> changes = decoder.decodeChanges(
                    new InputStreamReader( stream, StandardCharsets.UTF_8 ),
                    theIdDeserializer );

            MeshBaseState ret = theStoreMeshBaseStateMeshObjectHistory.recreateMeshBaseState( value.getTimeUpdated(), changes );

            return ret;

        } catch( MeshTypeNotFoundException ex ) {
            throw new StoreValueWithTimeUpdatedDecodingException( value, ex );

        } catch( DecodingException ex ) {
            throw new StoreValueWithTimeUpdatedDecodingException( value, ex );

        } catch( IOException ex ) {
            throw new StoreValueWithTimeUpdatedDecodingException( value, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdated encodeValue(
            String        key,
            MeshBaseState value )
        throws
            StoreValueWithTimeUpdatedEncodingException
    {
        try {
            ChangeListEncoder encoder = DEFAULT_ENCODER;

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            OutputStreamWriter    w   = new OutputStreamWriter( out );

            encoder.writeChangeList( value.getChangeList(), theIdSerializer, true, true, w );

            w.flush();
            w.close();

            return new StoreValueWithTimeUpdated( key, encoder.getEncodingId(), value.getTimeUpdated(), out.toByteArray() );

        } catch( EncodingException ex ) {
            throw new StoreValueWithTimeUpdatedEncodingException( ex );

        } catch( IOException ex ) {
            throw new StoreValueWithTimeUpdatedEncodingException( ex );
        }
    }

    /**
     * Lets us find our MeshBase.
     */
    protected StoreMeshBaseStateMeshObjectHistory theStoreMeshBaseStateMeshObjectHistory;

    /**
     * The object that knows how to serialize MeshObjectIdentifiers.
     */
    protected final MeshObjectIdentifierSerializer theIdSerializer;

    /**
     * The object that knows how to deserialize MeshObjectIdentifiers.
     */
    protected final MeshObjectIdentifierDeserializer theIdDeserializer;

    /**
     * The set of encoders currently known.
     */
    protected static final ChangeListEncoder [] theEncoders = {
            ChangeListJsonEncoder.create()
    };

    /**
     * The set of decoders currently known.
     */
    protected static final ChangeListDecoder [] theDecoders = {
            ChangeListJsonDecoder.create()
    };

    /**
     * The default encoder.
     */
    protected static final ChangeListEncoder DEFAULT_ENCODER = theEncoders[0];
}
