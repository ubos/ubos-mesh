//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.history;

import java.util.HashMap;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.AbstractEditableHistoricMeshBaseView;
import net.ubos.meshbase.a.AMeshBase;
import net.ubos.meshbase.security.ByAccessFilteringCursorIterator;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.GentleSmartMap;
import net.ubos.util.smartmap.SmartMap;

/**
 * A memory-only implementation of historical MeshBaseViews.
 */
public class StoreHistoricMeshBaseView
    extends
        AbstractEditableHistoricMeshBaseView
{
    private static final Log log = Log.getLogInstance( StoreHistoricMeshBaseView.class );

    /**
     * Factory method.
     *
     * @param mb the MeshBaset this MeshBaseView belongs to
     * @return the created object
     */
    public static StoreHistoricMeshBaseView create(
            StoreMeshBase mb )
    {
        SmartMap<MeshObjectIdentifier,MeshObject> cacheCache = new GentleSmartMap.Weak<>( new HashMap<>() ) {
            @Override
            public boolean isPersistent()
            {
                return true;
            }
        };

        StoreHistoricMeshBaseViewRepository cache = new StoreHistoricMeshBaseViewRepository( cacheCache );
        // This contains the modified MeshObjects at this Transaction. plus copies of the not-modified ones.

        StoreHistoricMeshBaseView ret = new StoreHistoricMeshBaseView( cache, mb );
        cache.setMeshBaseView( ret );

        return ret;
    }

    /**
     * Constructor, use factory method.
     *
     * @param repo smartly contains the MeshObjects in this MeshBaseView
     * @param mb the MeshBase this MeshBaseView belongs to
     */
    protected StoreHistoricMeshBaseView(
            StoreHistoricMeshBaseViewRepository repo,
            AMeshBase                           mb )
    {
        super( mb );

        theRepository = repo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getHomeObject()
    {
        MeshObject ret = theRepository.get( getHomeMeshObjectIdentifier() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        boolean ret = theRepository.containsKey( identifier );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject findMeshObjectByIdentifier(
            MeshObjectIdentifier identifier )
    {
        MeshObject ret = theRepository.get( identifier );
        try {
            getMeshBase().getAccessManager().checkPermittedAccess( ret );
            return ret;

        } catch( NotPermittedException ex ) {
            log.debug( ex );
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putNewMeshObject(
            MeshObject toAdd )
    {
        theRepository.put( toAdd.getIdentifier(), toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeMeshObject(
            MeshObjectIdentifier objId )
    {
        theRepository.remove( objId );
    }

    /**
     * Access the underlying repository
     *
     * @return the underlying MHistoricMeshBaseViewCache
     */
    public StoreHistoricMeshBaseViewRepository getMeshObjectStorage()
    {
        return theRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> changedMeshObjectsIterator()
    {
        ByAccessFilteringCursorIterator ret = ByAccessFilteringCursorIterator.create(
                getMeshObjectStorage().changedMeshObjectsIterator(),
                getMeshBase().getAccessManager() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void purgeClonedMeshObjectsCache()
    {
        getMeshObjectStorage().purgeClonedMeshObjectsCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshObject> iterator()
    {
        ByAccessFilteringCursorIterator ret = ByAccessFilteringCursorIterator.create(
                theRepository.valueIterator(),
                getMeshBase().getAccessManager() );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject[] createArray(
            int len )
    {
        return theMeshBase.createArray( len );
    }

    /**
     * The storage for the MeshBaseView.
     */
    protected final StoreHistoricMeshBaseViewRepository theRepository;
}

