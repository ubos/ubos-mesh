//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store;

import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.externalized.ExternalizedMeshObject;
import net.ubos.mesh.externalized.MeshObjectDecoder;
import net.ubos.mesh.externalized.MeshObjectEncoder;
import net.ubos.mesh.externalized.json.MeshObjectJsonDecoder;
import net.ubos.mesh.externalized.json.MeshObjectJsonEncoder;
import net.ubos.mesh.externalized.xml.MeshObjectXmlDecoder;
import net.ubos.mesh.externalized.xml.MeshObjectXmlEncoder;
import net.ubos.meshbase.AbstractEditableMeshBaseView;
import net.ubos.meshbase.AbstractMeshBaseView;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.a.AMeshBase;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.store.StoreValue;
import net.ubos.store.util.StoreValueDecodingException;
import net.ubos.store.util.StoreValueEncodingException;
import net.ubos.store.util.StoreValueMapper;

/**
 * Maps MeshObjects in and out from and to StoreValues.
 */
public class MeshObjectStoreValueMapper
    extends
        AbstractMeshObjectMapper
    implements
        StoreValueMapper<MeshObjectIdentifier,MeshObject,StoreValue>
{
    /**
     * Factory method.
     *
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @return the created object
     */
    public static MeshObjectStoreValueMapper create(
            MeshObjectIdentifierSerializer   idSerializer,
            MeshObjectIdentifierDeserializer idDeserializer )
    {
        return new MeshObjectStoreValueMapper( idSerializer, idDeserializer, null );
    }

    /**
     * Constructor.
     *
     * @param idSerializer knows how to serialize MeshObjectIdentifiers
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     */
    public MeshObjectStoreValueMapper(
            MeshObjectIdentifierSerializer   idSerializer,
            MeshObjectIdentifierDeserializer idDeserializer,
            AbstractEditableMeshBaseView     mbv )
    {
        super( idSerializer, idDeserializer );

        theMeshBaseView = mbv;
    }

    /**
     * Set the MeshBase to which this mapper belongs and in which it instantates MeshObjects
     *
     * @param mbv the MeshBaseView
     */
    public void setMeshBaseView(
            MeshBaseView mbv )
    {
        if( theMeshBaseView != null ) {
            throw new IllegalStateException( "Must not set more than once" );
        }
        theMeshBaseView = (AbstractEditableMeshBaseView) mbv;
    }

    /**
     * Obtain the a copy of this object, but with an alternate MeshBaseView.
     * This lets us easily "reuse" a StoreMeshBase's primary MeshObjectStoreValueMapper for historical MeshBaseViews.
     *
     * @param newMbv the alternate MeshBaseView
     * @return the copy
     */
    public MeshObjectStoreValueMapper withMeshBaseView(
            MeshBaseView newMbv )
    {
        if( newMbv != null ) {
            throw new IllegalStateException( "Cannot use null MeshBaseView" );
        }
        if( theMeshBaseView == newMbv ) {
            throw new IllegalStateException( "New MeshBaseView is the old MeshBaseView" );
        }
        if( theMeshBaseView.getMeshBase() != newMbv.getMeshBase() ) {
            throw new IllegalStateException( "MeshBaseView in wrong MeshBase" );
        }
        return new MeshObjectStoreValueMapper( theIdSerializer, theIdDeserializer, (AbstractEditableMeshBaseView) newMbv );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDefaultEncodingId()
    {
        return DEFAULT_ENCODER.getEncodingId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MeshObjectEncoder getPreferredEncoder()
    {
        return DEFAULT_ENCODER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MeshObjectDecoder getDecoderFor(
            String encoderId )
    {
        for( MeshObjectDecoder current : theDecoders ) {
            if( encoderId.equals( current.getEncodingId() )) {
                return current;
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject decodeValue(
            StoreValue value )
        throws
            StoreValueDecodingException
    {
        try {
            ExternalizedMeshObject externalized = deserializeValue( value );
            MeshObject ret = ((AMeshBase)theMeshBaseView.getMeshBase()).recreateMeshObject( externalized, theMeshBaseView );

            return ret;

        } catch( DecodingException ex ) {
            throw new StoreValueDecodingException( value, ex );

        } catch( IOException ex ) {
            throw new StoreValueDecodingException( value, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue encodeValue(
            MeshObjectIdentifier key,
            MeshObject           value )
        throws
            StoreValueEncodingException
    {
        try {
            MeshObjectEncoder encoder = getPreferredEncoder();
            byte [] serialized = serializeValue( value, encoder );
            return new StoreValue( keyToString( key ), encoder.getEncodingId(), serialized );

        } catch( EncodingException ex ) {
            throw new StoreValueEncodingException( ex );

        } catch( IOException ex ) {
            throw new StoreValueEncodingException( ex );
        }
    }

    /**
     * The MeshBaseView that this mapper belongs to.
     */
    protected AbstractEditableMeshBaseView theMeshBaseView;

    /**
     * The set of encoders currently known.
     */
    protected static final MeshObjectEncoder[] theEncoders = {
            MeshObjectXmlEncoder.create(),
            MeshObjectJsonEncoder.create()
    };

    /**
     * The set of decoders currently known.
     */
    protected static final MeshObjectDecoder[] theDecoders = {
            MeshObjectXmlDecoder.create(),
            MeshObjectJsonDecoder.create()
    };

    /**
     * The default encoder.
     */
    protected static final MeshObjectEncoder DEFAULT_ENCODER = theEncoders[0];
}
