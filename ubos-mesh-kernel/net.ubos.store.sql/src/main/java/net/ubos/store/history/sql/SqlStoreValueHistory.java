//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.sql;

import java.io.IOException;
import java.util.List;
import net.ubos.store.history.AbstractStoreValueWithTimeUpdatedHistory;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreWithHistoryEntryDoesNotExistException;
import net.ubos.util.Pair;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.history.AbstractFetchManyHistoryCursorIterator;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;

/**
 * SQL implementation of StoreValueHistory
 *
 * @param <T> the type of thing for which this is a history
 */
public class SqlStoreValueHistory<T extends StoreValueWithTimeUpdated>
    extends
        AbstractStoreValueWithTimeUpdatedHistory<T>
{
    private static final Log log = Log.getLogInstance( SqlStoreValueHistory.class );

    /**
     * Constructor.
     *
     * @param key the key for the StoreValues whose history this is
     * @param historyStore the HistoryStore we belong to
     */
    public SqlStoreValueHistory(
            String                         key,
            AbstractSqlStoreWithHistory<T> historyStore )
    {
        super( key );

        theHistoryStore = historyStore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<T> iterator()
    {
        return new MyCursorIterator<>( this, 0 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPersistent()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        try {
            theHistoryStore.deleteAllKey( theKey );
        } catch( IOException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     *
     * This is here, instead of the superclass, for easier breakpoint setting/debugging
     */
    @Override
    public void putOrThrow(
            T toAdd )
        throws
            IllegalArgumentException
    {
        T already = at( toAdd.getTimeUpdated() );
        if( already != null ) {
            if( already == toAdd ) {
                log.warn( "Re-putting same element:", already, new Throwable() );
            } else {
                throw new IllegalArgumentException( "Element exists already at time " + toAdd.getTimeUpdated() );
            }
        }
        putIgnorePrevious( toAdd );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putIgnorePrevious(
            T toAdd )
    {
        if( !theKey.equals( toAdd.getKey() )) {
            throw new IllegalArgumentException();
        }

        try {
            theHistoryStore.putOrUpdate( toAdd );
        } catch( IOException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeIgnorePrevious(
            long t )
    {
        try {
            theHistoryStore.delete( theKey, t );

        } catch( StoreWithHistoryEntryDoesNotExistException ex ) {
            // no op
        } catch( IOException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T at(
            long t )
    {
        try {
            T ret = theHistoryStore.get( theKey, t );
            return ret;

        } catch( StoreWithHistoryEntryDoesNotExistException ex ) {
            return null;

        } catch( IOException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * The HistoryStore that we belong to.
     */
    protected final AbstractSqlStoreWithHistory<T> theHistoryStore;

    /**
     * Iterator over the individual StoreValues in a particular SqlStoreValueHistory with a particular key.
     * We'll keep this as an inner class so the naming is less confusing.
     * Compare to SqlStoreValueHistoryCursorIterator, which iterates over the various histories
     * (each of which has a different key, and potentially many StoreValues itself).
     */
    static class MyCursorIterator<T extends StoreValueWithTimeUpdated>
        extends
            AbstractFetchManyHistoryCursorIterator<T>
    {
        /**
         * Constructor.
         *
         * @param history the history we iterate over
         * @param timeUpdatedPosition the location of the cursor
         */
        @SuppressWarnings("unchecked")
        public MyCursorIterator(
                SqlStoreValueHistory<T> history,
                long                    timeUpdatedPosition )
        {
            super( history,
                   (T one, T two ) -> one.getKey().equals( two.getKey() ) && one.getTimeUpdated() == two.getTimeUpdated());

            theTimeUpdatedPosition = timeUpdatedPosition;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<T> next(
                int n )
        {
            SqlStoreValueHistory<T>    history      = (SqlStoreValueHistory<T>) theHistory;
            AbstractSqlStoreWithHistory<T> historyStore = history.theHistoryStore;

            // we have to go forward by one more, so we can set the new position right after what's returned here
            List<T> found = historyStore.findNextValuesIncluding( history.getKey(), theTimeUpdatedPosition, n+1 );
            List<T> ret   = found.subList( 0, Math.min( n, found.size() ));

            if( found.size() == n+1 ) {
                theTimeUpdatedPosition = found.get( found.size()-1 ).getTimeUpdated();
            } else {
                moveToAfterLast();
            }
            return ret;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<T> previous(
                int n )
        {
            SqlStoreValueHistory<T>    history      = (SqlStoreValueHistory<T>) theHistory;
            AbstractSqlStoreWithHistory<T> historyStore = history.theHistoryStore;

            List<T> found = historyStore.findPreviousValuesExcluding( history.getKey(), theTimeUpdatedPosition, n );
            if( found.size() > 0 ) {
                theTimeUpdatedPosition = found.get( found.size()-1 ).getTimeUpdated();
            } else{
                moveToBeforeFirst();
            }
            return found;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToBeforeFirst()
        {
            return moveToJustBeforeTime( 0 );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int moveToAfterLast()
        {
            return moveToJustBeforeTime( Long.MAX_VALUE );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setPositionTo(
                CursorIterator<T> position )
            throws
                IllegalArgumentException
        {
            if( !( position instanceof MyCursorIterator ) || theHistory != ((MyCursorIterator) position).theHistory ) {
                throw new IllegalArgumentException( "Incompatible CursorIterators" );
            }
            theTimeUpdatedPosition = ((MyCursorIterator) position).theTimeUpdatedPosition;
        }

        @Override
        public int moveToJustBeforeTime(
                long time )
        {
            SqlStoreValueHistory<T>    history      = (SqlStoreValueHistory<T>) theHistory;
            AbstractSqlStoreWithHistory<T> historyStore = history.theHistoryStore;

            Pair<Integer,Long> found = historyStore.determineMoveToJustBeforeTime( history.getKey(), theTimeUpdatedPosition, time );

            theTimeUpdatedPosition = found.getValue();

            return found.getName();
        }

        @Override
        public int moveToJustAfterTime(
                long time )
        {
            SqlStoreValueHistory<T>    history      = (SqlStoreValueHistory<T>) theHistory;
            AbstractSqlStoreWithHistory<T> historyStore = history.theHistoryStore;

            Pair<Integer,Long> found = historyStore.determineMoveToJustAfterTime( history.getKey(), theTimeUpdatedPosition, time );

            theTimeUpdatedPosition = found.getValue();

            return found.getName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MyCursorIterator<T> createCopy()
        {
            return new MyCursorIterator<>( (SqlStoreValueHistory<T>) theHistory, theTimeUpdatedPosition );
        }

        /**
         * Location in the history.
         */
        protected long theTimeUpdatedPosition;
    }
}
