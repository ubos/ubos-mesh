//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.sql;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.store.AbstractKeyBasedStoreValueCursorIterator;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;

/**
 * Iterator implementation for the StoreValues in an AbstractSqlStore.
 *
 * @param <T> follows the AbstractSqlStore parameterization
 */
public class SqlStoreValueCursorIterator<T extends StoreValue>
        extends
            AbstractKeyBasedStoreValueCursorIterator<T>
{
    private static final Log log = Log.getLogInstance(SqlStoreValueCursorIterator.class ); // our own, private logger

    /**
     * Constructor. Start at a defined place.
     *
     * @param position the key of the current position
     * @param pattern the pattern to filter by
     * @param store the AbstractSqlStore to iterate over
     */
    public SqlStoreValueCursorIterator(
            String              position,
            String              pattern,
            AbstractSqlStore<T> store )
    {
        super( store, position );

        theStartsWith = pattern;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractSqlStore<T> getStore()
    {
        return (AbstractSqlStore<T>) theStore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<T> findNextIncluding(
            String key,
            int    n )
    {
        List<T> ret = getStore().findNextIncluding( key, n, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<String> findNextKeyIncluding(
            String key,
            int    n )
    {
        List<String> ret = getStore().findNextKeyIncluding( key, n, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<T> findPreviousExcluding(
            String key,
            int    n )
    {
        List<T> ret = getStore().findPreviousExcluding( key, n, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<String> findPreviousKeyExcluding(
            String key,
            int    n )
    {
        List<String> ret = getStore().findPreviousKeyExcluding( key, n, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasNextIncluding(
            String key )
    {
        int ret = getStore().hasNextIncluding( key, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasPreviousExcluding(
            String key )
    {
        int ret = getStore().hasPreviousExcluding( key, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String findKeyAt(
            String key,
            int    delta )
        throws
            NoSuchElementException
    {
        String ret = getStore().findKeyAt( key, delta, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int determineDistance(
            String from,
            String to )
    {
        int ret = getStore().determineDistance( from, to, theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getBeforeFirstPosition()
        throws
            NoSuchElementException
    {
        String ret = getStore().findFirstKey( theStartsWith );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getAfterLastPosition()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SqlStoreValueCursorIterator<T> createCopy()
    {
        return new SqlStoreValueCursorIterator<>( thePosition, theStartsWith, (AbstractSqlStore<T>) theStore );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
    {
        try {
            theStore.delete( thePosition );

            // we don't need to adjust the position
        } catch( StoreKeyDoesNotExistException ex ) {
            log.error( ex );
        } catch( IOException ex ) {
            log.error( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<T> position )
        throws
            ClassCastException
    {
        if( !( position instanceof SqlStoreValueCursorIterator )) {
            throw new ClassCastException( "Wrong type of CursorIterator: " + position );
        }
        SqlStoreValueCursorIterator realPosition = (SqlStoreValueCursorIterator) position;

        if( theStore != realPosition.theStore ) {
            throw new IllegalArgumentException( "Not the same instance of Store" );
        }

        thePosition = realPosition.thePosition;
    }

    /**
     * Only return those elements whose keys match this pattern. The syntax of this
     * pattern is defined by the subclass where it is used.
     */
    protected final String theStartsWith;
}
