//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.sql;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.store.history.AbstractStoreWithHistory;
import net.ubos.store.history.StoreWithHistoryKeyDoesNotExistException;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.util.Pair;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.util.sql.SqlDatabase;
import net.ubos.store.history.StoreValueWithTimeUpdatedHistory;

/**
 * SQL implementation of the HistoryStore interface.This needs to be subclassed to
 * support specific database engines.
 * @param <T> allows implementations to store more than StoreValueWithTimeUpdated
 */
public abstract class AbstractSqlStoreWithHistory<T extends StoreValueWithTimeUpdated>
        extends
            AbstractStoreWithHistory<T>
        implements
            CanBeDumped
{
    private final static Log log = Log.getLogInstance( AbstractSqlStoreWithHistory.class ); // our own, private logger

    /**
     * Constructor for subclasses only.
     *
     * @param db the Database object to delegate to
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     */
    protected AbstractSqlStoreWithHistory(
            SqlDatabase db,
            String      tableName )
    {
        theDatabase  = db;
        theTableName = tableName;
    }

    /**
     * Obtain the Database that this AbstractSqlStore works on.
     *
     * @return the Database
     */
    public SqlDatabase getDatabase()
    {
        return theDatabase;
    }

    /**
     * Obtain the name of the table in the SQL DataSource in which the data will be stored
     * @return
     */
    public String getTableName()
    {
        return theTableName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeHard()
            throws
                IOException
    {
        dropTables();
        createTables();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void initializeIfNecessary()
            throws
                IOException
    {
        if( !hasTables() ) {
            createTables();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll()
        throws
            IOException
    {
        return deleteAll( "" );
    }

    /**
     * Determine whether the SqlStore has the SQL tables it needs.
     *
     * @return true if the Store yhas the SQL tables it needs
     */
    protected abstract boolean hasTables();

    /**
     * Drop all tables that this SqlStore needs. Do nothing if there are none.
     */
    protected abstract void dropTables();

    /**
     * Create all tables that this SqlStore needs.
     *
     * @throws IOException thrown if creating the tables was not possible
     */
    protected abstract void createTables()
            throws
                IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public SqlStoreValueHistory<T> history(
            String key )
        throws
            StoreWithHistoryKeyDoesNotExistException
    {
        long timeUpdated = getTimeUpdatedBeforeFirstPositionInHistory( key ); // throws exception if key does not exist

        return new SqlStoreValueHistory<>( key, this );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<StoreValueWithTimeUpdatedHistory<T>> iterator()
    {
        return iterator( "" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<StoreValueWithTimeUpdatedHistory<T>> iterator(
            String startsWith )
    {
        String pattern = startsWith + "%";
        try {
            if( isEmpty( startsWith ) ) {
                return new SqlStoreValueHistoryCursorIterator<>( null, pattern, this ); // past last position
            } else {
                return new SqlStoreValueHistoryCursorIterator<>( findFirstKey( pattern ), pattern, this );
            }
        } catch( IOException ex ) {
            log.error( ex );
            return new SqlStoreValueHistoryCursorIterator<>( null, pattern, this ); // gotta be somewhere
        }
    }

    /**
     * Find the very first key.
     *
     * @return the first key
     * @param pattern the pattern to filter by
     * @throws NoSuchElementException thrown if the Store is empty
     */
    protected abstract String findFirstKey(
            String pattern )
        throws
            NoSuchElementException;

    /**
     * Find the key N rows up or down from the current key.
     *
     * @param key the current key
     * @param delta the number of rows up (positive) or down (negative)
     * @param pattern the pattern to filter by
     * @return the found key, or null
     * @throws NoSuchElementException thrown if the delta went beyond the "after last" or "before first" element
     */
    protected abstract String findKeyAt(
            String key,
            int    delta,
            String pattern )
        throws
            NoSuchElementException;

    /**
     * Find the next n keys, including key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key the first key
     * @param n the number of keys to find
     * @param pattern the pattern to filter by
     * @return the found keys
     */
    protected List<String> findNextKeyIncluding(
            String key,
            int    n,
            String pattern )
    {
        // FIXME, this can be made more efficient
        List<StoreValueWithTimeUpdatedHistory<T>> values = findNextHistoriesIncluding( key, n, pattern );
        List<String>                              ret    = new ArrayList<>( values.size() );

        for( StoreValueWithTimeUpdatedHistory<T> current : values ) {
            ret.add( current.getKey() );
        }
        return ret;
    }

    /**
     * Find the next n StoreValueHistories, including the StoreValueHistory for key.
     * This method will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValueWithTimeUpdatedHistory
     * @param n the number of StoreValueHistories to find
     * @param pattern the pattern to filter by
     * @return the found StoreValueHistories
     */
    protected abstract List<StoreValueWithTimeUpdatedHistory<T>> findNextHistoriesIncluding(
            String key,
            int    n,
            String pattern );

    /**
     * Find the previous n keys, excluding the key for key.
     * This method will return fewer values if only fewer values could be found.
     *
     * @param key the first key NOT to be returned
     * @param n the number of keys to find
     * @param pattern the pattern to filter by
     * @return the found keys
     */
    protected List<String> findPreviousKeyExcluding(
            String key,
            int    n,
            String pattern )
    {
        // FIXME, this can be made more efficient
        List<StoreValueWithTimeUpdatedHistory<T>> values = findPreviousHistoriesExcluding( key, n, pattern );
        List<String>                              ret    = new ArrayList<>();

        for( StoreValueWithTimeUpdatedHistory<T> current : values ) {
            ret.add( current.getKey() );
        }
        return ret;
    }

    /**
     * Find the previous n StoreValueHistories, excluding the StoreValueHistory for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValue NOT to be returned
     * @param n the number of StoreValues to find
     * @param pattern the pattern to filter by
     * @return the found StoreValues
     */
    protected abstract List<StoreValueWithTimeUpdatedHistory<T>> findPreviousHistoriesExcluding(
            String key,
            int    n,
            String pattern );

    /**
     * Count the number of keys following and including the one with the key.
     *
     * @param key the key
     * @param pattern the pattern to filter by
     * @return the number of rows
     */
    protected abstract int hasNextKeyIncluding(
            String key,
            String pattern );

    /**
     * Count the number of keys preceding and excluding the one with the key.
     *
     * @param key the key
     * @param pattern the pattern to filter by
     * @return the number of rows
     */
    protected abstract int hasPreviousKeyExcluding(
            String key,
            String pattern );

    /**
     * Determine the number of keys between two keys.
     *
     * @param from the start key
     * @param to the end key
     * @param pattern the pattern to filter by
     * @return the distance
     */
    protected abstract int determineKeyDistance(
            String from,
            String to,
            String pattern );

    /**
     * Find the next n StoreValues in the history for this key, including the StoreValue for this timeUpdated.
     * This method will return fewer values if only fewer values could be found.
     *
     * @param key the key identifying the history
     * @param timeUpdated the timeUpdated for the first StoreValue
     * @param n the number of StoreValues to find
     * @return the found StoreValues
     */
    protected abstract List<T> findNextValuesIncluding(
            String key,
            long   timeUpdated,
            int    n );

    /**
     * Find the previous n StoreValues in the history for this key, excluding the SToreValue for this timeUpdated.
     * This method will return fewer values if only fewer values could be found.
     *
     * @param key the key identifying the history
     * @param timeUpdated the timeUpdated beyond the first StoreValue
     * @param n the number of StoreValues to find
     * @return the found StoreValues
     */
    protected abstract List<T> findPreviousValuesExcluding(
            String key,
            long   timeUpdated,
            int    n );

    /**
     * Determine the number of steps, and the timeUpdated if we were to traverse from
     * the "from" time stamp to the just-before "to" time (if the "to" timestamp exists)
     * or to just before the next timestamp after the "to" timestamp that exists.
     *
     * @param key the key identifying the history
     * @param from the timeUpdated from the start
     * @param to the timeUpdated at the end
     * @return name is the distance (which may be zero or negative), the value is
     *         the timeUpdate at the end
     */
    protected abstract Pair<Integer,Long> determineMoveToJustBeforeTime(
            String key,
            long   from,
            long   to );

    /**
     * Determine the number of steps, and the timeUpdated if we were to traverse from
     * the "from" time stamp to the next timeUpdated after the "to" time.
     *
     * @param key the key identifying the history
     * @param from the timeUpdated from the start
     * @param to the timeUpdated at the end
     * @return name is the distance (which may be zero or negative), the value is
     *         the timeUpdate at the end
     */
    protected abstract Pair<Integer,Long> determineMoveToJustAfterTime(
            String key,
            long   from,
            long   to );

//    /**
//     * Determine the number of StoreValues in the history for this key before this timeUpdated.
//     *
//     * @param key the key identifying the history
//     * @param from the start timeUpdated
//     * @return name is the distance (which is zero or negative) and value is the timeUpdated of
//     *         the first element (this may be Long.MAX_VALUE if the history is empty)
//     */
//    protected abstract Pair<Integer,Long> determineMoveToJustBeforeFirst(
//            String key,
//            long   from );
//
//    /**
//     * Determine the number of StoreValues in the history for this key between two timeUpdated.
//     *
//     * @param key the key identifying the history
//     * @param from the start timeUpdated (inclusive)
//     * @param to the end timeUpdated (inclusive)
//     * @return name is the distance (which may be negative) and value is the "to" value adjusted
//     *         to the timeUpdated of the actual element (this may be Long.MAX_VALUE if end)
//     */
//    protected abstract Pair<Integer,Long> determineMoveToOrJustAfterTime(
//            String key,
//            long   from,
//            long   to );
//
//    /**
//     * Determine the number of StoreValues in the history for this key between two timeUpdated.
//     *
//     * @param key the key identifying the history
//     * @param from the start timeUpdated (inclusive)
//     * @param to the end timeUpdated (exclusive)
//     * @return name is the distance (which may be negative) and value is the to value adjusted
//     *         to the timeUpdated of the actual element
//     */
//    protected abstract Pair<Integer,Long> determineMoveToJustBeforeTime(
//            String key,
//            long   from,
//            long   to );

    /**
     * Determine the timeUpdated at the very beginning of the history for this key.
     * If the key does not exist, throw an exception.
     *
     * @param key the key identifying the history
     * @return the timeUpdated
     * @throws StoreWithHistoryKeyDoesNotExistException thrown if the key does not exist
     */
    protected abstract long getTimeUpdatedBeforeFirstPositionInHistory(
            String key )
        throws
            StoreWithHistoryKeyDoesNotExistException;

//    /**
//     * Determine the timeUpdated after the very end of the history for this key.
//     * If the key does not exist, throw an exception.
//     *
//     * @param key the key identifying the history
//     * @return the timeUpdated
//     * @throws StoreWithHistoryKeyDoesNotExistException thrown if the key does not exist
//     */
//    protected abstract long getTimeUpdatedAfterLastPositionInHistory(
//            String key );
//
    /**
     * Helper method to convert the SQL values back into System.currentTimeMillis() format.
     *
     * @param stamp the SQL timestamp
     * @param millis milli-seconds to be added to the SQL timestamp
     * @return Java time
     */
    protected static long reconstructTime(
            Timestamp stamp,
            int       millis )
    {
        if( stamp == null ) {
            return -1L;
        }
        long ret = ( stamp.getTime() / 1000 ) * 1000 + millis;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
        throws
            IOException
    {
        return size( "" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "database",
                    "tableName"
                },
                new Object[] {
                    theDatabase,
                    theTableName
                });
    }

    /**
     * The Database that this AbstractSqlStore stores data in.
     */
    protected final SqlDatabase theDatabase;

    /**
     * Name of the table in the JDBC data source. While we don't need this, it is
     * very useful in debugging.
     */
    protected final String theTableName;
}
