//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.sql;

import java.io.IOException;
import java.sql.SQLException;
import net.ubos.util.exception.LocalizedException;

/**
 * <p>An <code>IOException</code> that delegates to a <code>SQLException</code>.</p>
 */
public class SqlHistoryStoreIOException
        extends
            IOException
        implements
            LocalizedException
{
    /**
     * Constructor.
     *
     * @param store the SqlHistoryStore that threw the exception
     * @param operation name of the operation, such as "put"
     * @param cause the cause
     */
    public SqlHistoryStoreIOException(
            AbstractSqlStoreWithHistory<?> store,
            String                     operation,
            SQLException               cause )
    {
        this( store, operation, null, 0L, null, null, cause );
    }

    /**
     * Constructor.
     *
     * @param store the SqlHistoryStore that threw the exception
     * @param operation name of the operation, such as "put"
     * @param key key of the data element involved in the operation, if any
     * @param timeUpdated the timeUpdated of the data element involved in the operation, if any
     * @param cause the cause
     */
    public SqlHistoryStoreIOException(
            AbstractSqlStoreWithHistory<?> store,
            String                     operation,
            String                     key,
            long                       timeUpdated,
            SQLException               cause )
    {
        this( store, operation, key, timeUpdated, null, null, cause );
    }

    /**
     * Constructor.
     *
     * @param store the SqlHistoryStore that threw the exception
     * @param operation name of the operation, such as "put"
     * @param key key of the data element involved in the operation, if any
     * @param timeUpdated the timeUpdated of the data element involved in the operation, if any
     * @param encodingId the id of the encoding that was used to encode the data element, if any
     * @param data the data element, expressed as a sequence of bytes, if any
     * @param cause the cause
     */
    public SqlHistoryStoreIOException(
            AbstractSqlStoreWithHistory<?> store,
            String                     operation,
            String                     key,
            long                       timeUpdated,
            String                     encodingId,
            byte []                    data,
            SQLException               cause )
    {
        super( "SQL Exception", cause );

        theStore       = store;
        theOperation   = operation;
        theKey         = key;
        theTimeUpdated = timeUpdated;
        theEncodingId  = encodingId;
        theData        = data;
    }

    /**
     * Obtain the underlying cause which we know to be a SQLException.
     *
     * @return the cause
     */
    @Override
    public SQLException getCause()
    {
        return (SQLException) super.getCause();
    }

    /**
     * Obtain the AbstractSqlStoreWithHistory that threw the Exception.
     *
     * @return the AbstractSqlStoreWithHistory
     */
    public AbstractSqlStoreWithHistory<?> getStore()
    {
        return theStore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
                theOperation,
                theKey,
                theTimeUpdated,
                theEncodingId,
                theData,
                getCause().getMessage(),
                getCause().getLocalizedMessage() };
    }

    /**
     * The AbstractSqlStoreWithHistory that threw this exception.
     */
    protected AbstractSqlStoreWithHistory<?> theStore;

    /**
     * The name of the operation that produced this exception.
     */
    protected String theOperation;

    /**
     * The key of the data element used in the operation, if any.
     */
    protected String theKey;

    /**
     * The timeUpdated of the data element used in the operation, if any.
     */
    protected long theTimeUpdated;

    /**
     * The encoding ID of that data element used in the operation, if any.
     */
    protected String theEncodingId;

    /**
     * The data element, expressed as a sequence of bytes, if any.
     */
    protected byte [] theData;
}
