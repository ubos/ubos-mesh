//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.sql;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.store.AbstractStore;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.util.sql.SqlDatabase;
import net.ubos.store.StoreValueCursorIterator;

/**
 * SQL implementation of the Store interface. This needs to be subclassed to
 * support specific database engines.
 *
 * @param <T> Can store more than just StoreValue
 */
public abstract class AbstractSqlStore<T extends StoreValue>
        extends
            AbstractStore<T>
        implements
            CanBeDumped
{
    private final static Log log = Log.getLogInstance( AbstractSqlStore.class ); // our own, private logger

    /**
     * Constructor for subclasses only.
     *
     * @param db the Database object to delegate to
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     */
    protected AbstractSqlStore(
            SqlDatabase db,
            String      tableName )
    {
        theDatabase  = db;
        theTableName = tableName;
    }

    /**
     * Obtain the Database that this AbstractSqlStore works on.
     *
     * @return the Database
     */
    public SqlDatabase getDatabase()
    {
        return theDatabase;
    }

    /**
     * Obtain the name of the table in the SQL DataSource in which the data will be stored
     * @return
     */
    public String getTableName()
    {
        return theTableName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeHard()
            throws
                IOException
    {
        dropTables();
        createTables();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void initializeIfNecessary()
            throws
                IOException
    {
        if( !hasTables() ) {
            createTables();
        }
    }

    /**
     * Determine whether the SqlStore has the SQL tables it needs.
     *
     * @return true if the Store has the SQL tables it needs
     */
    protected abstract boolean hasTables();

    /**
     * Drop all tables that this SqlStore needs. Do nothing if there are none.
     */
    protected abstract void dropTables();

    /**
     * Create all tables that this SqlStore needs.
     *
     * @throws IOException thrown if creating the tables was not possible
     */
    protected abstract void createTables()
            throws
                IOException;

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueCursorIterator<T> iterator()
    {
        return iterator( "" );
    }

    /**
     * Find the very first key.
     *
     * @return the first key
     * @param pattern the pattern to filter by
     * @throws NoSuchElementException thrown if the Store is empty
     */
    protected abstract String findFirstKey(
            String pattern )
        throws
            NoSuchElementException;

    /**
     * Find the key N rows up or down from the current key.
     *
     * @param key the current key
     * @param delta the number of rows up (positive) or down (negative)
     * @param pattern the pattern to filter by
     * @return the found key, or null
     * @throws NoSuchElementException thrown if the delta went beyond the "after last" or "before first" element
     */
    protected abstract String findKeyAt(
            String key,
            int    delta,
            String pattern )
        throws
            NoSuchElementException;

    /**
     * Find the next n keys, including key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key the first key
     * @param n the number of keys to find
     * @param pattern the pattern to filter by
     * @return the found keys
     */
    protected List<String> findNextKeyIncluding(
            String key,
            int    n,
            String pattern )
    {
        // FIXME, this can be made more efficient
        List<T>      values = findNextIncluding( key, n, pattern );
        List<String> ret    = new ArrayList<>( values.size());

        for( T current : values ) {
            ret.add( current.getKey() );
        }
        return ret;
    }

    /**
     * Find the next n StoreValues, including the StoreValue for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValue
     * @param n the number of StoreValues to find
     * @param pattern the pattern to filter by
     * @return the found StoreValues
     */
    protected abstract List<T> findNextIncluding(
            String key,
            int    n,
            String pattern );

    /**
     * Find the previous n keys, excluding the key for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key the first key NOT to be returned
     * @param n the number of keys to find
     * @param pattern the pattern to filter by
     * @return the found keys
     */
    protected List<String> findPreviousKeyExcluding(
            String key,
            int    n,
            String pattern )
    {
        // FIXME, this can be made more efficient
        List<T>      values = findPreviousExcluding( key, n, pattern );
        List<String> ret    = new ArrayList<>( values.size() );

        for( T current : values ) {
            ret.add( current.getKey() );
        }
        return ret;
    }

    /**
     * Find the previous n StoreValues, excluding the StoreValue for key. This method
     * will return fewer values if only fewer values could be found.
     *
     * @param key key for the first StoreValue NOT to be returned
     * @param n the number of StoreValues to find
     * @param pattern the pattern to filter by
     * @return the found StoreValues
     */
    protected abstract List<T> findPreviousExcluding(
            String key,
            int    n,
            String pattern );

    /**
     * Count the number of rows following and including the one with the key.
     *
     * @param key the key
     * @param pattern the pattern to filter by
     * @return the number of rows
     */
    protected abstract int hasNextIncluding(
            String key,
            String pattern );

    /**
     * Count the number of rows preceding and excluding the one with the key.
     *
     * @param key the key
     * @param pattern the pattern to filter by
     * @return the number of rows
     */
    protected abstract int hasPreviousExcluding(
            String key,
            String pattern );

    /**
     * Determine the number of rows from one key to another.
     *
     * @param from the start key
     * @param to the end key
     * @param pattern the pattern to filter by
     * @return the distance
     */
    protected abstract int determineDistance(
            String from,
            String to,
            String pattern );

    /**
     * Helper method to convert the SQL values back into System.currentTimeMillis() format.
     *
     * @param stamp the SQL timestamp
     * @param millis milli-seconds to be added to the SQL timestamp
     * @return Java time
     */
    protected static long reconstructTime(
            Timestamp stamp,
            int       millis )
    {
        if( stamp == null ) {
            return -1L;
        }
        long ret = ( stamp.getTime() / 1000 ) * 1000 + millis;
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "database",
                    "tableName"
                },
                new Object[] {
                    theDatabase,
                    theTableName
                });
    }

    /**
     * The Database that this AbstractSqlStore stores data in.
     */
    protected SqlDatabase theDatabase;

    /**
     * Name of the table in the JDBC data source. While we don't need this, it is
     * very useful in debugging.
     */
    protected String theTableName;
}
