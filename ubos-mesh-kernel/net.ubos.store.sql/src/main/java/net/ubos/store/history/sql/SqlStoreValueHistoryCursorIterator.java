//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.history.sql;

import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.store.history.AbstractKeyBasedHistoryStoreValueHistoryCursorIterator;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.store.history.StoreValueWithTimeUpdatedHistory;

/**
 * Iterates over all histories in an AbstractSqlHistoryStore.
 * Compare with SqlStoreValueHistory$MyCursorIterator which iterates over
 * the elements within one history.
 *
 * @param <T> parameterization of the HistoryStore
 */
public class SqlStoreValueHistoryCursorIterator<T extends StoreValueWithTimeUpdated>
    extends
        AbstractKeyBasedHistoryStoreValueHistoryCursorIterator<T>
    implements
        CursorIterator<StoreValueWithTimeUpdatedHistory<T>>
{
    /**
     * Constructor. Start at a defined place.
     *
     * @param position the key for the current position.
     * @param startsWith only return those elements whose keys starts with this string
     * @param historyStore the AbstractSqlStoreWithHistory to iterate over
     */
    protected SqlStoreValueHistoryCursorIterator(
            String                     position,
            String                     startsWith,
            AbstractSqlStoreWithHistory<T> historyStore )
    {
        super( position, startsWith );

        theHistoryStore = historyStore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValueWithTimeUpdatedHistory<T>> findNextIncluding(
            String key,
            int    n,
            String pattern )
    {
        List<StoreValueWithTimeUpdatedHistory<T>> ret = theHistoryStore.findNextHistoriesIncluding( key, n, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<String> findNextKeyIncluding(
            String key,
            int    n,
            String pattern )
    {
        List<String> ret = theHistoryStore.findNextKeyIncluding( key, n, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValueWithTimeUpdatedHistory<T>> findPreviousExcluding(
            String key,
            int    n,
            String pattern )
    {
        List<StoreValueWithTimeUpdatedHistory<T>> ret = theHistoryStore.findPreviousHistoriesExcluding( key, n, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<String> findPreviousKeyExcluding(
            String key,
            int    n,
            String pattern )
    {
        List<String> ret = theHistoryStore.findPreviousKeyExcluding( key, n, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasNextIncluding(
            String key,
            String pattern )
    {
        int ret = theHistoryStore.hasNextKeyIncluding( key, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasPreviousExcluding(
            String key,
            String pattern )
    {
        int ret = theHistoryStore.hasPreviousKeyExcluding( key, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String findKeyAt(
            String key,
            int    delta,
            String pattern )
        throws
            NoSuchElementException
    {
        String ret = theHistoryStore.findKeyAt( key, delta, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int determineDistance(
            String from,
            String to,
            String pattern )
    {
        int ret = theHistoryStore.determineKeyDistance( from, to, pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getBeforeFirstPosition(
            String pattern )
        throws
            NoSuchElementException
    {
        String ret = theHistoryStore.findFirstKey( pattern );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getAfterLastPosition(
            String pattern )
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
    {
        throw new UnsupportedOperationException( "Cannot remove elements from a HistoryStore" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<StoreValueWithTimeUpdatedHistory<T>> position )
        throws
            IllegalArgumentException
    {
        if(    !( position instanceof SqlStoreValueHistoryCursorIterator )
            || theHistoryStore != ((SqlStoreValueHistoryCursorIterator) position).theHistoryStore
            || !theStartsWith.equals( ((SqlStoreValueHistoryCursorIterator) position).theStartsWith ))
        {
            throw new IllegalArgumentException( "Incompatible CursorIterators" );
        }
        thePosition = ((SqlStoreValueHistoryCursorIterator) position).thePosition;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SqlStoreValueHistoryCursorIterator<T> createCopy()
    {
        return new SqlStoreValueHistoryCursorIterator<>( thePosition, theStartsWith, theHistoryStore );
    }

    /**
     * The HistoryStore over whose StoreValueHistories we iterate.
     */
    protected final AbstractSqlStoreWithHistory<T> theHistoryStore;
}
