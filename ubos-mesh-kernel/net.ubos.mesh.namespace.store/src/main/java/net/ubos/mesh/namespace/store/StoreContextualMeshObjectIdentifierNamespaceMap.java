//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace.store;

import net.ubos.mesh.namespace.AbstractContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.store.Store;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.token.UniqueStringGenerator;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.store.StoreValue;
import net.ubos.store.prefixing.PrefixingStore;
import net.ubos.store.util.StoreBackedSwappingSmartMap;
import net.ubos.util.smartmap.SwappingSmartMap;
import net.ubos.util.smartmap.SwappingSmartMapListener;

/**
 * Store implementation.
 */
public class StoreContextualMeshObjectIdentifierNamespaceMap
    extends
        AbstractContextualMeshObjectIdentifierNamespaceMap
{
    /**
     * Factory.
     *
     * @param store where to store local information
     * @param delegate the delegate map
     * @return the created map
     */
    public static StoreContextualMeshObjectIdentifierNamespaceMap create(
            Store<StoreValue>                       store,
            PrimaryMeshObjectIdentifierNamespaceMap delegate )
    {
        StringToStringMapper localNameMapper = new StringToStringMapper();

        Store<StoreValue> forwardStore = PrefixingStore.create( "f", store );
        Store<StoreValue> reverseStore = PrefixingStore.create( "r", store );

        StoreBackedSwappingSmartMap<String,String,StoreValue> forwardStorage
                = StoreBackedSwappingSmartMap.Builder.create( localNameMapper, forwardStore ).build();

        StoreBackedSwappingSmartMap<String,String,StoreValue> reverseStorage
                = StoreBackedSwappingSmartMap.Builder.create( localNameMapper, reverseStore ).build();

        return new StoreContextualMeshObjectIdentifierNamespaceMap( forwardStorage, reverseStorage, delegate );
    }

    /**
     * Constructor with the map we delegate to.
     *
     * @param localNameMap resolves our local names to local names in the primary
     * @param inverseLocalNameMap resolves names in the primary to our local names
     * @param primary the primary map that owns our namespaces
     */
    protected StoreContextualMeshObjectIdentifierNamespaceMap(
            StoreBackedSwappingSmartMap<String,String,StoreValue> localNameMap,
            StoreBackedSwappingSmartMap<String,String,StoreValue> inverseLocalNameMap,
            PrimaryMeshObjectIdentifierNamespaceMap               primary )
    {
        super( localNameMap, inverseLocalNameMap, primary );

        localNameMap.addDirectSmartMapListener( new SwappingSmartMapListener<> () {
            @Override
            public void loadedFromStorage(
                    SwappingSmartMap<String,String> source,
                    String                          key,
                    String                          value )
            {
                // When restored from disk, update inverse mapping
                if( value != null ) {
                    // if value == null, get return null (not found)
                    theInverseLocalNameMap.put( value, key );
                }
            }
        } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> localNameIterator()
    {
        return getMap().keyIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String generateNewLocalName()
    {
        return theLocalNameGenerator.createUniqueToken();
    }

    /**
     * Obtain the underlying Map. This is for test instrumentation.
     *
     * @return the map
     */
    @SuppressWarnings("unchecked")
    public StoreBackedSwappingSmartMap<String,String,StoreValue> getMap()
    {
        return (StoreBackedSwappingSmartMap<String,String,StoreValue>) theLocalNameMap;
    }

    /**
     * Knows how to create unique local names. Easier to create random ones than to keep track of a counter.
     */
    protected final UniqueStringGenerator theLocalNameGenerator = UniqueStringGenerator.create( 32 );
}
