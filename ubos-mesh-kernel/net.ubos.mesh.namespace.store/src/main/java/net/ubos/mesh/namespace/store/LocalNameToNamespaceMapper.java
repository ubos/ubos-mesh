//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace.store;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import net.ubos.mesh.namespace.DefaultMeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.store.StoreValue;
import net.ubos.store.util.StoreValueDecodingException;
import net.ubos.store.util.StoreValueEncodingException;
import net.ubos.store.util.StoreValueMapper;

/**
 * Knows how to map local names to the actual namespace.
 */
class LocalNameToNamespaceMapper
    implements
        StoreValueMapper<String,DefaultMeshObjectIdentifierNamespace,StoreValue>
{
    /**
     * Let the Map tell us about itself.
     *
     * @param map the value for the map
     */
    protected void setNamespaceMap(
            StorePrimaryMeshObjectIdentifierNamespaceMap map )
    {
        theMap = map;
    }

    @Override
    public String keyToString(
            String key )
    {
        return key;
    }

    @Override
    public String stringToKey(
            String stringKey )
    {
        return stringKey;
    }

    @Override
    public DefaultMeshObjectIdentifierNamespace decodeValue(
            StoreValue value )
        throws
            StoreValueDecodingException
    {
        try {
            InputStreamReader r  = new InputStreamReader( value.getDataAsStream());
            JsonReader        jr = new JsonReader( r );

            DefaultMeshObjectIdentifierNamespace ret = readNamespace( jr );
            return ret;

        } catch( IOException ex ) {
            throw new StoreValueDecodingException( value, ex );
        }
    }

    @Override
    public String getDefaultEncodingId()
    {
        return ENCODING_ID;
    }

    @Override
    public StoreValue encodeValue(
            String                               key,
            DefaultMeshObjectIdentifierNamespace value )
        throws
            StoreValueEncodingException
    {
        try {
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            OutputStreamWriter    w   = new OutputStreamWriter( buf, StandardCharsets.UTF_8 );
            JsonWriter            jw  = new JsonWriter( w );

            writeNamespace( jw, value );

            jw.close();

            return new StoreValue( keyToString( key ), getDefaultEncodingId(), buf.toByteArray());

        } catch( IOException ex ) {
            throw new StoreValueEncodingException( ex );
        }
    }

    /**
     * JSON writing helper method.
     *
     * @param jw the JsonWriter to write to
     * @param ns the namespace to write
     * @throws IOException
     */
    protected void writeNamespace(
            JsonWriter                    jw,
            MeshObjectIdentifierNamespace ns )
        throws
            IOException
    {
        jw.beginObject();

        if( ns.getPreferredExternalName() != null ) {
            jw.name( PREFERRED_EXTERNALNAME_TAG );
            jw.value( ns.getPreferredExternalName() );
        }

        Set<String> externalNames = ns.getExternalNames();
        if( !externalNames.isEmpty() ) {
            jw.name( EXTERNALNAMES_TAG );
            jw.beginArray();
            for( String externalName : externalNames ) {
                jw.value( externalName );
            }
            jw.endArray();
        }

        jw.endObject();
    }

    /**
     * JSON reading helper method.
     *
     * @param jr the JsonReader to read from
     * @return the reconstructed namespace
     * @throws IOException
     */
    protected DefaultMeshObjectIdentifierNamespace readNamespace(
            JsonReader jr )
        throws
            IOException
    {
        String      preferred     = null;
        Set<String> externalNames = new HashSet<>();

        jr.beginObject();
        while( jr.hasNext() ) {
            String name = jr.nextName();
            switch( name ) {
                case PREFERRED_EXTERNALNAME_TAG:
                    preferred = jr.nextString();
                    break;

                case EXTERNALNAMES_TAG:
                    jr.beginArray();
                    while( jr.hasNext() ) {
                        String found = jr.nextString();
                        externalNames.add( found );
                    }
                    jr.endArray();
                    break;
            }
        }
        jr.endObject();

        DefaultMeshObjectIdentifierNamespace ret = new DefaultMeshObjectIdentifierNamespace( preferred, externalNames, theMap );
        return ret;
    }

    /**
     * The map on whose behalf we work.
     */
    protected StorePrimaryMeshObjectIdentifierNamespaceMap theMap;

    /**
     * The only known encoding.
     */
    public static final String ENCODING_ID = "enc-1";

    /**
     * Various JSON tags
     */
    public static final String PREFERRED_EXTERNALNAME_TAG = "preferredExternalName";
    public static final String EXTERNALNAMES_TAG = "externalNames";

    /**
     * Separator between namespace tag and local id when serializing MeshObjectIdentifiers.
     */
    protected static final String SEPARATOR = "#";
}
