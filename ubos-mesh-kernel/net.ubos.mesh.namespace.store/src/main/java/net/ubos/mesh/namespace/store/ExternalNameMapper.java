//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace.store;

import java.nio.charset.StandardCharsets;
import net.ubos.store.StoreValue;
import net.ubos.store.util.StoreValueDecodingException;
import net.ubos.store.util.StoreValueEncodingException;
import net.ubos.store.util.StoreValueMapper;

/**
 * Knows how to map external names to local names.
 */
public class ExternalNameMapper
    implements
        StoreValueMapper<String,String,StoreValue>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public String keyToString(
            String key )
    {
        return key;
    }

    @Override
    public String stringToKey(
            String stringKey )
    {
        return stringKey;
    }

    @Override
    public String decodeValue(
            StoreValue value )
    {
        String ret = new String( value.getData(), StandardCharsets.UTF_8 );
        return ret;
    }

    @Override
    public String getDefaultEncodingId()
    {
        return "noop";
    }

    @Override
    public StoreValue encodeValue(
            String key,
            String value )
        throws
            StoreValueEncodingException
    {
        return new StoreValue( keyToString( key ), getDefaultEncodingId(), value.getBytes( StandardCharsets.UTF_8 ));
    }
}
