//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.mesh.namespace.store;

import net.ubos.mesh.namespace.DefaultMeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.AbstractPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.prefixing.PrefixingStore;
import net.ubos.store.util.StoreBackedSwappingSmartMap;
import net.ubos.util.Pair;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.smartmap.SwappingSmartMap;
import net.ubos.util.token.UniqueStringGenerator;
import net.ubos.util.smartmap.SwappingSmartMapListener;

/**
 * Store-based implementation of MeshObjectIdentifierNamespaceMap that "owns" its
 * StoreMeshobjectIdentifierNamespaces (and not just references them).
 */
public class StorePrimaryMeshObjectIdentifierNamespaceMap
    extends
        AbstractPrimaryMeshObjectIdentifierNamespaceMap
{
    /**
     * Factory method.
     *
     * @param store where to store the map
     * @return the created StorePrimaryMeshObjectIdentifierNamespaceMap
     */
    public static StorePrimaryMeshObjectIdentifierNamespaceMap create(
            Store<StoreValue> store )
    {
        PrefixingStore<StoreValue> localNameStore    = PrefixingStore.create( "ln-", store ); // stores local name to Namespace
        PrefixingStore<StoreValue> externalNameStore = PrefixingStore.create( "alias-", store ); // stores external name to local name

        LocalNameToNamespaceMapper localNameMapper    = new LocalNameToNamespaceMapper();
        ExternalNameMapper          externalNameMapper = new ExternalNameMapper();

        StoreBackedSwappingSmartMap<String,DefaultMeshObjectIdentifierNamespace,StoreValue> localNameMap
                = StoreBackedSwappingSmartMap.Builder.create( localNameMapper, localNameStore ).build();
        StoreBackedSwappingSmartMap<String,String,StoreValue> externalNameMap
                = StoreBackedSwappingSmartMap.Builder.create( externalNameMapper, externalNameStore ).build();

        StorePrimaryMeshObjectIdentifierNamespaceMap ret = new StorePrimaryMeshObjectIdentifierNamespaceMap(
                localNameMap,
                externalNameMap );

        localNameMapper.setNamespaceMap( ret );
        return ret;
    }

    /**
     * Constructor.
     *
     * @param localNameMap resolves local names to namespaces
     * @param externalNameMap resolves external names to local names
     */
    protected StorePrimaryMeshObjectIdentifierNamespaceMap(
            StoreBackedSwappingSmartMap<String,DefaultMeshObjectIdentifierNamespace,StoreValue> localNameMap,
            StoreBackedSwappingSmartMap<String,String,StoreValue>                               externalNameMap )
    {
        super( localNameMap, externalNameMap );

        localNameMap.addDirectSmartMapListener(new SwappingSmartMapListener<> () {
            @Override
            public void loadedFromStorage(
                    SwappingSmartMap<String,DefaultMeshObjectIdentifierNamespace> source,
                    String                                                        key,
                    DefaultMeshObjectIdentifierNamespace                          value )
            {
                // When restored from disk, update inverse mapping
                if( value != null ) {
                    // if value == null, get return null (not found)
                    theInverseLocalNameMap.put( value, key );
                }
            }
        } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<String,DefaultMeshObjectIdentifierNamespace> createAllocateNamespace()
    {
        String localName = theLocalNameGenerator.createUniqueToken();

        Pair<String,DefaultMeshObjectIdentifierNamespace> ret = new Pair<>(
                localName,
                new DefaultMeshObjectIdentifierNamespace( this ));

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> localNameIterator()
    {
        return getLocalNameMap().keyIterator();
    }

    /**
     * Obtain the local name map. This is for test instrumentation.
     *
     * @return the map
     */
    @SuppressWarnings("unchecked")
    public StoreBackedSwappingSmartMap<String,DefaultMeshObjectIdentifierNamespace,StoreValue> getLocalNameMap()
    {
        return (StoreBackedSwappingSmartMap<String,DefaultMeshObjectIdentifierNamespace,StoreValue>) theLocalNameMap;
    }

    /**
     * Knows how to create unique local names. Easier to create random ones than to keep track of a counter.
     */
    protected final UniqueStringGenerator theLocalNameGenerator = UniqueStringGenerator.create( 32 );
}
