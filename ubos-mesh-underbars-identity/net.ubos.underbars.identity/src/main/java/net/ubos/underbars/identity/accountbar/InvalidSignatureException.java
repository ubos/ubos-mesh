//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.accountbar;

/**
 * Thrown if a signature was present but it was invalid.
 */
public class InvalidSignatureException
    extends
        Exception
{
    /**
     * Constructor.
     *
     * @param url the URL to redirect to
     */
    public InvalidSignatureException(
            String url )
    {
        theUrl = url;
    }

    /**
     * Obtain the URL to redirect to.
     *
     * @return the URL
     */
    public String getRedirectToUrl()
    {
        return theUrl;
    }

    /**
     * The URL to redirect to.
     */
    protected final String theUrl;
}
