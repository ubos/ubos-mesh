//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.helpers;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.underbars.HandlebarsModel;
import net.ubos.underbars.identity.accountbar.InvalidSignatureException;
import net.ubos.underbars.identity.accountbar.RequestSigner;
import net.ubos.underbars.identity.accountbar.SignedRequest;
import net.ubos.underbars.skin.SkinModel;
import net.ubos.util.StringHelper;
import net.ubos.web.vl.StructuredResponse;

/**
 * Inserts the HTML content for the AccountBar.
 */
public class AccountBarHelper
    extends
        AbstractIdentityHelper<SkinModel>
{
    /**
     * Constructor.
     *
     * @param signer knows how to validate signed URLs
     */
    public AccountBarHelper(
            RequestSigner signer )
    {
        theSigner = signer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            SkinModel model,
            Options   options )
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        // If this is put into a Skin, it will not actually show up
        StructuredResponse sr = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );
        sr.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars.identity/AccountBar.css" );
        sr.ensureHtmlHeaderScriptSource( "/s/net.ubos.underbars.identity/AccountBar.js" );

        ThreadIdentityManager.Who caller = ThreadIdentityManager.getWho();

        if( caller != null ) {
            // user has been authenticated
            return applyAuthenticated( model, options, caller );

        } else {
            // used has not been authenticated. but perhaps they bring a signed request from
            // e-mail configuration or password reset

            StructuredResponse response = (StructuredResponse) model.get( HandlebarsModel.STRUCTURED_RESPONSE_KEY );

            try {
                SignedRequest signed = theSigner.extractSignedRequest( response.getRequest() );
                if( signed != null ) {
                    return applySigned( model, options, signed );
                } else {
                    return applyAnonymous( model, options );
                }
            } catch( InvalidSignatureException ex ) {
                return applyAnonymous( model, options );
            }
        }
    }

    /**
     * Perform what's needed if the client is currently authenticated.
     *
     * @param model the context object
     * @param options the options object
     * @param caller identifies the caller
     * @return the to-be-emitted String
     */
    protected Object applyAuthenticated(
            SkinModel                 model,
            Options                   options,
            ThreadIdentityManager.Who caller )
    {
        String profileId          = getProfileId( caller );
        String relativeAccountUrl = getRelativeAccountUrl( caller );

        StringBuilder ret = new StringBuilder();
        ret.append( "<ul id=\"MeshAccountBar\">\n" );

        if( profileId != null ) {
            ret.append( " <li>Signed in as: " ).append( profileId ).append( "</li>\n" );
        }
        if( relativeAccountUrl != null ) {
            ret.append( " <li><a href=\"" ).append( relativeAccountUrl ).append( "\">Account</a></li>\n" );
        }

        ret.append( " <li><a href=\"javascript:MeshAccountBar.showLogout(); \"title=\"Click here to sign out\">Sign out</a></li>" );
        ret.append( "</ul>\n" );

        return ret.toString();
    }

    /**
     * Performs what's needed if the client currently doesn't have a session, but the incoming requested is validly
     * signed (from a password reset, or confirmation e-mail).
     *
     * @param model the context object
     * @param options the options object
     * @param signed the signed request
     * @return the to-be-emitted String
     */
    protected Object applySigned(
            SkinModel     model,
            Options       options,
            SignedRequest signed )
    {
        StringBuilder ret = new StringBuilder();

        ret.append( "<ul id=\"MeshAccountBar\">\n" );
        ret.append( " <li><a href=\"javascript:MeshAccountBar.showLogin()\">Sign in</a></li>" );
        ret.append( "</ul>\n");

        ret.append( "<script>\n" );
        ret.append( "MeshAccountBar.showSetPassword( '" )
                .append( signed.getPrincipal() )
                .append( "', '" )
                .append( signed.getToken() )
                .append( "' );\n" );
        ret.append( "</script>\n" );

        return ret.toString();
    }

    /**
     * Perform what's needed if the client is currently anonymous.
     *
     * @param model the context object
     * @param options the options object
     * @return the to-be-emitted String
     */
    protected Object applyAnonymous(
            SkinModel model,
            Options   options )
    {
        StringBuilder ret = new StringBuilder();

        ret.append( "<ul id=\"MeshAccountBar\">\n" );
        ret.append( " <li><a href=\"javascript:MeshAccountBar.showLogin()\">Sign in</a></li>" );
        ret.append( "</ul>\n");

        return ret.toString();
    }

    /**
     * Knows how to sign and validate signed requests.
     */
    protected RequestSigner theSigner;

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
