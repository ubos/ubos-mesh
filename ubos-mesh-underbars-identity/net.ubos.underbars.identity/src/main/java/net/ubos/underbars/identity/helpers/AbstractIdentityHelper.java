//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.helpers;

import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.logging.Log;

/**
 * Functionality common to Helpers related to identity.
 */
public abstract class AbstractIdentityHelper<T>
    extends
        AbstractHandlebarsHelper<T>
{
    private static final Log log = Log.getLogInstance( AbstractIdentityHelper.class );

    /**
     * Determine whether the caller has an account status that's mentioned in the provided String.
     *
     * @param statusString
     * @return
     */
    protected boolean hasAccountStatus(
            String statusString )
    {
        ThreadIdentityManager.Who who = ThreadIdentityManager.getWho();
        if( who == null ) {
            return false;
        }
        MeshObject account = who.getCaller();
        if( account == null ) {
            return false;
        }

        String actual = ((EnumeratedValue)account.getPropertyValue( IdentitySubjectArea.ACCOUNT_STATUS )).value();

        for( String candidate : statusString.split( "[ ,|]+" )) {
            if( actual.equals( candidate )) {
                return true;
            }
        }

        return false;
    }

    /**
     * Obtain the user-visible identity of the user's profile.
     *
     * @return the id
     */
    public String getProfileId(
            ThreadIdentityManager.Who caller )
    {
        MeshObject principal = caller.getPrincipal();
        if( principal != null ) {
            try {
                StringValue value = (StringValue) principal.getPropertyValue( IdentitySubjectArea.PRINCIPAL_NICKNAME );
                if( value != null ) {
                    return value.value();
                }

            } catch( IllegalPropertyTypeException ex ) {
                log.error( ex );
            } catch( NotPermittedException ex ) {
                log.error( ex );
            }
        }
        return null;
    }

    /**
     * Obtain the relative URL to the user's account.
     *
     * @return relative URL
     */
    public String getRelativeAccountUrl(
        ThreadIdentityManager.Who who )
    {
        // FIXME
        MeshObject caller = who.getCaller();
        if( who != null ) {
            return "/" + caller.getIdentifier().getLocalId();
        }
        return null;
    }
}
