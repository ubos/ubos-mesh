//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.accountbar;

import com.google.gson.JsonObject;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.session.Session;
import io.undertow.server.session.SessionConfig;
import io.undertow.server.session.SessionManager;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.underbars.Underbars;

/**
 * Collects the behvaior for the different modes of the AccountBar. This is called when the form is submitted in the
 * respective mode.
 */
enum AccountBarMode
{
    /**
     * "Please sign in" -- lid input only
     */
    MODE_A( "modeA" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData formData,
                HttpServerExchange hse )
            throws
                InvalidRequest
        {
            FormData.FormValue lidValue = formData.getFirst( "lid" );
            if( lidValue == null ) {
                throw new InvalidRequest();
            }

            JsonObject ret = new JsonObject();
            String     lid = handler.getAsValidLid( lidValue.getValue() );
            if( lid != null ) {
                // we do not check here whether the user exists; otherwise it would be easy to tell for
                // attackers who is registered and who is not
                ret.addProperty( AccountBarHandler.MODE_KEY, MODE_D.theNameInProtocol );

            } else {
                ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>Please enter a valid e-mail address.</p>" );
            }

            hse.setStatusCode( 200 );
            hse.getResponseSender().send( ret.toString() );
        }

        @Override
        public boolean isRegistration()
        {
            return false;
        }
    },

    /**
     * "Register for this site"
     */
    MODE_B( "modeB" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData           formData,
                HttpServerExchange hse )
            throws
                InvalidRequest
        {
            FormData.FormValue lidValue = formData.getFirst( "lid" );
            if( lidValue == null ) {
                throw new InvalidRequest();
            }

            JsonObject ret = new JsonObject();
            String     lid = handler.getAsValidLid( lidValue.getValue() );
            if( lid != null ) {
                // we do not check here whether the user exists; otherwise it would be easy to tell for
                // attackers who is registered and who is not
                if( handler.findPrincipalByLid( lid ) == null ) {
                    handler.sendRegistrationEmail( hse, formData.getFirst( "location" ), lid );
                } else {
                    handler.sendRegisteredAlreadyEmail( hse, formData.getFirst( "location" ), lid );
                }

                ret.addProperty( AccountBarHandler.INFO_MESSAGE_KEY,
                                 "<p>Thank you for registering.</p><p>Please check your e-mail in a few minutes to complete registration.</p>" );
                ret.addProperty( AccountBarHandler.MODE_KEY, MODE_C.theNameInProtocol );

            } else {
                ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>Please enter a valid e-mail address.</p>" );
            }

            hse.setStatusCode( 200 );
            hse.getResponseSender().send( ret.toString() );
        }

        @Override
        public boolean isRegistration()
        {
            return true;
        }
    },

    /**
     * "Message"
     */
    MODE_C( "modeC" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData           formData,
                HttpServerExchange hse )
            throws
                InvalidRequest
        {
            // There is no submit button in modeC
            throw new InvalidRequest();
        }

        @Override
        public boolean isRegistration()
        {
            return false;
        }
    },

    /**
     * "Please sign in" -- lid plus lid-credential
     */
    MODE_D( "modeD" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData           formData,
                HttpServerExchange hse )
            throws
                InvalidRequest
        {
            FormData.FormValue lidValue           = formData.getFirst( "lid" );
            FormData.FormValue lidCredTypeValue   = formData.getFirst( "lid-credtype" );
            FormData.FormValue lidCredentialValue = formData.getFirst( "lid-credential" );
            FormData.FormValue locationValue      = formData.getFirst( "location" );

            if( lidValue == null || lidCredTypeValue == null || lidCredentialValue == null || locationValue == null ) {
                throw new InvalidRequest();
            }

            JsonObject ret           = new JsonObject();
            String     lid           = handler.getAsValidLid( lidValue.getValue());
            String     lidCredType   = lidCredTypeValue.getValue();
            String     lidCredential = lidCredentialValue.getValue();
            String     location      = locationValue.getValue();

            if( lid != null ) {
                ThreadIdentityManager.Who who = handler.login( lid, lidCredType, lidCredential );

                if( who != null ) {
                    Session session = obtainSession( hse );
                    session.setAttribute( Underbars.WHO_SESSION_ATTRIBUTE_NAME, who );

                    ret.addProperty( AccountBarHandler.HTTP_LOCATION_KEY, location );

                } else {
                    ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>Invalid e-mail or password.</p>" );
                }
            } else {
                ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>Please enter a valid e-mail address.</p>" );
            }

            hse.setStatusCode( 200 );
            hse.getResponseSender().send( ret.toString() );
        }

        @Override
        public boolean isRegistration()
        {
            return false;
        }
    },

    /**
     * While logged in, user changes password.
     */
    MODE_E( "modeE" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData           formData,
                HttpServerExchange hse )
            throws
                InvalidRequest
        {
            FormData.FormValue lidValue            = formData.getFirst( "lid" );
            FormData.FormValue lidCredTypeValue    = formData.getFirst( "lid-credtype" );
            FormData.FormValue lidCredentialValue  = formData.getFirst( "lid-credential" );
            FormData.FormValue lidCredential1Value = formData.getFirst( "lid-credential1" );
            FormData.FormValue lidCredential2Value = formData.getFirst( "lid-credential2" );

            if( lidValue == null || lidCredTypeValue == null || lidCredentialValue == null || lidCredential1Value == null || lidCredential2Value == null ) {
                throw new InvalidRequest();
            }

            JsonObject ret            = new JsonObject();
            String     lid            = handler.getAsValidLid( lidValue.getValue());
            String     lidCredType    = lidCredTypeValue.getValue();
            String     lidCredential  = lidCredentialValue.getValue();
            String     lidCredential1 = lidCredential1Value.getValue();
            String     lidCredential2 = lidCredential2Value.getValue();

            if( lid != null ) {
                if( lidCredential1 != null && lidCredential1.equals( lidCredential2 )) {
                    ThreadIdentityManager.Who who = handler.login( lid, lidCredType, lidCredential );

                    if( who != null ) {
                        if( handler.changePassword( who.getPrincipal(), lidCredential1.getBytes() )) {

                            ret.addProperty( AccountBarHandler.INFO_MESSAGE_KEY,
                                             "<p>Your password was successfully changed.</p>" );
                            ret.addProperty( AccountBarHandler.MODE_KEY, MODE_C.theNameInProtocol );

                        } else {
                            ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>Your new password is insufficiently strong.</p>" );
                        }
                    } else {
                        ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>Invalid old password.</p>" );
                    }

                } else {
                    ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>The passwords do not match.</p>" );
                }

            } else {
                throw new InvalidRequest();
            }

            hse.setStatusCode( 200 );
            hse.getResponseSender().send( ret.toString() );
        }

        @Override
        public boolean isRegistration()
        {
            return false;
        }
    },

    /**
     * "Initiate Password reset"
     */
    MODE_F( "modeF" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData           formData,
                HttpServerExchange hse )
            throws
                InvalidRequest
        {
            FormData.FormValue lidValue = formData.getFirst( "lid" );
            if( lidValue == null ) {
                throw new InvalidRequest();
            }

            JsonObject ret = new JsonObject();
            String     lid = handler.getAsValidLid( lidValue.getValue() );
            if( lid != null ) {
                handler.sendPasswordResetEmailIfExists( hse, formData.getFirst( "location" ), lid );

                ret.addProperty( AccountBarHandler.INFO_MESSAGE_KEY,
                                 "<p>Please check your e-mail in a few minutes to complete password reset.</p>" );
                ret.addProperty( AccountBarHandler.MODE_KEY, MODE_C.theNameInProtocol );

            } else {
                ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>Please enter a valid e-mail address.</p>" );
            }

            hse.setStatusCode( 200 );
            hse.getResponseSender().send( ret.toString() );
        }

        @Override
        public boolean isRegistration()
        {
            return false;
        }
    },

    /**
     * "Log out"
     */
    MODE_G( "modeG" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData           formData,
                HttpServerExchange hse )
            throws
                InvalidRequest
        {
            JsonObject ret = new JsonObject();

            FormData.FormValue location = formData.getFirst( "location" );
            if( location == null || location.getValue() == null ) {
                throw new InvalidRequest();
            }

            handler.logout( hse );

            ret.addProperty( AccountBarHandler.HTTP_LOCATION_KEY, location.getValue() );

            hse.setStatusCode( 200 );
            hse.getResponseSender().send( ret.toString() );
        }

        @Override
        public boolean isRegistration()
        {
            return false;
        }
    },

    /**
     * Set password for the first time (this also creates principal and account) or
     * user resets password with e-mail confirmation
     */
    MODE_H( "modeH" ) {
        @Override
        public void execute(
                AccountBarHandler  handler,
                FormData           formData,
                HttpServerExchange hse )
                throws
                InvalidRequest
        {
            FormData.FormValue lidValue            = formData.getFirst( "lid" );
            FormData.FormValue lidCredTypeValue    = formData.getFirst( "lid-credtype" );
            FormData.FormValue lidCredential1Value = formData.getFirst( "lid-credential1" );
            FormData.FormValue lidCredential2Value = formData.getFirst( "lid-credential2" );
            FormData.FormValue locationValue       = formData.getFirst( "location" );
            FormData.FormValue tokenValue          = formData.getFirst( "token" );

            if( lidValue == null || lidCredTypeValue == null || lidCredential1Value == null || lidCredential2Value == null || locationValue == null || tokenValue == null ) {
                throw new InvalidRequest();
            }

            JsonObject ret            = new JsonObject();
            String     lid            = lidValue.getValue();
            String     lidCredType    = lidCredTypeValue.getValue();
            String     lidCredential1 = lidCredential1Value.getValue();
            String     lidCredential2 = lidCredential2Value.getValue();
            String     location       = locationValue.getValue();
            String     token          = tokenValue.getValue();

            lid = handler.getAsValidLid( lid );
            if( lid != null ) {
                if( lidCredential1 != null && lidCredential1.equals( lidCredential2 )) {

                    if( handler.isPasswordValid( lidCredential1 )) {
                        try {
                            SignedRequest signed = handler.extractSignedRequest( location, token );
                            if( signed != null ) {

                                ThreadIdentityManager.Who who = handler.createAccount( lid,
                                                                                       lidCredType,
                                                                                       lidCredential1 );
                                if( who != null ) {
                                    Session session = obtainSession( hse );
                                    session.setAttribute( Underbars.WHO_SESSION_ATTRIBUTE_NAME, who );

                                    String locationWithoutToken = handler.getLocationWithoutToken( location );

                                    ret.addProperty( AccountBarHandler.HTTP_LOCATION_KEY, locationWithoutToken );
                                    // ret.addProperty( AccountBarHandler.INFO_MESSAGE_KEY,
                                    //                  "<p>Your password was successfully set.</p>" );
                                    // ret.addProperty( AccountBarHandler.MODE_KEY, MODE_C.theNameInProtocol );

                                } else {
                                    ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY,
                                                     "<p>Your new password is insufficiently strong.</p>" );
                                }
                            } else {
                                ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY,
                                                 "<p>Invalid request.</p>" ); // FIXME better error message?
                            }
                        } catch( InvalidSignatureException ex ) {
                            ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY,
                                             "<p>Invalid token to this request.</p>" ); // This may never be printed
                            ret.addProperty( AccountBarHandler.HTTP_LOCATION_KEY, ex.getRedirectToUrl() );
                        }
                    } else {
                        ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>This password is not sufficiently complex.</p>" );
                    }

                } else {
                    ret.addProperty( AccountBarHandler.ERROR_MESSAGE_KEY, "<p>The passwords do not match.</p>" );
                }

            } else {
                throw new InvalidRequest();
            }

            hse.setStatusCode( 200 );
            hse.getResponseSender().send( ret.toString() );
        }

        @Override
        public boolean isRegistration()
        {
            return true; // FIXME: we should probably permit password resets
        }
    };

    /**
     * Constructor.
     *
     * @param nameInProtocol referred to by this name when communicating with AccountBar.js
     */
    AccountBarMode(
            String nameInProtocol )
    {
        theNameInProtocol = nameInProtocol;
    }

    /**
     * Look up the correct instance of Mode from the name sent from AccountBar.js
     *
     * @param modeName name of the mode
     * @return the Mode, or null
     * @throws InvalidRequest if the request was invalid
     */
    public static AccountBarMode find(
            FormData.FormValue modeName )
        throws
            InvalidRequest
    {
        if( modeName == null ) {
            throw new InvalidRequest();
        }
        String modeNameString = modeName.getValue();
        if( modeNameString == null ) {
            throw new InvalidRequest();
        }

        for( AccountBarMode v : values()) {
            if( v.theNameInProtocol.equals( modeNameString )) {
                return v;
            }
        }
        throw new InvalidRequest();
    }

    /**
     * Execute what needs executing given this mode.
     *
     * @param handler the AccoutnBarHandler
     * @param formData the submitted form data
     * @param hse the HTTP exchange
     * @throws InvalidRequest if the request was valid
     */
    public abstract void execute(
            AccountBarHandler  handler,
            FormData           formData,
            HttpServerExchange hse )
        throws
            InvalidRequest;

    /**
     * Determine whether this mode is used for signing up for new accounts.
     * This enables us to tell whether the mode is permitted when account creation is disabled.
     */
    public abstract boolean isRegistration();

    /**
     * Helper method to obtain the current session.
     *
     * @param hse the HTTP exchange
     */
    protected Session obtainSession(
            HttpServerExchange hse )
    {
        SessionManager sessionManager = hse.getAttachment( SessionManager.ATTACHMENT_KEY );
        SessionConfig  sessionConfig  = hse.getAttachment( SessionConfig.ATTACHMENT_KEY );

        Session session = sessionManager.getSession( hse, sessionConfig );
        if( session == null ) {
            session = sessionManager.createSession( hse, sessionConfig );
        }
        return session;
    }

    /**
     * Name of this mode in the protocol with AccountBar.js.
     */
    protected String theNameInProtocol;
}
