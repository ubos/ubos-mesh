//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.accountbar;

/**
 * An incoming request was impossible unless the user hacked on the protocol themselves.
 */
class InvalidRequest
    extends
        Exception
{}
