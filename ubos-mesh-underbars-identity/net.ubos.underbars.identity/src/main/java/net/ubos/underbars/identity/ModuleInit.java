//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity;

import com.github.jknack.handlebars.Handlebars;
import io.undertow.server.HttpHandler;
import io.undertow.server.session.InMemorySessionManager;
import io.undertow.server.session.SessionAttachmentHandler;
import io.undertow.server.session.SessionCookieConfig;
import io.undertow.server.session.SessionManager;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.util.logging.Log;
import org.diet4j.core.ModuleDeactivationException;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleSettings;
import net.ubos.underbars.identity.accountbar.AccountBarHandler;
import static net.ubos.underbars.identity.accountbar.AccountBarHandler.ENCODING_1;
import net.ubos.underbars.identity.accountbar.RequestSigner;
import net.ubos.underbars.identity.accountbar.SignatureCheckingHandler;
import net.ubos.underbars.identity.helpers.AccountBarHelper;
import net.ubos.underbars.identity.helpers.HasAccountHelper;
import net.ubos.underbars.identity.helpers.IsAnonymousHelper;
import net.ubos.underbars.identity.helpers.RequireAccountHelper;
import net.ubos.underbars.identity.helpers.UserHelper;

/**
 * Module initializer class.
     */
public class ModuleInit
{
    private static final Log log = Log.getLogInstance( ModuleInit.class ); // our own, private logger

    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-identity";

    /**
     * Name of the section where the handler is located.
     */
    public static final String HANDLER_DIR = "net.ubos.underbars.identity";

    /**
     * Activate this Module.
     *
     * @param thisModule the current Module
     * @throws ModuleActivationException thrown if the Module could not be activated
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        log.traceMethodCallEntry( null, "moduleActivate", thisModule );

        ModuleSettings settings = thisModule.getModuleSettings();

        Store<StoreValue> userCredentialStore = Underbars.getUserCredentialsStore();
        if( userCredentialStore == null ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), "No user credentials Store provided" );
        }

        // Admin account

        MeshBase mb = Underbars.getMeshBaseNameServer().getDefaultMeshBase();

        String adminCredential = settings.getString( "admin_credential" );
        String adminEmail      = settings.getString( "admin_email" );
        String adminUserId     = settings.getString( "admin_userid" );
        String adminUserName   = settings.getString( "admin_username" );

        String principalId   = AccountBarHandler.constructPrincipalId( "mailto:" + adminEmail );

        mb.sudoExecute( () -> {
                MeshObject account = createIfNeeded( mb, AccountBarHandler.OWNER_ACCOUNT_ID, IdentitySubjectArea.ACCOUNT );
                account.setPropertyValue( IdentitySubjectArea.ACCOUNT_STATUS, IdentitySubjectArea.ACCOUNT_STATUS_type_ACTIVE );

                MeshObject principal = account.traverse( IdentitySubjectArea.PRINCIPAL_MAYUSE_ACCOUNT_D ).getSingleMember();
                MeshObject profile   = null;
                if( principal != null ) {
                    // check whether it needs to be updated.
                    profile = principal.traverse( IdentitySubjectArea.PRINCIPALPROFILE_FOR_PRINCIPAL_D ).getSingleMember();
                    if( !principal.getIdentifier().getLocalId().equals( principalId )) {
                        // need a different one
                        if( profile != null ) {
                            mb.deleteMeshObject( profile );
                        }
                        mb.deleteMeshObject( principal );
                        principal = null;
                        profile   = null;
                    }
                }
                if( principal == null ) {
                    principal = mb.createMeshObject( principalId, IdentitySubjectArea.PRINCIPAL );
                    createIfNeeded( principal, IdentitySubjectArea.PRINCIPAL_MAYUSE_ACCOUNT.getSource(), account );
                }
                principal.setPropertyValue( IdentitySubjectArea.PRINCIPAL_NICKNAME, StringValue.create( adminUserId ));

                if( profile == null ) {
                    profile = mb.createMeshObject(
                            mb.createRandomMeshObjectIdentifierBelow( AccountBarHandler.PROFILE_ID_PREFIX ),
                            IdentitySubjectArea.PRINCIPALPROFILE );
                    profile.blessRole( IdentitySubjectArea.PRINCIPALPROFILE_FOR_PRINCIPAL_S, principal );
                }
                profile.setPropertyValue( IdentitySubjectArea.PRINCIPALPROFILE_FULLNAME, StringValue.create( adminUserName ) );
                profile.setPropertyValue( IdentitySubjectArea.PRINCIPALPROFILE_EMAILADDRESS, StringValue.create( adminEmail ));
        } );

        try {
            userCredentialStore.putOrUpdate(
                    new StoreValue(
                            principalId,
                            ENCODING_1,
                            adminCredential.getBytes() ));
        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }

        //

        PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

        Handlebars h = Underbars.getHandlebars();

        RequestSigner signer = RequestSigner.create(
                settings.getString( "url_signature_secret" ),
                settings.getString( "url_obfuscation_secret" ),
                settings.getInteger( "url_signature_max_elapsed", 0 ) * 1000L );


        h.registerHelper( "id:accountBar",     new AccountBarHelper( signer ) );
        h.registerHelper( "id:hasAccount",     new HasAccountHelper() );
        h.registerHelper( "id:isAnonymous",    new IsAnonymousHelper() );
        h.registerHelper( "id:requireAccount", new RequireAccountHelper() );
        h.registerHelper( "id:user",           new UserHelper() );


        Underbars.registerPathDelegate(
                "/s/" + HANDLER_DIR + "/account-bar-handler",
                AccountBarHandler.create(
                        Underbars.getMeshBaseNameServer().getDefaultMeshBase(),
                        userCredentialStore,
                        signer,
                        settings.getBoolean( "registration_enabled", false )));

        HttpHandler    next           = Underbars.getSecurityNextHttpHandler();
        SessionManager sessionManager = new InMemorySessionManager( "SessionManager" );

        sessionManager.setDefaultSessionTimeout( 60 * 60 * 24 ); // 24 hours
        Underbars.setSecurityHttpHandler(
                new SessionAttachmentHandler(
                        new SignatureCheckingHandler( signer, next ),
                        sessionManager,
                        new SessionCookieConfig()));

        Underbars.registerAssets( resourceManager.getAssetMap() );

        try {
            resourceManager.checkResources();

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }

    /**
     * Deactivate this Module.
     *
     * @param thisModule the current Module
     * @throws ModuleDeactivationException thrown if the Module could not be deactivated
     */
    public static void moduleDeactivate(
            Module thisModule )
        throws
            ModuleDeactivationException
    {
        log.traceMethodCallEntry( null, "moduleDeactivate", thisModule );
    }

    /**
     * Create a MeshObject it it does not exist yet.
     *
     * @param id
     * @param type
     */
    private static MeshObject createIfNeeded(
            MeshBase   mb,
            String     id,
            EntityType type )
        throws
            ParseException
    {
        MeshObject ret = mb.findMeshObjectByIdentifier( id );
        if( ret == null ) {
            ret = mb.createMeshObject( id );
        }
        if( !ret.isBlessedBy( type )) {
            ret.bless( type );
        }
        return ret;
    }

    /**
     * Create a blessed relationship if it does not exist yet.
     *
     * @param obj
     * @param rt
     * @param neighbor
     */
    private static void createIfNeeded(
            MeshObject obj,
            RoleType   rt,
            MeshObject neighbor )
    {
        if( !obj.isRelated( rt, neighbor )) {
            obj.blessRole( rt, neighbor );
        }
    }
}
