//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.accountbar;

/**
 * Captures the information in an incoming HttpRequest that was validly signed.
 */
public class SignedRequest
{
    /**
     * Private constructor, use factory method.
     *
     * @param principal the principal identifier of the user indicated in the request
     * @param token the token in its entirety
     */
    protected SignedRequest(
            String principal,
            String token )
    {
        thePrinicpal = principal;
        theToken     = token;
    }

    /**
     * Obtain the principal identifier (e.g. e-mail address) of the user indicated in the request.
     *
     * @return the principal identifier
     */
    public String getPrincipal()
    {
        return thePrinicpal;
    }

    /**
     * Obtain the entire token in the signed request
     *
     * @return the token
     */
    public String getToken()
    {
        return theToken;
    }

    /**
     * Principal identifier of the user indicated in the request.
     */
    protected final String thePrinicpal;

    /**
     * The token in the request.
     */
    protected final String theToken;
}
