//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.accountbar;

import com.google.gson.JsonObject;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.session.Session;
import io.undertow.server.session.SessionConfig;
import io.undertow.server.session.SessionManager;
import io.undertow.util.Headers;
import io.undertow.util.Methods;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.primitives.StringValue;
import net.ubos.store.Store;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.underbars.Underbars;
import net.ubos.util.logging.Log;

/**
 * Gets XmlHttpRequest callbacks from the AccountBar.
 *
 * Strategy:
 * * When the user first registers, or when the user activates password reset, we do not actually store any data.
 *   This cuts down on spam. Instead, we send a confirmation URL that has a nonce, and is signed, so we can
 *   make certain we sent it within the most recent X hours.
 * * Accessing the URL in the registration confirmation e-mail (if the SignedRequest is still valid) triggers the
 *   opening of the AccountBar in the setPassword mode. When submitted, the token from the signed request is
 *   checked one more time by this class, and if valid, the password is set/changed and 1) the principal and
 *   account are created, and 2) a session is started.
 */
public class AccountBarHandler
    implements
        HttpHandler
{
    private static final Log log = Log.getLogInstance( AccountBarHandler.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param mb: the main MeshBase
     * @param userCredentialsStore: stores user credentials, keyed by Principal's MeshObjectIdentifier (not e-mail directly)
     * @param signer knows how to sign URLs and validate them
     * @param registrationEnabled if true, new accounts can be created
     * @return the created instance
     */
    public static AccountBarHandler create(
            MeshBase          mb,
            Store<StoreValue> userCredentialsStore,
            RequestSigner     signer,
            boolean           registrationEnabled )
    {
        return new AccountBarHandler( mb, userCredentialsStore, signer, registrationEnabled );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param mb: the main MeshBase
     * @param userCredentialsStore: stores user credentials
     * @param signer knows how to sign URLs and validate them
     * @param registrationEnabled if true, new accounts can be created
     */
    protected AccountBarHandler(
            MeshBase          mb,
            Store<StoreValue> userCredentialsStore,
            RequestSigner     signer,
            boolean           registrationEnabled )
    {
        if( mb == null ) {
            throw new NullPointerException();
        }
        if( userCredentialsStore == null ) {
            throw new NullPointerException();
        }
        if( signer == null ) {
            throw new NullPointerException();
        }
        theMeshBase             = mb;
        theUserCredentialsStore = userCredentialsStore;
        theSigner               = signer;
        theRegistrationEnabled  = registrationEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleRequest(
            HttpServerExchange hse )
    {
        // whatever happens, we send JSON
        hse.getResponseHeaders().put( Headers.CONTENT_TYPE, "text/json;charset=utf-8" );

        try {
            if( !Methods.POST.equals( hse.getRequestMethod() ) ) {
                throw new InvalidRequest();
            }
            FormData data = hse.getAttachment( FormDataParser.FORM_DATA );
            if( data == null ) {
                data = Underbars.theFormParseFactory.createParser( hse ).parseBlocking();
            }
            FormData.FormValue mode = data.getFirst( "mode" );

            AccountBarMode m = AccountBarMode.find( mode ); // will throw if not found

            if( theRegistrationEnabled || !m.isRegistration() ) {
                m.execute( this, data, hse );

            } else {
                JsonObject ret = new JsonObject();
                ret.addProperty( ERROR_MESSAGE_KEY, "<p>400: Signup disabled.</p>" );

                hse.setStatusCode( 400 );
                hse.getResponseSender().send( ret.toString() );
            }

        } catch( InvalidRequest ex ) {

            JsonObject ret = new JsonObject();
            ret.addProperty( ERROR_MESSAGE_KEY, "<p>400: Bad request.</p>" );

            hse.setStatusCode( 400 );
            hse.getResponseSender().send( ret.toString() );

        } catch( Throwable ex ){
            log.error( ex );

            JsonObject ret = new JsonObject();
            ret.addProperty( ERROR_MESSAGE_KEY, "<p>500: Invalid Request.</p>" );

            hse.setStatusCode( 500 );
            hse.getResponseSender().send( ret.toString() );
        }
    }

    /**
     * Check that the principal identifier is valid for registration. If yes, return the (possibly repaired)
     * form, null otherwise
     *
     * @param lid the principal identifier
     * @return the complete principal identifier
     */
    protected String getAsValidLid(
            String lid )
    {
        Matcher m = VALID_LID_PATTERN.matcher( lid );
        if( !m.matches() ) {
            return null;
        }
        String ret;
        if( m.group( 1 ) == null || m.group( 1 ).isEmpty() ) {
            ret = "mailto:" + lid;
        } else {
            ret = lid;
        }
        if( ret.startsWith( "mailto:" )) {
            ret = ret.toLowerCase();
        }
        return ret;
    }

    /**
     * Check that this password is acceptable
     *
     * @param pass the password
     * @return true if acceptable
     */
    protected boolean isPasswordValid(
            String pass )
    {
        if( pass.length() < 7 ) {
            return false;
        }
        return true;
    }

    /**
     * Obtain the Principal that goes with a particular e-mail address, or return null
     *
     * @param lid e-mail address
     * @return the Principal or null
     */
    protected MeshObject findPrincipalByLid(
            String lid )
    {
        try {
            MeshObject principal = theMeshBase.findMeshObjectByIdentifier( constructPrincipalId( lid ) );
            return principal;

        } catch( ParseException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Obtain the principal and account of an existing user with e-mail lid, and the provided credentials
     * @param lid
     * @param lidCredType
     * @param lidCredential
     * @return
     * @throws InvalidRequest
     */
    protected ThreadIdentityManager.Who login(
            String lid,
            String lidCredType,
            String lidCredential )
         throws
            InvalidRequest
    {
        String principalId = constructPrincipalId( lid );

        StoreValue data;
        try {
            data = theUserCredentialsStore.get( principalId );
            if( data == null ) {
                return null;
            }

        } catch( StoreKeyDoesNotExistException ex ) {
            return null;

        } catch( IOException ex ) {
            log.error( ex );
            return null;
        }

        if( ENCODING_1.equals( data.getEncodingId())) {
            byte [] passwd = data.getData();
            if( Arrays.equals( passwd, lidCredential.getBytes() )) {

                try {
                    TimeStampValue now = TimeStampValue.now();

                    return ThreadIdentityManager.suExec( () -> {
                        ThreadIdentityManager.Who ret = null;

                        MeshObject principal = theMeshBase.findMeshObjectByIdentifier( principalId );
                        if( principal != null ) {
                            MeshObject account = principal.traverse( IdentitySubjectArea.PRINCIPAL_MAYUSE_ACCOUNT.getSource() ).getSingleMember();

                            theMeshBase.execute( () -> {
                                principal.setPropertyValue( IdentitySubjectArea.PRINCIPAL_LOGGEDINLAST, now );
                                account.setPropertyValue(   IdentitySubjectArea.ACCOUNT_LASTENTERED,    now );
                            });

                            ret = new ThreadIdentityManager.Who( account, principal );
                        }
                        return ret;
                    } );

                } catch( Exception ex ) {
                    log.error( ex );
                    return null;
                }
            } else {
                return null;
            }

        } else {
            log.error( "Unknown encoding:", data.getEncodingId() );
            return null;
        }
    }


    /**
     * Create a principal and account for a new user with e-mail lid
     *
     * @param lid the e-mail address
     * @param lidCredType the credential type provided
     * @param lidCredential the credential provided
     * @return the pair of principal and account
     * @throws InvalidRequest the request was invalid
     * @throws TransactionActionException the account could not be created
     */
    public ThreadIdentityManager.Who createAccount(
            String lid,
            String lidCredType,
            String lidCredential )
        throws
            InvalidRequest,
            TransactionActionException
    {
        // see also: owner account creation in ModuleInit

        if( !SIMPLE_PASSWORD_CRED_TYPE_VALUE.equals( lidCredType )) {
            throw new InvalidRequest();
        }

        String principalId = constructPrincipalId( lid );
        String email       = lid.startsWith( "mailto:" ) ? lid.substring( "mailto:".length() ) : null;
        String nickname    = email == null ? lid : email;

        ThreadIdentityManager.Who ret = theMeshBase.execute(
                (Transaction tx) -> {
                    MeshObject principal = theMeshBase.createMeshObject( principalId, IdentitySubjectArea.PRINCIPAL );
                    principal.setPropertyValue( IdentitySubjectArea.PRINCIPAL_NICKNAME, StringValue.create( nickname ) );

                    MeshObject account = theMeshBase.createMeshObject(
                            theMeshBase.createRandomMeshObjectIdentifierBelow( ACCOUNT_ID_PREFIX ),
                            IdentitySubjectArea.ACCOUNT );
                    account.setPropertyValue( IdentitySubjectArea.ACCOUNT_STATUS, NEW_ACCOUNT_STATUS );

                    principal.blessRole( IdentitySubjectArea.PRINCIPAL_MAYUSE_ACCOUNT.getSource(), account );

                    if( email != null ) {
                        MeshObject profile = theMeshBase.createMeshObject(
                                theMeshBase.createRandomMeshObjectIdentifierBelow( PROFILE_ID_PREFIX ),
                                IdentitySubjectArea.PRINCIPALPROFILE );
                        profile.setPropertyValue( IdentitySubjectArea.PRINCIPALPROFILE_EMAILADDRESS, StringValue.create( email ));
                        profile.blessRole( IdentitySubjectArea.PRINCIPALPROFILE_FOR_PRINCIPAL_S, principal );
                    }
                    theUserCredentialsStore.put(
                            new StoreValue(
                                    principalId,
                                    ENCODING_1,
                                    lidCredential.getBytes() ));

                    return new ThreadIdentityManager.Who( account, principal );
                } );

        return ret;
    }

    /**
     * Change a password.
     *
     * @param principal the Principal whose password shall be changed
     * @param newPassword the new password
     * @return true if successful
     */
    public boolean changePassword(
            MeshObject principal,
            byte []    newPassword )
    {
        try {
            theUserCredentialsStore.update(
                    new StoreValue(
                            principal.getIdentifier().getLocalId(),
                            ENCODING_1,
                            newPassword ));
            return true;

        } catch( StoreKeyDoesNotExistException ex ) {
            return false;

        } catch( IOException ex ) {
            log.error( ex );
            return false;
        }
    }

    /**
     * Send registration e-mail for new accounts.
     */
    protected void sendRegistrationEmail(
            HttpServerExchange hse,
            FormData.FormValue location,
            String             principalId )
    {
        String absoluteContextUrl = constructReturnUrl( hse, location );

        if( absoluteContextUrl != null ) { // if not, it's likely an attack
            String signedUrl = theSigner.signUrl( absoluteContextUrl, principalId );
            System.err.println( "Registration email will contain: " + signedUrl );
            // FIXME
        }
    }

    /**
     * Sends "you are registered already" e-mail for somebody trying to sign up for an existing account.
     */
    protected void sendRegisteredAlreadyEmail(
            HttpServerExchange hse,
            FormData.FormValue location,
            String             principalId )
    {
        String absoluteContextUrl = constructReturnUrl( hse, location );

        if( absoluteContextUrl != null ) { // if not, it's likely an attack
            String signedUrl = theSigner.signUrl( absoluteContextUrl, principalId );
            System.err.println( "Registered-already email will contain: " + signedUrl );
            // FIXME
        }
    }

    /**
     * Only send reset e-mail if the user actually exists
     */
    protected void sendPasswordResetEmailIfExists(
            HttpServerExchange hse,
            FormData.FormValue location,
            String             lid )
    {
        MeshObject principal          = findPrincipalByLid( lid );
        String     absoluteContextUrl = constructReturnUrl( hse, location );
        if( principal != null && absoluteContextUrl != null ) {

            String signedUrl = theSigner.signUrl( absoluteContextUrl, lid );
            System.err.println( "Password reset email will contain: " + signedUrl );
            // FIXME
        }
    }

    /**
     * Perform what is necessary to log the user out.
     *
     * @param hse the HTTP request/response exchange
     */
    protected void logout(
            HttpServerExchange hse )
    {
        SessionManager sm            = hse.getAttachment( SessionManager.ATTACHMENT_KEY );
        SessionConfig  sessionConfig = hse.getAttachment( SessionConfig.ATTACHMENT_KEY );
        Session        session       = sm.getSession( hse, sessionConfig );

        if( session != null ) {
            session.invalidate( hse );
        }
    }

    /**
     * Extract a SignedRequest from a location and a token
     *
     * @param location
     * @param token
     * @return
     * @throws InvalidSignatureException if the request was signed, but the signature was wrong
     */
    public SignedRequest extractSignedRequest(
            String location,
            String token )
        throws
            InvalidSignatureException
    {
        return theSigner.extractSignedRequest( location, token );
    }


    /**
     * Determine the return URL. This returns null if something fishy is going on.
     *
     * @param hse the current HTTP exchange
     * @param location URL of the currently accessed HTML page as reported by the browser
     * @return the return URL, or null
     */
    protected String constructReturnUrl(
            HttpServerExchange hse,
            FormData.FormValue location )
    {
        if( location == null || location.getValue() == null ) {
            return null;
        }
        try {
            URL    locationUrl  = new URL( location.getValue() );
            String originalHost = hse.getRequestHeaders().getFirst( "X-Forwarded-Host" );

            if( originalHost == null ) { // not behind proxy
                URL hseUrl = new URL( hse.getRequestURL() );
                originalHost = hseUrl.getHost();
            }

            if( !locationUrl.getHost().equals( originalHost ) ) {
                return null;
            }
            return location.getValue();

        } catch( MalformedURLException ex ) {
            log.error( ex );
            return null;
        }
    }

    /**
     * Given this principal identifier (e.g. e-mail address "foo@bar.com"), determine the corresponding MeshObjectIdentifier
     * (e.g. "principal/foo@bar.com").
     *
     * @param identifier the identifier
     * @return the InfoGrid identifier
     */
    public static String constructPrincipalId(
            String identifier )
    {
        return PRINCIPAL_ID_PREFIX + "/" + identifier;
    }

    /**
     * Strip of any token= from this URL.
     *
     * @param location URL possibly with token
     * @return URL without the token
     */
    public String getLocationWithoutToken(
            String location )
    {
        return theSigner.getLocationWithoutToken( location );
    }

    /**
     * The main MeshBase.
     */
    protected final MeshBase theMeshBase;

    /**
     * Stores user credentials.
     */
    protected final Store<StoreValue> theUserCredentialsStore;

    /**
     * Knows how to sign and validate signed requests./
     */
    protected final RequestSigner theSigner;

    /**
     * If true, new accounts can be created.
     */
    protected final boolean theRegistrationEnabled;

    /**
     * The regular expression that principal ids need to match. Currently it must be a valid
     * e-mail address.
     */
    protected static final Pattern VALID_LID_PATTERN = Pattern.compile( "^(mailto:)?\\S+@\\S+\\.\\S{2,}$" );

    /**
     * The JSON entries in the protocol with AccountBar.js.
     */
    public static final String INFO_MESSAGE_KEY  = "info_i18nmessage_html";
    public static final String ERROR_MESSAGE_KEY = "error_i18nmessage_html";
    public static final String HTTP_LOCATION_KEY = "http_location";
    public static final String MODE_KEY          = "mode";

    public static final String SIMPLE_PASSWORD_CRED_TYPE_VALUE = "simple-password";

    public static final String ENCODING_1 = "encoding-1";

    public static final String PRINCIPAL_ID_PREFIX = "principal";
    public static final String ACCOUNT_ID_PREFIX   = "account";
    public static final String PROFILE_ID_PREFIX   = "profile";

    /**
     * Identifier for well-known object: owner account
     */
    public static final String OWNER_ACCOUNT_ID = ACCOUNT_ID_PREFIX + "/owner";

    /**
     * The default Account status for newly created accounts.
     */
    public static final PropertyValue NEW_ACCOUNT_STATUS = IdentitySubjectArea.ACCOUNT_STATUS_type_APPLIEDFOR;
}

