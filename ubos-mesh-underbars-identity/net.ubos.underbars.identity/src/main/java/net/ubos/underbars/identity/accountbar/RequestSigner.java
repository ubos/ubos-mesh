//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.accountbar;

import io.undertow.server.HttpServerExchange;
import net.ubos.util.logging.Log;
import net.ubos.web.HttpRequest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import net.ubos.util.StringHelper;

/**
 * Knows how to create and validate signed URLs so HTTP requests can be authenticated for
 * password reset or account registration purposes. This uses a MessageDigest with
 * a secret, It also obfuscates the signed URL.
 */
public class RequestSigner
{
    private static final Log log = Log.getLogInstance( RequestSigner.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param signingSecretString the secret used in the signature construction, provided as a hex-encoded String
     * @param obfuscationSecretString the secret used to obfuscate the URL, provided as a hex-encoded String
     * @param maxMillisElapsed the maximum number of milliseconds that the URL is valid from the time of creation
     * @return the created instance
     */
    public static RequestSigner create(
            String signingSecretString,
            String obfuscationSecretString,
            long   maxMillisElapsed )
    {
        byte[] signingSecret     = fromHex( "signingSecret",     signingSecretString );
        byte[] obfuscationSecret = fromHex( "obfuscationSecret", obfuscationSecretString );

        return new RequestSigner( signingSecret, obfuscationSecret, maxMillisElapsed );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param signingSecret the secret used in the signature construction
     * @param obfuscationSecret the secret used to obfuscate the URL
     * @param maxMillisElapsed the maximum number of milliseconds that the URL is valid from the time of creation
     */
    protected RequestSigner(
            byte [] signingSecret,
            byte [] obfuscationSecret,
            long    maxMillisElapsed )
    {
        if( signingSecret == null || signingSecret.length < 8  ) {
            throw new IllegalArgumentException( "Invalid RequestSigner secret" );
        }
        if( maxMillisElapsed < 1000L ) {
            throw new IllegalArgumentException( "Invalid RequestSigner maxTimeElapsed" );
        }

        theSigningSecret  = signingSecret;
        theMaxTimeElapsed = maxMillisElapsed;

        SecretKey obfuscationKey = new SecretKeySpec( obfuscationSecret, "AES" );
        try {
            theObfuscator   = Cipher.getInstance( OBFUSCATION_TRANSFORM );
            theUnobfuscator = Cipher.getInstance( OBFUSCATION_TRANSFORM );

            theObfuscator.init(   Cipher.ENCRYPT_MODE, obfuscationKey, INIT_VECTOR_SPEC );
            theUnobfuscator.init( Cipher.DECRYPT_MODE, obfuscationKey, INIT_VECTOR_SPEC );

        } catch( Throwable t ) {
            log.error( t );
            theObfuscator   = null;
            theUnobfuscator = null;
        }
    }

    /**
     * Sign a given URL and a principal identifier by appending an obfuscated signature.
     *
     * @param url the base URL
     * @param principalId the principal identifier
     * @return the signed URL
     */
    public String signUrl(
            String url,
            String principalId )
    {
        String ts   = obtainTimestamp();
        String args = principalId + '&' + ts;

        char sep = url.contains( "?" ) ? '&' : '?';

        String sig  = constructSignature( url + sep + args );

        String ret = url + sep + TOKEN_KEYWORD_EQUALS + obfuscate( args + "&" + sig );
        return ret;
    }

    /**
     * Parse an incoming HttpRequest, and if it is validly signed, return a SignedRequest.
     *
     * @param hse
     * @return the extracted valid SignedRequest, or null
     * @throws InvalidSignatureException if the request was supposedly signed, but the signature was wrong
     */
    public SignedRequest extractSignedRequest(
            HttpServerExchange hse )
        throws
            InvalidSignatureException
    {
        return extractSignedRequest( hse.getRequestURL(), null );
    }

    /**
     * Parse an incoming HttpRequest, and if it is validly signed, return a SignedRequest.
     *
     * @param request the incoming request
     * @return the extracted valid SignedRequest, or null
     * @throws InvalidSignatureException if the request was supposedly signed, but the signature was wrong
     */
    public SignedRequest extractSignedRequest(
            HttpRequest request )
        throws
            InvalidSignatureException
    {
        return extractSignedRequest( request.getUrl().getAbsoluteFullUri(), null );
    }

    /**
     * Parse an incoming request, consisting of the base URL and the provided value of
     * the token.
     *
     * @param url the base URL
     * @param token the token
     * @return the extracted valid SignedRequest
     * @throws InvalidSignatureException if the request is signed, but the signature was wrong
     */
    public SignedRequest extractSignedRequest(
            String url,
            String token )
        throws
            InvalidSignatureException
    {
        // the url may also contain a token
        int tokenStarts  = url.lastIndexOf( TOKEN_KEYWORD_EQUALS );
        if( tokenStarts < 1 ) {
            if( token == null ) {
                return null;
            }
        } else {
            char beforeToken = url.charAt( tokenStarts - 1 );
            if( beforeToken == '&' || beforeToken == '?' ) {
                if( token == null ) {
                    token = url.substring( tokenStarts + TOKEN_KEYWORD_EQUALS.length() );
                }
                url = url.substring( 0, tokenStarts - 1 );
            }
        }

        String argString = unobfuscate( token );

        String[] args = argString.split( "&" );
        if( args.length != 3 ) {
            throw new InvalidSignatureException( url );
        }
        String principal = args[0];
        String timestamp = args[1];
        String sig       = args[2];

        if( isTimestampExpired( timestamp ) ) {
            throw new InvalidSignatureException( url );
        }

        char sep = url.contains( "?" ) ? '&' : '?';
        String sig2 = constructSignature( url + sep + principal + '&' + timestamp );
        if( !sig.equals( sig2 ) ) {
            throw new InvalidSignatureException( url );
        }

        String principalString = principal.startsWith( "mailto:" ) ? principal.substring( "mailto:".length() ) : principal;
        return new SignedRequest( principalString, token );
    }

    /**
     * Strip of any token= from this URL.
     *
     * @param location URL possibly with token
     * @return URL without the token
     */
    public String getLocationWithoutToken(
            String location )
    {
        int tokenStarts = location.lastIndexOf( TOKEN_KEYWORD_EQUALS );
        if( tokenStarts < 1 ) {
            return location;

        } else {
            char beforeToken = location.charAt( tokenStarts - 1 );
            if( beforeToken == '&' || beforeToken == '?' ) {
                return location.substring( 0, tokenStarts - 1 );
            } else {
                return location;
            }
        }
    }

    /**
     * Construct the signature for a String.
     *
     * @param s the String
     * @return the signature
     */
    protected String constructSignature(
            String s )
    {
        byte [] sAsBytes = s.getBytes();

        byte [] toSign = new byte[ theSigningSecret.length + sAsBytes.length ];
        System.arraycopy( theSigningSecret,  0, toSign, 0, theSigningSecret.length );
        System.arraycopy( sAsBytes,          0, toSign, theSigningSecret.length, sAsBytes.length );

        byte [] signature = theDigest.digest( toSign );
        String  ret       = StringHelper.toHexString( signature );

        return ret;
    }

    /**
     * Obfuscate a String.
     *
     * @param s the String
     * @return the obfuscated String
     * @see #unobfuscate(String)
     */
    protected String obfuscate(
            String s )
    {
        try {
            byte [] data = theObfuscator.doFinal( s.getBytes() );
            String  ret  = StringHelper.toHexString( data );

            return ret;

        } catch( Throwable t ) {
            log.error( t );
            return s;
        }
    }

    /**
     * Unobfuscate a String.
     *
     * @param s the obfuscated String
     * @return the unobfuscated String
     * @see #obfuscate(String)
     */
    protected String unobfuscate(
            String s )
    {
        try {
            byte[] data = StringHelper.hexStringToBytes( s );
            byte[] ret  = theUnobfuscator.doFinal( data );

            return new String( ret );

        } catch( Throwable t ) {
            log.error( t );
            return s;
        }
    }

    /**
     * Obtain a suitable timestamp for a to-be-created signature.
     *
     * @return the timestamp
     * @see #isTimestampExpired(String)
     */
    protected String obtainTimestamp()
    {
        return String.valueOf( System.currentTimeMillis() );
    }

    /**
     * Determine whether this timestamp is not within the allowed time window.
     *
     * @param timestamp the timestamp
     * @return true if the timestamp is too old
     * @see #obtainTimestamp()
     */
    protected boolean isTimestampExpired(
            String timestamp )
    {
        long ts    = Long.parseLong( timestamp );
        long delta = System.currentTimeMillis() - ts;

        return delta >= theMaxTimeElapsed;
    }

    /**
     * Helper method to parse a hex string.
     *
     * @param name name of the parameter, for error reporting
     * @param hex the value
     * @return the parsed value
     */
    private static byte [] fromHex(
            String name,
            String hex )
    {
        if( hex == null ) {
            throw new NullPointerException( "No " + name + " provided" );
        }

        int len = hex.length();
        if( len % 2 == 1 ) {
            throw new IllegalArgumentException( "Parameter " + name + " must consist of an even number of hex characters" );
        }

        byte [] ret = new byte[ len/2 ];
        for( int i=0 ; i<ret.length ; ++i ) {
            char c1 = hex.charAt( 2*i );
            char c2 = hex.charAt( 2*i + 1 );

            ret[i] = (byte) (( nibble( name, c1, 2*i ) << 4 ) + nibble( name, c2, 2*i+1 ));
        }
        return ret;
    }

    /**
     * Helper method to convert a hex char to an int
     *
     * @param name name of the parameter, for error reporting
     * @param hex the value
     * @param index the index in the string
     * @return the parsed value
     */
    private static byte nibble(
            String name,
            char   hex,
            int    index )
    {
        if( hex >= 'A' && hex <= 'F' ) {
            return (byte) ( hex + 10 - 'A' );
        }
        if( hex >= 'a' && hex <= 'f' ) {
            return (byte) ( hex + 10 - 'a' );
        }
        if( hex >= '0' && hex <= '9' ) {
            return (byte) ( hex - '0' );
        }
        throw new IllegalArgumentException( "Parameter " + name + " contains non-hex character at index " + index );
    }

    /**
     * The secret for signing.
     */
    protected byte [] theSigningSecret;

    /**
     * Knows how to obfuscate.
     */
    protected Cipher theObfuscator;

    /**
     * Knows how to un-obfuscate.
     */
    protected Cipher theUnobfuscator;

    /**
     * The number of milliseconds that a timestamp on a signed URL is still acceptable.
     */
    protected long theMaxTimeElapsed;

    /**
     * The digest algorithm to use.
     */
    protected static final MessageDigest theDigest;
    static {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance( "SHA-1" );
        } catch( NoSuchAlgorithmException ex ) {
            log.error( ex );
            digest = null;
        }
        theDigest = digest;
    }

    /**
     * Name of the URL argument that contains the obfuscated signature.
     */
    public static final String TOKEN_KEYWORD_EQUALS = "token=";

    /**
     * Name of the transformation used for obfuscation.
     */
    final String OBFUSCATION_TRANSFORM = "AES/CBC/PKCS5Padding";

    /**
     * The init vector for the AES key: 16 bytes
     */
    private static final IvParameterSpec INIT_VECTOR_SPEC = new IvParameterSpec( new byte[] {
         19,   0,  99, -12,
         32,-122, -22,   0,
         11,  -7,  57,  48,
         81, -29,  24,  77
    });

}
