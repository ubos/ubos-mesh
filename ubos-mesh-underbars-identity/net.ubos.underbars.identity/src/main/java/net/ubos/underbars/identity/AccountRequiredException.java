//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity;

import java.io.IOException;

/**
 * Abort processing, as an account is required and the user does not have one.
 */
public class AccountRequiredException
    extends
        IOException
{}
