//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.helpers;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.underbars.vl.ViewletModel;
import net.ubos.util.StringHelper;

/**
 * Determine whether the current user has an account, and if given, whether the Account is in the requested status.
 */
public class HasAccountHelper
    extends
        AbstractIdentityHelper<ViewletModel>
{
    /**
     * Constructor.
     */
    public HasAccountHelper()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean apply(
            ViewletModel model,
            Options      options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String statusString = optionalHash( STATUS_ARGNAME, null, options );

        boolean ret = hasAccountStatus( statusString );
        return ret;
    }

    /**
     * Name of the argument that indicates the required status of the account.
     */
    public static final String STATUS_ARGNAME = "status";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        STATUS_ARGNAME
    };
}
