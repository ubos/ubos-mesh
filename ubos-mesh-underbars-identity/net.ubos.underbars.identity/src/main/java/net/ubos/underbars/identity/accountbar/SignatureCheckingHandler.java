//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.accountbar;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

/**
 * Checks that incoming requests that are signed are signed validly, and if not, redirects
 */
public class SignatureCheckingHandler
    implements
        HttpHandler
{
    /**
     * @next the next handler
     */
    public SignatureCheckingHandler(
            final RequestSigner signer,
            final HttpHandler   next )
    {
        theSigner = signer;
        theNext   = next;
    }

    @Override
    public void handleRequest(
            final HttpServerExchange hse )
        throws
            Exception
    {
        try {
            theSigner.extractSignedRequest( hse );

            theNext.handleRequest( hse );

        } catch( InvalidSignatureException ex ) {
            hse.setStatusCode( StatusCodes.TEMPORARY_REDIRECT );
            hse.getResponseHeaders().add( Headers.LOCATION, ex.getRedirectToUrl() );
        }
    }

    /**
     * Knows how to check signatures.
     */
    private final RequestSigner theSigner;

    /**
     * The next handler to invoke.
     */
    private final HttpHandler theNext;
}
