//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.helpers;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.util.StringHelper;

/**
 * Shows information about the current user.
 */
public class UserHelper
    extends
        AbstractHandlebarsHelper<Object>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Object apply(
            Object  context,
            Options options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        String display     = options.hash( DISPLAY_ARGNAME, DISPLAY_ARGNAME_NICKNAME );
        String nullContent = options.hash( NULL_CONTENT_ARGNAME, "" );

        Options.Buffer buffer = options.buffer();
        String         content;

        ThreadIdentityManager.Who who = ThreadIdentityManager.getWho();
        if( who == null ) {
            content = nullContent;

        } else {
            MeshObject princ = who.getPrincipal();
            switch( display ) {
                case DISPLAY_ARGNAME_NICKNAME:
                    StringValue nick = (StringValue) princ.getPropertyValue( IdentitySubjectArea.PRINCIPAL_NICKNAME );
                    if( nick == null ) {
                        content = nullContent;
                    } else {
                        content = nick.value();
                    }
                    break;

                case DISPLAY_ARGNAME_EMAIL:
                    MeshObject  profile = princ.traverse( IdentitySubjectArea.PRINCIPALPROFILE_FOR_PRINCIPAL_D ).getSingleMember();
                    StringValue email   = profile == null ? null : (StringValue) profile.getPropertyValue( IdentitySubjectArea.PRINCIPALPROFILE_EMAILADDRESS );
                    if( email == null ) {
                        content = nullContent;
                    } else {
                        content = email.value();
                    }
                    break;

                default:
                    content = "???";
                    break;
            }
        }
        buffer.append( content );

        return buffer;
    }

    /**
     * Name of the argument indicating what information about the admin user should be shown.
     */
    public static final String DISPLAY_ARGNAME = "display";

    /**
     * Values for DISPLAY_ARGNAME.
     */
    public static final String DISPLAY_ARGNAME_NICKNAME = "nickname";
    public static final String DISPLAY_ARGNAME_EMAIL    = "email";

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = {
        DISPLAY_ARGNAME,
        NULL_CONTENT_ARGNAME
    };
}
