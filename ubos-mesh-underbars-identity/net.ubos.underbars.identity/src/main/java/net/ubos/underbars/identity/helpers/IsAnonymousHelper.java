//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.identity.helpers;

import com.github.jknack.handlebars.Options;
import java.io.IOException;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.underbars.helpers.AbstractHandlebarsHelper;
import net.ubos.underbars.skin.SkinModel;
import net.ubos.util.StringHelper;

/**
 * Determine whether the current user is anonymous.
 */
public class IsAnonymousHelper
    extends
        AbstractHandlebarsHelper<SkinModel>
{
    /**
     * Constructor.
     */
    public IsAnonymousHelper()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean apply(
            SkinModel model,
            Options   options )
        throws
            IOException
    {
        checkArguments( options, MANDATORY_ARGNAMES, OPTIONAL_ARGNAMES );

        ThreadIdentityManager.Who who = ThreadIdentityManager.getWho();

        return who == null;
    }

    /**
     * Names of the mandatory arguments.
     */
    private static final String [] MANDATORY_ARGNAMES = StringHelper.EMPTY_ARRAY;

    /**
     * Names of the optional arguments.
     */
    private static final String [] OPTIONAL_ARGNAMES = StringHelper.EMPTY_ARRAY;
}
