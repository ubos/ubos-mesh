MeshAccountBar = new function( context ) {
    var _DIALOG_ELEMENT_ID     = 'MeshAccountBar-dialog';
    var _OVERLAY_ELEMENT_ID    = 'MeshAccountBar-dialog-overlay';
    var _FORM_ID               = 'MeshAccountBar-dialog-form';
    var _BUSY_ELEMENT_ID       = 'MeshAccountBar-dialog-busy';
    var _ERROR_SECTION_ID      = 'MeshAccountBar-dialog-error-section';
    var _ERROR_ELEMENT_ID      = 'MeshAccountBar-dialog-error';
    var _MESSAGE_SECTION_ID    = 'MeshAccountBar-dialog-message-section';
    var _MESSAGE_ELEMENT_ID    = 'MeshAccountBar-dialog-message';

    var _dialogGenerated = false;
    var _theCurrentMode  = null;
    var _theDirWithSlash;

    var _ensureDialog = function() {
        if( _dialogGenerated ) {
            return;
        }
        _dialogGenerated = true;
        // Overlay
        var body = document.getElementsByTagName( 'body' )[0];

        var dialogElement  = document.createElement( 'div' );
        var overlayElement = document.createElement( 'div' );

        dialogElement.id  = _DIALOG_ELEMENT_ID;
        overlayElement.id = _OVERLAY_ELEMENT_ID;

        dialogElement.innerHTML =
              "<div id='" + _FORM_ID + "' onKeyPress='MeshAccountBar._checkSubmit( event )'>\n"
            + "    <div class='title'>\n"
            + "        <h1 class='mode modeA modeD'>Please sign in:</h1>\n"
            + "        <h1 class='mode modeB'>Register for this site:</h1>\n"
            + "        <h1 class='mode modeC'>Message:</h1>\n"
            + "        <h1 class='mode modeE'>Change password:</h1>\n"
            + "        <h1 class='mode modeF'>Password reset:</h1>\n"
            + "        <h1 class='mode modeG'>Sign out:</h1>\n"
            + "        <h1 class='mode modeH'>Set password:</h1>\n"
            + "        <a href='javascript:MeshAccountBar._hideDialog()'>\n"
            + "            <img src='" + _theDirWithSlash + "close.png' alt='close' />\n"
            + "        </a>\n"
            + "    </div>\n"
            + "    <div class='fields'>\n"
            + "        <div class='section mode modeA modeB modeD'>\n"
            + "            <table>\n"
            + "                <tr>\n"
            + "                    <th><p><strong>E-mail:</strong></p></th>\n"
            + "                    <td class='lid'>\n"
            + "                        <input type='email' autocorrect='off' autocapitalization='off' name='lid' id='lid' value='' class='mode modeA modeB modeD' />\n"
            + "                    </td>\n"
            + "                </tr>\n"
            + "            </table>\n"
            + "        </div>\n"
            + "        <div class='section mode modeE modeH'>\n"
            + "            <table>\n"
            + "                <tr>\n"
            + "                    <th><p><strong>E-mail:</strong></p></th>\n"
            + "                    <td class='lid'>\n"
            + "                        <input type='email' autocorrect='off' autocapitalization='off' name='lid' id='lid' value='' disabled='disabled' class='mode modeE modeH' />\n"
            + "                    </td>\n"
            + "                </tr>\n"
            + "            </table>\n"
            + "        </div>\n"
            + "        <div class='section mode modeD modeE'>\n"
            + "            <input type='hidden' name='lid-credtype' value='simple-password' class='modeD modeE' />\n"
            + "            <table>\n"
            + "                <tr>\n"
            + "                    <th><p>Password:</p></th>\n"
            + "                    <td><input type='password' name='lid-credential' id='lid-credential' value='' class='mode modeD modeE' /></td>\n"
            + "                </tr>\n"
            + "            </table>\n"
            + "        </div>\n"
            + "        <div class='section mode modeE modeH'>\n"
            + "            <input type='hidden' name='lid-credtype' value='simple-password' class='modeH' />\n"
            + "            <input type='hidden' name='token' value='' class='modeH' />\n"
            + "            <table>\n"
            + "                <tr>\n"
            + "                    <th><p>New password: *</p></th>\n"
            + "                    <td><input type='password' name='lid-credential1' id='lid-credential1' value='' class='mode modeE modeH' /></td>\n"
            + "                </tr>\n"
            + "                <tr>\n"
            + "                    <th><p>Again: *</p></th>\n"
            + "                    <td><input type='password' name='lid-credential2' id='lid-credential2' value='' class='mode modeE modeH' /></td>\n"
            + "                </tr>\n"
            + "            </table>\n"
            + "        </div>\n"
            + "        <div class='section mode modeF'>\n"
            + "            <input type='hidden' name='lid' value='' disabled='disabled' class='mode modeF' />\n"
            + "            <p>Do you want us to send you e-mail with a link that allows you to reset your password?</p>\n"
            + "        </div>\n"
            + "        <div class='section mode modeG'>\n"
            + "            <input type='hidden' name='lid' value='' disabled='disabled' class='mode modeG' />\n"
            + "            <p>Do you wish to sign out?</p>\n"
            + "        </div>\n"
            + "        <ul class='hints'>\n"
            + "            <li class='mode modeA modeD'><a tabindex=\"-1\" href='javascript:MeshAccountBar._switchOn( \"modeB\" )'>Not registered?</a></li>\n"
            + "            <li class='mode modeB'><a tabindex=\"-1\" href='javascript:MeshAccountBar._switchOn( \"modeA\" )'>Already registered?</a></li>\n"
            + "            <li class='mode modeD'><a tabindex=\"-1\" href='javascript:MeshAccountBar._switchOn( \"modeF\" )'>Forgotten password?</a></li>\n"
            + "        </ul>\n"
            + "        <div class='section error' id='" + _ERROR_SECTION_ID + "'>\n"
            + "            <table>\n"
            + "                <tr>\n"
            + "                    <th><p><img src='" + _theDirWithSlash + "exclamation.png'/></p></th>\n"
            + "                    <td id='" + _ERROR_ELEMENT_ID + "'>\n"
            + "                    </td>\n"
            + "                </tr>\n"
            + "            </table>\n"
            + "        </div>\n"
            + "        <div class='section message' id='" + _MESSAGE_SECTION_ID + "'>\n"
            + "            <table>\n"
            + "                <tr>\n"
            + "                    <th><p><img src='" + _theDirWithSlash + "tick.png'/></p></th>\n"
            + "                    <td id='" + _MESSAGE_ELEMENT_ID + "'>\n"
            + "                    </td>\n"
            + "                </tr>\n"
            + "            </table>\n"
            + "        </div>\n"
            + "        </div>\n"
            + "    </div>\n"
            + "    <div class='section submit mode modeA modeB modeD modeE modeF modeG modeH'>\n"
            + "        <table>\n"
            + "            <tr>\n"
            + "                <td class='busy'>\n"
            + "                    <img id='" + _BUSY_ELEMENT_ID + "' src='" + _theDirWithSlash + "wait24trans.gif' alt='[busy]' style='display: none'/>\n"
            + "                </td>\n"
            + "                <td>\n"
            + "                    <input type='submit' value='Sign in'  name='lid-submit' onclick='MeshAccountBar._submit()' class='lid-submit mode modeA modeD'/>\n"
            + "                    <input type='submit' value='Register' name='lid-submit' onclick='MeshAccountBar._submit()' class='lid-submit mode modeB'/>\n"
            + "                    <input type='submit' value='Save'     name='lid-submit' onclick='MeshAccountBar._submit()' class='lid-submit mode modeE modeH'/>\n"
            + "                    <input type='submit' value='Send'     name='lid-submit' onclick='MeshAccountBar._submit()' class='lid-submit mode modeF'/>\n"
            + "                    <input type='submit' value='Sign out' name='lid-submit' onclick='MeshAccountBar._submit()' class='lid-submit mode modeG'/>\n"
            + "                </td>\n"
            + "                <td class='busy'>&nbsp;</td>\n"
            + "            </tr>\n"
            + "        </table>\n"
            + "    </div>\n"
            + "</div>\n";

        body.insertBefore( dialogElement,  body.firstChild );
        body.insertBefore( overlayElement, body.firstChild );
    };

    var _setDialog = function( value ) {
        _ensureDialog();

        var visibleValue;
        var displayValue;
        if( value ) {
            visibleValue = 'visible';
            displayValue = 'block';
        } else {
            visibleValue = 'hidden';
            displayValue = 'none';
        }
        var overlay = document.getElementById( _OVERLAY_ELEMENT_ID );
        if( overlay ) {
            overlay.style.visibility = visibleValue;
            overlay.style.display    = displayValue;
        }
        var dialog = document.getElementById( _DIALOG_ELEMENT_ID );
        if( dialog ) {
            dialog.style.visibility = visibleValue;
            dialog.style.display    = displayValue;

            var fields = dialog.getElementsByTagName( 'input' );
            for( var i=0 ; i<fields.length ; ++i ) {
                if( fields[i].type == 'password' ) {
                    fields[i].value = '';
                }
            }
        }
    };

    var _setError = function( message ) {
        var errorSection = document.getElementById( _ERROR_SECTION_ID );
        var errorDiv     = document.getElementById( _ERROR_ELEMENT_ID );

        if( message ) {
            errorSection.style.display = 'block';
            errorDiv.innerHTML         = message;
        } else {
            errorSection.style.display = '';
            errorDiv.innerHTML         = '';
        }
    };

    var _setMessage = function( message ) {
        var messageSection = document.getElementById( _MESSAGE_SECTION_ID );
        var messageDiv     = document.getElementById( _MESSAGE_ELEMENT_ID );

        if( message ) {
            messageSection.style.display = 'block';
            messageDiv.innerHTML         = message;
        } else {
            messageSection.style.display = '';
            messageDiv.innerHTML         = '';
        }
    };

    this._switchOn = function( arg ) {
        var dialog = document.getElementById( _DIALOG_ELEMENT_ID );

        _setError( null );
        _setMessage( null );

        var lidElements = document.getElementsByName( 'lid' );
        var lid;
        for( var i=0 ; i<lidElements.length ; ++i ) {
            if( lidElements[i].style.visibility != 'hidden' ) {
                lid = lidElements[i].value;
                break;
            }
        }

        if( dialog ) {
            var modeElsAll = dialog.getElementsByClassName( 'mode' );
            for( var i=0 ; i<modeElsAll.length ; ++i ) {
                modeElsAll[i].className = modeElsAll[i].className.replace( / block/, '' ).replace( / inline/, '' );
            }

            var modeEls = dialog.getElementsByClassName( arg );
            for( var i=0 ; i<modeEls.length ; ++i ) {
                if( modeEls[i].nodeName == 'LI' || modeEls[i].nodeName == 'li' ) {
                    modeEls[i].className += ' inline';
                } else {
                    modeEls[i].className += ' block';
                }
            }
        }
        for( var i=0 ; i<lidElements.length ; ++i ) {
            if( lidElements[i].style.visibility != 'hidden' ) {
                lidElements[i].value = lid;
                lidElements[i].focus();
            }
        }

        _theCurrentMode = arg;

        if( _theCurrentMode == 'modeA' || _theCurrentMode == 'modeB' ) {
            var lidElements = document.getElementsByName( 'lid' );
            for( var i=0 ; i<lidElements.length ; ++i ) {
                if( lidElements[i].style.visibility != 'hidden' ) {
                    lidElements[i].focus();
                }
            }
        } else if( _theCurrentMode == 'modeD' ) {
            var lidElements = document.getElementsByName( 'lid-credential' );
            for( var i=0 ; i<lidElements.length ; ++i ) {
                if( lidElements[i].style.visibility != 'hidden' ) {
                    lidElements[i].focus();
                }
            }
        } else if( _theCurrentMode == 'modeE' || _theCurrentMode == 'modeH') {
            var lidElements = document.getElementsByName( 'lid-credential1' );
            for( var i=0 ; i<lidElements.length ; ++i ) {
                if( lidElements[i].style.visibility != 'hidden' ) {
                    lidElements[i].focus();
                }
            }
        }
    };

    var _startWaitingForResponse = function() {
        var buttons = document.getElementsByName( 'lid-submit' );
        for( var i=0 ; i<buttons.length ; ++i ) {
            buttons[i].disabled = true;
        }
        var busyElement = document.getElementById( _BUSY_ELEMENT_ID );
        busyElement.style.display = 'inline';

        _setError( null );
    }

    var _doneWaitingForResponse = function() {
        var buttons = document.getElementsByName( 'lid-submit' );
        for( var i=0 ; i<buttons.length ; ++i ) {
            buttons[i].disabled = false;
        }
        var busyElement = document.getElementById( _BUSY_ELEMENT_ID );
        busyElement.style.display = 'none';

        var creds = document.getElementsByName( 'lid-credential' );
        for( var i=0 ; i<creds.length ; ++i ) {
            creds[i].value = "";
        }
        var creds1 = document.getElementsByName( 'lid-credential1' );
        for( var i=0 ; i<creds1.length ; ++i ) {
            creds1[i].value = "";
        }
        var creds2 = document.getElementsByName( 'lid-credential2' );
        for( var i=0 ; i<creds2.length ; ++i ) {
            creds2[i].value = "";
        }
    }

    this._checkSubmit = function( event ) {
        if( !event ) {
            return;
        }
        if( event.keyCode == 13 ) {
            this._submit();
        }
    }

    this._submit = function() {
        _startWaitingForResponse();

        var outerThis = this;

        var req = new XMLHttpRequest();
        req.open("POST", _theDirWithSlash + "account-bar-handler", true );
        req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        req.onreadystatechange = function() {
            if( req.readyState == 4 ) {
                outerThis._responseReceived( req );
            }
        };
        var toSend = 'mode=' + _theCurrentMode + "&location=" + window.location;
        var dialog = document.getElementById( _DIALOG_ELEMENT_ID );
        if( dialog ) {
            var inputs = dialog.getElementsByTagName( 'input' );
            for( var i=0 ; i<inputs.length ; ++i ) {
                if( inputs[i].className.indexOf( _theCurrentMode ) >= 0 ) {
                    toSend += '&' + inputs[i].name + '=' + encodeURIComponent( inputs[i].value );
                }
            }
        }
        req.send( toSend );
    };

    this._responseReceived = function( req ) {
        var json = eval( '(' + req.responseText + ')' );

        if( json.http_location ) {
            window.location = json.http_location;
            // don't _doneWaitingForResponse();
            return;
        }
        _doneWaitingForResponse();

        if( json.mode ) {
            MeshAccountBar._switchOn( json.mode );
        }

        if( json.info_i18nmessage_html ) {
            _setMessage( json.info_i18nmessage_html );
        } else {
            _setMessage( null );
        }
        if( json.error_i18nmessage_html ) {
            _setError( json.error_i18nmessage_html );
        } else {
            _setError( null );
        }
    };

    this._initialize = function( currentScript ) {
        _theDirWithSlash = currentScript.src.match( /.*\// );

        var oldonload = window.onload;
        if( typeof oldonload != 'function' ) {
            window.onload = MeshAccountBar._onload;
        } else {
            window.onload = function() {
                oldonload();
                MeshAccountBar._onload();
            }
        }
    };

    this._onload = function() {
        if( _theCurrentMode != null ) {
            _setDialog( true );
            MeshAccountBar._switchOn( _theCurrentMode );
        }
    };

    this._hideDialog = function() {
        _setDialog( false );

        _doneWaitingForResponse();

        _theCurrentMode = null;
    };

    this.showLogin = function() {
        _setDialog( true );
       MeshAccountBar._switchOn( 'modeA' );
    };

    this.showSetPassword = function( lid, token ) {
        _setDialog( true );
        MeshAccountBar._switchOn( 'modeH' );

        var lidElements = document.getElementsByName( 'lid' );
        for( var i=0 ; i<lidElements.length ; ++i ) {
            if( lidElements[i].style.visibility != 'hidden' ) {
                lidElements[i].value = lid;
            }
        }
        lidElements = document.getElementsByName( 'token' );
        for( var i=0 ; i<lidElements.length ; ++i ) {
            if( lidElements[i].style.visibility != 'hidden' ) {
                lidElements[i].value = token;
            }
        }
    };

    this.showLogout = function() {
        _setDialog( true );
        MeshAccountBar._switchOn( 'modeG' );
    }
};
MeshAccountBar._initialize( document.currentScript );
