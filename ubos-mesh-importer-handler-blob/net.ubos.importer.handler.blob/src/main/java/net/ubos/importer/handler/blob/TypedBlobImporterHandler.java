//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.blob;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Map;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.model.Blob.BlobSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.util.StreamUtils;

/**
 * Knows how to handle other content as blob. This is typed; compare DefaultFileImporterHandler.
 */
public class TypedBlobImporterHandler
    extends
        AbstractBasicFileImporterHandler
{
    /**
     * Constructor with default.
     *
     * @param okScore when importing works, what score should be reported
     */
    public TypedBlobImporterHandler(
            double okScore )
    {
        super( DEFAULT_FILENAMES_PATTERN, okScore );

        theTypeMap      = DEFAULT_TYPE_MAP;
        theFallbackType = DEFAULT_FALLBACK_TYPE;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     */
    public TypedBlobImporterHandler(
            String  filenamePattern,
            double  okScore )
    {
        super( Pattern.compile( filenamePattern ), okScore );

        theTypeMap      = DEFAULT_TYPE_MAP;
        theFallbackType = DEFAULT_FALLBACK_TYPE;
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     * @param typeMap maps file extensions to the EntityType with which the MeshObject will be blessed
     * @param fallbackType the EntityType with which the Meshobject will be blessed if no other match is found
     */
    public TypedBlobImporterHandler(
            Pattern                filenamePattern,
            double                 okScore,
            Map<String,EntityType> typeMap,
            EntityType             fallbackType )
    {
        super( filenamePattern, okScore );

        theTypeMap      = typeMap;
        theFallbackType = fallbackType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream() ) {
            return IMPOSSIBLE;
        }
        double ret = theOkScore;

        String name    = toBeImported.getName();
        int    lastDot = name.lastIndexOf( "." );
        String mime    = Files.probeContentType( Paths.get( name ));

        EntityType type = theFallbackType;

        if( lastDot >= 0 ) {
            String ext = name.substring( lastDot+1 ).toLowerCase();
            if( theTypeMap.containsKey( ext )) {
                type = theTypeMap.get( ext );
            }
        }

        MeshObject hereObject = toBeImported.getHereMeshObject();
        hereObject.bless( type );
        hereObject.setPropertyValue(
                BlobSubjectArea.BLOBOBJECT_CONTENT,
                BlobSubjectArea.BLOBOBJECT_CONTENT_type.createBlobValue(
                        StreamUtils.slurp( toBeImported.createStream() ),
                        mime));

        return ret;
    }

    /**
     * The type map in use.
     */
    protected final Map<String,EntityType> theTypeMap;

    /**
     * The default type.
     */
    protected final EntityType theFallbackType;

    /**
     * The default pattern of file names we support.
     */
    public static final Pattern DEFAULT_FILENAMES_PATTERN = Pattern.compile( ".*\\.(gif|jpg|mp4|png)$" );

    /**
     * Match file extension to Blob type.
     */
    public static final Map<String,EntityType> DEFAULT_TYPE_MAP = Map.of(
            "mp4", BlobSubjectArea.VIDEO );

    /**
     * If the type map has no entry, use this type.
     */
    public static final EntityType DEFAULT_FALLBACK_TYPE = BlobSubjectArea.PHOTO;
}
