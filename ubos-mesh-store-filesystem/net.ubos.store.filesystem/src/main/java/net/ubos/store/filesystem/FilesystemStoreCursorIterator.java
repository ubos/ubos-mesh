//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.filesystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.store.AbstractStoreValueCursorIterator;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.Log;
import net.ubos.util.tree.FileTreeFacade;
import net.ubos.util.tree.TreeFacade;
import net.ubos.util.tree.TreeFacadeCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;

/**
 * Iterates over the content of a FilesystemStore.
 */
public class FilesystemStoreCursorIterator
        extends
            AbstractStoreValueCursorIterator<StoreValue>
{
    private static final Log log = Log.getLogInstance( FilesystemStoreCursorIterator.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param store the FilesystemStore to iterate over
     * @param startsWith the prefix to filter the keys by
     * @return the created FilesystemStoreIterator
     */
    public static FilesystemStoreCursorIterator create(
            FilesystemStore store,
            String          startsWith )
    {
        String topPrefix = store.getTopDirectory().getAbsolutePath() + '/';

        TreeFacade<File>     facade   = FileTreeFacade.create( store.getTopDirectory() );
        CursorIterator<File> delegate = TreeFacadeCursorIterator.create( facade );

        delegate = FilteringCursorIterator.create(
                delegate,
                ( File candidate ) -> {
                        if( !candidate.getAbsolutePath().startsWith( topPrefix + startsWith )) {
                            return false;
                        }
                        return !candidate.isDirectory();
                } );

        return new FilesystemStoreCursorIterator( store, delegate );
    }

    /**
     * Constructor. Start at the beginning.
     *
     * @param store the FilesystemStore to iterate over
     * @param delegate the delegate Iterator that iterates over the file system
     */
    protected FilesystemStoreCursorIterator(
            FilesystemStore      store,
            CursorIterator<File> delegate )
    {
        super( store );

        theDelegate = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilesystemStore getStore()
    {
        return (FilesystemStore) theStore;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue peekNext()
        throws
            NoSuchElementException
    {
        StoreValue ret      = null;
        boolean    success = false;

        try {
            File found = theDelegate.peekNext();
            if( found != null ) {
                try {
                    ret = getStore().getStoreValueMapper().readStoreValue( found );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
            success = true;

        } finally {
            firePeekNextPerformed( null, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue peekPrevious()
        throws
            NoSuchElementException
    {
        StoreValue ret      = null;
        boolean    success = false;

        try {
            File found = theDelegate.peekPrevious();
            if( found != null ) {
                try {
                    ret = getStore().getStoreValueMapper().readStoreValue( found );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
            success = true;

        } finally {
            firePeekPreviousPerformed( null, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValue> peekNext(
            int n )
    {
        List<File>       found = theDelegate.peekNext( n );
        List<StoreValue> ret   = new ArrayList<>( found.size() );

        for( File current : found ) {
            StoreValue decoded = null;
            if( current != null ) {
                try {
                    decoded = getStore().getStoreValueMapper().readStoreValue( current );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
            ret.add( decoded );
        }

        firePeekNextPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValue> peekPrevious(
            int n )
    {
        List<File>       found = theDelegate.peekPrevious( n );
        List<StoreValue> ret   = new ArrayList<>( found.size() );

        for( File current : found ) {
            StoreValue decoded = null;
            if( current != null ) {
                try {
                    decoded = getStore().getStoreValueMapper().readStoreValue( current );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
            ret.add( decoded );
        }

        firePeekPreviousPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        boolean ret = theDelegate.hasNext();
        
        fireHasNextPerformed( null, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious()
    {
        boolean ret = theDelegate.hasPrevious();
        
        fireHasPreviousPerformed( null, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext(
            int n )
    {
        boolean ret = theDelegate.hasNext( n );
        
        fireHasNextPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPrevious(
            int n )
    {
        boolean ret = theDelegate.hasPrevious( n );
        
        fireHasPreviousPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue next()
        throws
            NoSuchElementException
    {
        StoreValue ret      = null;
        boolean    success = false;

        try {
            File found = theDelegate.next();
            if( found != null ) {
                try {
                    ret = getStore().getStoreValueMapper().readStoreValue( found );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
        } finally {
            fireNextPerformed( null, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValue> next(
            int n )
    {
        List<File>       found = theDelegate.next( n );
        List<StoreValue> ret   = new ArrayList<>( found.size() );

        for( File current : found ) {
            StoreValue decoded = null;
            if( current != null ) {
                try {
                    decoded = getStore().getStoreValueMapper().readStoreValue( current );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
            ret.add( decoded );
        }

        fireNextPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue previous()
        throws
            NoSuchElementException
    {
        StoreValue ret      = null;
        boolean    success = false;

        try {
            File found = theDelegate.previous();
            if( found != null ) {
                try {
                    ret = getStore().getStoreValueMapper().readStoreValue( found );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
            success = true;

        } finally {
            firePreviousPerformed( null, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StoreValue> previous(
            int n )
    {
        List<File>       found = theDelegate.previous( n );
        List<StoreValue> ret   = new ArrayList<>( found.size() );

        for( File current : found ) {
            StoreValue decoded = null;
            if( current != null ) {
                try {
                    decoded = getStore().getStoreValueMapper().readStoreValue( current );
                } catch( IOException ex ) {
                    log.error( ex );
                }
            }
            ret.add( decoded );
        }
        
        firePeekNextPerformed( null, n, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveBy(
            int n )
        throws
            NoSuchElementException
    {
        boolean success = false;

        try {
            theDelegate.moveBy( n );
            success = true;
            
        } finally {
            fireMoveByPerformed( null, n, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            StoreValue pos )
        throws
            NoSuchElementException
    {
        int     ret     = 0;
        boolean success = false;

        try {
            ret = moveToBefore( pos.getKey() );
            success = true;

        } finally {
            fireMoveToBeforePerformed( null, pos, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            StoreValue pos )
        throws
            NoSuchElementException
    {
        int     ret     = 0;
        boolean success = false;

        try {
            ret = moveToAfter( pos.getKey() );
            success = true;
            
        } finally {
            fireMoveToAfterPerformed( null, pos, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove()
        throws
            UnsupportedOperationException,
            IllegalStateException
    {
        boolean success = false;
        
        try {
            theDelegate.remove();
            success = true;

        } finally {
            fireRemovePerformed( null, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPositionTo(
            CursorIterator<StoreValue> position )
        throws
            IllegalArgumentException
    {
        boolean success = false;

        try {
            if( position.hasNext() ) {
                StoreValue next = position.peekNext();

                File delegateNext = getStore().getKeyFileMapper().keyToFile( next.getKey() );
                theDelegate.moveToBefore( delegateNext );

            } else if( position.hasPrevious() ) {
                StoreValue next = position.peekPrevious();

                File delegateNext = getStore().getKeyFileMapper().keyToFile( next.getKey() );
                theDelegate.moveToAfter( delegateNext );

            } else {
                theDelegate.moveToBeforeFirst();
            }
            success = true;

        } finally {
            fireSetPositionToPerformed( null, position, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBefore(
            String key )
        throws
            NoSuchElementException
    {
        int     ret     = 0;
        boolean success = false;

        try {
            File delegatePos = getStore().getKeyFileMapper().keyToFile( key );

            ret = theDelegate.moveToBefore( delegatePos );
            success = true;
        } finally {
            fireMoveToBeforePerformed( null, key, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfter(
            String key )
        throws
            NoSuchElementException
    {
        int     ret     = 0;
        boolean success = false;

        try {
            File delegatePos = getStore().getKeyFileMapper().keyToFile( key );

            ret = theDelegate.moveToAfter( delegatePos );
            success = true;

        } finally {
            fireMoveToAfterPerformed( null, key, ret, success );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilesystemStoreCursorIterator createCopy()
    {
        CursorIterator<File> delegateCopy = theDelegate.createCopy();

        return new FilesystemStoreCursorIterator( getStore(), delegateCopy );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToBeforeFirst()
    {
        int ret = theDelegate.moveToBeforeFirst();
        
        fireMoveToBeforeFirstPerformed( null, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int moveToAfterLast()
    {
        int ret = theDelegate.moveToAfterLast();

        fireMoveToAfterLastPerformed( null, ret );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final FilesystemStoreCursorIterator iterator()
    {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final FilesystemStoreCursorIterator getIterator()
    {
        return iterator();
    }

    /**
     * The delegate iterator.
     */
    protected CursorIterator<File> theDelegate;
}
