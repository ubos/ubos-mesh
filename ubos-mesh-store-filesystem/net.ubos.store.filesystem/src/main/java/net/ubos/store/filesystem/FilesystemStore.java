//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.filesystem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import net.ubos.store.AbstractStore;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreKeyExistsAlreadyException;
import net.ubos.store.StoreValue;
import net.ubos.store.util.SimpleStoreValueStreamer;
import net.ubos.store.util.StoreValueStreamer;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.util.tree.FileTreeFacade;
import net.ubos.util.tree.TreeFacade;
import net.ubos.util.tree.TreeFacadeCursorIterator;

/**
 * Filesystem implementation of the Store interface.
 */
public class FilesystemStore
        extends
            AbstractStore<StoreValue>
        implements
            CanBeDumped
{
    private static final Log log = Log.getLogInstance( FilesystemStore.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param subDir the subdirectory in the file system that becomes the top mapping directory.
     * @return the created FilesystemStore
     */
    public static FilesystemStore create(
            File subDir )
    {
        return new FilesystemStore(
                subDir,
                SubdirectoryKeyFileMapper.create( subDir ),
                SimpleStoreValueStreamer.create() );
    }

    /**
     * Factory method.
     *
     * @param subDir the subdirectory in the file system that becomes the top mapping directory
     * @param keyMapper maps Store keys into file system paths
     * @param storeValueStreamer streams StoreValues to file content, and vice versa
     * @return the created FilesystemStore
     */
    public static FilesystemStore create(
            File               subDir,
            KeyFileMapper      keyMapper,
            StoreValueStreamer storeValueStreamer )
    {
        return new FilesystemStore( subDir, keyMapper, storeValueStreamer );
    }

    /**
     * Constructor.
     *
     * @param subDir the subdirectory in the file system that becomes the top mapping directory
     * @param keyMapper maps Store keys into file system paths
     * @param storeValueStreamer streams StoreValues to file content, and vice versa
     */
    protected FilesystemStore(
            File               subDir,
            KeyFileMapper      keyMapper,
            StoreValueStreamer storeValueStreamer )
    {
        theSubDir             = subDir;
        theKeyMapper          = keyMapper;
        theStoreValueStreamer = storeValueStreamer;

        if( log.isTraceEnabled() ) {
            log.traceConstructor( this, "constructor" );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeHard()
            throws
                IOException
    {
        if( !theSubDir.exists() ) {
            theSubDir.mkdirs();

        } else if( !theSubDir.isDirectory() ) {
            throw new IOException( "Cannot initialize FilesystemStore at " + theSubDir.getAbsolutePath() + ": file is in the way" );

        } else {
            File [] found = theSubDir.listFiles();
            for( int i = 0 ; i<found.length ; ++i ) {
                deleteRecursively( found[i] );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeIfNecessary()
            throws
                IOException
    {
        if( !theSubDir.exists() ) {
            theSubDir.mkdirs();

        } else if( !theSubDir.isDirectory() ) {
            throw new IOException( "Cannot initialize FilesystemStore at " + theSubDir.getAbsolutePath() + ": file is in the way" );
        }
    }

    /**
     * Obtain the top-level directory underneath which all data is stored.
     *
     * @return the top-level directory
     */
    public File getTopDirectory()
    {
        return theSubDir;
    }

    /**
     * Obtain the KeyFileMapper.
     *
     * @return the KeyFileMapper
     */
    public KeyFileMapper getKeyFileMapper()
    {
        return theKeyMapper;
    }

    /**
     * Obtain the StoreValueMapper.
     *
     * @return the StoreValueMapper
     */
    public StoreValueStreamer getStoreValueMapper()
    {
        return theStoreValueStreamer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void put(
            StoreValue toStore )
        throws
            StoreKeyExistsAlreadyException,
            IOException
    {
        if( log.isInfoEnabled() ) {
            log.info( this + ".put( " + toStore + " )" );
        }

        boolean success = false;
        try {
            String key  = toStore.getKey();
            File   file = theKeyMapper.keyToFile( key );

            if( file.exists() ) {
                throw new StoreKeyExistsAlreadyException( this, key );
            }
            file.getParentFile().mkdirs();
            file.createNewFile();

            try (OutputStream stream = new FileOutputStream( file )) {
                theStoreValueStreamer.writeStoreValue( toStore, stream );
            }
            success = true;

        } finally {
            firePutPerformed( toStore, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(
            StoreValue toUpdate )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        if( log.isInfoEnabled() ) {
            log.info( this + ".update( " + toUpdate + " )" );
        }

        boolean success = false;
        try {
            String key  = toUpdate.getKey();
            File   file = theKeyMapper.keyToFile( key );

            if( !file.exists() ) {
                throw new StoreKeyDoesNotExistException( this, key );
            }

            try (OutputStream stream = new FileOutputStream( file )) {
                theStoreValueStreamer.writeStoreValue( toUpdate, stream );
            }
            success = true;

        } finally {
            fireUpdatePerformed( toUpdate, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean putOrUpdate(
            StoreValue toStoreOrUpdate )
        throws
            IOException
    {

        if( log.isInfoEnabled() ) {
            log.info( this + ".putOrUpdate( " + toStoreOrUpdate + " )" );
        }
        boolean ret     = false; // good default?
        boolean success = false;
        try {

            String key  = toStoreOrUpdate.getKey();
            File   file = theKeyMapper.keyToFile( key );

            ret = file.exists();

            if( !ret ) {
                file.createNewFile();
            }
            try (OutputStream stream = new FileOutputStream( file )) {
                theStoreValueStreamer.writeStoreValue( toStoreOrUpdate, stream );
            }

            success = true;

        } finally {
            if( ret ) {
                fireUpdatePerformed( toStoreOrUpdate, success );
            } else {
                firePutPerformed( toStoreOrUpdate, success );
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue get(
            final String key )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        if( log.isInfoEnabled() ) {
            log.info( this + ".get( " + key + " )" );
        }

        StoreValue ret = null;
        try {
            File file = theKeyMapper.keyToFile( key );

            if( !file.exists()) {
                throw new StoreKeyDoesNotExistException( this, key );
            }

            ret = theStoreValueStreamer.readStoreValue( file );

        } finally {
            fireGetPerformed( key, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(
            final String key )
        throws
            StoreKeyDoesNotExistException,
            IOException
    {
        if( log.isInfoEnabled() ) {
            log.info( this + ".delete( " + key + " )" );
        }

        boolean success = false;
        try {
            File file = theKeyMapper.keyToFile( key );

            if( !file.exists()) {
                throw new StoreKeyDoesNotExistException( this, key );
            }
            success = file.delete();

        } finally {
            fireDeletePerformed( key, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll(
            final String startsWith )
        throws
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "deleteAll" );
        }

        TreeFacade<File>     facade = FileTreeFacade.create( getTopDirectory() );
        CursorIterator<File> iter   = TreeFacadeCursorIterator.create( facade );

        // we delete them backwards, so we get the directories last
        int ret = 0;

        try {
            iter.moveToAfterLast();
            while( iter.hasPrevious() ) {
                File current = iter.previous();
                if( !theSubDir.equals( current )) {
                    if( current.isDirectory() ) {
                        if( !current.delete() ) {
                            throw new IOException( "Could not delete directory " + current );
                        }
                    } else {
                        String key = theKeyMapper.fileToKey( current );

                        if( key.startsWith( startsWith )) {
                            if( current.delete() ) {
                                ++ret;
                            } else {
                                throw new IOException( "Could not delete file " + current );
                            }
                        }
                    }
                }
            }
        } finally {
            fireDeleteAllPerformed( startsWith, ret );
        }
        return ret;
    }

    /**
     * Helper method to recursively delete a File.
     *
     * @param root the root file
     * @throws IOException thrown if one or more files could not be deleted
     */
    public static void deleteRecursively(
            File root )
        throws
            IOException
    {
        if( root.isDirectory() ) {
            for( File current : root.listFiles() ) {
                deleteRecursively( current );
            }
        }

        if( !root.delete()) {
            throw new IOException( "Unable to delete file " + root );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            String key )
    {
        File file = theKeyMapper.keyToFile( key );

        boolean ret = false;
        try {
            ret = file.exists();

        } finally {
            fireContainsKey( key, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilesystemStoreCursorIterator iterator()
    {
        return iterator( "" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilesystemStoreCursorIterator iterator(
            String startsWith )
    {
        return FilesystemStoreCursorIterator.create( this, startsWith );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size(
            String startsWith )
    {
        FilesystemStoreCursorIterator iter = iterator();
        int                     ret  = 0;

        while( iter.hasNext() ) {
            StoreValue found = iter.next();

            if( found.getKey().startsWith( startsWith )) {
                ++ret;
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "subDir",
                    "keyMapper",
                    "storeValueStreamer"
                },
                new Object[] {
                    theSubDir,
                    theKeyMapper,
                    theStoreValueStreamer
                });
    }

    /**
     * The subdirectory underneath which all data is stored.
     */
    protected File theSubDir;

    /**
     * Maps Store keys to paths in the local file system and vice versa.
     */
    protected KeyFileMapper theKeyMapper;

    /**
     * Streams StoreValues to file content, and vice versa.
     */
    protected StoreValueStreamer theStoreValueStreamer;
}
