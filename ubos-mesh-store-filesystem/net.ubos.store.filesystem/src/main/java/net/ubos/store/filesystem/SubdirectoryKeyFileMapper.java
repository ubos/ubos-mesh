//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.filesystem;

import java.io.File;
import java.util.ArrayList;
import net.ubos.util.ResourceHelper;

/**
 * A trivial implementation to map Store keys to paths in the local file system and vice versa, by
 * mapping all keys into a subdirectory.
 * 
 * FIXME: this needs to do some kind of escaping to handle keys that contain FILE_SUFFIX.
 */
public class SubdirectoryKeyFileMapper
        implements
            KeyFileMapper
{
    /**
     * Factory method.
     * 
     * @param subdir the subdirectory into which to map
     * @return the created SubdirectoryKeyFileMapper
     */
    public static SubdirectoryKeyFileMapper create(
            File subdir )
    {
        return new SubdirectoryKeyFileMapper( subdir );
    }

    /**
     * Constructor.
     * 
     * @param subdir the subdirectory into which to map
     */
    protected SubdirectoryKeyFileMapper(
            File subdir )
    {
        theSubDir = subdir;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public File keyToFile(
            String key )
    {
        String subdirString = theSubDir.getPath();
        File ret = new File( subdirString + "/" + key + FILE_SUFFIX );
        return ret;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String fileToKey(
            File path )
        throws
            IllegalArgumentException
    {
        String pathString   = path.getPath();
        String subdirString = theSubDir.getPath();
        
        if( pathString.startsWith(  subdirString ) && pathString.endsWith( FILE_SUFFIX)) {
            String ret = pathString.substring(  subdirString.length(), pathString.length() - FILE_SUFFIX.length() );
            return ret;
        } else {
            throw new IllegalArgumentException( "Cannot be mapped: " + path );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<File> rootFiles()
    {
        ArrayList<File> ret = new ArrayList<>( 1 );
        ret.add( theSubDir );
        return ret;
    }

    /**
     * The subdirectory into which to map.
     */
    protected File theSubDir;
    
    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( SubdirectoryKeyFileMapper.class );
    
    /**
     * The suffix to use for files.
     */
    public static final String FILE_SUFFIX = theResourceHelper.getResourceStringOrDefault( "FileSuffix", ".dat" );
}
