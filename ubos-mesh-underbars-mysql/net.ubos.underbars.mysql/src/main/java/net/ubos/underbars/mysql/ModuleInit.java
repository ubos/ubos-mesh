//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.ubos.daemon.AppInfo;
import net.ubos.daemon.Daemon;
import net.ubos.license.PermitAuthenticatedAllLicenseManager;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.security.PermitAuthenticatedAllReadOnlyAccessManager;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.encrypted.EncryptedStore;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.store.mysql.history.MysqlStoreWithHistory;
import net.ubos.underbars.Underbars;
import net.ubos.util.logging.Log;
import net.ubos.util.quit.QuitManager;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleDeactivationException;
import org.diet4j.core.ModuleSettings;

/**
 * Main program of the Undertow daemon. Activate using diet4j.
 */
public class ModuleInit
{
    private static final Log log = Log.getLogInstance( ModuleInit.class );

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        log.traceMethodCallEntry( ModuleInit.class, "moduleActivate", thisModule );

        ModuleSettings settings = thisModule.getModuleSettings();

        String fullAppName  = settings.getString( "fullappname" );
        String shortAppName = settings.getString( "shortappname" );

        Daemon.setAppInfo( AppInfo.create( fullAppName, shortAppName ));

        boolean useHistory        = settings.getBoolean( "usehistory",        true );
        boolean useAccessManager  = settings.getBoolean( "useaccessmanager",  true );
        boolean useLicenseManager = settings.getBoolean( "uselicensemanager", true );

        // Keys
        String        dataKeyHex         = settings.getString( "datakey" );
        String        credentialsKeyHex  = settings.getString( "credentialskey" );
        SecretKeySpec dataKeySpec        = null;
        SecretKeySpec credentialsKeySpec = null;

        try {
            if( !dataKeyHex.trim().isEmpty() ) {
                dataKeySpec = new SecretKeySpec( createKey( dataKeyHex ), "AES" );
            }
            if( !credentialsKeyHex.trim().isEmpty() ) {
                credentialsKeySpec = new SecretKeySpec( createKey( credentialsKeyHex ), "AES" );
            }

        } catch( Throwable t ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), t );
        }

        // Database
        String dbName   = settings.getString( "maindbname" );
        String dbUser   = settings.getString( "maindbuser" );
        String dbPass   = settings.getString( "maindbpass" );
        String dbServer = settings.getString( "maindbserver", "127.0.0.1" );

         // MeshObjects

        MysqlDataSource ds = new MysqlDataSource();
        ds.setDatabaseName( dbName );
        ds.setUser( dbUser );
        ds.setPassword( dbPass );
        ds.setServerName( dbServer );

        Store<StoreValue> meshObjectsStore  = MysqlStore.create( ds, MESH_OBJECTS_TABLE );
        Store<StoreValue> primaryNsStore    = MysqlStore.create( ds, PRIMARY_NAMESPACE_TABLE );
        Store<StoreValue> contextualNsStore = MysqlStore.create( ds, CONTEXTUAL_NAMESPACE_TABLE );

        if( dataKeySpec != null ) {
            try {
                meshObjectsStore  = EncryptedStore.create( DATA_TRANSFORMATION, dataKeySpec, INIT_VECTOR_SPEC, meshObjectsStore );
                primaryNsStore    = EncryptedStore.create( DATA_TRANSFORMATION, dataKeySpec, INIT_VECTOR_SPEC, primaryNsStore );
                contextualNsStore = EncryptedStore.create( DATA_TRANSFORMATION, dataKeySpec, INIT_VECTOR_SPEC, contextualNsStore );

            } catch( Throwable t ) {
                throw new ModuleActivationException( thisModule.getModuleMeta(), t );
            }
        }

        StorePrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create(
                primaryNsStore );

        StoreMeshBase.Builder mainMbBuilder
                = StoreMeshBase.Builder.create( meshObjectsStore, contextualNsStore, primaryNsMap );

        if( useHistory ) {
            MysqlStoreWithHistory historyStore = MysqlStoreWithHistory.create( ds, HISTORY_TABLE );
            MysqlStoreWithHistory transactionLogStore = MysqlStoreWithHistory.create( ds, TRANSACTION_LOG_TABLE  );
            mainMbBuilder = mainMbBuilder.historyStores( historyStore, transactionLogStore );
        }
        if( useAccessManager ) {
            mainMbBuilder = mainMbBuilder.accessManager( PermitAuthenticatedAllReadOnlyAccessManager.create() );
        }

        StoreMeshBase mainMb = mainMbBuilder.build();

        Daemon.setMainMeshBase( mainMb );

        if( useLicenseManager ) {
            Daemon.setLicenseManager( PermitAuthenticatedAllLicenseManager.create() );
        }

        // Credentials
        if( credentialsKeySpec != null ) {
            // otherwise we don't instantiate the credentials store
            try {
                MysqlStore                 encryptedCredentialsStore = MysqlStore.create( ds, CREDENTIALS_TABLE );
                EncryptedStore<StoreValue> credentialsStore          = EncryptedStore.create( CREDENTIALS_TRANSFORMATION, credentialsKeySpec, INIT_VECTOR_SPEC, encryptedCredentialsStore );

                Underbars.setUserCredentialsStore( credentialsStore );

            } catch( Throwable t ) {
                throw new ModuleActivationException( thisModule.getModuleMeta(), t );
            }
        }

        //

        Underbars.start();
    }

    /**
     * Helper method to create a valid key from its hex representation.
     *
     * @param keyKey the key in hex format
     * @return the Key as bytes
     */
    protected static byte [] createKey(
            String keyHex )
    {
        byte [] key = new byte[24]; // DES-ede needs 24 bytes
        while( keyHex.length() < 48 ) {
            keyHex += '0'; // pad it
        }
        for( int i=0 ; i<key.length ; ++i ) {
            char high = keyHex.charAt( 2*i );
            char low  = keyHex.charAt( 2*i+1 );

            key[i] = (byte) ( 16 * hexToInt( high ) + hexToInt( low ));
        }
        return key;
    }

    /**
     * Diet4j module dectivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleDeactivationException thrown if module deactivation failed
     */
    public static void moduleDectivate(
            Module thisModule )
        throws
            ModuleDeactivationException
    {
        try {
            QuitManager.initiateQuit();
            QuitManager.waitForQuit();

        } catch( InterruptedException ex ) {
            throw new ModuleDeactivationException( thisModule.getModuleMeta(), ex );
        }
    }

    /**
     * Helper function.
     *
     * @param hex character representing a hexadecimal digital
     * @return the same as a number
     */
    static int hexToInt(
            char hex )
    {
        int ret;
        if( hex <= '0' ) {
            ret = 0;
        } else if( hex <= '9' ) {
            ret = hex - '0';
        } else if( hex < 'A' ) {
            ret = 9;
        } else if( hex <= 'F' ) {
            ret = hex - 'A' + 10;
        } else if( hex < 'a' ) {
            ret = 9;
        } else if( hex <= 'f' ) {
            ret = hex - 'a' + 10;
        } else {
            ret = 15;
        }
        return ret;
    }

    /**
     * Name of the crypto transformation used in the JCE for user credentials.
     */
    public static final String CREDENTIALS_TRANSFORMATION = "AES/CBC/PKCS5Padding";

    /**
     * Name of the crypto transformation used in the JCE for user data.
     */
    public static final String DATA_TRANSFORMATION = CREDENTIALS_TRANSFORMATION;

    /**
     * The init vector for the AES key: 16 bytes
     */
    private static final IvParameterSpec INIT_VECTOR_SPEC = new IvParameterSpec( new byte[] {
          9,   0,  99, -12,
         32, 122, -22,   0,
        111,  -3,  47,  48,
          1, -29,  34,  77
    });

    /**
     * Name of the SQL table that contains the MeshObjects
     */
    public static final String MESH_OBJECTS_TABLE = "MeshObjects";

    /**
     * Name of the SQL table that contains the known MeshObjectIdentifierNamespaces
     */
    public static final String PRIMARY_NAMESPACE_TABLE = "PrimaryNamespaceMap";

    /**
     * Name of the SQL table that contains the local namespace mappings for the
     * main StoreMeshBase.
     */
    public static final String CONTEXTUAL_NAMESPACE_TABLE = "ContextualNamespaceMap";

    /**
     * Name of the SQL table that contains user credentials
     */
    public static final String CREDENTIALS_TABLE = "Credentials";

    /**
     * Name of the SQL table that contains the MeshObject histories.
     */
    public static final String HISTORY_TABLE = "MeshObjectHistory";

    /**
     * Name of the SQL table that contains the transaction log.
     */
    public static final String TRANSACTION_LOG_TABLE = "MeshBaseTransactions";

    /**
     * Name of the SQL table that contains the words in the MeshBaseIndex.
     */
    public static final String WORD_INDEX_TABLE = "MeshBaseWordIndex";

    /**
     * Name of the SQL table that contains the types in the MeshBaseIndex.
     */
    public static final String TYPE_INDEX_TABLE = "MeshBaseTypeIndex";
}
