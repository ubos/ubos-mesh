#
# Creates the MySQL tables for storing a MeshBaseIndex
#

CREATE TABLE MeshBaseWordIndex (
    id         VARCHAR(511) COLLATE utf8mb4_nopad_bin NOT NULL PRIMARY KEY,
    encodingId VARCHAR(128),
    weight     BIGINT NOT NULL,
    content    LONGBLOB,
    INDEX( weight, id )
) CHARSET=utf8mb4;;

CREATE TABLE MeshBaseTypeIndex (
    id         VARCHAR(511) COLLATE utf8mb4_nopad_bin NOT NULL PRIMARY KEY,
    encodingId VARCHAR(128),
    weight     BIGINT NOT NULL,
    content    LONGBLOB,
    INDEX( weight, id )
) CHARSET=utf8mb4;;
