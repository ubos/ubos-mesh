#
# Creates the ContextualMeshObjectIdentifierNamespaceMap table
#

CREATE TABLE ContextualNamespaceMap (
    id         VARCHAR(511) COLLATE utf8mb4_nopad_bin NOT NULL PRIMARY KEY,
    encodingId VARCHAR(128),
    content    LONGBLOB
);
