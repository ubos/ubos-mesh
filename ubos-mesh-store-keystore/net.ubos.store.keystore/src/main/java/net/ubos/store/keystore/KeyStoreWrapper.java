//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.keystore;

import net.ubos.store.Store;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;

/**
 * Given the less-than-optimal API of the Java <code>java.security.KeyStore</code> class, this class
 * wraps around it, and makes sure that the KeyStore is initialized by reading
 * from a Store, and updated contents are written back to the Store. As a result,
 * clients can use the KeyStore API while the actual content of the keystore is held
 * by a Store.
 */
public class KeyStoreWrapper
{
    private static final Log log = Log.getLogInstance( KeyStoreWrapper.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param store the Store with the KeyStore content
     * @param keyIntoStore the key for the KeyStore
     * @param keyStorePassword the password for the KeyStore
     * @return the created KeyStoreWrapper
     */
    public static KeyStoreWrapper create(
            Store<StoreValue> store,
            String            keyIntoStore,
            String            keyStorePassword )
    {
        return new KeyStoreWrapper( store, keyIntoStore, keyStorePassword );
    }

    /**
     * Constructor.
     *
     * @param store the Store with the KeyStore content
     * @param keyIntoStore the key for the KeyStore
     * @param keyStorePassword the password for the KeyStore
     */
    protected KeyStoreWrapper(
            Store<StoreValue> store,
            String            keyIntoStore,
            String            keyStorePassword )
    {
        theStore            = store;
        theKeyIntoStore     = keyIntoStore;
        theKeyStorePassword = keyStorePassword;
    }

    /**
     * Load data into this key store.
     *
     * @param inStream the data
     * @throws IOException thrown if an I/O error occurred
     * @throws GeneralSecurityException thrown if the client was not permitted to invoke this operation
     */
    public void load(
            InputStream inStream )
        throws
            IOException,
            GeneralSecurityException
    {
        KeyStore keyStore = getKeyStore();

        try {
            keyStore.load( inStream, theKeyStorePassword.toCharArray() );

            internalSave( theStore, keyStore, theKeyStorePassword );

        } catch( GeneralSecurityException ex ) {
            log.error( ex );
        }
    }

    /**
     * Obtain a Certificate with a certain name.
     *
     * @param alias the name of the Certificate
     * @return the Certificate, or null
     * @throws KeyStoreException thrown if the Certificate could not be obtained from the KeyStore
     * @throws GeneralSecurityException thrown if the client was not permitted to invoke this operation
     */
    public Certificate getCertificate(
            String alias )
        throws
            KeyStoreException,
            GeneralSecurityException
    {
        KeyStore keyStore = getKeyStore();

        Certificate ret = keyStore.getCertificate( alias );
        return ret;
    }

    /**
     * Obtain a Key with a certain name.
     *
     * @param alias the name
     * @return the Key
     * @throws GeneralSecurityException thrown if the client was not permitted to invoke this operation
     */
    public Key getKey(
            String alias )
        throws
            GeneralSecurityException
    {
        return getKey( alias, theKeyStorePassword );
    }

    /**
     * Obtain a Key with a certain name.
     *
     * @param alias the name
     * @param password the password on the Key
     * @return the Key
     * @throws GeneralSecurityException thrown if the client was not permitted to invoke this operation
     */
    public Key getKey(
            String alias,
            String password )
        throws
            GeneralSecurityException
    {
        KeyStore keyStore = getKeyStore();

        Key ret = keyStore.getKey( alias, password.toCharArray() );
        return ret;
    }

    /**
     * Internal helper to create or obtain the KeyStore.
     *
     * @return the created or obtained KeyStore
     * @throws GeneralSecurityException thrown if the client was not permitted to invoke this operation
     */
    protected KeyStore getKeyStore()
        throws
            GeneralSecurityException
    {
        if( theKeyStore == null ) {

            try {
                theKeyStore = KeyStore.getInstance( "JKS" );

                StoreValue v = theStore.get( theKeyIntoStore );

                theKeyStore.load( v.getDataAsStream(), theKeyStorePassword.toCharArray() );

                // all others are right

            } catch( StoreKeyDoesNotExistException ex ) {
                // we don't have it yet, first-time invocation
                log.info( ex );

            } catch( IOException ex ) {
                log.error( ex );
            }
        }
        return theKeyStore;
    }

    /**
     * Save the content of the KeyStore back into the Store. FIXME do we need this?
     *
     * @throws IOException thrown if an I/O error occurred
     */
    public void saveToStore()
        throws
            IOException
    {
        KeyStore keyStore = theKeyStore;

        if( keyStore == null ) {
            return; // nothing to save here
        }

        internalSave( theStore, keyStore, theKeyStorePassword );
    }

    /**
     * Export the KeyStore's data to this stream.
     *
     * @param outStream the stream to write to
     * @throws IOException thrown if an I/O error occurred
     */
    public void store(
            OutputStream outStream )
        throws
            IOException
    {
        try {
            KeyStore keyStore = getKeyStore();
            keyStore.store( outStream, theKeyStorePassword.toCharArray() );

        } catch( GeneralSecurityException ex ) {
            log.error( ex );
        }
    }

    /**
     * Factors out saving functionality.
     *
     * @param store the Store to save to
     * @param keyStore the KeyStore to store
     * @param password the KeyStore password
     * @throws IOException thrown if an I/O error occurred
     */
    protected void internalSave(
            Store<StoreValue> store,
            KeyStore          keyStore,
            String            password )
        throws
            IOException
    {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            keyStore.store( outputStream, password.toCharArray() );

            byte [] data = outputStream.toByteArray();

            store.putOrUpdate( new StoreValue(
                    theKeyIntoStore,
                    getClass().getName(),
                    data ));

        } catch( GeneralSecurityException ex ) {
            log.error( ex );
        }
    }

    /**
     * The Store in which the key materials are stored.
     */
    protected Store<StoreValue> theStore;

    /**
     * The KeyStore we use.
     */
    protected KeyStore theKeyStore;

    /**
     * The key into the Store.
     */
    protected String theKeyIntoStore;

    /**
     * The password on the KeyStore.
     */
    protected String theKeyStorePassword;
}
