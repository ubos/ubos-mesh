//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.util.EnumSet;
import net.ubos.mesh.MeshObject;
import net.ubos.util.ResourceHelper;
import net.ubos.vl.AbstractViewlet;
import net.ubos.web.Url;

/**
 * Convenience class for WebViewlets.
 */
public abstract class AbstractWebViewlet
    extends
        AbstractViewlet
    implements
        WebViewlet
{
    /**
     * Constructor, for subclasses only.
     *
     * @param name the computable name of the Viewlet
     * @param viewed the JeeViewedMeshObjects to use
     */
    protected AbstractWebViewlet(
            String               name,
            WebViewedMeshObjects viewed )
    {
        super( viewed );

        theName = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleName()
    {
        ResourceHelper rh = ResourceHelper.getInstance( theName, getClass().getClassLoader() );

        String userVisibleName = rh.getResourceStringOrDefault( "UserVisibleName", theName );
        return userVisibleName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebViewedMeshObjects getViewedMeshObjects()
    {
        return (WebViewedMeshObjects) super.getViewedMeshObjects();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnumSet<WebViewletState> getPossibleStates()
    {
        // FIXME: should take MeshObject access rights into account

        MeshObject subject = getSubject();
        if( subject.getIsWriteable() ) {
            return WebViewletState.getReadWriteStates();
        } else {
            return WebViewletState.getReadOnlyStates();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebViewletState getState()
    {
        return getViewedMeshObjects().getViewletState();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPostUrl(
            WebMeshObjectsToView toView )
    {
        return toView.withViewletState( WebViewletState.VIEW ).asUrlString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView getFactoryKey()
    {
        return getViewedMeshObjects().getMeshObjectsToView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCssClass()
    {
        String ret;
        if( theName != null ) {
            ret = theName;
        } else {
            ret = getClass().getName();
        }

        ret = ret.replace( ".", "-" );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performBeforeGet(
            ToViewStructuredResponsePair rr )
    {
        // no op on this level
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performBeforePost(
            ToViewStructuredResponsePair rr )
    {
        return defaultPerformPost( rr );
    }

    /**
     * Default implementation of what happens upon POST.
     *
     * @param rr the request-response pair
     * @return if true, the result of the vl processing has been deposited into the response object
     *         already and regular processing will be skipped. If false, regular processing continues.
     */
    protected boolean defaultPerformPost(
            ToViewStructuredResponsePair rr )
    {
        StructuredResponse response = rr.getStructuredResponse();

        if( !response.haveProblemsBeenReported() ) {
            // only if no errors have been reported
            Url u = rr.getStructuredResponse().getRequest().getUrl();

            String redirect = u.getUrlArgument( "redirect" );
            if( redirect == null ) {
                redirect = u.getAbsoluteFullUri();
            }
            response.setLocation( 303, redirect ); // "See Other"
            return true;

        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void performAfter(
            ToViewStructuredResponsePair rr,
            Throwable                    thrown )
    {}

    /**
     * The computable name of the Viewlet.
     */
    protected final String theName;
}
