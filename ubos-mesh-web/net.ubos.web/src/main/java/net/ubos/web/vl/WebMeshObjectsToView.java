//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.mesh.MeshObject;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletPlacement;

/**
 * Augments MeshObjectsToView for Web Viewlets.
 */
public interface WebMeshObjectsToView
        extends
            MeshObjectsToView
{
    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withSubject(
            MeshObject obj );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withAtTime(
            long atTime );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withAtCurrentTime();

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRequiredViewletName(
           String newViewletType );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRecommendedViewletName(
           String newViewletType );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRequiredViewletPlacement(
            ViewletPlacement newValue );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRequiredViewletDimensionality(
            ViewletDimensionality newValue );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRecommendedViewletDimensionality(
            ViewletDimensionality newValue );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRequiredViewletDetail(
            ViewletDetail newValue );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRecommendedViewletDetail(
            ViewletDetail newValue );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withViewletParameter(
            String name,
            Object newValue );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withViewletParameter(
            String    name,
            Object [] newValues );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withoutViewletParameter(
            String name );

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withoutViewletParameters();

    /**
     * Obtain the desired WebViewletState.
     *
     * @return the desired state
     */
    public WebViewletState getViewletState();

    /**
     * Obtain the desired WebViewletStateTransition.
     *
     * @return the desired transition
     */
    public WebViewletStateTransition getViewletStateTransition();

    /**
     * Obtain the requested MIME type.
     *
     * @return the MIME type
     */
    public String getMimeType();

    /**
     * Create a copy of this object, but set the MIME Type to something else.
     *
     * @param newValue the new MIME type
     * @return new instance
     */
    public WebMeshObjectsToView withMimeType(
            String newValue );

    /**
     * Create a copy of this object, but set the WebViewletState to something else.
     *
     * @param newValue the new WebViewletState
     * @return new instance
     */
    public WebMeshObjectsToView withViewletState(
            WebViewletState newValue );

    /**
     * Create a copy of this object, but set the WebViewletStateTransition to something else.
     *
     * @param newValue the new WebViewletStateTransition
     * @return new instance
     */
    public WebMeshObjectsToView withViewletStateTransition(
            WebViewletStateTransition newValue );

    /**
     * Encode the information in this object as a relative URL, in String form.
     *
     * @return the URL as String
     */
    public String asUrlString();

    /**
     * Determine whether this incoming request is an update (e.g. HTTP POST).
     *
     * @return true if this is an update
     */
    public boolean isUpdate();

    /**
     * Name of the URL argument in the incoming request that indicates the time stamp
     * at which the MeshObjects shall be viewed.
     */
    public static final String WHEN_PAR_NAME = "when";

    /**
     * Name of URL argument in the incoming request that indicates the requested
     * Viewlet.
     */
    public static final String VIEWLET_PAR_NAME = "vl";

    /**
     * Name of URL argument in the incoming request that indicates the required Viewlet placement.
     */
    public static final String REQUIRED_VIEWLET_PLACEMENT_PAR_NAME = "vlp";

    /**
     * Name of URL argument in the incoming request that indicates the required Viewlet dimensionality.
     */
    public static final String REQUIRED_VIEWLET_DIMENSIONALITY_PAR_NAME = "vldimq";

    /**
     * Name of URL argument in the incoming request that indicates the recommended Viewlet dimensionality.
     */
    public static final String RECOMMENDED_VIEWLET_DIMENSIONALITY_PAR_NAME = "vldimr";

    /**
     * Name of URL argument in the incoming request that indicates the required Viewlet detail.
     */
    public static final String REQUIRED_VIEWLET_DETAIL_PAR_NAME = "vldetq";

    /**
     * Name of URL argument in the incoming request that indicates the recommended Viewlet detail.
     */
    public static final String RECOMMENDED_VIEWLET_DETAIL_PAR_NAME = "vldetr";

    /**
     * Name of the URL argument in the incoming request that indicates the requested
     * MIME type.
     */
    public static final String MIME_PAR_NAME = "mime";
}
