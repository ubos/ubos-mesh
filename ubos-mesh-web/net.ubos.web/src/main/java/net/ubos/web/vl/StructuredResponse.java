//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.ubos.util.exception.LocalizedException;
import net.ubos.util.logging.Log;
import net.ubos.web.HttpRequest;
import net.ubos.util.StringHelper;
import net.ubos.util.token.UniqueStringGenerator;
import net.ubos.web.ProblemReporter;

/**
 * A response to an incoming request, before it has been serialized to a byte stream.
 */
public class StructuredResponse
    implements
        ProblemReporter
{
    private static final Log log = Log.getLogInstance( StructuredResponse.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param request the incoming request to which this is a response
     * @return the created instance
     */
    public static StructuredResponse create(
            HttpRequest request )
    {
        String  csrfToken = request.getCsrfToken();
        if( csrfToken == null ) {
            csrfToken = UniqueStringGenerator.create( 32 ).createUniqueToken();
        }

        return new StructuredResponse( request, csrfToken );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param request the incoming request to which this is a response
     * @param csrfToken the CSRF token to emit as a cookie
     */
    protected StructuredResponse(
            HttpRequest request,
            String      csrfToken )
    {
        theRequest   = request;
        theCsrfToken = csrfToken;
    }

    /**
     * Obtain the incoming request.
     *
     * @return the incoming request
     */
    public HttpRequest getRequest()
    {
        return theRequest;
    }

    /**
     * Obtain the value for the CSRF token.
     *
     * @return the token
     */
    public String getCsrfToken()
    {
        return theCsrfToken;
    }

    /**
     * Obtain the name of the input field in a form that contains the CRSF token.
     *
     * @return the field name
     */
    public String getCsrfFormInputName()
    {
        return theRequest.getCsrfFormInputName();
    }

    /**
     * Set the content in one section.
     *
     * @param sectionName name of the section
     * @param sectionContent content of the section
     */
    public void setSectionContent(
            String sectionName,
            String sectionContent )
    {
        if( HTML_BODY_ERRORS_SECTION.equals( sectionName )) {
            throw new IllegalArgumentException( "Do not write to section: " + HTML_BODY_ERRORS_SECTION );
        }

        if( !theSections.containsKey( sectionName )) {
            theSections.put( sectionName, sectionContent );
        } else {
            log.error( "Response content for section", sectionName, "set previously:", sectionContent );
        }
    }

    /**
     * Append to the content in one section.
     *
     * @param sectionName name of the sectoin
     * @param contentToAdd content to add to the section
     */
    public void appendSectionContent(
            String sectionName,
            String contentToAdd )
    {
        if( HTML_BODY_ERRORS_SECTION.equals( sectionName )) {
            throw new IllegalArgumentException( "Do not write to section: " + HTML_BODY_ERRORS_SECTION );
        }

        String newContent;
        if( theSections.containsKey( sectionName )) {
            newContent = theSections.get( sectionName ) + contentToAdd;
        } else {
            newContent = contentToAdd;
        }
        theSections.put( sectionName, newContent );
    }

    /**
     * Obtain the content in one section.
     *
     * @param sectionName name of the section
     * @return the content, or null if the section does not exist
     */
    public String getSectionContent(
            String sectionName )
    {
        if( HTML_BODY_ERRORS_SECTION.equals( sectionName ) && !theSections.containsKey( HTML_BODY_ERRORS_SECTION )) {
            // construct content
            String sectionContent = constructErrorSectionContent();
            theSections.put( sectionName, sectionContent );
            return sectionContent;

        } else {
            return theSections.get( sectionName );
        }
    }

    /**
     * Ensure that a certain &lt;link> entry is in the header by checking
     * that it is present, and adding it if it is not.
     *
     * @param rel the type of relationship
     * @param href the href, relative to the application's context path
     */
    public void ensureHtmlHeaderLink(
            String rel,
            String href )
    {
        // we want to maintain order, but not duplicates
        if( theHtmlLinks == null ) {
            theHtmlLinks = new HashMap<>();
        }
        Set<String> hrefs = theHtmlLinks.get( rel );
        if( hrefs == null ) {
            hrefs = new LinkedHashSet<>();
            theHtmlLinks.put( rel, hrefs );
        }
        if( !hrefs.contains( href )) {
            hrefs.add( href );
        }
    }

    /**
     * Bulk operation for ensuring header links. This is for sub-StructuredResponses
     * to be able to easily pass on their header links to the parent.
     *
     * @param map the set of header links to add
     */
    public void ensureHtmlHeaderLinks(
            Map<String,Set<String>> map )
    {
        if( map == null || map.isEmpty() ) {
            return;
        }
        if( theHtmlLinks == null ) {
            theHtmlLinks = new HashMap<>();
        }
        for( Map.Entry<String,Set<String>> entry : map.entrySet() ) {
            String rel = entry.getKey();

            Set<String> hrefs = theHtmlLinks.get( rel );
            if( hrefs == null ) {
                hrefs = new LinkedHashSet<>();
                theHtmlLinks.put( rel, hrefs );
            }
            hrefs.addAll( entry.getValue() );
        }
    }

    /**
     * Obtain the &lt;link> entries for the header.
     *
     * @return hash of link rel, to array of hrefs, or null
     */
    public Map<String,Set<String>> getHtmlHeaderLinks()
    {
        return theHtmlLinks;
    }

    /**
     * Ensure that a certain &lt;script> entry is in the header by checking
     * that it is present, and adding it if it is not.
     *
     * @param src the source, relative to the application's context path
     */
    public void ensureHtmlHeaderScriptSource(
            String src )
    {
        if( theHtmlScripts == null ) {
            theHtmlScripts = new LinkedHashSet<>();
        }
        theHtmlScripts.add( src );
    }

    /**
     * Bulk operation for ensuring script entries. This is for sub-StructuredResponses
     * to be able to easily pass on their header links to the parent.
     *
     * @param scripts the set of scripts links to add
     */
    public void ensureHtmlHeaderScriptSources(
            Set<String> scripts )
    {
        if( scripts == null || scripts.isEmpty() ) {
            return;
        }
        if( theHtmlScripts == null ) {
            theHtmlScripts = new LinkedHashSet<>();
        }
        theHtmlScripts.addAll( scripts );
    }

    /**
     * Obtain the &lt;script> entries for teh header
     *
     * @return list of entries, or null
     */
    public LinkedHashSet<String> getHtmlHeaderScriptSources()
    {
        return theHtmlScripts;
    }

    /**
     * Obtain an Iterable over all sections.
     *
     * @return the Iterable
     */
    public Set<Map.Entry<String,String>> sections()
    {
        return theSections.entrySet();
    }

    /**
     * Determine whether this response is binary.
     *
     * @return true or false
     */
    public boolean isBinary()
    {
        return theBinary != null;
    }

    /**
     * Obtain the binary content of this response, if any.
     *
     * @return the binary content
     */
    public byte [] getBinaryContent()
    {
        return theBinary;
    }

    /**
     * If this is a binary response, determine the content type.
     *
     * @return the content type
     */
    public String getBinaryContentType()
    {
        return theBinaryContentType;
    }


    /**
     * Turn this response binary, and provide the content and content type
     *
     * @param content the binary content
     * @param contentType the binary content type
     */
    public void setBinaryContent(
            byte [] content,
            String  contentType )
    {
        theBinary            = content;
        theBinaryContentType = contentType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean haveProblemsBeenReported()
    {
        return theProblems != null && !theProblems.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reportProblem(
            Throwable ex )
    {
        if( theProblems == null ) {
            theProblems = new ArrayList<>();
        }
        theProblems.add( ex );
        theSections.remove( HTML_BODY_ERRORS_SECTION );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reportProblems(
            Throwable [] ex )
    {
        if( theProblems == null ) {
            theProblems = new ArrayList<>();
        }
        for( Throwable e : ex ) {
            theProblems.add( e );
        }
        theSections.remove( HTML_BODY_ERRORS_SECTION );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<Throwable> reportedProblems()
    {
        return theProblems;
    }

    /**
     * Set the location and status for a redirecting response.
     *
     * @param status the HTTP status
     * @param location the target URL for the redirect
     */
    public void setLocation(
            int    status,
            String location )
    {
        if( theLocation == null ) {
            theLocation = location;
            theStatus   = status;
        } else {
            log.error( "Location header set previously:", theLocation );
        }
    }

    /**
     * Obtain the location for a redirecting response.
     *
     * @return the target URL for the redirect
     */
    public String getLocation()
    {
        return theLocation;
    }

    /**
     * Obtain the desired HTTP status.
     *
     * @return the status
     */
    public int getHttpStatus()
    {
        return theStatus;
    }

    /**
     * Construct the content of the error section from the reported errors.
     *
     * @return the content
     */
    protected String constructErrorSectionContent()
    {
        if( theProblems == null ) {
            return null;
        }
        StringBuilder ret = new StringBuilder( theProblems.size() * 30 ); // fudge
        ret.append( "<ul class='errors'>\n" );
        for( Throwable t : theProblems ) {
            String msg = null;
            for( Throwable reportThis = t ; reportThis != null ; reportThis = reportThis.getCause() ) {
                msg = reportThis.getLocalizedMessage();
                if( msg != null ) {
                    if( !( reportThis instanceof LocalizedException )) {
                        msg = "<pre>" + StringHelper.stringToHtml( msg ) + "</pre>";
                    }
                    break;
                }
            }
            if( msg == null ) {
                msg = t.getClass().getName();
            }
            ret.append( "<li>" ).append( msg ).append( "</li>\n" );
        }
        ret.append( "</ul>\n" );
        return ret.toString();
    }

    /**
     * The incoming request.
     */
    protected final HttpRequest theRequest;

    /**
     * The CSRF token to emit as a cookie.
     */
    protected final String theCsrfToken;

    /**
     * Sections and content.
     */
    protected final Map<String,String> theSections = new HashMap<>();

    /**
     * If the response is binary, this contains the binary value.
     */
    protected byte [] theBinary;

    /**
     * If this response is binary, this contains the content type for the binary value.
     */
    protected String theBinaryContentType;

    /**
     * The reported problems, in sequence, if any.
     */
    protected List<Throwable> theProblems;

    /**
     * The HTTP status code for the response.
     */
    protected int theStatus = 200;

    /**
     * The Location header value for the response, if any.
     */
    protected String theLocation;

    /**
     * The HTML &lt;link>s to add to the header
     */
    protected Map<String,Set<String>> theHtmlLinks;

    /**
     * The HTML &lt;script> sources to add to the header.
     */
    protected LinkedHashSet<String> theHtmlScripts;

    /**
     * Predefined sections.
     */
    public static final String HTML_HEAD_SECTION         = "html_head";
    public static final String HTML_HEAD_TITLE_SECTION   = "html_head_title";
    public static final String HTML_BODY_CONTENT_SECTION = "html_body_content";
    public static final String HTML_BODY_INLINED_SECTION = "html_body_inlined";

    public static final String HTML_BODY_ERRORS_SECTION    = "html_body_errors";
    // This is a "virtual" section that one must not write to. It gets calculated
    // from the reported errors

    public static final String REQUESTED_SKIN_SECTION      = "requested_skin";
}
