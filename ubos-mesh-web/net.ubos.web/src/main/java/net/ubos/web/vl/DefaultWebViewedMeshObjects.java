//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.vl.AbstractViewedMeshObjects;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;

/**
 * Extends DefaultViewedMeshObjects with Web information.
 */
public class DefaultWebViewedMeshObjects
        extends
            AbstractViewedMeshObjects
        implements
            WebViewedMeshObjects
{
    /**
     * Constructor. Initializes to empty content.
     */
    public DefaultWebViewedMeshObjects(
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail )
    {
        super( placement, dimensionality, detail );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateFrom(
            MeshObjectsToView newObjectsToView )
    {
        WebMeshObjectsToView realNewObjectsToView = (WebMeshObjectsToView) newObjectsToView;

        super.updateFrom( realNewObjectsToView );

        theMimeType = realNewObjectsToView.getMimeType();
        theState    = realNewObjectsToView.getViewletState();

        if( theState == null ) {
            theState = WebViewletState.VIEW;
        }
        WebViewletStateTransition newTransition = realNewObjectsToView.getViewletStateTransition();
        if( newTransition != null ) {
            performStateTransition( newTransition );
        }
    }

    /**
     * Overridable method to perform a state transition.
     *
     * @param newTransition the transition to perform
     */
    protected void performStateTransition(
            WebViewletStateTransition newTransition )
    {
        if( newTransition.getNextState() != null ) {
            theState = newTransition.getNextState();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMimeType()
    {
        return theMimeType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebViewletState getViewletState()
    {
        return theState;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView getMeshObjectsToView()
    {
        return (WebMeshObjectsToView) super.getMeshObjectsToView();
    }

    /**
     * The current MIME type.
     */
    protected String theMimeType;

    /**
     * The current vl state, if any.
     */
    protected WebViewletState theState;
}
