//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Thrown when something went wrong in the
 * {@link net.ubos.web.httpshell.HttpShell HttpShell}. For
 * details, consider the cause of the exception using <code>getCause()</code>.
 */
public final class HttpShellException
    extends
        AbstractLocalizedException
    implements
        CanBeDumped
{
    /**
     * Constructor. This is the only constructor provided as this must be invoked with
     * a delegate Throwable.
     *
     * @param cause the underlying cause
     */
    public HttpShellException(
            Throwable cause )
    {
        super( null, cause ); // use this constructor, in order to avoid that Throwable calls cause.toString().
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getLocalizationParameters()
    {
        return new Object[] { getCause().getLocalizedMessage() };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] { "cause" },
                new Object[] { getCause() } );
    }
}
