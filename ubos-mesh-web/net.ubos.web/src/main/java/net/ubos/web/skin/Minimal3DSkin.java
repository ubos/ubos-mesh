//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.skin;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import net.ubos.web.util.ExceptionWithHttpStatusCode;
import net.ubos.web.vl.StructuredResponse;

/**
 * A very simple, fallback Skin.
 */
public class Minimal3DSkin
    implements
        Skin
{
    /**
     * Singleton instance.
     */
    public static final Minimal3DSkin SINGLETON = new Minimal3DSkin();

    /**
     * Private constructor, use factory method.
     */
    protected Minimal3DSkin()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SkinType getType()
    {
        return SkinType.DEFAULT_3D;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int format(
            StructuredResponse response,
            OutputStream       rawOutput )
        throws
            IOException,
            SkinProcessingException
    {
        final int OK = 200;

        PrintWriter w = new PrintWriter( rawOutput );
        int status    = OK;

        String title = response.getSectionContent( StructuredResponse.HTML_HEAD_TITLE_SECTION );
        if( title == null ) {
            title = "Untitled";
        }
        String head    = response.getSectionContent( StructuredResponse.HTML_HEAD_SECTION );
        String content = response.getSectionContent( StructuredResponse.HTML_BODY_CONTENT_SECTION );
        if( content == null ) {
            content = "<p>(No content)</p>";
        }

        w.println( "<!DOCTYPE html>\n" );
        w.println( "<html lang=\"en-us\">" );
        w.println( " <head>" );
        w.println( "  <title>" + title + "</title>" );
        if( head != null ) {
            w.println( head );
        }
        w.println( " </head>" );
        w.println( " <body>" );
        w.println( content );
        if( response.haveProblemsBeenReported()) {
            w.println( "  <div style='background: #ffa0a0'>" );
            w.println( "   <h4>Reported problems:</h4>" );
            w.println( "   <ul>" );
            for( Throwable problem : response.reportedProblems() ) {
                String msg = null;
                for( Throwable cause = problem ; msg == null && cause != null ; cause = cause.getCause() ) {
                    msg = cause.getMessage();
                }

                if( msg == null ) {
                    msg = problem.getClass().getName();
                }

                if( status == OK && problem instanceof ExceptionWithHttpStatusCode ) {
                    status = ((ExceptionWithHttpStatusCode) problem).getDesiredHttpStatusCode();

                    msg += " (HTTP Status: " + status + ")";
                }
                w.println( "    <li>" + msg + "</li>" );
            }
            w.println( "   </ul>" );
            w.println( "  </div>" );
        }
        w.println( " </body>" );
        w.println( "</html>" );

        w.flush();
        return status;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentType()
    {
        return "text/html;charset=utf-8";
    }
}
