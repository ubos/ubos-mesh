//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.io.IOException;
import net.ubos.vl.CannotViewException;

/**
 * Default implementation of WebViewletProcessor.
 */
public class DefaultWebViewletProcessor
    implements
        WebViewletProcessor
{
    /**
     * {@inheritDoc}
     */
    @Override
    public void processViewlet(
            WebViewlet                   viewlet,
            ToViewStructuredResponsePair rr )
        throws
            CannotViewException,
            IOException
    {
        WebMeshObjectsToView toView = rr.getToView();

        boolean   done;
        Throwable thrown = null;
        try {
            viewlet.view( toView );

            if( toView.isUpdate() ) {
                done = viewlet.performBeforePost( rr );

            } else {
                done = viewlet.performBeforeGet( rr );
            }

            if( !done ) {
                viewlet.processRequest( rr );
            }

        } finally {
            viewlet.performAfter( rr, thrown );
        }
    }
}
