//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.io.IOException;
import net.ubos.vl.CannotViewException;

/**
 * Knows how to process a WebViewlet.
 */
public interface WebViewletProcessor
{
    /**
     * Perform Viewlet processing.
     * 
     * @param rr the request-response pair
     * @param viewlet the Viewlet to use
     * @throws CannotViewException the Viewlet could not view the subject, after all
     * @throws IOException I/O problem
     */
    public void processViewlet(
            WebViewlet                   viewlet,
            ToViewStructuredResponsePair rr )
        throws
            CannotViewException,
            IOException;
}
