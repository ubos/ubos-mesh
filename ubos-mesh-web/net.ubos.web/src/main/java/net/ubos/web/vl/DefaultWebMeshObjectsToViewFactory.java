//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.text.ParseException;
import java.util.HashMap;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseNameServer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.util.DateTimeUtil;
import net.ubos.util.UrlUtils;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.HttpRequest;
import net.ubos.web.Url;

/**
 * The default factory for MeshObjectsToView objects to be used in a server-side context.
 */
public class DefaultWebMeshObjectsToViewFactory
        implements
            WebMeshObjectsToViewFactory
{
    private static final Log log = Log.getLogInstance( DefaultWebMeshObjectsToViewFactory.class );

    /**
     * Factory method.
     *
     * @param urlIdSerializer knows how to create MeshObjectIdentifiers from strings
     * @param meshBaseNameServer the name server with which to look up MeshBases
     * @return the created DefaultWebMeshObjectsToViewFactory
     */
    public static DefaultWebMeshObjectsToViewFactory create(
            MeshObjectIdentifierBothSerializer urlIdSerializer,
            MeshBaseNameServer                 meshBaseNameServer )
    {
        return new DefaultWebMeshObjectsToViewFactory( urlIdSerializer, meshBaseNameServer );
    }

    /**
     * Constructor.
     *
     * @param urlIdSerializer knows how to create MeshObjectIdentifiers from strings
     * @param meshBaseNameServer the name server with which to look up MeshBases
     */
    protected DefaultWebMeshObjectsToViewFactory(
            MeshObjectIdentifierBothSerializer urlIdSerializer,
            MeshBaseNameServer                 meshBaseNameServer )
    {
        theUrlIdSerializer    = urlIdSerializer;
        theMeshBaseNameServer = meshBaseNameServer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView obtainFor(
            HttpRequest request )
        throws
            CannotViewException,
            ParseException
    {
        MeshObject            subject;
        MeshBase              mb = theMeshBaseNameServer.getDefaultMeshBase(); // for now
        String                viewletName;
        String                mimeType;
        WebViewletPlacement   requiredPlacement;
        ViewletDimensionality requiredDimensionality;
        ViewletDimensionality recommendedDimensionality;
        ViewletDetail         requiredDetail;
        ViewletDetail         recommendedDetail;

        WebViewletState           viewletState;
        WebViewletStateTransition viewletTransition;
        HashMap<String,Object[]>  viewletPars;

        Url requestUrl = request.getUrl();

        // Subject and MeshBase
        String relativeBaseUrl = requestUrl.getRelativeBaseUri();

        String trailer = relativeBaseUrl.substring( requestUrl.getContextPath().length() );
        trailer = UrlUtils.decodeUrl( trailer );

        MeshObjectIdentifier subjectIdentifier = theUrlIdSerializer.meshObjectIdentifierFromExternalForm( trailer );
        String whenString = requestUrl.getUrlArgument( WebMeshObjectsToView.WHEN_PAR_NAME );


        long   when;
        if( whenString == null ) {
            when    = Long.MAX_VALUE;
            subject = mb.findMeshObjectByIdentifier( subjectIdentifier );

        } else {
            MeshObjectHistory history = mb.meshObjectHistory( subjectIdentifier );

            when    = DateTimeUtil.rfc3339ToDate( whenString ).getTime();
            subject = history.atOrBefore( when );
        }


        if( subject == null ) {
            throw new CannotViewException.NoSubject( subjectIdentifier );
        }
        // Viewlet name
        viewletName = requestUrl.getUrlArgument( WebMeshObjectsToView.VIEWLET_PAR_NAME );

        // MIME type
        mimeType = requestUrl.getUrlArgument( WebMeshObjectsToView.MIME_PAR_NAME );

        // Placement et al
        requiredPlacement         = parsePlacement(      requestUrl.getUrlArgument( WebMeshObjectsToView.REQUIRED_VIEWLET_PLACEMENT_PAR_NAME ));
        requiredDimensionality    = parseDimensionality( requestUrl.getUrlArgument( WebMeshObjectsToView.REQUIRED_VIEWLET_DIMENSIONALITY_PAR_NAME ));
        recommendedDimensionality = parseDimensionality( requestUrl.getUrlArgument( WebMeshObjectsToView.RECOMMENDED_VIEWLET_DIMENSIONALITY_PAR_NAME ));
        requiredDetail            = parseDetail(         requestUrl.getUrlArgument( WebMeshObjectsToView.REQUIRED_VIEWLET_DETAIL_PAR_NAME ));
        recommendedDetail         = parseDetail(         requestUrl.getUrlArgument( WebMeshObjectsToView.RECOMMENDED_VIEWLET_DETAIL_PAR_NAME ));

        // ViewletState
        viewletState = WebViewletState.fromRequest( request );

        // ViewletStateTransition
        viewletTransition = WebViewletStateTransition.fromRequest( request );

        // remaining arguments (but not lid-include) are viewletPars
        viewletPars = new HashMap<>();

        for( String key : requestUrl.getUrlArguments().keySet() ) {
            switch( key ) {
                case WebMeshObjectsToView.VIEWLET_PAR_NAME:
                    break;
                case WebMeshObjectsToView.WHEN_PAR_NAME:
                    break;
                case WebMeshObjectsToView.MIME_PAR_NAME:
                    break;
                case WebMeshObjectsToView.REQUIRED_VIEWLET_PLACEMENT_PAR_NAME:
                    break;
                case WebMeshObjectsToView.REQUIRED_VIEWLET_DIMENSIONALITY_PAR_NAME:
                    break;
                case WebMeshObjectsToView.RECOMMENDED_VIEWLET_DIMENSIONALITY_PAR_NAME:
                    break;
                case WebMeshObjectsToView.REQUIRED_VIEWLET_DETAIL_PAR_NAME:
                    break;
                case WebMeshObjectsToView.RECOMMENDED_VIEWLET_DETAIL_PAR_NAME:
                    break;
                case WebViewletState.VIEWLET_STATE_PAR_NAME:
                    break;
                case WebViewletStateTransition.VIEWLET_STATE_TRANSITION_PAR_NAME:
                    break;

                default:
                    Object [] values = requestUrl.getMultivaluedUrlArgument( key );
                    viewletPars.put( key, values );
            }
        }

        if( requiredPlacement == null ) {
            requiredPlacement = WebViewletPlacement.ROOT; // default
        }

        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                subject,
                when,
                null,
                viewletName, // we categorize this as "recommended" because URLs may be old and Viewlets may have been renamed
                requiredPlacement,
                requiredDimensionality,
                recommendedDimensionality,
                requiredDetail,
                recommendedDetail,
                viewletPars,
                viewletState,
                viewletTransition,
                mimeType,
                request.isPost(),
                theUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView obtainFor(
            MeshObject subject,
            Url        contextUrl )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                subject,
                Long.MAX_VALUE,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false,
                theUrlIdSerializer );

        return ret;
    }

    /**
     * Helper to parse Viewlet parameter.
     */
    protected WebViewletPlacement parsePlacement(
            String s )
    {
        if( s == null ) {
            return null;
        }
        WebViewletPlacement ret = WebViewletPlacement.valueOf( s.toUpperCase() );
        return ret;
    }

    /**
     * Helper to parse Viewlet parameter.
     */
    protected ViewletDimensionality parseDimensionality(
            String s )
    {
        if( s == null ) {
            return null;
        }
        ViewletDimensionality ret = ViewletDimensionality.valueOf( s.toUpperCase() );
        return ret;
    }

    /**
     * Helper to parse Viewlet parameter.
     */
    protected ViewletDetail parseDetail(
            String s )
    {
        if( s == null ) {
            return null;
        }
        ViewletDetail ret = ViewletDetail.valueOf( s.toUpperCase() );
        return ret;
    }

    /**
     * Knows how to translate Strings to MeshObjectIdentifiers and back
     */
    protected final MeshObjectIdentifierBothSerializer theUrlIdSerializer;

    /**
     * The name server with which to look up MeshBases.
     */
    protected final MeshBaseNameServer theMeshBaseNameServer;
}
