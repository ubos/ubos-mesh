//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Thrown if an operation requires an HTTP Shell variable to be non-null, but no value could be found.
 */
public class SpecifiedMeshObjectNotFoundException
        extends
            AbstractLocalizedException
{
    /**
     * Constructor.
     *
     * @param varName name of the variable without a value
     */
    public SpecifiedMeshObjectNotFoundException(
            String varName )
    {
        theVariableName = varName;
    }

    /**
     * Obtain the name of the null variable.
     *
     * @return name of the variable
     */
    public String getArgumentName()
    {
        return theVariableName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theVariableName };
    }

    /**
     * Name of the variable without a value.
     */
    protected final String theVariableName;
}
