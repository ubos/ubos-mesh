//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.skin;

import java.io.IOException;
import java.io.OutputStream;
import net.ubos.web.vl.StructuredResponse;

/**
 * Classes supporting this interface know how to convert a StructuredResponse
 * into an output stream that can be sent back to the requesting client.
 */
public interface Skin
{
    /**
     * Obtain the Skin's type.
     *
     * @return the SkinType
     */
    public SkinType getType();

    /**
     * Format the StructuredResponse so it can be sent back to the client
     *
     * @param response the StructuredResponse
     * @param rawOutput the output to stream to
     * @throws IOException an I/O problem occurred
     * @throws SkinProcessingException a problem occurred
     * @return desired HTTP status
     */
    public int format(
            StructuredResponse response,
            OutputStream       rawOutput )
        throws
            IOException,
            SkinProcessingException;

    /**
     * Obtain the mime type of created responses.
     *
     * @return MIME type
     */
    public String getContentType();
}
