//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.util;

import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Subclasses Exception to also carry a desired HTTP response code.
 */
public class ExceptionWithHttpStatusCode
        extends
            AbstractLocalizedException
        implements
            CarriesHttpStatusCodeException
{
    /**
     * Constructor.
     *
     * @param status the desired HTTP status code, or -1 if none
     */
    public ExceptionWithHttpStatusCode(
            int status )
    {
        theDesiredHttpStatusCode = status;
    }

    /**
     * Constructor.
     *
     * @param msg message
     * @param status the desired HTTP status code
     */
    public ExceptionWithHttpStatusCode(
            String    msg,
            int       status )
    {
        super( msg );

        theDesiredHttpStatusCode = status;
    }

    /**
     * Constructor.
     *
     * @param cause the cause
     * @param status the desired HTTP status code
     */
    public ExceptionWithHttpStatusCode(
            Throwable cause,
            int       status )
    {
        super( null, cause );

        theDesiredHttpStatusCode = status;
    }

    /**
     * Constructor.
     *
     * @param msg message
     * @param cause the cause
     * @param status the desired HTTP status code
     */
    public ExceptionWithHttpStatusCode(
            String    msg,
            Throwable cause,
            int       status )
    {
        super( msg, cause );

        theDesiredHttpStatusCode = status;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDesiredHttpStatusCode()
    {
        return theDesiredHttpStatusCode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        Throwable cause = getCause();
        String    msg;
        if( cause == null ) {
            msg = "Error";
        } else {
            msg = cause.getLocalizedMessage();
            if( msg == null ) {
                msg = cause.getMessage();
            }
            if( msg == null ) {
                msg = cause.getClass().getName();
            }
        }
        return new Object[] { theDesiredHttpStatusCode, msg };
    }

    /**
     * The HTTP status code desired by this exception.
     */
    protected final int theDesiredHttpStatusCode;
}
