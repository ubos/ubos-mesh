//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.assets;

import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.web.HttpRequest;
import net.ubos.web.Url;
import net.ubos.web.util.CarriesHttpStatusCodeException;

/**
 * A requested asset could not be found.
 */
public class AssetNotFoundException
    extends
        AbstractLocalizedException
    implements
        CarriesHttpStatusCodeException
{
    /**
     * Constructor.
     * 
     * @param request the incoming request
     */
    public AssetNotFoundException(
            HttpRequest request )
    {
        theRequest = request;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDesiredHttpStatusCode()
    {
        return 404;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getLocalizationParameters()
    {
        Url u = theRequest.getUrl();

        return new Object[] {
            u.getContextualBaseUri(),
            u.getRelativeBaseUri(),
            u.getAbsoluteBaseUri(),
            u.getAbsoluteFullUri()
        };
    }
    
    /**
     * The incoming request.
     */
    protected final HttpRequest theRequest;
}
