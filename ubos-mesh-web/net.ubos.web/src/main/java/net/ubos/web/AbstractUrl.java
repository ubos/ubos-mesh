//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.Map;
import java.util.StringTokenizer;
import net.ubos.util.ArrayHelper;
import net.ubos.util.UrlUtils;
import net.ubos.util.logging.Log;

/**
 * Factors out functionality common to many implementations of Url.
 */
public abstract class AbstractUrl
        implements
            Url
{
    private static final Log log = Log.getLogInstance( AbstractUrl.class ); // our own, private logger

    /**
     * Private constructor, for subclasses only.
     *
     * @param protocol http or https
     * @param server the server
     * @param port the server port
     * @param serverPlusNonDefaultPort the server, plus, if the port is non-default, a colon and the port number
     * @param relativeBaseUri the relative base URI
     * @param queryString the string past the ? in the URL
     * @param urlArguments the arguments given in the URL, if any
     * @param contextPath the JEE context path
     */
    protected AbstractUrl(
            String                 protocol,
            String                 server,
            int                    port,
            String                 serverPlusNonDefaultPort,
            String                 relativeBaseUri,
            String                 queryString,
            Map<String,String[]>   urlArguments,
            String                 contextPath )
    {
        theProtocol                 = protocol;
        theServer                   = server;
        thePort                     = port;
        theServerPlusNonDefaultPort = serverPlusNonDefaultPort;
        theRelativeBaseUri          = relativeBaseUri;
        theQueryString              = queryString;
        theUrlArguments             = urlArguments;
        theContextPath              = contextPath;
    }

    /**
     * Helper method to construct the host-port combination if non-standard ports.
     *
     * @param server just the server
     * @param protocol the protocol, e.g. http
     * @param port the port number
     * @return the server-port combination
     */
    public static String constructServerPlusNonDefaultPort(
            String server,
            String protocol,
            int    port )
    {
        if( server == null ) {
            return null;
        }

        String httpHost = server;
        if( "http".equals( protocol )) {
            if( port != 80 ) {
                httpHost += ":" + port;
            }
        } else if( "https".equals( protocol )) {
            if( port != 443 ) {
                httpHost += ":" + port;
            }
        } else {
            httpHost += ":" + port;
        }
        return httpHost;
    }

    /**
     * Helper to parse URL-encoded name-value pairs, and put them in the right places.
     *
     * @param data the data to add
     * @param arguments the URL arguments in the process of being assembled
     */
    public static void addUrlEncodedArguments(
            String               data,
            Map<String,String[]> arguments )
    {
        if( data == null || data.length() == 0 ) {
            return;
        }
        if( data.charAt( 0 ) == '?' ) {
            data = data.substring( 1 );
        }

        StringTokenizer pairTokenizer = new StringTokenizer( data, "&" );
        while( pairTokenizer.hasMoreTokens() ) {
            String    pair     = pairTokenizer.nextToken();
            String [] keyValue = pair.split( "=", 2 );

            String key   = UrlUtils.decodeUrlArgument( keyValue[0] );
            String value = UrlUtils.decodeUrlArgument( keyValue.length == 2 ? keyValue[1] : "" ); // reasonable default?

            String [] haveAlready = arguments.get( key );
            String [] newValue;
            if( haveAlready == null ) {
                newValue = new String[] { value };
            } else {
                newValue = ArrayHelper.append( haveAlready, value, String.class );
            }
            arguments.put( key, newValue );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRootUri()
    {
        if( theRootUri == null ) {
            StringBuilder buf = new StringBuilder( 64 );
            buf.append( getProtocol());
            buf.append( "://" );
            buf.append( getServerPlusNonDefaultPort());
            theRootUri = buf.toString();
        }
        return theRootUri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRelativeBaseUri()
    {
        return theRelativeBaseUri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAbsoluteBaseUri()
    {
        if( theAbsoluteBaseUri == null ) {
            theAbsoluteBaseUri = getRootUri() + getRelativeBaseUri();
        }
        return theAbsoluteBaseUri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextualBaseUri()
    {
        String abs = getAbsoluteBaseUri();
        String ctx = getAbsoluteContextUri();

        if( !abs.startsWith( ctx ) ) {
            log.error( "Absolute URI does not start with context URI", abs, ctx );
            return abs;
        }
        return abs.substring( ctx.length() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRelativeFullUri()
    {
        if( theQueryString != null && theQueryString.length() != 0 ) {
            return theRelativeBaseUri + "?" + theQueryString;
        } else {
            return theRelativeBaseUri;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAbsoluteFullUri()
    {
        if( theAbsoluteFullUri == null ) {
            theAbsoluteFullUri = getRootUri() + getRelativeFullUri();
        }
        return theAbsoluteFullUri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextualFullUri()
    {
        String abs = getAbsoluteFullUri();
        String ctx = getAbsoluteContextUri();

        if( !abs.startsWith( ctx ) ) {
            log.error( "Absolute URI does not start with context URI", abs, ctx );
            return abs;
        }
        return abs.substring( ctx.length() );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getServer()
    {
        return theServer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getServerPlusNonDefaultPort()
    {
        return theServerPlusNonDefaultPort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPort()
    {
        return thePort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProtocol()
    {
        return theProtocol;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String [] getMultivaluedUrlArgument(
            String argName )
    {
        String [] ret = theUrlArguments.get( argName );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String getUrlArgument(
            String name )
    {
        String [] almost = getMultivaluedUrlArgument( name );
        if( almost == null || almost.length == 0 ) {
            return null;
        } else if( almost.length == 1 ) {
            return almost[0];
        }
        // let it pass if all of them have the same value
        boolean letPass = true;
        String firstValue = almost[0];
        for( int i=1 ; i<almost.length ; ++i ) {
            if( firstValue == null ) {
                if( almost[i] != null ) {
                    letPass = false;
                    break;
                }
            } else if( !firstValue.equals( almost[i] )) {
                letPass = false;
                break;
            }
        }

        if( !letPass ) {
            throw new IllegalStateException( "Argument " + name + " has " + almost.length + " values" );
        } else {
            log.warn( "Multiple arguments but with same value: " + name + " -> " + firstValue );
        }
        return firstValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFirstUrlArgument(
            String name )
    {
        String [] almost = getMultivaluedUrlArgument( name );
        if( almost == null || almost.length == 0 ) {
            return null;
        } else {
            return almost[0];
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String,String[]> getUrlArguments()
    {
        return Collections.unmodifiableMap( theUrlArguments );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean matchUrlArgument(
            String name,
            String value )
    {
        String [] found = getMultivaluedUrlArgument( name );
        if( found == null ) {
            return false;
        }
        for( String current : found ) {
            if( value.equals( current )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextPath()
    {
        return theContextPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAbsoluteContextUri()
    {
        if( theAbsoluteContextUri == null ) {
            StringBuilder buf = new StringBuilder();
            buf.append( getRootUri() );
            buf.append( theContextPath );
            theAbsoluteContextUri = buf.toString();
        }
        return theAbsoluteContextUri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Url getContextUrl()
    {
        if( theContextUrl == null ) {
            try {
                theContextUrl = SimpleUrl.create( getAbsoluteContextUri() );
            } catch( MalformedURLException ex ) {
                log.error( ex );
            }
        }
        return theContextUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextPathWithSlash()
    {
        return getContextPath() + "/";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAbsoluteContextUriWithSlash()
    {
        return getAbsoluteContextUri() + "/";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getQueryString()
    {
        return theQueryString;
    }

    /**
     * The http or https protocol.
     */
    protected String theProtocol;

    /**
     * The server.
     */
    protected String theServer;

    /**
     * The port.
     */
    protected int thePort;

    /**
     * Same as the server, except if a non-default port number is used, in which case it is
     * server + ":" + thePort.
     */
    protected String theServerPlusNonDefaultPort;

    /**
     * The relative base URI of the Request.
     */
    protected String theRelativeBaseUri;

    /**
     * The root URI of the Request.
     */
    private String theRootUri;

    /**
     * The absolute base URI of the Request.
     */
    private String theAbsoluteBaseUri;

    /**
     * The absolute full URI of the Request.
     */
    private String theAbsoluteFullUri;

    /**
     * The query String, if any.
     */
    protected String theQueryString;

    /**
     * The arguments to the Request provided as part of the URL, mapping from argument name to argument value.
     */
    protected Map<String,String[]> theUrlArguments;

    /**
     * The full absolute context URI.
     */
    protected String theAbsoluteContextUri;

    /**
     * The relative context path for this app.
     */
    protected String theContextPath;

    /**
     * The full Url of the context for this app.
     */
    protected Url theContextUrl;
}
