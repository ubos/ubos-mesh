//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Common functionality for IncomingSimpleHttpCookie and OutgoingSimpleHttpCookie.
 */
public abstract class AbstractSimpleHttpCookie
        extends
            AbstractHttpCookie
        implements
            CanBeDumped
{
    /**
     * Constructor.
     * 
     * @param name the name of the cookie
     * @param value the value of the cookie
     */
    protected AbstractSimpleHttpCookie(
            String name,
            String value )
    {
        theName    = name;
        theValue   = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue()
    {
        return theValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "theName",
                    "theValue"
                },
                new Object[] {
                    theName,
                    theValue
                });
    }

    /**
     * Name of the cookie.
     */
    protected final String theName;
    
    /**
     * Value of the cookie.
     */
    protected final String theValue;
}

