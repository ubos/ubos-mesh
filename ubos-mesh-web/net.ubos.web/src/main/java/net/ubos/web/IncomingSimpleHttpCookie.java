//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

/**
 * A simple, incoming cookie.
 */
public class IncomingSimpleHttpCookie
        extends
            AbstractSimpleHttpCookie
        implements
            IncomingHttpCookie
{
    /**
     * Factory method.
     *
     * @param name the name of the cookie
     * @param value the value of the cookie
     * @return the created IncomingSimpleSaneCookie
     */
    public static IncomingSimpleHttpCookie create(
            String name,
            String value )
    {
        return new IncomingSimpleHttpCookie( name, value );
    }

    /**
     * Constructor.
     *
     * @param name the name of the cookie
     * @param value the value of the cookie
     */
    protected IncomingSimpleHttpCookie(
            String name,
            String value )
    {
        super( name, value );
    }
}
