//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.util.DateTimeUtil;
import net.ubos.util.UrlUtils;
import net.ubos.util.logging.Log;
import net.ubos.vl.AbstractMeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletPlacement;

/**
 * Augments MeshObjectsToView for Web Viewlets.
 */
public class DefaultWebMeshObjectsToView
        extends
            AbstractMeshObjectsToView
        implements
            WebMeshObjectsToView
{
    private static final Log log = Log.getLogInstance( DefaultWebMeshObjectsToView.class );

    /**
     * Private constructor, use factory class.
     *
     * @param subject the subject for the Viewlet at the requested time
     * @param when the time in the subject's MeshObjectHistory at which it shall be accessed, or MAX_LONG if current
     * @param requiredViewletName the name of the Viewlet that must be used
     * @param recommendedViewletName the name of the Viewlet that should be used
     * @param viewletParameters the Viewlet parameters (eg size, zoom, ...) to use
     * @param state the ViewletState to to assume
     * @param transition the ViewletStateTransition to make
     * @param mimeType the requested MIME type, if any
     * @param toUrlIdSerializer MeshObjectIdentifierSerializer used to create URLs
     */
    DefaultWebMeshObjectsToView(
            MeshObject                     subject,
            long                           when,
            String                         requiredViewletName,
            String                         recommendedViewletName,
            WebViewletPlacement            requiredPlacement,
            ViewletDimensionality          requiredDimensionality,
            ViewletDimensionality          recommendedDimensionality,
            ViewletDetail                  requiredDetail,
            ViewletDetail                  recommendedDetail,
            Map<String,Object[]>           viewletParameters,
            WebViewletState                state,
            WebViewletStateTransition      transition,
            String                         mimeType,
            boolean                        isUpdate,
            MeshObjectIdentifierSerializer toUrlIdSerializer )
    {
        super(  subject,
                when,
                requiredViewletName,
                recommendedViewletName,
                requiredPlacement,
                requiredDimensionality,
                recommendedDimensionality,
                requiredDetail,
                recommendedDetail,
                viewletParameters );

        theState             = state;
        theTransition        = transition;
        theMimeType          = mimeType;
        theIsUpdate          = isUpdate;
        theToUrlIdSerializer = toUrlIdSerializer;
    }

   /**
     * {@inheritDoc}
     */
    @Override
    public WebViewletState getViewletState()
    {
        return theState;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebViewletStateTransition getViewletStateTransition()
    {
        return theTransition;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMimeType()
    {
        return theMimeType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUpdate()
    {
        return theIsUpdate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withAtTime(
            long atTime )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                atTime,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withSubject(
            MeshObject subject )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                subject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withAtCurrentTime()
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                Long.MAX_VALUE,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DefaultWebMeshObjectsToView withRequiredViewletName(
           String newViewletName )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                newViewletName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DefaultWebMeshObjectsToView withRecommendedViewletName(
           String newViewletName )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                newViewletName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRequiredViewletPlacement(
            ViewletPlacement newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) newValue,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRequiredViewletDimensionality(
            ViewletDimensionality newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                newValue,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRecommendedViewletDimensionality(
            ViewletDimensionality newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                newValue,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRequiredViewletDetail(
            ViewletDetail newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                newValue,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withRecommendedViewletDetail(
            ViewletDetail newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                newValue,
                theViewletParameters,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withViewletParameter(
            String name,
            Object newValue )
    {
        return withViewletParameter( name, new Object[] { newValue } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withViewletParameter(
            String    name,
            Object [] newValues )
    {
        HashMap<String,Object[]> newParams = new HashMap<>();
        if( theViewletParameters != null ) {
            newParams.putAll( theViewletParameters );
        }
        newParams.put( name, newValues );

        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                newParams,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withoutViewletParameter(
            String name )
    {
        HashMap<String,Object[]> newParams = new HashMap<>();
        if( theViewletParameters != null ) {
            newParams.putAll( theViewletParameters );
        }
        newParams.remove( name );

        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                newParams,
                theState,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withoutViewletParameters()
    {
        WebMeshObjectsToView ret;

        if( theViewletParameters == null ) {
            ret = this;

        } else {
            ret = new DefaultWebMeshObjectsToView(
                    theSubject,
                    theWhen,
                    theRequiredName,
                    theRecommendedName,
                    (WebViewletPlacement) theRequiredPlacement,
                    theRequiredDimensionality,
                    theRecommendedDimensionality,
                    theRequiredDetail,
                    theRecommendedDetail,
                    null,
                    theState,
                    theTransition,
                    theMimeType,
                    theIsUpdate,
                    theToUrlIdSerializer );
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withMimeType(
            String newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                theTransition,
                newValue,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withViewletState(
            WebViewletState newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                newValue,
                theTransition,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView withViewletStateTransition(
            WebViewletStateTransition newValue )
    {
        DefaultWebMeshObjectsToView ret = new DefaultWebMeshObjectsToView(
                theSubject,
                theWhen,
                theRequiredName,
                theRecommendedName,
                (WebViewletPlacement) theRequiredPlacement,
                theRequiredDimensionality,
                theRecommendedDimensionality,
                theRequiredDetail,
                theRecommendedDetail,
                theViewletParameters,
                theState,
                newValue,
                theMimeType,
                theIsUpdate,
                theToUrlIdSerializer );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String asUrlString()
    {
        Map<String,String[]> pars = new HashMap<>();

        if( theWhen != Long.MAX_VALUE ) {
            pars.put( WHEN_PAR_NAME, new String[] { DateTimeUtil.dateToRfc3339( new Date( theWhen )) } );
        }
        if( theViewletParameters != null ) {
            // filter string parameters only
            for( Map.Entry<String,Object[]> current : theViewletParameters.entrySet() ) {
                Object [] values        = current.getValue();
                String [] stringValues = new String[ values.length ];
                for( int i=0 ; i<values.length ; ++i ) {
                    if( values[i] instanceof String ) {
                        stringValues[i] = (String) values[i];
                    } else {
                        stringValues = null;
                        break;
                    }
                }
                if( stringValues != null ) {
                    pars.put( current.getKey(), stringValues );
                }
            }
        }
        if( theRequiredName != null ) {
            pars.put( VIEWLET_PAR_NAME, new String[] { theRequiredName } );
        } else if( theRecommendedName != null ) {
            pars.put( VIEWLET_PAR_NAME, new String[] { theRecommendedName } );
        }
        if( theMimeType != null ) {
            pars.put( MIME_PAR_NAME, new String[] { theMimeType } );
        }
        if( theState != null && !theState.isDefaultState() ) {
            pars.put( WebViewletState.VIEWLET_STATE_PAR_NAME, new String[] { theState.getName() } );
        }
        if( theRequiredPlacement != null ) {
            pars.put( REQUIRED_VIEWLET_PLACEMENT_PAR_NAME, new String[] { theRequiredPlacement.getName() } );
        }

        // This only applies to POST, not sure whether this method is ever invoked for POST
        // if( theTransition != null ) {
        //     pars.put( WebViewletStateTransition.VIEWLET_STATE_TRANSITION_PAR_NAME, new String[] { theTransition.getName() } );
        // }

        String ret = theToUrlIdSerializer.toExternalForm( getSubject().getIdentifier());
        ret = UrlUtils.appendArgumentsToUrl( ret, pars );

        return ret;
    }

    /**
     * The request MIME type, if any.
     */
    protected final String theMimeType;

    /**
     * The desired vl state, if any.
     */
    protected final WebViewletState theState;

    /**
     * The desired vl state transition, if any.
     */
    protected final WebViewletStateTransition theTransition;

    /**
     * True if this is an update request, e.g. HTTP POST.
     */
    protected final boolean theIsUpdate;

    /**
     * Knows how to serialize MeshObjectIdentifiers for the purposes of creating URLs.
     */
    protected final MeshObjectIdentifierSerializer theToUrlIdSerializer;
}
