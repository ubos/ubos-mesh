//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.web.HttpRequest;

/**
 * Implemented by classes that would like to take action before, during, or after
 * regular HttpShell processing.
 */
public interface HttpShellHandler
{
    /**
     * Obtain the name of the HttpShellHandler.
     *
     * @return the name
     */
    public String getName();

    /**
     * Invoked by the HttpShell before any other processing takes place.
     *
     * @param request the incoming request
     * @param defaultMeshBase the default MeshBase to use
     * @param now the time at which the HttpShell was invoked
     * @throws HttpShellException a problem occurred, check cause for details
     */
    public void beforeTransactionStart(
            HttpRequest    request,
            MeshBase       defaultMeshBase,
            TimeStampValue now )
        throws
            HttpShellException;

    /**
     * Invoked by the HttpShell after any Transaction was created, but before any
     * processing within the transaction takes place.
     *
     * @param request the incoming request
     * @param defaultMeshBase the default MeshBase to use
     * @param now the time at which the HttpShell was invoked
     * @throws HttpShellException a problem occurred, check cause for details
     * @throws TransactionException a problem with the Transaction occurred
     */
    public void afterTransactionStart(
            HttpRequest                                     request,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now )
        throws
            HttpShellException,
            TransactionException;

    /**
     * Invoked by the HttpShell after MeshObjects were accessed, but before
     * any property setting or relationship management takes place.
     *
     * @param request the incoming request
     * @param vars the variables set by the HttpShell
     * @param defaultMeshBase the default MeshBase to use
     * @param now the time at which the HttpShell was invoked
     * @throws HttpShellException a problem occurred, check cause for details
     * @throws TransactionException a problem with the Transaction occurred
     */
    public void afterAccess(
            HttpRequest                                     request,
            Map<String,MeshObject>                          vars,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now )
        throws
            HttpShellException,
            TransactionException;

    /**
     * Invoked by the HttpShell before Transactions are closed.
     *
     * @param request the incoming request
     * @param vars the variables set by the HttpShell
     * @param defaultMeshBase the default MeshBase to use
     * @param now the time at which the HttpShell was invoked
     * @throws HttpShellException a problem occurred, check cause for details
     * @throws TransactionException a problem with the Transaction occurred
     */
    public void beforeTransactionEnd(
            HttpRequest                                     request,
            Map<String,MeshObject>                          vars,

            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now )
        throws
            HttpShellException,
            TransactionException;

    /**
     * Invoked by the HttpShell after the Transactions have been closed.
     *
     * @param request the incoming request
     * @param vars the variables set by the HttpShell
     * @param defaultMeshBase the default MeshBase to use
     * @param now the time at which the HttpShell was invoked
     * @param maybeThrown if a Throwable was thrown, it is passed here
     * @return a URL where to redirect to, or null
     * @throws HttpShellException a problem occurred, check cause for details
     */
    public String afterTransactionEnd(
            HttpRequest                                     request,
            Map<String,MeshObject>                          vars,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now,
            Throwable                                       maybeThrown )
        throws
            HttpShellException;
}
