//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.assets;

import net.ubos.util.nameserver.NameServer;

import java.nio.file.Path;

/**
 * Maps names of assets to Paths in the file system.
 */
public interface AssetMap
    extends
        NameServer<String,Path>
{
}
