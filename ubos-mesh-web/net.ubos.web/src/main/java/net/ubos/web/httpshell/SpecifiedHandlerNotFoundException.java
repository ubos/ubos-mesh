//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Thrown if a specified HttpShellHandler could not be found.
 */
public class SpecifiedHandlerNotFoundException
        extends
            AbstractLocalizedException
{
    /**
     * Constructor.
     *
     * @param handlerName name of the HttpShellHandler that could not be found
     */
    public SpecifiedHandlerNotFoundException(
            String handlerName )
    {
        theHandlerName = handlerName;
    }

    /**
     * Obtain the name of the HttpShellHandler.
     *
     * @return name of the handler
     */
    public String getHandlerName()
    {
        return theHandlerName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theHandlerName };
    }

    /**
     * Name of the HttpShellHandler that could not be found.
     */
    protected final String theHandlerName;
}
