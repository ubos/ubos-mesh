//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.vl.CannotViewException;
import net.ubos.web.HttpRequest;
import net.ubos.web.Url;

/**
 * A factory for WebMeshObjectsToView objects.
 */
public interface WebMeshObjectsToViewFactory
{
    /**
     * Create a MeshObjectsToView by parsing an incoming request
     *
     * @param request the incoming request
     * @return the created MeshObjectsToView
     * @throws CannotViewException thrown if there was no Subject in the requiest
     * @throws ParseException thrown if the request could not be parsed
     */
    public WebMeshObjectsToView obtainFor(
            HttpRequest request )
        throws
            CannotViewException,
            ParseException;

    /**
     * Create a MeshObjectsToView for this subject MeshObject.
     *
     * @param subject the subject MeshObject
     * @param contextUrl the context URL of the app
     * @return the created MeshObjectsToView
     */
    public WebMeshObjectsToView obtainFor(
            MeshObject subject,
            Url        contextUrl );
}
