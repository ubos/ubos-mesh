//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;

/**
 * An HttpRequest that is a sub-request of another.
 */
public class SubHttpRequest
    extends
        AbstractHttpRequest
{
    /**
     * Factory method,
     *
     * @param parentRequest
     * @return the created SubHttpRequest
     */
    public static SubHttpRequest create(
            HttpRequest parentRequest )
    {
        return new SubHttpRequest( parentRequest, parentRequest.getHttpRequestAtProxy() );
    }

    /**
     * Constructor.
     *
     * @param parentRequest the HttpRequest of which we are a sub-request
     * @param parentRequestAtProxy the HttpRequest at the proxy of which we are a sub-request
     */
    protected SubHttpRequest(
            HttpRequest parentRequest,
            HttpRequest parentRequestAtProxy )
    {
        super( parentRequest.getUrl(), parentRequest.getMethod(), parentRequestAtProxy == null ? null : create( parentRequestAtProxy ));

        theParentRequest = parentRequest;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getMultivaluedPostedArgument(
            String argName )
    {
        return null; // not passed through
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String[]> getPostedArguments()
    {
        return null; // not passed through
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IncomingHttpCookie[] getCookies()
    {
        return null; // not passed through
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getClientIp()
    {
        return theParentRequest.getClientIp();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentType()
    {
        return theParentRequest.getContentType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPostData()
    {
        return null; // not passed through
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<String> requestedMimeTypesIterator()
    {
        return theParentRequest.requestedMimeTypesIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAcceptHeader()
    {
        return theParentRequest.getAcceptHeader();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAuthorizationHeader()
    {
        return theParentRequest.getAuthorizationHeader();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(
            String name,
            Object value )
    {
        if( theAttributes == null ) {
            theAttributes = new HashMap<>();
        }
        theAttributes.put( name, value );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getAttribute(
            String name )
    {
        if( theAttributes == null ) {
            return null;
        }
        return theAttributes.get( name );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAttribute(
            String name )
    {
        if( theAttributes != null ) {
            theAttributes.remove( name );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> getMimePartNames()
    {
        return ZeroElementCursorIterator.create(); // not passed through
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MimePart[] getMultivaluedMimeParts(
            String argName )
    {
        return null;
    }

    /**
     * The parent request.
     */
    protected final HttpRequest theParentRequest;

    /**
     * The local Attributes of the request, allocated as needed.
     */
    protected Map<String,Object> theAttributes = null;
}
