//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.util;

import net.ubos.util.ResourceHelper;

/**
 * Defines paths where packages store their assets.
 */
public interface PackageAware
{
    /**
     * Name of an environment variable whose value overrides the default directory in which
     * resources are found. Useful during web page development.
     */
    public static final String PACKAGE_RESOURCES_PARENT_DIR_ENV_VAR = "PACKAGE_RESOURCES_PARENT_DIR";

    /**
     * The value of the override environment variable.
     */
    public static final String thePackageResourcesParentDirOverride = System.getenv( PACKAGE_RESOURCES_PARENT_DIR_ENV_VAR );

    /**
     * The parent directory of all package code directories.
     */
    public static String [] PACKAGE_RESOURCES_PARENT_DIRS = Hack.calculatePackageResourcesParentDirs();

    /**
     * No static initializers in Java interfaces, as it seems.
     */
    static class Hack {
        private static String [] calculatePackageResourcesParentDirs() {
            String [] ret;
            if( thePackageResourcesParentDirOverride == null ) {
                ret = new String[] {
                    ResourceHelper.getInstance( PackageAware.class ).getResourceStringOrDefault(
                        "PackageResourcesParentDir",
                        "/ubos/share" )
                    };

            } else {
                String [] overrides = thePackageResourcesParentDirOverride.split( "[,:;\\s]+" );
                ret = new String[ overrides.length + 1 ];
                for( int i=0 ; i<overrides.length ; ++i ) {
                    ret[i] = overrides[i];
                }
                ret[ ret.length - 1 ]
                    = ResourceHelper.getInstance( PackageAware.class ).getResourceStringOrDefault(
                        "PackageResourcesParentDir",
                        "/ubos/share" );
            }
            return ret;
        }
    }
}
