//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.mesh.MeshObject;
import net.ubos.vl.PaginatingViewlet;

/**
 * A WebViewlet that also pages its content.
 *
 * @param <T> the type of the content that can be paged
 */
public interface PaginatingWebViewlet<T>
    extends
        WebViewlet,
        PaginatingViewlet<T>
{
    /**
     * Obtain the URL to the same Viewlet in the same configuration, except displaying
     * a different page.
     *
     * @param startObject the first MeshObject on the to-be-shown page
     * @return the URL, as String
     */
    public String getDestinationUrlForStart(
            T startObject );
}
