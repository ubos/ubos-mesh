//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

/**
 * An incoming Cookie.
 */
public interface IncomingHttpCookie
    extends
        HttpCookie
{
    // nothing
}
