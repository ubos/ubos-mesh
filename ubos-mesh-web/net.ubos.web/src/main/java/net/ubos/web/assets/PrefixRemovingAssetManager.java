//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.assets;

import java.nio.file.Path;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.MappingCursorIterator;

/**
 * Removes a path prefix from an asset map before delegating to another AssetMap.
 */
public class PrefixRemovingAssetManager
    implements
        AssetMap
{
    /**
     * Factory method.
     *
     * @param prefix the prefix of the path to remove
     * @param delegate the AssetMap to delegate to after path prefix removal
     * @return the created instance
     */
    public static PrefixRemovingAssetManager create(
            String   prefix,
            AssetMap delegate )
    {
        return new PrefixRemovingAssetManager( prefix, delegate );
    }

    /**
     * Private constructor, use factory method.
     *
     * @param prefix the prefix of the path to remove
     * @param delegate the AssetMap to delegate to after path prefix removal
     */
    protected PrefixRemovingAssetManager(
            String   prefix,
            AssetMap delegate )
    {
        thePrefix   = prefix;
        theDelegate = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Path get(
            String key )
    {
        if( key.startsWith( thePrefix )) {
            return theDelegate.get( key.substring( thePrefix.length() ));
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<String> keyIterator()
    {
        return new MappingCursorIterator<>( theDelegate.keyIterator(), new MappingCursorIterator.Mapper<String,String>() {
            @Override
            public String mapDelegateValueToHere(
                    String delegateValue )
            {
                return thePrefix + delegateValue;
            }

            @Override
            public String mapHereToDelegateValue(
                    String value )
            {
                throw new UnsupportedOperationException( "Should not be invoked" );
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<Path> valueIterator()
    {
        return new MappingCursorIterator<>( keyIterator(), new MappingCursorIterator.Mapper<Path,String>() {
            @Override
            public Path mapDelegateValueToHere(
                    String delegateValue )
            {
                return get( delegateValue );
            }

            @Override
            public String mapHereToDelegateValue(
                    Path value )
            {
                throw new UnsupportedOperationException( "Should not be invoked" );
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty()
    {
        return theDelegate.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size()
    {
        return theDelegate.size();
    }

    /**
     * The prefix to remove.
     */
    protected final String thePrefix;

    /**
     * The delegate AssetMap.
     */
    protected final AssetMap theDelegate;
}
