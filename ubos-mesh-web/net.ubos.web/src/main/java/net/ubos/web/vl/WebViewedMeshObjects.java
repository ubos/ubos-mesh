//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.vl.ViewedMeshObjects;

/**
 * Extends ViewedMeshObjects with web information.
 */
public interface WebViewedMeshObjects
        extends
            ViewedMeshObjects
{
    /**
     * Obtain the MIME type of the current response.
     *
     * @return the MIME type
     */
    public String getMimeType();

    /**
     * The current JeeViewletState.
     *
     * @return the current state
     */
    public WebViewletState getViewletState();

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView getMeshObjectsToView();
}
