//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.web.HttpRequest;

/**
 * Thrown if an operation is invoked with inconsistent arguments.
 */
public class InconsistentArgumentsException
        extends
            AbstractLocalizedException
{
    /**
     * Constructor.
     *
     * @param request the incoming request
     */
    public InconsistentArgumentsException(
            HttpRequest request )
    {
        theRequest = request;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            theRequest,
            theRequest.getUrl().getAbsoluteFullUri()
        };
    }

    /**
     * The incoming request.
     */
    protected final HttpRequest theRequest;
}
