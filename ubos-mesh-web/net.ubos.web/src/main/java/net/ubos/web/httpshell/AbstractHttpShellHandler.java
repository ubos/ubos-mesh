//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.web.HttpRequest;

/**
 * Provides empty default implementations of the methods defined in HttpShellHandler,
 * for easier subclassing, and a few convenience methods that are often useful
 * for subclasses.
 */
public abstract class AbstractHttpShellHandler
        implements
            HttpShellHandler
{
    /**
     * Constructor for subclasses only.
     */
    protected AbstractHttpShellHandler()
    {
        // nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return getClass().getName();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void beforeTransactionStart(
            HttpRequest                                     request,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now )
        throws
            HttpShellException
    {
        // nothing
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void afterTransactionStart(
            HttpRequest                                     request,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now )
        throws
            HttpShellException,
            TransactionException
    {
        // nothing
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void afterAccess(
            HttpRequest                                     request,
            Map<String,MeshObject>                          vars,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now )
        throws
            HttpShellException,
            TransactionException
    {
        // nothing
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void beforeTransactionEnd(
            HttpRequest                                     request,
            Map<String,MeshObject>                          vars,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now )
        throws
            HttpShellException,
            TransactionException
    {
        // nothing
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public String afterTransactionEnd(
            HttpRequest                                     request,
            Map<String,MeshObject>                          vars,
            MeshBase                                        defaultMeshBase,
            TimeStampValue                                  now,
            Throwable                                       maybeThrown )
        throws
            HttpShellException
    {
        // nothing
        return null;
    }

    /**
     * Helper method to get the raw, unresolved value of an HTTP shell variable, or
     * throw an exception if not given.
     *
     * @param request the incoming request
     * @param varName name of the shell variable
     * @return the raw String value of the shell variable
     * @throws HttpShellException thrown if the argument is missing or empty
     */
    protected String getArgumentOrThrow(
            HttpRequest request,
            String      varName )
        throws
            HttpShellException
    {
        return getArgumentOrThrow( request, varName, null );
    }

    /**
     * Helper method to get the raw, unresolved value of an HTTP shell variable, or
     * throw an exception if not given.
     *
     * @param request the incoming request
     * @param varName name of the shell variable
     * @param t the Throwable to throw (wrapped in an HttpShellException)
     * @return the raw String value of the shell variable
     * @throws HttpShellException thrown if the argument is missing or empty
     */
    protected String getArgumentOrThrow(
            HttpRequest request,
            String      varName,
            Throwable   t )
        throws
            HttpShellException
    {
        String argName = HttpShellKeywords.PREFIX + varName;
        String ret     = request.getPostedArgument( argName );
        if( ret == null || HttpShell.UNASSIGNED_VALUE.equals( ret )) {
            if( t != null ) {
                throw new HttpShellException( t );
            } else {
                throw new HttpShellException( new UnassignedArgumentException( argName ));
            }
        }
        return ret;
    }

    /**
     * Helper method to get the resolved value of an HTTP shell variable, or throw
     * an exception if not given.
     *
     * @param vars the resolved variables
     * @param varName name of the shell variable
     * @return the resolved value of the shell variable
     * @throws HttpShellException thrown if the variable is null
     */
    protected MeshObject getVariableOrThrow(
            Map<String,MeshObject> vars,
            String                 varName )
        throws
            HttpShellException
    {
        return getVariableOrThrow( vars, varName, null );
    }

    /**
     * Helper method to get the resolved value of an HTTP shell variable, or throw
     * an exception if not given.
     *
     * @param vars the resolved variables
     * @param varName name of the shell variable
     * @param t the Throwable to throw (wrapped in an HttpShellException)
     * @return the resolved value of the shell variable
     * @throws HttpShellException thrown if the variable is null
     */
    protected MeshObject getVariableOrThrow(
            Map<String,MeshObject> vars,
            String                 varName,
            Throwable              t )
        throws
            HttpShellException
    {
        MeshObject ret = vars.get( varName );
        if( ret == null ) {
            String argName = HttpShellKeywords.PREFIX + varName;

            if( t != null ) {
                throw new HttpShellException( t );
            } else {
                throw new HttpShellException( new UnassignedArgumentException( argName ));
            }
        }
        return ret;
    }

    /**
     * Helper method to get the resolved value of an HTTP shell variable, or return
     * null if not given. This is merely a convenience method for consistency purposes.
     *
     * @param vars the resolved variables
     * @param varName name of the shell variable
     * @return the resolved value of the shell variable, or null
     */
    protected MeshObject getVariable(
            Map<String,MeshObject> vars,
            String                 varName )
    {
        MeshObject ret = vars.get( varName );
        return ret;
    }
}
