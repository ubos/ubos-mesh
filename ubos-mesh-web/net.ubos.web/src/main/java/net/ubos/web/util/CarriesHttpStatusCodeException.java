//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.util;

/**
 * Interface supported by those Exceptions that like to influence the HTTP status code returned
 * to the client.
 */
public interface CarriesHttpStatusCodeException
{
    /**
     * Obtain the HTTP status code desired by this exception.
     * 
     * @return the desired HTTP status code, or -1 if none
     */
    public int getDesiredHttpStatusCode();
}
