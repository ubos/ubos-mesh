//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import net.ubos.util.logging.Log;

/**
 * A simple, outgoing cookie.
 */
public class OutgoingSimpleHttpCookie
        extends
            AbstractSimpleHttpCookie
        implements
            OutgoingHttpCookie
{
    private static final Log log = Log.getLogInstance( OutgoingSimpleHttpCookie.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param name the name of the cookie
     * @param value the value of the cookie
     * @param domain the domain of the cookie
     * @param path the path of the cookie
     * @param secure if true, only send this cookie over a secure connection
     * @param removed if true, this means the cookie is to be removed
     * @param expires expiration date of the cookie
     * @return the created OutgoingSimpleSaneCookie
     */
    public static OutgoingSimpleHttpCookie create(
            String  name,
            String  value,
            String  domain,
            String  path,
            boolean secure,
            boolean removed,
            Date    expires )
    {
        return new OutgoingSimpleHttpCookie( name, value, domain, path, secure, removed, expires );
    }

    /**
     * Factory method.
     *
     * @param name the name of the cookie
     * @param value the value of the cookie
     * @param domain the domain of the cookie
     * @param path the path of the cookie
     * @param expires expiration date of the cookie
     * @return the created OutgoingSimpleSaneCookie
     */
    public static OutgoingSimpleHttpCookie create(
            String name,
            String value,
            String domain,
            String path,
            Date   expires )
    {
        return new OutgoingSimpleHttpCookie( name, value, domain, path, false, false, expires );
    }

    /**
     * Factory method for a cookie to be removed.
     *
     * @param name the name of the cookie
     * @param domain the domain of the cookie
     * @param path the path of the cookie
     * @return the created OutgoingSimpleSaneCookie
     */
    public static OutgoingSimpleHttpCookie createToBeRemoved(
            String name,
            String domain,
            String path )
    {
        return new OutgoingSimpleHttpCookie( name, "**removed**", domain, path, false, true, null );
    }

    /**
     * Constructor.
     *
     * @param name the name of the cookie
     * @param value the value of the cookie
     * @param domain the domain of the cookie
     * @param path the path of the cookie
     * @param secure if true, only send this cookie over a secure connection
     * @param removed if true, this means the cookie is to be removed
     * @param expires expiration date of the cookie
     */
    protected OutgoingSimpleHttpCookie(
            String  name,
            String  value,
            String  domain,
            String  path,
            boolean secure,
            boolean removed,
            Date    expires )
    {
        super( name, value );

        thePath    = path;
        theExpires = expires;
        theSecure  = secure;
        theRemoved = removed;

        if( "localhost".equalsIgnoreCase( domain )) {
            // weird special case rules for cookie settings, otherwise browser will ignore them
            domain = null;
        }
        theDomain  = domain;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDomain()
    {
        return theDomain;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath()
    {
        return thePath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getExpires()
    {
        return theExpires;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getSecure()
    {
        return theSecure;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getIsRemovedOrExpired()
    {
        if( theRemoved ) {
            return true;
        }
        if( theExpires == null ) {
            return false;
        } else if( theExpires.after( new Date() )) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAsHttpValue()
    {
        return "Set-Cookie: " + getAsJavascriptValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAsJavascriptValue()
    {
        StringBuilder buf = new StringBuilder();
        buf.append( encodeCookieName( theName ));
        buf.append( '=' );
        buf.append( encodeToQuotedString( theValue ));
        if( thePath != null ) {
            buf.append(  "; path=" );
            buf.append( thePath );
        }
        if( theDomain != null && !"localhost".equalsIgnoreCase( theDomain )) {
            // can't mention "localhost", browser won't take it
            buf.append( "; domain=" );
            buf.append( theDomain );
        }
        if( theExpires != null ) {
            buf.append( "; max-age=" );
            long delta = theExpires.getTime() - System.currentTimeMillis();
            if( delta > 0 ) {
                buf.append( String.valueOf( delta / 1000L ));
            } else {
                buf.append( "0" );
            }
        }
        if( theSecure ) {
            buf.append( "; secure" );
        }

        return buf.toString();
    }

    /**
     * Encode a cookie's name safely. Notably, PHP has this
     * nastly habit of turning periods into underscores.
     *
     * @param name the to-be-encoded name
     * @return the encoded name
     */
    public static String encodeCookieName(
            String name )
    {
        String temp = URLEncoder.encode( name, StandardCharsets.UTF_8 );
        String ret  = temp.replaceAll( "\\.", "!" );

        return ret;
    }

    /**
     * Encode a String a quoted String per the HTTP spec.
     *
     * @param raw the to-be-quoted String
     * @return the quoted String
     */
    public static String encodeToQuotedString(
            String raw )
    {
        StringBuilder buf = new StringBuilder( raw.length() + 4 ); // fudge

        buf.append( '"' );
        for( int i=0 ; i<raw.length() ; ++i ) {
            char c = raw.charAt( i );
            switch( c ) {
                case '"':
                    buf.append( "\\\"" );
                    break;
                default:
                    buf.append( c );
                    break;
            }
        }
        buf.append( '"' );
        return buf.toString();
    }

    /**
     * Domain of the cookie.
     */
    protected final String theDomain;

    /**
     * Path of the cookie, if any.
     */
    protected final String thePath;

    /**
     * Date when the cookie expires.
     */
    protected final Date theExpires;

    /**
     * True if this Cookie is supposed to be, or already removed.
     */
    protected final boolean theRemoved;

    /**
     * True if this Cookie shall only be sent over a secure protocol.
     */
    protected final boolean theSecure;
}
