//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.util.StringHelper;
import net.ubos.util.UrlUtils;

/**
 * Simple URL class implementing the SaneUrl interface.
 */
public class SimpleUrl
        extends
            AbstractUrl
{
    /**
     * Factory method.
     *
     * @param s the absolute URL in String form
     * @return the created SimpleUrl
     * @throws MalformedURLException thrown if the URL is malformed
     */
    public static SimpleUrl create(
            String s )
        throws
            MalformedURLException
    {
        return create( s, null );
    }

    /**
     * Factory method.
     *
     * @param s the absolute URL in String form
     * @param pars query parameters; the old ones will be discarded if this is not null;
     * @return the created SimpleUrl
     * @throws MalformedURLException thrown if the URL is malformed
     */
    public static SimpleUrl create(
            String               s,
            Map<String,String[]> pars )
        throws
            MalformedURLException
    {
        Matcher m = ABSOLUTE_URL_PATTERN.matcher( s );
        if( m.matches() ) {
            String protocol    = m.group( 1 );
            String server      = m.group( 2 );
            String port        = m.group( 4 );
            String file        = m.group( 5 );
            String queryString = m.group( 7 );

            int realPort;
            if( port != null && port.length() > 0 ) {
                realPort = Integer.parseInt( port );
            } else {
                if( "http".equals( protocol )) {
                    realPort = 80;
                } else if( "https".equals( protocol )) {
                    realPort = 443;
                } else {
                    realPort = -1; // don't know
                }
            }
            String serverPlusNonDefaultPort = constructServerPlusNonDefaultPort( server, protocol, realPort );

            if( file == null ) {
                file = "";
            }

            Map<String,String[]> urlArguments;

            if( pars == null ) {
                urlArguments = new HashMap<>();
                addUrlEncodedArguments( queryString, urlArguments );

            } else {
                urlArguments = pars;

                StringBuilder newQueryString = new StringBuilder();
                String        sep            = "";
                for( Map.Entry<String,String[]> current : pars.entrySet() ) {
                    String    encKey = UrlUtils.encodeToValidUrlArgument( current.getKey() );
                    String [] values = current.getValue();
                    if( values.length == 0 ) {
                        newQueryString.append( sep );
                        newQueryString.append( encKey );
                        sep = "&";
                    } else {
                        for( String value : values ) {
                            newQueryString.append( sep );
                            newQueryString.append( encKey );
                            newQueryString.append( '=' );
                            newQueryString.append( UrlUtils.encodeToValidUrlArgument( value ));
                            sep = "&";
                        }
                    }
                }
            }

            SimpleUrl ret = new SimpleUrl(
                    protocol,
                    server,
                    realPort,
                    serverPlusNonDefaultPort,
                    file,
                    queryString,
                    urlArguments,
                    "" );

            return ret;
        }

        // didn't work out
        throw new MalformedURLException( s );
    }

    /**
     * Private constructor, for subclasses only.
     *
     * @param protocol http or https
     * @param server the server
     * @param port the server port
     * @param serverPlusNonDefaultPort the server, plus, if the port is non-default, a colon and the port number
     * @param relativeBaseUri the relative base URI
     * @param queryString the string past the ? in the URL
     * @param urlArguments the arguments given in the URL, if any
     * @param contextPath the JEE context path
     */
    protected SimpleUrl(
            String                 protocol,
            String                 server,
            int                    port,
            String                 serverPlusNonDefaultPort,
            String                 relativeBaseUri,
            String                 queryString,
            Map<String,String[]>   urlArguments,
            String                 contextPath )
    {
        super( protocol, server, port, serverPlusNonDefaultPort, relativeBaseUri, queryString, urlArguments, contextPath );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SimpleUrl appendOrReplaceUrlArgument(
            String name,
            String value )
    {
        Map<String,String[]> newArguments = new HashMap<>();
        for( Map.Entry<String,String[]> entry : theUrlArguments.entrySet() ) {
            if( !name.equals(  entry.getKey() )) {
                newArguments.put( entry.getKey(), entry.getValue() );
            }
        }
        newArguments.put(  name, new String[] { value } );

        return new SimpleUrl(
                theProtocol,
                theServer,
                thePort,
                theServerPlusNonDefaultPort,
                theRelativeBaseUri,
                theQueryString,
                newArguments,
                theContextPath );
    }

    /**
     * The Pattern we use to parse absolute URLs. This is quite lenient, which may be a bug or a feature, not sure.
     */
    public static final Pattern ABSOLUTE_URL_PATTERN = Pattern.compile( "^([^:/]+)://([^:/]+)(:(\\d+))?(/[^\\?#]*)?(\\?([^#]*))?(#(.*))?$" );
}
