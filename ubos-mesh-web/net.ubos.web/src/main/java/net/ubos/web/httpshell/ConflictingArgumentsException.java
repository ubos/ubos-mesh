//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.web.HttpRequest;

/**
 * Thrown if an operation is invoked with conflicting arguments.
 */
public class ConflictingArgumentsException
        extends
            InconsistentArgumentsException
{
    /**
     * Constructor.
     *
     * @param arg1Name name of the first conflicting argument
     * @param arg2Name name of the second conflicting argument
     * @param request the incoming request
     */
    public ConflictingArgumentsException(
            String      arg1Name,
            String      arg2Name,
            HttpRequest request )
    {
        super( request );

        theArgument1Name = arg1Name;
        theArgument2Name = arg2Name;
    }
    
    /**
     * Obtain the name of the first argument that was conflicting.
     *
     * @return name of the argument
     */
    public String getArgument1Name()
    {
        return theArgument1Name;
    }

    /**
     * Obtain the name of the second argument that was conflicting.
     *
     * @return name of the argument
     */
    public String getArgument2Name()
    {
        return theArgument2Name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            theArgument1Name,
            theArgument2Name,
            theRequest,
            theRequest.getUrl().getAbsoluteFullUri()
        };
    }

    /**
     * Name of the first argument that was conflicting.
     */
    protected final String theArgument1Name;

    /**
     * Name of the second argument that was conflicting.
     */
    protected final String theArgument2Name;
}
