//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.util.exception.AbstractLocalizedException;

/**
 * Thrown if an operation is invoked with an argument that does not have a value.
 */
public class UnassignedArgumentException
        extends
            AbstractLocalizedException
{
    /**
     * Constructor. Emit the default error message.
     *
     * @param argName name of the argument without a value
     */
    public UnassignedArgumentException(
            String argName )
    {
        this( argName, null );
    }

    /**
     * Constructor. Emit a custom error message.
     *
     * @param argName name of the argument without a value
     * @param msg the custom error message
     */
    public UnassignedArgumentException(
            String argName,
            String msg )
    {
        theArgumentName = argName;
        theMessage      = msg;
    }

    /**
     * Obtain the name of the missing argument.
     *
     * @return name of the argument
     */
    public String getArgumentName()
    {
        return theArgumentName;
    }

    /**
     * Obtain resource parameters for the internationalization.
     *
     * @return the resource parameters
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] { theArgumentName };
    }

    /**
     * Name of the missing argument.
     */
    protected final String theArgumentName;
    
    /**
     * Custom error message.
     */
    protected final String theMessage;
}
