//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import java.util.Iterator;
import java.util.Map;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * Encapsulates an incoming HTTP request. Unlike typical implementations in
 * app or web servers, this implementation is aware of any reverse proxies,
 * and instances can be chained reflecting the reverse proxy topology.
 */
public interface HttpRequest
{
    /**
     * Obtain the URL that was requested.
     *
     * @return the URL
     */
    public abstract Url getUrl();

    /**
     * If this request was obtained by way of a reverse proxy, return the request
     * that the reverse proxy received. Returns null if no reverse proxy was involved.
     *
     * @return the request at the reverse proxy, or null if none
     */
    public abstract HttpRequest getHttpRequestAtProxy();

    /**
     * Obtain the original request as originally issued by the HTTP client. If a reverse
     * proxy was involved, return the request that the reverse proxy received. If
     * no reverse proxy was involved, return this request.
     *
     * @return the ultimate request
     */
    public abstract HttpRequest getOriginalHttpRequest();

    /**
     * Determine the HTTP method (such as GET).
     *
     * @return the HTTP method
     */
    public abstract String getMethod();

    /**
     * Obtain a POST'd argument. If more than one argument is given by this name,
     * this will throw an IllegalStateException.
     *
     * @param argName name of the argument
     * @return value.
     */
    public abstract String getPostedArgument(
            String argName );

    /**
     * Obtain all values of a multi-valued POST'd argument
     *
     * @param argName name of the argument
     * @return value.
     */
    public abstract String [] getMultivaluedPostedArgument(
            String argName );

    /**
     * Obtain all POST'd arguments of this Request.
     *
     * @return a Map of name to value mappings for all POST'd arguments
     */
    public abstract Map<String,String[]> getPostedArguments();

    /**
     * Determine whether a named POST'd argument has the given value.
     * This method is useful in case several arguments have been given with the same name.
     *
     * @param name the name of the argument
     * @param value the desired value of the argument
     * @return true if the request contains an argument with this name and value
     */
    public abstract boolean matchPostedArgument(
            String name,
            String value );

    /**
     * Obtain the cookies that were sent as part of this Request.
     *
     * @return the cookies that were sent as part of this Request.
     */
    public abstract IncomingHttpCookie [] getCookies();

    /**
     * Obtain a named cookie, or null if not present.
     *
     * @param name the name of the cookie
     * @return the named cookie, or null
     */
    public abstract IncomingHttpCookie getCookie(
            String name );

    /**
     * Obtain the value of a named cookie, or null if not present.
     *
     * @param name the name of the cookie
     * @return the value of the named cookie, or null
     */
    public abstract String getCookieValue(
            String name );

    /**
     * Obtain the client IP address.
     *
     * @return the client IP address
     */
    public abstract String getClientIp();

    /**
     * Obtain the MIME type of the content of the request.
     *
     * @return the MIME type, or null
     */
    public abstract String getContentType();

    /**
     * Obtain the content of the request, e.g. HTTP POST data.
     *
     * @return the content of the request, or null
     */
    public abstract String getPostData();

    /**
     * Obtain an Iterator over the requested MIME types, if any. Return the higher-priority
     * MIME types first.
     *
     * @return Iterator over the requested MIME types, if any.
     */
    public abstract Iterator<String> requestedMimeTypesIterator();

    /**
     * Obtain the value of the accept header, if any.
     *
     * @return the value of the accept header
     */
    public abstract String getAcceptHeader();

    /**
     * Obtain the value of the authorization header, if any.
     *
     * @return the value of the authorization header
     */
    public abstract String getAuthorizationHeader();

    /**
     * Set a request-context attribute. The semantics are equivalent to setting
     * an attribute on an HttpServletRequest.
     *
     * @param name the name of the attribute
     * @param value the value of the attribute
     * @see #getAttribute
     * @see #removeAttribute
     */
    public abstract void setAttribute(
            String name,
            Object value );

    /**
     * Get a request-context attribute. The semantics are equivalent to getting
     * an attribute on an HttpServletRequest.
     *
     * @param name the name of the attribute
     * @return the value of the attribute
     * @see #setAttribute
     * @see #removeAttribute
     */
    public abstract Object getAttribute(
            String name );

    /**
     * Remove a request-context attribute. The semantics are equivalent to removing
     * an attribute on an HttpServletRequest.
     *
     * @param name the name of the attribute
     * @see #setAttribute
     * @see #getAttribute
     */
    public abstract void removeAttribute(
            String name );

    /**
     * Obtain the names of the MimeParts conveyed.
     *
     * @return the names of the MimeParts
     */
    public abstract CursorIterator<String> getMimePartNames();

    /**
     * Obtain all MimeParts with a given name
     *
     * @param argName name of the MimePart
     * @return the values, or <code>null</code>
     */
    public abstract MimePart [] getMultivaluedMimeParts(
            String argName );

    /**
     * Obtain a MimePart that was HTTP POST'd. If more than one MimePart was posted with this name,
     * this will throw an IllegalStateException.
     *
     * @param argName name of the argument
     * @return the MimePart or null
     */
    public abstract MimePart getMimePart(
            String argName );

    /**
     * Determine whether this is a HTTP POST request.
     *
     * @return true if it is an HTTP POST request
     */
    public abstract boolean isPost();

    /**
     * Determine whether submitted data has been protected against CSRF attacks.
     * This returns false only for POST requests.
     */
    public abstract boolean isCsrfProtected();

    /**
     * Obtain the value for the CSRF token that came in as cookie.
     *
     * @return the token
     */
    public abstract String getCsrfToken();

    /**
     * Obtain the name of the cookie that contains the CSRF token.
     *
     * @return the cookie name
     */
    public default String getCsrfCookieName()
    {
        return "csrf";
    }

    /**
     * Obtain the name of the input field in a form that contains the CRSF token.
     *
     * @return the field name
     */
    public default String getCsrfFormInputName()
    {
        return "csrf";
    }
}
