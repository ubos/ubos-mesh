//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.skin;

/**
 * Something went wrong during processing of a Skin.
 */
public class SkinProcessingException
    extends
        Exception
{
    /**
     * Constructor.
     * 
     * @param cause the underlying cause
     */
    public SkinProcessingException(
            Throwable cause )
    {
        super( cause );
    }
}
