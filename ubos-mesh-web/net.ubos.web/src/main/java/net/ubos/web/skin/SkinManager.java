//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.skin;

import net.ubos.util.nameserver.NameServer;

/**
 * Knows how to manage the SkinMaps that of various SkinTypes, keyed by the SkinType.
 */
public interface SkinManager
    extends
        NameServer<SkinType,SkinMap>
{
    /**
     * Obtain the SkinMap to use when a SkinMap of the specified SkinType could
     * not be found.
     *
     * @return the fallback
     */
    public SkinMap getFallbackSkinMap();
}
