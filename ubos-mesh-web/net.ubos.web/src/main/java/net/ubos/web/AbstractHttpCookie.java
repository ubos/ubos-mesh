//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

/**
 * Factors out functionality common to HttpCookie implementations.
 */
public abstract class AbstractHttpCookie
        implements
            HttpCookie
{
    /**
     * {@inheritDoc}
     */
    @Override
    public char charAt(
            int index )
    {
        return getValue().charAt( index );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int length()
    {
        return getValue().length();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CharSequence subSequence(
            int start,
            int end )
    {
        return getValue().subSequence( start, end );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return getValue();
    }
}
