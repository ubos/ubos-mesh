//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import net.ubos.util.ArrayHelper;

/**
 * Factors out functionality common to many implementations of HttpRequest.
 */
public abstract class AbstractHttpRequest
        implements
            HttpRequest
{
    /**
     * Private constructor, for subclasses only.
     *
     * @param url the requested URL
     * @param method the HTTP method
     * @param requestAtProxy the request received by the reverse proxy, if any
     */
    protected AbstractHttpRequest(
            Url         url,
            String      method,
            HttpRequest requestAtProxy )
    {
        theUrl            = url;
        theMethod         = method.toUpperCase();
        theRequestAtProxy = requestAtProxy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Url getUrl()
    {
        return theUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMethod()
    {
        return theMethod;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HttpRequest getHttpRequestAtProxy()
    {
        return theRequestAtProxy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HttpRequest getOriginalHttpRequest()
    {
        if( theRequestAtProxy == null ) {
            return this;
        } else {
            return theRequestAtProxy.getOriginalHttpRequest();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String getPostedArgument(
            String argName )
    {
        String [] almost = getMultivaluedPostedArgument( argName );
        if( almost == null || almost.length == 0 ) {
            return null;
        } else if( almost.length == 1 ) {
            return almost[0];
        } else {
            throw new IllegalStateException( "POST argument '" + argName + "' posted more than once: " + ArrayHelper.join( almost ));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean matchPostedArgument(
            String name,
            String value )
    {
        String [] found = getMultivaluedPostedArgument( name );
        if( found == null ) {
            return false;
        }
        for( String current : found ) {
            if( value.equals( current )) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IncomingHttpCookie getCookie(
            String name )
    {
        IncomingHttpCookie [] cookies = getCookies();
        if( cookies != null ) {
            for( int i=0 ; i<cookies.length ; ++i ) {
                if( cookies[i].getName().equals( name )) {
                    return cookies[i];
                }
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCookieValue(
            String name )
    {
        HttpCookie cook = getCookie( name );
        if( cook != null ) {
            return cook.getValue();
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MimePart getMimePart(
            String argName )
    {
        MimePart [] parts = getMultivaluedMimeParts( argName );
        if( parts == null || parts.length == 0 ) {
            return null;
        } else if( parts.length == 1 ) {
            return parts[0];
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPost()
    {
        return POST_METHOD.equals( theMethod );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCsrfProtected()
    {
        if( ! POST_METHOD.equals( theMethod )) {
            return true;
        }

        IncomingHttpCookie cookie = getCookie( getCsrfCookieName() );
        if( cookie == null ) {
            return false;
        }
        String fromCookie = cookie.getValue();
        String fromForm   = getPostedArgument( getCsrfFormInputName() );
        
        return fromCookie.equals( fromForm );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCsrfToken()
    {
        IncomingHttpCookie cookie = getCookie( getCsrfCookieName() );
        if( cookie == null ) {
            return null;
        } else {
            return cookie.getValue();
        }
    }

    /**
     * Helper method to convert a class name into a suitable attribute name.
     *
     * @param clazz the Class
     * @return the attribute name
     */
    public static String classToAttributeName(
            Class<?> clazz )
    {
        String ret = clazz.getName();
        ret = ret.replaceAll( "\\.", "_" );
        return ret;
    }

    /**
     * Helper method to convert a class name and a local fragment into a suitable attribute name.
     *
     * @param clazz the Class
     * @param fragment the fragment, or local id
     * @return the attribute name
     */
    public static String classToAttributeName(
            Class<?> clazz,
            String   fragment )
    {
        String ret = clazz.getName();
        ret = ret.replaceAll( "\\.", "_" );
        ret = ret + "__" + fragment;
        return ret;
    }

    /**
     * The requested Url.
     */
    protected Url theUrl;

    /**
     * The http method, such as GET.
     */
    protected final String theMethod;

    /**
     * The request as it was received by the reverse proxy, if any.
     */
    protected final HttpRequest theRequestAtProxy;

    /**
     * Name of the cookie that might contain Accept-Language information.
     */
    public static final String ACCEPT_LANGUAGE_COOKIE_NAME = "Accept-Language";

    /**
     * Name of the HTTP Header that specifies the acceptable MIME types.
     */
    protected static final String ACCEPT_HEADER = "Accept";
    
    /**
     * Name of the POST method.
     */
    protected static final String POST_METHOD = "POST";
}
