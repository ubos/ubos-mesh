//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.skin;

import net.ubos.util.nameserver.WritableNameServer;

/**
 * Maps names of Skins to the actual Skins.
 */
public interface SkinMap
    extends
        WritableNameServer<String,Skin>
{
}
