//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.ubos.mesh.EntityBlessedAlreadyException;
import net.ubos.mesh.EntityNotBlessedException;
import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.IllegalPropertyValueException;
import net.ubos.mesh.IsAbstractException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.NotPermittedException;
import net.ubos.mesh.RoleTypeBlessedAlreadyException;
import net.ubos.mesh.RoleTypeNotBlessedException;
import net.ubos.mesh.RoleTypeRequiresEntityTypeException;
import net.ubos.mesh.security.ReadOnlyMeshBaseViewException;
import net.ubos.mesh.text.StringRepresentationScheme;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.PropertyValueParsingException;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierSerializer;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.ResourceHelper;
import net.ubos.util.UrlUtils;
import net.ubos.util.factory.FactoryException;
import net.ubos.util.logging.Log;
import net.ubos.util.nameserver.NameServer;
import net.ubos.web.HttpRequest;
import net.ubos.web.MimePart;
import net.ubos.web.ProblemReporter;

/**
 * <p>Recognizes <code>MeshObject</code> change-related requests as part of the incoming HTTP
 *    request and processes them. The protocol to express those change-related requests has been
 *    constructed to make it easy to issue them from HTML forms using HTTP POST.
 *
 * <p>Here is a summary of the syntax:
 *
 * <dl>
 *  <dt>shell.foo=&lt;id&gt;
 *  <dd>Shell variable <code>foo</code> points to MeshObject with id &lt;id&gt;
 *
 *  <dt>shell.foo.access=&lt;verb&gt;
 *  <dd>The MeshObject referred to by shell variable <code>foo</code> is supposed to be accessed with
 *      HttpShellAccessVerb &lt;verb&gt; (e.g. find, create...)
 *
 *  <dt>shell.foo.attributename.qux=&lt;name&gt;
 *  <dd>Identifies the Attribute with name &lt;name&gt; of the MeshObject referred to by
 *      shell variable <code>foo</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.attributevalue.qux=&lt;value&gt;
 *  <dd>Identifies the value of the Attribute named <code>name</code> of the MeshObject referred to by
 *      shell variable <code>foo</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.attributevalue.qux.null=true
 *  <dd>If given, indicates that the value of the Attribute named <code>name</code> of the MeshObject
 *      referred to by shell variable <code>foo</code> shall be null, instead of what is
 *      given by <code>shell.foo.attributevalue.qux</code>.
 *
 *  <dt>shell.foo.attributedelete.qux=true
 *  <dd>Indicates that the Attribute named <code>name</code> of the MeshObject referred to by
 *      shell variable <code>foo</code> is supposed to be deleted.
 *
 *  <dt>shell.foo.propertytype.qux=&lt;id&gt;
 *  <dd>Identifies the Property with PropertyType &lt;id&gt; of the MeshObject referred to by
 *      shell variable <code>foo</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.propertyvalue.qux=&lt;value&gt;
 *  <dd>Identifies the value of a Property referred to by <code>qux</code> of the MeshObject referred to by
 *      shell variable <code>foo</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.propertyvalue.qux.null=true
 *  <dd>If given, indicates that the value of the Property referred to by <code>qux</code> of the MeshObject
 *      referred to by shell variable <code>foo</code> shall be null, instead of what is
 *      given by <code>shell.foo.propertyvalue.qux</code>.
 *
 *  <dt>shell.foo.propertyvalue.qux.upload=&lt;part&gt;
 *  <dd>Names the MIME part in this request that contains the future value for the Property referred to
 *      by <code>qux</code> of the MeshObject referred to by shell variable <code>foo</code>.
 *
 *  <dt>shell.foo.propertyvalue.qux.mime=&lt;mimetype&gt;
 *  <dd>Specififies the MIME type of the future value for the Property referred to by
 *      <code>qux</code> of the MeshObject referred to by shell variable <code>foo</code>. This only
 *      applies to BlobValues.
 *
 *  <dt>shell.foo.bless=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the EntityTypes with which
 *      the MeshObject referred to by shell variable <code>foo</code> shall be blessed.
 *
 *  <dt>shell.foo.blessIfNeeded=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the EntityTypes with which
 *      the MeshObject referred to by shell variable <code>foo</code> shall be blessed.
 *      Silently ignore those EntityTypes with which the MeshObject is blessed already.
 *
 *  <dt>shell.foo.unbless=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the EntityTypes from which
 *      the MeshObject referred to by shell variable <code>foo</code> shall be unblessed.
 *
 *  <dt>shell.foo.unblessIfNeeded=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the EntityTypes from which
 *      the MeshObject referred to by shell variable <code>foo</code> shall be unblessed.
 *      Silently ignore those EntityTypes with which the MeshObject is not blessed.
 *
 *  <dt>shell.foo.to.bar.attributename.qux=&lt;name&gt;
 *  <dd>Identifies the Role Attribute with name &lt;name&gt; of the relationship from
 *      the MeshObject referred to by shell variable <code>foo</code>, to the MeshObject referred
 *      to by shell variable <code>bar</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.to.bar.attributevalue.qux=&lt;value&gt;
 *  <dd>Identifies the value of the Role Attribute named <code>name</code> of the relationship from
 *      the MeshObject referred to by shell variable <code>foo</code>, to the MeshObject referred
 *      to by shell variable <code>bar</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.to.bar.attributevalue.qux.null=true
 *  <dd>If given, indicates that the value of the Role Attribute named <code>name</code> of
 *      the relationship from the MeshObject referred to by shell variable <code>foo</code>, to
 *      the MeshObject referred to by shell variable <code>bar</code>, shall be null, instead
 *      of what is set in <code>shell.foo.to.bar.attributevalue.qux</code>.
 *
 *  <dt>shell.foo.to.bar.attributedelete.qux=true
 *  <dd>Indicates that the Role Attribute named <code>name</code> of
 *      the relationship from the MeshObject referred to by shell variable <code>foo</code>, to
 *      the MeshObject referred to by shell variable <code>bar</code>, supposed to be deleted.
 *
 *  <dt>shell.foo.to.bar.blessRole=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the RoleTypes with
 *      which the relationship from the MeshObject referred to by shell variable <code>foo</code>
 *      to the MeshObject referred to by shell variable <code>bar</code> shall be blessed.
 *
 *  <dt>shell.foo.to.bar.blessRoleIfNeeded=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the RoleTypes with
 *      which the relationship from the MeshObject referred to by shell variable <code>foo</code>
 *      to the MeshObject referred to by shell variable <code>bar</code> shall be blessed.
 *      Silently ignore those RoleTypes with which the relationship is blessed already.
 *
 *  <dt>shell.foo.to.bar.unblessRole=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the RoleTypes from
 *      which the relationship from the MeshObject referred to by shell variable <code>foo</code>
 *      to the MeshObject referred to by shell variable <code>bar</code> shall be unblessed.
 *
 *  <dt>shell.foo.to.bar.blessRoleIfNeeded=&lt;id&gt;
 *  <dd>Gives the identifier (or identifiers, if multi-valued), of the RoleTypes from
 *      which the relationship from the MeshObject referred to by shell variable <code>foo</code>
 *      to the MeshObject referred to by shell variable <code>bar</code> shall be unblessed.
 *      Silently ignore those RoleTypes with which the relationship is not blessed.
 *
 *  <dt>shell.foo.to.bar.unrelate=true
 *  <dd>Specifies that the MeshObject referred to by shell variable <code>foo</code>
 *      and the MeshObject referred to by shell variable <code>bar</code> shall be unrelated
 *      from each other. This includes unblessing from all RoleTypes and deleting all
 *      RoleProperties.
 *
 *  <dt>shell.foo.to.bar.propertytype.qux=&lt;id&gt;
 *  <dd>Identifies the Role Property with identifier &lt;id&gt; of the relationship from
 *      the MeshObject referred to by shell variable <code>foo</code>, to the MeshObject referred
 *      to be shell variable <code>bar</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.to.bar.propertyvalue.qux=&lt;value&gt;
 *  <dd>Identifies the value of the Role Property with identifier <code>id</code> of the relationship from
 *      the MeshObject referred to by shell variable <code>foo</code>, to the MeshObject referred
 *      to be shell variable <code>bar</code>, for the purposes of setting its value.
 *
 *  <dt>shell.foo.to.bar.propertyvalue.qux.null=true
 *  <dd>If given, indicates that the value of the Role Property with identifier <code>id</code> of
 *      the relationship from the MeshObject referred to by shell variable <code>foo</code>, to
 *      the MeshObject referred to by shell variable <code>bar</code>, shall be null, instead
 *      of what is set in <code>shell.foo.to.bar.propertyvalue.qux</code>.
 * </dl>
 *
 * <p>FIXME: document the check boxes and radio boxes.
 */
public class HttpShell
    implements
        HttpShellKeywords
{
    private static final Log log = Log.getLogInstance( HttpShell.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param handlerDirectory directory of known HttpShellHandlers
     * @param parsingScheme the StringRepresentationScheme to use
     * @param formIdDeserializer use this to deserialize MeshObjectIdentifiers that have been posted
     * @param redirectIdSerializer use this to serialize MeshObjectIdentifiers turned to redirect URLs
     * @param mb the MeshBase to operate on
     * @return the created shell
     */
    public static HttpShell create(
            NameServer<String,HttpShellHandler> handlerDirectory,
            StringRepresentationScheme          parsingScheme,
            MeshObjectIdentifierDeserializer    formIdDeserializer,
            MeshObjectIdentifierSerializer      redirectIdSerializer,
            MeshBase                            mb )
    {
        Set<String> preferVarsFromRequest = new HashSet<>();
        preferVarsFromRequest.add( "CALLER" );

        return new HttpShell(
                handlerDirectory,
                parsingScheme,
                formIdDeserializer,
                redirectIdSerializer,
                mb,
                preferVarsFromRequest );
    }

    /**
     * Factory method.
     *
     * @param handlerDirectory directory of known HttpShellHandlers
     * @param parsingScheme the StringRepresentationScheme to use
     * @param formIdDeserializer use this to deserialize MeshObjectIdentifiers that have been posted
     * @param redirectIdSerializer use this to serialize MeshObjectIdentifiers turned to redirect URLs
     * @param mb the MeshBase to operate on
     * @param preferVarsFromRequest a set of HTTP shell variables that should always taken from
     *        the request attributes, instead of from the incoming POST parameter
     * @return the created shell
     */
    public static HttpShell create(
            NameServer<String,HttpShellHandler> handlerDirectory,
            StringRepresentationScheme          parsingScheme,
            MeshObjectIdentifierDeserializer    formIdDeserializer,
            MeshObjectIdentifierSerializer      redirectIdSerializer,
            MeshBase                            mb,
            Set<String>                         preferVarsFromRequest )
    {
        return new HttpShell(
                handlerDirectory,
                parsingScheme,
                formIdDeserializer,
                redirectIdSerializer,
                mb,
                preferVarsFromRequest );
    }

    /**
     * Constructor.
     *
     * @param handlerDirectory directory of known HttpShellHandlers
     * @param parsingScheme the StringRepresentationScheme to use
     * @param formIdDeserializer use this to deserialize MeshObjectIdentifiers that have been posted
     * @param redirectIdSerializer use this to serialize MeshObjectIdentifiers turned to redirect URLs
     * @param mb the MeshBase to operate on
     * @param preferVarsFromRequest a set of HTTP shell variables that should always taken from
     *        the request attributes, instead of from the incoming POST parameter
     */
    protected HttpShell(
            NameServer<String,HttpShellHandler> handlerDirectory,
            StringRepresentationScheme          parsingScheme,
            MeshObjectIdentifierDeserializer    formIdDeserializer,
            MeshObjectIdentifierSerializer      redirectIdSerializer,
            MeshBase                            mb,
            Set<String>                         preferVarsFromRequest )
    {
        theHandlerDirectory      = handlerDirectory;
        theParsingScheme         = parsingScheme;
        theFormIdDeserializer    = formIdDeserializer;
        theRedirectIdSerializer  = redirectIdSerializer;
        theMeshBase              = mb;
        thePreferVarsFromRequest = preferVarsFromRequest;
    }

    /**
     * Main operation.
     *
     * @param request the incoming request
     * @return null, or, if given, a URL to redirect to without further processing
     *
     * @throws IOException if an input/output error occurs
     */
    public String performOperationsIfNeeded(
            final HttpRequest request )
        throws
            IOException
    {
        if( !request.isPost()) {
            return null;
        }

        String redirectUrl  = null;

        try {
            String command = request.getPostedArgument( FULL_SUBMIT_TAG );
            if( command == null || command.equals( SUBMIT_COMMIT_VALUE )) {
                redirectUrl = theMeshBase.execute( (tx) -> performOperations( request ));
            }

        } catch( Throwable ex ) {
            log.warn( ex );

            ProblemReporter reporter = (ProblemReporter) request.getAttribute( ProblemReporter.PROBLEM_REPORTER_ATTRIBUTE_NAME );
            if( reporter != null ) {
                reporter.reportProblem( ex );
            }
        }

        return redirectUrl;
    }

    /**
     * Perform all factory methods contained in the request.
     *
     * @param request the incoming request
     * @return URL to redirect to, if any
     * @throws NotPermittedException thrown if the caller had insufficient privileges to perform this operation
     * @throws HttpShellException a factory Exception occurred
     */
    protected String performOperations(
            final HttpRequest request )
        throws
            NotPermittedException,
            HttpShellException
    {
        Map<String,String[]>              postArguments = request.getPostedArguments();
        final ArrayList<HttpShellHandler> handlers      = new ArrayList<>();
        String                            ret           = null;

        // determine handlers
        String [] handlerNames = postArguments.get( HANDLER_TAG );
        if( handlerNames != null ) {
            for( String handlerName : handlerNames ) {
                try {
                    HttpShellHandler handler = theHandlerDirectory.get( handlerName );
                    if( handler == null ) {
                        throw new SpecifiedHandlerNotFoundException( handlerName );
                    }
                    handlers.add( handler );

                } catch( Throwable ex ) {
                    throw new HttpShellException( ex );
                }
            }
        }
        HashMap<String,MeshObject> variables = new HashMap<>();
        Throwable                  thrown    = null;
        TimeStampValue             now       = TimeStampValue.now();

        // invoke pre-transaction
        for( HttpShellHandler handler : handlers ) {
            handler.beforeTransactionStart( request, theMeshBase, now );
        }

        // pre-resolve the variables from the request attributes
        if( thePreferVarsFromRequest != null ) {
            for( String varName : thePreferVarsFromRequest ) {
                Object varValue = request.getAttribute( varName );
                if( varValue == null || varValue instanceof MeshObject ) {
                    variables.put( varName, (MeshObject) varValue );
                } else {
                    variables.put( varName, null );
                    log.error( "HttpShellFilter variable", varName, "obtained from request has wrong type:", varValue );
                }
            }
        }

        try {
            // first look for all arguments of the form <PREFIX>.<VARIABLE>
            for( String arg : postArguments.keySet() ) {
                if( !arg.startsWith( PREFIX )) {
                    continue; // skip all that aren't for us
                }
                String coreArg = arg.substring( PREFIX.length() );
                if( coreArg.equals( SUBMIT_TAG )) {
                    continue; // skip submit tag
                }
                if( coreArg.equals( COMMAND_TAG )) {
                    continue; // skip command tag
                }
                if( coreArg.contains( SEPARATOR ) ) {
                    continue; // skip all that aren't referring to the MeshObjects
                }
                String varName  = coreArg;

                if( variables.containsKey( varName )) {
                    continue; // no repetition please
                }
                String varValue = request.getPostedArgument( arg ); // use Request's error handling for multiple values

                if( UNASSIGNED_VALUE.equals( varValue )) {
                    throw new HttpShellException( new UnassignedArgumentException( arg ));
                }
                HttpShellAccessVerb accessVerb = HttpShellAccessVerb.findAccessFor( varName, request );

                MeshObjectIdentifier id = parseMeshObjectIdentifier( varValue );

                MeshObject accessed = accessVerb.access( id, theMeshBase, request );
                variables.put( varName, accessed );
            }

            theMeshBase.execute( () -> {
                // then look for all arguments of the form <PREFIX>.<VARIABLE>.<ACCESS_TAG> for which
                // there is no corresponding <PREFIX>.<VARIABLE>. This implies that a new MeshObject shall be created
                // with an automatically-generated MeshObjectIdentifier.
                for( String arg : postArguments.keySet() ) {
                    if( !arg.startsWith( PREFIX )) {
                        continue; // skip all that aren't for us
                    }
                    if( !arg.endsWith( ACCESS_TAG )) {
                        continue; // not in this loop
                    }
                    String coreArg = arg.substring( PREFIX.length(), arg.length()-ACCESS_TAG.length() );
                    String varName = coreArg;
                    if( variables.containsKey( varName )) {
                        // dealt with this one already
                        continue;
                    }
                    HttpShellAccessVerb accessVerb = HttpShellAccessVerb.findAccessFor( varName, request );

                    MeshObject accessed = accessVerb.access( null, theMeshBase, request );
                    variables.put( varName, accessed );
                }

                // invoke after access
                for( HttpShellHandler handler : handlers ) {
                    handler.afterAccess( request, variables, theMeshBase, now );
                }

                // perform operations that do not involve neighbors
                for( Map.Entry<String,MeshObject> entry : variables.entrySet() ) {
                    String     varName  = entry.getKey();
                    MeshObject accessed = entry.getValue();

                    if( accessed != null && !accessed.getIsDead() ) {

                        MeshBase mb = accessed.getMeshBase();
                        if( mb != null ) {
                            potentiallySetAttributes(    varName, accessed, request );
                            potentiallyDeleteAttributes( varName, accessed, request );

                            // first bless then unbless, then properties
                            potentiallyBless(         varName, accessed, request );
                            potentiallyUnbless(       varName, accessed, request );
                            potentiallySetProperties( varName, accessed, request );

                        } else {
                            throw new ReadOnlyMeshBaseViewException( mb );
                        }
                    }
                }

                // now bless roles
                for( Map.Entry<String,MeshObject> entry : variables.entrySet() ) {
                    String     var1Name = entry.getKey();
                    MeshObject var1     = entry.getValue();

                    if( var1 != null && !var1.getIsDead() ) {
                        String key = PREFIX + var1Name + TO_TAG + SEPARATOR;

                        for( String arg : postArguments.keySet() ) {
                            if( !arg.startsWith( key )) {
                                continue; // not relevant here
                            }
                            if( arg.endsWith( BLESS_ROLE_TAG )) {
                                String     var2Name = arg.substring( key.length(), arg.length()-BLESS_ROLE_TAG.length() );

                                potentiallyBlessRoles( var1Name, var2Name, variables, request.getMultivaluedPostedArgument( arg ));
                            }
                            if( arg.endsWith( BLESS_ROLE_IF_NEEDED_TAG )) {
                                String     var2Name = arg.substring( key.length(), arg.length()-BLESS_ROLE_IF_NEEDED_TAG.length() );

                                potentiallyBlessRolesIfNeeded( var1Name, var2Name, variables, request.getMultivaluedPostedArgument( arg ));
                            }
                        }
                    }
                }

                // now unbless roles
                for( Map.Entry<String,MeshObject> entry : variables.entrySet() ) {
                    String     var1Name = entry.getKey();
                    MeshObject var1     = entry.getValue();

                    if( var1 != null && !var1.getIsDead() ) {
                        String key = PREFIX + var1Name + TO_TAG + SEPARATOR;

                        for( String arg : postArguments.keySet() ) {
                            if( !arg.startsWith( key )) {
                                continue; // not relevant here
                            }
                            if( arg.endsWith( UNBLESS_ROLE_TAG )) {
                                String var2Name = arg.substring( key.length(), arg.length()-UNBLESS_ROLE_TAG.length() );

                                potentiallyUnblessRoles( var1Name, var2Name, variables, request.getMultivaluedPostedArgument( arg ));
                            }
                            if( arg.endsWith( UNBLESS_ROLE_IF_NEEDED_TAG )) {
                                String var2Name = arg.substring( key.length(), arg.length()-UNBLESS_ROLE_IF_NEEDED_TAG.length() );

                                potentiallyUnblessRolesIfNeeded( var1Name, var2Name, variables, request.getMultivaluedPostedArgument( arg ));
                            }
                        }
                    }
                }

                // set and delete Role Attributes
                for( Map.Entry<String,MeshObject> entry : variables.entrySet() ) {
                    String     var1Name = entry.getKey();
                    MeshObject var1     = entry.getValue();

                    if( var1 != null && !var1.getIsDead() ) {
                        String key = PREFIX + var1Name + TO_TAG + SEPARATOR;
                        StringBuilder buf;

                        for( String arg : postArguments.keySet() ) {
                            if( !arg.startsWith( key )) {
                                continue; // not relevant here
                            }

                            int anTag = arg.indexOf( ATTRIBUTE_NAME_TAG );
                            if( anTag > 0 ) {
                                // a Role Attribute name
                                String var2Name   = arg.substring( key.length(), anTag );
                                String attVarName = arg.substring( anTag + ATTRIBUTE_NAME_TAG.length() );

                                String attName    = request.getPostedArgument( arg );

                                MeshObject var2 = variables.get( var2Name );
                                if( var2 != null && var2.getIsDead() ) {
                                    continue;
                                }

                                buf = new StringBuilder();
                                buf.append( PREFIX );
                                buf.append( var1Name );
                                buf.append( TO_TAG );
                                buf.append( SEPARATOR );
                                buf.append( var2Name );
                                buf.append( ATTRIBUTE_DELETE_TAG );
                                buf.append( attVarName );

                                String attDeleteKey    = buf.toString();
                                String attDeleteString = request.getPostedArgument( attDeleteKey );

                                if( ATTRIBUTE_DELETE_TAG_TRUE.equals( attDeleteString )) {
                                    var1.deleteRoleAttribute( var2, attName );

                                } else {
                                    buf = new StringBuilder();
                                    buf.append( PREFIX );
                                    buf.append( var1Name );
                                    buf.append( TO_TAG );
                                    buf.append( SEPARATOR );
                                    buf.append( var2Name );
                                    buf.append( ATTRIBUTE_VALUE_TAG );
                                    buf.append( attVarName );

                                    String attValueKey    = buf.toString();
                                    String attValueString = request.getPostedArgument( attValueKey );

                                    buf = new StringBuilder();
                                    buf.append( PREFIX );
                                    buf.append( var1Name );
                                    buf.append( TO_TAG );
                                    buf.append( SEPARATOR );
                                    buf.append( var2Name );
                                    buf.append( ATTRIBUTE_VALUE_TAG );
                                    buf.append( attVarName );
                                    buf.append( NULL_ATTRIBUTE_VALUE_TAG );

                                    String nullValueKey    = buf.toString();
                                    String nullValueString = request.getPostedArgument( nullValueKey );

                                    Serializable value;

                                    // null has preference over upload, which has preference over the regular value
                                    if( NULL_ATTRIBUTE_VALUE_TAG_TRUE.equals( nullValueString )) {
                                        value = null;

                                    } else if( attValueString != null ) {
                                        value = theParsingScheme.parseAttributeValue( attValueString, attVarName );

                                    } else {
                                        // nothing given: leave as is
                                        continue;
                                    }

                                    var1.setRoleAttributeValue( var2, attName, value );
                                }
                            }
                        }
                    }
                }

                // now set RoleProperty values
                for( Map.Entry<String,MeshObject> entry : variables.entrySet() ) {
                    String     var1Name = entry.getKey();
                    MeshObject var1     = entry.getValue();

                    if( var1 != null && !var1.getIsDead() ) {
                        String key = PREFIX + var1Name + TO_TAG + SEPARATOR;
                        StringBuilder buf;

                        for( String arg : postArguments.keySet() ) {
                            if( !arg.startsWith( key )) {
                                continue; // not relevant here
                            }

                            int ptTag = arg.indexOf( PROPERTY_TYPE_TAG );
                            if( ptTag > 0 ) {
                                // a Role Attribute name
                                String var2Name   = arg.substring( key.length(), ptTag );
                                String attVarName = arg.substring( ptTag + PROPERTY_TYPE_TAG.length() );

                                MeshObject var2 = variables.get( var2Name );
                                if( var2 != null && var2.getIsDead() ) {
                                    continue;
                                }

                                buf = new StringBuilder();
                                buf.append( PREFIX );
                                buf.append( var1Name );
                                buf.append( TO_TAG );
                                buf.append( SEPARATOR );
                                buf.append( var2Name );
                                buf.append( PROPERTY_VALUE_TAG );
                                buf.append( attVarName );

                                String propValueKey    = buf.toString();
                                String propValueString = request.getPostedArgument( propValueKey );
                                String propTypeString  = request.getPostedArgument( arg );

                                PropertyType propertyType = (PropertyType) findMeshType( propTypeString );

                                buf = new StringBuilder();
                                buf.append( PREFIX );
                                buf.append( var1Name );
                                buf.append( TO_TAG );
                                buf.append( SEPARATOR );
                                buf.append( var2Name );
                                buf.append( PROPERTY_VALUE_TAG );
                                buf.append( attVarName );
                                buf.append( NULL_PROPERTY_VALUE_TAG );

                                String nullValueKey    = buf.toString();
                                String nullValueString = request.getPostedArgument( nullValueKey );

                                PropertyValue value;

                                // null has preference over upload, which has preference over the regular value
                                if( NULL_PROPERTY_VALUE_TAG_TRUE.equals( nullValueString )) {
                                    value = null;

                                } else if( propValueString != null ) {
                                    value = theParsingScheme.parsePropertyValue( propValueString, propertyType );

                                } else {
                                    // nothing given: leave as is
                                    continue;
                                }

                                var1.setRolePropertyValue( var2, propertyType, value );
                            }
                        }
                    }
                }

                // now unrelate
                for( Map.Entry<String,MeshObject> entry : variables.entrySet() ) {
                    String     var1Name = entry.getKey();
                    MeshObject var1     = entry.getValue();

                    if( var1 != null && !var1.getIsDead() ) {
                        String key = PREFIX + var1Name + TO_TAG + SEPARATOR;

                        for( String arg : postArguments.keySet() ) {
                            if( !arg.startsWith( key )) {
                                continue; // not relevant here
                            }
                            if( arg.endsWith( UNRELATE_TAG )) {
                                String var2Name = arg.substring( key.length(), arg.length()-UNRELATE_TAG.length() );
                                MeshObject var2 = variables.get( var2Name );
                                if( var2 != null && var2.getIsDead() ) {
                                    continue;
                                }

                                String unrelateValueString = request.getPostedArgument( arg );
                                if( UNRELATE_TAG_TRUE.equals( unrelateValueString )) {
                                    var1.unrelate( var2 );
                                }
                            }
                        }
                    }
                }

                // now deal with checkboxes and radioboxes
                for( String var1Name : variables.keySet() ) {
                    MeshObject var1 = variables.get( var1Name );
                    if( var1 != null && var1.getIsDead() ) {
                        continue;
                    }

                    String key = PREFIX + var1Name + TO_TAG + SEPARATOR;

                    for( String arg : postArguments.keySet() ) {
                        if( !arg.startsWith( key )) {
                            continue; // not relevant here
                        }
                        if( arg.endsWith( CHECKBOX_ROLE_TAG )) {
                            String     var2Name = arg.substring( key.length(), arg.length()-CHECKBOX_ROLE_TAG.length() );
                            MeshObject found2   = variables.get( var2Name );
                            MeshObject found1   = variables.get( var1Name );

                            if( found2 == null ) {
                                throw new HttpShellException( new SpecifiedMeshObjectNotFoundException( var2Name ));
                            }
                            if( found1 == null ) {
                                throw new HttpShellException( new SpecifiedMeshObjectNotFoundException( var1Name ));
                            }
                            String   value = request.getPostedArgument( arg );
                            RoleType rt    = (RoleType) findMeshType( value );

                            // now look for whether the checkbox argument has been POST'd or not
                            String arg2 = arg.substring( 0, arg.length()-CHECKBOX_ROLE_TAG.length() ) + CHECKBOX_TAG;
                            String [] values = request.getMultivaluedPostedArgument( arg2 );

                            if( found1.getMeshBase() != null ) {

                                if( values != null && values.length > 0 ) {
                                    // relate and bless
                                    try {
                                        found1.blessRole( rt, found2 );
                                    } catch( RoleTypeBlessedAlreadyException ex ) {
                                        // ignore
                                    }
                                } else {
                                    // unbless and possibly unrelate
                                    try {
                                        found1.unblessRole( rt, found2 );
                                    } catch( RoleTypeNotBlessedException ex ) {
                                        // ignore
                                    }
                                }
                            } else {
                                 throw new ReadOnlyMeshBaseViewException( found1.getMeshBaseView() );
                            }
                        }
                        if( arg.endsWith( RADIOBOX_ROLE_TAG )) {
                            String     var2Name = arg.substring( key.length(), arg.length()-RADIOBOX_ROLE_TAG.length() );
                            MeshObject found2   = variables.get( var2Name );
                            MeshObject found1   = variables.get( var1Name );

                            if( found2 == null ) {
                                throw new HttpShellException( new SpecifiedMeshObjectNotFoundException( var2Name ));
                            }
                            if( found1 == null ) {
                                throw new HttpShellException( new SpecifiedMeshObjectNotFoundException( var1Name ));
                            }
                            String   value = request.getPostedArgument( arg );
                            RoleType rt    = (RoleType) findMeshType( value );

                            String radiogroupName = request.getPostedArgument( key + var2Name + RADIOBOX_NAME_TAG );
                            if( radiogroupName == null ) {
                                continue;
                            }
                            if( found1.getMeshBase() != null ) {

                                String doBless = request.getPostedArgument( radiogroupName );
                                if( doBless != null && doBless.equals( key + var2Name + RADIOBOX_TAG )) {
                                    // relate and bless
                                    try {
                                        found1.blessRole( rt, found2 );
                                    } catch( RoleTypeBlessedAlreadyException ex ) {
                                        // ignore
                                    }
                                } else {
                                    // unbless and possibly unrelate
                                    try {
                                        found1.unblessRole( rt, found2 );
                                    } catch( RoleTypeNotBlessedException ex ) {
                                        // ignore
                                    }
                                }
                            } else {
                                 throw new ReadOnlyMeshBaseViewException( found1.getMeshBaseView() );
                            }
                        }
                    }
                }
                // invoke pre-transaction
                for( HttpShellHandler handler : handlers ) {
                    handler.beforeTransactionEnd( request, variables, theMeshBase, now );
                }
            } );

        } catch( HttpShellException ex ) {
            thrown = ex;
            throw ex;

        } catch( ParseException ex ) {
            thrown = ex;
            throw new HttpShellException( ex );

        } catch( MeshObjectsNotFoundException ex ) {
            thrown = ex;
            throw new HttpShellException( ex );

        } catch( TransactionException ex ) {
            thrown = ex;
            log.error( ex ); // should not happen

        } catch( RuntimeException ex ) {
            thrown = ex;
            throw new HttpShellException( ex );

        } finally {

            // invoke post-transaction
            for( HttpShellHandler handler : handlers ) {
                try {
                    String ret2 = handler.afterTransactionEnd( request, variables, theMeshBase, now, thrown );
                    if( ret2 != null ) {
                        if( ret == null || ret.equals( ret2 )) {
                            ret = ret2;
                        } else {
                            log.error( "More than one handler declared redirect URL: ", ret, ret2, handler );
                        }
                    }

                // make sure we pass on the first exception
                } catch( HttpShellException t ) {
                    if( thrown != null ) {
                        log.error( "Two exceptions, passing on first:", thrown, t );
                    } else {
                        throw t;
                    }

                } catch( RuntimeException t ) {
                    if( thrown != null ) {
                        log.error( "Two exceptions, passing on first:", thrown, t );
                    } else {
                        throw t;
                    }
                }
            }
        }

        // and now for redirects, if none has been found so far
        if( ret == null ) {
            String redirectVar   = null;
            String redirectValue = null;
            for( String var1Name : variables.keySet() ) {
                String    key   = PREFIX + var1Name + REDIRECT_TAG;
                String [] value = postArguments.get( key );

                if( value != null && value.length == 1 && value[0] != null && value[0].trim().length() > 0 ) {
                    if( redirectVar != null ) {
                        throw new HttpShellException( new ConflictingArgumentsException( key, redirectVar, request ));
                    }
                    redirectVar   = var1Name;
                    redirectValue = value[0].trim();
                }
            }

            if( redirectVar != null ) {
                MeshObject redirectObj = variables.get( redirectVar );

                String ret2 = theRedirectIdSerializer.toExternalForm( redirectObj.getIdentifier() );

                if( !REDIRECT_TAG_TRUE.equalsIgnoreCase( redirectValue )) {
                    ret2 = UrlUtils.appendArgumentPairToUrl( ret2, redirectValue );
                }

                if( ret2 != null ) {
                    if( ret == null || ret.equals( ret2 )) {
                        ret = ret2;
                    } else {
                        log.error( "Shell declaration of redirect after a handler declared redirect URL: ", ret, ret2, redirectVar );
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Parse a String into a MeshObjectIdentifier.
     *
     * @param raw the String
     * @return the parsed MeshObjectIdentifier
     * @throws ParseException thrown if the MeshObjectIdentifier could not be parsed
     */
    protected MeshObjectIdentifier parseMeshObjectIdentifier(
            String                           raw )
        throws
            ParseException
    {
        MeshObjectIdentifier ret = theFormIdDeserializer.meshObjectIdentifierFromExternalForm( raw );

        return ret;
    }

    /**
     * Determine from the request whether the object with a variable name is supposed to have any attributes
     * set, and if so, perform the attribute setting.
     *
     * @param varName the variable name of the to-be-unblessed object in the request
     * @param obj the accessed object
     * @param request the request
     * @throws TransactionException thrown if a problem occurred with the Transaction
     * @throws NotPermittedException thrown if the caller did not have sufficient permissions to perform this operation
     * @throws ParseException thrown if the Attribute could not be parsed
     */
    protected void potentiallySetAttributes(
            String                        varName,
            MeshObject                    obj,
            HttpRequest                   request )
        throws
            TransactionException,
            NotPermittedException,
            ParseException
    {
        Map<String,String[]> postArguments = request.getPostedArguments();

        StringBuilder buf = new StringBuilder();
        buf.append( PREFIX );
        buf.append( varName );
        buf.append( ATTRIBUTE_NAME_TAG );

        String attPrefix = buf.toString();

        for( String arg : postArguments.keySet() ) {
            if( !arg.startsWith( attPrefix )) {
                continue; // not relevant here
            }
            String attVarName = arg.substring( attPrefix.length() );

            buf = new StringBuilder();
            buf.append( PREFIX );
            buf.append( varName );
            buf.append( ATTRIBUTE_VALUE_TAG );
            buf.append( attVarName );

            String attValueKey    = buf.toString();
            String attValueString = request.getPostedArgument( attValueKey );
            String attName        = request.getPostedArgument( arg );

            buf = new StringBuilder();
            buf.append( PREFIX );
            buf.append( varName );
            buf.append( ATTRIBUTE_VALUE_TAG );
            buf.append( attVarName );
            buf.append( NULL_ATTRIBUTE_VALUE_TAG );

            String nullValueKey    = buf.toString();
            String nullValueString = request.getPostedArgument( nullValueKey );

            Serializable value;

            // null has preference over upload, which has preference over the regular value
            if( NULL_ATTRIBUTE_VALUE_TAG_TRUE.equals( nullValueString )) {
                value = null;

            } else if( attValueString != null ) {
                value = theParsingScheme.parseAttributeValue( attValueString, attVarName );

            } else {
                // nothing given: leave as is
                continue;
            }

            obj.setAttributeValue( attName, value );
        }
    }

    /**
     * Determine from the request whether the object with a variable name is supposed to have any attributes
     * deleted, and if so, perform the attribute deletion.
     *
     * @param varName the variable name of the to-be-unblessed object in the request
     * @param obj the accessed object
     * @param request the request
     * @throws TransactionException thrown if a problem occurred with the Transaction
     * @throws NotPermittedException thrown if the caller did not have sufficient permissions to perform this operation
     */
    protected void potentiallyDeleteAttributes(
            String                        varName,
            MeshObject                    obj,
            HttpRequest                   request )
        throws
            TransactionException,
            NotPermittedException
    {
        Map<String,String[]> postArguments = request.getPostedArguments();

        StringBuilder buf = new StringBuilder();
        buf.append( PREFIX );
        buf.append( varName );
        buf.append( ATTRIBUTE_NAME_TAG );

        String attPrefix = buf.toString();

        for( String arg : postArguments.keySet() ) {
            if( !arg.startsWith( attPrefix )) {
                continue; // not relevant here
            }
            String attVarName = arg.substring( attPrefix.length() );

            buf = new StringBuilder();
            buf.append( PREFIX );
            buf.append( varName );
            buf.append( ATTRIBUTE_DELETE_TAG );
            buf.append( attVarName );

            String attDeleteKey    = buf.toString();
            String attDeleteString = request.getPostedArgument( attDeleteKey );
            String attName         = request.getPostedArgument( arg );

            if( ATTRIBUTE_DELETE_TAG_TRUE.equals( attDeleteString )) {
                obj.deleteAttribute( attName );
            }
        }
    }

    /**
     * Determine from the request whether the object with a variable name is supposed to be blessed and how,
     * and if so, perform the blessing.
     *
     * @param varName the variable name of the to-be-blessed object in the request
     * @param obj the accessed object
     * @param request the request
     * @throws ClassCastException thrown if a MeshType with this identifier could be found, but it was of the wrong type
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a MeshType with this identifier could not be found
     * @throws EntityBlessedAlreadyException thrown if the MeshObject is already blessed with this MeshType
     * @throws IsAbstractException thrown if the MeshType is abstract
     * @throws TransactionException thrown if a problem occurred with the Transaction
     * @throws NotPermittedException thrown if the caller did not have sufficient permissions to perform this operation
     */
    protected void potentiallyBless(
            String                        varName,
            MeshObject                    obj,
            HttpRequest                   request )
        throws
            ParseException,
            ClassCastException,
            MeshTypeWithIdentifierNotFoundException,
            EntityBlessedAlreadyException,
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        StringBuilder buf1 = new StringBuilder();
        buf1.append( PREFIX );
        buf1.append( varName );
        buf1.append( BLESS_TAG );

        String [] values1 = request.getMultivaluedPostedArgument( buf1.toString() );
        if( values1 != null ) {
            for( String v : values1 ) {
                if( v.length() > 0 ) { // support "none" as an option in select fields
                    EntityType toBless = (EntityType) findMeshType( v ); // can thrown ClassCastException
                    obj.bless( toBless );
                }
            }
        }

        StringBuilder buf2 = new StringBuilder();
        buf2.append( PREFIX );
        buf2.append( varName );
        buf2.append( BLESS_IF_NEEDED_TAG );

        String [] values2 = request.getMultivaluedPostedArgument( buf2.toString() );
        if( values2 != null ) {
            for( String v : values2 ) {
                if( v.length() > 0 ) { // support "none" as an option in select fields
                    EntityType toBless = (EntityType) findMeshType( v ); // can thrown ClassCastException
                    if( !obj.isBlessedBy( toBless ) ) {
                        obj.bless( toBless );
                    }
                }
            }
        }
    }

    /**
     * Determine from the request whether the object with a variable name is supposed to be unblessed and how,
     * and if so, perform the unblessing.
     *
     * @param varName the variable name of the to-be-unblessed object in the request
     * @param obj the accessed object
     * @param request the request
     * @throws ClassCastException thrown if a MeshType with this identifier could be found, but it was of the wrong type
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a MeshType with this identifier could not be found
     * @throws EntityNotBlessedException thrown if the MeshObject is not blessed with this MeshType
     * @throws RoleTypeRequiresEntityTypeException thrown if this MeshObject cannot be unblessed as long as one of its role requires this EntityType
     * @throws IsAbstractException thrown if the MeshType is abstract
     * @throws TransactionException thrown if a problem occurred with the Transaction
     * @throws NotPermittedException thrown if the caller did not have sufficient permissions to perform this operation
     */
    protected void potentiallyUnbless(
            String                        varName,
            MeshObject                    obj,
            HttpRequest                   request )
        throws
            ParseException,
            ClassCastException,
            MeshTypeWithIdentifierNotFoundException,
            EntityNotBlessedException,
            RoleTypeRequiresEntityTypeException,
            IsAbstractException,
            TransactionException,
            NotPermittedException
    {
        StringBuilder buf1 = new StringBuilder();
        buf1.append( PREFIX );
        buf1.append( varName );
        buf1.append( UNBLESS_TAG );

        String [] values1 = request.getMultivaluedPostedArgument( buf1.toString() );
        if( values1 != null ) {
            for( String v : values1 ) {
                if( v.length() > 0 ) { // support "none" as an option in select fields
                    EntityType toUnbless = (EntityType) findMeshType( v ); // can thrown ClassCastException
                    obj.unbless( toUnbless );
                }
            }
        }

        StringBuilder buf2 = new StringBuilder();
        buf2.append( PREFIX );
        buf2.append( varName );
        buf2.append( UNBLESS_IF_NEEDED_TAG );

        String [] values2 = request.getMultivaluedPostedArgument( buf2.toString() );
        if( values2 != null ) {
            for( String v : values2 ) {
                if( v.length() > 0 ) { // support "none" as an option in select fields
                    EntityType toUnbless = (EntityType) findMeshType( v ); // can thrown ClassCastException
                    if( obj.isBlessedBy( toUnbless ) ) {
                        obj.unbless( toUnbless );
                    }
                }
            }
        }
    }

    /**
     * Determine from the request whether the object with a variable name is supposed to have any properties
     * set, and if so, perform the property setting.
     *
     * @param varName the variable name of the to-be-unblessed object in the request
     * @param obj the accessed object
     * @param request the request
     * @throws ClassCastException thrown if a MeshType with this identifier could be found, but it was of the wrong type
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a MeshType with this identifier could not be found
     * @throws PropertyValueParsingException thrown if a PropertyValue could not be parsed
     * @throws IllegalPropertyTypeException thrown if a PropertyType was not valid on this MeshObject
     * @throws IllegalPropertyValueException thrown if a PropertyValue was not valid for a PropertyType
     * @throws TransactionException thrown if a problem occurred with the Transaction
     * @throws NotPermittedException thrown if the caller did not have sufficient permissions to perform this operation
     */
    protected void potentiallySetProperties(
            String                        varName,
            MeshObject                    obj,
            HttpRequest                   request )
        throws
            ParseException,
            ClassCastException,
            MeshTypeWithIdentifierNotFoundException,
            PropertyValueParsingException,
            IllegalPropertyTypeException,
            IllegalPropertyValueException,
            TransactionException,
            NotPermittedException
    {
        Map<String,String[]> postArguments = request.getPostedArguments();

        StringBuilder buf = new StringBuilder();
        buf.append( PREFIX );
        buf.append( varName );
        buf.append( PROPERTY_TYPE_TAG );

        String propTypePrefix = buf.toString();

        for( String arg : postArguments.keySet() ) {
            if( !arg.startsWith( propTypePrefix )) {
                continue; // not relevant here
            }
            String propVarName = arg.substring( propTypePrefix.length() );

            buf = new StringBuilder();
            buf.append( PREFIX );
            buf.append( varName );
            buf.append( PROPERTY_VALUE_TAG );
            buf.append( propVarName );

            String propValueKey     = buf.toString();
            String propValueString  = request.getPostedArgument( propValueKey );
            String propMimeString   = request.getPostedArgument( propValueKey + MIME_TAG );
            String propTypeString   = request.getPostedArgument( arg );
            MimePart uploadPart     = request.getMimePart( propValueKey + UPLOAD_PROPERTY_VALUE_TAG );

            PropertyType propertyType = (PropertyType) findMeshType( propTypeString );

            buf = new StringBuilder();
            buf.append( PREFIX );
            buf.append( varName );
            buf.append( PROPERTY_VALUE_TAG );
            buf.append( propVarName );
            buf.append( NULL_PROPERTY_VALUE_TAG );

            String nullValueKey    = buf.toString();
            String nullValueString = request.getPostedArgument( nullValueKey );

            PropertyValue value;

            // null has preference over upload, which has preference over the regular value
            if( NULL_PROPERTY_VALUE_TAG_TRUE.equals( nullValueString )) {
                value = null;

            } else if( uploadPart != null && uploadPart.getContent().length > 0 && propertyType.getDataType() instanceof BlobDataType ) {
                BlobDataType type = (BlobDataType) propertyType.getDataType();

                if( uploadPart.getMimeType().startsWith( "text/" )) {
                    try {
                        value = type.createBlobValue( uploadPart.getContentAsString(), uploadPart.getMimeType() );
                    } catch( UnsupportedEncodingException ex ) {
                        log.warn( ex );
                        value = type.createBlobValue( uploadPart.getContent(), uploadPart.getMimeType() ); // try this instead
                    }
                } else {
                    value = type.createBlobValue( uploadPart.getContent(), uploadPart.getMimeType() );
                }


            } else if( propValueString != null ) {
                value = theParsingScheme.parsePropertyValue( propValueString, propMimeString, propertyType );

            } else {
                // nothing given: leave as is
                continue;
            }

            obj.setPropertyValue( propertyType, value );
        }
    }

    protected void potentiallyBlessRoles(
            String                                           var1Name,
            String                                           var2Name,
            HashMap<String,MeshObject>                       variables,
            String []                                        values )
        throws
            ParseException,
            FactoryException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshObject found2 = variables.get( var2Name );
        MeshObject found1 = variables.get( var1Name );

        if( found1 != null && found2 != null ) {
            // be lenient
            if( values != null ) {
                if( found1.getMeshBase() != null ) {
                    for( String v : values ) {
                        if( v.length() > 0 ) { // support "none" as an option in select fields
                            RoleType toBless = (RoleType) findMeshType( v ); // can thrown ClassCastException
                            found1.blessRole( toBless, found2 );
                        }
                    }
                }
            } else {
                 throw new ReadOnlyMeshBaseViewException( found1.getMeshBaseView() );
            }
        }
    }

    protected void potentiallyBlessRolesIfNeeded(
            String                                           var1Name,
            String                                           var2Name,
            HashMap<String,MeshObject>                       variables,
            String []                                        values )
        throws
            ParseException,
            FactoryException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshObject found2 = variables.get( var2Name );
        MeshObject found1 = variables.get( var1Name );

        if( found1 != null && found2 != null ) {
            // be lenient
            if( values != null ) {
                if( found1.getMeshBase() != null ) {
                    for( String v : values ) {
                        if( v.length() > 0 ) { // support "none" as an option in select fields
                            RoleType toBless = (RoleType) findMeshType( v ); // can thrown ClassCastException
                            if( !found1.isRelated( toBless, found2 )) {
                                found1.blessRole( toBless, found2 );
                            }
                        }
                    }
                } else {
                     throw new ReadOnlyMeshBaseViewException( found1.getMeshBaseView() );
                }
            }
        }
    }

    protected void potentiallyUnblessRoles(
            String                                           var1Name,
            String                                           var2Name,
            HashMap<String,MeshObject>                       variables,
            String []                                        values )
        throws
            ParseException,
            FactoryException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshObject found2 = variables.get( var2Name );
        MeshObject found1 = variables.get( var1Name );

        if( values != null ) {
            if( found1.getMeshBase() != null ) {
                for( String v : values ) {
                    if( v.length() > 0 ) { // support "none" as an option in select fields
                        RoleType toUnbless = (RoleType) findMeshType( v ); // can thrown ClassCastException
                        found1.unblessRole( toUnbless, found2 );
                    }
                }
            } else {
                 throw new ReadOnlyMeshBaseViewException( found1.getMeshBaseView() );
            }
        }
    }

    protected void potentiallyUnblessRolesIfNeeded(
            String                                           var1Name,
            String                                           var2Name,
            HashMap<String,MeshObject>                       variables,
            String []                                        values )
        throws
            ParseException,
            FactoryException,
            MeshTypeWithIdentifierNotFoundException
    {
        MeshObject found2 = variables.get( var2Name );
        MeshObject found1 = variables.get( var1Name );

        if( values != null ) {
            if( found1.getMeshBase() != null ) {
                for( String v : values ) {
                    if( v.length() > 0 ) { // support "none" as an option in select fields
                        RoleType toUnbless = (RoleType) findMeshType( v ); // can thrown ClassCastException
                        if( found1.isRelated( toUnbless, found2 ) ) {
                            found1.unblessRole( toUnbless, found2 );
                        }
                    }
                }
            } else {
                 throw new ReadOnlyMeshBaseViewException( found1.getMeshBaseView() );
            }
        }
    }

    /**
     * Find a MeshType from an identifier given as String.
     *
     * @param s the String
     * @return the MeshType
     * @throws MeshTypeWithIdentifierNotFoundException thrown if a MeshType with this identifier could not be found
     */
    protected MeshType findMeshType(
            String s )
        throws
            ParseException,
            MeshTypeWithIdentifierNotFoundException
    {
        s = s.trim();

        MeshTypeIdentifier identifier = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON.fromExternalForm( s );
        MeshType           ret        = ModelBase.SINGLETON.findMeshType( identifier );
        return ret;
    }

    /**
     * Directory of known HttpShellHandlers.
     */
    protected final NameServer<String,HttpShellHandler> theHandlerDirectory;

    /**
     * The StringRepresentationScheme to use.
     */
    protected final StringRepresentationScheme theParsingScheme;

    /**
     * The set of vars to take from the incoming request, instead of from HTTP POST.
     */
    protected final Set<String> thePreferVarsFromRequest;

    /**
     * Knows how to deserializer MeshobjectIdentifiers that have been posted.
     */
    protected final MeshObjectIdentifierDeserializer theFormIdDeserializer;

    /**
     * Knows how to serialize a MeshObjectIdentifier into a URL that is suitable to be a redirect to that
     * MeshObject.
     */
    protected final MeshObjectIdentifierSerializer theRedirectIdSerializer;

    /**
     * The MeshBase to operate on.
     */
    protected final MeshBase theMeshBase;

    /**
     * Special value that indicates a field should have been set (e.g. by JavaScript) but wasn't.
     */
    static final String UNASSIGNED_VALUE = ResourceHelper.getInstance( HttpShell.class ).getResourceStringOrDefault( "UnassignedValue", "?" );
}
