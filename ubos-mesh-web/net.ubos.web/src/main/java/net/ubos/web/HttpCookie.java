//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

/**
 * <p>Interface to HTTP Cookies. This is an interface so we can implement a delegation
 * model to the Java servlet cookies or whatever server-side technology is used. Subtypes
 * distinguish between incoming and outgoing cookies.
 * 
 * <p>Making this a subtype of CharSequence allows us to also treat it as a String,
 * which is its value. This simplifies the API.
 */
public interface HttpCookie
        extends
            CharSequence
{
    /**
     * Obtain the name of the Cookie.
     *
     * @return the name of the Cookie
     */
    public String getName();

    /**
     * Obtain the value of the Cookie.
     *
     * @return the value of the Cookie
     */
    public String getValue();
}
