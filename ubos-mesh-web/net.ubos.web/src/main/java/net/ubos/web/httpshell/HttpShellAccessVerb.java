//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.NotPermittedException;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshObjectsNotFoundException;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.web.HttpRequest;

/**
 * Captures how a MeshObject should be accessed by the HttpShell.
 */
public enum HttpShellAccessVerb
        implements
            HttpShellKeywords
{
    /**
     * Unconditionally create a MeshObject. This fails if the MeshObject
     * exists already.
     */
    CREATE( "create" ) {
            /**
             * {@inheritDoc}
             */
            @Override
            public MeshObject access(
                    MeshObjectIdentifier identifier,
                    MeshBase             mb,
                    HttpRequest          request )
                throws
                    MeshObjectIdentifierNotUniqueException,
                    TransactionException,
                    NotPermittedException
            {
                MeshObject  ret;

                if( identifier != null && !identifier.equals( mb.getHomeMeshObjectIdentifier())) {
                    ret = mb.createMeshObject( identifier );
                } else {
                    // empty form -- home object cannot have been meant
                    ret = mb.createMeshObject();
                }
                return ret;
            }
    },

    /**
     * Retrieve a MeshObject, given an identifier. This fails if the MeshObject
     * cannot be found.
     */
    FIND( "find" ) {
            /**
             * {@inheritDoc}
             */
            @Override
            public MeshObject access(
                    MeshObjectIdentifier identifier,
                    MeshBase             mb,
                    HttpRequest          request )
                throws
                    MeshObjectIdentifierNotUniqueException,
                    MeshObjectsNotFoundException,
                    TransactionException,
                    NotPermittedException
            {
                MeshObject found = null;
                if( identifier != null ) {
                    found = mb.findMeshObjectByIdentifier( identifier );
                }

                if( found != null ) {
                    return found;
                } else {
                    throw new MeshObjectsNotFoundException( mb, identifier );
                }
            }
    },

    /**
     * Retrieve a MeshObject, given an identifier. If the MeshObject cannot be found,
     * this does nothing.
     */
    FIND_IF_EXISTS( "findifexists" ) {
            /**
             * {@inheritDoc}
             */
            @Override
            public MeshObject access(
                    MeshObjectIdentifier identifier,
                    MeshBase             mb,
                    HttpRequest          request )
                throws
                    MeshObjectIdentifierNotUniqueException,
                    MeshObjectsNotFoundException,
                    TransactionException,
                    NotPermittedException
            {
                MeshObject found = null;
                if( identifier != null ) {
                    found = mb.findMeshObjectByIdentifier( identifier );
                }
                // return null if not found
                return found;
            }
    },

    /**
     * Retrieve a MeshObject by identifier, or, if it cannot be found, create
     * the MeshObject with this identifier.
     */
    CREATEORFIND( "createorfind" ) {
            /**
             * {@inheritDoc}
             */
            @Override
            public MeshObject access(
                    MeshObjectIdentifier identifier,
                    MeshBase             mb,
                    HttpRequest          request )
                throws
                    MeshObjectIdentifierNotUniqueException,
                    MeshObjectsNotFoundException,
                    TransactionException,
                    NotPermittedException
            {
                if( identifier == null ) {
                    throw new MeshObjectsNotFoundException( mb, identifier );
                }
                MeshObject found = mb.findMeshObjectByIdentifier( identifier );
                if( found == null ) {
                    found = mb.createMeshObject( identifier );
                }
                return found;
            }
    },

    /**
     * Unconditionally delete a MeshObject. This fails if the MeshObject cannot
     * be found.
     */
    DELETE( "delete" ) {
            /**
             * {@inheritDoc}
             */
            @Override
            public MeshObject access(
                    MeshObjectIdentifier identifier,
                    MeshBase             mb,
                    HttpRequest          request )
                throws
                    MeshObjectIdentifierNotUniqueException,
                    MeshObjectsNotFoundException,
                    TransactionException,
                    NotPermittedException
            {
                if( identifier == null ) {
                    throw new MeshObjectsNotFoundException( mb, identifier );
                }
                MeshObject found = mb.findMeshObjectByIdentifier( identifier );
                if( found == null ) {
                    throw new MeshObjectsNotFoundException( mb, identifier );
                }

                mb.deleteMeshObject( found );

                return found;
            }
    };

    /**
     * Constructor.
     *
     * @param s the String designating a verb
     */
    HttpShellAccessVerb(
            String s )
    {
        theString = s;
    }

    /**
     * Extract the right verb for this source object from this request.
     *
     * @param varName the variable name of the to-be-accessed object in the request
     * @param request the request
     * @return the found verb
     */
    public static HttpShellAccessVerb findAccessFor(
            String      varName,
            HttpRequest request )
    {
        StringBuilder key = new StringBuilder();
        key.append( PREFIX );
        key.append( varName );
        key.append( ACCESS_TAG );

        String value = request.getPostedArgument( key.toString() );
        if( value == null ) {
            return FIND;
        }
        value = value.toLowerCase();
        for( HttpShellAccessVerb v : values() ) {
            if( value.equals(  v.theString )) {
                return v;
            }
        }
        // if not found, we default to FIND
        return FIND;
    }

    /**
     * Perform this verb.
     *
     * @param identifier the MeshObjectIdentifier of the to-be-accessed object in the request
     * @param mb the MeshBase to use for the access
     * @param request the request
     * @return the accessed MeshObject
     * @throws MeshObjectIdentifierNotUniqueException thrown if a MeshObject with this identifier exists already
     * @throws MeshObjectsNotFoundException thrown if the MeshObject could not be found
     * @throws TransactionException thrown if a problem occurred with the Transaction
     * @throws NotPermittedException thrown if the caller did not have sufficient permissions to perform this operation
     */
    public abstract MeshObject access(
            MeshObjectIdentifier identifier,
            MeshBase             mb,
            HttpRequest          request )
        throws
            MeshObjectIdentifierNotUniqueException,
            MeshObjectsNotFoundException,
            TransactionException,
            NotPermittedException;

    /**
     * The String designating a verb.
     */
    protected String theString;
}
