//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.skin;

import net.ubos.web.HttpRequest;

/**
 * The types of skin we support.
 */
public enum SkinType
{
    /**
     * No skin at all.
     */
    NONE {
        @Override
        public Skin getFallbackSkin() {
            return null;
        }
    },

    /**
     * A skin for a typical web page.
     */
    DEFAULT_2D {
        @Override
        public Skin getFallbackSkin() {
            return Minimal2DSkin.SINGLETON;
        }
    },

    /**
     * A skin for an iframe in a typical web page.
     */
    IFRAME_2D {
        @Override
        public Skin getFallbackSkin() {
            return Minimal2DSkin.SINGLETON;
        }
    },

    /**
     * A skin for a 3D experience.
     */
    DEFAULT_3D {
        @Override
        public Skin getFallbackSkin() {
            return Minimal2DSkin.SINGLETON; // FIXME
        }
    },

    /**
     * A skin for an iframe displaying 3D in a typical web page.
     */
    IFRAME_3D {
        @Override
        public Skin getFallbackSkin() {
            return Minimal3DSkin.SINGLETON;
        }
    };

    /**
     * Obtain the correct member of this enum, given this incoming request.
     *
     * @param request the incoming request
     * @return the SkinType
     */
    public static SkinType fromRequestOrNull(
            HttpRequest request )
    {
        String value = request.getUrl().getUrlArgument( SKIN_TYPE_PAR_NAME );
        if( value != null ) {
            value = value.toUpperCase();
            for( SkinType candidate : values() ) {
                if( candidate.name().equals( value )) {
                    return candidate;
                }
            }
        }
        return null;
    }

    /**
     * Obtain the fallback skin for this SkinType.
     *
     * @return the fallback
     */
    public abstract Skin getFallbackSkin();

    /**
     * Name of the URL parameter that contains the skin type.
     */
    public static final String SKIN_TYPE_PAR_NAME = "skinType";
}
