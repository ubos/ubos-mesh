//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.vl.CannotViewException;
import net.ubos.vl.ViewletFactoryChoice;

/**
 * A ViewletFactoryChoice for WebViewlets.
 */
public interface WebViewletFactoryChoice
        extends
            ViewletFactoryChoice
{
    /**
     * Obtain the MIME type supported by this Viewlet.
     *
     * @return the MIME type
     */
    public abstract String getSupportedMimeType();

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract WebViewlet instantiateViewlet()
        throws
            CannotViewException;
}
