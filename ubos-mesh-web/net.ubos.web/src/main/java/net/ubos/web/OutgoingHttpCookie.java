//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

import java.util.Date;

/**
 * An outgoing Cookie.
 */
public interface OutgoingHttpCookie
        extends
            HttpCookie
{
    /**
     * Obtain the domain of the Cookie, if any.
     *
     * @return the domain of the Cookie
     */
    public String getDomain();

    /**
     * Obtain the path of the Cookie, if any.
     *
     * @return the path of the Cookie
     */
    public String getPath();

    /**
     * Obtain the expiration time of the Cookie, if any.
     *
     * @return the name of the Cookie
     */
    public Date getExpires();

    /**
     * Determine whether this Cookie should be sent only over a secure protocol.
     *
     * @return true if the Cookie should only be sent over a secure protocol
     */
    public boolean getSecure();

    /**
     * Determine whether this cookie is supposed to be removed.
     *
     * @return true if this cookie is removed or expired
     */
    public boolean getIsRemovedOrExpired();

    /**
     * Convert into a String according to the HTTP spec.
     *
     * @return the String
     */
    public String getAsHttpValue();

    /**
     * Convert into a String that is useful for assigning to document.cookie in Javascript.
     *
     * @return the String
     */
    public String getAsJavascriptValue();
}
