//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.util.ResourceHelper;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;

/**
 * Default implementation of ViewletFactoryChoice for WebViewlets.
 */
public abstract class AbstractWebViewletFactoryChoice
        implements
            WebViewletFactoryChoice
{
    /**
     * Private constructor for subclasses only.
     *
     * @param viewletName computable name of the Viewlet
     * @param supportedMimeType the supported MIME type
     * @param viewletType the Viewlet type supported by the Viewlet
     * @param matchQuality the quality of the match
     * @param resourceHelper the ResourceHelper to use
     */
    protected AbstractWebViewletFactoryChoice(
            String                viewletName,
            String                supportedMimeType,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceHelper        resourceHelper )
    {
        theName              = viewletName;
        theSupportedMimeType = supportedMimeType;
        thePlacement         = placement;
        theDimensionality    = dimensionality;
        theDetail            = detail;
        theMatchQuality      = matchQuality;
        theResourceHelper    = resourceHelper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getMatchQuality()
    {
        return theMatchQuality;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return theName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleName()
    {
        return theResourceHelper.getResourceString( "UserVisibleName" );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebViewletPlacement getPlacement()
    {
        return thePlacement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDimensionality getDimensionality()
    {
        return theDimensionality;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDetail getDetail()
    {
        return theDetail;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSupportedMimeType()
    {
        return theSupportedMimeType;
    }

    /**
     * The name of the Viewlet.
     */
    protected final String theName;

    /**
     * The placement
     */
    protected final WebViewletPlacement thePlacement;

    /**
     * The dimensionality.
     */
    protected final ViewletDimensionality theDimensionality;

    /**
     * The detail.
     */
    protected final ViewletDetail theDetail;

    /**
     * The match quality.
     */
    protected final double theMatchQuality;

    /**
     * The supported MIME type.
     */
    protected final String theSupportedMimeType;

    /**
     * ResourceHelper for the user-visible name
     */
    private final ResourceHelper theResourceHelper;
}
