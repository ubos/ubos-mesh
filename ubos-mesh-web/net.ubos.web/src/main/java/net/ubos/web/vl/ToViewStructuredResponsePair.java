//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

/**
 * Combines a parsed incoming request for a MeshObject to view and a StructuredResponse into a pair.
 */
public class ToViewStructuredResponsePair
{
    /**
     * Factory method.
     * 
     * @param toView the interpreted request
     * @param response the structured response
     * @return the request-response pair
     */
    public static ToViewStructuredResponsePair create(
            WebMeshObjectsToView toView,
            StructuredResponse   response )
    {
        return new ToViewStructuredResponsePair( toView, response );
    }

    /**
     * Constructor, for subclasses only.
     * 
     * @param toView the interpreted request
     * @param response the structured response
     */
    protected ToViewStructuredResponsePair(
            WebMeshObjectsToView toView,
            StructuredResponse   response )
    {
        theToView   = toView;
        theResponse = response;
    }
    
    /**
     * Obtain the first part of the pair.
     * 
     * @return the WebMeshObjectsToView
     */
    public WebMeshObjectsToView getToView()
    {
        return theToView;
    }

    /**
     * Obtain the second part of the pair.
     * 
     * @return the StructuredResponse
     */
    public StructuredResponse getStructuredResponse()
    {
        return theResponse;
    }
    
    /**
     * The interpreted request.
     */
    protected final WebMeshObjectsToView theToView;
    
    /**
     * The structured response.
     */
    protected final StructuredResponse theResponse;
}
