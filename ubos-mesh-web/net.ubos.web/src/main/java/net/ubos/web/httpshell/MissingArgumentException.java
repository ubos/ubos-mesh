//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.httpshell;

import net.ubos.web.HttpRequest;

/**
 * Thrown if an operation is invoked that requires a particular argument but it was not given.
 */
public class MissingArgumentException
        extends
            InconsistentArgumentsException
{
    /**
     * Constructor.
     *
     * @param argName name of the missing argument
     * @param request the incoming request
     */
    public MissingArgumentException(
            String      argName,
            HttpRequest request )
    {
        super( request );

        theArgumentName = argName;
    }

    /**
     * Obtain the name of the missing argument.
     *
     * @return name of the argument
     */
    public String getArgumentName()
    {
        return theArgumentName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getLocalizationParameters()
    {
        return new Object[] {
            theArgumentName,
            theRequest,
            theRequest.getUrl().getAbsoluteFullUri()
        };
    }

    /**
     * Name of the argument that was missing.
     */
    protected final String theArgumentName;
}
