//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web;

/**
 * An object with this type may be found in the request context. It allows
 * applications to report problems during the processing of an incoming request,
 * without having to abort processing. They can be processed later and inserted
 * as error messages into a fully-assembled HTML page, for example.
 */
public interface ProblemReporter
{
    /**
     * Report a problem that should be shown to the user.
     *
     * @param t the Throwable indicating the problem
     */
    public void reportProblem(
            Throwable t );

    /**
     * Convenience method to report several problems that should be shown to the user.
     *
     * @param ts [] the Throwables indicating the problems
     */
    public void reportProblems(
            Throwable [] ts );

    /**
     * Determine whether problems have been reported.
     *
     * @return true if at least one problem has been reported
     */
    public boolean haveProblemsBeenReported();

    /**
     * Obtain the problems reported so far.
     *
     * @return problems reported so far, in sequence
     */
    public Iterable<Throwable> reportedProblems();

    /**
     * Name of a Request-level attribute that contains an object of this type, if any.
     */
    public static final String PROBLEM_REPORTER_ATTRIBUTE_NAME
            = AbstractHttpRequest.classToAttributeName( ProblemReporter.class );
}
