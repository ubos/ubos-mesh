//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.assets;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import net.ubos.util.ResourceHelper;
import net.ubos.util.nameserver.AbstractMNameServer;
import net.ubos.util.FileUtils;

/**
 * Reads all files in a directory, and maps the local file name of those with known file types
 * to their respective Paths.
 */
public class DefaultAssetMap
    extends
        AbstractMNameServer<String,Path>
    implements
        AssetMap
{
    /**
     * Factory method.
     *
     * @return the created instance
     */
    public static DefaultAssetMap create()
    {
        DefaultAssetMap ret = new DefaultAssetMap();
        return ret;
    }

    /**
     * Private constructor, use factory method.
     */
    protected DefaultAssetMap()
    {}

    /**
     * Look for assets in this directory, and add them to the AssetMap.
     *
     * @param dir the directory containing the assets
     * @param allowedExtensions contains the allowed file extensions; will ignore everything else
     */
    public void addAssetsInDir(
            File        dir,
            Set<String> allowedExtensions )
    {
        if( !dir.exists() ) {
            return;
        }
        if( !dir.isDirectory() ) {
            throw new IllegalArgumentException( "Not a directory: " + dir.getAbsolutePath() );
        }

        File [] assets = FileUtils.recursivelyFindFiles(
                dir,
                (File file) -> {
                    if( !file.isFile() ) {
                        return false;
                    }
                    if( allowedExtensions == null ) {
                        return true;
                    }
                    String filename = file.getName();
                    int lastDot = filename.lastIndexOf( '.' );
                    if( lastDot < 0 ) {
                        return false;
                    } else {
                        return allowedExtensions.contains( filename.substring( lastDot+1 ));
                    }
                });

        if( assets.length > 0 ) {
            int dirLen = dir.toString().length();
            for( File t : assets ) {
                super.put( t.getPath().substring( dirLen ), t.toPath() );
            }
        }
    }

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance(DefaultAssetMap.class );

    /**
     * Defines known web asset types.
     */
    public static final Set<String> WEB_ASSET_EXTENSIONS = new HashSet<>();
    static {
        String [] extensions = theResourceHelper.getResourceStringArrayOrDefault(
                "KnownAssetExtensions",
                new String [] {
                    "html",
                    "css",
                    "js",
                    "jpg",
                    "gif",
                    "png",
                    "svg",
                    "ttf",
                    "woff2"
                } );

        WEB_ASSET_EXTENSIONS.addAll( Arrays.asList( extensions));
    }
}
