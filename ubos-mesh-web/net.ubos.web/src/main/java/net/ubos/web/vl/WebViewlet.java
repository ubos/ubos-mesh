//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.vl.Viewlet;

import java.io.IOException;
import java.util.Set;
import net.ubos.web.Url;

/**
 * <p>A Viewlet for a web application.
 */
public interface WebViewlet
        extends
            Viewlet
{
    /**
     * {@inheritDoc}
     */
    @Override
    public WebViewedMeshObjects getViewedMeshObjects();

    /**
     * {@inheritDoc}
     */
    @Override
    public WebMeshObjectsToView getFactoryKey();

    /**
     * Obtain all possible states of this Viewlet. This may depend on the current MeshObjectsToView
     * (e.g. whether the user may edit a MeshObject or not).
     *
     * @return the possible ViewletStates
     */
    public Set<WebViewletState> getPossibleStates();

    /**
     * The current WebViewletState.
     *
     * @return the current state
     */
    public WebViewletState getState();

    /**
     * Obtain the Html class name for this Viewlet that will be used for the enclosing <code>div</code> tag.
     *
     * @return the HTML class name
     */
    public String getCssClass();

    /**
     * Obtain the default URL as String to which forms should be HTTP POSTed.
     *
     * @param toView the MeshObjectsToView
     * @return the URL as String
     */
    public String getPostUrl(
            WebMeshObjectsToView toView );

    /**
     * <p>Invoked prior to the execution of the Viewlet if the GET method has been requested.
     *    It is the hook by which the WebViewlet can perform whatever operations needed prior to
     *    the GET execution of the Viewlet.
     *
     * @param rr the request-response pair
     * @return if true, the result of the Viewlet processing has been deposited into the response object
     *         already and regular processing will be skipped. If false, regular processing continues.
     * @see #performBeforePost
     * @see #performAfter
     */
    public boolean performBeforeGet(
            ToViewStructuredResponsePair rr );

    /**
     * <p>Invoked prior to the execution of the Viewlet if the POST method has been requested.
     *    It is the hook by which the WebViewlet can perform whatever operations needed prior to
     *    the POST execution of the Viewlet, e.g. the evaluation of POST commands.
     *
     * @param rr the request-response pair
     * @return if true, the result of the Viewlet processing has been deposited into the response object
     *         already and regular processing will be skipped. If false, regular processing continues.
     * @see #performBeforeGet
     * @see #performAfter
     */
    public boolean performBeforePost(
            ToViewStructuredResponsePair rr );

    /**
     * <p>Invoked after to the execution of the Viewlet. It is the hook by which
     * the WebViewlet can perform whatever operations needed after to the execution
     * of the Viewlet, e.g. logging. Subclasses will often override this.
     *
     * @param rr the request-response pair
     * @param thrown if this is non-null, it is the Viewlet indicating a problem that occurred
     *        either during execution of performBefore or of the Viewlet.
     * @see #performBeforeGet
     * @see #performBeforePost
     */
    public void performAfter(
            ToViewStructuredResponsePair rr,
            Throwable                    thrown );

    /**
     * Process the incoming request.
     *
     * @param rr the request-response pair
     * @throws IOException thrown if writing the output failed
     */
    public void processRequest(
            ToViewStructuredResponsePair rr )
        throws
            IOException;
}
