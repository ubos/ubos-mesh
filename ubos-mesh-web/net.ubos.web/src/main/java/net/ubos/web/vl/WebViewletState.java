//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import java.util.EnumSet;
import net.ubos.util.ResourceHelper;
import net.ubos.web.HttpRequest;


/**
 * Default implementation of WebViewletState.
 */
public enum WebViewletState
{
    VIEW(    "View" ),
    EDIT(    "Edit" ),
    PREVIEW( "Preview" );

    /**
     * Constructor.
     *
     * @param stateName state of the state in which the Viewlet is
     */
    private WebViewletState(
            String stateName )
    {
        theStateName = stateName;
    }

    /**
     * Obtain the name of this state.
     *
     * @return the name of this state
     */
    public String getName()
    {
        return theStateName;
    }

    /**
     * Obtain the user-visible name of this state.
     *
     * @return the user-visible name of this state
     */
    public String getUserVisibleName()
    {
        return theResourceHelper.getResourceString( theStateName + "Name" );
    }

    /**
     * If true, this is the default state of the JeeViewlet.
     *
     * @return true if default
     */
    public boolean isDefaultState()
    {
        return this == VIEW;
    }

    /**
     * Determine the WebViewletStates that are valid when the caller has update
     * rights to the Viewlet's subject.
     *
     * @return the set of WebViewletStates
     */
    public static EnumSet<WebViewletState> getReadWriteStates()
    {
        return theReadWriteStates;
    }

    /**
     * Determine the WebViewletStates that are valid when the caller does not have update
     * rights to the Viewlet's subject.
     *
     * @return the set of WebViewletStates
     */
    public static EnumSet<WebViewletState> getReadOnlyStates()
    {
        return theReadOnlyStates;
    }

    /**
     * Obtain the correct member of this enum, given this incoming request.
     *
     * @param request the incoming request
     * @return the WebViewletState
     */
    public static WebViewletState fromRequest(
            HttpRequest request )
    {
        String value = request.getUrl().getUrlArgument( VIEWLET_STATE_PAR_NAME );
        if( value != null ) {
            for( WebViewletState candidate : values() ) {
                if( candidate.theStateName.equals( value )) {
                    return candidate;
                }
            }
        }
        return VIEW;
    }

    /**
     * Name of the state.
     */
    protected final String theStateName;

    /**
     * The WebViewletStates that are available when the caller does have update rights.
     */
    protected static final EnumSet<WebViewletState> theReadWriteStates = EnumSet.allOf( WebViewletState.class );

    /**
     * The WebViewletStates that are available when the caller does not have update rights.
     */
    protected static final EnumSet<WebViewletState> theReadOnlyStates = EnumSet.of( VIEW );

    /**
     * Default URL parameter name containing the vl state.
     */
    public static final String VIEWLET_STATE_PAR_NAME = "vl-state";

    /**
     * Our ResourceHelper.
     */
    public static final ResourceHelper theResourceHelper = ResourceHelper.getInstance( WebViewletState.class );
}
