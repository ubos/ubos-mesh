//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.vl.ViewletPlacement;

/**
 * Implements ViewletPlacement for the web.
 */
public enum WebViewletPlacement
     implements
        ViewletPlacement
{
    /**
     * Can stand alone in a primary page, such as an image file that's directly contained as the top in a web page.
     */
    ROOT,

    /**
     * Can stand alone in an iframed page.
     */
    IFRAME,

    /**
     * Can be inlined into other Viewlets.
     */
    INLINED;

    @Override
    public String getName()
    {
        return name().toLowerCase();
    }

    /**
     * Obtain the name of a CSS class that indicates this placement.
     */
    public String getCssClass()
    {
        return "vl-" + name().toLowerCase();
    }
}
