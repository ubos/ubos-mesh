//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.web.vl;

import net.ubos.web.HttpRequest;

/**
 * A transition between WebViewletStates
 */
public enum WebViewletStateTransition
{
    DO_EDIT( "do-edit" ) {
        /**
         * {@inheritDoc}
         */
        @Override
        public WebViewletState getNextState()
        {
            return WebViewletState.EDIT;
        }
    },
    DO_PREVIEW( "do-preview" ) {
        /**
         * {@inheritDoc}
         */
        @Override
        public WebViewletState getNextState()
        {
            return WebViewletState.PREVIEW;
        }
    },
    DO_COMMIT( "do-commit" ) {
        /**
         * {@inheritDoc}
         */
        @Override
        public WebViewletState getNextState()
        {
            return WebViewletState.VIEW;
        }
    },
    DO_CANCEL( "do-cancel" ) {
        /**
         * {@inheritDoc}
         */
        @Override
        public WebViewletState getNextState()
        {
            return WebViewletState.VIEW;
        }
    };
    
    /**
     * Constructor.
     *
     * @param transitionName name of the transition which the Viewlet is about to make
     */
    private WebViewletStateTransition(
            String transitionName )
    {
        theTransitionName = transitionName;
    }

    /**
     * Obtain the name of this transition.
     *
     * @return the name of this transition
     */
    public String getName()
    {
        return theTransitionName;
    }

    /**
     * Obtain the desired state after this transition has been taken.
     *
     * @return the desired state after this transition has been taken
     */
    public abstract WebViewletState getNextState();

    /**
     * Obtain the correct member of this enum, given this incoming request.
     *
     * @param request the incoming request
     * @return the DefaultJeeViewletStateEnum
     */
    public static WebViewletStateTransition fromRequest(
            HttpRequest request )
    {
        String value = request.getPostedArgument( VIEWLET_STATE_TRANSITION_PAR_NAME );
        // this must be a post argument, while the state is determined from a regular argument
        if( value != null ) {
            for( WebViewletStateTransition candidate : values() ) {
                if( candidate.theTransitionName.equals( value )) {
                    return candidate;
                }
            }
        }

        return null;
    }

    /**
     * Name of the transition.
     */
    protected String theTransitionName;

    /**
     * Default HTTP POST parameter name containing the vl state transition.
     */
    public static final String VIEWLET_STATE_TRANSITION_PAR_NAME = "ViewletStateTransition";
}
