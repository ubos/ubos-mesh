#
# Creates the HistoryMeshObjects table
#

CREATE TABLE MeshObjectHistory (
    id          VARCHAR(511) COLLATE utf8mb4_nopad_bin NOT NULL,
    encodingId  VARCHAR(128),
    timeUpdated BIGINT NOT NULL,
    content     LONGBLOB,
    PRIMARY KEY(id,timeUpdated)
);
