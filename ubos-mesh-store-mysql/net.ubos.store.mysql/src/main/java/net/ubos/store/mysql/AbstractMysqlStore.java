//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.mysql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.store.sql.SqlStoreIOException;
import net.ubos.util.logging.Log;
import net.ubos.util.sql.SqlDatabase;
import net.ubos.util.sql.SqlExecutionAction;
import net.ubos.util.sql.SqlPreparedStatement;

/**
 * Collects functionality common to subclasses.
 *
 * @param <T> the value class for rows
 */
public abstract class AbstractMysqlStore<T extends StoreValue>
    extends
        AbstractSqlStore<T>
{
    private static final Log log = Log.getLogInstance( AbstractMysqlStore.class ); // our own, private logger

    /**
     * Constructor for subclasses only.
     *
     * @param db the SQL Database
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     */
    protected AbstractMysqlStore(
            SqlDatabase db,
            String      tableName )
    {
        super( db, tableName );

        theDropTablesStm             = new SqlPreparedStatement( theDatabase, DROP_TABLES_SQL,               tableName );
        theHasTablesStm              = new SqlPreparedStatement( theDatabase, HAS_TABLES_SQL,                tableName );
        theDeleteStm                 = new SqlPreparedStatement( theDatabase, DELETE_SQL,                    tableName );
        theDeleteAllStm              = new SqlPreparedStatement( theDatabase, DELETE_ALL_SQL,                tableName );
        theContainsKeyStm            = new SqlPreparedStatement( theDatabase, CONTAINS_KEY_SQL,              tableName );
        theSizeStm                   = new SqlPreparedStatement( theDatabase, SIZE_SQL,                      tableName );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean hasTables()
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "hasTables" );
        }

        try {
            new SqlExecutionAction<Object>( theHasTablesStm ) {
                @Override
                protected Object perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.execute();
                    return null;
                }
            }.execute();

            return true;

        } catch( Throwable ex ) {
            log.warn( ex );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void dropTables()
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "dropTables" );
        }

        try {
            new SqlExecutionAction<Object>( theDropTablesStm ) {
                @Override
                protected Object perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.execute();
                    return null;
                }
            }.execute();

        } catch( Throwable ex ) {
            if( ex.getMessage().contains( "Unknown table" )) {
                log.info( ex ); // Don't need warnings in this case
            } else {
                log.warn( ex );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(
            final String key )
        throws
            StoreKeyDoesNotExistException,
            SqlStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "delete", key );
        }

        checkKey( key );

        boolean success = false;
        try {
            success = new SqlExecutionAction<Boolean>( theDeleteStm ) {
                @Override
                protected Boolean perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    int updates = stm.executeUpdate();

                    boolean success = updates > 0;
                    return success;
                }
            }.execute();

            if( !success ) {
                throw new StoreKeyDoesNotExistException( this, key );
            }

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "delete", key, ex );

        } finally {
            fireDeletePerformed( key, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll(
            final String startsWith )
        throws
            SqlStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "deleteAll", startsWith );
        }

        checkKey( startsWith );

        int ret = -1;
        try {
            ret = new SqlExecutionAction<Integer>( theDeleteAllStm ) {
                @Override
                protected Integer perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, startsWith + '%' ); // trailing wild card
                    int ret = stm.executeUpdate();
                    return ret;
                }
            }.execute();

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "deleteAll", startsWith, ex );

        } finally {
            fireDeleteAllPerformed( startsWith, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(
            final String key )
        throws
            SqlStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "containsKey", key );
        }

        checkKey( key );

        boolean ret = false;
        try {
            ret = new SqlExecutionAction<Boolean>( theContainsKeyStm ) {
                @Override
                protected Boolean perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.execute();

                    boolean ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = Boolean.TRUE;
                        } else {
                            ret = Boolean.FALSE;
                        }
                    }
                    return ret;
                }
            }.execute();

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "containsKey", key, ex );

        } finally {
            fireContainsKey( key, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size(
            String startsWith )
        throws
            IOException
    {
        checkKey( startsWith );

        return sizeWithPattern( startsWith + '%' ); // trailing wild card
    }

    /**
     * Helper method to determine the number of StoreValues in this Store whose key
     * matches the provided pattern.
     *
     * @param pattern the pattern
     * @return the number of StoreValues in this Store whose key matches this pattern
     * @throws IOException thrown if an I/O error occurred
     */
    protected int sizeWithPattern(
            final String pattern )
        throws
            IOException
    {
        try {
            int ret = new SqlExecutionAction<Integer>( theSizeStm ) {
                @Override
                protected Integer perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, pattern );
                    stm.execute();

                    int ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = set.getInt( 1 );
                        } else {
                            ret = 0;
                        }
                    }
                    return ret;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return Integer.MAX_VALUE; // Is this a reasonable default?
    }

    /**
     * Drop tables.
     */
    protected final SqlPreparedStatement theDropTablesStm;
    protected static final String DROP_TABLES_SQL
            = "DROP TABLE {0};";

    /**
     * Do we have any tables?
     */
    protected final SqlPreparedStatement theHasTablesStm;
    protected static final String HAS_TABLES_SQL
            = "SELECT COUNT(*) FROM {0} WHERE id = '''';"; // need double-double quotes for MessageFormat

    /**
     * Delete.
     */
    protected final SqlPreparedStatement theDeleteStm;
    protected static final String DELETE_SQL
            = "DELETE FROM {0} WHERE id = ?;";

    /**
     * Delete all.
     */
    protected final SqlPreparedStatement theDeleteAllStm;
    protected static final String DELETE_ALL_SQL
            = "DELETE FROM {0} WHERE id LIKE ?;";

    /**
     * Does row with key exist?
     */
    protected final SqlPreparedStatement theContainsKeyStm;
    protected static final String CONTAINS_KEY_SQL
            = "SELECT id from {0} WHERE id = ?";

    /**
     * Number of rows in table.
     */
    protected final SqlPreparedStatement theSizeStm;
    protected static final String SIZE_SQL
            = "SELECT COUNT(*) FROM {0} WHERE id LIKE ?;";


}
