//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.mysql.history;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import javax.sql.DataSource;
import net.ubos.store.history.StoreWithHistoryEntryDoesNotExistException;
import net.ubos.store.history.StoreWithHistoryKeyDoesNotExistException;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.sql.AbstractSqlStoreWithHistory;
import net.ubos.store.history.sql.SqlHistoryStoreIOException;
import net.ubos.store.history.sql.SqlStoreValueHistory;
import net.ubos.util.Pair;
import net.ubos.util.logging.Log;
import net.ubos.util.sql.SqlDatabase;
import net.ubos.util.sql.SqlExecutionAction;
import net.ubos.util.sql.SqlPreparedStatement;
import net.ubos.store.history.StoreValueWithTimeUpdatedHistory;

/**
 * An implementation of HistoryStore in MySQL.
 */
public class MysqlStoreWithHistory
    extends
        AbstractSqlStoreWithHistory<StoreValueWithTimeUpdated>
{
    private static final Log log = Log.getLogInstance( MysqlStoreWithHistory.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param db the SqlDatabase
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     * @return the created AbstractSqlStore
     */
    public static MysqlStoreWithHistory create(
            SqlDatabase db,
            String      tableName )
    {
        return new MysqlStoreWithHistory( db, tableName );
    }

    /**
     * Factory method.
     *
     * @param ds the SQL DataSource
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     * @return the created AbstractSqlStore
     */
    public static MysqlStoreWithHistory create(
            DataSource ds,
            String     tableName )
    {
        SqlDatabase db = SqlDatabase.create( "MysqlHistoryStore of " + ds.toString() + ", table " + tableName, ds );

        return new MysqlStoreWithHistory( db, tableName );
    }

    /**
     * Constructor for subclasses only, use factory method.
     *
     * @param db the SQL Database
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     */
    protected MysqlStoreWithHistory(
            SqlDatabase db,
            String      tableName )
    {
        super( db, tableName );

        theCreateTablesPreparedStatement                    = new SqlPreparedStatement( theDatabase, CREATE_TABLES_SQL,                       tableName );
        theDropTablesPreparedStatement                      = new SqlPreparedStatement( theDatabase, DROP_TABLES_SQL,                         tableName );
        theHasTablesPreparedStatement                       = new SqlPreparedStatement( theDatabase, HAS_TABLES_SQL,                          tableName );
        thePutOrUpdatePreparedStatement                     = new SqlPreparedStatement( theDatabase, PUT_OR_UPDATE_SQL,                       tableName );
        theGetPreparedStatement                             = new SqlPreparedStatement( theDatabase, GET_SQL,                                 tableName );
        theGetMostRecentPreparedStatement                   = new SqlPreparedStatement( theDatabase, GET_MOST_RECENT_SQL,                     tableName );
        theFindFirstKeyPreparedStatement                    = new SqlPreparedStatement( theDatabase, FIND_FIRST_KEY_SQL,                      tableName );
        theFindKeyAtPositivePreparedStatement               = new SqlPreparedStatement( theDatabase, FIND_KEY_AT_POSITIVE_SQL,                tableName );
        theFindKeyAtNegativePreparedStatement               = new SqlPreparedStatement( theDatabase, FIND_KEY_AT_NEGATIVE_SQL,                tableName );
        theFindKeyAtEndPreparedStatement                    = new SqlPreparedStatement( theDatabase, FIND_KEY_AT_END_SQL,                     tableName );
        theFindNextKeysIncludingPreparedStatement           = new SqlPreparedStatement( theDatabase, FIND_NEXT_KEYS_INCLUDING_SQL,            tableName );
        theFindPreviousKeysExcludingPreparedStatement       = new SqlPreparedStatement( theDatabase, FIND_PREVIOUS_KEYS_EXCLUDING_SQL,        tableName );
        theFindLastKeysPreparedStatement                    = new SqlPreparedStatement( theDatabase, FIND_LAST_KEYS_SQL,                      tableName );
        theHasNextKeysIncludingPreparedStatement            = new SqlPreparedStatement( theDatabase, HAS_NEXT_KEYS_INCLUDING_SQL,             tableName );
        theHasPreviousKeysExcludingPreparedStatement        = new SqlPreparedStatement( theDatabase, HAS_PREVIOUS_KEYS_EXCLUDING_SQL,         tableName );
        theDetermineKeysDistancePreparedStatement           = new SqlPreparedStatement( theDatabase, DETERMINE_KEYS_DISTANCE_SQL,             tableName );
        theDetermineKeysDistanceToEndPreparedStatement      = new SqlPreparedStatement( theDatabase, DETERMINE_KEYS_DISTANCE_TO_END_SQL,      tableName );
        theSizePreparedStatement                            = new SqlPreparedStatement( theDatabase, SIZE_SQL,                                tableName );

        theDetermineGreaterOrEqualTimeUpdatedPreparedStatement = new SqlPreparedStatement( theDatabase, DETERMINE_GREATER_OR_EQUAL_TIME_UPDATED_SQL, tableName );
        theDetermineGreaterTimeUpdatedPreparedStatement        = new SqlPreparedStatement( theDatabase, DETERMINE_GREATER_TIME_UPDATED_SQL,          tableName );
        theFindNextValuesIncludingPreparedStatement            = new SqlPreparedStatement( theDatabase, FIND_NEXT_VALUES_INCLUDING_SQL,              tableName );
        theFindPreviousValuesExcludingPreparedStatement        = new SqlPreparedStatement( theDatabase, FIND_PREVIOUS_VALUES_EXCLUDING_SQL,          tableName );
        theFindLastValuesPreparedStatement                     = new SqlPreparedStatement( theDatabase, FIND_LAST_VALUES_SQL,                        tableName );
        theFindFirstValuePreparedStatement                     = new SqlPreparedStatement( theDatabase, FIND_FIRST_VALUE_SQL,                        tableName );
        theDetermineStepsToJustBeforeTimePreparedStatement     = new SqlPreparedStatement( theDatabase, DETERMINE_STEPS_TO_JUST_BEFORE_TIME_SQL,     tableName );
        theDetermineStepsToJustAfterTimePreparedStatement      = new SqlPreparedStatement( theDatabase, DETERMINE_STEPS_TO_JUST_AFTER_TIME_SQL,      tableName );
        theDeleteAllStm                                        = new SqlPreparedStatement( theDatabase, DELETE_ALL_SQL,                              tableName );

        if( log.isTraceEnabled() ) {
            log.traceConstructor( this );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean hasTables()
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "hasTables" );
        }

        try {
            new SqlExecutionAction<Object>( theHasTablesPreparedStatement ) {
                @Override
                protected Object perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.execute();
                    return null;
                }
            }.execute();

            return true;

        } catch( Throwable ex ) {
            log.warn( ex );
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void dropTables()
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "dropTables" );
        }

        try {
            new SqlExecutionAction<Object>( theDropTablesPreparedStatement ) {
                @Override
                protected Object perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.execute();
                    return null;
                }
            }.execute();

        } catch( Throwable ex ) {
            log.warn( ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createTables()
            throws
                IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "createTables" );
        }

        try {
            new SqlExecutionAction<Object>( theCreateTablesPreparedStatement ) {
                @Override
                protected Object perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.execute();
                    return null;
                }
            }.execute();

        } catch( SQLException ex ) {
            throw new SqlHistoryStoreIOException( this, "createTables", ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putOrUpdate(
            final StoreValueWithTimeUpdated toPutOrUpdate )
        throws
            SqlHistoryStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "put", toPutOrUpdate );
        }

        final String  key         = toPutOrUpdate.getKey();
        final String  encodingId  = toPutOrUpdate.getEncodingId();
        final long    timeUpdated = toPutOrUpdate.getTimeUpdated();
        final byte [] data        = toPutOrUpdate.getData();

        checkKey(      key );
        checkEncoding( encodingId );
        checkData(     data );

        boolean ret     = false;
        boolean success = false;
        try {
            ret = new SqlExecutionAction<Boolean>( thePutOrUpdatePreparedStatement ) {
                @Override
                protected Boolean perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setString( 2, encodingId );
                    stm.setLong(   3, timeUpdated );
                    stm.setBytes(  4, data );
                    stm.setString( 5, encodingId );
                    stm.setBytes(  6, data );

                    stm.execute();

                    boolean ret = stm.getUpdateCount() > 1; // the "duplicate key" seems to trigger two, instead of one

                    return ret;
                }
            }.execute();

            success = true;

        } catch( SQLException ex ) {
            throw new SqlHistoryStoreIOException( this, "putOrUpdate", key, timeUpdated, encodingId, data, ex );

        } finally {
            if( ret ) {
                fireUpdatePerformed( toPutOrUpdate, success );
            } else {
                firePutPerformed( toPutOrUpdate, success );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdated get(
            String key,
            long   timeUpdated )
        throws
            StoreWithHistoryEntryDoesNotExistException,
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "get", key, timeUpdated );
        }

        checkKey(  key );
        checkTime( timeUpdated );

        StoreValueWithTimeUpdated ret = null;
        try {
            ret = new SqlExecutionAction<StoreValueWithTimeUpdated>( theGetPreparedStatement ) {
                @Override
                protected StoreValueWithTimeUpdated perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    StoreValueWithTimeUpdated ret = null;

                    stm.setString( 1, key );
                    stm.setLong(   2, timeUpdated );

                    stm.execute();

                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            String  foundKey    = set.getString( "id" );
                            String  encodingId  = set.getString( "encodingId" );
                            long    timeUpdated = set.getLong(   "timeUpdated" );
                            byte [] data        = set.getBytes(  "content" );

                            ret = new StoreValueWithTimeUpdated(
                                    foundKey,
                                    encodingId,
                                    timeUpdated,
                                    data );
                        }
                    }
                    return ret;
                }
            }.execute();

            if( ret == null ) {
                throw new StoreWithHistoryEntryDoesNotExistException( MysqlStoreWithHistory.this, key, timeUpdated );
            }

        } catch( SQLException ex ) {
            throw new SqlHistoryStoreIOException( this, "get", key, timeUpdated, ex );

        } finally {
            fireGetPerformed( key, timeUpdated, ret );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(
            String key,
            long   t )
        throws
            StoreWithHistoryEntryDoesNotExistException,
            IOException
    {
        throw new UnsupportedOperationException( "Not supported yet." ); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAllKey(
            String key )
        throws
            IOException
    {
        throw new UnsupportedOperationException( "Not supported yet." ); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll(
            final String startsWith )
        throws
            IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "deleteAll", startsWith );
        }

        checkKey( startsWith );

        int ret = -1;
        try {
            ret = new SqlExecutionAction<Integer>( theDeleteAllStm ) {
                @Override
                protected Integer perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, startsWith + '%' ); // trailing wild card
                    int ret = stm.executeUpdate();
                    return ret;
                }
            }.execute();

        } catch( SQLException ex ) {
            throw new SqlHistoryStoreIOException( this, "deleteAll", startsWith, -1, ex );

        } finally {
            fireDeleteAllPerformed( startsWith );
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> findNextHistoriesIncluding(
            String key,
            int    n,
            String pattern )
    {
        if( key == null ) {
            return new ArrayList<>();
        }
        return findHistories( theFindNextKeysIncludingPreparedStatement, key, n, pattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> findPreviousHistoriesExcluding(
            String key,
            int    n,
            String pattern )
    {
        if( key != null ) {
            return findHistories( theFindPreviousKeysExcludingPreparedStatement, key, n, pattern );
        }

        List<String> tmp = new ArrayList<>( n );
        ArrayList<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> ret;

        try {
            try (ResultSet set = new SqlExecutionAction<ResultSet>( theFindLastKeysPreparedStatement ) {
                @Override
                protected ResultSet perform(
                        PreparedStatement stm,
                        Connection        conn )
                        throws
                        SQLException
                {
                    stm.setString( 1, pattern );
                    stm.setInt(    2, n );

                    stm.execute();
                    return stm.getResultSet();
                }
            }.execute()) {

                while( set.next() ) {
                    String foundKey = set.getString( "id" );

                    tmp.add( foundKey );
                }
            }

            ret = new ArrayList<>( n );
            for( String current : tmp ) {
                ret.add( new SqlStoreValueHistory<>( current, this ));
            }

        } catch( SQLException ex ) {
            log.error( ex );
            ret = new ArrayList<>();
        }
        return ret;
    }

    /**
     * Find n StoreValueHistories, using the provided PreparedStatement.
     *
     * @param stm the SQL to use
     * @param key key for the where clause
     * @param n the number of StoreValues to find
     * @param pattern the pattern to filter by
     * @return the found StoreValues
     */
    protected List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> findHistories(
            SqlPreparedStatement stm,
            final String         key,
            final int            n,
            final String         pattern )
    {
        try {
            List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> ret = new SqlExecutionAction<List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>>>( stm ) {
                @Override
                protected List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setString( 2, pattern );
                    stm.setInt(    3, n );

                    stm.execute();

                    List<String> temp;
                    try (ResultSet set = stm.getResultSet()) {
                        temp = new ArrayList<>( n );

                        while( set.next() ) {
                            String foundKey = set.getString( "id" );
                            temp.add( foundKey );
                       }
                    }

                    List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> ret = new ArrayList<>();
                    for( String current : temp ) {
                        ret.add(new SqlStoreValueHistory<>( current, MysqlStoreWithHistory.this ));
                    }

                    return ret;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );

            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasNextKeyIncluding(
            String key,
            String pattern )
    {
        if( key == null ) {
            return 0;
        }
        return countRows( theHasNextKeysIncludingPreparedStatement, key, pattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasPreviousKeyExcluding(
            String key,
            String pattern )
    {
        if( key == null ) {
            try {
                int ret = sizeWithPattern( pattern );
                return ret;
            } catch( IOException ex ) {
                log.error( ex );
                return 0;
            }
        }
        return countRows( theHasPreviousKeysExcludingPreparedStatement, key, pattern );
    }

    /**
     * Count the number of rows that meet the condition in the SQL statement.
     *
     * @param stm the SQL statement
     * @param key key parameter
     * @param pattern the pattern to filter by
     * @return the number of rows
     */
    protected int countRows(
            SqlPreparedStatement stm,
            final String         key,
            final String         pattern )
    {
        try {
            int ret = new SqlExecutionAction<Integer>( stm ) {
                @Override
                protected Integer perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setString( 2, pattern );
                    stm.execute();

                    int ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = set.getInt( 1 );
                        } else {
                            ret = 0;
                        }
                    }
                    return ret;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return Integer.MAX_VALUE; // Is this a reasonable default?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String findFirstKey(
            String pattern )
        throws
            NoSuchElementException
    {
        try {
            String ret = new SqlExecutionAction<String>( theFindFirstKeyPreparedStatement ) {
                @Override
                protected String perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, pattern );
                    stm.execute();

                    String ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = set.getString( 1 );
                        } else {
                            ret = null;
                        }
                    }
                    return ret;
                }
            }.execute();

            if( ret != null ) {
                return ret;
            } else {
                throw new NoSuchElementException();
            }

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String findKeyAt(
            String key,
            int    delta,
            String pattern )
        throws
            NoSuchElementException
    {
        if( key != null ) {
            SqlPreparedStatement stm;
            final int distance;
            if( delta >= 0 ) {
                stm = theFindKeyAtPositivePreparedStatement;
                distance = delta;
            } else {
                stm = theFindKeyAtNegativePreparedStatement;
                distance = -delta;
            }
            try {
                final String TOO_FAR_MARKER = "";

                String ret = new SqlExecutionAction<String>( stm ) {
                    @Override
                    protected String perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setString( 1, key );
                        stm.setString( 2, pattern );
                        stm.setInt(    3, distance );
                        stm.execute();

                        String ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.last() ) {
                                if( set.getRow() == distance ) {
                                    ret = set.getString( 1 );
                                } else if( set.getRow() == distance-1 ) {
                                    ret = null;
                                } else {
                                    ret = TOO_FAR_MARKER;
                                }
                            } else {
                                ret = null;
                            }
                        }
                        return ret;
                    }
                }.execute();

                if( (Object) ret != TOO_FAR_MARKER ) {
                    return ret;
                } else {
                    throw new NoSuchElementException();
                }

            } catch( SQLException ex ) {
                log.error( ex );
            }

        } else {
            if( delta >= 0 ) {
                return null;
            }
            final int distance = -delta;
            try {
                String ret = new SqlExecutionAction<String>( theFindKeyAtEndPreparedStatement ) {
                    @Override
                    protected String perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setInt(    1, distance );
                        stm.setString( 2, pattern );
                        stm.execute();

                        String ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.last() ) {
                                ret = set.getString( 1 );
                            } else {
                                ret = null;
                            }
                        }
                        return ret;
                    }
                }.execute();

                return ret;

            } catch( SQLException ex ) {
                log.error( ex );
            }
        }
        return null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int determineKeyDistance(
            String from,
            String to,
            String pattern )
    {
        try {
            int ret;

            if( to != null ) {
                ret = new SqlExecutionAction<Integer>( theDetermineKeysDistancePreparedStatement ) {
                    @Override
                    protected Integer perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setString( 1, from );
                        stm.setString( 2, pattern );
                        stm.setString( 3, to );
                        stm.execute();

                        int ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.next() ) {
                                ret = set.getInt( 1 );
                            } else {
                                ret = 0;
                            }
                        }
                        return ret;
                    }
                }.execute();
            } else {
                ret = new SqlExecutionAction<Integer>( theDetermineKeysDistanceToEndPreparedStatement ) {
                    @Override
                    protected Integer perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setString( 1, from );
                        stm.setString( 2, pattern );
                        stm.execute();

                        int ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.next() ) {
                                ret = set.getInt( 1 );
                            } else {
                                ret = 0;
                            }
                        }
                        return ret;
                    }
                }.execute();
            }

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return Integer.MAX_VALUE; // Is this a reasonable default?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValueWithTimeUpdated> findNextValuesIncluding(
            String key,
            long   timeUpdated,
            int    n )
    {
        if( timeUpdated == Long.MAX_VALUE ) {
            return new ArrayList<>();
        }
        return findValues( theFindNextValuesIncludingPreparedStatement, key, timeUpdated, n );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValueWithTimeUpdated> findPreviousValuesExcluding(
            String key,
            long   timeUpdated,
            int    n )
    {
        if( timeUpdated != Long.MAX_VALUE ) {
            return findValues( theFindPreviousValuesExcludingPreparedStatement, key, timeUpdated, n );
        }

        List<StoreValueWithTimeUpdated> ret = new ArrayList<>( n );

        try {
            try (ResultSet set = new SqlExecutionAction<ResultSet>( theFindLastValuesPreparedStatement ) {
                @Override
                protected ResultSet perform(
                        PreparedStatement stm,
                        Connection        conn )
                        throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setInt(    2, n );

                    stm.execute();
                    return stm.getResultSet();
                }
            }.execute()) {

                while( set.next() ) {
                    String  foundKey         = set.getString( "id" );
                    String  foundEncodingId  = set.getString( "encodingId" );
                    long    foundTimeUpdated = set.getLong(   "timeUpdated" );
                    byte [] foundData        = set.getBytes(  "content" );

                    // this is different from the get() method -- we find the one after, not the same one
                    ret.add( new StoreValueWithTimeUpdated( foundKey, foundEncodingId, foundTimeUpdated, foundData ));
                }
            }

        } catch( SQLException ex ) {
            log.error( ex );
            ret = new ArrayList<>();
        }
        return ret;
    }

    /**
     * Find n StoreValues, using the provided PreparedStatement.
     *
     * @param stm the SQL to use
     * @param key key for the where clause
     * @param timeUpdated key for the where clause
     * @param n the number of StoreValues to find
     * @return the found StoreValueWithTimeUpdated
     */
    protected List<StoreValueWithTimeUpdated> findValues(
            final SqlPreparedStatement stm,
            final String               key,
            final long                 timeUpdated,
            final int                  n )
    {
        try {
            List<StoreValueWithTimeUpdated> ret = new SqlExecutionAction<List<StoreValueWithTimeUpdated>>( stm ) {
                @Override
                protected List<StoreValueWithTimeUpdated> perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setLong(   2, timeUpdated );
                    stm.setInt(    3, n );

                    stm.execute();

                    List<StoreValueWithTimeUpdated> temp;

                    try (ResultSet set = stm.getResultSet()) {
                        temp = new ArrayList<>( n );

                        while( set.next() ) {
                            String  foundKey    = set.getString( "id" );
                            String  encodingId  = set.getString( "encodingId" );
                            long    timeUpdated = set.getLong(   "timeUpdated" );
                            byte [] data        = set.getBytes(  "content" );

                            // this is different from the get() method -- we find the one after, not the same one
                            temp.add( new StoreValueWithTimeUpdated( foundKey, encodingId, timeUpdated, data ));
                        }
                    }

                    return temp;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );

            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Pair<Integer,Long> determineMoveToJustBeforeTime(
            String key,
            long   from,
            long   to )
    {
        int  count;
        long adjustedTo = adjustTimeUpdated( key, to, theDetermineGreaterOrEqualTimeUpdatedPreparedStatement );

        if( from < adjustedTo ) {
            count = determineSteps( key, from, adjustedTo, theDetermineStepsToJustBeforeTimePreparedStatement );
        } else {
            count = - determineSteps( key, adjustedTo, from, theDetermineStepsToJustBeforeTimePreparedStatement );
        }
        return new Pair<>( count, adjustedTo );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Pair<Integer,Long> determineMoveToJustAfterTime(
            String key,
            long   from,
            long   to )
    {
        int  count;
        long adjustedTo = adjustTimeUpdated( key, to, theDetermineGreaterTimeUpdatedPreparedStatement );

        if( from <= adjustedTo ) {
            count = determineSteps( key, from, adjustedTo, theDetermineStepsToJustAfterTimePreparedStatement );
        } else {
            count = - determineSteps( key, adjustedTo, from, theDetermineStepsToJustAfterTimePreparedStatement );
        }
        return new Pair<>( count, adjustedTo );
    }

    /**
     * Helper to the various determineMoveXXX methods.
     *
     * @param key the key identifying the history
     * @param timeUpdated the timeUpdated
     * @param stm the SqlPreparedStatement to use
     * @return the adjusted timeUpdated
     */
    protected long adjustTimeUpdated(
            String               key,
            long                 timeUpdated,
            SqlPreparedStatement stm )
    {
        try {
            long ret = new SqlExecutionAction<Long>( stm ) {
                @Override
                protected Long perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setLong(   2, timeUpdated);
                    stm.execute();

                    long adjusted;
                    try (ResultSet set = stm.getResultSet()) {
                        set.next();
                        adjusted = set.getLong( 1 );
                        if( set.wasNull() ) { // who designed this API?
                            adjusted = Long.MAX_VALUE;
                        }
                    }
                    return adjusted;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return timeUpdated; // Is this a reasonable default?
    }

    /**
     * Helper to the various determineMoveXXX methods.
     *
     * @param key the key identifying the history
     * @param from the start timeUpdated
     * @param to the end timeUpdated
     * @param stm the SqlPreparedStatement to use
     * @return name is the distance, as a positive integer
     */
    protected int determineSteps(
            String               key,
            long                 from,
            long                 to,
            SqlPreparedStatement stm )
    {
        try {
            int ret = new SqlExecutionAction<Integer>( stm ) {
                @Override
                protected Integer perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setLong(   2, from );
                    stm.setLong(   3, to );
                    stm.execute();

                    int  count;
                    try (ResultSet set = stm.getResultSet()) {
                        set.next();
                        count = set.getInt( 1 );
                    }
                    return count;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return 0; // Is this a reasonable default?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected long getTimeUpdatedBeforeFirstPositionInHistory(
            String key )
        throws
            StoreWithHistoryKeyDoesNotExistException
    {
        try {
            Long ret = new SqlExecutionAction<Long>( theFindFirstValuePreparedStatement ) {
                @Override
                protected Long perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.execute();

                    Long ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = set.getLong( 1 );
                        } else {
                            ret = null;
                        }
                    }
                    return ret;
                }
            }.execute();

            if( ret != null ) {
                return ret;
            } else {
                return 0;
            }

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return -1L;
    }

//    /**
//     * {@inheritDoc}
//     */
//    @Override
//    protected long getTimeUpdatedAfterLastPositionInHistory(
//            String key )
//        throws
//            StoreWithHistoryKeyDoesNotExistException
//    {
//        getTimeUpdatedBeforeFirstPositionInHistory( key ); // to check that key exists
//
//        return Long.MAX_VALUE;
//    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size(
            String startsWith )
        throws
            IOException
    {
        checkKey( startsWith );

        return sizeWithPattern( startsWith + '%' ); // trailing wild card
    }

    /**
     * Helper method to determine the number of StoreValueHistories in this HistoryStore whose key
     * matches the provided pattern.
     *
     * @param pattern the pattern
     * @return the number of StoreValueHistoriess in this HistoryStore whose key matches this pattern
     * @throws IOException thrown if an I/O error occurred
     */
    protected int sizeWithPattern(
            final String pattern )
        throws
            IOException
    {
        try {
            int ret = new SqlExecutionAction<Integer>( theSizePreparedStatement ) {
                @Override
                protected Integer perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, pattern );
                    stm.execute();

                    int ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = set.getInt( 1 );
                        } else {
                            ret = 0;
                        }
                    }
                    return ret;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return Integer.MAX_VALUE; // Is this a reasonable default?
    }

    /**
     * The createTables PreparedStatement.
     */
    protected final SqlPreparedStatement theCreateTablesPreparedStatement;

    /**
     * The dropTables PreparedStatement.
     */
    protected final SqlPreparedStatement theDropTablesPreparedStatement;

    /**
     * The hasTables PreparedStatement.
     */
    protected final SqlPreparedStatement theHasTablesPreparedStatement;

    /**
     * The putOrUpdate PreparedStatement.
     */
    protected final SqlPreparedStatement thePutOrUpdatePreparedStatement;

    /**
     * The get-most-recent PreparedStatement.
     */
    protected final SqlPreparedStatement theGetMostRecentPreparedStatement;

    /**
     * The get PreparedStatement.
     */
    protected final SqlPreparedStatement theGetPreparedStatement;

    /**
     * The find-first-key PreparedStatement
     */
    protected final SqlPreparedStatement theFindFirstKeyPreparedStatement;

    /**
     * The PreparedStatement to find the key of N rows ahead.
     */
    protected final SqlPreparedStatement theFindKeyAtPositivePreparedStatement;

    /**
     * The PreparedStatement to find the key of N rows back.
     */
    protected final SqlPreparedStatement theFindKeyAtNegativePreparedStatement;

    /**
     * The PreparedStatement to find the key of N rows from the end of the table;
     */
    protected final SqlPreparedStatement theFindKeyAtEndPreparedStatement;

    /**
     * The PreparedStatement to obtain the next N keys including the specified key.
     */
    protected final SqlPreparedStatement theFindNextKeysIncludingPreparedStatement;

    /**
     * The PreparedStatement to obtain the previous N keys excluding the specified key.
     */
    protected final SqlPreparedStatement theFindPreviousKeysExcludingPreparedStatement;

    /**
     * The PreparedStatement to obtain the last N keys in the table.
     */
    protected final SqlPreparedStatement theFindLastKeysPreparedStatement;

    /**
     * The PreparedStatement to obtain the number of keys including and after this key.
     */
    protected final SqlPreparedStatement theHasNextKeysIncludingPreparedStatement;

    /**
     * The PreparedStatement to obtain the number of keys excluding and before this key.
     */
    protected final SqlPreparedStatement theHasPreviousKeysExcludingPreparedStatement;

    /**
     * The PreparedStatement to find the number of keys between two keys.
     */
    protected final SqlPreparedStatement theDetermineKeysDistancePreparedStatement;

    /**
     * The PreparedStatement to find the number of keys from a key to the end of the keys.
     */
    protected final SqlPreparedStatement theDetermineKeysDistanceToEndPreparedStatement;

    /**
     * The size PreparedStatement.
     */
    protected final SqlPreparedStatement theSizePreparedStatement;

    /**
     * The PreparedStatement to find the next n values for given key.
     */
    protected final SqlPreparedStatement theFindNextValuesIncludingPreparedStatement;

    /**
     * The PreparedStatement to find the previous n values for given key.
     */
    protected final SqlPreparedStatement theFindPreviousValuesExcludingPreparedStatement;

    /**
     * The PreparedStatement to find the last n values for given key.
     */
    protected final SqlPreparedStatement theFindLastValuesPreparedStatement;

    /**
     * The PreparedStatement to find out the next timeUpdated greater than or equal to some starting timeUpdated.
     */
    protected final SqlPreparedStatement theDetermineGreaterOrEqualTimeUpdatedPreparedStatement;

    /**
     * The PreparedStatement to find out the next timeUpdated greater than some starting timeUpdated.
     */
    protected final SqlPreparedStatement theDetermineGreaterTimeUpdatedPreparedStatement;

    /**
     * The PreparedStatement to find out how many steps to just before a time or the next thereafter,
     * and what the "to" timeUpdated is.
     */
    protected final SqlPreparedStatement theDetermineStepsToJustBeforeTimePreparedStatement;

    /**
     * The PreparedStatement to find out how many steps to just after a time or the next thereafter,
     * and what the "to" timeUpdated is, in forward direction.
     */
    protected final SqlPreparedStatement theDetermineStepsToJustAfterTimePreparedStatement;

    /**
     * The PreparedStatement to find the oldest timeUpdated for a given key.
     */
    protected final SqlPreparedStatement theFindFirstValuePreparedStatement;

    /**
     * The PreparedStatement that deletes all rows whose key starts with a certain string
     */
    protected final SqlPreparedStatement theDeleteAllStm;

    /**
     * The SQL to create the tables in the database.
     * See also separate file in src/resources/sql
     */
    protected static final String CREATE_TABLES_SQL
            = "CREATE TABLE {0} (\n"
            + "    id          VARCHAR(511) COLLATE utf8mb4_nopad_bin NOT NULL,\n" // this automatically creates an index
            + "    encodingId  VARCHAR(128),\n"
            + "    timeUpdated BIGINT NOT NULL,\n"
            + "    content     LONGBLOB,\n"
            + "    PRIMARY KEY(id,timeUpdated)\n"
            + ");";

    /**
     * The SQL to drop the tables in the database.
     */
    protected static final String DROP_TABLES_SQL
            = "DROP TABLE {0};";

    /**
     * The SQL to detect whether or not the tables exist in the database.
     */
    protected static final String HAS_TABLES_SQL
            = "SELECT COUNT(*) FROM {0} WHERE id = '''';"; // need double-double quotes for MessageFormat

    /**
     * The SQL to putOrUpdate data into the Store.
     */
    protected static final String PUT_OR_UPDATE_SQL
            = "INSERT INTO {0} (\n"
            + "    id,\n"           // 1
            + "    encodingId,\n"   // 2
            + "    timeUpdated,\n"  // 3
            + "    content )\n"     //
            + "VALUES(\n"
            + "    ?,\n"  // 1
            + "    ?,\n"  // 2
            + "    ?,\n"  // 3
            + "    ? )\n" // 4
            + "ON DUPLICATE KEY UPDATE\n"
            + "    encodingId  = ?,\n" // 5
            + "    content     = ?;";  // 6

    /**
     * The SQL to get data from the Store by id and timeUpdated
     */
    protected static final String GET_SQL
            = "SELECT * FROM {0} WHERE id = ? AND timeUpdated = ?;";

    /**
     * The SQL to get the most recent row for a given id.
     */
    protected static final String GET_MOST_RECENT_SQL
            = "SELECT * FROM {0} WHERE id= ? ORDER BY timeUpdated DESC LIMIT 1";

    /**
     * The SQL to get the first key for an iterator. We don't really care about
     * timeUpdated here.
     */
    protected static final String FIND_FIRST_KEY_SQL
            = "SELECT id FROM {0} WHERE id LIKE ? ORDER BY id LIMIT 1;";

    /**
     * The SQL to find the key N rows ahead.
     */
    protected static final String FIND_KEY_AT_POSITIVE_SQL
            = "SELECT DISTINCT(id) FROM {0} WHERE id > ? AND id LIKE ? ORDER BY id LIMIT ?";

    /**
     * The SQL to find the key N rows back.
     */
    protected static final String FIND_KEY_AT_NEGATIVE_SQL
            = "SELECT DISTINCT(id) FROM {0} WHERE id < ? AND id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * The SQL to find the key N rows from the end of the table.
     */
    protected static final String FIND_KEY_AT_END_SQL
            = "SELECT DISTINCT(id) FROM {0} WHERE id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * The SQL to obtain the next n keys in the Store.
     */
    protected static final String FIND_NEXT_KEYS_INCLUDING_SQL
            = "SELECT DISTINCT(id) FROM {0} WHERE id >= ? AND id LIKE ? ORDER BY id LIMIT ?;";

    /**
     * The SQL to obtain the previous n keys in the Store.
     */
    protected static final String FIND_PREVIOUS_KEYS_EXCLUDING_SQL
            = "SELECT DISTINCT(id) FROM {0} WHERE id < ? AND id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * The SQL to obtain the last n keys in the Store.
     */
    protected static final String FIND_LAST_KEYS_SQL
            = "SELECT DISTINCT(id) FROM {0} WHERE id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * The SQL to obtain the number of keys including and after this key.
     */
    protected static final String HAS_NEXT_KEYS_INCLUDING_SQL
            = "SELECT COUNT(DISTINCT(id)) FROM {0} WHERE id >= ? AND id LIKE ?;";

    /**
     * The SQL to obtain the number of keys excluding and before this key.
     */
    protected static final String HAS_PREVIOUS_KEYS_EXCLUDING_SQL
            = "SELECT COUNT(DISTINCT(id)) FROM {0} WHERE id < ? AND id LIKE ?;";

    /**
     * The SQL to determine the distance, in keys, between a first and a second key.
     */
    protected static final String DETERMINE_KEYS_DISTANCE_SQL
            = "SELECT COUNT(DISTINCT(id)) FROM {0} WHERE id >= ? AND id < ? AND id LIKE ?;";

    /**
     * The SQL to determine the distance, in keys, between a key and the end of the keys.
     */
    protected static final String DETERMINE_KEYS_DISTANCE_TO_END_SQL
            = "SELECT COUNT(DISTINCT(id)) FROM {0} WHERE id >= ? AND id LIKE ?;";

    /**
     * The SQL to determine the number of entries in the entire Store.
     */
    protected static final String SIZE_SQL
            = "SELECT COUNT(DISTINCT(id)) FROM {0} WHERE id LIKE ?;";

    /**
     * The SQL to determine the number of values including and after this timeUpdated for this key.
     */
    protected static final String FIND_NEXT_VALUES_INCLUDING_SQL
            = "SELECT * FROM {0} WHERE id = ? AND timeUpdated >= ? ORDER BY timeUpdated LIMIT ?;";

    /**
     * The SQL to determine the number of values excluding and before this timeUpdated for this key.
     */
    protected static final String FIND_PREVIOUS_VALUES_EXCLUDING_SQL
            = "SELECT * FROM {0} WHERE id = ? AND timeUpdated < ? ORDER BY timeUpdated DESC LIMIT ?;";

    /**
     * The SQL to determine the last n values for this key.
     */
    protected static final String FIND_LAST_VALUES_SQL
            = "SELECT * FROM {0} WHERE id = ? ORDER BY timeUpdated DESC LIMIT ?;";

    /**
     * The SQL to find out the timeUpdated greater than or equal to some starting timeUpdated.
     */
    protected static final String DETERMINE_GREATER_OR_EQUAL_TIME_UPDATED_SQL
            = "SELECT MIN( timeUpdated ) AS adjustedTo "
            + "FROM {0} WHERE id = ? AND timeUpdated >= ?;";

    /**
     * The SQL to find out the timeUpdated greater than some starting timeUpdated.
     */
    protected static final String DETERMINE_GREATER_TIME_UPDATED_SQL
            = "SELECT MIN( timeUpdated ) AS adjustedTo "
            + "FROM {0} WHERE id = ? AND timeUpdated > ?;";

    /**
     * The SQL to find out how many steps to just before a time or the next thereafter,
     * in forward direction.
     */
    protected static final String DETERMINE_STEPS_TO_JUST_BEFORE_TIME_SQL
            = "SELECT COUNT(*) AS steps "
            + "FROM {0} "
            + "WHERE id = ? AND timeUpdated >= ? AND timeUpdated < ?;";

    /**
     * The SQL to find out how many steps to just after a time or the next thereafter,
     * in forward direction.
     */
    protected static final String DETERMINE_STEPS_TO_JUST_AFTER_TIME_SQL
            = "SELECT COUNT(*) AS steps "
            + "FROM {0} "
            + "WHERE id = ? AND timeUpdated >= ? AND timeUpdated < ?;";

    /**
     * The SQL to find the oldest timeUpdated for a given key.
     */
    protected static final String FIND_FIRST_VALUE_SQL
            = "SELECT timeUpdated FROM {0} WHERE id = ? ORDER BY timeUpdated LIMIT 1;";

    /**
     * The SQL to delete all rows whose keys start with a certain string.
     */
    protected static final String DELETE_ALL_SQL
            = "DELETE FROM {0} WHERE id LIKE ?;";
}
