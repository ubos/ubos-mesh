//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.mysql;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import javax.sql.DataSource;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreKeyExistsAlreadyException;
import net.ubos.store.StoreValue;
import net.ubos.store.StoreValueCursorIterator;
import net.ubos.store.sql.SqlStoreIOException;
import net.ubos.store.sql.SqlStoreValueCursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.sql.SqlDatabase;
import net.ubos.util.sql.SqlExecutionAction;
import net.ubos.util.sql.SqlPreparedStatement;

/**
 * SqlStore implementation using MySQL.
 */
public class MysqlStore
        extends
            AbstractMysqlStore<StoreValue>
{
    private static final Log log = Log.getLogInstance( MysqlStore.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @param db the SqlDatabase
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     * @return the created AbstractSqlStore
     */
    public static MysqlStore create(
            SqlDatabase db,
            String      tableName )
    {
        return new MysqlStore( db, tableName );
    }

    /**
     * Factory method.
     *
     * @param ds the SQL DataSource
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     * @return the created AbstractSqlStore
     */
    public static MysqlStore create(
            DataSource ds,
            String     tableName )
    {
        SqlDatabase db = SqlDatabase.create( "MysqlStore of " + ds.toString() + ", table " + tableName, ds );

        return new MysqlStore( db, tableName );
    }

    /**
     * Constructor for subclasses only, use factory method.
     *
     * @param db the SQL Database
     * @param tableName the name of the table in the SQL DataSource in which the data will be stored
     */
    protected MysqlStore(
            SqlDatabase db,
            String      tableName )
    {
        super( db, tableName );

        theCreateTablesStm           = new SqlPreparedStatement( theDatabase, CREATE_TABLES_SQL,             tableName );
        thePutStm                    = new SqlPreparedStatement( theDatabase, PUT_SQL,                       tableName );
        theUpdateStm                 = new SqlPreparedStatement( theDatabase, UPDATE_SQL,                    tableName );
        thePutOrUpdateStm            = new SqlPreparedStatement( theDatabase, PUT_OR_UPDATE_SQL,             tableName );
        theGetStm                    = new SqlPreparedStatement( theDatabase, GET_SQL,                       tableName );

        theFindNextIncludingStm      = new SqlPreparedStatement( theDatabase, FIND_NEXT_INCLUDING_SQL,       tableName );
        theFindPreviousExcludingStm  = new SqlPreparedStatement( theDatabase, FIND_PREVIOUS_EXCLUDING_SQL,   tableName );
        theFindLastValuesStm         = new SqlPreparedStatement( theDatabase, FIND_LAST_VALUES_SQL,          tableName );
        theHasNextIncludingStm       = new SqlPreparedStatement( theDatabase, HAS_NEXT_INCLUDING_SQL,        tableName );
        theHasPreviousExcludingStm   = new SqlPreparedStatement( theDatabase, HAS_PREVIOUS_EXCLUDING_SQL,    tableName );
        theFindFirstKeyStm           = new SqlPreparedStatement( theDatabase, FIND_FIRST_KEY_SQL,            tableName );
        theFindKeyAtPositiveStm      = new SqlPreparedStatement( theDatabase, FIND_KEY_AT_POSITIVE_SQL,      tableName );
        theFindKeyAtNegativeStm      = new SqlPreparedStatement( theDatabase, FIND_KEY_AT_NEGATIVE_SQL,      tableName );
        theFindKeyAtEndStm           = new SqlPreparedStatement( theDatabase, FIND_KEY_AT_END_SQL,           tableName );
        theDetermineDistanceStm      = new SqlPreparedStatement( theDatabase, DETERMINE_DISTANCE_SQL,        tableName );
        theDetermineDistanceToEndStm = new SqlPreparedStatement( theDatabase, DETERMINE_DISTANCE_TO_END_SQL, tableName );

        if( log.isTraceEnabled() ) {
            log.traceConstructor( this );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createTables()
            throws
                IOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "createTables" );
        }

        try {
            new SqlExecutionAction<Object>( theCreateTablesStm ) {
                @Override
                protected Object perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.execute();
                    return null;
                }
            }.execute();

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "createTables", ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void put(
            final StoreValue toPut )
        throws
            StoreKeyExistsAlreadyException,
            SqlStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "put", toPut );
        }

        final String  key         = toPut.getKey();
        final String  encodingId  = toPut.getEncodingId();
        final byte [] data        = toPut.getData();

        checkKey(      key );
        checkEncoding( encodingId );
        checkData(     data );

        boolean success = false;
        try {
            success = new SqlExecutionAction<Boolean>( thePutStm ) {
                @Override
                protected Boolean perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setString( 2, encodingId );
                    stm.setBytes(  3, data );

                    stm.execute();

                    boolean success = stm.getUpdateCount() > 0;

                    return success;
                }
            }.execute();

            if( !success ) {
                throw new StoreKeyExistsAlreadyException( this, key );
            }

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "put", key, encodingId, data, ex );

        } finally {
            firePutPerformed( toPut, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(
            final StoreValue toUpdate )
        throws
            StoreKeyDoesNotExistException,
            SqlStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "update", toUpdate );
        }

        final String  key        = toUpdate.getKey();
        final String  encodingId = toUpdate.getEncodingId();
        final byte [] data       = toUpdate.getData();

        checkKey(      key );
        checkEncoding( encodingId );
        checkData(     data );

        boolean success = false;
        try {
            success = new SqlExecutionAction<Boolean>( theUpdateStm ) {
                @Override
                protected Boolean perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, encodingId );
                    stm.setBytes(  2, data );
                    stm.setString( 3, key );
                    stm.execute();

                    boolean success = stm.getUpdateCount() > 0;

                    return success;
                }
            }.execute();

            if( !success ) {
                throw new StoreKeyDoesNotExistException( this, key  );
            }

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "update", key, encodingId, data, ex );

        } finally {
            fireUpdatePerformed( toUpdate, success );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean putOrUpdate(
            final StoreValue toPutOrUpdate )
        throws
            SqlStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "putOrUpdate", toPutOrUpdate );
        }

        final String  key         = toPutOrUpdate.getKey();
        final String  encodingId  = toPutOrUpdate.getEncodingId();
        final byte [] data        = toPutOrUpdate.getData();

        checkKey(      key );
        checkEncoding( encodingId );
        checkData(     data );

        boolean ret     = false;
        boolean success = false;
        try {
            ret = new SqlExecutionAction<Boolean>( thePutOrUpdateStm ) {
                @Override
                protected Boolean perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setString( 2, encodingId );
                    stm.setBytes(  3, data );

                    stm.setString( 4, encodingId );
                    stm.setBytes(  5, data );

                    stm.execute();

                    boolean ret = stm.getUpdateCount() > 1; // the "duplicate key" seems to trigger two, instead of one

                    return ret;
                }

            }.execute();

            success = true;

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "putOrUpdate", key, encodingId, data, ex );

        } finally {
            if( ret ) {
                fireUpdatePerformed( toPutOrUpdate, success );
            } else {
                firePutPerformed( toPutOrUpdate, success );
            }
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValue get(
            final String key )
        throws
            StoreKeyDoesNotExistException,
            SqlStoreIOException
    {
        if( log.isTraceEnabled() ) {
            log.traceMethodCallEntry( this, "get", key );
        }

        checkKey( key );

        StoreValue ret = null;
        try {
            ret = new SqlExecutionAction<StoreValue>( theGetStm ) {
                @Override
                protected StoreValue perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    StoreValue ret = null;

                    stm.setString( 1, key );
                    stm.execute();

                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            String  foundKey   = new String( set.getBytes( "id" ), StandardCharsets.UTF_8 );
                                                 // this should work with getString( "id" ), but somehow the JDBC
                                                 // driver does not recognize this is UTF-8 and mangles the result
                            String  encodingId = set.getString( "encodingId" );
                            byte [] data       = set.getBytes(  "content" );

                            if( key != null && !key.equals( foundKey )) {
                                log.error( "Found different key: " + key + " vs. " + foundKey );
                            }
                            ret = new StoreValue(
                                    foundKey,
                                    encodingId,
                                    data );
                        }
                    }
                    return ret;
                }
            }.execute();


            if( ret == null ) {
                throw new StoreKeyDoesNotExistException( this, key );
            }

        } catch( SQLException ex ) {
            throw new SqlStoreIOException( this, "get", key, ex );

        } finally {
            fireGetPerformed( key, ret );
        }
        return ret;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValue> findNextIncluding(
            String key,
            int    n,
            String pattern )
    {
        if( key == null ) {
            return Collections.emptyList();
        }
        return findValues( theFindNextIncludingStm, key, n, pattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<StoreValue> findPreviousExcluding(
            final String key,
            final int    n,
            final String pattern )
    {
        if( key != null ) {
            return findValues( theFindPreviousExcludingStm, key, n, pattern );
        }

        List<StoreValue> ret = new ArrayList<>();
        try {
            try (ResultSet set = new SqlExecutionAction<ResultSet>( theFindLastValuesStm ) {
                @Override
                protected ResultSet perform(
                        PreparedStatement stm,
                        Connection        conn )
                        throws
                        SQLException
                {
                    stm.setString( 1, pattern );
                    stm.setInt(    2, n );

                    stm.execute();
                    return stm.getResultSet();
                }
            }.execute()) {
                while( set.next() ) {
                    String  foundKey   = new String( set.getBytes( "id" ), StandardCharsets.UTF_8 );
                                         // this should work with getString( "id" ), but somehow the JDBC
                                         // driver does not recognize this is UTF-8 and mangles the result
                    String  encodingId = set.getString( "encodingId" );
                    byte [] data       = set.getBytes(  "content" );

                    // this is different from the get() method -- we find the one after, not the same one
                    ret.add( new StoreValue( foundKey, encodingId, data ));
                }
            }

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return ret;
    }

    /**
     * Find n StoreValues, using the provided PreparedStatement.
     *
     * @param stm the SQL to use
     * @param key key for the where clause
     * @param n the number of StoreValues to find
     * @param pattern the pattern to filter by
     * @return the found StoreValues
     */
    protected List<StoreValue> findValues(
            SqlPreparedStatement stm,
            final String         key,
            final int            n,
            final String         pattern )
    {
        try {
            List<StoreValue> ret = new SqlExecutionAction<List<StoreValue>>( stm ) {
                @Override
                protected List<StoreValue> perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setString( 2, pattern );
                    stm.setInt(    3, n );

                    stm.execute();

                    List<StoreValue> temp;
                    try (ResultSet set = stm.getResultSet()) {
                        temp = new ArrayList<>( n );
                        while( set.next() ) {
                            String  foundKey   = new String( set.getBytes( "id" ), StandardCharsets.UTF_8 );
                                                 // this should work with getString( "id" ), but somehow the JDBC
                                                 // driver does not recognize this is UTF-8 and mangles the result
                            String  encodingId = set.getString( "encodingId" );
                            byte [] data       = set.getBytes(  "content" );

                            // this is different from the get() method -- we find the one after, not the same one
                            temp.add( new StoreValue( foundKey, encodingId, data ));
                        }
                    }
                    return temp;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );

            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String findFirstKey(
            final String pattern )
        throws
            NoSuchElementException
    {
        try {
            String ret = new SqlExecutionAction<String>( theFindFirstKeyStm ) {
                @Override
                protected String perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, pattern );
                    stm.execute();

                    String ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = set.getString( 1 );
                        } else {
                            ret = null;
                        }
                    }
                    return ret;
                }
            }.execute();

            if( ret != null ) {
                return ret;
            } else {
                throw new NoSuchElementException();
            }

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasNextIncluding(
            String key,
            String pattern )
    {
        if( key == null ) {
            return 0;
        }
        return countRows( theHasNextIncludingStm, key, pattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int hasPreviousExcluding(
            String key,
            String pattern )
    {
        if( key == null ) {
            try {
                int ret = sizeWithPattern( pattern );
                return ret;
            } catch( IOException ex ) {
                log.error( ex );
                return 0;
            }
        }
        return countRows( theHasPreviousExcludingStm, key, pattern );
    }

    /**
     * Count the number of rows that meet the condition in the SQL statement.
     *
     * @param stm the SQL statement
     * @param key key parameter
     * @param pattern the pattern to filter by
     * @return the number of rows
     */
    protected int countRows(
            SqlPreparedStatement stm,
            final String         key,
            final String         pattern )
    {
        try {
            int ret = new SqlExecutionAction<Integer>( stm ) {
                @Override
                protected Integer perform(
                        PreparedStatement stm,
                        Connection        conn )
                    throws
                        SQLException
                {
                    stm.setString( 1, key );
                    stm.setString( 2, pattern );
                    stm.execute();

                    int ret;
                    try (ResultSet set = stm.getResultSet()) {
                        if( set.next() ) {
                            ret = set.getInt( 1 );
                        } else {
                            ret = 0;
                        }
                    }
                    return ret;
                }
            }.execute();

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return Integer.MAX_VALUE; // Is this a reasonable default?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String findKeyAt(
            final String key,
            final int    delta,
            final String pattern )
        throws
            NoSuchElementException
    {
        if( key != null ) {
            SqlPreparedStatement stm;
            final int distance;
            if( delta >= 0 ) {
                stm = theFindKeyAtPositiveStm;
                distance = delta;
            } else {
                stm = theFindKeyAtNegativeStm;
                distance = -delta;
            }
            try {
                final String TOO_FAR_MARKER = "";

                String ret = new SqlExecutionAction<String>( stm ) {
                    @Override
                    protected String perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setString( 1, key );
                        stm.setString( 2, pattern );
                        stm.setInt(    3, distance );
                        stm.execute();

                        String ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.last() ) {
                                if( set.getRow() == distance ) {
                                    ret = set.getString( 1 );
                                } else if( set.getRow() == distance-1 ) {
                                    ret = null;
                                } else {
                                    ret = TOO_FAR_MARKER;
                                }
                            } else {
                                ret = null;
                            }
                        }
                        return ret;
                    }
                }.execute();

                if( (Object) ret != TOO_FAR_MARKER ) {
                    return ret;
                } else {
                    throw new NoSuchElementException();
                }

            } catch( SQLException ex ) {
                log.error( ex );
            }

        } else {
            if( delta >= 0 ) {
                return null;
            }
            final int distance = -delta;
            try {
                String ret = new SqlExecutionAction<String>( theFindKeyAtEndStm ) {
                    @Override
                    protected String perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setString( 1, pattern );
                        stm.setInt(    2, distance );
                        stm.execute();

                        String ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.last() ) {
                                ret = set.getString( 1 );
                            } else {
                                ret = null;
                            }
                        }
                        return ret;
                    }
                }.execute();

                return ret;

            } catch( SQLException ex ) {
                log.error( ex );
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int determineDistance(
            final String from,
            final String to,
            final String pattern )
    {
        try {
            int ret;

            if( to != null ) {
                ret = new SqlExecutionAction<Integer>( theDetermineDistanceStm ) {
                    @Override
                    protected Integer perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setString( 1, from );
                        stm.setString( 2, pattern );
                        stm.setString( 3, to );
                        stm.execute();

                        int ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.next() ) {
                                ret = set.getInt( 1 );
                            } else {
                                ret = 0;
                            }
                        }
                        return ret;
                    }
                }.execute();
            } else {
                ret = new SqlExecutionAction<Integer>( theDetermineDistanceToEndStm ) {
                    @Override
                    protected Integer perform(
                            PreparedStatement stm,
                            Connection        conn )
                        throws
                            SQLException
                    {
                        stm.setString( 1, from );
                        stm.setString( 2, pattern );
                        stm.execute();

                        int ret;
                        try (ResultSet set = stm.getResultSet()) {
                            if( set.next() ) {
                                ret = set.getInt( 1 );
                            } else {
                                ret = 0;
                            }
                        }
                        return ret;
                    }
                }.execute();
            }

            return ret;

        } catch( SQLException ex ) {
            log.error( ex );
        }
        return Integer.MAX_VALUE; // Is this a reasonable default?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueCursorIterator<StoreValue> iterator(
            String startsWith )
    {
        String pattern = startsWith + "%";
        try {
            if( isEmpty( startsWith ) ) {
                return new SqlStoreValueCursorIterator<>( null, pattern, this ); // past last position
            } else {
                return new SqlStoreValueCursorIterator<>( findFirstKey( pattern ), pattern, this );
            }
        } catch( IOException ex ) {
            log.error( ex );
            return new SqlStoreValueCursorIterator<>( null, pattern, this ); // gotta be somewhere
        }
    }

    /**
     * Create table. See also separate SQL file.
     */
    protected final SqlPreparedStatement theCreateTablesStm;
    protected static final String CREATE_TABLES_SQL
            = "CREATE TABLE {0} (\n"
            + "    id         VARCHAR(511) COLLATE utf8mb4_nopad_bin NOT NULL PRIMARY KEY,\n" // this automatically creates an index
            + "    encodingId VARCHAR(128),\n"
            + "    content    LONGBLOB"
            + ");";

    /**
     * Put a row into the table.
     */
    protected final SqlPreparedStatement thePutStm;
    protected static final String PUT_SQL
            = "INSERT INTO {0} (\n"
            + "    id,\n"
            + "    encodingId,\n"
            + "    content )\n"
            + "VALUES(\n"
            + "    ?,\n"
            + "    ?,\n"
            + "    ? );";

    /**
     * Update a row.
     */
    protected final SqlPreparedStatement theUpdateStm;
    protected static final String UPDATE_SQL
            = "UPDATE {0} SET\n"
            + "    encodingId = ?,\n"
            + "    content    = ?"
            + "WHERE id = ?;";

    /**
     * Put a row into the table, or update it if it exists already
     */
    protected final SqlPreparedStatement thePutOrUpdateStm;
    protected static final String PUT_OR_UPDATE_SQL
            = "INSERT INTO {0} (\n"
            + "    id,\n"                    // 1
            + "    encodingId,\n"            // 2
            + "    content )\n"              // 3
            + "VALUES(\n"
            + "    ?,\n"  // 1
            + "    ?,\n"  // 2
            + "    ? )\n" // 3
            + "ON DUPLICATE KEY UPDATE\n"
            + "    encodingId  = ?,\n" // 4
            + "    content     = ?;";  // 5


    /**
     * Get a row.
     */
    protected final SqlPreparedStatement theGetStm;
    protected static final String GET_SQL
            = "SELECT * from {0} WHERE id = ?;";

    /**
     * Find the next N rows, including the specified key's row.
     */
    protected final SqlPreparedStatement theFindNextIncludingStm;
    protected static final String FIND_NEXT_INCLUDING_SQL
            = "SELECT * from {0} WHERE id >= ? AND id LIKE ? ORDER BY id LIMIT ?;";

    /**
     * Find the previous N rows, excluding the specified key's row
     */
    protected final SqlPreparedStatement theFindPreviousExcludingStm;
    protected static final String FIND_PREVIOUS_EXCLUDING_SQL
            = "SELECT * from {0} WHERE id < ? AND id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * Find the last N rows in the table.
     */
    protected final SqlPreparedStatement theFindLastValuesStm;
    protected static final String FIND_LAST_VALUES_SQL
            = "SELECT * from {0} WHERE id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * How many rows including and after this key?
     */
    protected final SqlPreparedStatement theHasNextIncludingStm;
    protected static final String HAS_NEXT_INCLUDING_SQL
            = "SELECT COUNT(*) from {0} WHERE id >= ? AND id LIKE ?;";

    /**
     * How many rows excluding and before this key?
     */
    protected final SqlPreparedStatement theHasPreviousExcludingStm;
    protected static final String HAS_PREVIOUS_EXCLUDING_SQL
            = "SELECT COUNT(*) from {0} WHERE id < ? AND id LIKE ?;";

    /**
     * Find the first key.
     */
    protected final SqlPreparedStatement theFindFirstKeyStm;
    protected static final String FIND_FIRST_KEY_SQL
            = "SELECT id from {0} WHERE id LIKE ? ORDER BY id LIMIT 1;";

    /**
     * Find the key of N rows ahead.
     */
    protected final SqlPreparedStatement theFindKeyAtPositiveStm;
    protected static final String FIND_KEY_AT_POSITIVE_SQL
            = "SELECT id from {0} WHERE id > ? AND id LIKE ? ORDER BY id LIMIT ?";

    /**
     * Find the key of N rows back.
     */
    protected final SqlPreparedStatement theFindKeyAtNegativeStm;
    protected static final String FIND_KEY_AT_NEGATIVE_SQL
            = "SELECT id from {0} WHERE id < ? AND id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * Find the key of N rows from the end of the table;
     */
    protected final SqlPreparedStatement theFindKeyAtEndStm;
    protected static final String FIND_KEY_AT_END_SQL
            = "SELECT id from {0} WHERE id LIKE ? ORDER BY id DESC LIMIT ?;";

    /**
     * Determine the number of rows between two keys.
     */
    protected final SqlPreparedStatement theDetermineDistanceStm;
    protected static final String DETERMINE_DISTANCE_SQL
            = "SELECT COUNT(*) from {0} WHERE id >= ? AND id < ? AND id LIKE ?;";

    /**
     * Determine number of rows from a key to the end of the table.
     */
    protected final SqlPreparedStatement theDetermineDistanceToEndStm;
    protected static final String DETERMINE_DISTANCE_TO_END_SQL
            = "SELECT COUNT(*) from {0} WHERE id >= ? AND id LIKE ?;";
}
