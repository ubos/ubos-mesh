//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.peertalk;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import net.ubos.mesh.BlessedAlreadyException;
import net.ubos.mesh.EntityNotBlessedException;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.NotRelatedException;
import net.ubos.mesh.RoleTypeNotBlessedException;
import net.ubos.mesh.RoleTypeRequiresEntityTypeException;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.util.logging.Log;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.peertalk.externalized.json.PeerTalkJsonDecoder;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.model.primitives.externalized.DecodingException;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.util.UndertowHttpRequest;

/**
 * Accepts PeerTalk messages arriving over HTTP.
 */
public class PeerTalkHandler
    implements
        HttpHandler
{
    private static final Log log = Log.getLogInstance( PeerTalkHandler.class );

    /**
     * Constructor.
     *
     * @param authorizationToken if not null, check that incoming requests carry this bearer token
     * @param cascadingApply if true, also apply any additionally necessary changes to make the model valid again
     */
    public PeerTalkHandler(
            String  authorizationToken,
            boolean cascadingApply )
    {
        theAuthorizationToken = authorizationToken;
        theCascadingApply     = cascadingApply;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleRequest(
            HttpServerExchange hse )
    {
        UndertowHttpRequest request = UndertowHttpRequest.create( hse, Underbars.getContextPath() );

        if( !request.isPost() || !"application/json".equals( request.getContentType())) {
            hse.getResponseHeaders().put( Headers.CONTENT_TYPE, "text/plain" );
            hse.setStatusCode( StatusCodes.BAD_REQUEST );
            hse.getResponseSender().send( "Not a valid PeerTalk message.\n" );

        } else {
            try {
                checkAuthorization( request );

                handlePeerTalkRequest( request );

                hse.getResponseHeaders().put( Headers.CONTENT_TYPE, "text/plain" );
                hse.setStatusCode( StatusCodes.ACCEPTED );
                hse.getResponseSender().send( "OK\n" );

            } catch( Throwable t ) {
                log.error( t ); // FIXME for production

                hse.getResponseHeaders().put( Headers.CONTENT_TYPE, "text/plain" );
                hse.setStatusCode( StatusCodes.INTERNAL_SERVER_ERROR );
                hse.getResponseSender().send( "Something went wrong.\n" );
            }
        }
    }

    /**
     * Check that the incoming request is authorized.
     *
     * @param request the incoming request
     * @throws NotAuthorizedException the incoming request was not authorized
     */
    protected void checkAuthorization(
            UndertowHttpRequest request )
        throws
            NotAuthorizedException
    {
        if( theAuthorizationToken == null || theAuthorizationToken.isBlank() ) {
            return; // auth checks not active
        }
        String authString = request.getAuthorizationHeader();
        if( authString == null ) {
            throw new NotAuthorizedException();
        }

        String [] authStringComponents = authString.split( "\\s+" );
        if( authStringComponents.length != 2 ) {
            throw new NotAuthorizedException();
        }
        if( !"Bearer".equals( authStringComponents[0] )) {
            throw new NotAuthorizedException();
        }
        if( !theAuthorizationToken.equals( authStringComponents[1] )) {
            throw new NotAuthorizedException();
        }
    }

    /**
     * Main work method for incoming PeerTalk requests.
     *
     * @param request the incoming request
     */
    protected void handlePeerTalkRequest(
            UndertowHttpRequest request )
        throws
            DecodingException,
            IOException
    {
        String postData = request.getPostData();

        MeshBase                                mainMeshBase = Underbars.getMeshBaseNameServer().getDefaultMeshBase();
        PrimaryMeshObjectIdentifierNamespaceMap primaryMap   = Underbars.getPrimaryNamespaceMap();

        PeerTalkJsonDecoder decoder = PeerTalkJsonDecoder.create();

        MContextualMeshObjectIdentifierNamespaceMap localNsMap = MContextualMeshObjectIdentifierNamespaceMap.create(
                primaryMap );

        ImporterMeshObjectIdentifierDeserializer idDeserializer = ImporterMeshObjectIdentifierDeserializer.create(
                mainMeshBase,
                localNsMap );

        List<ExternalizedChange> changes;
        try( Reader r = new StringReader( postData )) {
            changes = decoder.decodeHeadBody( r, idDeserializer );

        }
         if( changes == null ) {
             log.info( "Found no BODY_TAG" );

         } else if( theCascadingApply ) {
             applyChangesCascading( mainMeshBase, changes );

         } else {
             applyChanges( mainMeshBase, changes );
         }
    }

    /**
     * Apply the changes.
     *
     * @param mb the MeshBase
     * @param changes the Changes
     */
    protected void applyChanges(
            MeshBase                 mb,
            List<ExternalizedChange> changes )
    {
        mb.sudoExecute( () -> {

            int max = changes.size();

            for( int i=0 ; i<max ; ++i ) { // this type of loop is better for debugging
                ExternalizedChange current = changes.get( i );
                current.applyTo( mb );
           }
        });
    }

    /**
     * Apply the changes, cascading.
     *
     * @param mb the MeshBase
     * @param changes the Changes
     */
    protected void applyChangesCascading(
            MeshBase                 mb,
            List<ExternalizedChange> changes )
    {
        mb.sudoExecute( () -> {

            int max = changes.size();

            for( int i=0 ; i<max ; ++i ) { // this type of loop is better for debugging
                for( int j=0 ; j<10 ; ++j ) { // maybe several exceptions are being thrown, so we try for a while
                    try {
                        changes.get( i ).applyTo( mb );

                        break; // success, we can move on

                    } catch( BlessedAlreadyException ex ) {
                        // no op

                    } catch( EntityNotBlessedException ex ) {
                        ex.getMeshObject().bless( ex.getType() );

                    } catch( RoleTypeNotBlessedException ex ) {
                        ex.getMeshObject().blessRole( ex.getType(), ex.getNeighbor() );

                    } catch( MeshObjectIdentifierNotUniqueException ex ) {
                        // no op

                    } catch( NotRelatedException ex ) {
                        // no op

                    } catch( RoleTypeRequiresEntityTypeException ex ) {
                        ex.getMeshObject().unblessRole( ex.getRoleType(), ex.getNeighbor() );
                    }
                }
            }
        });
    }

    /**
     * If given, check that incoming requests carry this bearer token.
     */
    protected final String theAuthorizationToken;

    /**
     * If true, cascading-apply the changes. This means that if, for example, A.Rel.B, and A is to be deleted,
     * A.Rel.B is deleted, too, instead of throwing an exception.
     */
    protected final boolean theCascadingApply;

    /**
     * The URL path at which the PeerTalkHandler responds.
     */
    public static final String PATH = "/pt";
}
