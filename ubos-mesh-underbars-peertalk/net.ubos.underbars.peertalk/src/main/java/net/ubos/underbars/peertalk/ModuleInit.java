//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.peertalk;

import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleDeactivationException;
import net.ubos.underbars.Underbars;
import org.diet4j.core.ModuleSettings;

/**
 * Module initialization.
 */
public class ModuleInit
{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        ModuleSettings settings = thisModule.getModuleSettings();

        String  authorizationToken = settings.getString(  "authorizationtoken", null );
        boolean cascadingApply     = settings.getBoolean( "cascadingapply",     true );

        Underbars.registerPathDelegate( PeerTalkHandler.PATH, new PeerTalkHandler( authorizationToken, cascadingApply ) );
    }

    /**
     * Diet4j module deactivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleDeactivationException thrown if module deactivation failed
     */
    public static void moduleDectivate(
            Module thisModule )
        throws
            ModuleDeactivationException
    {
        Underbars.unregisterPathDelegate( PeerTalkHandler.PATH );
    }
}
