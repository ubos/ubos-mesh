//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.peertalk;

/**
 * Thrown if an incoming PeerTalk request was not authorized.
 */
public class NotAuthorizedException
    extends
        Exception
{

}
