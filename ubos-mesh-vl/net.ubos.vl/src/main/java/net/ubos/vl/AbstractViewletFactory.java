//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import java.util.Arrays;
import java.util.Comparator;
import net.ubos.util.ArrayHelper;
import net.ubos.util.factory.FactoryException;
import net.ubos.util.logging.Log;

/**
 * Factors out functionality common to {@link ViewletFactory ViewletFactory}
 * implementations.
 */
public abstract class AbstractViewletFactory
        implements
            ViewletFactory
{
    private static final Log log = Log.getLogInstance( AbstractViewletFactory.class ); // our own, private logger

    /**
     * {@inheritDoc}
     */
    @Override
    public Viewlet obtainFor(
            MeshObjectsToView objectsToView,
            Void              argument )
        throws
            FactoryException
    {
        ViewletFactoryChoice [] candidates = determineOrderedViewletChoices( objectsToView );

        if( candidates.length == 0 ) {
            throw new NoViewletFoundException( this, objectsToView );
        }

        try {
            Viewlet ret = candidates[0].instantiateViewlet();
            ret.setFactory( this );
            return ret;

        } catch( CannotViewException ex ) {
            throw new FactoryException( this, ex );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletFactoryChoice [] determineOrderedViewletChoices(
            MeshObjectsToView theObjectsToView )
    {
        ViewletFactoryChoice [] ret = determineViewletChoices( theObjectsToView );

        Arrays.sort( ret, QUALITY_COMPARATOR );

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletFactoryChoice [] determineViewletChoices(
            MeshObjectsToView objectsToView )
    {
        ViewletFactoryChoice [] available = determineAvailableViewletChoices( objectsToView );

        // now: filter/bolster the ones that have the right name, type etc.

        ViewletFactoryChoice [] ret = new ViewletFactoryChoice[ available.length ];
        int count = 0;

        OUTER:
        for( int i=0 ; i<ret.length ; ++i ) {

            // filter out by required features
            ViewletPlacement placement = objectsToView.getRequiredViewletPlacement();
            if( placement != null && !placement.equals( available[i].getPlacement())) {
                continue OUTER;
            }
            ViewletDimensionality dimensionality = objectsToView.getRequiredViewletDimensionality();
            if( dimensionality != null && !dimensionality.equals( available[i].getDimensionality())) {
                continue OUTER;
            }
            ViewletDetail detail = objectsToView.getRequiredViewletDetail();
            if( detail != null && !detail.equals( available[i].getDetail())) {
                continue OUTER;
            }

            String requiredName = objectsToView.getRequiredViewletName();
            if( requiredName != null ) {
                if( requiredName.equals( available[i].getName())) {
                    // we ignore everything else
                    ret[count++] = new MatchQualityChangedViewletFactoryChoice(
                            ViewletFactoryChoice.USER_SELECTED_MATCH_QUALITY,
                            available[i] );
                    continue OUTER;
                } else {
                    continue OUTER;
                }
            }

            String recommendedName = objectsToView.getRecommendedViewletName();
            if( recommendedName != null && recommendedName.equals( available[i].getName() )) {
                ret[count++] = new MatchQualityChangedViewletFactoryChoice(
                        ViewletFactoryChoice.USER_SELECTED_MATCH_QUALITY,
                        available[i] );
                continue OUTER;
            }

            final double UNCHANGED = 1.0;
            double       modifier  = UNCHANGED;

            dimensionality = objectsToView.getRecommendedViewletDimensionality();
            if( dimensionality != null ) {
                if( dimensionality.equals( available[i].getDimensionality())) {
                    modifier *= 1.1f;
                } else {
                    modifier *= 0.9f;
                }
            }

            detail = objectsToView.getRecommendedViewletDetail();
            if( detail != null ) {
                if( detail.equals( available[i].getDetail())) {
                    modifier *= 1.1f;
                } else {
                    modifier *= 0.9f;
                }
            }


            if( modifier == UNCHANGED ) {
                ret[ count++ ] = available[i];

            } else {
                ret[ count++ ] = new MatchQualityChangedViewletFactoryChoice(
                        modifier * available[i].getMatchQuality(),
                        available[i] );
            }
        }

        if( count < ret.length ) {
            ret = ArrayHelper.copyIntoNewArray( ret, 0, count, ViewletFactoryChoice.class );
        }
        return ret;
    }

    /**
     * Find the ViewletFactoryChoices that apply to these MeshObjectsToView, ignoring
     * the preferences specified in the MeshObjectsToView, or on the MeshObject itself
     * If none are found, return an empty array.
     *
     * @param theObjectsToView the MeshObjectsToView
     * @return the found ViewletFactoryChoices, if any
     */
    protected abstract ViewletFactoryChoice [] determineAvailableViewletChoices(
            MeshObjectsToView theObjectsToView );

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletFactoryChoice determineBestViewletChoice(
            MeshObjectsToView theObjectsToView )
    {
        ViewletFactoryChoice [] ordered = determineOrderedViewletChoices( theObjectsToView );
        if( ordered != null && ordered.length > 0 ) {
            return ordered[0];
        } else {
            return null;
        }
    }

    /**
     * Defines the ordering of ViewletFactoryChoices. This is implemented as a separate
     * Comparator rather than using &quot;natural ordering&quot; because there is no natural order of
     * ViewletFactoryChoices without the MeshObjectsToView instance, which doesn't
     * fit into the JDK API.
     */
    protected static class ByQualityComparator
        implements
            Comparator<ViewletFactoryChoice>
    {
        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(
                ViewletFactoryChoice o1,
                ViewletFactoryChoice o2)
        {
            double q1 = o1.getMatchQuality();
            double q2 = o2.getMatchQuality();

            if( q1 == q2 ) {
                return 0;
            } else {
                return (int) ( q1 - q2 );
            }
        }
    }

    /**
     * The comparator to use.
     */
    public static final Comparator<ViewletFactoryChoice> QUALITY_COMPARATOR = new ByQualityComparator();
}
