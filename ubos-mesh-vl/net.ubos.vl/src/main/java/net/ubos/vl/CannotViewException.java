//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Thrown when a Viewlet cannot view the MeshObjectsToView that have been
 * given to it. Use the inner classes to be specific about what is going on.
 * This often indicates a programming error.
 */
public abstract class CannotViewException
        extends
            AbstractLocalizedException
        implements
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param v which Viewlet could not view
     * @param o which MeshObjectsToView it could not view
     * @param msg a message describing the Exception
     * @param cause underlying Exception, if any
     */
    protected CannotViewException(
            Viewlet           v,
            MeshObjectsToView o,
            String            msg,
            Throwable         cause )
    {
        super( msg, cause );

        theViewlet       = v;
        theObjectsToView = o;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "vl",
                    "objectsToView"
                },
                new Object[] {
                    theViewlet,
                    theObjectsToView
        } );
    }

    /**
     * The Viewlet that could not view.
     */
    protected final Viewlet theViewlet;

    /**
     * The MeshObjectsToView that the Viewlet could not view.
     */
    protected final MeshObjectsToView theObjectsToView;

    /**
     * An invalid parameter has been specified in the invocation of this Viewlet.
     */
    public static class InvalidParameter
            extends
                CannotViewException
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
         * Constructor.
         *
         * @param v which Viewlet could not view
         * @param name the name of the parameter that was invalid
         * @param value the value of the parameter that was invalid
         * @param o which MeshObjectsToView it could not view
         */
        public InvalidParameter(
                Viewlet           v,
                String            name,
                Object            value,
                MeshObjectsToView o )
        {
            super( v, o, "Invalid parameter: " + name, null );

            theName  = name;
            theValue = value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[] {
                theViewlet.getName(),
                theViewlet.getUserVisibleName(),
                theObjectsToView.getSubject(),
                theName,
                theValue
            };
        }

        /**
         * Name of the invalid parameter.
         */
        protected final String theName;

        /*
         * Value of the invalid parameter.
         */
        protected final Object theValue;
    }

    /**
     * No subject was provided.
     */
    public static class NoSubject
            extends
                CannotViewException
    {
        private static final long serialVersionUID = 1L; // helps with serialization

        /**
         * Constructor.
         *
         * @param identifier the Identifier of the non-existing Subject.
         */
        public NoSubject(
                MeshObjectIdentifier identifier )
        {
            super( null, null, null, null );

            theIdentifier = identifier;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object [] getLocalizationParameters()
        {
            return new Object[] {
                    theViewlet != null ? theViewlet.getName()            : null,
                    theViewlet != null ? theViewlet.getUserVisibleName() : null,
                    theIdentifier
            };
        }

        /**
         * Identifier of the non-existing Subject.
         */
        protected final MeshObjectIdentifier theIdentifier;
    }
}
