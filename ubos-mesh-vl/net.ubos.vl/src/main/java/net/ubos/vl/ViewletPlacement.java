//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

/**
 * Defines the desired placement of the Viewlet.
 * This is an abstract concept that may be interpreted differently depending on the
 * Viewlet implementation.
 */
public interface ViewletPlacement
{
    /**
     * Obtain the name of this value.
     *
     * @return the name
     */
    public abstract String getName();
}
