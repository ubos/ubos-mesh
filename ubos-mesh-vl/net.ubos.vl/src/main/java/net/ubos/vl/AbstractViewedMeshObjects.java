//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.util.exception.NotSingleMemberException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Factors out functionality common to ViewedMeshObjects implementations.
 */
public abstract class AbstractViewedMeshObjects
        implements
            ViewedMeshObjects,
            CanBeDumped
{
    /**
     * Constructor. Initializes to empty content. After the constructor has
     * been called, the setViewlet method has to be invoked to tell the ViewedMeshObjects
     * about its Viewlet.
     */
    public AbstractViewedMeshObjects(
            ViewletPlacement      placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail )
    {
        theDefaultPlacement      = placement;
        theDefaultDimensionality = dimensionality;
        theDefaultDetail         = detail;
    }

    /**
     * Set the Viewlet to which this ViewedMeshObjects belongs.
     *
     * @param v the Viewlet that we belong to.
     */
    public void setViewlet(
            Viewlet v )
    {
        theViewlet = v;
    }

    /**
     * Through this method, the Viewlet that this object belongs to updates this object.
     *
     * @param subject the new subject of the Viewlet
     * @param viewletParameters the parameters of the Viewlet, if any
     */
    public void update(
            MeshObject            subject,
            ViewletPlacement      placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            Map<String,Object[]>  viewletParameters )
    {
        theSubject           = subject;
        thePlacement         = placement      == null ? theDefaultPlacement      : placement;
        theDimensionality    = dimensionality == null ? theDefaultDimensionality : dimensionality;
        theDetail            = detail         == null ? theDefaultDetail         : detail;
        theViewletParameters = viewletParameters;

        theMeshObjectsToView = null; // must be null if this method was invoked
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateFrom(
            MeshObjectsToView newObjectsToView )
    {
        // FIXME: should take actual capabilities into account, not just what's being requested
        ViewletPlacement      placement      = newObjectsToView.getRequiredViewletPlacement();
        ViewletDimensionality dimensionality = newObjectsToView.getRequiredViewletDimensionality();
        if( dimensionality == null ) {
            dimensionality = newObjectsToView.getRecommendedViewletDimensionality();
        }
        ViewletDetail detail = newObjectsToView.getRequiredViewletDetail();
        if( detail == null ) {
            detail = newObjectsToView.getRecommendedViewletDetail();
        }

        update( newObjectsToView.getSubject(),
                placement,
                dimensionality,
                detail,
                newObjectsToView.getViewletParameters() );

        theMeshObjectsToView = newObjectsToView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MeshObject getSubject()
    {
        return theSubject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Viewlet getViewlet()
    {
        return theViewlet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletPlacement getViewletPlacement()
    {
        return thePlacement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDimensionality getViewletDimensionality()
    {
        return theDimensionality;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDetail getViewletDetail()
    {
        return theDetail;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getViewletParameter(
            String name )
        throws
            NotSingleMemberException
    {
        if( theViewletParameters == null ) {
            return null;
        }

        Object [] ret = theViewletParameters.get( name );
        if( ret == null ) {
            return null;
        }
        switch( ret.length ) {
            case 0:
                return null;

            case 1:
                return ret[0];

            default:
                throw new NotSingleMemberException( "Parameter '" + name + "' has more than one value", ret.length );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getViewletParameter(
            String name,
            Object defaultValue )
        throws
            NotSingleMemberException
    {
        Object ret = getViewletParameter( name );
        if( ret == null ) {
            ret = defaultValue;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getMultivaluedViewletParameter(
            String name )
    {
        if( theViewletParameters == null ) {
            return null;
        }

        Object [] ret = theViewletParameters.get( name );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Map<String,Object[]> getViewletParameters()
    {
        return theViewletParameters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectsToView getMeshObjectsToView()
    {
        return theMeshObjectsToView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String [] {
                        "subject",
                        "vl",
                        "meshObjectsToView"
                },
                new Object [] {
                        theSubject,
                        theViewlet,
                        theMeshObjectsToView
        } );
    }

    /**
     * The Viewlet that this object belongs to.
     */
    protected Viewlet theViewlet;

    /**
     * The current subject of the Viewlet.
     */
    protected MeshObject theSubject;

    /**
     * The default
     */
    protected final ViewletPlacement theDefaultPlacement;

    /**
     * The default
     */
    protected final ViewletDimensionality theDefaultDimensionality;

    /**
     * The default
     */
    protected final ViewletDetail theDefaultDetail;

    /**
     *
     */
    protected ViewletPlacement thePlacement;

    /**
     *
     */
    protected ViewletDimensionality theDimensionality;

    /**
     *
     */
    protected ViewletDetail theDetail;

    /**
     * The Viewlet parameters, if any.
     */
    protected Map<String,Object[]> theViewletParameters;

    /**
     * The MeshObjectsToView object that was received by the Viewlet, leading to this
     * ViewedMeshObjects.
     */
    protected MeshObjectsToView theMeshObjectsToView;
}
