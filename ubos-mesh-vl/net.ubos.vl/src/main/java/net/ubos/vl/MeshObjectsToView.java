//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.util.exception.NotSingleMemberException;
import net.ubos.util.logging.CanBeDumped;

/**
 * Objects supporting this interface are being used to tell a Viewlet which
 * MeshObjects it is supposed to view, in which context, with which parameters etc.
 *
 * Instances are immutable. To change a value, create a new instance with one
 * of the copy-modification methods.
 */
public interface MeshObjectsToView
        extends
            CanBeDumped
{
    /**
     * Obtain the subject that the Viewlet is supposed to view.
     *
     * @return the subject
     */
    public MeshObject getSubject();

    /**
     * Obtain the time in history when the subject is supposed to be viewed.
     *
     * @return the time in history. Long.MAX_VALUE indicates the HEAD version
     */
    public long getWhen();

    /**
     * Obtain the name of the Viewlet that must be used. This may be null.
     *
     * @return the name of the Viewlet
     */
    public String getRequiredViewletName();

    /**
     * Obtain the name of the Viewlet that should be used. This may be null.
     *
     * @return the name of the Viewlet
     */
    public String getRecommendedViewletName();

    /**
     * Obtain the placement that the Viewlet must support
     *
     * @return the placement
     */
    public ViewletPlacement getRequiredViewletPlacement();

    // There is no recommended placement, it makes no sense.

    /**
     * Obtain the dimensionality that the Viewlet must support
     *
     * @return the dimensionality
     */
    public ViewletDimensionality getRequiredViewletDimensionality();

    /**
     * Obtain the dimensionality that the Viewlet should (must is not required) support.
     *
     * @return the dimensionality
     */
    public ViewletDimensionality getRecommendedViewletDimensionality();

    /**
     * Obtain the detail that the Viewlet must support
     *
     * @return the detail
     */
    public ViewletDetail getRequiredViewletDetail();

    /**
     * Obtain the detail that the Viewlet should (must is not required) support.
     *
     * @return the detail
     */
    public ViewletDetail getRecommendedViewletDetail();

    /**
     * Obtain the value of a named Viewlet parameter.
     *
     * @param name the name of the Viewlet parameter
     * @return the value, if any
     * @throws NotSingleMemberException if a Viewlet parameter had more than one value
     */
    public Object getViewletParameter(
            String name )
        throws
            NotSingleMemberException;

    /**
     * Obtain all values of a multi-valued Viewlet parameter.
     *
     * @param name the name of the Viewlet parameter
     * @return the values, if any
     */
    public Object [] getMultivaluedViewletParameter(
            String name );

    /**
     * Obtain the parameters that the Viewlet is supposed to use.
     *
     * @return the parameters that the Viewlet is supposed to use
     */
    public Map<String,Object[]> getViewletParameters();

    /**
     * Create a copy of this object, but set an alternate subject.
     *
     * @param newSubject the new subject
     * @return new instance
     */
    public MeshObjectsToView withSubject(
            MeshObject newSubject );

    /**
     * Create a copy of this object, but set an alternate point in time for
     * the requested MeshObject from its MeshObjectHistory.
     *
     * @param atTime
     * @return new instance
     */
    public MeshObjectsToView withAtTime(
            long atTime );

    /**
     * Create a copy of this object, but use the most recent version of the MeshObject
     * from its MeshObjectHistory.
     *
     * @return new instance
     */
    public MeshObjectsToView withAtCurrentTime();

    /**
     * Create a copy of this object, but set the required Viewlet name to something else.
     *
     * @param newViewletName the new required Viewlet name, or null
     * @return new instance
     */
    public MeshObjectsToView withRequiredViewletName(
           String newViewletName );

    /**
     * Create a copy of this object, but set the recommended Viewlet name to something else.
     *
     * @param newViewletName the new recommended Viewlet name, or null
     * @return new instance
     */
    public MeshObjectsToView withRecommendedViewletName(
           String newViewletName );

    /**
     * Create a copy of this object, but add / change a required Viewlet placement.
     *
     * @param newValue the new value, or null
     * @return new instance
     */
    public MeshObjectsToView withRequiredViewletPlacement(
            ViewletPlacement newValue );

    /**
     * Create a copy of this object, but add / change a required Viewlet dimensionality.
     *
     * @param newValue the new value, or null
     * @return new instance
     */
    public MeshObjectsToView withRequiredViewletDimensionality(
            ViewletDimensionality newValue );

    /**
     * Create a copy of this object, but add / change a recommended Viewlet dimensionality.
     *
     * @param newValue the new value, or null
     * @return new instance
     */
    public MeshObjectsToView withRecommendedViewletDimensionality(
            ViewletDimensionality newValue );

    /**
     * Create a copy of this object, but add / change a required Viewlet detail.
     *
     * @param newValue the new value, or null
     * @return new instance
     */
    public MeshObjectsToView withRequiredViewletDetail(
            ViewletDetail newValue );

    /**
     * Create a copy of this object, but add / change a recommended Viewlet detail.
     *
     * @param newValue the new value, or null
     * @return new instance
     */
    public MeshObjectsToView withRecommendedViewletDetail(
            ViewletDetail newValue );

    /**
     * Create a copy of this object, but replace all values of a Viewlet parameter
     * with something else.
     *
     * @param name name of the Viewlet parameter
     * @param newValue new value of the Viewlet parameter
     * @return new instance
     */
    public MeshObjectsToView withViewletParameter(
            String name,
            Object newValue );

    /**
     * Create a copy of this object, but replace all values of a Viewlet parameter
     * with something else.
     *
     * @param name name of the Viewlet parameter
     * @param newValues new values of the Viewlet parameter
     * @return new instance
     */
    public MeshObjectsToView withViewletParameter(
            String    name,
            Object [] newValues );

    /**
     * Create a copy of this object, but remove all values of a Viewlet parameter
     *
     * @param name name of the Viewlet parameter
     * @return new instance
     */
    public MeshObjectsToView withoutViewletParameter(
            String name );

    /**
     * Create a copy of this object, but remove all values of all Viewlet parameters
     *
     * @return new instance
     */
    public MeshObjectsToView withoutViewletParameters();
}
