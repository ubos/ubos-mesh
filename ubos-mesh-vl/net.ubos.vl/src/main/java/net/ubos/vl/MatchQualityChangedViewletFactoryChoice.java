//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

/**
 * A ViewletFactoryChoice whose match quality has been overridden, but otherwise
 * delegates to another ViewletFactoryChoice.
 */
public class MatchQualityChangedViewletFactoryChoice
        implements
            ViewletFactoryChoice
{
    /**
     * Constructor.
     *
     * @param matchQuality the overridden match quality
     * @param delegate the underlying ViewletFactoryChoice
     */
    public MatchQualityChangedViewletFactoryChoice(
            double               matchQuality,
            ViewletFactoryChoice delegate )
    {
        theMatchQuality = matchQuality;
        theDelegate     = delegate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return theDelegate.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleName()
    {
        return theDelegate.getUserVisibleName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getMatchQuality()
    {
        return theMatchQuality;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Viewlet instantiateViewlet()
        throws
            CannotViewException
    {
        return theDelegate.instantiateViewlet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletPlacement getPlacement()
    {
        return theDelegate.getPlacement();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDimensionality getDimensionality()
    {
        return theDelegate.getDimensionality();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDetail getDetail()
    {
        return theDelegate.getDetail();
    }

    /**
     * The new value for the match quality.
     */
    protected final double theMatchQuality;

    /**
     * The underlying ViewletFactoryChoice that we override and delegate to.
     */
    protected final ViewletFactoryChoice theDelegate;
}
