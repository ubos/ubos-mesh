//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import net.ubos.mesh.MeshObject;
import net.ubos.util.exception.NotSingleMemberException;

import java.util.Map;

/**
 * A Subject MeshObject, plus information about its context, that is being viewed
 * by a Viewlet. It is similar to {@link MeshObjectsToView MeshObjectsToView},
 * but MeshObjectsToView is a specification looking for a good Viewlet, while
 * ViewedMeshObjects captures what a chosen Viewlet currently actually does.
 */
public interface ViewedMeshObjects
{
    /**
     * Obtain the subject of the Viewlet. As long as the Viewlet displays any
     * information whatsoever, this is non-null.
     *
     * @return the subject of the Viewlet
     */
    public MeshObject getSubject();

    /**
     * Obtain the Viewlet by which these MeshObjects are viewed.
     *
     * @return the Viewlet
     */
    public Viewlet getViewlet();

    /**
     * Determine the ViewletPlacement.
     *
     * @return the ViewletPlacement
     */
    public ViewletPlacement getViewletPlacement();

    /**
     * Determine the ViewletDimensionality.
     *
     * @return the ViewletDimensionality
     */
    public ViewletDimensionality getViewletDimensionality();

    /**
     * Determine the ViewletDetail.
     *
     * @return the ViewletDetail
     */
    public ViewletDetail getViewletDetail();

    /**
     * Obtain the parameters of the viewing Viewlet.
     *
     * @return the parameters of the viewing Viewlet. This may be null.
     */
    public Map<String,Object[]> getViewletParameters();

    /**
     * Obtain the value of a named Viewlet parameter.
     *
     * @param name the name of the Viewlet parameter
     * @return the value, if any
     * @throws NotSingleMemberException if a Viewlet parameter had more than one value
     */
    public Object getViewletParameter(
            String name )
        throws
            NotSingleMemberException;

    /**
     * Obtain the value of a named Viewlet parameter.
     *
     * @param name the name of the Viewlet parameter
     * @param defaultValue the default value
     * @return the value, if any
     * @throws NotSingleMemberException if a Viewlet parameter had more than one value
     */
    public Object getViewletParameter(
            String name,
            Object defaultValue )
        throws
            NotSingleMemberException;

    /**
     * Obtain all values of a multi-valued Viewlet parameter.
     *
     * @param name the name of the Viewlet parameter
     * @return the values, if any
     */
    public Object [] getMultivaluedViewletParameter(
            String name );

    /**
     * Obtain the MeshObjectsToView object that was received by the Viewlet, leading to this
     * ViewedMeshObjects.
     *
     * @return the MeshObjectsToView
     */
    public MeshObjectsToView getMeshObjectsToView();

    /**
     * Through this method, the Viewlet that this object belongs to updates this object.
     *
     * @param newObjectsToView the new objects accepted to be viewed by the Viewlet
     */
    public void updateFrom(
            MeshObjectsToView newObjectsToView );
}
