//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import net.ubos.mesh.MeshObject;
import net.ubos.util.ResourceHelper;
import net.ubos.util.factory.AbstractFactoryCreatedObject;

/**
 * Factors out commonly used functionality for Viewlets.
 */
public abstract class AbstractViewlet
        extends
            AbstractFactoryCreatedObject<MeshObjectsToView,Viewlet,Void>
        implements
            Viewlet
{
    /**
     * Constructor, for subclasses only.
     *
     * @param viewed the ViewedMeshObjects to use
     */
    protected AbstractViewlet(
            ViewedMeshObjects viewed )
    {
        theViewedMeshObjects = viewed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserVisibleName()
    {
        Class<?> clazz = getClass();
        String userVisibleName = ResourceHelper.getInstance( clazz ).getResourceStringOrDefault( "UserVisibleName", getName() );
        return userVisibleName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void view(
            MeshObjectsToView toView )
        throws
            CannotViewException
    {
        theViewedMeshObjects.updateFrom( toView );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getSubject()
    {
        return theViewedMeshObjects.getSubject();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewedMeshObjects getViewedMeshObjects()
    {
        return theViewedMeshObjects;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletFactory getFactory()
    {
        return (ViewletFactory) super.getFactory();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObjectsToView getFactoryKey()
    {
        return getViewedMeshObjects().getMeshObjectsToView();
    }

    /**
     * The objects being viewed.
     */
    protected final ViewedMeshObjects theViewedMeshObjects;
}
