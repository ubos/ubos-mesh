//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import net.ubos.mesh.MeshObject;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * A Viewlet whose content can be paged.
 *
 * @param <T> the type of the content that can be paged
 */
public interface PaginatingViewlet<T>
    extends
        Viewlet
{
    /**
     * Obtain an Iterator positioned on the first MeshObject in the current page,
     * iterating over the entire content.
     *
     * @return the location of the first MeshObject
     * @throws CannotViewException thrown if the paginating-specific URL arguments are invalid
     */
    public CursorIterator<T> getCurrentPageStartIterator()
        throws
            CannotViewException;

    /**
     * Obtain an Iterator positioned on the first first MeshObject in the current page,
     * iterating over the content in the current page.
     *
     * @return the location of the first MeshObject
     * @throws CannotViewException thrown if the paginating-specific URL arguments are invalid
     */
    public CursorIterator<T> getWithinPageIterator()
        throws
            CannotViewException;

    /**
     * Obtain the page size.
     *
     * @return the page size
     */
    public int getPageSize();
}
