//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;

/**
 * This ViewletFactory asks each known ViewletMatcher for a score, and picks the
 * Viewlet most appropriate.
 */
public class DefaultViewletFactory
    extends
        AbstractViewletFactory
{
    private static final Log log = Log.getLogInstance( DefaultViewletFactory.class ); // our own, private logger

    /**
     * Factory method.
     *
     * @return the created DefaultViewletFactory
     */
    public static DefaultViewletFactory create()
    {
        return new DefaultViewletFactory();
    }

    /**
     * Constructor.
     */
    protected DefaultViewletFactory()
    {}

    /**
     * {@inheritDoc}
     */
    @Override
    protected ViewletFactoryChoice [] determineAvailableViewletChoices(
            MeshObjectsToView theObjectsToView )
    {
        ArrayList<ViewletFactoryChoice> almost = new ArrayList<>( theViewletMatchers.size() );

        for( ViewletMatcher matcher : theViewletMatchers ) {
            ViewletFactoryChoice [] choices = matcher.match( theObjectsToView );
            if( choices != null ) {
                for( ViewletFactoryChoice choice : choices ) {
                    if( choice != null ) { // this should really not happen, but let's be defensive
                        almost.add( choice );
                    }
                }
            }
        }
        return ArrayHelper.copyIntoNewArray( almost, ViewletFactoryChoice.class );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerViewlet(
            ViewletMatcher matcher )
    {
        if( !theViewletMatchers.contains( matcher )) {
            theViewletMatchers.add( matcher );
        } else {
            log.error( "ViewletMatcher was added previously:", matcher );
        }
    }

    /**
     * The known ViewletMatchers
     */
    protected final Set<ViewletMatcher> theViewletMatchers = new HashSet<>();
}
