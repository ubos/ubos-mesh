//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

/**
 * Defines in how many dimensions the Viewlet renders content.
 */
public enum ViewletDimensionality
{
    DIM_2D,
    DIM_3D;

    /**
     * Obtain the name of this value.
     *
     * @return the name
     */
    public String getName()
    {
        return name().toLowerCase();
    }
}
