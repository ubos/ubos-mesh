//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

/**
 * This is the default implementation of ViewedMeshObjects. This is useful for
 * a wide variety of Viewlets.
 */
public class DefaultViewedMeshObjects
        extends
            AbstractViewedMeshObjects
{
    /**
     * Constructor. Initializes to empty content.
     */
    public DefaultViewedMeshObjects(
            ViewletPlacement      placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail )
    {
        super( placement, dimensionality, detail );
    }
}
