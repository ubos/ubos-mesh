//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.util.exception.NotSingleMemberException;
import net.ubos.util.logging.Dumper;

/**
 * Factors out functionality common to MeshObjectsToView implementations.
 */
public abstract class AbstractMeshObjectsToView
        implements
            MeshObjectsToView
{
    /**
     * Private constructor, use subclass.
     *
     * @param subject the subject for the Viewlet
     * @param when the time in the subject's MeshObjectHistory at which it shall be accessed, or MAX_LONG if current
     * @param requiredName the name of the Viewlet that must be used, if any
     * @param recommendedName the name of the Viewlet that should be used, if any
     * @param viewletParameters the Viewlet parameters (eg size, zoom, ...) to use
     */
    protected AbstractMeshObjectsToView(
            MeshObject            subject,
            long                  when,
            String                requiredName,
            String                recommendedName,
            ViewletPlacement      requiredPlacement,
            ViewletDimensionality requiredDimensionality,
            ViewletDimensionality recommendedDimensionality,
            ViewletDetail         requiredDetail,
            ViewletDetail         recommendedDetail,
            Map<String,Object[]>  viewletParameters )
    {
        if( subject == null ) {
            throw new NullPointerException( "Subject is null" );
        }
        theSubject                   = subject;
        theWhen                      = when;
        theRequiredName              = requiredName;
        theRecommendedName           = recommendedName;
        theRequiredPlacement         = requiredPlacement;
        theRequiredDimensionality    = requiredDimensionality;
        theRecommendedDimensionality = recommendedDimensionality;
        theRequiredDetail            = requiredDetail;
        theRecommendedDetail         = recommendedDetail;
        theViewletParameters         = viewletParameters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject getSubject()
    {
        return theSubject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getWhen()
    {
        return theWhen;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRequiredViewletName()
    {
        return theRequiredName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRecommendedViewletName()
    {
        return theRecommendedName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletPlacement getRequiredViewletPlacement()
    {
        return theRequiredPlacement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDimensionality getRequiredViewletDimensionality()
    {
        return theRequiredDimensionality;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDimensionality getRecommendedViewletDimensionality()
    {
        return theRecommendedDimensionality;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDetail getRequiredViewletDetail()
    {
        return theRequiredDetail;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletDetail getRecommendedViewletDetail()
    {
        return theRecommendedDetail;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getViewletParameter(
            String name )
        throws
            NotSingleMemberException
    {
        if( theViewletParameters == null ) {
            return null;
        }

        Object [] ret = theViewletParameters.get( name );
        if( ret == null ) {
            return null;
        }
        switch( ret.length ) {
            case 0:
                return null;

            case 1:
                return ret[0];

            default:
                throw new NotSingleMemberException( "Parameter name has more than one value", ret.length );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object [] getMultivaluedViewletParameter(
            String name )
    {
        if( theViewletParameters == null ) {
            return null;
        }

        Object [] ret = theViewletParameters.get( name );
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String,Object[]> getViewletParameters()
    {
        return theViewletParameters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "subject",
                    "when",
                    "requiredName",
                    "recommendedName",
                    "requiredPlacement",
                    "requiredDimensionality",
                    "recommendedDimensionality",
                    "requiredDetail",
                    "recommendedDetail",
                    "viewletPars"
                },
                new Object[] {
                    theSubject,
                    theWhen,
                    theRequiredName,
                    theRecommendedName,
                    theRequiredPlacement,
                    theRequiredDimensionality,
                    theRecommendedDimensionality,
                    theRequiredDetail,
                    theRecommendedDetail,
                    theViewletParameters
        });
    }

    /**
     * The subject to view.
     */
    protected final MeshObject theSubject;

    /**
     * When the time in the subject's MeshObjectHistory at which it shall be accessed, or MAX_LONG if current.
     */
    protected final long theWhen;

    /**
     * The required Viewlet name, if any.
     */
    protected final String theRequiredName;

    /**
     * The recommended Viewlet name, if any.
     */
    protected final String theRecommendedName;

    /**
     * The required Viewlet placement, if any.
     */
    protected final ViewletPlacement theRequiredPlacement;

    // There is no recommended placement; it doesn't make sense.

    /**
     * The required Viewlet dimensionality, if any.
     */
    protected final ViewletDimensionality theRequiredDimensionality;

    /**
     * The recommended Viewlet dimensionality, if any.
     */
    protected final ViewletDimensionality theRecommendedDimensionality;

    /**
     * The required Viewlet detail, if any.
     */
    protected final ViewletDetail theRequiredDetail;

    /**
     * The recommended Viewlet detail, if any.
     */
    protected final ViewletDetail theRecommendedDetail;

    /**
     * The parameters that we would like the Viewlet to use when viewing the selected objects.
     * This is multi-valued. Allocated as needed
     */
    protected final Map<String,Object[]> theViewletParameters;
}
