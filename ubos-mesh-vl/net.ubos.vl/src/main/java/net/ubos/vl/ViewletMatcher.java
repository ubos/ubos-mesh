//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

/**
 * Determines how will a particular Viewlet matches an incoming request.
 * The returned ViewletFactoryChoice contains quality information for the match
 * and allows the instantiation of the Viewlet.
 */
public interface ViewletMatcher
{
    /**
     * Given the MeshObjectsToView, how well does a particular Viewlet match?
     *
     * @param toView the incoming MeshObjectsToView
     * @return the ViewletFactoryChoices encompassing quality and acting as factory
     *         for the Viewlet
     */
    public ViewletFactoryChoice [] match(
            MeshObjectsToView toView );
}
