//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import net.ubos.mesh.MeshObject;
import net.ubos.util.factory.FactoryCreatedObject;

/**
 * <p>A software component of a user interface.
 *    Conceptually, the user interface consists of Viewlets  contained in some kind of container.
 * <p>A Viewlet typically has a subject, which is given as a <code>MeshObject</code>. For example,
 *    a Viewlet showing an electronic business card might have the owner of the business card
 *    as the subject.
 * <p>The <code>Viewlet</code> interface is supported by all Viewlets. More specific
 *    subtypes are provided for specific implementation technologies.
 */
public interface Viewlet
    extends
        FactoryCreatedObject<MeshObjectsToView,Viewlet,Void>
{
    /**
      * Obtain a String, to be shown to the user, that identifies this Viewlet to the user.
      *
      * @return a String
      */
    public String getUserVisibleName();

    /**
     * Obtain the computable name of the Viewlet.
     *
     * @return the Viewet's name
     */
    public abstract String getName();

    /**
      * The Viewlet is being instructed to view certain objects, which are packaged as
      * {@link MeshObjectsToView MeshObjectsToView}.
      *
      * @param toView the MeshObjects to view
      * @throws CannotViewException thrown if this Viewlet cannot view these MeshObjectsToView
      */
    public void view(
            MeshObjectsToView toView )
        throws
            CannotViewException;

    /**
     * Obtain the REST-ful subject. This is a convenience method as it only delegates to
     * the ViewedMeshObjects.
     *
     * @return the subject
     */
    public MeshObject getSubject();

    /**
      * Obtain the MeshObjects that this Viewlet is currently viewing, plus
      * context information. This method will return the same instance of ViewedMeshObjects
      * during the lifetime of the Viewlet, but potentially with different values.
      *
      * @return the ViewedMeshObjects
      */
    public ViewedMeshObjects getViewedMeshObjects();

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewletFactory getFactory();
}
