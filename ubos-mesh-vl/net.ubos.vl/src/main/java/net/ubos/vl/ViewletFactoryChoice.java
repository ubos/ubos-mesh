//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

/**
 * <p>A choice for instantiating a Viewlet by a ViewletFactory. See
 *    description at {@link ViewletFactory ViewletFactory}.
 */
public interface ViewletFactoryChoice
{
    /**
     * Obtain the computable name of the Viewlet.
     *
     * @return the Viewlet's name
     */
    public abstract String getName();

    /**
     * Obtain the user-visible name of the Viewlet.
     *
     * @return the Viewlet's user-visible name
     */
    public abstract String getUserVisibleName();

    /**
     * Obtain the Viewlet placement.
     *
     * @return the Viewlet placement.
     */
    public abstract ViewletPlacement getPlacement();

    /**
     * Obtain the Viewlet dimensionality.
     *
     * @return the Viewlet dimensionality
     */
    public abstract ViewletDimensionality getDimensionality();

    /**
     * Obtain the Viewlet detail.
     *
     * @return the Viewlet detail
     */
    public abstract ViewletDetail getDetail();

    /**
     * Obtain a measure of the match quality. 0 means &quot;perfect match&quot;,
     * while larger numbers mean increasingly worse match quality.
     *
     * @return the match quality
     */
    public abstract double getMatchQuality();

    /**
     * Instantiate a ViewletFactoryChoice into a Viewlet. The caller still must call
     * {Viewlet#view Viewlet.view} after having called
     * this method.
     *
     * @return the instantiated Viewlet
     * @throws CannotViewException if, against expectations, the Viewlet corresponding
     *         to this ViewletFactoryChoice could not view the MeshObjectsToView after
     *         all. This usually indicates a programming error.
     */
    public abstract Viewlet instantiateViewlet()
        throws
            CannotViewException;

    /**
     * User-selected match quality, expressing "the user wanted this one".
     */
    public static final double USER_SELECTED_MATCH_QUALITY = 0.0;

    /**
     * Best match quality, expressing "perfect match".
     */
    public static final double PERFECT_MATCH_QUALITY = 1.0;

    /**
     * Good match quality, expressing "not perfect but better than average".
     */
    public static final double GOOD_MATCH_QUALITY = 10.0;

    /**
     * Default match quality if none was given.
     */
    public static final double AVERAGE_MATCH_QUALITY = 100.0;

    /**
     * Bad match quality, expressing "worse than average".
     */
    public static final double BAD_MATCH_QUALITY = 1000.0;

    /**
     * Match quality so bad, it is only used as a last resort
     */
    public static final double FALLBACK_MATCH_QUALITY = 10000.0;

    /**
     * Worst match quality, expressing "there is a match, but it is very bad."
     */
    public static final double WORST_MATCH_QUALITY = Double.MAX_VALUE;
}
