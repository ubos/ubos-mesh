//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import net.ubos.util.factory.Factory;
import net.ubos.util.factory.FactoryException;

/**
 * <p>A factory for Viewlets. Objects supporting this interface not only support the
 *    regular {@link net.ubos.util.factory.Factory Factory} pattern, but also method
 *    {@link #determineViewletChoices determineFactoryChoices}.
 * <p>One can best visualize the purpose of this method in terms of a user interface:
 *    the obtained {@link ViewletFactoryChoice ViewletFactoryChoices} could
 *    be displayed as the choices the user has in executing the creation method of
 *    a factory. For example, a given MeshObject could be displayed using a PropertySheet
 *    or a custom Viewlet; either of those could be displayed in the current window
 *    or in a new window. In this example, 4 ViewletFactoryChoices would be returned,
 *    representing the 2x2 choices the user has to display the given MeshObject.
 *    The ViewletFactoryChoices are clearly distinct from the results of executing
 *    the factory method directly: they act as a mediator of some kind that allows
 *    the user to make a more informed choice about how to instantiate a Viewlet
 *    using the ViewletFactory.
 */
public interface ViewletFactory
        extends
            Factory<MeshObjectsToView,Viewlet,Void>
{
    /**
     * Factory method. This is inherited from the <code>Factory</code> interface, but
     * repeated here for clarity. This will instantiate the best ViewletFactoryChoice
     * found by the ViewletFactory.
     *
     * @param key the MeshObjectsToView with this Viewlet
     * @param argument any argument-style information required for object creation, if any
     * @return the created Viewlet
     * @throws FactoryException catch-all Exception, consider its cause
     */
    @Override
    public abstract Viewlet obtainFor(
            MeshObjectsToView key,
            Void              argument )
        throws
            FactoryException;

    /**
     * Find the ViewletFactoryChoices that apply to these MeshObjectsToView.
     *
     * @param theObjectsToView the MeshObjectsToView
     * @return the found ViewletFactoryChoices, if any
     */
    public ViewletFactoryChoice [] determineViewletChoices(
            MeshObjectsToView theObjectsToView );

    /**
     * Find the ViewletFactoryChoices that apply to these MeshObjectsToView, and
     * return them in order of match quality, with the best match being first.
     *
     * @param theObjectsToView the MeshObjectsToView
     * @return the found ViewletFactoryChoices, if any
     */
    public ViewletFactoryChoice [] determineOrderedViewletChoices(
            MeshObjectsToView theObjectsToView );

    /**
     * Find the best ViewletFactoryChoice that applies to these MeshObjectsToView.
     *
     * @param theObjectsToView the MeshObjectsToView
     * @return the found ViewletFactoryChoice, or null
     */
    public ViewletFactoryChoice determineBestViewletChoice(
            MeshObjectsToView theObjectsToView );

    /**
     * Register a new Viewlet by way of a ViewletMatcher.
     *
     * @param matcher the ViewletMatcher
     */
    public void registerViewlet(
            ViewletMatcher matcher );
}
