//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

import net.ubos.util.exception.AbstractLocalizedException;
import net.ubos.util.exception.LocalizedException;
import net.ubos.util.factory.FactoryException;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * No Viewlet could be found that could view the provided MeshObjectsToView.
 */
public class NoViewletFoundException
        extends
            FactoryException
        implements
            LocalizedException,
            CanBeDumped
{
    /**
     * Constructor.
     *
     * @param sender the Factory that threw this exception
     * @param o which MeshObjectsToView could not be viewed
     */
    public NoViewletFoundException(
            ViewletFactory    sender,
            MeshObjectsToView o )
    {
        super( sender );

        theObjectsToView = o;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dump(
            Dumper d )
    {
        d.dump( this,
                new String[] {
                    "objectsToView"
                },
                new Object[] {
                    theObjectsToView
        } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getLocalizationParameters()
    {
        return new Object[]{};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalizedMessage()
    {
        return AbstractLocalizedException.assembleDefaultLocalizedMessage( this );
    }

    /**
     * The MeshObjectsToView that could not be viewed.
     */
    protected final MeshObjectsToView theObjectsToView;

    /**
     * The default entry in the resource files, prefixed by the StringRepresentation's prefix.
     */
    public static final String DEFAULT_ENTRY = "String";

    /**
     * The default entry in the resource files for the case where no ViewletType has
     * been specified, prefixed by the StringRepresentation's prefix.
     */
    public static final String DEFAULT_NO_VIEWLET_TYPE_ENTRY = DEFAULT_ENTRY + "NoViewletType";

    /**
     * The default entry in the resource files for the case where a ViewletType has
     * been specified, prefixed by the StringRepresentation's prefix.
     */
    public static final String DEFAULT_VIEWLET_TYPE_ENTRY = DEFAULT_ENTRY + "ViewletType";
}
