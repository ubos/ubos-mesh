//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.vl;

/**
 * Defines the amount of detail that a Viewlet shows.
 */
public enum ViewletDetail
{
    OVERVIEW,
    DETAIL,
    NORMAL;

    /**
     * Obtain the name of this value.
     *
     * @return the name
     */
    public String getName()
    {
        return name().toLowerCase();
    }
}
