//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.converter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import net.ubos.importer.Importer;
import net.ubos.importer.ImporterException;
import net.ubos.importer.ImporterScore;
import net.ubos.mesh.BracketMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.externalized.json.DefaultMeshBaseJsonImporter;
import net.ubos.meshbase.externalized.json.MeshBaseJsonEncoder;
import net.ubos.meshbase.externalized.xml.DefaultMeshBaseXmlImporter;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.util.logging.Log;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleClassLoader;
import org.diet4j.core.ModuleMeta;
import org.diet4j.core.ModuleNotFoundException;
import org.diet4j.core.ModuleRegistry;
import org.diet4j.core.ModuleRequirement;
import org.diet4j.core.ModuleResolutionException;

/**
 * Main program.
 */

public class Converter
{
    /**
     * main() method.
     *
     * @param args command-line arguments passed in by diet4j
     * @return desired exit code
     * @throws ModuleActivationException A Module could not be activated
     * @throws ModuleNotFoundException a Module could not be found
     * @throws ModuleResolutionException a Module could not be resolved
     */
    public static int main(
            String [] args )
        throws
            ModuleActivationException,
            ModuleNotFoundException,
            ModuleResolutionException
    {
        ArgumentParser argParser = ArgumentParsers.newFor( "ubos-mesh-converter" ).build()
                .defaultHelp( true )
                .description( "Convert an external data file into UBOS Data Mesh format." );

        argParser.addArgument( "--input" )
                .required( true )
                .help( "Name of the source personal data export file" );

        argParser.addArgument( "--output" )
                .required( true )
                .help( "Name of the destination " + EXT + " file" );

        argParser.addArgument( "--importer" )
                .required( false )
                .help( "Only try importers whose identifier matches this regular expression" );

        argParser.addArgument( "--namespace" )
                .required( false )
                .help( "Use this as the primary identifier namespace" );

        argParser.addArgument( "--explain" )
                .required( false )
                .setDefault( false )
                .action(Arguments.storeTrue())
                .help( "Show an explanation for why a particular importer was chosen, or none was found" );

        argParser.addArgument( "--list" )
                .required( false )
                .setDefault( false )
                .action(Arguments.storeTrue())
                .help( "Only list the available importers" );

        argParser.addArgument( "--analyze" )
                .required( false )
                .setDefault( false )
                .action(Arguments.storeTrue())
                .help( "Analyze the input file with respect to the importers' expectations" );

        argParser.addArgument( "--statistics" )
                .required( false )
                .setDefault( false )
                .action(Arguments.storeTrue())
                .help( "Report statistics about the imported data" );


        Namespace ns;
        try {
            ns = argParser.parseArgs( args );

        } catch( ArgumentParserException ex ) {
            argParser.handleError( ex );
            return 1;
        }

        // find parser modules
        ModuleRegistry    registry            = ((ModuleClassLoader)Converter.class.getClassLoader()).getModuleRegistry();
        ModuleRequirement importerRequirement = new ModuleRequirement.Builder().requiredCapability( "ubos-mesh-importer" ).build();
        ModuleMeta []     importerMetas       = registry.determineResolutionCandidates( importerRequirement );
        Module []         importerModules     = new Module[ importerMetas.length ];

        for( int i=0 ; i<importerMetas.length ; ++i ) {
            importerModules[i] = registry.resolve( importerMetas[i] );
            importerModules[i].activateRecursively();
        }

        // determine which works the best
        List<Importer> allImporters = new ArrayList<>();

        allImporters.add(DefaultMeshBaseXmlImporter.create());
        allImporters.add(DefaultMeshBaseJsonImporter.create());

        for( int i=0 ; i<importerModules.length ; ++i ) {
            Object contextObject = importerModules[i].getContextObject();
            if( contextObject != null ) {
                if( contextObject instanceof Importer ) {
                    allImporters.add( (Importer) contextObject );

                } else if( contextObject instanceof Importer [] ) {
                    for( Importer current : (Importer []) contextObject ) {
                        allImporters.add( current );
                    }
                }
            }
        }

        int ret;
        if( ns.getBoolean( "list" )) {
            ret = doList( allImporters );

        } else {
            File    sourceFile      = new File( ns.getString( "input" ));  // we are certain it exists
            File    destFile        = new File( ns.getString( "output" )); // we are certain it exists
            String  importerRegex   = ns.getString( "importer" );
            Pattern importerPattern = importerRegex != null && !importerRegex.isBlank() ? Pattern.compile( importerRegex ) : null;

            if( !sourceFile.canRead() ) {
                return fatal( "Cannot read source file: " + sourceFile.getAbsolutePath() );
            }

            List<Importer> selectedImporters = new ArrayList<>();
            if( importerPattern != null ) {
                for( Importer importer : allImporters ) {
                    if( importerPattern.matcher( importer.getIdentifier()).matches() ) {
                        selectedImporters.add( importer );
                    }
                }
            } else {
                selectedImporters.addAll( allImporters );
            }

            if( ns.getBoolean( "analyze" )) {
                ret = doAnalyze( sourceFile, selectedImporters );

            } else {
                String namespace = ns.getString( "namespace" );
                if( namespace == null ) {
                    namespace = sourceFile.getName();
                    int lastPeriod = namespace.lastIndexOf( '.' );
                    if( lastPeriod > 0 ) {
                        namespace = namespace.substring( 0, lastPeriod );
                    }
                }
                ret = doImport( sourceFile, destFile, selectedImporters, namespace, ns.getBoolean( "explain" ), ns.getBoolean( "statistics" ) );
            }
        }

        return ret;
    }

    /**
     * List mode.
     *
     * @param allImporters the Importers to consider
     */
    protected static int doList(
            List<Importer> allImporters )
    {
        int maxNameLength = 16; // for label
        int maxIdLength   = 10; // for label
        for( Importer current : allImporters ) {
            maxNameLength = Integer.max( maxNameLength, current.getName().length());
            maxIdLength   = Integer.max( maxIdLength,   current.getIdentifier().length());
        }

        String space  = "  ";
        String format = "%-" + maxNameLength + "s" + space + "%-" + maxIdLength + "s\n";
        System.out.printf( format, "Name of importer", "Identifier" );
        for( int i=0 ; i<maxNameLength + maxIdLength + space.length() ; ++i ) {
            System.out.print( '=' );
        }
        System.out.println();

        for( Importer current : allImporters ) {
            System.out.printf( format, current.getName(), current.getIdentifier());
        }
        return 0;
    }

    /**
     * Analyze mode.
     *
     * @param sourceFile the source of the import
     * @param selectedImporters the Importers to consider
     * @return desired exit code
     */
    protected static int doAnalyze(
            File           sourceFile,
            List<Importer> selectedImporters )
    {
        Map<Importer,AnalyzeRecord> analyzeRecords = new HashMap<>();

        for( Importer importer : selectedImporters ) {
            AnalyzeRecord analyzeRecord = new AnalyzeRecord();
            analyzeRecords.put( importer, analyzeRecord );

            try {
                analyzeRecord.theReport = importer.analyze( sourceFile );

            } catch( Throwable t ) {
                analyzeRecord.theProblem = t;
            }
        }
        for( Importer importer : selectedImporters ) {
            AnalyzeRecord record = analyzeRecords.get( importer );

            System.out.println( "=== Importer: " + importer.getName() + " (" + importer.getIdentifier() + ") ===" );
            if( record.isEmpty() ) {
                System.out.println( "<not available>" );
            } else {
                System.out.println( record.getReport() );
            }
        }
        return 0;
    }

    /**
     * Import mode.
     *
     * @param sourceFile the source of the import
     * @param destFile the destination to write the imported data to
     * @param selectedImporters the Importers to consider
     * @param namespace the default namespace to use, if any
     * @param explain if true, explain why the converter did what it did
     * @param statistics if true, emit some conversion statistics
     * @return desired exit code
     */
    protected static int doImport(
            File           sourceFile,
            File           destFile,
            List<Importer> selectedImporters,
            String         namespace,
            boolean        explain,
            boolean        statistics )
    {
        Map<Importer,ImportRecord> importRecords = new HashMap<>();

        final Comparator<Importer> BY_SCORE_COMPARATOR =
                (Importer a, Importer b) -> {
                    ImporterScore as = importRecords.get( a ).theScore;
                    ImporterScore bs = importRecords.get( b ).theScore;

                    return -ImporterScore.compareTo( as, bs ); // invert

                };

        // Let's not share MeshBases and Primary Namespace Maps between importing attempts

        for( Importer importer : selectedImporters ) {
            ImportRecord importRecord = new ImportRecord();
            importRecords.put( importer, importRecord );

            try {
                importRecord.thePrimaryNsMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();
                importRecord.theMeshBase     = MMeshBase.Builder.create().namespaceMap( importRecord.thePrimaryNsMap ).normalizeChangeList( false ).build();

                importRecord.theScore = importer.importTo( sourceFile, namespace, importRecord.theMeshBase );

            } catch( Throwable t ) {
                importRecord.theProblem = t;
            }
        }

        // sort the importers by score -- hightest to lowest

        selectedImporters.sort( BY_SCORE_COMPARATOR );

        ImportRecord bestRecord = null;
        for( Importer current : selectedImporters ) {
            ImportRecord currentRecord = importRecords.get( current );

            if( currentRecord.theScore != null ) {
                bestRecord = currentRecord;
                break;
            }
        }

        if( explain ) {
            int maxNameLength  = 16; // for label
            int maxIdLength    = 10; // for label
            int maxScore       =  5; // for label
            int maxExplanation = 11; // for label

            for( Importer current : selectedImporters ) {
                ImportRecord record = importRecords.get( current );
                if( record == null ) {
                    continue;
                }
                maxNameLength = Integer.max( maxNameLength, current.getName().length());
                maxIdLength   = Integer.max( maxIdLength,   current.getIdentifier().length());

                if( record.getExplanation() != null ) {
                    maxExplanation = Integer.max( maxExplanation, record.getExplanation().length() );
                }
            }

            String space  = "  ";
            String formatWithScore    = "%1s"   + space + "%-" + maxNameLength + "s" + space + "%-" + maxIdLength + "s" + space + "%1.3f"                + space + "%-" + maxExplanation + "s\n";
            String formatWithoutScore = " " + space + "%-" + maxNameLength + "s" + space + "%-" + maxIdLength + "s" + space + "%" + maxScore + "s"   + space + "%-" + maxExplanation + "s\n";

            System.out.printf( formatWithoutScore, "Name of importer", "Identifier", "Score", "Explanation" );
            for( int i=0 ; i < 1 + maxNameLength + maxIdLength + maxScore + maxExplanation + 4*space.length() ; ++i ) {
                System.out.print( '=' );
            }
            System.out.println();

            for( Importer current : selectedImporters ) {
                ImportRecord record = importRecords.get( current );

                if( record == null ) {
                    continue;

                } else if( record.theScore == null ) {
                    System.out.printf(
                            formatWithoutScore,
                            current.getName(),
                            current.getIdentifier(),
                            "-",
                            record.getExplanation() );


                } else {
                    System.out.printf(
                            formatWithScore,
                            bestRecord == record ? "*" : "",
                            current.getName(),
                            current.getIdentifier(),
                            record.theScore.getScore(),
                            record.getExplanation());
                }

                if( record.theProblem != null ) {
                    info( record.theProblem );
                }
            }
        }

        if( bestRecord == null ) {
            return fatal( "Import failed, no suitable importer found" );
        }

        if( statistics ) {
            statistics( bestRecord );
        }

        MContextualMeshObjectIdentifierNamespaceMap exportNsMap = MContextualMeshObjectIdentifierNamespaceMap.create( bestRecord.thePrimaryNsMap );

        try( Writer w = new BufferedWriter( new FileWriter( destFile, StandardCharsets.UTF_8 ))) {
            MeshBaseJsonEncoder encoder = MeshBaseJsonEncoder.create();
            encoder.bulkWrite( bestRecord.theMeshBase, exportNsMap, w );

        } catch( IOException ex ) {
            error( "The destination file could not be written: " + destFile.getAbsolutePath(), ex );

        } catch( EncodingException ex ) {
            error( "An encoding problem occurred when trying to write: " + destFile.getAbsolutePath() );
        }
        return 0;
    }

    static void statistics(
            ImportRecord record )
    {
        MeshBase mb = record.theMeshBase;

        BracketMeshObjectIdentifierBothSerializer ser = BracketMeshObjectIdentifierBothSerializer.create( record.thePrimaryNsMap, mb );

        Map<EntityType,Integer> entityTypes      = new HashMap<>();
        Set<MeshObject>         notBlessed       = new HashSet<>();
        ExtremeNeighbors        extremeNeighbors = new ExtremeNeighbors();

        for( MeshObject current : mb ) {
            EntityType [] blessedWith = current.getEntityTypes();
            if( blessedWith.length == 0 ) {
                notBlessed.add( current );
            } else {
                for( EntityType type : blessedWith ) {
                    increment( entityTypes, type );
                }
            }
            extremeNeighbors.put( current );
        }

        List<EntityType> sortedEntityTypes = new ArrayList<>( entityTypes.size() );
        sortedEntityTypes.addAll( entityTypes.keySet() );
        Collections.sort( sortedEntityTypes, (a, b) -> - Integer.compare( entityTypes.get( a ), entityTypes.get( b ))); // most first

        //
        System.out.println( "Statistics report for best importer:" );
        System.out.println( "    MeshObjects: " + mb.size() );

        if( sortedEntityTypes.isEmpty() ) {
            System.out.println( "    No EntityTypes used" );

        } else {
            System.out.println( "    Most used EntityTypes:" );
            for( int i=0; i<5 && i<sortedEntityTypes.size() ; ++i ) {
                EntityType current = sortedEntityTypes.get( i );
                System.out.println( "        " + current.getName().value() + ": " + entityTypes.get( current ));
            }
            System.out.println( "    MeshObjects not blessed: " + notBlessed.size() );
        }

        System.out.println( "Most connected MeshObjects:" );
        for( MeshObject current : extremeNeighbors.most() ) {
            System.out.println( "    " + ser.toExternalForm( current.getIdentifier()) + ": " + current.traverseToNeighbors().size() );

            RoleType [] rts = current.getRoleTypes();
            if( rts.length > 0 ) {
                System.out.println( "        Most used RoleTypes:" );

                int      [] rtCounts = new int[ rts.length ];
                Integer  [] index    = new Integer[ rts.length ];
                for( int i=0 ; i<rts.length ; ++i ) {
                    index[i] = i;
                    rtCounts[i] = current.traverse( rts[i] ).size();
                }
                Arrays.sort( index, (Integer a, Integer b) -> - Integer.compare( rtCounts[ index[a]], rtCounts[ index[b] ] ));

                for( int i=0 ; i<index.length && i<5 ; ++i ) {
                    System.out.println( "            " + rts[index[i]].getName().value() + ": " + rtCounts[index[i]]);
                }
            }
            int unblessedRelationships = 0;
            for( MeshObject neighbor : current.traverseToNeighbors() ) {
                if( current.getRoleTypes( neighbor ).length == 0 ) {
                    ++unblessedRelationships;
                }
            }
            System.out.println( "        Unblessed relationships with neighbors: " + unblessedRelationships );
        }

        System.out.println( "Least connected MeshObjects:" );
        for( MeshObject current : extremeNeighbors.least() ) {
            System.out.println( "    " + ser.toExternalForm( current.getIdentifier()) + ": " + current.traverseToNeighbors().size() );
        }
    }

    static <T> void increment(
            Map<T,Integer> map,
            T              item )
    {
        Integer count = map.get( item );
        if( count == null ) {
            count = 1;
        } else {
            count = count+1;
        }
        map.put( item, count );
    }

    /**
     * Factored-out fatal method.
     *
     * @param args the message and parameters
     * @return the desired exit code
     */
    static int fatal(
            Object ... args )
    {
        Log.getLogInstance( Converter.class ).fatal( args );
        return 1;
    }

    /**
     * Factored-out error method
     *
     * @param args the message and parameters
     */
    static void error(
            Object ... args )
    {
        Log.getLogInstance( Converter.class ).error( args );
    }

    /**
     * Factored-out warn method
     *
     * @param args the message and parameters
     */
    static void warn(
            Object ... args )
    {
        Log.getLogInstance( Converter.class ).warn( args );
    }

    /**
     * Factored-out info method
     *
     * @param args the message and parameters
     */
    static void info(
            Object ... args )
    {
        Log.getLogInstance( Converter.class ).info( args );
    }

    /**
     * Default file extension.
     */
    public static final String EXT = ".mesh";

    /**
     * Collects all data related to an attempted analyze.
     */
    static class AnalyzeRecord
    {
        public String    theReport;
        public Throwable theProblem;

        public boolean isEmpty()
        {
            return theReport == null && theProblem == null;
        }

        /**
         * Construct a suitable report.
         */
        public String getReport()
        {
            if( theReport != null ) {
                return theReport;

            } else {
                Throwable realProblem = theProblem;

                while( true ) {
                    if( realProblem instanceof ImporterException ) {
                        return realProblem.getMessage();

                    } else {
                        if( realProblem.getMessage() == null ) {
                            return realProblem.getClass().getName();
                        } else {
                            return realProblem.getMessage();
                        }
                    }
                }

            }
        }
    }

    /**
     * Collects all data related to an attempted import.
     */
    static class ImportRecord
    {
        public PrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;
        public EditableHistoryMeshBase                 theMeshBase;
        public ImporterScore                           theScore;
        public Throwable                               theProblem;

        /**
         * Construct a suitable message representing the problem, if any.
         */
        public String getExplanation()
        {
            if( theProblem == null ) {
                if( theScore.getExplanation() == null ) {
                    return "";
                } else {
                    return theScore.getExplanation();
                }
            } else {
                Throwable realProblem = theProblem;

                while( true ) {
                    if( realProblem instanceof ImporterException ) {
                        return realProblem.getMessage();

                    } else {
                        if( realProblem.getMessage() == null ) {
                            return realProblem.getClass().getName();
                        } else {
                            return realProblem.getMessage();
                        }
                    }
                }

            }
        }
    }

    static class ExtremeNeighbors
    {
        public void put(
                MeshObject toAdd )
        {
            int n = toAdd.traverseToNeighbors().size();

            boolean addedMost = false;
            for( int i=0 ; i<theMost.size() ; ++i ) {
                MeshObject current = theMost.get( i );
                if( n > current.traverseToNeighbors().size() ) {
                    theMost.add( i, toAdd );
                    addedMost = true;
                    break;
                }
            }
            if( !addedMost ) {
                theMost.add( toAdd );
            }
            while( theMost.size() > MAX_MOST ) {
                theMost.remove( theMost.size()-1 );
            }

            boolean addedLeast = false;
            for( int i=0 ; i<theLeast.size() ; ++i ) {
                MeshObject current = theMost.get( i );
                if( n < current.traverseToNeighbors().size() ) {
                    theLeast.add( i, toAdd );
                    addedLeast = true;
                    break;
                }
            }
            if( !addedLeast ) {
                theLeast.add( toAdd );
            }
            while( theLeast.size() > MAX_LEAST ) {
                theLeast.remove( theLeast.size()-1 );
            }
        }


        public List<MeshObject> most()
        {
            return theMost;
        }
        public List<MeshObject> least()
        {
            return theLeast;
        }

        public static final int MAX_MOST  = 5;
        public static final int MAX_LEAST = 5;

        /**
         *
         */
        protected List<MeshObject> theMost  = new ArrayList<>();
        protected List<MeshObject> theLeast = new ArrayList<>();
    }

}
