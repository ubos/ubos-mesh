//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.generic;

import java.io.IOException;
import net.ubos.importer.handler.AbstractMultiHandlerImporter;
import net.ubos.importer.handler.file.DefaultFileImporterHandler;
import net.ubos.importer.handler.csv.DefaultCsvImporterHandler;
import net.ubos.importer.handler.directory.DefaultDirectoryImporterHandler;
import net.ubos.importer.handler.json.DefaultJsonImporterHandler;
import net.ubos.importer.handler.zip.DefaultZipImporterHandler;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.importer.handler.zip.NoOpZipDirectoryImporterHandler;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.history.EditableHistoryMeshBase;

/**
 * The fallback generic importer if no other importer is available.
 * It creates a tree of MeshObjects from any hierarchy information that it can find (if a directory in the
 * file system is the source, or entries in a zip file, or a hierarchy inside a JSON file), reads all files
 * that aren't ZIP, JSON or CSV as blobs, and stores everything as weakly typed Attributes and RoleAttributes.
 */
public class GenericImporter
    extends
        AbstractMultiHandlerImporter
{
    /**
     * Factory method.
     *
     * @return the Importer.
     */
    public static GenericImporter create()
    {
        return new GenericImporter();
    }

    /**
     * Constructor.
     */
    protected GenericImporter()
    {
        super( "Generic importer", HANDLERS, null );
    }

    /**
     * The ImporterContentHandlers for this Importer
     */
    public static final ImporterHandler [] HANDLERS = {
        new DefaultDirectoryImporterHandler(), // build the directory structure first
        new DefaultZipImporterHandler(),       // build that structure next

        new NoOpZipDirectoryImporterHandler( ImporterHandler.UNRATED, false ),
        new DefaultJsonImporterHandler( ImporterHandler.FALLBACK ),
        new DefaultCsvImporterHandler( ImporterHandler.FALLBACK ),

        new DefaultFileImporterHandler( ImporterHandler.FALLBACK )
    };
}
