{
  meshObject( meshObjectId: "default#workspace" ) {
    meshObjectId,

    appearsInSpace: traverse( traversalSpec: "net.ubos.model.Space/Object_AppearsIn_Space-D" ) {
      ... on Neighbor {
        reachedMeshObject {
          meshObjectId,
          entityTypeIds,
          properties {
            propertyTypeId,
            propertyValue {
              ... on StringValue {
                value
              }
            }
          },
          isInCategory : traverse( traversalSpec: "net.ubos.model.Marketing/Advertiser_IsIn_AdvertiserCategory-S" ) {
            reachedMeshObjectId
          },
          categoryContains : traverse( traversalSpec: "net.ubos.model.Marketing/Advertiser_IsIn_AdvertiserCategory-D" ) {
            reachedMeshObjectId
          }
        }
      }
    }
  }
}
