# space-and-contained.md

Compare with the example query at [space-and-contained.txt](space-and-contained.txt).

## Example data

In this example, we want to represent this:

* There's a space/workbench

* In the space, we have 4 objects of type `AmazonAdvertiser`: Benetton, Dior, Levi's and Zara. (Not actual advertisers
  as far as I know)

* We have two objects of type `Industry Category`: Large company, and medium company.

* We have two objects of type `Advertiser Location`: based in Europe, and based in the Americas.

* Some of the `AmazonAdvertisers` are in some of the categories, e.g. Benetton is based in Europe, and medium-sized.

Therefore, our MeshBase for this example contains:

* The home object (always there, not relevant for this example)

* Representing Dior: MeshObject with MeshObjectId `default#adv1`, blessed with MeshTypeId `net.ubos.model.Amazon/AmazonAdvertiser`,
  and values for some properties, like the name is Dior (MeshTypeId `net.ubos.model.Marketing/Advertiser_Name`)

* Representing the "large company" category: MeshObject with MeshObjectId `default#cat-large`, blessed with
  MeshTypeId `net.ubos.model.Marketing/IndustryCategory` and name "Large company"
  (MeshTypeId `net.ubos.model.Marketing/AdvertiserCategory_Name`)

* Similarly, the other three advertisers and other categories.

* Note that according to [the model](../../ubos-mesh-model-library/net.ubos.model.Marketing/src/main/models/model.xml),
  `Industry Category` and `Advertiser Location` are both subtypes of `Advertiser Category`.

* There are relationships between some of the advertisers and some of the categories, which are blessed with
  RelationshipType "IsIn" (MeshTypeId: `net.ubos.model.Marketing/Advertiser_IsIn_AdvertiserCategory-S` in one
  direction and `net.ubos.model.Marketing/Advertiser_IsIn_AdvertiserCategory-D` the other)

* A MeshObject that represents the space/workbench with MeshobjectId `default#workspace`.

* Relationships between `default#workspace` and the objects in the space, which are all these advertisers and
  categories. The RelationshipType is "appearsIn" (MeshTypeId `net.ubos.model.Space/Object_AppearsIn_Space`)

## Example query

We want to obtain the data necessary to draw the space. Our start object is the space, i.e. MeshObject with
MeshObjectId `default#workspace`.

```
{
  meshObject( meshObjectId: "default#workspace" ) {
     ...
  }
```

From there, we need to find the MeshObjects are appear in the space, so we traverse the relationship
that relates the space to the things that appear in the space. Its MeshTypeId is `net.ubos.model.Space/Object_AppearsIn_Space`
and we need to traverse it in the opposite direction (from the space to the object, not the object to the space). So
we use the `-D` MeshTypeId: `net.ubos.model.Space/Object_AppearsIn_Space-D` for the traversal:

```
    appearsInSpace: traverse( traversalSpec: "net.ubos.model.Space/Object_AppearsIn_Space-D" ) {
        ...
    }
```

This gives us a bunch of data. We are just intererested in the MeshObjects that we reached:

```
        reachedMeshObject {
            ...
        }
```

For each of those, we want to know its identifier and what type(s) it is blessed with: we need to know the types
so we can render each object appropriately, as the returned set will return both Advertisers and Categories.
Note: in this example, only MeshObjects of these types will be returned. In the real world, all sorts of other
types of MeshObjects may show up: whatever the user put into the space.

```
          meshObjectId,
          entityTypeIds,
```

We want all list of properties on those objects, but only values that are of type string (that's to keep the
example simpler):

```
          properties {
            propertyTypeId,
            propertyValue {
              ... on StringValue {
                value
              }
            }
          },
```

Note that properties are identified by their MeshTypeId, like all other "type-level" information, such as
EntityTypes and RelationshipTypes.

And finally, we want to know what MeshObjects have been categorized how. We do this by traversing
the relevant RelationshipType in both directions:

```
          isInCategory : traverse( traversalSpec: "net.ubos.model.Marketing/Advertiser_IsIn_AdvertiserCategory-S" ) {
            reachedMeshObjectId
          },
          categoryContains : traverse( traversalSpec: "net.ubos.model.Marketing/Advertiser_IsIn_AdvertiserCategory-D" ) {
            reachedMeshObjectId
          }
```

Note that this is overkill (but it does not hurt): None of the objects participates in these relationships
on both directions, and of course Advertisers are not Categories, and Categories not Advertisers, so some
of these traversals cannot possibly produce a result. But the way UBOS Mesh works, this is harmless; it will
simply provide an empty set. It also makes the query much simpler.

## Example query response with the data described above:

Note that some properties defined in the model have no assigned values in the data set used. For example,
ZipCode.

```
{
  "data": {
    "meshObject": {
      "meshObjectId": "default#workspace",
      "appearsInSpace": [
        {
          "reachedMeshObject": {
            "meshObjectId": "default#cat-eu",
            "entityTypeIds": [
              "net.ubos.model.Marketing/AdvertiserLocation"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/AdvertiserCategory_Name",
                "propertyValue": {
                  "value": "Europe"
                }
              }
            ],
            "isInCategory": [],
            "categoryContains": [
              {
                "reachedMeshObjectId": "default#adv1"
              },
              {
                "reachedMeshObjectId": "default#adv2"
              },
              {
                "reachedMeshObjectId": "default#adv4"
              }
            ]
          }
        },
        {
          "reachedMeshObject": {
            "meshObjectId": "default#cat-medium",
            "entityTypeIds": [
              "net.ubos.model.Marketing/IndustryCategory"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/AdvertiserCategory_Name",
                "propertyValue": {
                  "value": "Medium company"
                }
              }
            ],
            "isInCategory": [],
            "categoryContains": [
              {
                "reachedMeshObjectId": "default#adv3"
              }
            ]
          }
        },
        {
          "reachedMeshObject": {
            "meshObjectId": "default#cat-am",
            "entityTypeIds": [
              "net.ubos.model.Marketing/AdvertiserLocation"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/AdvertiserCategory_Name",
                "propertyValue": {
                  "value": "Americas"
                }
              }
            ],
            "isInCategory": [],
            "categoryContains": [
              {
                "reachedMeshObjectId": "default#adv3"
              }
            ]
          }
        },
        {
          "reachedMeshObject": {
            "meshObjectId": "default#adv4",
            "entityTypeIds": [
              "net.ubos.model.Amazon/AmazonAdvertiser"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Name",
                "propertyValue": {
                  "value": "Zara Group"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress2"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_City",
                "propertyValue": {
                  "value": "Arteixo"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_ZipCode"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_State"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Country",
                "propertyValue": {
                  "value": "Spain"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_PrivacyRequestUrl"
              }
            ],
            "isInCategory": [
              {
                "reachedMeshObjectId": "default#cat-eu"
              },
              {
                "reachedMeshObjectId": "default#cat-large"
              }
            ],
            "categoryContains": []
          }
        },
        {
          "reachedMeshObject": {
            "meshObjectId": "default#adv3",
            "entityTypeIds": [
              "net.ubos.model.Amazon/AmazonAdvertiser"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Name",
                "propertyValue": {
                  "value": "Levi\u0027s"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress2"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_City",
                "propertyValue": {
                  "value": "San Francisco"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_ZipCode"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_State"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Country",
                "propertyValue": {
                  "value": "USA"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_PrivacyRequestUrl"
              }
            ],
            "isInCategory": [
              {
                "reachedMeshObjectId": "default#cat-medium"
              },
              {
                "reachedMeshObjectId": "default#cat-am"
              }
            ],
            "categoryContains": []
          }
        },
        {
          "reachedMeshObject": {
            "meshObjectId": "default#adv1",
            "entityTypeIds": [
              "net.ubos.model.Amazon/AmazonAdvertiser"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Name",
                "propertyValue": {
                  "value": "Dior"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress2"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_City",
                "propertyValue": {
                  "value": "Paris"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_ZipCode"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_State"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Country",
                "propertyValue": {
                  "value": "France"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_PrivacyRequestUrl"
              }
            ],
            "isInCategory": [
              {
                "reachedMeshObjectId": "default#cat-eu"
              },
              {
                "reachedMeshObjectId": "default#cat-large"
              }
            ],
            "categoryContains": []
          }
        },
        {
          "reachedMeshObject": {
            "meshObjectId": "default#adv2",
            "entityTypeIds": [
              "net.ubos.model.Amazon/AmazonAdvertiser"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Name",
                "propertyValue": {
                  "value": "Benetton Group"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_StreetAddress2"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_City",
                "propertyValue": {
                  "value": "Ponzano Veneto"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_ZipCode"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_State"
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_Country",
                "propertyValue": {
                  "value": "Italy"
                }
              },
              {
                "propertyTypeId": "net.ubos.model.Marketing/Advertiser_PrivacyRequestUrl"
              }
            ],
            "isInCategory": [
              {
                "reachedMeshObjectId": "default#cat-eu"
              }
            ],
            "categoryContains": []
          }
        },
        {
          "reachedMeshObject": {
            "meshObjectId": "default#cat-large",
            "entityTypeIds": [
              "net.ubos.model.Marketing/IndustryCategory"
            ],
            "properties": [
              {
                "propertyTypeId": "net.ubos.model.Marketing/AdvertiserCategory_Name",
                "propertyValue": {
                  "value": "Large company"
                }
              }
            ],
            "isInCategory": [],
            "categoryContains": [
              {
                "reachedMeshObjectId": "default#adv4"
              },
              {
                "reachedMeshObjectId": "default#adv1"
              }
            ]
          }
        }
      ]
    }
  }
}
```