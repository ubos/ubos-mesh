{
  meshObject( meshObjectId: "" ) {
    meshObjectId,

    neighbors {
      neighbor {
        meshObjectId
      }
    }
  }
}
