//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.graphql;

import java.io.IOException;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import org.diet4j.core.ModuleDeactivationException;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Module initializer class.
     */
public class ModuleInit
{
    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-graphql";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        Underbars.registerPathDelegate(
                PATH,
                GraphQlHandler.create() );

        try {
            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

            Underbars.registerAssets( resourceManager.getAssetMap() );

            resourceManager.checkResources();

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }

    public static void moduleDeactivate(
            Module thisModule )
        throws
            ModuleDeactivationException
    {
        Underbars.unregisterPathDelegate( PATH );
    }

    /**
     * The path.
     */
    public static final String PATH = "/g/graphql";
}
