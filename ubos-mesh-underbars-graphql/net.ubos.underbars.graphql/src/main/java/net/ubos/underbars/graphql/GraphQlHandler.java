//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.graphql;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.TypeResolutionEnvironment;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import graphql.schema.idl.TypeRuntimeWiring;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import java.io.File;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.Attribute;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.Property;
import net.ubos.mesh.RoleAttribute;
import net.ubos.mesh.RoleProperty;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.modelbase.MeshTypeWithIdentifierNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.graphql.fetchers.MeshObjectFetcher;
import net.ubos.underbars.util.UndertowHttpRequest;

/**
 * Implements a GraphQL endpoint.
 */
public class GraphQlHandler
    implements
        HttpHandler
{
    /**
     * Factory method.
     *
     * @return the created instance
     */
    public static GraphQlHandler create()
    {
        return new GraphQlHandler();
    }

    /**
     * Private constructor, use factory method.
     */
    protected GraphQlHandler()
    {
        SchemaParser           schemaParser           = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse( new File( SCHEMA_FILE ));

        MeshBase mb = Underbars.getMeshBaseNameServer().getDefaultMeshBase();

        theIdSerializer = ExternalNameHashMeshObjectIdentifierBothSerializer.create(
                mb.getPrimaryNamespaceMap(), mb );

        theTypeIdSerializer = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type( TypeRuntimeWiring.newTypeWiring( "PropertyValue" )
                        .typeResolver( (TypeResolutionEnvironment tre) -> {
                            PropertyValue value = (PropertyValue) tre.getObject();
                            if( value == null ) {
                                return null;
                            }
                            String            shortName = value.getClass().getSimpleName();
                            GraphQLObjectType ret       = tre.getSchema().getObjectType( shortName );
                            return ret;
                        }).build()
                )
                .type( TypeRuntimeWiring.newTypeWiring( "BlobValue" )
                        .dataFetcher( "mimeType", (DataFetchingEnvironment dfe) -> {
                            BlobValue value = (BlobValue) dfe.getSource();
                            return value == null ? null : value.getMimeType();
                        }) // String
                        .dataFetcher( "value", (DataFetchingEnvironment dfe) -> {
                            BlobValue value = (BlobValue) dfe.getSource();
                            return value == null || !value.getHasTextMimeType() ? null : value.getAsString();
                        }) // String
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            BlobValue value = (BlobValue) dfe.getSource();
                            return value == null ? null : value.getAsString(); // FIXME
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "BooleanValue" )
                        .dataFetcher( "value", (DataFetchingEnvironment dfe) -> {
                            BooleanValue value = (BooleanValue) dfe.getSource();
                            return value == null ? null : value.value();
                        }) // Boolean
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            BooleanValue value = (BooleanValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "ColorValue" )
                        .dataFetcher( "r", (DataFetchingEnvironment dfe) -> {
                            ColorValue value = (ColorValue) dfe.getSource();
                            return value == null ? null : value.getRed();
                        }) // Int
                        .dataFetcher( "g", (DataFetchingEnvironment dfe) -> {
                            ColorValue value = (ColorValue) dfe.getSource();
                            return value == null ? null : value.getGreen();
                        }) // int
                        .dataFetcher( "b", (DataFetchingEnvironment dfe) -> {
                            ColorValue value = (ColorValue) dfe.getSource();
                            return value == null ? null : value.getBlue();
                        }) // Int
                        .dataFetcher( "a", (DataFetchingEnvironment dfe) -> {
                            ColorValue value = (ColorValue) dfe.getSource();
                            return value == null ? null : value.getAlpha();
                        }) // int
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            ColorValue value = (ColorValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "CurrencyValue" )
                        .dataFetcher( "positive", (DataFetchingEnvironment dfe) -> {
                            CurrencyValue value = (CurrencyValue) dfe.getSource();
                            return value == null ? null : value.isPositive();
                        }) // Boolean
                        .dataFetcher( "wholes", (DataFetchingEnvironment dfe) -> {
                            CurrencyValue value = (CurrencyValue) dfe.getSource();
                            return value == null ? null : value.getWholes();
                        }) // Int
                        .dataFetcher( "fractions", (DataFetchingEnvironment dfe) -> {
                            CurrencyValue value = (CurrencyValue) dfe.getSource();
                            return value == null ? null : value.getFractions();
                        }) // Int
                        .dataFetcher( "unit", (DataFetchingEnvironment dfe) -> {
                            CurrencyValue value = (CurrencyValue) dfe.getSource();
                            return value == null ? null : value.getUnit().getSymbol();
                        }) // String
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            CurrencyValue value = (CurrencyValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "EnumeratedValue" )
                        .dataFetcher( "value", (DataFetchingEnvironment dfe) -> {
                            EnumeratedValue value = (EnumeratedValue) dfe.getSource();
                            return value == null ? null : value.value();
                        }) // String
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            EnumeratedValue value = (EnumeratedValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "FloatValue" )
                        .dataFetcher( "value", (DataFetchingEnvironment dfe) -> {
                            FloatValue value = (FloatValue) dfe.getSource();
                            return value == null ? null : value.value();
                        }) // Float
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            FloatValue value = (FloatValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "IntegerValue" )
                        .dataFetcher( "value", (DataFetchingEnvironment dfe) -> {
                            IntegerValue value = (IntegerValue) dfe.getSource();
                            return value == null ? null : value.value();
                        }) // Int
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            IntegerValue value = (IntegerValue) dfe.getSource();
                            return value == null ? null : value.value();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "MultiplicityValue" )
                        .dataFetcher( "min", (DataFetchingEnvironment dfe) -> {
                            MultiplicityValue value = (MultiplicityValue) dfe.getSource();
                            return value == null ? null : value.getMinimum();
                        }) // Int
                        .dataFetcher( "max", (DataFetchingEnvironment dfe) -> {
                            MultiplicityValue value = (MultiplicityValue) dfe.getSource();
                            return value == null ? null : value.getMaximum();
                        }) // Int
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            MultiplicityValue value = (MultiplicityValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "PointValue" )
                        .dataFetcher( "x", (DataFetchingEnvironment dfe) -> {
                            PointValue value = (PointValue) dfe.getSource();
                            return value == null ? null : value.getX();
                        }) // Float
                        .dataFetcher( "y", (DataFetchingEnvironment dfe) -> {
                            PointValue value = (PointValue) dfe.getSource();
                            return value == null ? null : value.getY();
                        }) // Float
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            PointValue value = (PointValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "StringValue" )
                        .dataFetcher( "value", (DataFetchingEnvironment dfe) -> {
                            StringValue value = (StringValue) dfe.getSource();
                            return value == null ? null : value.value();
                        }) // String
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            StringValue value = (StringValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "TimePeriodValue" )
                        .dataFetcher( "year",   (DataFetchingEnvironment dfe) -> {
                            TimePeriodValue value = (TimePeriodValue) dfe.getSource();
                            return value == null ? null : value.getYear();
                        }) // Int
                        .dataFetcher( "month",  (DataFetchingEnvironment dfe) -> {
                            TimePeriodValue value = (TimePeriodValue) dfe.getSource();
                            return value == null ? null : value.getMonth();
                        }) // Int
                        .dataFetcher( "day",    (DataFetchingEnvironment dfe) -> {
                            TimePeriodValue value = (TimePeriodValue) dfe.getSource();
                            return value == null ? null : value.getDay();
                        }) // Int
                        .dataFetcher( "hour",   (DataFetchingEnvironment dfe) -> {
                            TimePeriodValue value = (TimePeriodValue) dfe.getSource();
                            return value == null ? null : value.getHour();
                        }) // Int
                        .dataFetcher( "minute", (DataFetchingEnvironment dfe) -> {
                            TimePeriodValue value = (TimePeriodValue) dfe.getSource();
                            return value == null ? null : value.getMinute();
                        }) // Int
                        .dataFetcher( "second", (DataFetchingEnvironment dfe) -> {
                            TimePeriodValue value = (TimePeriodValue) dfe.getSource();
                            return value == null ? null : value.getSecond();
                        }) // Float
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            TimePeriodValue value = (TimePeriodValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "TimeStampValue" )
                        .dataFetcher( "asString", (DataFetchingEnvironment dfe) -> {
                            TimeStampValue value = (TimeStampValue) dfe.getSource();
                            return value == null ? null : value.toString();
                        }) // String
                        // FIXME
                )

                .type( TypeRuntimeWiring.newTypeWiring( "Property" )
                        .dataFetcher( "meshObject", (DataFetchingEnvironment dfe) -> {
                            Property prop = (Property) dfe.getSource();
                            return prop.getMeshObject();
                        }) // MeshObject
                        .dataFetcher( "propertyTypeId", (DataFetchingEnvironment dfe) -> {
                            Property prop = (Property) dfe.getSource();
                            return theTypeIdSerializer.toExternalForm( prop.getPropertyType().getIdentifier());
                        }) // PropertyType
                        .dataFetcher( "propertyValue", (DataFetchingEnvironment dfe) -> {
                            Property prop = (Property) dfe.getSource();
                            return prop.getValue();
                        }) // PropertyValue
                )

                .type( TypeRuntimeWiring.newTypeWiring( "Attribute" )
                        .dataFetcher( "meshObject", (DataFetchingEnvironment dfe) -> {
                            Attribute att = (Attribute) dfe.getSource();
                            return att.getMeshObject();
                        }) // MeshObject
                        .dataFetcher( "attributeName", (DataFetchingEnvironment dfe) -> {
                            Attribute att = (Attribute) dfe.getSource();
                            return att.getAttributeName();
                        }) // String
                        .dataFetcher( "attributeValue", (DataFetchingEnvironment dfe) -> {
                            Attribute att = (Attribute) dfe.getSource();
                            return att.getValue() == null ? null : String.valueOf( att.getValue()); // FIXME
                        }) // String
                )

                .type( TypeRuntimeWiring.newTypeWiring( "RoleProperty" )
                        .dataFetcher( "meshObject", (DataFetchingEnvironment dfe) -> {
                            RoleProperty prop = (RoleProperty) dfe.getSource();
                            return prop.getMeshObject();
                        }) // MeshObject
                        .dataFetcher( "neighbor", (DataFetchingEnvironment dfe) -> {
                            RoleProperty prop = (RoleProperty) dfe.getSource();
                            return prop.getNeighbor();
                        }) // MeshObject
                        .dataFetcher( "propertyTypeId", (DataFetchingEnvironment dfe) -> {
                            RoleProperty prop = (RoleProperty) dfe.getSource();
                            return theTypeIdSerializer.toExternalForm( prop.getRolePropertyType().getIdentifier());
                        }) // String
                        .dataFetcher( "propertyValue", (DataFetchingEnvironment dfe) -> {
                            RoleProperty prop = (RoleProperty) dfe.getSource();
                            return prop.getValue();
                        }) // PropertyValue
                )

                .type( TypeRuntimeWiring.newTypeWiring( "RoleAttribute" )
                        .dataFetcher( "meshObject", (DataFetchingEnvironment dfe) -> {
                            RoleAttribute prop = (RoleAttribute) dfe.getSource();
                            return prop.getMeshObject();
                        }) // MeshObject
                        .dataFetcher( "neighbor", (DataFetchingEnvironment dfe) -> {
                            RoleProperty prop = (RoleProperty) dfe.getSource();
                            return prop.getNeighbor();
                        }) // MeshObject
                        .dataFetcher( "attributeName", (DataFetchingEnvironment dfe) -> {
                            RoleAttribute att = (RoleAttribute) dfe.getSource();
                            return att.getRoleAttributeName();
                        }) // String
                        .dataFetcher( "attributeValue", (DataFetchingEnvironment dfe) -> {
                            RoleAttribute att = (RoleAttribute) dfe.getSource();
                            return att.getValue() == null ? null : String.valueOf( att.getValue()); // FIXME
                        }) // String
                )
                .type( TypeRuntimeWiring.newTypeWiring( "TraverseResult" )
                        .typeResolver( (TypeResolutionEnvironment tre) -> {
                            // FIXME
                            GraphQLObjectType ret = tre.getSchema().getObjectType( "Neighbor" );
                            return ret;
                        }).build()
                )
                .type( TypeRuntimeWiring.newTypeWiring( "Neighbor" )
                        .dataFetcher( "meshObject", (DataFetchingEnvironment dfe) -> {
                            Neighbor n = (Neighbor) dfe.getSource();
                            return n.theMeshObject;
                        })
                        .dataFetcher( "meshObjectId", (DataFetchingEnvironment dfe) -> {
                            Neighbor n = (Neighbor) dfe.getSource();
                            return theIdSerializer.toExternalForm( n.theMeshObject.getIdentifier() );
                        })
                        .dataFetcher( "reachedMeshObject", (DataFetchingEnvironment dfe) -> {
                            Neighbor n = (Neighbor) dfe.getSource();
                            return n.theNeighbor;
                        })
                        .dataFetcher( "reachedMeshObjectId", (DataFetchingEnvironment dfe) -> {
                            Neighbor n = (Neighbor) dfe.getSource();
                            return theIdSerializer.toExternalForm( n.theNeighbor.getIdentifier() );
                        })
                        .dataFetcher( "neighbor", (DataFetchingEnvironment dfe) -> {
                            Neighbor n = (Neighbor) dfe.getSource();
                            return n.theNeighbor;
                        })
                        .dataFetcher( "neighborId", (DataFetchingEnvironment dfe) -> {
                            Neighbor n = (Neighbor) dfe.getSource();
                            return theIdSerializer.toExternalForm( n.theNeighbor.getIdentifier() );
                        })
                        .dataFetcher( "traversalSpec", (DataFetchingEnvironment dfe) -> {
                            Neighbor n = (Neighbor) dfe.getSource();
                            return theTypeIdSerializer.toExternalForm( n.theTraverseSpec.getIdentifier() );
                        })
                        .dataFetcher( "roleTypeIds", (DataFetchingEnvironment dfe) -> {
                            Neighbor    n      = (Neighbor) dfe.getSource();
                            RoleType [] almost = n.theMeshObject.getRoleTypes( n.theNeighbor );
                            return typeIdsOf( almost );
                        })
                        .dataFetcher( "roleProperties",     (DataFetchingEnvironment dfe) -> {
                            Neighbor        n   = (Neighbor) dfe.getSource();
                            RoleProperty [] ret = n.theMeshObject.getRoleProperties( n.theNeighbor );
                            return ret;
                        }) // Array of RoleProperty
                        .dataFetcher( "rolePropertyTypeIds", (DataFetchingEnvironment dfe) -> {
                            Neighbor        n      = (Neighbor) dfe.getSource();
                            PropertyType [] almost = n.theMeshObject.getRolePropertyTypes( n.theNeighbor );
                            return typeIdsOf( almost );
                        })
                        .dataFetcher( "rolePropertyValue",  (DataFetchingEnvironment dfe) -> {
                            try {
                                Neighbor n                = (Neighbor) dfe.getSource();
                                String   propTypeIdString = dfe.getArgument( "propertyTypeId" );

                                PropertyValue ret = n.theMeshObject.getRolePropertyValue( n.theNeighbor, ModelBase.SINGLETON.findPropertyType( propTypeIdString ));
                                return ret;

                            } catch( ParseException | MeshTypeWithIdentifierNotFoundException ex ) {
                                throw new RuntimeException( ex );
                            }
                        }) // PropertyValue
                        .dataFetcher( "rolePropertyValues",  (DataFetchingEnvironment dfe) -> {
                            try {
                                Neighbor        n                 = (Neighbor) dfe.getSource();
                                String []       propTypeIdStrings = dfe.getArgument( "propertyTypeIds" );
                                PropertyType [] propTypes         = new PropertyType[ propTypeIdStrings.length ];

                                for( int i=0 ; i<propTypeIdStrings.length ; ++i ) {
                                    propTypes[i] = ModelBase.SINGLETON.findPropertyType( propTypeIdStrings[i] );
                                }
                                PropertyValue [] ret = n.theMeshObject.getRolePropertyValues( n.theNeighbor, propTypes );
                                return ret;

                            } catch( ParseException | MeshTypeWithIdentifierNotFoundException ex ) {
                                throw new RuntimeException( ex );
                            }
                        }) // Array of PropertyValue
                        .dataFetcher( "roleAttributes", (DataFetchingEnvironment dfe) -> {
                            Neighbor         n   = (Neighbor) dfe.getSource();
                            RoleAttribute [] ret = n.theMeshObject.getRoleAttributes( n.theNeighbor );
                            return ret;
                        }) // Array of RoleAttribute
                        .dataFetcher( "roleAttributeNames", (DataFetchingEnvironment dfe) -> {
                            Neighbor  n   = (Neighbor) dfe.getSource();
                            String [] ret = n.theMeshObject.getRoleAttributeNames( n.theNeighbor );
                            return ret;
                        }) // Array of String
                        .dataFetcher( "roleAttributeValue", (DataFetchingEnvironment dfe) -> {
                            Neighbor     n   = (Neighbor) dfe.getSource();
                            String       an  = dfe.getArgument( "attributeName" );
                            Serializable ret = n.theMeshObject.getRoleAttributeValue( n.theNeighbor, an );
                            return ret == null ? ret : String.valueOf( ret ); // FIXME

                        }) // String
                        .dataFetcher( "roleAttributeValues", (DataFetchingEnvironment dfe) -> {
                            Neighbor        n      = (Neighbor) dfe.getSource();
                            String []       ans    = dfe.getArgument( "attributeNames" );
                            Serializable [] almost =  n.theMeshObject.getRoleAttributeValues( n.theNeighbor, ans );
                            String []       ret    = new String[ ans.length ];

                            for( int i=0 ; i<almost.length ; ++i ) {
                                ret[i] = almost[i] == null ? null : String.valueOf( almost[i] ); // FIXME
                            }
                            return ret;
                        }) // Array of String
                )
                .type( TypeRuntimeWiring.newTypeWiring( "MeshObject" )
                        .dataFetcher( "meshObjectId", (DataFetchingEnvironment dfe) -> {
                            MeshObject obj = (MeshObject) dfe.getSource();
                            return theIdSerializer.toExternalForm( obj.getIdentifier() );
                        }) // String
                        .dataFetcher( "entityTypeIds", (DataFetchingEnvironment dfe) -> {
                            MeshObject obj = (MeshObject) dfe.getSource();
                            return typeIdsOf( obj.getEntityTypes() );
                        }) // Array of String
                        .dataFetcher( "properties", (DataFetchingEnvironment dfe) -> {
                            MeshObject  obj = (MeshObject) dfe.getSource();
                            Property [] ret = obj.getProperties();
                            return ret;
                        }) // Array of Property
                        .dataFetcher( "propertyTypeIds", (DataFetchingEnvironment dfe) -> {
                            MeshObject  obj = (MeshObject) dfe.getSource();
                            return typeIdsOf( obj.getPropertyTypes());
                        }) // Array of String
                        .dataFetcher( "propertyValue", (DataFetchingEnvironment dfe) -> {
                            try {
                                MeshObject obj              = (MeshObject) dfe.getSource();
                                String     propTypeIdString = dfe.getArgument( "propertyTypeId" );

                                PropertyValue ret = obj.getPropertyValue( ModelBase.SINGLETON.findPropertyType( propTypeIdString ));
                                return ret;

                            } catch( ParseException | MeshTypeWithIdentifierNotFoundException ex ) {
                                throw new RuntimeException( ex );
                            }
                        }) // PropertyValue
                        .dataFetcher( "propertyValues", (DataFetchingEnvironment dfe) -> {
                            try {
                                MeshObject      obj               = (MeshObject) dfe.getSource();
                                String []       propTypeIdStrings = dfe.getArgument( "propertyTypeIds" );
                                PropertyType [] propertyTypes     = new PropertyType[ propTypeIdStrings.length ];

                                for( int i=0 ; i<propTypeIdStrings.length ; ++i ) {
                                    propertyTypes[i] = ModelBase.SINGLETON.findPropertyType( propTypeIdStrings[i] );
                                }

                                PropertyValue [] ret = obj.getPropertyValues( propertyTypes );
                                return ret;

                            } catch( ParseException | MeshTypeWithIdentifierNotFoundException ex ) {
                                throw new RuntimeException( ex );
                            }
                        }) // Array of PropertyValue
                        .dataFetcher( "attributes", (DataFetchingEnvironment dfe) -> {
                            MeshObject   obj = (MeshObject) dfe.getSource();
                            Attribute [] ret = obj.getAttributes();
                            return ret;
                        }) // Array of Attribute
                        .dataFetcher( "attributeNames", (DataFetchingEnvironment dfe) -> {
                            MeshObject obj = (MeshObject) dfe.getSource();
                            return obj.getAttributeNames();
                        }) // Array of String
                        .dataFetcher( "attributeValue", (DataFetchingEnvironment dfe) -> {
                            MeshObject   obj = (MeshObject) dfe.getSource();
                            String       an  = dfe.getArgument( "attributeName" );
                            Serializable ret = obj.getAttributeValue( an );
                            return ret == null ? ret : String.valueOf( ret ); // FIXME
                        }) // String
                        .dataFetcher( "attributeValues", (DataFetchingEnvironment dfe) -> {
                            MeshObject      obj    = (MeshObject) dfe.getSource();
                            String []       ans    = dfe.getArgument( "attributeNames" );
                            Serializable [] values = obj.getAttributeValues( ans );
                            String       [] ret    = new String[ values.length ];
                            for( int i=0 ; i<values.length ; ++i ) {
                                ret[i] = values[i] == null ? null : String.valueOf( values[i] ); //FIXME
                            }
                            return ret;
                        }) // Array of String
                        .dataFetcher( "neighbors", (DataFetchingEnvironment dfe) -> {
                            MeshObject    obj       = (MeshObject) dfe.getSource();
                            MeshObject [] neighbors = obj.traverseToNeighbors().getMeshObjects();
                            Neighbor   [] ret       = new Neighbor[ neighbors.length ];

                            for( int i=0 ; i<neighbors.length ; ++i ) {
                                ret[i] = new Neighbor( obj, null, neighbors[i] );
                            }
                            return ret;
                        }) // Array of Neighbor
                        .dataFetcher( "neighborIds", (DataFetchingEnvironment dfe) -> {
                            MeshObject    obj       = (MeshObject) dfe.getSource();
                            MeshObject [] neighbors = obj.traverseToNeighbors().getMeshObjects();
                            String     [] ret       = new String[ neighbors.length ];

                            for( int i=0 ; i<neighbors.length ; ++i ) {
                                ret[i] = theIdSerializer.toExternalForm( neighbors[i].getIdentifier() );
                            }
                            return ret;
                        }) // Array of String
                        .dataFetcher( "traverse", (DataFetchingEnvironment dfe) -> {
                            try {
                                MeshObject obj           = (MeshObject) dfe.getSource();
                                String     roleTypeId    = dfe.getArgument( "traversalSpec" ); // FIXME, not just RoleType id
                                RoleType   traversalSpec = ModelBase.SINGLETON.findRoleType( roleTypeId );

                                MeshObject [] neighbors = obj.traverse( traversalSpec ).getMeshObjects();
                                Neighbor   [] ret       = new Neighbor[ neighbors.length ];

                                for( int i=0 ; i<neighbors.length ; ++i ) {
                                    ret[i] = new Neighbor( obj, traversalSpec, neighbors[i] );
                                }
                                return ret;

                            } catch( ParseException | MeshTypeWithIdentifierNotFoundException ex ) {
                                throw new RuntimeException( ex );
                            }
                        }) // Array of Neighbor
                        .dataFetcher( "traverses", (DataFetchingEnvironment dfe) -> {
                            try {
                                MeshObject    obj         = (MeshObject) dfe.getSource();
                                String []     roleTypeIds = dfe.getArgument( "traversalSpecs" ); // FIXME, not just RoleType id
                                Neighbor [][] ret         = new Neighbor[ roleTypeIds.length ][];

                                for( int i=0 ; i<roleTypeIds.length ; ++i ) {
                                    RoleType      traversalSpec = ModelBase.SINGLETON.findRoleType( roleTypeIds[i] );
                                    MeshObject [] neighbors = obj.traverse( traversalSpec ).getMeshObjects();

                                    ret[i] = new Neighbor[ neighbors.length ];
                                    for( int j=0 ; j<neighbors.length ; ++j ) {
                                        ret[i][j] = new Neighbor( obj, traversalSpec, neighbors[j] );
                                    }
                                }
                                return ret;

                            } catch( ParseException | MeshTypeWithIdentifierNotFoundException ex ) {
                                throw new RuntimeException( ex );
                            }
                        }) // Array of Array of Neighbor
                )

                .type( TypeRuntimeWiring.newTypeWiring( "Query" )
                        .dataFetcher( "meshObject", new MeshObjectFetcher( theIdSerializer, mb ))
                )

                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();

        theGraphSqlSchema = schemaGenerator.makeExecutableSchema( typeDefinitionRegistry, runtimeWiring );
        theGraphQl        = GraphQL.newGraphQL( theGraphSqlSchema ).build();

        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();

        theGson = builder.create();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void handleRequest(
            HttpServerExchange hse )
    {
        // inspiration: https://github.com/graphql-java/graphql-java-spring/blob/master/graphql-java-spring-webmvc/src/main/java/graphql/spring/web/servlet/components/GraphQLController.java

        UndertowHttpRequest request = UndertowHttpRequest.create( hse, Underbars.getContextPath() );

        ExecutionInput executionInput;

        switch( request.getMethod() ) {
            case "GET":
                ExecutionInput.Builder executionInputBuilder = ExecutionInput.newExecutionInput();
                String query = request.getUrl().getUrlArgument( "query" );
                if( query != null ) {
                    executionInputBuilder = executionInputBuilder.query( query );
                }
                executionInput = executionInputBuilder.build();
                break;

            case "POST":
                if( "application/json".equals( request.getContentType() )) {

                    HashMap<String,Object> map = theGson.fromJson( request.getPostData(), HashMap.class );
                    executionInputBuilder = ExecutionInput.newExecutionInput();
                    if( map.get( "query" ) != null ) {
                        executionInputBuilder.query( (String) map.get( "query" ));
                    }
                    if( map.get( "operationName" ) != null ) {
                        executionInputBuilder.operationName( (String) map.get( "operationName" ));
                    }
                    if( map.get( "variables" ) != null ) {
                        executionInputBuilder.variables( (Map<String,Object>) map.get( "variables" ));
                    }

                    executionInput = executionInputBuilder.build();
                    break;
                } else {
                    hse.setStatusCode( 500 );
                    hse.getResponseSender().send( "ERROR: Unsupported content type for HTTP POST: " + request.getContentType() );
                    return;
                }

            default:
                hse.setStatusCode( 500 );
                hse.getResponseSender().send( "ERROR: Unsupported HTTP method: " + request.getMethod() );
                return;
        }

        if( executionInput == null ) {
            hse.setStatusCode( 500 );
            hse.getResponseSender().send( "ERROR: No GraphQL query found" );
            return;
        }

        ExecutionResult executionResult = theGraphQl.execute( executionInput );

        String response = theGson.toJson( executionResult.toSpecification() );

        hse.setStatusCode( 200 );
        hse.getResponseSender().send( response );
    }

    /**
     * Helper to obtain an array of MeshTypeIdentifiers as Strings from the MeshTypes array.
     *
     * @param types
     * @return
     */
    protected String [] typeIdsOf(
            MeshType [] types )
    {
        String [] ret = new String[ types.length ];
        for( int i=0 ; i<types.length ; ++i ) {
            ret[i] = theTypeIdSerializer.toExternalForm( types[i].getIdentifier() );
        }
        return ret;
    }

    /**
     * Used for serialization.
     */
    protected final MeshObjectIdentifierBothSerializer theIdSerializer;

    /**
     * Used for serialization.
     */
    protected final MeshTypeIdentifierBothSerializer theTypeIdSerializer;

    /**
     * The schema.
     */
    protected final GraphQLSchema theGraphSqlSchema;

    /**
     * The central GraphSQL object.
     */
    protected final GraphQL theGraphQl;

    /**
     * The central Gson object.
     */
    protected final Gson theGson;

    /**
     * Location of the schema file.
     */
    public static final String SCHEMA_FILE = "/ubos/share/ubos-mesh-underbars-graphql/schema/schema.gql";
}
