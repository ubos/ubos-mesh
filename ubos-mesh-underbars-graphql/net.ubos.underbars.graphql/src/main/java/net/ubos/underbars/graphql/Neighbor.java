//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.graphql;

import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.RoleType;

/**
 * Helper class to capture the relationship between a MeshObject and a particular neighbor, for  the
 * purposes of our GraphQL implementation.
 *
 * FIXME: For now this only supports RoleType
 */
public class Neighbor
{
    /**
     * Constructor for easier construction.
     *
     * @param meshObject the primary MeshObject
     * @param traverseSpec the TraversalSpecification through which we arrived at this neighbor from
     *        this starting MeshObject. May be null;
     * @param neighbor the neighbor MeshObject
     */
    public Neighbor(
            MeshObject meshObject,
            RoleType   traverseSpec,
            MeshObject neighbor )
    {
        theMeshObject   = meshObject;
        theTraverseSpec = traverseSpec;
        theNeighbor     = neighbor;
    }

    public final MeshObject theMeshObject;
    public final RoleType   theTraverseSpec;
    public final MeshObject theNeighbor;
}
