//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.graphql.fetchers;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierDeserializer;

/**
 * Knows how to fetch a MeshObject by MeshObjectIdentifier.
 */
public class MeshObjectFetcher
    implements
        DataFetcher<MeshObject>
{
    /**
     * Constructor.
     *
     * @param idDeserializer knows how to deserialize MeshObjectIdentifiers
     * @param mb where to find the MeshObjects
     */
    public MeshObjectFetcher(
            MeshObjectIdentifierDeserializer idDeserializer,
            MeshBase                         mb )
    {
        theIdDeserializer = idDeserializer;
        theMb             = mb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MeshObject get(
            DataFetchingEnvironment dfe )
        throws
            Exception
    {
        String               idString = dfe.getArgument( "meshObjectId" );
        MeshObjectIdentifier id       = theIdDeserializer.meshObjectIdentifierFromExternalForm( idString );

        MeshObject ret = theMb.findMeshObjectByIdentifierOrThrow( id );

        return ret;
    }

    /**
     * Knows how to deserialize MeshObjectIdentifiers.
     */
    protected final MeshObjectIdentifierDeserializer theIdDeserializer;

    /**
     * The MeshBase.
     */
    protected final MeshBase theMb;
}
