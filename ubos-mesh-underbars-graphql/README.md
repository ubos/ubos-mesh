# GraphQL support for UBOS Mesh

## How to run

Add accessory `ubos-mesh-underbars-graphql` to your `Site JSON`.

The endpoint is at `/g/graphql` below the root of the UBOS Mesh app.

## Schema

The GraphQL schema is at [schema/schema.gql](schema/schema.gql).

## Invocation with `curl`

Usually there's lots of escaping that needs to be done to pass JSON to `curl`.
Simon Willison described [this trick](https://til.simonwillison.net/tils/til/graphql_graphql-with-curl~2Emd)
which is excellent, which we packed up in a tiny shell script in `tools/do-query.sh`:

* Write your JSON query as a file. There are some examples in [example-queries/](example-queries/). Let's say the file
  is `example-queries/home-and-neighbor-ids.txt`.

* Install `jq`.

* Run your query as `tools/do-query.sh example-queries/home-and-neighbor-ids.txt`

* If you run your UBOS Mesh app at a URL other than `http://localhost`, specify the root URL as environment variable,
  e.g. invoke as: `EP=http://my.endpoint/g/graphql tools/do-query.sh example-queries/home-and-neighbor-ids.txt`.

