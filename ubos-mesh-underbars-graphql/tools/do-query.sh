#!/bin/bash
#
# Simplify invocation of GraphQL queries
#
# Invoke as:
#     do-query.sh <queryfile>
# or
#     EP=http://my.endpoint/g/graphql do-query.sh <queryfile>
#
# Hat tip to Simon Willison and this post: https://til.simonwillison.net/tils/til/graphql_graphql-with-curl~2Emd
#

GQL=$(cat $1)
CURLDATA=$(jq -c -n --arg query "${GQL}" '{"query":$query}')

curl -v -s -X POST -H "Content-Type: application/json" \
    ${EP:-http://localhost/g/graphql} \
    -d "${CURLDATA}"
