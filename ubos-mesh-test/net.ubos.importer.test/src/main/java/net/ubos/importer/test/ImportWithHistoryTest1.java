//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.test;

import java.io.File;
import java.io.IOException;
import net.ubos.importer.Importer;
import net.ubos.importer.ImporterException;
import net.ubos.importer.ImporterScore;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Tests importing a simple data set with objects at different points in history.
 */
@Disabled
public class ImportWithHistoryTest1
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MMeshBase mb = MMeshBase.Builder.create().history( true ).build();

        TestImporter importer = new TestImporter();

        //

        File root = new File( "dummy" );

        importer.importTo( root, null, mb );

        //

        HistoryCursorIterator<MeshBaseState> historyIter = mb.getHistory().iterator();
        historyIter.moveToBeforeFirst();

        MeshBaseState currentState = historyIter.next();
        MeshBaseView  currentView  = currentState.getMeshBaseView();

        checkEquals( currentView.size(), 1, "Wrong number of objects at start" );

        //

        for( int i=0 ; i<TEST_OBJECTS.length ; ++i ) {
            long when = TEST_OBJECTS[i];

            currentState = historyIter.next();
            currentView  = currentState.getMeshBaseView();

            checkEquals( currentView.size(), 1 + i, "Wrong number of objects at time " + when );
        }

        //

        for( MeshObject current : mb ) {
            MeshObjectHistory currentHistory = mb.meshObjectHistory( current.getIdentifier() );

            MeshObject currentOldest        = currentHistory.oldest();
            long       currentOldestCreated = currentOldest.getTimeCreated();

            checkEquals( current.getIdentifier().getLocalId(), String.valueOf( currentOldestCreated ), "Wrong time created for " + current.getIdentifier() );

            checkEquals( currentOldestCreated, current.getTimeCreated(), "Time created does not match for " + current.getIdentifier() );
        }
    }

    /**
     * The objects to be imported.
     */
    public static final long [] TEST_OBJECTS = {
        1000
    };

    private static final Log log = Log.getLogInstance( ImportWithHistoryTest1.class );

    /**
     * The Importer for this test.
     */
    public static class TestImporter
        implements
            Importer
    {
        @Override
        public String analyze(
                File importCandidate )
            throws
                IOException
        {
            return null;
        }

        @Override
        public ImporterScore importTo(
                File                    toBeImported,
                String                  defaultIdNamespace,
                EditableHistoryMeshBase mb )
            throws
                ImporterException,
                IOException
        {
            for( int i=0 ; i<TEST_OBJECTS.length ; ++i ) {
                long when = TEST_OBJECTS[i];

                mb.executeAt( when, () -> {
                    mb.createMeshObject( String.valueOf( when ));
                });
            }

            return new ImporterScore( this, ImporterScore.BEST, null );
        }
    }
}
