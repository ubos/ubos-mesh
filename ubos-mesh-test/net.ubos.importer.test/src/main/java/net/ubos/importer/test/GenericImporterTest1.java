//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.test;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.ubos.importer.generic.GenericImporter;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.nonblessed.NonblessedCsvUtils;
import net.ubos.mesh.nonblessed.NonblessedJsonUtils;
import net.ubos.mesh.nonblessed.NonblessedUtils;
import net.ubos.mesh.nonblessed.NonblessedZipUtils;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;
import net.ubos.importer.handler.NonblessedImporterHandler;

/**
 * Tests the GenericImporter.
 */
public class GenericImporterTest1
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MMeshBase mb = MMeshBase.Builder.create().history( false ).build();

        GenericImporter importer = GenericImporter.create();

        //

        File root = new File( "/ubos/share/ubos-mesh-test/testdirs/genericimportertest1" );

        importer.importTo( root, "testns", mb );

        //

        if( false ) {
            for( MeshObject current : mb ) {
                System.err.println( current.getIdentifier().getLocalId() );

                for( String att : current.getAttributeNames() ) {
                    Object val = current.getAttributeValue( att );
                    if( val instanceof byte [] ) {
                        val = new String( (byte []) val );
                    }
                    System.err.println( "  A: " + att + " : " + val );
                }
                for( MeshObject neigh : current.traverseToNeighbors() ) {
                    System.err.println( "  N: " + neigh.getIdentifier().getLocalId() );
                    for( String att : current.getRoleAttributeNames( neigh )) {
                        Object val = current.getRoleAttributeValue( neigh, att );
                        if( val instanceof byte [] ) {
                            val = new String( (byte []) val );
                        }
                        System.err.println( "    RA: " + att + " : " + val );
                    }
                }
            }
        }

        checkEquals( mb.size(), 28, "Wrong number of objects" );

        //

        Set<MeshObject> all = new HashSet<>();
        findAll( mb.getHomeObject(), all );

        checkEquals( all.size(), mb.size(), "Not connected" );

        //

        Map<String,Integer> importerCounts = new HashMap<>();
        Map<String,Integer> typeCounts     = new HashMap<>();

        for( MeshObject current : all ) {
            if( current.hasAttribute(NonblessedImporterHandler.IMPORTER_NAME_ATTRIBUTE )) {
                String  att = (String) current.getAttributeValue(NonblessedImporterHandler.IMPORTER_NAME_ATTRIBUTE  );
                Integer count = importerCounts.get( att );
                if( count == null ) {
                    count = 1;
                } else {
                    count = count+1;
                }
                importerCounts.put( att, count );
            }
            if( current.hasAttribute(NonblessedUtils.OBJECTTYPE_ATTRIBUTE )) {
                String  att = (String) current.getAttributeValue(NonblessedUtils.OBJECTTYPE_ATTRIBUTE );
                Integer count = typeCounts.get( att );
                if( count == null ) {
                    count = 1;
                } else {
                    count = count+1;
                }
                typeCounts.put( att, count );
            }
        }

        checkEquals( (int) importerCounts.get( "net.ubos.importer.handler.directory.DefaultDirectoryImporterHandler" ), 5, "Wrong number of directory importers" );
        checkEquals( (int) importerCounts.get( "net.ubos.importer.handler.file.DefaultFileImporterHandler" ),           6, "Wrong number of file importers" );
        checkEquals( (int) importerCounts.get( "net.ubos.importer.handler.json.DefaultJsonImporterHandler" ),           1, "Wrong number of JSON importers" );
        checkEquals( (int) importerCounts.get( "net.ubos.importer.handler.zip.DefaultZipImporterHandler" ),             1, "Wrong number of ZIP importers" );
        checkEquals( (int) importerCounts.get( "net.ubos.importer.handler.csv.DefaultCsvImporterHandler" ),             1, "Wrong number of CSV importers" );

        checkEquals((int) typeCounts.get(NonblessedUtils.OBJECTTYPE_VALUE_DIRECTORY ),     5, "Wrong number directories" );
        checkEquals((int) typeCounts.get(NonblessedUtils.OBJECTTYPE_VALUE_FILE ),          6, "Wrong number files" );
        checkEquals((int) typeCounts.get(NonblessedZipUtils.OBJECTTYPE_VALUE_ZIPFILE ),     1, "Wrong number ZIP files" );
        checkEquals((int) typeCounts.get(NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONOBJECT ), 3, "Wrong number JSON objects" );
        checkEquals((int) typeCounts.get(NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONARRAY ),  1, "Wrong number JSON arrays" );
        checkEquals((int) typeCounts.get(NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONSTRING ), 2, "Wrong number JSON strings" );
        checkEquals((int) typeCounts.get(NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONNUMBER ), 2, "Wrong number JSON numbers" );
        checkEquals((int) typeCounts.get(NonblessedJsonUtils.OBJECTTYPE_VALUE_JSONNULL   ), 2, "Wrong number JSON nulls" );
        checkEquals((int) typeCounts.get(NonblessedCsvUtils.OBJECTTYPE_VALUE_CSVFILE ),     1, "Wrong number CSV files" );
        checkEquals((int) typeCounts.get(NonblessedCsvUtils.OBJECTTYPE_VALUE_CSVROW ),      4, "Wrong number CSV rows" );
    }

    /**
     * Recursive helper to find all MeshObjects by traversing the graph.
     *
     * @param start start of traversal
     * @param foundSoFar collect here
     */
    protected void findAll(
            MeshObject      start,
            Set<MeshObject> foundSoFar )
    {
        foundSoFar.add( start );

        for( MeshObject neighbor : start.traverseToNeighbors() ) {
            if( !foundSoFar.contains( neighbor )) {
                findAll( neighbor, foundSoFar );
            }
        }
    }

    private static final Log log = Log.getLogInstance( GenericImporterTest1.class );
}
