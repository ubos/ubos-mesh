//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.misc;

import net.ubos.testharness.AbstractTest;
import net.ubos.util.UrlUtils;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests URL encoding via the HTTP class.
 */
public class UrlEncodingTest1
    extends
        AbstractTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [][] testCases = {
            {
                "abc def",
                "abc%20def",
                "abc%20def"
            },
            {
                "abc?def",
                "abc%3Fdef",
                "abc%3Fdef"
            },
            {
                "abc/def",
                "abc/def",
                "abc/def" // do not encode "/", Tomcat does not like that at all, it considers it a security issue
            },
            {
                "abc:def",
                "abc%3Adef",
                "abc%3Adef"
            },
            {
                "abc#def",
                "abc%23def",
                "abc%23def"
            },
            {
                "advertisers/$15 off $30 + Free Milk + Free Delivery!/1574479010000",
                "advertisers/%2415%20off%20%2430%20%2B%20Free%20Milk%20%2B%20Free%20Delivery%21/1574479010000",
                "advertisers/%2415%20off%20%2430%20%2B%20Free%20Milk%20%2B%20Free%20Delivery%21/1574479010000"
            }
        };

        for( int i=0 ; i<testCases.length ; ++i ) {
            log.debug( "Now testing " + i );

            String input = testCases[i][0];
            String safeUrl      = UrlUtils.encodeToValidUrl( input );
            String safeArgument = UrlUtils.encodeToValidUrlArgument( input );

            checkEquals( safeUrl,      testCases[i][1], "SafeURL encoding failed for test case \"" + input + "\" (" + i + ")" );
            checkEquals( safeArgument, testCases[i][2], "SafeURLArgument encoding failed for test case \"" + input + "\" (" + i + ")" );

            String decodedUrl         = UrlUtils.decodeUrl( safeUrl );
            String decodedUrlArgument = UrlUtils.decodeUrlArgument( safeArgument );

            checkEquals( decodedUrl, input, "Decoding failed for test case \"" + input + "\" (" + i + ")" );
            checkEquals( decodedUrlArgument, input, "Decoding argument failed for test case \"" + input + "\" (" + i + ")" );
        }
    }

    private static final Log log = Log.getLogInstance( UrlEncodingTest1.class  ); // our own, private logger
}
