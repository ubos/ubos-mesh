//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.history;

import net.ubos.util.history.m.MHistory;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests MHistory navigation.
 */
public class MHistoryTest2
    extends
        AbstractMHistoryTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final int LEN = 10;
        MHistory<Payload> theHistory = MHistory.create();

        Payload [] data = new Payload[ LEN ];

        for( int i=0 ; i<LEN ; ++i ) {
            data[i] = new Payload( i * 10 );
            theHistory.putOrThrow( data[i] );
        }

        //

        int index = 4;

        checkIdentity( theHistory.at( data[index].getTimeUpdated() ), data[index],   "Index " + index + ": Wrong at" );

        checkIdentity( theHistory.before( data[index].getTimeUpdated() ), data[index-1], "Index " + index + ": Wrong before" );
        checkIdentity( theHistory.after(  data[index].getTimeUpdated() ), data[index+1], "Index " + index + ": Wrong after" );

        checkIdentity( theHistory.before( data[index].getTimeUpdated()+1 ), data[index],   "Index " + index + ": Wrong before+1" );
        checkIdentity( theHistory.after(  data[index].getTimeUpdated()+1 ), data[index+1], "Index " + index + ": Wrong after+1" );

        checkIdentity( theHistory.before( data[index].getTimeUpdated()-1 ), data[index-1], "Index " + index + ": Wrong before-1" );
        checkIdentity( theHistory.after(  data[index].getTimeUpdated()-1 ), data[index],   "Index " + index + ": Wrong after-1" );

        checkIdentity( theHistory.atOrBefore( data[index].getTimeUpdated() ), data[index], "Index " + index + ": Wrong atOrBefore" );
        checkIdentity( theHistory.atOrAfter(  data[index].getTimeUpdated() ), data[index], "Index " + index + ": Wrong atOrAfter" );

        checkIdentity( theHistory.atOrBefore( data[index].getTimeUpdated() + 1 ), data[index],   "Index " + index + ": Wrong atOrBefore+1" );
        checkIdentity( theHistory.atOrBefore( data[index].getTimeUpdated() - 1 ), data[index-1], "Index " + index + ": Wrong atOrBefore-1" );
        checkIdentity( theHistory.atOrAfter(  data[index].getTimeUpdated() + 1 ), data[index+1], "Index " + index + ": Wrong atOrAfter+1" );
        checkIdentity( theHistory.atOrAfter(  data[index].getTimeUpdated() - 1 ), data[index],   "Index " + index + ": Wrong atOrAfter-1" );

        checkIdentity( theHistory.closest( data[index].getTimeUpdated() + 4 ), data[index],   "Index " + index + " : Wrong closest+4" );
        checkIdentity( theHistory.closest( data[index].getTimeUpdated() + 6 ), data[index+1], "Index " + index + " : Wrong closest+6" );

        checkCondition( theHistory.containsAt( data[index].getTimeUpdated() ),     "Index " + index + ": Wrong contains" );
        checkCondition( !theHistory.containsAt( data[index].getTimeUpdated() + 1), "Index " + index + ": Wrong contains+1" );
    }

    private static final Log log = Log.getLogInstance( MHistoryTest2.class );
}
