//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.misc;

import net.ubos.testharness.AbstractTest;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the ArrayHelper's determineDifference.
 */
public class ArrayHelperTest1
        extends
            AbstractTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] n = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        String TWO = new String( "2" );

        String [] one  = new String[] { n[1], n[2],         n[3], n[4],       n[6], n[7]       };
        String [] two  = new String[] { n[1], TWO /* !! */,             n[5],       n[7], n[8] };

        //

        log.info( "Trying with String and equals" );

        String [] add1 = new String[] {                                 n[5],             n[8] };
        String [] rem1 = new String[] {                     n[3], n[4],       n[6]             };

        ArrayHelper.Difference<String> diff1 = ArrayHelper.determineDifference( one, two, true, String.class );

        checkEqualsInSequence( diff1.getAdditions(), add1, "not the same additions" );
        checkEqualsInSequence( diff1.getRemovals(),  rem1, "not the same removals" );

        //

        log.info( "now with ==" );

        String [] add2 = new String[] {       n[2],                     n[5],             n[8] };
        String [] rem2 = new String[] {       n[2],         n[3], n[4],       n[6]             };

        ArrayHelper.Difference<String> diff2 = ArrayHelper.determineDifference( one, two, false, String.class );

        checkEqualsInSequence( diff2.getAdditions(), add2, "not the same additions" );
        checkEqualsInSequence( diff2.getRemovals(),  rem2, "not the same removals" );
    }

    private static final Log log = Log.getLogInstance( ArrayHelperTest1.class ); // our own, private logger
}
