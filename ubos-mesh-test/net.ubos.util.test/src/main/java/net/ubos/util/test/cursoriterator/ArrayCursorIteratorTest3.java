//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the ArrayCursorIterator.
 */
public class ArrayCursorIteratorTest3
        extends
            AbstractCursorIteratorTest3
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        ArrayCursorIterator<String> iter = ArrayCursorIterator.create( new String[0] );

        runWith( iter, log );
    }

    private static final Log log = Log.getLogInstance( ArrayCursorIteratorTest3.class ); // our own, private logger
}
