//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.ArrayList;
import java.util.Arrays;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the ArrayListCursorIterator. This is the substantially the same test as
 * ArrayCursorIteratorTest1.
 */
public class ArrayListCursorIteratorTest1
        extends
            AbstractCursorIteratorTest1
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] testData = new String[] {
            "a", // [0]
            "b", // [1]
            "c", // [2]
            "d", // [3]
            "e", // [4]
            "f", // [5]
            "g", // [6]
            "h"  // [7]
        };

        ArrayList<String> testCollection = new ArrayList<>();
        testCollection.addAll( Arrays.asList( testData ));

        ArrayListCursorIterator<String> iter = ArrayListCursorIterator.create( testCollection );

        runWith( getClass().getName() + ": ", testData, iter, log );
    }

    private static final Log log = Log.getLogInstance( ArrayListCursorIteratorTest1.class ); // our own, private logger
}
