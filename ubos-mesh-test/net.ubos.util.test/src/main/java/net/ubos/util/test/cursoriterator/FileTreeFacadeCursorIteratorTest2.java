//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.io.File;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.tree.FileTreeFacade;
import org.junit.jupiter.api.Test;

/**
 * Tests the TreeFacadeCursorIterator.
 */
public class FileTreeFacadeCursorIteratorTest2
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "create a test hierarchy" );

        File top = new File( "build/top" );
        deleteFile( top ); // cleanup

        top.mkdirs();

        FileTreeFacade facade = FileTreeFacade.create( top );

        //

        log.info( "testing iterator" );

        CursorIterator<File> iter1 = facade.iterator();

        super.runWith( top, iter1, log );
    }

    private static final Log log = Log.getLogInstance( FileTreeFacadeCursorIteratorTest2.class  ); // our own, private logger
}
