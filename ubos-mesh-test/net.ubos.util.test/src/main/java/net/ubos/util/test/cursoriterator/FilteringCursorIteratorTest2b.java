//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the FilteringCursorIterator.
 * The underlying iterator has only one element, which the filter accepts.
 */
public class FilteringCursorIteratorTest2b
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
            "a+A",  //  [0]  [0]
        };
        String [] testData = new String[] {
            "a+A",  //  [0]  [0]
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );

        FilteringCursorIterator<String> filterIterator = FilteringCursorIterator.create(
                baseIterator,
                (String s) -> {
                    boolean ret = s.contains("+");
                    return ret;
                } );

        runWith( testData[0], filterIterator, log );
    }

    private static final Log log = Log.getLogInstance( FilteringCursorIteratorTest2b.class ); // our own, private logger
}
