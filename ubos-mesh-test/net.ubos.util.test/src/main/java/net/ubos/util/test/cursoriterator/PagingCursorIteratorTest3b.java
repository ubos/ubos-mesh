//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.PagingCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the PagingCursorIterator.
 * The underlying Iterator has no element.
 */
public class PagingCursorIteratorTest3b
        extends
            AbstractCursorIteratorTest3
{
    @Test
    public void run()
        throws
            Exception
    {
        String [] baseData = new String[] {
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );
        baseIterator.moveToBeforeFirst();

        PagingCursorIterator<String> pagingIterator = PagingCursorIterator.create(
                10,
                baseIterator );

        runWith( pagingIterator, log );
    }

    private static final Log log = Log.getLogInstance( PagingCursorIteratorTest3b.class ); // our own, private logger
}
