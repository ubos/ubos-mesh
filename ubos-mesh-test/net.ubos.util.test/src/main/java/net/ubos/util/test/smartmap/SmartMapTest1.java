//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.smartmap;

import java.util.List;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.m.MSmartMap;
import org.junit.jupiter.api.Test;

/**
 * Tests the basic functionality
 */
public class SmartMapTest1
        extends
            AbstractSmartMapTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Setup" );

        MSmartMap<String,Integer> map = MSmartMap.create();

        CursorIterator<String> keyIter;
        CursorIterator<Integer> valueIter;

        checkEquals( map.size(), 0, "Map not empty" );

        keyIter   = map.keyIterator();
        valueIter = map.valueIterator();

        checkCondition( !keyIter.hasPrevious(),   "key iter has previous" );
        checkCondition( !keyIter.hasNext(),       "key iter has next" );
        checkCondition( !valueIter.hasPrevious(), "value iter has previous" );
        checkCondition( !valueIter.hasNext(),     "value iter has next" );

        //

        log.info( "Inserting" );

        final String [] data = new String[ 26 ];

        for( int i=0 ; i<data.length ; ++i ) {
            data[i] = Character.toString( 'a' + i );
            map.put( data[i], i );
        }

        checkEquals( map.size(), data.length, "Wrong size map" );

        //

        log.info( "Checking key iterator" );

        keyIter = map.keyIterator();

        checkCondition( !keyIter.hasPrevious(),              "key iter has previous" );
        checkCondition( keyIter.hasNext(),                   "key iter does not have next" );
        checkCondition( keyIter.hasNext( data.length ),      "key iter does not have " + data.length + " next" );
        checkCondition( !keyIter.hasNext( data.length+1 ),   "key iter has " + (data.length+1) + " next" );

        List<String> keys = keyIter.next( 100 );
        checkEquals( keys.size(), data.length, "Wrong number keys" );

        for( int i=0 ; i<data.length ; ++i ) {
            Integer found = map.get( data[i] );
            checkObject( found, "No object found at index " + i );
            checkEquals( i, (int) found, "Wrong number found at index " + i );
        }

        //

        log.info( "Checking value iterator" );

        valueIter = map.valueIterator();

        checkCondition( !valueIter.hasPrevious(),            "value iter has previous" );
        checkCondition( valueIter.hasNext(),                 "value iter does not have next" );
        checkCondition( valueIter.hasNext( data.length ),    "value iter does not have " + data.length + " next" );
        checkCondition( !valueIter.hasNext( data.length+1 ), "value iter has " + (data.length+1) + " next" );

        List<Integer> values = valueIter.next( 100 );
        checkEquals( values.size(), data.length, "Wrong number values" );

        for( int i=0 ; i<data.length ; ++i ) {
            checkCondition( values.contains( i ), "Cannot find " + i );
        }
    }

    private static final Log log = Log.getLogInstance( SmartMapTest1.class );
}
