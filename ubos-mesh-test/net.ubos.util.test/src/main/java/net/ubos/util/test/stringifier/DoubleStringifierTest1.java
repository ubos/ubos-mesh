//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import net.ubos.util.text.DoubleStringifier;
import org.junit.jupiter.api.Test;

/**
 * Tests LongStringifier.
 */
public class DoubleStringifierTest1
        extends
            AbstractBasicStringifierTest<Double>
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );


        DoubleStringifier str = DoubleStringifier.create();

        runOneValid(
                str,
                15.243d,
                "15.243",
                new Double[] {
                    1.d,
                    15.d,
                    15.2d,
                    15.24d,
                    15.243d
                },
                3 );

        runOneValid(
                str,
                -1234567.1d,
                "-1234567.1",
                new Double[] {
                    -1.d,
                    -12.d,
                    -123.d,
                    -1234.d,
                    -12345.d,
                    -123456.d,
                    -1234567.d,
                    -1234567.1d
                },
                5 );

        runOneInvalid(
                str,
                "" );

        runOneInvalid(
                str,
                "xyz" );
    }

    /**
     * Append a ".0" to a Stringified number, if necessary.
     *
     * @param in the String to potentially append to
     * @return the appended String
     */
    String appendPointZero(
            String in )
    {
        if( in.endsWith( "." )) {
            return in + "0";
        } else if( in.indexOf( '.' ) >= 0 ) {
            return in;
        } else {
            return in + ".0";
        }
    }

    private static final Log log = Log.getLogInstance( DoubleStringifierTest1.class ); // our own, private logger
}
