//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * CompoundStringifier tests for fixed-length hex's.
 */
public class MessageStringifierTest3
        extends
            AbstractMessageStringifierTest
{
    @Test
    @SuppressWarnings(value={"unchecked"})
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        for( int i=0 ; i<datasets.length ; ++i ) {
            Dataset current = datasets[i];

            runOne( current, true );
        }
    }

    private static final Log log = Log.getLogInstance( MessageStringifierTest3.class  ); // our own, private logger

    static Dataset [] datasets = {
            new StringDataset(
                    "One",
                    "#{0,hex2}",
                    new Object[] { 0xfe },
                    2,
                    "#fe" ),
            new StringDataset(
                    "Two",
                    "#{0,hex2}{1,hex2}",
                    new Object[] { 0xab, 0xcd },
                    3,
                    "#abcd" ),
            new StringDataset(
                    "Three",
                    "#{0,hex2}{1,hex2}{2,hex2}",
                    new Object[] { 0x12, 0x34, 0x56 },
                    4,
                    "#123456" ),
    };
}
