//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import net.ubos.util.text.IntegerStringifier;
import org.junit.jupiter.api.Test;

/**
 * Tests IntegerStringifier.
 */
public class IntegerStringifierTest1
        extends
            AbstractBasicStringifierTest<Integer>
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        IntegerStringifier str = IntegerStringifier.createDecimal();

        runOneValid(
                str,
                15243,
                "15243",
                new Integer[] {
                    1,
                    15,
                    152,
                    1524,
                    15243
                },
                3 );

        runOneValid(
                str,
                -99887766,
                "-99887766",
                new Integer[] {
                    -9,
                    -99,
                    -998,
                    -9988,
                    -99887,
                    -998877,
                    -9988776,
                    -99887766
                },
                5 );

        runOneInvalid(
                str,
                "" );

        runOneInvalid(
                str,
                "xyz" );
    }

    private static final Log log = Log.getLogInstance( IntegerStringifierTest1.class ); // our own, private logger
}
