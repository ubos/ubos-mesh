//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test;

import net.ubos.testharness.junit5.RunJUnit5;

/**
 * Run the JUnit tests contained in this package.
 */
public final class RunJUnit
{
    /**
     * Main program
     *
     * @param args the arguments
     * @return desired exit code
     */
    public static int main(
            String [] args )
    {
        return RunJUnit5.run( RunJUnit.class.getClassLoader(), "net.ubos.util.test", args );
    }

    /**
     * Keep this abstract.
     */
    private RunJUnit()
    {}
}
