//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.testharness.AbstractTest;
import net.ubos.util.ArrayFacade;
import net.ubos.util.logging.Log;
import net.ubos.util.text.ArrayStringifier;
import net.ubos.util.text.LongStringifier;
import org.junit.jupiter.api.Test;

/**
 * Tests ArrayStringifier.
 */
public class ArrayStringifierTest1
        extends
            AbstractTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "ArrayStringifier" );

        ArrayStringifier<Long> str = ArrayStringifier.create( "aaa", "bbb", "ccc", LongStringifier.createDecimal() );
        Long [] data = { 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 0L };

        String result = str.format( ArrayFacade.create( data ) );

        checkEquals( result.length(), str.getStart().length() + str.getEnd().length() + ( data.length-1 ) * str.getMiddle().length() + data.length, "wrong length of result string" );
        checkCondition( result.startsWith( str.getStart() ), "does not start right" );
        checkCondition( result.endsWith( str.getEnd() ), "does not end right" );

        int count = 0;
        int pos   = -1;
        while( ( pos = result.indexOf( str.getMiddle(), pos+1 )) > 0 ) {
            ++count;
        }
        checkEquals( count, data.length-1, "wrong number of middles" );

        log.debug( "Found result '" + result + "'." );
    }

    private static final Log log = Log.getLogInstance( ArrayStringifierTest1.class ); // our own, private logger
}
