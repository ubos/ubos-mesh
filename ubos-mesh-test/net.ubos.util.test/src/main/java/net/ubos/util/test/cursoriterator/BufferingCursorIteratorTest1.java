//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.BufferingCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the BufferingCursorIteratorTest1.
 */
public class BufferingCursorIteratorTest1
        extends
            AbstractCursorIteratorTest1
{
    @Test
    public void run()
        throws
            Exception
    {
        // use prime numbers for oddest results

        String [] testData = new String[ 1009 ];
        for( int i=0 ; i<testData.length ; ++i ) {
            testData[i] = String.format( "test-%04d", i );
        }
        
        for( int chunkSize = 11 ; chunkSize < 100 ; chunkSize += 17 ) {

            ArrayCursorIterator<String>     delegate = ArrayCursorIterator.create( testData );
            BufferingCursorIterator<String> iter = BufferingCursorIterator.create( chunkSize, delegate );

            runWith( getClass().getName() + " (chunkSize " + chunkSize + "): ",  testData, iter, log );
        }

    }

    private static final Log log = Log.getLogInstance( BufferingCursorIteratorTest1.class ); // our own, private logger
}
