//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * CompoundStringifier tests for stack traces. There are many "unchecked cast" exceptions, but somehow I can't figure it out better right now.
 */
public class MessageStringifierTest2
        extends
            AbstractMessageStringifierTest
{
    @Test
    @SuppressWarnings(value={"unchecked"})
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        for( int i=0 ; i<datasets.length ; ++i ) {
            Dataset current = datasets[i];

            runOne( current, false );
        }
    }

    private static void f1()
    {
        f2();
    }
    private static void f2()
    {
        f3();
    }
    private static void f3()
    {
        f4();
    }
    private static void f4()
    {
        throw new RuntimeException( "XXX" ) {
            private static final long serialVersionUID = 1L;
            @Override
            public String getLocalizedMessage()
            {
                return "YYY";
            }
        }; // subclass to test the embedded $
    }

    private static final Log log = Log.getLogInstance( MessageStringifierTest2.class  ); // our own, private logger
    private static final Throwable t1;
    static {
        Throwable caught = null;
        try {
            f1();
        } catch( Throwable ex ) {
            caught = ex;
        }
        t1 = caught;
    }

    static Dataset [] datasets = {
            new RegexDataset(
                    "One",
                    "Abc {0,string} def {1,string} ghi {2,stacktrace} jkl",
                    new Object[] { t1.getMessage(), t1.getLocalizedMessage(), t1 },
                    7,
                    "Abc XXX def YYY ghi "
                        + MessageStringifierTest2.class.getName().replaceAll(  "\\.", "\\\\." )
                        + "\\.f4\\(\\w+\\.java:\\d*\\)"
                        + "(\n[\\w\\.\\$<>/]+\\(((\\w+\\.java:\\d+)|(Native Method))\\)){4,}"
                        + " jkl" ),
    };
}
