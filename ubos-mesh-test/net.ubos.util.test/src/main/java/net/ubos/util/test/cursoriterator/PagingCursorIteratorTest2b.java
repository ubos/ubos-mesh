//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.PagingCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the PagingCursorIterator.
 * The underlying Iterator has only one element, and the window is larger than 1
 */
public class PagingCursorIteratorTest2b
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
            "a",  //  [0]  [0]
        };
        String [] testData = new String[] {
            "a",  //  [0]  [0]
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );
        baseIterator.moveToBefore( baseData[0] );

        PagingCursorIterator<String> pagingIterator = PagingCursorIterator.create(
                10, // bigger window
                baseIterator );

        runWith( testData[0], pagingIterator, log );
    }

    private static final Log log = Log.getLogInstance( PagingCursorIteratorTest2b.class ); // our own, private logger
}
