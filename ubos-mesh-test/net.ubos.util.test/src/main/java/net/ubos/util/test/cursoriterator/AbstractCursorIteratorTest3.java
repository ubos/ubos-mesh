//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.NoSuchElementException;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;

/**
 * Tests zero-element CursorIterators.
 * All CursorIterators are run through the same test sequence, which is factored out here.
 */
public abstract class AbstractCursorIteratorTest3
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @param iter the to-be-tested iterator
     * @param log the Logger to use
     * @param <T> the type of Iterator to test
     */
    protected <T> void runWith(
            CursorIterator<T> iter,
            Log               log )
    {
        //

        log.info( "Check at the beginning" );

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( !iter.hasNext(), "has next at the end" );

        try {
            Object found = iter.peekNext();
            reportError( "Found element after end: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }
        try {
            Object found = iter.peekPrevious();
            reportError( "Found element before beginning: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }

        //

        log.info( "Copy" );

        CursorIterator<?> copy = iter.createCopy();

        checkCondition( !copy.hasPrevious(), "has previous at the beginning" );
        checkCondition( !copy.hasNext(), "has next at the end" );

        try {
            Object found = copy.peekNext();
            reportError( "Found element after end: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }
        try {
            Object found = copy.peekPrevious();
            reportError( "Found element before beginning: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }

        //

        log.info( "Go to past last" );

        iter.moveToAfterLast();

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( !iter.hasNext(), "has next at the end" );

        //

        log.info( "Go before first" );

        iter.moveToBeforeFirst();

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( !iter.hasNext(), "has next at the end" );
    }
}
