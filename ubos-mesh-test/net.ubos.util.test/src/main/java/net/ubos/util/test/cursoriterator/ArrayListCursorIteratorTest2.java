//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.ArrayList;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the ArrayListCursorIterator.
 */
public class ArrayListCursorIteratorTest2
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String testData = "a";

        ArrayList<String> testCollection = new ArrayList<>();
        testCollection.add( testData );

        ArrayListCursorIterator<String> iter = ArrayListCursorIterator.create( testCollection );

        runWith( testData, iter, log );
    }

    private static final Log log = Log.getLogInstance( ArrayListCursorIteratorTest2.class ); // our own, private logger
}
