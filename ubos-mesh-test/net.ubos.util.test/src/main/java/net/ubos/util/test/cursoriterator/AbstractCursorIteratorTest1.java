//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.List;
import java.util.NoSuchElementException;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;

/**
 * Tests regular, several-element CursorIterators.
 * All CursorIterators are run through the same test sequence, which is factored out here.
 */
public abstract class AbstractCursorIteratorTest1
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @param prefix the prefix to emitted messages
     * @param testData the provided test data
     * @param iter the to-be-tested iterator
     * @param log the Logger to use
     * @param <T> the type of Iterator to test
     */
    protected <T> void runWith(
            String            prefix,
            T []              testData,
            CursorIterator<T> iter,
            Log               log )
    {
        //

        log.info( prefix + "Check at the beginning" );

        checkCondition( !iter.hasPrevious(), prefix + "Has previous at the beginning" );
        checkCondition( iter.hasNext( testData.length ), prefix + "Does not have enough nexts" );
        checkCondition( !iter.hasNext( testData.length + 1 ), prefix + "Has too many nexts" );
        checkEquals( iter.peekNext(), testData[0], prefix + "Wrong current element" );

        try {
            Object found = iter.peekPrevious();
            reportError( prefix + "Found element before start: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( prefix + "Correctly received exception" );
        }

        //

        log.info( prefix + "Check forward iteration" );

        for( int i=0 ; i<testData.length ; ++i ) {
            checkCondition( iter.hasNext(), prefix + "Not found next: " + i );

            Object found = iter.next();
            checkEquals( testData[i], found, prefix + "Not found element: " + i );
        }

        //

        log.info( prefix + "Check at the end" );

        checkCondition( !iter.hasNext(), prefix + "Has next at the end" );
        checkCondition( iter.hasPrevious( testData.length ), prefix + "Does not have enough previous" );
        checkCondition( !iter.hasPrevious( testData.length + 1 ), prefix + "Has too many previous" );
        checkEquals( iter.peekPrevious(), testData[testData.length-1], prefix + "Wrong last element" );

        try {
            Object found = iter.peekNext();
            reportError( prefix + "Found element after end: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( prefix + "Correctly received exception" );
        }

        //

        log.info( prefix + "Check backward iteration" );

        for( int i=testData.length-1 ; i>=0 ; --i ) {
            checkCondition( iter.hasPrevious(), prefix + "Not found previous: " + i );

            Object found = iter.previous();
            checkEquals( testData[i], found, prefix + "Not found element: " + i );
        }

        //

        log.info( prefix + "Check again at the beginning" );

        checkCondition( !iter.hasPrevious(), prefix + "Has previous at the beginning" );
        checkCondition( iter.hasNext( testData.length ), prefix + "Does not have enough nexts" );
        checkCondition( !iter.hasNext( testData.length + 1 ), prefix + "Has too many nexts" );

        try {
            Object found = iter.peekPrevious();
            reportError( prefix + "Found element before beginning: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( prefix + "Correctly received exception" );
        }

        //

        log.info( prefix + "Move to element" );

        iter.moveToBefore( testData[ 4 ] ); // "e"
        checkEquals( iter.peekNext(),     testData[4], prefix + "Wrong element found" );
        checkEquals( iter.peekPrevious(), testData[3], prefix + "Wrong element found" );

        //

        log.info( prefix + "Move by positive number" );

        iter.moveBy( 2 ); // "g"
        checkEquals( iter.peekNext(),     testData[6], prefix + "Wrong element found" );
        checkEquals( iter.peekPrevious(), testData[5], prefix + "Wrong element found" );

        //

        log.info( prefix + "Move by negative number" );

        iter.moveBy( -3 ); // "d"
        checkEquals( iter.peekNext(),     testData[3], prefix + "Wrong element found" );
        checkEquals( iter.peekPrevious(), testData[2], prefix + "Wrong element found" );
        checkEquals( iter.peekNext(),     testData[3], prefix + "Wrong element found" );
        checkEquals( iter.peekPrevious(), testData[2], prefix + "Wrong element found" );

        //

        log.info( prefix + "Copy" );

        CursorIterator<T> copy = iter.createCopy();

        checkEquals( iter.peekNext(), copy.peekNext(), prefix + "Copied iterator in a different place" );
        checkEquals( iter.peekNext(),     testData[3], prefix + "Wrong element found" );
        checkEquals( iter.peekPrevious(), testData[2], prefix + "Wrong element found" );
        checkEquals( copy.peekNext(),     testData[3], prefix + "Wrong element found" );
        checkEquals( copy.peekPrevious(), testData[2], prefix + "Wrong element found" );

        //

        log.info( prefix + "Look backward" );

        List<T> before = iter.previous( Integer.MAX_VALUE );

        checkEquals( before.size(), 3, prefix + "Wrong number of elements before" );
        for( int i=0 ; i<3 ; ++i ) {
            checkEquals( testData[i], before.get( before.size()-1-i ), prefix + "Wrong data at index " + i );
        }

        //

        log.info( prefix + "Look forward" );

        List<T> after  = copy.next( Integer.MAX_VALUE );

        checkEquals( after.size(), testData.length - 3, prefix + "Wrong number of elements after" );
        for( int i=3 ; i<testData.length ; ++i ) {
            checkEquals( testData[i], after.get( i-3 ), prefix + "Wrong data at index " + i );
        }

        //

        log.info( prefix + "Go to past last" );

        iter.moveToAfterLast();

        checkCondition( !iter.hasNext(), prefix + "Has next at the end" );
        checkCondition( iter.hasPrevious( testData.length ), prefix + "Does not have enough previous" );
        checkCondition( !iter.hasPrevious( testData.length + 1 ), prefix + "Has too many previous" );
        checkEquals( iter.peekPrevious(), testData[testData.length-1], prefix + "Wrong last element" );

        try {
            Object found = iter.peekNext();
            reportError( prefix + "Found element after end: " + found );
        } catch( NoSuchElementException t ) {
            log.debug( prefix + "Correctly received exception" );
        }

        //

        log.info( prefix + "Go before first" );

        iter.moveToBeforeFirst();

        checkCondition( !iter.hasPrevious(), prefix + "Has previous at the beginning" );
        checkCondition( iter.hasNext( testData.length ), prefix + "Does not have enough nexts" );
        checkCondition( !iter.hasNext( testData.length + 1 ), prefix + "Has too many nexts" );
        checkEquals( iter.peekNext(), testData[0], prefix + "Wrong current element" );

        try {
            Object found = iter.peekPrevious();
            reportError( prefix + "Found element before beginning:" + found );
        } catch( NoSuchElementException t ) {
            log.debug( prefix + "Correctly received exception" );
        }

        //

        log.info( prefix + "Go to the middle, and get all next elements" );

        iter.moveToBeforeFirst();
        iter.moveBy( 2 );
        checkCondition( iter.hasNext( testData.length - 2 ), prefix + "Not enough elements left" );
        checkCondition( !iter.hasNext( testData.length - 1 ), prefix + "Too many elements left" );
        checkEquals( iter.next( testData.length - 2 ).size(), testData.length - 2, prefix + "Elements not found" );

        //

        log.info( prefix + "Go to the middle, and get all previous elements" );

        iter.moveToBeforeFirst();
        iter.moveBy( 2 );
        checkCondition( iter.hasPrevious( 2 ), prefix + "Not enough elements left" );
        checkCondition( !iter.hasPrevious( 3 ), prefix + "Too many elements left" );
        checkEquals( iter.previous( 2 ).size(), 2, prefix + "Elements not found" );

        //

        log.info( prefix + "Go to the middle, and move to the end" );

        iter.moveToBeforeFirst();
        iter.moveBy( 2 );
        checkCondition( iter.hasNext( testData.length - 2 ), prefix + "Not enough elements left" );
        iter.moveBy( testData.length - 2 );
        checkCondition( !iter.hasNext(), prefix + "Has next at the end" );
        checkCondition( iter.hasPrevious(), prefix + "Does not have previous" );

        //

        log.info( prefix + "Go to the middle, and move to the start" );

        iter.moveToBeforeFirst();
        iter.moveBy( 2 );
        checkCondition( iter.hasPrevious( 2 ), prefix + "Not enough elements left" );
        iter.moveBy( -2 );
        checkCondition( iter.hasNext(), prefix + "Does not have next at the beginning" );
        checkCondition( !iter.hasPrevious(), prefix + "Has previous at the beginning" );
    }
}
