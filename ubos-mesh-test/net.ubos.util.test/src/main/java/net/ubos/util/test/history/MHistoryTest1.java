//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.history;

import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.history.HasTimeUpdated;
import net.ubos.util.history.m.MHistory;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests MHistory navigation by HistoryCursorIterator.
 */
public class MHistoryTest1
    extends
        AbstractMHistoryTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final int LEN     = 10;
        final int SPACING = 10;
        final int OFFSET  = 1000;
        MHistory<Payload> theHistory = MHistory.create();

        Payload [] data = new Payload[ LEN ];

        for( int i=0 ; i<LEN ; ++i ) {
            data[i] = new Payload( i * SPACING + OFFSET );
            theHistory.putOrThrow( data[i] );
        }

        //

        HistoryCursorIterator<Payload> iter = theHistory.iterator();

        checkCondition( !iter.hasPrevious(), "Beginning: has previous" );
        checkCondition( iter.hasNext(),      "Beginning: does not have next" );

        //

        iter.moveToAfterLast();
        checkCondition( iter.hasPrevious(), "End: does not have previous" );
        checkCondition( !iter.hasNext(),    "End: has next" );

        //

        iter.moveToBeforeFirst();
        checkCondition( !iter.hasPrevious(), "Beginning: has previous" );
        checkCondition( iter.hasNext(),      "Beginning: does not have next" );

        //

        int index = 4;
        iter.moveToBefore( data[index] );
        checkCondition( iter.hasPrevious(), "Position " + index + ": does not have previous" );
        checkCondition( iter.hasNext(),     "Position " + index + ": does not have next" );
        checkIdentity( iter.peekPrevious(), data[index-1], "Position " + index + ": wrong peek previous" );
        checkIdentity( iter.peekNext(),     data[index],   "Position " + index + ": wrong peek next" );

        checkIdentity( iter.previous(), data[index-1], "Position " + index + ": wrong previous" );
        iter.next();
        checkIdentity( iter.next(),     data[index],   "Position " + index + ": wrong next" );

        //

        index = 7;
        iter.moveToAfter( data[index] );
        checkCondition( iter.hasPrevious(), "Position " + index + ": does not have previous" );
        checkCondition( iter.hasNext(),     "Position " + index + ": does not have next" );
        checkIdentity( iter.peekPrevious(), data[index],   "Position " + index + ": wrong peek previous" );
        checkIdentity( iter.peekNext(),     data[index+1], "Position " + index + ": wrong peek next" );

        checkIdentity( iter.previous(), data[index],   "Position " + index + ": wrong previous" );
        iter.next();
        checkIdentity( iter.next(),     data[index+1], "Position " + index + ": wrong next" );

        //

        index = 3;
        iter.moveToJustBeforeTime( data[index].getTimeUpdated() );
        checkCondition( iter.hasPrevious(), "Position " + index + ": does not have previous" );
        checkCondition( iter.hasNext(),     "Position " + index + ": does not have next" );
        checkIdentity( iter.peekPrevious(), data[index-1], "Position " + index + ": wrong peek previous" );
        checkIdentity( iter.peekNext(),     data[index],   "Position " + index + ": wrong peek next" );

        checkIdentity( iter.previous(), data[index-1], "Position " + index + ": wrong previous" );
        iter.next();
        checkIdentity( iter.next(),     data[index],   "Position " + index + ": wrong next" );

        //

        index = 6;
        iter.moveToJustAfterTime( data[index].getTimeUpdated() );
        checkCondition( iter.hasPrevious(), "Position " + index + ": does not have previous" );
        checkCondition( iter.hasNext(),     "Position " + index + ": does not have next" );
        checkIdentity( iter.peekPrevious(), data[index],   "Position " + index + ": wrong peek previous" );
        checkIdentity( iter.peekNext(),     data[index+1], "Position " + index + ": wrong peek next" );

        checkIdentity( iter.previous(), data[index],   "Position " + index + ": wrong previous" );
        iter.next();
        checkIdentity( iter.next(),     data[index+1], "Position " + index + ": wrong next" );

        //

        iter.moveToJustAfterTime( data[3].getTimeUpdated() ); // somewhere
        iter.moveToJustAfterTime( 2 );
        checkCondition( !iter.hasPrevious(), "After 1: has previous" );
        checkCondition( iter.hasNext(),      "After 1: does not have next" );
        checkIdentity(  iter.peekNext(),     data[0], "After 1: wrong peek next" );

        iter.moveToJustAfterTime( data[3].getTimeUpdated() ); // somewhere
        iter.moveToJustBeforeTime( 2 );
        checkCondition( !iter.hasPrevious(), "Before 2: has previous" );
        checkCondition( iter.hasNext(),      "Before 2: does not have next" );
        checkIdentity(  iter.peekNext(),     data[0], "Before 2: wrong peek next" );

        //

        iter.moveToJustAfterTime( data[3].getTimeUpdated() ); // somewhere
        iter.moveToJustBeforeTime( Long.MAX_VALUE-10 );
        checkCondition( iter.hasPrevious(),  "Before MAX-10: does not have previous" );
        checkCondition( !iter.hasNext(),     "Before MAX-10: has next" );
        checkIdentity(  iter.peekPrevious(), data[data.length-1], "Before MAX-10: wrong peek previous" );

        iter.moveToJustAfterTime( data[3].getTimeUpdated() ); // somewhere
        iter.moveToJustAfterTime( Long.MAX_VALUE-10 );
        checkCondition( iter.hasPrevious(),  "After MAX-10: has previous" );
        checkCondition( !iter.hasNext(),     "After MAX-10: does not have next" );
        checkIdentity(  iter.peekPrevious(), data[data.length-1], "After MAX-10: wrong peek previous" );
    }

    private static final Log log = Log.getLogInstance( MHistoryTest1.class );
}
