//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the FilteringCursorIterator.
 * The underlying CursorIterator has plenty of elements, but only one qualifies for te filter.
 */
public class FilteringCursorIteratorTest2a
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
            "a",    //  [0]   -
            "b",    //  [1]   -
            "c",    //  [2]   -
            "d+A",  //  [3]  [0]
            "e",    //  [4]   -
            "f",    //  [5]   -
            "g",    //  [6]   -
        };
        String [] testData = new String[] {
            "d+A",  //  [3]  [0]
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );

        FilteringCursorIterator<String> filterIterator = FilteringCursorIterator.create(
                baseIterator,
                (String s) -> {
                    boolean ret = s.contains("+");
                    return ret;
                } );

        runWith( testData[0], filterIterator, log );
    }

    private static final Log log = Log.getLogInstance( FilteringCursorIteratorTest2a.class ); // our own, private logger
}
