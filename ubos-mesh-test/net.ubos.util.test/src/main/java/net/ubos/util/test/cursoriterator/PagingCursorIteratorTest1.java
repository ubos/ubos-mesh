//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.PagingCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the PagingCursorIterator.
 */
public class PagingCursorIteratorTest1
        extends
            AbstractCursorIteratorTest1
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
            "a",  //  [0]   -
            "b",  //  [1]   -
            "c",  //  [2]   -
            "d",  //  [3]  [0]
            "e",  //  [4]  [1]
            "f",  //  [5]  [2]
            "g",  //  [6]  [3]
            "h",  //  [7]  [4]
            "i",  //  [8]  [5]
            "j",  //  [9]  [6]
            "k",  // [10]  [7]
            "l",  // [11]   -
            "m",  // [12]   -
        };
        String [] testData = new String[] {
            "d",  //  [1]  [0]
            "e",  //  [3]  [1]
            "f",  //  [4]  [2]
            "g",  //  [7]  [3]
            "h",  //  [8]  [4]
            "i",  //  [9]  [5]
            "j",  //  [9]  [6]
            "k",  // [10]  [7]
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );
        baseIterator.moveToBefore( baseData[3] );

        PagingCursorIterator<String> pagingIterator = PagingCursorIterator.create(
                testData.length,
                baseIterator );

        runWith( getClass().getName() + ": ", testData, pagingIterator, log );
    }

    private static final Log log = Log.getLogInstance( PagingCursorIteratorTest1.class ); // our own, private logger
}
