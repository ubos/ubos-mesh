//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the ArrayCursorIterator.
 */
public class ArrayCursorIteratorTest1
        extends
            AbstractCursorIteratorTest1
{
    @Test
    public void run()
        throws
            Exception
    {
        String [] testData = new String[] {
            "a", // [0]
            "b", // [1]
            "c", // [2]
            "d", // [3]
            "e", // [4]
            "f", // [5]
            "g", // [6]
            "h"  // [7]
        };

        ArrayCursorIterator<String> iter = ArrayCursorIterator.create( testData );

        runWith( getClass().getName() + ": ", testData, iter, log );
    }

    private static final Log log = Log.getLogInstance( ArrayCursorIteratorTest1.class ); // our own, private logger
}
