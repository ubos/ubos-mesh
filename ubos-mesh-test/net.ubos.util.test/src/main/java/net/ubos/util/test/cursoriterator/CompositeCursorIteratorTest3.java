//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.ArrayList;
import java.util.List;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CompositeCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the CompoundCursorIterator.
 * There is no element.
 */
public class CompositeCursorIteratorTest3
        extends
            AbstractCursorIteratorTest3
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        ArrayList<String> collection1 = new ArrayList<>();
        ArrayList<String> collection2 = new ArrayList<>();
        ArrayList<String> collection3 = new ArrayList<>();

        ArrayListCursorIterator<String> iter1 = ArrayListCursorIterator.create( collection1 );
        ArrayListCursorIterator<String> iter2 = ArrayListCursorIterator.create( collection2 );
        ArrayListCursorIterator<String> iter3 = ArrayListCursorIterator.create( collection3 );

        List<CursorIterator<String>> iterators = new ArrayList<>();
        iterators.add( iter1 );
        iterators.add( iter2 );
        iterators.add( iter3 );

        CompositeCursorIterator<String> iter = CompositeCursorIterator.create( iterators );

        runWith( iter, log );
    }

    private static final Log log = Log.getLogInstance( CompositeCursorIteratorTest3.class  ); // our own, private logger
}
