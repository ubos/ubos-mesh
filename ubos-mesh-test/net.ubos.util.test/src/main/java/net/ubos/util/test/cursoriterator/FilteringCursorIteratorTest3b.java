//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the FilteringCursorIterator.
 * The underlying iterator has no elements.
 */
public class FilteringCursorIteratorTest3b
        extends
            AbstractCursorIteratorTest3
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );

        FilteringCursorIterator<String> filterIterator = FilteringCursorIterator.create(
                baseIterator,
                (String s) -> {
                    boolean ret = s.contains("+");
                    return ret;
                } );

        runWith( filterIterator, log );
    }

    private static final Log log = Log.getLogInstance( FilteringCursorIteratorTest3b.class ); // our own, private logger
}
