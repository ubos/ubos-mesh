//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import java.util.Iterator;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.ArrayHelper;
import net.ubos.util.text.Stringifier;
import net.ubos.util.text.StringifierException;
import net.ubos.util.text.StringifierParsingChoice;

/**
 * Factors out common code of the various StringifierTests1 that test stringification
 * of basic data types.
 *
 * @param <T> the basic data type being tested
 */
public abstract class AbstractBasicStringifierTest<T>
        extends
            AbstractTest
{
    /**
     * Run one test.
     *
     * @param str the Stringifier to test
     * @param data the data to stringify
     * @param correctFormatted the correct stringification
     * @param correctPartialRestored the correct values for the partial parsingChoiceIterator
     * @param max when testing the max functionality of the partialParsingChoiceIterator, use this max
     * @throws Exception something went wrong during the test
     */
    protected void runOneValid(
            Stringifier<T> str,
            T              data,
            String         correctFormatted,
            T []           correctPartialRestored,
            int            max )
        throws
            Exception
    {
        runOneValidFormatting( str, data, correctFormatted );
        runOneValidRestoring( str, correctFormatted, data );
        runOneValidAllIterator( str, data, correctFormatted );
        runOneValidPartialIterator( str, correctFormatted, correctPartialRestored );
        runOneValidPartialIteratorWithLimit( str, correctFormatted, correctPartialRestored, max );
    }

    protected void runOneValidFormatting(
            Stringifier<T> str,
            T              data,
            String         correctFormatted )
        throws
            Exception
    {
        String formatted  = str.format( data );
        checkEquals( formatted, correctFormatted, "not the same" );
    }

    protected void runOneValidRestoring(
            Stringifier<T> str,
            String         formatted,
            T              data )
        throws
            Exception
    {
        T restored  = str.unformat( formatted, null );
        checkEquals( restored, data, "Wrong found value" );
    }

    protected void runOneValidAllIterator(
            Stringifier<T> str,
            T              data,
            String         formatted )
        throws
            Exception
    {
        Iterator<StringifierParsingChoice<T>> allIter    = str.allParsingChoiceIterator( formatted );
        StringifierParsingChoice<?> []        allChoices = ArrayHelper.copyIntoNewArray( allIter, StringifierParsingChoice.class );

        checkEquals( allChoices.length,             1,                  "Wrong number of choices" );
        checkEquals( allChoices[0].getStartIndex(), 0,                  "Wrong start index" );
        checkEquals( allChoices[0].getEndIndex(),   formatted.length(), "Wrong end index");

        checkEquals( allChoices[0].unformat(), data, "Incorrect choice" );

    }

    protected void runOneValidPartialIterator(
            Stringifier<T> str,
            String         formatted,
            T []           partialRestored )
        throws
            Exception
    {
        Iterator<StringifierParsingChoice<T>> partialIter    = str.partialParsingChoiceIterator( formatted );
        @SuppressWarnings("unchecked")
        StringifierParsingChoice<T> []        partialChoices = ArrayHelper.copyIntoNewArray( partialIter, StringifierParsingChoice.class );

        checkEquals( partialChoices.length, partialRestored.length, "Wrong number of restored partial choices" );

        for( int i=0 ; i<partialChoices.length ; ++i ) {
            T restored = partialChoices[i].unformat();

            checkEquals( restored, partialRestored[i], "Incorrect choice for " + i );

            String restoredFormatted = str.format( restored );

            checkEquals( partialChoices[i].getStartIndex(), 0, "Wrong start index for " + i );
            checkCondition( partialChoices[i].getStartIndex() <= partialChoices[i].getEndIndex(), "Wrong end index for " + i );
                // This is the best we can do in the generic case, because some trailing characters may not show
                // e.g. "15." vs "15.0"
        }
    }

    protected void runOneValidPartialIteratorWithLimit(
            Stringifier<T> str,
            String         formatted,
            T []           partialRestored,
            int            max )
        throws
            Exception
    {
        Iterator<StringifierParsingChoice<T>> partialIter    = str.partialParsingChoiceIterator( formatted, max );
        @SuppressWarnings("unchecked")
        StringifierParsingChoice<T> []        partialChoices = ArrayHelper.copyIntoNewArray( partialIter, StringifierParsingChoice.class );

        checkEquals( partialChoices.length, max, "Wrong number of restored partial choices" );

        for( int i=0 ; i<partialChoices.length ; ++i ) {
            T restored = partialChoices[i].unformat();

            checkEquals( restored, partialRestored[i], "Incorrect choice for " + i );

            String restoredFormatted = str.format( restored );

            checkEquals( partialChoices[i].getStartIndex(), 0, "Wrong start index for " + i );
            checkCondition( partialChoices[i].getStartIndex() <= partialChoices[i].getEndIndex(), "Wrong end index for " + i );
                // This is the best we can do in the generic case, because some trailing characters may not show
                // e.g. "15." vs "15.0"
        }
    }

    protected void runOneInvalid(
            Stringifier<T> str,
            String         invalidFormatted )
    {
        try {
            Object temp = str.unformat( invalidFormatted );
            reportError( "Should have thrown exception" );

        } catch( StringifierException ex ) {
            // good
        }
    }
}
