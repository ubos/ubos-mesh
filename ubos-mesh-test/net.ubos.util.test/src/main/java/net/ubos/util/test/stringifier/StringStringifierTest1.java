//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import net.ubos.util.text.StringStringifier;
import org.junit.jupiter.api.Test;

/**
 * Tests StringStringifier; Simplest case.
 */
public class StringStringifierTest1
        extends
            AbstractBasicStringifierTest<String>
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        StringStringifier str = StringStringifier.create( );

        String data = "abcdefghi";

        runOneValid(
                str,
                data,
                data,
                new String[] {
                    "",
                    "a",
                    "ab",
                    "abc",
                    "abcd",
                    "abcde",
                    "abcdef",
                    "abcdefg",
                    "abcdefgh",
                    "abcdefghi"
                },
                4 );
    }

    private static final Log log = Log.getLogInstance( StringStringifierTest1.class ); // our own, private logger
}
