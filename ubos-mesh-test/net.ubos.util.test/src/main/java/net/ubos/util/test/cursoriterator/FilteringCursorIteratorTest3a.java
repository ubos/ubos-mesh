//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the FilteringCursorIterator.
 * The underlying Iterator has plenty of elements, but the filter accepts none.
 */
public class FilteringCursorIteratorTest3a
        extends
            AbstractCursorIteratorTest3
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
            "a",    //  [0]   -
            "b",    //  [1]   -
            "c",    //  [2]   -
            "d",    //  [3]   -
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );

        FilteringCursorIterator<String> filterIterator = FilteringCursorIterator.create(
                baseIterator,
                (String s) -> {
                    boolean ret = s.contains("+");
                    return ret;
                } );

        runWith( filterIterator, log );
    }

    private static final Log log = Log.getLogInstance( FilteringCursorIteratorTest3a.class ); // our own, private logger
}
