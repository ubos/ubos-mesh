//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import net.ubos.util.text.LongStringifier;
import org.junit.jupiter.api.Test;

/**
 * Tests LongStringifier.
 */
public class LongStringifierTest1
        extends
            AbstractBasicStringifierTest<Long>
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        LongStringifier str = LongStringifier.createDecimal();

        runOneValid(
                str,
                15243L,
                "15243",
                new Long[] {
                    1l,
                    15l,
                    152l,
                    1524l,
                    15243l
                },
                3 );

        runOneValid(
                str,
                -99887766l,
                "-99887766",
                new Long[] {
                    -9l,
                    -99l,
                    -998l,
                    -9988l,
                    -99887l,
                    -998877l,
                    -9988776l,
                    -99887766l
                },
                5 );

        runOneInvalid(
                str,
                "" );

        runOneInvalid(
                str,
                "xyz" );
    }

    private static final Log log = Log.getLogInstance( LongStringifierTest1.class ); // our own, private logger
}
