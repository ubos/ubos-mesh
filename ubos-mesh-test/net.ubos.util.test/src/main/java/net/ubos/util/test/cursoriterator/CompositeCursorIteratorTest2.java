//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.ArrayList;
import java.util.List;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CompositeCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the CompoundCursorIterator.
 * There is only one element.
 */
public class CompositeCursorIteratorTest2
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] testData = new String[] {
            "a", // [0]
        };

        ArrayList<String> collection1 = new ArrayList<>();
        ArrayList<String> collection2 = new ArrayList<>();
        ArrayList<String> collection3 = new ArrayList<>();
        ArrayList<String> collection4 = new ArrayList<>();

        // nothing in #1
        collection2.add( testData[0] );
        // nothing in #3 and #4

        ArrayListCursorIterator<String> iter1 = ArrayListCursorIterator.create( collection1 );
        ArrayListCursorIterator<String> iter2 = ArrayListCursorIterator.create( collection2 );
        ArrayListCursorIterator<String> iter3 = ArrayListCursorIterator.create( collection3 );
        ArrayListCursorIterator<String> iter4 = ArrayListCursorIterator.create( collection4 );

        List<CursorIterator<String>> iterators = new ArrayList<>();
        iterators.add( iter1 );
        iterators.add( iter2 );
        iterators.add( iter3 );
        iterators.add( iter4 );

        CompositeCursorIterator<String> iter = CompositeCursorIterator.create( iterators );

        runWith( testData[0], iter, log );
    }

    private static final Log log = Log.getLogInstance( CompositeCursorIteratorTest2.class  ); // our own, private logger
}
