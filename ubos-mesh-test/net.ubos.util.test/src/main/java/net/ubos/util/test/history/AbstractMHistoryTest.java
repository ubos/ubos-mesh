//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.history;

import net.ubos.testharness.AbstractTest;
import net.ubos.util.history.HasTimeUpdated;

/**
 * Factors out code common to HistoryTests.
 * @author jernst
 */
public class AbstractMHistoryTest
        extends
            AbstractTest
{

    public static class Payload
        implements
            HasTimeUpdated
    {
        public Payload(
               int timeUpdated )
        {
            theTimeUpdated = timeUpdated;
        }

        @Override
        public long getTimeUpdated()
        {
            return theTimeUpdated;
        }

        protected final int theTimeUpdated;
    }
}
