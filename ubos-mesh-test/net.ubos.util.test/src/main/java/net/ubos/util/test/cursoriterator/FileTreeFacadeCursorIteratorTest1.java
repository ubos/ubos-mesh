//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.io.File;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.util.tree.FileTreeFacade;
import org.junit.jupiter.api.Test;

/**
 * Tests the TreeFacadeCursorIterator.
 */
public class FileTreeFacadeCursorIteratorTest1
        extends
            AbstractCursorIteratorTest1
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "create a test hierarchy, out of order" );

        File top = new File( "build/top" );
        deleteFile( top ); // cleanup

        top.mkdirs();

        File a = new File( top, "a" );
        File b = new File( top, "b" );
        File c = new File( top, "c" );

        File b1 = new File( b, "1" );
        File b2 = new File( b, "2" );
        File b3 = new File( b, "3" );

        File b3x = new File( b3, "x" );
        File b3y = new File( b3, "y" );

        c.createNewFile();
        a.createNewFile();
        b3.mkdirs();
        b3x.createNewFile();
        b1.createNewFile();
        b2.createNewFile();
        b3y.createNewFile();

        File [] testData = {
            top,
            a,
            b,
            b1,
            b2,
            b3,
            b3x,
            b3y,
            c
        };

        FileTreeFacade facade = FileTreeFacade.create( top );

        //

        log.info( "testing facade" );

        checkEquals( facade.getForwardSiblingNode(  a ),   b,    "a->b wrong" );
        checkEquals( facade.getForwardSiblingNode(  b ),   c,    "b->c wrong" );
        checkEquals( facade.getForwardSiblingNode(  c ),   null, "c->null wrong" );
        checkEquals( facade.getBackwardSiblingNode( a ),   null, "a<-null wrong" );
        checkEquals( facade.getBackwardSiblingNode( b ),   a,    "b<-a wrong" );
        checkEquals( facade.getBackwardSiblingNode( c ),   b,    "c<-b wrong" );
        checkEquals( facade.getForwardSiblingNode(  b1 ),  b2,   "b1->b2 wrong" );
        checkEquals( facade.getForwardSiblingNode(  b2 ),  b3,   "b2->b3 wrong" );
        checkEquals( facade.getForwardSiblingNode(  b3 ),  null, "b3->null wrong" );
        checkEquals( facade.getBackwardSiblingNode( b1 ),  null, "b1<-null wrong");
        checkEquals( facade.getBackwardSiblingNode( b2 ),  b1,   "b2<-b1 wrong" );
        checkEquals( facade.getBackwardSiblingNode( b3 ),  b2,   "b3<-b2 wrong" );
        checkEquals( facade.getForwardSiblingNode(  b3x ), b3y,  "b3x->b3y wrong" );
        checkEquals( facade.getForwardSiblingNode(  b3y ), null, "b3y->null wrong" );
        checkEquals( facade.getBackwardSiblingNode( b3x ), null, "b3x<-null wrong" );
        checkEquals( facade.getBackwardSiblingNode( b3y ), b3x,  "b3y<-b3y wrong" );

        checkEqualsOutOfSequence( facade.getChildNodes( top ), new File[] { a, b, c    }, "top has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( a   ), new File[] {            }, "a has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( b   ), new File[] { b1, b2, b3 }, "b has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( c   ), new File[] {            }, "c has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( b1  ), new File[] {            }, "b1 has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( b2  ), new File[] {            }, "b2 has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( b3  ), new File[] { b3x, b3y   }, "b3 has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( b3x ), new File[] {            }, "b3x has wrong children" );
        checkEqualsOutOfSequence( facade.getChildNodes( b3y ), new File[] {            }, "b3y has wrong children" );

        checkEquals( facade.getParentNode( top ), null, "top has wrong parent" );
        checkEquals( facade.getParentNode( a   ), top,  "a has wrong parent" );
        checkEquals( facade.getParentNode( b   ), top,  "b has wrong parent" );
        checkEquals( facade.getParentNode( c   ), top,  "c has wrong parent" );
        checkEquals( facade.getParentNode( b1  ), b,    "b1 has wrong parent" );
        checkEquals( facade.getParentNode( b2  ), b,    "b2 has wrong parent" );
        checkEquals( facade.getParentNode( b3  ), b,    "b3 has wrong parent" );
        checkEquals( facade.getParentNode( b3x ), b3,   "b3x has wrong parent" );
        checkEquals( facade.getParentNode( b3y ), b3,   "b3y has wrong parent" );

        //

        log.info( "testing iterator" );

        CursorIterator<File> iter1 = facade.iterator();

        runWith( getClass().getName() + ": ", testData, iter1, log );
    }

    private static final Log log = Log.getLogInstance( FileTreeFacadeCursorIteratorTest1.class  ); // our own, private logger
}
