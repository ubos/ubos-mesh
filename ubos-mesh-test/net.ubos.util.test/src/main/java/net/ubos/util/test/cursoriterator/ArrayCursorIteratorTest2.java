//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the ArrayCursorIterator.
 */
public class ArrayCursorIteratorTest2
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String testData = "a";

        ArrayCursorIterator<String> iter = ArrayCursorIterator.<String>create( new String[] { testData } );

        runWith( testData, iter, log );
    }

    private static final Log log = Log.getLogInstance( ArrayCursorIteratorTest2.class ); // our own, private logger
}
