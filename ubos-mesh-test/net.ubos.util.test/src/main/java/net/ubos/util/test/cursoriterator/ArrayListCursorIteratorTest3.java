//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.ArrayList;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the ArrayListCursorIterator.
 */
public class ArrayListCursorIteratorTest3
        extends
            AbstractCursorIteratorTest3
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        ArrayList<String> testCollection = new ArrayList<>();

        ArrayListCursorIterator<String> iter = ArrayListCursorIterator.create( testCollection );

        runWith( iter, log );
    }

    private static final Log log = Log.getLogInstance( ArrayListCursorIteratorTest3.class ); // our own, private logger
}
