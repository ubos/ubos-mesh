//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.smartfactory;

import net.ubos.testharness.AbstractTest;
import net.ubos.util.factory.Factory;
import net.ubos.util.factory.MSmartFactory;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.m.MSmartMap;
import org.junit.jupiter.api.Test;

/**
 * Tests the basic behavior of the MSimpleSmartFactory.
 */
public class SmartFactoryTest1
        extends
            AbstractTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        Factory<String,Integer,Float> delegateFactory = new Factory<>() {
            @Override
            public Integer obtainFor(
                    String key,
                    Float  argument )
            {
                int value = argument.intValue();
                return value;
            }
        };

        MSmartFactory<String,Integer,Float> testFactory = MSmartFactory.create(
                delegateFactory,
                MSmartMap.create());

        String [] testKeys1 = {
            "abc",
            "def",
            "ghiklm"
        };
        Float [] testArgs1 = {
            1.f,
            2.f,
            3.4f
        };

        //

        log.info( "Creating a few objects" );

        Integer [] values1 = new Integer[ testKeys1.length ];
        for( int i=0 ; i<testKeys1.length ; ++i ) {
            values1[i] = testFactory.obtainFor( testKeys1[i], testArgs1[i] );

            checkEquals( testArgs1[i].intValue(), values1[i].intValue(), "Not the same" );
        }
        checkEquals( testFactory.size(), testKeys1.length, "wrong number of objects in factory" );

        //

        log.info( "Adding a few objects manually" );

        String [] testKeys2 = {
            "zztop",
            "aabottom",
            testKeys1[2]
        };
        Integer [] testValues2 = {
            5,
            6,
            7
        };
        Integer [] testValues2Return = {
            null,
            null,
            testArgs1[2].intValue()
        };
        Integer [] testValues2Actual = new Integer[ testValues2Return.length ];

        for( int i=0 ; i<testKeys2.length ; ++i ) {
            testValues2Actual[i] = testFactory.put( testKeys2[i], testValues2[i] );

            checkEquals( testValues2Return[i], testValues2Actual[i], "Not the same" );
        }
        checkEquals( testFactory.size(), testKeys1.length + testKeys2.length - 1, "wrong number of objects in factory" );

        //

        log.info( "looking up objects" );

        for( int i=0 ; i<testKeys1.length-1 /* skip the last one */ ; ++i ) {
            Integer ret = testFactory.get( testKeys1[i] );

            checkEquals( ret, values1[i], "not the same" );
        }
        for( int i=0 ; i<testKeys2.length ; ++i ) {
            Integer ret = testFactory.get( testKeys2[i] );

            checkEquals( ret, testValues2[i], "not the same" );
        }

        //

        log.info( "removing a few" );

        Integer ret = testFactory.remove( testKeys1[1] );
        checkEquals( ret, values1[1], "not the same" );

        ret = testFactory.remove( testKeys2[1] );
        checkEquals( ret, testValues2[1], "not the same" );
        checkEquals( testFactory.size(), testKeys1.length + testKeys2.length - 1 -2, "wrong number of objects in factory" );
    }

    private static final Log log = Log.getLogInstance( SmartFactoryTest1.class ); // our own, private logger
}
