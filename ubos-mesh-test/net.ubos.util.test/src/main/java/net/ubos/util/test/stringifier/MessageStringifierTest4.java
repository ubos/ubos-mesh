//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Same as MessageStringifierTest3 for long instead of int.
 */
public class MessageStringifierTest4
        extends
            AbstractMessageStringifierTest
{
    @Test
    @SuppressWarnings(value={"unchecked"})
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        for( int i=0 ; i<datasets.length ; ++i ) {
            Dataset current = datasets[i];

            runOne( current, true );
        }
    }

    private static final Log log = Log.getLogInstance( MessageStringifierTest4.class  ); // our own, private logger

    static Dataset [] datasets = {
            new StringDataset(
                    "One",
                    "#{0,longhex4}",
                    new Object[] { 0xfedcL },
                    2,
                    "#fedc" ),
            new StringDataset(
                    "Two",
                    "#{0,longhex4}{1,longhex4}",
                    new Object[] { 0xabcdL, 0xcdefL },
                    3,
                    "#abcdcdef" ),
            new StringDataset(
                    "Three",
                    "#{0,longhex4}{1,longhex4}{2,longhex4}",
                    new Object[] { 0x1234L, 0x5678L, 0x9abcL },
                    4,
                    "#123456789abc" ),
    };
}
