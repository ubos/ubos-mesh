//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//
package net.ubos.util.test.smartmap;

import net.ubos.testharness.AbstractTest;

/**
 * Factors out code common to SmartMapTests.
 */
public abstract class AbstractSmartMapTest
        extends
            AbstractTest
{
}
