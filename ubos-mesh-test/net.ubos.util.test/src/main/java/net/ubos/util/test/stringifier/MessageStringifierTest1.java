//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.stringifier;

import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * CompoundStringifier tests. There are many "unchecked cast" exceptions, but somehow I can't figure it out better right now.
 */
public class MessageStringifierTest1
        extends
            AbstractMessageStringifierTest
{
    @Test
    @SuppressWarnings(value={"unchecked"})
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        for( int i=0 ; i<datasets.length ; ++i ) {
            Dataset current = datasets[i];

            runOne( current, true );
        }
    }

    private static final Log log = Log.getLogInstance( MessageStringifierTest1.class  ); // our own, private logger


    static Dataset [] datasets = {
            new StringDataset(
                    "One",
                    "Abc {0,int}",
                    new Object[] { 12 },
                    2,
                    "Abc 12" ),
            new StringDataset(
                    "Two",
                    "Abc {0,int} def{1,string}",
                    new Object[] { 12, "XXX-X" },
                    4,
                    "Abc 12 defXXX-X" ),
            new StringDataset(
                    "Three",
                    "Abc {0,int} def",
                    new Object[] { 987 },
                    3,
                    "Abc 987 def" ),
            new StringDataset(
                    "Four",
                    "Abc {0,int} def{2,string}  ghi kl{0,int}mno {1,int}",
                    new Object[] { 0, 111, "222" },
                    8,
                    "Abc 0 def222  ghi kl0mno 111" ),
    };
}
