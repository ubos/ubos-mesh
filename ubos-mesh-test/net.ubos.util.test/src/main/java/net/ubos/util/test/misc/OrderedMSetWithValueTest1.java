//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.misc;

import net.ubos.testharness.AbstractTest;
import net.ubos.util.OrderedMSetWithValue;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests overlowing the OrderedMSetWithValue
 */
public class OrderedMSetWithValueTest1
        extends
            AbstractTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );
        
        int max = 10;

        OrderedMSetWithValue<String,Integer> smallToLarge = OrderedMSetWithValue.createSmallToLarge( max );
        OrderedMSetWithValue<String,Integer> largeToSmall = OrderedMSetWithValue.createLargeToSmall( max );
        
        //
        
        smallToLarge.put( "E-0", 10 );
        smallToLarge.put( "E-1", 12 );
        smallToLarge.put( "E-2",  8 );
        
        largeToSmall.put( "E-0", 10 );
        largeToSmall.put( "E-1", 12 );
        largeToSmall.put( "E-2",  8 );
        
        checkEquals( smallToLarge.size(), 3, "Wrong size smallToLarge" );
        checkEquals( largeToSmall.size(), 3, "Wrong size largeToSmall" );
        
        CursorIterator<String> smallToLargeIter = smallToLarge.iterator();
        checkEquals( smallToLargeIter.next(), "E-2", "Wrong on smallToLarge(0)" );
        checkEquals( smallToLargeIter.next(), "E-0", "Wrong on smallToLarge(1)" );
        checkEquals( smallToLargeIter.next(), "E-1", "Wrong on smallToLarge(2)" );
        
        CursorIterator<String> largeToSmallIter = largeToSmall.iterator();
        checkEquals( largeToSmallIter.next(), "E-1", "Wrong on largeToSmall(0)" );
        checkEquals( largeToSmallIter.next(), "E-0", "Wrong on largeToSmall(1)" );
        checkEquals( largeToSmallIter.next(), "E-2", "Wrong on largeToSmall(2)" );

        //
        
        smallToLarge.put( "E-3", 6 );
        smallToLarge.put( "E-4", 7 );
        smallToLarge.put( "E-5", 5 );
        smallToLarge.put( "E-6", 4 );
        smallToLarge.put( "E-7", 9 );
        smallToLarge.put( "E-8", 1 );
        smallToLarge.put( "E-9", 2 );
        
        largeToSmall.put( "E-3", 6 );
        largeToSmall.put( "E-4", 7 );
        largeToSmall.put( "E-5", 5 );
        largeToSmall.put( "E-6", 4 );
        largeToSmall.put( "E-7", 9 );
        largeToSmall.put( "E-8", 1 );
        largeToSmall.put( "E-9", 2 );
        
        checkEquals( smallToLarge.size(), 10, "Wrong size smallToLarge" );
        checkEquals( largeToSmall.size(), 10, "Wrong size largeToSmall" );

        smallToLargeIter = smallToLarge.iterator();
        checkEquals( smallToLargeIter.next(), "E-8", "Wrong on smallToLarge(0)" );
        checkEquals( smallToLargeIter.next(), "E-9", "Wrong on smallToLarge(1)" );
        checkEquals( smallToLargeIter.next(), "E-6", "Wrong on smallToLarge(2)" );
        checkEquals( smallToLargeIter.next(), "E-5", "Wrong on smallToLarge(3)" );
        checkEquals( smallToLargeIter.next(), "E-3", "Wrong on smallToLarge(4)" );
        checkEquals( smallToLargeIter.next(), "E-4", "Wrong on smallToLarge(5)" );
        checkEquals( smallToLargeIter.next(), "E-2", "Wrong on smallToLarge(6)" );
        checkEquals( smallToLargeIter.next(), "E-7", "Wrong on smallToLarge(7)" );
        checkEquals( smallToLargeIter.next(), "E-0", "Wrong on smallToLarge(8)" );
        checkEquals( smallToLargeIter.next(), "E-1", "Wrong on smallToLarge(9)" );
        
        largeToSmallIter = largeToSmall.iterator();
        checkEquals( largeToSmallIter.next(), "E-1", "Wrong on smallToLarge(0)" );
        checkEquals( largeToSmallIter.next(), "E-0", "Wrong on smallToLarge(1)" );
        checkEquals( largeToSmallIter.next(), "E-7", "Wrong on smallToLarge(2)" );
        checkEquals( largeToSmallIter.next(), "E-2", "Wrong on smallToLarge(3)" );
        checkEquals( largeToSmallIter.next(), "E-4", "Wrong on smallToLarge(4)" );
        checkEquals( largeToSmallIter.next(), "E-3", "Wrong on smallToLarge(5)" );
        checkEquals( largeToSmallIter.next(), "E-5", "Wrong on smallToLarge(6)" );
        checkEquals( largeToSmallIter.next(), "E-6", "Wrong on smallToLarge(7)" );
        checkEquals( largeToSmallIter.next(), "E-9", "Wrong on smallToLarge(8)" );
        checkEquals( largeToSmallIter.next(), "E-8", "Wrong on smallToLarge(9)" );
        
        //

        log.info( "Now let's overflow the thing (1)" );

        smallToLarge.put( "Y-0", 20 );
        smallToLarge.put( "Y-1", 20 );
        smallToLarge.put( "Y-2", 20 );

        largeToSmall.put( "X-0", -1 );
        largeToSmall.put( "X-1", -1 );
        largeToSmall.put( "X-2", -1 );

        checkEquals( smallToLarge.size(), 10, "Wrong size smallToLarge" );
        checkEquals( largeToSmall.size(), 10, "Wrong size largeToSmall" );
        
        checkEquals( smallToLarge.getFirst(), "E-8", "Wrong first smallToLarge" );
        checkEquals( smallToLarge.getLast(),  "E-1", "Wrong last smallToLarge" );
        checkEquals( largeToSmall.getFirst(), "E-1", "Wrong first largeToSmall" );
        checkEquals( largeToSmall.getLast(),  "E-8", "Wrong last largeToSmall" );
        
        //

        log.info( "Now let's overflow the thing (2)" );

        smallToLarge.put( "YY-0", 12 );
        smallToLarge.put( "YY-1", 12 );
        smallToLarge.put( "YY-2", 13 );
        smallToLarge.put( "YY-3", 12 );

        largeToSmall.put( "XX-0", 1 );
        largeToSmall.put( "XX-1", 1 );
        largeToSmall.put( "XX-2", 0 );
        largeToSmall.put( "XX-3", 1 );

        checkEquals( smallToLarge.size(), 13, "Wrong size smallToLarge" );
        checkEquals( largeToSmall.size(), 13, "Wrong size largeToSmall" );

        checkEquals( smallToLarge.getFirst(), "E-8", "Wrong first smallToLarge" );
        checkEquals( smallToLarge.getLast(),  "YY-3", "Wrong last smallToLarge" );
        checkEquals( largeToSmall.getFirst(), "E-1", "Wrong first largeToSmall" );
        checkEquals( largeToSmall.getLast(),  "XX-3", "Wrong last largeToSmall" );

        //
        
        log.info( "Now let's overflow the thing (3)" );

        smallToLarge.put( "YYY-0", 5 );
        largeToSmall.put( "YYY-0", 5 );

        checkEquals( smallToLarge.size(), 10, "Wrong size smallToLarge" );
        checkEquals( largeToSmall.size(), 10, "Wrong size largeToSmall" );

        checkEquals( smallToLarge.getFirst(), "E-8", "Wrong first smallToLarge" );
        checkEquals( smallToLarge.getLast(),  "E-0", "Wrong last smallToLarge" );
        checkEquals( largeToSmall.getFirst(), "E-1", "Wrong first largeToSmall" );
        checkEquals( largeToSmall.getLast(),  "E-9", "Wrong last largeToSmall" );
    }

    private static final Log log = Log.getLogInstance( OrderedMSetWithValueTest1.class ); // our own, private logger
}
