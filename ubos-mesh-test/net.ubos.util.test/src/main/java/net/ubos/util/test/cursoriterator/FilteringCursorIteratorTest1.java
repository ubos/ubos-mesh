//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the FilteringCursorIterator.
 */
public class FilteringCursorIteratorTest1
        extends
            AbstractCursorIteratorTest1
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
            "a",    //  [0]   -
            "b+A",  //  [1]  [0]
            "c",    //  [2]   -
            "d+B",  //  [3]  [1]
            "e+C",  //  [4]  [2]
            "f",    //  [5]   -
            "g",    //  [6]   -
            "h+D",  //  [7]  [3]
            "i+E",  //  [8]  [4]
            "j+F",  //  [9]  [5]
            "k+G",  // [10]  [6]
            "l+H",  // [11]  [7]
        };
        String [] testData = new String[] {
            "b+A",  //  [1]  [0]
            "d+B",  //  [3]  [1]
            "e+C",  //  [4]  [2]
            "h+D",  //  [7]  [3]
            "i+E",  //  [8]  [4]
            "j+F",  //  [9]  [5]
            "k+G",  // [10]  [6]
            "l+H",  // [11]  [7]
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );

        FilteringCursorIterator<String> filterIterator = FilteringCursorIterator.create(
                baseIterator,
                (String s) -> {
                    boolean ret = s.contains("+");
                    return ret;
                } );

        runWith( getClass().getName() + ": ", testData, filterIterator, log );
    }

    private static final Log log = Log.getLogInstance( FilteringCursorIteratorTest1.class ); // our own, private logger
}
