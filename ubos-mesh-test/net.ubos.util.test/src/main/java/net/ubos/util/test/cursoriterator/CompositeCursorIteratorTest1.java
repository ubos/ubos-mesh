//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import java.util.ArrayList;
import java.util.List;
import net.ubos.util.cursoriterator.ArrayListCursorIterator;
import net.ubos.util.cursoriterator.CompositeCursorIterator;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the CompoundCursorIterator. This is the substantially the same test as
 * ArrayCursorIteratorTest1.
 */
public class CompositeCursorIteratorTest1
        extends
            AbstractCursorIteratorTest1
{
    @Test
    public void run()
        throws
            Exception
    {
        String [] testData = new String[] {
            "a", // [0]
            "b", // [1]
            "c", // [2]
            "d", // [3]
            "e", // [4]
            "f", // [5]
            "g", // [6]
            "h"  // [7]
        };

        ArrayList<String> collection1 = new ArrayList<>();
        ArrayList<String> collection2 = new ArrayList<>();
        ArrayList<String> collection3 = new ArrayList<>();
        ArrayList<String> collection4 = new ArrayList<>();

        collection1.add( testData[0] );
        collection1.add( testData[1] );
        // nothing in #2
        collection3.add( testData[2] );
        collection3.add( testData[3] );
        collection3.add( testData[4] );
        collection3.add( testData[5] );
        collection3.add( testData[6] );
        collection4.add( testData[7] );

        ArrayListCursorIterator<String> iter1 = ArrayListCursorIterator.create( collection1 );
        ArrayListCursorIterator<String> iter2 = ArrayListCursorIterator.create( collection2 );
        ArrayListCursorIterator<String> iter3 = ArrayListCursorIterator.create( collection3 );
        ArrayListCursorIterator<String> iter4 = ArrayListCursorIterator.create( collection4 );

        List<CursorIterator<String>> iterators = new ArrayList<>();
        iterators.add( iter1 );
        iterators.add( iter2 );
        iterators.add( iter3 );
        iterators.add( iter4 );

        CompositeCursorIterator<String> iter = CompositeCursorIterator.create( iterators );

        runWith( getClass().getName() + ": ", testData, iter, log );
    }

    private static final Log log = Log.getLogInstance( CompositeCursorIteratorTest1.class  ); // our own, private logger
}
