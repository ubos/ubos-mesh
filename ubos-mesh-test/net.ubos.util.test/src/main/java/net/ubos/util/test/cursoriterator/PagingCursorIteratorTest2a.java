//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.util.test.cursoriterator;

import net.ubos.util.cursoriterator.ArrayCursorIterator;
import net.ubos.util.cursoriterator.PagingCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the PagingCursorIterator.
 * The underlying iterator has plenty of elements, but the page size is 1.
 */
public class PagingCursorIteratorTest2a
        extends
            AbstractCursorIteratorTest2
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] baseData = new String[] {
            "a",  //  [0]   -
            "b",  //  [1]   -
            "c",  //  [2]   -
            "d",  //  [3]  [0]
            "e",  //  [4]   -
            "f",  //  [5]   -
        };
        String [] testData = new String[] {
            "d",  //  [1]  [0]
        };

        ArrayCursorIterator<String> baseIterator = ArrayCursorIterator.create( baseData );
        baseIterator.moveToBefore( baseData[3] );

        PagingCursorIterator<String> pagingIterator = PagingCursorIterator.create(
                testData.length,
                baseIterator );

        runWith( testData[0], pagingIterator, log );
    }

    private static final Log log = Log.getLogInstance( PagingCursorIteratorTest2a.class ); // our own, private logger
}
