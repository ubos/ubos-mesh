//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.mesh.externalized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanDataType;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorDataType;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyDataType;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.DataType;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.ExtentDataType;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatDataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerDataType;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.L10PropertyValueMap;
import net.ubos.model.primitives.MultiplicityDataType;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointDataType;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodDataType;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampDataType;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.externalized.PropertyValueDecoder;
import net.ubos.model.primitives.externalized.PropertyValueEncoder;
import net.ubos.model.primitives.externalized.json.PropertyValueJsonDecoder;
import net.ubos.model.primitives.externalized.json.PropertyValueJsonEncoder;
import net.ubos.model.primitives.externalized.xml.PropertyValueXmlDecoder;
import net.ubos.model.primitives.externalized.xml.PropertyValueXmlEncoder;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests PropertyValue serialization and deserialization using the known Encoders/Decoders
 *
 * NOTE: ALL testS WITH UNIT IN THEM ARE COMMENTED OUT AS THE SERIALIZER
 * CURRENTLY DOES NOT SUPPORT UNITS!!!
 */
public class PropertyValueEncodingDecodingTest1
        extends
            AbstractSerializerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @Test
    public void run()
            throws
                Exception
    {
        log.info( "Starting test " + getClass().getName() );

        runWith( PropertyValueXmlEncoder.create(), PropertyValueXmlDecoder.create());
        runWith( PropertyValueJsonEncoder.create(), PropertyValueJsonDecoder.create());
    }

    /**
     * Run the test with a particular combination of encoder and decoder.
     *
     * @param encoder the Encoder
     * @param decoder the Decoder
     */
     protected void runWith(
             PropertyValueEncoder encoder,
             PropertyValueDecoder decoder )
     {
        for( int i=0 ; i<testData.length ; ++i ) {
            for( int j=0 ; j<testData[i].length ; ++j ) {
                log.info( "Testing " + testData[i][j].theType.getClass().getName() );

                for( int k=0 ; k<testData[i][j].theValues.length ; ++k ) {

                    PropertyValue original = testData[i][j].theValues[k];
                    PropertyValue decoded  = null;

                    byte [] encoded = null;
                    try {
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        OutputStreamWriter    w   = new OutputStreamWriter( out );

                        encoder.writePropertyValue( original, w );
                        w.flush();
                        out.close();

                        encoded = out.toByteArray();

                        log.info( "value: \"" + original + "\", serialized: \"" + encoded + "\"" );

                        ByteArrayInputStream in = new ByteArrayInputStream( encoded );

                        decoded = decoder.parsePropertyValue( testData[i][j].theType, new InputStreamReader( in, StandardCharsets.UTF_8 ));

                        checkEquals( original, decoded, "incorrect deserialization at index " + i + "/" + j + "/" + k );

                    } catch( Throwable ex ) {
                        ++theErrorCount;
                        reportError( "element", j, original, encoded, ( (encoded == null) ? "encoding" : "decoding" ), ex );
                        checkEquals( original, decoded, "what we received" );
                    }
                }
            }
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(PropertyValueEncodingDecodingTest1.class );

   /**
     * The test cases for BooleanValue
     */
    protected static TestCase [] booleanValueTestData = new TestCase [] {
            new TestCase(
                    BooleanDataType.theDefault,
                    new BooleanValue[] {
                         BooleanValue.FALSE,
                         BooleanValue.TRUE
                    } )
    };

   /**
     * The test cases for ColorValue
     */
    protected static TestCase [] colorValueTestData = new TestCase[] {
            new TestCase(
                    ColorDataType.theDefault,
                    new ColorValue[] {
                         ColorValue.create(  0 ),
                         ColorValue.create(  1 ),
                         ColorValue.create( -1 ),
                         ColorValue.create(  255 ),
                         ColorValue.create( -255 ),
                         ColorValue.create(  256 ),
                         ColorValue.create( -256 ),
                         ColorValue.create(  Integer.MAX_VALUE ),
                         ColorValue.create( -Integer.MAX_VALUE )
                    } )
    };

   /**
     * The test cases for CurrencyValue
     */
    protected static TestCase [] currencyValueTestData = new TestCase[] {
            new TestCase(
                    CurrencyDataType.theDefault,
                    new CurrencyValue[] {
                         CurrencyValue.create( true,    1,  1, CurrencyDataType.USD ),
                         CurrencyValue.create( true,    1,  0, CurrencyDataType.USD ),
                         CurrencyValue.create( true,    1,  1, CurrencyDataType.USD ),
                         CurrencyValue.create( true,  123, 45, CurrencyDataType.USD ),
                         CurrencyValue.create( true,  123, 45, CurrencyDataType.EUR ),
                         CurrencyValue.create( false,   1,  1, CurrencyDataType.USD ),
                         CurrencyValue.create( false,   1,  0, CurrencyDataType.USD ),
                         CurrencyValue.create( false,   1,  1, CurrencyDataType.USD ),
                         CurrencyValue.create( false, 123, 45, CurrencyDataType.USD ),
                         CurrencyValue.create( false, 123, 45, CurrencyDataType.EUR ),
                    } )
    };

    /**
     * The test cases for EnumeratedValue
     */
    protected static TestCase [] enumeratedValueTestData = new TestCase[] {
//            new TestCase(
//                    EnumeratedDataType.theDefault,
//                    new EnumeratedValue[] {
//                        EnumeratedValue.createFromRfc3339( null, "a", null, null ),
//                        EnumeratedValue.createFromRfc3339( null, "abcdefgh ijklmnopqrstuv wxyz ABCDEF GHIJKLM NOPQRST UVWXYZ 1234 5678 9 0", null, null )
//                    } )
// Took out the testing of the DataType pointer from the EnumeratedValue. FIXME?
            new EnumeratedTestCase(
                    EnumeratedDataType.create(
                            new String[] {
                                    "a",
                                    "abcdefgh ijklmnopqrstuv wxyz ABCDEF GHIJKLM NOPQRST UVWXYZ 1234 5678 9 0" },
                            new L10PropertyValueMap[] {
                                    null,
                                    null },
                            null,
                            EnumeratedDataType.theDefault )
            )
    };

   /**
     * The test cases for ExtentValue
     */
    protected static TestCase [] extentValueTestData = new TestCase[] {
            new TestCase(
                    ExtentDataType.theDefault,
                    new ExtentValue[] {
                         ExtentValue.create(     0.,    0. ),
                         ExtentValue.create(     1.,    0. ),
                         ExtentValue.create(     0.,    1. ),
                         ExtentValue.create(     1.,    1. ),
                         ExtentValue.create(     1.,    0. ),
                         ExtentValue.create(     0.,    1. ),
                         ExtentValue.create(     1.,    1. ),
                         ExtentValue.create(     1.,    1. ),
                         ExtentValue.create(     1.,    1. ),
                         ExtentValue.create(     0., 1000. ),
                         ExtentValue.create(  2000., 3000. ),
                         ExtentValue.create(  4000., 5000. ),
                         ExtentValue.create(  6000., 7000. ),
                         ExtentValue.create(  0.              , Double.MIN_VALUE ),
                         ExtentValue.create(  Double.MIN_VALUE, 0. ),
                         ExtentValue.create(  Double.MIN_VALUE, Double.MIN_VALUE ),
                         ExtentValue.create(  0.              , Double.MIN_VALUE ),
                         ExtentValue.create(  Double.MIN_VALUE, 0. ),
                         ExtentValue.create(  Double.MIN_VALUE, Double.MIN_VALUE ),
                         ExtentValue.create(  Double.MIN_VALUE, Double.MIN_VALUE ),
                         ExtentValue.create(  Double.MIN_VALUE, Double.MIN_VALUE ),
                         ExtentValue.create(  0.              , Double.MAX_VALUE ),
                         ExtentValue.create(  Double.MAX_VALUE, 0. ),
                         ExtentValue.create(  Double.MAX_VALUE, Double.MAX_VALUE ),
                         ExtentValue.create(  0.              , Double.MAX_VALUE ),
                         ExtentValue.create(  Double.MAX_VALUE, 0. ),
                         ExtentValue.create(  Double.MAX_VALUE, Double.MAX_VALUE ),
                         ExtentValue.create(  Double.MAX_VALUE, Double.MAX_VALUE ),
                         ExtentValue.create(  Double.MAX_VALUE, Double.MAX_VALUE ),
                         ExtentValue.create(  Double.MIN_VALUE, Double.MAX_VALUE ),
                         ExtentValue.create(  Double.MAX_VALUE, Double.MIN_VALUE )
                    } )
    };

    /**
     * The test cases for FloatValue
     */
    protected static TestCase [] floatValueTestData = new TestCase[] {
            new TestCase(
                    FloatDataType.theDefault,
                    new FloatValue[] {
                         FloatValue.create(   0. ),
                         FloatValue.create(   1. ),
                         FloatValue.create(  -1. ),
                         FloatValue.create(  10. ),
                         FloatValue.create( -10. ),
                         FloatValue.create(  Float.MAX_VALUE ),
                         FloatValue.create( -Float.MAX_VALUE ),
                         FloatValue.create(  Float.MIN_VALUE ),
                         FloatValue.create( -Float.MIN_VALUE ),
                         FloatValue.create(   1./Float.MAX_VALUE ),
                         FloatValue.create(  -1./Float.MAX_VALUE ),
                         FloatValue.create(   1./Float.MIN_VALUE ),
                         FloatValue.create(  -1./Float.MIN_VALUE )
                         // FIXME FloatValue.createFromRfc3339(   0., Unit.theMeterUnit ),
                         // FIXME FloatValue.createFromRfc3339(   1., Unit.theMileUnit ),
                         // FIXME FloatValue.createFromRfc3339(  -1., Unit.theGramUnit ),
                         // FIXME FloatValue.createFromRfc3339(  10., Unit.theMicroampereUnit ),
                         // FIXME FloatValue.createFromRfc3339( -10., Unit.theGigabyteUnit ),
                    } ),
        new TestCase(
                FloatDataType.thePositiveDefault,
                new FloatValue[] {
                     FloatValue.create(   0. ),
                     FloatValue.create(   1. ),
                     FloatValue.create(  10. ),
                     FloatValue.create(  Float.MAX_VALUE ),
                     FloatValue.create(   1./Float.MAX_VALUE ),
                     // FIXME FloatValue.createFromRfc3339(   0., Unit.theMeterUnit ),
                     // FIXME FloatValue.createFromRfc3339(   1., Unit.theMileUnit ),
                     // FIXME FloatValue.createFromRfc3339(  10., Unit.theMicroampereUnit ),
                } )
    };

    /**
     * The test cases for IntegerValues
     */
    protected static TestCase [] integerValueTestData = new TestCase [] {
            new TestCase(
                    IntegerDataType.theDefault,
                    new IntegerValue[] {
                         IntegerValue.create( 0 ),
                         IntegerValue.create( 1 ),
                         IntegerValue.create( 10 ),
                         IntegerValue.create( Integer.MAX_VALUE ),
                         IntegerValue.create( -1 ),
                         IntegerValue.create( -10 ),
                         IntegerValue.create( Integer.MIN_VALUE )
                    } )
    };

   /**
     * The test cases for MultiplicityValue
     */
    protected static TestCase [] multiplicityValueTestData = new TestCase[] {
            new TestCase(
                    MultiplicityDataType.theDefault,
                    new MultiplicityValue[] {
                         MultiplicityValue.create( 0, 1 ),
                         MultiplicityValue.create( 0, 2 ),
                         MultiplicityValue.create( 0, 10 ),
                         MultiplicityValue.create( 0, MultiplicityValue.N ),
                         MultiplicityValue.create( 1, 1 ),
                         MultiplicityValue.create( 1, 2 ),
                         MultiplicityValue.create( 1, 10 ),
                         MultiplicityValue.create( 2, MultiplicityValue.N ),
                         MultiplicityValue.create( 2, 2 ),
                         MultiplicityValue.create( 2, 10 ),
                         MultiplicityValue.create( 2, MultiplicityValue.N ),
                         MultiplicityValue.create( 10, 10 ),
                         MultiplicityValue.create( 10, MultiplicityValue.N )
                    } )
    };

   /**
     * The test cases for PointValue
     */
    protected static TestCase [] pointValueTestData = new TestCase[] {
            new TestCase(
                    PointDataType.theDefault,
                    new PointValue[] {
                         PointValue.create(  0.   ,  0. ),
                         PointValue.create(  1.   ,  0. ),
                         PointValue.create(  0.   ,  1. ),
                         PointValue.create(  1.   ,  1. ),
                         PointValue.create( -1.   ,  0. ),
                         PointValue.create(  0.   , -1. ),
                         PointValue.create( -1.   , -1. ),
                         PointValue.create( -1.   ,  1. ),
                         PointValue.create(  1.   , -1. ),
                         PointValue.create(     0., -1000. ),
                         PointValue.create( -2000., -3000. ),
                         PointValue.create( -4000.,  5000. ),
                         PointValue.create(  6000., -7000. ),
                         PointValue.create(  0.              ,  Double.MIN_VALUE ),
                         PointValue.create(  Double.MIN_VALUE,  0. ),
                         PointValue.create(  Double.MIN_VALUE,  Double.MIN_VALUE ),
                         PointValue.create(  0.              , -Double.MIN_VALUE ),
                         PointValue.create( -Double.MIN_VALUE,  0. ),
                         PointValue.create( -Double.MIN_VALUE, -Double.MIN_VALUE ),
                         PointValue.create(  Double.MIN_VALUE, -Double.MIN_VALUE ),
                         PointValue.create( -Double.MIN_VALUE,  Double.MIN_VALUE ),
                         PointValue.create(  0.              ,  Double.MAX_VALUE ),
                         PointValue.create(  Double.MAX_VALUE,  0. ),
                         PointValue.create(  Double.MAX_VALUE,  Double.MAX_VALUE ),
                         PointValue.create(  0.              , -Double.MAX_VALUE ),
                         PointValue.create( -Double.MAX_VALUE,  0. ),
                         PointValue.create( -Double.MAX_VALUE, -Double.MAX_VALUE ),
                         PointValue.create(  Double.MAX_VALUE, -Double.MAX_VALUE ),
                         PointValue.create( -Double.MAX_VALUE,  Double.MAX_VALUE ),
                         PointValue.create(  Double.MIN_VALUE,  Double.MAX_VALUE ),
                         PointValue.create(  Double.MAX_VALUE,  Double.MIN_VALUE )
                    } )
    };

    /**
     * The test cases for StringValue
     */
    protected static final StringBuilder testStringBuilder = new StringBuilder( 255 );
    static {
        // according to https://stackoverflow.com/questions/4237625/removing-invalid-xml-characters-from-a-string-in-java#4237934
        // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
        // tests the whole nine yards ... but not supplemental (>16 bit)

        testStringBuilder.append( 0x9 );
        testStringBuilder.append( 0xa );
        testStringBuilder.append( 0xd );


        for( char c = 0x20 ; c <= 0xd7ff ; ++c ) {
            testStringBuilder.append( c );
        }
        for( char c = 0xe000 ; c <= 0xfffd ; ++c ) {
            testStringBuilder.append( c );
        }
    }
    protected static final String testString = new String( testStringBuilder );
    protected static final TestCase [] stringValueTestData = new TestCase [] {
            new TestCase(
                    StringDataType.theDefault,
                    new StringValue[] {
                         StringValue.create( "a" ),
                         StringValue.create( "" ),
                         StringValue.create( "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" ),
                         StringValue.create( "ab<def>ghi" ),
                         StringValue.create( "a\\b" ),
                         StringValue.create( "a\tb" ),
                         StringValue.create( "a\nb" ),
                         StringValue.create( testString ),
                         StringValue.create( testString + testString + testString + testString )
                    } )
    };

    /**
     * The test cases for BlobValue
     */
    protected static final byte [] testBytesShort = new byte[ 17 ]; // one more than 16
    protected static final byte [] testBytesLong = new byte[ 1 << 17 ];
    static {
        for( int i=0 ; i<testBytesShort.length ; ++i ) {
            testBytesShort[i] = (byte) ( i % 256 );
        }
        for( int i=0 ; i<testBytesLong.length ; ++i ) {
            testBytesLong[i] = (byte) ( i % 256 );
        }
    }

    protected static TestCase [] blobValueTestData = new TestCase[] {
            new TestCase(
                    BlobDataType.theAnyType,
                    new BlobValue[] {
                         BlobDataType.theAnyType.createBlobValue( "a", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( "", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( testString, SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( testString + testString + testString + testString, SelectableMimeType.TEXT_PLAIN.getMimeType() ),

                         BlobDataType.theAnyType.createBlobValue( "a", SelectableMimeType.TEXT_HTML.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( "", SelectableMimeType.TEXT_HTML.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", SelectableMimeType.TEXT_HTML.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( testString, SelectableMimeType.TEXT_HTML.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( testString + testString + testString + testString, SelectableMimeType.TEXT_HTML.getMimeType() ),

                         BlobDataType.theAnyType.createBlobValue( testBytesShort, SelectableMimeType.OCTET_STREAM.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( testBytesLong, SelectableMimeType.OCTET_STREAM.getMimeType() ),

                         BlobDataType.theAnyType.createBlobValue( new byte[] { (byte) 1 }, SelectableMimeType.OCTET_STREAM.getMimeType() ),
                         BlobDataType.theAnyType.createBlobValue( new byte[] { (byte) 11 }, SelectableMimeType.OCTET_STREAM.getMimeType() )
                    } ),
        new TestCase(
                BlobDataType.theTextAnyType,
                new BlobValue[] {
                     BlobDataType.theTextAnyType.createBlobValue( "a", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                     BlobDataType.theTextAnyType.createBlobValue( "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                     BlobDataType.theTextAnyType.createBlobValue( testString, SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                     BlobDataType.theTextAnyType.createBlobValue( testString + testString + testString + testString, SelectableMimeType.TEXT_PLAIN.getMimeType() ),

                     BlobDataType.theTextAnyType.createBlobValue( "a", SelectableMimeType.TEXT_HTML.getMimeType() ),
                     BlobDataType.theTextAnyType.createBlobValue( "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", SelectableMimeType.TEXT_HTML.getMimeType() ),
                     BlobDataType.theTextAnyType.createBlobValue( testString, SelectableMimeType.TEXT_HTML.getMimeType() ),
                     BlobDataType.theTextAnyType.createBlobValue( testString + testString + testString + testString, SelectableMimeType.TEXT_HTML.getMimeType() ),
                } ),
        new TestCase(
                BlobDataType.theTextHtmlType,
                new BlobValue[] {
                     BlobDataType.theTextHtmlType.createBlobValue( testString, "text/html" ),
                } ),
    };

    /**
     * The test cases for TimeStampValue.
     */
    protected static TestCase [] timeStampValueTestData = new TestCase[] {
            new TestCase(
                    TimeStampDataType.theDefault,
                    new TimeStampValue[] {
                         TimeStampValue.create( (short) 1900, (short)  1, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 1900, (short)  1, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 1900, (short)  1, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimeStampValue.create( (short) 1900, (short)  1, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 1900, (short)  1, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 1900, (short)  1, (short) 31, (short) 23, (short) 59, 59.99876789f ),
                         TimeStampValue.create( (short) 1900, (short) 12, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 1900, (short) 12, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 1900, (short) 12, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimeStampValue.create( (short) 1900, (short) 12, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 1900, (short) 12, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 1900, (short) 12, (short) 31, (short) 23, (short) 59, 59.99876789f ),

                         TimeStampValue.create( (short) 2345, (short)  1, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 2345, (short)  1, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 2345, (short)  1, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimeStampValue.create( (short) 2345, (short)  1, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 2345, (short)  1, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 2345, (short)  1, (short) 31, (short) 23, (short) 59, 59.99876789f ),
                         TimeStampValue.create( (short) 2345, (short) 12, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 2345, (short) 12, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 2345, (short) 12, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimeStampValue.create( (short) 2345, (short) 12, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimeStampValue.create( (short) 2345, (short) 12, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimeStampValue.create( (short) 2345, (short) 12, (short) 31, (short) 23, (short) 59, 59.99876789f )
            } )
    };

    /**
     * The test cases for TimeStampValue.
     */
    protected static TestCase [] timePeriodValueTestData = new TestCase[] {
            new TestCase(
                    TimePeriodDataType.theDefault,
                    new TimePeriodValue[] {
                         TimePeriodValue.create( (short) 1900, (short)  1, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 1900, (short)  1, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 1900, (short)  1, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimePeriodValue.create( (short) 1900, (short)  1, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 1900, (short)  1, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 1900, (short)  1, (short) 31, (short) 23, (short) 59, 59.99876789f ),
                         TimePeriodValue.create( (short) 1900, (short) 12, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 1900, (short) 12, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 1900, (short) 12, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimePeriodValue.create( (short) 1900, (short) 12, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 1900, (short) 12, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 1900, (short) 12, (short) 31, (short) 23, (short) 59, 59.99876789f ),

                         TimePeriodValue.create( (short) 2345, (short)  1, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 2345, (short)  1, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 2345, (short)  1, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimePeriodValue.create( (short) 2345, (short)  1, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 2345, (short)  1, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 2345, (short)  1, (short) 31, (short) 23, (short) 59, 59.99876789f ),
                         TimePeriodValue.create( (short) 2345, (short) 12, (short)  1, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 2345, (short) 12, (short)  1, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 2345, (short) 12, (short)  1, (short) 23, (short) 59, 59.99876789f ),
                         TimePeriodValue.create( (short) 2345, (short) 12, (short) 31, (short)  0, (short)  0, 0.f ),
                         TimePeriodValue.create( (short) 2345, (short) 12, (short) 31, (short)  1, (short)  2, 3.4f ),
                         TimePeriodValue.create( (short) 2345, (short) 12, (short) 31, (short) 23, (short) 59, 59.99876789f )
            } )
    };

    /**
      * all test cases put together
      */
    protected static TestCase [] [] testData = {
//            blobValueTestData,
//            booleanValueTestData,
//            colorValueTestData,
//            currencyValueTestData,
//            enumeratedValueTestData,
//            extentValueTestData,
//            floatValueTestData,
//            integerValueTestData,
//            multiplicityValueTestData,
//            pointValueTestData,
            stringValueTestData,
//            timeStampValueTestData,
//            timePeriodValueTestData
    };

    /**
     * Groups together the PropertyValues we are testing with their
     * corresponding DataTypes.
     */
    public static class TestCase
    {
        /**
         * Constructor.
         *
         * @param type the DataType to test
         * @param values the PropertyValues to be tested with this DataType.
         */
        public TestCase(
                DataType         type,
                PropertyValue [] values )
        {
            theType   = type;
            theValues = values;
        }

        /**
         * DataType involved in this set of test cases.
         */
        public DataType theType;

        /**
         * PropertyValues, each of which is a test case in this set.
         */
        public PropertyValue [] theValues;
    }

    /**
     * Special setup for EnumeratedDataTypes.
     */
    public static class EnumeratedTestCase
            extends
                TestCase
    {
        /**
         * Constructor.
         *
         * @param type the EnumeratedDataType to test, which also defines the PropertyValues
         */
        public EnumeratedTestCase(
                EnumeratedDataType type )
        {
            super( type, type.getDomain() );
        }
    }
}
