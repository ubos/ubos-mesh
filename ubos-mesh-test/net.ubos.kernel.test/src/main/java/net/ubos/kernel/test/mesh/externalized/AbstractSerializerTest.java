//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.mesh.externalized;

import net.ubos.testharness.AbstractTest;

/**
 * Factors out common functionality of SerializerTests.
 */
public abstract class AbstractSerializerTest
        extends
            AbstractTest
{
}
