//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.importer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Creates a simple history of changes to a MeshBase. Exports them.
 * Re-imports them, and runs the differencer.
 *
 * Role Attributes
 */
public class ExportImportTest2
    extends
        AbstractExportImportTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshObjects" );

        startClock();

        MMeshBase mb1 = MMeshBase.Builder.create().history( true ).namespaceMap( thePrimaryNsMap ).build();

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.createMeshObject( "a" );
            MeshObject b = mb1.createMeshObject( "b" );

            a.setRoleAttributeValue( b, "a->b", "From a to b" );
        } );

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.findMeshObjectByIdentifier( "a" );
            MeshObject b = mb1.findMeshObjectByIdentifier( "b" );

            b.setRoleAttributeValue( a, "b->a", "From b to a" );

            MeshObject c = mb1.createMeshObject( "c" );
        } );

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.findMeshObjectByIdentifier( "a" );
            MeshObject b = mb1.findMeshObjectByIdentifier( "b" );
            MeshObject c = mb1.findMeshObjectByIdentifier( "c" );

            a.setRoleAttributeValue( c, "a->c", "From a to c" );
            a.deleteRoleAttribute( b, "a->b" );

        } );

        runExportImportCompare( mb1 );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ExportImportTest2.class);
}
