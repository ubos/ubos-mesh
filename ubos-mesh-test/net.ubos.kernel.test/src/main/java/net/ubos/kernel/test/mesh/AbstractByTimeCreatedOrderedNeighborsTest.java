//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.mesh;

import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

/**
 * Functionality common to ByTimeCreatedOrderedNeighborsTests.
 */
public abstract class AbstractByTimeCreatedOrderedNeighborsTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        theMeshBase = MMeshBase.Builder.create().build();
    }

    /**
     * Tear down
     */
    @AfterEach
    public void tearDown()
    {
        theMeshBase.die();
        theMeshBase = null;
    }

    /**
     * The MeshBase under test.
     */
    protected MeshBase theMeshBase;
}
