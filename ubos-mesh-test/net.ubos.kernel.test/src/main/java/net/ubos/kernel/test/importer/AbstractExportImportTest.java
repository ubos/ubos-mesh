//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.importer;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import net.ubos.importer.ImporterException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.externalized.json.DefaultMeshBaseJsonImporter;
import net.ubos.meshbase.externalized.json.MeshBaseJsonEncoder;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.externalized.EncodingException;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality for tests of MeshBase importing and exporting.
 */
public abstract class AbstractExportImportTest
        extends
            AbstractTest
{
    private static final Log log = Log.getLogInstance(AbstractExportImportTest.class );

    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        thePrimaryNsMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();
    }

    /**
     * Export from the provided MeshBase, re-import into a new MeshBase, and compare.
     *
     * @param mb1 the provided MeshBase
     * @throws IOException
     * @throws EncodingException
     * @throws ImporterException
     */
    protected void runExportImportCompare(
            MeshBase mb1 )
        throws
            IOException,
            EncodingException,
            ImporterException
    {
        int mbSize = mb1.size();
        int mbHistorySize = mb1.meshObjectHistoriesIterator().moveToAfterLast();
        int historyLength = mb1.getHistory().getLength();

        //

        log.info( "Exporting" );

        MeshBaseJsonEncoder encoder = MeshBaseJsonEncoder.create();
        encoder.setExportHistory( true );
        encoder.setExportTransactions( true );

        ContextualMeshObjectIdentifierNamespaceMap encodingNsMap1
                = MContextualMeshObjectIdentifierNamespaceMap.create( thePrimaryNsMap );

        StringWriter w1 = new StringWriter();
        encoder.bulkWrite( mb1, encodingNsMap1, w1 );

        // log.info( "=== One ===\n" + w1.toString() );

        //

        log.info( "Importing" );

        MMeshBase mb2 = MMeshBase.Builder.create().history( true ).namespaceMap( thePrimaryNsMap ).build();

        DefaultMeshBaseJsonImporter decoder = DefaultMeshBaseJsonImporter.create();
        decoder.importTo( new StringReader( w1.toString() ), mb2 );

        //

        log.info( "Re-exporting" );

        ContextualMeshObjectIdentifierNamespaceMap encodingNsMap2
                = MContextualMeshObjectIdentifierNamespaceMap.create( thePrimaryNsMap );

        StringWriter w2 = new StringWriter();
        encoder.bulkWrite( mb2, encodingNsMap2, w2 );

        // log.info( "=== Two ===\n" + w2.toString() );

        //

        log.info( "Checking overall" );

        checkEquals( mb2.size(), mbSize, "Wrong MeshBase size" );
        checkEquals( mb2.meshObjectHistoriesIterator().moveToAfterLast(), mbHistorySize, "Wrong MeshObjectHistories size" );
        checkEquals( mb2.getHistory().getLength(), historyLength, "Wrong length of MeshBase history");

        log.info( "Checking HEAD" );

        for( MeshObject obj1 : mb1 ) {
            MeshObject obj2 = mb2.findMeshObjectByIdentifier( obj1.getIdentifier() );

            checkMeshObjectEquals( obj1, obj2, "Not equal" );
        }

        log.info( "Checking MeshObjectHistories" );

        for( MeshObjectHistory hist1 : mb1.meshObjectHistoriesIterator() ) {
            MeshObjectHistory hist2 = mb2.meshObjectHistory( hist1.getMeshObjectIdentifier() );

            checkEquals( hist1.getLength(), hist2.getLength(), "Unequal history length for: " + hist1.getMeshObjectIdentifier() );

            HistoryCursorIterator<MeshObject> histIter1 = hist1.iterator();
            HistoryCursorIterator<MeshObject> histIter2 = hist2.iterator();

            while( histIter1.hasNext() ) {
                MeshObject obj1 = histIter1.next();
                MeshObject obj2 = histIter2.next();

                checkMeshObjectEquals( obj1, obj2, "Not equal" );
            }
        }

        log.info( "Checking MeshBaseHistory" );

        HistoryCursorIterator<MeshBaseState> histIter1 = mb1.getHistory().iterator();
        HistoryCursorIterator<MeshBaseState> histIter2 = mb2.getHistory().iterator();

        checkEquals( histIter1.moveToAfterLast(), histIter2.moveToAfterLast(), "Different lengths" );

        histIter1.moveToBeforeFirst();
        histIter2.moveToBeforeFirst();

        while( histIter1.hasNext() ) {
            MeshBaseState state1 = histIter1.next();
            MeshBaseState state2 = histIter2.next();
            checkNotIdentity( state1, state2, "Same instance" );

            checkEquals( state1.getTimeUpdated(), state2.getTimeUpdated(), "Wrong time updated" );

            checkEqualsOutOfSequence( state1.getChangeList().getChanges().toArray(), state2.getChangeList().getChanges().toArray(), "Changes different" );
            // they are only partially ordered, not fully ordered

            HistoricMeshBaseView view1 = state1.getMeshBaseView();
            HistoricMeshBaseView view2 = state2.getMeshBaseView();

            checkEquals( view1.size(), view2.size(), "Different size views" );
            for( MeshObject obj1 : view1.iterator() ) {
                MeshObject obj2 = view2.findMeshObjectByIdentifier( obj1.getIdentifier() );

                checkMeshObjectEquals( obj1, obj2, "Not equal: " + obj1.getIdentifier() );
            }
        }

        //

        log.info( "Diff'ing HEAD" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( mb1 );
        ChangeList changes = diff.determineChangeList( mb2 );

        checkEquals( changes.size(), 0, "Difference found" );
    }

    /**
     * Helper to test equality of two MeshObjects.
     */
    protected void checkMeshObjectEquals(
            MeshObject one,
            MeshObject two,
            String     msg )
    {
        checkEquals( one.getIdentifier(), two.getIdentifier(), msg + ": identifier" );

        checkEquals( one.getIsDead(), two.getIsDead(), msg + ": dead" );
        if( one.getIsDead() ) {
            return;
        }

        checkEquals( one.getTimeCreated(), two.getTimeCreated(), "Different creation times" );
        checkEquals( one.getTimeUpdated(), two.getTimeUpdated(), "Different update times" );

        checkEqualsInSequence( one.getOrderedAttributeNames(), two.getOrderedAttributeNames(), msg + ": attributes" );
        checkEqualsInSequence( one.getAttributeValues( one.getOrderedAttributeNames() ), two.getAttributeValues( two.getOrderedAttributeNames() ), msg + ": attribute values" );

        checkEqualsOutOfSequence( one.getEntityTypes(), two.getEntityTypes(), msg + ": EntityTypes" );
        checkEqualsOutOfSequence( one.getProperties(),  two.getProperties(),  msg + ": Properties" );

        checkEqualsOutOfSequence( one.traverseToNeighbors().getMeshObjects(), two.traverseToNeighbors().getMeshObjects(), msg + ": neighbors" );
        for( MeshObject neighbor1 : one.traverseToNeighbors() ) {
            MeshObject neighbor2 = two.getMeshBaseView().findMeshObjectByIdentifier( neighbor1.getIdentifier() );

            checkEqualsInSequence( one.getOrderedRoleAttributeNames( neighbor1 ), two.getOrderedRoleAttributeNames( neighbor2 ), msg + ": Role Attributes" );
            checkEqualsInSequence(
                    one.getRoleAttributeValues( neighbor1, one.getOrderedRoleAttributeNames( neighbor1 ) ),
                    two.getRoleAttributeValues( neighbor2, two.getOrderedRoleAttributeNames( neighbor2 ) ),
                    msg + ": Role Attribute values" );

            checkEqualsOutOfSequence( one.getRoleTypes( neighbor1 ), two.getRoleTypes( neighbor2 ), msg + ": RoleTypes" );

            checkEqualsOutOfSequence( one.getRolePropertyTypes( neighbor1 ), two.getRolePropertyTypes( neighbor2 ), msg + ": Role Properties" );
            checkEqualsInSequence(
                    one.getRolePropertyValues( neighbor1, one.getRolePropertyTypes( neighbor1 ) ),
                    two.getRolePropertyValues( neighbor2, two.getRolePropertyTypes( neighbor2 ) ),
                    msg + ": Role Property values" );
        }
    }

    /**
     * Manages our namespaces.
     */
    protected PrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;
}
