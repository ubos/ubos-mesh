//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.importer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Creates a simple history of changes to a MeshBase. Exports them.
 * Re-imports them, and runs the differencer.
 *
 * MeshObject deletion and recreation.
 */
public class ExportImportTest6
    extends
        AbstractExportImportTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshObjects" );

        startClock();

        MMeshBase mb1 = MMeshBase.Builder.create().history( true ).namespaceMap( thePrimaryNsMap ).build();

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.createMeshObject( "a" );
            MeshObject b = mb1.createMeshObject( "b" );
            MeshObject c = mb1.createMeshObject( "c" );

            a.setAttributeValue( "prop-a", "a1" );
            b.setAttributeValue( "prop-b", "b1" );
            c.setAttributeValue( "prop-c", "c1" );

            a.setRoleAttributeValue( b, "a->b", "From a to b (1)" );
            b.setRoleAttributeValue( c, "b->c", "From b to c (1)" );
            c.setRoleAttributeValue( a, "c->a", "From c to a (1)" );
        } );

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.findMeshObjectByIdentifier( "a" );
            a.setAttributeValue( "prop-a", "a2" );

            mb1.deleteMeshObject( a );
        } );

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.createMeshObject( "a" );
            a.setAttributeValue( "prop-a", "a3" );

            MeshObject b = mb1.findMeshObjectByIdentifier( "b" );
            b.setAttributeValue( "prop-b", "b3" );
            mb1.deleteMeshObject( b );
        } );

        runExportImportCompare( mb1 );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(ExportImportTest6.class);
}
