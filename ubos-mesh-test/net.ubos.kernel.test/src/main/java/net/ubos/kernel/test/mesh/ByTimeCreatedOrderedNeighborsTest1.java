//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.mesh;

import net.ubos.mesh.MeshObject;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that neighbor MeshObjects are ordered by TimeUpdated.
 */
public class ByTimeCreatedOrderedNeighborsTest1
    extends
        AbstractByTimeCreatedOrderedNeighborsTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Create a bunch of MeshObjects, at different times" );

        final int    N      = 100;
        final String prefix = "id-";

        MeshObject [] obj = new MeshObject[ N ];
        for( int i=0 ; i<N ; ++i ) {
            final int weLoveJava = i;
            obj[i] = theMeshBase.execute( (tx) -> theMeshBase.createMeshObject( String.format( "%s%02d", prefix, weLoveJava )));

            Thread.sleep( 1L );
        }

        //

        log.info( "Relating them" );

        final int pivot = 42;

        final int step = 17; // prime number
        theMeshBase.execute( () -> {
            for( int count=0, i=0 ; i<N ; ++i, count+= step ) {
                int neighbor = count % N;
                if( i==pivot || neighbor==pivot ) {
                    continue;
                }
                obj[pivot].setRoleAttributeValue( obj[ neighbor ], "foo", "bar" );
            }
        } );

        //

        log.info( "Checking" );

        MeshObject [] neighbors = obj[pivot].traverseToNeighbors().getMeshObjects();
        long previous = neighbors[0].getTimeCreated();
        for( int i=1 ; i<neighbors.length ; ++i ) {
            long current = neighbors[i].getTimeCreated();
            checkCondition( previous > current, "Not ordered at index %d: %d vs %d", i, previous, current );
            // previous must be younger, therefore > 
            previous = current;
        }
    }

    private static final Log log = Log.getLogInstance( ByTimeCreatedOrderedNeighborsTest1.class );
}
