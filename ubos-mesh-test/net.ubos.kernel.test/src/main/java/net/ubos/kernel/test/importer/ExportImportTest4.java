//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.importer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Creates a simple history of changes to a MeshBase. Exports them.
 * Re-imports them, and runs the differencer.
 *
 * MeshObject deletion and recreation.
 */
public class ExportImportTest4
    extends
        AbstractExportImportTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Creating MeshObjects" );

        startClock();

        MMeshBase mb1 = MMeshBase.Builder.create().history( true ).namespaceMap( thePrimaryNsMap ).build();

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.createMeshObject( "a" );
        } );

        sleepFor( 100 );

        mb1.execute( () -> {
            MeshObject a = mb1.findMeshObjectByIdentifier( "a" );
            mb1.deleteMeshObject( a );
        } );

        runExportImportCompare( mb1 );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ExportImportTest4.class);
}
