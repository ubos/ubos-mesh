//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.kernel.test.meshbase;

import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionTimeoutException;
import net.ubos.util.instrument.Breakpoint;
import net.ubos.util.instrument.InstrumentedThread;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that we cannot create two simultaneous Transactions.
 */
public class ConcurrentMeshBaseTest1
    extends
        AbstractConcurrentMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @Test
    public void run()
            throws
                Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        MMeshBase meshBase = MMeshBase.Builder.create().build();

        Breakpoint bp1 = new Breakpoint( "bp1", 0 );
        Breakpoint bp2 = new Breakpoint( "bp2", 0 );

        Runnable r1 = () -> {
            meshBase.execute(
                    (Transaction tx1) -> {
                        meshBase.createMeshObject( "a" );

                        bp1.reached();
                        return null;
                    });

        };
        Runnable r2 = () -> {
            try {
                meshBase.execute(
                        (Transaction tx2) -> {
                            meshBase.createMeshObject( "b" );

                            bp2.reached(); // should never get here
                            return null;
                        });
                reportError( "Transaction did not time out" );

            } catch( TransactionTimeoutException ex ) {
                // should throw this
                bp1.getWaitingThread().advanceNormally();
            }
        };

        InstrumentedThread t1 = new InstrumentedThread( r1, "t1" );
        InstrumentedThread t2 = new InstrumentedThread( r2, "t2" );

        t1.advanceTo( bp1 );

        //

        sleepFor( 1000 );

        Transaction tx1 = meshBase.getCurrentTransaction();
        checkObject( tx1, "No transaction" );
        checkIdentity( t1, bp1.getWaitingThread(), "Not t1 waiting at bp1" );

        t2.completeThreadAndWait();
        t1.completeThreadAndWait();

        //

        checkEquals( meshBase.size(), 2, "Wrong size MeshBase" );

    }

    private static final Log log = Log.getLogInstance( ConcurrentMeshBaseTest1.class );
}
