//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.fs.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;
import net.fusejna.FuseException;
import net.ubos.fs.Filesystem;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.testharness.AbstractTest;
import net.ubos.model.Filesystem.FilesystemSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests the FS mapping of a few MeshObjects at different graphpaths.. Does not deal with types,
 * attributes, content or relationships.
 *
 * Inherits from BaseFsTest from jnr-fuse, which is used as superclass of tests
 * there.
 */
public class MeshObjectTest1
    extends
        AbstractTest
{
    @Test
    public void filesExist()
        throws
            IOException
    {
        log.info( "Starting test " + getClass().getName() + ".fileExist()");

        for( String id : FILES_EXIST ) {
            File f = new File( theRootDir.getAbsolutePath() + "/" + id );

            checkCondition( f.exists(), "File does not exist: %s", f.getAbsolutePath() );
            checkCondition( f.isFile(), "Not a file: %s", f.getAbsolutePath());
        }
        for( String id : DIRS_EXIST ) {
            File f = new File( theRootDir.getAbsolutePath() + "/" + id );

            checkCondition( f.exists(),      "Directory does not exist: %s", f.getAbsolutePath());
            checkCondition( f.isDirectory(), "Not a directory: %s", f.getAbsolutePath());
        }

        for( String id : NO_EXISTS ) {
            File f = new File( theRootDir.getAbsolutePath() + "/" + id );

            checkCondition( !f.exists(), "File exists: %s", f.getAbsolutePath() );
        }
        for( String id : FILES_HIDDEN ) {
            File f = new File( theRootDir.getAbsolutePath() + "/" + id );

            checkCondition( !f.exists(), "Hidden file exists: %s", f.getAbsolutePath() );
        }
    }

    @Test
    public void permissions()
        throws
            IOException
    {
        log.info( "Starting test " + getClass().getName() + ".permissions()" );

        for( String id : FILES_EXIST ) {
            File f = new File( theRootDir.getAbsolutePath() + "/" + id );

            Set<PosixFilePermission> permissions = Files.getPosixFilePermissions( f.toPath() );
            checkEquals( "rw-------", PosixFilePermissions.toString( permissions ), "Permissions wrong on " + id );
        }
    }

    @BeforeAll
    public static void setUpClass()
        throws
            Exception
    {
        theMeshBase   = MMeshBase.Builder.create().build();

        // populate with test data
        theMeshBase.execute( (Transaction tx ) -> {
            try {
                for( String dir : DIRS_EXIST ) {
                    MeshObject found = theMeshBase.findMeshObjectByIdentifier( dir );
                    if( found == null ) {
                        theMeshBase.createMeshObject( dir, FilesystemSubjectArea.DIRECTORY );
                    } else {
                        found.bless( FilesystemSubjectArea.DIRECTORY );
                    }
                }
                for( String file : FILES_EXIST ) {
                    if( theMeshBase.findMeshObjectByIdentifier( file ) == null ) {
                        theMeshBase.createMeshObject( file, FilesystemSubjectArea.FILE );
                    }
                }
                for( String file : FILES_HIDDEN ) {
                    if( theMeshBase.findMeshObjectByIdentifier( file ) == null ) {
                        theMeshBase.createMeshObject( file );
                    }
                }

                return null;

            } catch( Exception ex ) {
                throw new RuntimeException( ex );
            }
        });
    }

    @AfterAll
    public static void tearDownClass()
    {
        if( theMeshBase != null ) {
            theMeshBase.die();
        }
        theMeshBase = null;
    }

    @BeforeEach
    public void setUp()
        throws
            IOException,
            FuseException,
            InterruptedException
    {
        theFilesystem = Filesystem.create( theMeshBase ); // , "-d" ); // for debug messages on console

        theRootDir = Files.createTempDirectory("memfuse").toFile();

        theFilesystem.mount(theRootDir, false);
    }

    @AfterEach
    public void tearDown()
        throws
            FuseException,
            IOException
    {
        if( theFilesystem != null ) {
            theFilesystem.unmount();
            theFilesystem = null;
        }
        if( theRootDir.exists() ) {
            theRootDir.delete();
        }
    }

    /**
     * MeshBase containing test objects.
     */
    protected static MeshBase theMeshBase;

    /**
     * Filesystem under test.
     */
    protected Filesystem theFilesystem;

    /**
     * Where is the Filesystem mounted.
     */
    protected File theRootDir;

    // Identifiers of  MeshObjects that should exist in the Filesystem
    public static final String OBJ_HOME = "";
    public static final String OBJ_A    = "aaa";
    public static final String OBJ_B    = "bbb";
    public static final String OBJ_C    = "ccc";
    public static final String OBJ_C_D  = "ccc/ddd";
    public static final String [] DIRS_EXIST = {
        OBJ_HOME,
        OBJ_C
    };
    public static final String [] FILES_EXIST  = {
        OBJ_A,
        OBJ_B,
        OBJ_C_D
    };

    // Identifiers of MeshObjects that should not exist in the Filesystem
    public static final String NOOBJ_B_E = "bbb/eee"; // hidden by bbb
    public static final String NOOBJ_F   = "fff";     // not created
    public static final String NOOBJ_F_G = "fff/ggg"; // no fff
    public static final String [] NO_EXISTS = {
        NOOBJ_F
    };
    public static final String [] FILES_HIDDEN = {
        NOOBJ_B_E,
        NOOBJ_F_G
    };

    private static final Log log = Log.getLogInstance( MeshObjectTest1.class );
}
