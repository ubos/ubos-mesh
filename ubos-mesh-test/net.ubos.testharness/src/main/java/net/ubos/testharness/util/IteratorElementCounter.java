//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.testharness.util;

import net.ubos.util.logging.Log;

import java.util.Iterator;

/**
 * A utility method that counts the number of elements returned by an Iterator.
 */
public abstract class IteratorElementCounter
{
    private static final Log log = Log.getLogInstance( IteratorElementCounter.class ); // our own, private logger

    /**
     * Count the number of elements returned by an Iterator.
     *
     * @param iter the Iterator
     * @return the number of elements
     */
    public static int countIteratorElements(
            Iterator<? extends Object> iter )
    {
        int ret = 0;
        while( iter.hasNext() ) {
            Object current = iter.next();
            
            if( log.isDebugEnabled() ) {
                log.debug( "Found element " + current );
            }
            ++ret;
        }
        return ret;
    }
}
