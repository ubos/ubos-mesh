//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.testharness;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.Reference;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.ubos.util.ArrayHelper;
import net.ubos.util.factory.FactoryException;
import net.ubos.util.NamedThreadFactory;
import net.ubos.util.logging.BufferingDumper;
import net.ubos.util.logging.BufferingDumperFactory;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;
import net.ubos.util.logging.Log;
import net.ubos.util.logging.ToStringDumperFactory;
import org.junit.jupiter.api.Assertions;

/**
 * An abstract superclass for tests. It provides a bunch of generic test
 * harness infrastructure.
 */
public abstract class AbstractTest
{
    /**
     * Constructor, for subclasses only.
     */
    protected AbstractTest()
    {}

    /**
     * Construct the fully-qualified file name of a file relative to a Class.
     *
     * @param clazz the Class
     * @param relative the relative file name
     * @return the fully-qualified file name
     */
    protected static String thisPackage(
            Class<?> clazz,
            String   relative )
    {
        String clazzName = clazz.getName();
        int    period    = clazzName.lastIndexOf( '.' );

        StringBuilder ret = new StringBuilder();
        ret.append( clazzName.substring( 0, period+1 ).replace( '.', '/' ));
        ret.append( relative );
        return ret.toString();
    }

    /**
     * Report error if the arguments are not equal.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkEquals(
            Object    one,
            Object    two,
            String    format,
            Object... args )
    {
        Assertions.assertEquals( two, one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the arguments are equal.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkNotEquals(
            Object    one,
            Object    two,
            String    format,
            Object... args )
    {
        Assertions.assertNotEquals( two, one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the arguments are not equal.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkEqualByteArrays(
            byte []   one,
            byte []   two,
            String    format,
            Object... args )
    {
        Assertions.assertArrayEquals( two, one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the arguments do not have the same content in the same sequence.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkEqualsInSequence(
            Object [] one,
            Object [] two,
            String    format,
            Object... args )
    {
        Assertions.assertArrayEquals( two, one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the arguments do not have the same content in the same sequence.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkEqualsInSequence(
            List<?>   one,
            Object [] two,
            String    format,
            Object... args )
    {
        boolean ret = true;

        if( one == null ) {
            if( two != null ) {
                reportError( String.format( format, args ), two, one );
                ret = false;
            }
        } else if( two == null ) {
            reportError( String.format( format, args ), two, one );
            ret = false;
        } else {
            int length = one.size();
            if( length != two.length ) {
                ret = false;
                reportError( String.format( format, args ), "different length", one.size(), two.length );
                if( length > two.length ) {
                    length = two.length;
                }
            }

            for( int i=0 ; i<length ; ++i ) {
                if( one.get( i ) == null ) {
                    if( two[ i ] != null ) {
                        ret = false;
                        reportError( String.format( format, args ), "element at index " + i + " is different", one.get( i ), two[i] );
                    }
                } else if( !one.get( i ).equals( two[i] )) {
                    ret = false;
                    reportError( String.format( format, args ), "element at index " + i + " is different", one.get( i ), two[i] );
                }
            }
        }
        return ret;
    }

    /**
     * Report error if the arguments do not have the same content in any sequence.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkEqualsOutOfSequence(
            Object [] one,
            Object [] two,
            String    format,
            Object... args )
    {
        boolean ret = ArrayHelper.hasSameContentOutOfOrder( one, two, true );
        if( !ret ) {
            reportError( String.format( format, args ), two, one );
        }
        return ret;
    }

    /**
     * Report error if the arguments are not identical.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkIdentity(
            Object    one,
            Object    two,
            String    format,
            Object... args )
    {
        Assertions.assertSame( two, one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the arguments are identical.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkNotIdentity(
            Object    one,
            Object    two,
            String    format,
            Object... args )
    {
        Assertions.assertNotSame( two, one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the first argument does not start with the second argument String.
     *
     * @param haystack argument in which to look for the needle
     * @param needle the String to look for
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkStartsWith(
            String    haystack,
            String    needle,
            String    format,
            Object... args )
    {
        if( haystack == null ) {
            if( needle == null ) {
                return true;
            } else {
                reportError( String.format( format, args ), haystack, needle );
                return false;
            }
        }
        if( haystack.startsWith( needle )) {
            return true;
        } else {
            reportError( String.format( format, args ), haystack, needle );
            return false;
        }
    }

    /**
     * Report error if the first argument does start with the second argument String.
     *
     * @param one first argument to compare
     * @param two second argument to compare
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkNotStartsWith(
            String    one,
            String    two,
            String    format,
            Object... args )
    {
        if( one == null ) {
            if( two == null ) {
                return true;
            } else {
                reportError( String.format( format, args ), one, two );
                return false;
            }
        }
        if( !one.startsWith( two )) {
            return true;
        } else {
            reportError( String.format( format, args ), one, two );
            return false;
        }
    }

    /**
     * Report error if the argument is null.
     *
     * @param one the object to test
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkObject(
            Object    one,
            String    format,
            Object... args )
    {
        Assertions.assertNotNull( one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the argument is not null.
     *
     * @param one the object to test
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkNotObject(
            Object    one,
            String    format,
            Object... args )
    {
        Assertions.assertNull( one, () -> String.format( format, args ) );
    }

    /**
     * Report error if the object is not of a certain type.
     *
     * @param one the object whose type we test
     * @param typeOne the type against which we test
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkType(
            Object    one,
            Class<?>  typeOne,
            String    format,
            Object... args )
    {
        if( one == null ) {
            reportError( String.format( format, args ), "object is null" );
            return false;
        }
        if( !typeOne.isInstance( one )) {
            reportError( String.format( format, args ), "object is instance of " + one.getClass().getName() + ", not of " + typeOne.getName() );
            return false;
        }
        return true;
    }

    /**
     * Report error if the condition is false.
     *
     * @param condition the condition
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     */
    public final void checkCondition(
            Boolean   condition,
            String    format,
            Object... args )
    {
        Assertions.assertNotNull( condition, () -> String.format( format, args ) );
        if( condition != null ) {
            Assertions.assertTrue( condition, () -> String.format( format, args ) );
        }
    }

    /**
     * Report error if the tested number is outside of a certain range.
     *
     * @param test the number to test
     * @param median the median of the range against which the number is being tested
     * @param plusminus half the length of the range against which the number is being tested
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkInPlusMinusRange(
            long      test,
            long      median,
            long      plusminus,
            String    format,
            Object... args )
    {
        if( test < median - plusminus ) {
            reportError( String.format( format, args ), String.valueOf( test ) + " is outside of " + median + " +/- " + plusminus );
            return false;
        }
        if( test > median + plusminus ) {
            reportError( String.format( format, args ), String.valueOf( test ) + " is outside of " + median + " +/- " + plusminus );
            return false;
        }
        return true;
    }

    /**
     * Report error if the tested number is outside of a certain range.
     *
     * @param test the number to test
     * @param median the median of the range against which the number is being tested
     * @param plusminus half the length of the range against which the number is being tested
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkInPlusMinusRange(
            double    test,
            double    median,
            double    plusminus,
            String    format,
            Object... args )
    {
        if( test < median - plusminus ) {
            reportError( String.format( format, args ), String.valueOf( test ) + " is outside of " + median + " +/- " + plusminus );
            return false;
        }
        if( test > median + plusminus ) {
            reportError( String.format( format, args ), String.valueOf( test ) + " is outside of " + median + " +/- " + plusminus );
            return false;
        }
        return true;
    }

    /**
     * Report error if the tested numbers are outside of a certain range.
     *
     * @param tests the sequence of numbers to test
     * @param medians the sequence of medians against which the numbers are being tested
     * @param jitter the amount of jitter allowed
     * @param margin the percentage by which the test numbers may be off
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkInMarginRange(
            long []   tests,
            long []   medians,
            long      jitter,
            double    margin,
            String    format,
            Object... args )
    {
        return checkInMarginRange( tests, medians, jitter, margin, 0, format, args );
    }

    /**
     * Report error if the tested numbers are outside of a certain range.
     *
     * @param tests the sequence of numbers to test
     * @param medians the sequence of medians against which the numbers are being tested
     * @param jitter the amount of jitter allowed
     * @param margin the percentage by which the test numbers may be off
     * @param offset a static offset between the units in which medians and tests are measured
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkInMarginRange(
            long []   tests,
            long []   medians,
            long      jitter,
            double    margin,
            long      offset,
            String    format,
            Object... args )
    {
        long [] adjustedTests = new long[ tests.length ];
        for( int i=0 ; i<tests.length ; ++i ) {
            adjustedTests[i] = tests[i] - offset;
        }
        StringBuilder buf = new StringBuilder();
        buf.append( "checkInMarginRange( " );
        buf.append( ArrayHelper.arrayToString( adjustedTests ));
        buf.append( ", " );
        buf.append( ArrayHelper.arrayToString( medians ));
        buf.append( ", " );
        buf.append( jitter );
        buf.append( ", " );
        buf.append( margin );
        if( getLog().isDebugEnabled() ) {
            getLog().debug( buf.toString() );
        }

        boolean ret = true;
        int length = tests.length;
        if( length != medians.length ) {
            ret = false;
            reportError(
                    String.format( format, args ),
                    "different length: "
                        + adjustedTests.length
                        + " vs. "
                        + medians.length
                        + ": "
                        + ArrayHelper.join( adjustedTests )
                        + " vs. "
                        + ArrayHelper.join( medians ) );

            if( length > medians.length ) {
                length = medians.length;
            }
        }

        for( int i=0 ; i<length ; ++i ) {
            if( tests[i] < medians[i]*(1.-margin) + offset - jitter || tests[i] > medians[i]*(1.+margin ) + offset + jitter ) {
                ret = false;
                reportError( String.format( format, args ), "element at index " + i + " is outside of range: " + (tests[i] - offset) + " vs. " + medians[i] + " +/- " + jitter + " +/- " + ( margin*100.) + "% (offset: " + offset + ", jitter: " + jitter + ")" );
            }
        }
        return ret;
    }

    /**
     * Report error if the tested number is outside of a certain range.
     *
     * @param test the number to test
     * @param min the lower end of the range
     * @param max the higher end of the range
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkInRange(
            long     test,
            long      min,
            long      max,
            String    format,
            Object... args )
    {
        if( test < min ) {
            reportError( String.format( format, args ), String.valueOf( test ) + " is outside of [ " + min + ", " + max + " ] (by " + (min-test) + ")" );
            return false;
        }
        if( test > max ) {
            reportError( String.format( format, args ), String.valueOf( test ) + " is outside of [ " + min + ", " + max + " ] (by " + (test-max) + ")" );
            return false;
        }
        return true;
    }

    /**
     * Report error if a set contains duplicates.
     *
     * @param iter iterator over the set
     * @param useEquals if true, use the equals method to determine equality; identity otherwise
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkNoDuplicates(
            Iterator<?> iter,
            boolean     useEquals,
            String      format,
            Object...   args )
    {
        ArrayList<Object> found = new ArrayList<>();

        while( iter.hasNext() ) {
            Object current = iter.next();

            for( Object currentFound : found ) {
                if( useEquals ) {
                    if( currentFound == null ) {
                        if( current == null ) {
                            reportError( String.format( format, args ), "Null value contained at least twice" );
                            return false;
                        }
                    } else if( currentFound.equals( current )) {
                        reportError( String.format( format, args ), "Object contained at least twice", current, currentFound );
                        return false;
                    }
                } else {
                    if( currentFound == found ) {
                        reportError( String.format( format, args ), "Object contained at least twice", current );
                        return false;
                    }
                }
            }
            found.add( current );
        }
        return true;
    }

    /**
     * Check that a String matches a regular expression.
     *
     * @param regex the regular expression to match
     * @param candidate the candidate String
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkRegex(
            String    regex,
            String    candidate,
            String    format,
            Object... args )
    {
        if( candidate == null ) {
            reportError( String.format( format, args ) + ": cannot match null against regex '" + regex + "'" );
            return false;
        }
        Pattern p = Pattern.compile( regex );
        Matcher m = p.matcher( candidate );
        if( !m.matches() ) {
            reportError( String.format( format, args ) + ": regex '" + regex + "' did not match candidate '" + candidate + "'" );
            return false;
        }
        return true;
    }

    /**
     * Check that a String matches a regular expression.
     *
     * @param regex the regular expression to match
     * @param flags the flags for the regular expression per Pattern.compile
     * @param candidate the candidate String
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkRegex(
            String    regex,
            int       flags,
            String    candidate,
            String    format,
            Object... args )
    {
        if( candidate == null ) {
            reportError( String.format( format, args ) + ": cannot match null against regex '" + regex + "'" );
            return false;
        }
        Pattern p = Pattern.compile( regex, flags );
        Matcher m = p.matcher( candidate );
        if( !m.matches() ) {
            reportError( String.format( format, args ) + ": regex '" + regex + "' did not match candidate '" + candidate + "'" );
            return false;
        }
        return true;
    }

    /**
     * Check that a String does not match a regular expression.
     *
     * @param regex the regular expression to match
     * @param candidate the candidate String
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkNotRegex(
            String     regex,
            String    candidate,
            String    format,
            Object... args )
    {
        if( candidate == null ) {
            reportError( String.format( format, args ) + ": cannot match null against regex '" + regex + "'" );
            return false;
        }
        Pattern p = Pattern.compile( regex );
        Matcher m = p.matcher( candidate );
        if( m.matches() ) {
            reportError( String.format( format, args ) + ": regex '" + regex + "' matches candidate '" + candidate + "'" );
            return false;
        }
        return true;
    }

    /**
     * Check that a String does not match a regular expression.
     *
     * @param regex the regular expression to match
     * @param flags the flags for the regular expression per Pattern.compile
     * @param candidate the candidate String
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     */
    public final boolean checkNotRegex(
            String    regex,
            int       flags,
            String    candidate,
            String    format,
            Object... args )
     {
        if( candidate == null ) {
            reportError( String.format( format, args ) + ": cannot match null against regex '" + regex + "'" );
            return false;
        }
        Pattern p = Pattern.compile( regex, flags );
        Matcher m = p.matcher( candidate );
        if( m.matches() ) {
            reportError( String.format( format, args ) + ": regex '" + regex + "' matches candidate '" + candidate + "'" );
            return false;
        }
        return true;
    }

    /**
     * Check that a Collection contains the correct items, regardless in which sequence.
     *
     * @param collection the Collection whose content to test
     * @param selectors a collection of selector, each of which must match exactly once, with no members of the collection left over
     * @param format format String for message to emit when there is an error
     * @param args arguments for message
     * @return true if check passed
     * @param <T> type parameter
     */
    public final <T> boolean checkContains(
            Collection<? extends T>           collection,
            Collection<? extends Selector<T>> selectors,
            String                            format,
            Object...                         args )
    {
        if( collection.size() != selectors.size() ) {
            reportError( String.format( format, args ) + ": incorrect number of elements: " + collection.size() + " vs. " + selectors.size() );
            return false;
        }
        HashSet<Selector<T>> usedAlready = new HashSet<>();
        for( T candidate : collection ) {
            boolean found = false;
            for( Selector<T> selector : selectors ) {
                if( usedAlready.contains( selector )) {
                    continue;
                }
                if( selector.selects( candidate )) {
                    usedAlready.add( selector );
                    found = true;
                    break;
                }
            }
            if( !found ) {
                reportError( String.format( format, args ) + ": candidate " + candidate + " does not match any selector" );
                return false;
            }
        }
        if( selectors.size() != usedAlready.size() ) {
            reportError( String.format( format, args ) + ": " + ( selectors.size() - usedAlready.size() ) + " selectors left over" );
            return false;
        }
        return true;
    }

    /**
     * Count the number of elements remaining on this Iterator.
     *
     * @param iter the Iterator
     * @return the number of elements remaining on this Iterator
     */
    public int countRemaining(
            Iterator<?> iter )
    {
        int ret = 0;
        while( iter.hasNext() ) {
            iter.next();
            ++ret;
        }
        return ret;
    }

    /**
     * Check that a collection is empty, and if not, print the members.
     * We pass it in as an Iterable, which seems to be the most-broadly applicable.
     *
     * @param coll the Collection
     * @param msg message to print
     * @param args optional arguments
     * @return true if check passed
     */
    public boolean checkEmpty(
            Iterable<?> coll,
            String      msg,
            Object ...  args )
    {
        Iterator<?> iter = coll.iterator();
        if( !iter.hasNext() ) {
            return true;
        }
        StringBuilder buf = new StringBuilder();
        buf.append( String.format( msg, args ));
        for( int i=0 ; iter.hasNext() && i<10 ; ++i ) {
            buf.append( "\n    " );
            buf.append( iter.next() );
        }
        if( iter.hasNext() ) {
            buf.append(  "\n    (and more,skipping()" );
        }
        return reportError( buf.toString() );
    }

    /**
     * Report error.
     *
     * @param msg message to print
     * @param args optional arguments
     * @return true if check passed
     */
    public final boolean reportError(
            String     msg,
            Object ... args )
    {
        return reportError( msg, DEFAULT_DUMPER_FACTORY, args );
    }

    /**
     * Report error.
     *
     * @param msg message to print
     * @param df the DumperFactory to use
     * @param args optional arguments
     * @return true if check passed
     */
    public final boolean reportError(
            String                 msg,
            BufferingDumperFactory df,
            Object ...             args )
    {
        if( msg != null ) {
            StringBuilder buf = new StringBuilder();
            buf.append( msg );

            String    sep = " : ";
            Throwable t   = null;

            for( int i=0 ; i<args.length ; ++i ) {
                if( i<args.length-1 || !( args[i] instanceof Throwable )) {
                    try {
                        BufferingDumper d = df.obtainFor( args[i] );
                        d.dump( args[i] );
                        buf.append( sep );
                        buf.append( d.getBuffer() );
                        sep = ", ";
                    } catch( FactoryException ex ) {
                        log.error( ex );
                    }
                } else {
                    t = (Throwable) args[i];
                }
            }
            if( t != null ) {
                getLog().error( buf.toString(), t );
            } else {
                getLog().error( buf.toString() );
            }
            Assertions.fail( buf.toString() );
        }
        ++theErrorCount;
        return false;
    }

    /**
     * This starts a relative timer.
     *
     * @return the time at which the clock was started, in System.currentTimeMillis() format
     */
    protected final long startClock()
    {
        theStartTime = System.currentTimeMillis();
        getLog().info( "Starting clock -- it is now " + theStartTime );

        return theStartTime;
    }

    /**
     * Obtain the absolute start time.
     *
     * @return the time at which the clock was started, in System.currentTimeMillis() format
     */
    public final long getStartTime()
    {
        return theStartTime;
    }

    /**
     * Sleep for a specified number of milliseconds.
     *
     * @param delta the number of milliseconds to wait
     * @throws InterruptedException thrown when another thread interrupts this Thread while sleeping
     */
    protected final static void sleepFor(
            long delta )
        throws
            InterruptedException
    {
        Thread.sleep( delta );
    }

    /**
     * This puts the invoking Thread to sleep until a specified relative time
     * (in milli-seconds) from the time startClock() was called.
     *
     * @param relativeTimeInMillis the relative time by which this Thread is supposed to wake up
     * @throws InterruptedException thrown if the Thread was interrupted
     */
    protected final void sleepUntil(
            long relativeTimeInMillis )
        throws
            InterruptedException
    {
        long current = System.currentTimeMillis();

        getLog().info( "sleeping until relative time: " + relativeTimeInMillis + " (it is now: relative: " + (current-theStartTime) + " / absolute: " + current + " )" );

        long delta = relativeTimeInMillis - (current - theStartTime);

        if( delta > 0 ) {
            Thread.sleep( delta );

        } else if( delta < 0 ) {
            getLog().error(
                    "Wait time has passed already: current relative time: "
                    + (current-theStartTime)
                    + " vs. "
                    + relativeTimeInMillis
                    + " absolute: "
                    + current );
        }
    }

    /**
     * Obtain the current time relative to the time startClock() was called.
     *
     * @return the current time relative to the time startClock() was called
     */
    protected final long getRelativeTime()
    {
        long current = System.currentTimeMillis();

        return current - theStartTime;
    }

    /**
     * Sleep until a Reference has been cleared, or the maximum amount of time, whatever comes first.
     *
     * @param ref the Reference that needs to be cleared
     * @param max the maximum number of milliseconds
     * @param msg the error message is the object is still around after max milliseconds
     * @throws InterruptedException thrown if the Thread was interrupted
     */
    protected void sleepUntilIsGone(
            Reference<?> ref,
            long         max,
            String       msg )
        throws
            InterruptedException
    {
        if( ref.get() == null ) {
            return;
        }

        long end = System.currentTimeMillis() + max;

        System.gc();

        while( ref.get() != null && System.currentTimeMillis() < end ) {
            Thread.sleep( 50L );
            System.gc();
        }
        if( ref.get() != null ) {
            reportError( msg );
        }
    }

    /**
     * Helper method that allows tests to use local file names for files in the JAR file.
     *
     * @param testClass the test class to which the file is local
     * @param file the local file name
     * @return the file name relative to the project directory
     */
    public static String localFileName(
            Class<?> testClass,
            String   file )
    {
        // this is relative to the class loader
        StringBuilder ret = new StringBuilder();

        String name = testClass.getName();
        String packageName = name.substring( 0, name.lastIndexOf( '.' ));
        ret.append( packageName.replace( '.', '/' ));
        if( !file.startsWith( "/" )) {
            ret.append( "/" );
        }
        ret.append( file );
        return ret.toString();
    }

    /**
     * Helper method that allows tests to use local file names for files in the file system.
     *
     * @param testClass the test class to which the file is local
     * @param file the local file name
     * @return the file name relative to the project directory
     */
    public static String fileSystemFileName(
            Class<?> testClass,
            String   file )
    {
        StringBuilder ret = new StringBuilder();
        ret.append( "src/test/resources/" );

        String name = testClass.getName();
        String packageName = name.substring( 0, name.lastIndexOf( '.' ));
        ret.append( packageName.replace( '.', '/' ));
        if( !file.startsWith( "/" )) {
            ret.append( "/" );
        }
        ret.append( file );
        return ret.toString();
    }

    /**
     * Helper method to determine the location of a temporary input file.
     *
     * @param testClass the test class that needs a temporary input file
     * @param file the local file name
     * @return the file name relative to the project directory
     */
    public static String tempInputFileName(
            Class<?> testClass,
            String   file )
    {
        StringBuilder ret = new StringBuilder();
        ret.append( "build" );
        if( !file.startsWith( "/" )) {
            ret.append( "/" );
        }
        ret.append( file );
        return ret.toString();
    }

    /**
     * Helper smart factory method to create an XML parser.
     *
     * @return the XML DocumentBuilder
     * @throws ParserConfigurationException thrown if the parser cannot be created
     */
    protected DocumentBuilder createXmlParser()
            throws
                ParserConfigurationException
    {
        Thread.currentThread().setContextClassLoader( getClass().getClassLoader() );

        DocumentBuilderFactory theDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
        theDocumentBuilderFactory.setNamespaceAware( true );

        theDocumentBuilderFactory.setValidating( false );
        theDocumentBuilderFactory.setIgnoringComments( true );
        theDocumentBuilderFactory.setIgnoringElementContentWhitespace( true );

        return theDocumentBuilderFactory.newDocumentBuilder();
    }

    /**
     * This converts the current stack trace into a string.
     *
     * @return the current stack trace as string
     */
    public static final String stackTraceToString()
    {
        StringWriter theStringWriter = new StringWriter();
        PrintWriter  thePrintWriter  = new PrintWriter( theStringWriter );
        new Exception().printStackTrace( thePrintWriter );
        thePrintWriter.close();
        return theStringWriter.toString();
    }

    /**
     * Invoke the garbage collector.
     */
    protected final void collectGarbage()
    {
        try {
            for( int i=0 ; i<20 ; ++i ) {
                System.gc();
                Thread.sleep( 50L );
            }
        } catch( InterruptedException ex ) {
            reportError( "unexpected interruption of thread", ex );
        }
    }

    /**
     * This helper copies files for us. It waits for one second, in order to get around
     * the one-second delay resolution for File.lastModified() -- at least on MacOSX.
     *
     * @param from file name of the source file
     * @param to   file name of the destination file
     * @throws IOException accessing one of the two files failed
     */
    protected final void copyFile(
            String from,
            String to )
        throws
            IOException
    {
        File fromFile = new File( from );
        File toFile   = new File( to );

        getLog().info( "copying " + fromFile.getCanonicalPath() + " to " + toFile.getCanonicalPath() );

        try {
            Thread.sleep( 1010L ); // we need to sleep some in order to prevent that OSX considers the file unchanged
                                   // because the time resolution on the lastModified stamp is seconds only
        } catch( InterruptedException ex ) {
            log.error( ex );
        }

        if( !fromFile.exists() ) {
            throw new IOException( "Cannot copy from file " + fromFile.getCanonicalPath() + ": does not exist" );
        }
        if( fromFile.isDirectory() ) {
            throw new IOException( "Cannot copy from file " + fromFile.getCanonicalPath() + ": is a directory" );
        }
        if( !fromFile.canRead() ) {
            throw new IOException( "Cannot copy from file " + fromFile.getCanonicalPath() + ": cannot read" );
        }
        if( !toFile.getParentFile().isDirectory() ) {
            throw new IOException( "Cannot copy to file " + toFile.getCanonicalPath() + ": enclosing directory does not exist" );
        }
        if( toFile.exists() ) {
            if( !toFile.canWrite() ) {
                throw new IOException( "Cannot copy to file " + toFile.getCanonicalPath() + ": cannot write" );
            }
        } else  if( !toFile.getParentFile().canWrite() ) {
            throw new IOException( "Cannot copy to file " + toFile.getCanonicalPath() + ": cannot write to enclosing directory" );
        }
        OutputStream toStream;
        try (InputStream fromStream = new FileInputStream( fromFile )) {
            toStream = new FileOutputStream( toFile );
            int c;
            while( ( c=fromStream.read()) >= 0 ) {
                toStream.write( c );
            }
            toStream.flush();
        }
        toStream.close();
    }

    /**
     * Recursively delete a directory or file.
     *
     * @param f the file
     */
    public static void deleteFile(
            File f )
    {
        if( !f.exists() ) {
            return;
        }
        if( f.isDirectory() ) {
            File [] contained = f.listFiles();
            for( int i=0 ; i<contained.length ; ++i ) {
                deleteFile( contained[i] );
            }
        }
        f.delete();
    }

    /**
     * This checks whether the content of these two files is the same.
     *
     * @param one the first file to compare
     * @param two the second file to compare
     * @param msg the message to print if the files do not have the same content
     * @return true if the test passed
     */
    public final boolean checkFileDiff(
            File   one,
            File   two,
            String msg )
    {
        boolean ret = true;

        if( ! one.exists() ) {
            reportError( msg, "File does not exist: " + one );
            ret = false;
        }
        if( ! two.exists() ) {
            reportError( msg, "File does not exist: " + two );
            ret = false;
        }
        if( !ret ) {
            return ret;
        }
        try {
            InputStream oneStream = new BufferedInputStream( new FileInputStream( one ));
            InputStream twoStream = new BufferedInputStream( new FileInputStream( two ));

            while( true ) {
                int v1 = oneStream.read();
                int v2 = twoStream.read();

                if( v1 != v2 ) {
                    reportError( msg, "file content is different" );
                    ret = false;
                    break;
                }
                if( v1 <= 0 ) {
                    break;
                }
            }

            oneStream.close();
            twoStream.close();

        } catch( Exception ex ) {
            getLog().error( ex );
            ret = false;
        }
        return ret;
    }

    /**
     * Obtain the Log for this subclass.
     *
     * @return the Log for this subclass
     */
    protected Log getLog()
    {
        return Log.getLogInstance( getClass() );
    }

    /**
     * Obtain a Thread pool for a test.
     *
     * @param testName name of the test, used to label the Threads created by the ThreadFactory
     * @param nThreads the number of threads in the thread pool
     * @return the created ScheduledExecutorService
     */
    protected ScheduledExecutorService createThreadPool(
            String testName,
            int    nThreads )
    {
        NamedThreadFactory factory = new NamedThreadFactory( testName );

        return new MyScheduledThreadPoolExecutor( nThreads, factory, testName );
    }

    /**
     * Obtain a Thread pool for a test.
     *
     * @param nThreads the number of threads in the thread pool
     * @return the created ScheduledExecutorService
     */
    protected ScheduledExecutorService createThreadPool(
            int nThreads )
    {
        Class<? extends AbstractTest> testClass = getClass();

        return createThreadPool( testClass.getName(), nThreads );
    }

    /**
     * Helper method to convert a String to bytes.
     *
     * @param arg the String
     * @return the created array
     */
    protected static byte [] bytes(
            String arg )
    {
        return arg.getBytes( StandardCharsets.UTF_8 );
    }

    // Our Logger
    private static Log log;

    /**
     * The absolute time in millis when the timer was started.
     */
    protected long theStartTime;

    /**
     * The number of errors we have encountered up to now.
     */
    protected static int theErrorCount = 0;

    /**
     * The default DumperFactory to use.
     */
    public static final ToStringDumperFactory DEFAULT_DUMPER_FACTORY
            = ToStringDumperFactory.create();

    /**
     * Our local subclass of ScheduledThreadPoolExecutor.
     */
    static class MyScheduledThreadPoolExecutor
            extends
                ScheduledThreadPoolExecutor
            implements
                CanBeDumped
    {
        /**
         * Constructor.
         *
         * @param corePoolSize number of Threads
         * @param threadFactory factory for Threads
         * @param name name of this MyScheduledThreadPoolExecutor, for debugging purposes
         */
        MyScheduledThreadPoolExecutor(
                int           corePoolSize,
                ThreadFactory threadFactory,
                String        name )
        {
            super( corePoolSize, threadFactory );

            theName = name;

            setContinueExistingPeriodicTasksAfterShutdownPolicy( false );
            setExecuteExistingDelayedTasksAfterShutdownPolicy( false );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void dump(
                Dumper d )
        {
            d.dump( this,
                    new String[] {
                        "name"
                    },
                    new Object[] {
                        "ThreadPoolExecutor " + theName + " (" + getCorePoolSize() + " threads)"
                    });
        }

        /**
         *
         */
        protected String theName;
    }

    /**
     * Must be subclassed to use #checkContains.
     *
     * @param <T> type parameter
     */
    public static interface Selector<T>
    {
        /**
         * Returns true if this selector selects the candidate.
         *
         * @param candidate the candidate
         * @return true of the candidate is selected by the Selector
         */
        public boolean selects(
                T candidate );
    }
}
