//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.testharness.junit5;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import net.ubos.util.logging.Log;
import org.junit.platform.engine.discovery.ClassNameFilter;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.engine.discovery.PackageSelector;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

/**
 * Run jUnit5 tests. This is modeled after ConsoleLauncher and ConsoleTestExecutor
 * in junit5..
 */
public class RunJUnit5
{
    private static final Log log = Log.getLogInstance( RunJUnit5.class ); // our own, private logger

    public static final String PACKAGE_KEYWORD = "--package";
    public static final String CLASSNAMEPATTERN_KEYWORD = "--classnamepattern";

    /**
     * Run method.
     *
     * @param loader the top-level ClassLoader to use
     * @param defaultPackageName the default package name when no package has been specified
     * @param args command-line arguments
     * @return desired exit code
     */
    public static int run(
            ClassLoader loader,
            String      defaultPackageName,
            String ...  args )
    {
        int         ret = -1;
        ClassLoader org = Thread.currentThread().getContextClassLoader();

        try {
            Thread.currentThread().setContextClassLoader( loader );

            ArrayList<String> packageNames      = new ArrayList<>();
            ArrayList<String> classNamePatterns = new ArrayList<>();

            boolean inPackageNames      = false;
            boolean inClassNamePatterns = false;
            for( String arg : args ) {
                if( inPackageNames ) {
                    switch( arg ) {
                        case PACKAGE_KEYWORD:
                            return fatalInvocation();
                        case CLASSNAMEPATTERN_KEYWORD:
                            inPackageNames      = false;
                            inClassNamePatterns = true;
                            break;
                        default:
                            packageNames.add( arg );
                            break;
                    }

                } else if( inClassNamePatterns ) {
                    switch( arg ) {
                        case PACKAGE_KEYWORD:
                            inPackageNames      = true;
                            inClassNamePatterns = false;
                            break;
                        case CLASSNAMEPATTERN_KEYWORD:
                            return fatalInvocation();
                        default:
                            classNamePatterns.add( arg );
                            break;
                    }

                } else {
                    switch( arg ) {
                        case PACKAGE_KEYWORD:
                            inPackageNames = true;
                            break;
                        case CLASSNAMEPATTERN_KEYWORD:
                            inClassNamePatterns = true;
                            break;
                        default:
                            return fatalInvocation();
                    }
                }
            }

            if( packageNames.isEmpty() ) {
                packageNames.add( defaultPackageName );
            }
            ret = run( packageNames, classNamePatterns );

        } finally {
            Thread.currentThread().setContextClassLoader( org );
        }
        return ret;
    }

    static int run(
            List<String> packageNames,
            List<String> classNamePatterns )
    {
        LauncherDiscoveryRequestBuilder requestBuilder = LauncherDiscoveryRequestBuilder.request();
        if( packageNames != null && !packageNames.isEmpty()) {
            requestBuilder.selectors(
                packageNames.stream().map( DiscoverySelectors::selectPackage ).toArray( PackageSelector[]::new )
            );
        } else {
            requestBuilder.selectors(
                    new PackageSelector[] { DiscoverySelectors.selectPackage( "" ) }
            );
        }
        if( classNamePatterns != null && !classNamePatterns.isEmpty() ) {
            requestBuilder.filters(
                ClassNameFilter.includeClassNamePatterns( classNamePatterns.stream().toArray( String[]::new ))
            );
        } else {
            requestBuilder.filters(
                ClassNameFilter.includeClassNamePatterns(".*Test.*")
            );
        }

        LauncherDiscoveryRequest request = requestBuilder.build();

        Launcher launcher = LauncherFactory.create();

        // Register a listener of your choice
        SummaryGeneratingListener listener = new SummaryGeneratingListener();
        launcher.registerTestExecutionListeners(listener);

        launcher.execute(request);

        TestExecutionSummary summary = listener.getSummary();
        PrintWriter w = new PrintWriter( System.out );
        if( summary.getTotalFailureCount() > 0 ) {
            try {
                summary.printFailuresTo( w, 30 ); // default is 10, that's not good enough for us
            } catch( Throwable t ) {
                log.error( "RunJUnit5: attempted to print", summary.getTotalFailureCount(), "failures, but exception occurred", t );
            }
        }
        summary.printTo( w );

        return summary.getTotalFailureCount() > 0 ? 1 : 0;
    }

    /**
     * A fatal invocation error has occurred.
     */
    static int fatalInvocation()
    {
        log.error( "Invalid invocation. Synopsis: [ " + PACKAGE_KEYWORD + " <package>... ] [ " + CLASSNAMEPATTERN_KEYWORD + " <pattern>... ]" );
        return 1;
    }
}
