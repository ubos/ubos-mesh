//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.testharness;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import net.ubos.util.ResourceHelper;

/**
 * This is NOT invoked as a diet4j ModuleInit by this Module. However, test Modules can
 * specify it (from outside this Module) and if they do so, it will initialize
 * the application ResourceHelper with reasonable defaults.
 */

public class InitializeModuleInit
{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        try {
            configureResourcesAndLogging( thisModule );

        } catch( Throwable t ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), t );
        }
    }

    /**
     * Configure application ResourceHelper with reasonable defaults
     *
     * @param thisModule the Module that contains the settings to use
     * @throws IOException a configuration file could not be accessed
     */
    public static void configureResourcesAndLogging(
            Module thisModule )
        throws
            IOException
    {
        ClassLoader loader = thisModule.getClassLoader();

        String moduleName = thisModule.getModuleArtifactId();
        String path       = moduleName.replaceAll( "\\.", "/" );

        String nameOfResourceHelperFile  = path + "/ResourceHelper.properties";

        if( nameOfResourceHelperFile.endsWith( ".properties" )) {
            nameOfResourceHelperFile = nameOfResourceHelperFile.substring( 0, nameOfResourceHelperFile.length() - ".properties".length() );
        }

        ResourceHelper.setApplicationResourceBundle( ResourceBundle.getBundle(
                nameOfResourceHelperFile,
                Locale.getDefault(),
                loader ));
    }
}
