//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import java.util.ArrayList;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.MeshObjectCreateChange;
import net.ubos.meshbase.transaction.MeshObjectDeleteChange;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;
import net.ubos.meshbase.transaction.TransactionListener;

/**
 * Tests that the MeshBase creates the right lifecycle events.
 */
public class MeshBaseTest16
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Setting up listener" );

        TestListener listener = new TestListener();
        theMeshBase.addDirectTransactionListener( listener );

        //

        log.info(  "Create a few objects" );

        MeshObject [] created = theMeshBase.execute( (tx) -> {
            return new MeshObject[] {
                    theMeshBase.createMeshObject( "obj-0" ),
                    theMeshBase.createMeshObject( "obj-1" ),
                    theMeshBase.createMeshObject( "obj-2" )
            };
        });
        MeshObject zero = created[0];
        MeshObject one  = created[1];
        MeshObject two  = created[2];

        checkEquals( listener.theCreatedEvents.size(), 3, "Wrong created events" );
        checkEquals( listener.theDeletedEvents.size(), 0, "Wrong deleted events" );

        checkIdentity( zero, listener.theCreatedEvents.get( 0 ).getChangedMeshObject(), "Wrong zero object" );
        checkIdentity( one,  listener.theCreatedEvents.get( 1 ).getChangedMeshObject(), "Wrong one object" );
        checkIdentity( two,  listener.theCreatedEvents.get( 2 ).getChangedMeshObject(), "Wrong two object" );

        listener.clear();

        //

        log.info( "Delete some objects" );

        theMeshBase.execute( () -> {
            theMeshBase.deleteMeshObjects( new MeshObject[] { zero, two } );
        });

        checkEquals( listener.theCreatedEvents.size(), 0, "Wrong created events" );
        checkEquals( listener.theDeletedEvents.size(), 2, "Wrong deleted events" );

        checkIdentity( zero, listener.theDeletedEvents.get( 0 ).getChangedMeshObject(), "Wrong zero object" );
        checkIdentity( two,  listener.theDeletedEvents.get( 1 ).getChangedMeshObject(), "Wrong two object" );
    }

    // Our Logger
    private static Log log = Log.getLogInstance( MeshBaseTest16.class );

    /**
     * Test listener.
     */
    protected static class TestListener
            implements
                TransactionListener
    {
        /**
         * {@inheritDoc }
         */
        @Override
        public void transactionCommitted(
                HeadTransaction tx )
        {
            for( Change current : tx.getChangeList() ) {

                if( current instanceof MeshObjectCreateChange ) {
                    theCreatedEvents.add((MeshObjectCreateChange) current );

                } else if( current instanceof MeshObjectDeleteChange ) {
                    theDeletedEvents.add((MeshObjectDeleteChange) current );
                }
            }
        }

        /**
         * Clear collected events.
         */
        public void clear()
        {
            theCreatedEvents.clear();
            theDeletedEvents.clear();
        }

        /**
         * Stores received created events.
         */
        protected ArrayList<MeshObjectCreateChange> theCreatedEvents = new ArrayList<>();

        /**
         * Stores received deleted events.
         */
        protected ArrayList<MeshObjectDeleteChange> theDeletedEvents = new ArrayList<>();
    }
}
