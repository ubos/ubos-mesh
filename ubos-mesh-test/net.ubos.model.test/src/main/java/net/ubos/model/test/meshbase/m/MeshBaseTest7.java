//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import java.util.HashMap;
import net.ubos.mesh.MeshObject;
import net.ubos.util.logging.Log;

/**
 * Tests that setting RoleAttributes creates relationships.
 */
public class MeshBaseTest7
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    public void run()
        throws
            Exception
    {
        log.info( "Creating MeshObjects" );

        MeshObject [] created = theMeshBase.execute( ( tx ) -> {
            MeshObject a = theMeshBase.createMeshObject( "a" );
            MeshObject b = theMeshBase.createMeshObject( "b" );
            MeshObject c = theMeshBase.createMeshObject( "c" );
            MeshObject d = theMeshBase.createMeshObject( "d" );
            MeshObject e = theMeshBase.createMeshObject( "e" );
            MeshObject f = theMeshBase.createMeshObject( "f" );

            return new MeshObject[] { a, b, c, d, e, f };
        });

        final MeshObject a = created[0];
        final MeshObject b = created[1];
        final MeshObject c = created[2];
        final MeshObject d = created[4];
        final MeshObject e = created[5];
        final MeshObject f = created[5];

        checkEquals( a.traverseToNeighbors().size(), 0, "a has neighbors" );
        checkEquals( b.traverseToNeighbors().size(), 0, "b has neighbors" );
        checkEquals( c.traverseToNeighbors().size(), 0, "c has neighbors" );
        checkEquals( d.traverseToNeighbors().size(), 0, "d has neighbors" );
        checkEquals( e.traverseToNeighbors().size(), 0, "e has neighbors" );
        checkEquals( f.traverseToNeighbors().size(), 0, "f has neighbors" );

        //

        log.info( "Setting RoleAttributes" );

        theMeshBase.execute( ( tx ) -> {
            a.setRoleAttributeValue( b, "ab",  true );
            a.setRoleAttributeValue( c, "ac",  null );
            a.setRoleAttributeValue( d, "ad",  1 );
            b.setRoleAttributeValue( c, "bc",  "string" );
            d.setRoleAttributeValue( e, "de",  new HashMap<Integer,Integer>() );
            d.setRoleAttributeValue( e, "de2", 'x' );
            e.setRoleAttributeValue( f, "ef",  new Object[ 5 ] );
            return null;
        } );

        //

        log.debug( "Checking neighbors" );

        checkEqualsOutOfSequence( a.traverseToNeighbors().getMeshObjects(), new MeshObject[] { b, c, d }, "Wrong neighbors for a" );
        checkEqualsOutOfSequence( b.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, c    }, "Wrong neighbors for b" );
        checkEqualsOutOfSequence( c.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, b    }, "Wrong neighbors for c" );
        checkEqualsOutOfSequence( d.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, e    }, "Wrong neighbors for d" );
        checkEqualsOutOfSequence( e.traverseToNeighbors().getMeshObjects(), new MeshObject[] { d, f    }, "Wrong neighbors for e" );
        checkEqualsOutOfSequence( f.traverseToNeighbors().getMeshObjects(), new MeshObject[] { e       }, "Wrong neighbors for e" );

        //

        log.info( "Removing a RoleAttribute (1) -- not sufficient to delete the relationship" );

        theMeshBase.execute( ( tx ) -> {
            d.deleteRoleAttribute( e, "de" ) ;
            return null;
        } );

        //

        log.debug( "Checking neighbors" );

        checkEqualsOutOfSequence( a.traverseToNeighbors().getMeshObjects(), new MeshObject[] { b, c, d }, "Wrong neighbors for a" );
        checkEqualsOutOfSequence( b.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, c    }, "Wrong neighbors for b" );
        checkEqualsOutOfSequence( c.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, b    }, "Wrong neighbors for c" );
        checkEqualsOutOfSequence( d.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, e    }, "Wrong neighbors for d" );
        checkEqualsOutOfSequence( e.traverseToNeighbors().getMeshObjects(), new MeshObject[] { d, f    }, "Wrong neighbors for e" );
        checkEqualsOutOfSequence( f.traverseToNeighbors().getMeshObjects(), new MeshObject[] { e       }, "Wrong neighbors for e" );

        //

        log.info( "Removing a RoleAttribute (2) -- sufficient to delete the relationship" );

        theMeshBase.execute( ( tx ) -> {
            d.deleteRoleAttribute( e, "de2" ) ;
            return null;
        } );

        //

        log.debug( "Checking neighbors" );

        checkEqualsOutOfSequence( a.traverseToNeighbors().getMeshObjects(), new MeshObject[] { b, c, d }, "Wrong neighbors for a" );
        checkEqualsOutOfSequence( b.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, c    }, "Wrong neighbors for b" );
        checkEqualsOutOfSequence( c.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a, b    }, "Wrong neighbors for c" );
        checkEqualsOutOfSequence( d.traverseToNeighbors().getMeshObjects(), new MeshObject[] { a       }, "Wrong neighbors for d" );
        checkEqualsOutOfSequence( e.traverseToNeighbors().getMeshObjects(), new MeshObject[] { f       }, "Wrong neighbors for e" );
        checkEqualsOutOfSequence( f.traverseToNeighbors().getMeshObjects(), new MeshObject[] { e       }, "Wrong neighbors for e" );

        //

        log.info( "Deleting the a MeshObject" );

        theMeshBase.execute( ( tx ) -> {
            tx.getMeshBase().deleteMeshObject( a );

            return null;
        } );

        //

        log.debug( "Checking neighbors" );

        checkCondition( a.getIsDead(), "a is not dead" );
        checkEqualsOutOfSequence( b.traverseToNeighbors().getMeshObjects(), new MeshObject[] { c    }, "Wrong neighbors for b" );
        checkEqualsOutOfSequence( c.traverseToNeighbors().getMeshObjects(), new MeshObject[] { b    }, "Wrong neighbors for c" );
        checkEqualsOutOfSequence( d.traverseToNeighbors().getMeshObjects(), new MeshObject[] {      }, "Wrong neighbors for d" );
        checkEqualsOutOfSequence( e.traverseToNeighbors().getMeshObjects(), new MeshObject[] { f    }, "Wrong neighbors for e" );
        checkEqualsOutOfSequence( f.traverseToNeighbors().getMeshObjects(), new MeshObject[] { e    }, "Wrong neighbors for e" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest7.class );
}
