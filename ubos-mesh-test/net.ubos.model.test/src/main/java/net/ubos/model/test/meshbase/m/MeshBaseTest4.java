//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotBlessedException;
import net.ubos.mesh.RoleTypeRequiresEntityTypeException;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.Pair;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that we can create and delete relationships between MeshObjects and
 * that we can traverse them
 */
public class MeshBaseTest4
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating MeshObjects" );

        Pair<MeshObject,MeshObject> ret = theMeshBase.execute( ( tx ) -> {
            MeshObject a = theMeshBase.createMeshObject( TestSubjectArea.AA );
            MeshObject b = theMeshBase.createMeshObject( TestSubjectArea.B );

            return new Pair<>( a, b );
        } );
        MeshObject a = ret.getName();
        MeshObject b = ret.getValue();

        //

        log.info( "blessing the relationship" );

        theMeshBase.execute( () -> a.blessRole( TestSubjectArea.RR.getSource(), b ));

        //

        log.info( "attempting to bless the wrong relationship" );

        theMeshBase.execute( () -> {
            try {
                a.blessRole( TestSubjectArea.AR1A.getSource(), b );

                reportError( "Was able to bless relationship between wrongly blessed objects" );

            } catch( NotBlessedException ex ) {
                // okay
            }
        });

        //

        log.info( "Trying a traversal" );

        MeshObjectSet other = a.traverse( TestSubjectArea.RR.getSource() );

        checkEquals( 1, other.size(), "did not reach destination" );
        checkEquals( b, other.getMeshObjects()[0], "wrong destination" );

        log.info( "Trying reverse traversal" );

        MeshObjectSet first = b.traverse( TestSubjectArea.RR.getDestination() );
        checkEquals( 1, first.size(), "did not reach source" );

        //

        log.info( "Trying a supertype traversal" );

        other = a.traverse( TestSubjectArea.R.getSource() );

        checkEquals( 1, other.size(), "did not reach destination" );
        checkEquals( b, other.getMeshObjects()[0], "wrong destination" );

        log.info( "Trying reverse supertype traversal" );

        first = b.traverse( TestSubjectArea.R.getDestination() );
        checkEquals( 1, first.size(), "did not reach source" );

        //

        log.info( "attempting to unbless object" );

        theMeshBase.execute( () -> {
            try {
                b.unbless( TestSubjectArea.B );
                reportError( "Was able to unbless in spite of attached relationship role requirements" );

            } catch( RoleTypeRequiresEntityTypeException ex ) {
                // no op
            }
        } );

        //

        log.info( "attempting to unbless again after relationship unblessed" );

        theMeshBase.execute( () -> {
            a.unblessRole( TestSubjectArea.RR.getSource(), b );
            b.unbless( TestSubjectArea.B );
        } );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest4.class);
}
