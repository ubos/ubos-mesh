//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.meshbase.transaction.MeshObjectRoleTypeAddChange;
import net.ubos.meshbase.transaction.MeshObjectRoleTypeRemoveChange;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;

/**
 * This tests that blessing and unblessing relationships creates the right events.
 */
public class MeshBaseTest6
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    public void run()
        throws
            Exception
    {
        log.info( "Creating MeshObjects" );

        MeshObject [] created = theMeshBase.execute( ( tx ) -> {
            MeshObject a = theMeshBase.createMeshObject( TestSubjectArea.AA );
            MeshObject b = theMeshBase.createMeshObject( TestSubjectArea.B );

            return new MeshObject[] { a, b };
        });

        final AMeshObject a = (AMeshObject) created[0];
        final AMeshObject b = (AMeshObject) created[1];

        MeshObjectTransactionListener listenerA = new MeshObjectTransactionListener( a );
        MeshObjectTransactionListener listenerB = new MeshObjectTransactionListener( b );
        theMeshBase.addWeakTransactionListener( listenerA );
        theMeshBase.addWeakTransactionListener( listenerB );

        //

        log.info( "Blessing relationship between MeshObjects" );

        checkEquals( a.getNeighborMeshObjectIdentifiers().length, 0, "Has neighbors" );
        checkEquals( b.getNeighborMeshObjectIdentifiers().length, 0, "Has neighbors" );

        theMeshBase.execute( ( tx ) -> {
            a.blessRole( TestSubjectArea.R.getSource(), b );
            return null;
        } );

        checkEquals( a.getNeighborMeshObjectIdentifiers().length, 1, "Does not have neighbor" );
        checkEquals( b.getNeighborMeshObjectIdentifiers().length, 1, "Does not have neighbor" );

        //

        log.debug( "Checking events" );

        checkEquals( listenerA.theEvents.size(), 1, "Wrong number of events at object A" );
        checkEquals( listenerB.theEvents.size(), 1, "Wrong number of events at object B" );

        checkCondition(listenerA.theEvents.get( 0 ) instanceof MeshObjectRoleTypeAddChange, "Wrong type" );
        checkCondition(listenerB.theEvents.get( 0 ) instanceof MeshObjectRoleTypeAddChange, "Wrong type" );

        listenerA.clear();
        listenerB.clear();

        //

        log.info( "Unblessing relationship between MeshObjects" );

        theMeshBase.execute( ( tx ) -> {
            a.unblessRole( TestSubjectArea.R.getSource(), b );
            return null;
        } );

        //

        log.debug( "Checking events" );

        checkEquals( listenerA.theEvents.size(), 1, "Wrong number of events at object A" );
        checkEquals( listenerB.theEvents.size(), 1, "Wrong number of events at object B" );

        checkCondition(listenerA.theEvents.get( 0 ) instanceof MeshObjectRoleTypeRemoveChange, "Wrong type" );
        checkCondition(listenerB.theEvents.get( 0 ) instanceof MeshObjectRoleTypeRemoveChange, "Wrong type" );

        checkEquals( a.getNeighborMeshObjectIdentifiers().length, 0, "Still has neighbors" );
        checkEquals( b.getNeighborMeshObjectIdentifiers().length, 0, "Still has neighbors" );

        //
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest6.class );
}
