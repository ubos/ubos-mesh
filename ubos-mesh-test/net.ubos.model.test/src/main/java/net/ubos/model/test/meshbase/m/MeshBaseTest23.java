//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Make sure relationship changes trigger changes to TimeUpdated.
 */
public class MeshBaseTest23
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final MeshObjectIdentifier A = theMeshBase.createMeshObjectIdentifier( "a" );
        final MeshObjectIdentifier B = theMeshBase.createMeshObjectIdentifier( "b" );

        startClock();

        //

        log.info( "Setup" );

        theMeshBase.execute( () -> {
            theMeshBase.createMeshObject( A );
            theMeshBase.createMeshObject( B );
        } );

        long t0 = theMeshBase.findMeshObjectByIdentifier( A ).getTimeCreated();
        checkEquals( t0, theMeshBase.findMeshObjectByIdentifier( A ).getTimeUpdated(), "Wrong update time for a" );
        checkEquals( t0, theMeshBase.findMeshObjectByIdentifier( B ).getTimeCreated(), "Wrong create time for b" );
        checkEquals( t0, theMeshBase.findMeshObjectByIdentifier( B ).getTimeUpdated(), "Wrong update time for b" );

        //

        sleepUntil( 100 );

        log.info( "Relate" );

        theMeshBase.execute( () -> {
            MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
            MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );

            a.setRoleAttributeValue( b, "ATT", "t1" );
        });

        long t1 = theMeshBase.findMeshObjectByIdentifier( A ).getTimeUpdated();
        checkNotEquals( t0, t1, "t0 and t1 are same" );

        checkEquals( t0, theMeshBase.findMeshObjectByIdentifier( A ).getTimeCreated(), "Wrong create time for a" );
        checkEquals( t0, theMeshBase.findMeshObjectByIdentifier( B ).getTimeCreated(), "Wrong create time for b" );
        checkEquals( t1, theMeshBase.findMeshObjectByIdentifier( B ).getTimeUpdated(), "Wrong update time for b" );

        //

        sleepUntil( 200 );

        log.info( "Unrelate" );

        theMeshBase.execute( () -> {
            MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
            MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );

            a.unrelate( b );
        });

        long t2 = theMeshBase.findMeshObjectByIdentifier( A ).getTimeUpdated();
        checkNotEquals( t0, t2, "t0 and t2 are same" );
        checkNotEquals( t1, t2, "t1 and t2 are same" );

        checkEquals( t0, theMeshBase.findMeshObjectByIdentifier( A ).getTimeCreated(), "Wrong create time for a" );
        checkEquals( t0, theMeshBase.findMeshObjectByIdentifier( B ).getTimeCreated(), "Wrong create time for b" );
        checkEquals( t2, theMeshBase.findMeshObjectByIdentifier( B ).getTimeUpdated(), "Wrong update time for b" );

        //
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest23.class );
}