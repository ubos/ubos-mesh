//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * <p>Tests the "common neighbors" functionality with RoleTypes.
 * <pre>
 * obj1(AA) -R- obj2(B) -R- objCenter(AA) -R- obj4(B) -R- obj3(AA)
 *                             | (RR)
 *                           obj6(B)
 *                             | (R)
 *                           obj5(AA)
 * </pre>
 */
public class MeshBaseTest12
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Create a few MeshObjects" );

        MeshObject [] created = theMeshBase.execute( (tx) -> {
            MeshObject obj1 = theMeshBase.createMeshObject( TestSubjectArea.AA );
            MeshObject obj2 = theMeshBase.createMeshObject( TestSubjectArea.B );
            MeshObject obj3 = theMeshBase.createMeshObject( TestSubjectArea.AA );
            MeshObject obj4 = theMeshBase.createMeshObject( TestSubjectArea.B );
            MeshObject obj5 = theMeshBase.createMeshObject( TestSubjectArea.AA );
            MeshObject obj6 = theMeshBase.createMeshObject( TestSubjectArea.B );
            MeshObject objCenter = theMeshBase.createMeshObject( TestSubjectArea.AA );

            obj1.blessRole( TestSubjectArea.R.getSource(),      obj2      );
            obj2.blessRole( TestSubjectArea.R.getDestination(), objCenter );

            obj3.blessRole( TestSubjectArea.R.getSource(),      obj4 );
            obj4.blessRole( TestSubjectArea.R.getDestination(), objCenter );

            obj5.blessRole( TestSubjectArea.R.getSource(),       obj6 );
            obj6.blessRole( TestSubjectArea.RR.getDestination(), objCenter );

            return new MeshObject[] {
                obj1,
                obj2,
                obj3,
                obj4,
                obj5,
                obj6,
                objCenter,
            };
        } );

        MeshObject obj1 = created[0];
        MeshObject obj2 = created[1];
        MeshObject obj3 = created[2];
        MeshObject obj4 = created[3];
        MeshObject obj5 = created[4];
        MeshObject obj6 = created[5];
        MeshObject objCenter = created[6];

        //

        log.info( "Checking we got it right" );

        checkEquals( obj1.traverseToNeighbors().size(), 1, "obj1 neighbors are wrong" );
        checkEquals( obj2.traverseToNeighbors().size(), 2, "obj2 neighbors are wrong" );
        checkEquals( obj3.traverseToNeighbors().size(), 1, "obj3 neighbors are wrong" );
        checkEquals( obj4.traverseToNeighbors().size(), 2, "obj4 neighbors are wrong" );
        checkEquals( obj5.traverseToNeighbors().size(), 1, "obj5 neighbors are wrong" );
        checkEquals( obj6.traverseToNeighbors().size(), 2, "obj6 neighbors are wrong" );
        checkEquals( objCenter.traverseToNeighbors().size(), 3, "objCenter neighbors are wrong" );

        //

        log.info( "Creating and checking set2a" );

        MeshObjectSet set2a = theMeshBase.findCommonNeighbors( obj1, TestSubjectArea.R.getSource(), objCenter, TestSubjectArea.R.getSource() );
        checkEquals( set2a.size(), 1, "Wrong size of set2a" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        set2a.getMeshObjects(),
                        new MeshObject[] { obj2 },
                        false ),
                "set2a has wrong content" );

        //

        log.info( "Creating and checking set2b" );

        MeshObjectSet set2b = theMeshBase.findCommonNeighbors( obj1, TestSubjectArea.R.getDestination(), objCenter, TestSubjectArea.R.getSource() );
        checkEquals( set2b.size(), 0, "Wrong size of set2b" );

        //

        log.info( "Creating and checking set34" );

        MeshObjectSet set34 = theMeshBase.findCommonNeighbors( obj3, obj4 );
        checkEquals( set34.size(), 0, "Wrong size of set34" );

        //

        log.info( "Creating and checking set4" );

        MeshObjectSet set4 = theMeshBase.findCommonNeighbors( obj3, objCenter );
        checkEquals( set4.size(), 1, "Wrong size of set4" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        set4.getMeshObjects(),
                        new MeshObject[] { obj4 },
                        false ),
                "set4 has wrong content" );

        //

        log.info( "Creating and checking setCenterA" );

        MeshObjectSet setCenterA = theMeshBase.findCommonNeighbors( obj2, obj4 );
        checkEquals( setCenterA.size(), 1, "Wrong size of setCenterA" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        setCenterA.getMeshObjects(),
                        new MeshObject[] { objCenter },
                        false ),
                "setCenterA has wrong content" );

        //

        log.info( "Creating and checking setCenterB" );

        MeshObjectSet setCenterB = theMeshBase.findCommonNeighbors( new MeshObject[] { obj2, obj4, obj6 } );
        checkEquals( setCenterB.size(), 1, "Wrong size of setCenterB" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        setCenterB.getMeshObjects(),
                        new MeshObject[] { objCenter },
                        false ),
                "setCenterB has wrong content" );

        //

        log.info( "Creating and checking setCenterC" );

        MeshObjectSet setCenterC = theMeshBase.findCommonNeighbors( new MeshObject[] { obj2, obj4, obj5 } );
        checkEquals( setCenterC.size(), 0, "Wrong size of setCenterC" );
    }

    // Our Logger
    private static Log log = Log.getLogInstance( MeshBaseTest12.class );
}
