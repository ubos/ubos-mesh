//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.modelbase;

import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.L10PropertyValueMapImpl;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierDeserializer;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.InheritanceConflictException;
import net.ubos.modelbase.MeshTypeLifecycleManager;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;

/**
 * Instantiates a test model.
 */
public class ModelBaseTest3
        extends
            AbstractModelBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    public void run()
        throws
            Exception
    {
        MeshTypeLifecycleManager       life   = ModelBase.SINGLETON.getMeshTypeLifecycleManager();
        MeshTypeIdentifierDeserializer idFact = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

        ClassLoader loader = getClass().getClassLoader();

        //

        log.info( "Creating test Model" );

        SubjectArea sa = life.createSubjectArea(
                idFact.fromExternalForm( "Test.Sa/sa"),
                StringValue.create( "TestSa" ),
                L10PropertyValueMapImpl.create( StringValue.create( "Test SA" ) ),
                null,
                new SubjectArea[0],
                loader );
        ModelBase.SINGLETON.checkSubjectArea( sa );

        EntityType a = life.createEntityType(
                idFact.fromExternalForm( "pre.fix/A" ),
                StringValue.create( "A" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is a" ) ),
                null,
                null,
                sa,
                new EntityType[0],
                new MeshTypeIdentifier[0],
                BooleanValue.FALSE,
                BooleanValue.FALSE );

        PropertyType a_name = life.createPropertyType(
                idFact.fromExternalForm( "pre.fix/A_Name" ),
                StringValue.create( "Name" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is a name" ) ),
                null,
                a,
                sa,
                StringDataType.theDefault,
                null,
                BooleanValue.TRUE,
                BooleanValue.FALSE,
                FloatValue.create( 123.0 ) );
        ModelBase.SINGLETON.checkPropertyType( a_name );
        ModelBase.SINGLETON.checkEntityType( a );

        EntityType b = life.createEntityType(
                idFact.fromExternalForm( "pre.fix/B" ),
                StringValue.create( "B" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is b name" ) ),
                null,
                null,
                sa,
                new EntityType[] { a },
                new MeshTypeIdentifier[0],
                BooleanValue.FALSE,
                BooleanValue.FALSE );

        PropertyType b_name = life.createOverridingPropertyType(
                new PropertyType[] { a_name },
                idFact.fromExternalForm( "pre.fix/B_Name" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is b name" ) ),
                b,
                sa,
                StringDataType.theDefault,
                null,
                BooleanValue.TRUE,
                BooleanValue.FALSE );
        ModelBase.SINGLETON.checkPropertyType( b_name );
        ModelBase.SINGLETON.checkEntityType( b );

        EntityType c = life.createEntityType(
                idFact.fromExternalForm( "pre.fix/C" ),
                StringValue.create( "C" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is c" ) ),
                null,
                null,
                sa,
                new EntityType[] { a },
                new MeshTypeIdentifier[0],
                BooleanValue.FALSE,
                BooleanValue.FALSE );

        PropertyType c_name = life.createOverridingPropertyType(
                new PropertyType[] { a_name },
                idFact.fromExternalForm( "pre.fix/C_Name" ),
                null,
                c,
                sa,
                StringDataType.theDefault,
                null,
                BooleanValue.TRUE,
                BooleanValue.FALSE );
        ModelBase.SINGLETON.checkPropertyType( c_name );
        ModelBase.SINGLETON.checkEntityType( c );

        EntityType d = life.createEntityType(
                idFact.fromExternalForm( "pre.fix/D" ),
                StringValue.create( "D" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is d" ) ),
                null,
                null,
                sa,
                new EntityType[] { a },
                new MeshTypeIdentifier[0],
                BooleanValue.FALSE,
                BooleanValue.FALSE );
        ModelBase.SINGLETON.checkEntityType( d );

        // we don't do an override on D

        EntityType e = life.createEntityType(
                idFact.fromExternalForm( "pre.fix/E" ),
                StringValue.create( "E" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is e" ) ),
                null,
                null,
                sa,
                new EntityType[] { d },
                new MeshTypeIdentifier[0],
                BooleanValue.FALSE,
                BooleanValue.FALSE );

        PropertyType e_name = life.createOverridingPropertyType(
                new PropertyType[] { a_name },
                idFact.fromExternalForm( "pre.fix/E_Name" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is e name" ) ),
                e,
                sa,
                StringDataType.theDefault,
                null,
                BooleanValue.TRUE,
                BooleanValue.FALSE );
        ModelBase.SINGLETON.checkPropertyType( e_name );
        ModelBase.SINGLETON.checkEntityType( e );

        boolean exceptionThrown = false;
        try {
            EntityType f = life.createEntityType(
                    idFact.fromExternalForm( "pre.fix/F" ),
                    StringValue.create( "F" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "This is f" ) ),
                    null,
                    null,
                    sa,
                    new EntityType[] { b, c },
                    new MeshTypeIdentifier[0],
                    BooleanValue.FALSE,
                    BooleanValue.FALSE );
            ModelBase.SINGLETON.checkEntityType( f );
        } catch( InheritanceConflictException ex ) {
            exceptionThrown = true;
        }
        checkCondition( exceptionThrown, "exception was not thrown when creating a conflict on meta-entity F" );

        exceptionThrown = false;
        try {
            EntityType g = life.createEntityType(
                    idFact.fromExternalForm( "pre.fix/G" ),
                    StringValue.create( "G" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "This is g" ) ),
                    null,
                    null,
                    sa,
                    new EntityType[] { c, e },
                    new MeshTypeIdentifier[0],
                    BooleanValue.FALSE,
                    BooleanValue.FALSE );
            ModelBase.SINGLETON.checkEntityType( g );
        } catch( InheritanceConflictException ex ) {
            exceptionThrown = true;
        }
        checkCondition( exceptionThrown, "exception was not thrown when creating a conflict on meta-entity G" );

        EntityType h = life.createEntityType(
                idFact.fromExternalForm( "pre.fix/H" ),
                StringValue.create( "H" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is h" ) ),
                null,
                null,
                sa,
                new EntityType[] { c, d },
                new MeshTypeIdentifier[0],
                BooleanValue.FALSE,
                BooleanValue.FALSE );

        PropertyType h_name = life.createOverridingPropertyType(
                new PropertyType[] { c_name },
                idFact.fromExternalForm( "pre.fix/H_Name" ),
                null,
                h,
                sa,
                StringDataType.theDefault,
                null,
                BooleanValue.TRUE,
                BooleanValue.FALSE );
        ModelBase.SINGLETON.checkPropertyType( h_name );
        ModelBase.SINGLETON.checkEntityType( h );

        EntityType i = life.createEntityType(
                idFact.fromExternalForm( "pre.fix/I" ),
                StringValue.create( "I" ),
                L10PropertyValueMapImpl.create( StringValue.create( "This is i" ) ),
                null,
                null,
                sa,
                new EntityType[] { c },
                new MeshTypeIdentifier[0],
                BooleanValue.FALSE,
                BooleanValue.FALSE );

        exceptionThrown = false;
        try {
            PropertyType i_name = life.createOverridingPropertyType(
                    new PropertyType[] { a_name },
                    idFact.fromExternalForm( "pre.fix/I_Name" ),
                    null,
                    i,
                    sa,
                    StringDataType.theDefault,
                    null,
                    BooleanValue.TRUE,
                    BooleanValue.FALSE );
            ModelBase.SINGLETON.checkPropertyType( i_name );
            ModelBase.SINGLETON.checkEntityType( i );
        } catch( InheritanceConflictException ex ) {
            exceptionThrown = true;
        }
        checkCondition( exceptionThrown, "exception was not thrown when creating a conflict on meta-entity I" );

        //

        log.info( "checking" );

        EntityType [] all = new EntityType[] { a, b, c, e, h }; // don't check d, it doesn't have a local override
        for( int k=0 ; k<all.length ; ++k ) {
            log.debug( "now looking at EntityType " + all[k] );

            PropertyType [] allPts       = all[k].getPropertyTypes();
            PropertyType [] localPts     = all[k].getLocalPropertyTypes();
            PropertyType [] localOverPts = all[k].getOverridingLocalPropertyTypes();

            checkEquals( allPts.length, 1, "incorrect number of all PropertyTypes" );
            checkEquals( localPts.length + localOverPts.length, 1, "incorrect number of local PropertyTypes" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ModelBaseTest3.class);
}
