//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that an otherwise unrelated MeshObject can be created and updated in the past.
 *
 * First pass in the first column  |   second pass later in the second column
 * t0: create a
 *     a.att = 0
 *                                     t1: create b
 *                                         b.att = 1
 * t2: a.att = 2
 */
public class EditableHistoryTest4
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT_NAME = "att";
        final long   delta    = 200L;

        Map<Long,String> captureTs = new HashMap<>();

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        long startTime = startClock();

        // printHistory( "Beginning", theMeshBase );

        //

        log.info( "t0: create a, a.att = 0" );

        MeshObject a = theMeshBase.execute(
                (Transaction tx ) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT_NAME, 0 );
                    return ret;
                });
        captureTs.put( a.getTimeUpdated(), "t0" );
        checkStockFlowIntegrity( "t0", captureTs );

        // printHistory( "After t0", theMeshBase );

        //

        log.info( "t2: a.att = 2" );

        sleepUntil( delta * 2 );
        theMeshBase.execute( () -> a.setAttributeValue( ATT_NAME, 2 ) );
        captureTs.put( a.getTimeUpdated(), "t2" );
        checkStockFlowIntegrity( "t2", captureTs );

        // printHistory( "After t2", theMeshBase );

        //

        log.info( "t1: create b, b.att = 1" );

        MeshObject b = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    MeshObject ret = theMeshBase.createMeshObject( B );
                    ret.setAttributeValue( ATT_NAME, 1 );
                    return ret;
                });
        captureTs.put( b.getTimeUpdated(), "t1" );
        checkStockFlowIntegrity( "t1", captureTs );

        // printHistory( "Going back in history: after t1", theMeshBase );

        log.info( "Checking history" );

        checkEquals( theMeshBase.meshObjectHistory( A ).getLength(), 2, "Wrong length of history for a" );
        checkEquals( theMeshBase.meshObjectHistory( B ).getLength(), 1, "Wrong length of history for b" );
        checkEquals( theMeshBase.getHistory().getLength(), 4, "Wrong length of history for MeshBase" );

        log.info( "Checking HEAD" );

        MeshObject aHead = theMeshBase.findMeshObjectByIdentifier( a.getIdentifier() );
        MeshObject bHead = theMeshBase.findMeshObjectByIdentifier( b.getIdentifier() );

        checkObject( aHead, "No a in HEAD" );
        checkObject( bHead, "No b in HEAD" );
        checkIdentity( a, aHead, "Different a" );
        checkNotIdentity( b, bHead, "Same b" );
        checkEquals( b.getTimeCreated(), bHead.getTimeCreated(), "b not created at same time" );
        checkEquals( b.getTimeUpdated(), bHead.getTimeUpdated(), "b not updated at same time" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( EditableHistoryTest4.class );
}
