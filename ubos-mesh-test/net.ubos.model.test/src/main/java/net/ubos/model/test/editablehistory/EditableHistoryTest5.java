//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that an otherwise unrelated MeshObject can be created and updated at various points in the past.
 *
 * First pass in the first column  |   second pass later in the second column
 * t0: create a
 *     a.att = 0
 *                                     t1: create b
 *                                         b.att = 1
 * t2: a.att = 2
 *                                     t3: b.att = 3
 * t4: a.att = 4
 *                                     t5: b.att = 5
 * t6: a.att = 6
 */
public class EditableHistoryTest5
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT_NAME = "att";
        final long   delta    = 200L;

        Map<Long,String> captureTs = new HashMap<>();
        captureTs.put( theMeshBase.getHomeObject().getTimeUpdated(), "t_home" );


        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        long startTime = startClock();

        //

        log.info( "t0: create a, a.att = 0" );

        MeshObject a = theMeshBase.execute(
                (Transaction tx ) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT_NAME, 0 );
                    return ret;
                });
        captureTs.put( a.getTimeUpdated(), "t0" );

        //

        log.info( "t2: a.att = 2" );

        sleepUntil( delta * 2 );
        theMeshBase.execute( () -> a.setAttributeValue( ATT_NAME, 2 ) );
        captureTs.put( a.getTimeUpdated(), "t2" );

        //

        log.info( "t4: a.att = 4" );

        sleepUntil( delta * 4 );
        theMeshBase.execute( () ->  a.setAttributeValue( ATT_NAME, 4 ) );
        captureTs.put( a.getTimeUpdated(), "t4" );

        //

        log.info( "t6: a.att = 6" );

        sleepUntil( delta * 6 );
        theMeshBase.execute( () ->  a.setAttributeValue( ATT_NAME, 6 ) );
        captureTs.put( a.getTimeUpdated(), "t6" );

        // printHistory( "After t6", theMeshBase, captureTs );

        checkStockFlowIntegrity( "t6", captureTs );
        checkEquals( theMeshBase.getHistory().getLength(), 5, "Wrong length of history" );

        //

        log.info( "t1: create b, b.att = 1" );

        MeshObject b = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    MeshObject ret = theMeshBase.createMeshObject( B );
                    ret.setAttributeValue( ATT_NAME, 1 );
                    return ret;
                });
        captureTs.put( b.getTimeUpdated(), "t1" );

        // printHistory( "Going back in history: after t1", theMeshBase, captureTs );

        checkStockFlowIntegrity( "t1", captureTs );

        //

        log.info( "t3: b.att = 3" );

        MeshObject bAtT3 = theMeshBase.executeAt(
                startTime + 3*delta,
                ( tx ) -> {
                    MeshObject rightB = theMeshBase.findMeshObjectByIdentifier( B );
                    rightB.setAttributeValue( ATT_NAME, 3 );
                    return rightB;
                });
        captureTs.put( bAtT3.getTimeUpdated(), "t3" );

        // printHistory( "Going back in history: after t3", theMeshBase, captureTs );

        checkStockFlowIntegrity( "t3", captureTs );


        //

        log.info( "t5: b.att = 5" );

        MeshObject bAtT5 = theMeshBase.executeAt(
                startTime + 5*delta,
                ( tx ) -> {
                    MeshObject rightB = theMeshBase.findMeshObjectByIdentifier( B );
                    rightB.setAttributeValue( ATT_NAME, 5 );
                    return rightB;
                });
        captureTs.put( bAtT5.getTimeUpdated(), "t5" );

        // printHistory( "Going back in history: after t5", theMeshBase, captureTs );

        checkStockFlowIntegrity( "t5", captureTs );


        //

        checkEquals( theMeshBase.meshObjectHistory( A ).getLength(), 4, "Wrong length of history for a" );
        checkEquals( theMeshBase.meshObjectHistory( B ).getLength(), 3, "Wrong length of history for b" );
        checkEquals( theMeshBase.getHistory().getLength(), 8, "Wrong length of history for MeshBase" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( EditableHistoryTest5.class );
}
