//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that a MeshObject inserted into the history shows up in the head version.
 */
public class EditableHistoryTest6
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT_NAME = "att";
        final long   delta    = 200L;

        //

        Map<Long,String> captureTs = new HashMap<>();

        long startTime = startClock();

        checkNotObject( theMeshBase.findMeshObjectByIdentifier( A ), "a is there already" );

        //

        sleepUntil( 2*delta );

        theMeshBase.execute(
               () -> theMeshBase.getHomeObject().setAttributeValue( ATT_NAME, 1 )); // just do something

        captureTs.put( theMeshBase.getHomeObject().getTimeUpdated(), "t2" );
        checkStockFlowIntegrity( "t2", captureTs );

        //

        MeshObject a = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT_NAME, 1 );
                    return ret;
                });

        captureTs.put( a.getTimeUpdated(), "t1" );
        checkStockFlowIntegrity( "t1", captureTs );

        //

        MeshObject        aHead    = theMeshBase.findMeshObjectByIdentifier( A );
        MeshObjectHistory aHistory = theMeshBase.meshObjectHistory( A );

        checkObject( aHead, "a not found in HEAD" );
        checkEquals( aHead.getAttributeValue( ATT_NAME ), 1, "wrong value" );
        checkEquals( aHistory.getLength(), 1, "Wrong length of history" );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest6.class );
}
