//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.testharness.AbstractTest;

/**
 * Factors out common features of tests in this package.
 */
public abstract class AbstractMeshBaseTest
        extends
            AbstractTest
{
}
