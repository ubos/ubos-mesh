//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests whether we can create MeshObjects, and whether their time created / updated times are right.
 */
public class MeshBaseTest1
        extends
            AbstractMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        long t1 = System.currentTimeMillis();

        MMeshBase theMeshBase = MMeshBase.Builder.create().build();

        long t2 = System.currentTimeMillis();

        checkObject( theMeshBase.getHomeObject(), "No home object" );
        checkIdentity( theMeshBase.getHomeObject().getMeshBaseView(), theMeshBase.getHeadMeshBaseView(), "Home object in wrong MeshBaseView" );
        checkIdentity( theMeshBase.getHomeObject().getMeshBase(), theMeshBase, "Home object in wrong MeshBase" );
        checkInRange( theMeshBase.getHomeObject().getTimeCreated(), t1, t2, "Home object created at wrong time" );
        checkInRange( theMeshBase.getHomeObject().getTimeUpdated(), t1, t2, "Home object updated at wrong time" );

        //

        log.info( "Checking that transactions are required" );

        try {
            theMeshBase.createMeshObject();
            reportError( "createMeshObject did not throw TransactionException" );
        } catch( TransactionException ex ) {
            // noop
        }


        //

        log.info( "Creating MeshObjects" );

        t1 = System.currentTimeMillis();
        MeshObject [] mesh = new MeshObject[ 1000 ];

        theMeshBase.execute( (Transaction tx) -> {
            for( int i=0 ; i<mesh.length ; ++i ) {
                mesh[i] = theMeshBase.createMeshObject();
            }

            return null;
        });

        t2 = System.currentTimeMillis();
        for( int i=0 ; i<mesh.length ; ++i ) {
            checkObject( mesh[i], "MeshObject " + i + "is null" );
            checkIdentity( mesh[i].getMeshBaseView(), theMeshBase.getHeadMeshBaseView(), "MeshObject " + i + " in wrong MeshBase" );
            checkIdentity( mesh[i].getMeshBase(), theMeshBase, "MeshObject " + i + " in wrong MeshBase" );
            checkInRange(
                    mesh[i].getTimeCreated(),
                    ( i==0 ) ? t1 : mesh[i-1].getTimeCreated(),
                    ( i==mesh.length-1 ) ? t2 : mesh[i+1].getTimeCreated(),
                    "MeshObject " + i + " created at wrong time" );
            checkInRange(
                    mesh[i].getTimeUpdated(),
                    ( i==0 ) ? t1 : mesh[i-1].getTimeUpdated(),
                    ( i==mesh.length-1 ) ? t2 : mesh[i+1].getTimeUpdated(),
                    "MeshObject " + i + " updated at wrong time" );
        }

        //

        log.info( "Duplicate Identifiers not allowed" );

        theMeshBase.execute((Transaction tx) -> {

            try{
                theMeshBase.createMeshObject( mesh[0].getIdentifier() );
                reportError( "createMeshObject with duplicate Identifier did not throw IdentifierNotUniqueException" );
            } catch( MeshObjectIdentifierNotUniqueException ex ) {
                // noop
            }
            return null;
        } );

        //

        theMeshBase.die();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest1.class );
}
