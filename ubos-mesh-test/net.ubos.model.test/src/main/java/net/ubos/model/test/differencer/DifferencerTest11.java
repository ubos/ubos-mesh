//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests adding a related MeshObject
 */
public class DifferencerTest11
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating MeshObjects in MeshBase1" );

        theMeshBase1.execute( (tx) -> {
            MeshObject a1 = theMeshBase1.createMeshObject( "aaaa1", TestSubjectArea.AA );

            return null;
        });

        //

        log.info( "Creating MeshObjects in MeshBase2" );

        theMeshBase2.execute( (tx) -> {
            MeshObject a1 = theMeshBase2.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject a5 = theMeshBase2.createMeshObject( "aaaa5", TestSubjectArea.AA ); // DIFFERENCE: create, bless

            a1.blessRole( TestSubjectArea.AR1A.getSource(), a5 ); // DIFFERENCE: new relationship: bless

            return null;
        });

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList changeList = diff.determineChangeList( theMeshBase2 );

        printChangeList( log, changeList );

        checkEquals( changeList.size(), 3, "Not the right number of changes" );
        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeList.applyChangeListTo( theMeshBase1 ));

        //

        ChangeList emptyChangeList = diff.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeList.getChanges().size(), 0, "ChangeList not empty after applying differences" );

        log.info( "Empty ChangeList is " + emptyChangeList );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( DifferencerTest11.class );
}
