//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 */
public abstract class AbstractSingleMeshBaseTest
    extends
        AbstractMeshBaseTest
{
    /**
     * Set up.
     */
    @BeforeEach
    public void setup()
    {
        theMeshBase = MMeshBase.Builder.create().build();
    }

    /**
     * Clean up after the test.
     */
    @AfterEach
    public void cleanup()
    {
        theMeshBase.die();
    }

    /**
     * The MeshBase for the test.
     */
    protected MeshBase theMeshBase;
}
