//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.history;

import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.history.MeshBaseStateHistory;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that deleted MeshObjects don't show up in the wrong places later:
 * To avoid holes in MeshObjectHistories, when a MeshObject is deleted it shows up there but as "getIsDead() == true".
 * It does not show up in MeshBaseStates where it does not exist.
 */
public class MeshBaseStateHistoryTest6
        extends
            AbstractMeshBaseStateHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long delta = 500; // half the time precision interval
        final MeshObjectIdentifier OBJ = theMeshBase.createMeshObjectIdentifier( "obj" );

        startClock();

        sleepUntil( delta );

        //

        log.info( "Creating and deleting" );

        long t1Before = System.currentTimeMillis();
        theMeshBase.execute( () -> theMeshBase.createMeshObject( OBJ ) );

        checkObject( theMeshBase.findMeshObjectByIdentifier( OBJ ), "OBJ not found at t1" );

        //

        sleepUntil( 2*delta );

        long t2Before = System.currentTimeMillis();
        theMeshBase.execute( () -> theMeshBase.deleteMeshObject( theMeshBase.findMeshObjectByIdentifier( OBJ )) );

        checkNotObject( theMeshBase.findMeshObjectByIdentifier( OBJ ), "OBJ found at t2" );

        //

        sleepUntil( 3*delta );

        long t3Before = System.currentTimeMillis();
        theMeshBase.execute( () -> theMeshBase.createMeshObject( OBJ ) );

        checkObject( theMeshBase.findMeshObjectByIdentifier( OBJ ), "OBJ not found at t3" );

        //

        MeshObjectHistory objHistory = theMeshBase.meshObjectHistory( OBJ );
        checkEquals( objHistory.getLength(), 3, "Wrong length of history" );

        checkNotObject(  objHistory.before(    t1Before ),             "Object found at t1Before" );
        checkCondition( !objHistory.atOrAfter( t1Before ).getIsDead(), "Dead object found at t1After" );

        checkCondition( !objHistory.before(    t2Before ).getIsDead(), "Dead object found at t2Before" );
        checkCondition(  objHistory.atOrAfter( t2Before ).getIsDead(), "Non-dead object found at t2After" );

        checkCondition(  objHistory.before(    t3Before ).getIsDead(), "Non-dead object found at t3Before" );
        checkCondition( !objHistory.atOrAfter( t3Before ).getIsDead(), "Dead object not found at t3After" );

        //

        MeshBaseStateHistory stateHistory = theMeshBase.getHistory();
        checkNotObject( stateHistory.before(    t1Before ).getMeshBaseView().findMeshObjectByIdentifier( OBJ ), "Object found in view at t1Before" );
        checkObject(    stateHistory.atOrAfter( t1Before ).getMeshBaseView().findMeshObjectByIdentifier( OBJ ), "Object not found in view at t1After" );

        checkObject(    stateHistory.before(     t2Before ).getMeshBaseView().findMeshObjectByIdentifier( OBJ ), "Object not found in view at t2Before" );
        checkNotObject( stateHistory.atOrAfter(  t2Before ).getMeshBaseView().findMeshObjectByIdentifier( OBJ ), "Object found in view at t2After" );

        checkNotObject( stateHistory.before(     t3Before ).getMeshBaseView().findMeshObjectByIdentifier( OBJ ), "Object found in view at t3Before" );
        checkObject(    stateHistory.atOrAfter(  t3Before ).getMeshBaseView().findMeshObjectByIdentifier( OBJ ), "Object not found in view at t3After" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseStateHistoryTest6.class );
}
