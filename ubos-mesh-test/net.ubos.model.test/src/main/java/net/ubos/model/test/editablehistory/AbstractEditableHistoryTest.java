//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RoleType;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.history.History;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality for EditableHistoryTests.
 */
public class AbstractEditableHistoryTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        theMeshBase = MMeshBase.Builder.create().history( true ).isHistoryEditable( true ).normalizeChangeList( true ).build();

        A = theMeshBase.createMeshObjectIdentifier( "a" );
        B = theMeshBase.createMeshObjectIdentifier( "b" );
        C = theMeshBase.createMeshObjectIdentifier( "c" );
        D = theMeshBase.createMeshObjectIdentifier( "d" );
    }

    /**
     * Clean up after the test.
     */
    @AfterEach
    public void cleanup()
    {
        if( theMeshBase != null ) {
            theMeshBase.die();
        }
    }

    /**
     * Helper to print time consistently.
     *
     * @param time the time to print
     * @param timesToNames assigns timestamps symbolic names for easier debugging
     * @return String to print
     */
    public String time(
            long             time,
            Map<Long,String> timesToNames )
    {
        String ret;
        if( timesToNames == null ) {
            ret = String.format( "%d (%+d)", time, time - theStartTime );
        } else {
            ret = String.format( "%s = %d (%+d)", timesToNames.get( time ), time, time - theStartTime );
        }
        return ret;
    }

    /**
     * Validates the consistency of the stock and the flow in the MeshBase history, i.e. that the
     * Changes in each MeshBaseState indeed lead to the next MeshBaseView.
     *
     * @param msg a message
     * @param timesToNames assigns timestamps symbolic names for easier debugging
     */
    protected void checkStockFlowIntegrity(
            String           msg,
            Map<Long,String> timesToNames )
    {
        MeshBase trackerMeshBase = new MMeshBase.Builder() {
            {
                theInitializeHomeObject = false;
            }
        }.history( false ).build();
        MeshBaseDifferencer differencer = MeshBaseDifferencer.create( trackerMeshBase );

        History<MeshBaseState> stateHistory = theMeshBase.getHistory();
        int count = 0; // for easier debugging
        for( MeshBaseState state : stateHistory ) {
            ChangeList changes = state.getChangeList();

            trackerMeshBase.execute( () -> {
                for( Change change : changes ) {
                    change.applyTo( trackerMeshBase );
                }
            } );

            ChangeList shouldBeEmpty = differencer.determineChangeList( state.getMeshBaseView() );

            checkEmpty(
                    shouldBeEmpty,
                    msg + " - checkStockFlowIntegrity: There's a difference at time: %s",
                    time( state.getTimeUpdated(), timesToNames ));
            ++count;
        }
    }

    /**
     * Print the history of this MeshBase, for debugging purposes.
     *
     * @param msg a prefix message
     * @param mb the MeshBase
     * @param timesToNames assigns timestamps symbolic names for easier debugging
     */
    protected void printHistory(
            String           msg,
            MeshBase         mb,
            Map<Long,String> timesToNames )
    {
        System.err.println( msg );
        System.err.println( getClass().getName() + " -- MeshBase history" );
        for( MeshBaseState mbs : mb.getHistory() ) {
            System.err.printf( "    MeshBaseState at %s (%d changes)\n", time( mbs.getTimeUpdated(), timesToNames ), mbs.getChangeList().size() );

            System.err.println( "        Changes:" );
            for( Change ch : mbs.getChangeList() ) {
                System.err.printf( "            %s\n", ch.toString() );
            }

            System.err.println( "        MeshObjects:" );
            for( MeshObject obj : mbs.getMeshBaseView() ) {
                printHistoryMbv( msg, obj, timesToNames );
            }
        }
        System.err.println( "    MeshBase HEAD" );
        System.err.println( "        MeshObjects:" );
        for( MeshObject obj : mb ) {
            printHistoryMbv( msg, obj, timesToNames );
        }
    }

    /**
     * Helper
     */
    private void printHistoryMbv(
            String           msg,
            MeshObject       obj,
            Map<Long,String> timesToNames )
    {
        System.err.printf( "            %-10s: created: %s, updated: %s\n", obj.getIdentifier().toString(), time( obj.getTimeCreated(), timesToNames ), time( obj.getTimeUpdated(), timesToNames ));
        if( obj.getEntityTypes().length > 0 ) {
            System.err.printf( "                EntityTypes:\n" );
            for( EntityType et : obj.getEntityTypes() ) {
                System.err.printf( "                    %s:\n", et.getIdentifier().toString() );
                for( PropertyType pt : et.getOrderedPropertyTypes() ) {
                    System.err.printf( "                        %s: %s\n", pt.getIdentifier().toString(), obj.getPropertyValue( pt ));
                }
            }
        }
        if( obj.getAttributeNames().length > 0 ) {
            System.err.println( "                Attributes:" );
            for( String att : obj.getAttributeNames() ) {
                System.err.printf( "                    %s: %s\n", att, obj.getAttributeValue( att ));
            }
        }
        if( !obj.traverseToNeighbors().isEmpty() ) {
            System.err.printf( "                Neighbors:\n" );
            for( MeshObject neighbor : obj.traverseToNeighbors() ) {
                System.err.printf( "                    %s:\n", neighbor.getIdentifier().toString() );
                if( obj.getOrderedRoleTypes( neighbor ).length > 0 ) {
                    System.err.printf( "                    RoleTypes:\n" );
                    for( RoleType rt : obj.getOrderedRoleTypes( neighbor )) {
                        System.err.printf( "                            %s:\n", rt.getIdentifier().toString() );
                        for( PropertyType pt : rt.getOrderedPropertyTypes() ) {
                            System.err.printf( "                                %s: %s\n", pt.getIdentifier().toString(), obj.getRolePropertyValue( neighbor, pt ) );
                        }
                    }
                }
                if( obj.getOrderedRoleAttributeNames( neighbor ).length > 0 ) {
                    System.err.printf( "                    RoleAttributes:\n" );
                    for( String att : obj.getOrderedRoleAttributeNames( neighbor )) {
                        System.err.printf( "                        %s: %s\n", att, obj.getRoleAttributeValue( neighbor, att ));
                    }
                }
            }
        }
    }

    /**
     * Helper function to check an up-to-4 element graph in a MeshBaseView.
     *
     * @param mbv the MeshBaseView
     * @param size the expected size of the MeshBaseView
     * @param attName name of the RoleAttribute to check
     * @param aNeighborIds expected identifiers of the neighbors of MeshObject A
     * @param aRoleAttributes expected value of RoleAttribute attName on the side of MeshObject A in the relationship
     *        with the MeshObject identified in the same position in the array in the previous parameter, or null if none
     * @param bNeighborIds same
     * @param bRoleAttributes same
     * @param cNeighborIds same
     * @param cRoleAttributes same
     * @param dNeighborIds same
     * @param dRoleAttributes same
     * @param msg error message
     * @return true if no error
     */
    protected boolean check4ObjGraphInMbv(
            MeshBaseView            mbv,
            int                     size,
            String                  attName,
            MeshObjectIdentifier [] aNeighborIds,
            String               [] aRoleAttributes,
            MeshObjectIdentifier [] bNeighborIds,
            String               [] bRoleAttributes,
            MeshObjectIdentifier [] cNeighborIds,
            String               [] cRoleAttributes,
            MeshObjectIdentifier [] dNeighborIds,
            String               [] dRoleAttributes,
            String                  msg )
    {
        if( size >= 0 ) {
            checkEquals( mbv.size(), size, msg + ": wrong size of MeshBaseView" );
        }

        boolean ret = true;
        if( aNeighborIds != null ) {
            ret &= check4ObjGraphInMbvOne( mbv, mbv.findMeshObjectByIdentifier( A ), attName, aNeighborIds, aRoleAttributes, msg );
        }
        if( bNeighborIds != null ) {
            ret &= check4ObjGraphInMbvOne( mbv, mbv.findMeshObjectByIdentifier( B ), attName, bNeighborIds, bRoleAttributes, msg );
        }
        if( cNeighborIds != null ) {
            ret &= check4ObjGraphInMbvOne( mbv, mbv.findMeshObjectByIdentifier( C ), attName, cNeighborIds, cRoleAttributes, msg );
        }
        if( dNeighborIds != null ) {
            ret &= check4ObjGraphInMbvOne( mbv, mbv.findMeshObjectByIdentifier( D ), attName, dNeighborIds, dRoleAttributes, msg );
        }
        return ret;
    }

    /**
     * Helper function.
     */
    private boolean check4ObjGraphInMbvOne(
            MeshBaseView            mbv,
            MeshObject              obj,
            String                  attName,
            MeshObjectIdentifier [] neighborIds,
            String               [] roleAttributes,
            String                  msg )
    {
        boolean ret = true;

        MeshObject [] neighbors = mbv.findMeshObjectsByIdentifier( neighborIds );
        ret &= checkEqualsOutOfSequence( obj.traverseToNeighbors().getMeshObjects(), neighbors, msg + ": %s has wrong neighbors", obj.getIdentifier() );

        for( int i = 0 ; i < neighbors.length ; ++i ) {
            if( roleAttributes[i] == null ) {
                checkCondition( !obj.hasRoleAttribute( neighbors[i], attName ), msg + ": has RoleAttribute %s -> %s", obj.getIdentifier(), neighbors[i].getIdentifier() );
            } else {
                checkCondition( obj.hasRoleAttribute( neighbors[i], attName ), msg + ": does not have RoleAttribute %s -> %s", obj.getIdentifier(), neighbors[i].getIdentifier() );
                checkEquals( obj.getRoleAttributeValue( neighbors[i], attName ), roleAttributes[i], msg + ": wrong RoleAttributeName %s -> %s", obj.getIdentifier(), neighbors[i].getIdentifier());
            }
        }
        return true;
    }

    /**
     * The MeshBase for the test.
     */
    protected MMeshBase theMeshBase;

    /**
     * Pre-defined identifiers.
     */
    protected MeshObjectIdentifier A;
    protected MeshObjectIdentifier B;
    protected MeshObjectIdentifier C;
    protected MeshObjectIdentifier D;
}
