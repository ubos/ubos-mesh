//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.history;

import java.util.HashSet;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.m.history.MHistoricMeshBaseView;
import net.ubos.meshbase.m.history.MHistoricMeshBaseViewRepository;

/**
 * Walks historical MeshBaseViews forward from the start to the present and counts
 * the number of MeshObjects in each. Makes sure they are all point to the right MeshBaseView.
 *
 * See StoreMeshBaseHistoryTest2.
 */
public class MeshBaseStateHistoryTest2
        extends
            AbstractMeshBaseStateHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        final long delta = 500;

        startClock();

        //

        log.info( "Running a few transactions" );

        sleepUntil( delta );

        theMeshBase.execute( () -> theMeshBase.createMeshObject( "obj-1" ));

        sleepUntil( delta*2 );

        theMeshBase.execute( () -> theMeshBase.createMeshObject( "obj-2" ));

        sleepUntil( delta*3 );

        theMeshBase.execute( () -> theMeshBase.createMeshObject( "obj-3" ));

        //
        log.info( "Creating MeshBaseView at the start" );

        HistoryCursorIterator<MeshBaseState> iter = theMeshBase.getHistory().iterator();
        iter.moveToBeforeFirst();
        MeshBaseState state1 = iter.next();

        HistoricMeshBaseView view1 = state1.getMeshBaseView();

        checkMeshBaseView( view1, 1, 0, "Beginning" );

        //

        log.info( "Moving forward" );

        MeshBaseState state2 = iter.next();

        HistoricMeshBaseView view2 = state2.getMeshBaseView();

        checkMeshBaseView( view2, 1, 1, "Step 2" );

        //

        log.info( "Moving forward again" );

        MeshBaseState state3 = iter.next();

        HistoricMeshBaseView view3 = state3.getMeshBaseView();

        checkMeshBaseView( view3, 1, 2, "Step 3" );
    }

    /**
     * Helper method to check a MeshBaseView.
     *
     * @param mbv the MeshBaseView to check
     * @param sizeChangedInView the number MeshObjects in the MeshBaseView that were actually changed here
     * @param sizeMerelyClonedFromPast the number of other MeshObjects in the MeshBaseView that were merely cloned from a previous MeshBaseView
     * @param msg error message if something goes wrong
     */
    protected void checkMeshBaseView(
            HistoricMeshBaseView mbv,
            int                  sizeChangedInView,
            int                  sizeMerelyClonedFromPast,
            String               msg )
    {
        MHistoricMeshBaseView      realMbv = (MHistoricMeshBaseView) mbv;
        MHistoricMeshBaseViewRepository cache   = realMbv.getMeshObjectStorage();

        HashSet<MeshObject> keepFromCleanup = new HashSet<>();

        for( MeshObject found : mbv ) {
            checkIdentity( found.getMeshBaseView(), mbv, msg + ": wrong MeshBaseView" );
            checkCondition( found.getTimeUpdated() <= mbv.getTimeUpdated(), msg + ": wrong timeUpdated" ); // may have been updated here, or before

            keepFromCleanup.add( found );
        }

        checkEquals( cache.changedMeshObjectsIterator().moveToAfterLast(), sizeChangedInView, msg + ": wrong number of primary objects" );
        checkEquals( cache.cloneMeshObjectsIterator().moveToAfterLast(), sizeMerelyClonedFromPast, msg + ": wrong number of cloned objects" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseStateHistoryTest2.class );
}
