//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.changelist;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that a single pair of Attribute changes is reduced
 * We create the ChangeList in a Transaction, because it's the easiest way of doing it
 */
public class ChangeListNormalizeAttributeTest1
        extends
            AbstractChangeListNormalizeTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final MeshObjectIdentifier A   = theMeshBase.createMeshObjectIdentifier( "a" );
        final String               ATT = "att";

        log.info( "Creating MeshObject(s)" );

        theMeshBase.execute( () -> {
            MeshObject a = theMeshBase.createMeshObject( A );
            a.setAttributeValue( ATT, 0 );
        });

        //

        log.info( "Create the ChangeList" );

        ChangeList list = theMeshBase.execute( (tx) -> {
            MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
            a.setAttributeValue( ATT, 1 );
            a.setAttributeValue( ATT, 2 );
            return tx.getChangeList().clone(); // get clone, the Transaction's ChangeList will be normalized on
        });
        checkEquals( list.size(), 2, "Wrong size before" );

        //

        log.info( "Normalizing" );

        list.normalize();

        checkEquals( list.size(), 1, "Wrong size after" );

        MeshObjectAttributeChange change = (MeshObjectAttributeChange) list.getChanges().get( 0 );
        checkEquals( change.getOldValue(), 0, "Wrong before value" );
        checkEquals( change.getNewValue(), 2, "Wrong after value" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(ChangeListNormalizeAttributeTest1.class);
}

