//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.IllegalRolePropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.modelbase.PropertyTypeNotFoundException;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that properties on RoleTypes go away with the RoleTypes
 */
public class MeshBaseTest20
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshObjectIdentifier oneId = theMeshBase.meshObjectIdentifierFromExternalForm( "one" );
        MeshObjectIdentifier twoId = theMeshBase.meshObjectIdentifierFromExternalForm( "two" );
        StringValue newValue =  StringValue.create( "NewFooSrcValue" );

        log.info( "Creating test objects" );

        theMeshBase.execute( ( Transaction tx ) -> {
            MeshObject one = theMeshBase.createMeshObject( oneId, TestSubjectArea.AA );
            MeshObject two = theMeshBase.createMeshObject( twoId, TestSubjectArea.AA );

            one.blessRole( TestSubjectArea.ARANY.getSource(), two );
            one.setRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO, newValue );
            return null;
        } );
        MeshObject one = theMeshBase.findMeshObjectByIdentifier( oneId );
        MeshObject two = theMeshBase.findMeshObjectByIdentifier( twoId );

        checkEquals( one.getRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO ), newValue, "wrong initial value" );
        checkEquals( two.getRolePropertyValue( one, TestSubjectArea.ARANY_D_FOO ), TestSubjectArea.ARANY_D_FOO.getDefaultValue(), "wrong initial value" );

        //

        log.debug( "Checking existence of Role PropertyTypes" );

        checkEquals( one.getRolePropertyTypes( two ).length, 1, "Wrong number of Role PropertyTypes in source" );
        checkEquals( two.getRolePropertyTypes( one ).length, 1, "Wrong number of Role PropertyTypes in destination" );

        //

        log.info( "Unblessing relationship" );

        theMeshBase.execute( ( Transaction tx ) -> {
            one.unblessRole(TestSubjectArea.ARANY_S, two );
            return null;
        } );

        //

        log.debug( "Checking that Role PropertyTypes have gone away" );

        checkEquals( one.getRolePropertyTypes( two ).length, 0, "Wrong number of Role PropertyTypes in source" );
        checkEquals( two.getRolePropertyTypes( one ).length, 0, "Wrong number of Role PropertyTypes in destination" );

        try {
            Object found = one.getRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO );
            reportError( "Unexpected value found (1): " + found );

        } catch( IllegalRolePropertyTypeException ex ) {
            // correct
        }

        try {
            Object found = one.getRolePropertyTypeByName( two, "Foo" );
            reportError( "Unexpected value found (2): " + found );

        } catch( PropertyTypeNotFoundException ex ) {
            // correct
        }

        try {
            Object found = two.getRolePropertyValue( one, TestSubjectArea.ARANY_D_FOO );
            reportError( "Unexpected value found (3): " + found );

        } catch( IllegalRolePropertyTypeException ex ) {
            // correct
        }

        try {
            Object found = two.getRolePropertyTypeByName( one, "Foo" );
            reportError( "Unexpected value found (4): " + found );

        } catch( PropertyTypeNotFoundException ex ) {
            // correct
        }

    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest20.class );
}