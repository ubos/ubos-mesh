//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests regular expressions on StringDataTypes.
 */
public class MeshBaseTest18
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshObject home = theMeshBase.getHomeObject();

        theMeshBase.execute( () -> home.bless( TestSubjectArea.OPTIONALSTRINGREGEX ) );

        //

        PropertyValue [] validValues = {
                StringValue.create( "0.0.0.0" ),
                StringValue.create( "1.2.3.4" ),
                StringValue.create( "111.222.222.111" ),
                StringValue.create( "127.0.0.1" ),
                StringValue.create( "255.255.255.255" ),
        };
        PropertyValue [] invalidValues = {
                StringValue.create( "some string" ),
                StringValue.create( "1.2.3" ),
                StringValue.create( "1.2.3.4." ),
        };

        theMeshBase.execute( () -> {
            for( int i=0 ; i<validValues.length ; ++i ) {
                try {
                    home.setPropertyValue( TestSubjectArea.OPTIONALSTRINGREGEX_OPTIONALSTRINGREGEXDATATYPE, validValues[i] );

                } catch( Throwable t ) {
                    reportError( "Valid value " + i + " threw exception", t );
                }
            }
            for( int i=0 ; i<invalidValues.length ; ++i ) {
                try {
                    home.setPropertyValue( TestSubjectArea.OPTIONALSTRINGREGEX_OPTIONALSTRINGREGEXDATATYPE, invalidValues[i] );
                    reportError( "Invalid value " + i + " did not throw exception" );

                } catch( Throwable t ) {
                    // that's right
                }
            }
        });
    }

    // Our Logger
    private static Log log = Log.getLogInstance( MeshBaseTest18.class );
}
