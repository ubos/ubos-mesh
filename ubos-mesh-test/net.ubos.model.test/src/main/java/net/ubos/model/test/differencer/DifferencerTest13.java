//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectRoleTypeRemoveChange;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests deleting two related MehObjects
 */
public class DifferencerTest13
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating MeshObjects in MeshBase1" );

        theMeshBase1.execute( (tx) -> {
            MeshObject a1 = theMeshBase1.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject a2 = theMeshBase1.createMeshObject( "aaaa2", TestSubjectArea.AA );
            a1.blessRole( TestSubjectArea.AR1A_S, a2) ;

            return null;
        });

        //

        log.info( "Leave MeshBase2 empty" );

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList changeList = diff.determineChangeList( theMeshBase2 );

        printChangeList( log, changeList );

        checkEquals( changeList.size(), 5, "Not the right number of changes" );

        int rptChanges = 0;
        for( Change change : changeList.getChanges() ) {
            if( change instanceof MeshObjectRoleTypeRemoveChange ) {
                ++rptChanges;
            }
        }
        checkEquals( rptChanges, 1, "Wrong number of MeshObjectRoleTypeRemoveEvent" );

        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeList.applyChangeListTo( theMeshBase1 ) );

        //

        ChangeList emptyChangeList = diff.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeList.getChanges().size(), 0, "ChangeList not empty after applying differences" );

        log.info( "Empty ChangeList is " + emptyChangeList );

        checkEquals( theMeshBase1.size(), 1, "Wrong number of objects left" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( DifferencerTest13.class );
}
