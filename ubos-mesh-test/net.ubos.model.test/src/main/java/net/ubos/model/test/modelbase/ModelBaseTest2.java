//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.modelbase;

import java.util.Iterator;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.modelbase.ModelBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that overridden PropertyTypes in multiple inheritance
 * cases only show up once in subtypes.
 */
public class ModelBaseTest2
        extends
            AbstractModelBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Loading Subject Areas" );

        populateModelBase();

        //

        log.info( "Iterating over content, and checking that no MeshTypeWithProperties has more than one PropertyType with the same ancestor" );

        Iterator<MeshType> theIter = ModelBase.SINGLETON.iterator();
        while( theIter.hasNext() ) {
            MeshType current = theIter.next();

            if( !( current instanceof MeshTypeWithProperties )) {
                continue;
            }

            MeshTypeWithProperties realCurrent = (MeshTypeWithProperties) current;

            log.info(
                    "looking at "
                    + ( realCurrent instanceof EntityType ? "EntityType" : "RoleType" )
                    + ": "
                    + realCurrent.getIdentifier() );

            PropertyType [] allPts = realCurrent.getPropertyTypes();
            for( int i=0 ; i<allPts.length ; ++i ) {
                PropertyType iPt         = allPts[i];
                PropertyType ancestorIPt = iPt.getOverrideAncestor();

                for( int j=0 ; j<i ; ++j ) {
                    PropertyType jPt = allPts[j];

                    if( iPt.getIdentifier().equals( jPt.getIdentifier() )) {
                        reportError( "found the same PropertyType twice", iPt, jPt );
                    }
                    PropertyType ancestorJPt = jPt.getOverrideAncestor();

                    if( ancestorIPt.getIdentifier().equals( ancestorJPt.getIdentifier() )) {
                        reportError( "two PropertyTypes have same ancestor", iPt, jPt, ancestorIPt );
                    }
                }
            }
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ModelBaseTest2.class);
}
