//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import java.io.File;
import java.util.ArrayList;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.externalized.xml.DefaultMeshBaseXmlImporter;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectCreateChange;
import net.ubos.meshbase.transaction.MeshObjectPropertyChange;
import net.ubos.meshbase.transaction.MeshObjectRoleTypeAddChange;
import net.ubos.meshbase.transaction.MeshObjectBlessChange;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Reads a .mesh file, and compares against empty MeshBase, serializes the ChangeList in memory,
 * and then applies it to a new MeshBase.
 */
public class DifferencerTest12
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        File dataFile = new File( "/ubos/share/ubos-mesh-test/testfiles/DifferencerTest12-in.mesh" );

        log.info( "Importing the file" );

        DefaultMeshBaseXmlImporter importer = DefaultMeshBaseXmlImporter.create();
        importer.importTo( dataFile, null, theMeshBase1 );

        //

        log.info( "Check some data has arrived" );

        checkEquals( theMeshBase1.size(), 3, "Wrong number of MeshObjects" );
        checkObject( theMeshBase1.findMeshObjectByIdentifier( "some/obj1"),   "Not found" );
        checkObject( theMeshBase1.findMeshObjectByIdentifier( "some/other1"), "Not found" );

        //

        log.info( "Run differencer against empty MeshBase as the baseline" );

        MeshBaseDifferencer diff2        = MeshBaseDifferencer.create( theMeshBase2 );
        ChangeList          changeList21 = diff2.determineChangeList( theMeshBase1 );
        Change []           changes21    = ArrayHelper.copyIntoNewArray( changeList21.getChanges(), Change.class );

        checkEquals( changeList21.size(), 5, "Wrong number of changes" );
        checkEquals(ArrayHelper.select(changes21, (x) -> x instanceof MeshObjectCreateChange, Object.class ).length,
                     2,
                     "Wrong number of MeshObjectCreateEvent" );
        checkEquals(ArrayHelper.select(changes21, (x) -> x instanceof MeshObjectPropertyChange, Object.class ).length,
                     1,
                     "Wrong number of MeshObjectPropertyChangeEvent" );
        checkEquals(ArrayHelper.select(changes21, (x) -> x instanceof MeshObjectBlessChange, Object.class ).length,
                     1,
                     "Wrong number of MeshObjectTypeAddEvent" );
        checkEquals(ArrayHelper.select(changes21, (x) -> x instanceof MeshObjectRoleTypeAddChange, Object.class ).length,
                     1,
                     "Wrong number of MeshObjectRoleTypeAddEvent" );

        //

        log.info( "Externalizing changes" );

        ArrayList<ExternalizedChange> extChanges = new ArrayList<>();
        for( Change change : changeList21 ) {
            ExternalizedChange extChange = change.asExternalized();
            extChanges.add( extChange );
        }

        //

        log.info( "Applying on new MeshBase" );

        MMeshBase theMeshBase3 = MMeshBase.Builder.create().namespaceMap( thePrimaryNsMap ).build();
        theMeshBase3.execute( () -> {
            for( ExternalizedChange extChange : extChanges ) {
                extChange.applyTo( theMeshBase3 );
            }
        } );

        checkEquals( theMeshBase3.size(), theMeshBase1.size(), "Wrong number of objects" );

        //

        log.info( "Diff'ing original and restored from changes" );

        MeshBaseDifferencer diff1        = MeshBaseDifferencer.create( theMeshBase1 );
        ChangeList          changeList13 = diff1.determineChangeList( theMeshBase3 );

        checkEquals( changeList13.size(), 0, "Has changes" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( DifferencerTest12.class );
}
