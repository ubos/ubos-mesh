//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests whether rollbacks work.
 */
public class RollbackTest3
        extends
            AbstractMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshBase theMeshBase = MMeshBase.Builder.create().build();

        final MeshObjectIdentifier fixed1Id   = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed1" );
        final MeshObjectIdentifier fixed2Id   = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed2" );
        final FloatValue           rightValue = FloatValue.create( 111.11 );

        //

        log.debug( "Creating MeshObjectGraph that won't be rolled back" );

        theMeshBase.execute( () -> {
                MeshObject fixed1 = theMeshBase.createMeshObject( fixed1Id, TestSubjectArea.AA );
                fixed1.setPropertyValue( TestSubjectArea.AA_Y, rightValue );

                MeshObject fixed2 = theMeshBase.createMeshObject( fixed2Id, TestSubjectArea.AA );
                fixed2.setPropertyValue( TestSubjectArea.AA_Y, rightValue );
        });

        checkEquals( theMeshBase.size(), 3, "Wrong initial number of MeshObjects" );

        final MeshObject fixed1 = theMeshBase.findMeshObjectByIdentifier( fixed1Id );
        final MeshObject fixed2 = theMeshBase.findMeshObjectByIdentifier( fixed2Id );

        //

        log.debug( "Creating failing Transaction that will automatically be rolled back." );

        theMeshBase.execute( () -> {
            fixed1.unbless( TestSubjectArea.AA );
            theMeshBase.deleteMeshObject( fixed2 );

            throw new TransactionActionException.Rollback();
        });

        //

        final MeshObject fixed2new = theMeshBase.findMeshObjectByIdentifier( fixed2Id );

        checkEquals( theMeshBase.size(), 3, "Wrong number of MeshObjects" );
        checkEquals( fixed1.getPropertyValue( TestSubjectArea.AA_Y ), rightValue, "PropertyValue not rolled back right");
        checkEquals( fixed2new.getPropertyValue( TestSubjectArea.AA_Y ), rightValue, "PropertyValue not rolled back right");
    }

    // Our Logger
    private static Log log = Log.getLogInstance( RollbackTest3.class );
}
