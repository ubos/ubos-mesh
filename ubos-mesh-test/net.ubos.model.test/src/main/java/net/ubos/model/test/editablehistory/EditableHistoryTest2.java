//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//
package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that empty HistoryTransactions don't remain.
 */
public class EditableHistoryTest2
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT_NAME = "att";
        final long   delta    = 200L;

        //

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        Map<Long,String> captureTs = new HashMap<>();

        startClock();

        // printHistory( "Start", theMeshBase );

        MeshObject a = theMeshBase.execute(
                (Transaction tx ) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT_NAME, 1 );
                    return ret;
                });
        captureTs.put( a.getTimeUpdated(), "t0" );
        checkStockFlowIntegrity( "t0", captureTs );

        //

        sleepUntil( delta * 2 );

        theMeshBase.execute( () -> a.setAttributeValue( ATT_NAME, 2 ) );
        captureTs.put( a.getTimeUpdated(), "t2" );
        checkStockFlowIntegrity( "t2", captureTs );

        // printHistory( "t2", theMeshBase );

        checkEquals( theMeshBase.getHistory().getLength(), 3, "Wrong length of history" );

        //

        theMeshBase.executeAt( delta, () -> {} ); // do nothing
        checkStockFlowIntegrity( "after t2", null );

        // printHistory( "after t2", theMeshBase );

        checkEquals( theMeshBase.getHistory().getLength(), 3, "Wrong length of history" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( EditableHistoryTest2.class );
}
