//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.changelist;

import java.util.Arrays;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that a several pairs of Attribute changes on several MeshObjects are reduced.
 * We create the ChangeList in a Transaction, because it's the easiest way of doing it
 */
public class ChangeListNormalizeAttributeTest3
        extends
            AbstractChangeListNormalizeTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final MeshObjectIdentifier A    = theMeshBase.createMeshObjectIdentifier( "a" );
        final MeshObjectIdentifier B    = theMeshBase.createMeshObjectIdentifier( "b" );
        final String               ATT1 = "att-1";
        final String               ATT2 = "att-2";

        log.info( "Creating MeshObject(s)" );

        theMeshBase.execute( () -> {
            MeshObject a = theMeshBase.createMeshObject( A );
            a.setAttributeValue( ATT1, 0 );
            a.setAttributeValue( ATT2, "alpha" );

            MeshObject b = theMeshBase.createMeshObject( B );
            b.setAttributeValue( ATT1, "alpha" );
            b.setAttributeValue( ATT2, 0 );
        });

        //

        log.info( "Create the ChangeList" );

        ChangeList list = theMeshBase.execute( (tx) -> {
            MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
            MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );

            a.setAttributeValue( ATT1, 1 );
            b.setAttributeValue( ATT2, 1 );
            a.setAttributeValue( ATT2, "beta" );
            b.setAttributeValue( ATT1, "beta" );
            a.setAttributeValue( ATT1, 2 );
            b.setAttributeValue( ATT2, 2 );
            a.setAttributeValue( ATT2, "gamma" );
            b.setAttributeValue( ATT1, "gamma" );
            a.setAttributeValue( ATT2, "delta" );
            b.setAttributeValue( ATT1, "delta" );
            a.setAttributeValue( ATT1, 3 );
            b.setAttributeValue( ATT2, 3 );

            return tx.getChangeList().clone(); // get clone, the Transaction's ChangeList will be normalized on commit
        });
        checkEquals( list.size(), 12, "Wrong size before" );

        //

        log.info( "Normalizing" );

        list.normalize();

        checkEquals( list.size(), 4, "Wrong size after" );
        MeshObjectAttributeChange [] changes = new MeshObjectAttributeChange[ list.size() ];
        list.getChanges().toArray( changes );

        Arrays.sort( changes, (MeshObjectAttributeChange x, MeshObjectAttributeChange y) -> {
            int ret = x.getSource().getIdentifier().compareTo( y.getSource().getIdentifier() );
            if( ret == 0 ) {
                ret = x.getProperty().compareTo( y.getProperty() );
            }
            return ret;
        });

        MeshObjectAttributeChange changeA1 = changes[0];
        MeshObjectAttributeChange changeA2 = changes[1];
        MeshObjectAttributeChange changeB1 = changes[2];
        MeshObjectAttributeChange changeB2 = changes[3];

        checkEquals( changeA1.getOldValue(), 0, "Wrong before value a" );
        checkEquals( changeA1.getNewValue(), 3, "Wrong after value a" );
        checkEquals( changeA2.getOldValue(), "alpha", "Wrong before value a" );
        checkEquals( changeA2.getNewValue(), "delta", "Wrong after value a" );
        checkEquals( changeB1.getOldValue(), "alpha", "Wrong before value b" );
        checkEquals( changeB1.getNewValue(), "delta", "Wrong after value b" );
        checkEquals( changeB2.getOldValue(), 0, "Wrong before value b" );
        checkEquals( changeB2.getNewValue(), 3, "Wrong after value b" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ChangeListNormalizeAttributeTest3.class);
}

