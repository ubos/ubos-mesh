There are a bunch of use cases.

1. MeshObject a does not exist at any point in time t >= t1 (may exist before t1)

   a. MeshObject a is created at t1.

      do: create at t1
          create MeshObjectCreated event at t1
          create a(HEAD) as copy of a(t1)

2. MeshObject a exists at point t1, and there are no further changes through HEAD

   a. MeshObject a is updated at t2 > t1

      do: create at t2
          diff a(t2), a(t1) and create relevant events at t2 from diff
          update a(HEAD) without generating any events at HEAD

3. MeshObject a exists at point t1. There may be other changes after t1.

   a. MeshObject a is created at t0 < t1

      do: create at t0
          diff a(t1), a(t0). Remove all a-related events at t1, and create relevant events at t1 from diff

4. MeshObject a exists at point t1, and was changed next at t3

   a. MeshObject a is updated at t2 with t1 < t2 < t3

      do: create at t2
          diff a(t2), a(t1) and create relevant events at t2 from diff
          diff a(t3), a(t2). Remove all a-related events at t3, and create relevant events at t3 from diff

5. MeshObject a exists at point t1, and there are no further changes through HEAD

   a. MeshObject a is deleted at t2 > t1

      do: delete at t2 (must be deleted-marker in the MeshBaseView, otherwise it will be transparently inserted)
          create delete events at t2
          remove from HEAD

6. MeshObject a exists at point t1 and there are changes at point t3

   a. MeshObject a is deleted at t2 with t1 < t2 < t3.

      do: delete at t2 (must be deleted-marker in the MeshBaseView, otherwise it will be transparently inserted)
          create delete events at t2
          create at t3
          add create and related events at t3


