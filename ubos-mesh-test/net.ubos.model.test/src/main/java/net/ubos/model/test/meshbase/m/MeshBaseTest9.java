//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that assigning an invalid PropertyValue during a Transaction rolls back the entire Transaction.
 */
public class MeshBaseTest9
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        MeshObjectIdentifier A = theMeshBase.createMeshObjectIdentifier( "a" );
        MeshObjectIdentifier B = theMeshBase.createMeshObjectIdentifier( "b" );

        MeshObject a = theMeshBase.execute( (tx) -> {
            return theMeshBase.createMeshObject( A, TestSubjectArea.MANDATORYSTRING );
        });

        checkEquals( theMeshBase.size(), 2, "Wrong size" );
        checkObject( theMeshBase.findMeshObjectByIdentifier( A ), "No a" );
        checkCondition( a.isBlessedBy( TestSubjectArea.MANDATORYSTRING ), "No a" );
        checkEquals( a.getEntityTypes().length, 1, "Too many types" );
        checkNotObject( theMeshBase.findMeshObjectByIdentifier( B ), "Has b" );

        //

        try {
            theMeshBase.execute( () -> {
                MeshObject b = theMeshBase.createMeshObject( B, TestSubjectArea.AA );
                a.bless( TestSubjectArea.AA );
                a.blessRole( TestSubjectArea.ARANY_S, b );
                a.setPropertyValue( TestSubjectArea.MANDATORYSTRING_MANDATORYSTRINGDATATYPE, null ); // throws
            });
            reportError( "Not thrown" );
        } catch( Throwable t ) {}

        //

        checkEquals( theMeshBase.size(), 2, "Wrong size" );
        checkObject( theMeshBase.findMeshObjectByIdentifier( A ), "No a" );
        checkCondition( a.isBlessedBy( TestSubjectArea.MANDATORYSTRING ), "No a" );
        checkEquals( a.getEntityTypes().length, 1, "Too many types" );
        checkNotObject( theMeshBase.findMeshObjectByIdentifier( B ), "Has b" );

    }

    private static final Log log = Log.getLogInstance( MeshBaseTest9.class );
}
