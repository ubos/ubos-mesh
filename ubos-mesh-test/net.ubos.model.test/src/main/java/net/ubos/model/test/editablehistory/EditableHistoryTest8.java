//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Same as the previous EditableHistoryTests, just this time with EntityTypes/PropertyTypes instead of Attributes.
 */
public class EditableHistoryTest8
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long   delta = 200L;

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        log.info( "Setting up t0, t2, t3" );

        long startTime = startClock();

        theMeshBase.execute(
                () -> {
                    MeshObject a = theMeshBase.createMeshObject( A );
                    a.bless( TestSubjectArea.AA );
                    a.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 0.0d ));
                });

        sleepUntil( 2*delta );

        theMeshBase.execute(
                () -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    a.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 2.0d ));
                });

        sleepUntil( 3*delta );

        theMeshBase.execute(
                () -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    a.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 3.0d ));
                });

        //

        log.info( "Patching in t1" );

        theMeshBase.executeAt(
                startTime + delta,
                () -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    a.bless( TestSubjectArea.B );
                    a.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 1.0d ));
                    a.setPropertyValue( TestSubjectArea.B_U, StringValue.create( "t1" ));
                });

        //

        MeshObject        aHead    = theMeshBase.findMeshObjectByIdentifier( A );
        MeshObjectHistory aHistory = theMeshBase.meshObjectHistory( A );
        checkEquals( aHistory.getLength(), 4, "Wrong length of history" );

        HistoryCursorIterator<MeshObject> aHistoryIter = aHistory.iterator();
        aHistoryIter.moveToBeforeFirst();
        MeshObject at0 = aHistoryIter.next();
        MeshObject at1 = aHistoryIter.next();
        MeshObject at2 = aHistoryIter.next();
        MeshObject at3 = aHistoryIter.next();

        checkEquals( at0.getTimeCreated(), at0.getTimeUpdated(), "Not created and updated at the same time t0" );

        checkCondition( at0.getTimeUpdated() < at1.getTimeUpdated(), "Wrong sequence: at0 vs at1" );
        checkCondition( at1.getTimeUpdated() < at2.getTimeUpdated(), "Wrong sequence: at1 vs at2" );
        checkCondition( at2.getTimeUpdated() < at3.getTimeUpdated(), "Wrong sequence: at2 vs at3" );

        checkCondition( at0.isBlessedBy(   TestSubjectArea.AA ), "Not blessed with AA at t0" );
        checkCondition( at1.isBlessedBy(   TestSubjectArea.AA ), "Not blessed with AA at t1" );
        checkCondition( at2.isBlessedBy(   TestSubjectArea.AA ), "Not blessed with AA at t2" );
        checkCondition( at3.isBlessedBy(   TestSubjectArea.AA ), "Not blessed with AA at t3" );
        checkCondition( aHead.isBlessedBy( TestSubjectArea.AA ), "Not blessed with AA at HEAD" );

        checkEquals( at0.getPropertyValue(   TestSubjectArea.AA_Y ), FloatValue.create( 0.0 ), "Wrong AA_Y at t0" );
        checkEquals( at1.getPropertyValue(   TestSubjectArea.AA_Y ), FloatValue.create( 1.0 ), "Wrong AA_Y at t1" );
        checkEquals( at2.getPropertyValue(   TestSubjectArea.AA_Y ), FloatValue.create( 2.0 ), "Wrong AA_Y at t2" );
        checkEquals( at3.getPropertyValue(   TestSubjectArea.AA_Y ), FloatValue.create( 3.0 ), "Wrong AA_Y at t3" );
        checkEquals( aHead.getPropertyValue( TestSubjectArea.AA_Y ), FloatValue.create( 3.0 ), "Wrong AA_Y at HEAD" );

        checkCondition( !at0.isBlessedBy(  TestSubjectArea.B ), "Bessed with B at t0" );
        checkCondition( at1.isBlessedBy(   TestSubjectArea.B ), "Bessed with B at t1" );
        checkCondition( at2.isBlessedBy(   TestSubjectArea.B ), "Bessed with B at t2" );
        checkCondition( at3.isBlessedBy(   TestSubjectArea.B ), "Bessed with B at t3" );
        checkCondition( aHead.isBlessedBy( TestSubjectArea.B ), "Bessed with B at HEAD" );

        checkEquals( at1.getPropertyValue(   TestSubjectArea.B_U ), StringValue.create( "t1" ), "Wrong B_U at t1" );
        checkEquals( at2.getPropertyValue(   TestSubjectArea.B_U ), StringValue.create( "t1" ), "Wrong B_U at t2" );
        checkEquals( at3.getPropertyValue(   TestSubjectArea.B_U ), StringValue.create( "t1" ), "Wrong B_U at t3" );
        checkEquals( aHead.getPropertyValue( TestSubjectArea.B_U ), StringValue.create( "t1" ), "Wrong B_U at HEAD" );

        checkStockFlowIntegrity( "end", null );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest8.class );
}
