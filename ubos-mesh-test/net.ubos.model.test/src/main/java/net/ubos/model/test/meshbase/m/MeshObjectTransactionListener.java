//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import java.util.ArrayList;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.TransactionListener;

/**
 * Listener class.
 */
public class MeshObjectTransactionListener
    implements
        TransactionListener
{
    /**
     * Constructor.
     *
     * @param obj only record events affecting this MeshObject
     */
    public MeshObjectTransactionListener(
            MeshObject obj )
    {
        theIdentifier = obj.getIdentifier();
    }

    /**
     * Constructor.
     *
     * @param identifier only record events affecting the MeshObject with this identifier
     */
    public MeshObjectTransactionListener(
            MeshObjectIdentifier identifier )
    {
        theIdentifier = identifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void transactionCommitted(
            HeadTransaction tx )
    {
        for( Change ch : tx.getChangeList() ) {
            if( ch.affects( theIdentifier )) {
                theEvents.add(ch);
            }
        }
    }

    /**
     * Reset the recording.
     */
    public void clear()
    {
        theEvents.clear();
    }

    /**
     * Convert to String, for debugging.
     *
     * @return String representation
     */
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("MyListener:");
        for (Change ch : theEvents) {
            buf.append("\n");
            buf.append(ch);
        }
        return buf.toString();
    }

    /**
     * Only listen to events affecting the MeshObject with this identifier
     */
    protected MeshObjectIdentifier theIdentifier;

    /**
     * The received events.
     */
    protected ArrayList<Change> theEvents = new ArrayList<>();
}
