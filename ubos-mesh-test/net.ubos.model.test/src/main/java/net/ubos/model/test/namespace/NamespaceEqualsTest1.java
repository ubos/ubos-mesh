//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.namespace;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;

/**
 * Tests that MeshObjects whose MeshObjectIdentifiers have been allocated in different
 * MeshObjectIdentifierNamespaces correct evaluate equals and hashcode.
 */
public class NamespaceEqualsTest1
    extends
        AbstractMeshObjectIdentifierNamespaceTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshObjectIdentifierNamespaceMap and MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap map = MPrimaryMeshObjectIdentifierNamespaceMap.create();

        MeshBase theMeshBase = MMeshBase.Builder.create().namespaceMap( map ).build();

        checkEquals( countRemaining( map.localNameIterator() ), 1,  "Too many NSs at the start" );

        //

        log.info( "Creating MeshObjects" );

        String [] namespaceNames = {
            null,
            "abc",
            "def"
        };

        String [] meshObjectIds = {
            "aaaa",
            "bbbb",
            "cccc"
        };

        MeshObjectIdentifier [] trackedIds = new MeshObjectIdentifier[ namespaceNames.length * meshObjectIds.length ];
        theMeshBase.execute( () -> {
            int count = 0;
            for( String namespaceName : namespaceNames ) {
                for( String meshObjectId : meshObjectIds ) {
                    MeshObjectIdentifier createdId = theMeshBase.meshObjectIdentifierFromExternalForm(
                            namespaceName == null
                            ? meshObjectId
                            : namespaceName + "#" + meshObjectId );
                    trackedIds[count++] = createdId;
                    MeshObject created = theMeshBase.createMeshObject( createdId );
                }
            }
        });

        checkEquals( countRemaining( map.localNameIterator()), namespaceNames.length,  "Wrong number NSs" );

        //

        log.info( "Checking MeshObjectIdentifiers do not equal" );

        int [] trackedIdHashes = new int[ trackedIds.length ];
        for( int i=0 ; i<trackedIdHashes.length ; ++i ) {
            trackedIdHashes[i] = trackedIds[i].hashCode();
        }

        for( int i=0 ; i<trackedIds.length ; ++i ) {
            for( int j=i+1 ; j<trackedIds.length ; ++j ) {
                checkNotEquals( trackedIds[i], trackedIds[j],        "Equal identifiers: " + i + " vs " + j );
                checkNotEquals( trackedIdHashes, trackedIdHashes[j], "Equal hash codes: "  + i + " vs " + j );
            }
        }

        //

        log.info(  "Changing the external names and checking hashes are still the same" );

        for( String localName : map.localNameIterator() ) {
            MeshObjectIdentifierNamespace ns = map.findByLocalName( localName );

            String current = ns.getPreferredExternalName();
            if( current == null ) {
                String modified = "null-mod";
                ns.addExternalName( modified );
                ns.setPreferredExternalName( modified );

            } else {
                String modified = current + "-mod";
                ns.addExternalName( modified );
                ns.setPreferredExternalName( modified );
                ns.removeExternalName( current );
            }
        }

        for( int i=0 ; i<trackedIds.length ; ++i ) {
            checkEquals( trackedIds[i].hashCode(), trackedIdHashes[i], "Hash has changed at index: " + i );
        }

        //

        log.info( "Done" );

        theMeshBase.die();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( NamespaceEqualsTest1.class );
}
