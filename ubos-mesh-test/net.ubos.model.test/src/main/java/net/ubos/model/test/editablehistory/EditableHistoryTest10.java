//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Let's test untyped relationships -- simplest.
 */
public class EditableHistoryTest10
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT   = "att";
        final long   delta = 200L;

        Map<Long,String> captureTs = new HashMap<>();
        captureTs.put( theMeshBase.getHomeObject().getTimeUpdated(), "t_home" );

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        log.info( "Setting up t0, t2" );

        long startTime = startClock();

        long t0 = theMeshBase.execute(
                (tx) -> {
                    MeshObject a = theMeshBase.createMeshObject( A );
                    MeshObject b = theMeshBase.createMeshObject( B );
                    MeshObject c = theMeshBase.createMeshObject( C );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t0, "t0" );

        sleepUntil( 2*delta );

        long t2 = theMeshBase.execute(
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );
                    a.setRoleAttributeValue( b, ATT, "t2" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t2, "t2" );

        //

        log.info( "Patching in t1" );

        long t1 = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject c = theMeshBase.findMeshObjectByIdentifier( C );
                    a.setRoleAttributeValue( c, ATT, "t1" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t1, "t1" );

        //

        log.info( "Checking stock flow integrity" );
        checkStockFlowIntegrity( "end", captureTs );

        //

        log.info( "Checking history" );

        checkEquals( theMeshBase.getHistory().getLength(), 4, "Wrong length of MeshBase history" );

        MeshObjectHistory aHistory = theMeshBase.meshObjectHistory( A );
        MeshObjectHistory bHistory = theMeshBase.meshObjectHistory( B );
        MeshObjectHistory cHistory = theMeshBase.meshObjectHistory( C );

        checkEquals( aHistory.getLength(), 3, "Wrong length of a history" );
        checkEquals( bHistory.getLength(), 2, "Wrong length of b history" );
        checkEquals( cHistory.getLength(), 2, "Wrong length of c history" );

        //

        log.info( "Checking each state in the history" );

        HistoryCursorIterator<MeshBaseState> mbHistoryIter = theMeshBase.getHistory().iterator();
        mbHistoryIter.moveToBeforeFirst();
        mbHistoryIter.next(); // home
        HistoricMeshBaseView mbvAt0 = mbHistoryIter.next().getMeshBaseView(); // t0
        HistoricMeshBaseView mbvAt1 = mbHistoryIter.next().getMeshBaseView(); // t1
        HistoricMeshBaseView mbvAt2 = mbHistoryIter.next().getMeshBaseView(); // t2

        check4ObjGraphInMbv( mbvAt0,
                  4,
                  ATT,
                  new MeshObjectIdentifier[0],
                  new String[0],
                  new MeshObjectIdentifier[0],
                  new String[0],
                  new MeshObjectIdentifier[0],
                  new String[0],
                  null,
                  null,
                  "t0" );

        check4ObjGraphInMbv( mbvAt1,
                  4,
                  ATT,
                  new MeshObjectIdentifier[] { C },
                  new String[] { "t1" },
                  null,
                  null,
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  "t1" );

        check4ObjGraphInMbv( mbvAt2,
                  4,
                  ATT,
                  new MeshObjectIdentifier[] { B, C },
                  new String[] { "t2", "t1" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  "t2" );

        log.info( "Checking head" );

        check4ObjGraphInMbv( theMeshBase.getHeadMeshBaseView(),
                  4,
                  ATT,
                  new MeshObjectIdentifier[] { B, C },
                  new String[] { "t2", "t1" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  "HEAD" );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest10.class );
}
