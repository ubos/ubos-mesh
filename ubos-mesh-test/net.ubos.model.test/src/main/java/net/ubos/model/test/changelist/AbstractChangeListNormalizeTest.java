//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.changelist;

import net.ubos.meshbase.m.MMeshBase;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out functionality common to ChangeListNormalizeTests.
 */
public class AbstractChangeListNormalizeTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        theMeshBase = MMeshBase.Builder.create().build();
    }

    /**
     * Clean up after the test.
     */
    @AfterEach
    public void cleanup()
    {
        if( theMeshBase != null ) {
            theMeshBase.die();
        }
    }

    /**
     * The MeshBase for the test.
     */
    protected MMeshBase theMeshBase;
}
