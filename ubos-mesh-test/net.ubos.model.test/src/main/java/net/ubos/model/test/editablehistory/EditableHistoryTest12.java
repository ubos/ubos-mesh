//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that multiple changes can be inserted into the past that cancel each other out.
 */
public class EditableHistoryTest12
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT   = "att";
        final long   delta = 200L;

        Map<Long,String> captureTs = new HashMap<>();
        captureTs.put( theMeshBase.getHomeObject().getTimeUpdated(), "t_home" );

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        log.info( "Setting up t0, t3" );

        long startTime = startClock();

        long t0 = theMeshBase.execute(
                (tx) -> {
                    MeshObject a = theMeshBase.createMeshObject( A );
                    MeshObject b = theMeshBase.createMeshObject( B );

                    a.setRoleAttributeValue( b, ATT, "t0" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t0, "t0" );

        sleepUntil( 3*delta );

        long t3 = theMeshBase.execute(
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );

                    a.setRoleAttributeValue( b, ATT, "t3" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t3, "t2" );

        // printHistory( "Before t1", theMeshBase, captureTs );

        check4ObjGraphInMbv( theMeshBase.getHistory().at( t0 ).getMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t0" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "t0" );

        check4ObjGraphInMbv( theMeshBase.getHistory().at( t3 ).getMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "t3" );

        check4ObjGraphInMbv( theMeshBase.getHeadMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "HEAD" );
        //

        log.info( "Patching in t1" );

        long t1 = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );

                    b.setRoleAttributeValue( a, ATT, "t1" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t1, "t1" );

        check4ObjGraphInMbv( theMeshBase.getHistory().at( t0 ).getMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t0" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "t0" );

        check4ObjGraphInMbv( theMeshBase.getHistory().at( t1 ).getMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t0" },
                  new MeshObjectIdentifier[] { A },
                  new String[] { "t1" },
                  null,
                  null,
                  null,
                  null,
                  "t1" );

        check4ObjGraphInMbv( theMeshBase.getHeadMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[] { "t1" }, // !!
                  null,
                  null,
                  null,
                  null,
                  "HEAD" );

        //

        log.info( "Patching in t2" );

        long t2 = theMeshBase.executeAt(
                startTime + 2*delta,
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );

                    b.deleteRoleAttribute( a, ATT );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t2, "t2" );

        //

        log.info( "Checking stock flow integrity" );
        checkStockFlowIntegrity( "end", captureTs );

        //

        log.info( "Checking history" );

        checkEquals( theMeshBase.getHistory().getLength(), 5, "Wrong length of MeshBase history" );

        MeshObjectHistory aHistory = theMeshBase.meshObjectHistory( A );
        MeshObjectHistory bHistory = theMeshBase.meshObjectHistory( B );

        checkEquals( aHistory.getLength(), 4, "Wrong length of a history" );
        checkEquals( bHistory.getLength(), 4, "Wrong length of b history" );

        //

        log.info( "Checking each state in the history" );

        HistoryCursorIterator<MeshBaseState> mbHistoryIter = theMeshBase.getHistory().iterator();
        mbHistoryIter.moveToBeforeFirst();
        mbHistoryIter.next(); // home
        HistoricMeshBaseView mbvAt0 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt1 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt2 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt3 = mbHistoryIter.next().getMeshBaseView();

        check4ObjGraphInMbv( mbvAt0,
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t0" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "t0" );

        check4ObjGraphInMbv( mbvAt1,
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t0" },
                  new MeshObjectIdentifier[] { A },
                  new String[] { "t1" },
                  null,
                  null,
                  null,
                  null,
                  "t1" );

        check4ObjGraphInMbv( mbvAt2,
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t0" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "t2" );

        check4ObjGraphInMbv( mbvAt3,
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "t3" );

        //

        log.info( "Checking head" );

        check4ObjGraphInMbv( theMeshBase.getHeadMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "HEAD" );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest12.class );
}
