//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that a Transaction in the past can be augmented in a non-conflicting way.
 */
public class EditableHistoryTest14
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT   = "att";
        final long   delta = 200L;

        HistoryCursorIterator<MeshBaseState> mbHistoryIter = theMeshBase.getHistory().iterator();

        //

        Map<Long,String> captureTs = new HashMap<>();

        checkNotObject( theMeshBase.findMeshObjectByIdentifier( A ), "a is there already" );
        sleepFor( 1 );

        //

        startClock();

        log.info( "Creating at t0" );

        MeshObject a = theMeshBase.execute(
                (tx) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT, "a1" );
                    return ret;
                });
        captureTs.put( a.getTimeUpdated(), "t0" );
        checkStockFlowIntegrity( "t0", captureTs );

        log.info( "Checking at t0 (a)" );

        mbHistoryIter.moveToBeforeFirst();
        mbHistoryIter.next(); // home
        HistoricMeshBaseView mbvAt0a = mbHistoryIter.next().getMeshBaseView();

        check4ObjGraphInMbv( mbvAt0a,
                  2,
                  ATT,
                  new MeshObjectIdentifier[0],
                  new String[] { "t0" },
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "t0 (a)" );

        check4ObjGraphInMbv( theMeshBase.getHeadMeshBaseView(),
                  2,
                  ATT,
                  new MeshObjectIdentifier[0],
                  new String[] { "t0" },
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "HEAD (a)" );
        //

        sleepUntil( delta );

        log.info( "Modifying t0 at t1" );

        MeshObject b = theMeshBase.executeAt(
                a.getTimeCreated(),
                (tx) -> {
                    MeshObject ret = theMeshBase.createMeshObject( B );
                    ret.setAttributeValue( ATT, "b1" );
                    return ret;
                });
        captureTs.put( b.getTimeUpdated(), "t0 again" );
        checkStockFlowIntegrity( "t0", captureTs );

        //

        log.info( "Checking history" );

        checkEquals( theMeshBase.size(), 3, "Wrong number of MeshObjects in the MeshBase" );
        checkEquals( theMeshBase.getHistory().getLength(), 2, "Wrong length of MeshBaseState history" );

        mbHistoryIter.moveToBeforeFirst();
        mbHistoryIter.next(); // home
        HistoricMeshBaseView mbvAt0b = mbHistoryIter.next().getMeshBaseView();

        check4ObjGraphInMbv( mbvAt0b,
                  3,
                  ATT,
                  new MeshObjectIdentifier[0],
                  new String[] { "t0" },
                  new MeshObjectIdentifier[0],
                  new String[] { "t0" },
                  null,
                  null,
                  null,
                  null,
                  "t0 (b)" );

        //

        log.info( "Checking HEAD" );

        check4ObjGraphInMbv( theMeshBase.getHeadMeshBaseView(),
                  3,
                  ATT,
                  new MeshObjectIdentifier[0],
                  new String[] { "t0" },
                  new MeshObjectIdentifier[0],
                  new String[] { "t0" },
                  null,
                  null,
                  null,
                  null,
                  "HEAD (b)" );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest14.class );
}
