//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests everything together: object creation and deletion, type changes, property changes,
 * relationship graph changes, and relationship type changes.
 */
public class DifferencerTest50
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating MeshObjects in MeshBase1" );

        theMeshBase1.execute( (tx) -> {
            MeshObject a1 = theMeshBase1.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject a2 = theMeshBase1.createMeshObject( "aaaa2", TestSubjectArea.AA );
            MeshObject a3 = theMeshBase1.createMeshObject( "aaaa3", TestSubjectArea.AA );
            MeshObject a4 = theMeshBase1.createMeshObject( "aaaa4", TestSubjectArea.AA );

            a1.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa1" ));
            a2.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa2" ));
            a3.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa3" ));
            a4.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa4" ));

            a1.blessRole( TestSubjectArea.AR1A.getSource(), a2 );
            a1.blessRole( TestSubjectArea.AR1A.getSource(), a3 );

            return null;
        });

        //

        log.info( "Creating MeshObjects in MeshBase2" );

        theMeshBase2.execute( (tx) -> {
            MeshObject a1 = theMeshBase2.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject a2 = theMeshBase2.createMeshObject( "aaaa2", TestSubjectArea.AA );
            MeshObject a3 = theMeshBase2.createMeshObject( "aaaa3", TestSubjectArea.AA );
            // DIFFERENCE: no a4
            MeshObject a5 = theMeshBase2.createMeshObject( "aaaa5", TestSubjectArea.AA ); // DIFFERENCE

            a1.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa1" ));
            a1.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 123 )); // DIFFERENCE
            a2.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa2" ));
            a3.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "aaaa3" ));

            a2.blessRole( TestSubjectArea.AR1A.getSource(), a1 ); // DIFFERENCE: switch source and destination
            a1.blessRole( TestSubjectArea.AR1A.getSource(), a3 );
            a1.blessRole( TestSubjectArea.AR1A.getSource(), a5 ); // DIFFERENCE: new relationship

            return null;
        });

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList   changeList = diff.determineChangeList( theMeshBase2 );
        List<Change> changes    = changeList.getChanges();

        log.info( "found " + changes.size() + " changes" );

        if( log.isInfoEnabled() ) {
            for( int i=0 ; i<changes.size() ; ++i ) {
                Change current = changes.get( i );
                log.info( "    " + i + ": " + current );
            }
        }

        // now count

        Class [] changeTypes   = new Class[ changes.size() ]; // this is more than we likely need
        int   [] changeNumbers = new int  [ changes.size() ];

        for( int i=0 ; i<changes.size() ; ++i ) {
            for( int j=0 ; j<changeTypes.length ; ++j ) {
                if( changeTypes[j] == null ) {
                    changeTypes[j] = changes.get( i ).getClass();
                    changeNumbers[j] = 1;
                    break;
                }
                if( changeTypes[j] == changes.get( i ).getClass() ) {
                    ++changeNumbers[j];
                    break;
                }
            }
        }
        for( int j=0 ; j<changeTypes.length ; ++j ) {
            if( changeTypes[j] == null ) {
                break;
            }
            log.info( "  Type " + changeTypes[j] + " -- " + changeNumbers[j] + " times" );
        }

        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeList.applyChangeListTo( theMeshBase1 ));

        //

        ChangeList emptyChangeList = diff.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeList.getChanges().size(), 0, "Change set not empty after applying differences" );

        log.info( "Empty ChangeList is " + emptyChangeList );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(DifferencerTest50.class );
}
