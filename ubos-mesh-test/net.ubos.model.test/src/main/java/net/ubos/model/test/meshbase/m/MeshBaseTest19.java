//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests properties on RoleTypes.
 */
public class MeshBaseTest19
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshObjectIdentifier oneId = theMeshBase.meshObjectIdentifierFromExternalForm( "one" );
        MeshObjectIdentifier twoId = theMeshBase.meshObjectIdentifierFromExternalForm( "two" );

        log.debug( "Creating test objects" );

        theMeshBase.execute( ( Transaction tx ) -> {
            MeshObject one = theMeshBase.createMeshObject( oneId, TestSubjectArea.AA );
            MeshObject two = theMeshBase.createMeshObject( twoId, TestSubjectArea.AA );

            one.blessRole( TestSubjectArea.ARANY.getSource(), two );
            return null;
        } );

        MeshObject one = theMeshBase.findMeshObjectByIdentifier( oneId );
        MeshObject two = one.traverse( TestSubjectArea.ARANY.getSource() ).getSingleMember();

        //

        log.debug( "Checking existence of Role PropertyTypes" );

        checkEquals( one.getRolePropertyTypes( two ).length, 1, "Wrong number of Role PropertyTypes in source" );
        checkEquals( two.getRolePropertyTypes( one ).length, 1, "Wrong number of Role PropertyTypes in destination" );

        //

        log.debug( "Checking default value of RoleType's PropertyTypes" );

        checkEquals( one.getRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO ), TestSubjectArea.ARANY_S_FOO.getDefaultValue(), "source not the same" );
        checkEquals( two.getRolePropertyValue( one, TestSubjectArea.ARANY_D_FOO ), TestSubjectArea.ARANY_D_FOO.getDefaultValue(), "dest not the same" );

        //

        log.debug( "Setting new value" );

        theMeshBase.execute( ( Transaction tx ) -> {

            one.setRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO, StringValue.create( "NewFooSrcValue" ) );

            return null;
        });

        checkEquals( one.getRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO ), StringValue.create( "NewFooSrcValue" ), "source not the same" );
        checkEquals( two.getRolePropertyValue( one, TestSubjectArea.ARANY_D_FOO ), TestSubjectArea.ARANY_D_FOO.getDefaultValue(), "dest not the same" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest19.class );
}
