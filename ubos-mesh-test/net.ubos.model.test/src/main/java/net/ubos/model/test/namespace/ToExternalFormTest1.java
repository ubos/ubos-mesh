//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

//
// Copyright (C) Johannes Ernst. All rights reserved. License: see package.
//

package net.ubos.model.test.namespace;

import java.text.ParseException;
import net.ubos.mesh.BracketMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierFactory;
import net.ubos.mesh.UrlMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.a.AMeshObjectIdentifierFactory;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;

/**
 * Tests that MeshObjectIdentifiers are serialized with the correct namespace prefix,
 * taking the MeshBase's default namespace into account.
 */
public class ToExternalFormTest1
    extends
        AbstractMeshObjectIdentifierNamespaceTest
{
    /**
     * Run one test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run1()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() + ".run1()" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap  = MPrimaryMeshObjectIdentifierNamespaceMap.create();
        MeshObjectIdentifierFactory             idFact = AMeshObjectIdentifierFactory.create( nsMap, nsMap.obtainByExternalName( "default"));

        runWith( nsMap, idFact, BracketMeshObjectIdentifierBothSerializer.create( nsMap, idFact ) );
    }

    /**
     * Run one test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run2()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() + ".run2()" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap  = MPrimaryMeshObjectIdentifierNamespaceMap.create();
        MeshObjectIdentifierFactory             idFact = AMeshObjectIdentifierFactory.create( nsMap, nsMap.obtainByExternalName( "default"));

        runWith( nsMap, idFact, ExternalNameHashMeshObjectIdentifierBothSerializer.create( nsMap, idFact ) );
    }

    /**
     * Run one test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run3()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() + ".run3()" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap  = MPrimaryMeshObjectIdentifierNamespaceMap.create();
        MeshObjectIdentifierFactory             idFact = AMeshObjectIdentifierFactory.create( nsMap, nsMap.obtainByExternalName( "default"));

        BracketMeshObjectIdentifierBothSerializer delegate = BracketMeshObjectIdentifierBothSerializer.create( nsMap, idFact );

        runWith( nsMap, idFact, UrlMeshObjectIdentifierBothSerializer.create( "http://some.where", delegate ));
    }

    /**
     * Run one test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run4()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() + ".run4()" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap  = MPrimaryMeshObjectIdentifierNamespaceMap.create();
        MeshObjectIdentifierFactory             idFact = AMeshObjectIdentifierFactory.create( nsMap, nsMap.obtainByExternalName( "default"));

        ExternalNameHashMeshObjectIdentifierBothSerializer delegate = ExternalNameHashMeshObjectIdentifierBothSerializer.create( nsMap, idFact );

        runWith( nsMap, idFact, UrlMeshObjectIdentifierBothSerializer.create( "http://some.where", delegate ));
    }

    /**
     * One test run.
     */
    protected void runWith(
            PrimaryMeshObjectIdentifierNamespaceMap nsMap,
            MeshObjectIdentifierFactory             idFact,
            MeshObjectIdentifierBothSerializer      ser )
    {
        log.info( "Running with", ser );

        String [] namespaceNames = {
            null,
            "abc",
            "def",
            "foo"
        };

        String [] meshObjectIds = {
            "aaaa",
            "bbbb",
            "cccc"
        };

        int count = 0;
        for( String namespaceName : namespaceNames ) {
            for( String meshObjectId : meshObjectIds ) {

                MeshObjectIdentifierNamespace ns;
                if( namespaceName == null ) {
                    ns = nsMap.getPrimary();
                } else {
                    ns = nsMap.obtainByExternalName( namespaceName );
                }

                MeshObjectIdentifier createdId = idFact.createMeshObjectIdentifierIn( ns, meshObjectId );

                String ext = ser.toExternalForm( createdId );

                try {
                    MeshObjectIdentifier restoredId = ser.meshObjectIdentifierFromExternalForm( ext );

                    checkEquals( createdId, restoredId, "Not the same when restored" );

                } catch( ParseException ex ) {
                    reportError( "Could not parse", ex );
                }
            }
        }

        //

        log.info( "Done" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ToExternalFormTest1.class );
}
