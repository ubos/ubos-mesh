//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Let's test untyped relationships.
 *
 * t0: a -- b
 *                             t1: a -- d
 * t2: a -- b
 *     a -- c
 *                             t3: a -- d
 *                                 b -- d
 *
 *                             t4: b -- d
 * t5: a -- c
 */
public class EditableHistoryTest13
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String               ATT   = "att";
        final long                 delta = 200L;

        Map<Long,String> captureTs = new HashMap<>();
        captureTs.put( theMeshBase.getHomeObject().getTimeUpdated(), "t_home" );

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        log.info( "Setting up t0, t2, t5" );

        long startTime = startClock();

        long t0 = theMeshBase.execute(
                (tx) -> {
                    MeshObject a = theMeshBase.createMeshObject( A );
                    MeshObject b = theMeshBase.createMeshObject( B );
                    a.setRoleAttributeValue( b, ATT, "t0" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t0, "t0" );

        sleepUntil( 2*delta );

        long t2 = theMeshBase.execute(
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject c = theMeshBase.createMeshObject( C );
                    a.setRoleAttributeValue( c, ATT, "t2" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t2, "t2" );

        sleepUntil( 5*delta );

        long t5 = theMeshBase.execute(
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );
                    a.unrelate( b );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t5, "t5" );

        //

        log.info( "Patching at t1" );

        long t1 = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject d = theMeshBase.createMeshObject( D );
                    a.setRoleAttributeValue( d, ATT, "t1" );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t2, "t2" );

        log.info( "Patching at t3" );

        long t3 = theMeshBase.executeAt(
                startTime + 3*delta,
                (tx) -> {
                    MeshObject b = theMeshBase.findMeshObjectByIdentifier( B );
                    MeshObject d = theMeshBase.findMeshObjectByIdentifier( D );
                    b.setRoleAttributeValue( d, ATT, "t3" );
                    return b;
                }).getTimeUpdated();
        captureTs.put( t3, "t3" );

        log.info( "Patching at t4" );

        long t4 = theMeshBase.executeAt(
                startTime + 4*delta,
                (tx) -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    MeshObject d = theMeshBase.findMeshObjectByIdentifier( D );
                    a.unrelate( d );
                    return a;
                }).getTimeUpdated();
        captureTs.put( t4, "t4" );

        //

        log.info( "Checking stock flow integrity" );
        checkStockFlowIntegrity( "end", captureTs );

        //

        log.info( "Checking history" );

        checkEquals( theMeshBase.getHistory().getLength(), 7, "Wrong length of MeshBase history" );

        MeshObjectHistory aHistory = theMeshBase.meshObjectHistory( A );
        MeshObjectHistory bHistory = theMeshBase.meshObjectHistory( B );
        MeshObjectHistory cHistory = theMeshBase.meshObjectHistory( C );
        MeshObjectHistory dHistory = theMeshBase.meshObjectHistory( D );

        checkEquals( aHistory.getLength(), 5, "Wrong length of a history" );
        checkEquals( bHistory.getLength(), 3, "Wrong length of b history" );
        checkEquals( cHistory.getLength(), 1, "Wrong length of c history" );
        checkEquals( dHistory.getLength(), 3, "Wrong length of d history" );

        //

        log.info( "Checking each state in the history" );

        HistoryCursorIterator<MeshBaseState> mbHistoryIter = theMeshBase.getHistory().iterator();
        mbHistoryIter.moveToBeforeFirst();
        mbHistoryIter.next(); // home
        HistoricMeshBaseView mbvAt0 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt1 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt2 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt3 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt4 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt5 = mbHistoryIter.next().getMeshBaseView();

        check4ObjGraphInMbv( mbvAt0,
                  3,
                  ATT,
                  new MeshObjectIdentifier[] { B },
                  new String[] { "t0" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  null,
                  null,
                  "t0" );

        check4ObjGraphInMbv( mbvAt1,
                  4,
                  ATT,
                  new MeshObjectIdentifier[] { B, D },
                  new String[] { "t0", "t1" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  null,
                  null,
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  "t1" );

        check4ObjGraphInMbv( mbvAt2,
                  5,
                  ATT,
                  new MeshObjectIdentifier[] { B, C, D },
                  new String[] { "t0", "t2", "t1" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  "t2" );

        check4ObjGraphInMbv( mbvAt3,
                  5,
                  ATT,
                  new MeshObjectIdentifier[] { B, C, D },
                  new String[] { "t0", "t2", "t1" },
                  new MeshObjectIdentifier[] { A, D },
                  new String[] { null, "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { A, B },
                  new String[2],
                  "t3" );

        check4ObjGraphInMbv( mbvAt4,
                  5,
                  ATT,
                  new MeshObjectIdentifier[] { B, C },
                  new String[] { "t0", "t2" },
                  new MeshObjectIdentifier[] { A, D },
                  new String[] { null, "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { B },
                  new String[1],
                  "t4" );

        check4ObjGraphInMbv( mbvAt5,
                  5,
                  ATT,
                  new MeshObjectIdentifier[] { C },
                  new String[] { "t2" },
                  new MeshObjectIdentifier[] { D },
                  new String[] { "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { B },
                  new String[1],
                  "t5" );

        //

        log.info( "Checking HEAD" );

        check4ObjGraphInMbv( theMeshBase.getHeadMeshBaseView(),
                  5,
                  ATT,
                  new MeshObjectIdentifier[] { C },
                  new String[] { "t2" },
                  new MeshObjectIdentifier[] { D },
                  new String[] { "t3" },
                  new MeshObjectIdentifier[] { A },
                  new String[1],
                  new MeshObjectIdentifier[] { B },
                  new String[1],
                  "HEAD" );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest13.class );
}
