//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests whether rollbacks work.
 */
public class RollbackTest1
        extends
            AbstractMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshBase theMeshBase = MMeshBase.Builder.create().build();

        final MeshObjectIdentifier fixed1Id = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed1" );
        final MeshObjectIdentifier fixed2Id = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed2" );
        final MeshObjectIdentifier wrong1Id = theMeshBase.meshObjectIdentifierFromExternalForm( "wrong1" );
        final MeshObjectIdentifier wrong2Id = theMeshBase.meshObjectIdentifierFromExternalForm( "wrong2" );

        //

        log.debug( "Creating MeshObjectGraph that won't be rolled back" );

        theMeshBase.execute( t -> {
            MeshObject fixed1 = theMeshBase.createMeshObject( fixed1Id, TestSubjectArea.AA );
            MeshObject fixed2 = theMeshBase.createMeshObject( fixed2Id, TestSubjectArea.AA );

            return null;
        });

        checkEquals( theMeshBase.size(), 3, "Wrong initial number of MeshObjects" );

        final MeshObject fixed1 = theMeshBase.findMeshObjectByIdentifier( fixed1Id );
        final MeshObject fixed2 = theMeshBase.findMeshObjectByIdentifier( fixed2Id );

        PropertyValue fixed1Value = fixed1.getPropertyValue( TestSubjectArea.AA_Y );
        PropertyValue fixed2Value = fixed2.getPropertyValue( TestSubjectArea.AA_Y );

        //

        log.debug( "Creating failing Transaction that will automatically be rolled back." );

        theMeshBase.execute( ()-> {
                MeshObject wrong1 = theMeshBase.createMeshObject( wrong1Id, TestSubjectArea.AA );
                MeshObject wrong2 = theMeshBase.createMeshObject( wrong2Id, TestSubjectArea.AA );

                fixed1.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 1.2 ));

                wrong1.blessRole( TestSubjectArea.AR1A.getSource(), wrong2 );
                fixed1.setRoleAttributeValue( wrong1, "foo", "bar" );
                wrong2.blessRole( TestSubjectArea.AR1A.getSource(), fixed2 );

                wrong2.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 3.4 ));

                fixed1.blessRole( TestSubjectArea.AR1A.getSource(), fixed2 );

                throw new TransactionActionException.Rollback();
        });

        //

        checkEquals( theMeshBase.size(), 3, "Wrong initial number of MeshObjects" );
        checkCondition( theMeshBase.findMeshObjectByIdentifier( wrong1Id ) == null, "Found wrong1Id" );
        checkCondition( theMeshBase.findMeshObjectByIdentifier( wrong2Id ) == null, "Found wrong2Id" );
        checkEquals( fixed1Value, fixed1.getPropertyValue( TestSubjectArea.AA_Y ), "Fixed1 has wrong property value" );
        checkEquals( fixed2Value, fixed2.getPropertyValue( TestSubjectArea.AA_Y ), "Fixed2 has wrong property value" );
        checkCondition( !fixed1.isRelated( fixed2 ), "fixed1 is still related to fixed2" );
    }

    // Our Logger
    private static Log log = Log.getLogInstance( RollbackTest1.class );
}
