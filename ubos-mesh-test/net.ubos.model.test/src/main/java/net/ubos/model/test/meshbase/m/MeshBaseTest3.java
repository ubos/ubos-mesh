//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * This tests the setting of Attributes.
 */
public class MeshBaseTest3
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating MeshObjects" );

        MeshObject [] created = theMeshBase.execute( (Transaction tx) -> {
            return new MeshObject[] {
                theMeshBase.createMeshObject( TestSubjectArea.AA ),
                theMeshBase.createMeshObject( TestSubjectArea.B )
            };
        } );

        MeshObject a = created[0];
        MeshObject b = created[1];

        //

        log.info( "Checking that Transactions are required" );
        try {
            a.setAttributeValue(  "a", 1 );

            reportError( "No TransactionException thrown" );
        } catch( TransactionException ex ) {
            // fine
        }

        //

        log.info( "Setting Attributes" );

        theMeshBase.execute( (Transaction tx) -> {
            b.setAttributeValue( "b1", "wrong" );

            a.setAttributeValue( "a1", "A1" );
            a.setAttributeValue( "a2", "A2" );

            b.setAttributeValue( "b1", "B1" );
            return null;
        } );

        checkEquals( a.getAttributeValue( "a1" ), "A1", "Wrong value" );
        checkEquals( a.getAttributeValue( "a2" ), "A2", "Wrong value" );
        checkEquals( b.getAttributeValue( "b1" ), "B1", "Wrong value" );

        checkEqualsOutOfSequence( a.getAttributeNames(), new String[] { "a1", "a2" }, "Wrong Attributes" );
        checkEqualsOutOfSequence( b.getAttributeNames(), new String[] { "b1" },       "Wrong Attributes" );

        //

        log.info( "Rolling back Attributes" );

        theMeshBase.execute( (Transaction tx) -> {
            b.setAttributeValue( "b1", "wrong" );
            b.setAttributeValue( "b2", "wrong" );

            throw new TransactionActionException.Rollback();
        } );

        checkEquals( a.getAttributeValue( "a1" ), "A1", "Wrong value" );
        checkEquals( a.getAttributeValue( "a2" ), "A2", "Wrong value" );
        checkEquals( b.getAttributeValue( "b1" ), "B1", "Wrong value" );

        checkEqualsOutOfSequence( a.getAttributeNames(), new String[] { "a1", "a2" }, "Wrong Attributes" );
        checkEqualsOutOfSequence( b.getAttributeNames(), new String[] { "b1" },       "Wrong Attributes" );

        //

        log.info( "Deleting Attributes" );

        theMeshBase.execute( (Transaction tx) -> {
            a.deleteAttribute( "a1" );
            b.deleteAttribute( "b1" );

            return null;
        } );

        checkEquals( a.getAttributeValue( "a2" ), "A2", "Wrong value" );

        checkEqualsOutOfSequence( a.getAttributeNames(), new String[] { "a2" }, "Wrong Attributes" );
        checkEqualsOutOfSequence( b.getAttributeNames(), new String[] {},       "Wrong Attributes" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest3.class);
}
