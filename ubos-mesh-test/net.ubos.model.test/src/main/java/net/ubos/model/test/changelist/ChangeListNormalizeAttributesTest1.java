//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.changelist;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.meshbase.transaction.MeshObjectAttributesAddChange;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that the set of Attributes is correctly reduced.
 */
public class ChangeListNormalizeAttributesTest1
        extends
            AbstractChangeListNormalizeTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final MeshObjectIdentifier A    = theMeshBase.createMeshObjectIdentifier( "a" );
        final String               ATT1 = "att1";
        final String               ATT2 = "att2";

        log.info( "Creating MeshObject(s)" );

        theMeshBase.execute( () -> {
            MeshObject a = theMeshBase.createMeshObject( A );
        });

        //

        log.info( "Create the ChangeList" );

        ChangeList list = theMeshBase.execute( (tx) -> {
            MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
            a.setAttributeValue( ATT1, 1 );
            a.setAttributeValue( ATT2, 2 );
            a.deleteAttribute( ATT1 );
            return tx.getChangeList().clone(); // get clone, the Transaction's ChangeList will be normalized on
        });
        checkEquals( list.size(), 6, "Wrong size before" );

        //

        log.info( "Normalizing" );

        list.normalize();

        checkEquals( list.size(), 2, "Wrong size after" );

        MeshObjectAttributesAddChange change1 = (MeshObjectAttributesAddChange) list.getChanges().get( 0 );
        MeshObjectAttributeChange     change2 = (MeshObjectAttributeChange) list.getChanges().get( 1 );

        checkEqualsInSequence( change1.getOldValue(), new String[0],         "Wrong before attributes value" );
        checkEqualsInSequence( change1.getNewValue(), new String[] { ATT2 }, "Wrong after attributes value" );
        checkEquals(           change2.getOldValue(), null,                  "Wrong before ATT2 value" );
        checkEquals(           change2.getNewValue(), 2,                     "Wrong after ATT2 value" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ChangeListNormalizeAttributesTest1.class );
}

