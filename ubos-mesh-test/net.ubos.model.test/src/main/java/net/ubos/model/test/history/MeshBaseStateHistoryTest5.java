//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.history.MeshBaseStateHistory;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that traversing related MeshObjects starting with a MeshObject in a non-HEAD MeshBaseView
 * stays within that MeshBaseView.
 */
public class MeshBaseStateHistoryTest5
        extends
            AbstractMeshBaseStateHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long delta = 500;

        final MeshObjectIdentifier ID1 = theMeshBase.createMeshObjectIdentifier( "obj-1" );
        final MeshObjectIdentifier ID2 = theMeshBase.createMeshObjectIdentifier( "obj-2" );
        final MeshObjectIdentifier ID3 = theMeshBase.createMeshObjectIdentifier( "obj-3" );

        startClock();

        //

        log.info( "Creating MeshObjects (t1) -- creating Home Object was t0" );

        sleepUntil( delta );

        long t1 = theMeshBase.execute( ( Transaction tx ) -> {
            theMeshBase.createMeshObject( ID1, TestSubjectArea.AA );
            theMeshBase.createMeshObject( ID2, TestSubjectArea.B );
            theMeshBase.createMeshObject( ID3, TestSubjectArea.B );
            return tx;
        } ).getTimeEnded();

        //

        sleepUntil( delta*2 );

        log.info( "Relating MeshObjects (t2)" );

        long t2 = theMeshBase.execute( ( Transaction tx ) -> {
            MeshObject obj1 = theMeshBase.findMeshObjectByIdentifier( ID1 );
            MeshObject obj2 = theMeshBase.findMeshObjectByIdentifier( ID2 );

            obj1.blessRole( TestSubjectArea.ARANY_S, obj2 );
            return tx;
        } ).getTimeEnded();

        //

        sleepUntil( delta*3 );

        log.info( "Relating MeshObjects (t3)" );

        long t3 = theMeshBase.execute( ( Transaction tx ) -> {
            MeshObject obj1 = theMeshBase.findMeshObjectByIdentifier( ID1 );
            MeshObject obj3 = theMeshBase.findMeshObjectByIdentifier( ID3 );

            obj1.blessRole( TestSubjectArea.ARANY_S, obj3 );
            return tx;
        } ).getTimeEnded();

        //

        sleepUntil( delta*4 );

        log.info( "Unrelating MeshObjects (t4)" );

        long t4 = theMeshBase.execute( ( Transaction tx ) -> {
            MeshObject obj1 = theMeshBase.findMeshObjectByIdentifier( ID1 );
            MeshObject obj2 = theMeshBase.findMeshObjectByIdentifier( ID2 );

            obj1.unrelate( obj2 );
            return tx;
        } ).getTimeEnded();

        //

        log.info(  "Checking histories (1)" );

        MeshBaseStateHistory mbHist = theMeshBase.getHistory();
        MeshObjectHistory    hist1  = theMeshBase.meshObjectHistory( ID1 );
        MeshObjectHistory    hist2  = theMeshBase.meshObjectHistory( ID2 );
        MeshObjectHistory    hist3  = theMeshBase.meshObjectHistory( ID3 );

        checkEquals( mbHist.getLength(), 5, "Wrong MeshBaseStateHistory" ); // Home - t0, create - t1, relate - t2, relate -t3, unrelate t4
        checkEquals( hist3.getLength(),  2, "Wrong history 3" ); // t1, t3
        checkEquals( hist2.getLength(),  3, "Wrong history 2" ); // t1, t2, t4
        checkEquals( hist1.getLength(),  4, "Wrong history 1" ); // t1, t2, t3, t4

        //

        log.info( "Traversing in HEAD" );

        MeshObject obj1head = theMeshBase.findMeshObjectByIdentifier( ID1 );
        MeshObject obj2head = theMeshBase.findMeshObjectByIdentifier( ID2 );
        MeshObject obj3head = theMeshBase.findMeshObjectByIdentifier( ID3 );

        checkIdentity( obj1head.getMeshBaseView(), theMeshBase.getHeadMeshBaseView(), "Wrong MeshBaseView 1" );
        checkIdentity( obj2head.getMeshBaseView(), theMeshBase.getHeadMeshBaseView(), "Wrong MeshBaseView 2" );
        checkIdentity( obj3head.getMeshBaseView(), theMeshBase.getHeadMeshBaseView(), "Wrong MeshBaseView 3" );

        checkIdentity( obj1head.getMeshBase(), theMeshBase, "Wrong MeshBase 1" );
        checkIdentity( obj2head.getMeshBase(), theMeshBase, "Wrong MeshBase 2" );
        checkIdentity( obj3head.getMeshBase(), theMeshBase, "Wrong MeshBase 3" );

        checkEquals( obj1head.traverse( TestSubjectArea.ARANY_S ).size(), 1, "Wrong neighbors 1" );
        checkEquals( obj2head.traverse( TestSubjectArea.ARANY_D ).size(), 0, "Wrong neighbors 2" );
        checkEquals( obj3head.traverse( TestSubjectArea.ARANY_D ).size(), 1, "Wrong neighbors 3" );

        checkIdentity( obj1head.traverse( TestSubjectArea.ARANY_S ).getSingleMember(), obj3head, "Not same instance obj2" );
        checkIdentity( obj3head.traverse( TestSubjectArea.ARANY_D ).getSingleMember(), obj1head, "Not same instance obj1" );

        obj1head = null;
        obj2head = null;
        obj3head = null;

        //

        log.info( "Checking Home Object creation" );

        MeshObject homet0   = mbHist.oldest().getMeshBaseView().getHomeObject();
        MeshObject homeHead = theMeshBase.getHomeObject();

        checkEquals( homet0.getIdentifier(), homeHead.getIdentifier(), "Home objects do not equal" ); // not sure this is actually the right semantics
        checkNotIdentity( homet0, homeHead, "Identical home object instance" );

        homet0   = null;
        homeHead = null;

        //

        log.info( "Checking t1" );

        HistoryCursorIterator<MeshBaseState> mbHistIter = mbHist.iterator();
        mbHistIter.moveToBeforeFirst();
        mbHistIter.next();

        MeshBaseState state1 = mbHistIter.next();
        MeshBaseView  viewt1 = state1.getMeshBaseView();
        checkNotIdentity( viewt1, theMeshBase, "MeshBaseView at t1 is HEAD" );
        checkEquals( state1.getTimeUpdated(), t1, "Wrong t1" );

        MeshObject obj1t1 = hist1.oldest();
        MeshObject obj2t1 = hist2.oldest();
        MeshObject obj3t1 = hist3.oldest();

        checkEquals( obj1t1.traverseToNeighbors().size(), 0, "1 has neighbors at t1" );
        checkEquals( obj2t1.traverseToNeighbors().size(), 0, "2 has neighbors at t1" );
        checkEquals( obj3t1.traverseToNeighbors().size(), 0, "3 has neighbors at t1" );

        checkEquals( obj1t1.getMeshBaseView(), viewt1, "Wrong MeshBaseView 1 at t1" );
        checkEquals( obj2t1.getMeshBaseView(), viewt1, "Wrong MeshBaseView 2 at t1" );
        checkEquals( obj3t1.getMeshBaseView(), viewt1, "Wrong MeshBaseView 3 at t1" );

        //

        log.info(  "Checking t2" );

        MeshBaseState state2 = mbHistIter.next();
        MeshBaseView  viewt2 = state2.getMeshBaseView();
        checkNotIdentity( viewt2, theMeshBase, "MeshBaseView at t2 is identical to HEAD" );
        checkNotIdentity( viewt2, viewt1, "MeshBaseView at t2 is identical to t1" );
        checkEquals( state2.getTimeUpdated(), t2, "Wrong t2" );

        MeshObject obj1t2 = viewt2.findMeshObjectByIdentifier( ID1 );
        MeshObject obj2t2 = viewt2.findMeshObjectByIdentifier( ID2 );
        MeshObject obj3t2 = viewt2.findMeshObjectByIdentifier( ID3 );

        checkNotIdentity( obj1t2, obj1t1, "1 is identical at t1 and t2" );
        checkNotIdentity( obj2t2, obj2t1, "2 is identical at t1 and t2" );
        checkNotIdentity( obj3t2, obj3t1, "3 is identical at t1 and t2" );

        checkIdentity( obj1t2.traverse( TestSubjectArea.ARANY_S ).getSingleMember(), obj2t2, "Not identical 2 at t2" );
        checkIdentity( obj2t2.traverse( TestSubjectArea.ARANY_D ).getSingleMember(), obj1t2, "Not identical 1 at t2" );
        checkEquals( obj3t2.traverseToNeighbors().size(), 0, "3 has neighbors at t2" );

        //

        log.info(  "Checking t3" );

        MeshBaseState state3 = mbHistIter.next();
        MeshBaseView  viewt3 = state3.getMeshBaseView();
        checkNotIdentity( viewt3, theMeshBase, "MeshBaseView at t3 is identical to HEAD" );
        checkNotIdentity( viewt3, viewt1, "MeshBaseView at t3 is identical to t1" );
        checkNotIdentity( viewt3, viewt2, "MeshBaseView at t3 is identical to t2" );
        checkEquals( state3.getTimeUpdated(), t3, "Wrong t3" );

        MeshObject obj1t3 = viewt3.findMeshObjectByIdentifier( ID1 );
        MeshObject obj2t3 = viewt3.findMeshObjectByIdentifier( ID2 );
        MeshObject obj3t3 = viewt3.findMeshObjectByIdentifier( ID3 );

        checkNotIdentity( obj1t3, obj1t1, "1 is identical at t1 and t3" );
        checkNotIdentity( obj2t3, obj2t1, "2 is identical at t1 and t3" );
        checkNotIdentity( obj3t3, obj3t1, "3 is identical at t1 and t3" );

        checkNotIdentity( obj1t3, obj1t2, "1 is identical at t2 and t3" );
        checkNotIdentity( obj2t3, obj2t2, "2 is identical at t2 and t3" );
        checkNotIdentity( obj3t3, obj3t2, "3 is identical at t2 and t3" );

        checkIdentity( obj1t3.traverse( TestSubjectArea.ARANY_S ).find( (o) -> o.getIdentifier().equals( ID2 ) ), obj2t3, "Not identical 2 at t2" );
        checkIdentity( obj1t3.traverse( TestSubjectArea.ARANY_S ).find( (o) -> o.getIdentifier().equals( ID3 ) ), obj3t3, "Not identical 3 at t3" );
        checkIdentity( obj2t3.traverse( TestSubjectArea.ARANY_D ).getSingleMember(), obj1t3, "Not identical 1 at t3 (2)" );
        checkIdentity( obj3t3.traverse( TestSubjectArea.ARANY_D ).getSingleMember(), obj1t3, "Not identical 1 at t3 (3)" );

        //

        log.info(  "Checking t4" );

        MeshBaseState state4 = mbHistIter.next();
        MeshBaseView  viewt4 = state4.getMeshBaseView();
        checkNotIdentity( viewt4, theMeshBase, "MeshBaseView at t4 is identical to HEAD" );
        checkNotIdentity( viewt4, viewt1, "MeshBaseView at t4 is identical to t1" );
        checkNotIdentity( viewt4, viewt2, "MeshBaseView at t4 is identical to t2" );
        checkNotIdentity( viewt4, viewt3, "MeshBaseView at t4 is identical to t3" );
        checkEquals( state4.getTimeUpdated(), t4, "Wrong t4" );

        MeshObject obj1t4 = viewt4.findMeshObjectByIdentifier( ID1 );
        MeshObject obj2t4 = viewt4.findMeshObjectByIdentifier( ID2 );
        MeshObject obj3t4 = viewt4.findMeshObjectByIdentifier( ID3 );

        checkNotIdentity( obj1t4, obj1t1, "1 is identical at t1 and t4" );
        checkNotIdentity( obj2t4, obj2t1, "2 is identical at t1 and t4" );
        checkNotIdentity( obj3t4, obj3t1, "3 is identical at t1 and t4" );

        checkNotIdentity( obj1t4, obj1t2, "1 is identical at t2 and t4" );
        checkNotIdentity( obj2t4, obj2t2, "2 is identical at t2 and t4" );
        checkNotIdentity( obj3t4, obj3t2, "3 is identical at t2 and t4" );

        checkNotIdentity( obj1t4, obj1t3, "1 is identical at t3 and t4" );
        checkNotIdentity( obj2t4, obj2t3, "2 is identical at t3 and t4" );
        checkNotIdentity( obj3t4, obj3t3, "3 is identical at t3 and t4" );

        checkIdentity( obj1t4.traverse( TestSubjectArea.ARANY_S ).getSingleMember(), obj3t4, "Not identical 3 at t4" );
        checkIdentity( obj3t4.traverse( TestSubjectArea.ARANY_D ).getSingleMember(), obj1t4, "Not identical 1 at t4" );
        checkEquals( obj2t4.traverseToNeighbors().size(), 0, "3 has neighbors at t4" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseStateHistoryTest5.class);
}
