//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.importer;

import net.ubos.meshbase.externalized.json.DefaultMeshBaseJsonImporter;
import net.ubos.meshbase.history.MeshBaseStateHistory;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Same as LoadMeshBaseNoHistoryTest1, but with History enabled.
 */
public class LoadMeshBaseNoHistoryTest1b
    extends
        AbstractExternalizedMeshBaseDeEnCoderTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    @Disabled // need to fix the "create MeshObjects in the past" API
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Loading MeshObjects" );

        MMeshBase mb = MMeshBase.Builder.create().history( true ).namespaceMap( thePrimaryNsMap ).build();

        DefaultMeshBaseJsonImporter decoder = DefaultMeshBaseJsonImporter.create();
        decoder.importTo( LoadMeshBaseNoHistoryTest1a.class.getResourceAsStream( "LoadMeshBaseNoHistoryTest1.mesh"), mb );

        //

        log.info( "Checking" );

        checkEquals( mb.size(), 3, "Wrong number of MeshObjects" );

        MeshBaseStateHistory mbh = mb.getHistory();

        checkEquals( mbh.getLength(), 2, "Wrong history length" );
        checkEquals( mbh.oldest().getMeshBaseView().size(), 3, "Wrong oldest number of MeshObjects" );
        checkEquals( mbh.current().getMeshBaseView().size(), 3, "Wrong newest number of MeshObjects" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(LoadMeshBaseNoHistoryTest1b.class);
}
