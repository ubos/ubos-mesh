//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.changelist;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.meshbase.transaction.MeshObjectAttributesAddChange;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that several sets of Attributes are correctly reduced.
 */
public class ChangeListNormalizeAttributesTest2
        extends
            AbstractChangeListNormalizeTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final MeshObjectIdentifier A    = theMeshBase.createMeshObjectIdentifier( "a" );
        final MeshObjectIdentifier B    = theMeshBase.createMeshObjectIdentifier( "b" );
        final String               ATT1 = "att1";
        final String               ATT2 = "att2";
        final String               ATT3 = "att3";

        log.info( "Creating MeshObject(s)" );

        theMeshBase.execute( () -> {
            MeshObject a = theMeshBase.createMeshObject( A );
            a.setAttributeValue( ATT3, 30 );
        });

        //

        log.info( "Create the ChangeList" );

        ChangeList list = theMeshBase.execute( (tx) -> {
            MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
            a.setAttributeValue( ATT1, 11 );   // 2
            a.deleteAttribute( ATT1 );         // 2
            a.setAttributeValue( ATT3, 31 );   // 1
            a.setAttributeValue( ATT2, 21 );   // 2
            a.setAttributeValue( ATT1, 12 );   // 2
            a.setAttributeValue( ATT2, 23 );   // 1
            a.deleteAttribute( ATT2 );         // 2

            return tx.getChangeList().clone(); // get clone, the Transaction's ChangeList will be normalized on
        });
        checkEquals( list.size(), 12, "Wrong size before" );

        //

        log.info( "Normalizing" );

        list.normalize();

        checkEquals( list.size(), 3, "Wrong size after" );

        MeshObjectAttributesAddChange change1 = (MeshObjectAttributesAddChange) list.getChanges().get( 0 );
        MeshObjectAttributeChange     change2 = (MeshObjectAttributeChange) list.getChanges().get( 1 );
        MeshObjectAttributeChange     change3 = (MeshObjectAttributeChange) list.getChanges().get( 2 );

        checkEqualsInSequence(    change1.getOldValue(), new String[] { ATT3 },       "Wrong change1 before attributes value" );
        checkEqualsOutOfSequence( change1.getNewValue(), new String[] { ATT1, ATT3 }, "Wrong change1 after attributes value" );
        checkEquals(              change2.getOldValue(), null,                        "Wrong change2 before ATT1 value" );
        checkEquals(              change2.getNewValue(), 12,                          "Wrong change2 after ATT1 value" );
        checkEquals(              change3.getOldValue(), 30,                          "Wrong change3 before ATT1 value" );
        checkEquals(              change3.getNewValue(), 31,                          "Wrong change3 after ATT1 value" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ChangeListNormalizeAttributesTest2.class );
}

