//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.modelbase;

import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests parsing and serialization of MeshTypeIdentifiers
 */
public class MeshTypeIdentifierTest1
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshTypeIdentifierBothSerializer ser = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

        for( String [] test : TESTS ) {
            log.debug( (Object) test );

            MeshTypeIdentifier id = ser.fromExternalForm( test[0] );

            checkEquals( id.getSubjectAreaPart(),       test[1], "SubjectArea part wrong" );
            checkEquals( id.getLocalPart(),  test[2], "SubjectArea local part wrong" );

            String ext = ser.toExternalForm( id );

            checkEquals( ext, test[0], "Externalized form wrong" );
        }
    }

    /**
     * The test cases.
     * String form, SubjectArea part, CollectableMeshObject part, RoleType direction part
     */
    public static final String [][] TESTS = {
        {   "foo",                    "foo",     null                 },
        {   "foo.bar",                "foo.bar", null                 },
        {   "foo/Bar",                "foo",     "Bar"                },
        {   "foo/Bar_Baz",            "foo",     "Bar_Baz"            },
        {   "foo/Bar_Baz_Bar",        "foo",     "Bar_Baz_Bar"        },
        {   "foo/Bar_Baz_Bar-S",      "foo",     "Bar_Baz_Bar-S"      },
        {   "foo/Bar_Baz_Bar-D",      "foo",     "Bar_Baz_Bar-D"      },
        {   "foo/Bar_Baz_Bar-T",      "foo",     "Bar_Baz_Bar-T"      },
        {   "foo/Bar_Baz_Bar-D_Argl", "foo",     "Bar_Baz_Bar-D_Argl" },
    };

    /**
     * Our own, private logger.
     */
    private static final Log log = Log.getLogInstance( MeshTypeIdentifierTest1.class );
}
