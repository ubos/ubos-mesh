//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import net.ubos.util.logging.Log;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Test.TestSubjectArea;
import org.junit.jupiter.api.Test;

/**
 * Creates unrelated MeshObjects and makes sure that events are created.
 */
public class DifferencerTest1
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshObjects in MeshBase1" );

        theMeshBase1.execute( (Transaction tx ) -> {
            return theMeshBase1.createMeshObject( "aaaa1", TestSubjectArea.AA );
        } );

        //

        log.info( "Creating MeshObjects in MeshBase2" );

        theMeshBase2.execute( (Transaction tx ) -> {
            return theMeshBase2.createMeshObject( "aaaa5", TestSubjectArea.AA ); // DIFFERENCE
        } );

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList changeList = diff.determineChangeList( theMeshBase2 );

        printChangeList( log, changeList );

        checkEquals( changeList.size(), 4, "Not the right number of changes" ); // unbless, delete, create, bless

        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeList.applyChangeListTo( theMeshBase1 ));

        //

        ChangeList emptyChangeList = diff.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeList.size(), 0, "ChangeList not empty after applying differences" );

        if( emptyChangeList.size() > 0 ) {
            log.debug( "ChangeList is " + emptyChangeList );
        } else {
            log.debug( "ChangeList is empty" );
        }

    }

    // Our Logger
    private static final Log log = Log.getLogInstance( DifferencerTest1.class);
}
