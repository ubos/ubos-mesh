//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributeChange;
import net.ubos.meshbase.transaction.MeshObjectRoleAttributesRemoveChange;
import net.ubos.util.logging.Log;

/**
 * Tests that relating and unrelating MeshObjects by setting RoleAttributes creates the right events.
 */
public class MeshBaseTest8
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    public void run()
        throws
            Exception
    {
        log.info( "Creating MeshObjects" );

        MeshObject [] created = theMeshBase.execute( ( tx ) -> {
            MeshObject a = theMeshBase.createMeshObject();
            MeshObject b = theMeshBase.createMeshObject();
            MeshObject c = theMeshBase.createMeshObject();

            return new MeshObject[] { a, b, c };
        });

        final MeshObject a = created[0];
        final MeshObject b = created[1];
        final MeshObject c = created[2];

        MeshObjectTransactionListener listenerA = new MeshObjectTransactionListener( a );
        MeshObjectTransactionListener listenerB = new MeshObjectTransactionListener( b );
        MeshObjectTransactionListener listenerC = new MeshObjectTransactionListener( c );
        theMeshBase.addWeakTransactionListener( listenerA );
        theMeshBase.addWeakTransactionListener( listenerB );
        theMeshBase.addWeakTransactionListener( listenerC );

        //

        log.info( "Relating MeshObjects" );

        theMeshBase.execute( ( tx ) -> {
            a.setRoleAttributeValue( b, "foo", true );
            a.setRoleAttributeValue( c, "bar", true );
            return null;
        } );

        //

        log.debug( "Checking events" );

        checkEquals( listenerA.theEvents.size(), 2, "Wrong number of events at object A" );
        checkEquals( listenerB.theEvents.size(), 1, "Wrong number of events at object B" );
        checkEquals( listenerC.theEvents.size(), 1, "Wrong number of events at object C" );

        checkCondition(listenerA.theEvents.get( 0 ) instanceof MeshObjectRoleAttributeChange, "Wrong type" );
        checkCondition(listenerA.theEvents.get( 1 ) instanceof MeshObjectRoleAttributeChange, "Wrong type" );
        checkCondition(listenerB.theEvents.get( 0 ) instanceof MeshObjectRoleAttributeChange, "Wrong type" );
        checkCondition(listenerC.theEvents.get( 0 ) instanceof MeshObjectRoleAttributeChange, "Wrong type" );

        listenerA.clear();
        listenerB.clear();
        listenerC.clear();

        //

        log.info( "Unrelating MeshObjects (1)" );

        theMeshBase.execute( ( tx ) -> {
            a.unrelate( b );

            return null;
        } );

        //

        log.debug( "Checking events" );

        checkEquals( listenerA.theEvents.size(), 1, "Wrong number of events at object A" );
        checkEquals( listenerB.theEvents.size(), 0, "Wrong number of events at object B" );
        checkEquals( listenerB.theEvents.size(), 0, "Wrong number of events at object C" );

        checkCondition(listenerA.theEvents.get( 0 ) instanceof MeshObjectRoleAttributesRemoveChange,        "Wrong type" );

        listenerA.clear();
        listenerB.clear();
        listenerC.clear();

        //

        log.info( "Unrelating MeshObjects (2)" );

        theMeshBase.execute( ( tx ) -> {
            a.deleteRoleAttribute( c, "bar" );

            return null;
        } );

        //

        log.debug( "Checking events" );

        checkEquals( listenerA.theEvents.size(), 1, "Wrong number of events at object A" );
        checkEquals( listenerB.theEvents.size(), 0, "Wrong number of events at object B" );
        checkEquals( listenerB.theEvents.size(), 0, "Wrong number of events at object C" );

        checkCondition(listenerA.theEvents.get( 0 ) instanceof MeshObjectRoleAttributesRemoveChange,        "Wrong type" );

        listenerA.clear();
        listenerB.clear();
        listenerC.clear();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest8.class );
}
