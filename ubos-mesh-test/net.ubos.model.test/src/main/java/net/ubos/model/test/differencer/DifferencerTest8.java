//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests Role Properties on the other end of the relationship
 */
public class DifferencerTest8
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final PropertyValue newSrcValue  = StringValue.create( "NewSrcValue" );
        final PropertyValue newSrcValue2 = StringValue.create( "NewSrcValue2" );
        final PropertyValue newDestValue = StringValue.create( "NewDestValue" );

        //

        log.info( "Creating MeshObjects in MeshBase1" );

        MeshObject [] mb1Objs = theMeshBase1.execute( (tx) -> {
            MeshObject a1 = theMeshBase1.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject a2 = theMeshBase1.createMeshObject( "aaaa2", TestSubjectArea.AA );

            a1.blessRole( TestSubjectArea.ARANY_S, a2 );

            return new MeshObject[] { a1, a2 };
        });

        //

        log.info( "Creating MeshObjects in MeshBase2" );

        MeshObject [] mb2Objs = theMeshBase2.execute( (tx) -> {
            MeshObject a1 = theMeshBase2.createMeshObject( "aaaa1", TestSubjectArea.AA );
            MeshObject a2 = theMeshBase2.createMeshObject( "aaaa2", TestSubjectArea.AA );

            a1.blessRole( TestSubjectArea.ARANY_S, a2 ); // same

            a2.setRolePropertyValue( a1, TestSubjectArea.ARANY_D_FOO, newDestValue );  // DIFFERENCE: Role Property changed

            return new MeshObject[] { a1, a2 };
        });

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList changeList = diff.determineChangeList( theMeshBase2 );

        printChangeList( log, changeList );

        checkEquals( changeList.size(), 1, "Not the right number of changes" );

        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeList.applyChangeListTo( theMeshBase1 ));

        //

        ChangeList emptyChangeList = diff.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeList.size(), 0, "ChangeList not empty after applying differences" );

        if( emptyChangeList.size() > 0 ) {
            log.debug( "ChangeList is " + emptyChangeList );
        } else {
            log.debug( "ChangeList is empty" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( DifferencerTest8.class );
}
