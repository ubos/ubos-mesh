//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.history;

import java.util.NoSuchElementException;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;
import net.ubos.meshbase.history.MeshBaseStateHistory;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;

/**
 * Tests that the MeshBaseStateHistory is being created and we can traverse back and forth in it.
 * Does not test the MeshObjects contained in the history.
 *
 * See StoreMeshBaseHistoryTest1
 */
public class MeshBaseStateHistoryTest1
        extends
            AbstractMeshBaseStateHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long delta = 500; // half the time precision interval

        startClock();

        sleepUntil( delta );

        //

        log.info( "Running a few transactions" );

        theMeshBase.execute( (tx) -> {
            return tx.getMeshBase().createMeshObject( "obj-1" );
        } );

        sleepUntil( 2*delta );

        theMeshBase.execute( (tx) -> {
            return tx.getMeshBase().createMeshObject( "obj-2" );
        } );

        sleepUntil( 3*delta );

        theMeshBase.execute( (tx) -> {
            return tx.getMeshBase().createMeshObject( "obj-3" );
        } );

        sleepUntil( 4*delta );

        theMeshBase.execute( (tx) -> {
            return tx.getMeshBase().createMeshObject( "obj-4" );
        } );

        //

        log.info( "Checking history" );

        MeshBaseStateHistory history = theMeshBase.getHistory();
        checkEquals( history.getLength(), 5, "Wrong length history" );

        long [] when = new long[ history.getLength() ];
        int i=0;
        for( MeshBaseState state : history ) {
            checkEquals( state.getChangeList().size(), 1, "Wrong number of changes in history" );
            when[i] = state.getTimeUpdated();

            if( i>0 ) {
                checkCondition( when[i-1] < when[i], "Not ordered right: i=" + i + ", " + when[i-1] + " vs " + when[i] );
            }
            ++i;
        }

        //

        log.info( "Traversing around the history" );

        HistoryCursorIterator<MeshBaseState> iter = history.iterator();

        checkCondition( iter.hasNext(), "Does not have next" );
        checkCondition( !iter.hasPrevious(), "Has previous" );
        checkCondition( iter.hasNext( 4 ), "Does not have 4 next" );
        checkCondition( !iter.hasNext( 100 ), "Has 100 next" );

        MeshBaseState first = iter.peekNext();
        checkEquals( first.getTimeUpdated(), when[0], "First state wrong" );

        try {
            iter.peekPrevious();
            reportError( "Should have thrown exception" );

        } catch( NoSuchElementException ex ) {
            // correct
        }

        MeshBaseState current = iter.next();
        checkEquals( current, first, "Not same" );
        checkCondition( iter.hasNext(), "Does not have next" );
        checkCondition( iter.hasPrevious(), "Does not have previous" );

        MeshBaseState second = iter.peekNext();
        checkEquals( second.getTimeUpdated(), when[1], "Second state wrong" );

        current = iter.peekPrevious();
        checkEquals( current, first, "Not same" );

        HistoryCursorIterator<MeshBaseState> iter2 = iter.createCopy();

        current = iter.peekPrevious();
        checkEquals( current, first, "Not same" );

        checkEquals( iter.next( 2 ).size(), 2, "Wrong length" );
        iter2.next();
        iter2.next();

        checkEquals( iter.peekNext(), iter2.peekNext(), "Arrived in different place" );

        iter2.moveToAfterLast();
        checkCondition( !iter2.hasNext(), "Has next" );
        checkCondition( iter2.hasPrevious(), "Does not have previous" );

        iter.moveToJustBeforeTime( when[2] );
        checkCondition( iter.hasNext(), "Does not have next" );
        checkCondition( iter.hasPrevious(), "Does not have previous" );
        checkCondition( iter.hasPrevious( 2 ), "Does not have previous 2" );
        checkCondition( !iter.hasPrevious( 3 ), "Has previous 3" );

        iter.moveToJustAfterTime( when[2] );
        checkCondition( iter.hasNext(), "Does not have next" );
        checkCondition( iter.hasPrevious(), "Does not have previous" );
        checkCondition( !iter.hasPrevious( 4 ), "Has previous 4" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(MeshBaseStateHistoryTest1.class);
}
