//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that an unrelated modification in a MeshObject's history continues to be carried through future versions
 * of that MeshObject.
 */
public class EditableHistoryTest7
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT1_NAME = "att1";
        final String ATT2_NAME = "att2";
        final long   delta     = 200L;

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        log.info( "Setting up t0, t2, t3" );

        long startTime = startClock();

        theMeshBase.execute(
                () -> {
                    MeshObject a = theMeshBase.createMeshObject( A );
                    a.setAttributeValue( ATT1_NAME, "t0" );
                });

        sleepUntil( 2*delta );

        theMeshBase.execute(
                () -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    a.setAttributeValue( ATT1_NAME, "t2" );
                });

        sleepUntil( 3*delta );

        theMeshBase.execute(
                () -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    a.setAttributeValue( ATT1_NAME, "t3" );
                });

        //

        log.info( "Patching in t1" );

        theMeshBase.executeAt(
                startTime + delta,
                () -> {
                    MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
                    a.setAttributeValue( ATT2_NAME, "t1" );
                });

        //

        MeshObject        aHead    = theMeshBase.findMeshObjectByIdentifier( A );
        MeshObjectHistory aHistory = theMeshBase.meshObjectHistory( A );
        checkEquals( aHistory.getLength(), 4, "Wrong length of history" );

        HistoryCursorIterator<MeshObject> aHistoryIter = aHistory.iterator();
        aHistoryIter.moveToBeforeFirst();
        MeshObject at0 = aHistoryIter.next();
        MeshObject at1 = aHistoryIter.next();
        MeshObject at2 = aHistoryIter.next();
        MeshObject at3 = aHistoryIter.next();

        checkEquals( at0.getTimeCreated(), at0.getTimeUpdated(), "Not created and updated at the same time t0" );

        checkCondition( at0.getTimeUpdated() < at1.getTimeUpdated(), "Wrong sequence: at0 vs at1" );
        checkCondition( at1.getTimeUpdated() < at2.getTimeUpdated(), "Wrong sequence: at1 vs at2" );
        checkCondition( at2.getTimeUpdated() < at3.getTimeUpdated(), "Wrong sequence: at2 vs at3" );

        checkEquals( at0.getAttributeValue( ATT1_NAME ), "t0", "Wrong ATT1 at t0" );
        checkEquals( at1.getAttributeValue( ATT1_NAME ), "t0", "Wrong ATT1 at t1" );
        checkEquals( at2.getAttributeValue( ATT1_NAME ), "t2", "Wrong ATT1 at t2" );
        checkEquals( at3.getAttributeValue( ATT1_NAME ), "t3", "Wrong ATT1 at t3" );
        checkEquals( aHead.getAttributeValue( ATT1_NAME ), "t3", "Wrong ATT1 at HEAD" );

        checkCondition( !at0.hasAttribute(  ATT2_NAME ), "t0", "Has ATT2 at t0" );
        checkEquals( at1.getAttributeValue( ATT2_NAME ), "t1", "Wrong ATT2 at t1" );
        checkEquals( at2.getAttributeValue( ATT2_NAME ), "t1", "Wrong ATT2 at t2" );
        checkEquals( at3.getAttributeValue( ATT2_NAME ), "t1", "Wrong ATT2 at t3" );
        checkEquals( aHead.getAttributeValue( ATT2_NAME ), "t1", "Wrong ATT2 at HEAD" );

        checkStockFlowIntegrity( "end", null );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest7.class );
}
