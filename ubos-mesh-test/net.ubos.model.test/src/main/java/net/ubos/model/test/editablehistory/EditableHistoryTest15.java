//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Test that an existing MeshObject can be created earlier in the history after the fact.
 * FIXME: For now, we don't actually support this, so we expect an Exception
 */
@Disabled
public class EditableHistoryTest15
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String ATT   = "att";
        final long   delta = 200L;

        //

        Map<Long,String> captureTs = new HashMap<>();

        checkNotObject( theMeshBase.findMeshObjectByIdentifier( A ), "a is there already" );
        sleepFor( 1 );

        //

        long startTime = startClock();

        sleepUntil( 2*delta );

        //

        log.info( "Creating at t2" );

        MeshObject a2 = theMeshBase.execute(
               (tx) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT, "t2" );
                    return ret;
                });
        captureTs.put( a2.getTimeUpdated(), "t2" );
        checkStockFlowIntegrity( "t2 a", captureTs );

        //

        log.info( "Attempting to create at t1" );

        try {
            MeshObject a1 = theMeshBase.executeAt(
                    startTime + delta,
                    (tx) -> {
                        MeshObject ret = theMeshBase.createMeshObject( A );
                        ret.setAttributeValue( ATT, "t1" );
                        return ret;
                    });
            captureTs.put( a1.getTimeUpdated(), "t1" );

            reportError( "Transaction not thrown" );

        } catch( TransactionActionException.Error ex ) {
            checkCondition( ex.getCause() instanceof MeshObjectIdentifierNotUniqueException, "Wrong type of exception thrown" );
        }
        checkStockFlowIntegrity( "t2 b", captureTs );

        //

        log.info( "Checking history" );

        checkEquals( theMeshBase.getHistory().getLength(), 2, "Wrong length of MeshBaseHistory" );

        MeshObjectHistory aHistory = theMeshBase.meshObjectHistory( A );
        checkEquals( aHistory.getLength(), 1, "Wrong length of history" );

        MeshObject aHead = theMeshBase.findMeshObjectByIdentifier( A );
        checkObject( aHead, "a not found in HEAD" );
        checkEquals( aHead.getAttributeValue( ATT ), "t2", "wrong value" );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest15.class );
}
