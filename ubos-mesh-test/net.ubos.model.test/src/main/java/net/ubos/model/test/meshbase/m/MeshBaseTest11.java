//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * <p>Tests the "common neighbors" functionality.
 * <pre>
 * obj1 - obj2 - objCenter - obj4 - obj3
 *                  |
 *                obj8
 *                  |
 *                obj7 - obj9
 *                  |
 *                obj6
 *                  |
 *                obj5
 * </pre>
 */
public class MeshBaseTest11
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Create a few MeshObjects" );

        MeshObject [] created = theMeshBase.execute( (tx) -> {
            MeshObject obj1 = theMeshBase.createMeshObject( "obj1" );
            MeshObject obj2 = theMeshBase.createMeshObject( "obj2" );
            MeshObject obj3 = theMeshBase.createMeshObject( "obj3" );
            MeshObject obj4 = theMeshBase.createMeshObject( "obj4" );
            MeshObject obj5 = theMeshBase.createMeshObject( "obj5" );
            MeshObject obj6 = theMeshBase.createMeshObject( "obj6" );
            MeshObject obj7 = theMeshBase.createMeshObject( "obj7" );
            MeshObject obj8 = theMeshBase.createMeshObject( "obj8" );
            MeshObject obj9 = theMeshBase.createMeshObject( "obj9" );
            MeshObject objCenter = theMeshBase.createMeshObject( "objCenter" );

            obj1.setRoleAttributeValue( obj2,      "obj1", "obj2" );
            obj2.setRoleAttributeValue( objCenter, "obj2", "objCenter" );

            obj3.setRoleAttributeValue( obj4,      "obj3", "obj4" );
            obj4.setRoleAttributeValue( objCenter, "obj4", "objCenter" );

            obj5.setRoleAttributeValue( obj6,      "obj5", "obj6" );
            obj6.setRoleAttributeValue( obj7,      "obj6", "obj7" );
            obj7.setRoleAttributeValue( obj8,      "obj7", "obj8" );
            obj8.setRoleAttributeValue( objCenter, "obj8", "objCenter" );

            obj9.setRoleAttributeValue( obj7,      "obj9", "obj7" );

            return new MeshObject[] {
                obj1,
                obj2,
                obj3,
                obj4,
                obj5,
                obj6,
                obj7,
                obj8,
                obj9,
                objCenter
            } ;
        });

        MeshObject obj1 = created[0];
        MeshObject obj2 = created[1];
        MeshObject obj3 = created[2];
        MeshObject obj4 = created[3];
        MeshObject obj5 = created[4];
        MeshObject obj6 = created[5];
        MeshObject obj7 = created[6];
        MeshObject obj8 = created[7];
        MeshObject obj9 = created[8];
        MeshObject objCenter = created[9];

        //

        log.info( "Checking we got it right" );

        checkEquals( obj1.traverseToNeighbors().size(), 1, "obj1 neighbors are wrong" );
        checkEquals( obj2.traverseToNeighbors().size(), 2, "obj2 neighbors are wrong" );
        checkEquals( obj3.traverseToNeighbors().size(), 1, "obj3 neighbors are wrong" );
        checkEquals( obj4.traverseToNeighbors().size(), 2, "obj4 neighbors are wrong" );
        checkEquals( obj5.traverseToNeighbors().size(), 1, "obj5 neighbors are wrong" );
        checkEquals( obj6.traverseToNeighbors().size(), 2, "obj6 neighbors are wrong" );
        checkEquals( obj7.traverseToNeighbors().size(), 3, "obj7 neighbors are wrong" );
        checkEquals( obj8.traverseToNeighbors().size(), 2, "obj8 neighbors are wrong" );
        checkEquals( obj9.traverseToNeighbors().size(), 1, "obj9 neighbors are wrong" );
        checkEquals( objCenter.traverseToNeighbors().size(), 3, "objCenter neighbors are wrong" );

        //

        MeshObjectSet set12 = theMeshBase.findCommonNeighbors( obj1, obj2 );
        checkEquals( set12.size(), 0, "Wrong size of set12" );

        MeshObjectSet set2 = theMeshBase.findCommonNeighbors( obj1, objCenter );
        checkEquals( set2.size(), 1, "Wrong size of set2" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        set2.getMeshObjects(),
                        new MeshObject[] { obj2 },
                        false ),
                "set2 has wrong content" );

        MeshObjectSet set34 = theMeshBase.findCommonNeighbors( obj3, obj4 );
        checkEquals( set34.size(), 0, "Wrong size of set34" );

        MeshObjectSet set4 = theMeshBase.findCommonNeighbors( obj3, objCenter );
        checkEquals( set4.size(), 1, "Wrong size of set4" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        set4.getMeshObjects(),
                        new MeshObject[] { obj4 },
                        false ),
                "set4 has wrong content" );

        MeshObjectSet setCenterA = theMeshBase.findCommonNeighbors( obj2, obj4 );
        checkEquals( setCenterA.size(), 1, "Wrong size of setCenterA" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        setCenterA.getMeshObjects(),
                        new MeshObject[] { objCenter },
                        false ),
                "setCenterA has wrong content" );

        MeshObjectSet setCenterB = theMeshBase.findCommonNeighbors( new MeshObject[] { obj2, obj4, obj8 } );
        checkEquals( setCenterB.size(), 1, "Wrong size of setCenterB" );
        checkCondition(
                ArrayHelper.hasSameContentOutOfOrder(
                        setCenterB.getMeshObjects(),
                        new MeshObject[] { objCenter },
                        false ),
                "setCenterB has wrong content" );

        MeshObjectSet setCenterC = theMeshBase.findCommonNeighbors( new MeshObject[] { obj2, obj4, obj7 } );
        checkEquals( setCenterC.size(), 0, "Wrong size of setCenterC" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest11.class );
}
