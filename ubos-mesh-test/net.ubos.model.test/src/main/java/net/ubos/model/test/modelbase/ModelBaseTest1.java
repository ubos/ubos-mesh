//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.modelbase;

import java.util.Iterator;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.MeshTypeWithProperties;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.ModelBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the MMeshBase's ability to store and retrive MeshTypes.
 */
public class ModelBaseTest1
        extends
            AbstractModelBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Loading Subject Areas" );

        populateModelBase();

        //

        log.info( "Iterating over content, and trying to find all those MeshTypes" );

        Iterator<MeshType> theIter = ModelBase.SINGLETON.iterator();

        while( theIter.hasNext() ) {
            MeshType           current = theIter.next();
            MeshTypeIdentifier id      = current.getIdentifier();

            log.debug( "Looking at MeshType " + id );

            //

            if( current instanceof EntityType ) {

                EntityType  realCurrent = (EntityType) current;
                SubjectArea sa          = realCurrent.getSubjectArea();

                MeshType other = ModelBase.SINGLETON.findMeshType( id );
                checkIdentity( current, other, "Look-up by Identifier failed for EntityType" );

                SubjectArea otherSa = ModelBase.SINGLETON.findSubjectAreaOrNull( sa.getName().value());
                checkIdentity( sa, otherSa, "Look-up by name failed for SubjectArea" );

                other = ModelBase.SINGLETON.findEntityType( otherSa, realCurrent.getName().value() );
                checkIdentity( current, other, "Look-up by name failed for EntityType" );

                other = ModelBase.SINGLETON.findEntityType(
                        sa.getName().value(),
                        realCurrent.getName().value() );
                checkIdentity( current, other, "Look-up direct failed for EntityType" );

            } else if( current instanceof RelationshipType ) {

                RelationshipType realCurrent = (RelationshipType) current;
                SubjectArea      sa          = realCurrent.getSubjectArea();

                MeshType other = ModelBase.SINGLETON.findMeshType( id );
                checkIdentity( current, other, "Look-up by Identifier failed for RelationshipType" );

                SubjectArea otherSa = ModelBase.SINGLETON.findSubjectAreaOrNull( sa.getName().value() );
                checkIdentity( sa, otherSa, "Look-up by name failed for SubjectArea" );

                other = ModelBase.SINGLETON.findRelationshipType( otherSa, realCurrent.getName().value() );
                checkIdentity( current, other, "Look-up by name failed for RelationshipType" );

                other = ModelBase.SINGLETON.findRelationshipType(
                        sa.getName().value(),
                        realCurrent.getName().value() );
                checkIdentity( current, other, "Look-up direct failed for RelationshipType" );

            } else if( current instanceof RoleType ) {

                RoleType    realCurrent = (RoleType) current;
                SubjectArea sa          = realCurrent.getSubjectArea();

                MeshType other = ModelBase.SINGLETON.findMeshType( id );
                checkIdentity( current, other, "Look-up by Identifier failed for RoleType" );

                SubjectArea otherSa = ModelBase.SINGLETON.findSubjectAreaOrNull( sa.getName().value() );
                checkIdentity( sa, otherSa, "Look-up by name failed for SubjectArea" );

                other = ModelBase.SINGLETON.findRoleType( otherSa, realCurrent.getName().value() );
                checkIdentity( current, other, "Look-up by name failed for RoleType" );

                other = ModelBase.SINGLETON.findRoleType(
                        sa.getName().value(),
                        realCurrent.getName().value() );
                checkIdentity( current, other, "Look-up direct failed for RoleType" );

            } else if( current instanceof PropertyType ) {

                PropertyType           realCurrent = (PropertyType) current;
                SubjectArea            sa          = realCurrent.getSubjectArea();
                MeshTypeWithProperties amo         = realCurrent.getMeshTypeWithProperties();
                SubjectArea            amoSa       = amo.getSubjectArea();

                MeshType other = ModelBase.SINGLETON.findMeshType( id );
                checkIdentity( current, other, "Look-up by Identifier failed for PropertyType" );

                SubjectArea otherSa = ModelBase.SINGLETON.findSubjectAreaOrNull( sa.getName().value() );
                checkIdentity( sa, otherSa, "SubjectAreas not the same" );
                checkIdentity( sa, amoSa,   "SubjectAreas not the same" );

                MeshTypeWithProperties otherAmo;
                if( amo instanceof EntityType ) {
                    otherAmo = ModelBase.SINGLETON.findEntityType( sa, amo.getName().value() );
                } else if( amo instanceof RoleType ) {
                    otherAmo = ModelBase.SINGLETON.findRoleType( sa, amo.getName().value() );
                } else {
                    throw new UnsupportedOperationException( "FIXME" );
                }
                checkIdentity( amo, otherAmo, "MeshTypeWithProperties not the same" );

                other = ModelBase.SINGLETON.findPropertyType( amo, realCurrent.getName().value() );
                checkIdentity( current, other, "Look-up by name failed for PropertyType" );

            } else if( current instanceof SubjectArea ) {

                SubjectArea realCurrent = (SubjectArea) current;

                SubjectArea otherSa = ModelBase.SINGLETON.findSubjectAreaOrNull( realCurrent.getName().value() );
                checkIdentity( realCurrent, otherSa, "Look-up direct failed for SubjectArea" );

            } else {

                log.error( "what is this: " + current );
            }
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ModelBaseTest1.class);
}
