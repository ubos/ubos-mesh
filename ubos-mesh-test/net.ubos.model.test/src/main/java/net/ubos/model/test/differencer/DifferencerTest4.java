//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import net.ubos.util.logging.Log;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.ChangeList;
import org.junit.jupiter.api.Test;

/**
 * Creates related MeshObjects, changes relationship graph and makes sure appropriate
 * MeshObjectNeighborChangeEvents are created.
 */
public class DifferencerTest4
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String [] ids = new String[] {
            "aaaa0",
            "aaaa1",
            "aaaa2",
            "aaaa3"
        };

        log.info( "Creating MeshObjects in MeshBase1" );

        MeshObject [] mb1Objs = theMeshBase1.execute( (tx) -> {
            MeshObject [] ret = new MeshObject[ ids.length ];
            for( int i=0 ; i<ids.length ; ++i ) {
                ret[i] = theMeshBase1.createMeshObject( ids[i] );
            }

            ret[0].setRoleAttributeValue( ret[1], "0->1", true );
            ret[0].setRoleAttributeValue( ret[2], "0->2", true );

            return ret;
        });

        //

        log.info( "Creating MeshObjects in MeshBase2" );

        MeshObject [] mb2Objs =  theMeshBase2.execute( (tx) -> {
            MeshObject [] ret = new MeshObject[ ids.length ];
            for( int i=0 ; i<ids.length ; ++i ) {
                ret[i] = theMeshBase2.createMeshObject( ids[i] );
            }

            ret[0].setRoleAttributeValue( ret[1], "0->1", true );
            ret[0].setRoleAttributeValue( ret[3], "0->3", true );

            return ret;
        });

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diffA = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList changeListA = diffA.determineChangeList( theMeshBase2 );

        printChangeList( log, changeListA );

        checkEquals( changeListA.size(), 4, "Not the right number of changes" ); // add, remove, and 2 changes

        //

        log.info( "now differencing in the reverse direction" );

        MeshBaseDifferencer diffB = MeshBaseDifferencer.create( theMeshBase2 );

        ChangeList changeListB = diffB.determineChangeList( theMeshBase1 );

        printChangeList( log, changeListB );

        checkEquals( changeListB.size(), 4, "Not the right number of changes" ); // add, remove, and 2 changes

        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeListA.applyChangeListTo( theMeshBase1 ));

        //

        ChangeList emptyChangeListA = diffA.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeListA.size(), 0, "ChangeList not empty after applying differences" );

        if( emptyChangeListA.size() > 0 ) {
            log.debug( "ChangeList is " + emptyChangeListA );
        } else {
            log.debug( "ChangeList is empty" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( DifferencerTest4.class );
}
