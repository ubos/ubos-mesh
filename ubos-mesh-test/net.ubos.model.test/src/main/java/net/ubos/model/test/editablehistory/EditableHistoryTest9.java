//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.meshbase.history.HistoricMeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Create objects at multiple places in the past.
 */
public class EditableHistoryTest9
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long delta = 200L;

        Map<Long,String> captureTs = new HashMap<>();
        captureTs.put( theMeshBase.getHomeObject().getTimeUpdated(), "t_home" );

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        log.info( "Setting up t0, t2, t3" );

        long startTime = startClock();

        long t0 = theMeshBase.execute(
                (tx) -> {
                    return theMeshBase.createMeshObject( A );
                }).getTimeUpdated();
        captureTs.put( t0, "t0" );

        sleepUntil( 3*delta );

        long t3 = theMeshBase.execute(
                (tx) -> {
                    return theMeshBase.createMeshObject( D );
                }).getTimeUpdated();
        captureTs.put( t3, "t3" );

        //

        log.info( "Patching in t1" );

        long t1 = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    return theMeshBase.createMeshObject( B );
                }).getTimeUpdated();
        captureTs.put( t1, "t1" );

        log.info( "Patching in t2" );

        long t2 = theMeshBase.executeAt(
                startTime + 2*delta,
                (tx) -> {
                    return theMeshBase.createMeshObject( C );
                }).getTimeUpdated();
        captureTs.put( t2, "t2" );

        //

        // printHistory( "Before checking integrity", theMeshBase, captureTs );
        checkStockFlowIntegrity( "end", null );

        checkEquals( theMeshBase.size(), 5, "Wrong MeshBase size in HEAD" );

        HistoryCursorIterator<MeshBaseState> mbHistoryIter = theMeshBase.getHistory().iterator();
        mbHistoryIter.moveToBeforeFirst();
        mbHistoryIter.next(); // home
        HistoricMeshBaseView mbvAt0 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt1 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt2 = mbHistoryIter.next().getMeshBaseView();
        HistoricMeshBaseView mbvAt3 = mbHistoryIter.next().getMeshBaseView();

        checkEquals( mbvAt0.size(), 2, "Wrong MeshBaseView size at t0" );
        checkEquals( mbvAt1.size(), 3, "Wrong MeshBaseView size at t1" );
        checkEquals( mbvAt2.size(), 4, "Wrong MeshBaseView size at t2" );
        checkEquals( mbvAt3.size(), 5, "Wrong MeshBaseView size at t3" );

        checkEquals( theMeshBase.meshObjectHistory( A ).getLength(), 1, "Wrong length of history for A" );
        checkEquals( theMeshBase.meshObjectHistory( B ).getLength(), 1, "Wrong length of history for B" );
        checkEquals( theMeshBase.meshObjectHistory( C ).getLength(), 1, "Wrong length of history for C" );
        checkEquals( theMeshBase.meshObjectHistory( D ).getLength(), 1, "Wrong length of history for D" );
    }

    private static final Log log = Log.getLogInstance( EditableHistoryTest9.class );
}
