//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.modelbase;

import java.io.IOException;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.externalized.xml.XmlModelExporter;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.testharness.AbstractTest;

/**
 * Factors out common behaviors of ModelBaseTests.
 */
public abstract class AbstractModelBaseTest
        extends
            AbstractTest
{
    /**
     * Populate the ModelBase.
     *
     * @throws IOException all sorts of things may go wrong during tests
     */
    protected void populateModelBase()
        throws
            IOException
    {
        String [] theSubjectAreas = { "net.ubos.model.Test" };

        for( int i=0 ; i<theSubjectAreas.length ; ++i ) {
            ModelBase.SINGLETON.findSubjectAreaOrNull( theSubjectAreas[i] );
        }

        //

        if( getLog().isDebugEnabled() ) {
            XmlModelExporter theExporter = XmlModelExporter.create();
            theExporter.exportToXML( ModelBase.SINGLETON, System.err );
        }
    }
}
