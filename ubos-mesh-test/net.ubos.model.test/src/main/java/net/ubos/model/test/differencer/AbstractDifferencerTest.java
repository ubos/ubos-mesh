//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;

/**
 * Factors out common functionality for DifferencerTests.
 */
public abstract class AbstractDifferencerTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        thePrimaryNsMap     = MPrimaryMeshObjectIdentifierNamespaceMap.create();

        theMeshBase1 = MMeshBase.Builder.create().namespaceMap( thePrimaryNsMap ).build();
        theMeshBase2 = MMeshBase.Builder.create().namespaceMap( thePrimaryNsMap ).build();
    }

    /**
     * Helper to print the contents of a MeshBase.
     *
     * @param useThis the Log to print to
     * @param theMeshBase the MeshBase to print
     */
    protected void printMeshBase(
            Log      useThis,
            MeshBase theMeshBase )
    {
        if( useThis.isDebugEnabled() ) {
            useThis.debug( "MeshBase content: " + theMeshBase.size() + " MeshObjects" );

            int i=0;
            for( MeshObject current : theMeshBase ) {
                useThis.debug( " " + i + ": " + current );
                ++i;
            }
        }
    }

    /**
     * Helper to print a ChangeList.
     *
     * @param useThis the Log to print to
     * @param changeList the ChangeList to print
     */
    protected void printChangeList(
            Log        useThis,
            ChangeList changeList )
    {
        if( useThis.isDebugEnabled() ) {
            List<Change> theChanges = changeList.getChanges();

            useThis.debug( "found " + theChanges.size() + " changes" );

            for( int i=0 ; i<theChanges.size() ; ++i ) {
                Change current = theChanges.get( i );
                useThis.debug( " " + i + ": " + current );
            }
        }
    }

    /**
     * Clean up after the test.
     */
    @AfterEach
    public void cleanup()
    {
        theMeshBase2.die();
        theMeshBase1.die();
    }

    /**
     * Manages our namespaces.
     */
    protected PrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;

    /**
     * The first MeshBase for the test.
     */
    protected MMeshBase theMeshBase1;

    /**
     * The second MeshBase for the test.
     */
    protected MeshBase theMeshBase2;
}
