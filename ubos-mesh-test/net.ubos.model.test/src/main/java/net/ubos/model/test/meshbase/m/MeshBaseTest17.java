//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests a large number of traversals.
 */
public class MeshBaseTest17
        extends
            AbstractMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        for( int i=BOTTOM_TEST_INDEX ; i<TOP_TEST_INDEX ; ++i ) {

            collectGarbage();

            log.info( "Running test " + i );

            Runtime rt = Runtime.getRuntime();
            log.debug( "Memory: max: " + rt.maxMemory() + ", total: " + rt.totalMemory() + ", free: " + rt.freeMemory() );

            MeshBase theMeshBase = MMeshBase.Builder.create().build();

            MeshObject [] objs = create( theMeshBase, i );
            relate(   theMeshBase, objs, i );
            traverse( theMeshBase, objs, i );

            log.debug( "Memory: max: " + rt.maxMemory() + ", total: " + rt.totalMemory() + ", free: " + rt.freeMemory() );

            theMeshBase.die();
        }
    }

    protected MeshObject [] create(
            MeshBase mb,
            int      index )
        throws
            Exception
    {
        int n = 2<<index;
        MeshObject [] objs = new MeshObject[ n ];
        String PREFIX = "object-";

        //

        log.debug( "Creating " + n + " test objects" );

        long start = startClock();
        mb.execute( () -> {
            for( int i=0 ; i<n ; ++i ) {
                objs[i] = mb.createMeshObject( PREFIX + String.valueOf( i ) );
            }
        });

        long stop = getRelativeTime();

        log.info( "Took " + stop + " msec: " + ( stop * 1000L / n ) + " usec per object to create" );

        return objs;
    }

    protected void relate(
            MeshBase      mb,
            MeshObject [] objs,
            int           index )
        throws
            Exception
    {
        log.debug( "Relating " + objs.length + " test objects with " + RELATE_PREVIOUS );

        long start = startClock();

        int nRelates = mb.execute( (tx) -> {
            int ret = 0;
            for( int i=0 ; i<objs.length ; ++i ) {
                for( int j=i-RELATE_PREVIOUS ; j<i ; ++j ) {
                    if( j >= 0 ) {
                        objs[i].setRoleAttributeValue( objs[j], "rel", String.format( "%d->%d (%d)", i, j, ret ));
                        ++ret;
                    }
                }
            }
            return ret;
        } );

        long stop = getRelativeTime();

        log.info( "Took " + stop + " msec: " + ( stop * 1000L / nRelates ) + " usec per relate" );
    }

    protected void traverse(
            MeshBase      mb,
            MeshObject [] objs,
            int           index )
        throws
            Exception
    {
        log.debug( "Traversing " + objs.length + " test objects with " + NUMBER_TRAVERSALS );

        int nTraversals = 0;

        long start = startClock();
        for( int i=0 ; i<objs.length ; ++i ) {
            MeshObjectSet current = objs[i].traverseToNeighbors();
            ++nTraversals;
            for( int j=1 ; j<NUMBER_TRAVERSALS ; ++j ) {
                current = current.traverseToNeighbors();
                ++nTraversals;
            }
        }

        long stop = getRelativeTime();

        log.info( "Took " + stop + " msec: " + ( stop * 1000L / nTraversals ) + " usec per traversal" );
    }

    /**
     * Number of tests to run.
     */
    public static final int BOTTOM_TEST_INDEX = 14;
    public static final int TOP_TEST_INDEX    = 15;

    /**
     * How many previous objects to relate to.
     */
    public static final int RELATE_PREVIOUS = 4;

    /**
     * How many traversals per object.
     */
    public static final int NUMBER_TRAVERSALS = 3;


    // Our Logger
    private static Log log = Log.getLogInstance( MeshBaseTest17.class );
}
