//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import java.util.Arrays;
import net.ubos.mesh.IllegalRolePropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * More complex Role Property tests
 */
public class MeshBaseTest22
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final StringValue newSrcValue  = StringValue.create( "NewFooSrcValue" );
        final StringValue newDestValue = StringValue.create( "NewFooDestValue" );

        log.info( "Creating test objects" );

        MeshObject [] objs = theMeshBase.execute( ( Transaction tx ) -> {
            MeshObject [] ret = new MeshObject[ N ];
            for( int i=0 ; i<N ; ++i ) {
                ret[i] = theMeshBase.createMeshObject( "id-" + i, TestSubjectArea.AA );
            }
            for( int i=0 ; i<N ; ++i ) {
                ret[i].setRoleAttributeValue( ret[ ( i+1 ) % N ], "i", "i+1" );
                ret[i].setRoleAttributeValue( ret[ ( i+3 ) % N ], "i", "i+3" );
                ret[i].setRoleAttributeValue( ret[ ( i+4 ) % N ], "i", "i+4" );
            }

            return ret;
        } );

        //

        log.info( "Checking that no Role PropertyTypes exist" );

        for( int i=0 ; i<N ; ++i ) {
            for( int j=0 ; j<N ; ++j ) { // against itself, too
                for( PropertyType pt : Arrays.asList( TestSubjectArea.ARANY_S_FOO, TestSubjectArea.ARANY_D_FOO )) {
                    try {
                        Object found = objs[ i ].getRolePropertyValue( objs[ (i+j) % N ], pt );

                        reportError( "Found Role PropertyValue where there should not be: ", i, j, pt, found );

                        checkEquals( j, 0, "Not thrown: IllegalRolePropertyTypeRuntimeException" );

                    } catch( IllegalRolePropertyTypeException ex ) {
                        // no op
                    }
                }
            }
        }

        //

        log.info( "Blessing Relationship three ways around" );

        theMeshBase.execute( ( Transaction tx ) -> {
            for( int i=0 ; i<N ; ++i ) {
                // forward to next
                objs[i].blessRole( TestSubjectArea.ARANY_S, objs[ (i+1) % N ]);

                // forward to next+2
                objs[i].blessRole( TestSubjectArea.ARANY_S, objs[ (i+3) % N ]);

                // backward to next+3
                objs[i].blessRole( TestSubjectArea.ARANY_D, objs[ (i+4) % N ]);
            }

            return null;
        } );

        //

        log.info( "Checking Role PropertyTypes" );

        checkRolePropertyValues( objs, TestSubjectArea.ARANY_S_FOO.getDefaultValue(), TestSubjectArea.ARANY_D_FOO.getDefaultValue() );

        //

        log.info( "Setting PropertyValues" );

        theMeshBase.execute((tx) -> {
            for( int i=0 ; i<N ; ++i ) {
                for( int j=0 ; j<N ; ++j ) { // against itself, too
                    PropertyType [] found = objs[i].getRolePropertyTypes( objs[ (i+j) % N ] );

                    for( PropertyType current : found ) {

                        if( current == TestSubjectArea.ARANY_S_FOO ) {
                            objs[i].setRolePropertyValue( objs[ (i+j) % N ], current, newSrcValue );

                        } else if( current == TestSubjectArea.ARANY_D_FOO ) {
                            objs[i].setRolePropertyValue( objs[ (i+j) % N ], current, newDestValue );

                        } else {
                            reportError( "Unexpected PropertyType: " + current );
                        }
                    }
                }
            }
            return null;
        });

        //

        log.info( "Checking new values" );

        checkRolePropertyValues( objs, newSrcValue, newDestValue );
    }

    /**
     * Factored out checking for RolePropertyValues.
     *
     * @param objs the related MeshObjects
     * @param srcPropertyValue the correct value for the Property on the "source" Role
     * @param destPropertyValue the correct value for the Property on the "destination" Role
     */
    protected void checkRolePropertyValues(
            MeshObject [] objs,
            PropertyValue srcPropertyValue,
            PropertyValue destPropertyValue )
    {
        for( int i=0 ; i<N ; ++i ) {
            for( int j=0 ; j<N ; ++j ) { // against itself, too
                PropertyType pt;
                switch( j ) {
                    case 1:
                    case 3:
                    case N-4:
                        pt = TestSubjectArea.ARANY_S_FOO;
                        break;
                    case 4:
                    case N-1:
                    case N-3:
                        pt = TestSubjectArea.ARANY_D_FOO;
                        break;
                    default:
                        pt = null;
                }
                if( pt == TestSubjectArea.ARANY_S_FOO ) {
                    checkEquals( objs[ i ].getRolePropertyValue( objs[ (i+j) % N ], pt ), srcPropertyValue, "wrong source value" );

                } else if( pt == TestSubjectArea.ARANY_D_FOO ) {
                    checkEquals( objs[ i ].getRolePropertyValue( objs[ (i+j) % N ], pt ), destPropertyValue, "wrong destination value" );

                } else if( pt != null ) {
                    try {
                        Object found = objs[ i ].getRolePropertyValue( objs[ (i+j) % N ], pt );

                        reportError( "Found Role PropertyValue where there should not be: ", i, j, pt, found );

                    } catch( IllegalRolePropertyTypeException ex ) {
                        // correct
                    }
                }
            }
        }
    }

    /**
     * Number of test objects.
     */
    public static final int N = 20;

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest22.class );
}