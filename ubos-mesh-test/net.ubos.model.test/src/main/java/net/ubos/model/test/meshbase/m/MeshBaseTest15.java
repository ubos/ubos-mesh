//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Make sure Properties go away when their EntityType is unblessed.
 */
public class MeshBaseTest15
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Instantiating MeshObject of type AA" );

        MeshObject obj = theMeshBase.execute( (tx) -> {
            MeshObject ret = theMeshBase.createMeshObject( TestSubjectArea.AA );
            ret.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "Some value" ));
            return ret;
        } );

        PropertyType [] types = obj.getPropertyTypes();
        checkEquals( types.length, 4, "Wrong number of Properties" );

        //

        log.info( "Unblessing and checking" );

        theMeshBase.execute( (tx) -> {
            obj.unbless( TestSubjectArea.AA );
            return null;
        } );

        checkEquals( obj.getPropertyTypes().length, 0, "Wrong number of Properties" );

        //

        log.info( "Making sure that the values are gone, too" );

        for( PropertyType current : types ) {
            try {
                PropertyValue value = obj.getPropertyValue( current );

                reportError( "Accessed non-existing property successfully", current, value );

            } catch( IllegalPropertyTypeException ex ) {
                log.debug( "Correctly received exception", ex );
            }
        }
    }

    // Our Logger
    private static Log log = Log.getLogInstance( MeshBaseTest15.class);
}
