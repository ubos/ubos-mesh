//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that properties on roles generate the right events
 */
public class MeshBaseTest21
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshObjectIdentifier oneId = theMeshBase.meshObjectIdentifierFromExternalForm( "one" );
        MeshObjectIdentifier twoId = theMeshBase.meshObjectIdentifierFromExternalForm( "two" );

        log.info( "Creating test objects" );

        theMeshBase.execute( ( Transaction tx ) -> {
            theMeshBase.createMeshObject( oneId, TestSubjectArea.AA );
            theMeshBase.createMeshObject( twoId, TestSubjectArea.AA );

            return null;
        } );
        MeshObject one = theMeshBase.findMeshObjectByIdentifier( oneId );
        MeshObject two = theMeshBase.findMeshObjectByIdentifier( twoId );

        RecordingTransactionListener listener = new RecordingTransactionListener();
        theMeshBase.addDirectTransactionListener( listener );

        //

        log.info( "Relating test objects" );

        theMeshBase.execute( ( Transaction tx ) -> {
            one.setRoleAttributeValue( two, "foo", true );
            return null;
        });

        checkEquals( listener.theChangeLists.size(), 1, "Wrong number of ChangeLists" );
        checkEquals( listener.theChangeLists.get( 0 ).size(), 2, "Wrong number of changes" ); // Attribute added, Attribute changed

        listener.clear();

        //

        log.info( "Blessing relationship" );

        theMeshBase.execute( ( Transaction tx ) -> {
            one.blessRole( TestSubjectArea.ARANY_S, two );
            return null;
        });

        checkEquals( listener.theChangeLists.size(), 1, "Wrong number of ChangeLists" );
        checkEquals( listener.theChangeLists.get( 0 ).size(), 1, "Wrong number of changes" ); // RoleAdded

        listener.clear();

        //

        log.info( "Setting Role PropertyValue" );

        theMeshBase.execute( ( Transaction tx ) -> {
            one.setRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO, null );
            return null;
        });

        checkEquals( listener.theChangeLists.size(), 1, "Wrong number of ChangeLists" );
        checkEquals( listener.theChangeLists.get( 0 ).size(), 1, "Wrong number of changes" ); // RoleProperty changed

        listener.clear();

        //

        log.info( "Unblessing relationship" );

        theMeshBase.execute( ( Transaction tx ) -> {
            one.unblessRole(TestSubjectArea.ARANY_S, two );
            return null;
        } );

        checkEquals( listener.theChangeLists.size(), 1, "Wrong number of ChangeLists" );
        checkEquals( listener.theChangeLists.get( 0 ).size(), 2, "Wrong number of changes" ); // 1 RoleProperty changed, 1 RoleRemove

        listener.clear();

        //

        log.info( "Unrelating unblessed" );

        theMeshBase.execute( ( Transaction tx ) -> {
            one.unrelate( two );
            return null;
        } );

        checkEquals( listener.theChangeLists.size(), 1, "Wrong number of ChangeLists" );
        checkEquals( listener.theChangeLists.get( 0 ).size(), 2, "Wrong number of changes" ); // 1 RoleAttribute changed, RoleAttribute removed

        listener.clear();
        //

        log.info( "Re-relating test objects" );

        theMeshBase.execute( ( Transaction tx ) -> {
            one.blessRole( TestSubjectArea.ARANY.getSource(), two );
            return null;
        });

        checkEquals( listener.theChangeLists.size(), 1, "Wrong number of ChangeLists" );
        checkEquals( listener.theChangeLists.get( 0 ).size(), 1, "Wrong number of changes" ); // 1 Role added

        listener.clear();

        //

        log.info( "Unrelating with unblessing" );

        theMeshBase.execute( ( Transaction tx ) -> {
            tx.getMeshBase().deleteMeshObject( one );
            return null;
        } );

        checkEquals( listener.theChangeLists.size(), 1, "Wrong number of ChangeLists" );
        checkEquals( listener.theChangeLists.get( 0 ).size(), 3, "Wrong number of changes" ); // unblessRole, unbless, delete
                // 2 Roles removed, type removed, delete

        listener.clear();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest21.class );
}
