//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.BlessedAlreadyException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests whether we can create more than one relationship between the same
 * two MeshObjects.
 */
public class MeshBaseTest13
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Setting up objects" );

        MeshObject [] created = theMeshBase.execute( (tx) -> {
            MeshObject aa = theMeshBase.createMeshObject( TestSubjectArea.AA );
            MeshObject b  = theMeshBase.createMeshObject( TestSubjectArea.B );

            aa.blessRole( TestSubjectArea.R.getSource(), b );

            return new MeshObject[] { aa, b };
        });

        MeshObject aa = created[0];
        MeshObject b  = created[1];

        //

        log.info( "Trying to create illegal R relationship" );

        theMeshBase.execute( () -> {
            try {
                aa.blessRole( TestSubjectArea.R.getSource(), b );

                reportError( "Should have thrown exception" );
            } catch( BlessedAlreadyException ex ) {
                // noop
            }
        });

        //

        log.info( "Trying to bless with RR, should downcast" );

        theMeshBase.execute( () -> {
            aa.blessRole( TestSubjectArea.RR.getSource(), b );
        } );

        checkEquals( aa.getRoleTypes().length, 1, "wrong number of role types" );
        checkEquals( TestSubjectArea.RR.getSource(), aa.getRoleTypes()[0], "wrong role type" );

        //

        log.info( "Trying to bless instead (S)" );

        theMeshBase.execute( () -> {
            aa.blessRole( TestSubjectArea.S.getSource(), b );
        } );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest13.class );
}
