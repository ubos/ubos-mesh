//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests creation of blessed relationships.
 */
public class DifferencerTest5
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Creating MeshObjects in MeshBase1" );

        theMeshBase1.execute( (tx) -> {
            MeshObject a4_mb1 = theMeshBase1.createMeshObject( "aaaa4", TestSubjectArea.AA );
            MeshObject a5_mb1 = theMeshBase1.createMeshObject( "aaaa5", TestSubjectArea.AA );

            // no relationships

            return null;
        });

        //

        log.info( "Creating MeshObjects in MeshBase2" );

        theMeshBase2.execute( (tx) -> {
            MeshObject a4_mb2 = theMeshBase2.createMeshObject( "aaaa4", TestSubjectArea.AA );
            MeshObject a5_mb2 = theMeshBase2.createMeshObject( "aaaa5", TestSubjectArea.AA );

            a4_mb2.blessRole( TestSubjectArea.AR1A.getSource(), a5_mb2 ); // DIFFERENCE: blessed relationship

            return null;
        });

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList changeList = diff.determineChangeList( theMeshBase2 );

        printChangeList( log, changeList );

        checkEquals( changeList.size(), 1, "Not the right number of changes" );

        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeList.applyChangeListTo( theMeshBase1 ));

        //

        ChangeList emptyChangeList = diff.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeList.size(), 0, "ChangeList not empty after applying differences" );

        if( emptyChangeList.size() > 0 ) {
            log.debug( "ChangeList is " + emptyChangeList );
        } else {
            log.debug( "ChangeList is empty" );
        }
    }

    // Our Logger
    private static Log log = Log.getLogInstance( DifferencerTest5.class );
}
