//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.IllegalPropertyValueException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that we can't assign the wrong PropertyValue subtypes.
 */
public class MeshBaseTest10
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshObject home = theMeshBase.getHomeObject();

        //

        log.info( "Blessing Home Object" );

        theMeshBase.execute( () -> {
            home.bless( TestSubjectArea.OPTIONALPROPERTIES );
            home.bless( TestSubjectArea.MANDATORYPROPERTIES );
        } );

        //

        log.debug( "Checking we have all PropertyTypes" );

        checkEqualsOutOfSequence(
                home.getEntityTypes(),
                new EntityType[] { TestSubjectArea.OPTIONALPROPERTIES, TestSubjectArea.MANDATORYPROPERTIES },
                "wrong types on home object" );

        //

        PropertyValue [] testValues1 = {
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEANY_type.createBlobValue( "test1", "text/plain" ),
                BooleanValue.TRUE,
                ColorValue.create( 0x202020 ),
                CurrencyValue.parseCurrencyValue( "1.23 USD" ),
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALENUMERATEDDATATYPE_type.select( "Value3" ),
                ExtentValue.create( 1.2, 3.4 ),
                FloatValue.create( 56.78 ),
                IntegerValue.create( 99 ),
                MultiplicityValue.create( 2, 7 ),
                PointValue.create( 9.8, 7.6 ),
                StringValue.create( "some string" ),
                TimePeriodValue.create( (short) 1, (short) 2, (short) 3, (short)  4, (short)  5, (float)  6. ),
                TimeStampValue.create(  (short) 7, (short) 8, (short) 9, (short) 10, (short) 11, (float) 12. )
        };
        PropertyType [] testProperties1 = {
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEANY,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBOOLEANDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALCOLORDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALCURRENCYDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALENUMERATEDDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALEXTENTDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALFLOATDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALINTEGERDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALMULTIPLICITYDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALPOINTDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALSTRINGDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALTIMEPERIODDATATYPE,
                TestSubjectArea.OPTIONALPROPERTIES_OPTIONALTIMESTAMPDATATYPE
        };

        runWith( theMeshBase, testProperties1, testValues1 );

        //

        PropertyValue [] testValues2 = {
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYBLOBDATATYPEANY_type.createBlobValue( "test", "text/plain" ),
                BooleanValue.TRUE,
                ColorValue.create( 0x202020 ),
                CurrencyValue.parseCurrencyValue( "0.01\nEUR" ),
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYENUMERATEDDATATYPE_type.select( "Value3" ),
                ExtentValue.create( 1.2, 3.4 ),
                FloatValue.create( 56.78 ),
                IntegerValue.create( 99 ),
                MultiplicityValue.create( 2, 7 ),
                PointValue.create( 9.8, 7.6 ),
                StringValue.create( "some string" ),
                TimePeriodValue.create( (short) 1, (short) 2, (short) 3, (short)  4, (short)  5, (float)  6. ),
                TimeStampValue.create(  (short) 7, (short) 8, (short) 9, (short) 10, (short) 11, (float) 12. )
        };
        PropertyType [] testProperties2 = {
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYBLOBDATATYPEANY,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYBOOLEANDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYCOLORDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYCURRENCYDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYENUMERATEDDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYEXTENTDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYFLOATDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYINTEGERDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYMULTIPLICITYDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYPOINTDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYSTRINGDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYTIMEPERIODDATATYPE,
                TestSubjectArea.MANDATORYPROPERTIES_MANDATORYTIMESTAMPDATATYPE
        };

        runWith( theMeshBase, testProperties2, testValues2 );
    }

    /**
     * Run one test set.
     *
     * @param mb the MeshBase to use for the test
     * @param testProperties the PropertyTypes to test
     * @param testValues the PropertyValues corresponding to the PropertyTypes to test
     * @throws Exception all sorts of things may go wrong during a test.
     */
    protected void runWith(
            MeshBase         mb,
            PropertyType []  testProperties,
            PropertyValue [] testValues )
        throws
            Exception
    {
        checkEquals( testValues.length, testProperties.length, "inconsistency in test data" );

        MeshObject home = mb.getHomeObject();

        mb.execute( () -> {
            for( int i=0 ; i<testValues.length ; ++i ) {
                log.info( "Now running with offset " + i );
                for( int j=0 ; j<testValues.length ; ++j ) {
                    PropertyValue currentValue = testValues[ (j+i) % testValues.length ];
                    log.debug( "Looking at test value " + j + ": " + currentValue );
                    if( i == 0 ) {
                        // should not throw an exception
                        home.setPropertyValue( testProperties[j], currentValue );
                    } else {
                        // should throw an exception
                        try {
                            home.setPropertyValue( testProperties[j], currentValue );
                            reportError( "attempting to set unexpectedly succeeded", testProperties[i].getIdentifier(), currentValue );
                        } catch( IllegalPropertyValueException ex ) {
                            // do nothing
                            log.debug( "Good, that did not work" );
                        }
                    }
                    checkEquals( home.getPropertyValue( testProperties[i] ), testValues[i], "wrong value found" );
                }
            }
        } );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest10.class );
}
