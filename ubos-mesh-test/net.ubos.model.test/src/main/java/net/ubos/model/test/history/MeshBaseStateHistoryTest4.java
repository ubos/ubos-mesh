//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.history;

import net.ubos.mesh.MeshObject;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that MeshObjects found in a non-HEAD MeshBaseView (from a MeshBaseStateHistory) cannot
 * be modified on regular (head) Transactions.
 */
public class MeshBaseStateHistoryTest4
        extends
            AbstractMeshBaseStateHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        final long   delta = 500;
        final String ID1   = "obj-1";
        final String ID2   = "obj-2";

        startClock();

        //

        log.info( "Creating MeshObjectHistory" );

        sleepUntil( delta );

        theMeshBase.execute( () -> {
            theMeshBase.createMeshObject( ID1, TestSubjectArea.AA );
            theMeshBase.createMeshObject( ID2, TestSubjectArea.B );
        } );

        sleepUntil( delta*2 );

        theMeshBase.execute( () -> {
            MeshObject obj1 = theMeshBase.findMeshObjectByIdentifier( ID1 );
            obj1.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "modified" ));
        } );

        //

        log.info(  "Attempting modification" );

        MeshObject old1 = theMeshBase.meshObjectHistory( ID1 ).oldest();
        MeshObject old2 = theMeshBase.meshObjectHistory( ID2 ).oldest();

        checkCondition(
                theMeshBase.execute( (tx) -> {
                    try {
                        old1.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "wrong" ));
                        return Boolean.FALSE;
                    } catch( Throwable t ) {} // ok
                    return Boolean.TRUE;
                } ),
                "Could change Property" );

        checkCondition(
                theMeshBase.execute( (tx) -> {
                    try {
                        old1.bless( TestSubjectArea.B );
                        return Boolean.FALSE;
                    } catch( Throwable t ) {} // ok
                    return Boolean.TRUE;
                } ),
                "Could bless" );

        checkCondition(
                theMeshBase.execute( (tx) -> {
                    try {
                        old1.blessRole( TestSubjectArea.ARANY_S, old2 );
                        return Boolean.FALSE;
                    } catch( Throwable t ) {} // ok
                    return Boolean.TRUE;
                } ),
                "Could relate" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(MeshBaseStateHistoryTest4.class);
}
