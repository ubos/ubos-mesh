//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.IllegalPropertyTypeException;
import net.ubos.mesh.IsAbstractException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.NotBlessedException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.PropertyValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * This tests the blessing and unblessing of MeshObjects. This does not test
 * relationships between MeshObjects.
 */
public class MeshBaseTest2
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Trying to instantiate abstract MeshType" );

        theMeshBase.execute( () -> {
            try {
                MeshObject obj = theMeshBase.createMeshObject( TestSubjectArea.A );

                reportError( "Should not be able to instantiate abstract type" );
            } catch( IsAbstractException ex ) {
                // this is good
            }
        });

        //

        log.info( "Creating MeshObject" );

        MeshObject obj = theMeshBase.execute( (tx) -> {
            MeshObject ret = theMeshBase.createMeshObject( TestSubjectArea.AA );
            checkEqualsOutOfSequence( ret.getEntityTypes(), new EntityType[] { TestSubjectArea.AA }, "not blessed with the same type" );
            return ret;
        });

        checkEqualsOutOfSequence( obj.getEntityTypes(), new EntityType[] { TestSubjectArea.AA }, "not blessed with the same type" );
        checkEquals( obj.getPropertyValue( TestSubjectArea.A_X  ), null,                       "Property X not initialized to null" );
        checkEquals( obj.getPropertyValue( TestSubjectArea.AA_Y ), FloatValue.create( 12.34 ), "Property Y not initialized to correct value" );

        //

        log.info( "Blessing MeshObject" );

        theMeshBase.execute( () -> {
            obj.bless( TestSubjectArea.B );
            checkEqualsOutOfSequence( obj.getEntityTypes(), new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B }, "not blessed with the same type" );
        });


        checkEqualsOutOfSequence( obj.getEntityTypes(), new EntityType[] { TestSubjectArea.AA, TestSubjectArea.B }, "not blessed with the same type" );
        checkEquals( obj.getPropertyValue( TestSubjectArea.A_X  ), null, "Property X wrong" );
        checkEquals( obj.getPropertyValue( TestSubjectArea.AA_Y ), FloatValue.create( 12.34 ), "Property Y wrong" );
        checkEquals( obj.getPropertyValue( TestSubjectArea.B_Z  ), TestSubjectArea.B_Z_type.select( "Value2" ), "Property Z not initialized to correct value" );
        checkEquals( obj.getPropertyValue( TestSubjectArea.B_U  ), null, "Property U not initialized to null" );

        //

        log.info( "Unblessing MeshObject" );

        theMeshBase.execute( () -> {
            obj.unbless( TestSubjectArea.AA );
            checkEqualsOutOfSequence( obj.getEntityTypes(), new EntityType[] { TestSubjectArea.B }, "not blessed with the same type" );
        } );


        checkEqualsOutOfSequence( obj.getEntityTypes(), new EntityType[] { TestSubjectArea.B }, "not blessed with the same type" );

        //

        log.info( "Making sure we cannot unbless what isn't blessed" );

        theMeshBase.execute( () -> {
            try {
                obj.unbless( TestSubjectArea.AA );
                reportError( "Unblessing unblessed object should not have worked" );

            } catch( NotBlessedException ex ) {
                // good
            }
        });

        //

        log.info( "Making sure we cannot access properties of the unblessed type" );

        try {
            PropertyValue v = obj.getPropertyValue( TestSubjectArea.AA_Y );
            reportError( "Accessing property of a non-blessed object should throw exception" );

        } catch( IllegalPropertyTypeException ex ) {
            // good
        }

        theMeshBase.execute( () -> {
            try {
                obj.setPropertyValue( TestSubjectArea.A_X, StringValue.create( "wrong value" ) );
                reportError( "Accessing property of a non-blessed object should throw exception" );

            } catch( IllegalPropertyTypeException ex ) {
                // good
            }
        });
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest2.class);
}
