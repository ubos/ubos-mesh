//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import java.util.ArrayList;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.HeadTransaction;
import net.ubos.meshbase.transaction.TransactionListener;

/**
 * Helper class to record ChangeLists in commits of Transactions.
 */
public class RecordingTransactionListener
    implements
        TransactionListener
{
    /**
     * {@inheritDoc}
     */
    @Override
    public void transactionCommitted(
            HeadTransaction tx )
    {
        theChangeLists.add( tx.getChangeList() );
    }

    /**
     * Clear the recording.
     */
    public void clear()
    {
        theChangeLists.clear();
    }

    /**
     * The recorded ChangeLists, in order.
     */
    public ArrayList<ChangeList> theChangeLists = new ArrayList<>();
}
