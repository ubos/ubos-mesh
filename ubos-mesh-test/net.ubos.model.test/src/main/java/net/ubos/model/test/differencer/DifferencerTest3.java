//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.differencer;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Creates unrelated MeshObjects with TestSubjectArea.s and makes sure MeshObjectTestSubjectArea.ChangeEvents
 * are created.
 */
public class DifferencerTest3
        extends
            AbstractDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );


        //

        log.info( "Creating MeshObjects in MeshBase1" );

        theMeshBase1.execute( (tx) -> {
            MeshObject a1_mb1 = theMeshBase1.createMeshObject( "aaaa1", TestSubjectArea.AA );
            a1_mb1.bless( TestSubjectArea.B );
            a1_mb1.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 444.44 ));
            a1_mb1.setPropertyValue( TestSubjectArea.B_U, StringValue.create( "initial") );

            MeshObject a5_mb1 = theMeshBase1.createMeshObject( "aaaa5", TestSubjectArea.AA );

            return null;
        } );

        printMeshBase( log, theMeshBase1 );

        //

        log.info( "Creating MeshObjects in MeshBase2" );

        theMeshBase2.execute( (tx) -> {
            MeshObject a1_mb2 = theMeshBase2.createMeshObject( "aaaa1" );

            MeshObject a5_mb2 = theMeshBase2.createMeshObject( "aaaa5", TestSubjectArea.AA );
            a5_mb2.setPropertyValue(TestSubjectArea.A_X, StringValue.create( "new value" ));
            a5_mb2.bless( TestSubjectArea.C );

            return null;
        });

        printMeshBase( log, theMeshBase2 );

        //

        log.info( "now differencing" );

        MeshBaseDifferencer diff = MeshBaseDifferencer.create( theMeshBase1 );

        ChangeList changeList = diff.determineChangeList( theMeshBase2 );

        printChangeList( log, changeList );

        checkEquals( changeList.size(), 5, "Not the right number of changes" ); // type add, type remove, and 3 property changes

        //

        log.info( "now applying changes" );

        theMeshBase1.execute( () -> changeList.applyChangeListTo( theMeshBase1 ));

        //

        ChangeList emptyChangeList = diff.determineChangeList( theMeshBase2 );

        checkEquals( emptyChangeList.size(), 0, "ChangeList not empty after applying differences" );

        if( emptyChangeList.size() > 0 ) {
            log.debug( "ChangeList is " + emptyChangeList );
        } else {
            log.debug( "ChangeList is empty" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( DifferencerTest3.class );
}
