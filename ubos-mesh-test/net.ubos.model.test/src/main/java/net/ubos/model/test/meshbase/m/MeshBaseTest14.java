//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.BlessedAlreadyException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the blessing of MeshObjects, unblessing, and reblessing. This does not test
 * relationships between MeshObjects.
 */
public class MeshBaseTest14
        extends
            AbstractSingleMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Instantiating MeshObject" );

        MeshObject obj = theMeshBase.execute( (tx) -> {
            return theMeshBase.createMeshObject();
        });

        //

        log.info( "Blessing with C" );

        theMeshBase.execute( (tx) -> {
            obj.bless( TestSubjectArea.C );
            return null;
        });

        //

        log.info( "Making sure re-blessing doesn't work" );

        try {
            theMeshBase.execute( (tx) -> {
                obj.bless( TestSubjectArea.C );

                reportError( "Reblessing with C worked but should not" );

                return null;
            });

        } catch( TransactionActionException.Error ex ) {

            log.debug( "Correctly received exception", ex );
            checkCondition( ex.getCause() instanceof BlessedAlreadyException, "Wrong exception received" );
        }

        //

        log.info( "Testing down-blessing to D" );

        theMeshBase.execute( (tx) -> {
            obj.bless( TestSubjectArea.D );
            return null;
        });

        //

        log.info( "Checking that it's only D now, not C" );

        super.checkEqualsOutOfSequence( obj.getEntityTypes(), new EntityType[] { TestSubjectArea.D }, "Wrong types" );

        //

        log.info( "Making sure up-blessing doesn't work" );

        try {
            theMeshBase.execute( (tx) -> {
                obj.bless( TestSubjectArea.C );

                reportError( "Up-blessing with C worked but should not" );

                return null;
            });

        } catch( TransactionActionException.Error ex ) {

            log.debug( "Correctly received exception", ex );
            checkCondition( ex.getCause() instanceof BlessedAlreadyException, "Wrong exception received" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshBaseTest14.class);
}
