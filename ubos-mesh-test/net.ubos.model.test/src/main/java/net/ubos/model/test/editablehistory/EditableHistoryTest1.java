//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//
package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that within a HistoryTransaction, MeshObject lookup produces
 * the right version of the MeshObject. So there is no actual "edit" in this test yet.
 */
public class EditableHistoryTest1
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long delta = 500; // half the time precision interval
        final long eps   = 50;

        final long [] TIMESTAMPS = {
            delta,
            2*delta,
            3*delta,
            4*delta,
            5*delta
        };

        final long [] CHECK_AT = {
            delta,
            4*delta,
            2*delta
        };

        final MeshObjectIdentifier A = theMeshBase.createMeshObjectIdentifier( "a" );
        final String ATT_NAME = "att";

        //
        Map<Long,String> captureTs = new HashMap<>();

        long startTime = startClock();

        sleepUntil( TIMESTAMPS[0] );

        MeshObject a = theMeshBase.execute(
                (Transaction tx ) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT_NAME, TIMESTAMPS[0] );
                    return ret;
                });
        captureTs.put( a.getTimeUpdated(), "t0" );
        checkStockFlowIntegrity( "t0", captureTs );

        for( int i=1 ; i<TIMESTAMPS.length ; ++i ) {
            long when = TIMESTAMPS[i];

            sleepUntil( when );

            theMeshBase.execute(
                    () -> a.setAttributeValue( ATT_NAME, when ) );

            captureTs.put( a.getTimeUpdated(), "t" + i );
            checkStockFlowIntegrity( "t" + i, captureTs );
        }

        // printHistory( "After setting", theMeshBase );

        //

        log.info( "Checking at the time of transactions" );

        for( int i=0 ; i<CHECK_AT.length ; ++i ) {
            long when      = startTime + CHECK_AT[i] + eps;
            long whenValue = CHECK_AT[i];

            MeshObject aFoundInMeshObjectHistory = theMeshBase.meshObjectHistory( A ).atOrBefore( when );
            checkEquals( aFoundInMeshObjectHistory.getAttributeValue( ATT_NAME ), whenValue, "Wrong attribute value (found in MeshObjectHistory)" );

            MeshObject aFoundInMeshBaseStateHistory = theMeshBase.getHistory().atOrBefore( when ).getMeshBaseView().findMeshObjectByIdentifier( A );
            checkEquals( aFoundInMeshBaseStateHistory.getAttributeValue( ATT_NAME ), whenValue, "Wrong attribute value (found in MeshBaseStateHistory)" );

            long realWhen = aFoundInMeshObjectHistory.getTimeUpdated();

            MeshObject aFoundInTx = theMeshBase.executeAt(
                    realWhen,
                    (tx) -> {
                        return theMeshBase.findMeshObjectByIdentifier( A );
                    } );
            checkEquals( aFoundInTx.getAttributeValue( ATT_NAME ), whenValue, "Wrong attribute value (found in HistoryTransaction)" );

            checkIdentity( aFoundInMeshObjectHistory, aFoundInMeshBaseStateHistory, "Different in MeshObjectHistory and MeshBaseStateHistory" );
            checkIdentity( aFoundInMeshObjectHistory, aFoundInTx,                   "Different in MeshObjectHistory and HistoryTransaction" );
        }

        //

        log.info( "Checking after transactions" );

        for( int i=0 ; i<CHECK_AT.length ; ++i ) {
            long when      = startTime + CHECK_AT[i] + eps;
            long whenValue = CHECK_AT[i];

//            System.err.printf( "Checking at %d (%d)\n", when, whenValue );

            MeshObject aFoundInMeshObjectHistory = theMeshBase.meshObjectHistory( A ).atOrBefore( when );
            long realWhen = aFoundInMeshObjectHistory.getTimeUpdated();

            MeshObject aFoundInTx = theMeshBase.executeAt(
                    realWhen + eps,
                    (tx) -> {
                        return theMeshBase.findMeshObjectByIdentifier( A );
                    } );
            checkEquals( aFoundInTx.getAttributeValue( ATT_NAME ), whenValue, "Wrong attribute value (found in HistoryTransaction)" );

            checkNotIdentity( aFoundInMeshObjectHistory, aFoundInTx, "Not different instances in MeshObjectHistory and HistoryTransaction" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( EditableHistoryTest1.class );
}
