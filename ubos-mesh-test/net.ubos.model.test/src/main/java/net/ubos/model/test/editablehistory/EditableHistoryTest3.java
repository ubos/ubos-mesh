//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//
package net.ubos.model.test.editablehistory;

import java.util.HashMap;
import java.util.Map;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that an otherwise unrelated MeshObject can be created in the past.
 */
public class EditableHistoryTest3
    extends
        AbstractEditableHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        final String ATT_NAME = "att";
        final long   delta    = 200L;

        Map<Long,String> captureTs = new HashMap<>();

        //

        MeshObject home = theMeshBase.getHomeObject();
        captureTs.put( home.getTimeUpdated(), "t_home" );

        sleepFor( 1 ); // avoid getting the first Transaction into the same millisecond as the home object

        //

        long startTime = startClock();

        log.info( "Creating a" );

        MeshObject a = theMeshBase.execute(
                (Transaction tx ) -> {
                    MeshObject ret = theMeshBase.createMeshObject( A );
                    ret.setAttributeValue( ATT_NAME, 0 );
                    return ret;
                });
        captureTs.put( a.getTimeUpdated(), "t0" );
        checkStockFlowIntegrity( "t0", captureTs );

        //

        log.info( "Updating a" );

        sleepUntil( delta * 2 );

        theMeshBase.execute( () -> a.setAttributeValue( ATT_NAME, 2 ) );
        captureTs.put( a.getTimeUpdated(), "t2" );

        checkEquals( theMeshBase.getHistory().getLength(), 3, "Wrong length of history" );
        checkEquals( theMeshBase.size(), 2, "Wrong MeshBase size" );

        //

        log.info( "Creating b" );

        MeshObject b = theMeshBase.executeAt(
                startTime + delta,
                (tx) -> {
                    MeshObject ret = theMeshBase.createMeshObject( B );
                    ret.setAttributeValue( ATT_NAME, 1 );
                    return ret;
                });
        checkEquals( theMeshBase.getHistory().getLength(), 4, "Wrong length of history" );

        captureTs.put( b.getTimeUpdated(), "t1" );
        checkStockFlowIntegrity( "t1", captureTs );

        // printHistory( "After executeAt creating b", theMeshBase, null );

        checkEquals(    home.getTimeCreated(), home.getTimeUpdated(), "Not: Home updated since creation" );
        checkEquals(    b.getTimeCreated(),    b.getTimeUpdated(),    "Not: b updated since creation" );
        checkCondition(  a.getTimeCreated()    < a.getTimeUpdated(),  "a not updated" );

        checkCondition(  home.getTimeCreated() < a.getTimeCreated(),  "Not: Home created before a" );
        checkCondition(  a.getTimeCreated() < b.getTimeCreated(),     "Not: a created before b" );
        checkCondition(  b.getTimeCreated() < a.getTimeUpdated(),     "Not: b created before a updated" );

        //

        log.info( "Checking MeshBaseState history" );

        HistoryCursorIterator<MeshBaseState> stateIter = theMeshBase.getHistory().iterator();
        stateIter.moveToBeforeFirst();

        MeshBaseState mbs = stateIter.next();
        MeshBaseView  mbv = mbs.getMeshBaseView();

        checkEquals( mbs.getChangeList().size(), 1, "Wrong ChangeList size at oldest" );
        checkEquals( mbv.size(), 1, "Wrong mbv size at oldest" );

        mbs = stateIter.next();
        mbv = mbs.getMeshBaseView();

        checkEquals( mbs.getChangeList().size(), 3, "Wrong ChangeList size at (a created)" );
        checkEquals( mbv.size(), 2, "Wrong mbv size at (a created)" );
        checkObject( mbv.findMeshObjectByIdentifier( A ), "a not found at (a created)" );

        mbs = stateIter.next();
        mbv = mbs.getMeshBaseView();

        checkEquals( mbs.getChangeList().size(), 3, "Wrong ChangeList size at (b created)" );
        checkEquals( mbv.size(), 3, "Wrong mbv size at (b created)" );
        checkObject( mbv.findMeshObjectByIdentifier( A ), "a not found at (b created)" );
        checkObject( mbv.findMeshObjectByIdentifier( B ), "b not found at (b created)" );

        mbs = stateIter.next();
        mbv = mbs.getMeshBaseView();

        checkEquals( mbs.getChangeList().size(), 1, "Wrong ChangeList size at (a updated)" );
        checkEquals( mbv.size(), 3, "Wrong mbv size at (a updated)" );
        checkObject( mbv.findMeshObjectByIdentifier( A ), "a not found at (a updated)" );
        checkObject( mbv.findMeshObjectByIdentifier( B ), "b not found at (a updated)" );

        //

        log.info( "Check that the HEAD version is correct" );
        checkEquals( theMeshBase.size(), 3, "Wrong MeshBase size" );

        MeshObject aHead = theMeshBase.findMeshObjectByIdentifier( A );
        MeshObject bHead = theMeshBase.findMeshObjectByIdentifier( B );

        checkEquals( aHead.getAttributeValue( ATT_NAME ), 2, "Wrong value for a.attribute in HEAD" );
        checkEquals( bHead.getAttributeValue( ATT_NAME ), 1, "Wrong value for b.attribute in HEAD" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( EditableHistoryTest3.class );
}
