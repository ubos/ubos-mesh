//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.changelist;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.MeshObjectAttributeChange;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that a several pairs of Attribute changes are reduced.
 * We create the ChangeList in a Transaction, because it's the easiest way of doing it
 */
public class ChangeListNormalizeAttributeTest2
        extends
            AbstractChangeListNormalizeTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final MeshObjectIdentifier A    = theMeshBase.createMeshObjectIdentifier( "a" );
        final String               ATT1 = "att-1";
        final String               ATT2 = "att-2";

        log.info( "Creating MeshObject(s)" );

        theMeshBase.execute( () -> {
            MeshObject a = theMeshBase.createMeshObject( A );
            a.setAttributeValue( ATT1, 0 );
            a.setAttributeValue( ATT2, "alpha" );
        });

        //

        log.info( "Create the ChangeList" );

        ChangeList list = theMeshBase.execute( (tx) -> {
            MeshObject a = theMeshBase.findMeshObjectByIdentifier( A );
            a.setAttributeValue( ATT1, 1 );
            a.setAttributeValue( ATT2, "beta" );
            a.setAttributeValue( ATT1, 2 );
            a.setAttributeValue( ATT2, "gamma" );
            a.setAttributeValue( ATT2, "delta" );
            a.setAttributeValue( ATT1, 3 );

            return tx.getChangeList().clone(); // get clone, the Transaction's ChangeList will be normalized on commit
        });
        checkEquals( list.size(), 6, "Wrong size before" );

        //

        log.info( "Normalizing" );

        list.normalize();

        checkEquals( list.size(), 2, "Wrong size after" );

        MeshObjectAttributeChange change1 = (MeshObjectAttributeChange) list.getChanges().get( 0 );
        MeshObjectAttributeChange change2 = (MeshObjectAttributeChange) list.getChanges().get( 1 );
        checkEquals( change1.getOldValue(), 0, "Wrong before value" );
        checkEquals( change1.getNewValue(), 3, "Wrong after value" );
        checkEquals( change2.getOldValue(), "alpha", "Wrong before value" );
        checkEquals( change2.getNewValue(), "delta", "Wrong after value" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(ChangeListNormalizeAttributeTest2.class);
}

