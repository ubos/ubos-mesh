//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests whether rollbacks work.
 */
public class RollbackTest2
        extends
            AbstractMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        MeshBase theMeshBase = MMeshBase.Builder.create().build();

        final MeshObjectIdentifier fixed1Id = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed1" );
        final MeshObjectIdentifier fixed2Id = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed2" );
        final MeshObjectIdentifier fixed3Id = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed3" );
        final MeshObjectIdentifier fixed4Id = theMeshBase.meshObjectIdentifierFromExternalForm( "fixed4" );

        //

        log.debug( "Creating MeshObjectGraph that won't be rolled back" );

        theMeshBase.execute( tx -> {
                MeshObject fixed1 = theMeshBase.createMeshObject( fixed1Id, TestSubjectArea.AA );
                MeshObject fixed2 = theMeshBase.createMeshObject( fixed2Id, TestSubjectArea.AA );
                MeshObject fixed3 = theMeshBase.createMeshObject( fixed3Id, TestSubjectArea.AA );
                MeshObject fixed4 = theMeshBase.createMeshObject( fixed4Id, TestSubjectArea.AA );

                fixed1.blessRole( TestSubjectArea.AR1A.getSource(), fixed2 );
                fixed2.blessRole( TestSubjectArea.AR1A.getSource(), fixed3 );
                fixed3.blessRole( TestSubjectArea.AR1A.getSource(), fixed4 );
                fixed4.blessRole( TestSubjectArea.AR1A.getSource(), fixed1 );

                return null;
        });

//        checkEquals( theMeshBase.size(), 5, "Wrong initial number of MeshObjects" );

        final MeshObject fixed1 = theMeshBase.findMeshObjectByIdentifier( fixed1Id );
        final MeshObject fixed2 = theMeshBase.findMeshObjectByIdentifier( fixed2Id );
        final MeshObject fixed3 = theMeshBase.findMeshObjectByIdentifier( fixed3Id );
        final MeshObject fixed4 = theMeshBase.findMeshObjectByIdentifier( fixed4Id );

        //

        log.debug( "Creating failing Transaction that will automatically be rolled back." );

        theMeshBase.execute( () -> {
                theMeshBase.deleteMeshObject( fixed4 );

                fixed1.unrelate( fixed2 );

                fixed2.unblessRole( TestSubjectArea.AR1A.getSource(), fixed3 );

                throw new TransactionActionException.Rollback();
        });

        //

        final MeshObject fixed4New = theMeshBase.findMeshObjectByIdentifier( fixed4Id );
        // fixed4 is now dead and cannot currently be revived (FIXME?)

        checkEquals( theMeshBase.size(), 5, "Wrong number of MeshObjects" );
        checkObject( theMeshBase.findMeshObjectByIdentifier( fixed1Id ), "fixed1Id not found" );
        checkObject( theMeshBase.findMeshObjectByIdentifier( fixed2Id ), "fixed2Id not found" );
        checkObject( theMeshBase.findMeshObjectByIdentifier( fixed3Id ), "fixed3Id not found" );
        checkObject( theMeshBase.findMeshObjectByIdentifier( fixed4Id ), "fixed4Id not found" );
        checkCondition( fixed1.isRelated( fixed2 ), "fixed1 is not related to fixed2" );
        checkCondition( fixed2.isRelated( fixed3 ), "fixed2 is not related to fixed3" );
        checkCondition( fixed3.isRelated( fixed4New ), "fixed3 is not related to fixed4" );
        checkCondition( fixed4New.isRelated( fixed1 ), "fixed4 is not related to fixed1" );
        checkEquals( fixed1.getRoleTypes( fixed2 ).length, 1, "Not blessed: fixed1 to fixed2");
        checkEquals( fixed2.getRoleTypes( fixed3 ).length, 1, "Not blessed: fixed2 to fixed3");
        checkEquals( fixed3.getRoleTypes( fixed4New ).length, 1, "Not blessed: fixed3 to fixed4");
        checkEquals( fixed4New.getRoleTypes( fixed1 ).length, 1, "Not blessed: fixed4 to fixed1");
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( RollbackTest2.class );
}
