//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.importer;

import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality for tests of MeshBase encoding and decoding.
 */
public abstract class AbstractExternalizedMeshBaseDeEnCoderTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        thePrimaryNsMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();
    }

    /**
     * Manages our namespaces.
     */
    protected PrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;
}
