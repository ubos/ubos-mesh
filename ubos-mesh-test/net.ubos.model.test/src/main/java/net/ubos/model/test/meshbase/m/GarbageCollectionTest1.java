//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.model.test.meshbase.m;

import java.lang.ref.WeakReference;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.EnumeratedDataType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests whether MeshBases are deallocated when not needed any more.
 */
public class GarbageCollectionTest1
        extends
            AbstractMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test.
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        EnumeratedDataType zType  = (EnumeratedDataType) TestSubjectArea.B_Z.getDataType();

        log.info( "Creating MeshBases" );

        MeshBase mb1 = MMeshBase.Builder.create().build();
        MeshBase mb2 = MMeshBase.Builder.create().build();
        MeshBase mb3 = MMeshBase.Builder.create().build();

        //

        log.info( "Creating WeakReferences" );

        WeakReference<MeshBase> ref1 = new WeakReference<>( mb1 );
        WeakReference<MeshBase> ref2 = new WeakReference<>( mb2 );
        WeakReference<MeshBase> ref3 = new WeakReference<>( mb3 );

        checkEquals( ref1.get(), mb1, "Weak reference to first MeshBase lost" );
        checkEquals( ref2.get(), mb2, "Weak reference to second MeshBase lost" );
        checkEquals( ref3.get(), mb3, "Weak reference to third MeshBase lost" );

        //

        log.info( "Removing reference to third MeshBase, waiting, and checking" );

        mb3 = null;

        collectGarbage();

        checkEquals( ref3.get(), null, "Third MeshBase still exists");

        //

        log.info( "Setting up some data in first MeshBase" );

        mb1.execute( ( Transaction tx ) -> {
            MeshBase mb = tx.getMeshBase();

            MeshObject obj1_A = mb.createMeshObject( TestSubjectArea.AA );
            obj1_A.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 12.34 ) );

            MeshObject obj2_A = mb.createMeshObject( TestSubjectArea.B );
            obj2_A.setPropertyValue( TestSubjectArea.B_Z, zType.select( "Value1" ) );

            MeshObject obj3_A = mb.createMeshObject( TestSubjectArea.B );
            obj3_A.setPropertyValue( TestSubjectArea.B_Z, zType.select( "Value3" ) );

            obj1_A.blessRole( TestSubjectArea.R.getSource(), obj2_A );
            obj1_A.blessRole( TestSubjectArea.R.getSource(), obj3_A );

            return null;
        } );

        //

        if( log.isDebugEnabled() ) {
            Thread[] theThreads = new Thread[100]; // wild guess, there's no good API ...
            Thread.enumerate(theThreads);

            for( int i=0 ; theThreads[i] != null ; ++i ) {
                log.debug( "Thread " + i + ": " + theThreads[i] );
            }
        }

        log.info( "Checking that everything is still fine" );

        checkEquals( ref1.get(), mb1, "Weak reference to first repository lost" );
        checkEquals( ref2.get(), mb2, "Weak reference to second repository lost" );
        checkEquals( ref3.get(), null, "Weak reference to third repository has re-appeared" );

        //

        log.info( "Setting all local references to null" );

        mb1 = null;
        mb2 = null;
        mb3 = null;

        collectGarbage();
        Thread.sleep( 10000L );
        collectGarbage();

        if( log.isDebugEnabled() ) {
            Thread[] theThreads = new Thread[100]; // wild guess, there's no good API ...
            Thread.enumerate(theThreads);

            for( int i=0 ; theThreads[i] != null ; ++i ) {
                log.debug( "Thread " + i + ": " + theThreads[i] );
            }
        }

        checkEquals( ref1.get(), null, "Weak reference to first MeshBase is still there" );
        checkEquals( ref2.get(), null, "Weak reference to second MeshBase is still there" );
        checkEquals( ref3.get(), null, "Weak reference to third MeshBase has re-appeared" );

    }

   // Our Logger
    private static Log log = Log.getLogInstance( GarbageCollectionTest1.class );
}

