//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test restoring of relationships with Role PropertyTypes.
 */
public class StoreMeshBaseTest3
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Populating MeshBase" );

        MeshObjectIdentifier oneId = mb.meshObjectIdentifierFromExternalForm( "one" );
        MeshObjectIdentifier twoId = mb.meshObjectIdentifierFromExternalForm( "two" );

        FloatValue  newYValue    = FloatValue.create( 123.456 );
        StringValue newSrcValue  = StringValue.create( "NewFooSrcValue" );
        StringValue newDestValue = StringValue.create( "NewFooDestValue" );

        mb.execute( (Transaction tx) -> {
            MeshObject one = mb.createMeshObject( oneId, TestSubjectArea.AA );
            MeshObject two = mb.createMeshObject( twoId, TestSubjectArea.AA );

            one.blessRole( TestSubjectArea.ARANY.getSource(), two );

            return null;
        } );

        MeshObject one = mb.findMeshObjectByIdentifier(oneId );
        MeshObject two = mb.findMeshObjectByIdentifier( twoId );

        checkEquals( one.getPropertyValue( TestSubjectArea.AA_Y ), TestSubjectArea.AA_Y.getDefaultValue(), "Wrong y value" );
        checkEquals( one.getRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO ), TestSubjectArea.ARANY_S_FOO.getDefaultValue(), "Wrong source property value" );
        checkEquals( two.getRolePropertyValue( one, TestSubjectArea.ARANY_D_FOO ), TestSubjectArea.ARANY_D_FOO.getDefaultValue(), "Wrong destination property value" );

        //

        log.info( "Clearing memory cache" );

        one = null;
        two = null;
        mb.clearMemoryCache();

        //

        log.info( "Setting property values" );

        mb.execute( (Transaction tx) -> {
            MeshObject oneone = mb.findMeshObjectByIdentifier( oneId );
            MeshObject twotwo = mb.findMeshObjectByIdentifier( twoId );

            oneone.setPropertyValue( TestSubjectArea.AA_Y, newYValue );
            oneone.setRolePropertyValue( twotwo, TestSubjectArea.ARANY_S_FOO, newSrcValue );
            twotwo.setRolePropertyValue( oneone, TestSubjectArea.ARANY_D_FOO, newDestValue );

            return null;
        } );

        one = mb.findMeshObjectByIdentifier( oneId );
        two = mb.findMeshObjectByIdentifier( twoId );

        checkEquals( one.getPropertyValue( TestSubjectArea.AA_Y ), newYValue, "Wrong y value" );
        checkEquals( one.getRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO ), newSrcValue,  "Wrong source property value" );
        checkEquals( two.getRolePropertyValue( one, TestSubjectArea.ARANY_D_FOO ), newDestValue, "Wrong destination property value" );

        //

        log.info( "Clearing memory cache" );

        one = null;
        two = null;
        mb.clearMemoryCache();

        //

        log.info( "Checking property values" );

        one = mb.findMeshObjectByIdentifier( oneId );
        two = mb.findMeshObjectByIdentifier( twoId );

        checkEquals( one.getPropertyValue( TestSubjectArea.AA_Y ), newYValue, "Wrong y value" );
        checkEquals( one.getRolePropertyValue( two, TestSubjectArea.ARANY_S_FOO ), newSrcValue,  "Wrong source property value" );
        checkEquals( two.getRolePropertyValue( one, TestSubjectArea.ARANY_D_FOO ), newDestValue, "Wrong destination property value" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest3.class );
}
