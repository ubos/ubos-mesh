//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import java.io.Serializable;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests RoleAttributeValues.
 */
public class StoreMeshBaseTest4
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Populating MeshBase" );

        MeshObjectIdentifier aaaaId  = mb.meshObjectIdentifierFromExternalForm( "aaaa" );
        MeshObjectIdentifier bbbbId  = mb.meshObjectIdentifierFromExternalForm( "bbbb" );
        String               raName  = "test";
        Serializable         raValue = 1;

        mb.execute( () -> {
            MeshObject aaaa = mb.createMeshObject( aaaaId );
            MeshObject bbbb = mb.createMeshObject( bbbbId );
        } );

        collectGarbage();

        //

        log.info( "Relating objects" );

        mb.execute( () -> {
            MeshObject aaaa = mb.findMeshObjectByIdentifier( aaaaId );
            MeshObject bbbb = mb.findMeshObjectByIdentifier( bbbbId );

            aaaa.setRoleAttributeValue( bbbb, raName, raValue );
        } );

        collectGarbage();

        //

        log.info( "Checking" );

        MeshObject aaaa = mb.findMeshObjectByIdentifier( aaaaId );
        MeshObject bbbb = mb.findMeshObjectByIdentifier( bbbbId );

        checkCondition( aaaa.isRelated( bbbb ), "aaaa not related" );
        checkCondition( bbbb.isRelated( aaaa ), "bbbb not related" );

        checkCondition( aaaa.hasRoleAttribute( bbbb, raName ), "aaaa has no RoleAttribute" );
        checkEquals( aaaa.getRoleAttributeValue( bbbb, raName ), raValue, "aaaa has wrong RoleAttribute value" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest4.class );
}
