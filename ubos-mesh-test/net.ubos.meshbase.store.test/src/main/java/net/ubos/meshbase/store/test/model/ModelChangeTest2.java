//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.model;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.L10PropertyValueMapImpl;
import net.ubos.model.primitives.MeshTypeIdentifierDeserializer;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.MeshTypeLifecycleManager;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Models are not supposed to change. But sometimes, they do, such as during development.
 * Tests that a mandatory PropertyType can be added without too bad consequences.
 */
public class ModelChangeTest2
    extends
        AbstractModelChangeTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @Disabled // This does not work any more because we have a single Singleton instance of ModelBase
    @SuppressWarnings( "UnusedAssignment" )
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating and populating MB1" );

        StoreMeshBase mb1 = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, thePrimaryNsMap ).build();

        try {
            initializeModelAndMeshBase( mb1 );

            mb1.die();
            mb1 = null;

        } finally {
            cleanupModelBase();
        }


        //

        SubjectArea  sa           = null;
        EntityType   ent1         = null;
        PropertyType ent1_prop1   = null;
        PropertyType ent1_prop2   = null;
        EntityType   ent2         = null;
        RelationshipType rel3_4   = null;
        EntityType   ent3         = null;
        EntityType   ent4         = null;

        try {

            log.info( "Creating changed ModelBase" );

            MeshTypeLifecycleManager       typeLife   = theModelBase.getMeshTypeLifecycleManager();
            MeshTypeIdentifierDeserializer typeIdFact = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

            sa = typeLife.createSubjectArea(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model" ),
                    StringValue.create( "net.ubos.meshbase.store.test.model" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "test.model") ),
                    null, null, null );

            ent1 = typeLife.createEntityType(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent1" ),
                    StringValue.create( "Ent1" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "Ent1") ),
                    null, null,
                    sa,
                    null, null,
                    BooleanValue.TRUE, // abstract
                    BooleanValue.TRUE );

            ent1_prop1 = typeLife.createPropertyType(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent1_Prop1" ),
                    StringValue.create( "Ent1_Prop1" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "Ent1_Prop1") ),
                    null,
                    ent1,
                    sa,
                    StringDataType.theDefault,
                    null,
                    BooleanValue.TRUE,
                    BooleanValue.FALSE,
                    FloatValue.create( 1. ) );

            // added mandatory PropertyType
            ent1_prop2 = typeLife.createPropertyType(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent1_Prop2" ),
                    StringValue.create( "Ent1_Prop2" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "Ent1_Prop2") ),
                    null,
                    ent1,
                    sa,
                    StringDataType.theDefault,
                    StringValue.create( "Default value of mandatory new PropertyType" ),
                    BooleanValue.FALSE, // not optional
                    BooleanValue.FALSE,
                    FloatValue.create( 1. ) );

            ent2 = typeLife.createEntityType(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent2" ),
                    StringValue.create( "Ent2" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "Ent2") ),
                    null, null,
                    sa,
                    new EntityType[] { ent1 },
                    null,
                    BooleanValue.TRUE, // abstract
                    BooleanValue.TRUE );

            ent3 = typeLife.createEntityType(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent3" ),
                    StringValue.create( "Ent3" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "Ent3") ),
                    null, null,
                    sa,
                    new EntityType[] { ent2 },
                    null,
                    BooleanValue.FALSE, // not abstract
                    BooleanValue.TRUE );

            ent4 = typeLife.createEntityType(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent4" ),
                    StringValue.create( "Ent4" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "Ent4") ),
                    null, null,
                    sa,
                    null, null,
                    BooleanValue.FALSE, // not abstract
                    BooleanValue.TRUE );

            rel3_4 = typeLife.createRelationshipType(
                    typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Rel3_4" ),
                    StringValue.create( "Rel3_4" ),
                    L10PropertyValueMapImpl.create( StringValue.create( "Rel3_4") ),
                    null,
                    sa,
                    MultiplicityValue.ZERO_N,
                    MultiplicityValue.ZERO_N,
                    ent3,
                    ent4,
                    (RoleType[]) null, null,
                    BooleanValue.FALSE );

            //

            log.info( "Creating mb2" );

            StoreMeshBase mb2 = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, thePrimaryNsMap ).build();

            // Read all elements
            log.info( "Traversing mb2" );

            boolean found = false;
            for( MeshObject current : mb2 ) {
                if( log.isDebugEnabled() ) {
                    log.debug( "Found", current );
                }
                if( current.getIdentifier().getLocalId().equals( "a" )) {
                    checkEquals( current.getPropertyValue( ent1_prop2 ), ent1_prop2.getDefaultValue(), "wrong value" );
                    found = true;
                }
            }
            if( !found ) {
                reportError( "Could not find #a" );}

            mb2.die();
            mb2 = null;

        } finally {
            theModelBase.remove( sa );
            theModelBase.remove( ent1 );
            theModelBase.remove( ent1_prop1 );
            theModelBase.remove( ent1_prop2 );
            theModelBase.remove( ent2 );
            theModelBase.remove( ent3 );
            theModelBase.remove( ent4 );
            theModelBase.remove( rel3_4 );
            theModelBase.remove( rel3_4.getSource() );
            theModelBase.remove( rel3_4.getDestination() );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( ModelChangeTest2.class );
}
