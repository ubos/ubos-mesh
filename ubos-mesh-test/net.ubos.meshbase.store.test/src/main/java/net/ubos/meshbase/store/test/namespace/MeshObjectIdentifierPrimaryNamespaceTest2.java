//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.namespace;

import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that changes in a StorePrimaryMeshObjectIdentifierNamespaceMap are persisted.
 */
public class MeshObjectIdentifierPrimaryNamespaceTest2
        extends
            AbstractMeshObjectIdentifierNamespaceTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        populate();
        restoreAndChange();
        restoreAndCheck();
    }

    protected void populate()
    {
        log.info( "Create and populate StorePrimaryMeshObjectIdentifierNamespaceMap" );

        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        MeshObjectIdentifierNamespace ns1 = primaryNsMap.obtainByExternalName( "one" );
        ns1.addExternalName( "one-1" );

        MeshObjectIdentifierNamespace ns2 = primaryNsMap.obtainByExternalName( "two" );
        ns2.addExternalName( "two-1" );
        ns2.addExternalName( "two-2" );

        MeshObjectIdentifierNamespace ns3 = primaryNsMap.obtainByExternalName( "three" );
        ns3.addExternalName( "three-1" );
    }

    protected void restoreAndChange()
    {
        log.info( "Restore and check StorePrimaryMeshObjectIdentifierNamespaceMap" );

        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        MeshObjectIdentifierNamespace ns1 = primaryNsMap.findByExternalName( "one" );
        checkObject( ns1, "ns1 not found" );
        checkEquals( ns1.getExternalNames().size(), 2, "Wrong number external names" );

        for( String current : new String[] { "one", "one-1" } ) {
            checkCondition( ns1.getExternalNames().contains( current ), "ns1 does not have external name: " + current );
        }
        checkEquals( ns1.getPreferredExternalName(), "one", "ns1 has wrong preferred external name" );

        MeshObjectIdentifierNamespace ns2 = primaryNsMap.findByExternalName( "two" );
        checkObject( ns2, "ns2 not found" );
        checkEquals( ns2.getExternalNames().size(), 3, "Wrong number external names" );

        for( String current : new String[] { "two", "two-1", "two-2" } ) {
            checkCondition( ns2.getExternalNames().contains( current ), "ns2 does not have external name: " + current );
        }
        checkEquals( ns2.getPreferredExternalName(), "two", "ns2 has wrong preferred external name" );

        MeshObjectIdentifierNamespace ns3 = primaryNsMap.findByExternalName( "three" );
        checkObject( ns3, "ns3 not found" );
        checkEquals( ns3.getExternalNames().size(), 2, "Wrong number external names" );

        for( String current : new String[] { "three", "three-1" } ) {
            checkCondition( ns3.getExternalNames().contains( current ), "ns3 does not have external name: " + current );
        }
        checkEquals( ns3.getPreferredExternalName(), "three", "ns3 has wrong preferred external name" );

        //

        log.info( "Making changes" );

        ns1.addExternalName( "one-added" );
        ns2.removeExternalName( "two-1" );
        ns3.setPreferredExternalName( "three-1" );
    }

    protected void restoreAndCheck()
    {
        log.info( "Restore and check StorePrimaryMeshObjectIdentifierNamespaceMap" );

        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        MeshObjectIdentifierNamespace ns1 = primaryNsMap.findByExternalName( "one" );
        checkObject( ns1, "ns1 not found" );
        checkEquals( ns1.getExternalNames().size(), 3, "Wrong number external names" );

        for( String current : new String[] { "one", "one-1", "one-added" } ) {
            checkCondition( ns1.getExternalNames().contains( current ), "ns1 does not have external name: " + current );
        }
        checkEquals( ns1.getPreferredExternalName(), "one", "ns1 has wrong preferred external name" );

        MeshObjectIdentifierNamespace ns2 = primaryNsMap.findByExternalName( "two" );
        checkObject( ns2, "ns2 not found" );
        checkEquals( ns2.getExternalNames().size(), 2, "Wrong number external names" );

        for( String current : new String[] { "two", "two-2" } ) {
            checkCondition( ns2.getExternalNames().contains( current ), "ns2 does not have external name: " + current );
        }
        checkEquals( ns2.getPreferredExternalName(), "two", "ns2 has wrong preferred external name" );

        MeshObjectIdentifierNamespace ns3 = primaryNsMap.findByExternalName( "three" );
        checkObject( ns3, "ns3 not found" );
        checkEquals( ns3.getExternalNames().size(), 2, "Wrong number external names" );

        for( String current : new String[] { "three", "three-1" } ) {
            checkCondition( ns3.getExternalNames().contains( current ), "ns3 does not have external name: " + current );
        }
        checkEquals( ns3.getPreferredExternalName(), "three-1", "ns3 has wrong preferred external name" );

    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshObjectIdentifierPrimaryNamespaceTest2.class );
}

