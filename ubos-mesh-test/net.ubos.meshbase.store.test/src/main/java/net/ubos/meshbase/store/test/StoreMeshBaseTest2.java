//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.meshbase.transaction.TransactionException;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Creates MeshObjects in the Sql implementation of the StoreMeshBase, removes them from
 * cache and transparently re-reads them.
 */
public class StoreMeshBaseTest2
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        EntityType [] abctypes = { TestSubjectArea.AA, TestSubjectArea.B };

        //

        log.info( "Creating MeshBase" );

        super.startClock();
        long t1 = System.currentTimeMillis();

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        long t2 = System.currentTimeMillis();

        checkObject( mb.getHomeObject(), "No home object" );
        checkIdentity( mb.getHomeObject().getMeshBaseView().getMeshBase(), mb, "Home object in wrong MeshBaseView" );
        checkIdentity( mb.getHomeObject().getMeshBase(), mb, "Home object in wrong MeshBase" );
        checkInRange( mb.getHomeObject().getTimeCreated(), t1, t2, "Home object created at wrong time" );
        checkInRange( mb.getHomeObject().getTimeUpdated(), t1, t2, "Home object updated at wrong time" );

        //

        log.info( "Checking that transactions are required" );

        try {
            mb.createMeshObject();
            reportError( "createMeshObject did not throw TransactionException" );
        } catch( TransactionException ex ) {

        }

        //

        final String PROP_VALUE_PREFIX = "This is <b>important&trade;</b>&#33; MeshObject "; // make sure HTML is in it
        EnumeratedValue [] ptZdomain = TestSubjectArea.B_Z_type.getDomain();

        log.info( "Creating MeshObjects" );

        t1 = System.currentTimeMillis();

        final MeshObject []           mesh  = new MeshObject[ theTestSize ];
        final MeshObjectIdentifier [] names = new MeshObjectIdentifier[ theTestSize ];

        mb.execute( (Transaction tx) -> {
            for( int i=0 ; i<mesh.length ; ++i ) {
                mesh[i] = mb.createMeshObject();
                mesh[i].bless( abctypes[ i % abctypes.length ] );

                if( i>0 && i<mesh.length*3/4 ) {
                    mesh[i].setRoleAttributeValue( mesh[i-1], "something", "else" );
                }
                if( i>0 && i<mesh.length/2 ) {
                    if( i % 2 == 0 ) {
                        mesh[i].blessRole( TestSubjectArea.R.getSource(), mesh[i-1] );
                    } else {
                        mesh[i].blessRole( TestSubjectArea.R.getDestination(), mesh[i-1] );
                    }
                }
                if( i/2 % 2 == 0 ) {
                    if( mesh[i].isBlessedBy( TestSubjectArea.AA )) {
                        mesh[i].setPropertyValue( TestSubjectArea.A_X,  StringValue.create( PROP_VALUE_PREFIX + i ));
                        mesh[i].setPropertyValue( TestSubjectArea.A_XX, TestSubjectArea.A_XX_type.createBlobValue( PROP_VALUE_PREFIX + i, SelectableMimeType.TEXT_PLAIN.getMimeType() ));
                    } else {
                        mesh[i].setPropertyValue( TestSubjectArea.B_Z, ptZdomain[ i % ptZdomain.length ] );
                    }
                }

                names[i] = mesh[i].getIdentifier();
            }
            return null;
        } );

        t2 = System.currentTimeMillis();

        for( int i=0 ; i<mesh.length ; ++i ) {
            checkObject( mesh[i], "MeshObject " + i + "is null" );
            checkIdentity( mesh[i].getMeshBaseView().getMeshBase(), mb, "MeshObject " + i + " in wrong MeshBaseView" );
            checkIdentity( mesh[i].getMeshBase(), mb, "MeshObject " + i + " in wrong MeshBase" );
            checkInRange(
                    mesh[i].getTimeCreated(),
                    ( i==0 ) ? t1 : mesh[i-1].getTimeCreated(),
                    ( i==mesh.length-1 ) ? t2 : mesh[i+1].getTimeCreated(),
                    "MeshObject " + i + " created at wrong time" );
            checkInRange(
                    mesh[i].getTimeUpdated(),
                    ( i==0 ) ? t1 : mesh[i-1].getTimeUpdated(),
                    ( i==mesh.length-1 ) ? t2 : mesh[i+1].getTimeUpdated(),
                    "MeshObject " + i + " updated at wrong time" );
        }

        double duration = getRelativeTime()/1000.0;

        log.info( "Test (writing) using " + mesh.length + " objects took " + duration + " seconds at " + ( duration / mesh.length * 1000.0 ) + " msec/object." );

        //

        log.info( "Clearing cache, and loading MeshObjects again" );

        for( int i=0 ; i<mesh.length ; ++i ) {
            mesh[i] = null;
        }
        mb.clearMemoryCache();

        super.startClock();

        for( int i=0 ; i<names.length ; ++i ) {

            if( log.isDebugEnabled() ) {
                log.debug( "Looking for object " + names[i] );
            }
            mesh[i] = mb.findMeshObjectByIdentifier( names[i] );

            checkObject( mesh[i], "Could not retrieve MeshObject with Identifier " + names[i] );
            if( mesh[0] != null ) {
                // it always should be, but if it isn't and the test failed, we might still want to continue the test
                checkEquals( mesh[i].getEntityTypes().length, 1, "not the right number of MeshTypes" );
                checkEquals( mesh[i].getEntityTypes()[0], abctypes[ i % abctypes.length ], "not the right MeshType" );
            }
            if( i>0 && i<mesh.length*3/4 ) {
                checkEquals( mesh[i].getRoleAttributeValue( mesh[i-1], "something" ), "else", "Wrong RoleAttribute" );
            }
            if( i/2 % 2 == 0 ) {
                if( mesh[i].isBlessedBy( TestSubjectArea.AA )) {
                    checkEquals( mesh[i].getPropertyValue( TestSubjectArea.A_X ),  StringValue.create( PROP_VALUE_PREFIX + i ), "Wrong ptX value" );
                    checkEquals( mesh[i].getPropertyValue( TestSubjectArea.A_XX ), TestSubjectArea.A_XX_type.createBlobValue( PROP_VALUE_PREFIX + i, SelectableMimeType.TEXT_PLAIN.getMimeType() ), "Wrong ptX value" );
                } else {
                    checkEquals( mesh[i].getPropertyValue( TestSubjectArea.B_Z ), ptZdomain[ i % ptZdomain.length ], "Wrong ptZ value" );
                }
            }
        }

        duration = super.getRelativeTime()/1000.0;

        log.info( "Test (reading) using " + mesh.length + " objects took " + duration + " seconds at " + ( duration / mesh.length * 1000.0 ) + " msec/object." );
    }

    /**
     * The number of MeshObjects to create for the test.
     */
    protected int theTestSize = 1000;

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest2.class);
}
