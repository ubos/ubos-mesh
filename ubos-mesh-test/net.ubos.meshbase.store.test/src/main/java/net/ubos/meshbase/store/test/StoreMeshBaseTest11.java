//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests large amounts of data in a StoreMeshBase.
 */
public class StoreMeshBaseTest11
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final String FORMAT = "obj-%3d";

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Collecting garbage and taking memory snapshot 1" );

        collectGarbage();

        Runtime rt = Runtime.getRuntime();

        long memFree1  = rt.freeMemory();
        long memMax1   = rt.maxMemory();
        long memTotal1 = rt.totalMemory();

        log.debug( "Memory free: " + memFree1 + ", max: " + memMax1 + ", total: " + memTotal1 + ", consumed: " + ( memTotal1 - memFree1 ) );

        //

        log.info( "Creating " + theTestSize + " objects, 100 objects per transaction" );

        for( int i=0 ; i<theTestSize ; i+=100 ) {
            int javaIsGreat = i;
            mb.execute( () -> {
                for( int j=javaIsGreat ; j<Math.min( javaIsGreat+100, theTestSize ) ; ++j ) {
                    mb.createMeshObject( String.format( FORMAT, j ) );
                }
            } );
        }

        //

        log.info( "Relating objects" );

        int quarter = theTestSize/4;
        for( int i=0 ; i<quarter ; i+=100 ) {
            int javaIsGreat = i;
            mb.execute( () -> {
                for( int j=javaIsGreat ; j<Math.min( javaIsGreat+100, theTestSize ) ; ++j ) {
                    MeshObjectIdentifier zeroId  = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, j )  );
                    MeshObjectIdentifier oneId   = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, j + quarter ));
                    MeshObjectIdentifier twoId   = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, j + quarter + quarter ));
                    MeshObjectIdentifier threeId = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, j + quarter + quarter + quarter ));

                    MeshObject zero  = mb.findMeshObjectByIdentifier( zeroId );
                    MeshObject one   = mb.findMeshObjectByIdentifier( oneId );
                    MeshObject two   = mb.findMeshObjectByIdentifier( twoId );
                    MeshObject three = mb.findMeshObjectByIdentifier( threeId );

                    one.setRoleAttributeValue(   two,   "1 -> 2", true );
                    three.setRoleAttributeValue( zero,  "3 -> 0", true );
                    one.setRoleAttributeValue(   three, "1 -> 3", true );
                }
            } );
        }

        //

        log.info( "Checking graph (1)" );

        for( int i=0 ; i<quarter ; ++i ) {
            MeshObjectIdentifier zeroId  = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i ));
            MeshObjectIdentifier oneId   = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i + quarter ));
            MeshObjectIdentifier twoId   = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i + quarter + quarter ));
            MeshObjectIdentifier threeId = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i + quarter + quarter + quarter ));

            MeshObject zero  = mb.findMeshObjectByIdentifier( zeroId );
            MeshObject one   = mb.findMeshObjectByIdentifier( oneId );
            MeshObject two   = mb.findMeshObjectByIdentifier( twoId );
            MeshObject three = mb.findMeshObjectByIdentifier( threeId );

            checkEqualsOutOfSequence(
                    zero.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        three
                    },
                    "Iteration: " + i + ": One has wrong relationships " + zeroId );
            checkEqualsOutOfSequence(
                    one.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        two,
                        three
                    },
                    "Iteration: " + i + ": Two has wrong relationships " + oneId );
            checkEqualsOutOfSequence(
                    two.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        one
                    },
                    "Iteration: " + i + ": Three has wrong relationships " + twoId );
            checkEqualsOutOfSequence(
                    three.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        zero,
                        one
                    },
                    "Iteration: " + i + ": Four has wrong relationships " + threeId );
        }

        //

        log.info( "Collecting garbage and taking memory snapshot 2" );

        collectGarbage();

        long memFree2  = rt.freeMemory();
        long memMax2   = rt.maxMemory();
        long memTotal2 = rt.totalMemory();

        log.debug( "Memory free: " + memFree2 + ", max: " + memMax2 + ", total: " + memTotal2 + ", consumed: " + ( memTotal2 - memFree2 ));

        //

        log.info( "Checking graph (2)" );

        for( int i=0 ; i<quarter ; ++i ) {
            MeshObjectIdentifier zeroId  = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i ));
            MeshObjectIdentifier oneId   = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i + quarter ));
            MeshObjectIdentifier twoId   = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i + quarter + quarter ));
            MeshObjectIdentifier threeId = mb.meshObjectIdentifierFromExternalForm( String.format( FORMAT, i + quarter + quarter + quarter ));

            MeshObject zero  = mb.findMeshObjectByIdentifier( zeroId );
            MeshObject one   = mb.findMeshObjectByIdentifier( oneId );
            MeshObject two   = mb.findMeshObjectByIdentifier( twoId );
            MeshObject three = mb.findMeshObjectByIdentifier( threeId );

            checkEqualsOutOfSequence(
                    zero.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        three
                    },
                    "Post-read iteration: " + i + ": One has wrong relationships " + zeroId );
            checkEqualsOutOfSequence(
                    one.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        two,
                        three
                    },
                    "Post-read iteration: " + i + ": One has wrong relationships " + oneId );
            checkEqualsOutOfSequence(
                    two.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        one
                    },
                    "Post-read iteration: " + i + ": One has wrong relationships " + twoId );
            checkEqualsOutOfSequence(
                    three.traverseToNeighbors().getMeshObjects(),
                    new MeshObject[] {
                        zero,
                        one
                    },
                    "Post-read iteration: " + i + ": One has wrong relationships " + threeId );
        }

        //

        log.info( "Collecting garbage and taking memory snapshot 3" );

        collectGarbage();

        long memFree3  = rt.freeMemory();
        long memMax3   = rt.maxMemory();
        long memTotal3 = rt.totalMemory();

        log.debug( "Memory free: " + memFree3 + ", max: " + memMax3 + ", total: " + memTotal3 + ", consumed: " + ( memTotal3 - memFree3 ) );
    }

    /**
     * The number of MeshObjects to create for the test.
     */
    protected int theTestSize = 400;

    // Our Logger
    private static final Log log = Log.getLogInstance(StoreMeshBaseTest11.class );
}
