//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.history;

import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.store.test.RecordingStoreListener;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.FloatValue;
import net.ubos.store.StoreValue;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Tests MeshObjectHistory with multiple changes in a single Transaction
 */
public class StoreMeshObjectHistoryTest3
        extends
            AbstractStoreMeshObjectHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Setting up listeners" );

        RecordingStoreListener<StoreValue>                       headListener    = new RecordingStoreListener<>( this );
        RecordingHistoryStoreListener<StoreValueWithTimeUpdated> historyListener = new RecordingHistoryStoreListener<>( this );

        theMeshObjectStore.addDirectStoreListener( headListener );
        theMeshObjectHistoryStore.addDirectHistoryStoreListener( historyListener );

        //

        log.info( "Creating MeshBase" );

        super.startClock();

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, thePrimaryNsMap )
                                                .historyStores( theMeshObjectHistoryStore, theTransactionHistoryStore )
                                                .build();

        checkEquals( headListener.theSuccessfulPuts.size(),    1, "Wrong home-object only put (current)" );
        checkEquals( historyListener.theSuccessfulPuts.size(), 1, "Wrong home-object only put (history)" );

        headListener.reset();
        historyListener.reset();

        //

        log.info( "Creating graph" );

        mb.execute(
                () -> {
                    MeshObject a = mb.createMeshObject( "aaaa", TestSubjectArea.AA );
                    MeshObject b = mb.createMeshObject( "bbbb", TestSubjectArea.B );

                    a.blessRole( TestSubjectArea.ARANY_S, b );
                    a.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 12.34 ));
                    a.setAttributeValue( "att", "val" );

                    b.setAttributeValue( "btt", "vbl" );
                });

        checkEquals( headListener.theSuccessfulPuts.size(),    2, "Wrong put (current)" );
        checkEquals( headListener.theSuccessfulUpdates.size(), 0, "Wrong update (current)" );
        checkEquals( historyListener.theSuccessfulPuts.size(), 2, "Wrong put (history)" );

        headListener.reset();
        historyListener.reset();

        //

        log.info( "Updating graph" );

        mb.execute(
                () -> {
                    MeshObject a = mb.findMeshObjectByIdentifier( "aaaa" );
                    MeshObject b = mb.findMeshObjectByIdentifier( "bbbb" );

                    a.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 5.6 ));
                });

        checkEquals( headListener.theSuccessfulPuts.size(),    0, "Wrong put (current)" );
        checkEquals( headListener.theSuccessfulUpdates.size(), 1, "Wrong update (current)" );
        checkEquals( historyListener.theSuccessfulPuts.size(), 1, "Wrong put (history)" );

        headListener.reset();
        historyListener.reset();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshObjectHistoryTest3.class );
}
