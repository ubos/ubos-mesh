//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.namespace;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.io.IOException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.ContextualMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.namespace.store.StoreContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.store.test.MySQLDefinitions;
import net.ubos.store.StoreValue;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test that the various caches don't get into each other's way.
 */
public class CacheTest1
        extends
            AbstractTest
{
    /**
     * Setup.
     *
     * @throws IOException all sorts of things may go wrong during tests
     */
    @BeforeEach
    public void setup()
        throws
            IOException
    {
        theDataSource        = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser(         MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword(     MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theMeshObjectStore   = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_MESHOBJECTS_TABLE_NAME );
        thePrimaryNsStore    = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_PRIMARY_NAMESPACES_TABLE_NAME );
        theContextualNsStore = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_CONTEXTUAL_NAMESPACES_TABLE_NAME );
    }

    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        for( String id : new String[] { "aaaa1", "[example.com]foobar" } ) {
            for( int i=0 ; i<8 ; ++i ) {
                runOne( id, ( i & 0x1 ) != 0, ( i & 0x2 ) != 0, ( i & 0x4 ) != 0 );
            }
        }
    }

    /**
     * Set up the data.
     *
     * @param id identifier of the MeshObject to be created
     * @param clearMeshObjects
     * @param clearContextualNs
     * @param clearPrimaryNs
     * @throws Exception all sorts of things can be wrong during tests
     */
    protected void runOne(
            String  id,
            boolean clearMeshObjects,
            boolean clearContextualNs,
            boolean clearPrimaryNs )
        throws
            Exception
    {
        log.info( "Initializing and creating MeshObjects for" + id );

        thePrimaryNsStore.initializeHard();
        theMeshObjectStore.initializeHard();
        theContextualNsStore.initializeHard();

        StorePrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, primaryNsMap ).build();

        mb.execute( () -> {
            mb.createMeshObject( id );
        } );

        //

        log.info( "Clearing caches", clearMeshObjects, clearContextualNs, clearPrimaryNs );

        if( clearMeshObjects ) {
            mb.clearMemoryCache();
        }

        if( clearContextualNs ) {
            ((StoreContextualMeshObjectIdentifierNamespaceMap)((ContextualMeshObjectIdentifierBothSerializer)mb.getPersistenceIdSerializer()).getMap()).getMap().clearLocalCache();
        }

        if( clearPrimaryNs ) {
            primaryNsMap.getLocalNameMap().clearLocalCache();
        }

        //

        log.info(  "Collecting garbage" );

        collectGarbage();

        //

        log.info( "Accessing again" );

        MeshObject found = mb.findMeshObjectByIdentifier( id );
        checkObject( found, "MeshObject not found" );

    }

    /**
     * The Database connection.
     */
    protected MysqlDataSource theDataSource;

    /**
     * The SqlStore for the current MeshObjects.
     */
    protected AbstractSqlStore<StoreValue> theMeshObjectStore;

    /**
     * The SqlStore for the primary namespaces.
     */
    protected AbstractSqlStore<StoreValue> thePrimaryNsStore;

    /**
     * The SqlStore for the contextual namespaces.
     */
    protected AbstractSqlStore<StoreValue> theContextualNsStore;

    // Our Logger
    private static final Log log = Log.getLogInstance( CacheTest1.class );
}
