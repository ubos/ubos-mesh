//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.FloatValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Rollback transaction does not leave stuff in the Store.
 */
public class StoreMeshBaseTest7
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        checkEquals( theMeshObjectStore.size(), 1, "Wrong number of entries in Store" );

        //

        log.info( "Creating Transaction and rolling it back" );

        mb.execute( () -> {
            MeshObject obj1 = mb.createMeshObject( "obj1" );
            MeshObject obj2 = mb.createMeshObject( "obj2", TestSubjectArea.AA );
            obj2.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 1.2f ));

            throw new TransactionActionException.Rollback();
        } );

        //

        checkEquals( theMeshObjectStore.size(), 1, "Wrong number of entries in Store after rolled-back Transaction" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest7.class );
}
