//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.RoleType;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Reproduces a StoreMeshBase integrity problem found 2007-06-22.
 */
public class StoreMeshBaseTest5
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Creating a few objects" );

        mb.execute( ()-> {
            MeshObject obj = mb.createMeshObject( "wsItEtOFGML7KyXCQ0slH6w+Jc9Tw5tY9+kc0TTlz8U=", TestSubjectArea.AA );
            mb.getHomeObject().blessRole( TestSubjectArea.ARANY.getDestination(), obj );
        } );

        //

        log.info( "collecting garbage" );

        collectGarbage();

        //

        log.info( "checking everything's still there" );

        MeshObjectSet objs = mb.getHomeObject().traverseToNeighbors();
        checkEquals( objs.size(), 1, "wrong number neighbors found" );

        RoleType [] rts = mb.getHomeObject().getRoleTypes( objs.getSingleMember() );
        checkEquals( rts.length, 1, "Wrong number of roles" );
        checkEquals( rts[0], TestSubjectArea.ARANY.getDestination(), "Wrong role" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest5.class);
}
