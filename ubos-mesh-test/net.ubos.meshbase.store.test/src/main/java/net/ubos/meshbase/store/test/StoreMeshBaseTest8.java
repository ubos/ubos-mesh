//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.a.AMeshObject;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Deleting and recreating within the same transaction.
 */
public class StoreMeshBaseTest8
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Creating MeshObjects" );

        MeshObjectIdentifier oneId   = mb.meshObjectIdentifierFromExternalForm( "one" );
        MeshObjectIdentifier twoId   = mb.meshObjectIdentifierFromExternalForm( "two" );
        MeshObjectIdentifier threeId = mb.meshObjectIdentifierFromExternalForm( "three" );

        mb.execute( () -> {
            MeshObject one   = mb.createMeshObject( oneId, TestSubjectArea.AA );
            MeshObject two   = mb.createMeshObject( twoId, TestSubjectArea.AA );
            MeshObject three = mb.createMeshObject( threeId, TestSubjectArea.AA );

            two.setRoleAttributeValue( three, "A", "B" ); // just something to relate them
        } );

        mb.clearMemoryCache();
        collectGarbage();

        //

        log.info( "Modifying" );

        mb.execute( () -> {
            MeshObject one   = mb.findMeshObjectByIdentifier( oneId );
            MeshObject three = mb.findMeshObjectByIdentifier( threeId );

            mb.deleteMeshObject( one );
            mb.deleteMeshObject( three );

            MeshObject newOne = mb.createMeshObject( oneId, TestSubjectArea.B );
        } );

        MeshObject two    = mb.findMeshObjectByIdentifier( twoId );
        MeshObject newOne = mb.findMeshObjectByIdentifier( oneId );

        checkEquals( ((AMeshObject)two).getNeighborMeshObjectIdentifiers().length, 0, "leftover neighbors" );
        checkCondition( !newOne.isBlessedBy( TestSubjectArea.AA ), "Still blessed by AA" );
        checkCondition( newOne.isBlessedBy( TestSubjectArea.B ), "Not blessed by B" );

        newOne = null;

        mb.clearMemoryCache();
        collectGarbage();

        newOne = mb.findMeshObjectByIdentifier( oneId );
        checkCondition( !newOne.isBlessedBy( TestSubjectArea.AA ), "After restore: still blessed by AA" );
        checkCondition( newOne.isBlessedBy( TestSubjectArea.B ), "After restore: not blessed by B" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest8.class );
}
