//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.MeshObjectIdentifierNotUniqueException;
import net.ubos.mesh.RoleTypeBlessedAlreadyException;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Rollbacks.
 */
public class StoreMeshBaseTest9
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Creating MeshObjects" );

        MeshObjectIdentifier oneId   = mb.meshObjectIdentifierFromExternalForm( "one" );
        MeshObjectIdentifier twoId   = mb.meshObjectIdentifierFromExternalForm( "two" );
        MeshObjectIdentifier threeId = mb.meshObjectIdentifierFromExternalForm( "three" );

        mb.execute( () -> {
            MeshObject one   = mb.createMeshObject( oneId, TestSubjectArea.AA );
            MeshObject two   = mb.createMeshObject( twoId, TestSubjectArea.AA );

            one.blessRole( TestSubjectArea.AR1A.getSource(), two );
        } );


        checkEquals( mb.size(), 3, "Wrong number of MeshObjects in MeshBase" );

        mb.clearMemoryCache();
        collectGarbage();

        checkEquals( mb.size(), 3, "Wrong number of MeshObjects in MeshBase" );

        //

        log.info( "Modifying (1)" );

        mb.execute( () -> {
            try {
                mb.createMeshObject( threeId );
                mb.createMeshObject( oneId, TestSubjectArea.B );

                reportError( "MeshObjectIdentifierNotUniqueException not thrown" );

            } catch( MeshObjectIdentifierNotUniqueException ex ) {
                // good
            } finally {
                throw new TransactionActionException.Rollback();
            }
        } );
        mb.clearMemoryCache();
        collectGarbage();

        MeshObject one   = mb.findMeshObjectByIdentifier( oneId );
        MeshObject two   = mb.findMeshObjectByIdentifier( twoId );
        MeshObject three = mb.findMeshObjectByIdentifier( threeId );

        checkObject( one, "MeshObject one does not exist" );
        checkObject( two, "MeshObject two does not exist" );
        checkNotObject( three, "MeshObject three exists" );
        checkCondition( one.isBlessedBy( TestSubjectArea.AA ), "One not blessed with AA" );
        checkCondition( two.isBlessedBy( TestSubjectArea.AA ), "Two not blessed with AA" );
        checkCondition( !two.isBlessedBy( TestSubjectArea.B ), "Two blessed with B" );
        checkEquals( mb.size(), 3, "Wrong number of MeshObjects in MeshBase" );

        //

        log.info( "Modifying (3)" );

        mb.execute( () -> {
            try {
                MeshObject innerOne = mb.findMeshObjectByIdentifier( oneId );
                MeshObject innerTwo = mb.findMeshObjectByIdentifier( twoId );
                mb.createMeshObject( threeId );
                innerOne.blessRole( TestSubjectArea.AR1A.getSource(), innerTwo );

                reportError( "RoleTypeBlessedAlreadyException not thrown" );

            } catch( RoleTypeBlessedAlreadyException ex ) {
                // good
            } finally {
                throw new TransactionActionException.Rollback();
            }
        } );

        checkObject( one, "MeshObject one does not exist" );
        checkObject( two, "MeshObject two does not exist" );
        checkNotObject( three, "MeshObject three exists" );
        checkCondition( one.isBlessedBy( TestSubjectArea.AA ), "One not blessed with AA" );
        checkCondition( two.isBlessedBy( TestSubjectArea.AA ), "Two not blessed with AA" );
        checkCondition( !two.isBlessedBy( TestSubjectArea.B ), "Two blessed with B" );
        checkCondition( one.isRelated( TestSubjectArea.AR1A.getSource(), two ), "not related and blessed" );
        checkEquals( mb.size(), 3, "Wrong number of MeshObjects in MeshBase" );

        one = null;
        two = null;
        mb.clearMemoryCache();
        collectGarbage();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest9.class );
}
