//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.namespace;

import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the default namespace in a MeshBase.
 */
public class MeshObjectIdentifierContextualNamespaceTest1
        extends
            AbstractMeshObjectIdentifierNamespaceTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        create( primaryNsMap );

        restore( primaryNsMap );
    }

    /**
     * Factored out first portion of the test.
     *
     * @param primaryNsMap the primary map to use
     */
    protected void create(
            PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap )
    {
        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, primaryNsMap ).build();

        //

        log.info(  "Creating MeshObjects" );

        mb.execute( () -> {
            mb.createMeshObject( "abc" );
        } );

        checkEquals( countRemaining( primaryNsMap.localNameIterator() ), 1, "Wrong number of namespaces" );
        checkEquals( countRemaining( theContextualNsStore.getIterator() ), 2 * 1, "Wrong number of rows" );
        // 2 * 1: one forward, one reverse

        //

        mb.die( false );
    }

    /**
     * Factored out second portion of the test.
     *
     * @param primaryNsMap the primary map to use
     * @throws ParseException thrown if parsing failed
     */
    protected void restore(
            PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap )
        throws
            ParseException
    {
        log.info( "Switching to new MeshBase" );

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, primaryNsMap ).build();

        //

        log.info( "Checking namespace map" );

        checkEquals( countRemaining( primaryNsMap.localNameIterator()), 1, "Wrong number of namespaces" );
        checkEquals( countRemaining( theContextualNsStore.getIterator() ), 2 * 1, "Wrong number of rows" );
        // 2 * 1: one forward, one reverse

        MeshObject found = mb.findMeshObjectByIdentifier( "abc" );
        checkObject( found, "failed to restore" );

        checkEquals( countRemaining( primaryNsMap.localNameIterator()), 1, "Wrong number of namespaces" );
        checkEquals( countRemaining( theContextualNsStore.getIterator() ), 2 * 1, "Wrong number of rows" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MeshObjectIdentifierContextualNamespaceTest1.class );
}
