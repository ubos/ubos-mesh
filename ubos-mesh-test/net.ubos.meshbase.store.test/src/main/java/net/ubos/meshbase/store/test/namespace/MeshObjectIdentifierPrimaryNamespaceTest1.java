//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.namespace;

import java.util.Map;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests creating and restoring a StorePrimaryMeshObjectIdentifierNamespaceMap.
 */
public class MeshObjectIdentifierPrimaryNamespaceTest1
        extends
            AbstractMeshObjectIdentifierNamespaceTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        populate();
        restore();
    }

    protected void populate()
    {
        log.info( "Create StorePrimaryMeshObjectIdentifierNamespaceMap" );

        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        //

        log.info( "Initial checks" );

        checkObject( primaryNsMap.getPrimary(), "No primary namespace" );
        checkEquals( countRemaining( primaryNsMap.localNameIterator() ), 1, "Wrong number of local names" );

        //

        log.info( "Populating" );

        for( Map.Entry<String,String[]> current : initial.entrySet() ) {
            MeshObjectIdentifierNamespace ns = primaryNsMap.obtainByExternalName( current.getKey() );

            checkObject( ns, "No namespace for: " + current.getKey() );

            for( String current2 : current.getValue() ) {
                ns.addExternalName( current2 );
            }
        }

        //

        log.info( "Checking initial content" );

        checkObject( primaryNsMap.getPrimary(), "No primary namespace" );
        checkEquals( countRemaining( primaryNsMap.localNameIterator() ), 4, "Wrong number of local names" );

        for( Map.Entry<String,String[]> current : initial.entrySet() ) {
            MeshObjectIdentifierNamespace ns = primaryNsMap.findByExternalName( current.getKey() );

            checkObject( ns, "No namespace for: " + current.getKey() );

            checkEquals( ns.getPreferredExternalName(), current.getKey(), "Not the correct preferred name" );

            for( String current2 : current.getValue() ) {
                MeshObjectIdentifierNamespace ns2 = primaryNsMap.findByExternalName( current2 );

                checkIdentity( ns, ns2, "Not the same namespace" );
            }
        }

        checkIntegrity( primaryNsMap );
    }

    protected void restore()
    {
        log.info( "Recreate StorePrimaryMeshObjectIdentifierNamespaceMap" );

        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        log.info( "Checking restored content" );

        checkObject( primaryNsMap.getPrimary(), "No primary namespace" );
        checkEquals( countRemaining( primaryNsMap.localNameIterator() ), 4, "Wrong number of local names" );

        for( Map.Entry<String,String[]> current : initial.entrySet() ) {
            MeshObjectIdentifierNamespace ns = primaryNsMap.findByExternalName( current.getKey() );

            checkObject( ns, "No namespace for: " + current.getKey() );

            checkEquals( ns.getPreferredExternalName(), current.getKey(), "Not the correct preferred name" );

            for( String current2 : current.getValue() ) {
                MeshObjectIdentifierNamespace ns2 = primaryNsMap.findByExternalName( current2 );

                checkIdentity( ns, ns2, "Not the same namespace" );
            }
        }

        checkIntegrity( primaryNsMap );
    }

    /**
     * Factored out.
     *
     * @param primaryNsMap
     */
    protected void checkIntegrity(
            PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap )
    {
        log.info( "Checking local name iterator" );

        for( String localName : primaryNsMap.localNameIterator()) {
            checkObject( primaryNsMap.findByLocalName( localName ), "No namespace for local name: " + localName );
        }

        //

        log.info( "Checking traversal from external and local names" );

        for( String localName : primaryNsMap.localNameIterator() ) {

            MeshObjectIdentifierNamespace ns = primaryNsMap.findByLocalName( localName );
            checkObject( localName, "No namespace for: " + localName );

            String localName2 = primaryNsMap.getLocalNameFor( ns );

            checkEquals( localName, localName2, "Not pointing back: " + localName + " " + localName2 + " " + ns );
        }
    }
    /**
     * Test data.
     */
    protected static Map<String,String[]> initial = Map.of(
            "one",   new String[] { "one1", "one2" },
            "two",   new String[] { "two1" },
            "three", new String[] { "three1", "three2", "three3" } );

    // Our Logger
    private static final Log log = Log.getLogInstance(MeshObjectIdentifierPrimaryNamespaceTest1.class );
}
