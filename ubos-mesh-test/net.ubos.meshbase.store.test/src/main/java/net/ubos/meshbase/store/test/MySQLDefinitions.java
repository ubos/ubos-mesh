//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

/**
 * Defines the names of the MySQL databases and tables for the tests in this module
 * (not just package).
 */
public interface MySQLDefinitions
{
    /**
     * The name of the database that we use to store test data.
     */
    public static final String TEST_DATABASE_NAME = "test";

    /**
     * The name of the database user.
     */
    public static final String TEST_DATABASE_USER = "test";

    /**
     * The database users' password.
     */
    public static final String TEST_DATABASE_USER_PASSWORD = "test";

    /**
     * The name of the table that we use to store test MeshObjects.
     */
    public static final String TEST_MESHOBJECTS_TABLE_NAME = "SqlStoreMeshBaseMeshObjectsTest";

    /**
     * The name of the table that we use to store primary namespaces.
     */
    public static final String TEST_PRIMARY_NAMESPACES_TABLE_NAME = "SqlStoreMeshBasePrimaryNamespacesTest";

    /**
     * The name of the table that we use to store contextual namespaces.
     */
    public static final String TEST_CONTEXTUAL_NAMESPACES_TABLE_NAME = "SqlStoreMeshBaseContextualNamespacesTest";

    /**
     * The name of the table that we use to store MeshObjectHistories.
     */
    public static final String TEST_TRANSACTION_HISTORY_TABLE_NAME = "SqlStoreTransactionHistoryTest";

    /**
     * The name of the table that we use to store the transaction log.
     */
    public static final String TEST_MESH_OBJECT_HISTORY_TABLE_NAME = "SqlStoreMeshObjectHistoryTest";
}
