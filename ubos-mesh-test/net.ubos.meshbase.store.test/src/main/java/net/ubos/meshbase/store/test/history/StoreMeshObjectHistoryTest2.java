//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.history;

import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Tests MeshObjectHistory on a single MeshObject.
 */
public class StoreMeshObjectHistoryTest2
        extends
            AbstractStoreMeshObjectHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, thePrimaryNsMap )
                                                .historyStores( theMeshObjectHistoryStore, theTransactionHistoryStore )
                                                .build();

        //

        super.startClock();

        log.info( "Creating and modifying a MeshObject" );

        long [] timeUpdateds = new long[ 10 ];

        MeshObject obj = mb.execute(
                ( Transaction tx ) -> mb.createMeshObject( "AAA" ));

        timeUpdateds[0] = obj.getTimeUpdated();

        for( int i=1 ; i<timeUpdateds.length ; ++i ) {
            final int ii = i; // silly language
            sleepUntil( 100 * i );
            mb.execute(
                () -> {
                    obj.setAttributeValue( "ATT", ii );
                });
            timeUpdateds[i] = obj.getTimeUpdated();
       }

        //

        collectGarbage();

        //

        log.info( "Accessing history" );

        MeshObjectHistory objHistory = mb.meshObjectHistory( obj.getIdentifier() );
        checkObject( objHistory, "No history found" );

        checkEquals( objHistory.getLength(), timeUpdateds.length, "wrong length of history" );

        //

        collectGarbage();

        //

        log.info( "Checking default position and length of history" );

        HistoryCursorIterator<MeshObject> iter = objHistory.iterator();
        iter.moveToAfterLast();

        // we are at the end of the meshObjectHistory
        checkCondition( !iter.hasNext(), "Has next(1)" );
        checkCondition( iter.hasPrevious( timeUpdateds.length ), "Does not have enough previous" );
        checkCondition( !iter.hasPrevious( timeUpdateds.length+1 ), "Has too many previous" );

        checkEquals( iter.peekPrevious().getTimeUpdated(), obj.getTimeUpdated(), "Not the head object" );

        //

        log.info( "Checking history sequence" );

        iter.moveToBeforeFirst();

        MeshObject previous = iter.next();

        for( int i=1 ; i<timeUpdateds.length ; ++i ) {
            MeshObject current = iter.next();
            checkCondition( previous.getTimeUpdated() < current.getTimeUpdated(), "out of sequence: " + i );

            previous = current;
        }

        //

        log.info( "Checking moving to time" );

        for( int i=0 ; i<timeUpdateds.length ; ++i ) {
            iter.moveToJustBeforeTime( timeUpdateds[i] );

            if( i==0 ) {
                checkCondition( !iter.hasPrevious(), "Wrong hasPrevious (1a) at index " + i );

            } else {
                checkCondition( iter.hasPrevious(), "Wrong hasPrevious (1b) at index " + i );
                previous = iter.peekPrevious();
                checkEquals( previous.getTimeUpdated(), timeUpdateds[i-1], "Wrong timeUpdated of previous at index " + i );
            }
            checkCondition( iter.hasNext(), "Wrong hasNext (1) at index " + i );
            MeshObject next = iter.peekNext();
            checkEquals( next.getTimeUpdated(), timeUpdateds[i], "Wrong timeUpdated of next at index " + i );
        }

        for( int i=0 ; i<timeUpdateds.length ; ++i ) {
            iter.moveToJustAfterTime( timeUpdateds[i] );

            if( i== timeUpdateds.length-1 ) {
                checkCondition( !iter.hasNext(), "Wrong hasNext (2a) at index " + i );

            } else {
                checkCondition( iter.hasNext(), "Wrong hasNext (2b) at index " + i );
                MeshObject next = iter.peekNext();
                checkEquals( next.getTimeUpdated(), timeUpdateds[i+1], "Wrong timeUpdated of next at index " + i );
            }
            checkCondition( iter.hasPrevious(), "Wrong hasPrevious (2) at index " + i );
            previous = iter.peekPrevious();
            checkEquals( previous.getTimeUpdated(), timeUpdateds[i], "Wrong timeUpdated of previous at index " + i );
        }

        //

        log.info( "Checking next and previous arrays from the middle" );
        int middle = timeUpdateds.length * 2 / 3; // not exactly the middle

        iter.moveToJustBeforeTime( timeUpdateds[ middle ] );

        List<MeshObject> beforeMiddle = iter.previous( timeUpdateds.length );

        iter.moveToJustBeforeTime( timeUpdateds[ middle ] );

        List<MeshObject> afterMiddle  = iter.next( timeUpdateds.length );

        checkEquals( beforeMiddle.size(), middle, "Before has wrong size" );
        checkEquals( afterMiddle.size(), timeUpdateds.length - middle, "After has wrong size" );
        checkEquals( beforeMiddle.size() + afterMiddle.size(), timeUpdateds.length, "Before and after together not whole" );

        for( int i=0 ; i<middle ; ++i ) {
            // reverse direction
            checkEquals( beforeMiddle.get( middle-1-i ).getTimeUpdated(), timeUpdateds[i], "Wrong before element at location " + i );
        }
        for( int i=middle ; i<timeUpdateds.length ;  ++i ) {
            checkEquals( afterMiddle.get( i-middle ).getTimeUpdated(), timeUpdateds[i], "Wrong before element at location " + i );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshObjectHistoryTest2.class );
}
