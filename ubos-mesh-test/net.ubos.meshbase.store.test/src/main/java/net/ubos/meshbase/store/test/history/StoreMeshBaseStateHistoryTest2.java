//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.history;

import net.ubos.meshbase.MeshBaseView;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Creates MeshBaseViews (but does not walk them) from the start to the present.
 *
 * See MeshBaseHistoryTest2.
 */
public class StoreMeshBaseStateHistoryTest2
        extends
            AbstractStoreMeshBaseHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long delta = 500;

        startClock();

        //

        log.info( "Running a few transactions" );

        sleepUntil( delta );

        theMeshBase.execute( (tx) -> {
            return tx.getMeshBase().createMeshObject( "obj-1" );
        } );

        sleepUntil( delta*2 );

        theMeshBase.execute( (tx) -> {
            return tx.getMeshBase().createMeshObject( "obj-2" );
        } );

        sleepUntil( delta*3 );

        theMeshBase.execute( (tx) -> {
            return tx.getMeshBase().createMeshObject( "obj-3" );
        } );

        //

        collectGarbage();

        //

        log.info( "Creating MeshBaseView at the start" );

        HistoryCursorIterator<MeshBaseState> iter = theMeshBase.getHistory().iterator();
        iter.moveToBeforeFirst();
        MeshBaseState state1 = iter.next();

        MeshBaseView view1 = state1.getMeshBaseView();

        checkEquals( view1.size(), 1, "Wrong number of MeshObjects at the beginning" );

        //

        log.info( "Moving forward" );

        MeshBaseState state2 = iter.next();

        MeshBaseView view2 = state2.getMeshBaseView();

        checkEquals( view2.size(), 2, "Wrong number of MeshObjects at step 2" );

        //

        log.info( "Moving forward again" );

        MeshBaseState state3 = iter.next();

        MeshBaseView view3 = state3.getMeshBaseView();

        checkEquals( view3.size(), 3, "Wrong number of MeshObjects at step 3" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(StoreMeshBaseStateHistoryTest2.class);
}
