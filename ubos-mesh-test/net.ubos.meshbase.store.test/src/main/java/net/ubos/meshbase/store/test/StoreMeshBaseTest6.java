//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import java.util.Date;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.ColorValue;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.ExtentValue;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PointValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimePeriodValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests PropertyValue serialization.
 */
public class StoreMeshBaseTest6
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Creating test objects" );

        mb.execute( () -> {
            for( int i=0 ; i<testData.length ; ++i ) {
                DataSet test = testData[i];

                MeshObject obj = mb.createMeshObject( test.theIdentifier, TestSubjectArea.OPTIONALPROPERTIES );

                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEANY,         test.theBlobAny );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEIMAGE,       test.theBlobImage );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEJPG,         test.theBlobJpg );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEPLAIN,       test.theBlobPlain );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEPLAINORHTML, test.theBlobHtml );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBOOLEANDATATYPE,         test.theBoolean );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALCOLORDATATYPE,           test.theColor );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALCURRENCYDATATYPE,        test.theCurrency );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALENUMERATEDDATATYPE,      test.theEnumerated );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALEXTENTDATATYPE,          test.theExtent );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALFLOATDATATYPE,           test.theFloat );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALINTEGERDATATYPE,         test.theInteger );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALMULTIPLICITYDATATYPE,    test.theMultiplicity );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALPOINTDATATYPE,           test.thePoint );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALSTRINGDATATYPE,          test.theString );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALTIMEPERIODDATATYPE,      test.theTimePeriod );
                obj.setPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALTIMESTAMPDATATYPE,       test.theTimeStamp );
            }
        } );

        //

        log.info( "collecting garbage" );

        collectGarbage();

        //

        log.info( "Recovering and comparing test objects" );

        for( int i=0 ; i<testData.length ; ++i ) {
            DataSet test = testData[i];

            MeshObjectIdentifier identifier = mb.meshObjectIdentifierFromExternalForm( test.theIdentifier );
            MeshObject obj = mb.findMeshObjectByIdentifier( identifier );

            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEANY ),         test.theBlobAny,      "Wrong Blob/any value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEIMAGE ),       test.theBlobImage,    "Wrong Blob/image value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEJPG ),         test.theBlobJpg,      "Wrong Blob/jpg value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEPLAIN ),       test.theBlobPlain,    "Wrong Blob/plain value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBLOBDATATYPEPLAINORHTML ), test.theBlobHtml,     "Wrong Blob/html value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALBOOLEANDATATYPE ),         test.theBoolean,      "Wrong Boolean value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALCOLORDATATYPE ),           test.theColor,        "Wrong Color value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALCURRENCYDATATYPE ),        test.theCurrency,     "Wrong Currency value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALENUMERATEDDATATYPE ),      test.theEnumerated,   "Wrong Enumerated value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALEXTENTDATATYPE ),          test.theExtent,       "Wrong Extent value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALFLOATDATATYPE ),           test.theFloat,        "Wrong Float value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALINTEGERDATATYPE ),         test.theInteger,      "Wrong Integer value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALMULTIPLICITYDATATYPE ),    test.theMultiplicity, "Wrong Multiplicity value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALPOINTDATATYPE ),           test.thePoint,        "Wrong Point value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALSTRINGDATATYPE ),          test.theString,       "Wrong String value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALTIMEPERIODDATATYPE ),      test.theTimePeriod,   "Wrong TimePeriod value" );
            checkEquals( obj.getPropertyValue( TestSubjectArea.OPTIONALPROPERTIES_OPTIONALTIMESTAMPDATATYPE ),       test.theTimeStamp,    "Wrong TimeStamp value" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest6.class );

    /**
     * Test data.
     */
    protected static DataSet [] testData;
    static {
        // This way we can catch exceptions
        try {
            testData = new DataSet[] {
                new DataSet(
                        "dataset1",
                        BlobDataType.theAnyType.createBlobValue(                "Simple", SelectableMimeType.TEXT_HTML.getMimeType() ),
                        BlobDataType.theJdkSupportedBitmapType.createBlobValue( new byte[] { 1, 2, 3 }, SelectableMimeType.IMAGE_JPEG.getMimeType() ),
                        BlobDataType.theJpgType.createBlobValue(                new byte[] { 4, 5, 6 }, SelectableMimeType.IMAGE_JPEG.getMimeType() ),
                        BlobDataType.theTextPlainType.createBlobValue(          "Simple", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                        BlobDataType.theTextAnyType.createBlobValue(            "Simple", SelectableMimeType.TEXT_HTML.getMimeType() ),
                        BooleanValue.create( true ),
                        ColorValue.create( 123 ),
                        CurrencyValue.parseCurrencyValue( "0.01 USD" ),
                        TestSubjectArea.OPTIONALPROPERTIES_OPTIONALENUMERATEDDATATYPE_type.select( "Value1" ),
                        ExtentValue.create( 12.34, 56.78 ),
                        FloatValue.create( 12.34 ),
                        IntegerValue.create( 123 ),
                        MultiplicityValue.create( 0, 17),
                        PointValue.create( 12.34, 56.78 ),
                        StringValue.create( "Simple String" ),
                        TimePeriodValue.create( (short) 1, (short) 2, (short) 3, (short) 4, (short) 5, (short) 6 ),
                        TimeStampValue.create( 1L ) ),
                new DataSet(
                        "dataset2",
                        BlobDataType.theAnyType.createBlobValue(                "An <b>important&trade;</b>&#33; HTML String", SelectableMimeType.TEXT_HTML.getMimeType() ),
                        BlobDataType.theJdkSupportedBitmapType.createBlobValue( new byte[] { 111, 112, 113 }, SelectableMimeType.IMAGE_JPEG.getMimeType() ),
                        BlobDataType.theJpgType.createBlobValue(                new byte[] { 114, 115, 116 }, SelectableMimeType.IMAGE_JPEG.getMimeType() ),
                        BlobDataType.theTextPlainType.createBlobValue(          "An <b>important&trade;</b>&#33; HTML String", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                        BlobDataType.theTextAnyType.createBlobValue(            "An <b>important&trade;</b>&#33; HTML String", SelectableMimeType.TEXT_HTML.getMimeType() ),
                        BooleanValue.create( false ),
                        ColorValue.create( 255, 255, 255, 255 ),
                        CurrencyValue.parseCurrencyValue( "1234567890\t USD" ),
                        TestSubjectArea.OPTIONALPROPERTIES_OPTIONALENUMERATEDDATATYPE_type.select( "Value2" ),
                        ExtentValue.create( -12.34, 56.78 ),
                        FloatValue.create( -12.34 ),
                        IntegerValue.create( -123 ),
                        MultiplicityValue.create( 17, 19 ),
                        PointValue.create( -12.34, -56.78 ),
                        StringValue.create( "An <b>important&trade;</b>&#33; HTML String" ),
                        TimePeriodValue.create( (short) 2999, (short) 12, (short) 31, (short) 23, (short) 59, 59.999f ),
                        TimeStampValue.create( 1L ) ),
                new DataSet(
                        "dataset3",
                        BlobDataType.theAnyType.createBlobValue(                "An <foo:bar>XML</foo:bar> String <!CDATA[with a CDATA]]> section.", SelectableMimeType.TEXT_HTML.getMimeType() ),
                        BlobDataType.theJdkSupportedBitmapType.createBlobValue( new byte[] { -111, -112, 113 }, SelectableMimeType.IMAGE_JPEG.getMimeType() ),
                        BlobDataType.theJpgType.createBlobValue(                new byte[] { -114, -115, 116 }, SelectableMimeType.IMAGE_JPEG.getMimeType() ),
                        BlobDataType.theTextPlainType.createBlobValue(          "An <foo:bar>XML</foo:bar> String <!CDATA[with a CDATA]]> section.", SelectableMimeType.TEXT_PLAIN.getMimeType() ),
                        BlobDataType.theTextAnyType.createBlobValue(            "An <foo:bar>XML</foo:bar> String <!CDATA[with a CDATA]]> section.", SelectableMimeType.TEXT_HTML.getMimeType() ),
                        null,
                        ColorValue.create( 1378 ),
                        CurrencyValue.parseCurrencyValue( ".1EUR" ),
                        TestSubjectArea.OPTIONALPROPERTIES_OPTIONALENUMERATEDDATATYPE_type.select( "Value3" ),
                        ExtentValue.create( Double.MAX_VALUE, 0 ),
                        FloatValue.create( Double.MIN_VALUE ),
                        IntegerValue.create( Integer.MAX_VALUE ),
                        MultiplicityValue.ZERO_N,
                        PointValue.create( Double.MIN_VALUE, -Double.MAX_VALUE ),
                        StringValue.create( "An <foo:bar>XML</foo:bar> String <!CDATA[with a CDATA]]> section." ),
                        null,
                        TimeStampValue.create( new Date() ) ),
            };
        } catch( Throwable t ) {
            log.error( t );
        }
    }

    /**
     * Captures one test case.
     */
    protected static class DataSet
    {
        /**
         * Constructor.
         *
         * @param identifier the identifier
         * @param tBlobAny a BlobValue
         * @param tBlobImage a BlobValue
         * @param tBlobJpg a BlobValue
         * @param tBlobPlain a BlobValue
         * @param tBlobHtml a BlobValue
         * @param tBoolean the BooleanValue
         * @param tColor the ColorValue
         * @param tCurrency the CurrencyValue
         * @param tEnumerated the EnumeratedValue
         * @param tExtent the ExtentValue
         * @param tFloat the FloatValue
         * @param tInteger the IntegerValue
         * @param tMultiplicity the MultiplicityValue
         * @param tPoint the PointValue
         * @param tString the StringValue
         * @param tTimePeriod the TimePeriodValue
         * @param tTimeStamp the TimeStampValue
         */
        DataSet(
                String            identifier,
                BlobValue         tBlobAny,
                BlobValue         tBlobImage,
                BlobValue         tBlobJpg,
                BlobValue         tBlobPlain,
                BlobValue         tBlobHtml,
                BooleanValue      tBoolean,
                ColorValue        tColor,
                CurrencyValue     tCurrency,
                EnumeratedValue   tEnumerated,
                ExtentValue       tExtent,
                FloatValue        tFloat,
                IntegerValue      tInteger,
                MultiplicityValue tMultiplicity,
                PointValue        tPoint,
                StringValue       tString,
                TimePeriodValue   tTimePeriod,
                TimeStampValue    tTimeStamp )
        {
            theIdentifier = identifier;

            theBlobAny      = tBlobAny;
            theBlobImage    = tBlobImage;
            theBlobJpg      = tBlobJpg;
            theBlobPlain    = tBlobPlain;
            theBlobHtml     = tBlobHtml;
            theBoolean      = tBoolean;
            theColor        = tColor;
            theCurrency     = tCurrency;
            theEnumerated   = tEnumerated;
            theExtent       = tExtent;
            theFloat        = tFloat;
            theInteger      = tInteger;
            theMultiplicity = tMultiplicity;
            thePoint        = tPoint;
            theString       = tString;
            theTimePeriod   = tTimePeriod;
            theTimeStamp    = tTimeStamp;
        }

        String   theIdentifier;

        BlobValue         theBlobAny;
        BlobValue         theBlobImage;
        BlobValue         theBlobJpg;
        BlobValue         theBlobPlain;
        BlobValue         theBlobHtml;
        BooleanValue      theBoolean;
        ColorValue        theColor;
        CurrencyValue     theCurrency;
        EnumeratedValue   theEnumerated;
        ExtentValue       theExtent;
        FloatValue        theFloat;
        IntegerValue      theInteger;
        MultiplicityValue theMultiplicity;
        PointValue        thePoint;
        StringValue       theString;
        TimePeriodValue   theTimePeriod;
        TimeStampValue    theTimeStamp;
    }
}
