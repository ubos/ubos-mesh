//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.namespace;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that multiple MeshObjectIdentifierNamespaces are honored when restoring.
 */
public class MeshObjectIdentifierContextualNamespaceTest2
        extends
            AbstractMeshObjectIdentifierNamespaceTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        create( primaryNsMap );

        restore( primaryNsMap );
    }

    /**
     * Factored out first portion of the test.
     *
     * @param primaryNsMap the primary map to use
     */
    protected void create(
            PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap )
    {
        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, primaryNsMap ).build();

        //

        log.info( "Creating MeshObjects" );

        for( String ns : namespaces ) {
            for( String local : localIds ) {
                mb.execute( () -> {
                    String externalForm;
                    if( ns == null ) {
                        externalForm = local;
                    } else {
                        externalForm = ns + "#" + local;
                    }
                    MeshObjectIdentifier moId = mb.meshObjectIdentifierFromExternalForm( externalForm );

                    MeshObject current = mb.findMeshObjectByIdentifier( moId );
                    if( current == null ) {
                        current = mb.createMeshObject( moId );
                    }
                    current.setAttributeValue( "ns",    ns );
                    current.setAttributeValue( "local", local );
                } );
            }
        }

        checkEquals( countRemaining( primaryNsMap.localNameIterator() ), namespaces.length, "Wrong number of namespaces" );

        //

        mb.die( false );
    }

    /**
     * Factored out second portion of the test
     *
     * @param primaryNsMap the primary map to use
     */
    protected void restore(
            PrimaryMeshObjectIdentifierNamespaceMap primaryNsMap )
    {
        log.info( "Switching to new MeshBase" );

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, primaryNsMap ).build();

        //

        log.info( "Checking namespace map" );

        checkEquals( countRemaining( primaryNsMap.localNameIterator() ), namespaces.length, "Wrong number of namespaces" );

        for( String nsName : namespaces ) {
            if( nsName != null ) {
                MeshObjectIdentifierNamespace ns = primaryNsMap.findByExternalName( nsName );

                Set<String> externalNames = ns.getExternalNames();
                checkEquals( externalNames.size(), 1, "Too many external names" );

                checkEquals( externalNames.iterator().next(), nsName, "Inconsistent naming" );
            }
        }

        //

        log.info( "Checking MeshObjects" );

        HashMap<String,Set<String>> found = new HashMap<>();

        for( MeshObject current : mb ) {
            MeshObjectIdentifier id = current.getIdentifier();

            MeshObjectIdentifierNamespace ns            = id.getNamespace();
            String                        localId       = id.getLocalId();
            Set<String>                   externalNames = ns.getExternalNames();

            String nsName;
            if( ns == mb.getDefaultNamespace()) {
                checkEquals( externalNames.size(), 0, "wrong size" );

                nsName = null;

            } else {
                checkEquals( externalNames.size(), 1, "wrong size" );

                nsName = externalNames.iterator().next();
            }
            Set<String> found2 = found.get( nsName );
            if( found2 == null ) {
                found2 = new HashSet<>();
                found.put( nsName, found2 );
            }
            checkCondition( found2.add( localId ), "Already there" );
        }

        checkEquals( found.size(), namespaces.length, "Wrong number of namespaces" );
        for( Map.Entry<String,Set<String>> current : found.entrySet() ) {
            checkEquals( current.getValue().size(), localIds.length, "Wrong number of objects for " + current.getKey() );
        }
    }

    /**
     * The namespaces we try.
     */
    protected static final String [] namespaces = {
            null, // local
            "facebook.com",
            "=ubos",
            "did:some:where"
    };

    /**
     * The local IDs we try.
     */
    protected static final String [] localIds = {
            "", // home object
            "abc",
            "abc/def",
    };


    // Our Logger
    private static final Log log = Log.getLogInstance( MeshObjectIdentifierContextualNamespaceTest2.class );
}
