//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.io.IOException;
import net.ubos.store.StoreValue;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality of StoreMeshBaseTests.
 */
public abstract class AbstractStoreMeshBaseTest
        extends
            AbstractTest
{
    /**
     * Setup.
     *
     * @throws IOException all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            IOException
    {
        theDataSource = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser(         MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword(     MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theMeshObjectStore   = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_MESHOBJECTS_TABLE_NAME );
        thePrimaryNsStore    = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_PRIMARY_NAMESPACES_TABLE_NAME );
        theContextualNsStore = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_CONTEXTUAL_NAMESPACES_TABLE_NAME );

        thePrimaryNsStore.initializeHard();
        theMeshObjectStore.initializeHard();
        theContextualNsStore.initializeHard();
    }

    /**
     * The Database connection.
     */
    protected MysqlDataSource theDataSource;

    /**
     * The Store for the MeshObjects to be tested.
     */
    protected AbstractSqlStore<StoreValue> theMeshObjectStore;

    /**
     * The Store for the primary namespaces
     */
    protected AbstractSqlStore<StoreValue> thePrimaryNsStore;

    /**
     * The Store for the MeshBase's contextual namespaces
     */
    protected AbstractSqlStore<StoreValue> theContextualNsStore;
}
