//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.model.primitives.FloatValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Default values are not re-set upon restore.
 */
public class StoreMeshBaseTest10
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        log.info( "Creating MeshObjects" );

        MeshObjectIdentifier ID1 = mb.meshObjectIdentifierFromExternalForm( "object1" );
        MeshObjectIdentifier ID2 = mb.meshObjectIdentifierFromExternalForm( "object2" );

        mb.execute( () -> {
            MeshObject obj1 = mb.createMeshObject( ID1, TestSubjectArea.AA );
            MeshObject obj2 = mb.createMeshObject( ID2, TestSubjectArea.AA );

            obj2.setPropertyValue( TestSubjectArea.AA_Y, FloatValue.create( 1.11 ));
        } );

        MeshObject obj1 = mb.findMeshObjectByIdentifier( ID1 );
        MeshObject obj2 = mb.findMeshObjectByIdentifier( ID2 );

        checkEquals( obj1.getPropertyValue( TestSubjectArea.AA_Y ), FloatValue.create( 12.34 ), "obj1 has wrong value" );
        checkEquals( obj2.getPropertyValue( TestSubjectArea.AA_Y ), FloatValue.create(  1.11 ), "obj2 has wrong value" );

        //

        log.info( "Clearing cache, and loading MeshObjects again" );

        obj1 = null;
        obj2 = null;

        mb.clearMemoryCache();

        obj1 = mb.findMeshObjectByIdentifier( ID1 );
        obj2 = mb.findMeshObjectByIdentifier( ID2 );

        //

        log.info( "Checking property values" );

        checkEquals( obj1.getPropertyValue( TestSubjectArea.AA_Y ), FloatValue.create( 12.34 ), "obj1 has wrong value" );
        checkEquals( obj2.getPropertyValue( TestSubjectArea.AA_Y ), FloatValue.create(  1.11 ), "obj2 has wrong value" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest10.class );
}
