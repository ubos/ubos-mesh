//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.store.test.RecordingStoreListener;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.store.StoreValue;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests MeshObjectHistory on a single MeshObject.
 */
public class StoreMeshObjectHistoryTest1
        extends
            AbstractStoreMeshObjectHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Setting up listeners" );

        RecordingStoreListener<StoreValue>                       headListener    = new RecordingStoreListener<>( this );
        RecordingHistoryStoreListener<StoreValueWithTimeUpdated> historyListener = new RecordingHistoryStoreListener<>( this );

        theMeshObjectStore.addDirectStoreListener( headListener );
        theMeshObjectHistoryStore.addDirectHistoryStoreListener( historyListener );

        //

        log.info( "Creating MeshBase" );

        super.startClock();

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, thePrimaryNsMap )
                                                .historyStores( theMeshObjectHistoryStore, theTransactionHistoryStore )
                                                .build();

        checkEquals( headListener.theSuccessfulPuts.size(),    1, "Wrong home-object only put (current)" );
        checkEquals( historyListener.theSuccessfulPuts.size(), 1, "Wrong home-object only put (history)" );

        headListener.reset();
        historyListener.reset();

        final MeshObject home = mb.getHomeObject();

        //

        log.info( "Modifying home object (one)" );

        mb.execute(
                ( Transaction tx ) -> {
                    home.bless( TestSubjectArea.B );
                    home.setPropertyValue( TestSubjectArea.B_U, StringValue.create( "one" ));
                    return null;
                });

        checkEquals( headListener.theSuccessfulPuts.size(),    0, "Wrong home-object only put (current)" );
        checkEquals( headListener.theSuccessfulUpdates.size(), 1, "Wrong home-object only update (current)" );
        checkEquals( historyListener.theSuccessfulPuts.size(), 1, "Wrong home-object only put (history)" );

        headListener.reset();
        historyListener.reset();

        //

        log.info( "Modifying home object (two)" );

        mb.execute(
                ( Transaction tx ) -> {
                    home.setPropertyValue( TestSubjectArea.B_U, StringValue.create( "two" ));
                    return null;
                });

        checkEquals( headListener.theSuccessfulPuts.size(),    0, "Wrong home-object only put (current)" );
        checkEquals( headListener.theSuccessfulUpdates.size(), 1, "Wrong home-object only update (current)" );
        checkEquals( historyListener.theSuccessfulPuts.size(), 1, "Wrong home-object only put (history)" );

        headListener.reset();
        historyListener.reset();

        //

        collectGarbage();

        //

        log.info( "Accessing history" );

        MeshObjectHistory homeHistory = mb.meshObjectHistory( "" );
        checkObject( homeHistory, "No history found" );

        checkEquals( homeHistory.getLength(), 3, "wrong length of history" );

        //

        collectGarbage();

        //

        log.info( "Retrieving history" );

        MeshObject oldest = homeHistory.oldest();
        MeshObject newest = homeHistory.current();

        checkObject( oldest, "Oldest not found" );
        checkObject( newest, "Newest not found" );

        checkNotIdentity( oldest, newest, "Oldest and newest are actually the same instance" );

        checkCondition( !oldest.isBlessedBy( TestSubjectArea.B ), "oldest incorrectly blessed" );
        checkCondition(  newest.isBlessedBy( TestSubjectArea.B ), "newest not blessed" );
        checkEquals( newest.getPropertyValue( TestSubjectArea.B_U ), StringValue.create( "two"), "Wrong value after second update" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshObjectHistoryTest1.class );
}
