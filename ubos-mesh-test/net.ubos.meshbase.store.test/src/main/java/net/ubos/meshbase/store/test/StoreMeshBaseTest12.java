//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.store.StoreValue;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests iteration over the MeshObjects in a MeshBase.
 */
public class StoreMeshBaseTest12
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        int nDigits = 0;
        for( int i=TEST_SIZE ; i>=10 ; i /= 10 ) {
            ++nDigits;
        }
        final String PREFIX = "obj-2";
        final String FORMAT = "obj-%" + nDigits + "d";

        //
        
        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, nsMap ).build();

        RecordingStoreListener<StoreValue> listener = new RecordingStoreListener<>( this );
        theMeshObjectStore.addDirectStoreListener( listener );

        //

        log.info( "Populating MeshBase" );
        
        mb.execute( () -> {
            for( int i=0 ; i<TEST_SIZE ; ++i ) {
                mb.createMeshObject( String.format( FORMAT, i ));
            }
        } );
        mb.clearMemoryCache();
        
        listener.reset();

        //
        
        log.info( "Iterating" );
        
        startClock();

        CursorIterator<MeshObject> iter = mb.iterator( PREFIX );
        int count = 0;
        for(  ; iter.hasNext() ; ++count ) {
            MeshObject found = iter.next();
            checkCondition( found.getIdentifier().getLocalId().startsWith( PREFIX ), "Does not start with prefix at " + count ); 
        }
        
        checkEquals( count, TEST_SIZE/10, "Wrong number of objects" );
        
        log.info( "MeshBase with " + TEST_SIZE + " MeshObjects, iterated over " + count );
        log.info( String.format( "Iterating took %d ms", getRelativeTime()));
        log.info( "Database operations:\n" + listener.toString() );
    }

    protected final int TEST_SIZE  = 10000;

    // Our Logger
    private static final Log log = Log.getLogInstance(StoreMeshBaseTest11.class );
}
