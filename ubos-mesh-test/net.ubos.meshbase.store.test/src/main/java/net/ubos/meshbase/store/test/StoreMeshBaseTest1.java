//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import java.lang.ref.WeakReference;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.store.StoreValue;
import net.ubos.util.Pair;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that we do the right number of writes and reads.
 */
public class StoreMeshBaseTest1
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @SuppressWarnings("UnusedAssignment")
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Subscribing" );

        RecordingStoreListener<StoreValue> listener = new RecordingStoreListener<>( this );
        theMeshObjectStore.addDirectStoreListener( listener );

        //

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, nsMap ).build();

        listener.checkPuts( 1, 0 ).checkUpdates( 0, 0 ).checkGets( 0, 2 );
        listener.reset();

        //

        log.info( "Creating MeshObjects" );

        Pair<MeshObjectIdentifier [],WeakReference<?> []> pair
                = mb.execute( (tx) -> {
                    MeshObject []           mesh  = new MeshObject[ theTestSize ];
                    MeshObjectIdentifier [] names = new MeshObjectIdentifier[ theTestSize ];

                    WeakReference<?> [] refs = new WeakReference<?>[ theTestSize ];

                    for( int i=0 ; i<mesh.length ; ++i ) {
                        mesh[i]  = mb.createMeshObject();
                        names[i] = mesh[i].getIdentifier();
                        refs[i]  = new WeakReference<>( mesh[i] );

                        if( i % 3 == 1 ) {
                            mesh[i].bless( TestSubjectArea.AA );
                        } else if( i % 3 == 2 ) {
                            mesh[i].bless( TestSubjectArea.AA );
                            mesh[i].setPropertyValue( TestSubjectArea.A_X, StringValue.create( "Testing ... " + i ));
                        }
                    }

                    return new Pair<>( names, refs );
                } );

        MeshObjectIdentifier [] names = pair.getName();
        WeakReference<?> [] refs = pair.getValue();

        checkEquals( listener.theSuccessfulPuts.size(),         theTestSize, "Wrong number of successful puts" );
        checkEquals( listener.theFailedPuts.size(),             0,           "Wrong number of failed puts" );
        checkEquals( listener.theSuccessfulUpdates.size(),      0,           "Wrong number of successful updates" );
        checkEquals( listener.theFailedUpdates.size(),          0,           "Wrong number of failed updates" );
        checkEquals( listener.theSuccessfulGets.size(),         0,           "Wrong number of successful gets" );
        checkEquals( listener.theFailedGets.size(),             theTestSize, "Wrong number of failed gets" );
        checkEquals( listener.theSuccessfulDeletes.size(),      0,           "Wrong number of successful deletes" );
        checkEquals( listener.theFailedDeletes.size(),          0,           "Wrong number of failed deletes" );
        checkEquals( listener.theAllDeletes.size(),             0,           "Wrong number of allDeletes" );
        checkEquals( listener.theSuccessfulContainsKeys.size(), 0,           "Wrong number of successful containsKey" );
        checkEquals( listener.theFailedContainsKeys.size(),     0,           "Wrong number of successful containsKey" );

        listener.reset();

        //

        log.info( "Clearing cache, and loading MeshObjects again" );

        MeshObject [] mesh = new MeshObject[ names.length ]; // forget old references
        mb.clearMemoryCache();
        collectGarbage();

        for( int i=0 ; i<refs.length ; ++i ) {
            checkCondition( refs[i].get() == null, "MeshObject " + i + " still found" );
        }

        for( int i=0 ; i<names.length ; ++i ) {
            mesh[i] = mb.findMeshObjectByIdentifier( names[i] );

            checkObject( mesh[i], "Could not retrieve MeshObject with Identifier " + names[i] );
        }

        checkEquals( listener.theSuccessfulPuts.size(),         0,            "Wrong number of successful puts" );
        checkEquals( listener.theFailedPuts.size(),             0,            "Wrong number of failed puts" );
        checkEquals( listener.theSuccessfulUpdates.size(),      0,            "Wrong number of successful updates" );
        checkEquals( listener.theFailedUpdates.size(),          0,            "Wrong number of failed updates" );
        checkEquals( listener.theSuccessfulGets.size(),         names.length, "Wrong number of successful gets" );
        checkEquals( listener.theFailedGets.size(),             0,            "Wrong number of failed gets" );
        checkEquals( listener.theSuccessfulDeletes.size(),      0,            "Wrong number of successful deletes" );
        checkEquals( listener.theFailedDeletes.size(),          0,            "Wrong number of failed deletes" );
        checkEquals( listener.theAllDeletes.size(),             0,            "Wrong number of allDeletes" );
        checkEquals( listener.theSuccessfulContainsKeys.size(), 0,            "Wrong number of successful containsKey" );
        checkEquals( listener.theFailedContainsKeys.size(),     0,            "Wrong number of successful containsKey" );

        listener.reset();
    }

    /**
     * The number of MeshObjects to create for the test.
     */
    protected int theTestSize = 1000;

    // Our Logger
    private static final Log log = Log.getLogInstance( StoreMeshBaseTest1.class );
}
