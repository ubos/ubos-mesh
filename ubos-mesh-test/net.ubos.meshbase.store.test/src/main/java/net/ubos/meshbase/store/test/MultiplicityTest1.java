//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MultiplicityException;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.transaction.TransactionActionException;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Test that invalid multiplicities roll back transactions.
 *
 * Note: the ONETHREE relationship has multiplicities of 1:1 / 3:3.
 */
public class MultiplicityTest1
        extends
            AbstractStoreMeshBaseTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Creating MeshBase" );

        PrimaryMeshObjectIdentifierNamespaceMap nsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        StoreMeshBase mb = StoreMeshBase.Builder.create(theMeshObjectStore, theContextualNsStore, nsMap ).build();

        //

        super.startClock();

        log.info( "Attempt one" );

        try {
            mb.execute( () -> {
                MeshObject one   = mb.createMeshObject( TestSubjectArea.ONE );
                MeshObject three = mb.createMeshObject( TestSubjectArea.THREE );
            } );

            reportError( "Should have thrown exception (one)" );

        } catch( TransactionActionException ex ) {
            log.info( "Correctly thrown", ex );
            checkCondition( ex.getCause() instanceof MultiplicityException, "Wrong type of exception: " + ex.getCause() );
        }

        checkEquals( mb.size(), 1, "Wrong number of MeshObjects" );

        mb.clearMemoryCache();
        collectGarbage();

        checkEquals( mb.size(), 1, "Wrong number of MeshObjects" );

        //

        log.info( "Attempt two" );

        try {
            mb.execute( () -> {
                MeshObject one   = mb.createMeshObject( TestSubjectArea.ONE );
                MeshObject three = mb.createMeshObject( TestSubjectArea.THREE );
                one.blessRole( TestSubjectArea.ONETHREE.getSource(), three );

            } );
            reportError( "Should have thrown exception (two)" );

        } catch( TransactionActionException ex ) {
            log.info( "Correctly thrown", ex );
            checkCondition( ex.getCause() instanceof MultiplicityException, "Wrong type of exception: " + ex.getCause() );
        }

        checkEquals( mb.size(), 1, "Wrong number of MeshObjects" );

        mb.clearMemoryCache();
        collectGarbage();

        checkEquals( mb.size(), 1, "Wrong number of MeshObjects" );

        //

        log.info( "Attempt three" );

        try {
            mb.execute( () -> {
                MeshObject one1  = mb.createMeshObject( TestSubjectArea.ONE );
                MeshObject one2  = mb.createMeshObject( TestSubjectArea.ONE );
                MeshObject one3  = mb.createMeshObject( TestSubjectArea.ONE );
                MeshObject three = mb.createMeshObject( TestSubjectArea.THREE );
                one1.blessRole( TestSubjectArea.ONETHREE.getSource(), three );
                one2.blessRole( TestSubjectArea.ONETHREE.getSource(), three );
                one3.blessRole( TestSubjectArea.ONETHREE.getSource(), three );
            } );

        } catch( TransactionActionException ex ) {
            reportError( "Should have been no exception", ex );
        }

        checkEquals( mb.size(), 5, "Wrong number of MeshObjects" );

        mb.clearMemoryCache();
        collectGarbage();

        checkEquals( mb.size(), 5, "Wrong number of MeshObjects" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( MultiplicityTest1.class );
}
