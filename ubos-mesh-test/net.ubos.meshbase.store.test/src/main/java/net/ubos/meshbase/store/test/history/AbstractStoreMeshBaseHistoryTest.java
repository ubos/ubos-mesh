//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.history;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.io.IOException;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.store.StoreMeshBase;
import net.ubos.meshbase.store.test.MySQLDefinitions;
import net.ubos.store.StoreValue;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.sql.AbstractSqlStoreWithHistory;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.store.mysql.history.MysqlStoreWithHistory;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality of history-related tests..
 */
public abstract class AbstractStoreMeshBaseHistoryTest
        extends
            AbstractTest
{
    /**
     * Setup.
     *
     * @throws IOException all sorts of things may go wrong during tests
     */
    @BeforeEach
    public void setup()
        throws
            IOException
    {
        theDataSource = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser(         MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword(     MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theMeshObjectStore   = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_MESHOBJECTS_TABLE_NAME );
        thePrimaryNsStore    = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_PRIMARY_NAMESPACES_TABLE_NAME );
        theContextualNsStore = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_CONTEXTUAL_NAMESPACES_TABLE_NAME );

        theMeshObjectHistoryStore  = MysqlStoreWithHistory.create( theDataSource, MySQLDefinitions.TEST_MESH_OBJECT_HISTORY_TABLE_NAME );
        theTransactionHistoryStore = MysqlStoreWithHistory.create( theDataSource, MySQLDefinitions.TEST_TRANSACTION_HISTORY_TABLE_NAME );

        thePrimaryNsStore.initializeHard();
        theMeshObjectStore.initializeHard();
        theContextualNsStore.initializeHard();
        theMeshObjectHistoryStore.initializeHard();
        theTransactionHistoryStore.initializeHard();

        thePrimaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );

        theMeshBase = StoreMeshBase.Builder.create( theMeshObjectStore, theContextualNsStore, thePrimaryNsMap )
                                           .historyStores( theMeshObjectHistoryStore, theTransactionHistoryStore )
                                           .build();
    }

    /**
     * The Database connection.
     */
    protected MysqlDataSource theDataSource;

    /**
     * The SqlStore for the current MeshObjects.
     */
    protected AbstractSqlStore<StoreValue> theMeshObjectStore;

    /**
     * The SqlStore for the primary namespaces.
     */
    protected AbstractSqlStore<StoreValue> thePrimaryNsStore;

    /**
     * The SqlStore for the contextual namespaces.
     */
    protected AbstractSqlStore<StoreValue> theContextualNsStore;

    /**
     * The SqlStore for the MeshObject history.
     */
    protected AbstractSqlStoreWithHistory<StoreValueWithTimeUpdated> theMeshObjectHistoryStore;

    /**
     * The SqlStore for the Transaction log.
     */
    protected AbstractSqlStoreWithHistory<StoreValueWithTimeUpdated> theTransactionHistoryStore;

    /**
     * The MeshBase under test.
     */
    protected StoreMeshBase theMeshBase;

    /**
     * The primary namespace map.
     */
    protected PrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;
}
