//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.model;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.util.ArrayList;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.PrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.mesh.namespace.store.StorePrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.store.test.MySQLDefinitions;
import net.ubos.meshbase.transaction.Transaction;
import net.ubos.model.primitives.BooleanValue;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.FloatValue;
import net.ubos.model.primitives.L10PropertyValueMapImpl;
import net.ubos.model.primitives.MeshType;
import net.ubos.model.primitives.MeshTypeIdentifierDeserializer;
import net.ubos.model.primitives.MultiplicityValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.RelationshipType;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringDataType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.SubjectArea;
import net.ubos.modelbase.MeshTypeLifecycleManager;
import net.ubos.modelbase.ModelBase;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.modelbase.m.MModelBase;
import net.ubos.store.StoreValue;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality of ModelChangeTests.
 */
public abstract class AbstractModelChangeTest
        extends
            AbstractTest
{
    /**
     * Setup.
     *
     * @throws Exception all sorts of things may go wrong in tests
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        theDataSource = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser(         MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword(     MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theMeshObjectStore   = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_MESHOBJECTS_TABLE_NAME );
        thePrimaryNsStore    = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_PRIMARY_NAMESPACES_TABLE_NAME );
        theContextualNsStore = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_CONTEXTUAL_NAMESPACES_TABLE_NAME );

        thePrimaryNsStore.initializeHard();
        theMeshObjectStore.initializeHard();
        theContextualNsStore.initializeHard();

        thePrimaryNsMap = StorePrimaryMeshObjectIdentifierNamespaceMap.create( thePrimaryNsStore );
    }

    /**
     * Initialize the Model and populate the MeshBase.
     *
     * @param mb the MeshBase
     * @throws Exception all sorts of things may go wrong in tests
     */
    protected void initializeModelAndMeshBase(
            MeshBase mb )
        throws
            Exception
    {
        MeshTypeLifecycleManager       typeLife   = theModelBase.getMeshTypeLifecycleManager();
        MeshTypeIdentifierDeserializer typeIdFact = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

        final SubjectArea sa = typeLife.createSubjectArea(
                typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model" ),
                StringValue.create( "net.ubos.meshbase.store.test.model" ),
                L10PropertyValueMapImpl.create( StringValue.create( "test.model") ),
                null, null, null );
         theCreatedMeshTypes.add( sa );

        final EntityType ent1 = typeLife.createEntityType(
                typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent1" ),
                StringValue.create( "Ent1" ),
                L10PropertyValueMapImpl.create( StringValue.create( "Ent1") ),
                null, null,
                sa,
                null, null,
                BooleanValue.TRUE, // abstract
                BooleanValue.TRUE );
        theCreatedMeshTypes.add( ent1 );

        final PropertyType ent1_prop1 = typeLife.createPropertyType(
                typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent1_Prop1" ),
                StringValue.create( "Ent1_Prop1" ),
                L10PropertyValueMapImpl.create( StringValue.create( "Ent1_Prop1") ),
                null,
                ent1,
                sa,
                StringDataType.theDefault,
                null,
                BooleanValue.TRUE,
                BooleanValue.FALSE,
                FloatValue.create( 1. ) );
        theCreatedMeshTypes.add( ent1_prop1 );

        final EntityType ent2 = typeLife.createEntityType(
                typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent2" ),
                StringValue.create( "Ent2" ),
                L10PropertyValueMapImpl.create( StringValue.create( "Ent2") ),
                null, null,
                sa,
                new EntityType[] { ent1 },
                null,
                BooleanValue.TRUE, // abstract
                BooleanValue.TRUE );
        theCreatedMeshTypes.add( ent2 );

        final EntityType ent3 = typeLife.createEntityType(
                typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent3" ),
                StringValue.create( "Ent3" ),
                L10PropertyValueMapImpl.create( StringValue.create( "Ent3") ),
                null, null,
                sa,
                new EntityType[] { ent2 },
                null,
                BooleanValue.FALSE, // not abstract
                BooleanValue.TRUE );
        theCreatedMeshTypes.add( ent3 );

        final EntityType ent4 = typeLife.createEntityType(
                typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Ent4" ),
                StringValue.create( "Ent4" ),
                L10PropertyValueMapImpl.create( StringValue.create( "Ent4") ),
                null, null,
                sa,
                null, null,
                BooleanValue.FALSE, // not abstract
                BooleanValue.TRUE );
        theCreatedMeshTypes.add( ent4 );

        final RelationshipType rel3_4 = typeLife.createRelationshipType(
                typeIdFact.fromExternalForm( "net.ubos.meshbase.store.test.model/Rel3_4" ),
                StringValue.create( "Rel3_4" ),
                L10PropertyValueMapImpl.create( StringValue.create( "Rel3_4") ),
                null,
                sa,
                MultiplicityValue.ZERO_N,
                MultiplicityValue.ZERO_N,
                ent3,
                ent4,
                (RoleType[]) null, null,
                BooleanValue.FALSE );
        theCreatedMeshTypes.add( rel3_4 );
        theCreatedMeshTypes.add( rel3_4.getSource() );
        theCreatedMeshTypes.add( rel3_4.getDestination() );

        mb.execute(
            ( Transaction tx ) -> {
                MeshObject a = mb.createMeshObject( "aaaa", ent3 );
                MeshObject b = mb.createMeshObject( "bbbb", ent4 );

                a.blessRole( rel3_4.getSource(), b);

                a.setPropertyValue( ent1_prop1, StringValue.create( "Test value" ));
                return null;
            });
    }

    /**
     * Cleanup the Modelbase so that subsequent tests can run.
     */
    protected void cleanupModelBase()
    {
        // clean up in reverse order
        for( int i=theCreatedMeshTypes.size()-1 ; i>=0 ; --i ) {
            theModelBase.remove( theCreatedMeshTypes.get( i ) );
        }
    }

    /**
     * The ModelBase.
     */
    protected MModelBase theModelBase = (MModelBase) ModelBase.SINGLETON;

    /**
     * The Database connection.
     */
    protected MysqlDataSource theDataSource;

    /**
     * The AbstractSqlStore containing MeshObjects to be tested.
     */
    protected AbstractSqlStore<StoreValue> theMeshObjectStore;

    /**
     * The SqlStore for the primary namespaces.
     */
    protected AbstractSqlStore<StoreValue> thePrimaryNsStore;

    /**
     * The AbstractSqlStore containing the namespaces.
     */
    protected AbstractSqlStore<StoreValue> theContextualNsStore;

    /**
     * The primary namespace map.
     */
    protected PrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;

    /**
     * The MeshTypes we create.
     */
    protected final ArrayList<MeshType> theCreatedMeshTypes = new ArrayList<>();
}
