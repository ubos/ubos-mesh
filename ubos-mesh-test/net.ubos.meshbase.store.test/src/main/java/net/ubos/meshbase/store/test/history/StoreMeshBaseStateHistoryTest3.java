//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.history;

import net.ubos.mesh.MeshObject;
import net.ubos.mesh.history.MeshObjectHistory;
import net.ubos.meshbase.history.MeshBaseStateHistory;
import net.ubos.model.Test.TestSubjectArea;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Test that relating two MeshObjects creates an entry in both MeshObjectHistories.
 */
public class StoreMeshBaseStateHistoryTest3
        extends
            AbstractStoreMeshBaseHistoryTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final long   delta = 500;
        final String ID1   = "obj-1";
        final String ID2   = "obj-2";

        startClock();

        //

        log.info( "Creating MeshObjects" );

        sleepUntil( delta );

        theMeshBase.execute( () -> {
            theMeshBase.createMeshObject( ID1, TestSubjectArea.AA );
        } );

        sleepUntil( delta*2 );

        theMeshBase.execute( () -> {
            theMeshBase.createMeshObject( ID2, TestSubjectArea.B );
        } );

        //

        log.info(  "Checking histories (1)" );

        MeshBaseStateHistory mbHist = theMeshBase.getHistory();
        MeshObjectHistory    hist1 = theMeshBase.meshObjectHistory( ID1 );
        MeshObjectHistory    hist2 = theMeshBase.meshObjectHistory( ID2 );

        checkEquals( mbHist.getLength(), 3, "Wrong MeshBaseStateHistory" ); // Home, ID1, ID2
        checkEquals( hist1.getLength(),  1, "Wrong history 1" );
        checkEquals( hist2.getLength(),  1, "Wrong history 2" );

        //

        log.info( "Relating MeshObjects" );

       theMeshBase.execute( () -> {
            MeshObject obj1 = theMeshBase.findMeshObjectByIdentifier( ID1 );
            MeshObject obj2 = theMeshBase.findMeshObjectByIdentifier( ID2 );

            obj1.blessRole( TestSubjectArea.ARANY_S, obj2 );
        } );

        //

        collectGarbage();

        //

        log.info(  "Checking histories (2)" );

        checkEquals( mbHist.getLength(), 4, "Wrong MeshBaseStateHistory" ); // Home, ID1, ID2, rel
        checkEquals( hist1.getLength(),  2, "Wrong history 1" );
        checkEquals( hist2.getLength(),  2, "Wrong history 2" );

        checkEquals( hist1.oldest().traverseToNeighbors().size(), 0, "Wrong neighbors of old obj1" );
        checkEquals( hist2.oldest().traverseToNeighbors().size(), 0, "Wrong neighbors of old obj2" );

        checkEquals( hist1.current().traverseToNeighbors().size(), 1, "Wrong neighbors of new obj1" );
        checkEquals( hist2.current().traverseToNeighbors().size(), 1, "Wrong neighbors of new obj2" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(StoreMeshBaseStateHistoryTest3.class);
}
