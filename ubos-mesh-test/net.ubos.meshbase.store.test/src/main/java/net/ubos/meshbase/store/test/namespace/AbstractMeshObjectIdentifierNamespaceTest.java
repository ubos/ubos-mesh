//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.meshbase.store.test.namespace;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.io.IOException;
import net.ubos.meshbase.store.test.MySQLDefinitions;
import net.ubos.store.StoreValue;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out functionality common to MeshObjectIdentifierNamespace tests.
 */
public abstract class AbstractMeshObjectIdentifierNamespaceTest
        extends
            AbstractTest
{
    /**
     * Setup.
     *
     * @throws IOException all sorts of things may go wrong during tests
     */
    @BeforeEach
    public void setup()
        throws
            IOException
    {
        theDataSource        = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser(         MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword(     MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theMeshObjectStore   = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_MESHOBJECTS_TABLE_NAME );
        thePrimaryNsStore    = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_PRIMARY_NAMESPACES_TABLE_NAME );
        theContextualNsStore = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_CONTEXTUAL_NAMESPACES_TABLE_NAME );

        thePrimaryNsStore.initializeHard();
        theMeshObjectStore.initializeHard();
        theContextualNsStore.initializeHard();
    }

    /**
     * The Database connection.
     */
    protected MysqlDataSource theDataSource;

    /**
     * The SqlStore for the current MeshObjects.
     */
    protected AbstractSqlStore<StoreValue> theMeshObjectStore;

    /**
     * The SqlStore for the primary namespaces.
     */
    protected AbstractSqlStore<StoreValue> thePrimaryNsStore;

    /**
     * The SqlStore for the contextual namespaces.
     */
    protected AbstractSqlStore<StoreValue> theContextualNsStore;
}
