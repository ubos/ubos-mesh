//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import java.util.Arrays;
import java.util.List;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.sql.AbstractSqlStoreWithHistory;
import net.ubos.testharness.AbstractTest;

/**
 * Factors out common functionality of SqlHistoryStoreTests.
 */
public abstract class AbstractSqlHistoryStoreTest
        extends
            AbstractTest
{
    /**
     * The AbstractSqlHistoryStore to be tested.
     */
    protected AbstractSqlStoreWithHistory<StoreValueWithTimeUpdated> theTestStore;

    protected static final long START  = 1000000L; // something memorable
    protected static final long MIDDLE = 1100000L; // something memorable
    protected static final long END    = 1200000L; // something memorable

    /**
     * Test data.
     */
    protected static final TestData[] one = new TestData[] {
        new TestData( "abc", START, bytes( "some abc data at START" )),
        new TestData( "def", START, bytes( "some def data at START" )),
        new TestData( "ghi", START, bytes( "some def data at START" )),

        new TestData( "abc", MIDDLE, bytes( "some abc data at MIDDLE" )),
        new TestData( "def", MIDDLE, bytes( "some def data at MIDDLE" )),

        new TestData( "abc", END, bytes( "some abc data at END" )),
    };

    protected static final List<String> oneKeys = Arrays.asList( "abc", "def", "ghi" );

    protected static class TestData
    {
        public TestData(
                String  key,
                long    timeUpdated,
                byte [] data )
        {
            theKey          = key;
            theTimeUpdated  = timeUpdated;
            theData         = data;
        }

        @Override
        public String toString()
        {
            return "TestData with key '" + theKey + "'";
        }

        String  theKey;
        long    theTimeUpdated;
        byte [] theData;
    }
}
