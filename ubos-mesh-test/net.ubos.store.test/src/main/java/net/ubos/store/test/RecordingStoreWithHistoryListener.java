//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

import java.util.ArrayList;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.Pair;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.StoreWithHistoryListener;

/**
 * HistoryStoreListener that records the events. Useful for several of the tests.
 *
 * @param <T> parameterization allows implementations that store more than just StoreValueWithTimeUpdated
 */
public class RecordingStoreWithHistoryListener<T extends StoreValueWithTimeUpdated>
        implements
            StoreWithHistoryListener<T>
{
    /**
     * Constructor.
     *
     * @param test the test being run
     */
    public RecordingStoreWithHistoryListener(
            AbstractTest test )
    {
        theTest = test;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putPerformed(
            StoreWithHistory<T> store,
            T               value,
            boolean         success )
    {
        if( success ) {
            theSuccessfulPuts.add( value );
        } else {
            theFailedPuts.add( value );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getPerformed(
            StoreWithHistory<T> store,
            String          key,
            long            timeUpdated,
            T               value )
    {
        if( value != null ) {
            theSuccessfulGets.add( value );
        } else {
            theFailedGets.add( new Pair<>( key, timeUpdated ));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updatePerformed(
            StoreWithHistory<T> store,
            T                   value,
            boolean             success )
    {
        // FIXME?
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAllPerformed(
            StoreWithHistory<T> store,
            String              key )
    {
        // FIXME?
    }

    /**
     * Aggregate check function.
     *
     * @param successfulPuts the expected number of successful put operations
     * @param failedPuts the expected number of failed put operations
     * @param successfulGets the expected number of successful get operations
     * @param failedGets the expected number of failed get operations
     */
    public void checkSizes(
            int successfulPuts,
            int failedPuts,
            int successfulGets,
            int failedGets )
    {
        theTest.checkEquals( theSuccessfulPuts.size(), successfulPuts, "Wrong number of successful history puts" );
        theTest.checkEquals( theFailedPuts.size(),     failedPuts,     "Wrong number of failed history puts" );

        theTest.checkEquals( theSuccessfulGets.size(), successfulGets, "Wrong number of successful history gets" );
        theTest.checkEquals( theFailedGets.size(),     failedGets,     "Wrong number of failed history gets" );
    }

    /**
     * Reset the listener.
     */
    public void reset()
    {
        theSuccessfulPuts.clear();
        theFailedPuts.clear();

        theSuccessfulGets.clear();
        theFailedGets.clear();
    }

    protected final AbstractTest theTest;

    public final ArrayList<StoreValueWithTimeUpdated> theSuccessfulPuts = new ArrayList<>();
    public final ArrayList<StoreValueWithTimeUpdated> theFailedPuts     = new ArrayList<>();

    public final ArrayList<StoreValueWithTimeUpdated> theSuccessfulGets = new ArrayList<>();
    public final ArrayList<Pair<String,Long>> theFailedGets     = new ArrayList<>();
}
