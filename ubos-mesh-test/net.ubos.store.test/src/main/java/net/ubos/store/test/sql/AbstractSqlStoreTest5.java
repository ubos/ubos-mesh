//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import net.ubos.store.Store;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.store.util.DynamicLoadFromStoreMap;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the DynamicLoadFromStoreMap.
 */
public abstract class AbstractSqlStoreTest5
        extends
            AbstractSqlStoreTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @Override
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating new database" );

        theSqlStore.initializeHard();

        //

        MyTestMap testMap = new MyTestMap( theSqlStore, "testentry" );

        for( var current : testData.entrySet() ) {
            testMap.put( current.getKey(), current.getValue() );
        }

        checkObject( testMap.getDelegate(), "no delegate, but have put data in" );

        //

        log.info( "Removing local cache" );

        testMap.clearLocalCache();

        checkCondition( testMap.getDelegate() == null, "has delegate but should not" );

        //

        log.info( "Accessing data" );

        for( int key : testData.keySet() ) {
            String testValue = testData.get( key );
            String actualValue = testMap.get( key );

            checkEquals( testValue, actualValue, "values not the same for key " + key );
        }
        checkObject( testMap.getDelegate(), "no delegate, but have recovered data" );

        //

        log.info( "Modifying data and saving again" );

        int    additionalKey   = 111;
        String additionalValue = "hundredeleven";

        testMap.put( additionalKey, additionalValue );

        testMap.clearLocalCache();

        //

        log.info( "Accessing data" );

        for( int key : testData.keySet() ) {
            String testValue = testData.get( key );
            String actualValue = testMap.get( key );

            checkEquals( testValue, actualValue, "values not the same for key " + key );
        }
        String actualValue = testMap.get( additionalKey );
        checkEquals( additionalValue, actualValue, "values not the same for key " + additionalKey );

        checkObject( testMap.getDelegate(), "no delegate, but have recovered data" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractSqlStoreTest5.class );


    /**
     * Test data.
     */
    static final HashMap<Integer,String> testData = new HashMap<Integer,String>();
    static {
        testData.put( 1, "one" );
        testData.put( 2, "two" );
        testData.put( 4, "four" );
        testData.put( 6, "six" );
        testData.put( 7, "seven" );
        testData.put( 3, "three" );
        testData.put( 10, "ten" );
    }

    /**
     * Test map implementation.
     */
    static class MyTestMap
            extends
                DynamicLoadFromStoreMap<Integer,String,StoreValue>
    {
        /**
         * Constructor.
         *
         * @param store the Store to load from
         * @param storeEntryKey the key used to write the entire content of the Map into the Store
         */
        public MyTestMap(
                Store<StoreValue> store,
                String            storeEntryKey )
        {
            super( store, storeEntryKey );
        }

        /**
         * Override load method.
         *
         * @return the new delegate HashMap
         * @throws StoreKeyDoesNotExistException thrown if no map content could be found
         * @throws IOException thrown if loading failed
         */
        @SuppressWarnings(value={"unchecked"})
        @Override
        protected HashMap<Integer,String> load()
            throws
                StoreKeyDoesNotExistException,
                IOException
        {
            StoreValue storeValue = theStore.get( theStoreEntryKey );

            ObjectInputStream inStream = new ObjectInputStream( storeValue.getDataAsStream() );

            HashMap<Integer,String> ret = null;
            try {
                ret = (HashMap<Integer,String>) inStream.readObject();
            } catch( ClassNotFoundException ex ) {
                log.error( ex );
            }
            return ret;
        }

        /**
         * Override save method.
         *
         * @throws IOException thrown if saving failed
         */
        @Override
        protected void save()
            throws
                IOException
        {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            ObjectOutputStream outStream = new ObjectOutputStream( buffer );

            outStream.writeObject( theDelegate );

            outStream.close();

            long now = System.currentTimeMillis();

            theStore.putOrUpdate( new StoreValue( theStoreEntryKey, "", buffer.toByteArray() ));
        }

        /**
         * Obtain delegate, this is for test instrumentation.
         *
         * @return the delegate
         */
        public HashMap<Integer,String> getDelegate()
        {
            return theDelegate;
        }

        /**
         * Invoked only by objects held in this DynamicLoadFromStoreMap, this enables
         * the held objects to indicate to the DynamicLoadFromStoreMap that they have been updated.
         * Depending on the implementation of the DynamicLoadFromStoreMap, that may cause the
         * DynamicLoadFromStoreMap to write changes to disk, for example.
         *
         * @param key the key
         * @param value the value
         */
        @Override
        public void valueUpdated(
                Integer key,
                String  value )
        {
            log.debug( "ValueUpdated" );
        }
    }
}
