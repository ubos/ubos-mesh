//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.history;

import java.nio.charset.StandardCharsets;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedDecodingException;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedEncodingException;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedMapper;
import net.ubos.util.history.HasTimeUpdatedPair;

/**
 * Mapper for these tests.
 */
public class TestStoreValueWithTimeUpdatedMapper
    implements
        StoreValueWithTimeUpdatedMapper<HasTimeUpdatedPair<String>,StoreValueWithTimeUpdated>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDefaultEncodingId()
    {
        return "default-encoding";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HasTimeUpdatedPair<String> decodeValue(
            StoreValueWithTimeUpdated value )
        throws
            StoreValueWithTimeUpdatedDecodingException
    {
        String payload = new String( value.getData(), StandardCharsets.UTF_8 );
        return new HasTimeUpdatedPair<>( value.getTimeUpdated(), payload );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreValueWithTimeUpdated encodeValue(
            String                     key,
            HasTimeUpdatedPair<String> value )
        throws
            StoreValueWithTimeUpdatedEncodingException
    {
        StoreValueWithTimeUpdated ret = new StoreValueWithTimeUpdated(
                key,
                getDefaultEncodingId(),
                value.getTimeUpdated(),
                value.getPayload().getBytes( StandardCharsets.UTF_8 ));
        return ret;
    }
};
