//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.store.test.AbstractStoreIteratorTest3;
import net.ubos.store.test.MySQLDefinitions;
import org.junit.jupiter.api.BeforeEach;

/**
 * Tests the MysqlStoreIterator.
 */
public class MysqlStoreIteratorTest3
        extends
            AbstractStoreIteratorTest3
{
    @BeforeEach
    public void setup()
    {
        MysqlDataSource theDataSource = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser( MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword( MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theTestStore = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_TABLE_NAME );
    }
}
