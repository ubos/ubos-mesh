//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;
import net.ubos.store.mysql.history.MysqlStoreWithHistory;
import net.ubos.store.test.AbstractStoreBackedSwappingMapOfHistoryTest1;
import net.ubos.store.test.MySQLDefinitions;
import org.junit.jupiter.api.BeforeEach;

/**
 * Tests SmartMap&lt;History&gt;, backed by a Store, where both levels swap:
 * the outer SmartMap swaps, and the elements of the histories swap, too.
 */
public class MysqlBackedSwappingMapOfHistoryTest1
    extends
        AbstractStoreBackedSwappingMapOfHistoryTest1
{
    @BeforeEach
    public void setup()
    {
        MysqlDataSource theDataSource = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser( MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword( MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theTestStore = MysqlStoreWithHistory.create( theDataSource, MySQLDefinitions.TEST_TABLE_NAME );
    }
}
