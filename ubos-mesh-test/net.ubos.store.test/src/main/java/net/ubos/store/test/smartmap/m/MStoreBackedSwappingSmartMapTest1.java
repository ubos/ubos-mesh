//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.smartmap.m;

import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.m.MStore;
import net.ubos.store.test.smartmap.AbstractStoreBackedSwappingSmartMapTest1;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 *
 */
public class MStoreBackedSwappingSmartMapTest1
    extends
        AbstractStoreBackedSwappingSmartMapTest1
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        Store<StoreValue> store = MStore.create();

        run( store );
    }

    private static final Log log = Log.getLogInstance( MStoreBackedSwappingSmartMapTest1.class );
}
