//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;
import net.ubos.store.StoreValue;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.store.test.MySQLDefinitions;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * This isn't a unit test, but a performance test for the local Mysql database.
 * So this is disabled by default.
 */
@Disabled
public class MysqlPutUpdatePerformanceTest1
        extends
            AbstractTest
{
    @BeforeEach
    public void setup()
    {
        MysqlDataSource theDataSource = new MysqlDataSource();
        theDataSource.setDatabaseName(MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser(MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword(MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theTestStore = MysqlStore.create(theDataSource, MySQLDefinitions.TEST_TABLE_NAME );
    }

    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        final int N_ELEMENTS = 1000;
        final int N_RUNS     = 4;

        final String  encoding = "test1";
        // final byte [] data     = "A short string".getBytes();
        final byte [] data     = new byte[ 100000 ];
        for( int i=0 ; i<data.length ; ++i ) {
            data[i] = (byte) i; // trunc is fine
        }

        long [] putTook         = new long[ N_RUNS ];
        long [] putOrUpdateTook = new long[ N_RUNS ];

        //


        for( int i=0 ; i<4 ; ++i ) {
            theTestStore.initializeHard();

            log.info( "Putting data: " + i );

            startClock();

            for( int j=0 ; j<N_ELEMENTS ; ++j ) {
                theTestStore.put( new StoreValue( "key-" + j, encoding, data ));
            }

            putTook[i] = getRelativeTime();

            //

            theTestStore.initializeHard();

            log.info( "PuttingOrUpdating data: " + i );

            startClock();

            for( int j=0 ; j<N_ELEMENTS ; ++j ) {
                theTestStore.putOrUpdate( new StoreValue( "key-" + j, encoding, data ));
            }

            putOrUpdateTook[i] = getRelativeTime();
        }
        System.err.println( "Results:" );
        System.err.print( "    Put:" );
        for( int i=0 ; i<N_RUNS ; ++i ) {
            System.err.print( " " );
            System.err.print( putTook[i] );
        }
        System.err.println( " ms (avg " + avg( putTook ) / N_ELEMENTS + " per operation)" );
        System.err.print( "    PutOrUpdate:" );
        for( int i=0 ; i<N_RUNS ; ++i ) {
            System.err.print( " " );
            System.err.print( putOrUpdateTook[i] );
        }
        System.err.println( " ms (avg " + avg( putOrUpdateTook ) / N_ELEMENTS + " per operation)" );

    }

    private double avg(
            long [] items )
    {
        double ret = 0;
        for( long i : items ) {
            ret += i;
        }
        ret /= items.length;
        return ret;
    }

    /**
     * The SqlStore to be tested.
     */
    protected AbstractSqlStore<StoreValue> theTestStore;

    private static final Log log = Log.getLogInstance( MysqlPutUpdatePerformanceTest1.class );
}
