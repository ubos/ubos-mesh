//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;
import net.ubos.store.mysql.MysqlStore;
import net.ubos.store.test.MySQLDefinitions;
import net.ubos.store.test.sql.AbstractSqlStoreTest6;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 */
public class MysqlStoreTest6
    extends
        AbstractSqlStoreTest6
{
    @BeforeEach
    public void setup()
    {
        MysqlDataSource theDataSource = new MysqlDataSource();
        theDataSource.setDatabaseName( MySQLDefinitions.TEST_DATABASE_NAME );
        theDataSource.setUser( MySQLDefinitions.TEST_DATABASE_USER );
        theDataSource.setPassword( MySQLDefinitions.TEST_DATABASE_USER_PASSWORD );

        theSqlStore = MysqlStore.create( theDataSource, MySQLDefinitions.TEST_TABLE_NAME );
        theTestStore = theSqlStore;
    }
}
