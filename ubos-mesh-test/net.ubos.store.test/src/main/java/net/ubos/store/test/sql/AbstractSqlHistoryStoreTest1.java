//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import java.util.ArrayList;
import java.util.List;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.test.RecordingStoreWithHistoryListener;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;
import net.ubos.store.history.StoreValueWithTimeUpdatedHistory;

/**
 * Tests that we can iterate over all keys in the HistoryStore.
 */
public abstract class AbstractSqlHistoryStoreTest1
        extends
            AbstractSqlHistoryStoreTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating new database" );

        theTestStore.initializeHard();

        RecordingStoreWithHistoryListener<StoreValueWithTimeUpdated> listener = new RecordingStoreWithHistoryListener<>( this );
        theTestStore.addDirectHistoryStoreListener( listener );

        //

        log.info( "Checking the HistoryStore is empty" );

        checkCondition( theTestStore.isEmpty(), "HistoryStore not empty" );

        CursorIterator<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> iter = theTestStore.iterator();

        checkCondition( iter.hasNext()     == false, "Has next" );
        checkCondition( iter.hasPrevious() == false, "Has previous" );

        //

        log.info( "Inserting data" );

        for( int i=0 ; i<one.length ; ++i ) {
            TestData current = one[i];
            theTestStore.putOrUpdate( new StoreValueWithTimeUpdated(
                    current.theKey,
                    "enc",
                    current.theTimeUpdated,
                    current.theData ));
        }

        listener.checkSizes( one.length, 0, 0, 0 );
        listener.reset();

        //

        log.info( "Checking inserted data is there" );

        for( int i=0 ; i<one.length ; ++i ) {
            TestData                  current = one[i];
            StoreValueWithTimeUpdated value   = theTestStore.get( current.theKey, current.theTimeUpdated );

            checkEquals(          current.theKey,         value.getKey(),         "not the same id" );
            checkEquals(          current.theTimeUpdated, value.getTimeUpdated(), "not the same timeUpdated" );
            checkEqualByteArrays( current.theData,        value.getData(),        "not the same content" );
        }

        listener.checkSizes( 0, 0, one.length, 0 );
        listener.reset();

        checkCondition( !theTestStore.isEmpty(), "HistoryStore is empty" );
        checkEquals( theTestStore.size(), oneKeys.size(), "Wrong length" );

        //

        log.info( "Checking iterator" );

        CursorIterator<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> iter2 = theTestStore.iterator();

        checkCondition( iter2.hasPrevious() == false, "Has previous" );
        checkCondition( iter2.hasPrevious( 3 ) == false, "Has previous 3" );

        checkCondition( iter2.hasNext() == true, "Does not have next" );
        checkCondition( iter2.hasNext( 2 ) == true, "Does not have next 2 " );
        checkCondition( iter2.hasNext( oneKeys.size()-1 ) == true,  "Does not have next " + (oneKeys.size()-1) );
        checkCondition( iter2.hasNext( oneKeys.size()   ) == true,  "Does not have next " + oneKeys.size() );
        checkCondition( iter2.hasNext( oneKeys.size()+1 ) == false, "Has next " + (oneKeys.size()+1) );

        //

        log.info( "Iterating over the StoreHistories forward (step 1)" );

        List<String> remainingKeys = new ArrayList<>( oneKeys );

        CursorIterator<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> iter3 = theTestStore.iterator();
        while( iter3.hasNext() ) {
            StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated> current = iter3.next();

            log.debug( "Found ", current.getKey() );

            checkCondition( remainingKeys.remove( current.getKey() ), "key not found: " + current.getKey() );
        }

        //

        log.info( "Iterating over the StoreHistories forward (step 100)" );

        remainingKeys = new ArrayList<>( oneKeys );

        iter3 = theTestStore.iterator();

        List<StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated>> found = iter3.previous( 100 );
        checkEquals( found.size(), 0, "Wrong number of previous found" );

        found = iter3.next( 100 );

        checkEquals( found.size(), oneKeys.size(), "Wrong number of next found" );

        for( StoreValueWithTimeUpdatedHistory<StoreValueWithTimeUpdated> current : found ) {
            checkCondition( remainingKeys.remove( current.getKey() ), "key not found: " + current.getKey() );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractSqlHistoryStoreTest1.class);
}
