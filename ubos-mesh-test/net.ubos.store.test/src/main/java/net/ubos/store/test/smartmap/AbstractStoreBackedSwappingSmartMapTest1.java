//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.smartmap;

import java.util.ArrayList;
import net.ubos.store.util.SerializingStoreValueMapper;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.test.AbstractStoreTest;
import net.ubos.store.util.StoreBackedSwappingSmartMap;
import net.ubos.util.logging.Log;
import net.ubos.util.smartmap.SwappingSmartMap;
import net.ubos.util.smartmap.SwappingSmartMapListener;

/**
 * Tests the StoreBackedSwappingHashMap's persistence.
 */
public class AbstractStoreBackedSwappingSmartMapTest1
        extends
            AbstractStoreTest
{
    /**
     * Run the test.
     *
     * @param store the Store under test
     * @throws Exception thrown if an Exception occurred during the test
     */
    protected void run(
            Store<StoreValue> store )
        throws
            Exception
    {
        SerializingStoreValueMapper<String,Integer> mapper = new SerializingStoreValueMapper<>() {
            @Override
            public String stringToKey(
                    String s )
            {
                return s;
            }
            @Override
            public String getDefaultEncodingId()
            {
                return "enc-1";
            }
        };

        MyListener<String,Integer> listener = new MyListener<>();

        StoreBackedSwappingSmartMap<String,Integer,StoreValue> map = StoreBackedSwappingSmartMap.Builder.create( mapper, store ).useWeak().build();
        map.addSoftSmartMapListener( listener );

        String [] keys = {
            "a",
            "d",
            "c",
            "e",
            "b",
        };

        log.info( "Inserting" );

        for( int i=0 ; i<keys.length ; ++i ) {
            map.put(keys[i], i);
        }

        checkEquals( map.size(), keys.length, "Wrong size map" );

        //

        log.info( "Waiting and checking that they are still there" );

        Thread.sleep( 2000L );
        collectGarbage();

        checkEquals( listener.loadedEvents.size(),  0,           "wrong number of loadedEvents" );
        checkEquals( listener.savedEvents.size(),   keys.length, "wrong number of savedEvents" );
        checkEquals( listener.removedEvents.size(), 0,           "wrong number of removedEvents" );

        listener.reset();

        checkEquals( map.size(), keys.length, "Wrong size map" );

        for( int i=0 ; i<keys.length ; ++i ) {
            Integer found = map.get( keys[i] );
            checkObject( found, "Value not found at index " + i );
            checkEquals( i, found.intValue(), "Wrong value found at index " + i );
        }

        // FIXME: this does not seem to work any more with more recent gc() collection algorithms
        // Not sure we can force this?
        // checkEquals( listener.loadedEvents.size(),  keys.length, "wrong number of loadedEvents" );
        checkEquals( listener.savedEvents.size(),   0,           "wrong number of savedEvents" );
        checkEquals( listener.removedEvents.size(), 0,           "wrong number of removedEvents" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractStoreBackedSwappingSmartMapTest1.class  );

    /**
     * Listener implementation for this test.
     *
     * @param <K> key parameter
     * @param <V> value parameter
     */
    static class MyListener<K,V>
            implements
                SwappingSmartMapListener<K,V>
    {
        /**
         * A key-value pair has been loaded from storage.
         *
         * @param source the SwappingHashMap that emitted this event
         * @param key the key of the key-value pair that was loaded
         */
        @Override
        public void loadedFromStorage(
                SwappingSmartMap<K,V> source,
                K                     key,
                V                     value )
        {
            loadedEvents.add( key );
        }

        /**
         * A key-value pair has been saved to storage.
         *
         * @param source the SwappingHashMap that emitted this event
         * @param key the key of the key-value pair that was saved
         */
        @Override
        public void savedToStorage(
                SwappingSmartMap<K,V> source,
                K                     key,
                V                     value )
        {
            savedEvents.add( key );
        }

        /**
         * A key-value pair has been removed from storage.
         *
         * @param source the SwappingHashMap that emitted this event
         * @param key the key of the key-value pair that was saved
         */
        @Override
        public void removedFromStorage(
                SwappingSmartMap<K,V> source,
                K                     key )
        {
            removedEvents.add( key );
        }

        /**
         * Reset the event stores.
         */
        public void reset()
        {
            loadedEvents.clear();
            savedEvents.clear();
            removedEvents.clear();
        }

        public ArrayList<K> loadedEvents  = new ArrayList<>();
        public ArrayList<K> savedEvents   = new ArrayList<>();
        public ArrayList<K> removedEvents = new ArrayList<>();
    }
}
