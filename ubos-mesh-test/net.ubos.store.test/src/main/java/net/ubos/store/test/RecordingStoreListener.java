//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.ubos.store.Store;
import net.ubos.store.StoreListener;
import net.ubos.store.StoreValue;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.Pair;
import net.ubos.util.Tuple;
import net.ubos.util.cursoriterator.CursorIterator;

/**
 * StoreListener that records the events. Useful for several of the tests.
 *
 * @param <T> parameter for the Store
 */
public class RecordingStoreListener<T extends StoreValue>
        implements
            StoreListener<T>
{
    /**
     * Constructor.
     *
     * @param test the test being run
     */
    public RecordingStoreListener(
            AbstractTest test )
    {
        theTest = test;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putPerformed(
            Store<T> store,
            T        value,
            boolean  success )
    {
        if( success ) {
            theSuccessfulPuts.add( value.getKey() );
        } else {
            theFailedPuts.add( value.getKey() );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updatePerformed(
            Store<T> store,
            T        value,
            boolean  success )
    {
        if( success ) {
            theSuccessfulUpdates.add( value.getKey() );
        } else {
            theFailedUpdates.add( value.getKey() );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getPerformed(
            Store<T> store,
            String   key,
            T        value )
    {
        if( value != null ) {
            theSuccessfulGets.add( key );
        } else {
            theFailedGets.add( key );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePerformed(
            Store<T> store,
            String   key,
            boolean  success )
    {
        if( success ) {
            theSuccessfulDeletes.add( key );
        } else {
            theFailedDeletes.add( key );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAllPerformed(
            Store<T> store,
            String   prefix,
            int      n )
    {
        theAllDeletes.add( new Pair<>( prefix, n ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void containsKeyPerformed(
            Store<T> store,
            String   key,
            boolean  success )
    {
        if( success ) {
            theSuccessfulContainsKeys.add( key );
        } else {
            theFailedContainsKeys.add( key );
        }
    }

    @Override
    public void peekNextPerformed(
            Store<T> store,
            String   key,
            T        value,
            boolean  success )
    {
        if( success ) {
            theSuccessfulPeekNexts.add( new Pair<>( key, value ));
        } else {
            theFailedPeekNexts.add( new Pair<>( key, value ));
        }
    }

    @Override
    public void peekPreviousPerformed(
            Store<T> store,
            String   key,
            T        value,
            boolean  success )
    {
        if( success ) {
            theSuccessfulPeekPreviouss.add( new Pair<>( key, value ));
        } else {
            theFailedPeekPreviouss.add( new Pair<>( key, value ));
        }
    }

    @Override
    public void peekNextPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  value )
    {
        thePeekNextNs.add( Tuple.create( key, n, value ));
    }

    @Override
    public void peekPreviousPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  value )
    {
        thePeekPreviousNs.add( Tuple.create( key, n, value ));
    }

    @Override    
    public void hasNextPerformed(
            Store<T> store,
            String   key,
            boolean  ret )
    {
        theHasNexts.add( new Pair<>( key, ret ));        
    }

    @Override
    public void hasPreviousPerformed(
            Store<T> store,
            String   key,
            boolean  ret )
    {
        theHasPreviouss.add( new Pair<>( key, ret ));        
    }

    @Override
    public void hasNextPerformed(
            Store<T> store,
            String   key,
            int      n,
            boolean  ret )
    {
        theHasNextNs.add( Tuple.create( key, n, ret ));
    }

    @Override
    public void hasPreviousPerformed(
            Store<T> store,
            String   key,
            int      n,
            boolean  ret )
    {
        theHasPreviousNs.add( Tuple.create( key, n, ret ));
    }

    @Override
    public void nextPerformed(
            Store<T> store,
            String   key,
            T        ret,
            boolean  success )
    {
        if( success ) {
            theSuccessfulNexts.add( new Pair<>( key, ret ));
        } else {
            theFailedNexts.add( new Pair<>( key, ret ));
        }
    }

    @Override
    public void previousPerformed(
            Store<T> store,
            String   key,
            T        ret,
            boolean  success )
    {
        if( success ) {
            theSuccessfulPreviouss.add( new Pair<>( key, ret ));
        } else {
            theFailedPreviouss.add( new Pair<>( key, ret ));
        }
    }

    @Override
    public void nextPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  ret )
    {
        theNextNs.add( Tuple.create( key, n, ret ));
    }

    @Override
    public void previousPerformed(
            Store<T> store,
            String   key,
            int      n,
            List<T>  ret )
    {
        thePreviousNs.add( Tuple.create( key, n, ret ));
    }

    @Override
    public void moveByPerformed(
            Store<T> store,
            String   key,
            int      n,
            boolean  success )
    {
        if( success ) {
            theSuccessfulMoveBys.add( new Pair<>( key, n ));
        } else {
            theFailedMoveBys.add( new Pair<>( key, n ));
        }
    }

    @Override
    public void moveToBeforePerformed(
            Store<T> store,
            T        newPos,
            int      ret,
            boolean  success )
    {
        if( success ) {
            theSuccessfulMoveToBefores.add( new Pair<>( newPos, ret ));
        } else {
            theFailedMoveToBefores.add( new Pair<>( newPos, ret ));
        }
    }

    @Override
    public void moveToAfterPerformed(
            Store<T> store,
            T        newPos,
            int      ret,
            boolean  success )
    {
        if( success ) {
            theSuccessfulMoveToAfters.add( new Pair<>( newPos, ret ));
        } else {
            theFailedMoveToAfters.add( new Pair<>( newPos, ret ));
        }
    }

    @Override
    public void removePerformed(
            Store<T> store,
            String   key,
            boolean  success )
    {
        if( success ) {
            theSuccessfulRemoves.add( key );
        } else {
            theFailedRemoves.add( key );
        }
    }

    @Override
    public void moveToBeforePerformed(
            Store<T> store,
            String   position,
            String   key,
            int      ret,
            boolean  success )
    {
        if( success ) {
            theSuccessfulMoveToBeforeKeys.add( Tuple.create( position, key, ret ));
        } else {
            theFailedMoveToBeforeKeys.add( Tuple.create( position, key, ret ));
        }
    }

    @Override
    public void moveToAfterPerformed(
            Store<T> store,
            String   position,
            String   key,
            int      ret,
            boolean  success )
    {
        if( success ) {
            theSuccessfulMoveToAfterKeys.add( Tuple.create( position, key, ret ));            
        } else {
            theFailedMoveToAfterKeys.add( Tuple.create( position, key, ret ));
        }
    }

    @Override
    public void setPositionToPerformed(
            Store<T>          store,
            String            position,
            CursorIterator<T> newPosition,
            boolean           success )
    {
        if( success ) {
            theSuccessfulSetPositionTos.add( new Pair<>( position, newPosition.createCopy() ));
        } else {
            theFailedSetPositionTos.add( new Pair<>( position, newPosition.createCopy() ));
        }
    }

    @Override
    public void moveToBeforeFirstPerformed(
            Store<T> store,
            String   position,
            int      ret )
    {
        theMoveToBeforeFirst.add( new Pair<>( position, ret ));
    }

    @Override
    public void moveToAfterLastPerformed(
            Store<T> store,
            String   position,
            int      ret )
    {
        theMoveToAfterLasts.add( new Pair<>( position, ret ));
    }

    public RecordingStoreListener checkPuts(
            int successfulPuts,
            int failedPuts )
    {
        theTest.checkEquals( theSuccessfulPuts.size(),         successfulPuts,         "Wrong number of successful puts" );
        theTest.checkEquals( theFailedPuts.size(),             failedPuts,             "Wrong number of failed puts" );
        return this;
    }
    
    public RecordingStoreListener checkUpdates(
            int successfulUpdates,
            int failedUpdates )
    {
        theTest.checkEquals( theSuccessfulUpdates.size(),      successfulUpdates,      "Wrong number of successful updates" );
        theTest.checkEquals( theFailedUpdates.size(),          failedUpdates,          "Wrong number of failed updates" );
        return this;
    }

    public RecordingStoreListener checkGets(
            int successfulGets,
            int failedGets )
    {
        theTest.checkEquals( theSuccessfulGets.size(),         successfulGets,         "Wrong number of successful gets" );
        theTest.checkEquals( theFailedGets.size(),             failedGets,             "Wrong number of failed gets" );
        return this;
    }
    
    public RecordingStoreListener checkDeletes(
            int successfulDeletes,
            int failedDeletes,
            int successfulAllDeletes )
    {
        theTest.checkEquals( theSuccessfulDeletes.size(),      successfulDeletes,      "Wrong number of successful deletes" );
        theTest.checkEquals( theFailedDeletes.size(),          failedDeletes,          "Wrong number of failed deletes" );

        theTest.checkEquals( theAllDeletes.size(),             successfulAllDeletes,   "Wrong number of successful all deletes" );        
        return this;
    }

    public RecordingStoreListener checkContainsKeys(
            int successfulContainsKeys,
            int failedContainsKeys )
    {
        theTest.checkEquals( theSuccessfulContainsKeys.size(), successfulContainsKeys, "Wrong number of successful contains keys" );
        theTest.checkEquals( theFailedContainsKeys.size(),     failedContainsKeys,     "Wrong number of failed contains keys" );
        return this;
    }
    
    public RecordingStoreListener checkPeekNexts(
            int successfulPeekNexts,
            int failedPeekNexts,
            int peekNextNs )
    {
        theTest.checkEquals( theSuccessfulPeekNexts.size(), successfulPeekNexts, "Wrong number of successful peek nexts" );
        theTest.checkEquals( theFailedPeekNexts.size(),     failedPeekNexts,     "Wrong number of failed peek nexts" );
        theTest.checkEquals( thePeekNextNs.size(),          peekNextNs,          "Wrong number of peek next N" );
        return this;
    }

    public RecordingStoreListener checkPeekPreviouss(
            int successfulPeekPreviouss,
            int failedPeekPrevious,
            int peekPreviousNs )
    {
        theTest.checkEquals( theSuccessfulPeekPreviouss.size(), successfulPeekPreviouss, "Wrong number of successful peek previouss" );
        theTest.checkEquals( theFailedPeekPreviouss.size(),     failedPeekPrevious,     "Wrong number of failed peek previouss" );
        theTest.checkEquals( thePeekPreviousNs.size(),          peekPreviousNs,          "Wrong number of peek previous N" );
        return this;
    }

    public RecordingStoreListener checkHasNexts(
            int hasNexts,
            int hasNextNs )
    {
        theTest.checkEquals( theHasNexts.size(),  hasNexts,  "Wrong number of has nexts" );
        theTest.checkEquals( theHasNextNs.size(), hasNextNs, "Wrong number of has N nexts" );
        return this;
    }

    public RecordingStoreListener checkHasPrevious(
            int hasPreviouss,
            int hasPreviousNs )
    {
        theTest.checkEquals( theHasPreviouss.size(),  hasPreviouss,  "Wrong number of has previous" );
        theTest.checkEquals( theHasPreviousNs.size(), hasPreviousNs, "Wrong number of has N previouss" );
        return this;
    }

    public RecordingStoreListener checkNexts(
            int successfulNexts,
            int failedNexts,
            int nextNs )
    {
        theTest.checkEquals( theSuccessfulNexts.size(), successfulNexts, "Wrong number of successful nexts" );
        theTest.checkEquals( theFailedNexts.size(),     failedNexts,     "Wrong number of failed nexts" );
        theTest.checkEquals( theNextNs.size(),          nextNs,          "Wrong number of next Ns" );
        return this;
    }

    public RecordingStoreListener checkPreviouss(
            int successfulPreviouss,
            int failedPreviouss,
            int previousNs )
    {
        theTest.checkEquals( theSuccessfulPreviouss.size(), successfulPreviouss, "Wrong number of successful previouss" );
        theTest.checkEquals( theFailedPreviouss.size(),     failedPreviouss,     "Wrong number of failed previouss" );
        theTest.checkEquals( thePreviousNs.size(),          previousNs,          "Wrong number of previous Ns" );
        return this;
    }

    public RecordingStoreListener checkMoveBys(
            int successfulMoveBys,
            int failedMoveBys )
    {
        theTest.checkEquals( theSuccessfulMoveBys.size(), successfulMoveBys, "Wrong number of successful move by's" );
        theTest.checkEquals( theFailedMoveBys.size(),     failedMoveBys,     "Wrong number of failed move by's" );
        return this;
    }

    public RecordingStoreListener checkMoveToBefores(
            int successfulMoveToBefores,
            int failedMoveToBefores )
    {
        theTest.checkEquals( theSuccessfulMoveToBefores.size(), successfulMoveToBefores, "Wrong number of successful move to before's" );
        theTest.checkEquals( theFailedMoveToBefores.size(),     failedMoveToBefores,     "Wrong number of failed move to before's" );
        return this;
    }

    public RecordingStoreListener checkMoveToAfters(
            int successfulMoveToAfters,
            int failedMoveToAfterss )
    {
        theTest.checkEquals( theSuccessfulMoveToAfters.size(), successfulMoveToAfters, "Wrong number of successful move to after's" );
        theTest.checkEquals( theFailedMoveToAfters.size(),     failedMoveToAfterss,    "Wrong number of failed move to after's" );
        return this;
    }

    public RecordingStoreListener checkRemoves(
            int successfulRemoves,
            int failedRemoves )
    {
        theTest.checkEquals( theSuccessfulRemoves.size(), successfulRemoves, "Wrong number of successful removes" );
        theTest.checkEquals( theFailedRemoves.size(),     failedRemoves,     "Wrong number of failed removes" );
        return this;
    }

    public RecordingStoreListener checkMoveToBeforeKeys(
            int successfulMoveToBeforeKeys,
            int failedMoveToBeforeKeys )
    {
        theTest.checkEquals( theSuccessfulMoveToBeforeKeys.size(), successfulMoveToBeforeKeys, "Wrong number of successful move to before key's" );
        theTest.checkEquals( theFailedMoveToBeforeKeys.size(),     failedMoveToBeforeKeys,     "Wrong number of failed move to before key's" );
        return this;
    }

    public RecordingStoreListener checkMoveToAfterKeys(
            int successfulMoveToAfterKeys,
            int failedMoveToAfterKeys )
    {
        theTest.checkEquals( theSuccessfulMoveToBeforeKeys.size(), successfulMoveToAfterKeys, "Wrong number of successful move to after key's" );
        theTest.checkEquals( theFailedMoveToBeforeKeys.size(),     failedMoveToAfterKeys,     "Wrong number of failed move to after key's" );
        return this;
    }

    public RecordingStoreListener checkMoveToPositions(
            int successfulMoveToPositions,
            int failedMoveToPositions )
    {
        theTest.checkEquals( theSuccessfulSetPositionTos.size(), successfulMoveToPositions, "Wrong number of successful move to position's" );
        theTest.checkEquals( theFailedSetPositionTos.size(),     failedMoveToPositions,     "Wrong number of failed move to positions's" );
        return this;
    }

    public RecordingStoreListener checkMoveToBeforeFirsts(
            int successfulMoveToBeforeFirsts )
    {
        theTest.checkEquals( theMoveToBeforeFirst.size(), successfulMoveToBeforeFirsts, "Wrong number of successful move to before first's" );
        return this;
    }

    public RecordingStoreListener checkMoveToAfterLasts(
            int successfulMoveToAfterLasts )
    {
        theTest.checkEquals( theMoveToAfterLasts.size(), successfulMoveToAfterLasts, "Wrong number of successful move to after last's" );
        return this;
    }


    /**
     * Emit as String what we found:
     * 
     * @return String
     */
    @Override
    public String toString()
    {
        StringBuilder ret = new StringBuilder();
        
        ret.append( String.format( "PUT                success: %d\n", theSuccessfulPuts.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedPuts.size() ));

        ret.append( String.format( "UPDATE             success: %d\n", theSuccessfulUpdates.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedUpdates.size() ));

        ret.append( String.format( "GET                success: %d\n", theSuccessfulGets.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedGets.size() ));

        ret.append( String.format( "DELETE             success: %d\n", theSuccessfulDeletes.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedDeletes.size() ));
        ret.append( String.format( "                   all:     %d\n", theAllDeletes.size() ));

        ret.append( String.format( "CONTAINS           success: %d\n", theSuccessfulContainsKeys.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedContainsKeys.size() ));

        ret.append( String.format( "PEEK NEXT          success: %d\n", theSuccessfulPeekNexts.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedPeekNexts.size() ));

        ret.append( String.format( "PEEK PREVIOUS      success: %d\n", theSuccessfulPeekPreviouss.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedPeekPreviouss.size() ));

        ret.append( String.format( "PEEK NEXT     Ns:           %d (total: %d)\n", thePeekNextNs.size(),     aggCollectionAt2( thePeekNextNs )));
        ret.append( String.format( "PEEK PREVIOUS Ns:           %d (total: %d)\n", thePeekPreviousNs.size(), aggCollectionAt2( thePeekPreviousNs )));

        ret.append( String.format( "HAS NEXT:                   %d\n", theHasNexts.size() ));
        ret.append( String.format( "HAS PREVIOUS:               %d\n", theHasPreviouss.size() ));

        ret.append( String.format( "HAS NEXT     Ns:            %d (total: %d)\n", theHasNextNs.size(),     aggIntAt2( theHasNextNs )));
        ret.append( String.format( "HAS PREVIOUS Ns:            %d (total: %d)\n", theHasPreviousNs.size(), aggIntAt2( theHasPreviousNs )));

        ret.append( String.format( "NEXT               success: %d\n", theSuccessfulNexts.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedNexts.size() ));

        ret.append( String.format( "PRE                success: %d\n", theSuccessfulPreviouss.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedPreviouss.size() ));

        ret.append( String.format( "NEXT     Ns:                %d (total: %d)\n", theNextNs.size(),     aggCollectionAt2( theNextNs )));
        ret.append( String.format( "PREVIOUS Ns:                %d (total: %d)\n", thePreviousNs.size(), aggCollectionAt2( thePreviousNs )));

        ret.append( String.format( "MOVE BY            success: %d\n", theSuccessfulMoveBys.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedMoveBys.size() ));

        ret.append( String.format( "MOVE TO BEFORE     success: %d\n", theSuccessfulMoveToBefores.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedMoveToBefores.size() ));

        ret.append( String.format( "MOVE TO AFTER      success: %d\n", theSuccessfulMoveToAfters.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedMoveToAfters.size() ));

        ret.append( String.format( "REMOVE             success: %d\n", theSuccessfulRemoves.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedRemoves.size() ));

        ret.append( String.format( "MOVE TO BEFORE KEY success: %d\n", theSuccessfulMoveToBeforeKeys.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedMoveToBeforeKeys.size() ));

        ret.append( String.format( "MOVE TO AFTER KEY  success: %d\n", theSuccessfulMoveToAfterKeys.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedMoveToAfterKeys.size() ));

        ret.append( String.format( "SET POSITION TO    success: %d\n", theSuccessfulSetPositionTos.size() ));
        ret.append( String.format( "                   failed:  %d\n", theFailedSetPositionTos.size() ));

        ret.append( String.format( "MOVE TO BEFORE FIRST:       %d\n", theMoveToBeforeFirst.size() ));
        ret.append( String.format( "MOVE TO AFTER LAST:         %d\n", theMoveToAfterLasts.size() ));

        return ret.toString();
    }

    /**
     * Helper to aggregate.
     * 
     * @param items the items with stuff to aggregate
     * @return total
     */
    protected int aggCollectionAt2(
            ArrayList<Tuple> items )
    {
        int ret = 0;
        for( Tuple current : items ) {
            Object found = current.get( 2 );
            ret += ((Collection<?>)found).size();
        }
        return ret;
    }
    
    /**
     * Helper to aggregate.
     * 
     * @param items the items with stuff to aggregate
     * @return total
     */
    protected int aggIntAt2(
            ArrayList<Tuple> items )
    {
        int ret = 0;
        for( Tuple current : items ) {
            Object found = current.get( 2 );
            ret += (Integer)found;
        }
        return ret;
    }

    /**
     * Reset the listener.
     */
    public void reset()
    {
        theSuccessfulPuts.clear();
        theFailedPuts.clear();

        theSuccessfulUpdates.clear();
        theFailedUpdates.clear();

        theSuccessfulGets.clear();
        theFailedGets.clear();

        theSuccessfulDeletes.clear();
        theFailedDeletes.clear();

        theAllDeletes.clear();

        theSuccessfulContainsKeys.clear();
        theFailedContainsKeys.clear();

        theSuccessfulPeekNexts.clear();
        theFailedPeekNexts.clear();

        theSuccessfulPeekPreviouss.clear();
        theFailedPeekPreviouss.clear();
    
        thePeekNextNs.clear();
        thePeekPreviousNs.clear();

        theHasNexts.clear();
        theHasPreviouss.clear();

        theHasNextNs.clear();
        theHasPreviousNs.clear();

        theSuccessfulNexts.clear();
        theFailedNexts.clear();

        theSuccessfulPreviouss.clear();
        theFailedPreviouss.clear();

        theNextNs.clear();
        thePreviousNs.clear();

        theSuccessfulMoveBys.clear();
        theFailedMoveBys.clear();

        theSuccessfulMoveToBefores.clear();
        theFailedMoveToBefores.clear();

        theSuccessfulMoveToAfters.clear();
        theFailedMoveToAfters.clear();

        theSuccessfulRemoves.clear();
        theFailedRemoves.clear();

        theSuccessfulMoveToBeforeKeys.clear();
        theFailedMoveToBeforeKeys.clear();

        theSuccessfulMoveToAfterKeys.clear();
        theFailedMoveToAfterKeys.clear();

        theSuccessfulSetPositionTos.clear();
        theFailedSetPositionTos.clear();

        theMoveToBeforeFirst.clear();
        theMoveToAfterLasts.clear();
    }

    protected final AbstractTest theTest;

    public final ArrayList<String> theSuccessfulPuts = new ArrayList<>();
    public final ArrayList<String> theFailedPuts     = new ArrayList<>();

    public final ArrayList<String> theSuccessfulUpdates = new ArrayList<>();
    public final ArrayList<String> theFailedUpdates     = new ArrayList<>();

    public final ArrayList<String> theSuccessfulGets = new ArrayList<>();
    public final ArrayList<String> theFailedGets     = new ArrayList<>();

    public final ArrayList<String> theSuccessfulDeletes = new ArrayList<>();
    public final ArrayList<String> theFailedDeletes     = new ArrayList<>();

    public final ArrayList<Pair<String,Integer>> theAllDeletes = new ArrayList<>();

    public final ArrayList<String> theSuccessfulContainsKeys = new ArrayList<>();
    public final ArrayList<String> theFailedContainsKeys     = new ArrayList<>();
    
    public final ArrayList<Pair<String,T>> theSuccessfulPeekNexts = new ArrayList<>();
    public final ArrayList<Pair<String,T>> theFailedPeekNexts = new ArrayList<>();

    public final ArrayList<Pair<String,T>> theSuccessfulPeekPreviouss = new ArrayList<>();
    public final ArrayList<Pair<String,T>> theFailedPeekPreviouss = new ArrayList<>();
    
    public final ArrayList<Tuple> thePeekNextNs
            = new ArrayList<>();
    public final ArrayList<Tuple> thePeekPreviousNs = new ArrayList<>();

    public final ArrayList<Pair<String,Boolean>> theHasNexts = new ArrayList<>();
    public final ArrayList<Pair<String,Boolean>> theHasPreviouss = new ArrayList<>();

    public final ArrayList<Tuple> theHasNextNs = new ArrayList<>();
    public final ArrayList<Tuple> theHasPreviousNs = new ArrayList<>();

    public final ArrayList<Pair<String,T>> theSuccessfulNexts = new ArrayList<>();
    public final ArrayList<Pair<String,T>> theFailedNexts     = new ArrayList<>();

    public final ArrayList<Pair<String,T>> theSuccessfulPreviouss = new ArrayList<>();
    public final ArrayList<Pair<String,T>> theFailedPreviouss     = new ArrayList<>();

    public final ArrayList<Tuple> theNextNs = new ArrayList<>();
    public final ArrayList<Tuple> thePreviousNs = new ArrayList<>();

    public final ArrayList<Pair<String,Integer>> theSuccessfulMoveBys = new ArrayList<>();
    public final ArrayList<Pair<String,Integer>> theFailedMoveBys = new ArrayList<>();

    public final ArrayList<Pair<T,Integer>> theSuccessfulMoveToBefores = new ArrayList<>();
    public final ArrayList<Pair<T,Integer>> theFailedMoveToBefores = new ArrayList<>();

    public final ArrayList<Pair<T,Integer>> theSuccessfulMoveToAfters = new ArrayList<>();
    public final ArrayList<Pair<T,Integer>> theFailedMoveToAfters = new ArrayList<>();

    public final ArrayList<String> theSuccessfulRemoves = new ArrayList<>();
    public final ArrayList<String> theFailedRemoves     = new ArrayList<>();

    public final ArrayList<Tuple> theSuccessfulMoveToBeforeKeys = new ArrayList<>();
    public final ArrayList<Tuple> theFailedMoveToBeforeKeys = new ArrayList<>();

    public final ArrayList<Tuple> theSuccessfulMoveToAfterKeys = new ArrayList<>();
    public final ArrayList<Tuple> theFailedMoveToAfterKeys = new ArrayList<>();

    public final ArrayList<Pair<String,CursorIterator<T>>> theSuccessfulSetPositionTos = new ArrayList<>();
    public final ArrayList<Pair<String,CursorIterator<T>>> theFailedSetPositionTos = new ArrayList<>();

    public final ArrayList<Pair<String,Integer>> theMoveToBeforeFirst = new ArrayList<>();
    public final ArrayList<Pair<String,Integer>> theMoveToAfterLasts = new ArrayList<>();
}
