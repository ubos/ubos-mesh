//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import net.ubos.store.StoreValue;
import net.ubos.store.keystore.KeyStoreWrapper;
import net.ubos.util.StreamUtils;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the Keystore.
 */
public abstract class AbstractSqlKeyStoreTest1
        extends
            AbstractSqlStoreTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @Override
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating new database" );

        theSqlStore.initializeHard();

        //

        log.info( "Creating the KeyStoreWrapper" );

        KeyStoreWrapper wrapper = KeyStoreWrapper.create( theTestStore, KEY_INTO_STORE, theKeyStorePassword );

        InputStream inStream = AbstractSqlKeyStoreTest1.class.getResourceAsStream( theKeyStoreFile );
        wrapper.load( inStream );

        wrapper = null;
        collectGarbage();

        //

        log.info( "checking that the content is in the Store" );

        StoreValue v = theTestStore.get( KEY_INTO_STORE );
        checkObject( v, "No StoreValue found" );
        checkCondition( v.getData().length > 0, "empty data in store" );

        //

        log.info( "trying to recover" );

        wrapper = KeyStoreWrapper.create( theTestStore, KEY_INTO_STORE, theKeyStorePassword );

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        wrapper.store( outStream );

        byte [] dataFromStore = outStream.toByteArray();
        byte [] dataFromFile  = StreamUtils.slurp(AbstractSqlKeyStoreTest1.class.getResourceAsStream( theKeyStoreFile ));

        checkEqualByteArrays( dataFromFile, dataFromStore, "different content" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractSqlKeyStoreTest1.class );

    /**
     * Key into to the store for the key Store content.
     */
    public static final String KEY_INTO_STORE = "key-store-test-key";

    /**
     * THe test file.
     */
    protected String theKeyStoreFile;

    /**
     * The password on the key store.
     */
    protected String theKeyStorePassword;
}
