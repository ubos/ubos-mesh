//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

import java.util.NoSuchElementException;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests one-element CursorIterators.
 * Tests any StoreIterator. Subclasses define the Store implementation.
 */
public abstract class AbstractStoreIteratorTest2
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating the Store" );

        theTestStore.initializeHard();

        //

        log.info( "Inserting data" );

        theTestStore.put( new StoreValue(
                testData.getKey(),
                testData.getEncodingId(),
                testData.getData() ));

        CursorIterator<StoreValue> iter = theTestStore.iterator();

        //

        //

        log.info( "Check at the beginning" );

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( iter.hasNext( 1 ), "Does not have enough nexts" );
        checkCondition( !iter.hasNext( 2 ), "Has too many nexts" );
        checkEquals( iter.peekNext(), testData, "wrong current element" );

        //

        log.info( "Check forward iteration" );

        checkCondition( iter.hasNext(), "Not found next" );

        Object found = iter.next();
        checkEquals( testData, found, "Not found element" );

        //

        log.info( "Check at the end" );

        checkCondition( !iter.hasNext(), "has next at the end" );
        checkCondition( iter.hasPrevious( 1 ), "Does not have enough previous" );
        checkCondition( !iter.hasPrevious( 2 ), "Has too many previous" );
        checkEquals( iter.peekPrevious(), testData, "wrong last element" );

        try {
            Object found2 = iter.peekNext();
            reportError( "Found element after end", found2 );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }

        //

        log.info( "Check backward iteration" );

        checkCondition( iter.hasPrevious(), "Not found previous" );

        Object found3 = iter.previous();
        checkEquals( testData, found3, "Not found element" );

        //

        log.info( "Check again at the beginning" );

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( iter.hasNext( 1 ), "Does not have enough nexts" );
        checkCondition( !iter.hasNext( 2 ), "Has too many nexts" );

        try {
            Object found4 = iter.peekPrevious();
            reportError( "Found element before beginning", found4 );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }

        //

        log.info( "Move to element" );

        iter.moveToBefore( testData ); // "e"
        checkEquals( iter.peekNext(),     testData, "wrong element found" );
        checkCondition( !iter.hasPrevious(), "previous found" );

        //

        log.info( "Copy" );

        CursorIterator<?> copy = iter.createCopy();

        checkEquals( iter.peekNext(), copy.peekNext(), "copied iterator in a different place" );

        //

        log.info( "Go to past last" );

        iter.moveToAfterLast();

        checkCondition( !iter.hasNext(), "has next at the end" );
        checkCondition( iter.hasPrevious( 1 ), "Does not have enough previous" );
        checkCondition( !iter.hasPrevious( 2 ), "Has too many previous" );
        checkEquals( iter.peekPrevious(), testData, "wrong last element" );

        try {
            Object found5 = iter.peekNext();
            reportError( "Found element after end", found5 );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }

        //

        log.info( "Go before first" );

        iter.moveToBeforeFirst();

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( iter.hasNext( 1 ), "Does not have enough nexts" );
        checkCondition( !iter.hasNext( 2 ), "Has too many nexts" );
        checkEquals( iter.peekNext(), testData, "wrong current element" );

        try {
            Object found6 = iter.peekPrevious();
            reportError( "Found element before beginning", found6 );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( AbstractStoreIteratorTest2.class );

    /**
     * The actual Store to be tested. This may or may not be pointed to theSqlStore
     * by subclasses.
     */
    protected Store<StoreValue> theTestStore;

    /**
     * Test data.
     */

    protected static final long now = System.currentTimeMillis();
    protected static final StoreValue testData =
        new StoreValue( "a", "enc1", bytes( "some data" ) );
}
