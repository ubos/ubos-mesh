//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.filesystem;

import java.io.File;
import net.ubos.store.test.AbstractStoreIteratorTest1;
import net.ubos.store.filesystem.FilesystemStore;
import org.junit.jupiter.api.BeforeEach;

/**
 * Tests the FilesystemStoreIterator.
 */
public class FilesystemStoreIteratorTest1
        extends
            AbstractStoreIteratorTest1
{
    @BeforeEach
    public void setup()
    {
        File subdir = new File( AbstractFilesystemStoreTest.test_SUBDIR_NAME );

        theTestStore = FilesystemStore.create( subdir );
    }
}
