//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.encrypted.EncryptedStore;
import net.ubos.store.sql.AbstractSqlStore;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Factors out common functionality of SqlStoreTests.
 */
public abstract class AbstractSqlStoreTest
        extends
            AbstractTest
{
    /**
     * Run the test encrypted.
     *
     * @throws Exception all sorts of things can go wrong in a test
     */
    @Test
    public void runEncrypted()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String  transformation = "AES/CBC/PKCS5Padding";
        byte [] keyBytes = new byte[] { // needs 16 bytes = 128 bits
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
        };
        SecretKeySpec key = new SecretKeySpec( keyBytes, "AES" );

        theEncryptedStore = EncryptedStore.create( transformation, key, new IvParameterSpec( new byte[16] ), theSqlStore );
        theTestStore      = theEncryptedStore;

        run();
    }

    /**
     * Run the test.
     *
     * @throws Exception all sorts of things can go wrong in a test
     */
    public abstract void run()
        throws
            Exception;

    /**
     * The AbstractSqlStore to be tested.
     */
    protected AbstractSqlStore<StoreValue> theSqlStore;

    /**
     * The actual Store to be tested. This may or may not be pointed to theSqlStore
     * by subclasses.
     */
    protected Store<StoreValue> theTestStore;

    /**
     * Encrypted Store, if the encrypted test is being run.
     */
    protected Store<StoreValue> theEncryptedStore;


    private static final Log log = Log.getLogInstance( AbstractSqlStoreTest.class );
}
