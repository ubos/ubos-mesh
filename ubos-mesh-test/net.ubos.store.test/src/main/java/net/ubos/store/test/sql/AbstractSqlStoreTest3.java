//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.store.test.RecordingStoreListener;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that we can recover from a closed database connection. It basically
 * performed SqlStoreTest1 (thus the implementation inheritance) but occasionally
 * closes the database connection.
 */
public abstract class AbstractSqlStoreTest3
        extends
            AbstractSqlStoreTest1
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Override
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating new database" );

        theSqlStore.initializeHard();

        RecordingStoreListener<StoreValue> listener = new RecordingStoreListener<>( this );
        theTestStore.addDirectStoreListener( listener );

        //

        log.info( "Inserting data and checking it's there" );

        for( int i=0 ; i<firstSet.length ; ++i ) {
            TestData current = firstSet[i];
            theTestStore.put( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));

            if( i % 2 == 1 ) {
                theSqlStore.getDatabase().closeConnection();
            }
        }

        listener.checkPuts( firstSet.length, 0 ).checkUpdates( 0, 0 ).checkGets( 0, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        for( int i=0 ; i<firstSet.length ; ++i ) {
            TestData   current = firstSet[i];
            StoreValue value   = theTestStore.get( current.theKey );

            checkEquals(          current.theEncodingId, value.getEncodingId(), "not the same encodingId" );
            checkEqualByteArrays( current.theData,       value.getData(),       "not the same content" );

            if( i % 3 == 2 ) {
                theSqlStore.getDatabase().closeConnection();
            }
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( firstSet.length, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        //

        log.info( "Iterating over what's in the Store" );

        int count = 0;
        for( StoreValue current : theTestStore ) {
            ++count;
            TestData found = null;
            for( TestData data : firstSet ) {
                if( data.theKey.equals( current.getKey() )) {
                    found = data;
                    break;
                }
            }
            if( found == null ) {
                reportError( "Could not find record with key", current.getKey() );
            }
        }
        checkEquals( count, firstSet.length, "wrong length of set" );

        //

        log.info( "Updating data and checking it's there" );

        for( int i=0 ; i<secondSet.length ; ++i ) {
            TestData current = secondSet[i];
            theTestStore.update( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));

            if( i % 4 == 3 ) {
                theSqlStore.getDatabase().closeConnection();
            }
        }

        listener.checkPuts( 0, 0 ).checkUpdates( secondSet.length, 0 ).checkGets( 0, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        for( int i=0 ; i<secondSet.length ; ++i ) {
            TestData   current = secondSet[i];
            StoreValue value   = theTestStore.get( current.theKey );

            checkEquals(          current.theEncodingId, value.getEncodingId(), "not the same encodingId" );
            checkEqualByteArrays( current.theData,       value.getData(),       "not the same content" );

            if( i % 2 == 1 ) {
                theSqlStore.getDatabase().closeConnection();
            }
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( secondSet.length, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        //

        log.info( "Deleting some data and checking it's gone" );

        for( int i=0 ; i<thirdSet.length ; ++i ) {
            TestData current = thirdSet[i];
            theTestStore.delete( current.theKey );

            if( i % 2 == 1 ) {
                theSqlStore.getDatabase().closeConnection();
            }
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( 0, 0 ).checkDeletes( thirdSet.length, 0, 0 );
        listener.reset();

        for( int i=0 ; i<thirdSet.length ; ++i ) {
            TestData current = thirdSet[i];
            try {
                theTestStore.get( current.theKey );

                reportError( "delete was unsuccessful", current );

                if( i % 3 == 2 ) {
                    theSqlStore.getDatabase().closeConnection();
                }

            } catch( StoreKeyDoesNotExistException ex ) {
                // noop
            }
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( 0, thirdSet.length ).checkDeletes( 0, 0, 0 );
        listener.reset();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractSqlStoreTest3.class);
}
