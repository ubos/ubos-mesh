//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

import net.ubos.testharness.AbstractTest;

/**
 * Factors out common functionality for tests in this package..
 */
public abstract class AbstractStoreTest
        extends
            AbstractTest
{
    /**
     * The EncodingId for the tests.
     */
    public static final String ENCODING_ID = "TestEncodingId";
}
