//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.history;

import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.m.MStoreWithHistory;
import net.ubos.store.history.util.StoreWithHistoryBackedSwappingHistory;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.history.HasTimeUpdatedPair;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests navigation.
 *
 * see HistoryCursorIteratorTest1
 */
public class StoreBackedSwappingHistoryTest1
        extends
            AbstractTest
{
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Setup" );

        final long [] testData = new long[] { 0, 1000, 2000, 2999, 3000, 3001, 4000 };

        StoreWithHistory<StoreValueWithTimeUpdated> store = MStoreWithHistory.create();

        StoreWithHistoryBackedSwappingHistory<HasTimeUpdatedPair<String>,StoreValueWithTimeUpdated> history
                = StoreWithHistoryBackedSwappingHistory.Builder.create(
                        "mykey",
                        new TestStoreValueWithTimeUpdatedMapper(),
                        store )
                .build();

        for( int i=0 ; i<testData.length ; ++i ) {
            history.putOrThrow( new HasTimeUpdatedPair<>( testData[i], toPayload( testData[i] )));
        }

        HistoryCursorIterator<HasTimeUpdatedPair<String>> iter = history.iterator();
        HasTimeUpdatedPair<String> found;
        int  count;

        //

        log.info( "Has the right length" );

        checkCondition( !iter.hasPrevious(), "has previous" );
        checkCondition( iter.hasNext(), "no next" );
        checkCondition( iter.hasNext( testData.length ), "not " + testData.length + " nexts" );
        checkCondition( !iter.hasNext( testData.length + 1 ), "too many nexts" );

        //

        log.info( "Before and after each step" );

        for( int i=0 ; i<testData.length ; ++i ) {
            found = iter.next();
            checkEquals( found.getPayload(), toPayload( testData[i] ), "Wrong content at " + i );

            if( i>0 ) {
                found = iter.peekPrevious();
                checkEquals( found.getPayload(), toPayload( testData[i] ), "Wrong content before " + i );
            }
            if( i<testData.length-1 ) {
                found = iter.peekNext();
                checkEquals( found.getPayload(), toPayload( testData[i+1] ), "Wrong content after " + i );
            }
        }

        //

        log.info( "Start and end" );

        iter.moveToBeforeFirst();
        checkCondition( !iter.hasPrevious(), "has previous" );

        found = iter.peekNext();
        checkEquals( found.getPayload(), toPayload( testData[0] ), "wrong first" );

        iter.moveToAfterLast();
        checkCondition( !iter.hasNext(), "has next" );

        found = iter.peekPrevious();
        checkEquals( found.getPayload(), toPayload( testData[testData.length-1] ), "wrong last" );

        //

        log.info( "Move to just before specific time" );

        iter.moveToBeforeFirst();
        count = iter.moveToJustBeforeTime( testData[2] );
        found = iter.peekNext();

        checkEquals( count, 2, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[2] ), "Wrong 2" );

        iter.moveToAfterLast();
        count = iter.moveToJustBeforeTime( testData[2] );
        found = iter.peekNext();

        checkEquals( count, 2 - testData.length, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[2] ), "Wrong 2" );

        //

        log.info( "Move to just after specific time" );

        iter.moveToBeforeFirst();
        count = iter.moveToJustAfterTime( testData[2] );
        found = iter.peekNext();

        checkEquals( count, 3, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[3] ), "Wrong 3" );

        iter.moveToAfterLast();
        count = iter.moveToJustAfterTime( testData[2] );
        found = iter.peekNext();

        checkEquals( count, 3 - testData.length, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[3] ), "Wrong 3" );

        //

        log.info( "Move to before specific content" );

        iter.moveToBeforeFirst();
        count = iter.moveToBefore( new HasTimeUpdatedPair<>( testData[2], toPayload( testData[2] )));
        found = iter.peekNext();

        checkEquals( count, 2, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[2] ), "Wrong 2" );

        iter.moveToAfterLast();
        count = iter.moveToBefore( new HasTimeUpdatedPair<>( testData[2], toPayload( testData[2] )));
        found = iter.peekNext();

        checkEquals( count, 2 - testData.length, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[2] ), "Wrong 2" );

        //

        log.info( "Move to after specific content" );

        iter.moveToBeforeFirst();
        count = iter.moveToAfter( new HasTimeUpdatedPair<>( testData[2], toPayload( testData[2] )));
        found = iter.peekNext();

        checkEquals( count, 3, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[3] ), "Wrong 3" );

        iter.moveToAfterLast();
        count = iter.moveToAfter( new HasTimeUpdatedPair<>( testData[2], toPayload( testData[2] )));
        found = iter.peekNext();

        checkEquals( count, 3 - testData.length, "Wrong number moves" );
        checkEquals( found.getPayload(), toPayload( testData[3] ), "Wrong 3" );

        //

        log.info(  "Off-by-one -- time " );

        for( int i=3 ; i<=5 ; ++i ) { // points to the data that is off by one

            iter.moveToBeforeFirst();
            count = iter.moveToJustBeforeTime( testData[i] );
            found = iter.peekNext();

            checkEquals( count, i, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i] ), "Wrong " + i );

            iter.moveToAfterLast();
            count = iter.moveToJustBeforeTime( testData[i] );
            found = iter.peekNext();

            checkEquals( count, i - testData.length, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i] ), "Wrong " + i );

            iter.moveToBeforeFirst();
            count = iter.moveToJustAfterTime( testData[i] );
            found = iter.peekNext();

            checkEquals( count, i+1, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i+1] ), "Wrong " + (i+1) );

            iter.moveToAfterLast();
            count = iter.moveToJustAfterTime( testData[i] );
            found = iter.peekNext();

            checkEquals( count, i+1 - testData.length, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i+1] ), "Wrong " + (i+1) );
        }

        //

        log.info(  "Off-by-one -- content " );

        for( int i=3 ; i<=5 ; ++i ) { // points to the data that is off by one

            iter.moveToBeforeFirst();
            count = iter.moveToBefore( new HasTimeUpdatedPair<>( testData[i], toPayload( testData[i] )));
            found = iter.peekNext();

            checkEquals( count, i, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i] ), "Wrong " + i );

            iter.moveToAfterLast();
            count = iter.moveToBefore( new HasTimeUpdatedPair<>( testData[i], toPayload( testData[i] )) );
            found = iter.peekNext();

            checkEquals( count, i - testData.length, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i] ), "Wrong " + i );

            iter.moveToBeforeFirst();
            count = iter.moveToAfter( new HasTimeUpdatedPair<>( testData[i], toPayload( testData[i] )) );
            found = iter.peekNext();

            checkEquals( count, i+1, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i+1] ), "Wrong " + (i+1) );

            iter.moveToAfterLast();
            count = iter.moveToAfter( new HasTimeUpdatedPair<>( testData[i], toPayload( testData[i] )) );
            found = iter.peekNext();

            checkEquals( count, i+1 - testData.length, "Wrong number moves" );
            checkEquals( found.getPayload(), toPayload( testData[i+1] ), "Wrong " + (i+1) );
        }
    }

    /**
     * Helper method to convert the timestamp to a test payload.
     */
    protected static String toPayload(
            long t )
    {
        final String  FORMAT  = "data-%04d";

        String ret = String.format( FORMAT, t );
        return ret;
    }

    private static final Log log = Log.getLogInstance(StoreBackedSwappingHistoryTest1.class ); // our own, private logger
}
