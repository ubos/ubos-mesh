//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.smartmap;

import net.ubos.store.util.SerializingStoreValueMapper;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.store.test.AbstractStoreTest;
import net.ubos.store.util.StoreBackedSwappingSmartMap;
import net.ubos.util.logging.Log;

/**
 * Tests iteration over the content of a StoreBackedSwappingHashMap.
 */
public abstract class AbstractStoreBackedSwappingSmartMapTest2
        extends
            AbstractStoreTest
{
    /**
     * Run the test.
     *
     * @param store the Store under test
     * @throws Exception all sorts of things may go wrong during a test
     */
    protected void run(
            Store<StoreValue> store )
        throws
            Exception
    {
        SerializingStoreValueMapper<String,Integer> mapper = new SerializingStoreValueMapper<>() {
            @Override
            public String stringToKey(
                    String s )
            {
                return s;
            }
            @Override
            public String getDefaultEncodingId()
            {
                return "enc-2";
            }
        };

        StoreBackedSwappingSmartMap<String,Integer,StoreValue> map = StoreBackedSwappingSmartMap.Builder.create( mapper, store ).useWeak().build();

        String [] keys = {
            "a",
            "d",
            "c",
            "e",
            "b",
        };

        log.info( "Inserting" );

        for( int i=0 ; i<keys.length ; ++i ) {
            map.put( keys[i], i );
        }

        checkEquals( map.size(), keys.length, "Wrong size map" );

        //

        log.info( "Waiting and iterating" );

        Thread.sleep( 2000L );
        collectGarbage();

        int count = 0;
        for( String key : map.keyIterator()) {
            Integer value = map.get( key );

            checkObject( value, "Value not found for key " + key );

            // they come in a different sequence
            int found = -1;
            for( int i=0 ; i<keys.length ; ++i ) {
                if( key.equals( keys[i] )) {
                    found = i;
                    break;
                }
            }
            if( found < 0 ) {
                reportError( "Could not find key in the original key list", key );
            } else {
                checkEquals( found, value.intValue(), "Wrong value found at index " + found );
            }

            ++count;
        }
        checkEquals( count,      keys.length, "Wrong number of elements found via iterator" );
        checkEquals( map.size(), keys.length, "Wrong size map" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractStoreBackedSwappingSmartMapTest2.class );
}
