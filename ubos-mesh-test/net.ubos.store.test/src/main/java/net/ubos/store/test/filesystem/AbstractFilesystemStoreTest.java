//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.filesystem;

import java.io.File;
import net.ubos.store.filesystem.FilesystemStore;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.testharness.AbstractTest;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality of FilesystemStoreTests.
 */
public abstract class AbstractFilesystemStoreTest
        extends
            AbstractTest
{
    @BeforeEach
    public void setup()
    {
        File subdir = new File( test_SUBDIR_NAME );

        theFilesystemStore = FilesystemStore.create( subdir );
    }

    /**
     * The FilesystemStore to be tested.
     */
    protected FilesystemStore theFilesystemStore;

    /**
     * The actual Store to be tested. This may or may not be pointed to theFilesystemStore
     * by subclasses.
     */
    protected Store<StoreValue> theTestStore;

    /**
     * The name of the subdirectory in which to store test data
     */
    public static final String test_SUBDIR_NAME = "target/test-FilesystemStore";

    /**
     * The EncodingId for the tests.
     */
    public static final String ENCODING_ID = "TestEncodingId";
}
