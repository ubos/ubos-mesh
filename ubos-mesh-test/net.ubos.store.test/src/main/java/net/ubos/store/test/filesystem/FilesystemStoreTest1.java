//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.filesystem;

import java.io.IOException;
import java.io.File;
import net.ubos.store.filesystem.FilesystemStore;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.store.test.RecordingStoreListener;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StreamUtils;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests the basic FilesystemStore functions. See also SqlStoreTest1.
 */
public class FilesystemStoreTest1
        extends
            AbstractFilesystemStoreTest
{
    @BeforeEach
    @Override
    public void setup()
    {
        File subdir = new File( AbstractFilesystemStoreTest.test_SUBDIR_NAME );

        theFilesystemStore = FilesystemStore.create( subdir );
        theTestStore       = theFilesystemStore;
    }

    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        //

        log.info( "Deleting old Store and creating new Store" );

        try {
            theFilesystemStore.deleteAll();
        } catch( IOException ex ) {
            // ignore this one
        }

        RecordingStoreListener<StoreValue> listener = new RecordingStoreListener<>( this );
        theTestStore.addDirectStoreListener( listener );

        //

        log.info( "Inserting data and checking it's there" );

        for( int i=0 ; i<firstSet.length ; ++i ) {
            TestData current = firstSet[i];
            theTestStore.put( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));
        }
        listener.checkPuts( firstSet.length, 0 );
        listener.reset();

        for( int i=0 ; i<firstSet.length ; ++i ) {
            TestData   current = firstSet[i];
            StoreValue value   = theTestStore.get( current.theKey );

            checkEquals(          current.theEncodingId, value.getEncodingId(), "not the same encodingId" );
            checkEqualByteArrays( current.theData,       value.getData(),       "not the same content" );
        }

        checkEquals( listener.theSuccessfulPuts.size(),    0,               "Wrong number of successful puts" );
        checkEquals( listener.theFailedPuts.size(),        0,               "Wrong number of failed puts" );
        checkEquals( listener.theSuccessfulUpdates.size(), 0,               "Wrong number of successful updates" );
        checkEquals( listener.theFailedUpdates.size(),     0,               "Wrong number of failed updates" );
        checkEquals( listener.theSuccessfulGets.size(),    firstSet.length, "Wrong number of successful gets" );
        checkEquals( listener.theFailedGets.size(),        0,               "Wrong number of failed gets" );
        checkEquals( listener.theSuccessfulDeletes.size(), 0,               "Wrong number of successful deletes" );
        checkEquals( listener.theFailedDeletes.size(),     0,               "Wrong number of failed deletes" );

        listener.checkPuts( 0, 0 ).checkGets( firstSet.length, 0 );
        listener.reset();

        //

        log.info( "Iterating over what's in the Store" );

        int count = 0;
        for( StoreValue current : theTestStore ) {
            log.debug( "Found " + count + ": " + current.getKey() );
            ++count;
            TestData found = null;
            for( TestData data : firstSet ) {
                if( data.theKey.equals( current.getKey() )) {
                    found = data;
                    break;
                }
            }
            if( found == null ) {
                reportError( "Could not find record with key", current.getKey() );
            }
        }
        checkEquals( count, firstSet.length, "wrong length of set" );

        //

        log.info( "Updating data and checking it's there" );

        for( int i=0 ; i<secondSet.length ; ++i ) {
            TestData current = secondSet[i];
            theTestStore.update( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));
        }

        listener.checkPuts( 0, 0 ).checkUpdates( secondSet.length, 0 ).checkGets( 0, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        for( int i=0 ; i<secondSet.length ; ++i ) {
            TestData   current = secondSet[i];
            StoreValue value   = theTestStore.get( current.theKey );

            checkEquals(          current.theEncodingId, value.getEncodingId(), "not the same encodingId" );
            checkEqualByteArrays( current.theData,       value.getData(),       "not the same content" );
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( secondSet.length, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        //

        log.info( "Deleting some data and checking it's gone" );

        for( int i=0 ; i<thirdSet.length ; ++i ) {
            TestData current = thirdSet[i];
            theTestStore.delete( current.theKey );
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( 0, 0 ).checkDeletes( thirdSet.length, 0, 0 );
        listener.reset();

        for( int i=0 ; i<thirdSet.length ; ++i ) {
            TestData current = thirdSet[i];
            try {
                theTestStore.get( current.theKey );

                reportError( "delete was unsuccessful", current );

            } catch( StoreKeyDoesNotExistException ex ) {
                // noop
            }
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( 0, thirdSet.length ).checkDeletes( 0, 0, 0 );
        listener.reset();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( FilesystemStoreTest1.class );

    protected static final long now = System.currentTimeMillis();
    protected static final byte [] testImage;
    static {
        byte [] data = new byte[0];
        try {
            data = StreamUtils.slurp( FilesystemStoreTest1.class.getResourceAsStream( "TestImage.jpg" ));
        } catch( IOException ex ) {
            log.error( ex );
        }
        testImage = data;
    }
    protected static final TestData[] one = new TestData[] {
        new TestData( "/x/abc",       "enc1",           bytes( "some data" )),
        new TestData( "/x/def",       "other encoding", bytes( "some longer data, but not very long" )),
        new TestData( "/x",           "third encoding", testImage )
    };
    protected static final TestData[] two = new TestData[] {
        new TestData( "/x/abc", "testing", bytes( "some changed data" )),
    };
    protected static final TestData[] three = new TestData[] {
        new TestData( "/x/ghi", "Shakespeare's collected cucumbers", bytes( "Some <b>HTML</b> data" ))
    };
    protected static final TestData[] firstSet  = ArrayHelper.append( one, three, TestData.class );
    protected static final TestData[] secondSet = two;
    protected static final TestData[] thirdSet  = three;

    protected static class TestData
    {
        public TestData(
                String  key,
                String  encodingId,
                byte [] data )
        {
            theKey          = key;
            theEncodingId   = encodingId;
            theData         = data;
        }

        @Override
        public String toString()
        {
            return "TestData with key '" + theKey + "'";
        }

        String  theKey;
        String  theEncodingId;
        byte [] theData;
    }
}
