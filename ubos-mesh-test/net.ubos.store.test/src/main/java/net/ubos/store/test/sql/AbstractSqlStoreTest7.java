//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests that keys are case-sensitive.
 */
public abstract class AbstractSqlStoreTest7
        extends
            AbstractSqlStoreTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @Override
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        String key1  = "abc";
        String key2  = "ABC";
        String enc   = "enc";
        byte [] data = bytes( "some data" );

        //

        log.info( "Deleting old database and creating new database" );

        theSqlStore.initializeHard();

        checkEquals( theSqlStore.size(), 0, "Store not empty" );

        //

        log.info( "Inserting key1 and checking it's there" );

        theTestStore.put( new StoreValue( key1, enc, data ));
        checkEquals( theSqlStore.size(), 1, "Wrong stuff in store" );

        //

        log.info( "Check that we can't retrieve with wrong key" );

        checkEqualByteArrays( theTestStore.get( key1 ).getData(), data, "Wrong data retrieved" );
        try {
            StoreValue found = theTestStore.get( key2 );
            reportError( "Could retrieve data with wrong key", found );

        } catch( StoreKeyDoesNotExistException ex ) {
            // ok
        }

        //

        log.info( "Inserting key2 and checking it's there" );

        theTestStore.put( new StoreValue( key2, enc, data ));
        checkEquals( theSqlStore.size(), 2, "Wrong stuff in store" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractSqlStoreTest7.class );
}
