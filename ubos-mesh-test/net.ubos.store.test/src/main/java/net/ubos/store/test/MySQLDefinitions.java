//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

/**
 * Defines the names of the MySQL databases and tables for the tests in this module
 * (not just package).
 */
public interface MySQLDefinitions
{
    /**
     * The name of the database that we use to store test data.
     */
    public static final String TEST_DATABASE_NAME = "test";

    /**
     * The name of the database user.
     */
    public static final String TEST_DATABASE_USER = "test";

    /**
     * The database user's password.
     */
    public static final String TEST_DATABASE_USER_PASSWORD = "test";

    /**
     * The name of the table that we use to store test data.
     */
    public static final String TEST_TABLE_NAME = "SqlStoreTest";
}
