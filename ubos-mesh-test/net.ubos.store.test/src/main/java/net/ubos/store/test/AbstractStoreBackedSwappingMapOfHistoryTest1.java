//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.store.history.StoreWithHistory;
import net.ubos.store.history.util.KeyAndStoreValueWithTimeUpdatedMapper;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedDecodingException;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedEncodingException;
import net.ubos.store.history.util.StoreValueWithTimeUpdatedMapper;
import net.ubos.store.history.util.StoreWithHistoryBackedSwappingSmartMapOfHistory;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.history.HasTimeUpdated;
import net.ubos.util.history.History;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests SmartMap&lt;History&gt;, backed by a Store, where both levels swap:
 * the outer SmartMap swaps, and the elements of the histories swap, too.
 */
public abstract class AbstractStoreBackedSwappingMapOfHistoryTest1
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating the Store" );

        theTestStore.initializeHard();

        //

        Map<String,Long []> testData = new HashMap<>();
        testData.put( "a", new Long[] { 0L, 1000L,       3000L } );
        testData.put( "b", new Long[] {            2000L } );
        testData.put( "c", new Long[] { 0L, 1000L, 2000L, 3000L } );
        testData.put( "d", new Long[] { 0L, 1000L, 3000L } );

        //

        KeyAndStoreValueWithTimeUpdatedMapper<String,Payload,StoreValueWithTimeUpdated> valueMapper = new KeyAndStoreValueWithTimeUpdatedMapper<>() {
            @Override
            public String getDefaultEncodingId()
            {
                return "my-default";
            }

            @Override
            public Payload decodeValue(
                    StoreValueWithTimeUpdated value )
                throws
                    StoreValueWithTimeUpdatedDecodingException
            {
                return new Payload( value.getData(), value.getTimeUpdated() );
            }

            @Override
            public StoreValueWithTimeUpdated encodeValue(
                    String key,
                    Payload value )
                throws
                    StoreValueWithTimeUpdatedEncodingException
            {
                return new StoreValueWithTimeUpdated( key, getDefaultEncodingId(), value.getTimeUpdated(), value.getData() );
            }

            @Override
            public String keyToString(
                    String key )
            {
                return key;
            }

            @Override
            public String stringToKey(
                    String stringKey )
                throws
                    ParseException
            {
                return stringKey;
            }
        };

        log.info( "Setup" );

        StoreWithHistoryBackedSwappingSmartMapOfHistory<String,Payload,StoreValueWithTimeUpdated> map
                = StoreWithHistoryBackedSwappingSmartMapOfHistory.Builder.create(
                        valueMapper,
                        theTestStore ).build();

        //

        log.info( "Insert" );

        for( Map.Entry<String,Long []> current : testData.entrySet() ) {
            History<Payload> history = map.get( current.getKey() ); // this "get" is actually a smart factory
            for( long value : current.getValue() ) {
                String data = String.format( "data-%s-%d", current.getKey(), value );
                history.putOrThrow( new Payload( data.getBytes(), value ));
            }
        }

        //

        log.info( "Look for the histories" );

        checkEquals( map.size(), testData.size(), "Wrong size" );
        for( Map.Entry<String,Long []> current : testData.entrySet() ) {
            History<Payload> history = map.get( current.getKey() );

            checkObject( history, "No history for " + current.getKey() );
        }

        for( String key : map.keyIterator() ) {
            History<Payload> history = map.get( key );
            Long []          data    = testData.get( key );

            checkObject( history, "No history for "   + key );
            checkObject( data,    "No test data for " + key );

            boolean found = false;
            for( History<Payload> history2 : map.valueIterator() ) {
                if( history == history2 ) {
                    found = true;
                    break;
                }
            }
            checkCondition( found, "History not found for " + key );
        }

        //

        log.info( "Look for the content of the histories" );

        for( String key : map.keyIterator() ) {
            History<Payload> history = map.get( key );
            Long []          data    = testData.get( key );

            checkEquals( history.getLength(), data.length, "Wrong length at " + key );

            HistoryCursorIterator<Payload> iter = history.iterator();
            iter.moveToBeforeFirst(); // default position is in the present
            for( int i=0 ; i<data.length ; ++i ) {
                Payload current = iter.next();
                checkEquals( current.getTimeUpdated(), (long) data[i], "Wrong timeUpdated at " + key );
            }
        }

    }

    // Our Logger
    private static final Log log = Log.getLogInstance( AbstractStoreBackedSwappingMapOfHistoryTest1.class );

    /**
     * The actual Store to be tested. This may or may not be pointed to theSqlStore
     * by subclasses.
     */
    protected StoreWithHistory<StoreValueWithTimeUpdated> theTestStore;

    /**
     * Payload class.
     */
    public static class Payload
        implements
            HasTimeUpdated
    {
        public Payload(
                byte [] data,
                long    timeUpdated )
        {
            theData           = data;
            theHasTimeUpdated = timeUpdated;
        }

        public byte [] getData()
        {
            return theData;
        }

        @Override
        public long getTimeUpdated()
        {
            return theHasTimeUpdated;
        }

        protected final byte [] theData;
        protected final long theHasTimeUpdated;
    }
}
