//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

import net.ubos.store.history.StoreValueWithTimeUpdated;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.store.history.StoreWithHistory;
import org.junit.jupiter.api.Test;

/**
 * Tests time-based navigation of any HistoryStoreIterator. Subclasses define the Store implementation.
 */
public abstract class AbstractHistoryStoreIteratorTest2
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating the Store" );

        theTestStore.initializeHard();

        //

        log.info( "Inserting data" );

        for( int i=0 ; i<testData.length ; ++i ) {
            StoreValueWithTimeUpdated current = testData[i];
            theTestStore.putOrUpdate( new StoreValueWithTimeUpdated(
                    current.getKey(),
                    current.getEncodingId(),
                    current.getTimeUpdated(),
                    current.getData() ));
        }

        HistoryCursorIterator<StoreValueWithTimeUpdated> iter = theTestStore.history( KEY ).iterator();
        // This puts the cursor at the most recent value, which is at the end of
        // the History, not the beginning

        //

        int count;

        log.info( "Navigating from beginning (just before)" );

        for( int i=0 ; i<testData.length ; ++i ) {
            iter.moveToBeforeFirst();

            int countBefore = iter.moveToJustBeforeTime( testData[i].getTimeUpdated() );
            checkEquals( countBefore, i, "Wrong before count at index " + i );
            checkEquals( testData[i].getTimeUpdated(), iter.peekNext().getTimeUpdated(), "Peek next before is wrong at index " + i );
        }

        iter.moveToBeforeFirst();

        count = iter.moveToJustBeforeTime( testData[0].getTimeUpdated() );
        checkEquals(  count, 0, "Wrong count at index last" );
        checkEquals( testData[0].getTimeUpdated(), iter.peekNext().getTimeUpdated(), "Peek next before is wrong at index 0" );

        iter.moveToBeforeFirst();

        count = iter.moveToJustBeforeTime( Long.MAX_VALUE );
        checkEquals(  count, testData.length, "Wrong count at index last" );
        checkEquals( testData[ testData.length-1 ].getTimeUpdated(), iter.peekPrevious().getTimeUpdated(), "Peek previous after is wrong at index last" );

        //

        log.info( "Navigating from beginning (just before minus 1)" );

        for( int i=0 ; i<testData.length ; ++i ) {
            iter.moveToBeforeFirst();

            int countBefore = iter.moveToJustBeforeTime( testData[i].getTimeUpdated() - 1L );
            checkEquals( countBefore, i, "Wrong before count at index " + i );
            checkEquals( testData[i].getTimeUpdated(), iter.peekNext().getTimeUpdated(), "Peek next before is wrong at index " + 1 );
        }

        iter.moveToBeforeFirst();

        count = iter.moveToJustBeforeTime( Long.MAX_VALUE - 1 );
        checkEquals(  count, testData.length, "Wrong count at index last" );
        checkEquals( testData[ testData.length-1 ].getTimeUpdated(), iter.peekPrevious().getTimeUpdated(), "Peek previous after is wrong at index last" );

        //

        log.info( "Navigating from beginning (just after)" );

        for( int i=0 ; i<testData.length ; ++i ) {
            iter.moveToBeforeFirst();

            int countAfter = iter.moveToJustAfterTime( testData[i].getTimeUpdated() );
            checkEquals( countAfter, i+1, "Wrong after count at index " + i );
            checkEquals( testData[i].getTimeUpdated(), iter.peekPrevious().getTimeUpdated(), "Peek previous after is wrong at index " + i );
        }

        //

        log.info( "Navigating from beginning (just after plus 1)" );

        for( int i=0 ; i<testData.length ; ++i ) {
            iter.moveToBeforeFirst();

            int countAfter = iter.moveToJustAfterTime( testData[i].getTimeUpdated() + 1L );
            checkEquals( countAfter, i+1, "Wrong after count at index " + i );
            checkEquals( testData[i].getTimeUpdated(), iter.peekPrevious().getTimeUpdated(), "Peek previous after is wrong at index " + i );
        }

        //

        log.info( "Navigating from end (just before)" );

        for( int i=0 ; i<testData.length ; ++i ) {
            iter.moveToAfterLast();

            int countBefore = iter.moveToJustBeforeTime( testData[i].getTimeUpdated() );
            checkEquals( countBefore, i-testData.length, "Wrong before count at index " + i );
            checkEquals( testData[i].getTimeUpdated(), iter.peekNext().getTimeUpdated(), "Peek next before is wrong at index " + 1 );
        }

        iter.moveToAfterLast();

        count = iter.moveToJustBeforeTime( testData[0].getTimeUpdated() );
        checkEquals(  count, -testData.length, "Wrong count at index 0" );
        checkEquals( testData[0].getTimeUpdated(), iter.peekNext().getTimeUpdated(), "Peek next after is wrong at index 0" );

        iter.moveToAfterLast();

        count = iter.moveToJustBeforeTime( Long.MAX_VALUE );
        checkEquals(  count, 0, "Wrong count at index last" );
        checkEquals( testData[ testData.length-1 ].getTimeUpdated(), iter.peekPrevious().getTimeUpdated(), "Peek previous after is wrong at index last" );

        //

        log.info( "Navigating from end (just before minus 1)" );

        for( int i=0 ; i<testData.length ; ++i ) {
            iter.moveToAfterLast();

            int countBefore = iter.moveToJustBeforeTime( testData[i].getTimeUpdated()-1 );
            checkEquals( countBefore, i-testData.length, "Wrong before count at index " + i );
            checkEquals( testData[i].getTimeUpdated(), iter.peekNext().getTimeUpdated(), "Peek next before is wrong at index " + 1 );
        }

        //

        log.info( "Navigating from end (just after)" );

        for( int i=0 ; i<testData.length ; ++i ) {
            iter.moveToAfterLast();

            int countAfter = iter.moveToJustAfterTime( testData[i].getTimeUpdated() );
            checkEquals( countAfter, i+1-testData.length, "Wrong after count at index " + i );
            checkEquals( testData[i].getTimeUpdated(), iter.peekPrevious().getTimeUpdated(), "Peek previous after is wrong at index " + i );
        }
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( AbstractHistoryStoreIteratorTest2.class );

    /**
     * The actual Store to be tested. This may or may not be pointed to theSqlStore
     * by subclasses.
     */
    protected StoreWithHistory<StoreValueWithTimeUpdated> theTestStore;

    private static final long START = 1000000L; // something memorable
    private static final long DELTA =    1000L;
    private static final String KEY = "mykey";
    private static final String ENC = "enc";

    /**
     * Test data.
     */
    protected static final StoreValueWithTimeUpdated[] testData = new StoreValueWithTimeUpdated[ 100 ];
    static {
        for( int i=0 ; i<testData.length ; ++i ) {
            testData[i] = new StoreValueWithTimeUpdated( KEY, ENC, START + DELTA * i, bytes( "some data at " + ( START + DELTA * i ) ));
        }
    };
}
