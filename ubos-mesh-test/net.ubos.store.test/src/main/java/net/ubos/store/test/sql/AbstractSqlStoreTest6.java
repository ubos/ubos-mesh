//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import net.ubos.store.StoreValue;
import net.ubos.store.prefixing.PrefixingStore;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests iteration over a PrefixingStore.
 */
public abstract class AbstractSqlStoreTest6
        extends
            AbstractSqlStoreTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @Override
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        log.info( "Deleting old database and creating new database" );

        theSqlStore.initializeHard();

        final String prefix1 = "one-";
        final String prefix2 = "two-";

        PrefixingStore<StoreValue> store1 = PrefixingStore.create( prefix1, theSqlStore );
        PrefixingStore<StoreValue> store2 = PrefixingStore.create( prefix2, theSqlStore );

        //

        log.info( "Inserting data" );

        for( int i=0 ; i<firstSet.length ; ++i ) {
            TestData current = firstSet[i];
            store1.put( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));
        }
        for( int i=0 ; i<secondSet.length ; ++i ) {
            TestData current = secondSet[i];
            store2.put( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));
        }

        //

        checkEquals( theSqlStore.size(), firstSet.length + secondSet.length, "Wrong size of SqlStore" );
        checkEquals( store1.size(), firstSet.length,  "Wrong size of store1" );
        checkEquals( store2.size(), secondSet.length, "Wrong size of store2" );

        //

        int count;

//        log.info( "Iterating over what's in the SQL Store" );
//
//        count = 0;
//        for( StoreValue current : theSqlStore ) {
//            log.traceMethodCallEntry( "Found " + count + ": " + current.getKey() );
//            ++count;
//            TestData found = null;
//            for( TestData data : firstSet ) {
//                if( ( prefix1 + " " + data.theKey ).equals( current.getKey() )) {
//                    found = data;
//                    break;
//                }
//            }
//            for( TestData data : secondSet ) {
//                if( ( prefix2 + " " + data.theKey ).equals( current.getKey() )) {
//                    found = data;
//                    break;
//                }
//            }
//            if( found == null ) {
//                reportError( "Could not find record with key", current.getKey() );
//            }
//        }
//        checkEquals( count, firstSet.length + secondSet.length, "wrong length of set" );

        //

        log.info( "Iterating over what's in store1" );

        count = 0;
        for( StoreValue current : store1 ) {
            log.debug( "Found " + count + ": " + current.getKey() );
            ++count;
            TestData found = null;
            for( TestData data : firstSet ) {
                if( data.theKey.equals( current.getKey() )) {
                    found = data;
                    break;
                }
            }
            if( found == null ) {
                reportError( "Could not find record with key", current.getKey() );
            }
        }
        checkEquals( count, firstSet.length, "wrong length of set" );

        //

        log.info( "Iterating over what's in store2" );

        count = 0;
        for( StoreValue current : store2 ) {
            log.debug( "Found " + count + ": " + current.getKey() );
            ++count;
            TestData found = null;
            for( TestData data : secondSet ) {
                if( data.theKey.equals( current.getKey() )) {
                    found = data;
                    break;
                }
            }
            if( found == null ) {
                reportError( "Could not find record with key", current.getKey() );
            }
        }
        checkEquals( count, secondSet.length, "wrong length of set" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractSqlStoreTest6.class);

    /**
     * Test data.
     */
    protected static final long now = System.currentTimeMillis();
    protected static final TestData[] firstSet = new TestData[] {
        new TestData( "a", "enc1",           bytes( "some data" )),
        new TestData( "b", "other encoding", bytes( "some longer data, but not very long" )),
        new TestData( "c", "Shakespeare's collected cucumbers", bytes( "other data" )),
        new TestData( "d", "enc1",           bytes( "aliergaierbg" )),
    };
    protected static final TestData[] secondSet = new TestData[] {
        new TestData( "e", "enc1",           bytes( "aqertghaqer" )),
        new TestData( "f", "enc1",           bytes( "qewrgqergqer" )),
        new TestData( "g", "enc1",           bytes( "zsdbgadgb" )),
        new TestData( "h", "enc1",           bytes( "afgae"  )),
        new TestData( "i", "enc1",           bytes( "qerg" )),
    };

    protected static class TestData
    {
        public TestData(
                String  key,
                String  encodingId,
                byte [] data )
        {
            theKey        = key;
            theEncodingId = encodingId;
            theData       = data;
        }

        @Override
        public String toString()
        {
            return "TestData with key '" + theKey + "'";
        }
        String  theKey;
        String  theEncodingId;
        byte [] theData;
    }
}
