//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test;

import java.util.NoSuchElementException;
import net.ubos.store.Store;
import net.ubos.store.StoreValue;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests zero-element CursorIterators.
 * Tests any StoreIterator. Subclasses define the Store implementation.
 */
public abstract class AbstractStoreIteratorTest3
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating the Store" );

        theTestStore.initializeHard();

        CursorIterator<StoreValue> iter = theTestStore.iterator();

        //

        log.info( "Check at the beginning" );

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( !iter.hasNext(), "has next at the end" );

        try {
            Object found = iter.peekNext();
            reportError( "Found element after end", found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }
        try {
            Object found = iter.peekPrevious();
            reportError( "Found element before beginning", found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }

        //

        log.info( "Copy" );

        CursorIterator<?> copy = iter.createCopy();

        checkCondition( !copy.hasPrevious(), "has previous at the beginning" );
        checkCondition( !copy.hasNext(), "has next at the end" );

        try {
            Object found = copy.peekNext();
            reportError( "Found element after end", found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }
        try {
            Object found = copy.peekPrevious();
            reportError( "Found element before beginning", found );
        } catch( NoSuchElementException t ) {
            log.debug( "Correctly received exception" );
        }

        //

        log.info( "Go to past last" );

        iter.moveToAfterLast();

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( !iter.hasNext(), "has next at the end" );

        //

        log.info( "Go before first" );

        iter.moveToBeforeFirst();

        checkCondition( !iter.hasPrevious(), "has previous at the beginning" );
        checkCondition( !iter.hasNext(), "has next at the end" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( AbstractStoreIteratorTest3.class );

    /**
     * The actual Store to be tested. This may or may not be pointed to theSqlStore
     * by subclasses.
     */
    protected Store<StoreValue> theTestStore;
}
