//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

import java.io.IOException;
import net.ubos.store.StoreKeyDoesNotExistException;
import net.ubos.store.StoreValue;
import net.ubos.util.ArrayHelper;
import net.ubos.util.StreamUtils;
import net.ubos.util.logging.Log;
import net.ubos.store.test.RecordingStoreListener;
import org.junit.jupiter.api.Test;
import net.ubos.store.StoreValueCursorIterator;

/**
 * Tests the basic SqlStore functions. See also FilesystemStoreTest1.
 */
public abstract class AbstractSqlStoreTest1
        extends
            AbstractSqlStoreTest
{
    /**
     * Run the test.
     *
     * @throws Exception thrown if an Exception occurred during the test
     */
    @Test
    @Override
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        log.info( "Deleting old database and creating new database" );

        theSqlStore.initializeHard();

        RecordingStoreListener<StoreValue> listener = new RecordingStoreListener<>( this );
        theTestStore.addDirectStoreListener( listener );

        //

        log.info( "Inserting data and checking it's there" );

        for( int i=0 ; i<firstSet.length ; ++i ) {
            TestData current = firstSet[i];
            theTestStore.put( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));
        }

        listener.checkPuts( firstSet.length, 0 ).checkUpdates( 0, 0 ).checkGets( 0, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        for( int i=0 ; i<firstSet.length ; ++i ) {
            TestData   current = firstSet[i];
            StoreValue value   = theTestStore.get( current.theKey );

            checkEquals(          current.theEncodingId, value.getEncodingId(), "not the same encodingId" );
            checkEqualByteArrays( current.theData,       value.getData(),       "not the same content" );
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( firstSet.length, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        //

        log.info( "Iterating over what's in the Store" );

        int count = 0;
        StoreValueCursorIterator<StoreValue> iter = theTestStore.iterator();
        while( iter.hasNext() ) {
            StoreValue current = iter.next();

            log.debug( "Found " + count + ": " + current.getKey() );
            ++count;
            TestData found = null;
            for( TestData data : firstSet ) {
                if( data.theKey.equals( current.getKey() )) {
                    found = data;
                    break;
                }
            }
            if( found == null ) {
                reportError( "Could not find record with key", current.getKey() );
            }
        }
        checkEquals( count, firstSet.length, "wrong length of set" );

        //

        log.info( "Updating data and checking it's there" );

        for( int i=0 ; i<secondSet.length ; ++i ) {
            TestData current = secondSet[i];
            theTestStore.update( new StoreValue(
                    current.theKey,
                    current.theEncodingId,
                    current.theData ));
        }

        listener.checkPuts( 0, 0 ).checkUpdates( secondSet.length, 0 ).checkGets( 0, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        for( int i=0 ; i<secondSet.length ; ++i ) {
            TestData   current = secondSet[i];
            StoreValue value   = theTestStore.get( current.theKey );

            checkEquals(          current.theEncodingId, value.getEncodingId(), "not the same encodingId" );
            checkEqualByteArrays( current.theData,       value.getData(),       "not the same content" );
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( secondSet.length, 0 ).checkDeletes( 0, 0, 0 );
        listener.reset();

        //

        log.info( "Deleting some data and checking it's gone" );

        for( int i=0 ; i<thirdSet.length ; ++i ) {
            TestData current = thirdSet[i];
            theTestStore.delete( current.theKey );
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( 0, 0 ).checkDeletes( thirdSet.length, 0, 0 );
        listener.reset();

        for( int i=0 ; i<thirdSet.length ; ++i ) {
            TestData current = thirdSet[i];
            try {
                theTestStore.get( current.theKey );

                reportError( "delete was unsuccessful", current );

            } catch( StoreKeyDoesNotExistException ex ) {
                // noop
            }
        }

        listener.checkPuts( 0, 0 ).checkUpdates( 0, 0 ).checkGets( 0, thirdSet.length ).checkDeletes( 0, 0, 0 );
        listener.reset();
    }

    // Our Logger
    private static final Log log = Log.getLogInstance(AbstractSqlStoreTest1.class);

    /**
     * Test data.
     */
    protected static final long now = System.currentTimeMillis();
    protected static final byte [] testImage;
    static {
        byte [] data = new byte[0];
        try {
            data = StreamUtils.slurp(AbstractSqlStoreTest1.class.getResourceAsStream( "TestImage.jpg" ));
        } catch( IOException ex ) {
            log.error( ex );
        }
        testImage = data;
    }
    protected static final TestData[] one = new TestData[] {
        new TestData( "/x/abc",       "enc1",           12345L, 67890L, bytes( "some data" )),
        new TestData( "/x/def",       "other encoding",    11L,    22L, bytes( "some longer data, but not very long" )),
        new TestData( "/x",           "third encoding",      1,      2, testImage )
    };
    protected static final TestData[] two = new TestData[] {
        new TestData( "/x/abc", "testing", 5555L, 666L, bytes( "some changed data" )),
    };
    protected static final TestData[] three = new TestData[] {
        new TestData( "/x/ghi", "Shakespeare's collected cucumbers", now,    now+1, bytes( "Some <b>HTML</b> data" ) )
    };
    protected static final TestData[] firstSet  = ArrayHelper.append( one, three, TestData.class );
    protected static final TestData[] secondSet = two;
    protected static final TestData[] thirdSet  = three;
}
