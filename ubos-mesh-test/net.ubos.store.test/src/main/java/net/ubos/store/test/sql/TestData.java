//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.store.test.sql;

/**
 * Holds test data for various tests.
 */
public class TestData
{
    public TestData(
            String  key,
            String  encodingId,
            long    timeCreated,
            long    timeUpdated,
            byte [] data )
    {
        theKey        = key;
        theEncodingId = encodingId;
        theData       = data;
    }

    @Override
    public String toString()
    {
        return "TestData with key '" + theKey + "'";
    }

    String  theKey;
    String  theEncodingId;
    byte [] theData;
}
