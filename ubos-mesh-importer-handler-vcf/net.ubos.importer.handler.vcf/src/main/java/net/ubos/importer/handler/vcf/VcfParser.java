//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.vcf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import net.ubos.util.logging.CanBeDumped;
import net.ubos.util.logging.Dumper;

/**
 * Knows how to parse a .vcf file, but not what to do with the data.
 */
public class VcfParser
{
    /**
     * Parse this stream.
     *
     * @param in the stream to be parsed
     * @return the found data
     * @throws SyntaxError
     * @throws IOException
     */
    public static ParsedVcf parse(
            InputStream in )
        throws
            SyntaxError,
            IOException
    {
        // strategy:
        // 1) read the whole thing into memory, while doing so, merge "continuing" lines and
        //    break into name / parameter / value portions
        // 2) parse

    // part 1, read

        ArrayList<OneLine> oneLines = new ArrayList<>();
        BufferedReader     r        = new BufferedReader( new InputStreamReader( in, StandardCharsets.UTF_8 )); // what's the charset?

        String currentLine = r.readLine(); // the line currently being assembled
        if( currentLine == null ) {
            return null;
        }

        while( true ) {
            String nextLine = r.readLine();
            if( nextLine != null && nextLine.startsWith( " " )) {
                currentLine = currentLine + nextLine.substring( 1 );
                continue;
            }

            // now we have a full line in currentLine, and the beginnings of the next
            // line in nextLine
            OneLine parsed = OneLine.parse( currentLine );
            oneLines.add( parsed );

            if( nextLine == null ) {
                break;
            }

            currentLine = nextLine; // pass on
        }

    // part 2: parse
        ParsedVcf ret = new ParsedVcf();

        Iterator<OneLine> oneLineIter = oneLines.iterator();
        while( oneLineIter.hasNext() ) {
            OneLine current = oneLineIter.next();

            switch( current.theName ) {
                case "BEGIN":
                    if( ! "VCARD".equalsIgnoreCase( current.theValue )) {
                        throw new SyntaxError( "No BEGIN:VCARD keyword" );
                    }
                    ParsedVcf.VCard found = handleBegin( oneLineIter );
                    ret.theVCards.add( found );
                    break;

                default:
                    throw new SyntaxError( "Unexpected keyword: " + current.theName );
            }
        }

        return ret;
    }

    /**
     * The BEGIN keyword has been found.
     *
     * @param oneLineIter read next line from here
     * @return the newly allocated VCard object
     */
    protected static ParsedVcf.VCard handleBegin(
            Iterator<OneLine> oneLineIter )
        throws
            SyntaxError
    {
        ParsedVcf.VCard ret = new ParsedVcf.VCard();

        // Google seems to be using only a small subset of the format

        while( oneLineIter.hasNext() ) {
            OneLine current = oneLineIter.next();

            switch( current.theName ) {
                case "END":
                    return ret; // we are done

                 case "FN":
                     handleFN( current, ret );
                     break;

                 case "N":
                     handleN( current, ret );
                     break;

                 case "EMAIL":
                     handleEMAIL( current, ret );
                     break;

                 case "URL":
                     handleURL( current, ret );
                     break;

                 case "X-ABLabel":
                     handleABLabel( current, ret );
                     break;
            }
        }
        throw new SyntaxError( "Unexpected end of file" );
    }

    protected static void handleFN(
            OneLine         fnLine,
            ParsedVcf.VCard vcard )
    {
        vcard.fn = fnLine.theValue;
    }

    /**
     * Handle the value for a "N" entry.
     */
    protected static void handleN(
            OneLine         nameLine,
            ParsedVcf.VCard vcard )
    {
        String [] nameComponents = nameLine.theValue.split( ";" );
        String familyName        = null;
        String givenName         = null;
        String additionalNames   = null;
        String honorificPrefixes = null;
        String honorificSuffixes = null;

        switch( nameComponents.length ) {
            case 5:
                vcard.honorificSuffixes = nameComponents[4]; // no break
            case 4:
                vcard.honorificPrefixes = nameComponents[3]; // no break
            case 3:
                vcard.additionalNames = nameComponents[2]; // no break
            case 2:
                vcard.givenName = nameComponents[1]; // no break
            case 1:
                vcard.familyName = nameComponents[0]; // no break
            default:
                // do nothing
        }
    }

    protected static void handleEMAIL(
            OneLine         emailLine,
            ParsedVcf.VCard vcard )
    {
        if( emailLine.theGroup == null ) {
            vcard.emailAddresses.add( emailLine.theValue );
        } else {
            vcard.groupEmail.put( emailLine.theGroup, emailLine.theValue );
        }
    }

    protected static void handleURL(
            OneLine         urlLine,
            ParsedVcf.VCard vcard )
    {
        if( urlLine.theGroup == null ) {
            vcard.urls.add( urlLine.theValue );
        } else {
            vcard.groupUrl.put( urlLine.theGroup, urlLine.theValue );
        }
    }

    protected static void handleABLabel(
            OneLine         ablabelLine,
            ParsedVcf.VCard vcard )
    {
        vcard.groupABLabel.put( ablabelLine.theGroup, ablabelLine.theValue );
    }

    /**
     * This class contains the content of one unfolded RFC 2425 line as a structure.
     */
    static class OneLine
            implements
                CanBeDumped
    {
        /**
         * Constructor with the components.
         *
         * @param group the Group field in a VCard line
         * @param name the Name field in a VCard line
         * @param params the Params field in a VCard line
         * @param value the Value field in a VCard line
         */
        public OneLine(
                String group,
                String name,
                String [] params,
                String value )
        {
            theGroup  = group;
            theName   = name;
            theParams = params;
            theValue  = value;
        }

        /**
         * Parse a line into its components.
         *
         * @param raw the string to be parsed
         * @return found and instantiated object at this line
         */
        @SuppressWarnings( "fallthrough" )
        public static OneLine parse(
                String raw )
        {
            char [] c = raw.toCharArray();

            String            group      = null;
            String            name       = null;
            ArrayList<String> params     = null;
            String            value      = null;
            int               dotIndex   = -1;
            int               semiIndex  = -1;
            int               colonIndex = -1;
            boolean           escape     = false;

            for( int i=0 ; i<c.length ; ++i ) {
                if( escape ) {
                    escape = false;

                } else {
                    switch( c[i] ) {
                        case '\\':
                            escape = true;
                            break;

                        case ':':
                            colonIndex = i;
                            // NO break here

                        case ';':
                            if( semiIndex < 0 ) {
                                if( dotIndex >= 0 ) {
                                    group = raw.substring( 0, dotIndex );
                                    name  = raw.substring( dotIndex+1, i );
                                } else {
                                    group = null;
                                    name  = raw.substring( 0, i );
                                }
                            } else if( colonIndex < 0 || c[i] == ':' ) { // further parameter
                                if( params == null ) {
                                    params = new ArrayList<>();
                                }
                                params.add( raw.substring( semiIndex+1, i ));
                            }
                            semiIndex = i;
                            break;

                        case '.':
                            if( dotIndex == -1 ) {
                                dotIndex = i;
                            }
                            break;
                    }
                }
            }
            if( colonIndex >=0 ) {
                value = raw.substring( colonIndex+1 );
            }

            group = stripBackslash( group );
            name  = stripBackslash( name );
            value = stripBackslash( value );

            String [] paramsAsStrings;
            if( params == null ) {
                paramsAsStrings = new String[0];
            } else {
                paramsAsStrings = new String[ params.size() ];
                for( int i=0 ; i<paramsAsStrings.length ; ++i ) {
                    paramsAsStrings[i] = stripBackslash( params.get( i ));
                }
            }

            return new OneLine(
                    group,
                    name,
                    paramsAsStrings,
                    value );
        }

        /**
         * Helper to unescape \ in a String.
         *
         * @param in the String potentially containing \, or null
         * @return the unescaped String, or null
         */
        protected static String stripBackslash(
                String in )
        {
            if( in == null ) {
                return in;
            }
            int where = in.indexOf( '\\' );
            if( where < 0 ) {
                return in;
            }
            StringBuilder ret = new StringBuilder();
            ret.append( in.substring( 0, where ));

            int start = where + 1; // start after the \\
            while( ( where = in.indexOf( '\\', start + 1 )) >= 0 ) { // if \\ is repeated, pass it on
                ret.append( in.substring( start, where ));
                start = where + 1;
            }
            ret.append( in.substring( start ));
            return ret.toString();
        }

        /**
         * Dump this object.
         *
         * @param d the Dumper to dump to
         */
        public void dump(
                Dumper d )
        {
            d.dump( this,
                    new String [] {
                        "group",
                        "name",
                        "params",
                        "value"
                    },
                    new Object [] {
                        theGroup,
                        theName,
                        theParams,
                        theValue
                    } );
        }

        /**
         * Component of one unfolded line.
         */
        public String    theGroup;

        /**
         * Component of one unfolded line.
         */
        public String    theName;

        /**
         * Component of one unfolded line.
         */
        public String [] theParams;

        /**
         * Component of one unfolded line.
         */
        public String    theValue;
    }


    /**
     * SYntax error.
     */
    public static class SyntaxError
        extends
            Exception
    {
        public SyntaxError(
                String msg )
        {
            super( msg );
        }
    }
}
