//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.vcf;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import java.util.regex.Pattern;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Contact.ContactSubjectArea;
import net.ubos.model.primitives.StringValue;

/**
 * Knows how to handle a VCard file.
 */
public class DefaultVcfImporterHandler
    extends
        AbstractBasicFileImporterHandler
{
    /**
     * Constructor with default.
     *
     * @param okScore when importing works, what score should be reported
     */
    public DefaultVcfImporterHandler(
            double  okScore )
    {
        super( DEFAULT_VCF_FILENAME_PATTERN, okScore );
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     */
    public DefaultVcfImporterHandler(
            String  filenamePattern,
            double  okScore )
    {
        super( Pattern.compile( filenamePattern ), okScore );
    }

    /**
     * Constructor.
     *
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     * @param okScore when importing works, what score should be reported
     */
    public DefaultVcfImporterHandler(
            Pattern filenamePattern,
            double  okScore )
    {
        super( filenamePattern, okScore );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream() ) {
            return IMPOSSIBLE;
        }
        double ret = theOkScore;

        MeshObject              hereObject = toBeImported.getHereMeshObject();
        EditableHistoryMeshBase mb         = context.getMeshBase();

        ParsedVcf parsed;
        try {
            parsed = VcfParser.parse( toBeImported.createStream() );

//            mb.executeAt( context.getTimeOfExport(),
//                          ( Transaction tx ) -> {
                                hereObject.bless( ContactSubjectArea.CONTACTCOLLECTION );

                                for( ParsedVcf.VCard vc : parsed.theVCards ) {
                                    String fullName = unquote( vc.fn );

                                    String idCandidate;
                                    if( fullName == null || fullName.isBlank() ) {
                                        idCandidate = "contacts/names/unnamed";
                                    } else {
                                        idCandidate = "contacts/names/" + fullName;
                                    }

                                    MeshObject contactObj = ImporterUtils.createWithoutConflict(
                                            idCandidate,
                                            ContactSubjectArea.CONTACT,
                                            mb );

                                    contactObj.setPropertyValue( ContactSubjectArea.CONTACT_FIRSTNAME,  StringValue.createOrNull( unquote( vc.givenName )));
                                    contactObj.setPropertyValue( ContactSubjectArea.CONTACT_MIDDLENAME, StringValue.createOrNull( unquote( vc.additionalNames )));
                                    contactObj.setPropertyValue( ContactSubjectArea.CONTACT_LASTNAME,   StringValue.createOrNull( unquote( vc.familyName )));
                                    contactObj.setPropertyValue( ContactSubjectArea.CONTACT_FULLNAME,   StringValue.createOrNull( fullName ));
                                    contactObj.setPropertyValue( ContactSubjectArea.CONTACT_HONORIFICPREFIXES, StringValue.createOrNull( unquote( vc.honorificPrefixes )));
                                    contactObj.setPropertyValue( ContactSubjectArea.CONTACT_HONORIFICSUFFIXES, StringValue.createOrNull( unquote( vc.honorificSuffixes )));

                                    for( String email : vc.emailAddresses ) {
                                        MeshObject emailObj = ImporterUtils.createWithoutConflict(
                                                mb.createMeshObjectIdentifierBelow( contactObj.getIdentifier(), cleanupUrlForIdentifier( email )),
                                                ContactSubjectArea.ONLINELOCATION,
                                                mb );
                                        emailObj.setPropertyValue( ContactSubjectArea.ONLINELOCATION_ADDRESS, StringValue.create( email ));
                                        emailObj.setPropertyValue( ContactSubjectArea.ONLINELOCATION_TYPE, ContactSubjectArea.ONLINELOCATION_TYPE_type_EMAIL );

                                        contactObj.blessRole( ContactSubjectArea.CONTACT_HASPRESENCEAT_ONLINELOCATION_S , emailObj );
                                    }

                                    for( Map.Entry<String,String> groupEmail : vc.groupEmail.entrySet() ) {
                                        MeshObject groupEmailObj = ImporterUtils.createWithoutConflict(
                                                mb.createMeshObjectIdentifierBelow( contactObj.getIdentifier(), cleanupUrlForIdentifier( groupEmail.getValue() )),
                                                ContactSubjectArea.ONLINELOCATION,
                                                mb );
                                        groupEmailObj.setPropertyValue( ContactSubjectArea.ONLINELOCATION_ADDRESS, StringValue.create( groupEmail.getValue() ));
                                        groupEmailObj.setPropertyValue( ContactSubjectArea.ONLINELOCATION_TYPE, ContactSubjectArea.ONLINELOCATION_TYPE_type_EMAIL );

                                        contactObj.blessRole( ContactSubjectArea.CONTACT_HASPRESENCEAT_ONLINELOCATION_S, groupEmailObj );

                                        String abLabel = vc.groupABLabel.get( groupEmail.getKey() );
                                        if( abLabel != null && !abLabel.isEmpty() ) {
                                            contactObj.setRoleAttributeValue( groupEmailObj, "ABLabel", abLabel );
                                        }
                                    }

                                    for( String url : vc.urls ) {
                                        MeshObject urlObj = ImporterUtils.createWithoutConflict(
                                                mb.createMeshObjectIdentifierBelow( contactObj.getIdentifier(), cleanupUrlForIdentifier( url )),
                                                ContactSubjectArea.ONLINELOCATION,
                                                mb );
                                        urlObj.setPropertyValue( ContactSubjectArea.ONLINELOCATION_ADDRESS, StringValue.create( url ));
                                        // missing: type

                                        contactObj.blessRole( ContactSubjectArea.CONTACT_HASPRESENCEAT_ONLINELOCATION_S, urlObj );
                                    }

                                    for( Map.Entry<String,String> groupUrl : vc.groupUrl.entrySet() ) {
                                        MeshObject groupUrlObj = ImporterUtils.createWithoutConflict(
                                                mb.createMeshObjectIdentifierBelow( contactObj.getIdentifier(), cleanupUrlForIdentifier( groupUrl.getValue() )),
                                                ContactSubjectArea.ONLINELOCATION,
                                                mb );
                                        groupUrlObj.setPropertyValue( ContactSubjectArea.ONLINELOCATION_ADDRESS, StringValue.create( groupUrl.getValue() ));
                                        // missing: type

                                        contactObj.blessRole( ContactSubjectArea.CONTACT_HASPRESENCEAT_ONLINELOCATION_S , groupUrlObj );

                                        String abLabel = vc.groupABLabel.get( groupUrl.getKey() );
                                        if( abLabel != null && !abLabel.isEmpty() ) {
                                            contactObj.setRoleAttributeValue( groupUrlObj, "ABLabel", abLabel );
                                        }
                                    }
                                }
//                                return null;
//                          } );

        } catch( VcfParser.SyntaxError ex ) {
            ret = IMPOSSIBLE;
        }

        return ret;
    }

    /**
     * Unquote a String if needed
     *
     * @param raw the possibly quoted String
     * @return the unquoted String
     */
    protected String unquote(
            String raw )
    {
        if( raw == null ) {
            return raw;
        }

        final String [] QUOTES = new String[] { "\"", "'" };

        String ret = raw;

        for( String quote : QUOTES ) {
            if( raw.startsWith( quote ) && raw.endsWith( quote )) {
                ret = ret.substring( 1, ret.length()-2 );
            }
        }
        return ret;
    }

    /**
     * Construct a String from an e-mail or URL that can be used as a reasonably
     * stable relative MeshObjectIdentifier.
     *
     * @param s the e-mail or URL
     * @return the String
     */
    protected String cleanupUrlForIdentifier(
            String s )
    {
        // \"http\://netmesh.info/jernst\"@netmesh.us
        // netmesh.info/jernst@netmesh.us

        String ret = s.replaceAll( "[\"/:]", "" );
        ret = ret.replaceAll( "https?", "" );

        return ret;
    }

    /**
     * The default pattern of file names we support.
     */
    public static final Pattern DEFAULT_VCF_FILENAME_PATTERN = Pattern.compile( ".*\\.vcf$", Pattern.CASE_INSENSITIVE );
}
