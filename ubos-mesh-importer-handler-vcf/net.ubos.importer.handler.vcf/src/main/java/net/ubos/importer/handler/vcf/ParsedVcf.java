//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.handler.vcf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The result of parsing one .vcf file.
 */
public class ParsedVcf
{
    /**
     * The set of VCards found.
     */
    public ArrayList<VCard> theVCards = new ArrayList<>();

    /**
     * Represents a single VCard
     */
    public static class VCard
    {
        public String fn;
        public String honorificSuffixes;
        public String honorificPrefixes;
        public String additionalNames;
        public String givenName;
        public String familyName;
        public List<String> emailAddresses = new ArrayList<>(); // not in a group
        public List<String> urls           = new ArrayList<>(); // not in a group

        public Map<String,String> groupEmail   = new HashMap<>(); // group -> e-mail
        public Map<String,String> groupUrl     = new HashMap<>(); // group -> URL
        public Map<String,String> groupABLabel = new HashMap<>(); // group -> ABLabel
    }
}
